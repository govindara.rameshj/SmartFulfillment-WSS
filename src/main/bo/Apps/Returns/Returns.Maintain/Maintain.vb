﻿Imports DevExpress.XtraGrid
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid.Columns
Imports BOPurchases
Imports Cts.Oasys.WinForm

Public Class Maintain
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _retHeader As New BOPurchases.cReturnHeader(_Oasys3DB)
    Private _supNotes As New BOPurchases.cSupplierNote(_Oasys3DB)
    Private _factory As IRefreshReturnsHeader = (New RefreshReturnsHeaderFactory).GetImplementation
    Private _openAction As String = "open"
    Private _printAction As String = "print"
    Private _releaseAction As String = "release"
    Private _deleteAction As String = "delete"
    Private _qtyLimit As Integer = 0
    Private _buttonPlaceHolderHeight As Integer = 45
    Private _returnDetails As New BOPurchases.cReturnLine(_Oasys3DB)
    Private _returnDetailsCurrencyManager As CurrencyManager
    Private _defaultQuantity As Integer = 0
    Private _focusedHeaderRowChanging As Boolean = False
    Private _newDetailRowAdding As Boolean = False
    Private _focusedHeaderRowChangingDisable As Boolean = False
    Private _LowLevel As Decimal = 0

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()

        Select Case RunParameters.ToLower.Trim
            Case _openAction
                btnCommand.Text = My.Resources.F5OpenReturns
                grpReturn.Text = My.Resources.SelectPrint
                btnPrintSummary.Visible = True
                Dim Param As New BOSystem.cParameter(_Oasys3DB)
                _qtyLimit = Param.GetParameterInteger(3650)
                _LowLevel = Param.GetParameterDecimal(3660)
                btnAddItem.Enabled = True
                xgdDetail.Height = xgdDetail.Height - _buttonPlaceHolderHeight
            Case _printAction
                btnCommand.Text = My.Resources.F5Print
                grpReturn.Text = My.Resources.SelectPrint
            Case _releaseAction
                btnCommand.Text = My.Resources.F5Release
                grpReturn.Text = My.Resources.SelectRelease
            Case _deleteAction
                btnCommand.Text = My.Resources.F5Delete
                grpReturn.Text = My.Resources.SelectDelete
        End Select

    End Sub

    Private Sub CurrencyManager_ItemChanged(ByVal sender As Object, ByVal e As ItemChangedEventArgs)
        ChangeButtonsEnableState()
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5 : btnCommand.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Keys.F6 : btnPrintSummary.PerformClick()
            Case Keys.F7 : btnPrintAll.PerformClick()
            Case Else : e.Handled = False
        End Select
    End Sub

    Protected Overrides Sub DoProcessing()

        Try
            Trace.WriteLine(My.Resources.AppStart, Name)
            Cursor = Cursors.WaitCursor

            'load open returns
            _retHeader.TableLoadOpenReturns()

            'check that there are any returns to process if not reprint mode and exit if so
            If btnCommand.Text <> My.Resources.F5Print AndAlso _retHeader.Table.Rows.Count = 0 Then
                MessageBox.Show(My.Resources.NoReturnsAndExit, Me.FindForm.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                FindForm.Close()
            Else
                'else load return headers into grid control
                xgdHeader_LoadHeaders()
            End If

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub


    Private Sub xgdHeader_LoadHeaders()

        If IsOpenReturnsAction() Then
            'Preventing addition redundant handlers
            RemoveHandler FindForm.FormClosing, AddressOf Form_OnClosing

            AddHandler FindForm.FormClosing, AddressOf Form_OnClosing
        End If

        'create masks for column formatting
        Dim riDate As New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        riDate.Mask.EditMask = "dd/MM/yyyy"
        riDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        riDate.Mask.UseMaskAsDisplayFormat = True

        Dim riCurrency As New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        riCurrency.Mask.EditMask = "c2"
        riCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        riCurrency.Mask.UseMaskAsDisplayFormat = True

        'set column headings, visibility and order for table
        xgdHeader.DataSource = _retHeader.Table
        Dim col As DevExpress.XtraGrid.Columns.GridColumn

        For index As Integer = 0 To xgvHeader.Columns.Count - 1
            col = xgvHeader.Columns(index)
            Select Case index
                Case BOPurchases.cReturnHeader.TableCol.Number
                    col.Caption = My.Resources.ReturnNumberColumnCaption
                    col.VisibleIndex = 0

                Case BOPurchases.cReturnHeader.TableCol.SupplierNumber
                    col.Caption = My.Resources.SupplierColumnCaption

                Case BOPurchases.cReturnHeader.TableCol.SupplierName
                    col.Caption = My.Resources.SupplierNameColumnCaption

                Case BOPurchases.cReturnHeader.TableCol.DateCreated
                    col.Caption = My.Resources.DateCreatedColumnCaption
                    col.ColumnEdit = riDate

                Case BOPurchases.cReturnHeader.TableCol.DateExpectCollect
                    col.Caption = My.Resources.DateCollectionExpectedColumnCaption
                    col.ColumnEdit = riDate

                Case BOPurchases.cReturnHeader.TableCol.Value
                    col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                    col.Caption = My.Resources.ValueColumnCaption
                    col.ColumnEdit = riCurrency
                    col.SummaryItem.FieldName = xgvHeader.Columns(index).FieldName
                    col.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum

                Case Else
                    col.Visible = False
            End Select
        Next

        xgvHeader.BestFitColumns()
        xgvHeader_FocusedRowChanged(xgvHeader, New DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs(-1, 0))
        xgdHeader.Focus()

    End Sub

    Private Sub xgvHeader_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles xgvHeader.FocusedRowChanged
        If _focusedHeaderRowChangingDisable Then
            Return
        End If

        If _focusedHeaderRowChanging Then
            _focusedHeaderRowChanging = False
            Return
        End If

        If Not IsCanLeaveReturnDetails() Then
            _focusedHeaderRowChanging = True
            xgvHeader.FocusedRowHandle = e.PrevFocusedRowHandle
            Return
        End If

        Dim view As GridView = CType(sender, GridView)
        If view.RowCount = 0 Then Exit Sub
        If view.Columns.Count = 0 Then Exit Sub

        LoadDetails(view)

        'get supplier return policy checking if different from whats already there
        Dim supplierNumber As String = CStr(view.GetRowCellValue(e.FocusedRowHandle, view.Columns(BOPurchases.cReturnHeader.TableCol.SupplierNumber)))
        If CStr(lstPolicy.Tag) <> supplierNumber Then
            _supNotes.TableLoadReturnPolicy(CInt(supplierNumber))
            lstPolicy.Items.Clear()
            lstPolicy.Tag = supplierNumber

            For Each dr As DataRow In _supNotes.Table.Rows
                lstPolicy.Items.Add(dr(BOPurchases.cSupplierNote.TableCol.Text))
            Next
        End If

    End Sub

    Private Sub xgvHeader_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles xgvHeader.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            btnCommand.PerformClick()
        End If

    End Sub

    Private Sub xgvDetail_LoadDetails(ByVal headerId As Integer)

        'load details
        _returnDetails.TableLoadDetails(headerId)
        _returnDetails.Table.AcceptChanges()

        'remove old details change handler
        If _returnDetailsCurrencyManager IsNot Nothing Then
            RemoveHandler _returnDetailsCurrencyManager.ItemChanged, AddressOf CurrencyManager_ItemChanged
        End If

        'check if columns not setup
        If xgvDetail.Columns.Count = 0 Then
            'create currency mask
            Dim riCurrency As New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
            riCurrency.Mask.EditMask = "c2"
            riCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
            riCurrency.Mask.UseMaskAsDisplayFormat = True

            'set column headings, visibility and order for table
            xgdDetail.DataSource = _returnDetails.Table
            xgvDetail.BestFitColumns()

            For index As Integer = 0 To xgvDetail.Columns.Count - 1
                Dim col As DevExpress.XtraGrid.Columns.GridColumn
                col = GetDetailColumn(index)

                Select Case index
                    Case BOPurchases.cReturnLine.TableCol.SkuNumber
                        col.Caption = My.Resources.SkuColumnCaption
                        col.VisibleIndex = 0
                        DisableEditForDetailColumn(col)

                    Case BOPurchases.cReturnLine.TableCol.SkuDescription
                        col.Caption = My.Resources.DescriptionColumnCaption
                        DisableEditForDetailColumn(col)

                    Case BOPurchases.cReturnLine.TableCol.Quantity
                        col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                        col.Caption = My.Resources.QuantityColumnCaption
                        col.SummaryItem.FieldName = GetDetailColumnFieldName(index)
                        col.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                        EnableEditForDetailColumn(col)

                    Case BOPurchases.cReturnLine.TableCol.Price
                        col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                        col.Caption = My.Resources.PriceColumnCaption
                        col.ColumnEdit = riCurrency
                        DisableEditForDetailColumn(col)

                    Case BOPurchases.cReturnLine.TableCol.Value
                        col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                        col.Caption = My.Resources.ValueColumnCaption
                        col.ColumnEdit = riCurrency
                        col.SummaryItem.FieldName = GetDetailColumnFieldName(index)
                        col.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                        DisableEditForDetailColumn(col)

                    Case BOPurchases.cReturnLine.TableCol.ReasonCode
                        col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                        col.Caption = My.Resources.ReasonColumnCaption
                        AddResonCodeEditor(col)

                    Case BOPurchases.cReturnLine.TableCol.ReasonDescription
                        col.Caption = My.Resources.DescriptionColumnCaption
                        DisableEditForDetailColumn(col)
                    Case Else
                        col.Visible = False
                        DisableEditForDetailColumn(col)
                End Select
            Next
        Else
            xgdDetail.DataSource = _returnDetails.Table
        End If

        'add new details change handler
        _returnDetailsCurrencyManager = CType(Me.BindingContext(xgdDetail.DataSource), Windows.Forms.CurrencyManager)
        AddHandler _returnDetailsCurrencyManager.ItemChanged, AddressOf CurrencyManager_ItemChanged

        If IsOpenReturnsAction() Then
            ChangeButtonsEnableState()
        End If

        If Not IsDeleteReturnsAction() Then
            'set enabled state of F5 button
            btnCommand.Enabled = (_returnDetails.Table.Rows.Count > 0)
        End If

    End Sub


    Private Sub btnF5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCommand.Click
        ExecuteCommand(btnCommand.Text, My.Resources.YesNoAreYouSure)
    End Sub

    Private Sub ExecuteCommand(ByVal commandName As String, ByRef deletionWarningText As String)

        Try
            'get row from header view to create business object
            Dim dr As DataRow = xgvHeader.GetDataRow(xgvHeader.FocusedRowHandle)
            _retHeader = _factory.ReturnsHeader(_retHeader, _Oasys3DB, dr)

            Select Case commandName
                Case My.Resources.F5Delete
                    'get confimation
                    Dim result As DialogResult = MessageBox.Show(deletionWarningText, Me.FindForm.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                    If result <> DialogResult.Yes Then Exit Sub

                    _retHeader.UpdateDeleted(MyBase.UserId)
                    MessageBox.Show(My.Resources.DeleteSuccessfulFor & _retHeader.Number.Value, Me.FindForm.Text)
                    DeleteCurrentHeaderRow()

                Case My.Resources.F5Print
                    PrintReport(_retHeader)

                Case My.Resources.F5OpenReturns
                    Dim print As New CtsControls.PrintOpenReturns(_Oasys3DB, _retHeader, xgdDetail)
                    print.PrintCreationReports(True)

                Case My.Resources.F5Release
                    Dim ReturnPolicyRef As Decimal = _retHeader.Supplier.ReturnDetails.ReturnsMessageRef.Value

                    'load comment input box with return policy message
                    Dim comment As String = String.Empty
                    If ReturnPolicyRef = 0 Then
                        comment = InputBox(My.Resources.PleaseEnterComment, Me.FindForm.Text)
                    Else
                        Dim Parameter As New BOSystem.cParameter(_Oasys3DB)
                        Dim message As New MessageInput(Parameter.GetParameterString(CInt(3600 + ReturnPolicyRef)).Trim)
                        If message.ShowDialog(Me) = DialogResult.OK Then
                            comment = message.Message
                        Else
                            Exit Sub
                        End If
                        message.Dispose()
                    End If

                    comment = _factory.LimitCommentTo20Characters(comment)
                    DisplayStatus(My.Resources.ReleasingReturn)
                    Cursor = Cursors.WaitCursor

                    _retHeader.ReleaseReturn(MyBase.UserId, comment)
                    MessageBox.Show(My.Resources.ReleaseSuccessfulFor & _retHeader.DrlNumber.Value, Me.FindForm.Text)

                    'print copies
                    Dim print As New CtsControls.PrintReturns(_Oasys3DB, _retHeader, xgdDetail)
                    print.PrintReleaseReports()

                    'remove row from header view
                    DeleteCurrentHeaderRow()
            End Select

            'check that there are any returns to process if not reprint mode and exit if so
            _factory.ReturnsReleaseMessage(btnCommand.Text, Me.FindForm, _retHeader, My.Resources.F5Print, My.Resources.NoReturnsAndExit)

            xgvHeader_FocusedRowChanged(xgvHeader, New DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs(-1, xgvHeader.FocusedRowHandle))

            ChangeHeaderButtonsEnableState()
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub DeleteCurrentHeaderRow()
        _focusedHeaderRowChangingDisable = True
        xgvHeader.DeleteRow(xgvHeader.FocusedRowHandle)
        _focusedHeaderRowChangingDisable = False
        xgdDetail.DataSource = Nothing
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        FindForm.Close()
    End Sub

    Private Sub Form_OnClosing(sender As Object, e As System.ComponentModel.CancelEventArgs)
        If (Not IsCanLeaveReturnDetails()) Then
            e.Cancel = True
        Else
            RemoveHandler FindForm.FormClosing, AddressOf Form_OnClosing
        End If
    End Sub

    Private Sub btnPrintSummary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintSummary.Click
        Dim assemblyName As String = "Reporting.dll"
        Dim className As String = "Reporting.ReportViewer"
        Dim appName As String = "Open Returns Summary"
        Dim parameters As String = "500"

        Trace.WriteLine("Launching Open Returns Summary Report")

        Dim hostForm As New Cts.Oasys.WinForm.HostForm(assemblyName, className, appName, 0, 1, 9, parameters, "", True)
        hostForm.ShowDialog()
    End Sub

    Private Sub btnPrintAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintAll.Click
        'For Each openReturn As DataRow In _retHeader.Table

        'Next
    End Sub

    Private Sub xgvDetail_CellValueChanged(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles xgvDetail.CellValueChanged
        Dim view As GridView = CType(sender, GridView)
        Dim quantityFieldName = GetDetailColumnFieldName(BOPurchases.cReturnLine.TableCol.Quantity)
        Dim reasonFieldName = GetDetailColumnFieldName(BOPurchases.cReturnLine.TableCol.ReasonCode)
        Dim dr As DataRow = view.GetDataRow(e.RowHandle)

        If dr Is Nothing Then
            Return
        End If

        Select Case e.Column.FieldName
            Case quantityFieldName
                Dim quantity As String = dr(quantityFieldName).ToString()
                If Not String.IsNullOrEmpty(quantity.Trim()) Then
                    dr(GetDetailColumnFieldName(BOPurchases.cReturnLine.TableCol.Value)) = (CDec(dr(GetDetailColumnFieldName(BOPurchases.cReturnLine.TableCol.Price))) * CDbl(quantity)).ToString("N2")
                End If

            Case reasonFieldName
                dr.ClearErrors()

        End Select

        If _newDetailRowAdding <> True Then
            _returnDetailsCurrencyManager.EndCurrentEdit()
        End If
    End Sub


    Private Sub xgvDetail_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles xgvDetail.KeyDown

        Select Case e.KeyData
            Case Keys.Delete
                DeleteItem()

            Case Keys.Down, Keys.Up
                'check in reason code column and open editor if so
                If xgvDetail.FocusedColumn.Equals(GetDetailColumn(BOPurchases.cReturnLine.TableCol.ReasonCode)) Then
                    xgvDetail.ShowEditor()
                    If xgvDetail.ActiveEditor IsNot Nothing Then
                        CType(xgvDetail.ActiveEditor, LookUpEdit).ShowPopup()
                        CType(xgvDetail.ActiveEditor, LookUpEdit).Focus()
                    End If
                End If

        End Select

    End Sub

    Private Sub riReason_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim editor As DevExpress.XtraEditors.LookUpEdit = CType(sender, DevExpress.XtraEditors.LookUpEdit)
        Dim editorRow As DataRowView = CType(editor.Properties.GetDataSourceRowByKeyValue(editor.EditValue), DataRowView)
        Dim returnDetialRow As DataRow = xgvDetail.GetDataRow(xgvDetail.FocusedRowHandle)
        Dim reasonCodeColumnIndex = 1
        Dim reasonNameColumnIndex = 2

        returnDetialRow(GetDetailColumnFieldName(BOPurchases.cReturnLine.TableCol.ReasonCode)) = editorRow(reasonCodeColumnIndex)
        returnDetialRow(GetDetailColumnFieldName(BOPurchases.cReturnLine.TableCol.ReasonDescription)) = editorRow(reasonNameColumnIndex)
    End Sub

    Private Sub xgvDetail_ValidatingEditor(sender As System.Object, e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles xgvDetail.ValidatingEditor

        'get view in question
        Dim view As GridView = CType(sender, GridView)

        Select Case True
            Case view.FocusedColumn.Equals(GetDetailColumn(BOPurchases.cReturnLine.TableCol.Quantity))
                'validate qty entered
                Select Case CInt(e.Value)
                    Case Is <= 0
                        e.ErrorText = My.Resources.PositiveQtyMustBeEntered
                        e.Valid = False
                    Case Is > _qtyLimit
                        Dim response As DialogResult = MessageBox.Show(My.Resources.YesNoQtyHigh, Me.AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                        If response <> DialogResult.Yes Then
                            e.ErrorText = My.Resources.QtyHigh
                            e.Valid = False
                        End If

                End Select
                If view.FocusedRowHandle <> GridControl.NewItemRowHandle Then view.UpdateCurrentRow()

        End Select

        'if an error exists then select all text for easy editing
        If e.Valid = False Then
            view.ActiveEditor.SelectAll()
        Else
            view.CloseEditor()
        End If
    End Sub

    Private Sub btnAddItem_Click(sender As System.Object, e As System.EventArgs) Handles btnAddItem.Click
        Using lookup As New Stock.Enquiry.Enquiry(0, 0, 0, String.Empty)
            lookup.ShowAccept()

            Dim supplierNumber As String = CStr(xgvHeader.GetRowCellValue(xgvHeader.FocusedRowHandle, xgvHeader.Columns(BOPurchases.cReturnHeader.TableCol.SupplierNumber)))
            lookup.SetSupplier(supplierNumber)

            Using host As New Cts.Oasys.WinForm.HostForm(lookup)
                host.ShowDialog()
                If lookup.SkuNumbers.Count > 0 Then
                    Try
                        Dim sku As String = lookup.SkuNumbers(0).ToString()
                        'get stock for this sku/barcode
                        Using stockObject As New BOStock.cStock(_Oasys3DB)
                            Dim stock As BOStock.cStock = stockObject.GetStockItem(sku)
                            If stock IsNot Nothing Then
                                CreateNewReturnDetailRow(stock)
                            Else
                                MessageBox.Show(My.Resources.StockNotFound)
                            End If
                        End Using
                    Catch ex As DevExpress.Utils.HideException
                    End Try
                End If
            End Using

        End Using
    End Sub

    Private Sub btnSaveChanges_Click(sender As System.Object, e As System.EventArgs) Handles btnSaveChanges.Click
        Try
            Cursor = Cursors.WaitCursor
            DisplayStatus(My.Resources.SavingReturn)

            If ValidateDetails() Then

                Dim retHeader As BOPurchases.cReturnHeader = SaveReturn()
                If retHeader IsNot Nothing Then
                    UpdateHeaderValueColumn()
                    ChangeButtonsEnableState()
                    ShowSuccessMessage()
                    PrintReport(retHeader)
                End If

            End If

        Finally
            DisplayStatus(String.Empty)
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Function SaveReturn() As BOPurchases.cReturnHeader
        Dim retHeader As New BOPurchases.cReturnHeader(_Oasys3DB)
        Dim linesSumValue As Decimal = 0

        retHeader.LoadFromRow(xgvHeader.GetDataRow(xgvHeader.FocusedRowHandle))
        retHeader.ClearLines()

        For Each detailRow As DataRow In _returnDetails.Table.Rows()
            If detailRow.RowState <> DataRowState.Deleted Then
                Dim price As Decimal = CDec(detailRow(GetDetailColumnFieldName(BOPurchases.cReturnLine.TableCol.Price)))
                Dim quantity As Integer = CInt(detailRow(GetDetailColumnFieldName(BOPurchases.cReturnLine.TableCol.Quantity)))

                retHeader.AddLine(
                    CStr(detailRow(GetDetailColumnFieldName(BOPurchases.cReturnLine.TableCol.SkuNumber))),
                    CDec(detailRow(GetDetailColumnFieldName(BOPurchases.cReturnLine.TableCol.Cost))),
                    price,
                    quantity,
                    CStr(detailRow(GetDetailColumnFieldName(BOPurchases.cReturnLine.TableCol.ReasonCode))),
                    retHeader.ID.Value)

                linesSumValue += price * quantity
            End If
        Next

        retHeader.Value.Value = linesSumValue

        'check value against lowlevel param 3660
        If retHeader.IsOverLowLimit(_LowLevel) = False Then
            Using login As New User.UserAuthorise(User.LoginLevel.Manager, My.Resources.ReturnBelowLimitOf & FormatNumber(_LowLevel, 2))
                If login.ShowDialog(Me) <> DialogResult.OK Then Return Nothing
            End Using
        End If

        'insert header, delete and re-insert lines, recalculate stock
        retHeader.Update(MyBase.UserId, retHeader.ID.Value, retHeader.Value.Value)
        _returnDetails.Table.AcceptChanges()

        Return retHeader
    End Function

    Private Sub PrintReport(retHeader As BOPurchases.cReturnHeader)
        Dim report As New CtsControls.PrintReturns(_Oasys3DB, retHeader, xgdDetail)
        report.PrintCreationReports()
    End Sub

    Private Function ValidateDetails() As Boolean
        Dim result As Boolean = True
        Dim reasonColumn As GridColumn = GetDetailColumn(BOPurchases.cReturnLine.TableCol.ReasonCode)
        For index As Integer = 0 To xgvDetail.RowCount - 1
            Dim detailRow As DataRow = xgvDetail.GetDataRow(index)
            Dim reasonValue = detailRow(reasonColumn.FieldName)
            If IsDBNull(reasonValue) Then
                detailRow.SetColumnError(BOPurchases.cReturnLine.TableCol.ReasonCode, My.Resources.ReasonIsRequired)
                result = False
            Else
                detailRow.ClearErrors()
            End If
            xgvDetail.UpdateCurrentRow()
        Next

        Return result
    End Function

    Private Sub ShowSuccessMessage()
        Dim returnNumber As Integer = CInt(xgvHeader.GetRowCellValue(xgvHeader.FocusedRowHandle, xgvHeader.Columns(BOPurchases.cReturnHeader.TableCol.Number)))
        MessageBox.Show(My.Resources.ReturnSaved & returnNumber, Me.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub UpdateHeaderValueColumn()
        Dim valueColumn As DevExpress.XtraGrid.Columns.GridColumn = GetDetailColumn(BOPurchases.cReturnLine.TableCol.Value)
        If valueColumn IsNot Nothing Then
            Dim totalSumItem As GridSummaryItem = valueColumn.SummaryItem
            If totalSumItem IsNot Nothing Then
                Dim totalSumValue As Decimal = CType(totalSumItem.SummaryValue, Decimal)
                Dim headerRow As DataRow = xgvHeader.GetDataRow(xgvHeader.FocusedRowHandle)
                headerRow(GetHeaderColumnFieldName(BOPurchases.cReturnHeader.TableCol.Value)) = totalSumValue
            End If
        End If
    End Sub

    Private Sub btnReset_Click(sender As System.Object, e As System.EventArgs) Handles btnReset.Click
        LoadDetails(xgvHeader)
        ChangeButtonsEnableState()
    End Sub

    Private Function IsOpenReturnsAction() As Boolean
        If RunParameters.ToLower.Trim = _openAction Then
            Return True
        End If
        Return False
    End Function

    Private Function IsDeleteReturnsAction() As Boolean
        If RunParameters.ToLower.Trim = _deleteAction Then
            Return True
        End If
        Return False
    End Function

    Private Sub DisableEditForDetailColumn(ByRef col As DevExpress.XtraGrid.Columns.GridColumn)
        If IsOpenReturnsAction() Then
            col.OptionsColumn.AllowEdit = False
            col.OptionsColumn.AllowFocus = False
        End If
    End Sub

    Private Sub EnableEditForDetailColumn(ByRef col As DevExpress.XtraGrid.Columns.GridColumn)
        If IsOpenReturnsAction() Then
            xgvDetail.OptionsBehavior.Editable = True
            col.ColumnEdit = Me.riMaskInteger
        End If
    End Sub

    Private Function GetDetailColumn(ByVal columnIndex As Integer) As DevExpress.XtraGrid.Columns.GridColumn
        Return xgvDetail.Columns(columnIndex)
    End Function

    Private Function GetDetailColumnFieldName(ByVal columnIndex As Integer) As String
        Return GetDetailColumn(columnIndex).FieldName
    End Function

    Private Function GetHeaderColumnFieldName(ByVal columnIndex As Integer) As String
        Return GetHeaderColumn(columnIndex).FieldName
    End Function

    Private Function GetHeaderColumn(ByVal columnIndex As Integer) As DevExpress.XtraGrid.Columns.GridColumn
        Return xgvHeader.Columns(columnIndex)
    End Function

    Private Sub AddResonCodeEditor(ByRef col As DevExpress.XtraGrid.Columns.GridColumn)
        If IsOpenReturnsAction() Then
            Dim riReason As RepositoryItemLookUpEdit = CreateResonCodeEditor()

            'add to grid and reason column
            xgdDetail.RepositoryItems.Add(riReason)
            col.ColumnEdit = riReason
        End If
    End Sub

    Private Function CreateResonCodeEditor() As RepositoryItemLookUpEdit
        Dim sysCodes As New BOSystem.cSystemCodes(_Oasys3DB)
        Dim riReason As New RepositoryItemLookUpEdit
        Dim displayMember As String = "Display"

        riReason.DataSource = sysCodes.GetReturnCodes
        riReason.ValueMember = sysCodes.Number.ColumnName
        riReason.DisplayMember = sysCodes.Number.ColumnName
        riReason.ShowHeader = False
        riReason.ShowFooter = False
        riReason.DropDownRows = CType(riReason.DataSource, DataTable).Rows.Count
        riReason.NullText = My.Resources.ResonCodeEditorNullText
        riReason.Columns.Add(New LookUpColumnInfo(displayMember, 40))
        riReason.AllowNullInput = DevExpress.Utils.DefaultBoolean.False
        riReason.Mask.IgnoreMaskBlank = False

        AddHandler riReason.KeyDown, AddressOf xgvDetail_KeyDown
        AddHandler riReason.EditValueChanged, AddressOf riReason_EditValueChanged

        Return riReason
    End Function

    Private Sub DeleteItem()
        'check that row has been selected
        If xgvDetail.SelectedRowsCount = 1 Then
            If IsLastRowInDetailsGrid() Then
                ExecuteCommand(My.Resources.F5Delete, My.Resources.YesNoDeleteWholeReturn)
                ChangeButtonsEnableState()
            Else
                Dim result As DialogResult = MessageBox.Show(My.Resources.YesNoDeleteItem, Me.AppName, MessageBoxButtons.YesNo)
                If result = DialogResult.Yes Then
                    xgvDetail.DeleteRow(xgvDetail.FocusedRowHandle)
                End If
            End If
        End If
    End Sub

    Private Function IsLastRowInDetailsGrid() As Boolean
        Return xgvDetail.RowCount = 1
    End Function

    Private Sub CreateNewReturnDetailRow(ByRef stock As BOStock.cStock)
        xgdDetail.Focus()
        xgvDetail.AddNewRow()
        Dim newRowHandle As Integer = xgvDetail.FocusedRowHandle
        _newDetailRowAdding = True
        xgvDetail.SetRowCellValue(newRowHandle, GetDetailColumnFieldName(BOPurchases.cReturnLine.TableCol.SkuNumber), stock.SkuNumber.Value)
        xgvDetail.SetRowCellValue(newRowHandle, GetDetailColumnFieldName(BOPurchases.cReturnLine.TableCol.SkuDescription), stock.Description.Value)
        xgvDetail.SetRowCellValue(newRowHandle, GetDetailColumnFieldName(BOPurchases.cReturnLine.TableCol.Cost), stock.CostPrice.Value)
        xgvDetail.SetRowCellValue(newRowHandle, GetDetailColumnFieldName(BOPurchases.cReturnLine.TableCol.Price), stock.NormalSellPrice.Value)
        xgvDetail.SetRowCellValue(newRowHandle, GetDetailColumnFieldName(BOPurchases.cReturnLine.TableCol.Quantity), _defaultQuantity)
        xgvDetail.UpdateCurrentRow()
        _newDetailRowAdding = False
    End Sub

    Private Sub LoadDetails(ByRef gridView As GridView)

        Dim headerId As Integer = CInt(gridView.GetRowCellValue(gridView.FocusedRowHandle, gridView.Columns(BOPurchases.cReturnHeader.TableCol.Id)))
        xgvDetail_LoadDetails(headerId)

    End Sub

    Private Sub ChangeButtonsEnableState()
        Dim isChanged As Boolean = HasChanges()
        Dim detailButtons As List(Of SimpleButton) = New List(Of SimpleButton)
        detailButtons.Add(btnReset)
        detailButtons.Add(btnSaveChanges)
        ChangeEnableStateIfDifferent(detailButtons, isChanged)
        btnAddItem.Enabled = xgvHeader.RowCount > 0
        btnDeleteItem.Enabled = xgvDetail.RowCount > 0

        ChangeHeaderButtonsEnableState()
    End Sub

    Private Sub ChangeHeaderButtonsEnableState()
        Dim isChanged As Boolean = HasChanges()
        Dim commonButtons As List(Of SimpleButton) = New List(Of SimpleButton)
        Dim commonButtonsEnabled = Not isChanged
        If commonButtonsEnabled Then
            commonButtonsEnabled = xgvHeader.RowCount > 0
        End If
        commonButtons.Add(btnPrintSummary)
        commonButtons.Add(btnCommand)
        ChangeEnableStateIfDifferent(commonButtons, commonButtonsEnabled)
    End Sub

    Private Sub ChangeEnableStateIfDifferent(ByRef buttons As List(Of SimpleButton), ByVal newValue As Boolean)
        For Each button As SimpleButton In buttons
            If Not button.Enabled = newValue Then
                button.Enabled = newValue
            End If
        Next
    End Sub

    Private Function HasChanges() As Boolean
        Dim dataTable As DataTable = CType(xgdDetail.DataSource, DataTable)
        If dataTable Is Nothing Then
            Return False
        End If
        Dim changedRecordsTable As DataTable = dataTable.GetChanges()
        Return HasChanges(changedRecordsTable)
    End Function

    Private Function HasChanges(ByRef dataTable As DataTable) As Boolean
        If dataTable Is Nothing Then
            Return False
        End If

        For Each row As DataRow In dataTable.Rows
            Select Case row.RowState
                Case DataRowState.Added
                    Return True
                Case DataRowState.Deleted
                    Return True
                Case Else
                    If HasChanges(row) Then
                        Return True
                    End If
            End Select
        Next

        Return False
    End Function

    Private Function HasChanges(ByRef row As DataRow) As Boolean
        For index As Integer = 0 To row.ItemArray.Length - 1
            If index = BOPurchases.cReturnLine.TableCol.ReasonDescription Then
                Continue For
            End If
            If Not (row(index, DataRowVersion.Current)).Equals(row(index, DataRowVersion.Original)) Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Function IsCanLeaveReturnDetails() As Boolean
        If IsOpenReturnsAction() AndAlso HasChanges() Then
            Dim Result As DialogResult = MessageBox.Show(My.Resources.YesNoLostChanges, Me.AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
            If Result = DialogResult.No Then
                Return False
            End If
        End If
        Return True
    End Function

    Private Sub btnDeleteItem_Click(sender As System.Object, e As System.EventArgs) Handles btnDeleteItem.Click
        DeleteItem()
    End Sub

    Private Sub riMaskInteger_Leave(sender As System.Object, e As System.EventArgs) Handles riMaskInteger.Leave
        Dim editor As DevExpress.XtraEditors.TextEdit = CType(sender, DevExpress.XtraEditors.TextEdit)
        xgvDetail.SetRowCellValue(xgvDetail.FocusedRowHandle, GetDetailColumnFieldName(BOPurchases.cReturnLine.TableCol.Quantity), editor.EditValue)
    End Sub
End Class
