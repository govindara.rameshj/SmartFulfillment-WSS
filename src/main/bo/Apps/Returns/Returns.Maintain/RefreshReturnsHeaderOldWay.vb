﻿Public Class RefreshReturnsHeaderOldWay

    Implements IRefreshReturnsHeader

    Public Function ReturnsHeader(ByVal retHeader As BOPurchases.cReturnHeader, ByVal Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal dr As System.Data.DataRow) As BOPurchases.cReturnHeader Implements IRefreshReturnsHeader.ReturnsHeader
        retHeader.LoadFromRow(dr)
        Return retHeader
    End Function

    Public Sub ReturnsReleaseMessage(ByVal btnCommandText As String, ByVal myForm As Form, ByVal retHeader As BOPurchases.cReturnHeader, ByVal F5PrintText As String, ByVal NoReturnsAndExitText As String) Implements IRefreshReturnsHeader.ReturnsReleaseMessage
        'check that there are any returns to process if not reprint mode and exit if so
        If (btnCommandText <> F5PrintText) AndAlso retHeader.Table.Rows.Count = 0 Then
            MessageBox.Show(NoReturnsAndExitText, myForm.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            myForm.Close()
        End If
    End Sub

    Public Function LimitCommentTo20Characters(ByVal comment As String) As String Implements IRefreshReturnsHeader.LimitCommentTo20Characters
        Return comment
    End Function
End Class
