﻿Public Interface IRefreshReturnsHeader
    Function ReturnsHeader(ByVal retHeader As BOPurchases.cReturnHeader, ByVal Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal dr As DataRow) As BOPurchases.cReturnHeader
    Sub ReturnsReleaseMessage(ByVal btnCommandText As String, ByVal myForm As Form, ByVal retHeader As BOPurchases.cReturnHeader, ByVal F5PrintText As String, ByVal NoReturnsAndExitText As String)
    Function LimitCommentTo20Characters(ByVal comment As String) As String
End Interface
