﻿Public Class RefreshReturnsHeader

    Implements IRefreshReturnsHeader

    Friend _returnsHeader As IReturnsHeaderRepository = New ReturnsHeaderRepository

    Public Function ReturnsHeader(ByVal retHeader As BOPurchases.cReturnHeader, ByVal Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal dr As System.Data.DataRow) As BOPurchases.cReturnHeader Implements IRefreshReturnsHeader.ReturnsHeader
        Dim MyReturnsHeader As BOPurchases.cReturnHeader

        MyReturnsHeader = _returnsHeader.GetReturnsHeader(Oasys3DB, dr)
        Return MyReturnsHeader
    End Function

    Public Function LimitCommentTo20Characters(ByVal comment As String) As String Implements IRefreshReturnsHeader.LimitCommentTo20Characters
        Return Mid(comment, 1, 20)
    End Function

    Public Sub ReturnsReleaseMessage(ByVal btnCommandText As String, ByVal myForm As System.Windows.Forms.Form, ByVal retHeader As BOPurchases.cReturnHeader, ByVal F5PrintText As String, ByVal NoReturnsAndExitText As String) Implements IRefreshReturnsHeader.ReturnsReleaseMessage
        If (btnCommandText = NoReturnsAndExitText) AndAlso retHeader.Table.Rows.Count = 0 Then
            MessageBox.Show(NoReturnsAndExitText, myForm.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            myForm.Close()
        End If
    End Sub

End Class

Public Interface IReturnsHeaderRepository
    Function GetReturnsHeader(ByVal Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal dr As System.Data.DataRow) As BOPurchases.cReturnHeader
End Interface

Public Class ReturnsHeaderRepository

    Implements IReturnsHeaderRepository

    Public Function GetReturnsHeader(ByVal Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal dr As System.Data.DataRow) As BOPurchases.cReturnHeader Implements IReturnsHeaderRepository.GetReturnsHeader
        Dim MyReturnsHeader As New BOPurchases.cReturnHeader(Oasys3DB)
        MyReturnsHeader.LoadFromRow(dr)
        Return MyReturnsHeader
    End Function

End Class
