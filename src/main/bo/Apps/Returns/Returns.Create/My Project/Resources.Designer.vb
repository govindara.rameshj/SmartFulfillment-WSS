﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.1008
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System

Namespace My.Resources
    
    'This class was auto-generated by the StronglyTypedResourceBuilder
    'class via a tool like ResGen or Visual Studio.
    'To add or remove a member, edit your .ResX file then rerun ResGen
    'with the /str option, or rebuild your VS project.
    '''<summary>
    '''  A strongly-typed resource class, for looking up localized strings, etc.
    '''</summary>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),  _
     Global.Microsoft.VisualBasic.HideModuleNameAttribute()>  _
    Friend Module Resources
        
        Private resourceMan As Global.System.Resources.ResourceManager
        
        Private resourceCulture As Global.System.Globalization.CultureInfo
        
        '''<summary>
        '''  Returns the cached ResourceManager instance used by this class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend ReadOnly Property ResourceManager() As Global.System.Resources.ResourceManager
            Get
                If Object.ReferenceEquals(resourceMan, Nothing) Then
                    Dim temp As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("Returns.Create.Resources", GetType(Resources).Assembly)
                    resourceMan = temp
                End If
                Return resourceMan
            End Get
        End Property
        
        '''<summary>
        '''  Overrides the current thread's CurrentUICulture property for all
        '''  resource lookups using this strongly typed resource class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Property Culture() As Global.System.Globalization.CultureInfo
            Get
                Return resourceCulture
            End Get
            Set
                resourceCulture = value
            End Set
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Collection date not entered.
        '''</summary>
        Friend ReadOnly Property CollectionDateNotEntered() As String
            Get
                Return ResourceManager.GetString("CollectionDateNotEntered", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Retrieving suppliers.
        '''</summary>
        Friend ReadOnly Property GetSuppliers() As String
            Get
                Return ResourceManager.GetString("GetSuppliers", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Retrieving system codes and required parameters.
        '''</summary>
        Friend ReadOnly Property GetSysCodesParameters() As String
            Get
                Return ResourceManager.GetString("GetSysCodesParameters", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Initialising controls.
        '''</summary>
        Friend ReadOnly Property InitControls() As String
            Get
                Return ResourceManager.GetString("InitControls", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to A positive quantity must be entered.
        '''</summary>
        Friend ReadOnly Property PositiveQtyMustBeEntered() As String
            Get
                Return ResourceManager.GetString("PositiveQtyMustBeEntered", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to High quantity.
        '''</summary>
        Friend ReadOnly Property QtyHigh() As String
            Get
                Return ResourceManager.GetString("QtyHigh", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Return is below the specified limit of .
        '''</summary>
        Friend ReadOnly Property ReturnBelowLimitOf() As String
            Get
                Return ResourceManager.GetString("ReturnBelowLimitOf", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Return saved successfully
        '''Return number: .
        '''</summary>
        Friend ReadOnly Property ReturnSaved() As String
            Get
                Return ResourceManager.GetString("ReturnSaved", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Saving Return.
        '''</summary>
        Friend ReadOnly Property SavingReturn() As String
            Get
                Return ResourceManager.GetString("SavingReturn", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Stock item not found.
        '''</summary>
        Friend ReadOnly Property StockNotFound() As String
            Get
                Return ResourceManager.GetString("StockNotFound", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Stock item is not from selected supplier.
        '''</summary>
        Friend ReadOnly Property StockNotFromSupplier() As String
            Get
                Return ResourceManager.GetString("StockNotFromSupplier", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Store not open on this day.
        '''</summary>
        Friend ReadOnly Property StoreNotOpen() As String
            Get
                Return ResourceManager.GetString("StoreNotOpen", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Are you sure you would like to delete this item?.
        '''</summary>
        Friend ReadOnly Property YesNoDeleteItem() As String
            Get
                Return ResourceManager.GetString("YesNoDeleteItem", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Quantity seems high.
        '''Are you sure?.
        '''</summary>
        Friend ReadOnly Property YesNoQtyHigh() As String
            Get
                Return ResourceManager.GetString("YesNoQtyHigh", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to The current return will be lost
        '''Do you wish to continue resetting?.
        '''</summary>
        Friend ReadOnly Property YesNoReset() As String
            Get
                Return ResourceManager.GetString("YesNoReset", resourceCulture)
            End Get
        End Property
    End Module
End Namespace
