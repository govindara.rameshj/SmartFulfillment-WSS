<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Create
    Inherits Cts.Oasys.WinForm.Form

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Create))
        Me.lblDate = New System.Windows.Forms.Label()
        Me.grpSupplier = New System.Windows.Forms.GroupBox()
        Me.lueSuppliers = New DevExpress.XtraEditors.LookUpEdit()
        Me.dteDate = New DevExpress.XtraEditors.DateEdit()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lstPolicy = New System.Windows.Forms.ListBox()
        Me.xgridReturns = New DevExpress.XtraGrid.GridControl()
        Me.dsReturns = New System.Data.DataSet()
        Me.dtReturns = New System.Data.DataTable()
        Me.dcSkuNumber = New System.Data.DataColumn()
        Me.dcDescription = New System.Data.DataColumn()
        Me.dcQty = New System.Data.DataColumn()
        Me.dcCost = New System.Data.DataColumn()
        Me.dcPrice = New System.Data.DataColumn()
        Me.dcValue = New System.Data.DataColumn()
        Me.dcReason = New System.Data.DataColumn()
        Me.xviewReturns = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.xcolSkuNumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riMaskInteger = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.xcolDescription = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.xcolQty = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.xcolCost = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.xcolPrice = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.xcolValue = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.xcolReason = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.epReturns = New DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(Me.components)
        Me.btnSave = New DevExpress.XtraEditors.SimpleButton()
        Me.btnReset = New DevExpress.XtraEditors.SimpleButton()
        Me.btnExit = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStockEnquiry = New DevExpress.XtraEditors.SimpleButton()
        Me.grpSupplier.SuspendLayout()
        CType(Me.lueSuppliers.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xgridReturns, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dsReturns, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtReturns, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xviewReturns, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riMaskInteger, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.epReturns, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblDate
        '
        Me.lblDate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblDate.Location = New System.Drawing.Point(6, 45)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(57, 20)
        Me.lblDate.TabIndex = 2
        Me.lblDate.Text = "&Collection"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'grpSupplier
        '
        Me.grpSupplier.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpSupplier.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.grpSupplier.Controls.Add(Me.lueSuppliers)
        Me.grpSupplier.Controls.Add(Me.dteDate)
        Me.grpSupplier.Controls.Add(Me.Label1)
        Me.grpSupplier.Controls.Add(Me.lstPolicy)
        Me.grpSupplier.Controls.Add(Me.lblDate)
        Me.grpSupplier.Location = New System.Drawing.Point(3, 3)
        Me.grpSupplier.Name = "grpSupplier"
        Me.grpSupplier.Padding = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.grpSupplier.Size = New System.Drawing.Size(794, 119)
        Me.grpSupplier.TabIndex = 0
        Me.grpSupplier.TabStop = False
        Me.grpSupplier.Text = "Return Details"
        '
        'lueSuppliers
        '
        Me.lueSuppliers.Location = New System.Drawing.Point(69, 20)
        Me.lueSuppliers.Name = "lueSuppliers"
        Me.lueSuppliers.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lueSuppliers.Properties.ImmediatePopup = True
        Me.lueSuppliers.Properties.NullText = ""
        Me.lueSuppliers.Properties.PopupWidth = 250
        Me.lueSuppliers.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.lueSuppliers.Size = New System.Drawing.Size(250, 20)
        Me.lueSuppliers.TabIndex = 1
        '
        'dteDate
        '
        Me.dteDate.EditValue = Nothing
        Me.dteDate.EnterMoveNextControl = True
        Me.dteDate.Location = New System.Drawing.Point(69, 46)
        Me.dteDate.Name = "dteDate"
        Me.dteDate.Properties.Appearance.Options.UseTextOptions = True
        Me.dteDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.dteDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dteDate.Size = New System.Drawing.Size(116, 20)
        Me.dteDate.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(6, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "&Supplier"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lstPolicy
        '
        Me.lstPolicy.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstPolicy.BackColor = System.Drawing.SystemColors.Control
        Me.lstPolicy.FormattingEnabled = True
        Me.lstPolicy.Location = New System.Drawing.Point(325, 18)
        Me.lstPolicy.Name = "lstPolicy"
        Me.lstPolicy.SelectionMode = System.Windows.Forms.SelectionMode.None
        Me.lstPolicy.Size = New System.Drawing.Size(463, 95)
        Me.lstPolicy.TabIndex = 32
        Me.lstPolicy.TabStop = False
        Me.lstPolicy.Visible = False
        '
        'xgridReturns
        '
        Me.xgridReturns.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.xgridReturns.DataMember = "dtReturns"
        Me.xgridReturns.DataSource = Me.dsReturns
        Me.xgridReturns.Location = New System.Drawing.Point(3, 128)
        Me.xgridReturns.MainView = Me.xviewReturns
        Me.xgridReturns.Name = "xgridReturns"
        Me.xgridReturns.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riMaskInteger})
        Me.xgridReturns.Size = New System.Drawing.Size(794, 324)
        Me.xgridReturns.TabIndex = 1
        Me.xgridReturns.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.xviewReturns})
        '
        'dsReturns
        '
        Me.dsReturns.DataSetName = "NewDataSet"
        Me.dsReturns.Tables.AddRange(New System.Data.DataTable() {Me.dtReturns})
        '
        'dtReturns
        '
        Me.dtReturns.Columns.AddRange(New System.Data.DataColumn() {Me.dcSkuNumber, Me.dcDescription, Me.dcQty, Me.dcCost, Me.dcPrice, Me.dcValue, Me.dcReason})
        Me.dtReturns.TableName = "dtReturns"
        '
        'dcSkuNumber
        '
        Me.dcSkuNumber.AllowDBNull = False
        Me.dcSkuNumber.Caption = "SKU Number"
        Me.dcSkuNumber.ColumnName = "SkuNumber"
        Me.dcSkuNumber.DataType = GetType(ULong)
        '
        'dcDescription
        '
        Me.dcDescription.AllowDBNull = False
        Me.dcDescription.ColumnName = "Description"
        '
        'dcQty
        '
        Me.dcQty.AllowDBNull = False
        Me.dcQty.ColumnName = "Qty"
        Me.dcQty.DataType = GetType(UShort)
        Me.dcQty.DefaultValue = CType(0US, UShort)
        '
        'dcCost
        '
        Me.dcCost.AllowDBNull = False
        Me.dcCost.ColumnName = "Cost"
        Me.dcCost.DataType = GetType(Decimal)
        Me.dcCost.DefaultValue = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'dcPrice
        '
        Me.dcPrice.AllowDBNull = False
        Me.dcPrice.ColumnName = "Price"
        Me.dcPrice.DataType = GetType(Decimal)
        Me.dcPrice.DefaultValue = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'dcValue
        '
        Me.dcValue.ColumnName = "Value"
        Me.dcValue.DataType = GetType(Decimal)
        Me.dcValue.DefaultValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.dcValue.Expression = "Qty*Price"
        Me.dcValue.ReadOnly = True
        '
        'dcReason
        '
        Me.dcReason.AllowDBNull = False
        Me.dcReason.ColumnName = "Reason"
        '
        'xviewReturns
        '
        Me.xviewReturns.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xviewReturns.Appearance.HeaderPanel.Options.UseFont = True
        Me.xviewReturns.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.xviewReturns.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.xviewReturns.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.xviewReturns.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.xcolSkuNumber, Me.xcolDescription, Me.xcolQty, Me.xcolCost, Me.xcolPrice, Me.xcolValue, Me.xcolReason})
        Me.xviewReturns.GridControl = Me.xgridReturns
        Me.xviewReturns.Name = "xviewReturns"
        Me.xviewReturns.NewItemRowText = "Click here to add a new item.  Press 'F2' in Sku Number column for stock look up"
        Me.xviewReturns.OptionsCustomization.AllowColumnMoving = False
        Me.xviewReturns.OptionsCustomization.AllowGroup = False
        Me.xviewReturns.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.xviewReturns.OptionsView.ShowFooter = True
        Me.xviewReturns.OptionsView.ShowGroupPanel = False
        '
        'xcolSkuNumber
        '
        Me.xcolSkuNumber.AppearanceCell.Options.UseTextOptions = True
        Me.xcolSkuNumber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.xcolSkuNumber.Caption = "Sku Number"
        Me.xcolSkuNumber.ColumnEdit = Me.riMaskInteger
        Me.xcolSkuNumber.FieldName = "SkuNumber"
        Me.xcolSkuNumber.Name = "xcolSkuNumber"
        Me.xcolSkuNumber.Visible = True
        Me.xcolSkuNumber.VisibleIndex = 0
        Me.xcolSkuNumber.Width = 89
        '
        'riMaskInteger
        '
        Me.riMaskInteger.AutoHeight = False
        Me.riMaskInteger.Mask.EditMask = "[0-9]+"
        Me.riMaskInteger.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.riMaskInteger.Name = "riMaskInteger"
        '
        'xcolDescription
        '
        Me.xcolDescription.Caption = "Description"
        Me.xcolDescription.FieldName = "Description"
        Me.xcolDescription.Name = "xcolDescription"
        Me.xcolDescription.OptionsColumn.AllowEdit = False
        Me.xcolDescription.OptionsColumn.AllowFocus = False
        Me.xcolDescription.Visible = True
        Me.xcolDescription.VisibleIndex = 1
        Me.xcolDescription.Width = 218
        '
        'xcolQty
        '
        Me.xcolQty.Caption = "Qty"
        Me.xcolQty.ColumnEdit = Me.riMaskInteger
        Me.xcolQty.FieldName = "Qty"
        Me.xcolQty.Name = "xcolQty"
        Me.xcolQty.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.xcolQty.Visible = True
        Me.xcolQty.VisibleIndex = 2
        Me.xcolQty.Width = 81
        '
        'xcolCost
        '
        Me.xcolCost.Caption = "Cost"
        Me.xcolCost.DisplayFormat.FormatString = "c2"
        Me.xcolCost.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.xcolCost.FieldName = "Cost"
        Me.xcolCost.Name = "xcolCost"
        Me.xcolCost.OptionsColumn.AllowEdit = False
        Me.xcolCost.OptionsColumn.AllowFocus = False
        Me.xcolCost.Width = 81
        '
        'xcolPrice
        '
        Me.xcolPrice.Caption = "Price"
        Me.xcolPrice.DisplayFormat.FormatString = "c2"
        Me.xcolPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.xcolPrice.FieldName = "Price"
        Me.xcolPrice.Name = "xcolPrice"
        Me.xcolPrice.OptionsColumn.AllowEdit = False
        Me.xcolPrice.OptionsColumn.AllowFocus = False
        Me.xcolPrice.Visible = True
        Me.xcolPrice.VisibleIndex = 3
        Me.xcolPrice.Width = 81
        '
        'xcolValue
        '
        Me.xcolValue.Caption = "Value"
        Me.xcolValue.DisplayFormat.FormatString = "c2"
        Me.xcolValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.xcolValue.FieldName = "Value"
        Me.xcolValue.Name = "xcolValue"
        Me.xcolValue.OptionsColumn.AllowEdit = False
        Me.xcolValue.OptionsColumn.AllowFocus = False
        Me.xcolValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.xcolValue.Visible = True
        Me.xcolValue.VisibleIndex = 4
        Me.xcolValue.Width = 81
        '
        'xcolReason
        '
        Me.xcolReason.Caption = "Reason Code"
        Me.xcolReason.FieldName = "Reason"
        Me.xcolReason.Name = "xcolReason"
        Me.xcolReason.Visible = True
        Me.xcolReason.VisibleIndex = 5
        Me.xcolReason.Width = 138
        '
        'epReturns
        '
        Me.epReturns.ContainerControl = Me
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Enabled = False
        Me.btnSave.Location = New System.Drawing.Point(86, 458)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 39)
        Me.btnSave.TabIndex = 8
        Me.btnSave.Text = "F5 Save"
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.Location = New System.Drawing.Point(641, 458)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(75, 39)
        Me.btnReset.TabIndex = 9
        Me.btnReset.Text = "F11 Reset"
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(722, 458)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 39)
        Me.btnExit.TabIndex = 10
        Me.btnExit.Text = "F12 Exit"
        '
        'btnAddItem
        '
        Me.btnStockEnquiry.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnStockEnquiry.Appearance.Options.UseTextOptions = True
        Me.btnStockEnquiry.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.btnStockEnquiry.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btnStockEnquiry.Location = New System.Drawing.Point(3, 458)
        Me.btnStockEnquiry.Name = "btnAddItem"
        Me.btnStockEnquiry.Size = New System.Drawing.Size(77, 39)
        Me.btnStockEnquiry.TabIndex = 133
        Me.btnStockEnquiry.Text = "F2 Stock Enquiry"
        '
        'Create
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Controls.Add(Me.btnStockEnquiry)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.xgridReturns)
        Me.Controls.Add(Me.grpSupplier)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Create"
        Me.Size = New System.Drawing.Size(800, 500)
        Me.grpSupplier.ResumeLayout(False)
        CType(Me.lueSuppliers.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xgridReturns, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dsReturns, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtReturns, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xviewReturns, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riMaskInteger, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.epReturns, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents grpSupplier As System.Windows.Forms.GroupBox
    Friend WithEvents lstPolicy As System.Windows.Forms.ListBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dteDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lueSuppliers As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents xgridReturns As DevExpress.XtraGrid.GridControl
    Friend WithEvents xcolSkuNumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents xcolDescription As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents xcolQty As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents xcolPrice As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents xcolValue As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents xcolReason As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dsReturns As System.Data.DataSet
    Friend WithEvents dtReturns As System.Data.DataTable
    Friend WithEvents dcSkuNumber As System.Data.DataColumn
    Friend WithEvents dcDescription As System.Data.DataColumn
    Friend WithEvents xcolCost As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dcQty As System.Data.DataColumn
    Friend WithEvents dcCost As System.Data.DataColumn
    Friend WithEvents dcPrice As System.Data.DataColumn
    Friend WithEvents dcValue As System.Data.DataColumn
    Friend WithEvents dcReason As System.Data.DataColumn
    Friend WithEvents riMaskInteger As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Private WithEvents xviewReturns As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents epReturns As DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider
    Friend WithEvents btnExit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnReset As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStockEnquiry As DevExpress.XtraEditors.SimpleButton

End Class
