Imports Cts.Oasys.WinForm
Imports DevExpress.XtraEditors

Public Class Create
    Private _oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _sysDates As New BOSystem.cSystemDates(_oasys3DB)
    Private _suppliers As New BOPurchases.vwSuppliersWithStockItems(_oasys3DB)
    Private _LowLevel As Decimal = 0
    Private _qtyLimit As Integer = 0

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
        dteDate.Properties.MinValue = Now.Date
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F2 : btnStockEnquiry.PerformClick()
            Case Keys.F5 : btnSave.PerformClick()
            Case Keys.F11
                btnReset.Focus()
                btnReset.PerformClick()
            Case Keys.F12
                btnExit.Focus()
                btnExit.PerformClick()
            Case Else
                e.Handled = False
        End Select

    End Sub

    Protected Overrides Sub DoProcessing()

        Try
            Cursor = Cursors.WaitCursor

            'get system return codes, dates, options and retail options
            DisplayProgress(0, My.Resources.GetSysCodesParameters)
            Dim Param As New BOSystem.cParameter(_oasys3DB)
            _qtyLimit = Param.GetParameterInteger(3650)
            _LowLevel = Param.GetParameterDecimal(3660)
            _sysDates.Load()

            'get suppliers
            DisplayProgress(50, My.Resources.GetSuppliers)
            lueSuppliers_Initialise()

            'initialise controls
            DisplayProgress(70, My.Resources.InitControls)
            xgridReturns_Initialise()

            'set initial focus
            lueSuppliers.Focus()

        Finally
            Cursor = Cursors.Default
            DisplayProgress(100)
            DisplayProgress()
        End Try

    End Sub



    Private Sub lueSuppliers_Initialise()

        _suppliers.LoadView(True)

        'set datasource and value member - display member is set depending on last entry made
        lueSuppliers.Properties.DataSource = _suppliers.Table
        lueSuppliers.Properties.ValueMember = _suppliers.Table.Columns(BOPurchases.vwSuppliersWithStockItems.col.Number).ColumnName
        lueSuppliers.Properties.DisplayMember = _suppliers.Table.Columns(BOPurchases.vwSuppliersWithStockItems.col.Display).ColumnName

        'set up columns
        lueSuppliers.Properties.Columns.Add(New LookUpColumnInfo(_suppliers.Table.Columns(BOPurchases.vwSuppliersWithStockItems.col.Number).ColumnName))
        lueSuppliers.Properties.Columns.Add(New LookUpColumnInfo(_suppliers.Table.Columns(BOPurchases.vwSuppliersWithStockItems.col.Name).ColumnName))
        lueSuppliers.Properties.AutoSearchColumnIndex = BOPurchases.vwSuppliersWithStockItems.col.Number
        lueSuppliers.Properties.SortColumnIndex = BOPurchases.vwSuppliersWithStockItems.col.Name
        lueSuppliers.Properties.BestFit()

    End Sub

    Private Sub lueSuppliers_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles lueSuppliers.KeyPress

        Select Case True
            Case IsNumeric(e.KeyChar)
                lueSuppliers.Properties.AutoSearchColumnIndex = BOPurchases.vwSuppliersWithStockItems.col.Number
                lueSuppliers.Properties.DisplayMember = _suppliers.Table.Columns(BOPurchases.vwSuppliersWithStockItems.col.Display).ColumnName
            Case Else
                lueSuppliers.Properties.AutoSearchColumnIndex = BOPurchases.vwSuppliersWithStockItems.col.Name
                lueSuppliers.Properties.DisplayMember = _suppliers.Table.Columns(BOPurchases.vwSuppliersWithStockItems.col.Name).ColumnName
        End Select

    End Sub

    Private Sub lueSuppliers_Closed(ByVal sender As Object, ByVal e As ClosedEventArgs) Handles lueSuppliers.Closed

        Select Case e.CloseMode
            Case DevExpress.XtraEditors.PopupCloseMode.Cancel
            Case DevExpress.XtraEditors.PopupCloseMode.CloseUpKey
            Case Else : SupplierChosen()
        End Select

    End Sub

    Private Sub dteDate_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles dteDate.Validating

        'checks that day is valid
        If dteDate.EditValue IsNot Nothing Then
            Dim dayWeek As Integer = CDate(dteDate.EditValue).DayOfWeek
            If dayWeek = 0 Then dayWeek = 7
            If dayWeek > _sysDates.DaysOpen.Value Then
                e.Cancel = True
                epReturns.SetError(dteDate, My.Resources.StoreNotOpen, DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical)
                Exit Sub
            End If

            'check not open on certain days - need list

            'reset error if all ok
            epReturns.SetError(dteDate, String.Empty)
        End If

        xviewReturns.FocusedRowHandle = GridControl.NewItemRowHandle
        xviewReturns.FocusedColumn = xcolSkuNumber
        xgridReturns.Focus()

    End Sub

    Private Sub SupplierChosen()

        If lueSuppliers.EditValue Is Nothing Then Exit Sub
        lueSuppliers.Properties.DisplayMember = _suppliers.Table.Columns(BOPurchases.vwSuppliersWithStockItems.col.Display).ColumnName

        'load return policy
        Dim supplier As New BOPurchases.cSupplierMaster(_oasys3DB)
        supplier.LoadSupplier(CStr(lueSuppliers.EditValue))

        For Each note As BOPurchases.cSupplierNote In supplier.ReturnNotes
            lstPolicy.Items.Add(note.Text.Value)
        Next

        'if notes loaded then make visible
        If lstPolicy.Items.Count > 0 Then
            lstPolicy.Visible = True
        End If

        'lock supplier details
        lueSuppliers.Enabled = False
        btnReset.Enabled = True
        btnSave.Enabled = True

    End Sub



    Private Sub xgridReturns_Initialise()

        'create reason code repository item
        Dim _sysCodes As New BOSystem.cSystemCodes(_oasys3DB)
        Dim riReason As New RepositoryItemLookUpEdit
        riReason.DataSource = _sysCodes.GetReturnCodes
        riReason.ValueMember = _sysCodes.Number.ColumnName
        riReason.DisplayMember = "Display"
        riReason.ShowHeader = False
        riReason.ShowFooter = False
        riReason.DropDownRows = CType(riReason.DataSource, DataTable).Rows.Count
        riReason.NullText = "<Select Reason>"
        riReason.Columns.Add(New LookUpColumnInfo("Display", 40))
        riReason.AllowNullInput = DevExpress.Utils.DefaultBoolean.False
        AddHandler riReason.KeyDown, AddressOf xviewReturns_KeyDown

        'add to grid and reason column
        xgridReturns.RepositoryItems.Add(riReason)
        xcolReason.ColumnEdit = riReason

        'create sku number repository item 
        Dim riSku As New RepositoryItemTextEdit
        AddHandler riSku.KeyDown, AddressOf xviewReturns_KeyDown
        xgridReturns.RepositoryItems.Add(riSku)
        xcolSkuNumber.ColumnEdit = riSku

    End Sub

    Private Sub xviewReturns_ShowingEditor(ByVal sender As Object, ByVal e As CancelEventArgs) Handles xviewReturns.ShowingEditor

        'get view in question
        Dim view As GridView = CType(sender, GridView)
        If view.FocusedRowHandle <> GridControl.NewItemRowHandle Then
            Select Case True
                Case view.FocusedColumn.Equals(xcolQty)
                Case view.FocusedColumn.Equals(xcolReason)
                Case Else : e.Cancel = True
            End Select
        End If

    End Sub

    Private Sub xviewReturns_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles xviewReturns.CellValueChanged

        Dim view As GridView = CType(sender, GridView)
        If e.Column.FieldName = xcolQty.FieldName AndAlso e.RowHandle = GridControl.NewItemRowHandle Then
            Dim dr As DataRow = view.GetDataRow(e.RowHandle)
            dr(xcolValue.FieldName) = CDec(dr(xcolPrice.FieldName)) * CDbl(dr(xcolQty.FieldName))
        End If

    End Sub

    Private Sub xviewReturns_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles xviewReturns.ValidatingEditor

        'get view in question
        Dim view As GridView = CType(sender, GridView)

        'check if reset button has been pressed
        Select Case Me.ActiveControl.Name
            Case btnExit.Name, btnReset.Name
                view.CancelUpdateCurrentRow()
                Exit Sub
        End Select

        Select Case True
            Case view.FocusedColumn.Equals(xcolSkuNumber)
                'get sku number and check if anything entered
                Dim sku As String = CStr(e.Value)
                If sku IsNot Nothing AndAlso sku.Length > 0 Then
                    'get stock for this sku/barcode
                    Using stockObject As New BOStock.cStock(_oasys3DB)
                        Dim stock As BOStock.cStock = stockObject.GetStockItem(sku)
                        If stock IsNot Nothing Then
                            'check that is for this supplier if loaded
                            If Not lueSuppliers.Enabled Then
                                If stock.SupplierNo.Value <> CStr(lueSuppliers.EditValue) Then
                                    e.ErrorText = My.Resources.StockNotFromSupplier
                                    e.Valid = False
                                    Exit Select
                                End If
                            End If

                            'else enter stock items
                            e.Value = stock.SkuNumber.Value
                            xviewReturns.SetRowCellValue(GridControl.NewItemRowHandle, xcolDescription, stock.Description.Value)
                            xviewReturns.SetRowCellValue(GridControl.NewItemRowHandle, xcolCost, stock.CostPrice.Value)
                            xviewReturns.SetRowCellValue(GridControl.NewItemRowHandle, xcolPrice, stock.NormalSellPrice.Value)
                            lueSuppliers.EditValue = stock.SupplierNo.Value

                        Else
                            e.ErrorText = My.Resources.StockNotFound
                            e.Valid = False
                        End If
                    End Using
                End If

            Case view.FocusedColumn.Equals(xcolQty)
                'validate qty entered
                Select Case CInt(e.Value)
                    Case Is <= 0
                        e.ErrorText = My.Resources.PositiveQtyMustBeEntered
                        e.Valid = False
                    Case Is > _qtyLimit
                        Dim response As DialogResult = MessageBox.Show(My.Resources.YesNoQtyHigh, Me.AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                        If response <> DialogResult.Yes Then
                            e.ErrorText = My.Resources.QtyHigh
                            e.Valid = False
                        End If

                End Select
                If view.FocusedRowHandle <> GridControl.NewItemRowHandle Then view.UpdateCurrentRow()

        End Select

        'if an error exists then select all text for easy editing
        If e.Valid = False Then
            view.ActiveEditor.SelectAll()
        Else
            view.CloseEditor()
        End If

    End Sub

    Private Sub xviewReturns_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles xviewReturns.ValidateRow

        Dim view As GridView = CType(sender, GridView)
        If e.RowHandle = GridControl.NewItemRowHandle AndAlso view.RowCount = 0 Then
            SupplierChosen()
        End If

    End Sub

    Private Sub xviewReturns_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles xviewReturns.KeyDown

        Select Case e.KeyData
            'Case Keys.F2
            '    'check that in new row and sku column
            '    If xviewReturns.FocusedRowHandle = GridControl.NewItemRowHandle AndAlso xviewReturns.FocusedColumn.Equals(xcolSkuNumber) Then
            '        'set up look up passing in supplier number if selected
            '        Using lookup As New Stock.Enquiry.Enquiry(UserId, WorkstationId, SecurityLevel, String.Empty)
            '            If lueSuppliers.Enabled = False Then
            '                lookup.SetSupplier(lueSuppliers.EditValue)
            '            End If

            '            Using host As New Oasys.Windows.HostForm(lookup)
            '                host.ShowDialog()
            '                If lookup.SkuNumbers.Count > 0 Then
            '                    xviewReturns.ShowEditor()
            '                    xviewReturns.ActiveEditor.EditValue = lookup.SkuNumbers(0)
            '                    xviewReturns.ValidateEditor()
            '                    xviewReturns.FocusedColumn = xcolQty
            '                End If
            '            End Using
            '        End Using

            '    End If

            Case Keys.Delete
                'check that row has been selected
                If xviewReturns.SelectedRowsCount = 1 Then
                    Dim result As DialogResult = MessageBox.Show(My.Resources.YesNoDeleteItem, Me.AppName, MessageBoxButtons.YesNo)
                    If result = DialogResult.Yes Then
                        xviewReturns.DeleteRow(xviewReturns.FocusedRowHandle)
                    End If
                End If

            Case Keys.Down, Keys.Up
                'check in reason code column and open editor if so
                If xviewReturns.FocusedColumn.Equals(xcolReason) Then
                    xviewReturns.ShowEditor()
                    If xviewReturns.ActiveEditor IsNot Nothing Then
                        CType(xviewReturns.ActiveEditor, LookUpEdit).ShowPopup()
                        CType(xviewReturns.ActiveEditor, LookUpEdit).Focus()
                    End If
                End If

        End Select

    End Sub


    Private Sub btnStockEnquiry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStockEnquiry.Click

        Using lookup As New Stock.Enquiry.Enquiry(0, 0, 0, String.Empty)
            lookup.ShowAccept()
            If lueSuppliers.Enabled = False Then
                lookup.SetSupplier(CStr(lueSuppliers.EditValue))
            End If

            Using host As New Cts.Oasys.WinForm.HostForm(lookup)
                host.ShowDialog()
                If lookup.SkuNumbers.Count > 0 Then
                    Try
                        xviewReturns.FocusedRowHandle = GridControl.NewItemRowHandle
                        xviewReturns.FocusedColumn = xcolSkuNumber
                        xviewReturns.ShowEditor()
                        xviewReturns.ActiveEditor.EditValue = lookup.SkuNumbers(0)
                        xviewReturns.ValidateEditor()
                    Catch ex As DevExpress.Utils.HideException
                    End Try
                End If
            End Using

        End Using

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Try
            'check date collection value
            If dteDate.EditValue Is Nothing Then
                epReturns.SetError(dteDate, My.Resources.CollectionDateNotEntered)
                dteDate.Focus()
                Exit Sub
            End If

            Cursor = Cursors.WaitCursor
            DisplayStatus(My.Resources.SavingReturn)

            'create return header
            Dim retHeader As New BOPurchases.cReturnHeader(_oasys3DB)
            retHeader.CreateReturn(CStr(lueSuppliers.EditValue), CDate(dteDate.EditValue))

            'add lines to new return header
            For Each dr As DataRow In dtReturns.Rows
                retHeader.AddLine(CStr(dr(xcolSkuNumber.FieldName)), CDec(dr(xcolCost.FieldName)), CDec(dr(xcolPrice.FieldName)), CInt(dr(xcolQty.FieldName)), CStr(dr(xcolReason.FieldName)))
            Next

            'check value against lowlevel param 3660
            If retHeader.IsOverLowLimit(_LowLevel) = False Then
                Using login As New User.UserAuthorise(User.LoginLevel.Manager, My.Resources.ReturnBelowLimitOf & FormatNumber(_LowLevel, 2))
                    If login.ShowDialog(Me) <> DialogResult.OK Then Exit Sub
                End Using
            End If

            'insert header into database
            retHeader.Insert(MyBase.UserId)
            MessageBox.Show(My.Resources.ReturnSaved & retHeader.Number.Value, Me.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information)

            'print reports and update return if done
            Dim report As New CtsControls.PrintReturns(_oasys3DB, retHeader, xgridReturns)
            report.PrintCreationReports()

            'reset
            dtReturns.Rows.Clear()
            btnReset.PerformClick()

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click

        'If any rows then ask confirmation
        If dtReturns.Rows.Count > 0 Then
            Dim result As DialogResult = MessageBox.Show(My.Resources.YesNoReset, Me.AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
            If result <> DialogResult.Yes Then Exit Sub
        End If

        dtReturns.Rows.Clear()
        lueSuppliers.Enabled = True
        lueSuppliers.EditValue = Nothing
        lueSuppliers.Focus()
        lstPolicy.Items.Clear()
        lstPolicy.Visible = False
        dteDate.EditValue = Nothing
        btnSave.Enabled = False

        DisplayStatus()

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        FindForm.Close()
    End Sub

End Class