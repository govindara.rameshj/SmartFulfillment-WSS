﻿Imports FarPoint.Win
Imports System.Data.SqlClient
Imports Oasys.Data
Imports Cts.Oasys.Core.System

Public Class ProjectLoanReport
    Private _Proxy As Oasys.DataProxy
    Private _PrintNotPrinted As Boolean
    Private _ReportDate As DateTime
    Private _CalledFromNight As Boolean
    Private _StoreNumber As String
    Private _StoreName As String
    Private _ShortAssemblyName As String
    Private _FileVersion As String

    Private Sub New()
        MyBase.New()
        SetupEnvironment()
    End Sub

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String, ByVal ImagePath As String, ByVal IsMaximized As Boolean, ByVal Value As Integer)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        SetupEnvironment()
    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Author - Partha Dutta
    'Date   - 05/08/2010
    'Reason - Not finding contructor with the correct parameters
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Author - Partha Dutta
    'Date   - 16/08/2010
    'Reason - Object reference not set to an instance of an object, missed out call to SetupEnvironment()
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Public Sub New(ByVal intUserId As Integer, ByVal intWorkstationId As Integer, ByVal intSecurityLevel As Integer, ByVal strRunParameters As String)
        MyBase.New(intUserId, intWorkstationId, intSecurityLevel, strRunParameters)
        SetupEnvironment()
    End Sub

    Private Sub SetupEnvironment()

        With FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetAssembly(Me.GetType).Location)
            _ShortAssemblyName = .FileName.Substring(.FileName.LastIndexOf("\"c) + 1, .FileName.LastIndexOf("."c) - .FileName.LastIndexOf("\"c) - 1)
            _FileVersion = .FileVersion
        End With

        MyTrace("Initialising")
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Dim db As New OasysDBBO.Oasys3.DB.clsOasys3DB()
        _Proxy = CType(Activator.CreateInstance(db.GetConfigValue("ProxyAssembly"), db.GetConfigValue("ProxyTypeName"), False, Reflection.BindingFlags.CreateInstance, Nothing, New Object() {db.GetConfigValue("sqlConnection")}, Globalization.CultureInfo.InvariantCulture, Nothing, Nothing).Unwrap, Oasys.DataProxy)
        _ReportDate = CDate(_Proxy.ExecuteScalar(New SqlCommand("SELECT TODT FROM SYSDAT")))
        dtpReportDate.Value = _ReportDate

        With _Proxy.ExecuteDataTable(New SqlCommand("SELECT STOR, SNAM FROM RETOPT WHERE FKEY='01'")).Rows(0)
            _StoreNumber = CStr(.Item("STOR"))
            _StoreName = CStr(.Item("SNAM"))
        End With

        _CalledFromNight = RunParameters.Contains(",CFC")
    End Sub

    Private Sub Print()
        Dim row As Integer = 0
        Dim reportData As DataTable = Nothing

        Dim saleTotal As Decimal = 0
        Dim refundTotal As Decimal = 0
        Dim workAmount As Decimal

        Dim reportMessage() As String = { _
                "The Net total of Project Loans should always match exactly to the daily banking report.", _
                "This report does not include Project Loans refunded through the tills.", _
                "NB. For each loan, the original Project loan agreement plus a copy must be attached to this listing and", _
                "sent daily by 1st class post to the Project loan Department at the Support Centre." _
            }

        Try
            If ReportSpread_Sheet1.RowCount > 0 Then
                ReportSpread_Sheet1.RemoveRows(0, ReportSpread_Sheet1.RowCount)
            End If

            Using reportCommand As New SqlClient.SqlCommand

                ReportSpread.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                ReportSpread_Sheet1.ColumnCount = 7

                For i As Integer = 0 To ReportSpread_Sheet1.ColumnCount - 1
                    ReportSpread_Sheet1.Columns(i).CellType = New Spread.CellType.TextCellType
                Next

                ReportSpread_Sheet1.Columns(0).Width = 216
                ReportSpread_Sheet1.Columns(1).Width = 77
                ReportSpread_Sheet1.Columns(2).Width = 172
                ReportSpread_Sheet1.Columns(3).Width = 216
                ReportSpread_Sheet1.Columns(4).Width = 92
                ReportSpread_Sheet1.Columns(5).Width = 109
                ReportSpread_Sheet1.Columns(6).Width = 92

                ReportSpread_Sheet1.Columns(0).Label = "Customer Name"
                ReportSpread_Sheet1.Columns(1).Label = "Order No."
                ReportSpread_Sheet1.Columns(2).Label = "Project Group"
                ReportSpread_Sheet1.Columns(3).Label = "Salesperson"
                ReportSpread_Sheet1.Columns(4).Label = "Order Loan"
                ReportSpread_Sheet1.Columns(5).Label = "Agreement No."
                ReportSpread_Sheet1.Columns(6).Label = "Total Loan"

                ReportSpread_Sheet1.Columns(0).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(1).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(2).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(3).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(4).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
                ReportSpread_Sheet1.Columns(5).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(6).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right

                'set up headings 
                ReportSpread_Sheet1.RowCount = 1
                row = 0
                reportCommand.CommandText = "SELECT DATA FROM TMPFIL WHERE FKEY = 'PRLTXT' ORDER BY TKEY "
                reportData = _Proxy.ExecuteDataTable(reportCommand)

                If reportData.Rows.Count > 0 Then
                    For t As Integer = 0 To reportData.Rows.Count - 1
                        reportMessage(t) = CStr(reportData.Rows(t)("DATA")).Trim
                    Next
                End If

                reportCommand.CommandText = String.Concat( _
                    "SELECT ", _
                        "Coalesce(dr.[NAME], 'No Customer Name') [CustomerName], ", _
                        "dp.[TILL], dp.[TRAN], dp.[AMNT], dp.[CARD], dt.[ORDN], ", _
                        "Coalesce(cm.[Name], 'No Salesperson Name') [CashierName] ", _
                    "FROM DLPAID dp ", _
                    "INNER JOIN DLTOTS dt ON ", _
                        "dt.[DATE1] = dp.[DATE1] AND ", _
                        "dt.[TILL] = dp.[TILL] AND ", _
                        "dt.[TRAN] = dp.[TRAN] AND ", _
                        "dt.[ORDN] = '000000' ", _
                    "INNER JOIN RETOPT ro ON ", _
                        "ro.[FKEY] = '01' AND ", _
                        "ro.[WION] = dp.[TYPE] ", _
                    "LEFT OUTER JOIN DLRCUS dr ON ", _
                        "dr.[DATE1] = dp.[DATE1] AND ", _
                        "dr.[TILL] = dp.[TILL] AND ", _
                        "dr.[TRAN] = dp.[TRAN] AND ", _
                        "dr.[NUMB] = 0 ", _
                    "LEFT OUTER JOIN SystemUsers cm ON ", _
                        "cm.[EmployeeCode] = dt.[CASH] ", _
                    "WHERE ", _
                        "dp.[DATE1] = @reportingDate AND ", _
                        "dp.[AMNT] < 0 ", _
                    "ORDER BY dp.[DATE1], dp.[TILL], dp.[TRAN], dp.[NUMB]" _
                )
                For i As Integer = 0 To 1
                    reportCommand.Parameters.Add("@reportingDate", SqlDbType.DateTime).Value = _ReportDate

                    MyTrace("Acquiring data")
                    reportData = _Proxy.ExecuteDataTable(reportCommand)
                    reportCommand.Parameters.Clear()

                    MyTrace("Generating report")

                    For Each reportRow As DataRow In reportData.Rows
                        workAmount = CDec(reportRow("AMNT"))
                        If workAmount > 0 Then
                            refundTotal += workAmount
                        Else
                            workAmount = -workAmount
                            saleTotal += workAmount
                        End If

                        ReportSpread_Sheet1.SetText(row, 0, CStr(reportRow("CustomerName")))
                        ReportSpread_Sheet1.SetText(row, 1, String.Format("{0}{1}", CStr(reportRow("TILL")), CStr(reportRow("TRAN"))))
                        ReportSpread_Sheet1.SetText(row, 2, "Instore Till Transaction")
                        ReportSpread_Sheet1.SetText(row, 3, CStr(reportRow("CashierName")))
                        ReportSpread_Sheet1.SetText(row, 4, workAmount.ToString("0.00"))
                        ReportSpread_Sheet1.SetText(row, 5, CStr(reportRow("CARD")).Substring(10))
                        ReportSpread_Sheet1.SetText(row, 6, workAmount.ToString("0.00"))

                        row += 1
                        ReportSpread_Sheet1.RowCount += 1
                    Next

                    Select Case i
                        Case 0
                            If saleTotal <> 0 Then
                                row += 1
                                ReportSpread_Sheet1.RowCount += 1
                                ReportSpread_Sheet1.SetText(row, 5, "Sale Sub Total:")
                                ReportSpread_Sheet1.Cells(row, 5).HorizontalAlignment = Spread.CellHorizontalAlignment.Right
                                ReportSpread_Sheet1.SetText(row, 6, saleTotal.ToString("0.00"))
                                row += 2
                                ReportSpread_Sheet1.RowCount += 2
                            End If
                        Case 1 ' print refund totals
                            If refundTotal <> 0 Then
                                row += 1
                                ReportSpread_Sheet1.RowCount += 1
                                ReportSpread_Sheet1.SetText(row, 5, "Refund Sub Total:")
                                ReportSpread_Sheet1.Cells(row, 5).HorizontalAlignment = Spread.CellHorizontalAlignment.Right
                                ReportSpread_Sheet1.SetText(row, 6, refundTotal.ToString("0.00"))
                                row += 2
                                ReportSpread_Sheet1.RowCount += 2
                            End If
                    End Select
                    reportCommand.CommandText = reportCommand.CommandText.Replace("dp.[AMNT] < 0", "dp.[AMNT] > 0")
                Next

                If refundTotal <> 0 Or saleTotal <> 0 Then ' Grand totals
                    ReportSpread_Sheet1.SetText(row, 5, "Net Total:")
                    ReportSpread_Sheet1.Cells(row, 5).HorizontalAlignment = Spread.CellHorizontalAlignment.Right
                    ReportSpread_Sheet1.SetText(row, 6, (saleTotal - refundTotal).ToString("0.00"))
                    row += 1
                    ReportSpread_Sheet1.RowCount += 1

                    ReportSpread_Sheet1.SetText(row, 0, String.Format("Store {0} {1}", _StoreName, _StoreNumber))
                    ReportSpread_Sheet1.SetText(row, 0, _StoreName)

                    row += 2
                    ReportSpread_Sheet1.RowCount += 2

                    ReportSpread_Sheet1.SetText(row, 0, String.Concat("Date ", _ReportDate.ToShortDateString))

                    row += 2
                    ReportSpread_Sheet1.RowCount += 2

                    ReportSpread_Sheet1.SetText(row, 0, String.Concat(New String(" "c, 15), reportMessage(0)))
                    row += 1
                    ReportSpread_Sheet1.RowCount += 1

                    ReportSpread_Sheet1.SetText(row, 0, String.Concat(New String(" "c, 15), reportMessage(1)))
                    row += 2
                    ReportSpread_Sheet1.RowCount += 2

                    ReportSpread_Sheet1.SetText(row, 0, String.Concat(New String(" "c, 15), "Signature"))
                    ReportSpread_Sheet1.SetText(row, 3, String.Concat(New String(" "c, 15), "Signature"))
                    row += 1
                    ReportSpread_Sheet1.RowCount += 1

                    ReportSpread_Sheet1.SetText(row, 0, String.Concat(New String(" "c, 15), "Banking Clerk ___________________________"))
                    ReportSpread_Sheet1.SetText(row, 3, String.Concat(New String(" "c, 15), "Duty Manager ___________________________"))
                    row += 3
                    ReportSpread_Sheet1.RowCount += 3

                    ReportSpread_Sheet1.SetText(row, 0, String.Concat(New String(" "c, 15), reportMessage(2)))
                    row += 1
                    ReportSpread_Sheet1.RowCount += 1

                    ReportSpread_Sheet1.SetText(row, 0, String.Concat(New String(" "c, 15), reportMessage(3)))
                End If
            End Using

            ReportSpread_Sheet1.PrintInfo.Header = _
                    String.Concat( _
                        _ShortAssemblyName, "  /  v", _FileVersion, _
                        New String(" "c, 10), "Parked Transactions Listing", _
                        New String(" "c, 10), "Store: ", _StoreNumber, "-"c, _StoreName, _
                        New String(" "c, 10), "Date Printed: ", DateTime.Now.Date.ToString("d"), _
                        "/rPage: /p" _
                   )
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        Finally
            If reportData IsNot Nothing Then
                reportData.Dispose()
                reportData = Nothing
            End If
        End Try
    End Sub

    Private Sub ReportSpread_ColumnWidthChanged(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.ColumnWidthChangedEventArgs) Handles ReportSpread.ColumnWidthChanged
        Debug.WriteLine("Report Column Widths:")
        For i As Integer = 0 To ReportSpread_Sheet1.ColumnCount - 1
            Debug.WriteLine(String.Format("ReportSpread_Sheet1.Columns({0}).Width = {1}", i, ReportSpread_Sheet1.Columns(i).Width))
        Next
    End Sub

    Private Sub ReportSpread_PrintMessageBox(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.PrintMessageBoxEventArgs) Handles ReportSpread.PrintMessageBox
        If e.BeginPrinting = False Then _PrintNotPrinted = False
    End Sub

    Private Sub Finish()
        _PrintNotPrinted = True
        Do While _PrintNotPrinted
            Application.DoEvents()
            System.Threading.Thread.Sleep(1)
        Loop
    End Sub

    Private Sub ParentForm_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs)
        RetrieveButton.PerformClick()
    End Sub

    Private Sub RetrieveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RetrieveButton.Click
        Cursor = Cursors.WaitCursor
        ButtonPanel.Enabled = False
        CollapsibleGroupBox1.Enabled = False
        Print()
        ButtonPanel.Enabled = True
        CollapsibleGroupBox1.Enabled = True
        Cursor = Cursors.Default
        PrintButton.Enabled = True

        If _CalledFromNight Then PrintButton.PerformClick()
    End Sub

    Private Sub PrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintButton.Click
        Cursor = Cursors.WaitCursor

        If ReportSpread_Sheet1.RowCount > 1 Then
            ButtonPanel.Enabled = False
            CollapsibleGroupBox1.Enabled = False

            ReportSpread_Sheet1.PrintInfo.Margin.Header = CInt(ReportSpread_Sheet1.Rows(-1).Height)
            ReportSpread.PrintSheet(0)
            'wait until printing finished
            Finish()
            ButtonPanel.Enabled = True
            CollapsibleGroupBox1.Enabled = True
            Cursor = Cursors.Default
        End If

        If _CalledFromNight Then ExitButton.PerformClick()
    End Sub

    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click
        FindForm.Close()
    End Sub

    Private Sub _KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs)
        Select Case e.KeyCode
            Case Keys.F5
                RetrieveButton.PerformClick()
            Case Keys.F9
                PrintButton.PerformClick()
            Case Keys.F12
                ExitButton.PerformClick()
        End Select
    End Sub

    Private Sub dtpReportDate_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dtpReportDate.ValueChanged
        _ReportDate = CType(sender, DateTimePicker).Value.Date
        PrintButton.Enabled = False
    End Sub

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        AddHandler FindForm.Shown, AddressOf ParentForm_Shown
        AddHandler FindForm.KeyDown, AddressOf _KeyDown
        FindForm.KeyPreview = True
        ReportSpread_Sheet1.OperationMode = Spread.OperationMode.ReadOnly
    End Sub

    Private Sub MyTrace(ByVal message As String)
        Trace.WriteLine(String.Format("{0}/v{1} - {2}", _ShortAssemblyName, _FileVersion, message))
    End Sub

End Class
