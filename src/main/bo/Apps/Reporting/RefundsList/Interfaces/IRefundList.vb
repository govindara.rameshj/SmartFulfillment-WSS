﻿Public Interface IRefundList

    Function GetRefundDetails(ByVal reportDate As Date) As DataTable
    Function GetTenderDesriptions() As DataTable
    Function SetReportColumnCount() As Integer
    Sub SetReportwidth(ByRef Sheet As FarPoint.Win.Spread.SheetView)
    Sub SetReportColumnNames(ByRef Sheet As FarPoint.Win.Spread.SheetView)
    Sub SetReportColumnAlignment(ByRef Sheet As FarPoint.Win.Spread.SheetView)
    Sub SetValueForReportColumnRefundCashier(ByRef Sheet As FarPoint.Win.Spread.SheetView, ByVal row As Integer, ByVal value As DataRow)
    Function GetTenderDetails(ByVal reportDate As Date, ByVal tillId As String, ByVal tranNo As String) As DataTable
    Function GetLineDetails(ByVal reportDate As Date, ByVal tillId As String, ByVal tranNo As String) As DataTable
    Sub SetValueForReportColumnSupervisor(ByRef Sheet As FarPoint.Win.Spread.SheetView, ByVal row As Integer, ByVal value As DataRow)
    Sub SetValueForReportColumnTime(ByRef Sheet As FarPoint.Win.Spread.SheetView, ByVal row As Integer, ByVal value As DataRow)
    Sub SetValueForReportColumnSkuNumber(ByRef Sheet As FarPoint.Win.Spread.SheetView, ByVal row As Integer, ByVal value As DataRow)
    Sub SetValueForReportColumnRefundType(ByRef Sheet As FarPoint.Win.Spread.SheetView, ByVal row As Integer, ByVal reportDate As Date, ByVal tillId As String, ByVal tranNo As String)
    Sub SetValueForReportColumnDescription(ByRef Sheet As FarPoint.Win.Spread.SheetView, ByVal row As Integer, ByVal value As DataRow, ByVal stockTypes As Dictionary(Of Char, String))
    ReadOnly Property ReportColumnPriceIndex() As Integer
    ReadOnly Property ReportColumnQuantityIndex() As Integer
    ReadOnly Property ReportColumnExtpIndex() As Integer
    ReadOnly Property ReportColumnTenderDescriptionIndex() As Integer
    ReadOnly Property ReportColumnTenderAmountIndex() As Integer
    ReadOnly Property ReportColumnInonIndex() As Integer
    ReadOnly Property ReportColumnSaltIndex() As Integer
    ReadOnly Property ReportColumnDescriptionIndex() As Integer
    Function SetUpPrintReportInfo() As FarPoint.Win.Spread.PrintInfo
End Interface
