﻿Imports TpWickes.Library

Public Class RefundListFactory
    Inherits RequirementSwitchFactory(Of IRefundList)

    Public Overrides Function ImplementationA() As IRefundList
        Return New RefundList
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(-22030)
    End Function

    Public Overrides Function ImplementationB() As IRefundList
        Return New RefundListOld
    End Function
End Class
