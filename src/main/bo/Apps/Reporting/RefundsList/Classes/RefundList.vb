﻿Public Class RefundList
    Implements IRefundList

#Region "Private Variables"
    Private Const refundListColumnCount As Integer = 16
    Private Const refundListColumnTimeIndex As Integer = 4
    Private Const refundListColumnRefundCashierIndex As Integer = 5
    Private Const refundListColumnSupervisorIndex As Integer = 6
    Private Const refundListColumnSkuNumberIndex As Integer = 7
    Private Const refundListColumnDescriptionIndex As Integer = 8
    Private Const refundListColumnInonIndex As Integer = 9
    Private Const refundListColumnPriceIndex As Integer = 10
    Private Const refundListColumnQuantityIndex As Integer = 11
    Private Const refundListColumnExtpIndex As Integer = 12
    Private Const refundListColumnTenderDescriptionIndex As Integer = 13
    Private Const refundListColumnTenderAmountIndex As Integer = 14
    Private Const refund As String = "Refund"
    Private Const exchange As String = "Exchange"
    Private refundListRepository As New RefundListRepository
#End Region

#Region "Functions & Procedures"

    Public Function GetRefundDetails(ByVal reportDate As Date) As System.Data.DataTable Implements IRefundList.GetRefundDetails
        Return refundListRepository.GetRefundListDetails(reportDate)
    End Function

    Public Function GetTenderDesriptions() As System.Data.DataTable Implements IRefundList.GetTenderDesriptions
        Return refundListRepository.GetTenderDescriptionDetails
    End Function

    Friend Function SetReportColumnCount() As Integer Implements IRefundList.SetReportColumnCount
        Return refundListColumnCount
    End Function

    Friend Sub SetReportwidth(ByRef Sheet As FarPoint.Win.Spread.SheetView) Implements IRefundList.SetReportwidth
        Sheet.Columns(0).Width = 112
        Sheet.Columns(1).Width = 29
        Sheet.Columns(2).Width = 46
        Sheet.Columns(3).Width = 132
        Sheet.Columns(4).Width = 46
        Sheet.Columns(5).Width = 112
        Sheet.Columns(6).Width = 112
        Sheet.Columns(7).Width = 58
        Sheet.Columns(8).Width = 261
        Sheet.Columns(9).Width = 80
        Sheet.Columns(10).Width = 52
        Sheet.Columns(11).Width = 48
        Sheet.Columns(12).Width = 61
        Sheet.Columns(13).Width = 62
        Sheet.Columns(14).Width = 64
        Sheet.Columns(15).Width = 100
    End Sub

    Friend Sub SetReportColumnNames(ByRef Sheet As FarPoint.Win.Spread.SheetView) Implements IRefundList.SetReportColumnNames
        Sheet.Columns(0).Label = "SALE CASHIER"
        Sheet.Columns(1).Label = "TILL"
        Sheet.Columns(2).Label = "TRAN"
        Sheet.Columns(3).Label = "OVC TRAN"
        Sheet.Columns(4).Label = "TIME"
        Sheet.Columns(5).Label = "REFUND CASHIER"
        Sheet.Columns(6).Label = "SUPV"
        Sheet.Columns(7).Label = "SKU NO"
        Sheet.Columns(8).Label = "DESCRIPTION"
        Sheet.Columns(9).Label = "NON STOCK"
        Sheet.Columns(10).Label = "PRICE"
        Sheet.Columns(11).Label = "QUAN"
        Sheet.Columns(12).Label = "VALUE"
        Sheet.Columns(13).Label = "TENDER"
        Sheet.Columns(14).Label = "AMOUNT"
        Sheet.Columns(15).Label = "REFUND TYPE"
    End Sub

    Friend Sub SetReportColumnAlignment(ByRef Sheet As FarPoint.Win.Spread.SheetView) Implements IRefundList.SetReportColumnAlignment
        Sheet.Columns(0).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Sheet.Columns(1).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Sheet.Columns(2).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Sheet.Columns(3).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Sheet.Columns(4).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Sheet.Columns(5).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Sheet.Columns(6).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Sheet.Columns(7).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Sheet.Columns(8).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Sheet.Columns(9).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Center
        Sheet.Columns(10).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
        Sheet.Columns(11).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
        Sheet.Columns(12).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
        Sheet.Columns(13).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Sheet.Columns(14).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
        Sheet.Columns(15).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
    End Sub

    Friend Sub SetValueForReportColumnTime(ByRef Sheet As FarPoint.Win.Spread.SheetView, ByVal row As Integer, ByVal value As System.Data.DataRow) Implements IRefundList.SetValueForReportColumnTime
        Sheet.SetText(row, refundListColumnTimeIndex, CStr(value("TIME")))
    End Sub

    Public Function GetTenderDetails(ByVal reportDate As Date, ByVal tillId As String, ByVal tranNo As String) As System.Data.DataTable Implements IRefundList.GetTenderDetails
        Return refundListRepository.GetTenderValueDetails(reportDate, tillId, tranNo)
    End Function

    Public Function GetLineDetails(ByVal reportDate As Date, ByVal tillId As String, ByVal tranNo As String) As System.Data.DataTable Implements IRefundList.GetLineDetails
        Return refundListRepository.GetTransactionLineDetails(reportDate, tillId, tranNo)
    End Function

    Public Sub SetValueForReportColumnSupervisor(ByRef Sheet As FarPoint.Win.Spread.SheetView, ByVal row As Integer, ByVal value As System.Data.DataRow) Implements IRefundList.SetValueForReportColumnSupervisor
        Dim workString As String

        workString = CStr(value("RMAN"))
        If workString = "000" Then
            workString = CStr(value("RSUP"))
            If workString = "000" Then
                workString = String.Empty
            Else
                workString = String.Concat(value("RSUP"), " "c)
                If Not value("SupervisorName") Is DBNull.Value Then
                    workString += CStr(value("SupervisorName")).Trim
                End If
            End If
        Else
            workString += "M" + Space(1)
            If Not value("ManagerName") Is DBNull.Value Then
                workString += CStr(value("ManagerName")).Trim
            End If
        End If
        Sheet.SetText(row, refundListColumnSupervisorIndex, workString)
    End Sub

    Public Sub SetValueForReportColumnRefundCashier(ByRef Sheet As FarPoint.Win.Spread.SheetView, ByVal row As Integer, ByVal value As System.Data.DataRow) Implements IRefundList.SetValueForReportColumnRefundCashier
        Dim workString As String

        If CStr(value("RefundCashier")) <> "000" Then
            workString = String.Concat(value("RefundCashier"), " "c)
            If value("RefundCashierName") Is DBNull.Value Then
                workString += "**  Not Known **"
            Else
                workString += CStr(value("RefundCashierName")).Trim
            End If
        Else
            workString = String.Concat(value("EmployeeCode"), " "c, value("Name")).Trim
        End If
        Sheet.SetText(row, refundListColumnRefundCashierIndex, workString)
    End Sub
    Public Sub SetValueForReportColumnRefundType(ByRef Sheet As FarPoint.Win.Spread.SheetView, ByVal row As Integer, ByVal reportDate As Date, ByVal tillId As String, ByVal tranNo As String) Implements IRefundList.SetValueForReportColumnRefundType
        Dim DT As DataTable
        DT = refundListRepository.GetRefundType(reportDate, tillId, tranNo)

        If CDbl(DT.Rows(0)(0).ToString) > 0 Then
            Sheet.SetText(row, 15, exchange)
        Else
            Sheet.SetText(row, 15, refund)
        End If
    End Sub

    Public Sub SetValueForReportColumnDescription(ByRef Sheet As FarPoint.Win.Spread.SheetView, ByVal row As Integer, ByVal value As System.Data.DataRow, ByVal stockTypes As Dictionary(Of Char, String)) Implements IRefundList.SetValueForReportColumnDescription
        Dim workString As String

        If value("DESCR") Is DBNull.Value Then
            Sheet.SetText(row, refundListColumnDescriptionIndex, "** Sku Unknown **")
            Sheet.SetText(row, refundListColumnInonIndex, "N/A")
        Else
            Sheet.SetText(row, refundListColumnDescriptionIndex, CStr(value("DESCR")))

            If CBool(value("INON")) Then
                workString = "YES"
            Else
                workString = "NO"
            End If
            Sheet.SetText(row, refundListColumnInonIndex, workString)

            If stockTypes.Keys.Contains(CStr(value("SALT"))(0)) Then
                workString = stockTypes(CStr(value("SALT"))(0))
            Else
                workString = stockTypes("O"c) ' Other
            End If
        End If
    End Sub


    Friend Sub SetValueForReportColumnSkuNumber(ByRef Sheet As FarPoint.Win.Spread.SheetView, ByVal row As Integer, ByVal value As System.Data.DataRow) Implements IRefundList.SetValueForReportColumnSkuNumber
        Sheet.SetText(row, refundListColumnSkuNumberIndex, CStr(value("SKUN")))
    End Sub
    Friend Function SetUpPrintReportInfo() As FarPoint.Win.Spread.PrintInfo Implements IRefundList.SetUpPrintReportInfo
        Dim printRules As New FarPoint.Win.Spread.SmartPrintRulesCollection()
        Dim printSet As New FarPoint.Win.Spread.PrintInfo()

        printRules.Add(New FarPoint.Win.Spread.BestFitColumnRule(FarPoint.Win.Spread.ResetOption.None))
        printRules.Add(New FarPoint.Win.Spread.LandscapeRule(FarPoint.Win.Spread.ResetOption.None))
        printRules.Add(New FarPoint.Win.Spread.ScaleRule(FarPoint.Win.Spread.ResetOption.All, 1.0F, 0.4F, 0.2F))
        printSet.SmartPrintRules = printRules
        printSet.UseSmartPrint = True

        Return printSet
    End Function
#End Region

#Region "Properties"

    Friend ReadOnly Property ReportColumnDescriptionIndex() As Integer Implements IRefundList.ReportColumnDescriptionIndex
        Get
            Return refundListColumnDescriptionIndex
        End Get
    End Property

    Friend ReadOnly Property ReportColumnExtpIndex() As Integer Implements IRefundList.ReportColumnExtpIndex
        Get
            Return refundListColumnExtpIndex
        End Get
    End Property

    Friend ReadOnly Property ReportColumnInonIndex() As Integer Implements IRefundList.ReportColumnInonIndex
        Get
            Return refundListColumnInonIndex
        End Get
    End Property

    Friend ReadOnly Property ReportColumnPriceIndex() As Integer Implements IRefundList.ReportColumnPriceIndex
        Get
            Return refundListColumnPriceIndex
        End Get
    End Property

    Friend ReadOnly Property ReportColumnQuantityIndex() As Integer Implements IRefundList.ReportColumnQuantityIndex
        Get
            Return refundListColumnQuantityIndex
        End Get
    End Property

    Friend ReadOnly Property ReportColumnSaltIndex() As Integer Implements IRefundList.ReportColumnSaltIndex
        Get

        End Get
    End Property

    Friend ReadOnly Property ReportColumnTenderAmountIndex() As Integer Implements IRefundList.ReportColumnTenderAmountIndex
        Get
            Return refundListColumnTenderAmountIndex
        End Get
    End Property

    Friend ReadOnly Property ReportColumnTenderDescriptionIndex() As Integer Implements IRefundList.ReportColumnTenderDescriptionIndex
        Get
            Return refundListColumnTenderDescriptionIndex
        End Get
    End Property
#End Region


End Class
