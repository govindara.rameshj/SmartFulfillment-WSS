﻿Public Class RefundListOld
    Implements IRefundList



#Region "Private Variables"
    Private Const refundListColumnCount As Integer = 14
    Private Const refundListColumnRefundCashierIndex As Integer = 3
    Private Const refundListColumnSupervisorIndex As Integer = 4
    Private Const refundListColumnSkuNumberIndex As Integer = 5
    Private Const refundListColumnDescriptionIndex As Integer = 6
    Private Const refundListColumnInonIndex As Integer = 7
    Private Const refundListColumnSaltIndex As Integer = 8
    Private Const refundListColumnPriceIndex As Integer = 9
    Private Const refundListColumnQuantityIndex As Integer = 10
    Private Const refundListColumnExtpIndex As Integer = 11
    Private Const refundListColumnTenderDescriptionIndex As Integer = 12
    Private Const refundListColumnTenderAmountIndex As Integer = 13
    Private refundListRepository As New RefundListRepositoryExisting
#End Region

#Region "Functions & Procedures"

    Public Function GetRefundDetails(ByVal reportDate As Date) As System.Data.DataTable Implements IRefundList.GetRefundDetails
        Return refundListRepository.GetRefundListDetails(reportDate)
    End Function

    Public Function GetTenderDesriptions() As System.Data.DataTable Implements IRefundList.GetTenderDesriptions
        Return refundListRepository.GetTenderDescriptionDetails
    End Function

    Public Function SetReportColumnCount() As Integer Implements IRefundList.SetReportColumnCount
        Return refundListColumnCount
    End Function

    Friend Sub SetReportwidth(ByRef Sheet As FarPoint.Win.Spread.SheetView) Implements IRefundList.SetReportwidth
        Sheet.Columns(0).Width = 112
        Sheet.Columns(1).Width = 29
        Sheet.Columns(2).Width = 46
        Sheet.Columns(3).Width = 112
        Sheet.Columns(4).Width = 112
        Sheet.Columns(5).Width = 58
        Sheet.Columns(6).Width = 261
        Sheet.Columns(7).Width = 80
        Sheet.Columns(8).Width = 84
        Sheet.Columns(9).Width = 52
        Sheet.Columns(10).Width = 48
        Sheet.Columns(11).Width = 61
        Sheet.Columns(12).Width = 62
        Sheet.Columns(13).Width = 64
    End Sub

    Friend Sub SetReportColumnNames(ByRef Sheet As FarPoint.Win.Spread.SheetView) Implements IRefundList.SetReportColumnNames
        Sheet.Columns(0).Label = "SALE CASHIER"
        Sheet.Columns(1).Label = "TILL"
        Sheet.Columns(2).Label = "TRAN"
        Sheet.Columns(3).Label = "REFUND CASHIER"
        Sheet.Columns(4).Label = "SUPV"
        Sheet.Columns(5).Label = "SKU NO"
        Sheet.Columns(6).Label = "DESCRIPTION"
        Sheet.Columns(7).Label = "NON STOCK"
        Sheet.Columns(8).Label = "STOCK TYPE"
        Sheet.Columns(9).Label = "PRICE"
        Sheet.Columns(10).Label = "QUAN"
        Sheet.Columns(11).Label = "VALUE"
        Sheet.Columns(12).Label = "TENDER"
        Sheet.Columns(13).Label = "AMOUNT"
    End Sub

    Friend Sub SetReportColumnAlignment(ByRef Sheet As FarPoint.Win.Spread.SheetView) Implements IRefundList.SetReportColumnAlignment
        Sheet.Columns(0).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Sheet.Columns(1).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Sheet.Columns(2).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Sheet.Columns(3).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Sheet.Columns(4).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Sheet.Columns(5).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Sheet.Columns(6).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Sheet.Columns(7).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Center
        Sheet.Columns(8).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Sheet.Columns(9).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
        Sheet.Columns(10).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
        Sheet.Columns(11).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
        Sheet.Columns(12).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Sheet.Columns(13).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
    End Sub

    Public Sub SetValueForReportColumnTime(ByRef Sheet As FarPoint.Win.Spread.SheetView, ByVal row As Integer, ByVal value As System.Data.DataRow) Implements IRefundList.SetValueForReportColumnTime

    End Sub

    Public Function GetTenderDetails(ByVal reportDate As Date, ByVal tillId As String, ByVal tranNo As String) As System.Data.DataTable Implements IRefundList.GetTenderDetails
        Return refundListRepository.GetTenderValueDetails(reportDate, tillId, tranNo)
    End Function

    Public Function GetLineDetails(ByVal reportDate As Date, ByVal tillId As String, ByVal tranNo As String) As System.Data.DataTable Implements IRefundList.GetLineDetails
        Return refundListRepository.GetTransactionLineDetails(reportDate, tillId, tranNo)
    End Function

    Public Sub SetValueForReportColumnSupervisor(ByRef Sheet As FarPoint.Win.Spread.SheetView, ByVal row As Integer, ByVal value As System.Data.DataRow) Implements IRefundList.SetValueForReportColumnSupervisor

        Dim workString As String

        workString = CStr(value("RMAN"))
        If workString = "000" Then
            workString = CStr(value("RSUP"))
            If workString = "000" Then
                workString = CStr(value("SUPV"))
                If workString = "000" Then
                    workString = String.Empty
                End If
            End If
        Else
            workString += "M"
        End If
        Sheet.SetText(row, refundListColumnSupervisorIndex, workString)
    End Sub

    Public Sub SetValueForReportColumnRefundCashier(ByRef Sheet As FarPoint.Win.Spread.SheetView, ByVal row As Integer, ByVal value As System.Data.DataRow) Implements IRefundList.SetValueForReportColumnRefundCashier
        Dim workString As String

        If CStr(value("RefundCashier")) <> "000" Then
            workString = String.Concat(value("RefundCashier"), " "c)
            If value("RefundCashierName") Is DBNull.Value Then
                workString += "**  Not Known **"
            Else
                workString += CStr(value("RefundCashierName")).Trim
            End If
        Else
            workString = String.Concat(value("EmployeeCode"), " "c, value("Name")).Trim
        End If
        Sheet.SetText(row, refundListColumnRefundCashierIndex, workString)
    End Sub

    Public Sub SetValueForReportColumnDescription(ByRef Sheet As FarPoint.Win.Spread.SheetView, ByVal row As Integer, ByVal value As System.Data.DataRow, ByVal stockTypes As Dictionary(Of Char, String)) Implements IRefundList.SetValueForReportColumnDescription
        Dim workString As String

        If value("DESCR") Is DBNull.Value Then
            Sheet.SetText(row, refundListColumnDescriptionIndex, "** Sku Unknown **")
            Sheet.SetText(row, refundListColumnInonIndex, "N/A")
            Sheet.SetText(row, refundListColumnSaltIndex, "N/A")
        Else
            Sheet.SetText(row, refundListColumnDescriptionIndex, CStr(value("DESCR")))

            If CBool(value("INON")) Then
                workString = "YES"
            Else
                workString = "NO"
            End If
            Sheet.SetText(row, refundListColumnInonIndex, workString)

            If stockTypes.Keys.Contains(CStr(value("SALT"))(0)) Then
                workString = stockTypes(CStr(value("SALT"))(0))
            Else
                workString = stockTypes("O"c) ' Other
            End If
            Sheet.SetText(row, refundListColumnSaltIndex, workString)
        End If
    End Sub

    Friend Sub SetValueForReportColumnSkuNumber(ByRef Sheet As FarPoint.Win.Spread.SheetView, ByVal row As Integer, ByVal value As System.Data.DataRow) Implements IRefundList.SetValueForReportColumnSkuNumber
        Sheet.SetText(row, refundListColumnSkuNumberIndex, CStr(value("SKUN")))
    End Sub
    Friend Sub SetValueForReportColumnRefundType(ByRef Sheet As FarPoint.Win.Spread.SheetView, ByVal row As Integer, ByVal reportDate As Date, ByVal tillId As String, ByVal tranNo As String) Implements IRefundList.SetValueForReportColumnRefundType

    End Sub
    Friend Function SetUpPrintReportInfo() As FarPoint.Win.Spread.PrintInfo Implements IRefundList.SetUpPrintReportInfo
        Return Nothing
    End Function
#End Region

#Region "Properties"

    Friend ReadOnly Property ReportColumnDescriptionIndex() As Integer Implements IRefundList.ReportColumnDescriptionIndex
        Get
            Return refundListColumnDescriptionIndex
        End Get
    End Property

    Friend ReadOnly Property ReportColumnExtpIndex() As Integer Implements IRefundList.ReportColumnExtpIndex
        Get
            Return refundListColumnExtpIndex
        End Get
    End Property

    Friend ReadOnly Property ReportColumnInonIndex() As Integer Implements IRefundList.ReportColumnInonIndex
        Get
            Return refundListColumnInonIndex
        End Get
    End Property

    Friend ReadOnly Property ReportColumnPriceIndex() As Integer Implements IRefundList.ReportColumnPriceIndex
        Get
            Return refundListColumnPriceIndex
        End Get
    End Property

    Friend ReadOnly Property ReportColumnQuantityIndex() As Integer Implements IRefundList.ReportColumnQuantityIndex
        Get
            Return refundListColumnQuantityIndex
        End Get
    End Property

    Friend ReadOnly Property ReportColumnSaltIndex() As Integer Implements IRefundList.ReportColumnSaltIndex
        Get
            Return refundListColumnSaltIndex
        End Get
    End Property

    Friend ReadOnly Property ReportColumnTenderAmountIndex() As Integer Implements IRefundList.ReportColumnTenderAmountIndex
        Get
            Return refundListColumnTenderAmountIndex
        End Get
    End Property

    Friend ReadOnly Property ReportColumnTenderDescriptionIndex() As Integer Implements IRefundList.ReportColumnTenderDescriptionIndex
        Get
            Return refundListColumnTenderDescriptionIndex
        End Get
    End Property
#End Region


    
End Class
