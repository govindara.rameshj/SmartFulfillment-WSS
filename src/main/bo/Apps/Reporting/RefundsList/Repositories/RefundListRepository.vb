﻿Imports Oasys.Data

Public Class RefundListRepository

    Public Function GetRefundListDetails(ByVal reportDate As Date) As DataTable
        Dim DT As DataTable
        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = "usp_GetRefundListDetails"
                com.AddParameter("@ReportDate", reportDate, SqlDbType.Date)
                DT = com.ExecuteDataTable
            End Using
        End Using
        Return DT
    End Function

    Public Function GetTenderDescriptionDetails() As DataTable
        Dim DT As DataTable
        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = "usp_GetTenderDescriptions"
                DT = com.ExecuteDataTable
            End Using
        End Using
        Return DT
    End Function
    Public Function GetTenderValueDetails(ByVal reportDate As Date, ByVal tillId As String, ByVal tranNo As String) As DataTable
        Dim DT As DataTable
        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = "usp_GetTenderDetails"
                com.AddParameter("@ReportDate", reportDate, SqlDbType.Date)
                com.AddParameter("@TillId", tillId, SqlDbType.Char)
                com.AddParameter("@TranNo", tranNo, SqlDbType.Char)
                DT = com.ExecuteDataTable
            End Using
        End Using
        Return DT
    End Function
    Public Function GetTransactionLineDetails(ByVal reportDate As Date, ByVal tillId As String, ByVal tranNo As String) As DataTable
        Dim DT As DataTable
        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = "usp_GetTransactionLines"
                com.AddParameter("@ReportDate", reportDate, SqlDbType.Date)
                com.AddParameter("@TillId", tillId, SqlDbType.Char)
                com.AddParameter("@TranNo", tranNo, SqlDbType.Char)
                DT = com.ExecuteDataTable
            End Using
        End Using
        Return DT
    End Function

    Public Function GetRefundType(ByVal reportDate As Date, ByVal tillId As String, ByVal tranNo As String) As DataTable
        Dim DT As DataTable
        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = "usp_GetRefundType"
                com.AddParameter("@ReportDate", reportDate, SqlDbType.Date)
                com.AddParameter("@TillId", tillId, SqlDbType.Char)
                com.AddParameter("@TranNo", tranNo, SqlDbType.Char)
                DT = com.ExecuteDataTable
            End Using
        End Using
        Return DT
    End Function

End Class
