﻿Imports Oasys.Data

Public Class RefundListRepositoryExisting

    Public Function GetRefundListDetails(ByVal reportDate As Date) As DataTable
        Dim DT As DataTable
        Using con As New Connection
            Using com As New Command(con)
                com.CommandText = String.Concat( _
               "SELECT ", _
                   "CM.[EmployeeCode], CM.[Name], DT.[TCOD], DT.[TOTL], DT.[RMAN], ", _
                   "DT.[TILL],DT.[TIME],DT.[TRAN], DT.[SUPV], DT.[RSUP], DT.[RCAS] RefundCashier, RC.[Name] RefundCashierName, SU.Name SupervisorName ", _
               "FROM SystemUsers CM ", _
               "INNER JOIN DLTOTS DT ON ", _
                   "DT.[CASH] = CM.[EmployeeCode] AND ", _
                   "DT.[DATE1] = @reportingDate AND ", _
                   "DT.[VOID] = 0 AND ", _
                   "DT.[TMOD] = 0 AND ", _
                   "(DT.[TCOD] = 'SA' OR DT.[TCOD] = 'RF') ", _
               "LEFT OUTER JOIN SystemUsers RC ON ", _
                   "RC.[EmployeeCode] = DT.[RCAS] ", _
                   "LEFT OUTER JOIN SystemUsers SU ON SU.[EmployeeCode]= DT.[RSUP] ", _
               "ORDER BY CM.[EmployeeCode], DT.[TIME], DT.[TILL], DT.[TRAN]")
                com.AddParameter("@reportingDate", reportDate)
                DT = com.ExecuteDataTable()
            End Using
        End Using
        Return DT
    End Function

    Public Function GetTenderDescriptionDetails() As DataTable
        Dim DT As DataTable
        Using con As New Connection
            Using com As New Command(con)
                com.CommandText = "SELECT TTDE1, TTDE2, TTDE3, TTDE4, TTDE5, TTDE6, TTDE7, TTDE8, TTDE9, TTDE10 FROM RETOPT WHERE FKEY = '01'"
                DT = com.ExecuteDataTable()
            End Using
        End Using
        Return DT
    End Function

    Public Function GetTenderValueDetails(ByVal reportDate As Date, ByVal tillId As String, ByVal tranNo As String) As DataTable
        Dim DT As DataTable
        Using con As New Connection
            Using com As New Command(con)
                com.CommandText = String.Concat( _
                    "SELECT DP.[AMNT], DP.[TYPE] ", _
                    "FROM DLPAID DP ", _
                    "WHERE ", _
                        "DATE1=@reportingDate AND ", _
                        "TILL=@reportTill AND ", _
                        "[TRAN]=@reportTran AND ", _
                        "[TYPE]<>1 AND ", _
                        "[TYPE]<>99 ")
                com.AddParameter("@reportingDate", reportDate)
                com.AddParameter("@reportTill", tillId)
                com.AddParameter("@reportTran", tranNo)
                DT = com.ExecuteDataTable()
            End Using
        End Using
        Return DT
    End Function
    Public Function GetTransactionLineDetails(ByVal reportDate As Date, ByVal tillId As String, ByVal tranNo As String) As DataTable
        Dim DT As DataTable
        Using con As New Connection
            Using com As New Command(con)
                com.CommandText = String.Concat( _
                    "SELECT ", _
                        "DL.[SKUN], DL.[PRIC], DL.[QUAN], DL.[EXTP], ", _
                        "IM.[DESCR], IM.[INON], IM.[SALT], ", _
                        "DL.[QBPD], DL.[DGPD], DL.[MBPD], DL.[HSPD], DL.[ESEV] ", _
                    "FROM DLLINE DL ", _
                    "LEFT OUTER JOIN STKMAS IM ON ", _
                        "IM.SKUN = DL.SKUN ", _
                    "WHERE ", _
                        "DATE1=@reportingDate AND ", _
                        "TILL=@reportTill AND ", _
                        "[TRAN]=@reportTran AND ", _
                        "QUAN < 0 AND ", _
                        "LREV = 0 ", _
                    "ORDER BY NUMB" _
                )
                com.AddParameter("@reportingDate", reportDate)
                com.AddParameter("@reportTill", tillId)
                com.AddParameter("@reportTran", tranNo)
                DT = com.ExecuteDataTable()
            End Using
        End Using
        Return DT
    End Function
End Class
