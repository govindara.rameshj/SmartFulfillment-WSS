﻿Imports FarPoint.Win
Imports System.Data.SqlClient
Imports Oasys.Data
Imports Cts.Oasys.Core.System

Public Class RefundsList
    Private _printNotPrinted As Boolean
    Private _storeIdName As String
    Private _shortAssemblyName As String
    Private _fileVersion As String
    Private refundList As IRefundList

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
        refundList = (New RefundListFactory).GetImplementation
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyCode
            Case Keys.F5 : RetrieveButton.PerformClick()
            Case Keys.F9 : PrintButton.PerformClick()
            Case Keys.F12 : ExitButton.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Protected Overrides Sub DoProcessing()

        With FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetAssembly(Me.GetType).Location)
            _shortAssemblyName = .FileName.Substring(.FileName.LastIndexOf("\"c) + 1, .FileName.LastIndexOf("."c) - .FileName.LastIndexOf("\"c) - 1)
            _fileVersion = .FileVersion
        End With

        dtpReportDate.Value = Dates.GetToday
        _storeIdName = Store.GetIdName

        RetrieveButton.PerformClick()

        If RunParameters.Contains(",CFC") Then
            PrintButton.PerformClick()
            ExitButton.PerformClick()
        End If

    End Sub
    Private Function GetStockTypes() As Collections.Generic.Dictionary(Of Char, String)
        Dim stockTypes As New Collections.Generic.Dictionary(Of Char, String)
        stockTypes.Add("B"c, "Basic Item")
        stockTypes.Add("P"c, "Performance Item")
        stockTypes.Add("A"c, "Aesthetic Item")
        stockTypes.Add("S"c, "Showroom Item")
        stockTypes.Add("G"c, "Good Pallet Item")
        stockTypes.Add("R"c, "Reduced Pallet Item")
        stockTypes.Add("D"c, "Delivery Item")
        stockTypes.Add("I"c, "Installation Item")
        stockTypes.Add("O"c, "Other")
        Return stockTypes
    End Function
    Private Sub PopulateTenderDescriptions(ByVal tenderDescriptions As String(), ByVal detailData As DataTable)
        For i As Integer = 0 To 19
            tenderDescriptions(i) = CStr(detailData.Rows(0)(String.Concat("TTDE", (i + 1).ToString))).Trim
        Next
    End Sub
    Private Sub SetReportCellType()
        For i As Integer = 0 To ReportSpread_Sheet1.ColumnCount - 1
            ReportSpread_Sheet1.Columns(i).CellType = New Spread.CellType.TextCellType
        Next
    End Sub
    Private Sub SetUpReportHeadings()
        If ReportSpread_Sheet1.RowCount > 0 Then
            ReportSpread_Sheet1.RemoveRows(0, ReportSpread_Sheet1.RowCount)
        End If
    End Sub
    Private Sub SetUpReportData(ByRef row As Integer, ByVal tenderDescriptions As String(), ByRef reportData As DataTable, ByVal detailData As DataTable)
        MyTrace("Beginning")
        detailData = refundList.GetTenderDesriptions()
        PopulateTenderDescriptions(tenderDescriptions, detailData)
        MyTrace("Acquiring transaction list")
        reportData = refundList.GetRefundDetails(dtpReportDate.Value)
        ReportSpread.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ReportSpread_Sheet1.ColumnCount = refundList.SetReportColumnCount
        SetReportCellType()
        refundList.SetReportwidth(ReportSpread_Sheet1)
        SetUpReportHeadings()
        row = 0
        ReportSpread_Sheet1.RowCount = 1
        refundList.SetReportColumnNames(ReportSpread_Sheet1)
        refundList.SetReportColumnAlignment(ReportSpread_Sheet1)
    End Sub
    Private Sub SetCurrentTillAndCashier(ByVal row As Integer, ByRef currentCashier As String, ByRef currentTill As String, ByVal workCashier As String, ByVal reportRow As DataRow)
        If currentCashier <> workCashier Or currentCashier.Length = 0 Then
            ReportSpread_Sheet1.SetText(row, 0, String.Concat(workCashier, " "c, CStr(reportRow("Name")).Trim))
            currentCashier = workCashier
            currentTill = String.Empty
        End If
    End Sub
    Private Shared Function PopulateTenderAmounts(ByVal tenderAmounts As Decimal(), ByVal detailData As DataTable, ByVal reportRow As DataRow) As Integer
        Dim tenderIndex As Integer

        For i As Integer = 0 To 19
            tenderAmounts(i) = 0
        Next

        tenderAmounts(0) = CDec(reportRow("TOTL"))
        Dim tenderType As Integer
        For Each detailRow As DataRow In detailData.Rows
            tenderType = CInt(detailRow("TYPE")) - 1
            If tenderType <> 0 Then
                tenderAmounts(0) += CDec(detailRow("AMNT"))
                tenderAmounts(tenderType) -= CDec(detailRow("AMNT"))
            End If
        Next
        tenderIndex = 0
        Return tenderIndex
    End Function
    Private Sub SetTenderDescriptionAndAmount(ByVal row As Integer, ByVal tenderAmounts As Decimal(), ByVal tenderDescriptions As String(), ByRef tenderIndex As Integer)
        While tenderIndex < 20 AndAlso tenderAmounts(tenderIndex) = 0
            tenderIndex += 1
        End While

        If tenderIndex < 20 Then
            If tenderIndex < 10 Then
                ReportSpread_Sheet1.SetText(row, refundList.ReportColumnTenderDescriptionIndex, tenderDescriptions(tenderIndex))
            Else
                ReportSpread_Sheet1.SetText(row, refundList.ReportColumnTenderDescriptionIndex, String.Concat("M-", tenderDescriptions(tenderIndex)))
            End If
            ReportSpread_Sheet1.SetText(row, refundList.ReportColumnTenderAmountIndex, tenderAmounts(tenderIndex).ToString("0.00"))
            tenderIndex += 1
        End If
    End Sub
    Private Sub SetReportTenderDetails(ByRef row As Integer, ByVal tenderAmounts As Decimal(), ByVal tenderDescriptions As String(), ByRef tenderIndex As Integer)
        If tenderIndex < 20 Then
            While tenderIndex < 20
                If tenderAmounts(tenderIndex) <> 0 Then
                    If tenderIndex < 10 Then
                        ReportSpread_Sheet1.SetText(row, refundList.ReportColumnTenderDescriptionIndex, tenderDescriptions(tenderIndex))
                    Else
                        ReportSpread_Sheet1.SetText(row, refundList.ReportColumnTenderDescriptionIndex, String.Concat("M-", tenderDescriptions(tenderIndex - 10)))
                    End If
                    ReportSpread_Sheet1.SetText(row, refundList.ReportColumnTenderAmountIndex, tenderAmounts(tenderIndex).ToString("0.00"))
                    row += 1
                    ReportSpread_Sheet1.RowCount += 1
                End If
                tenderIndex += 1
            End While
        End If
    End Sub
    Private Sub SetEventDiscountDetails(ByRef row As Integer, ByVal eventsDiscounts As Decimal)
        If eventsDiscounts <> 0 Then
            ReportSpread_Sheet1.SetText(row, refundList.ReportColumnDescriptionIndex, "( Event Discounts : ")
            ReportSpread_Sheet1.SetText(row, refundList.ReportColumnInonIndex, eventsDiscounts.ToString("0.00"))
            ReportSpread_Sheet1.SetText(row, refundList.ReportColumnSaltIndex, " )")
            ReportSpread_Sheet1.Cells(row, refundList.ReportColumnSaltIndex).HorizontalAlignment = Spread.CellHorizontalAlignment.Left
            row += 1
            ReportSpread_Sheet1.RowCount += 1
        End If
    End Sub
    Private Sub SetReportPriceQuantityAndTenderDetails(ByRef row As Integer, ByVal tenderAmounts As Decimal(), ByVal tenderDescriptions As String(), ByRef tenderIndex As Integer, ByRef eventsDiscounts As Decimal, ByVal detailData As DataTable, ByRef grandTotal As Decimal, ByRef linesCount As Integer, ByVal stockTypes As Collections.Generic.Dictionary(Of Char, String), ByVal reportRow As DataRow, ByRef firstRow As Boolean)
        For Each detailRow As DataRow In detailData.Rows
            grandTotal += CDec(detailRow("EXTP"))
            refundList.SetValueForReportColumnRefundCashier(ReportSpread_Sheet1, row, reportRow)
            If firstRow Then
                refundList.SetValueForReportColumnSupervisor(ReportSpread_Sheet1, row, reportRow)
                firstRow = False
            End If
            refundList.SetValueForReportColumnSkuNumber(ReportSpread_Sheet1, row, detailRow)
            refundList.SetValueForReportColumnDescription(ReportSpread_Sheet1, row, detailRow, stockTypes)
            ReportSpread_Sheet1.SetText(row, refundList.ReportColumnPriceIndex, CDec(detailRow("PRIC")).ToString("0.00"))
            ReportSpread_Sheet1.SetText(row, refundList.ReportColumnQuantityIndex, CStr(detailRow("QUAN")))
            ReportSpread_Sheet1.SetText(row, refundList.ReportColumnExtpIndex, CDec(detailRow("EXTP")).ToString("0.00"))

            ' Attempt to print a tender and amount
            SetTenderDescriptionAndAmount(row, tenderAmounts, tenderDescriptions, tenderIndex)

            linesCount += 1
            eventsDiscounts += CDec(detailRow("QBPD"))
            eventsDiscounts += CDec(detailRow("DGPD"))
            eventsDiscounts += CDec(detailRow("MBPD"))
            eventsDiscounts += CDec(detailRow("HSPD"))
            eventsDiscounts += CDec(detailRow("ESEV"))

            row += 1
            ReportSpread_Sheet1.RowCount += 1
        Next
    End Sub
    Private Sub NothingToReport(ByVal row As Integer, ByRef currentCashier As String, ByRef currentTill As String)
        If ReportSpread_Sheet1.GetText(row, 0).Length > 0 Then
            currentCashier = String.Empty
        End If
        If ReportSpread_Sheet1.GetText(row, 1).Length > 0 Then
            currentTill = String.Empty
        End If
    End Sub
    Private Sub PopulateReportData(ByRef row As Integer, ByVal currentCashier As String, ByVal currentTill As String, ByVal currentTransaction As String, ByVal tenderAmounts As Decimal(), ByVal tenderDescriptions As String(), ByVal reportData As DataTable, ByVal detailData As DataTable, ByVal grandTotal As Decimal, ByRef transactionTotal As Decimal, ByRef transactionCount As Integer, ByVal linesCount As Integer, ByVal stockTypes As Collections.Generic.Dictionary(Of Char, String))
        Dim workCashier As String
        Dim workTill As String
        Dim workTransaction As String
        Dim workOVCTransaction As String
        Dim tenderIndex As Integer
        Dim eventsDiscounts As Decimal
        For Each reportRow As DataRow In reportData.Rows
            eventsDiscounts = 0

            workCashier = CStr(reportRow("EmployeeCode"))
            workTill = CStr(reportRow("TILL"))
            workTransaction = CStr(reportRow("TRAN"))
            If Not Equals(reportRow("OVCTranNumber"), DBNull.Value) Then
                workOVCTransaction = CStr(reportRow("OVCTranNumber"))
            Else : workOVCTransaction = ""
            End If
            SetCurrentTillAndCashier(row, currentCashier, currentTill, workCashier, reportRow)
            ReportSpread_Sheet1.SetText(row, 1, workTill)
            currentTill = workTill
            ReportSpread_Sheet1.SetText(row, 2, workTransaction)
            currentTransaction = workTransaction
            ReportSpread_Sheet1.SetText(row, 3, workOVCTransaction)
            refundList.SetValueForReportColumnTime(ReportSpread_Sheet1, row, reportRow)
            SetRefundType(row, dtpReportDate.Value, currentTill, currentTransaction)

            MyTrace("Counting tenders")
            detailData = refundList.GetTenderDetails(dtpReportDate.Value, currentTill, currentTransaction)
            tenderIndex = PopulateTenderAmounts(tenderAmounts, detailData, reportRow)
            MyTrace("Retrieving lines")
            detailData = refundList.GetLineDetails(dtpReportDate.Value, currentTill, currentTransaction)

            Dim firstRow As Boolean = True

            If detailData.Rows.Count > 0 Then
                MyTrace("Printing lines")
                SetReportPriceQuantityAndTenderDetails(row, tenderAmounts, tenderDescriptions, tenderIndex, eventsDiscounts, detailData, grandTotal, linesCount, stockTypes, reportRow, firstRow)
                SetReportTenderDetails(row, tenderAmounts, tenderDescriptions, tenderIndex)
                SetEventDiscountDetails(row, eventsDiscounts)
                transactionCount += 1
                transactionTotal += CDec(reportRow("TOTL"))
            Else
                NothingToReport(row, currentCashier, currentTill)
            End If
        Next
    End Sub
    Private Sub SetReportSummary(ByRef row As Integer)
        row += 2
        ReportSpread_Sheet1.RowCount += 2

        ReportSpread_Sheet1.SetText(row, 0, "Summary: ")
    End Sub
    Private Sub SetUpReportTransactions(ByRef row As Integer, ByVal transactionCount As Integer)
        row += 1
        ReportSpread_Sheet1.RowCount += 1
        ReportSpread_Sheet1.SetText(row, 1, "No. of Transactions: ")
        ReportSpread_Sheet1.SetText(row, 5, transactionCount.ToString)
    End Sub
    Private Sub SetUpNoOfLines(ByRef row As Integer, ByVal linesCount As Integer)
        row += 1
        ReportSpread_Sheet1.RowCount += 1
        ReportSpread_Sheet1.SetText(row, 1, "No. of Lines: ")
        ReportSpread_Sheet1.SetText(row, 5, linesCount.ToString)
    End Sub
    Private Sub SetUpTotalValue(ByRef row As Integer, ByVal grandTotal As Decimal)
        row += 1
        ReportSpread_Sheet1.RowCount += 1
        ReportSpread_Sheet1.SetText(row, 1, "Total Value: ")
        ReportSpread_Sheet1.SetText(row, 5, grandTotal.ToString)
    End Sub
    Private Sub SetUpTotalValueOfTransactions(ByRef row As Integer, ByVal transactionTotal As Decimal)
        row += 1
        ReportSpread_Sheet1.RowCount += 1
        ReportSpread_Sheet1.SetText(row, 1, "Total Value of Transactions: ")
        ReportSpread_Sheet1.SetText(row, 5, transactionTotal.ToString)
    End Sub
    Private Sub SetUpDateValue(ByRef row As Integer)
        row += 2
        ReportSpread_Sheet1.RowCount += 2
        ReportSpread_Sheet1.SetText(row, 0, String.Concat("Data Date = ", dtpReportDate.Value.ToString("d")))
    End Sub
    Private Sub SetUpPrintReportDetails(ByVal row As Integer)
        row += 1
        Dim reportHeaderText As String = String.Concat( _
                    _shortAssemblyName, "  /  v", _fileVersion, _
                    New String(" "c, 10), "Refunds List", _
                    New String(" "c, 10), "Store: ", _storeIdName, _
                    New String(" "c, 10), "Date Printed: ", DateTime.Now.Date.ToString("d"), "/rPage: /p")
        ReportSpread_Sheet1.RowCount += 1
        ReportSpread_Sheet1.SetText(row, 0, _storeIdName)
        ReportSpread_Sheet1.PrintInfo = refundList.SetUpPrintReportInfo
        ReportSpread_Sheet1.PrintInfo.Header = reportHeaderText
    End Sub
    Private Sub CleanUpReport()
        If ReportSpread_Sheet1.RowCount > 0 Then
            ReportSpread_Sheet1.RemoveRows(0, ReportSpread_Sheet1.RowCount)
        End If
    End Sub
    Private Sub Print()
        Dim row As Integer = 0
        Dim currentCashier As String = String.Empty
        Dim currentTill As String = String.Empty
        Dim currentTransaction As String = String.Empty
        Dim tenderAmounts(19) As Decimal
        Dim tenderDescriptions(19) As String
        Dim reportData As DataTable = Nothing
        Dim detailData As DataTable = Nothing
        Dim grandTotal As Decimal = 0
        Dim transactionTotal As Decimal = 0
        Dim transactionCount As Integer = 0
        Dim linesCount As Integer = 0

        Dim stockTypes As Collections.Generic.Dictionary(Of Char, String) = GetStockTypes()
        CleanUpReport()
        SetUpReportData(row, tenderDescriptions, reportData, detailData)
        MyTrace("Beginning report")
        PopulateReportData(row, currentCashier, currentTill, currentTransaction, tenderAmounts, tenderDescriptions, reportData, detailData, grandTotal, transactionTotal, transactionCount, linesCount, stockTypes)
        SetReportSummary(row)
        SetUpReportTransactions(row, transactionCount)
        SetUpNoOfLines(row, linesCount)
        SetUpTotalValue(row, grandTotal)
        SetUpTotalValueOfTransactions(row, transactionTotal)
        SetUpDateValue(row)
        SetUpPrintReportDetails(row)
    End Sub

    Private Sub Finish()
        _printNotPrinted = True
        Do While _printNotPrinted
            Application.DoEvents()
            System.Threading.Thread.Sleep(1)
        Loop
    End Sub

    Private Sub ReportSpread_PrintMessageBox(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.PrintMessageBoxEventArgs) Handles ReportSpread.PrintMessageBox
        If e.BeginPrinting = False Then _printNotPrinted = False
    End Sub

    Private Sub dtpReportDate_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dtpReportDate.ValueChanged
        PrintButton.Enabled = False
    End Sub

    Private Sub MyTrace(ByVal message As String)
        Trace.WriteLine(String.Format("{0}/v{1} - {2}", _shortAssemblyName, _fileVersion, message))
    End Sub


    Private Sub RetrieveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RetrieveButton.Click

        Try
            Cursor = Cursors.WaitCursor
            ButtonPanel.Enabled = False
            CollapsibleGroupBox1.Enabled = False
            Print()
            ButtonPanel.Enabled = True
            CollapsibleGroupBox1.Enabled = True
            PrintButton.Enabled = True

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub PrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintButton.Click

        Try
            Cursor = Cursors.WaitCursor
            ButtonPanel.Enabled = False
            CollapsibleGroupBox1.Enabled = False

            ReportSpread.PrintSheet(0)
            'wait until printing finished
            Finish()
            ButtonPanel.Enabled = True
            CollapsibleGroupBox1.Enabled = True

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click
        FindForm.Close()
    End Sub

    Private Sub SetRefundType(ByVal reportColumn As Integer, ByVal reportDate As Date, ByVal tillId As String, ByVal tranNo As String)
        refundList.SetValueForReportColumnRefundType(ReportSpread_Sheet1, reportColumn, reportDate, tillId, tranNo)
    End Sub

End Class
