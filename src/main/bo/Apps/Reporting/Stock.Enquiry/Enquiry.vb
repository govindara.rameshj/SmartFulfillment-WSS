Option Explicit On
Public Class Enquiry

    Private _oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private _saleType As String = String.Empty
    Private _selectMultiple As Boolean
    Private _supplierNumber As String = String.Empty
    Private _SkuNumbers As New ArrayList

    Public ReadOnly Property SkuNumbers() As ArrayList
        Get
            Return _SkuNumbers
        End Get
    End Property

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F2 : btnSearch.PerformClick()
            Case Keys.F3 : btnDetails.PerformClick()
            Case Keys.F4 : btnAccept.PerformClick()
            Case Keys.F5 : chkSkuNumber.Checked = Not chkSkuNumber.Checked
            Case Keys.F6 : chkSkuDescription.Checked = Not chkSkuDescription.Checked
            Case Keys.F7 : chkSupplier.Checked = Not chkSupplier.Checked
            Case Keys.F8 : chkHierarchy.Checked = Not chkHierarchy.Checked
            Case Keys.F11 : btnReset.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Protected Overrides Sub DoProcessing()

        LoadSuppliers()
        LoadHierarchies()

        If _supplierNumber IsNot String.Empty Then
            chkSupplier.Checked = True
            lueSupplier.ClosePopup()
            lueSupplier.EditValue = _supplierNumber

            If TpWickes.StockFactory.RequirementActivated = True Then

                LoadStockNew()

            Else

                LoadStocks()

            End If

        Else
            chkSkuNumber.Checked = True
        End If

        xviewStock.OptionsSelection.MultiSelect = _selectMultiple

        ConfigureExcludeFilters()

    End Sub

    Public Sub SetSaleType(ByVal type As String)
        _saleType = type
    End Sub

    Public Sub SetSelectMultiple(ByVal value As Boolean)
        _selectMultiple = value
    End Sub

    Public Sub SetSupplier(ByVal supplierNumber As String)
        _supplierNumber = supplierNumber
    End Sub

    Public Sub ShowAccept()
        btnAccept.Visible = True
    End Sub

    Private Sub LoadHierarchies()

        Dim hierarchy As New BOHierarchy.vwHierarchies(_oasys3DB)
        hierarchy.LoadView(True)
        Dim dt As DataTable = hierarchy.Table

        Dim numberCat As String = String.Empty
        Dim numberGroup As String = String.Empty
        Dim numberSub As String = String.Empty
        Dim nodeCat As TreeNode = Nothing
        Dim nodeGroup As TreeNode = Nothing
        Dim nodeSub As TreeNode = Nothing

        For Each dr As DataRow In dt.Rows
            If CStr(dr(BOHierarchy.vwHierarchies.col.CategoryNumber)) <> numberCat Then
                numberCat = CStr(dr(BOHierarchy.vwHierarchies.col.CategoryNumber))
                numberGroup = String.Empty
                numberSub = String.Empty

                nodeCat = New TreeNode
                nodeCat.Tag = numberCat
                nodeCat.Text = CStr(dr(BOHierarchy.vwHierarchies.col.CategoryName))
                trvHierarchy.Nodes.Add(nodeCat)
            End If

            If CStr(dr(BOHierarchy.vwHierarchies.col.GroupNumber + 1)) <> numberGroup Then
                numberGroup = CStr(dr(BOHierarchy.vwHierarchies.col.GroupNumber + 1))
                numberSub = String.Empty

                nodeGroup = New TreeNode
                nodeGroup.Tag = numberGroup
                nodeGroup.Text = CStr(dr(BOHierarchy.vwHierarchies.col.GroupName + 1))
                nodeCat.Nodes.Add(nodeGroup)
            End If

            If CStr(dr(BOHierarchy.vwHierarchies.col.SubgroupNumber + 2)) <> numberSub Then
                numberSub = CStr(dr(BOHierarchy.vwHierarchies.col.SubgroupNumber + 2))

                nodeSub = New TreeNode
                nodeSub.Tag = numberSub
                nodeSub.Text = CStr(dr(BOHierarchy.vwHierarchies.col.SubgroupName + 2))
                nodeGroup.Nodes.Add(nodeSub)
            End If

            Dim nodeStyle As New TreeNode
            nodeStyle.Tag = dr(BOHierarchy.vwHierarchies.col.StyleNumber + 3)
            nodeStyle.Text = CStr(dr(BOHierarchy.vwHierarchies.col.StyleName + 3))
            nodeSub.Nodes.Add(nodeStyle)
        Next

    End Sub

    Private Sub LoadSuppliers()

        lueSupplier.Properties.DataSource = Supplier.GetAll
        lueSupplier.Properties.ValueMember = GetPropertyName(Function(f As Supplier) f.Number)
        lueSupplier.Properties.DisplayMember = GetPropertyName(Function(f As Supplier) f.Name)

        lueSupplier.Properties.Columns.Add(New LookUpColumnInfo(GetPropertyName(Function(f As Supplier) f.Number)))
        lueSupplier.Properties.Columns.Add(New LookUpColumnInfo(GetPropertyName(Function(f As Supplier) f.Name)))
        lueSupplier.Properties.Columns.Add(New LookUpColumnInfo(GetPropertyName(Function(f As Supplier) f.Alpha)))
        lueSupplier.Properties.AutoSearchColumnIndex = 0
        lueSupplier.Properties.SortColumnIndex = 1
        lueSupplier.Properties.PopupWidth = 400
        lueSupplier.Properties.Columns(0).Width = 80
        lueSupplier.Properties.Columns(1).Width = 200
        lueSupplier.Properties.Columns(2).Width = 120

    End Sub

    Private Sub LoadStocks()

        Try
            Cursor = Cursors.WaitCursor

            Dim sb As New StringBuilder
            Dim words() As String = Nothing
            Dim blnSkuOrder As Boolean = False
            Dim needsJoin As Boolean = False

            sb.Append("SELECT sk.SKUN, rtrim(sk.DESCR) as 'Description', sk.PRIC, sk.ONHA, sk.ONOR, rtrim(sm.NAME) as 'SupplierName', rtrim(hc.DESCR) as 'HierarchyCategory', sk.IDEL, sk.IOBS, sk.INON, sk.IRIS ")
            sb.Append("FROM STKMAS sk ")
            sb.Append("INNER JOIN SUPMAS sm ON sk.SUPP=sm.SUPN ")
            sb.Append("INNER JOIN HIECAT hc ON sk.CTGY=hc.NUMB ")
            sb.Append("WHERE ")

            If txtSkuNumber.Text.Trim.Length = 0 AndAlso txtSkuDescription.Text.Trim.Length = 0 _
            AndAlso (chkSupplier.Checked = False) AndAlso chkHierarchy.Checked = False Then
                If MessageBox.Show("Are you sure you want to display all items?", "Full Search Requested", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If
            End If

            'check for sku number
            If txtSkuNumber.Text.Length > 0 Then
                If needsJoin Then sb.Append(" AND ")
                If txtSkuNumber.Text.Trim.Length > 6 Then
                    sb.Append(" SKUN IN (SELECT SKUN FROM EANMAS WHERE EANMAS.NUMB = '" & Format(CLng(txtSkuNumber.Text.Trim), "0000000000000000") & "') ")
                Else
                    sb.Append(" SKUN = '" & txtSkuNumber.Text.Trim & "' ")
                End If
                needsJoin = True
            End If

            If chkSkuDescription.Checked AndAlso chkSkuDescription.Enabled Then
                If txtSkuDescription.Text.Trim.Length > 0 Then

                    If chkExactMatch.Checked Then
                        If needsJoin Then sb.Append(" AND ")
                        sb.Append("(")
                        sb.Append(" (sk.DESCR LIKE '% " & txtSkuDescription.Text.Trim & " %' OR ")
                        sb.Append(" sk.DESCR LIKE '" & txtSkuDescription.Text.Trim & " %') ")
                        sb.Append(" OR ")
                        sb.Append(" [EQUV] Like '% " & txtSkuDescription.Text.Trim & " %' OR ")
                        sb.Append(" [EQUV] LIKE '" & txtSkuDescription.Text.Trim & " %' ")
                        sb.Append(")")
                        needsJoin = True
                    Else
                        words = txtSkuDescription.Text.Trim.Split(" "c)
                        If needsJoin Then sb.Append(" AND ")
                        sb.Append("(")
                        needsJoin = True

                        Dim join As Boolean = False

                        For Each strWord As String In words
                            If strWord.Trim <> String.Empty Then
                                If join Then sb.Append(" AND ")
                                sb.Append(" sk.DESCR LIKE'%" & strWord.Trim & "%' ")
                                sb.Append(" OR ")
                                sb.Append(" [EQUV] Like '%" & strWord.Trim & "%' ")
                                join = True
                            End If
                        Next

                        sb.Append(")")
                    End If

                End If
            End If

            If _saleType IsNot String.Empty Then
                If needsJoin Then sb.Append(" AND ")
                sb.Append(" SALT = '" + _saleType + "' ")
                needsJoin = True
            End If

            If chkSupplier.Checked Then
                blnSkuOrder = True
                If needsJoin Then sb.Append(" AND ")
                sb.Append(String.Format(" SUPP = '{0}' ", lueSupplier.EditValue))
                needsJoin = True
            End If

            If chkHierarchy.Checked AndAlso chkHierarchy.Enabled Then
                If trvHierarchy.SelectedNode Is Nothing Then
                    MessageBox.Show("No Hierachy Node has been selected. Make a selection and try again.", "Search Criteria not Met", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If

                blnSkuOrder = True
                Dim node As TreeNode = trvHierarchy.SelectedNode
                Dim hie As String = String.Empty

                Select Case node.Level
                    Case 0 : hie = "CTGY"
                    Case 1 : hie = "GRUP"
                    Case 2 : hie = "SGRP"
                    Case 3 : hie = "STYL"
                    Case Else : Debug.Print("Shouldn't get here.")
                End Select

                If needsJoin Then sb.Append(" AND ")
                sb.Append(String.Format("{0} = '{1}' ", hie, node.Tag))
                needsJoin = True
            End If

            If chkNonStock.Visible AndAlso (chkNonStock.Checked) Then
                If needsJoin Then sb.Append(" AND ")
                sb.Append(" INON = 0")
                needsJoin = True
            End If

            If chkDeleted.Visible AndAlso (chkDeleted.Checked) Then
                If needsJoin Then sb.Append(" AND ")
                sb.Append(" (IDEL = 0 AND IOBS = 0) ")
                needsJoin = True
            Else

                If (sb.ToString().IndexOf("WHERE").Equals(sb.Length - 6)) Then
                    sb.Replace("WHERE", String.Empty)
                End If

            End If

            If blnSkuOrder Then
                sb.Append(" ORDER BY SKUN ASC")
            End If

            Dim ds As DataSet = _oasys3DB.ExecuteSql(sb.ToString)
            Dim dt As DataTable = ds.Tables(0)

            If dt.Rows.Count = 0 Then
                MessageBox.Show("No Matches Found.", "Search", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

            dt.Columns.Add("Strength", GetType(Integer))
            dt.Columns.Add("Status", GetType(String))
            dt.Columns("Status").SetOrdinal(0)

            For Each row As DataRow In dt.Rows
                row("Strength") = 0
                Select Case True
                    Case CStr(row.Item("IDEL")) = "True" : row("Status") = "Deleted"
                    Case CStr(row.Item("IOBS")) = "True" : row("Status") = "Obsolete"
                    Case CStr(row.Item("INON")) = "True" : row("Status") = "Non-Stock"
                    Case CStr(row.Item("IRIS")) = "True" : row("Status") = "Single"
                End Select

                'get match ratio
                If words IsNot Nothing Then
                    Dim itemWords() As String = row("Description").ToString.Split(" "c)
                    For Each word As String In words
                        For Each itemWord As String In itemWords
                            If word.Trim.ToLower = itemWord.Trim.ToLower Then row("Strength") = CInt(row("Strength")) + 10
                        Next
                    Next
                End If
            Next

            xgridStock.DataSource = dt

            For Each col As GridColumn In xviewStock.Columns
                Select Case col.FieldName
                    Case "Status"
                    Case "SKUN"
                        col.Caption = My.Resources.ColumnHeaders.SkuNumber
                        col.MinWidth = 60
                    Case "Description"
                        col.Caption = My.Resources.ColumnHeaders.Description
                    Case "PRIC"
                        col.Caption = My.Resources.ColumnHeaders.Price
                        col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                        col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                        col.DisplayFormat.FormatString = "n2"
                    Case "ONHA"
                        col.Caption = My.Resources.ColumnHeaders.OnHand
                        col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                        col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                        col.DisplayFormat.FormatString = "n0"
                    Case "ONOR"
                        col.Caption = My.Resources.ColumnHeaders.OnOrder
                        col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                        col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                        col.DisplayFormat.FormatString = "n0"
                    Case "SupplierName"
                        col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                        col.Caption = My.Resources.ColumnHeaders.Supplier
                    Case "HierarchyCategory"
                        col.Caption = My.Resources.ColumnHeaders.Category
                    Case Else : col.Visible = False
                End Select
            Next
            xviewStock.BestFitColumns()
            xviewStock.Focus()

            Try
                xviewStock.BeginSort()
                xviewStock.ClearSorting()
                xviewStock.Columns("Strength").SortOrder = DevExpress.Data.ColumnSortOrder.Descending
                xviewStock.Columns("HierarchyCategory").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
            Finally
                xviewStock.EndSort()
            End Try


            If xviewStock.RowCount > 0 Then
                btnDetails.Enabled = True
                btnAccept.Enabled = True
            End If

            DisplayStatus("Item Count = " & xviewStock.RowCount)

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub txt_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSkuNumber.KeyPress, txtSkuDescription.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            btnSearch.PerformClick()

        Else
            Dim name As String = CType(sender, TextBox).Name
            If name = txtSkuNumber.Name Then
                Select Case e.KeyChar
                    Case "0"c To "9"c : e.Handled = False
                    Case ChrW(Keys.Back) : e.Handled = False
                    Case ChrW(Keys.Delete) : e.Handled = False
                    Case Else : e.Handled = True
                End Select
            End If
        End If

    End Sub

    Private Sub txt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSkuDescription.TextChanged, txtSkuNumber.TextChanged

        Dim name As String = CType(sender, TextBox).Name
        Select Case name
            Case txtSkuNumber.Name
                btnSearch.Enabled = (txtSkuNumber.Text.Trim.Length > 0 AndAlso chkSkuNumber.Checked)
            Case Else
                Dim length As Integer = txtSkuDescription.Text.Trim.Length
                btnSearch.Enabled = (length > 0 AndAlso chkSkuDescription.Checked)
        End Select

    End Sub

    Private Sub lueSupplier_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lueSupplier.EditValueChanged
        If lueSupplier.EditValue IsNot Nothing Then
            btnSearch.Enabled = True
        End If
    End Sub

    Private Sub lueSupplier_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles lueSupplier.KeyPress

        Select Case True
            Case e.KeyChar = ChrW(Keys.Enter)
                btnSearch.PerformClick()
            Case IsNumeric(e.KeyChar)
                lueSupplier.Properties.AutoSearchColumnIndex = 0
                lueSupplier.Properties.DisplayMember = GetPropertyName(Function(f As Supplier) f.Number)
            Case Else
                lueSupplier.Properties.AutoSearchColumnIndex = 1
                lueSupplier.Properties.DisplayMember = GetPropertyName(Function(f As Supplier) f.Name)
        End Select

    End Sub

    Private Sub chk_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSkuNumber.CheckedChanged, chkSkuDescription.CheckedChanged, chkSupplier.CheckedChanged, chkHierarchy.CheckedChanged

        Dim checked As Boolean = CType(sender, CheckBox).Checked
        Dim name As String = CType(sender, CheckBox).Name
        chkNonStock.Visible = True
        chkDeleted.Visible = True

        Select Case name
            Case chkSkuNumber.Name
                txtSkuNumber.Clear()
                txtSkuNumber.Visible = checked
                If checked Then
                    chkSkuDescription.Checked = False
                    chkSupplier.Checked = False
                    chkHierarchy.Checked = False
                    chkNonStock.Visible = False
                    chkDeleted.Visible = False
                    txtSkuNumber.Focus()
                End If

            Case chkSkuDescription.Name
                txtSkuDescription.Clear()
                txtSkuDescription.Visible = checked
                If checked Then
                    chkSkuNumber.Checked = False
                    txtSkuDescription.Focus()
                End If

            Case chkSupplier.Name
                lueSupplier.EditValue = Nothing
                lueSupplier.Visible = checked
                If checked Then
                    chkSkuNumber.Checked = False
                    lueSupplier.ShowPopup()
                    lueSupplier.Focus()
                End If

            Case chkHierarchy.Name
                trvHierarchy.CollapseAll()
                trvHierarchy.Visible = checked
                If checked Then
                    chkSkuNumber.Checked = False
                    trvHierarchy.Focus()
                End If
        End Select

    End Sub

    Private Sub xviewStock_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles xviewStock.DoubleClick
        btnDetails.PerformClick()
    End Sub

    Private Sub xviewStock_CustomUnboundColumnData(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs) Handles xviewStock.CustomUnboundColumnData

        Dim View As GridView = CType(sender, GridView)
        Dim FilePath As String

        If e.Column.FieldName = "StockImageLocation0" AndAlso e.IsGetData Then

            View = CType(sender, GridView)
            FilePath = CStr(View.GetRowCellValue(e.RowHandle, CStr(e.Column.Tag)))

            If FilePath IsNot Nothing AndAlso System.IO.File.Exists(FilePath) Then e.Value = Image.FromFile(FilePath)

        End If

    End Sub

#Region "Buttom Events"

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Cursor = Cursors.WaitCursor
        Cursor.Show()
        Application.DoEvents()
        If TpWickes.StockFactory.RequirementActivated = True Then
            LoadStockNew()
        Else
            LoadStocks()
        End If
        Cursor = Cursors.Default
        Cursor.Show()
    End Sub

    Private Sub btnDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDetails.Click

        Dim dr As DataRow = xviewStock.GetDataRow(xviewStock.FocusedRowHandle)
        Dim enquiry As New Reporting.StockEnquiryHost(CStr(dr("SKUN")), UserId, CStr(WorkstationId), btnAccept.Visible)
        enquiry.WindowState = FormWindowState.Maximized
        enquiry.Show()

    End Sub

    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click

        _SkuNumbers.Clear()
        For Each rowHandle As Integer In xviewStock.GetSelectedRows
            _SkuNumbers.Add(xviewStock.GetDataRow(rowHandle).Item("SKUN"))
        Next
        btnExit.PerformClick()

    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click

        DisplayStatus()
        xgridStock.DataSource = Nothing
        chkDeleted.Checked = False
        chkNonStock.Checked = False
        chkExactMatch.Checked = False
        chkSkuNumber.Checked = True
        chkPIM.Checked = False

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.FindForm.Close()
    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Sub ConfigureExcludeFilters()

        Dim UI As IStockEnquiryUI

        UI = (New StockEnquiryUIFactory).GetImplementation

        chkDeleted.Checked = UI.ExcludeFilterObsoleteDeleteDefaultCheckedState
        chkNonStock.Checked = UI.ExcludeFilterNonStockDefaultCheckedState

    End Sub

    Private Sub LoadStockNew()

        Dim cStock As TpWickes.IStock
        Dim StockDT As DataTable

        Dim Node As TreeNode

        Dim Supplier As String
        Dim HierarchyCategory As String
        Dim HierarchyGroup As String
        Dim HierarchySubGroup As String
        Dim HierarchyStyleNumber As String
        Dim ExcludeNonStock As System.Nullable(Of Boolean)
        Dim ExcludeDeletedStock As System.Nullable(Of Boolean)
        Dim ExcludeObsoleteStock As System.Nullable(Of Boolean)

        Supplier = String.Empty              'default value: supplier not set
        HierarchyCategory = String.Empty     'default value: category hierarchy  not set
        HierarchyGroup = String.Empty        'default value: group hierarchy     not set
        HierarchySubGroup = String.Empty     'default value: sub group hierarchy not set
        HierarchyStyleNumber = String.Empty  'default value: style hierarchy     not set

        If txtSkuNumber.Text.Trim.Length = 0 AndAlso txtSkuDescription.Text.Trim.Length = 0 AndAlso _
                                                            (chkSupplier.Checked = False) AndAlso chkHierarchy.Checked = False Then
            If MessageBox.Show("Are you sure you want to display all items?", "Full Search Requested", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        If chkHierarchy.Checked AndAlso chkHierarchy.Enabled Then
            If trvHierarchy.SelectedNode Is Nothing Then
                MessageBox.Show("No Hierachy Node has been selected. Make a selection and try again.", "Search Criteria not Met", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If

            Node = trvHierarchy.SelectedNode

            Select Case Node.Level
                Case 0 : HierarchyCategory = Node.Tag.ToString.Trim
                Case 1 : HierarchyGroup = Node.Tag.ToString.Trim
                Case 2 : HierarchySubGroup = Node.Tag.ToString.Trim
                Case 3 : HierarchyStyleNumber = Node.Tag.ToString.Trim
            End Select

        End If

        If chkNonStock.Visible AndAlso chkNonStock.Checked = True Then ExcludeNonStock = True
        If chkDeleted.Visible AndAlso chkDeleted.Checked = True Then
            ExcludeDeletedStock = True
            ExcludeObsoleteStock = True
        End If

        If chkSupplier.Checked = True Then Supplier = lueSupplier.EditValue.ToString.Trim

        cStock = TpWickes.StockFactory.Create
        StockDT = cStock.LoadStock(txtSkuNumber.Text.Trim, txtSkuDescription.Text.Trim, _saleType.Trim, Supplier, _
                                   HierarchyCategory, HierarchyGroup, HierarchySubGroup, HierarchyStyleNumber, ExcludeNonStock, _
                                   ExcludeDeletedStock, ExcludeObsoleteStock, chkExactMatch.Checked, 0, GetAllowMisSpelling, GetElevateForHierarchy)

        If StockDT.Rows.Count = 0 Then MessageBox.Show("No Matches Found.", "Search", MessageBoxButtons.OK, MessageBoxIcon.Information)

        PopulateScreenStockInformation(StockDT, Nothing)

    End Sub

    Private Sub PopulateScreenStockInformation(ByRef StockDT As DataTable, ByVal SearchWordsBreakDown() As String)

        With StockDT
            .Columns.Add("Strength", GetType(Integer))
            .Columns.Add("Status", GetType(String))
            .Columns("Status").SetOrdinal(0)

            For Each Row As DataRow In .Rows
                Row("Strength") = 0
                Select Case True
                    Case CStr(Row.Item("IDEL")) = "True" : Row("Status") = "Deleted"
                    Case CStr(Row.Item("IOBS")) = "True" : Row("Status") = "Obsolete"
                    Case CStr(Row.Item("INON")) = "True" : Row("Status") = "Non-Stock"
                    Case CStr(Row.Item("IRIS")) = "True" : Row("Status") = "Single"
                End Select

                'get match ratio
                If SearchWordsBreakDown IsNot Nothing Then

                    Dim RecordWordsBreakDown() As String = Row("Description").ToString.Split(" "c)

                    For Each SearchWord As String In SearchWordsBreakDown

                        For Each RecordWord As String In RecordWordsBreakDown
                            If SearchWord.Trim.ToLower = RecordWord.Trim.ToLower Then Row("Strength") = CInt(Row("Strength")) + 10
                        Next

                    Next
                End If

            Next

        End With

        xgridStock.DataSource = StockDT

        Dim CustomiseImage As New RepositoryItemPictureEdit
        Dim ImageColumn As New GridColumn
        Dim TargetCol As GridColumn

        TargetCol = Nothing
        For Each Col As GridColumn In xviewStock.Columns
            If Col.Name = "StockColumn" Then TargetCol = Col
        Next
        If TargetCol IsNot Nothing Then xviewStock.Columns.Remove(TargetCol)

        xviewStock.RowHeight = -1

        If chkPIM.Checked = True Then
            CustomiseImage.SizeMode = PictureSizeMode.Squeeze
            With ImageColumn
                .ColumnEdit = CustomiseImage
                .FieldName = "StockImageLocation0"
                .UnboundType = DevExpress.Data.UnboundColumnType.Object
                .Name = "StockColumn"
            End With

            xviewStock.Columns.Add(ImageColumn)
            xviewStock.RowHeight = 150
        End If

        For Each col As GridColumn In xviewStock.Columns
            Select Case col.FieldName
                Case "StockImageLocation"
                    col.Visible = False

                Case "StockImageLocation0"
                    With col
                        .Tag = "StockImageLocation"
                        .Caption = "PIM Image"
                        .Visible = True
                        .VisibleIndex = 0
                        .MinWidth = 150
                        .MaxWidth = 150
                    End With

                Case "Status"
                Case "SKUN"
                    col.Caption = My.Resources.ColumnHeaders.SkuNumber
                    col.MinWidth = 60
                Case "Description"
                    col.Caption = My.Resources.ColumnHeaders.Description
                Case "PRIC"
                    col.Caption = My.Resources.ColumnHeaders.Price
                    col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "n2"
                Case "ONHA"
                    col.Caption = My.Resources.ColumnHeaders.OnHand
                    col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "n0"
                Case "ONOR"
                    col.Caption = My.Resources.ColumnHeaders.OnOrder
                    col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    col.DisplayFormat.FormatString = "n0"
                Case "SupplierName"
                    col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                    col.Caption = My.Resources.ColumnHeaders.Supplier
                Case "HierarchyCategory"
                    col.Caption = My.Resources.ColumnHeaders.Category
                Case Else : col.Visible = False
            End Select
        Next
        xviewStock.BestFitColumns()
        xviewStock.Focus()






        Dim EnquirySort As IStockEnquirySortUI

        EnquirySort = (New StockEnquirySortUIFactory).GetImplementation

        EnquirySort.ConfigureSort(xviewStock)


        'Try
        '    xviewStock.BeginSort()
        '    xviewStock.ClearSorting()
        '    xviewStock.Columns("Strength").SortOrder = DevExpress.Data.ColumnSortOrder.Descending
        '    xviewStock.Columns("HierarchyCategory").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        'Finally
        '    xviewStock.EndSort()
        'End Try






        If xviewStock.RowCount > 0 Then
            btnDetails.Enabled = True
            btnAccept.Enabled = True
        End If

        DisplayStatus("Item Count = " & xviewStock.RowCount)

    End Sub

    Private Function GetAllowMisSpelling() As System.Nullable(Of Boolean)
        Dim DigDeeperImplementation As IDigDeeper = (New DigDeeperFactory).GetImplementation

        GetAllowMisSpelling = New System.Nullable(Of Boolean)
        If DigDeeperImplementation IsNot Nothing Then
            GetAllowMisSpelling = DigDeeperImplementation.AllowMisSpelling()
        End If
    End Function

    Private Function GetElevateForHierarchy() As System.Nullable(Of Boolean)
        Dim DigDeeperImplementation As IDigDeeper = (New DigDeeperFactory).GetImplementation

        GetElevateForHierarchy = New System.Nullable(Of Boolean)
        If DigDeeperImplementation IsNot Nothing Then
            GetElevateForHierarchy = DigDeeperImplementation.ElevateForHierarchy()
        End If
    End Function
#End Region

End Class