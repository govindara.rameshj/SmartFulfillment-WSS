<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Enquiry
    Inherits Cts.Oasys.WinForm.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Enquiry))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkPIM = New System.Windows.Forms.CheckBox()
        Me.lueSupplier = New DevExpress.XtraEditors.LookUpEdit()
        Me.chkSkuNumber = New System.Windows.Forms.CheckBox()
        Me.txtSkuDescription = New System.Windows.Forms.TextBox()
        Me.chkExactMatch = New System.Windows.Forms.CheckBox()
        Me.chkSkuDescription = New System.Windows.Forms.CheckBox()
        Me.chkDeleted = New System.Windows.Forms.CheckBox()
        Me.chkNonStock = New System.Windows.Forms.CheckBox()
        Me.txtSkuNumber = New System.Windows.Forms.TextBox()
        Me.chkHierarchy = New System.Windows.Forms.CheckBox()
        Me.chkSupplier = New System.Windows.Forms.CheckBox()
        Me.trvHierarchy = New System.Windows.Forms.TreeView()
        Me.btnAccept = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.xgridStock = New DevExpress.XtraGrid.GridControl()
        Me.xviewStock = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.SupplierName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Address = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.AlphaKey = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnDetails = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.lueSupplier.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xgridStock, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xviewStock, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.chkPIM)
        Me.GroupBox1.Controls.Add(Me.lueSupplier)
        Me.GroupBox1.Controls.Add(Me.chkSkuNumber)
        Me.GroupBox1.Controls.Add(Me.txtSkuDescription)
        Me.GroupBox1.Controls.Add(Me.chkExactMatch)
        Me.GroupBox1.Controls.Add(Me.chkSkuDescription)
        Me.GroupBox1.Controls.Add(Me.chkDeleted)
        Me.GroupBox1.Controls.Add(Me.chkNonStock)
        Me.GroupBox1.Controls.Add(Me.txtSkuNumber)
        Me.GroupBox1.Controls.Add(Me.chkHierarchy)
        Me.GroupBox1.Controls.Add(Me.chkSupplier)
        Me.GroupBox1.Controls.Add(Me.trvHierarchy)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(288, 543)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Enter Selection Criteria"
        '
        'chkPIM
        '
        Me.chkPIM.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkPIM.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkPIM.Location = New System.Drawing.Point(86, 468)
        Me.chkPIM.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.chkPIM.Name = "chkPIM"
        Me.chkPIM.Size = New System.Drawing.Size(196, 21)
        Me.chkPIM.TabIndex = 36
        Me.chkPIM.Text = "Show &PIM Image (Alt+P)"
        Me.chkPIM.UseVisualStyleBackColor = True
        '
        'lueSupplier
        '
        Me.lueSupplier.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lueSupplier.Location = New System.Drawing.Point(156, 64)
        Me.lueSupplier.Name = "lueSupplier"
        Me.lueSupplier.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lueSupplier.Properties.ImmediatePopup = True
        Me.lueSupplier.Properties.NullText = ""
        Me.lueSupplier.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.lueSupplier.Size = New System.Drawing.Size(126, 20)
        Me.lueSupplier.TabIndex = 35
        Me.lueSupplier.Visible = False
        '
        'chkSkuNumber
        '
        Me.chkSkuNumber.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkSkuNumber.Location = New System.Drawing.Point(6, 17)
        Me.chkSkuNumber.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.chkSkuNumber.Name = "chkSkuNumber"
        Me.chkSkuNumber.Size = New System.Drawing.Size(144, 21)
        Me.chkSkuNumber.TabIndex = 21
        Me.chkSkuNumber.Text = "SKU / EAN Number (F5)"
        Me.chkSkuNumber.UseVisualStyleBackColor = True
        '
        'txtSkuDescription
        '
        Me.txtSkuDescription.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSkuDescription.Location = New System.Drawing.Point(156, 40)
        Me.txtSkuDescription.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.txtSkuDescription.Name = "txtSkuDescription"
        Me.txtSkuDescription.Size = New System.Drawing.Size(126, 20)
        Me.txtSkuDescription.TabIndex = 1
        Me.txtSkuDescription.Visible = False
        '
        'chkExactMatch
        '
        Me.chkExactMatch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkExactMatch.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkExactMatch.Location = New System.Drawing.Point(86, 444)
        Me.chkExactMatch.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.chkExactMatch.Name = "chkExactMatch"
        Me.chkExactMatch.Size = New System.Drawing.Size(196, 21)
        Me.chkExactMatch.TabIndex = 18
        Me.chkExactMatch.Text = "E&xact Match (Alt+X)"
        Me.chkExactMatch.UseVisualStyleBackColor = True
        '
        'chkSkuDescription
        '
        Me.chkSkuDescription.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkSkuDescription.Location = New System.Drawing.Point(6, 40)
        Me.chkSkuDescription.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.chkSkuDescription.Name = "chkSkuDescription"
        Me.chkSkuDescription.Size = New System.Drawing.Size(144, 21)
        Me.chkSkuDescription.TabIndex = 1
        Me.chkSkuDescription.Text = "Description (F6)"
        Me.chkSkuDescription.UseVisualStyleBackColor = True
        '
        'chkDeleted
        '
        Me.chkDeleted.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkDeleted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkDeleted.Checked = True
        Me.chkDeleted.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkDeleted.Location = New System.Drawing.Point(86, 492)
        Me.chkDeleted.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.chkDeleted.Name = "chkDeleted"
        Me.chkDeleted.Size = New System.Drawing.Size(196, 21)
        Me.chkDeleted.TabIndex = 8
        Me.chkDeleted.Text = "Exclude &Obsolete/Deleted (Alt+O)"
        Me.chkDeleted.UseVisualStyleBackColor = True
        '
        'chkNonStock
        '
        Me.chkNonStock.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkNonStock.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkNonStock.Checked = True
        Me.chkNonStock.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkNonStock.Location = New System.Drawing.Point(86, 516)
        Me.chkNonStock.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.chkNonStock.Name = "chkNonStock"
        Me.chkNonStock.Size = New System.Drawing.Size(196, 21)
        Me.chkNonStock.TabIndex = 7
        Me.chkNonStock.Text = "Exclude &Non-Stock (Alt+N)"
        Me.chkNonStock.UseVisualStyleBackColor = True
        '
        'txtSkuNumber
        '
        Me.txtSkuNumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSkuNumber.Location = New System.Drawing.Point(156, 16)
        Me.txtSkuNumber.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.txtSkuNumber.MaxLength = 16
        Me.txtSkuNumber.Name = "txtSkuNumber"
        Me.txtSkuNumber.Size = New System.Drawing.Size(126, 20)
        Me.txtSkuNumber.TabIndex = 0
        Me.txtSkuNumber.Visible = False
        '
        'chkHierarchy
        '
        Me.chkHierarchy.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkHierarchy.Location = New System.Drawing.Point(6, 88)
        Me.chkHierarchy.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.chkHierarchy.Name = "chkHierarchy"
        Me.chkHierarchy.Size = New System.Drawing.Size(144, 20)
        Me.chkHierarchy.TabIndex = 9
        Me.chkHierarchy.Text = "Hierarchy (F8)"
        Me.chkHierarchy.UseVisualStyleBackColor = True
        '
        'chkSupplier
        '
        Me.chkSupplier.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkSupplier.Location = New System.Drawing.Point(6, 64)
        Me.chkSupplier.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.chkSupplier.Name = "chkSupplier"
        Me.chkSupplier.Size = New System.Drawing.Size(144, 21)
        Me.chkSupplier.TabIndex = 5
        Me.chkSupplier.Text = "Supplier (F7)"
        Me.chkSupplier.UseVisualStyleBackColor = True
        '
        'trvHierarchy
        '
        Me.trvHierarchy.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.trvHierarchy.HideSelection = False
        Me.trvHierarchy.Location = New System.Drawing.Point(6, 111)
        Me.trvHierarchy.Name = "trvHierarchy"
        Me.trvHierarchy.Size = New System.Drawing.Size(276, 276)
        Me.trvHierarchy.TabIndex = 10
        Me.trvHierarchy.Visible = False
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Enabled = False
        Me.btnAccept.Location = New System.Drawing.Point(170, 555)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(76, 39)
        Me.btnAccept.TabIndex = 4
        Me.btnAccept.TabStop = False
        Me.btnAccept.Text = "F4 Accept"
        Me.btnAccept.UseVisualStyleBackColor = True
        Me.btnAccept.Visible = False
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(968, 555)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 14
        Me.btnExit.TabStop = False
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.Location = New System.Drawing.Point(889, 555)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(76, 39)
        Me.btnReset.TabIndex = 13
        Me.btnReset.Text = "F11 Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.Location = New System.Drawing.Point(6, 555)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(76, 39)
        Me.btnSearch.TabIndex = 11
        Me.btnSearch.Text = "F2 Search"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'xgridStock
        '
        Me.xgridStock.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.xgridStock.Location = New System.Drawing.Point(300, 6)
        Me.xgridStock.MainView = Me.xviewStock
        Me.xgridStock.Name = "xgridStock"
        Me.xgridStock.Size = New System.Drawing.Size(744, 543)
        Me.xgridStock.TabIndex = 15
        Me.xgridStock.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.xviewStock})
        '
        'xviewStock
        '
        Me.xviewStock.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xviewStock.Appearance.HeaderPanel.Options.UseFont = True
        Me.xviewStock.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.xviewStock.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.xviewStock.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.xviewStock.ColumnPanelRowHeight = 35
        Me.xviewStock.GridControl = Me.xgridStock
        Me.xviewStock.Name = "xviewStock"
        Me.xviewStock.OptionsBehavior.Editable = False
        Me.xviewStock.OptionsSelection.EnableAppearanceFocusedCell = False
        '
        'SupplierName
        '
        Me.SupplierName.Text = "Name"
        Me.SupplierName.Width = 145
        '
        'Address
        '
        Me.Address.Text = "Address"
        Me.Address.Width = 145
        '
        'AlphaKey
        '
        Me.AlphaKey.Text = "Alpha Key"
        Me.AlphaKey.Width = 155
        '
        'btnDetails
        '
        Me.btnDetails.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnDetails.Enabled = False
        Me.btnDetails.Location = New System.Drawing.Point(88, 555)
        Me.btnDetails.Name = "btnDetails"
        Me.btnDetails.Size = New System.Drawing.Size(76, 39)
        Me.btnDetails.TabIndex = 23
        Me.btnDetails.TabStop = False
        Me.btnDetails.Text = "F3 Details"
        Me.btnDetails.UseVisualStyleBackColor = True
        '
        'Enquiry
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.xgridStock)
        Me.Controls.Add(Me.btnDetails)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnAccept)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Enquiry"
        Me.Padding = New System.Windows.Forms.Padding(3)
        Me.Size = New System.Drawing.Size(1050, 600)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.lueSupplier.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xgridStock, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xviewStock, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnAccept As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents trvHierarchy As System.Windows.Forms.TreeView
    Friend WithEvents chkHierarchy As System.Windows.Forms.CheckBox
    Friend WithEvents chkSupplier As System.Windows.Forms.CheckBox
    Friend WithEvents txtSkuNumber As System.Windows.Forms.TextBox
    Friend WithEvents chkDeleted As System.Windows.Forms.CheckBox
    Friend WithEvents chkNonStock As System.Windows.Forms.CheckBox
    Friend WithEvents chkSkuDescription As System.Windows.Forms.CheckBox
    Friend WithEvents chkExactMatch As System.Windows.Forms.CheckBox
    'Friend WithEvents SupplierNumber As System.Windows.Forms.ColumnHeader
    Friend WithEvents SupplierName As System.Windows.Forms.ColumnHeader
    Friend WithEvents Address As System.Windows.Forms.ColumnHeader
    Friend WithEvents AlphaKey As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtSkuDescription As System.Windows.Forms.TextBox
    Friend WithEvents btnDetails As System.Windows.Forms.Button
    Friend WithEvents chkSkuNumber As System.Windows.Forms.CheckBox
    Friend WithEvents lueSupplier As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents xgridStock As DevExpress.XtraGrid.GridControl
    Friend WithEvents xviewStock As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents chkPIM As System.Windows.Forms.CheckBox
End Class
