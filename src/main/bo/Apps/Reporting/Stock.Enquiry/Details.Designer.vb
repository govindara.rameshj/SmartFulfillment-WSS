﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Details
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblSkuLabel = New DevExpress.XtraEditors.LabelControl
        Me.lblSku = New DevExpress.XtraEditors.LabelControl
        Me.lblEanLabel = New DevExpress.XtraEditors.LabelControl
        Me.lblSupplierRef = New DevExpress.XtraEditors.LabelControl
        Me.lblSupplierRefLabel = New DevExpress.XtraEditors.LabelControl
        Me.pnlReports = New DevExpress.XtraEditors.PanelControl
        Me.cmbEan = New System.Windows.Forms.ComboBox
        Me.btnExit = New System.Windows.Forms.Button
        CType(Me.pnlReports, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblSkuLabel
        '
        Me.lblSkuLabel.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblSkuLabel.Appearance.Options.UseFont = True
        Me.lblSkuLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblSkuLabel.Location = New System.Drawing.Point(12, 12)
        Me.lblSkuLabel.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
        Me.lblSkuLabel.Name = "lblSkuLabel"
        Me.lblSkuLabel.Size = New System.Drawing.Size(53, 20)
        Me.lblSkuLabel.TabIndex = 0
        Me.lblSkuLabel.Text = "SKU"
        '
        'lblSku
        '
        Me.lblSku.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblSku.Location = New System.Drawing.Point(71, 12)
        Me.lblSku.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
        Me.lblSku.Name = "lblSku"
        Me.lblSku.Size = New System.Drawing.Size(305, 20)
        Me.lblSku.TabIndex = 1
        Me.lblSku.Text = "sku number and description"
        '
        'lblEanLabel
        '
        Me.lblEanLabel.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblEanLabel.Appearance.Options.UseFont = True
        Me.lblEanLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblEanLabel.Location = New System.Drawing.Point(382, 12)
        Me.lblEanLabel.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
        Me.lblEanLabel.Name = "lblEanLabel"
        Me.lblEanLabel.Size = New System.Drawing.Size(52, 20)
        Me.lblEanLabel.TabIndex = 2
        Me.lblEanLabel.Text = "EAN"
        '
        'lblSupplierRef
        '
        Me.lblSupplierRef.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblSupplierRef.Location = New System.Drawing.Point(777, 12)
        Me.lblSupplierRef.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
        Me.lblSupplierRef.Name = "lblSupplierRef"
        Me.lblSupplierRef.Size = New System.Drawing.Size(95, 20)
        Me.lblSupplierRef.TabIndex = 5
        Me.lblSupplierRef.Text = "supplier ref"
        '
        'lblSupplierRefLabel
        '
        Me.lblSupplierRefLabel.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblSupplierRefLabel.Appearance.Options.UseFont = True
        Me.lblSupplierRefLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblSupplierRefLabel.Location = New System.Drawing.Point(692, 12)
        Me.lblSupplierRefLabel.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
        Me.lblSupplierRefLabel.Name = "lblSupplierRefLabel"
        Me.lblSupplierRefLabel.Size = New System.Drawing.Size(79, 20)
        Me.lblSupplierRefLabel.TabIndex = 4
        Me.lblSupplierRefLabel.Text = "Supplier Ref"
        '
        'pnlReports
        '
        Me.pnlReports.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlReports.FireScrollEventOnMouseWheel = True
        Me.pnlReports.Location = New System.Drawing.Point(12, 39)
        Me.pnlReports.Name = "pnlReports"
        Me.pnlReports.Size = New System.Drawing.Size(1060, 542)
        Me.pnlReports.TabIndex = 6
        '
        'cmbEan
        '
        Me.cmbEan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbEan.FormattingEnabled = True
        Me.cmbEan.Location = New System.Drawing.Point(441, 12)
        Me.cmbEan.Name = "cmbEan"
        Me.cmbEan.Size = New System.Drawing.Size(148, 21)
        Me.cmbEan.TabIndex = 7
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(996, 587)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 15
        Me.btnExit.TabStop = False
        Me.btnExit.Text = "F10 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'Details
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1084, 638)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.cmbEan)
        Me.Controls.Add(Me.pnlReports)
        Me.Controls.Add(Me.lblSupplierRef)
        Me.Controls.Add(Me.lblSupplierRefLabel)
        Me.Controls.Add(Me.lblEanLabel)
        Me.Controls.Add(Me.lblSku)
        Me.Controls.Add(Me.lblSkuLabel)
        Me.KeyPreview = True
        Me.Name = "Details"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Item Enquiry"
        CType(Me.pnlReports, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblSkuLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblSku As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblEanLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblSupplierRef As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblSupplierRefLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents pnlReports As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmbEan As System.Windows.Forms.ComboBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
End Class
