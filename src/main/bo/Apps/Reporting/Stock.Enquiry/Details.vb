﻿Imports Reporting
Imports Oasys.Windows.Form

Public Class Details
    Private _grid(2, 3) As Boolean

    Public Sub New(ByVal skuNumber As String)
        InitializeComponent()

        'get stock from table and set screen labels
        Dim stock As Core.Stock = Core.Stock.GetStock(skuNumber)
        lblSku.Text = stock.SkuNumber & Space(1) & stock.Description
        cmbEan.DataSource = stock.StockEans
        cmbEan.DisplayMember = GetPropertyName(Function(f As Stock.Core.StockEan) f.Number)
        cmbEan.ValueMember = GetPropertyName(Function(f As Stock.Core.StockEan) f.Number)
        lblSupplierRef.Text = stock.ProductCode

        'get report ids to show from parameters (id=5000)
        Dim repIds() As String = Oasys.System.Parameter.GetString(5000).Split(",")
        For Each repId As Integer In repIds
            Dim repControl As New ReportControl(repId, New Object() {skuNumber})
            repControl.LoadData()

            If Not (repControl.HideWhenNoData AndAlso repControl.HasData = False) Then
                pnlReports.Controls.Add(repControl)
            End If
        Next

        ' TileReports()

    End Sub

    Private Sub Details_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F10 Then
            e.Handled = True
            btnExit.PerformClick()
        End If
    End Sub

    Private Sub pnlReports_ControlAdded(ByVal sender As Object, ByVal e As System.Windows.Forms.ControlEventArgs) Handles pnlReports.ControlAdded

        'get control added
        Dim rep As ReportControl = e.Control
        Dim rowMax As Integer = _grid.GetUpperBound(0) + 1
        Dim colMax As Integer = _grid.GetUpperBound(1) + 1
        Dim sectionWidth As Integer = pnlReports.Width / colMax
        Dim sectionHeight As Integer = pnlReports.Height / rowMax


        'get next grid section to fill and check that report fits in it
        For rowIndex As Integer = 0 To rowMax - 1
            For colIndex As Integer = 0 To colMax - 1
                'check that this grid section is empty
                If _grid(rowIndex, colIndex) = True Then Continue For

                'check how many grid sections is long and tall
                Dim gridsWidth As Integer = Math.Ceiling(rep.MinimumSize.Width / sectionWidth)
                Dim gridsHeight As Integer = Math.Ceiling(rep.MinimumSize.Height / sectionHeight)

                'check that report does not go over max number grid columns
                If colIndex + gridsWidth > colMax Then Continue For

                'check that there is nothing underneath proposed grid sections
                Dim okToAdd As Boolean = True
                For rowCheck As Integer = colIndex To colIndex + (gridsWidth - 1)
                    If _grid(rowIndex, colIndex) = True Then okToAdd = False
                Next
                If okToAdd = False Then Continue For

                'resize report control to required size and re-position
                rep.Size = New Size(sectionWidth * gridsWidth, sectionHeight * gridsHeight)
                rep.Location = New Point(sectionWidth * colIndex, sectionHeight * rowIndex)

                'set grid flags to true
                For colcheck As Integer = colIndex To colIndex + (gridsWidth - 1)
                    For rowCheck As Integer = rowIndex To rowIndex + (gridsHeight - 1)
                        _grid(rowCheck, colcheck) = True
                    Next
                Next

                'exit sub
                Exit Sub
            Next
        Next

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

End Class