﻿Public Class DigDeeperDependantOnParameter
    Implements IDigDeeper

    Private _AllowMisSpelling As Boolean
    Private _ElevateForHierarchy As Boolean

    Public Sub New(ByVal AllowMisSpelling As Boolean, ByVal ElevateForHierarchy As Boolean)

        _AllowMisSpelling = AllowMisSpelling
        _ElevateForHierarchy = ElevateForHierarchy
    End Sub

    Public Function AllowMisSpelling() As Boolean? Implements IDigDeeper.AllowMisSpelling

        AllowMisSpelling = New System.Nullable(Of Boolean)
        AllowMisSpelling = _AllowMisSpelling
    End Function

    Public Function ElevateForHierarchy() As Boolean? Implements IDigDeeper.ElevateForHierarchy

        ElevateForHierarchy = New System.Nullable(Of Boolean)
        ElevateForHierarchy = _ElevateForHierarchy
    End Function
End Class
