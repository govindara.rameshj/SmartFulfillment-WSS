﻿Namespace TpWickes

    Public Class StockDatabase
        Implements IStockDatabase

        Public Function StockItemEnquiry(ByVal ProductCode As String, ByVal EanNumber As String, ByVal ProductDescription As String, _
                                         ByVal SaleType As String, ByVal Supplier As String, ByVal HierarchyCategory As String, _
                                         ByVal HierarchyGroup As String, ByVal HierarchySubGroup As String, ByVal HierarchyStyleNumber As String, _
                                         ByVal ExcludeNonStock As System.Nullable(Of Boolean), _
                                         ByVal ExcludeDeletedStock As System.Nullable(Of Boolean), _
                                         ByVal ExcludeObsoleteStock As System.Nullable(Of Boolean), ByVal ExactMatch As Boolean, _
                                         ByVal FuzzyMethod As Integer, ByVal MaxResults As Integer, _
                                         ByVal AllowMisSpelling As System.Nullable(Of Boolean), _
                                         ByVal ElevateForHierarchy As System.Nullable(Of Boolean)) As System.Data.DataTable Implements IStockDatabase.StockItemEnquiry

            Using con As New Connection
                Using com As New Command(con)
                    com.StoredProcedureName = "StockItemEnquiry"

                    If ProductCode.Length > 0 Then com.AddParameter("@ProductCode", ProductCode, SqlDbType.NVarChar, 6)
                    If EanNumber.Length > 0 Then com.AddParameter("@EanNumber", EanNumber, SqlDbType.NVarChar, 16)
                    If ProductDescription.Length > 0 Then com.AddParameter("@ProductDescription", ProductDescription, SqlDbType.NVarChar, 40)
                    If SaleType.Length > 0 Then com.AddParameter("@SaleType", SaleType, SqlDbType.NChar, 1)
                    If Supplier.Length > 0 Then com.AddParameter("@Supplier", Supplier, SqlDbType.NVarChar, 5)

                    If HierarchyCategory.Length > 0 Then com.AddParameter("@HierarchyCategory", HierarchyCategory, SqlDbType.NVarChar, 6)
                    If HierarchyGroup.Length > 0 Then com.AddParameter("@HierarchyGroup", HierarchyGroup, SqlDbType.NVarChar, 6)
                    If HierarchySubGroup.Length > 0 Then com.AddParameter("@HierarchySubGroup", HierarchySubGroup, SqlDbType.NVarChar, 6)
                    If HierarchyStyleNumber.Length > 0 Then com.AddParameter("@HierarchyStyleNumber", HierarchyStyleNumber, SqlDbType.NVarChar, 6)

                    If ExcludeNonStock.HasValue = True Then com.AddParameter("@ExcludeNonStock", ExcludeNonStock.Value, SqlDbType.Bit)
                    If ExcludeDeletedStock.HasValue = True Then com.AddParameter("@ExcludeDeletedStock", ExcludeDeletedStock.Value, SqlDbType.Bit)
                    If ExcludeObsoleteStock.HasValue = True Then com.AddParameter("@ExcludeObsoleteStock", ExcludeObsoleteStock.Value, SqlDbType.Bit)

                    com.AddParameter("@ExactMatch", ExactMatch, SqlDbType.Bit)
                    com.AddParameter("@FuzzyMethod", FuzzyMethod, SqlDbType.Int)
                    com.AddParameter("@MaxResults", MaxResults, SqlDbType.Int)

                    If AllowMisSpelling.HasValue Then
                        com.AddParameter("@AllowMisSpelling", AllowMisSpelling.Value, SqlDbType.Bit)
                    End If
                    If ElevateForHierarchy.HasValue Then
                        com.AddParameter("@ElevateForHierarchy", ElevateForHierarchy.Value, SqlDbType.Bit)
                    End If

                    StockItemEnquiry = com.ExecuteDataTable

                End Using
            End Using

        End Function

    End Class

End Namespace