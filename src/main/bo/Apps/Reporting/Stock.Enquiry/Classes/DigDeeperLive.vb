﻿Public Class DigDeeperLive
    Implements IDigDeeper

    Public Function AllowMisSpelling() As Boolean? Implements IDigDeeper.AllowMisSpelling

        Return New System.Nullable(Of Boolean)
    End Function

    Public Function ElevateForHierarchy() As Boolean? Implements IDigDeeper.ElevateForHierarchy

        Return New System.Nullable(Of Boolean)
    End Function
End Class
