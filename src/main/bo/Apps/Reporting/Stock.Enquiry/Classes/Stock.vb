﻿Namespace TpWickes

    Public Class StockNew
        Implements IStock

#Region "Properties"

        Private _DB As IStockDatabase
        Private _MaximumRecordsReturned As Integer

        Public Property Database() As IStockDatabase Implements IStock.Database
            Get
                Return _DB
            End Get
            Set(ByVal value As IStockDatabase)
                _DB = value
            End Set
        End Property

        Public Property MaximumRecordsReturned() As Integer Implements IStock.MaximumRecordsReturned
            Get
                Return _MaximumRecordsReturned
            End Get
            Set(ByVal value As Integer)
                _MaximumRecordsReturned = value
            End Set
        End Property

#End Region

#Region "Public Procedures And Functions"

        Public Sub New(ByVal MaxRecords As Integer)

            Database = StockDatabaseFactory.Create()
            MaximumRecordsReturned = MaxRecords

        End Sub

        Public Function LoadStock(ByVal ProductOrEANCode As String, ByVal ProductDescription As String, ByVal SaleType As String, _
                                  ByVal Supplier As String, ByVal HierarchyCategory As String, ByVal HierarchyGroup As String, _
                                  ByVal HierarchySubGroup As String, ByVal HierarchyStyleNumber As String, _
                                  ByVal ExcludeNonStock As System.Nullable(Of Boolean), ByVal ExcludeDeletedStock As System.Nullable(Of Boolean), _
                                  ByVal ExcludeObsoleteStock As System.Nullable(Of Boolean), _
                                  ByVal ExactMatch As Boolean, ByVal FuzzyMethod As Integer, _
                                  ByVal AllowMisSpelling As System.Nullable(Of Boolean), _
                                  ByVal ElevateForHierarchy As System.Nullable(Of Boolean)) As System.Data.DataTable Implements IStock.LoadStock

            Dim ProductCode As String = String.Empty
            Dim EANCode As String = String.Empty

            If ProductOrEANCode.Trim.Length > 0 Then
                If ProductOrEANCode.Trim.Length > 6 Then
                    EANCode = ProductOrEANCode.Trim.PadLeft(16, CType("0", Char))
                Else
                    ProductCode = ProductOrEANCode.Trim
                End If
            End If

            LoadStock = Database.StockItemEnquiry(ProductCode, EANCode, ProductDescription, SaleType, Supplier, HierarchyCategory, HierarchyGroup, _
                                                  HierarchySubGroup, HierarchyStyleNumber, ExcludeNonStock, ExcludeDeletedStock, ExcludeObsoleteStock, _
                                                  ExactMatch, FuzzyMethod, Me.MaximumRecordsReturned, AllowMisSpelling, ElevateForHierarchy)

        End Function

#End Region

#Region "Private Procedures And Functions"

#End Region

    End Class

End Namespace