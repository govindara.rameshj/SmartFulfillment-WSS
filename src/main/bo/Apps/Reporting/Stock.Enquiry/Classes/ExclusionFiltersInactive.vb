﻿Public Class ExclusionFiltersInactive
    Implements IStockEnquiryUI

    Public Function ExcludeFilterNonStockDefaultCheckedState() As Boolean Implements IStockEnquiryUI.ExcludeFilterNonStockDefaultCheckedState

        Return False

    End Function

    Public Function ExcludeFilterObsoleteDeleteDefaultCheckedState() As Boolean Implements IStockEnquiryUI.ExcludeFilterObsoleteDeleteDefaultCheckedState

        Return False

    End Function

End Class