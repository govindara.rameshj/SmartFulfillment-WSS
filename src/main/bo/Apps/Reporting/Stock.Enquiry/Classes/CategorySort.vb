﻿Public Class CategorySort
    Implements IStockEnquirySortUI

    Friend _View As GridView

    Public Sub ConfigureSort(ByRef View As GridView) Implements IStockEnquirySortUI.ConfigureSort

        SetView(View)

        AddSort()

    End Sub

#Region "Private Procedures & Functions"

    Friend Sub SetView(ByRef View As GridView)

        _View = View

    End Sub

    Friend Sub AddSort()

        Try
            _View.BeginSort()
            _View.ClearSorting()
            _View.Columns("Strength").SortOrder = DevExpress.Data.ColumnSortOrder.Descending
            _View.Columns("HierarchyCategory").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Finally

            _View.EndSort()

        End Try

    End Sub

#End Region

End Class