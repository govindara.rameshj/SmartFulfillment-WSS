﻿Public Interface IDigDeeper

    Function AllowMisSpelling() As System.Nullable(Of Boolean)
    Function ElevateForHierarchy() As System.Nullable(Of Boolean)
End Interface
