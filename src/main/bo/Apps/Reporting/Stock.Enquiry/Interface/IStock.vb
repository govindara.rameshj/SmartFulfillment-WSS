﻿Namespace TpWickes

    Public Interface IStock

        Property Database() As IStockDatabase
        Property MaximumRecordsReturned() As Integer

        Function LoadStock(ByVal ProductOrEANCode As String, ByVal ProductDescription As String, ByVal SaleType As String, ByVal Supplier As String, _
                          ByVal HierarchyCategory As String, ByVal HierarchyGroup As String, ByVal HierarchySubGroup As String, _
                          ByVal HierarchyStyleNumber As String, ByVal ExcludeNonStock As System.Nullable(Of Boolean), _
                          ByVal ExcludeDeletedStock As System.Nullable(Of Boolean), ByVal ExcludeObsoleteStock As System.Nullable(Of Boolean), _
                          ByVal ExactMatch As Boolean, ByVal FuzzyMethod As Integer, _
                          ByVal AllowMisSpelling As System.Nullable(Of Boolean), _
                          ByVal ElevateForHierarchy As System.Nullable(Of Boolean)) As System.Data.DataTable

    End Interface

End Namespace
