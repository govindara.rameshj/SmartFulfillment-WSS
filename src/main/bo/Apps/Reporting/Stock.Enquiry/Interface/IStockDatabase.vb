﻿Namespace TpWickes

    Public Interface IStockDatabase

        Function StockItemEnquiry(ByVal ProductCode As String, ByVal EanNumber As String, ByVal ProductDescription As String, ByVal SaleType As String, _
                                  ByVal Supplier As String, ByVal HierarchyCategory As String, ByVal HierarchyGroup As String, _
                                  ByVal HierarchySubGroup As String, ByVal HierarchyStyleNumber As String, _
                                  ByVal ExcludeNonStock As System.Nullable(Of Boolean), ByVal ExcludeDeletedStock As System.Nullable(Of Boolean), _
                                  ByVal ExcludeObsoleteStock As System.Nullable(Of Boolean), ByVal ExactMatch As Boolean, _
                                  ByVal FuzzyMethod As Integer, ByVal MaxResults As Integer, _
                                  ByVal AllowMisSpelling As System.Nullable(Of Boolean), _
                                  ByVal ElevateForHierarchy As System.Nullable(Of Boolean)) As System.Data.DataTable

    End Interface

End Namespace