﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.1
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System

Namespace My.Resources
    
    'This class was auto-generated by the StronglyTypedResourceBuilder
    'class via a tool like ResGen or Visual Studio.
    'To add or remove a member, edit your .ResX file then rerun ResGen
    'with the /str option, or rebuild your VS project.
    '''<summary>
    '''  A strongly-typed resource class, for looking up localized strings, etc.
    '''</summary>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),  _
     Global.Microsoft.VisualBasic.HideModuleNameAttribute()>  _
    Friend Module Resources
        
        Private resourceMan As Global.System.Resources.ResourceManager
        
        Private resourceCulture As Global.System.Globalization.CultureInfo
        
        '''<summary>
        '''  Returns the cached ResourceManager instance used by this class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend ReadOnly Property ResourceManager() As Global.System.Resources.ResourceManager
            Get
                If Object.ReferenceEquals(resourceMan, Nothing) Then
                    Dim temp As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("Stock.Enquiry.Resources", GetType(Resources).Assembly)
                    resourceMan = temp
                End If
                Return resourceMan
            End Get
        End Property
        
        '''<summary>
        '''  Overrides the current thread's CurrentUICulture property for all
        '''  resource lookups using this strongly typed resource class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Property Culture() As Global.System.Globalization.CultureInfo
            Get
                Return resourceCulture
            End Get
            Set
                resourceCulture = value
            End Set
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to F4 Select All.
        '''</summary>
        Friend ReadOnly Property F4SelectAll() As String
            Get
                Return ResourceManager.GetString("F4SelectAll", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to F4 Unselect All.
        '''</summary>
        Friend ReadOnly Property F4UnselectAll() As String
            Get
                Return ResourceManager.GetString("F4UnselectAll", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to F5 Details.
        '''</summary>
        Friend ReadOnly Property F5Details() As String
            Get
                Return ResourceManager.GetString("F5Details", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to F5 Summary.
        '''</summary>
        Friend ReadOnly Property F5Summary() As String
            Get
                Return ResourceManager.GetString("F5Summary", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Retrieving system adjustment codes.
        '''</summary>
        Friend ReadOnly Property GetAdjustCodes() As String
            Get
                Return ResourceManager.GetString("GetAdjustCodes", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Retrieving selected adjustments.
        '''</summary>
        Friend ReadOnly Property GetAdjustments() As String
            Get
                Return ResourceManager.GetString("GetAdjustments", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Initialising spread sheets.
        '''</summary>
        Friend ReadOnly Property InitSpread() As String
            Get
                Return ResourceManager.GetString("InitSpread", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Problem retrieving stock items.
        '''</summary>
        Friend ReadOnly Property ProblemGetItems() As String
            Get
                Return ResourceManager.GetString("ProblemGetItems", resourceCulture)
            End Get
        End Property
    End Module
End Namespace
