﻿Namespace TpWickes

    Public Class StockFactory

#Region "Properties"

        Private Shared mStock As IStock = Nothing
        Private Shared mblnRequirementActivated As System.Nullable(Of Boolean)

        Public Shared Sub SetRequestDecisionEngineDatabase(ByVal Stock As IStock)
            mStock = Stock
        End Sub

#End Region

#Region "Public Procedures And Functions"

        Public Shared Function Create() As IStock

            If mStock Is Nothing Then

                Return New StockNew(Cts.Oasys.Core.System.Parameter.GetInteger(5003))

            Else

                Return mStock   'test  

            End If

        End Function

        Public Shared Function RequirementActivated() As Boolean


            If mblnRequirementActivated.HasValue = False Then mblnRequirementActivated = True 'Cts.Oasys.Core.System.Parameter.GetBoolean(-26)

            RequirementActivated = mblnRequirementActivated.Value

        End Function

#End Region

    End Class

End Namespace


