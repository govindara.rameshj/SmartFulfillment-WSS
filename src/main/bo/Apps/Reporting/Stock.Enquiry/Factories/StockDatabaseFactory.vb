﻿Namespace TpWickes

    Public Class StockDatabaseFactory

        Private Shared mStockDatabase As IStockDatabase = Nothing

        Public Shared Sub SetRequestDecisionEngineDatabase(ByVal StockDatabase As IStockDatabase)
            mStockDatabase = StockDatabase
        End Sub

        Public Shared Function Create() As IStockDatabase

            If mStockDatabase Is Nothing Then

                Return New StockDatabase

            Else

                Return mStockDatabase   'test  

            End If

        End Function

    End Class

End Namespace