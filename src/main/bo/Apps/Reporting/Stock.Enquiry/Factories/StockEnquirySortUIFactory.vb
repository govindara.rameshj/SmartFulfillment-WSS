﻿Public Class StockEnquirySortUIFactory
    Inherits RequirementSwitchFactory(Of IStockEnquirySortUI)

    Public Overrides Function ImplementationA() As IStockEnquirySortUI

        Return New NaturalSort

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(-22008)

    End Function

    Public Overrides Function ImplementationB() As IStockEnquirySortUI

        Return New CategorySort

    End Function

End Class