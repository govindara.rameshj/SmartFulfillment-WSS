﻿Public Class DigDeeperFactory
    Inherits RequirementSwitchFactory(Of IDigDeeper)

    Private _implementationA_IsActive As Boolean
    Private _allowMisSpelling As Boolean = False
    Private _elevateForHierarchy As Boolean = False
    Private _implemetationA_ActivityGot As Boolean

    Public Overrides Function ImplementationA() As IDigDeeper

        GetImplementationAActivity()
        Return New DigDeeperDependantOnParameter(_allowMisSpelling, _elevateForHierarchy)
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Return GetImplementationAActivity()
    End Function

    Public Overrides Function ImplementationB() As IDigDeeper

        Return New DigDeeperLive
    End Function

    Private Function GetImplementationAActivity() As Boolean

        If Not _implemetationA_ActivityGot Then
            Dim SwitchRepository As IRequirementRepository

            SwitchRepository = RequirementRepositoryFactory.FactoryGet()
            _allowMisSpelling = SwitchRepository.IsSwitchPresentAndEnabled(5100)
            _elevateForHierarchy = SwitchRepository.IsSwitchPresentAndEnabled(5101)
            _implementationA_IsActive = _allowMisSpelling Or _elevateForHierarchy
            _implemetationA_ActivityGot = True
        End If
        Return _implementationA_IsActive
    End Function
End Class
