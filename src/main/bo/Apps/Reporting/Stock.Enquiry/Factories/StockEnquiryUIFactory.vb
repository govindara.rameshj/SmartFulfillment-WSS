﻿Public Class StockEnquiryUIFactory
    Inherits RequirementSwitchFactory(Of IStockEnquiryUI)

    Public Overrides Function ImplementationA() As IStockEnquiryUI

        Return New ExclusionFiltersInactive

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(-22006)

    End Function

    Public Overrides Function ImplementationB() As IStockEnquiryUI

        Return New ExclusionFiltersActive

    End Function

End Class