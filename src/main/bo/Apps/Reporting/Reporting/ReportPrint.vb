﻿Imports Cts.Oasys.Core.System
Imports DevExpress.XtraPrintingLinks
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports System.Drawing
Imports DevExpress.XtraGrid.Views.Grid

Public Class ReportPrint
    Implements IDisposable
    Private _grids As New List(Of GridControl)
    Private _printLandscape As Boolean = True
    Private _reportHeader As String = String.Empty
    Private _reportTitle As String = String.Empty
    Private _reportParameters As New List(Of String)
    Private _workstationId As Integer
    Private _userId As Integer
    Private _userIdName As String = String.Empty
    Private _storeIdName As String = String.Empty

    Public Property Grids As List(Of GridControl)
        Get
            Return _grids
        End Get
        Set(ByVal value As List(Of GridControl))
            _grids = value
        End Set
    End Property
    Public Property PrintLandscape As Boolean
        Get
            Return _printLandscape
        End Get
        Set(ByVal value As Boolean)
            _printLandscape = value
        End Set
    End Property
    Public Property ReportHeader As String
        Get
            Return _reportHeader
        End Get
        Set(ByVal value As String)
            _reportHeader = value
        End Set
    End Property
    Public Property ReportTitle As String
        Get
            Return _reportTitle
        End Get
        Set(ByVal value As String)
            _reportTitle = value
        End Set
    End Property
    Public Property ReportParameters As List(Of String)
        Get
            Return _reportParameters
        End Get
        Set(ByVal value As List(Of String))
            _reportParameters = value
        End Set
    End Property
    Public Property WorkstationId As Integer
        Get
            Return _workstationId
        End Get
        Set(ByVal value As Integer)
            _workstationId = value
        End Set
    End Property
    Public Property UserId As Integer
        Get
            Return _userId
        End Get
        Set(ByVal value As Integer)
            _userId = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal grid As GridControl)
        Me.Grids.Add(grid)
    End Sub

    Public Sub New(ByVal grid As GridControl, ByVal userId As Integer, ByVal workstationId As Integer)
        Me.Grids.Add(grid)
        Me.UserId = userId
        Me.WorkstationId = workstationId
    End Sub

    Public Sub New(ByVal repControl As ReportControl, ByVal userId As Integer, ByVal workstationId As Integer)
        Me.ReportHeader = repControl.Report.Header
        Me.ReportTitle = repControl.Report.Title
        Me.UserId = userId
        Me.WorkstationId = workstationId
        Me.Grids = repControl.Grids
        Me.PrintLandscape = repControl.Report.PrintLandscape
        For Each param As Cts.Oasys.Core.Reporting.ReportParameter In repControl.Report.Parameters
            Dim paramFormat As String = GetParameterDisplayFormat(param.DataType)
            Dim s As String = String.Format(paramFormat, param.Description, param.Value)
            Me.ReportParameters.Add(s)
        Next
    End Sub

    Private Function GetParameterDisplayFormat(ByVal type As SqlDbType) As String
        Dim format As String
        Select Case type
            Case SqlDbType.Date
                format = "{0}: {1:d}"
            Case Else
                format = "{0}: {1}"
        End Select
        Return format
    End Function

    Private Sub Report_CreateMarginalHeaderArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        e.Graph.Modifier = BrickModifier.MarginalHeader
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near, StringAlignment.Far)
        e.Graph.Font = New Font("Tahoma", 9, FontStyle.Bold)
        Dim tb As TextBrick = e.Graph.DrawString(_reportHeader, Color.Black, New RectangleF(0, 10, e.Graph.ClientPageSize.Width / 2, 15), DevExpress.XtraPrinting.BorderSide.None)

        e.Graph.Font = New Font("Tahoma", 9, FontStyle.Regular)
        tb = e.Graph.DrawString(_reportTitle, Color.Black, New RectangleF(0, 25, e.Graph.ClientPageSize.Width / 2, 15), DevExpress.XtraPrinting.BorderSide.None)

        Dim sb As New StringBuilder
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Far, StringAlignment.Far)
        For Each s As String In Me.ReportParameters
            If sb.Length > 0 Then sb.Append(Environment.NewLine)
            sb.Append(s)
        Next
        tb = e.Graph.DrawString(sb.ToString, Color.Black, New RectangleF(e.Graph.ClientPageSize.Width / 2, 15, e.Graph.ClientPageSize.Width / 2, 30), DevExpress.XtraPrinting.BorderSide.None)

    End Sub

    Private Sub Report_CreateMarginalFooterArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        e.Graph.Modifier = BrickModifier.MarginalFooter

        Dim r As RectangleF = RectangleF.Empty
        r.Height = 30

        Dim sb As New StringBuilder
        sb.Append("Printed: " & Now.Date.ToShortDateString & " {0:HH:mm}" & Space(4) & "By: " & _userIdName & Space(4) & "WS: " & _workstationId)
        sb.Append(Environment.NewLine)
        sb.Append("Wickes " & _storeIdName)

        Dim pibPrinted As New PageInfoBrick
        pibPrinted = e.Graph.DrawPageInfo(PageInfo.DateTime, sb.ToString, Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pibPrinted.Alignment = BrickAlignment.Near
        pibPrinted.HorzAlignment = DevExpress.Utils.HorzAlignment.Near

        Dim pibPage As New PageInfoBrick
        pibPage = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, "Page: {0} of {1}", Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pibPage.Alignment = BrickAlignment.Far

    End Sub

    Private Sub ReportBreak_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim tb As TextBrick = New TextBrick()
        tb.Rect = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 15)
        tb.BackColor = Color.Transparent
        tb.BorderStyle = BrickBorderStyle.Inset
        tb.BorderColor = Color.White
        e.Graph.DrawBrick(tb)
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 21/06/2011
    ' Referral No : 677
    ' Notes       : Modifed to Allow support users to use the support logins provided.
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Private Function GetReportCompositeLink() As CompositeLink

        _storeIdName = Store.GetIdName
        _userIdName = User.GetUserIdName(Me.UserId, IncludeExternalIds:=True)

        Dim compLink As New CompositeLink(New PrintingSystem)
        compLink.Margins.Bottom = 70
        compLink.Margins.Top = 70
        compLink.Margins.Left = 50
        compLink.Margins.Right = 50
        compLink.Landscape = Me.PrintLandscape
        compLink.PaperKind = System.Drawing.Printing.PaperKind.A4
        compLink.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Watermark, CommandVisibility.None)
        compLink.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.FillBackground, CommandVisibility.None)
        compLink.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.EditPageHF, CommandVisibility.None)

        AddHandler compLink.CreateMarginalHeaderArea, AddressOf Report_CreateMarginalHeaderArea
        AddHandler compLink.CreateMarginalFooterArea, AddressOf Report_CreateMarginalFooterArea

        'set up grid components
        For Each grid As GridControl In Me.Grids
            Dim reportGrid As New PrintableComponentLink
            reportGrid.Component = grid
            compLink.Links.Add(reportGrid)

            Dim break As Link = New Link()
            AddHandler break.CreateDetailArea, AddressOf ReportBreak_CreateDetailArea
            compLink.Links.Add(break)
        Next

        Return compLink

    End Function


    Public Sub ShowPreviewDialog()
        Dim compLink As CompositeLink = GetReportCompositeLink()
        compLink.ShowPreviewDialog()
    End Sub

    Public Sub Print()
        Dim ps As New System.Drawing.Printing.PrinterSettings
        Dim compLink As CompositeLink = GetReportCompositeLink()
        compLink.Print(ps.PrinterName)
    End Sub

    Public Sub Print(ByVal printerName As String)
        Dim compLink As CompositeLink = GetReportCompositeLink()
        compLink.Print(printerName)
    End Sub

#Region " IDisposable Support "
    Private disposedValue As Boolean = False

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
            End If
        End If
        Me.disposedValue = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
