﻿Imports Cts.Oasys.Core.System
Imports System.Drawing

Public Class ReportContainer
    Private _grid(,) As Boolean
    Private _gridWidth As Decimal = 100
    Private _gridHeight As Decimal = 100

    Public Sub New()
        InitializeComponent()

        'get grid widths and heights from parameters (ids=5001 & 5002)
        Dim gridx As Integer = Parameter.GetInteger(5001)
        Dim gridy As Integer = Parameter.GetInteger(5002)
        _gridWidth = Parameter.GetDecimal(5001)
        _gridHeight = Parameter.GetDecimal(5002)
        ReDim _grid(gridx - 1, gridy - 1)

    End Sub

    Private Sub pnlReports_ControlAdded(ByVal sender As Object, ByVal e As System.Windows.Forms.ControlEventArgs) Handles pnlReports.ControlAdded

        'get control added
        Dim rep As ReportControl = CType(e.Control, ReportControl)
        Dim rowMax As Integer = _grid.GetUpperBound(0) + 1
        Dim colMax As Integer = _grid.GetUpperBound(1) + 1

        'get next grid section to fill and check that report fits in it
        For rowIndex As Integer = 0 To rowMax - 1
            For colIndex As Integer = 0 To colMax - 1
                'check that this grid section is empty
                If _grid(rowIndex, colIndex) = True Then Continue For

                'check how many grid sections is long and tall
                Dim gridsWidth As Integer = CInt(Math.Ceiling(rep.MinimumSize.Width / _gridWidth))
                Dim gridsHeight As Integer = CInt(Math.Ceiling(rep.MinimumSize.Height / _gridHeight))

                'check that report does not go over max number grid columns
                If colIndex + gridsWidth > colMax Then Continue For

                'check that there is nothing underneath proposed grid sections
                Dim okToAdd As Boolean = True
                For rowCheck As Integer = colIndex To colIndex + (gridsWidth - 1)
                    If _grid(rowIndex, colIndex) = True Then okToAdd = False
                Next
                If okToAdd = False Then Continue For

                'resize report control to required size and re-position
                rep.Size = New Size(CInt(_gridWidth * gridsWidth), CInt(_gridHeight * gridsHeight))
                rep.Location = New Point(CInt(_gridWidth * colIndex), CInt(_gridHeight * rowIndex))

                'set grid flags to true
                For colcheck As Integer = colIndex To colIndex + (gridsWidth - 1)
                    For rowCheck As Integer = rowIndex To rowIndex + (gridsHeight - 1)
                        _grid(rowCheck, colcheck) = True
                    Next
                Next

                'exit sub
                Exit Sub
            Next
        Next

    End Sub

End Class
