﻿Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid
Imports Cts.Oasys.Core.Reporting
Imports System.Drawing

Public Interface IReportControl

    Sub SetupColumns(ByRef view As GridView, ByVal repTable As ReportTable, ByRef pnlGrids As System.Windows.Forms.Panel)
    Sub view_CustomRowCellEdit(ByVal sender As Object, ByVal e As CustomRowCellEditEventArgs, ByVal repTable As ReportTable)
    Sub view_RowCellStyle(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs, ByVal ReportTables As ReportTableCollection)
    Function SetBoldAttribute(ByVal ColumnTag As Object, ByVal DefaultFont As Font, ByVal EmboldenThisValue As Object) As Font
End Interface
