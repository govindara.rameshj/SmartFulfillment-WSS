﻿Public Class ReportHost

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 12/05/2011
    ' Referral No : 442
    ' Notes       : Modifed to handle Enter key
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5, Keys.Enter : btnRefresh.Enabled = True
                btnRefresh.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 12/05/2011
    ' Referral No : 442
    ' Notes       : Modifed to bring focus on the textbox
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''' <summary>
    ''' Modifed to bring focus on the textbox
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub DoProcessing()

        Try
            Cursor = Cursors.WaitCursor

            'get spread control for first config and set up selection criteria
            Dim dStatus As New CtsControls.Enquiry.DisplayStatus(AddressOf MyBase.DisplayStatus)
            Dim dError As New CtsControls.Enquiry.DisplayError(Sub(ex, title) MyBase.DisplayStatus())
            Dim dOasys As New CtsControls.Enquiry.DisplayOasys(Sub(ex, title) MyBase.DisplayStatus())

            Dim enq As New CtsControls.Enquiry(dStatus, dError, dOasys, RunParameters, MyBase.UserId, MyBase.WorkstationId, False)
            AddHandler enq.btnExit.Click, AddressOf btnExit_Click

            pnlSpreads.Controls.Add(enq)
            For Each selection As CtsControls.EnquirySelection In enq.Selections
                cgbSelection.Controls.Add(selection)
            Next

            If cgbSelection.Controls.Count > 2 Then
                cgbSelection.Visible = True

                If (Me.AppName.ToLower.Equals("label maintenance")) Then
                    cgbSelection.Controls.Item(cgbSelection.Controls.Count - 1).Controls.Item(6).Focus()
                    cgbSelection.Controls.Item(cgbSelection.Controls.Count - 1).Controls.Item(5).Hide()
                    btnRefresh.Enabled = False
                    enq.btnExcel.Hide()
                    enq.btnExit.Hide()
                    enq.btnRefresh.Hide()
                    enq.btnPrint.Hide()
                    enq.btnSave.Hide()
                End If

                cgbSelection.LayoutControls(CtsControls.CollapsibleGroupBox.Layouts.Horizontal, True)
                btnRefresh.Visible = True

                Dim moveHeight As Integer = cgbSelection.Height + (cgbSelection.Margin.Top * 2)
                pnlSpreads.Top += moveHeight
                pnlSpreads.Height -= moveHeight
            End If

            'if no selection criteria then do refresh
            If cgbSelection.Visible = False Then
                Refresh()
                For Each ctl As Control In pnlSpreads.Controls
                    CType(ctl, CtsControls.Enquiry).btnRefresh.PerformClick()
                Next
            End If

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub



    Private Sub pnlSpreads_ControlAdded() Handles pnlSpreads.ControlAdded, pnlSpreads.ControlRemoved, pnlSpreads.Resize

        If pnlSpreads.Controls.Count = 0 Then Exit Sub

        'resize all controls height evenly
        Dim h As Integer = CInt(pnlSpreads.Height / pnlSpreads.Controls.Count)
        Dim w As Integer = pnlSpreads.Width
        Dim t As Integer = 0

        For Each ctl As Control In pnlSpreads.Controls
            ctl.Left = 0
            ctl.Top = t
            ctl.Width = w
            ctl.Height = h
            t = ctl.Bottom
        Next

    End Sub


    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 12/05/2011
    ' Referral No : 442
    ' Notes       : Modifed to handle 'Enter'
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''' <summary>
    ''' Modifed to handle 'Enter'
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click

        If (Me.AppName.ToLower.Equals("label maintenance")) Then
            For Each ctl As Control In pnlSpreads.Controls
                Dim spd As CtsControls.Enquiry = CType(ctl, CtsControls.Enquiry)
                spd.btnRefresh.Show()
                spd.btnSave.Show()
                If spd.SheetActive >= 0 And spd.SheetActiveColumn < 3 Then
                    spd.SheetColumnTab = spd.SheetActiveColumn

                    spd.btnRefresh.PerformClick()
                ElseIf spd.SheetActive >= 0 Then
                    spd.btnSave.PerformClick()
                Else
                    spd.btnRefresh.PerformClick()
                End If
                btnRefresh.Enabled = False
                spd.btnRefresh.Hide()
                spd.btnSave.Hide()
            Next
        Else
            For Each ctl As Control In pnlSpreads.Controls
                Dim spd As CtsControls.Enquiry = CType(ctl, CtsControls.Enquiry)
                spd.btnRefresh.PerformClick()
            Next
        End If

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        FindForm.Close()
    End Sub

End Class