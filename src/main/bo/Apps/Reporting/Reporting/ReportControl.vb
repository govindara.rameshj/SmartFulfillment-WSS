﻿Imports Cts.Oasys.Core.Reporting

Public Class ReportControl

    Private Enum MousePositions
        None
        Header
        Top
        Right
        Left
        Bottom
        CornerNW
        CornerNE
        CornerSW
        CornerSE
    End Enum

    Private _MousePosition As MousePositions = MousePositions.None
    Private _MouseRelative As Point = Nothing
    Private _report As Report
    Private _grids As New List(Of GridControl)
    Private _userId As Integer
    Private _workstationId As Integer
    Private _locked As Boolean

    Public ReadOnly Property Report() As Report
        Get
            Return _report
        End Get
    End Property

    Public ReadOnly Property Grids() As List(Of GridControl)
        Get
            Return _grids
        End Get
    End Property

    Public Property Locked() As Boolean
        Get
            Return _locked
        End Get
        Set(ByVal value As Boolean)
            _locked = value
        End Set
    End Property

    Sub New(ByVal report As Report, ByVal userId As Integer, ByVal workstationId As Integer)

        InitializeComponent()
        _report = report
        _userId = userId
        _workstationId = workstationId
        Initialise()
    End Sub

    Sub New(ByVal reportId As Integer, ByVal parameterValues() As Object, ByVal userId As Integer, ByVal workstationId As Integer)

        InitializeComponent()
        _report = Report.GetReport(reportId)
        _userId = userId
        _workstationId = workstationId
        If parameterValues.Count > 0 Then
            For index As Integer = 0 To parameterValues.Count - 1
                _report.Parameters(index).Value = parameterValues(index)
            Next
        End If
        Initialise()
    End Sub

    Private Sub Initialise()

        lblTitle.Text = _report.Title
        If _report.MinWidth > 0 Then
            Me.MinimumSize = New Size(_report.MinWidth, Me.MinimumSize.Height)
        End If
        If _report.MinHeight > 0 Then
            Me.MinimumSize = New Size(Me.MinimumSize.Width, _report.MinHeight)
        End If
    End Sub

    Public Sub SetParameters(ByVal dic As Dictionary(Of String, Object))

        For Each parameter As ReportParameter In _report.Parameters
            Dim value As Object = Nothing
            If dic.TryGetValue(parameter.Name, value) Then
                parameter.Value = value
            End If
        Next
    End Sub

    Public Sub LoadData() Handles btnRefresh.Click

        If Cts.Oasys.Core.System.Parameter.GetBoolean(-61) Then
            DataLoadNew()
        Else
            DataLoad()
        End If
    End Sub

    Public Sub Print() Handles btnPrint.Click

        Using print As New ReportPrint(Me, _userId, _workstationId)
            print.ShowPreviewDialog()
        End Using
    End Sub

    Public Sub PrintQuick()

        Using print As New ReportPrint(Me, _userId, _workstationId)
            print.Print()
        End Using
    End Sub

    Public Sub PrintQuick(ByVal printerName As String)

        Using print As New ReportPrint(Me, _userId, _workstationId)
            print.Print(printerName)
        End Using
    End Sub

    Private Sub SetupView(ByRef view As GridView, ByVal repTable As ReportTable)

        view.Name = repTable.Name
        view.Appearance.HeaderPanel.Font = New Font("Tahoma", 8, System.Drawing.FontStyle.Bold)
        view.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        view.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        view.Appearance.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        view.Appearance.HeaderPanel.Options.UseFont = True
        view.Appearance.HeaderPanel.Options.UseTextOptions = True
        view.Appearance.Row.Font = New Font("Tahoma", 7.5, FontStyle.Regular)
        view.Appearance.GroupFooter.Font = New Font("Tahoma", 7.5, FontStyle.Bold)
        view.Appearance.GroupFooter.Options.UseFont = True
        view.Appearance.FooterPanel.Font = New Font("Tahoma", 7.5, FontStyle.Bold)
        view.Appearance.FooterPanel.Options.UseFont = True
        'view.HorzScrollVisibility.Always()

        view.OptionsView.ShowGroupPanel = repTable.AllowGrouping
        view.OptionsView.ShowColumnHeaders = repTable.ShowHeaders
        view.OptionsView.ShowHorzLines = repTable.ShowLinesHorizontal
        view.OptionsView.ShowVertLines = repTable.ShowLinesVertical
        view.OptionsView.ShowIndicator = repTable.ShowIndicator
        view.OptionsView.ColumnAutoWidth = repTable.AutoFitColumns

        view.AppearancePrint.Assign(view.Appearance)
        view.AppearancePrint.HeaderPanel.Font = New Font("Tahoma", 7.5, FontStyle.Bold)
        view.AppearancePrint.Row.Font = New Font("Tahoma", 7, FontStyle.Regular)
        view.OptionsPrint.UsePrintStyles = True
        view.OptionsPrint.PrintHorzLines = repTable.ShowLinesHorizontal
        view.OptionsPrint.PrintVertLines = repTable.ShowLinesVertical
        view.OptionsPrint.PrintHeader = repTable.ShowHeaders
        view.OptionsPrint.PrintFilterInfo = False

        view.OptionsCustomization.AllowGroup = repTable.AllowGrouping
        view.OptionsBehavior.Editable = False
        view.OptionsSelection.EnableAppearanceFocusedRow = repTable.AllowSelection
        view.OptionsSelection.EnableAppearanceFocusedCell = False

        If repTable.HeaderHeight > 0 Then view.ColumnPanelRowHeight = repTable.HeaderHeight
        If repTable.RowHeight > 0 Then view.RowHeight = repTable.RowHeight

        AddHandler view.CustomUnboundColumnData, AddressOf view_CustomUnboundColumnData
        AddHandler view.CustomRowCellEdit, AddressOf view_CustomRowCellEdit
        AddHandler view.RowCellStyle, AddressOf view_RowCellStyle
        AddHandler view.CustomSummaryCalculate, AddressOf view_CustomSummaryCalculate
        AddHandler view.MouseDown, AddressOf view_MouseDown
    End Sub

    Private Sub SetupColumns(ByRef view As GridView, ByVal repTable As ReportTable)
        Dim Implementation As IReportControl = (New ReportControlFactory).GetImplementation

        Implementation.SetupColumns(view, repTable, pnlGrids)
    End Sub

    Private Sub SetupSummaries(ByRef view As GridView, ByVal repTable As ReportTable)

        If repTable.Summaries.Count > 0 Then
            view.OptionsView.ShowFooter = True

            For Each repSummary As ReportSummary In repTable.Summaries
                view.Columns(repSummary.Name).SummaryItem.SummaryType = CType(repSummary.SummaryType, SummaryItemType)
                view.Columns(repSummary.Name).SummaryItem.FieldName = repSummary.Name

                If repSummary.Format IsNot Nothing Then
                    view.Columns(repSummary.Name).SummaryItem.DisplayFormat = repSummary.Format
                End If

                If repSummary.ApplyToGroups Then
                    Dim item As New GridGroupSummaryItem
                    item.FieldName = repSummary.Name
                    item.SummaryType = CType(repSummary.SummaryType, SummaryItemType)
                    If repSummary.Format IsNot Nothing Then item.DisplayFormat = repSummary.Format
                    item.ShowInGroupColumnFooter = view.Columns(repSummary.Name)
                    view.GroupSummary.Add(item)
                    view.GroupFooterShowMode = Views.Grid.GroupFooterShowMode.VisibleIfExpanded
                End If
            Next
        End If

    End Sub

    Private Sub SetupGrouping(ByRef view As GridView, ByVal repTable As ReportTable)

        'apply grouping
        If repTable.Groupings.Count > 0 Then
            Dim expandGroups As Boolean = False

            Try
                view.ClearGrouping()
                view.BeginSort()

                For Each group As ReportGrouping In repTable.Groupings

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ' Author      : Partha Dutta
                    '
                    ' Date        : 16/08/2010
                    '
                    ' Referral No : NA
                    '
                    ' Notes       : If group column value does not exist in grouping collect it will fall over with "...object not found...."
                    '
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    If view.Columns(group.Name) IsNot Nothing Then
                        view.Columns(group.Name).GroupIndex = repTable.Groupings.IndexOf(group)
                        If group.ShowExpanded Then expandGroups = True

                        'add summary if there
                        If CType(group.SummaryType, DevExpress.Data.SummaryItemType) <> DevExpress.Data.SummaryItemType.None Then
                            Dim item As New GridGroupSummaryItem
                            item.FieldName = group.Name
                            item.SummaryType = CType(group.SummaryType, SummaryItemType)
                            view.GroupSummary.Add(item)
                        End If
                    End If

                    'Old Code

                    'view.Columns(group.Name).GroupIndex = repTable.Groupings.IndexOf(group)
                    'If group.ShowExpanded Then expandGroups = True

                    ''add summary if there
                    'If CType(group.SummaryType, DevExpress.Data.SummaryItemType) <> DevExpress.Data.SummaryItemType.None Then
                    '    Dim item As New GridGroupSummaryItem
                    '    item.FieldName = group.Name
                    '    item.SummaryType = group.SummaryType
                    '    view.GroupSummary.Add(item)
                    'End If
                Next

            Finally
                view.EndSort()
                If expandGroups Then view.ExpandAllGroups()
            End Try

        End If

    End Sub

    Public Function HasData() As Boolean

        If _grids.Count > 0 Then
            Return (_grids(0).Views(0).RowCount > 0)
        End If
        Return False

    End Function

    Private Sub view_CustomUnboundColumnData(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs)

        Dim view As GridView = CType(sender, GridView)
        If e.Column.FieldName.StartsWith("Image") AndAlso e.IsGetData Then
            'get column that holds filename from column tag and load image
            Dim filePath As String = CStr(view.GetRowCellValue(e.RowHandle, CStr(e.Column.Tag)))
            If filePath IsNot Nothing AndAlso File.Exists(filePath) Then
                e.Value = Image.FromFile(filePath)
            End If
        ElseIf e.Column.Tag.ToString.Length > 0 Then

        End If

    End Sub

    Private Sub view_CustomRowCellEdit(ByVal sender As Object, ByVal e As CustomRowCellEditEventArgs)
        Dim view As GridView = CType(sender, GridView)
        Dim repTable As ReportTable = _report.Tables.First(Function(f) f.Name = view.Name)
        Dim Implementation As IReportControl = (New ReportControlFactory).GetImplementation

        Implementation.view_CustomRowCellEdit(sender, e, repTable)
    End Sub

    Private Sub view_RowCellStyle(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs)
        Dim Implementation As IReportControl = (New ReportControlFactory).GetImplementation

        Implementation.view_RowCellStyle(sender, e, _report.Tables)
    End Sub

    Private Sub view_CustomSummaryCalculate(ByVal sender As System.Object, ByVal e As DevExpress.Data.CustomSummaryEventArgs)

        If e.IsTotalSummary Then
            Dim view As GridView = CType(sender, GridView)
            Dim repTable As ReportTable = _report.Tables(0)

            For Each repSummary As ReportSummary In repTable.Summaries
                If repSummary.SummaryType = SummaryItemType.Custom Then
                    If CType(e.Item, GridColumnSummaryItem).FieldName = repSummary.Name Then
                        Select Case e.SummaryProcess
                            Case CustomSummaryProcess.Start
                                repSummary.NumeratorValue = 0
                                repSummary.DenominatorValue = 0

                            Case CustomSummaryProcess.Calculate
                                repSummary.NumeratorValue += CDec(e.GetValue(repSummary.NumeratorName))
                                repSummary.DenominatorValue += CDec(e.GetValue(repSummary.DenominatorName))

                            Case CustomSummaryProcess.Finalize
                                If repSummary.DenominatorValue = 0 Then
                                    e.TotalValue = 0
                                Else
                                    e.TotalValue = CDec(repSummary.NumeratorValue / repSummary.DenominatorValue * 100)
                                End If
                        End Select
                    End If
                End If
            Next

        End If

    End Sub

    Private Sub view_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)

        'get view and report table in question
        Dim view As GridView = CType(sender, GridView)
        Dim repTable As ReportTable = _report.Tables.First(Function(f) f.Name = view.Name)

        Dim hi As GridHitInfo = view.CalcHitInfo(New Point(e.X, e.Y))
        If hi.InRowCell Then

            'check there is a RowId column first exiting if not found
            Dim rowIdColName As String = repTable.Columns.ColumnName(0)
            If rowIdColName Is Nothing Then Exit Sub

            'get rowId for this cell's row exiting if is null
            Dim rowIdValue As Object = view.GetRowCellValue(hi.RowHandle, rowIdColName)
            If rowIdValue Is Nothing OrElse IsDBNull(rowIdValue) Then Exit Sub

            'loop over all reportHyperlinks for this rowId value
            For Each repHyperlink As ReportHyperlink In repTable.Hyperlinks.Where(Function(f) f.RowId = CInt(rowIdValue))
                'check that this cell's column is that of the hyperlink column
                If hi.Column.FieldName = repHyperlink.ColumnName Then

                    Select Case True
                        Case repHyperlink.Value.Trim.StartsWith("ReportId=")
                            Dim reportId As Integer
                            Dim parameterValues As New ArrayList
                            'split into report id and parameters (if exist)
                            Dim values() As String = repHyperlink.Value.Trim.Split("|"c)
                            For index As Integer = 0 To values.Count - 1
                                'get report id
                                If index = 0 Then
                                    reportId = CInt(values(0).Substring(9))
                                End If

                                'get parameter values checking if from another column or constant
                                If values(index).StartsWith("[") Then
                                    Dim colName As String = values(index).Substring(1, values(index).Length - 2)
                                    parameterValues.Add(view.GetRowCellValue(hi.RowHandle, colName))
                                End If
                            Next

                            Dim repControl As New ReportControl(reportId, CType(parameterValues.ToArray(GetType(Object)), Object()), _userId, _workstationId)
                            Dim repViewer As New ReportViewer(0, 0, 9, "", repControl)

                            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                            ' Author      : Partha Dutta
                            '
                            ' Date        : 23/08/2010
                            '
                            ' Referral No : 138B
                            '
                            ' Notes       : Main menu has flag to decide if screen to be called maximised or not
                            '               Screens called from hyperlinks have no such flag, pragmatic solution is to call all screens maximixsed
                            '               and not write a more comprehensive customisable database driven solution
                            '
                            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                            Dim hostform As New HostForm(repViewer, True)





                            hostform.Owner = Me.FindForm
                            hostform.Show()

                        Case repHyperlink.Value.Trim.StartsWith("MenuId=")
                            Dim menuId As Integer
                            Dim parameterValues As New StringBuilder

                            'split into report id and parameters (if exist)
                            Dim values() As String = repHyperlink.Value.Trim.Split("|"c)
                            For index As Integer = 0 To values.Count - 1
                                If index = 0 Then
                                    menuId = CInt(values(0).Substring(7))
                                Else
                                    'get parameter values checking if from another column or constant
                                    If values(index).StartsWith("[") Then
                                        Dim colName As String = values(index).Substring(1, values(index).Length - 2)
                                        If parameterValues.Length > 0 Then parameterValues.Append(",")
                                        parameterValues.Append(view.GetRowCellValue(hi.RowHandle, colName))
                                    Else
                                        If parameterValues.Length > 0 Then parameterValues.Append(",")
                                        parameterValues.Append(values(index))
                                    End If
                                End If
                            Next

                            Dim MenuToLoad As MenuOption.Item
                            Dim ReportUI As IReportControlUI

                            ReportUI = (New ReportControlUIFactory).GetImplementation()

                            MenuToLoad = ReportUI.GetSingleGetMenuItem(menuId)

                            Dim hostform As New HostForm(MenuToLoad.AssemblyName, MenuToLoad.ClassName, _
                                                         MenuToLoad.Description, _userId, _workstationId, _
                                                         9, parameterValues.ToString, MenuToLoad.ImagePath, MenuToLoad.IsMaximised)

                            hostform.Owner = Me.FindForm
                            hostform.Show()

                        Case repHyperlink.Value.Trim.StartsWith("IncorrectEANReport.exe")
                            Process.Start("IncorrectEANReport.exe", "")

                        Case repHyperlink.Value.Trim.StartsWith("saleordermaintain.exe")
                            Process.Start("som\saleordermaintain.exe", "")


                        Case repHyperlink.Value.Trim.StartsWith("StockDetail")
                            'split into parameters (if exist)
                            Dim parameterValues As New ArrayList
                            Dim values() As String = repHyperlink.Value.Trim.Split("|"c)
                            For index As Integer = 0 To values.Count - 1
                                'get parameter values checking if from another column or constant
                                If values(index).StartsWith("[") Then
                                    Dim colName As String = values(index).Substring(1, values(index).Length - 2)
                                    parameterValues.Add(view.GetRowCellValue(hi.RowHandle, colName))
                                End If
                            Next

                            Dim stockHost As New StockEnquiryHost(CStr(parameterValues(0)), _userId, CStr(_workstationId))
                            stockHost.WindowState = FormWindowState.Maximized
                            stockHost.Show()

                            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                            ' Author      : Partha Dutta
                            '
                            ' Date        : 10/08/2010
                            '
                            ' Referral No : 244
                            '
                            ' Notes       : Price Change Report is called modally without any parameters
                            '
                            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                        Case repHyperlink.Value.Trim.StartsWith("VB Price Change Report")
                            Dim newProcess As Process = Process.Start("PriceChangeReport.exe", "")

                        Case repHyperlink.Value.Trim.StartsWith("VB Price Change Confirmation Report")
                            Dim newProcess As Process = Process.Start("PriceChangeConfirm.exe", "")

                        Case repHyperlink.Value.Trim.StartsWith("VB Label Request Audit")
                            Dim newProcess As Process = Process.Start("LabelsRequestAudit.exe", "")

                            newProcess.WaitForExit() 'modal
                    End Select
                End If
            Next
        End If

    End Sub

    Private Sub grid_Resize(ByVal sender As Object, ByVal e As System.EventArgs)

        'If _report.Tables(0).RowHeight = -1 Then
        '    xviewReport.RowHeight = xgridReport.Height
        'End If

    End Sub

    Private Sub lblTitle_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lblTitle.MouseMove

        If _locked Then Exit Sub
        If e.Button = Windows.Forms.MouseButtons.Left Then
            'move control
            BringToFront()
            If _MouseRelative = Nothing Then _MouseRelative = New Point(e.X, e.Y)

            Dim newLeft As Integer = Math.Max(0, Me.Left + e.X - _MouseRelative.X)
            Dim newTop As Integer = Math.Max(0, Me.Top + e.Y - _MouseRelative.Y)

            Me.Left = newLeft
            Me.Top = newTop
        Else
            'set mouse position and cursor
            _MouseRelative = Nothing
            Cursor = Cursors.SizeAll
        End If

    End Sub

    Private Sub lblTitle_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblTitle.MouseLeave
        Cursor = Cursors.Default
    End Sub

    Private Sub pnl_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pnl.MouseLeave
        Cursor = Cursors.Default
    End Sub

    Private Sub pnl_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pnl.MouseMove

        If _locked Then Exit Sub
        If e.Button = Windows.Forms.MouseButtons.Left Then
            If _MouseRelative = Nothing Then _MouseRelative = New Point(e.X, e.Y)

            'resize control
            BringToFront()
            Select Case Cursor
                Case Cursors.SizeNS
                    Select Case _MousePosition
                        Case MousePositions.Top : ResizeTop(e.Y)
                        Case MousePositions.Bottom : ResizeBottom(e.Y)
                    End Select

                Case Cursors.SizeWE
                    Select Case _MousePosition
                        Case MousePositions.Left : ResizeLeft(e.X)
                        Case MousePositions.Right : ResizeRight(e.X)
                    End Select

                Case Cursors.SizeNESW
                    Select Case _MousePosition
                        Case MousePositions.CornerNE
                            ResizeTop(e.Y)
                            ResizeRight(e.X)
                        Case MousePositions.CornerSW
                            ResizeBottom(e.Y)
                            ResizeLeft(e.X)
                    End Select

                Case Cursors.SizeNWSE
                    Select Case _MousePosition
                        Case MousePositions.CornerNW
                            ResizeTop(e.Y)
                            ResizeLeft(e.X)
                        Case MousePositions.CornerSE
                            ResizeBottom(e.Y)
                            ResizeRight(e.X)
                    End Select
            End Select
            Refresh()
            Me.Parent.Refresh()

        Else
            'set mouse position and cursor
            _MouseRelative = Nothing
            Select Case e.Y
                Case Is < 4
                    Select Case e.X
                        Case Is < 4 : _MousePosition = MousePositions.CornerNW
                        Case Is > Me.Width - 4 : _MousePosition = MousePositions.CornerNE
                        Case Else : _MousePosition = MousePositions.Top
                    End Select

                Case Is > pnl.Height - 4
                    Select Case e.X
                        Case Is < 4 : _MousePosition = MousePositions.CornerSW
                        Case Is > Me.Width - 4 : _MousePosition = MousePositions.CornerSE
                        Case Else : _MousePosition = MousePositions.Bottom
                    End Select

                Case Else
                    Select Case e.X
                        Case Is < 4 : _MousePosition = MousePositions.Left
                        Case Is > Me.Width - 4 : _MousePosition = MousePositions.Right
                        Case Else : _MousePosition = MousePositions.None
                    End Select
            End Select

            'set cursor type
            Select Case _MousePosition
                Case MousePositions.Bottom, MousePositions.Top : Cursor = Cursors.SizeNS
                Case MousePositions.Left, MousePositions.Right : Cursor = Cursors.SizeWE
                Case MousePositions.CornerNW, MousePositions.CornerSE : Cursor = Cursors.SizeNWSE
                Case MousePositions.CornerNE, MousePositions.CornerSW : Cursor = Cursors.SizeNESW
                Case Else : Cursor = Cursors.Default
            End Select
        End If

    End Sub

    Private Sub ResizeTop(ByVal y As Integer)
        Dim newTop As Integer = Math.Max(0, Me.Top + y - _MouseRelative.Y)
        Dim newHeight As Integer = Me.Height + Me.Top - newTop

        If Me.MinimumSize.Height > 0 AndAlso newHeight < Me.MinimumSize.Height Then Exit Sub
        Me.Height += Me.Top - newTop
        Me.Top = newTop

    End Sub

    Private Sub ResizeBottom(ByVal y As Integer)
        Dim newHeight As Integer = Me.Height + y - _MouseRelative.Y
        If Me.MinimumSize.Height > 0 AndAlso newHeight < Me.MinimumSize.Height Then Exit Sub
        Me.Height = newHeight
        _MouseRelative.Y = y

    End Sub

    Private Sub ResizeLeft(ByVal x As Integer)
        Dim newLeft As Integer = Math.Max(0, Me.Left + x - _MouseRelative.X)
        Dim newWidth As Integer = Me.Width + Me.Left - newLeft

        If Me.MinimumSize.Width > 0 AndAlso newWidth < Me.MinimumSize.Width Then Exit Sub
        Me.Width += Me.Left - newLeft
        Me.Left = newLeft

    End Sub

    Private Sub ResizeRight(ByVal x As Integer)
        Dim newWidth As Integer = Me.Width + x - _MouseRelative.X
        If Me.MinimumSize.Width > 0 AndAlso newWidth < Me.MinimumSize.Width Then Exit Sub
        Me.Width = newWidth
        _MouseRelative.X = x

    End Sub

    Private Sub btnExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcel.Click

        For Each grid As GridControl In pnlGrids.Controls
            Using dialog As New SaveFileDialog
                dialog.RestoreDirectory = True
                dialog.Filter = "Excel Files (*.xls)|*.xls"
                dialog.CheckPathExists = True

                If dialog.ShowDialog(Me) = DialogResult.OK Then
                    grid.ExportToXls(dialog.FileName)
                End If
            End Using
        Next

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Dispose()
    End Sub

    Private Sub DataLoad()

        'remove old grids first
        pnlGrids.Controls.Clear()
        _grids.Clear()

        'get data from stored procedure and set up dataset
        _report.LoadData()

        'set up grids and views
        Dim ds As DataSet = _report.Dataset

        For Each repTable As ReportTable In _report.Tables
            Dim dt As DataTable = ds.Tables(repTable.Name)
            Dim grid As GridControl
            Dim view As GridView

            If dt.ParentRelations.Count = 0 Then
                'create new grid and view
                view = New GridView
                grid = New GridControl
                grid.MainView = view
                grid.Name = "grid" & dt.TableName
                grid.ViewCollection.Add(view)
                view.GridControl = grid
                view.HorzScrollVisibility = Views.Base.ScrollVisibility.Auto
                grid.Anchor = AnchorStyles.Left Or AnchorStyles.Top Or AnchorStyles.Bottom Or AnchorStyles.Right


            Else


                'find grid to add new view to
                grid = _grids.Find(Function(f) f.Name = "grid" & dt.ParentRelations(0).ParentTable.TableName)

                view = New GridView(grid)

                grid.LevelTree.Nodes.Add(dt.ParentRelations(0).RelationName, view)
                For Each repColumn As ReportColumn In repTable.Columns
                    view.Columns.AddField(repColumn.Name).VisibleIndex = -1
                Next

                SetupView(view, repTable)
                SetupColumns(view, repTable)
                SetupGrouping(view, repTable)
                SetupSummaries(view, repTable)

                Continue For
            End If

            pnlGrids.Controls.Add(grid)

            grid.DataSource = _report.Dataset
            grid.DataMember = dt.TableName

            _grids.Add(grid)


            SetupView(view, repTable)
            SetupColumns(view, repTable)
            SetupSummaries(view, repTable)
            SetupGrouping(view, repTable)

            If repTable.ShowInPrintOnly = False Then
                grid.Location = New Point(0, 0)
                grid.Size = New Size(pnlGrids.Width, pnlGrids.Height)

                'set up anchors
                Select Case True
                    Case repTable.AnchorTop AndAlso repTable.AnchorBottom
                    Case repTable.AnchorTop
                        grid.Anchor = AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Top
                        grid.Height = view.RowCount * 20
                        If view.OptionsView.ShowColumnHeaders Then
                            Select Case view.ColumnPanelRowHeight
                                Case -1 : grid.Height += 50
                                Case Is > 0 : grid.Height += view.ColumnPanelRowHeight
                            End Select
                        End If
                    Case repTable.AnchorBottom
                        grid.Anchor = AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Bottom
                        grid.Height = view.RowCount * 20
                        If view.OptionsView.ShowColumnHeaders Then
                            Select Case view.ColumnPanelRowHeight
                                Case -1 : grid.Height += 50
                                Case Is > 0 : grid.Height += view.ColumnPanelRowHeight
                            End Select
                        End If

                End Select

                'check size and location of any other existing grids
                Dim top As Integer = 0
                Dim height As Integer = pnlGrids.Height
                If pnlGrids.Controls.Count = 1 Then Continue For

                For Each g As GridControl In pnlGrids.Controls
                    If g Is grid Then Continue For
                    'resize any grids anchored to top and bottom
                    Select Case True
                        Case g.Anchor = (AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Top Or AnchorStyles.Bottom)
                            g.Height -= grid.Height - 10
                            top = g.Bottom + 5
                            height -= g.Height + 5

                        Case g.Anchor = (AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Top)
                            top = g.Bottom + 5
                            height -= g.Height + 5
                    End Select
                Next

                grid.Location = New Point(0, top)
                grid.Size = New Size(pnlGrids.Width, height)

            End If

        Next
    End Sub

    Private Sub DataLoadNew()
        'remove old grids first
        pnlGrids.Controls.Clear()
        _grids.Clear()

        'get data from stored procedure and set up dataset
        _report.LoadData()

        'set up grids and views
        Dim ds As DataSet = _report.Dataset
        Dim grid As GridControl = Nothing
        Dim rootTableName As String = ""

        For Each repTable As ReportTable In _report.Tables
            Dim dt As DataTable = ds.Tables(repTable.Name)
            Dim view As GridView

            If dt.ParentRelations.Count = 0 Then
                'create new grid and view
                view = New GridView
                grid = New GridControl
                grid.MainView = view
                grid.Name = "grid" & dt.TableName
                rootTableName = dt.TableName
                grid.ViewCollection.Add(view)
                view.GridControl = grid
                view.HorzScrollVisibility = Views.Base.ScrollVisibility.Auto
                grid.Anchor = AnchorStyles.Left Or AnchorStyles.Top Or AnchorStyles.Bottom Or AnchorStyles.Right

                '************************************************************************
                'MO'C 07-07-2011 - Live store issue for referral 61 
                'This has been removed as it changes the order of the report from being
                'store procedure driven, to ordanal order, which is not required.
                '************************************************************************
                'For Each repColumn As ReportColumn In repTable.Columns
                '    view.Columns.AddField(repColumn.Name).VisibleIndex = -1
                'Next
                'END*********************************************************************

                pnlGrids.Controls.Add(grid)

                grid.DataSource = _report.Dataset
                grid.DataMember = dt.TableName

                _grids.Add(grid)

                SetupView(view, repTable)
                SetupColumns(view, repTable)
                SetupSummaries(view, repTable)
                SetupGrouping(view, repTable)

                If repTable.ShowInPrintOnly = False Then
                    grid.Location = New Point(0, 0)
                    grid.Size = New Size(pnlGrids.Width, pnlGrids.Height)

                    'set up anchors
                    Select Case True
                        Case repTable.AnchorTop AndAlso repTable.AnchorBottom
                        Case repTable.AnchorTop
                            grid.Anchor = AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Top
                            grid.Height = view.RowCount * 20
                            If view.OptionsView.ShowColumnHeaders Then
                                Select Case view.ColumnPanelRowHeight
                                    Case -1 : grid.Height += 50
                                    Case Is > 0 : grid.Height += view.ColumnPanelRowHeight
                                End Select
                            End If
                        Case repTable.AnchorBottom
                            grid.Anchor = AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Bottom
                            grid.Height = view.RowCount * 20
                            If view.OptionsView.ShowColumnHeaders Then
                                Select Case view.ColumnPanelRowHeight
                                    Case -1 : grid.Height += 50
                                    Case Is > 0 : grid.Height += view.ColumnPanelRowHeight
                                End Select
                            End If

                    End Select

                    'check size and location of any other existing grids
                    Dim top As Integer = 0
                    Dim height As Integer = pnlGrids.Height
                    If pnlGrids.Controls.Count = 1 Then Continue For

                    For Each g As GridControl In pnlGrids.Controls
                        If g Is grid Then Continue For
                        'resize any grids anchored to top and bottom
                        Select Case True
                            Case g.Anchor = (AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Top Or AnchorStyles.Bottom)
                                g.Height -= grid.Height - 10
                                top = g.Bottom + 5
                                height -= g.Height + 5

                            Case g.Anchor = (AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Top)
                                top = g.Bottom + 5
                                height -= g.Height + 5
                        End Select
                    Next

                    grid.Location = New Point(0, top)
                    grid.Size = New Size(pnlGrids.Width, height)

                End If
            Else
                Dim added As Boolean = False

                With dt.ParentRelations(0)
                    'find grid to add new view to
                    If grid Is Nothing Then
                        grid = _grids.Find(Function(f) f.Name = "grid" & .ParentTable.TableName)
                    End If
                    If grid IsNot Nothing Then
                        view = New GridView(grid)
                        If .ParentTable.TableName = rootTableName Then
                            grid.LevelTree.Nodes.Add(.RelationName, view)
                            added = True
                        Else
                            added = AddViewToParentNode(grid.LevelTree.Nodes, view, .ParentTable.TableName, .RelationName)
                        End If
                        If added Then
                            For Each repColumn As ReportColumn In repTable.Columns
                                view.Columns.AddField(repColumn.Name).VisibleIndex = -1
                            Next
                            SetupView(view, repTable)
                            SetupColumns(view, repTable)
                            SetupGrouping(view, repTable)
                            SetupSummaries(view, repTable)
                        End If
                    End If
                End With
            End If
        Next
    End Sub

    Private Function AddViewToParentNode(ByRef LevelNodes As GridLevelNodeCollection, ByRef ViewToAdd As GridView, ByVal ParentName As String, ByVal RelationName As String) As Boolean
        Dim added As Boolean

        For Each nextNode As GridLevelNode In LevelNodes
            If String.Compare(nextNode.RelationName, ParentName, True) = 0 Then
                nextNode.Nodes.Add(RelationName, ViewToAdd)
                added = True
                Exit For
            End If
        Next
        If Not added Then
            For Each nextNode As GridLevelNode In LevelNodes
                added = AddViewToParentNode(nextNode.Nodes, ViewToAdd, ParentName, RelationName)
                If added Then
                    Exit For
                End If
            Next
        End If
        AddViewToParentNode = added
    End Function

End Class