Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.System
Imports Reporting
Imports System.Drawing

Public Class StockEnquiryHost
    Private _grid(,) As Boolean
    Private _gridColWidth As Decimal = 100
    Private _gridRowHeight As Decimal = 100
    Private _skuNumber As String

    Public ReadOnly Property SkuNumber() As String
        Get
            Return _skuNumber
        End Get
    End Property

    Public Sub New(ByVal skuNumber As String, ByVal userId As Integer, ByVal workstationId As String, Optional ByVal showAccept As Boolean = False)
        InitializeComponent()
        _skuNumber = skuNumber
        btnAccept.Visible = showAccept

        'get stock from table and set screen labels
        Dim stockItem As Stock.Stock = Stock.Stock.GetStock(skuNumber)
        lblSku.Text = stockItem.SkuNumber & Space(1) & stockItem.Description
        cmbEan.DataSource = stockItem.StockEans
        cmbEan.DisplayMember = GetPropertyName(Function(f As Stock.StockEan) f.Number)
        cmbEan.ValueMember = GetPropertyName(Function(f As Stock.StockEan) f.Number)
        lblSupplierRef.Text = stockItem.ProductCode

        'get grid widths and heights from parameters (ids=5001 & 5002)
        Dim numberColumns As Integer = Parameter.GetInteger(5001)
        Dim numberRows As Integer = Parameter.GetInteger(5002)
        _gridColWidth = Parameter.GetDecimal(5001)
        _gridRowHeight = Parameter.GetDecimal(5002)

        ReDim _grid(numberRows - 1, numberColumns - 1)

        'get report ids to show from parameters (id=5000)
        Dim repIds() As String = Parameter.GetString(5000).Split(","c)
        For Each repId As Integer In repIds
            Dim repControl As New ReportControl(repId, New Object() {skuNumber}, userId, CInt(workstationId))
            repControl.LoadData()

            If Not (repControl.Report.HideWhenNoData AndAlso repControl.HasData = False) Then
                pnlReports.Controls.Add(repControl)
            End If
        Next

    End Sub


    Private Sub Details_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F10 Then
            e.Handled = True
            btnExit.PerformClick()
        End If
    End Sub

    Private Sub pnlReports_ControlAdded(ByVal sender As Object, ByVal e As System.Windows.Forms.ControlEventArgs) Handles pnlReports.ControlAdded

        'get control added
        Dim rep As ReportControl = CType(e.Control, ReportControl)
        Dim gridColumnCount As Integer = _grid.GetUpperBound(1) + 1
        Dim gridRowCount As Integer = _grid.GetUpperBound(0) + 1

        'get next grid section to fill and check that report fits in it
        For rowIndex As Integer = 0 To gridRowCount - 1
            For colIndex As Integer = 0 To gridColumnCount - 1

                'check that this grid section is empty
                If _grid(rowIndex, colIndex) = True Then Continue For

                'check how many grid sections is long and tall
                Dim repNumberGridsWidth As Integer = CInt(Math.Ceiling(rep.MinimumSize.Width / _gridColWidth))
                Dim repNumberGridsHeight As Integer = CInt(Math.Ceiling(rep.MinimumSize.Height / _gridRowHeight))

                'check that report does not go over max number grid columns
                If colIndex + repNumberGridsWidth > gridColumnCount Then Continue For

                'check that there is nothing underneath proposed grid sections
                Dim okToAdd As Boolean = True
                For rowCheck As Integer = colIndex To colIndex + (repNumberGridsWidth - 1)
                    If _grid(rowIndex, colIndex) = True Then okToAdd = False
                Next
                If okToAdd = False Then Continue For

                'resize report control to required size and re-position
                rep.Size = New Size(CInt(_gridColWidth * repNumberGridsWidth), CInt(_gridRowHeight * repNumberGridsHeight))
                rep.Location = New Point(CInt(_gridColWidth * colIndex), CInt(_gridRowHeight * rowIndex))

                'set grid flags to true
                For colcheck As Integer = colIndex To colIndex + (repNumberGridsWidth - 1)
                    For rowCheck As Integer = rowIndex To rowIndex + (repNumberGridsHeight - 1)
                        _grid(rowCheck, colcheck) = True
                    Next
                Next

                Exit Sub
            Next
        Next

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

End Class