﻿Imports Cts.Oasys.Core.System
Imports Cts.Oasys.Core.Reporting
Imports System.Drawing
Imports DevExpress.XtraGrid
Imports System.Text.RegularExpressions

Public Class ReportViewer
    Private _repControls As New List(Of ReportControl)
    Private _fromNightRoutine As Boolean
    Private _grid(,) As Boolean
    Private _gridColWidth As Decimal = 100
    Private _gridRowHeight As Decimal = 100

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()

        'instantiate new report control for each report id in run parameters
        Dim runDate As Date
        Dim runDateIsSpecified As Boolean = False

        If RunParameters.Trim.Length > 0 Then
            'check for from open close
            If RunParameters.Contains("(/P=',CFC')") Then
                _fromNightRoutine = True
                RunParameters = RunParameters.Replace("(/P=',CFC')", "")
            End If

            Dim match As Match = Regex.Match(RunParameters, "--date=([0-9\/\-]+)?", RegularExpressions.RegexOptions.IgnoreCase)
            If match.Success Then
                runDate = Date.Parse(match.Groups(1).Value)
                runDateIsSpecified = True
                RunParameters = RunParameters.Replace(match.Value, "").Trim()
            End If

            For Each id As String In RunParameters.Split(","c)
                Dim rep As Report = Report.GetReport(CInt(id))
                If rep.ShowResetButton Then
                    btnReset.Visible = rep.ShowResetButton
                End If
                Dim repControl As New ReportControl(rep, userId, workstationId)
                _repControls.Add(repControl)
            Next
        End If

        Initialise()

        ' Set date parameter
        If runDateIsSpecified Then
            For Each ctl As Control In cgbSelection.Controls
                If TypeOf ctl Is ReportParameterControl Then
                    Dim rp As ReportParameterControl = CType(ctl, ReportParameterControl)
                    If (rp.ParameterName = "Date" Or rp.ParameterName = "DateStart" Or rp.ParameterName = "DateEnd") Then
                        rp.dtp.Value = runDate
                    End If
                End If
            Next
        End If
    End Sub

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String, ByVal repControl As ReportControl)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
        _repControls.Add(repControl)
        Initialise()
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyData

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Author      : Partha Dutta
            '
            ' Date        : 24/08/2010
            '
            ' Referral No : 231B
            '
            ' Notes       : Enable user to hit enter as well as using F5 to get data
            '
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Case Keys.F5, Keys.Enter : btnRefresh.PerformClick()



            Case Keys.F6 : btnTileCascade.PerformClick()
            Case Keys.F7 : btnVertical.PerformClick()
            Case Keys.F8 : btnWrap.PerformClick()
            Case Keys.F9 : btnPrint.PerformClick()
            Case Keys.F11 : btnReset.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Protected Overrides Sub DoProcessing()

        RefreshData()

        If _fromNightRoutine Then
            PrintQuick()
            Me.FindForm.Close()
        End If

    End Sub



    Private Sub Initialise()

        'get grid widths and heights from parameters (ids=5001 & 5002)
        Dim numberColumns As Integer = Parameter.GetInteger(5001)
        Dim numberRows As Integer = Parameter.GetInteger(5002)
        _gridColWidth = Parameter.GetDecimal(5001)
        _gridRowHeight = Parameter.GetDecimal(5002)

        ReDim _grid(numberRows - 1, numberColumns - 1)

        AddReportParameters()
        AddReportControls()

    End Sub

    Private Sub AddReportParameters()

        For Each repcontrol As ReportControl In _repControls
            'check for parameters and show if not already set or already displayed
            For Each param As ReportParameter In repcontrol.Report.Parameters
                If param.Value Is Nothing Then
                    Dim paramAlreadyPresent As Boolean = False

                    For Each ctl As Control In cgbSelection.Controls
                        If TypeOf ctl Is ReportParameterControl AndAlso CType(ctl, ReportParameterControl).ParameterName = param.Name Then
                            paramAlreadyPresent = True
                            Exit For
                        End If
                    Next

                    If Not paramAlreadyPresent Then
                        Dim paramControl As New ReportParameterControl(param)
                        cgbSelection.Controls.Add(paramControl)
                    End If
                End If
            Next
        Next

        If cgbSelection.Controls.Count > 2 Then
            cgbSelection.Visible = True
            cgbSelection.LayoutControls(CtsControls.CollapsibleGroupBox.Layouts.Horizontal, True)
            btnRefresh.Visible = True

            Dim moveHeight As Integer = cgbSelection.Height + (cgbSelection.Margin.Top * 2)
            pnlSpreads.Top += moveHeight
            pnlSpreads.Height -= moveHeight
        End If

    End Sub

    Private Sub AddReportControls()

        For Each repcontrol As ReportControl In _repControls
            repcontrol.Visible = False
            pnlSpreads.Controls.Add(repcontrol)
        Next

        'check whether to anchor or wrap reports
        Select Case pnlSpreads.Controls.Count
            Case 1
                Dim repControl As ReportControl = CType(pnlSpreads.Controls(0), ReportControl)
                repControl.Locked = True
                AddHandler repControl.btnExit.Click, AddressOf btnExit_Click

                If repControl.MinimumSize.Width > pnlSpreads.Width Then
                    Dim w As Integer = repControl.MinimumSize.Width - Width
                    Me.Width += w + 12
                    Me.Left -= CInt(w / 2)
                End If

                If repControl.MinimumSize.Height > pnlSpreads.Height Then
                    Dim h As Integer = repControl.MinimumSize.Height - Height
                    Me.Height += h + 12
                    Me.Top -= CInt(h / 2)
                End If

                pnlSpreads.Controls(0).Dock = DockStyle.Fill

            Case Is > 1
                ReportWrap()

        End Select

    End Sub



    Public Sub RefreshData() Handles btnRefresh.Click

        Try
            Cursor = Cursors.WaitCursor

            'get parameter values
            Dim dic As New Dictionary(Of String, Object)

            For Each ctl As Control In cgbSelection.Controls
                If TypeOf ctl Is ReportParameterControl Then
                    CType(ctl, ReportParameterControl).GetValue(dic)
                End If
            Next

            For Each repControl As ReportControl In _repControls
                repControl.Visible = True
                repControl.SetParameters(dic)
                repControl.LoadData()
            Next repControl

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Public Sub ReportWrap() Handles btnWrap.Click

        'reset grid
        Dim gridColumnCount As Integer = _grid.GetUpperBound(1) + 1
        Dim gridRowCount As Integer = _grid.GetUpperBound(0) + 1
        Array.Clear(_grid, 0, _grid.GetUpperBound(0) * _grid.GetUpperBound(1))

        'loop over all controls in panel
        For Each ctl As Control In pnlSpreads.Controls
            Dim rep As ReportControl = CType(ctl, ReportControl)
            Dim repAdded As Boolean = False

            'get next grid section to fill and check that report fits in it
            For rowIndex As Integer = 0 To gridRowCount - 1
                If repAdded Then Exit For

                For colIndex As Integer = 0 To gridColumnCount - 1

                    'check that this grid section is empty
                    If repAdded Then Exit For
                    If _grid(rowIndex, colIndex) = True Then Continue For

                    'check how many grid sections is long and tall
                    Dim repNumberGridsWidth As Integer = CInt(Math.Ceiling(rep.MinimumSize.Width / _gridColWidth))
                    Dim repNumberGridsHeight As Integer = CInt(Math.Ceiling(rep.MinimumSize.Height / _gridRowHeight))

                    'check that report does not go over max number grid columns
                    If colIndex + repNumberGridsWidth > gridColumnCount Then Continue For

                    'check that there is nothing underneath proposed grid sections
                    Dim okToAdd As Boolean = True
                    For rowCheck As Integer = colIndex To colIndex + (repNumberGridsWidth - 1)
                        If _grid(rowIndex, colIndex) = True Then okToAdd = False
                    Next
                    If okToAdd = False Then Continue For

                    'resize report control to required size and re-position
                    rep.Size = New Size(CInt(_gridColWidth * repNumberGridsWidth), CInt(_gridRowHeight * repNumberGridsHeight))
                    rep.Location = New Point(CInt(_gridColWidth * colIndex), CInt(_gridRowHeight * rowIndex))

                    'set grid flags to true
                    For colcheck As Integer = colIndex To colIndex + (repNumberGridsWidth - 1)
                        For rowCheck As Integer = rowIndex To rowIndex + (repNumberGridsHeight - 1)
                            _grid(rowCheck, colcheck) = True
                        Next
                    Next

                    repAdded = True
                Next
            Next
        Next

        Select Case pnlSpreads.Controls.Count
            Case 1
                btnRefresh.Visible = True
                pnlSpreads.Visible = True
            Case Else
                pnlButtonWrap.Visible = True
        End Select

    End Sub

    Public Sub PrintQuick()

        Select Case pnlSpreads.Controls.Count
            Case 1
                CType(pnlSpreads.Controls(0), ReportControl).PrintQuick()
            Case Is > 1
                'get all grids in panel
                Dim reps As New List(Of ReportControl)
                For Each ctl As Control In pnlSpreads.Controls
                    reps.Add(CType(ctl, ReportControl))
                Next
                Dim rep As New ReportPrints(reps, UserId, WorkstationId)
                rep.Print()
        End Select

    End Sub


    Private Sub btnTileCascade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTileCascade.Click

        Dim p As Integer = 0
        For Each ctl As Control In pnlSpreads.Controls
            ctl.Location = New Point(p, p)
            ctl.BringToFront()
            p += 50
        Next

    End Sub

    Private Sub btnVertical_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVertical.Click

        Dim p As Integer = 0
        For Each ctl As Control In pnlSpreads.Controls
            ctl.Location = New Point(0, p)
            p += ctl.Height
        Next

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Select Case pnlSpreads.Controls.Count
            Case 1
                CType(pnlSpreads.Controls(0), ReportControl).Print()
            Case Is > 1
                'get all grids in panel
                Dim reps As New List(Of ReportControl)
                For Each ctl As Control In pnlSpreads.Controls
                    reps.Add(CType(ctl, ReportControl))
                Next
                Dim rep As New ReportPrints(reps, UserId, WorkstationId)
                rep.ShowPreviewDialog()
        End Select

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        FindForm.Close()
    End Sub

    Private Sub ReportViewer_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize

    End Sub

    Private Sub btnReset_Click(sender As System.Object, e As System.EventArgs) Handles btnReset.Click
        ResetParameters()
        RefreshData()
    End Sub

    Private Sub ResetParameters()
        For Each ctl As Control In cgbSelection.Controls
            If TypeOf ctl Is ReportParameterControl Then
                CType(ctl, ReportParameterControl).ResetValues()
            End If
        Next
    End Sub
End Class