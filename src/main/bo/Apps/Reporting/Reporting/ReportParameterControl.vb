﻿Imports Cts.Oasys.Core.Reporting
Imports System.Data

Public Class ReportParameterControl
    Private _parameterName As String
    Private _parameterType As SqlDbType

    Public ReadOnly Property ParameterName() As String
        Get
            Return _parameterName
        End Get
    End Property

    Sub New(ByRef parameter As ReportParameter)
        InitializeComponent()
        _parameterType = parameter.DataType
        _parameterName = parameter.Name

        'set up text label and operator dropdown
        lblName.Text = parameter.Description

        If parameter.LookupValuesProcedure IsNot Nothing Then
            Dim dt As DataTable = parameter.GetLookupValues

            If parameter.AllowMultiple Then
                cmbChecked.Visible = True
                cmbChecked.Properties.SeparatorChar = ","c
                cmbChecked.Properties.DataSource = dt
                cmbChecked.Properties.ValueMember = dt.Columns(0).ColumnName
                cmbChecked.Properties.DisplayMember = dt.Columns(1).ColumnName

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ' Author      : Partha Dutta
                ' Date        : 17/09/2010
                ' Referral No : 355
                ' Notes       : Allow one or more items in a checked combo box to pre-selected
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                If parameter.DefaultValue IsNot Nothing Then cmbChecked.SetEditValue(parameter.DefaultValue)

            Else
                cmb.Visible = True
                cmb.ValueMember = dt.Columns(0).ColumnName
                cmb.DisplayMember = dt.Columns(1).ColumnName
                cmb.DataSource = dt
                If parameter.DefaultValue IsNot Nothing Then cmb.SelectedValue = parameter.DefaultValue
            End If

        Else
            Select Case parameter.DataType
                Case Data.SqlDbType.Date
                    dtp.Visible = True
                    If parameter.DefaultValue IsNot Nothing Then
                        Select Case True
                            Case IsNumeric(parameter.DefaultValue)
                                dtp.Value = Now.Date.AddDays(CDbl(parameter.DefaultValue)).Date
                            Case parameter.DefaultValue.ToString.ToLower = "startmonth"
                                dtp.Value = Now.Date.AddDays(-Now.Day + 1).Date
                            Case parameter.DefaultValue.ToString.ToLower = "endmonth"
                                dtp.Value = Now.Date.AddDays(-Now.Day + Date.DaysInMonth(Now.Year, Now.Month)).Date
                        End Select
                    End If

                Case Data.SqlDbType.Char
                    txt.Visible = True
                    txt.Properties.MaxLength = parameter.Size
                    If parameter.Mask.Trim.Length > 0 Then
                        txt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
                        txt.Properties.Mask.EditMask = parameter.Mask
                        txt.Properties.Mask.ShowPlaceHolders = False
                    End If
                    If parameter.DefaultValue IsNot Nothing Then txt.EditValue = parameter.DefaultValue

                Case SqlDbType.Int, SqlDbType.Decimal
                    txt.Visible = True
                    txt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                    If parameter.Mask.Trim.Length > 0 Then
                        txt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
                        txt.Properties.Mask.EditMask = parameter.Mask
                        txt.Properties.Mask.ShowPlaceHolders = False
                    End If
                    If parameter.DefaultValue IsNot Nothing Then txt.EditValue = parameter.DefaultValue

                Case Data.SqlDbType.Bit
                    chk.Visible = True
                    If parameter.DefaultValue IsNot Nothing Then chk.Checked = CBool(parameter.DefaultValue)

            End Select

        End If

    End Sub

    Public Sub GetValue(ByRef dic As Dictionary(Of String, Object))

        Select Case True
            Case cmb.Visible : dic.Add(_parameterName, cmb.SelectedValue)
            Case dtp.Visible : dic.Add(_parameterName, dtp.Value.Date)
            Case txt.Visible
                Select Case _parameterType
                    Case SqlDbType.Int, SqlDbType.Decimal
                        If IsNumeric(txt.EditValue) Then
                            dic.Add(_parameterName, txt.EditValue)
                        End If
                    Case Else
                        dic.Add(_parameterName, txt.EditValue)
                End Select
            Case chk.Visible : dic.Add(_parameterName, chk.Checked)
            Case cmbChecked.Visible : dic.Add(_parameterName, cmbChecked.EditValue)
        End Select

    End Sub

    Public Sub ResetValues()

        Select Case True
            Case dtp.Visible : dtp.Value = Date.Now
            Case cmb.Visible : cmb.SelectedIndex = 0
            Case txt.Visible : txt.EditValue = Nothing
            Case chk.Visible : chk.Checked = False
            Case cmbChecked.Visible : cmbChecked.EditValue = Nothing    'Sorry, we have no report with this type of parameter. Please, check the correctness of this line and use it carefully
        End Select

    End Sub

End Class
