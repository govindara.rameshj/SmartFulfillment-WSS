﻿
Friend Class ViewColTag

    Private _properties As Dictionary(Of String, Object)
    Private _propertyFlagNames As List(Of String)

    Private Const PROPERTYNAME_COLOUR As String = "Colour"
    Private Const PROPERTYNAME_FONTSIZE As String = "Fontsize"

    Private Const PROPERTYDELIMITER_PROPERTIES As Char = ";"c
    Private Const PROPERTYDELIMITER_NAMEVALUE As Char = ":"c

    Private Const TAGPROPERTYSPLITINDEX_NAME As Integer = 0
    Private Const TAGPROPERTYSPLITINDEX_VALUE As Integer = 1

    Private Const PROPERTYFLAGNAME_HIGHLIGHTTHIS As String = "HighlightThis"
    Private Const PROPERTYFLAGNAME_RESIZETHIS As String = "ResizeThis"
    Private Const PROPERTYFLAGNAME_OVERRIDECOLOUR As String = "OverrideColour"
    Private Const PROPERTYFLAGNAME_EMBOLDENTHIS As String = "EmboldenThis"

    Public Sub New()

        InitialiseProperties()
    End Sub

    Public Sub New(ByVal TagValue As Object)

        Me.New()
        If TagValue IsNot Nothing Then
            Dim Tag As String = ""
            Dim Properties() As String = {}
            Dim PropertyNameValue() As String = {}

            Tag = TagValue.ToString
            Properties = Tag.Split(PROPERTYDELIMITER_PROPERTIES)
            For Each TagProperty As String In Properties
                PropertyNameValue = TagProperty.Split(PROPERTYDELIMITER_NAMEVALUE)
                If String.Compare(PropertyNameValue(TAGPROPERTYSPLITINDEX_NAME), PROPERTYNAME_COLOUR, True) = 0 Then
                    Dim ColourValue As Integer = 0

                    If Integer.TryParse(PropertyNameValue(TAGPROPERTYSPLITINDEX_VALUE), ColourValue) Then
                        _properties.Add(PROPERTYNAME_COLOUR, ColourValue)
                    End If
                ElseIf String.Compare(PropertyNameValue(TAGPROPERTYSPLITINDEX_NAME), PROPERTYNAME_FONTSIZE, True) = 0 Then
                    Dim FontsizeValue As Single = 0

                    If Single.TryParse(PropertyNameValue(TAGPROPERTYSPLITINDEX_VALUE), FontsizeValue) Then
                        _properties.Add(PROPERTYNAME_FONTSIZE, FontsizeValue)
                    End If
                End If
            Next
        End If
    End Sub

    Public Sub SafeAddColour(ByVal ColourValue As Integer)

        If _properties.ContainsKey(PROPERTYNAME_COLOUR) Then
            _properties.Remove(PROPERTYNAME_COLOUR)
        End If
        _properties.Add(PROPERTYNAME_COLOUR, ColourValue)
    End Sub

    Public Sub SafeAddFontsize(ByVal FontsizeValue As Single)

        If _properties.ContainsKey(PROPERTYNAME_FONTSIZE) Then
            _properties.Remove(PROPERTYNAME_FONTSIZE)
        End If
        _properties.Add(PROPERTYNAME_FONTSIZE, FontsizeValue)
    End Sub

    Public Function TryGetColour(ByRef ToGet As Integer) As Boolean

        TryGetColour = False
        If _properties.ContainsKey(PROPERTYNAME_COLOUR) Then
            Dim ExtractValue As Integer = 0

            If Integer.TryParse(CStr(_properties.Item(PROPERTYNAME_COLOUR)), ExtractValue) Then
                ToGet = ExtractValue
                TryGetColour = True
            End If
        End If
    End Function

    Public Function TryGetFontsize(ByRef ToGet As Single) As Boolean

        TryGetFontsize = False
        If _properties.ContainsKey(PROPERTYNAME_FONTSIZE) Then
            Dim ExtractValue As Single = 0

            If Single.TryParse(CStr(_properties.Item(PROPERTYNAME_FONTSIZE)), ExtractValue) Then
                ToGet = ExtractValue
                TryGetFontsize = True
            End If
        End If
    End Function

    Public Function GetTag() As Object

        GetTag = Nothing
        If _properties.Count > 0 Then
            Dim TagValue As String = ""

            If _properties.ContainsKey(PROPERTYNAME_COLOUR) Then
                TagValue = PROPERTYNAME_COLOUR & PROPERTYDELIMITER_NAMEVALUE & _properties.Item(PROPERTYNAME_COLOUR).ToString
            End If
            If _properties.ContainsKey(PROPERTYNAME_FONTSIZE) Then
                If TagValue.Length > 0 Then
                    TagValue &= PROPERTYDELIMITER_PROPERTIES
                End If
                TagValue &= PROPERTYNAME_FONTSIZE & PROPERTYDELIMITER_NAMEVALUE & _properties.Item(PROPERTYNAME_FONTSIZE).ToString
            End If
            GetTag = TagValue
        End If
    End Function

    Public ReadOnly Property PropertyFlagColumnNameHighlightThis() As String
        Get
            Return PROPERTYFLAGNAME_HIGHLIGHTTHIS
        End Get
    End Property

    Public ReadOnly Property PropertyFlagColumnNameResizeThis() As String
        Get
            Return PROPERTYFLAGNAME_RESIZETHIS
        End Get
    End Property

    Public ReadOnly Property PropertyFlagColumnNameOverideColour() As String
        Get
            Return PROPERTYFLAGNAME_OVERRIDECOLOUR
        End Get
    End Property

    Public ReadOnly Property PropertyFlagColumnNameEmboldenThis() As String
        Get
            Return PROPERTYFLAGNAME_EMBOLDENTHIS
        End Get
    End Property

    Public Shared Function IsAdditionalPropertyFlagColumn(ByVal ColumnName As String) As Boolean
        Dim PotentialColumnNamePrefix As String = "col"

        IsAdditionalPropertyFlagColumn = False
        If Not String.IsNullOrEmpty(ColumnName) Then
            If ColumnName.StartsWith(PotentialColumnNamePrefix, StringComparison.CurrentCultureIgnoreCase) Then
                ColumnName = ColumnName.Substring(PotentialColumnNamePrefix.Length)
            End If
            If String.Compare(PROPERTYFLAGNAME_HIGHLIGHTTHIS, ColumnName, True) = 0 _
            Or String.Compare(PROPERTYFLAGNAME_RESIZETHIS, ColumnName, True) = 0 _
            Or String.Compare(PROPERTYFLAGNAME_OVERRIDECOLOUR, ColumnName, True) = 0 _
            Or String.Compare(PROPERTYFLAGNAME_EMBOLDENTHIS, ColumnName, True) = 0 Then
                IsAdditionalPropertyFlagColumn = True
            End If
        End If
    End Function

    Friend Sub InitialiseProperties()

        _properties = New Dictionary(Of String, Object)
    End Sub
End Class
