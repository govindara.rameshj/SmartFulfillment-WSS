﻿Imports Cts.Oasys.Core.Reporting
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraEditors.Controls
Imports System.Drawing

Public Class ReportControlLive
    Implements IReportControl

    Private Sub SetupColumns(ByRef view As GridView, ByVal repTable As ReportTable, ByRef pnlGrids As System.Windows.Forms.Panel) Implements IReportControl.SetupColumns
        'instantiate image repository item
        Dim ri As New RepositoryItemPictureEdit
        ri.SizeMode = PictureSizeMode.Zoom

        For Each repColumn As ReportColumn In repTable.Columns
            'check that column exists first and just ignore if not present
            Dim viewCol As GridColumn = view.Columns(repColumn.Name)
            If viewCol Is Nothing Then Continue For

            'set up column properties
            viewCol.Caption = repColumn.Caption
            viewCol.AppearanceCell.TextOptions.HAlignment = repColumn.Alignment
            viewCol.Visible = repColumn.IsVisible

            'check if image path column
            If repColumn.IsImagePath Then
                Dim newCol As New GridColumn
                newCol.ColumnEdit = ri
                newCol.Tag = repColumn.Name
                newCol.FieldName = "Image" & repTable.Columns.IndexOf(repColumn)
                newCol.VisibleIndex = repTable.Columns.IndexOf(repColumn)
                newCol.UnboundType = DevExpress.Data.UnboundColumnType.Object
                view.Columns.Add(newCol)
                If repTable.RowHeight = -1 Then view.RowHeight = view.GridControl.Height

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ' Author      : Partha Dutta
                '
                ' Date        : 10/08/2010
                '
                ' Referral No : 136
                '
                ' Notes       : Do not fully understand the DevExpreess suite of controls inc the "grid" control
                '               I am not sure if this processing which involves images is called by other parts of the application
                '
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                newCol.Width = pnlGrids.Width
                view.RowHeight = pnlGrids.Height

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ' Finish
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            End If

            If repColumn.MinWidth > 0 Then viewCol.MinWidth = repColumn.MinWidth
            If repColumn.MaxWidth > 0 Then viewCol.MaxWidth = repColumn.MaxWidth

            'set up column format
            If repColumn.FormatType > 0 AndAlso repColumn.Format.Trim.Length > 0 Then
                viewCol.DisplayFormat.FormatType = repColumn.FormatType
                viewCol.DisplayFormat.FormatString = repColumn.Format
            End If



            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Author      : Partha Dutta
            '
            ' Date        : 24/08/2010
            '
            ' Referral No : 243B
            '
            ' Notes       : Do not know why the "date" columns need to be right justified, use report engine setting instead
            '
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            ''right align if date
            'If viewCol.ColumnType Is GetType(Date) Then
            '    viewCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            'End If


            'column font
            If repColumn.IsBold Then
                viewCol.AppearanceCell.Font = New Font(viewCol.AppearanceCell.Font, FontStyle.Bold)
            End If
        Next

    End Sub

    Private Sub view_CustomRowCellEdit(ByVal sender As Object, ByVal e As CustomRowCellEditEventArgs, ByVal repTable As ReportTable) Implements IReportControl.view_CustomRowCellEdit
        'get view and report table in question
        Dim view As GridView = sender

        'check there is a RowId column first exiting if not found
        Dim rowIdColName As String = repTable.Columns.ColumnName(0)
        If rowIdColName Is Nothing Then Exit Sub

        'get rowId for this cell's row exiting if is null
        Dim rowIdValue As Object = view.GetRowCellValue(e.RowHandle, rowIdColName)
        If rowIdValue Is Nothing OrElse IsDBNull(rowIdValue) Then Exit Sub

        'loop over all reportHyperlinks for this rowId value
        For Each repHyperlink As ReportHyperlink In repTable.Hyperlinks.Where(Function(f) f.RowId = rowIdValue)
            'check that this cell's column is that of the hyperlink column and add hyperlink
            If e.Column.FieldName = repHyperlink.ColumnName Then
                Dim riHyperlink As New RepositoryItemHyperLinkEdit
                view.GridControl.RepositoryItems.Add(riHyperlink)
                e.RepositoryItem = riHyperlink
            End If
        Next
    End Sub

    Private Sub view_RowCellStyle(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs, ByVal reportTables As ReportTableCollection) Implements IReportControl.view_RowCellStyle

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author      : Partha Dutta
        '
        ' Date        : 10/08/2010
        '
        ' Referral No : 136
        '
        ' Notes       : Price Change screen, added a report hyperlink at bottom
        '               The flaw in the code is that it just matches on "ColumnName" but not also match on "RowId"
        '               This results in the first row in the grid is being highlighted blue
        '               Enabling an hyperlink turns row blue, so this code is not required
        '               Yes it is, where the first line is a hyperlink it is black, why I do not know
        '               New to the system so keeping old code, just in case
        '               
        '
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        'Old Code

        'Dim view As GridView = sender
        'If e.RowHandle = view.FocusedRowHandle Then
        '    For Each table As ReportTable In _report.Tables
        '        For Each hyperlink As ReportHyperlink In table.Hyperlinks
        '            If e.Column.FieldName = hyperlink.ColumnName Then
        '                e.Appearance.ForeColor = Color.Blue
        '            End If
        '        Next
        '    Next
        'End If

        Dim view As GridView
        Dim strRowIdColumnName As String
        Dim objRowIdValue As Object
        Dim intRowIdValue As Integer

        view = sender
        If e.RowHandle = view.FocusedRowHandle Then
            For Each table As ReportTable In reportTables
                'no columns or not "ROWID"
                strRowIdColumnName = table.Columns.ColumnName(0)
                If strRowIdColumnName Is Nothing Then Exit For
                If strRowIdColumnName.ToUpper <> "ROWID" Then Exit For

                'row id value nothing or null
                objRowIdValue = view.GetRowCellValue(e.RowHandle, strRowIdColumnName)
                If objRowIdValue Is Nothing OrElse IsDBNull(objRowIdValue) Then Exit Sub

                intRowIdValue = CType(objRowIdValue, Integer)
                For Each hyperlink As ReportHyperlink In table.Hyperlinks.Where(Function(f) f.RowId = objRowIdValue)

                    If e.Column.FieldName = hyperlink.ColumnName Then
                        e.Appearance.ForeColor = Color.Blue
                    End If
                Next
            Next
        End If

    End Sub
End Class
