﻿Imports Cts.Oasys.Core.Reporting
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraEditors.Controls
Imports System.Drawing

Public Class ReportControlAdditionalProperties
    Implements IReportControl

    Public Sub SetupColumns(ByRef view As GridView, ByVal repTable As ReportTable, ByRef pnlGrids As System.Windows.Forms.Panel) Implements IReportControl.SetupColumns
        'instantiate image repository item
        Dim ri As New RepositoryItemPictureEdit
        ri.SizeMode = PictureSizeMode.Zoom

        For Each repColumn As IReportColumnLive In repTable.Columns
            'check that column exists first and just ignore if not present
            Dim viewCol As GridColumn = view.Columns(repColumn.Name)

            If viewCol IsNot Nothing Then
                Dim repColumnWithAdditionalProperties As IReportColumn = Nothing

                If TypeOf repColumn Is IReportColumn Then
                    repColumnWithAdditionalProperties = CType(repColumn, IReportColumn)
                End If

                'set up column properties
                viewCol.Caption = repColumn.Caption
                viewCol.AppearanceCell.TextOptions.HAlignment = CType(repColumn.Alignment, DevExpress.Utils.HorzAlignment)
                viewCol.Visible = repColumn.IsVisible

                'check if image path column
                If repColumn.IsImagePath Then
                    Dim newCol As New GridColumn
                    newCol.ColumnEdit = ri
                    newCol.Tag = repColumn.Name
                    newCol.FieldName = "Image" & repTable.Columns.IndexOf(CType(repColumn, ReportColumn))
                    newCol.VisibleIndex = repTable.Columns.IndexOf(CType(repColumn, ReportColumn))
                    newCol.UnboundType = DevExpress.Data.UnboundColumnType.Object
                    view.Columns.Add(newCol)
                    If repTable.RowHeight = -1 Then
                        view.RowHeight = view.GridControl.Height
                    End If
                    newCol.Width = pnlGrids.Width
                    view.RowHeight = pnlGrids.Height
                End If

                If repColumn.MinWidth > 0 Then
                    viewCol.MinWidth = repColumn.MinWidth
                End If

                If repColumn.MaxWidth > 0 Then
                    viewCol.MaxWidth = repColumn.MaxWidth
                End If

                'set up column format
                If repColumn.FormatType > 0 AndAlso repColumn.Format.Trim.Length > 0 Then
                    viewCol.DisplayFormat.FormatType = CType(repColumn.FormatType, DevExpress.Utils.FormatType)
                    viewCol.DisplayFormat.FormatString = repColumn.Format
                End If

                'column font...
                If repColumn.IsBold Then
                    viewCol.AppearanceCell.Font = New Font(viewCol.AppearanceCell.Font, FontStyle.Bold)
                End If

                ' ... and additional properties
                If repColumnWithAdditionalProperties IsNot Nothing Then
                    With repColumnWithAdditionalProperties
                        Dim AdditionalProperties As New ViewColTag

                        If .Fontsize > 0 Then
                            AdditionalProperties.SafeAddFontsize(.Fontsize)
                        End If
                        If .IsHighlight Then
                            AdditionalProperties.SafeAddColour(.HighlightColour)
                        End If
                        viewCol.Tag = AdditionalProperties.GetTag
                    End With
                End If
            End If
            For Each nextCol As GridColumn In view.Columns
                With nextCol
                    If CBool(ViewColTag.IsAdditionalPropertyFlagColumn(.Name)) Then
                        .Visible = False
                    End If
                End With
            Next
        Next
    End Sub

    Public Sub view_CustomRowCellEdit(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs, ByVal repTable As ReportTable) Implements IReportControl.view_CustomRowCellEdit
        'get view and report table in question
        Dim view As GridView = CType(sender, GridView)
        'check there is a RowId column first exiting if not found
        Dim rowIdColName As String = repTable.Columns.ColumnName(0)

        If rowIdColName IsNot Nothing Then
            'get rowId for this cell's row exiting if is null
            Dim rowIdValue As Object = view.GetRowCellValue(e.RowHandle, rowIdColName)

            If rowIdValue IsNot Nothing _
            AndAlso Not IsDBNull(rowIdValue) Then
                'loop over all reportHyperlinks for this rowId value
                For Each repHyperlink As ReportHyperlink In repTable.Hyperlinks.Where(Function(f) f.RowId = CInt(rowIdValue))
                    'check that this cell's column is that of the hyperlink column and add hyperlink
                    If e.Column.FieldName = repHyperlink.ColumnName Then
                        Dim riHyperlink As New RepositoryItemHyperLinkEdit
                        Dim AdditionalProperties As New ViewColTag()
                        Dim HightlightThisValue As Object = view.GetRowCellValue(e.RowHandle, AdditionalProperties.PropertyFlagColumnNameHighlightThis)
                        Dim HighlightColour As Color
                        Dim ResizeThisValue As Object = view.GetRowCellValue(e.RowHandle, AdditionalProperties.PropertyFlagColumnNameResizeThis)
                        Dim EmboldenThisValue As Object = view.GetRowCellValue(e.RowHandle, AdditionalProperties.PropertyFlagColumnNameEmboldenThis)
                        Dim OverrideColourValue As Object = view.GetRowCellValue(e.RowHandle, AdditionalProperties.PropertyFlagColumnNameOverideColour)

                        HighlightColour = Color.Blue
                        ' Additional properties
                        If TagHasAdditionalProperties(e.Column.Tag, e.Column.FieldName) Then
                            ' Highlight colour
                            HighlightColour = SetHighlightColour(HightlightThisValue, e.Column.Tag, riHyperlink.LinkColor)
                            ' new font 
                            riHyperlink.Appearance.Font = SetFont(ResizeThisValue, e.Column.Tag, riHyperlink.Appearance.Font, EmboldenThisValue)
                        End If
                        ' Override Colour
                        HighlightColour = GetOverrideColour(OverrideColourValue, HighlightColour)

                        riHyperlink.LinkColor = HighlightColour
                        view.GridControl.RepositoryItems.Add(riHyperlink)
                        e.RepositoryItem = riHyperlink
                    End If
                Next
            End If
        End If
    End Sub

    Public Sub CheckAndSetBoldAttribute(ByVal e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs, ByVal EmboldenThisValue As Object)
        Dim ApplyBold As Boolean = False
        Boolean.TryParse(CStr(EmboldenThisValue), ApplyBold)
        If ApplyBold Then
            e.Appearance.Font = SetBoldAttribute(e.Column.Tag, e.Appearance.Font, EmboldenThisValue)
        End If
    End Sub

    Private Sub SetFontAndHighlightColour(ByVal e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs, ByVal HightlightThisValue As Object, ByVal ResizeThisValue As Object, ByVal EmboldenThisValue As Object, ByRef HighlightColour As Color)
        If TagHasAdditionalProperties(e.Column.Tag, e.Column.FieldName) Then
            ' Highlight colour
            HighlightColour = SetHighlightColour(HightlightThisValue, e.Column.Tag, HighlightColour)

            ' new font
            e.Appearance.Font = SetFont(ResizeThisValue, e.Column.Tag, e.Appearance.Font, EmboldenThisValue)
        End If
    End Sub

    Private Shared Function SetHyperLink(ByVal e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs, ByVal objRowIdValue As Object, ByVal table As ReportTable, ByRef HighlightColour As Color) As Integer
        Dim intRowIdValue As Integer
        intRowIdValue = CType(objRowIdValue, Integer)
        'loop over all reportHyperlinks for this rowId value
        For Each repHyperlink As ReportHyperlink In table.Hyperlinks.Where(Function(f) f.RowId = intRowIdValue)
            'check that this cell's column is that of the hyperlink column and add hyperlink
            If e.Column.FieldName = repHyperlink.ColumnName Then
                HighlightColour = Color.Blue
            End If
        Next
        Return intRowIdValue
    End Function

    Public Sub view_RowCellStyle(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs, ByVal ReportTables As ReportTableCollection) Implements IReportControl.view_RowCellStyle
        Dim view As GridView
        Dim strRowIdColumnName As String
        Dim objRowIdValue As Object
        Dim intRowIdValue As Integer

        view = CType(sender, GridView)
        For Each table As ReportTable In ReportTables
            'no columns or not "ROWID"
            strRowIdColumnName = table.Columns.ColumnName(0)
            If strRowIdColumnName IsNot Nothing _
            AndAlso String.Compare(strRowIdColumnName, "RowId", True) = 0 Then
                ' only proceed if have a row id
                objRowIdValue = view.GetRowCellValue(e.RowHandle, strRowIdColumnName)
                If objRowIdValue IsNot Nothing _
                AndAlso Not IsDBNull(objRowIdValue) Then
                    Dim AdditionalProperties As New ViewColTag()
                    Dim HightlightThisValue As Object = view.GetRowCellValue(e.RowHandle, AdditionalProperties.PropertyFlagColumnNameHighlightThis)
                    Dim ResizeThisValue As Object = view.GetRowCellValue(e.RowHandle, AdditionalProperties.PropertyFlagColumnNameResizeThis)
                    Dim OverrideColourValue As Object = view.GetRowCellValue(e.RowHandle, AdditionalProperties.PropertyFlagColumnNameOverideColour)
                    Dim EmboldenThisValue As Object = view.GetRowCellValue(e.RowHandle, AdditionalProperties.PropertyFlagColumnNameEmboldenThis)
                    Dim HighlightColour As Color = e.Appearance.ForeColor

                    'Does cell need to be bold
                    If EmboldenThisValue IsNot Nothing Then
                        CheckAndSetBoldAttribute(e, EmboldenThisValue)
                    End If

                    ' Hyperlinks want to be blue by default
                    intRowIdValue = SetHyperLink(e, objRowIdValue, table, HighlightColour)

                    ' Additional properties
                    SetFontAndHighlightColour(e, HightlightThisValue, ResizeThisValue, EmboldenThisValue, HighlightColour)

                    ' Override Colour
                    HighlightColour = GetOverrideColour(OverrideColourValue, HighlightColour)

                    e.Appearance.ForeColor = HighlightColour
                End If
            End If
        Next
    End Sub

    Private Function TagHasAdditionalProperties(ByVal ColumnTag As Object, ByVal ColumnFieldName As String) As Boolean

        TagHasAdditionalProperties = False
        If ColumnTag IsNot Nothing _
        AndAlso ColumnTag.ToString.Length > 0 _
        And Not ColumnFieldName.StartsWith("Image") Then
            TagHasAdditionalProperties = True
        End If
    End Function

    Private Function SetHighlightColour(ByVal HighlightThisValue As Object, ByVal ColumnTag As Object, ByVal DefaultColour As Color) As Color
        Dim ApplyHighlight As Boolean = False
        Dim AdditionalProperties As ViewColTag

        SetHighlightColour = DefaultColour
        If HighlightThisValue IsNot Nothing _
        AndAlso Boolean.TryParse(HighlightThisValue.ToString, ApplyHighlight) _
        AndAlso ApplyHighlight Then
            Dim HighlightColourValue As Integer

            AdditionalProperties = New ViewColTag(ColumnTag)
            If AdditionalProperties.TryGetColour(HighlightColourValue) Then
                SetHighlightColour = Color.FromArgb(HighlightColourValue)
            End If
        End If
    End Function

    Private Function SetFont(ByVal ResizeThisValue As Object, ByVal ColumnTag As Object, ByVal DefaultFont As Font, ByVal EmboldenThisValue As Object) As Font
        Dim ApplyResize As Boolean = False
        Dim AdditionalProperties As ViewColTag

        SetFont = DefaultFont
        If ResizeThisValue IsNot Nothing _
        AndAlso Boolean.TryParse(CStr(ResizeThisValue), ApplyResize) _
        AndAlso ApplyResize Then
            Dim Fontsize As Single
            Dim ApplyBold As Boolean = False

            If EmboldenThisValue IsNot Nothing Then
                Boolean.TryParse(CStr(EmboldenThisValue), ApplyBold)
            End If
            AdditionalProperties = New ViewColTag(ColumnTag)
            If AdditionalProperties.TryGetFontsize(Fontsize) Then
                If ApplyBold Then
                    SetFont = New Font(DefaultFont.FontFamily, Fontsize, FontStyle.Bold)
                Else
                    SetFont = New Font(DefaultFont.FontFamily, Fontsize)
                End If
            Else
                If ApplyBold Then
                    SetFont = New Font(DefaultFont, FontStyle.Bold)
                End If
            End If
        End If
    End Function

    Public Function SetBoldAttribute(ByVal ColumnTag As Object, ByVal DefaultFont As Font, ByVal EmboldenThisValue As Object) As Font Implements IReportControl.SetBoldAttribute
        Dim FontSize As Single

        FontSize = 8.5
        SetBoldAttribute = New Font(DefaultFont.FontFamily, FontSize, FontStyle.Bold)
    End Function

    Private Function GetOverrideColour(ByVal OverrideColour As Object, ByVal DefaultColour As Color) As Color
        Dim OverrideColourValue As Integer = 0

        GetOverrideColour = DefaultColour
        If OverrideColour IsNot Nothing _
        AndAlso Integer.TryParse(OverrideColour.ToString, OverrideColourValue) Then
            GetOverrideColour = Color.FromArgb(OverrideColourValue)
        End If
    End Function
End Class
