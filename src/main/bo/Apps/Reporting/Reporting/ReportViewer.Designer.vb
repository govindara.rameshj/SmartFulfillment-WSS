﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReportViewer
    Inherits Cts.Oasys.WinForm.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReportViewer))
        Me.btnExit = New System.Windows.Forms.Button()
        Me.pnlSpreads = New System.Windows.Forms.Panel()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.cgbSelection = New CtsControls.CollapsibleGroupBox()
        Me.pnlButtonWrap = New System.Windows.Forms.Panel()
        Me.btnWrap = New System.Windows.Forms.Button()
        Me.btnVertical = New System.Windows.Forms.Button()
        Me.btnTileCascade = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.pnlButtonWrap.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnExit.Location = New System.Drawing.Point(821, 508)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 5
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'pnlSpreads
        '
        Me.pnlSpreads.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlSpreads.Location = New System.Drawing.Point(3, 3)
        Me.pnlSpreads.Name = "pnlSpreads"
        Me.pnlSpreads.Size = New System.Drawing.Size(894, 499)
        Me.pnlSpreads.TabIndex = 1
        '
        'btnRefresh
        '
        Me.btnRefresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRefresh.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnRefresh.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnRefresh.Location = New System.Drawing.Point(3, 508)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(76, 39)
        Me.btnRefresh.TabIndex = 2
        Me.btnRefresh.Text = "F5 Retrieve"
        Me.btnRefresh.UseVisualStyleBackColor = True
        Me.btnRefresh.Visible = False
        '
        'cgbSelection
        '
        Me.cgbSelection.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgbSelection.Location = New System.Drawing.Point(3, 3)
        Me.cgbSelection.MinHeight = 18
        Me.cgbSelection.MinWidth = 18
        Me.cgbSelection.Name = "cgbSelection"
        Me.cgbSelection.Size = New System.Drawing.Size(894, 18)
        Me.cgbSelection.Style = CtsControls.CollapsibleGroupBox.Styles.Height
        Me.cgbSelection.TabIndex = 0
        Me.cgbSelection.TabStop = False
        Me.cgbSelection.Text = "Selection Criteria"
        Me.cgbSelection.Visible = False
        '
        'pnlButtonWrap
        '
        Me.pnlButtonWrap.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pnlButtonWrap.Controls.Add(Me.btnWrap)
        Me.pnlButtonWrap.Controls.Add(Me.btnVertical)
        Me.pnlButtonWrap.Controls.Add(Me.btnTileCascade)
        Me.pnlButtonWrap.Location = New System.Drawing.Point(85, 508)
        Me.pnlButtonWrap.Name = "pnlButtonWrap"
        Me.pnlButtonWrap.Size = New System.Drawing.Size(243, 39)
        Me.pnlButtonWrap.TabIndex = 3
        Me.pnlButtonWrap.Visible = False
        '
        'btnWrap
        '
        Me.btnWrap.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnWrap.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnWrap.Location = New System.Drawing.Point(164, 0)
        Me.btnWrap.Name = "btnWrap"
        Me.btnWrap.Size = New System.Drawing.Size(76, 39)
        Me.btnWrap.TabIndex = 2
        Me.btnWrap.Text = "F8 Tile Wrapping"
        Me.btnWrap.UseVisualStyleBackColor = True
        '
        'btnVertical
        '
        Me.btnVertical.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnVertical.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnVertical.Location = New System.Drawing.Point(82, 0)
        Me.btnVertical.Name = "btnVertical"
        Me.btnVertical.Size = New System.Drawing.Size(76, 39)
        Me.btnVertical.TabIndex = 1
        Me.btnVertical.Text = "F7 Tile Vertical"
        Me.btnVertical.UseVisualStyleBackColor = True
        '
        'btnTileCascade
        '
        Me.btnTileCascade.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnTileCascade.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnTileCascade.Location = New System.Drawing.Point(0, 0)
        Me.btnTileCascade.Name = "btnTileCascade"
        Me.btnTileCascade.Size = New System.Drawing.Size(76, 39)
        Me.btnTileCascade.TabIndex = 0
        Me.btnTileCascade.Text = "F6 Tile Cascade"
        Me.btnTileCascade.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnPrint.Location = New System.Drawing.Point(739, 508)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(76, 39)
        Me.btnPrint.TabIndex = 4
        Me.btnPrint.Text = "F9 Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnReset.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnReset.Location = New System.Drawing.Point(657, 508)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(76, 39)
        Me.btnReset.TabIndex = 6
        Me.btnReset.Text = "F11 Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        Me.btnReset.Visible = False
        '
        'ReportViewer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.pnlButtonWrap)
        Me.Controls.Add(Me.cgbSelection)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.pnlSpreads)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ReportViewer"
        Me.Size = New System.Drawing.Size(900, 550)
        Me.pnlButtonWrap.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents pnlSpreads As System.Windows.Forms.Panel
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents cgbSelection As CtsControls.CollapsibleGroupBox
    Friend WithEvents pnlButtonWrap As System.Windows.Forms.Panel
    Friend WithEvents btnWrap As System.Windows.Forms.Button
    Friend WithEvents btnVertical As System.Windows.Forms.Button
    Friend WithEvents btnTileCascade As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents btnReset As System.Windows.Forms.Button

End Class
