﻿Public Class ReportControlUIFactory
    Inherits RequirementSwitchFactory(Of IReportControlUI)

    Public Overrides Function ImplementationA() As IReportControlUI

        Return New ReportControlHyperLink

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(-22040)

    End Function

    Public Overrides Function ImplementationB() As IReportControlUI

        Return New ReportControlHyperLinkExisting

    End Function

End Class