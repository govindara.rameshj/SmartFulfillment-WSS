﻿Public Class ReportControlFactory

    Inherits RequirementSwitchFactory(Of IReportControl)

    Friend Const REQUIREMENTSWITCHP022_002 As Integer = -22002

    Public Overrides Function ImplementationA() As IReportControl

        Return New ReportControlAdditionalProperties
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(REQUIREMENTSWITCHP022_002)
    End Function

    Public Overrides Function ImplementationB() As IReportControl

        Return New ReportControlOld
    End Function

End Class
