﻿' Alanl 8/6/2010
' Referral 150.
' See Sub Print

Imports System.Data.SqlClient

Public Class ForkTruckPrint
    Private dl As New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
    Private log As New BOSecurityProfile.cActivityLog(dl)
    Private retopt As BOSystem.cRetailOptions = New BOSystem.cRetailOptions(dl)
    Private _PrintNotPrinted As Boolean = False
    Private printset As New FarPoint.Win.Spread.PrintInfo()
    Private _CheckWorkstation As Integer = 13
    Private _Proxy As Oasys.DataProxy
    Private _CalledFromNight As Boolean
    Private _StoreNumber As String
    Private _StoreName As String
    Private _ShortAssemblyName As String
    Private _FileVersion As String

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)

        With FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetAssembly(Me.GetType).Location)
            _ShortAssemblyName = .FileName.Substring(.FileName.LastIndexOf("\"c) + 1, .FileName.LastIndexOf("."c) - .FileName.LastIndexOf("\"c) - 1)
            _FileVersion = .FileVersion
        End With

        MyTrace("Initialising")
        InitializeComponent()
        initializeScreen()
    End Sub
    Private Sub initializeScreen()
        Dim dsStartDate As DataSet
        Dim dsEnddate As DataSet

        log.AddAggregateField(OasysDBBO.Oasys3.DB.clsOasys3DB.eAggregates.pAggMin, log.LogDate, "MinDate")
        dsStartDate = log.GetAggregateDataSet()
        dtpStartDate.Value = CDate(dsStartDate.Tables(0).Rows(0).Item("MinDate"))

        log.AddAggregateField(OasysDBBO.Oasys3.DB.clsOasys3DB.eAggregates.pAggMax, log.LogDate, "MaxDate")
        dsEnddate = log.GetAggregateDataSet()
        dtpEndDate.Value = CDate(dsEnddate.Tables(0).Rows(0).Item("MaxDate"))

        ' Add any initialization after the InitializeComponent() call.
        Dim db As New OasysDBBO.Oasys3.DB.clsOasys3DB()
        _Proxy = CType(Activator.CreateInstance(db.GetConfigValue("ProxyAssembly"), db.GetConfigValue("ProxyTypeName"), False, Reflection.BindingFlags.CreateInstance, Nothing, New Object() {db.GetConfigValue("sqlConnection")}, Globalization.CultureInfo.InvariantCulture, Nothing, Nothing).Unwrap, Oasys.DataProxy)

        With _Proxy.ExecuteDataTable(New SqlCommand("SELECT STOR, SNAM FROM RETOPT WHERE FKEY='01'")).Rows(0)
            _StoreNumber = CStr(.Item("STOR"))
            _StoreName = CStr(.Item("SNAM"))
        End With

        _CalledFromNight = RunParameters.Contains(",CFC")
    End Sub

    ' Alanl 8/6/2010
    ' Referral 150.
    ' Reduce report columns to 3, dropping End Time and changing Start Time to Logged Time
    Private Sub Print()
        Dim lstLog As List(Of BOSecurityProfile.cActivityLog)
        Dim systemUser As New BOSecurityProfile.cSystemUsers(dl)
        Dim row As Integer = 0
        Dim drRetopt As DataRow
        Dim wstn As BOSystem.cParameter = New BOSystem.cParameter(dl)
        Dim checkWorkstation = wstn.GetParameterString(976)
        Dim employeeName As String = String.Empty

        If ReportSpread_Sheet1.RowCount > 0 Then
            ReportSpread_Sheet1.RemoveRows(0, ReportSpread_Sheet1.RowCount)
        End If

        Me.ReportSpread_Sheet1.ColumnCount = 3
        For i As Integer = 0 To ReportSpread_Sheet1.ColumnCount - 1
            ReportSpread_Sheet1.Columns(i).CellType = New Spread.CellType.TextCellType
        Next

        Me.ReportSpread_Sheet1.Columns(0).Width = 75
        Me.ReportSpread_Sheet1.Columns(1).Width = 258
        Me.ReportSpread_Sheet1.Columns(2).Width = 100
        'Me.ReportSpread_Sheet1.Columns(3).Width = 80   ' Referral 150.

        'set up headings 
        ReportSpread_Sheet1.Columns(0).Label = "Date"
        ReportSpread_Sheet1.Columns(1).Label = "Employee ID"
        ReportSpread_Sheet1.Columns(2).Label = "Logged Time"   ' Referral 150.
        'ReportSpread_Sheet1.Columns(3).Label = "End Time"   ' Referral 150.

        Me.ReportSpread_Sheet1.Columns(0).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Me.ReportSpread_Sheet1.Columns(1).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        Me.ReportSpread_Sheet1.Columns(2).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        'Me.ReportSpread_Sheet1.Columns(3).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left   ' Referral 150.

        Me.ReportSpread_Sheet1.PrintInfo.ShowColumnHeaders = True
        Me.ReportSpread_Sheet1.ColumnHeaderVisible = True

        'get store number and name 
        drRetopt = retopt.RetailOptions()

        'get records for print 
        log.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, log.WorkstationID, checkWorkstation)
        log.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        log.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pGreaterThanOrEquals, log.LogDate, dtpStartDate.Value)
        log.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        log.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pLessThanOrEquals, log.LogDate, dtpEndDate.Value)
        log.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        log.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, log.MenuOptionID, -1)
        lstLog = log.LoadMatches()

        ReportSpread.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))

        ReportSpread_Sheet1.RowCount = 1
        If lstLog.Count > 0 Then
            'load records from data 
            For Each log As BOSecurityProfile.cActivityLog In lstLog
                ReportSpread_Sheet1.SetText(row, 0, log.LogDate.Value.ToString("d"))
                ' Alanl - 8/6/2010
                ' Awaiting referal for User Id value of 0, in meantime add this to prevent problems when systemuser is nothing - id 0 returns nothing for getuser
                ' which means cannot use systemuser.name for report and next loop cannot use systemuser.getuser.  NB Look into making GetUser a Shared/Static method.
                systemUser = (New BOSecurityProfile.cSystemUsers(dl)).GetUser(log.EmployeeID.Value)
                If systemUser Is Nothing Then
                    ReportSpread_Sheet1.SetValue(row, 1, String.Format("{0}  {1}", log.EmployeeID.Value.ToString.PadLeft(3, "0"c), "Unknown User"))
                Else
                    ReportSpread_Sheet1.SetValue(row, 1, String.Format("{0}  {1}", log.EmployeeID.Value.ToString.PadLeft(3, "0"c), systemUser.Name.Value))
                End If
                With log.StartTime.Value
                    ReportSpread_Sheet1.SetText(row, 2, String.Format("{0}:{1}:{2}", .Substring(0, 2), .Substring(2, 2), .Substring(4, 2)))
                End With
                ' Referral 150.
                'If Not String.IsNullOrEmpty(log.EndTime.Value) Then
                '    With log.EndTime.Value
                '        ReportSpread_Sheet1.SetText(row, 3, String.Format("{0}:{1}:{2}", .Substring(0, 2), .Substring(2, 2), .Substring(4, 2)))
                '    End With
                'End If
                ReportSpread_Sheet1.RowCount += 1
                row += 1
            Next
            'add trailer records
            ReportSpread_Sheet1.RowCount += 1
            row += 1
            ReportSpread_Sheet1.AddRows(row, 1)
            ReportSpread_Sheet1.SetText(row, 0, "Lines Printed : " & lstLog.Count)
            ReportSpread_Sheet1.AddSpanCell(row, 0, 1, 2)   ' Referral 150.
        Else
            'output nothing comment 
            ReportSpread_Sheet1.RowCount += 1
            row += 1

            ReportSpread_Sheet1.AddSpanCell(row, 0, 1, 2)   ' Referral 150.
            ReportSpread_Sheet1.SetText(row, 0, "Nothing to Report")
        End If
        'add trailer records
        ReportSpread_Sheet1.RowCount += 2
        row += 2
        ReportSpread_Sheet1.AddSpanCell(row, 0, 1, 2)   ' Referral 150.
        ReportSpread_Sheet1.SetText(row, 0, String.Format("{0} {1}", _StoreNumber, _StoreName))

        ReportSpread_Sheet1.RowCount += 1
        row += 1
        ReportSpread_Sheet1.SetText(row, 0, String.Format("Date Range Selected : {0} to {1}", dtpStartDate.Value.ToString("d"), dtpEndDate.Value.ToString("d")))
        ReportSpread_Sheet1.AddSpanCell(row, 0, 1, 3)   ' Referral 150.

        'set header details 
        ReportSpread_Sheet1.PrintInfo.Header = _
                String.Concat( _
                    _ShortAssemblyName, "  /  v", _FileVersion, _
                    New String(" "c, 10), "Daily Line Reversals List", _
                    New String(" "c, 10), "Store: ", _StoreNumber, "-"c, _StoreName, _
                    New String(" "c, 10), "Date Printed: ", DateTime.Now.Date.ToString("d"), _
                    "/rPage: /p" _
               )
    End Sub

    Private Sub ReportSpread_PrintMessageBox(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.PrintMessageBoxEventArgs) Handles ReportSpread.PrintMessageBox
        If e.BeginPrinting = False Then _PrintNotPrinted = False
    End Sub
    Private Sub Finish()
        _PrintNotPrinted = True
        Do While _PrintNotPrinted
            Application.DoEvents()
            System.Threading.Thread.Sleep(1)
        Loop
    End Sub

    Private Sub ParentForm_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs)
        RetrieveButton.PerformClick()
    End Sub

    Private Sub RetrieveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RetrieveButton.Click
        If dtpEndDate.Value < dtpStartDate.Value Then
            MessageBox.Show("End date cannot be earlier than start date.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        Cursor = Cursors.WaitCursor
        ButtonPanel.Enabled = False
        CollapsibleGroupBox1.Enabled = False
        Print()
        ButtonPanel.Enabled = True
        CollapsibleGroupBox1.Enabled = True
        Cursor = Cursors.Default
        PrintButton.Enabled = True

        If _CalledFromNight Then PrintButton.PerformClick()
    End Sub

    Private Sub PrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintButton.Click
        Cursor = Cursors.WaitCursor
        ButtonPanel.Enabled = False
        CollapsibleGroupBox1.Enabled = False

        ReportSpread_Sheet1.PrintInfo.Margin.Header = CInt(ReportSpread_Sheet1.Rows(-1).Height)
        ReportSpread.PrintSheet(0)
        'wait until printing finished
        Finish()
        ButtonPanel.Enabled = True
        CollapsibleGroupBox1.Enabled = True
        Cursor = Cursors.Default

        If _CalledFromNight Then ExitButton.PerformClick()
    End Sub

    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click
        FindForm.Close()
    End Sub

    Private Sub _KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs)
        Select Case e.KeyCode
            Case Keys.F5
                RetrieveButton.PerformClick()
            Case Keys.F9
                PrintButton.PerformClick()
            Case Keys.F12
                ExitButton.PerformClick()
        End Select
    End Sub

    Private Sub dtpReportDate_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dtpStartDate.ValueChanged, dtpEndDate.ValueChanged
        PrintButton.Enabled = False
    End Sub

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        AddHandler FindForm.Shown, AddressOf ParentForm_Shown
        AddHandler FindForm.KeyDown, AddressOf _KeyDown
        FindForm.KeyPreview = True
    End Sub

    Private Sub MyTrace(ByVal message As String)
        Trace.WriteLine(String.Format("{0}/v{1} - {2}", _ShortAssemblyName, _FileVersion, message))
    End Sub
End Class