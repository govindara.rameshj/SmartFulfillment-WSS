﻿
Public Class GetCallFromCloseFactory
    Inherits RequirementSwitchFactory(Of ICallFromClose)

    Public Overrides Function ImplementationA() As ICallFromClose

        Return New ReceiptsOnly
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet()

        ImplementationA_IsActive = CBool(SwitchRepository.RequirementEnabled(-22019))
    End Function

    Public Overrides Function ImplementationB() As ICallFromClose

        Return New NoDRLFilter
    End Function

End Class


