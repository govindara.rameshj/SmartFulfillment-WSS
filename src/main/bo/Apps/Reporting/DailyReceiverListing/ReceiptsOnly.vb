﻿Public Class ReceiptsOnly
    Implements ICallFromClose

    Public Function ReceiptFilterSQL() As String Implements ICallFromClose.ReceiptFilterSQL
        Return "AND ((DS.[TYPE] = 0 and DS.[0BBC] = 0) OR (DS.[TYPE] in (1, 2) and init <> 'Auto'))"
    End Function

End Class
