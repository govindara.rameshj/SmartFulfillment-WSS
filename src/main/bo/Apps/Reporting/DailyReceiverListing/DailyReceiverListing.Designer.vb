﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DailyReceiverListing
    Inherits Cts.Oasys.WinForm.Form

    'UserControl1 overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DailyReceiverListing))
        Me.ReportSpread = New FarPoint.Win.Spread.FpSpread()
        Me.ReportSpread_Sheet1 = New FarPoint.Win.Spread.SheetView()
        Me.TypeGroupBox = New System.Windows.Forms.GroupBox()
        Me.DetailTypeRadioButton = New System.Windows.Forms.RadioButton()
        Me.ListingTypeRadioButton = New System.Windows.Forms.RadioButton()
        Me.LevelGroupBox = New System.Windows.Forms.GroupBox()
        Me.TotalsLevelRadioButton = New System.Windows.Forms.RadioButton()
        Me.DetailsLevelRadioButton = New System.Windows.Forms.RadioButton()
        Me.CopyGroupBox = New System.Windows.Forms.GroupBox()
        Me.StoreCopyRadioButton = New System.Windows.Forms.RadioButton()
        Me.HeadOfficeCopyRadioButton = New System.Windows.Forms.RadioButton()
        Me.lblStartDate = New System.Windows.Forms.Label()
        Me.dtpReportDate = New System.Windows.Forms.DateTimePicker()
        Me.CollapsibleGroupBox1 = New CtsControls.CollapsibleGroupBox()
        Me.ButtonPanel = New System.Windows.Forms.Panel()
        Me.ExitButton = New System.Windows.Forms.Button()
        Me.RetrieveButton = New System.Windows.Forms.Button()
        Me.PrintButton = New System.Windows.Forms.Button()
        CType(Me.ReportSpread, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReportSpread_Sheet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TypeGroupBox.SuspendLayout()
        Me.LevelGroupBox.SuspendLayout()
        Me.CopyGroupBox.SuspendLayout()
        Me.CollapsibleGroupBox1.SuspendLayout()
        Me.ButtonPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'ReportSpread
        '
        Me.ReportSpread.About = "3.0.2004.2005"
        Me.ReportSpread.AccessibleDescription = "ReportSpread, Sheet1, Row 0, Column 0, "
        Me.ReportSpread.AllowCellOverflow = True
        Me.ReportSpread.BackColor = System.Drawing.SystemColors.Control
        Me.ReportSpread.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ReportSpread.Location = New System.Drawing.Point(0, 87)
        Me.ReportSpread.Name = "ReportSpread"
        Me.ReportSpread.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ReportSpread.Sheets.AddRange(New FarPoint.Win.Spread.SheetView() {Me.ReportSpread_Sheet1})
        Me.ReportSpread.Size = New System.Drawing.Size(729, 326)
        Me.ReportSpread.TabIndex = 11
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.ReportSpread.TextTipAppearance = TipAppearance1
        '
        'ReportSpread_Sheet1
        '
        Me.ReportSpread_Sheet1.Reset()
        Me.ReportSpread_Sheet1.SheetName = "Sheet1"
        'Formulas and custom names must be loaded with R1C1 reference style
        Me.ReportSpread_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1
        Me.ReportSpread_Sheet1.ColumnCount = 0
        Me.ReportSpread_Sheet1.RowCount = 0
        Me.ReportSpread_Sheet1.RowHeader.ColumnCount = 0
        Me.ReportSpread_Sheet1.PrintInfo.Margin.Left = 50
        Me.ReportSpread_Sheet1.PrintInfo.Orientation = FarPoint.Win.Spread.PrintOrientation.Portrait
        Me.ReportSpread_Sheet1.PrintInfo.ShowBorder = False
        Me.ReportSpread_Sheet1.PrintInfo.ShowGrid = False
        Me.ReportSpread_Sheet1.PrintInfo.UseMax = False
        Me.ReportSpread_Sheet1.RowHeader.Columns.Default.Resizable = False
        Me.ReportSpread_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1
        Me.ReportSpread.SetActiveViewport(0, 1, 1)
        '
        'TypeGroupBox
        '
        Me.TypeGroupBox.Controls.Add(Me.DetailTypeRadioButton)
        Me.TypeGroupBox.Controls.Add(Me.ListingTypeRadioButton)
        Me.TypeGroupBox.Location = New System.Drawing.Point(6, 19)
        Me.TypeGroupBox.Name = "TypeGroupBox"
        Me.TypeGroupBox.Size = New System.Drawing.Size(78, 60)
        Me.TypeGroupBox.TabIndex = 12
        Me.TypeGroupBox.TabStop = False
        Me.TypeGroupBox.Text = "Type"
        '
        'DetailTypeRadioButton
        '
        Me.DetailTypeRadioButton.AutoSize = True
        Me.DetailTypeRadioButton.Checked = True
        Me.DetailTypeRadioButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DetailTypeRadioButton.Location = New System.Drawing.Point(6, 37)
        Me.DetailTypeRadioButton.Name = "DetailTypeRadioButton"
        Me.DetailTypeRadioButton.Size = New System.Drawing.Size(58, 17)
        Me.DetailTypeRadioButton.TabIndex = 13
        Me.DetailTypeRadioButton.TabStop = True
        Me.DetailTypeRadioButton.Text = "Detail"
        Me.DetailTypeRadioButton.UseVisualStyleBackColor = True
        '
        'ListingTypeRadioButton
        '
        Me.ListingTypeRadioButton.AutoSize = True
        Me.ListingTypeRadioButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListingTypeRadioButton.Location = New System.Drawing.Point(6, 15)
        Me.ListingTypeRadioButton.Name = "ListingTypeRadioButton"
        Me.ListingTypeRadioButton.Size = New System.Drawing.Size(62, 17)
        Me.ListingTypeRadioButton.TabIndex = 0
        Me.ListingTypeRadioButton.Text = "Listing"
        Me.ListingTypeRadioButton.UseVisualStyleBackColor = True
        '
        'LevelGroupBox
        '
        Me.LevelGroupBox.Controls.Add(Me.TotalsLevelRadioButton)
        Me.LevelGroupBox.Controls.Add(Me.DetailsLevelRadioButton)
        Me.LevelGroupBox.Location = New System.Drawing.Point(90, 19)
        Me.LevelGroupBox.Name = "LevelGroupBox"
        Me.LevelGroupBox.Size = New System.Drawing.Size(78, 60)
        Me.LevelGroupBox.TabIndex = 13
        Me.LevelGroupBox.TabStop = False
        Me.LevelGroupBox.Text = "Level"
        '
        'TotalsLevelRadioButton
        '
        Me.TotalsLevelRadioButton.AutoSize = True
        Me.TotalsLevelRadioButton.Checked = True
        Me.TotalsLevelRadioButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TotalsLevelRadioButton.Location = New System.Drawing.Point(6, 16)
        Me.TotalsLevelRadioButton.Name = "TotalsLevelRadioButton"
        Me.TotalsLevelRadioButton.Size = New System.Drawing.Size(60, 17)
        Me.TotalsLevelRadioButton.TabIndex = 13
        Me.TotalsLevelRadioButton.TabStop = True
        Me.TotalsLevelRadioButton.Text = "Totals"
        Me.TotalsLevelRadioButton.UseVisualStyleBackColor = True
        '
        'DetailsLevelRadioButton
        '
        Me.DetailsLevelRadioButton.AutoSize = True
        Me.DetailsLevelRadioButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DetailsLevelRadioButton.Location = New System.Drawing.Point(6, 37)
        Me.DetailsLevelRadioButton.Name = "DetailsLevelRadioButton"
        Me.DetailsLevelRadioButton.Size = New System.Drawing.Size(64, 17)
        Me.DetailsLevelRadioButton.TabIndex = 0
        Me.DetailsLevelRadioButton.Text = "Details"
        Me.DetailsLevelRadioButton.UseVisualStyleBackColor = True
        '
        'CopyGroupBox
        '
        Me.CopyGroupBox.Controls.Add(Me.StoreCopyRadioButton)
        Me.CopyGroupBox.Controls.Add(Me.HeadOfficeCopyRadioButton)
        Me.CopyGroupBox.Enabled = False
        Me.CopyGroupBox.Location = New System.Drawing.Point(174, 19)
        Me.CopyGroupBox.Name = "CopyGroupBox"
        Me.CopyGroupBox.Size = New System.Drawing.Size(245, 60)
        Me.CopyGroupBox.TabIndex = 14
        Me.CopyGroupBox.TabStop = False
        Me.CopyGroupBox.Text = "Copy"
        '
        'StoreCopyRadioButton
        '
        Me.StoreCopyRadioButton.AutoSize = True
        Me.StoreCopyRadioButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StoreCopyRadioButton.Location = New System.Drawing.Point(6, 37)
        Me.StoreCopyRadioButton.Name = "StoreCopyRadioButton"
        Me.StoreCopyRadioButton.Size = New System.Drawing.Size(55, 17)
        Me.StoreCopyRadioButton.TabIndex = 13
        Me.StoreCopyRadioButton.Text = "Store"
        Me.StoreCopyRadioButton.UseVisualStyleBackColor = True
        '
        'HeadOfficeCopyRadioButton
        '
        Me.HeadOfficeCopyRadioButton.AutoSize = True
        Me.HeadOfficeCopyRadioButton.Checked = True
        Me.HeadOfficeCopyRadioButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HeadOfficeCopyRadioButton.Location = New System.Drawing.Point(6, 15)
        Me.HeadOfficeCopyRadioButton.Name = "HeadOfficeCopyRadioButton"
        Me.HeadOfficeCopyRadioButton.Size = New System.Drawing.Size(236, 17)
        Me.HeadOfficeCopyRadioButton.TabIndex = 0
        Me.HeadOfficeCopyRadioButton.TabStop = True
        Me.HeadOfficeCopyRadioButton.Text = "Head Office (excludes BBC receipts) "
        Me.HeadOfficeCopyRadioButton.UseVisualStyleBackColor = True
        '
        'lblStartDate
        '
        Me.lblStartDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblStartDate.AutoSize = True
        Me.lblStartDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(443, 36)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(85, 13)
        Me.lblStartDate.TabIndex = 15
        Me.lblStartDate.Text = "Trading Date:"
        '
        'dtpReportDate
        '
        Me.dtpReportDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpReportDate.CustomFormat = ""
        Me.dtpReportDate.Location = New System.Drawing.Point(534, 32)
        Me.dtpReportDate.MaxDate = New Date(2050, 12, 31, 0, 0, 0, 0)
        Me.dtpReportDate.MinDate = New Date(1990, 1, 1, 0, 0, 0, 0)
        Me.dtpReportDate.Name = "dtpReportDate"
        Me.dtpReportDate.Size = New System.Drawing.Size(189, 20)
        Me.dtpReportDate.TabIndex = 18
        Me.dtpReportDate.Value = New Date(2008, 12, 19, 0, 0, 0, 0)
        '
        'CollapsibleGroupBox1
        '
        Me.CollapsibleGroupBox1.Controls.Add(Me.CopyGroupBox)
        Me.CollapsibleGroupBox1.Controls.Add(Me.TypeGroupBox)
        Me.CollapsibleGroupBox1.Controls.Add(Me.LevelGroupBox)
        Me.CollapsibleGroupBox1.Controls.Add(Me.dtpReportDate)
        Me.CollapsibleGroupBox1.Controls.Add(Me.lblStartDate)
        Me.CollapsibleGroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.CollapsibleGroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.CollapsibleGroupBox1.MinHeight = 18
        Me.CollapsibleGroupBox1.MinWidth = 18
        Me.CollapsibleGroupBox1.Name = "CollapsibleGroupBox1"
        Me.CollapsibleGroupBox1.Size = New System.Drawing.Size(729, 87)
        Me.CollapsibleGroupBox1.Style = CtsControls.CollapsibleGroupBox.Styles.Height
        Me.CollapsibleGroupBox1.TabIndex = 19
        Me.CollapsibleGroupBox1.TabStop = False
        Me.CollapsibleGroupBox1.Text = "Selection Criteria"
        '
        'ButtonPanel
        '
        Me.ButtonPanel.Controls.Add(Me.ExitButton)
        Me.ButtonPanel.Controls.Add(Me.RetrieveButton)
        Me.ButtonPanel.Controls.Add(Me.PrintButton)
        Me.ButtonPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ButtonPanel.Location = New System.Drawing.Point(0, 413)
        Me.ButtonPanel.Name = "ButtonPanel"
        Me.ButtonPanel.Size = New System.Drawing.Size(729, 50)
        Me.ButtonPanel.TabIndex = 21
        '
        'ExitButton
        '
        Me.ExitButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ExitButton.Location = New System.Drawing.Point(651, 8)
        Me.ExitButton.Name = "ExitButton"
        Me.ExitButton.Size = New System.Drawing.Size(75, 39)
        Me.ExitButton.TabIndex = 11
        Me.ExitButton.Text = "F12 Exit"
        Me.ExitButton.UseVisualStyleBackColor = True
        '
        'RetrieveButton
        '
        Me.RetrieveButton.Location = New System.Drawing.Point(3, 8)
        Me.RetrieveButton.Name = "RetrieveButton"
        Me.RetrieveButton.Size = New System.Drawing.Size(75, 39)
        Me.RetrieveButton.TabIndex = 0
        Me.RetrieveButton.Text = "F5 Retrieve"
        Me.RetrieveButton.UseVisualStyleBackColor = True
        '
        'PrintButton
        '
        Me.PrintButton.Location = New System.Drawing.Point(84, 8)
        Me.PrintButton.Name = "PrintButton"
        Me.PrintButton.Size = New System.Drawing.Size(75, 39)
        Me.PrintButton.TabIndex = 10
        Me.PrintButton.Text = "F9 Print"
        Me.PrintButton.UseVisualStyleBackColor = True
        '
        'DailyReceiverListing
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.ReportSpread)
        Me.Controls.Add(Me.CollapsibleGroupBox1)
        Me.Controls.Add(Me.ButtonPanel)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "DailyReceiverListing"
        Me.Size = New System.Drawing.Size(729, 463)
        CType(Me.ReportSpread, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReportSpread_Sheet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TypeGroupBox.ResumeLayout(False)
        Me.TypeGroupBox.PerformLayout()
        Me.LevelGroupBox.ResumeLayout(False)
        Me.LevelGroupBox.PerformLayout()
        Me.CopyGroupBox.ResumeLayout(False)
        Me.CopyGroupBox.PerformLayout()
        Me.CollapsibleGroupBox1.ResumeLayout(False)
        Me.CollapsibleGroupBox1.PerformLayout()
        Me.ButtonPanel.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ReportSpread As FarPoint.Win.Spread.FpSpread
    Friend WithEvents ReportSpread_Sheet1 As FarPoint.Win.Spread.SheetView
    Friend WithEvents TypeGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents ListingTypeRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents DetailTypeRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents LevelGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents TotalsLevelRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents DetailsLevelRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents CopyGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents StoreCopyRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents HeadOfficeCopyRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpReportDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents CollapsibleGroupBox1 As CtsControls.CollapsibleGroupBox
    Friend WithEvents ButtonPanel As System.Windows.Forms.Panel
    Friend WithEvents ExitButton As System.Windows.Forms.Button
    Friend WithEvents RetrieveButton As System.Windows.Forms.Button
    Friend WithEvents PrintButton As System.Windows.Forms.Button

End Class
