﻿Imports System.Configuration.ConfigurationManager
Imports FarPoint.Win
Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.VisualBasic
Imports Oasys.Data
Imports Cts.Oasys.Core.System
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("CallFromClose_UnitCallFromClose_UnitTests, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
"9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
"3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
"97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
"54e0a4a4")> 

Public Class DailyReceiverListing
    Private _PrintNotPrinted As Boolean
    Private _StoreNumber As String
    Private _ShortAssemblyName As String
    Private _FileVersion As String

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyCode
            Case Keys.F5 : RetrieveButton.PerformClick()
            Case Keys.F9 : PrintButton.PerformClick()
            Case Keys.F12 : ExitButton.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Private Function CalledFromClose() As Boolean
        Return RunParameters.Contains(",CFC")
    End Function
    Protected Overrides Sub DoProcessing()

        With FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetAssembly(Me.GetType).Location)
            _ShortAssemblyName = .FileName.Substring(.FileName.LastIndexOf("\"c) + 1, .FileName.LastIndexOf("."c) - .FileName.LastIndexOf("\"c) - 1)
            _FileVersion = .FileVersion
        End With

        _StoreNumber = Store.GetIdName
        dtpReportDate.Value = Dates.GetToday

        RetrieveButton.PerformClick()

        Me.Refresh()
        Application.DoEvents()

        If CalledFromClose() Then
            PrintButton.PerformClick()
            ExitButton.PerformClick()
        End If

    End Sub

    Private Sub Print()
        If ListingTypeRadioButton.Checked Then
            If TotalsLevelRadioButton.Checked Then
                PrintListing(False)
            Else
                PrintListing(True)
            End If
        Else
            If HeadOfficeCopyRadioButton.Checked Then
                PrintDetail(True)
            Else
                PrintDetail(False)
            End If
            ' These reports eject their own pages.
        End If
        'FindForm.Close()
    End Sub

    Private Sub PrintListing(ByVal printDetails As Boolean)
        Dim row As Integer = 0
        Dim reportData As DataTable = Nothing
        Dim workString As String
        Dim drlType As Integer
        Dim workValue As Decimal
        Dim directTotal As Decimal = 0
        Dim bbcTotal As Decimal = 0
        Dim omNumberInfo As String
        Dim pattern As String = "OM Reference (\d+)"
        Dim reg As New RegularExpressions.Regex(pattern)
        Dim regMath As RegularExpressions.Match

        ReportSpread_Sheet1.PrintInfo.ShowColumnHeaders = True
        ReportSpread_Sheet1.ColumnHeaderVisible = True
        Try
            Using con As New Connection
                Using com As New Command(con)
                    com.CommandText = PrintDailyReceiverListingSQL()
                    com.CommandText = com.CommandText.Replace("#Date", dtpReportDate.Value.ToString("yyyy-MM-dd"))
                    reportData = com.ExecuteDataTable()
                End Using
            End Using

            ReportSpread.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))

            If ReportSpread_Sheet1.RowCount > 0 Then
                ReportSpread_Sheet1.RemoveRows(0, ReportSpread_Sheet1.RowCount)
            End If
            ReportSpread_Sheet1.ColumnCount = 11

            For i As Integer = 0 To ReportSpread_Sheet1.ColumnCount - 1
                ReportSpread_Sheet1.Columns(i).CellType = New Spread.CellType.TextCellType
            Next

            ReportSpread_Sheet1.Columns(0).Width = 65
            ReportSpread_Sheet1.Columns(1).Width = 100
            ReportSpread_Sheet1.Columns(2).Width = 100
            ReportSpread_Sheet1.Columns(3).Width = 60
            ReportSpread_Sheet1.Columns(4).Width = 200
            ReportSpread_Sheet1.Columns(5).Width = 120
            ReportSpread_Sheet1.Columns(6).Width = 120
            ReportSpread_Sheet1.Columns(7).Width = 80
            ReportSpread_Sheet1.Columns(8).Width = 90
            ReportSpread_Sheet1.Columns(9).Width = 70
            ReportSpread_Sheet1.Columns(10).Width = 100

            ReportSpread_Sheet1.Columns(1).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
            ReportSpread_Sheet1.Columns(2).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
            If printDetails Then
                ReportSpread_Sheet1.Columns(0).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(3).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(4).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(5).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(6).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(7).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(8).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(9).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(10).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
            Else
                ReportSpread_Sheet1.Columns(3).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
            End If

            'set up headings 
            If printDetails Then
                ReportSpread_Sheet1.Columns(0).Label = "DRL NO"
            Else
                ReportSpread_Sheet1.Columns(0).Label = " "c
                ReportSpread_Sheet1.PrintInfo.ColStart = 1
            End If
            ReportSpread_Sheet1.Columns(1).Label = "DIRECT VAL"
            ReportSpread_Sheet1.Columns(2).Label = "BBC VALUE"
            If Not printDetails Then
                ReportSpread_Sheet1.Columns(3).Label = "TOTALS ONLY"
                ReportSpread_Sheet1.Columns(3).Width = 120
                ReportSpread_Sheet1.ColumnCount = 4
            Else
                ReportSpread_Sheet1.Columns(3).Label = "SUPPL"
                ReportSpread_Sheet1.Columns(4).Label = "SUPPLIER NAME"
                ReportSpread_Sheet1.Columns(5).Label = " "c
                ReportSpread_Sheet1.Columns(6).Label = "OM ORDER NUMBER"
                ReportSpread_Sheet1.Columns(7).Label = "DRL TYPE"
                ReportSpread_Sheet1.Columns(8).Label = "ADVICE NO."
                ReportSpread_Sheet1.Columns(9).Label = "CONSMT"
                ReportSpread_Sheet1.Columns(10).Label = "H/O USE"
            End If

            ReportSpread_Sheet1.RowCount = 1
            For Each reportRow As DataRow In reportData.Rows
                MyTrace(String.Concat("Printing DRL: ", CStr(reportRow("NUMB"))))

                drlType = CInt(reportRow("TYPE"))
                ReportSpread_Sheet1.SetText(row, 0, CStr(reportRow("NUMB")))

                workValue = CDec(reportRow("VALU"))
                If drlType >= 0 And drlType <= 3 And Not CBool(reportRow("0BBC")) Then
                    If drlType = 2 Or drlType = 3 Then
                        workValue *= -1
                    End If

                    ReportSpread_Sheet1.SetText(row, 1, workValue.ToString("0.00"))
                    directTotal += workValue
                End If

                If drlType = 0 And CBool(reportRow("0BBC")) Then
                    ReportSpread_Sheet1.SetText(row, 2, workValue.ToString("0.00"))
                    bbcTotal += workValue
                End If

                If printDetails Then
                    Select Case drlType
                        Case 0
                            If Not IsDBNull(reportRow("0SUP")) Then ReportSpread_Sheet1.SetText(row, 3, CStr(reportRow("0Sup")))
                            If reportRow("SupplierName") Is DBNull.Value Then
                                ReportSpread_Sheet1.SetText(row, 4, "***** NOT FOUND *****")
                            Else
                                ReportSpread_Sheet1.SetText(row, 4, CStr(reportRow("Suppliername")))
                            End If
                        Case 1, 2
                            If Not IsDBNull(reportRow("1STR")) Then ReportSpread_Sheet1.SetText(row, 3, CStr(reportRow("1STR")))
                            If reportRow("StoreName") Is DBNull.Value Then
                                ReportSpread_Sheet1.SetText(row, 4, "***** NOT FOUND *****")
                            Else
                                ReportSpread_Sheet1.SetText(row, 4, CStr(reportRow("StoreName")))
                            End If
                        Case 3
                            If Not IsDBNull(reportRow("3SUP")) Then ReportSpread_Sheet1.SetText(row, 3, CStr(reportRow("3SUP")))
                            If reportRow("ReturnSuppName") Is DBNull.Value Then
                                ReportSpread_Sheet1.SetText(row, 4, "***** NOT FOUND *****")
                            Else
                                ReportSpread_Sheet1.SetText(row, 4, CStr(reportRow("ReturnSuppName")))
                            End If
                    End Select

                    If drlType = 1 Or drlType = 2 Then
                        If Not IsDBNull(reportRow("INFO")) Then
                            omNumberInfo = CStr(reportRow("INFO"))
                            regMath = reg.Match(omNumberInfo)
                            If regMath.Success Then
                                ReportSpread_Sheet1.SetText(row, 6, regMath.Groups(1).ToString())
                            End If
                        End If
                    End If

                    If drlType = 0 Then
                        ReportSpread_Sheet1.SetText(row, 7, "RECEIPT")
                        If Not IsDBNull(reportRow("0DL1")) Then ReportSpread_Sheet1.SetText(row, 8, CStr(reportRow("0DL1")))
                        If Not IsDBNull(reportRow("0CON")) Then ReportSpread_Sheet1.SetText(row, 9, CStr(reportRow("0CON")))
                    End If

                    If drlType = 1 Then
                        ReportSpread_Sheet1.SetText(row, 7, "IBT-IN")
                    End If

                    If drlType = 2 Then
                        ReportSpread_Sheet1.SetText(row, 7, "IBT-OUT")
                    End If

                    If drlType = 3 Then
                        ReportSpread_Sheet1.SetText(row, 7, "RETURN-OUT")
                    End If

                    workString = New String(" "c, 10)
                    ReportSpread_Sheet1.Cells(row, 10, row, 10).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)

                    If Not IsDBNull(reportRow("0DL2")) Then
                        If reportRow("0DL2").ToString.Trim = "N" Then
                            workString = "UN-MATCHED"
                        End If

                        If reportRow("0DL2").ToString.Trim = "Y" Then
                            workString = String.Empty
                        End If
                    End If

                    If workString.Trim.Length > 0 Then
                        row += 1
                        ReportSpread_Sheet1.RowCount += 1
                        ReportSpread_Sheet1.SetText(row, 8, workString)
                    End If

                    If Not IsDBNull(reportRow("0DL3")) Then
                        If CStr(reportRow("0DL3")).Trim.Length > 0 Then
                            row += 1
                            ReportSpread_Sheet1.RowCount += 1
                            ReportSpread_Sheet1.SetText(row, 8, CStr(reportRow("0DL3")))
                        End If
                    End If

                    If Not IsDBNull(reportRow("0DL4")) Then
                        If CStr(reportRow("0DL4")).Trim.Length > 0 Then
                            row += 1
                            ReportSpread_Sheet1.RowCount += 1
                            ReportSpread_Sheet1.SetText(row, 8, CStr(reportRow("0DL4")))
                        End If
                    End If

                    If Not IsDBNull(reportRow("0DL5")) Then
                        If CStr(reportRow("0DL5")).Trim.Length > 0 Then
                            row += 1
                            ReportSpread_Sheet1.RowCount += 1
                            ReportSpread_Sheet1.SetText(row, 8, CStr(reportRow("0DL5")))
                        End If
                    End If

                    If Not IsDBNull(reportRow("0DL6")) Then
                        If CStr(reportRow("0DL6")).Trim.Length > 0 Then
                            row += 1
                            ReportSpread_Sheet1.RowCount += 1
                            ReportSpread_Sheet1.SetText(row, 8, CStr(reportRow("0DL6")))
                        End If
                    End If

                    If Not IsDBNull(reportRow("0DL7")) Then
                        If CStr(reportRow("0DL7")).Trim.Length > 0 Then
                            row += 1
                            ReportSpread_Sheet1.RowCount += 1
                            ReportSpread_Sheet1.SetText(row, 8, CStr(reportRow("0DL7")))
                        End If
                    End If

                    If Not IsDBNull(reportRow("0DL8")) Then
                        If CStr(reportRow("0DL8")).Trim.Length > 0 Then
                            row += 1
                            ReportSpread_Sheet1.RowCount += 1
                            ReportSpread_Sheet1.SetText(row, 8, CStr(reportRow("0DL8")))
                        End If
                    End If

                    If Not IsDBNull(reportRow("0DL9")) Then
                        If CStr(reportRow("0DL9")).Trim.Length > 0 Then
                            row += 1
                            ReportSpread_Sheet1.RowCount += 1
                            ReportSpread_Sheet1.SetText(row, 8, CStr(reportRow("0DL9")))
                        End If
                    End If

                End If

                If Not printDetails Then
                    ReportSpread_Sheet1.RemoveRows(row, 1)
                    row -= 1
                End If

                row += 1
                ReportSpread_Sheet1.RowCount += 1
                If ReportSpread_Sheet1.GetRowPageBreak(row) Then
                    MessageBox.Show(row.ToString)
                End If
            Next

            ReportSpread_Sheet1.SetText(row, 1, directTotal.ToString("0.00"))
            ReportSpread_Sheet1.SetText(row, 2, bbcTotal.ToString("0.00"))
            If printDetails Then
                ReportSpread_Sheet1.Cells(row, 1, row, 2).Border = New LineBorder(Drawing.Color.Black, 1, False, True, False, True)
            End If

            row += 2
            ReportSpread_Sheet1.RowCount += 2

            ReportSpread_Sheet1.SetText(row, 1, (directTotal + bbcTotal).ToString("0.00"))
            ReportSpread_Sheet1.Cells(row, 2).HorizontalAlignment = Spread.CellHorizontalAlignment.Left
            ReportSpread_Sheet1.SetText(row, 2, "Net Total for Day")

            If printDetails Then

                row += 3
                ReportSpread_Sheet1.RowCount += 3
                ReportSpread_Sheet1.Cells(row, 1, row, 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
                ReportSpread_Sheet1.Cells(row, 2).HorizontalAlignment = Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.SetText(row, 2, "ADD:  Total Brought Forward")
                ReportSpread_Sheet1.SetText(row, 5, "......................................................................")

                row += 1
                ReportSpread_Sheet1.RowCount += 1
                ReportSpread_Sheet1.SetText(row, 7, "Manager")

                row += 1
                ReportSpread_Sheet1.RowCount += 1
                ReportSpread_Sheet1.Cells(row, 1, row, 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
                ReportSpread_Sheet1.Cells(row, 2).HorizontalAlignment = Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.SetText(row, 2, "+/- Correction Prior Day")

                row += 2
                ReportSpread_Sheet1.RowCount += 2
                ReportSpread_Sheet1.Cells(row, 1, row, 1).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
                ReportSpread_Sheet1.Cells(row, 2).HorizontalAlignment = Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.SetText(row, 2, "Period Culmulative Total     [To Be Carried Forward]")

                ReportSpread_Sheet1.PrintInfo.Header = _
                                        String.Concat( _
                                    _ShortAssemblyName, "  /  v", _FileVersion, _
                                            New String(" "c, 10), "Daily Receiver Listing Details", _
                                            New String(" "c, 10), "Store: ", _StoreNumber, _
                                            New String(" "c, 10), "Date Printed: ", DateTime.Now.Date.ToString("d"))
                ReportSpread_Sheet1.PrintInfo.Footer = "/l/dl /tl /rPage: /p of /pc"
                ReportSpread_Sheet1.PrintInfo.Orientation = Spread.PrintOrientation.Portrait
            Else
                ReportSpread_Sheet1.PrintInfo.Header = _
                        String.Concat( _
                    _ShortAssemblyName, "  /  v", _FileVersion, _
                            New String(" "c, 10), "Daily Receiver Listing Totals", _
                            New String(" "c, 10), "Store: ", _StoreNumber, _
                            New String(" "c, 10), "Date Printed: ", DateTime.Now.Date.ToString("d"))
                ReportSpread_Sheet1.PrintInfo.Footer = "/l/dl /rPage: /p of /pc"
                ReportSpread_Sheet1.PrintInfo.Orientation = Spread.PrintOrientation.Portrait
            End If


            ReportSpread_Sheet1.OperationMode = Spread.OperationMode.ReadOnly

        Finally
            If reportData IsNot Nothing Then
                reportData.Dispose()
            End If
        End Try

    End Sub

    Private Function GetDetailSQL() As String
        Dim sb As StringBuilder = New StringBuilder

        sb.AppendLine("SELECT ")
        sb.AppendLine("       [DATE1], DS.[NUMB], [VALU], DS.[TYPE], [INFO], ")
        sb.AppendLine("       [0BBC], [0DAT], [0SUP], [0CON], [0SOQ], [0PON], [0REL], ")
        sb.AppendLine("       [0DL1], [0DL2], [0DL3], [0DL4], [0DL5], [0DL6], [0DL7], [0DL8], [0DL9], ds.[1IBT], ")
        sb.AppendLine("       [1STR], [3SUP], [INFO], VM.[NAME] SupplierName, RM.[Name] ReturnSuppName, OS.[ADD1] StoreName, ")
        sb.AppendLine("       [3PON], [3DAT] ")
        sb.AppendLine("FROM DRLSUM DS ")
        sb.AppendLine("LEFT OUTER JOIN SUPMAS VM ON VM.SUPN = DS.[0SUP] ")
        sb.AppendLine("LEFT OUTER JOIN SUPMAS RM ON RM.SUPN = DS.[3SUP] ")
        sb.AppendLine("LEFT OUTER JOIN STRMAS OS ON OS.NUMB = DS.[1STR] ")
        sb.AppendLine("WHERE DATE1 = @reportingDate ")
        sb.Append(DRLTypeFilter)
        sb.AppendLine(" ORDER BY NUMB ")

        Return sb.ToString
    End Function

    Private Function PrintDailyReceiverListingSQL() As String
        Dim sb As StringBuilder = New StringBuilder

        sb.AppendLine("SELECT ")
        sb.AppendLine("[DATE1], DS.[NUMB], [VALU], DS.[TYPE], [0BBC], [0SUP], [0DL1], [0DL2], ")
        sb.AppendLine("[0DL3], [0DL4], [0DL5], [0DL6], [0DL7], [0DL8], [0DL9], [0CON], DS.INFO,")
        sb.AppendLine("[1STR], [3SUP], VM.[NAME] SupplierName, RM.[Name] ReturnSuppName, OS.[ADD1] StoreName ")
        sb.AppendLine("FROM DRLSUM DS ")
        sb.AppendLine("LEFT OUTER JOIN SUPMAS VM ON VM.SUPN = DS.[0SUP] ")
        sb.AppendLine("LEFT OUTER JOIN SUPMAS RM ON RM.SUPN = DS.[3SUP] ")
        sb.AppendLine("LEFT OUTER JOIN STRMAS OS ON OS.NUMB = DS.[1STR] ")
        sb.AppendLine("WHERE [DATE1] = '#Date' ")
        sb.Append(DRLTypeFilter)
        sb.AppendLine("ORDER BY NUMB ")

        Return sb.ToString
    End Function

    Private Function PrintDailyReceiverListingDetailSQL() As String
        Dim sb As StringBuilder = New StringBuilder

        sb.AppendLine("SELECT ")
        sb.AppendLine("DD.[SKUN], DD.[BDES], DD.[BSPC], DD.[RECQ], DD.[IBTQ], ")
        sb.AppendLine("DD.[RETQ], DD.[PRIC], IM.[DESCR], IM.[PROD] ")
        sb.AppendLine("FROM DRLDET DD ")
        sb.AppendLine("LEFT OUTER JOIN STKMAS IM ON ")
        sb.AppendLine("IM.SKUN = DD.SKUN ")
        sb.AppendLine("WHERE DD.NUMB = @drlNumber ")
        'sb.Append(DRLTypeFilter)
        sb.AppendLine("ORDER BY DD.[SKUN] ")

        Return sb.ToString

    End Function

    Private Function DRLTypeFilter() As String

        Dim result As String = String.Empty

        If CalledFromClose() Then
            Dim factory As ICallFromClose = (New GetCallFromCloseFactory).GetImplementation()
            result = factory.ReceiptFilterSQL()
        End If

        Return result

    End Function

    Private Sub PrintDetail(ByVal headOfficeCopy As Boolean)
        Dim row As Integer = 0
        Dim reportData As DataTable = Nothing
        Dim detailData As DataTable = Nothing
        Dim copyDescription As String

        Dim workString As String
        Dim drlType As Integer
        Dim lineQuantity As Integer
        Dim linePrice As Decimal
        Dim lineValue As Decimal

        Dim totalQuantity As Integer
        Dim totalValue As Decimal

        Dim blnIBT As Boolean = False

        ReportSpread_Sheet1.PrintInfo.ShowColumnHeaders = False
        ReportSpread_Sheet1.ColumnHeaderVisible = False

        If headOfficeCopy Then
            copyDescription = "*** Head Office Copy ***"
        Else
            copyDescription = "***    Store Copy    ***"
        End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author      : Partha Dutta
        ' Date        : 21/09/2010
        ' Referral No : 181
        ' Notes       : Added missing fields "3PON" & "3DAT"
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Try
            Using con As New Connection
                Using com As New Command(con)
                    com.CommandText = GetDetailSQL()
                    com.AddParameter("@reportingDate", dtpReportDate.Value)
                    reportData = com.ExecuteDataTable()
                End Using
            End Using


            ReportSpread.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            If ReportSpread_Sheet1.RowCount > 0 Then
                ReportSpread_Sheet1.RemoveRows(0, ReportSpread_Sheet1.RowCount)
            End If
            ReportSpread_Sheet1.ColumnCount = 7

            For i As Integer = 0 To ReportSpread_Sheet1.ColumnCount - 1
                ReportSpread_Sheet1.Columns(i).CellType = New Spread.CellType.TextCellType
            Next

            ReportSpread_Sheet1.Columns(0).Width = 60
            ReportSpread_Sheet1.Columns(1).Width = 330
            ReportSpread_Sheet1.Columns(2).Width = 90
            ReportSpread_Sheet1.Columns(3).Width = 65
            ReportSpread_Sheet1.Columns(4).Width = 65
            ReportSpread_Sheet1.Columns(5).Width = 65
            ReportSpread_Sheet1.Columns(6).Width = 65

            ReportSpread_Sheet1.Columns(0).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
            ReportSpread_Sheet1.Columns(1).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
            ReportSpread_Sheet1.Columns(2).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
            ReportSpread_Sheet1.Columns(3).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
            ReportSpread_Sheet1.Columns(4).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
            ReportSpread_Sheet1.Columns(5).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
            ReportSpread_Sheet1.Columns(6).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right

            ReportSpread_Sheet1.RowCount = 1
            For Each drlRow As DataRow In reportData.Rows
                If headOfficeCopy AndAlso CBool(drlRow("0BBC")) Then
                    MyTrace(String.Concat("Skipping BBC DRL: ", drlRow("NUMB")))
                    Continue For
                End If
                MyTrace(String.Concat("Printing DRL: ", drlRow("NUMB")))

                'set up headings 
                drlType = CInt(drlRow("TYPE"))

                ReportSpread_Sheet1.SetText(row, 0, _
                    String.Concat(_StoreNumber, New String(" "c, 40), copyDescription))


                ReportSpread_Sheet1.RowCount += 2
                row += 2
                Select Case drlType
                    Case 0
                        workString = "Receipt"
                        blnIBT = False
                    Case 1
                        workString = "IBT-In"
                        blnIBT = True
                    Case 2
                        workString = "IBT-Out"
                        blnIBT = True
                    Case 3
                        workString = "Return"
                        blnIBT = False
                    Case Else
                        workString = "???????"
                        blnIBT = False
                End Select
                ReportSpread_Sheet1.SetText(row, 0, _
                                    String.Concat("DRL Number:- ", drlRow("NUMB"), _
                                    "    Type:- ", workString, _
                                    "    Created:- ", CDate(drlRow("DATE1")).ToString("d"), _
                                    "          Value:- ", CDec(drlRow("VALU")).ToString("0.00")))

                If blnIBT = True Then
                    workString = "Store  :- "
                Else
                    workString = "Supplier  :- "
                End If

                ReportSpread_Sheet1.RowCount += 2
                row += 2
                Select Case drlType
                    Case 0
                        workString = String.Concat(workString, drlRow("0SUP"), " ", drlRow("SupplierName"))
                    Case 1, 2
                        workString = String.Concat(workString, drlRow("1STR"), " ", drlRow("StoreName"))
                    Case 3
                        workString = String.Concat(workString, drlRow("3SUP"), " ", drlRow("ReturnSuppName"))
                End Select
                If CBool(drlRow("0BBC")) Then
                    workString += "   BBC"
                Else
                    workString += New String(" "c, 9)
                End If
                workString += "      P/O No:- "
                Select Case drlType
                    Case 0
                        workString = String.Concat(workString, drlRow("0PON"), "."c, drlRow("0REL"))
                    Case 1, 2
                        workString = String.Concat(workString, drlRow("1IBT"))
                    Case 3
                        workString = String.Concat(workString, drlRow("3PON"))
                End Select
                ReportSpread_Sheet1.SetText(row, 0, workString)

                ReportSpread_Sheet1.RowCount += 2
                row += 2

                If drlType = 3 Then
                    ReportSpread_Sheet1.SetText(row, 0, _
                        String.Concat("P/O Date  :- ", CDate(drlRow("3DAT")).ToString("d")))
                ElseIf drlType = 0 Then
                    Dim sb As New StringBuilder
                    sb.Append("P/O Date  :- ")
                    If Not IsDBNull(drlRow("0DAT")) Then sb.Append(CDate(drlRow("0DAT")).ToString("d"))
                    sb.Append("      SOQ CTL:- ")
                    If Not IsDBNull(drlRow("0SOQ")) Then sb.Append(drlRow("0SOQ").ToString)
                    sb.Append("      Consignment No:- ")
                    If Not IsDBNull(drlRow("0CON")) Then sb.Append(drlRow("0CON").ToString)
                    ReportSpread_Sheet1.SetText(row, 0, sb.ToString)
                End If

                If drlType = 0 Or drlType = 3 Then
                    ReportSpread_Sheet1.RowCount += 2
                    row += 2
                End If

                If drlType = 0 Then
                    workString = String.Empty
                    If Not IsDBNull(drlRow("0DL2")) Then workString = drlRow("0DL2").ToString()

                    If workString.Equals("N") Then
                        workString = "Un-Matched"
                    ElseIf workString.Equals("Y") Then
                        workString = String.Empty
                    End If
                    ReportSpread_Sheet1.SetText(row, 0, _
                    String.Concat("Delivery Notes:- ", drlRow("0DL1"), _
                                      New String(" "c, 3), workString, _
                                      New String(" "c, 3), drlRow("0DL3"), _
                                      New String(" "c, 3), drlRow("0DL4"), _
                                      New String(" "c, 3), drlRow("0DL5"), _
                                      New String(" "c, 3), drlRow("0DL6")))
                    row += 1
                    ReportSpread_Sheet1.RowCount += 1
                    ReportSpread_Sheet1.SetText(row, 0, _
                    String.Concat(New String(" "c, 20), _
                                      New String(" "c, 3), drlRow("0DL7"), _
                                      New String(" "c, 3), drlRow("0DL8"), _
                                      New String(" "c, 3), drlRow("0DL9")))
                    row += 2
                    ReportSpread_Sheet1.RowCount += 2
                End If

                ReportSpread_Sheet1.SetText(row, 0, String.Concat("Comment  :- ", drlRow("INFO")))
                row += 1
                ReportSpread_Sheet1.RowCount += 1

                If drlType = 0 Then
                    row += 1
                    ReportSpread_Sheet1.RowCount += 1
                End If

                ReportSpread_Sheet1.SetText(row, 0, "Sku No")
                ReportSpread_Sheet1.SetText(row, 1, "Item Description and Selling Unit")
                'ReportSpread_Sheet1.SetText(row, 2, "Selu")
                ReportSpread_Sheet1.SetText(row, 2, "Supp. Ref.")
                ReportSpread_Sheet1.SetText(row, 3, "Quantity")
                ReportSpread_Sheet1.SetText(row, 4, "Price")
                ReportSpread_Sheet1.SetText(row, 5, "Sku No")
                ReportSpread_Sheet1.SetText(row, 6, "Value")

                ReportSpread_Sheet1.Cells(row, 0, row, 6).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)

                Using con As New Connection
                    Using com As New Command(con)
                        com.CommandText = PrintDailyReceiverListingDetailSQL()
                        com.AddParameter("@drlNumber", CStr(drlRow("NUMB")))
                        If detailData IsNot Nothing Then
                            detailData.Dispose()
                            detailData = Nothing
                        End If
                        detailData = com.ExecuteDataTable()
                    End Using
                End Using

                totalValue = 0
                totalQuantity = 0
                MyTrace(String.Concat("Printing details for DRL: ", drlRow("NUMB")))
                For Each detailRow As DataRow In detailData.Rows
                    row += 1
                    ReportSpread_Sheet1.RowCount += 1
                    ReportSpread_Sheet1.SetText(row, 0, detailRow("SKUN").ToString)

                    If IsDBNull(detailRow("DESCR")) Then
                        workString = "***** NOT FOUND *****"
                    Else
                        workString = detailRow("DESCR").ToString
                    End If

                    If Not IsDBNull(detailRow("BDES")) Then
                        If (drlType = 0 And CStr(detailRow("BDES")).Trim.Length = 0) Or (drlType <> 0) Then
                            ReportSpread_Sheet1.SetText(row, 1, workString)
                            If IsDBNull(detailRow("PROD")) Then
                                workString = String.Empty
                            Else
                                workString = CStr(detailRow("PROD"))
                            End If
                            ReportSpread_Sheet1.SetText(row, 2, workString)
                        End If

                    Else
                        'If drlType = 0 Then  'Removed as I don't see whty a desc should not be printed
                        ReportSpread_Sheet1.SetText(row, 1, workString)
                        If IsDBNull(detailRow("PROD")) Then
                            workString = String.Empty
                        Else
                            workString = CStr(detailRow("PROD"))
                        End If
                        ReportSpread_Sheet1.SetText(row, 2, workString)
                        'End If
                    End If

                    If Not IsDBNull(detailRow("BSPC")) Then
                        If drlType = 0 And CStr(detailRow("BSPC")).Trim.Length <> 0 Then
                            ReportSpread_Sheet1.SetText(row, 1, detailRow("BDES").ToString)
                            ReportSpread_Sheet1.SetText(row, 2, detailRow("BSPC").ToString)
                        End If
                    End If

                    Select Case drlType
                        Case 0
                            lineQuantity = CInt(detailRow("RECQ"))
                        Case 1, 2
                            lineQuantity = CInt(detailRow("IBTQ"))
                        Case 3
                            lineQuantity = CInt(detailRow("RETQ"))
                        Case Else
                            lineQuantity = 0
                    End Select
                    linePrice = CDec(detailRow("PRIC"))
                    lineValue = lineQuantity * linePrice

                    ReportSpread_Sheet1.SetText(row, 3, lineQuantity.ToString)
                    ReportSpread_Sheet1.SetText(row, 4, linePrice.ToString("0.00"))
                    ReportSpread_Sheet1.SetText(row, 5, CStr(detailRow("SKUN")))
                    ReportSpread_Sheet1.SetText(row, 6, lineValue.ToString("0.00"))

                    totalQuantity += lineQuantity
                    totalValue += lineValue
                Next
                ReportSpread_Sheet1.Cells(row, 3, row, 3).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
                ReportSpread_Sheet1.Cells(row, 6, row, 6).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)

                row += 1
                ReportSpread_Sheet1.RowCount += 1
                ReportSpread_Sheet1.SetText(row, 2, "Grand Total")
                ReportSpread_Sheet1.SetText(row, 3, totalQuantity.ToString)
                ReportSpread_Sheet1.SetText(row, 6, totalValue.ToString("0.00"))
                ReportSpread_Sheet1.Cells(row, 3, row, 3).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
                ReportSpread_Sheet1.Cells(row, 6, row, 6).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)

                row += 1
                ReportSpread_Sheet1.RowCount += 1
                'ReportSpread_Sheet1.SetRowPageBreak(row, False)
                ReportSpread_Sheet1.Rows(row).PageBreak = True
            Next


            ReportSpread_Sheet1.PrintInfo.Header = _
                    String.Concat( _
                        _ShortAssemblyName, "  /  v", _FileVersion, _
                        New String(" "c, 10), "D.R.L. Detail Report", _
                        New String(" "c, 10), "Store: ", _StoreNumber, _
                        New String(" "c, 10), "Date Printed: ", DateTime.Now.Date.ToString("d"))
            ReportSpread_Sheet1.PrintInfo.Footer = "/l/dl /tl /rPage: /p of /pc"
            ReportSpread_Sheet1.PrintInfo.Orientation = Spread.PrintOrientation.Portrait
        Finally
            If reportData IsNot Nothing Then reportData.Dispose()
            If detailData IsNot Nothing Then detailData.Dispose()
        End Try

    End Sub

    Private Sub ListingTypeRadioButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListingTypeRadioButton.CheckedChanged
        LevelGroupBox.Enabled = DirectCast(sender, RadioButton).Checked
        CopyGroupBox.Enabled = Not LevelGroupBox.Enabled
        PrintButton.Enabled = False
    End Sub

    Private Sub Other_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HeadOfficeCopyRadioButton.CheckedChanged, DetailsLevelRadioButton.CheckedChanged
        PrintButton.Enabled = False
    End Sub

    Private Sub ReportSpread_ActiveSheetChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ReportSpread.ActiveSheetChanged
        Trace.WriteLine("Im here now.")
    End Sub

    Private Sub ReportSpread_PrintMessageBox(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.PrintMessageBoxEventArgs) Handles ReportSpread.PrintMessageBox
        If e.BeginPrinting = False Then _PrintNotPrinted = False
    End Sub

    Private Sub dtpReportDate_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dtpReportDate.ValueChanged
        PrintButton.Enabled = False
    End Sub

    Private Sub Finish()
        _PrintNotPrinted = True
        Do While _PrintNotPrinted
            Application.DoEvents()
            System.Threading.Thread.Sleep(1)
        Loop
    End Sub


    Private Sub RetrieveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RetrieveButton.Click

        Cursor = Cursors.WaitCursor
        ButtonPanel.Enabled = False
        CollapsibleGroupBox1.Enabled = False
        Print()
        ButtonPanel.Enabled = True
        CollapsibleGroupBox1.Enabled = True
        Cursor = Cursors.Default
        PrintButton.Enabled = True

    End Sub

    Private Sub PrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintButton.Click
        Cursor = Cursors.WaitCursor
        ButtonPanel.Enabled = False
        CollapsibleGroupBox1.Enabled = False

        ReportSpread_Sheet1.PrintInfo.Margin.Header = CInt(ReportSpread_Sheet1.Rows(-1).Height)
        ReportSpread.PrintSheet(0)
        'wait until printing finished
        Finish()
        ButtonPanel.Enabled = True
        CollapsibleGroupBox1.Enabled = True
        Cursor = Cursors.Default

    End Sub

    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click
        FindForm.Close()
    End Sub

    Private Sub MyTrace(ByVal message As String)
        Trace.WriteLine(String.Format("{0}/v{1} - {2}", _ShortAssemblyName, _FileVersion, message))
    End Sub

End Class
