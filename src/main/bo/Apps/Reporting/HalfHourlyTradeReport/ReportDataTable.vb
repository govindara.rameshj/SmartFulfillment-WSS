﻿Imports System.Text

Public Class ReportDataTable
    Inherits DataTable
    Private _dataDate As Date

    Public Enum col
        Start
        [End]
        Sales
        Refund
        NetSales
        RefValue
        SubTotal
    End Enum

    Public Sub New(ByVal dataDate As Date)
        MyBase.New()
        _dataDate = dataDate
        SetupColumns()
        SetupRows()
    End Sub

    Private Sub SetupColumns()
        Me.Columns.Add("START", GetType(String))
        Me.Columns.Add("END", GetType(String))
        Me.Columns.Add("SALES", GetType(Integer))
        Me.Columns.Add("REFUND", GetType(Integer))
        Me.Columns.Add("NET-SALES", GetType(Decimal))
        Me.Columns.Add("REF-VALUE", GetType(Decimal))
        Me.Columns.Add("SUB-TOTAL", GetType(Decimal))

        Me.Columns(col.Sales).DefaultValue = 0
        Me.Columns(col.Refund).DefaultValue = 0
        Me.Columns(col.NetSales).DefaultValue = 0
        Me.Columns(col.RefValue).DefaultValue = 0
        Me.Columns(col.SubTotal).DefaultValue = 0
    End Sub

    Private Sub SetupRows()

        For index As Integer = 7 To 21
            Dim nr As DataRow = Me.NewRow
            nr(col.Start) = String.Format("{0}0100", index.ToString("00"))
            nr(col.End) = String.Format("{0}3000", index.ToString("00"))
            Me.Rows.Add(nr)

            Dim nr2 As DataRow = Me.NewRow
            nr2(col.Start) = String.Format("{0}3100", index.ToString("00"))
            nr2(col.End) = String.Format("{0}0000", (index + 1).ToString("00"))
            Me.Rows.Add(nr2)
        Next

    End Sub



    Public Sub AddValues(ByVal type As String, ByVal time As String, ByVal value As Decimal)

        'check that there are some rows
        If Me.Rows.Count = 0 Then Exit Sub

        'find datarow matching to the passed in time
        For Each dr As DataRow In Me.Rows
            Dim start As String = (CInt(dr(col.Start)) - 100).ToString("000000")

            Select Case True
                Case time < start
                Case (start <= time) AndAlso (time < dr(col.End).ToString)
                Case Else : Continue For
            End Select

            AddValuesToRow(dr, type, value)
            RecalcSubTotal()
            Exit Sub
        Next

        'if here then check if outside last row
        Dim drLast As DataRow = Me.Rows(Me.Rows.Count - 1)
        If time >= drLast(col.End).ToString Then
            AddValuesToRow(drLast, type, value)
            RecalcSubTotal()
        End If

    End Sub

    Private Sub AddValuesToRow(ByRef dr As DataRow, ByVal type As String, ByVal value As Decimal)
        Select Case type
            Case "SA", "M+", "M-"
                dr(col.Sales) = CInt(dr(col.Sales)) + 1
                dr(col.NetSales) = CDec(dr(col.NetSales)) + value
            Case "SC", "C+", "C-"
                dr(col.Sales) = CInt(dr(col.Sales)) - 1
                dr(col.NetSales) = CDec(dr(col.NetSales)) + value
            Case "RF"
                dr(col.Refund) = CInt(dr(col.Refund)) + 1
                dr(col.RefValue) = CDec(dr(col.RefValue)) + value
            Case "RC"
                dr(col.Refund) = CInt(dr(col.Refund)) - 1
                dr(col.RefValue) = CDec(dr(col.RefValue)) + value
        End Select
    End Sub

    Private Sub RecalcSubTotal()

        Dim subtotal As Decimal = 0
        For Each dr As DataRow In Me.Rows
            subtotal += CDec(dr(col.NetSales))
            dr(col.SubTotal) = subtotal
        Next

    End Sub


    Public Function RowHeaderString() As String

        Dim sb As New StringBuilder
        sb.Append(Space(3))
        sb.Append(Me.Columns(col.Start).Caption.PadLeft(5, " "c) & Space(2))
        sb.Append(Me.Columns(col.End).Caption.PadLeft(5, " "c) & Space(2))
        sb.Append(Me.Columns(col.Sales).Caption.PadLeft(6, " "c) & Space(2))
        sb.Append(Me.Columns(col.Refund).Caption.PadLeft(6, " "c) & Space(8))
        sb.Append(Me.Columns(col.NetSales).Caption.PadLeft(9, " "c) & Space(2))
        sb.Append(Me.Columns(col.RefValue).Caption.PadLeft(10, " "c) & Space(3))
        sb.Append(Me.Columns(col.SubTotal).Caption.PadLeft(9, " "c))
        Return sb.ToString

    End Function

    Public Function RowHeaderDashedString() As String

        Dim sb As New StringBuilder
        sb.Append(Space(3))
        sb.Append("-----" & Space(2))
        sb.Append("-----" & Space(2))
        sb.Append("------" & Space(2))
        sb.Append("------" & Space(8))
        sb.Append("---------" & Space(2))
        sb.Append("----------" & Space(3))
        sb.Append("---------")
        Return sb.ToString

    End Function

    Public Function RowTotalDashedString() As String

        Dim sb As New StringBuilder
        sb.Append(Space(17))
        sb.Append("------" & Space(2))
        sb.Append("------" & Space(8))
        sb.Append("---------" & Space(2))
        sb.Append("----------")
        Return sb.ToString

    End Function

    Public Function RowTotalDashedDoubleString() As String

        Dim sb As New StringBuilder
        sb.Append(Space(17))
        sb.Append("======" & Space(2))
        sb.Append("======" & Space(8))
        sb.Append("=========" & Space(2))
        sb.Append("==========")
        Return sb.ToString

    End Function

    Public Function RowString(ByVal index As Integer) As String

        Dim dr As DataRow = Me.Rows(index)
        Dim sb As New StringBuilder
        sb.Append(Space(3))
        sb.Append(String.Format("{0}.{1}", dr(col.Start).ToString.Substring(0, 2), dr(col.Start).ToString.Substring(2, 2)).PadLeft(5, " "c) & Space(2))
        sb.Append(String.Format("{0}.{1}", dr(col.End).ToString.Substring(0, 2), dr(col.End).ToString.Substring(2, 2)).PadLeft(5, " "c) & Space(2))
        sb.Append(Math.Abs(CDec(dr(col.Sales))).ToString.PadLeft(6, " "c) & Space(2))
        sb.Append(Math.Abs(CDec(dr(col.Refund))).ToString.PadLeft(6, " "c) & Space(8))

        sb.Append(Math.Abs(CDec(dr(col.NetSales))).ToString("0.00").PadLeft(9, " "c))
        If CDec(dr(col.NetSales)) < 0 Then
            sb.Append("-" & Space(1))
        Else
            sb.Append(Space(2))
        End If

        sb.Append(Math.Abs(CDec(dr(col.RefValue))).ToString("0.00").PadLeft(10, " "c))
        If CDec(dr(col.RefValue)) < 0 Then
            sb.Append("-" & Space(2))
        Else
            sb.Append(Space(3))
        End If

        sb.Append(Math.Abs(CDec(dr(col.SubTotal))).ToString("0.00").PadLeft(9, " "c))
        If CDec(dr(col.SubTotal)) < 0 Then
            sb.Append("-" & Space(7))
        Else
            sb.Append(Space(8))
        End If

        If index = 0 Then
            sb.Append("Data Date = " & _dataDate.ToString("dd/MM/yy"))
        End If

        Return sb.ToString

    End Function

    Public Function RowTotalString() As String

        Dim sales As Integer = 0
        Dim refunds As Integer = 0
        Dim netsales As Decimal = 0
        Dim refvalue As Decimal = 0
        For Each dr As DataRow In Me.Rows
            sales += CInt(dr(col.Sales))
            refunds += CInt(dr(col.Refund))
            netsales += CInt(dr(col.NetSales))
            refvalue += CInt(dr(col.RefValue))
        Next

        Dim sb As New StringBuilder
        sb.Append(Space(5) & "GRAND TOTAL" & Space(1))
        sb.Append(sales.ToString.PadLeft(6, " "c) & Space(2))
        sb.Append(refunds.ToString.PadLeft(6, " "c) & Space(8))
        sb.Append(Math.Abs(netsales).ToString("0.00").PadLeft(9, " "c))
        If netsales < 0 Then
            sb.Append("-" & Space(1))
        Else
            sb.Append(Space(2))
        End If

        sb.Append(Math.Abs(refvalue).ToString("0.00").PadLeft(10, " "c))
        If refvalue < 0 Then sb.Append("-")

        Return sb.ToString

    End Function

End Class
