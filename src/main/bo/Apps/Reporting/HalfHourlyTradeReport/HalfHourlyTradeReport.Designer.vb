﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HalfHourlyTradeReport
    Inherits Cts.Oasys.WinForm.Form

    'UserControl1 overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(HalfHourlyTradeReport))
        Me.uxPrintButton = New System.Windows.Forms.Button
        Me.uxDatePicker = New System.Windows.Forms.DateTimePicker
        Me.uxDateLabel = New System.Windows.Forms.Label
        Me.uxExportButton = New System.Windows.Forms.Button
        Me.uxExitButton = New System.Windows.Forms.Button
        Me.uxRetrieveButton = New System.Windows.Forms.Button
        Me.uxReportGrid = New DevExpress.XtraGrid.GridControl
        Me.uxReportView = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.PrintingSystemHHTR = New DevExpress.XtraPrinting.PrintingSystem(Me.components)
        Me.PrintableComponentLinkHHTR = New DevExpress.XtraPrinting.PrintableComponentLink(Me.components)
        CType(Me.uxReportGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxReportView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrintingSystemHHTR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrintableComponentLinkHHTR.ImageCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'uxPrintButton
        '
        Me.uxPrintButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxPrintButton.Location = New System.Drawing.Point(165, 508)
        Me.uxPrintButton.Name = "uxPrintButton"
        Me.uxPrintButton.Size = New System.Drawing.Size(75, 39)
        Me.uxPrintButton.TabIndex = 10
        Me.uxPrintButton.Text = "F9 Print"
        Me.uxPrintButton.UseVisualStyleBackColor = True
        '
        'uxDatePicker
        '
        Me.uxDatePicker.CustomFormat = ""
        Me.uxDatePicker.Location = New System.Drawing.Point(94, 3)
        Me.uxDatePicker.MaxDate = New Date(2050, 12, 31, 0, 0, 0, 0)
        Me.uxDatePicker.MinDate = New Date(1990, 1, 1, 0, 0, 0, 0)
        Me.uxDatePicker.Name = "uxDatePicker"
        Me.uxDatePicker.Size = New System.Drawing.Size(189, 20)
        Me.uxDatePicker.TabIndex = 7
        Me.uxDatePicker.Value = New Date(2008, 12, 19, 0, 0, 0, 0)
        '
        'uxDateLabel
        '
        Me.uxDateLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uxDateLabel.Location = New System.Drawing.Point(3, 3)
        Me.uxDateLabel.Name = "uxDateLabel"
        Me.uxDateLabel.Size = New System.Drawing.Size(85, 20)
        Me.uxDateLabel.TabIndex = 6
        Me.uxDateLabel.Text = "Trading Date:"
        Me.uxDateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'uxExportButton
        '
        Me.uxExportButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxExportButton.Location = New System.Drawing.Point(84, 508)
        Me.uxExportButton.Name = "uxExportButton"
        Me.uxExportButton.Size = New System.Drawing.Size(75, 39)
        Me.uxExportButton.TabIndex = 12
        Me.uxExportButton.Text = "F6 Export"
        Me.uxExportButton.UseVisualStyleBackColor = True
        '
        'uxExitButton
        '
        Me.uxExitButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxExitButton.Location = New System.Drawing.Point(722, 508)
        Me.uxExitButton.Name = "uxExitButton"
        Me.uxExitButton.Size = New System.Drawing.Size(75, 39)
        Me.uxExitButton.TabIndex = 11
        Me.uxExitButton.Text = "F12 Exit"
        Me.uxExitButton.UseVisualStyleBackColor = True
        '
        'uxRetrieveButton
        '
        Me.uxRetrieveButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxRetrieveButton.Location = New System.Drawing.Point(3, 508)
        Me.uxRetrieveButton.Name = "uxRetrieveButton"
        Me.uxRetrieveButton.Size = New System.Drawing.Size(75, 39)
        Me.uxRetrieveButton.TabIndex = 0
        Me.uxRetrieveButton.Text = "F5 Retrieve"
        Me.uxRetrieveButton.UseVisualStyleBackColor = True
        '
        'uxReportGrid
        '
        Me.uxReportGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxReportGrid.Location = New System.Drawing.Point(3, 29)
        Me.uxReportGrid.MainView = Me.uxReportView
        Me.uxReportGrid.Name = "uxReportGrid"
        Me.uxReportGrid.Size = New System.Drawing.Size(794, 473)
        Me.uxReportGrid.TabIndex = 13
        Me.uxReportGrid.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uxReportView})
        '
        'uxReportView
        '
        Me.uxReportView.GridControl = Me.uxReportGrid
        Me.uxReportView.Name = "uxReportView"
        Me.uxReportView.OptionsBehavior.Editable = False
        Me.uxReportView.OptionsView.ShowFooter = True
        Me.uxReportView.OptionsView.ShowGroupPanel = False
        '
        'PrintingSystemHHTR
        '
        Me.PrintingSystemHHTR.Links.AddRange(New Object() {Me.PrintableComponentLinkHHTR})
        '
        'PrintableComponentLinkHHTR
        '
        Me.PrintableComponentLinkHHTR.Component = Me.uxReportGrid
        '
        '
        '
        Me.PrintableComponentLinkHHTR.ImageCollection.ImageStream = CType(resources.GetObject("PrintableComponentLink1.ImageCollection.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.PrintableComponentLinkHHTR.Landscape = True
        Me.PrintableComponentLinkHHTR.PaperKind = System.Drawing.Printing.PaperKind.A4
        Me.PrintableComponentLinkHHTR.PrintingSystem = Me.PrintingSystemHHTR
        '
        'HalfHourlyTradeReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.uxReportGrid)
        Me.Controls.Add(Me.uxExportButton)
        Me.Controls.Add(Me.uxRetrieveButton)
        Me.Controls.Add(Me.uxPrintButton)
        Me.Controls.Add(Me.uxExitButton)
        Me.Controls.Add(Me.uxDatePicker)
        Me.Controls.Add(Me.uxDateLabel)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "HalfHourlyTradeReport"
        Me.Size = New System.Drawing.Size(800, 550)
        CType(Me.uxReportGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxReportView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrintingSystemHHTR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrintableComponentLinkHHTR.ImageCollection, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents uxPrintButton As System.Windows.Forms.Button
    Friend WithEvents uxDatePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents uxDateLabel As System.Windows.Forms.Label
    Friend WithEvents uxRetrieveButton As System.Windows.Forms.Button
    Friend WithEvents uxExitButton As System.Windows.Forms.Button
    Friend WithEvents uxExportButton As System.Windows.Forms.Button
    Friend WithEvents uxReportGrid As DevExpress.XtraGrid.GridControl
    Friend WithEvents uxReportView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents PrintingSystemHHTR As DevExpress.XtraPrinting.PrintingSystem
    Friend WithEvents PrintableComponentLinkHHTR As DevExpress.XtraPrinting.PrintableComponentLink

End Class
