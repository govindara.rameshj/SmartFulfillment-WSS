﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.IO
Imports Cts.Oasys.Data
Imports Cts.Oasys.Core.System

Public Class HalfHourlyTradeReport
    Private _dt As ReportDataTable
    Private _fromNightRoutine As Boolean

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyCode
            Case Keys.F5 : uxRetrieveButton.PerformClick()
            Case Keys.F6 : uxExportButton.PerformClick()
            Case Keys.F9 : uxPrintButton.PerformClick()
            Case Keys.F12 : uxExitButton.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Protected Overrides Sub DoProcessing()

        'get todays date and check if run from night routine
        uxDatePicker.Value = Dates.GetToday
        uxRetrieveButton.PerformClick()

        If RunParameters.Contains(",CFC") Then
            _fromNightRoutine = True

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Author      : Partha Dutta
            ' Date        : 30/09/2010
            ' Referral No : 356
            ' Notes       : DevExpress grid is defaulting to letter / protrait
            '               To make the control print A4 / landsacpe implement procedure listed here : ms-help://DevExpress.NETv9.1/DevExpress.XtraUtils/CustomDocument2424.htm
            '               Basically you need to use the DevExpress PrintingSystem & PrintableComponentLink controls to set page size / orientation / margin etc...
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim LocalPrinter As New System.Drawing.Printing.PrinterSettings

            PrintableComponentLinkHHTR.Print(LocalPrinter.PrinterName)

            'old code
            'uxReportGrid.Print()




            uxExportButton.PerformClick()
            FindForm.Close()
        End If

    End Sub


    Private Sub uxRetrieveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxRetrieveButton.Click

        Try
            Cursor = Cursors.WaitCursor

            Dim rawData As New DataTable
            Using con As New Connection
                Using com As New Command(con)
                    Dim sb As New StringBuilder
                    sb.Append("SELECT t.[TCOD], t.[TOTL], t.[TIME] FROM DLTOTS t ")
                    sb.Append("WHERE t.[DATE1] = @reportingDate ")
                    sb.Append("AND t.[VOID] = 0 AND t.[TMOD] = 0 AND t.[TCOD] <> 'XR' ")
                    sb.Append("AND t.[TCOD] <> 'ZR' AND t.[TOTL] <> 0 ")

                    com.CommandText = sb.ToString
                    com.AddParameter("@reportingDate", uxDatePicker.Value)
                    rawData = com.ExecuteDataTable
                End Using
            End Using

            _dt = New ReportDataTable(uxDatePicker.Value)
            For Each dr As DataRow In rawData.Rows
                _dt.AddValues(dr("TCOD").ToString, dr("TIME").ToString, CDec(dr("TOTL")))
            Next

            uxReportGrid.DataSource = _dt
            uxReportView.Columns(ReportDataTable.col.Sales).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            uxReportView.Columns(ReportDataTable.col.Refund).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            uxReportView.Columns(ReportDataTable.col.NetSales).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            uxReportView.Columns(ReportDataTable.col.RefValue).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub uxExportButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxExportButton.Click

        'check for rows first
        If _dt.Rows.Count = 0 Then Exit Sub

        'check if directory exists and create if not but first get comms filepath for report export (parameter 912)
        Dim fileFolder As String = Parameter.GetString(912)
        If Not fileFolder.EndsWith("\") Then fileFolder += "\"
        If Not Directory.Exists(fileFolder) Then Directory.CreateDirectory(fileFolder)

        Dim fileName As String = fileFolder & "RRRDTR.JMB"
        If _fromNightRoutine Then fileName = fileFolder & "RRRDTR.DLY"

        'create text stream to write report data into
        Using ts As New StreamWriter(fileName)
            'add header
            Dim sb As New StringBuilder
            If _fromNightRoutine Then
                Dim s As Store.Store = Store.GetStore

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ' Author      : Partha Dutta
                ' Date        : 30/09/2010
                ' Referral No : 429
                ' Notes       : Check length of address string
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                If s.Address1.Length > 17 Then
                    'greater than 17
                    sb.Append(s.Id.ToString("000") & Space(1) & s.Address1.Substring(0, 17))
                Else
                    'less than or equal 17
                    sb.Append(s.Id.ToString("000") & Space(1) & s.Address1)
                End If

                'old code - checking the length first would be useful
                'sb.Append(s.Id.ToString("000") & Space(1) & s.Address1.Substring(0, 17))

            Else
                sb.Append("<CTS Test>" & Space(11))
            End If

            sb.Append("W04 - Half Hourly Trade Report")
            sb.Append(Space(60))
            sb.Append(Now.Date.ToString("dd/MM/yy") & Space(2))
            sb.Append("Page 1")

            ts.WriteLine(sb.ToString)
            ts.WriteLine()
            ts.WriteLine(_dt.RowHeaderString)
            ts.WriteLine(_dt.RowHeaderDashedString)

            'add all rows
            For index As Integer = 0 To _dt.Rows.Count - 1
                ts.WriteLine(_dt.RowString(index))
            Next

            'add totals
            ts.WriteLine()
            ts.WriteLine(_dt.RowTotalDashedString)
            ts.WriteLine(_dt.RowTotalString)
            ts.WriteLine(_dt.RowTotalDashedDoubleString)

            ts.Flush()
        End Using

    End Sub

    Private Sub uxPrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxPrintButton.Click
        Dim print As New Reporting.ReportPrint(uxReportGrid, UserId, WorkstationId)
        print.ReportHeader = "Sales"
        print.ReportTitle = "Half Hourly Trade Report"
        print.ReportParameters.Add("Date: " & uxDatePicker.Value.ToShortDateString)
        print.ShowPreviewDialog()
    End Sub

    Private Sub uxExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxExitButton.Click
        FindForm.Close()
    End Sub

End Class
