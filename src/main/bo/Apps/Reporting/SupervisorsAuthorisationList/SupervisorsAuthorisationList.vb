﻿Imports FarPoint.Win
Imports System.Data.SqlClient
Imports Oasys.Data
Imports Cts.Oasys.Core.System

Public Class SupervisorsAuthorisationList
    Private _printNotPrinted As Boolean
    Private _storeIdName As String
    Private _shortAssemblyName As String
    Private _fileVersion As String

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        e.Handled = True
        Select Case e.KeyCode
            Case Keys.F5 : RetrieveButton.PerformClick()
            Case Keys.F9 : PrintButton.PerformClick()
            Case Keys.F12 : ExitButton.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Protected Overrides Sub DoProcessing()
        With FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetAssembly(Me.GetType).Location)
            _shortAssemblyName = .FileName.Substring(.FileName.LastIndexOf("\"c) + 1, .FileName.LastIndexOf("."c) - .FileName.LastIndexOf("\"c) - 1)
            _fileVersion = .FileVersion
        End With

        dtpReportDate.Value = Dates.GetToday
        _storeIdName = Store.GetIdName

        RetrieveButton.PerformClick()

        If RunParameters.Contains(",CFC") Then
            PrintButton.PerformClick()
            ExitButton.PerformClick()
        End If

    End Sub


    Private Sub Print()
        Dim row As Integer = 0
        Dim reportData As DataTable = Nothing

        If ReportSpread_Sheet1.RowCount > 0 Then
            ReportSpread_Sheet1.RemoveRows(0, ReportSpread_Sheet1.RowCount)
        End If
        Application.DoEvents()

        ReportSpread_Sheet1.RowCount = 1

        Using con As New Connection
            Using com As New Command(con)
                com.CommandText = String.Concat( _
                    "SELECT ", _
                        "t.[TILL], t.[TRAN], t.[OVCTranNumber], t.[TCOD], t.[OPEN], t.[RMAN], t.[VOID], t.[TOTL],", _
                        "t.[CASH], Coalesce(c.[Name], '<-- User NOT on File') CashierName, ", _
                        "t.[SUPV], Coalesce(s.[Name], '<-- User NOT on File') SuperName, ", _
                        "t.[VSUP], Coalesce(v.[Name], '<-- User NOT on File') VoidSuperName, ", _
                        "t.[DSUP], Coalesce(d.[Name], '<-- User NOT on File') DiscountSuperName, ", _
                        "t.[RSUP], Coalesce(r.[Name], '<-- User NOT on File') RefundSuperName, ", _
                        "t.[RMAN], Coalesce(m.[Name], '<-- User NOT on File') ManagerName, ", _
                        "t.[TOCD], Coalesce(sy.[DESCR], '<-- Unknown Reason') OverrideReason ", _
                    "FROM vwDLTOTS t ", _
                    "LEFT OUTER JOIN SystemUsers c ON c.EmployeeCode = t.[CASH] ", _
                    "LEFT OUTER JOIN SystemUsers s ON s.EmployeeCode = t.[SUPV] ", _
                    "LEFT OUTER JOIN SystemUsers v ON v.EmployeeCode = t.[VSUP] ", _
                    "LEFT OUTER JOIN SystemUsers d ON d.EmployeeCode = t.[DSUP] ", _
                    "LEFT OUTER JOIN SystemUsers r ON r.EmployeeCode = t.[RSUP] ", _
                    "LEFT OUTER JOIN SystemUsers m ON m.EmployeeCode = t.[RMAN] ", _
                    "LEFT OUTER JOIN SYSCOD sy ON sy.TYPE = 'TO' AND sy.CODE = t.[TOCD] ", _
                    "WHERE ", _
                        "t.[DATE1] = @reportingDate AND ", _
                        "t.[SUSE] = 1 AND ", _
                        "(t.[VOID] = 0 OR t.[PARK] = 0) AND ", _
                        "(t.[VOID] = 0 OR t.[PKRC] = 0) ", _
                    "ORDER BY t.[CASH], t.[TIME], t.[TILL], t.[TRAN] " _
                )
                com.AddParameter("@reportingDate", dtpReportDate.Value)
                reportData = com.ExecuteDataTable()
            End Using
        End Using

        ReportSpread.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))

        ReportSpread_Sheet1.Columns(0).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        ReportSpread_Sheet1.Columns(1).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        ReportSpread_Sheet1.Columns(2).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        ReportSpread_Sheet1.Columns(3).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Center
        ReportSpread_Sheet1.Columns(4).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        ReportSpread_Sheet1.Columns(5).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
        ReportSpread_Sheet1.Columns(6).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
        ReportSpread_Sheet1.Columns(9).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right

        'set up headings 
        ReportSpread_Sheet1.AddRows(row, 1)
        ReportSpread_Sheet1.Columns(0).Label = "Till"
        ReportSpread_Sheet1.Columns(1).Label = "Tran"
        ReportSpread_Sheet1.Columns(2).Label = "OVC Tran"
        ReportSpread_Sheet1.Columns(3).Label = "Type"
        ReportSpread_Sheet1.Columns(4).Label = "Cashier Number and Name"
        ReportSpread_Sheet1.Columns(5).Label = "Supervisor"
        ReportSpread_Sheet1.Columns(6).Label = "Total"
        ReportSpread_Sheet1.Columns(7).Label = "Type"
        ReportSpread_Sheet1.Columns(8).Label = "Sku No"
        ReportSpread_Sheet1.Columns(9).Label = "Quantity"
        ReportSpread_Sheet1.Columns(10).Label = "Price Override"

        For Each reportRow As DataRow In reportData.Rows

            ReportSpread_Sheet1.SetText(row, 0, CStr(reportRow("TILL")))
            ReportSpread_Sheet1.SetText(row, 1, CStr(reportRow("TRAN")))
            If Not Equals(reportRow("OVCTranNumber"), DBNull.Value) Then
                ReportSpread_Sheet1.SetText(row, 2, CStr(reportRow("OVCTranNumber")))
            End If
            ReportSpread_Sheet1.SetText(row, 3, TranslateType(CStr(reportRow("TCOD"))))

            ReportSpread_Sheet1.SetText(row, 4, String.Concat(String.Concat(CStr(reportRow("CASH")), " ", CStr(reportRow("CashierName")))))

            If CStr(reportRow("TCOD")).Equals("OD") Then
                If CInt(reportRow("OPEN")) = 0 Then
                    ReportSpread_Sheet1.SetText(row, 6, "Void")
                Else
                    Using con As New Connection
                        Using com As New Command(con)
                            com.CommandText = String.Concat("SELECT ODRC", CInt(reportRow("OPEN")), " FROM RETOPT WHERE FKEY = '01'")
                            ReportSpread_Sheet1.SetText(row, 6, CStr(com.ExecuteValue()))
                        End Using
                    End Using
                End If
            End If

            If Not CStr(reportRow("SUPV")).Equals("000") Then
                ReportSpread_Sheet1.SetText(row, 5, String.Concat(CStr(reportRow("SUPV")), " ", CStr(reportRow("SuperName"))))
            End If

            If CBool(reportRow("VOID")) And Not CStr(reportRow("TCOD")).Equals("OD") Then
                ReportSpread_Sheet1.SetText(row, 5, String.Concat(CStr(reportRow("VSUP")), " ", CStr(reportRow("VoidSuperName"))))
                ReportSpread_Sheet1.SetText(row, 7, "Void")
            End If

            If Not CStr(reportRow("DSUP")).Equals("000") And Not CStr(reportRow("TCOD")).Equals("OD") Then
                ReportSpread_Sheet1.SetText(row, 5, String.Concat(CStr(reportRow("DSUP")), " ", CStr(reportRow("DiscountSuperName"))))
                ReportSpread_Sheet1.SetText(row, 7, "Discount")
            End If

            If Not CStr(reportRow("RSUP")).Equals("000") And Not CStr(reportRow("TCOD")).Equals("OD") Then
                ReportSpread_Sheet1.SetText(row, 5, String.Concat(CStr(reportRow("RSUP")), " "c, CStr(reportRow("RefundSuperName"))))
                ReportSpread_Sheet1.SetText(row, 7, "Refund")
            End If

            ReportSpread_Sheet1.SetText(row, 6, CDec(reportRow("TOTL")).ToString("0.00"))

            If Not CStr(reportRow("RMAN")).Equals("000") Then
                row += 1
                ReportSpread_Sheet1.RowCount += 1
                ReportSpread_Sheet1.SetText(row, 4, "Manager Authorisation: ")
                ReportSpread_Sheet1.Cells(row, 4).HorizontalAlignment = Spread.CellHorizontalAlignment.Right
                ReportSpread_Sheet1.SetText(row, 5, String.Concat(CStr(reportRow("RMAN")), " ", CStr(reportRow("ManagerName"))))

                If Not CStr(reportRow("TOCD")).Equals("00") Then
                    ReportSpread_Sheet1.SetText(row, 7, CStr(reportRow("TOCD")))
                    ReportSpread_Sheet1.SetText(row, 8, CStr(reportRow("OverrideReason")))
                End If
            End If
            ReportLines(CStr(reportRow("TILL")), CStr(reportRow("TRAN")), row)
            row += 2
            ReportSpread_Sheet1.RowCount += 2
        Next

        row += 1
        ReportSpread_Sheet1.RowCount += 1

        ReportSpread_Sheet1.SetText(row, 0, String.Concat("Data Date = ", dtpReportDate.Value.ToShortDateString))
        row += 1
        ReportSpread_Sheet1.RowCount += 1

        ReportSpread_Sheet1.SetText(row, 0, _storeIdName)
        ReportSpread_Sheet1.PrintInfo.Header = _
                String.Concat( _
                    _shortAssemblyName, "  /  v", _fileVersion, _
                    New String(" "c, 10), "Supervisors Authorisation List", _
                    New String(" "c, 10), "Store: ", _storeIdName, _
                    New String(" "c, 10), "Date Printed: ", DateTime.Now.Date.ToString("d"), _
                    "/rPage: /p")

    End Sub

    Private Function ReportLines(ByVal registerNumber As String, ByVal transactionNumber As String, ByRef row As Integer) As Integer

        Dim returnValue As Integer ' returns the number of detail rows printed
        Dim detailData As New DataTable
        Dim reason As String

        Try
            Using con As New Connection
                Using com As New Command(con)
                    com.CommandText = String.Concat( _
                        "SELECT ", _
                            "[SKUN], [QUAN], [LREV], [PORC], ", _
                            "[SUPV], s.[NAME] SuperName, [QSUP], q.[Name] QuarantineSuperName ", _
                        "FROM DLLINE ", _
                        "LEFT OUTER JOIN SystemUsers s ON s.EmployeeCode = [SUPV] ", _
                        "LEFT OUTER JOIN SystemUsers q ON q.EmployeeCode = [QSUP] ", _
                        "WHERE ", _
                            "[DATE1] = @reportingDate AND ", _
                            "[TILL] = @registerNumber AND ", _
                            "[TRAN] = @transactionNumber AND ", _
                            "([SUPV] <> '000' OR [QSUP] <> '000') " _
                    )
                    com.AddParameter("@reportingDate", dtpReportDate.Value)
                    com.AddParameter("@registerNumber", registerNumber)
                    com.AddParameter("@transactionNumber", transactionNumber)
                    detailData = com.ExecuteDataTable()
                End Using
            End Using

            returnValue = detailData.Rows.Count

            For Each detailRow As DataRow In detailData.Rows
                row += 1
                ReportSpread_Sheet1.RowCount += 1
                ReportSpread_Sheet1.SetText(row, 8, CStr(detailRow("SKUN")))

                If Not CStr(detailRow("QSUP")).Equals("000") Then
                    ReportSpread_Sheet1.SetText(row, 5, String.Concat(CStr(detailRow("QSUP")), " ", CStr(detailRow("QuarantineSuperName"))))
                Else
                    ReportSpread_Sheet1.SetText(row, 5, String.Concat(CStr(detailRow("SUPV")), " ", CStr(detailRow("SuperName"))))
                End If
                ReportSpread_Sheet1.SetText(row, 9, CStr(detailRow("QUAN")))

                If CInt(detailRow("PORC")) > 0 Then
                    ReportSpread_Sheet1.SetText(row, 7, "P/Override")
                    If CInt(detailRow("PORC")) > 10 Then
                        ReportSpread_Sheet1.SetText(row, 10, "Mark Down Item")
                    Else
                        Using con As New Connection
                            Using com As New Command(con)
                                com.CommandText = String.Concat("SELECT PORC", CInt(detailRow("PORC")), " FROM RETOPT WHERE FKEY='01'")
                                ReportSpread_Sheet1.SetText(row, 10, CStr(com.ExecuteValue))
                            End Using
                        End Using
                    End If
                Else
                    reason = "Line Rev."

                    If Not CBool(detailRow("LREV")) Then
                        reason = "Obsolete"
                    End If

                    If Not CStr(detailRow("QSUP")).Equals("000") Then
                        reason = "Quarantine"
                    End If
                    ReportSpread_Sheet1.SetText(row, 7, reason)
                End If
            Next
            'row += 1
            'ReportSpread_Sheet1.RowCount += 1
            ReportSpread_Sheet1.OperationMode = Spread.OperationMode.ReadOnly


        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            row += 1
            ReportSpread_Sheet1.RowCount += 1
            ReportSpread_Sheet1.SetText(row, 0, "*** Error in Details ***")
            row += 1
            ReportSpread_Sheet1.RowCount += 1
        Finally
            detailData.Dispose()
        End Try

        Return returnValue

    End Function

    Private Function TranslateType(ByVal transactionCode As String) As String
        Select Case transactionCode
            Case "ZR"
                Return "Z-Read"
            Case "XR"
                Return "X-Read"
            Case "SA"
                Return "Sale  "
            Case "SC"
                Return "S/Corr"
            Case "RF"
                Return "Refund"
            Case "RC"
                Return "R/Corr"
            Case "OD"
                Return "O/Drwr"
            Case "M+"
                Return "Misc +"
            Case "C+"
                Return "+/Corr"
            Case "M-"
                Return "Misc -"
            Case "C-"
                Return "-/Corr"
            Case "CO"
                Return "S/On  "
            Case "CC"
                Return "S/Off "
            Case Else
                Return "??????"
        End Select
    End Function

    Private Sub ReportSpread_PrintMessageBox(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.PrintMessageBoxEventArgs) Handles ReportSpread.PrintMessageBox
        If e.BeginPrinting = False Then _printNotPrinted = False
    End Sub

    Private Sub Finish()
        _printNotPrinted = True
        Do While _printNotPrinted
            Application.DoEvents()
            System.Threading.Thread.Sleep(1)
        Loop
    End Sub

    Private Sub dtpReportDate_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dtpReportDate.ValueChanged
        PrintButton.Enabled = False
    End Sub

    Private Sub MyTrace(ByVal message As String)
        Trace.WriteLine(String.Format("{0}/v{1} - {2}", _shortAssemblyName, _fileVersion, message))
    End Sub


    Private Sub RetrieveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RetrieveButton.Click
        Cursor = Cursors.WaitCursor
        ButtonPanel.Enabled = False
        CollapsibleGroupBox1.Enabled = False
        Print()
        ButtonPanel.Enabled = True
        CollapsibleGroupBox1.Enabled = True
        Cursor = Cursors.Default
        PrintButton.Enabled = True


    End Sub

    Private Sub PrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintButton.Click
        Cursor = Cursors.WaitCursor
        ButtonPanel.Enabled = False
        CollapsibleGroupBox1.Enabled = False

        ReportSpread_Sheet1.PrintInfo.Margin.Header = CInt(ReportSpread_Sheet1.Rows(-1).Height)
        ReportSpread.PrintSheet(0)
        'wait until printing finished
        Finish()
        ButtonPanel.Enabled = True
        CollapsibleGroupBox1.Enabled = True
        Cursor = Cursors.Default

    End Sub

    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click
        FindForm.Close()
    End Sub

End Class
