﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ParkedTransactionsListing
    Inherits Cts.Oasys.WinForm.Form

    'UserControl1 overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TipAppearance1 As FarPoint.Win.Spread.TipAppearance = New FarPoint.Win.Spread.TipAppearance
        Me.PrintButton = New System.Windows.Forms.Button
        Me.dtpReportDate = New System.Windows.Forms.DateTimePicker
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.ReportSpread = New FarPoint.Win.Spread.FpSpread
        Me.ReportSpread_Sheet1 = New FarPoint.Win.Spread.SheetView
        Me.CollapsibleGroupBox1 = New CtsControls.CollapsibleGroupBox
        Me.ButtonPanel = New System.Windows.Forms.Panel
        Me.ExitButton = New System.Windows.Forms.Button
        Me.RetrieveButton = New System.Windows.Forms.Button
        CType(Me.ReportSpread, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReportSpread_Sheet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CollapsibleGroupBox1.SuspendLayout()
        Me.ButtonPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'PrintButton
        '
        Me.PrintButton.Location = New System.Drawing.Point(84, 8)
        Me.PrintButton.Name = "PrintButton"
        Me.PrintButton.Size = New System.Drawing.Size(75, 39)
        Me.PrintButton.TabIndex = 10
        Me.PrintButton.Text = "F9 Print"
        Me.PrintButton.UseVisualStyleBackColor = True
        '
        'dtpReportDate
        '
        Me.dtpReportDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpReportDate.CustomFormat = ""
        Me.dtpReportDate.Location = New System.Drawing.Point(534, 19)
        Me.dtpReportDate.MaxDate = New Date(2050, 12, 31, 0, 0, 0, 0)
        Me.dtpReportDate.MinDate = New Date(1990, 1, 1, 0, 0, 0, 0)
        Me.dtpReportDate.Name = "dtpReportDate"
        Me.dtpReportDate.Size = New System.Drawing.Size(189, 20)
        Me.dtpReportDate.TabIndex = 7
        Me.dtpReportDate.Value = New Date(2008, 12, 19, 0, 0, 0, 0)
        '
        'lblStartDate
        '
        Me.lblStartDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblStartDate.AutoSize = True
        Me.lblStartDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(443, 23)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(85, 13)
        Me.lblStartDate.TabIndex = 6
        Me.lblStartDate.Text = "Trading Date:"
        '
        'ReportSpread
        '
        Me.ReportSpread.About = "3.0.2004.2005"
        Me.ReportSpread.AccessibleDescription = "ReportSpread, Sheet1, Row 0, Column 0, "
        Me.ReportSpread.AllowCellOverflow = True
        Me.ReportSpread.BackColor = System.Drawing.SystemColors.Control
        Me.ReportSpread.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ReportSpread.Location = New System.Drawing.Point(0, 46)
        Me.ReportSpread.Name = "ReportSpread"
        Me.ReportSpread.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ReportSpread.Sheets.AddRange(New FarPoint.Win.Spread.SheetView() {Me.ReportSpread_Sheet1})
        Me.ReportSpread.Size = New System.Drawing.Size(729, 367)
        Me.ReportSpread.TabIndex = 11
        TipAppearance1.BackColor = System.Drawing.SystemColors.Info
        TipAppearance1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.ReportSpread.TextTipAppearance = TipAppearance1
        '
        'ReportSpread_Sheet1
        '
        Me.ReportSpread_Sheet1.Reset()
        Me.ReportSpread_Sheet1.SheetName = "Sheet1"
        'Formulas and custom names must be loaded with R1C1 reference style
        Me.ReportSpread_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1
        Me.ReportSpread_Sheet1.ColumnCount = 0
        Me.ReportSpread_Sheet1.RowCount = 0
        Me.ReportSpread_Sheet1.RowHeader.ColumnCount = 0
        Me.ReportSpread_Sheet1.PrintInfo.Margin.Left = 50
        Me.ReportSpread_Sheet1.PrintInfo.Orientation = FarPoint.Win.Spread.PrintOrientation.Landscape
        Me.ReportSpread_Sheet1.PrintInfo.ShowBorder = False
        Me.ReportSpread_Sheet1.PrintInfo.ShowGrid = False
        Me.ReportSpread_Sheet1.RowHeader.Columns.Default.Resizable = False
        Me.ReportSpread_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1
        Me.ReportSpread.SetActiveViewport(0, 1, 1)
        '
        'CollapsibleGroupBox1
        '
        Me.CollapsibleGroupBox1.Controls.Add(Me.dtpReportDate)
        Me.CollapsibleGroupBox1.Controls.Add(Me.lblStartDate)
        Me.CollapsibleGroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.CollapsibleGroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.CollapsibleGroupBox1.MinHeight = 18
        Me.CollapsibleGroupBox1.MinWidth = 18
        Me.CollapsibleGroupBox1.Name = "CollapsibleGroupBox1"
        Me.CollapsibleGroupBox1.Size = New System.Drawing.Size(729, 46)
        Me.CollapsibleGroupBox1.Style = CtsControls.CollapsibleGroupBox.Styles.Height
        Me.CollapsibleGroupBox1.TabIndex = 18
        Me.CollapsibleGroupBox1.TabStop = False
        Me.CollapsibleGroupBox1.Text = "Selection Criteria"
        '
        'ButtonPanel
        '
        Me.ButtonPanel.Controls.Add(Me.ExitButton)
        Me.ButtonPanel.Controls.Add(Me.RetrieveButton)
        Me.ButtonPanel.Controls.Add(Me.PrintButton)
        Me.ButtonPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ButtonPanel.Location = New System.Drawing.Point(0, 413)
        Me.ButtonPanel.Name = "ButtonPanel"
        Me.ButtonPanel.Size = New System.Drawing.Size(729, 50)
        Me.ButtonPanel.TabIndex = 20
        '
        'ExitButton
        '
        Me.ExitButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ExitButton.Location = New System.Drawing.Point(651, 8)
        Me.ExitButton.Name = "ExitButton"
        Me.ExitButton.Size = New System.Drawing.Size(75, 39)
        Me.ExitButton.TabIndex = 11
        Me.ExitButton.Text = "F12 Exit"
        Me.ExitButton.UseVisualStyleBackColor = True
        '
        'RetrieveButton
        '
        Me.RetrieveButton.Location = New System.Drawing.Point(3, 8)
        Me.RetrieveButton.Name = "RetrieveButton"
        Me.RetrieveButton.Size = New System.Drawing.Size(75, 39)
        Me.RetrieveButton.TabIndex = 0
        Me.RetrieveButton.Text = "F5 Retrieve"
        Me.RetrieveButton.UseVisualStyleBackColor = True
        '
        'ParkedTransactionsListing
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.ReportSpread)
        Me.Controls.Add(Me.ButtonPanel)
        Me.Controls.Add(Me.CollapsibleGroupBox1)
        Me.Name = "ParkedTransactionsListing"
        Me.Size = New System.Drawing.Size(729, 463)
        CType(Me.ReportSpread, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReportSpread_Sheet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CollapsibleGroupBox1.ResumeLayout(False)
        Me.CollapsibleGroupBox1.PerformLayout()
        Me.ButtonPanel.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PrintButton As System.Windows.Forms.Button
    Friend WithEvents dtpReportDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents ReportSpread As FarPoint.Win.Spread.FpSpread
    Friend WithEvents ReportSpread_Sheet1 As FarPoint.Win.Spread.SheetView
    Friend WithEvents CollapsibleGroupBox1 As CtsControls.CollapsibleGroupBox
    Friend WithEvents ButtonPanel As System.Windows.Forms.Panel
    Friend WithEvents RetrieveButton As System.Windows.Forms.Button
    Friend WithEvents ExitButton As System.Windows.Forms.Button
End Class
