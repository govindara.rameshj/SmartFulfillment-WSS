﻿Imports FarPoint.Win
Imports System.Data.SqlClient

Public Class ParkedTransactionsListing
    Private _Proxy As Oasys.DataProxy
    Private _PrintNotPrinted As Boolean
    Private _ReportDate As DateTime
    Private _CalledFromNight As Boolean
    Private _StoreNumber As String
    Private _StoreName As String
    Private _ShortAssemblyName As String
    Private _FileVersion As String

    Private Sub New()
        MyBase.New()
        SetupEnvironment()
    End Sub

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        SetupEnvironment()
    End Sub
    Private Sub SetupEnvironment()
        Dim workDate As String

        With FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetAssembly(Me.GetType).Location)
            _ShortAssemblyName = .FileName.Substring(.FileName.LastIndexOf("\"c) + 1, .FileName.LastIndexOf("."c) - .FileName.LastIndexOf("\"c) - 1)
            _FileVersion = .FileVersion
        End With

        MyTrace("Initialising")
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Dim db As New OasysDBBO.Oasys3.DB.clsOasys3DB()
        _Proxy = CType(Activator.CreateInstance(db.GetConfigValue("ProxyAssembly"), db.GetConfigValue("ProxyTypeName"), False, Reflection.BindingFlags.CreateInstance, Nothing, New Object() {db.GetConfigValue("sqlConnection")}, Globalization.CultureInfo.InvariantCulture, Nothing, Nothing).Unwrap, Oasys.DataProxy)
        workDate = CStr(_Proxy.ExecuteScalar(New SqlCommand("SELECT DOLR FROM RETOPT")))
        _ReportDate = New Date( _
                        CInt(workDate.Substring(6, 2)) + 2000, _
                        CInt(workDate.Substring(3, 2)), _
                        CInt(workDate.Substring(0, 2)) _
                    )
        dtpReportDate.Value = _ReportDate

        With _Proxy.ExecuteDataTable(New SqlCommand("SELECT STOR, SNAM FROM RETOPT WHERE FKEY='01'")).Rows(0)
            _StoreNumber = CStr(.Item("STOR"))
            _StoreName = CStr(.Item("SNAM"))
        End With

        _CalledFromNight = RunParameters.Contains(",CFC")
    End Sub

    Private Sub Print()
        Dim row As Integer = 0
        Dim reportData As DataTable = Nothing
        Dim detailData As DataTable = Nothing

        Dim currentCashier As String = String.Empty

        'Dim transactionTotal As Decimal
        'Dim orderNumber As String

        'Dim lineQuantity As Integer
        'Dim lineValue As Decimal

        Dim parkedSalesCount As Integer = 0
        Dim parkedRefundsCount As Integer = 0
        Dim unsoldOrdersCount As Integer = 0
        Dim unrefundedOrdersCount As Integer = 0
        Dim transactionsCount As Integer = 0

        Dim parkedSalesValue As Decimal = 0
        Dim parkedRefundsValue As Decimal = 0
        Dim unsoldOrdersValue As Decimal = 0
        Dim unrefundedOrdersValue As Decimal = 0
        Dim transactionsValue As Decimal = 0

        Dim parkedSalesLineCount As Integer = 0
        Dim parkedRefundsLineCount As Integer = 0
        Dim unsoldOrdersLineCount As Integer = 0
        Dim unrefundedOrdersLineCount As Integer = 0
        Dim transactionsLineCount As Integer = 0

        Dim eventDiscountsValue As Decimal = 0

        Try
            If ReportSpread_Sheet1.RowCount > 0 Then
                ReportSpread_Sheet1.RemoveRows(0, ReportSpread_Sheet1.RowCount)
            End If

            Using reportCommand As New SqlClient.SqlCommand

                ReportSpread.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                ReportSpread_Sheet1.ColumnCount = 10

                For i As Integer = 0 To ReportSpread_Sheet1.ColumnCount - 1
                    ReportSpread_Sheet1.Columns(i).CellType = New Spread.CellType.TextCellType
                Next

                ReportSpread_Sheet1.Columns(0).Width = 216
                ReportSpread_Sheet1.Columns(1).Width = 48
                ReportSpread_Sheet1.Columns(2).Width = 56
                ReportSpread_Sheet1.Columns(3).Width = 132
                ReportSpread_Sheet1.Columns(4).Width = 66
                ReportSpread_Sheet1.Columns(5).Width = 92
                ReportSpread_Sheet1.Columns(6).Width = 58
                ReportSpread_Sheet1.Columns(7).Width = 92
                ReportSpread_Sheet1.Columns(8).Width = 183
                ReportSpread_Sheet1.Columns(9).Width = 68

                ReportSpread_Sheet1.Columns(0).Label = "CASHIER"
                ReportSpread_Sheet1.Columns(1).Label = "TILL"
                ReportSpread_Sheet1.Columns(2).Label = "TRAN"
                ReportSpread_Sheet1.Columns(3).Label = "OVC TRAN"
                ReportSpread_Sheet1.Columns(4).Label = "SKU NO"
                ReportSpread_Sheet1.Columns(5).Label = "PRICE"
                ReportSpread_Sheet1.Columns(6).Label = "QUAN"
                ReportSpread_Sheet1.Columns(7).Label = "EXT VALUE"
                ReportSpread_Sheet1.Columns(8).Label = "CUSTOMER NAME"
                ReportSpread_Sheet1.Columns(9).Label = "ORDER"

                ReportSpread_Sheet1.Columns(0).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(1).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(2).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(3).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(4).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(5).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
                ReportSpread_Sheet1.Columns(6).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
                ReportSpread_Sheet1.Columns(7).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
                ReportSpread_Sheet1.Columns(8).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(9).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left

                'set up headings 
                ReportSpread_Sheet1.RowCount = 1
                row = 0

                MakeReportRecalledTrans("0", "1", reportCommand, row, transactionsCount, parkedSalesCount, parkedRefundsCount, parkedSalesValue, parkedRefundsValue, unsoldOrdersValue, unrefundedOrdersValue, transactionsValue, unsoldOrdersCount, unrefundedOrdersCount, parkedSalesLineCount, parkedRefundsLineCount, unsoldOrdersLineCount, unrefundedOrdersLineCount, transactionsLineCount, eventDiscountsValue)

                row += 2
                ReportSpread_Sheet1.RowCount += 2

                MakeReportRecalledTrans("1", "0", reportCommand, row, transactionsCount, parkedSalesCount, parkedRefundsCount, parkedSalesValue, parkedRefundsValue, unsoldOrdersValue, unrefundedOrdersValue, transactionsValue, unsoldOrdersCount, unrefundedOrdersCount, parkedSalesLineCount, parkedRefundsLineCount, unsoldOrdersLineCount, unrefundedOrdersLineCount, transactionsLineCount, eventDiscountsValue)

                row += 2
                ReportSpread_Sheet1.RowCount += 2


                ReportSpread_Sheet1.SetText(row, 0, "Summary:")
                ReportSpread_Sheet1.SetText(row, 4, "No. Of")
                ReportSpread_Sheet1.SetText(row, 5, "No. Of")
                ReportSpread_Sheet1.SetText(row, 6, "Total")
                row += 1
                ReportSpread_Sheet1.RowCount += 1

                ReportSpread_Sheet1.SetText(row, 4, "Transactions")
                ReportSpread_Sheet1.SetText(row, 5, "Lines")
                ReportSpread_Sheet1.SetText(row, 6, "Value")
                ReportSpread_Sheet1.Cells(row, 4, row, 6).Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
                row += 1
                ReportSpread_Sheet1.RowCount += 1

                ReportSpread_Sheet1.Cells(row, 3).HorizontalAlignment = Spread.CellHorizontalAlignment.Right
                ReportSpread_Sheet1.SetText(row, 3, "Sales Parked")
                ReportSpread_Sheet1.SetText(row, 4, parkedSalesCount.ToString)
                ReportSpread_Sheet1.SetText(row, 5, parkedSalesLineCount.ToString)
                ReportSpread_Sheet1.SetText(row, 6, parkedSalesValue.ToString("0.00"))
                row += 1
                ReportSpread_Sheet1.RowCount += 1

                ReportSpread_Sheet1.Cells(row, 3).HorizontalAlignment = Spread.CellHorizontalAlignment.Right
                ReportSpread_Sheet1.SetText(row, 3, "Refunds Parked")
                ReportSpread_Sheet1.SetText(row, 4, parkedRefundsCount.ToString)
                ReportSpread_Sheet1.SetText(row, 5, parkedRefundsLineCount.ToString)
                ReportSpread_Sheet1.SetText(row, 6, parkedRefundsValue.ToString("0.00"))
                row += 1
                ReportSpread_Sheet1.RowCount += 1

                ReportSpread_Sheet1.Cells(row, 3).HorizontalAlignment = Spread.CellHorizontalAlignment.Right
                ReportSpread_Sheet1.SetText(row, 3, "Total Parked")
                ReportSpread_Sheet1.SetText(row, 4, transactionsCount.ToString)
                ReportSpread_Sheet1.SetText(row, 5, transactionsLineCount.ToString)
                ReportSpread_Sheet1.SetText(row, 6, transactionsValue.ToString("0.00"))
                row += 2
                ReportSpread_Sheet1.RowCount += 2

                ReportSpread_Sheet1.Cells(row, 3).HorizontalAlignment = Spread.CellHorizontalAlignment.Right
                ReportSpread_Sheet1.SetText(row, 3, "Customer Orders Not Sold")
                ReportSpread_Sheet1.SetText(row, 4, unsoldOrdersCount.ToString)
                ReportSpread_Sheet1.SetText(row, 5, unsoldOrdersLineCount.ToString)
                ReportSpread_Sheet1.SetText(row, 6, unsoldOrdersValue.ToString("0.00"))
                row += 1
                ReportSpread_Sheet1.RowCount += 1

                ReportSpread_Sheet1.Cells(row, 3).HorizontalAlignment = Spread.CellHorizontalAlignment.Right
                ReportSpread_Sheet1.SetText(row, 3, "Customer Orders Not Refunded")
                ReportSpread_Sheet1.SetText(row, 4, unrefundedOrdersCount.ToString)
                ReportSpread_Sheet1.SetText(row, 5, unrefundedOrdersLineCount.ToString)
                ReportSpread_Sheet1.SetText(row, 6, unrefundedOrdersValue.ToString("0.00"))
                row += 2
                ReportSpread_Sheet1.RowCount += 2

                ReportSpread_Sheet1.SetText(row, 0, String.Concat("Data Date = ", _ReportDate.ToShortDateString))
                row += 1
                ReportSpread_Sheet1.RowCount += 1

                ReportSpread_Sheet1.SetText(row, 0, _StoreName)
            End Using

            ReportSpread_Sheet1.PrintInfo.Header = _
                    String.Concat( _
                        _ShortAssemblyName, "  /  v", _FileVersion, _
                        New String(" "c, 10), "Parked Transactions Listing", _
                        New String(" "c, 10), "Store: ", _StoreNumber, "-"c, _StoreName, _
                        New String(" "c, 10), "Date Printed: ", DateTime.Now.Date.ToString("d"), _
                        "/rPage: /p" _
                   )
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        Finally
            If reportData IsNot Nothing Then
                reportData.Dispose()
                reportData = Nothing
            End If
            If detailData IsNot Nothing Then
                detailData.Dispose()
                reportData = Nothing
            End If
        End Try
    End Sub

    Private Sub ReportSpread_ColumnWidthChanged(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.ColumnWidthChangedEventArgs) Handles ReportSpread.ColumnWidthChanged
        Debug.WriteLine("Report Column Widths:")
        For i As Integer = 0 To ReportSpread_Sheet1.ColumnCount - 1
            Debug.WriteLine(String.Format("ReportSpread_Sheet1.Columns({0}).Width = {1}", i, ReportSpread_Sheet1.Columns(i).Width))
        Next
    End Sub

    Private Sub ReportSpread_PrintMessageBox(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.PrintMessageBoxEventArgs) Handles ReportSpread.PrintMessageBox
        If e.BeginPrinting = False Then _PrintNotPrinted = False
    End Sub

    Private Sub Finish()
        _PrintNotPrinted = True
        Do While _PrintNotPrinted
            Application.DoEvents()
            System.Threading.Thread.Sleep(1)
        Loop
    End Sub

    Private Sub ParentForm_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs)
        RetrieveButton.PerformClick()
    End Sub

    Private Sub RetrieveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RetrieveButton.Click
        Cursor = Cursors.WaitCursor
        ButtonPanel.Enabled = False
        CollapsibleGroupBox1.Enabled = False
        Print()
        ButtonPanel.Enabled = True
        CollapsibleGroupBox1.Enabled = True
        Cursor = Cursors.Default
        PrintButton.Enabled = True

        If _CalledFromNight Then PrintButton.PerformClick()
    End Sub

    Private Sub PrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintButton.Click
        Cursor = Cursors.WaitCursor
        ButtonPanel.Enabled = False
        CollapsibleGroupBox1.Enabled = False

        ReportSpread_Sheet1.PrintInfo.Margin.Header = CInt(ReportSpread_Sheet1.Rows(-1).Height)
        ReportSpread.PrintSheet(0)
        'wait until printing finished
        Finish()
        ButtonPanel.Enabled = True
        CollapsibleGroupBox1.Enabled = True
        Cursor = Cursors.Default

        If _CalledFromNight Then ExitButton.PerformClick()
    End Sub

    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click
        FindForm.Close()
    End Sub

    Private Sub _KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs)
        Select Case e.KeyCode
            Case Keys.F5
                RetrieveButton.PerformClick()
            Case Keys.F9
                PrintButton.PerformClick()
            Case Keys.F12
                ExitButton.PerformClick()
        End Select
    End Sub

    Private Sub dtpReportDate_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dtpReportDate.ValueChanged
        _ReportDate = CType(sender, DateTimePicker).Value.Date
        PrintButton.Enabled = False
    End Sub

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        AddHandler FindForm.Shown, AddressOf ParentForm_Shown
        AddHandler FindForm.KeyDown, AddressOf _KeyDown
        FindForm.KeyPreview = True
        ReportSpread_Sheet1.OperationMode = Spread.OperationMode.ReadOnly
    End Sub

    Private Sub MyTrace(ByVal message As String)
        Trace.WriteLine(String.Format("{0}/v{1} - {2}", _ShortAssemblyName, _FileVersion, message))
    End Sub

    Private Sub MakeReportRecalledTrans(ByVal strRecallMode As String, _
                                        ByVal strParkedMode As String, _
                                        ByVal reportCommand As SqlClient.SqlCommand, _
                                        ByRef row As Integer, _
                                        ByRef transactionsCount As Integer, _
                                        ByRef parkedSalesCount As Integer, _
                                        ByRef parkedRefundsCount As Integer, _
                                        ByRef parkedSalesValue As Decimal, _
                                        ByRef parkedRefundsValue As Decimal, _
                                        ByRef unsoldOrdersValue As Decimal, _
                                        ByRef unrefundedOrdersValue As Decimal, _
                                        ByRef transactionsValue As Decimal, _
                                        ByRef unsoldOrdersCount As Integer, _
                                        ByRef unrefundedOrdersCount As Integer, _
                                        ByRef parkedSalesLineCount As Integer, _
                                        ByRef parkedRefundsLineCount As Integer, _
                                        ByRef unsoldOrdersLineCount As Integer, _
                                        ByRef unrefundedOrdersLineCount As Integer, _
                                        ByRef transactionsLineCount As Integer, _
                                        ByRef eventDiscountsValue As Decimal)

        Dim reportData As DataTable = Nothing
        Dim detailData As DataTable = Nothing

        Dim currentCashier As String = String.Empty

        Dim transactionTotal As Decimal
        Dim orderNumber As String

        Dim lineQuantity As Integer
        Dim lineValue As Decimal

        If strRecallMode = "0" Then
            ReportSpread_Sheet1.SetText(row, 0, "Non Recalled Transactions:")
        Else
            ReportSpread_Sheet1.SetText(row, 0, "Recalled Transactions:")
        End If
        row += 1
        ReportSpread_Sheet1.RowCount += 1

        reportCommand.CommandText = String.Concat( _
                     "SELECT ", _
                         "dt.[CASH], Coalesce(su.[Name], '<-- User NOT on File') [CashierName], ", _
                         "dt.[TILL], dt.[TRAN], dt.OVCTranNumber, dt.[ORDN], dt.[TOTL], ", _
                         "Coalesce(dr.[Name], '*** Customer Unknown ***') CustomerName ", _
                     "FROM vwDLTOTS dt ", _
                     "LEFT OUTER JOIN SystemUsers su ON ", _
                         "su.[EmployeeCode] = dt.[CASH] ", _
                     "LEFT OUTER JOIN CORHDR ch ON ", _
                         "ch.[NUMB] = dt.[ORDN] ", _
                     "LEFT OUTER JOIN QUOHDR q1 ON ", _
                         "q1.[NUMB] = dt.[ORDN] ", _
                     "LEFT OUTER JOIN DLRCUS dr ON ", _
                         "dr.[DATE1] = dt.[DATE1] AND ", _
                         "dr.[TILL] = dt.[TILL] AND ", _
                         "dr.[TRAN] = dt.[TRAN] AND ", _
                         "dr.[NUMB] = 0 ", _
                     "WHERE ", _
                         "dt.[DATE1] = @reportingDate AND ", _
                         "dt.[CASH] <> '000' AND ", _
                         "dt.[PARK] = " & strParkedMode & " AND ", _
                         "dt.[PKRC] = " & strRecallMode & " AND ", _
                         "(dt.ORDN = '00000' OR q1.[NUMB] IS NULL OR q1.[SOLD] = 1 OR ch.NUMB IS NOT NULL) ", _
                     "ORDER BY [CASH], [TIME], [TILL], [TRAN] " _
                 )
        reportCommand.Parameters.Add("@reportingDate", SqlDbType.DateTime).Value = _ReportDate

        MyTrace("Acquiring data")
        reportData = _Proxy.ExecuteDataTable(reportCommand)
        reportCommand.Parameters.Clear()

        MyTrace("Generating report")

        For Each reportRow As DataRow In reportData.Rows
            reportCommand.CommandText = String.Concat( _
                "SELECT ", _
                    "[SKUN], [PRIC], [QUAN], [EXTP], ", _
                    "[QBPD], [DGPD], [MBPD], [HSPD], [ESEV] ", _
                "FROM DLLINE ", _
                "WHERE ", _
                    "[DATE1] = @reportingDate AND ", _
                    "[TILL] = @reportingTill AND ", _
                    "[TRAN] = @reportingTran ", _
                "ORDER BY NUMB " _
            )

            reportCommand.Parameters.Add("@reportingDate", SqlDbType.DateTime).Value = _ReportDate
            reportCommand.Parameters.Add("@reportingTill", SqlDbType.Char, 2).Value = CStr(reportRow("TILL"))
            reportCommand.Parameters.Add("@reportingTran", SqlDbType.Char, 4).Value = CStr(reportRow("TRAN"))
            MyTrace(String.Format("Reading lines for till/transaction {0}/{1}", CStr(reportRow("TILL")), CStr(reportRow("TRAN"))))
            If detailData IsNot Nothing Then
                detailData.Dispose()
                detailData = Nothing
            End If
            detailData = _Proxy.ExecuteDataTable(reportCommand)
            reportCommand.Parameters.Clear()

            transactionTotal = CDec(reportRow("TOTL"))
            orderNumber = CStr(reportRow("ORDN"))

            If orderNumber = "000000" Then
                transactionsCount += 1
                If transactionTotal > 0 Then
                    parkedSalesCount += 1
                Else
                    parkedRefundsCount += 1
                End If
            Else
                If transactionTotal > 0 Then
                    unsoldOrdersCount += 1
                Else
                    unrefundedOrdersCount += 1
                End If
            End If

            If currentCashier <> CStr(reportRow("CASH")) Then
                currentCashier = CStr(reportRow("CASH"))

                ReportSpread_Sheet1.SetText(row, 0, String.Format("{0} {1}", currentCashier, CStr(reportRow("CashierName"))))
            End If
            ReportSpread_Sheet1.SetText(row, 1, CStr(reportRow("TILL")))
            ReportSpread_Sheet1.SetText(row, 2, CStr(reportRow("TRAN")))

            If Not Equals(reportRow("OVCTranNumber"), DBNull.Value) Then
                ReportSpread_Sheet1.SetText(row, 3, CStr(reportRow("OVCTranNumber")))
            End If

            ReportSpread_Sheet1.SetText(row, 8, CStr(reportRow("CustomerName")))
            If orderNumber <> "000000" Then
                ReportSpread_Sheet1.SetText(row, 9, orderNumber)
            End If

            eventDiscountsValue = 0

            For Each detailRow As DataRow In detailData.Rows
                lineQuantity = CInt(detailRow("QUAN"))
                lineValue = CDec(detailRow("EXTP"))

                ReportSpread_Sheet1.SetText(row, 4, CStr(detailRow("SKUN")))
                ReportSpread_Sheet1.SetText(row, 5, CDec(detailRow("PRIC")).ToString("0.00"))
                ReportSpread_Sheet1.SetText(row, 6, CDec(detailRow("QUAN")).ToString())
                ReportSpread_Sheet1.SetText(row, 7, CDec(detailRow("EXTP")).ToString("0.00"))

                If orderNumber = "000000" Then
                    transactionsLineCount += 1
                    transactionsValue += lineValue
                    If lineQuantity > 0 Then
                        parkedSalesLineCount += 1
                        parkedSalesValue += lineValue
                    Else
                        parkedRefundsLineCount += 1
                        parkedRefundsValue += lineValue
                    End If
                Else
                    If lineQuantity > 0 Then
                        unsoldOrdersLineCount += 1
                        unsoldOrdersValue += lineValue
                    Else
                        unrefundedOrdersLineCount += 1
                        unrefundedOrdersValue += lineValue
                    End If
                End If
                eventDiscountsValue += CDec(detailRow("QBPD"))
                eventDiscountsValue += CDec(detailRow("DGPD"))
                eventDiscountsValue += CDec(detailRow("MBPD"))
                eventDiscountsValue += CDec(detailRow("HSPD"))
                eventDiscountsValue += CDec(detailRow("ESEV"))

                row += 1
                ReportSpread_Sheet1.RowCount += 1
            Next
            If eventDiscountsValue <> 0 Then
                ReportSpread_Sheet1.SetText(row, 6, "( Event Discounts:")
                ReportSpread_Sheet1.SetText(row, 7, eventDiscountsValue.ToString("0.00"))
                ReportSpread_Sheet1.SetText(row, 8, ")")
                row += 1
                ReportSpread_Sheet1.RowCount += 1
            End If
            row += 1
            ReportSpread_Sheet1.RowCount += 1
        Next

    End Sub

End Class
