﻿namespace RetailSalesReports
{
    partial class RetailSalesReports : Cts.Oasys.WinForm.Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            FarPoint.Win.Spread.TipAppearance tipAppearance1 = new FarPoint.Win.Spread.TipAppearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RetailSalesReports));
            this.ReportSpread = new FarPoint.Win.Spread.FpSpread();
            this.ReportSpread_Sheet1 = new FarPoint.Win.Spread.SheetView();
            this.TypeGroupBox = new System.Windows.Forms.GroupBox();
            this.TillTypeRadioButton = new System.Windows.Forms.RadioButton();
            this.CashierTypeRadioButton = new System.Windows.Forms.RadioButton();
            this.LevelGroupBox = new System.Windows.Forms.GroupBox();
            this.SummaryLevelRadioButton = new System.Windows.Forms.RadioButton();
            this.DetailsLevelRadioButton = new System.Windows.Forms.RadioButton();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.dtpReportDate = new System.Windows.Forms.DateTimePicker();
            this.CollapsibleGroupBox1 = new CtsControls.CollapsibleGroupBox();
            this.ChoiceLabel = new System.Windows.Forms.Label();
            this.ChoiceComboBox = new PCI.Controls.ComboBoxWithLabel();
            this.ButtonPanel = new System.Windows.Forms.Panel();
            this.ExitButton = new System.Windows.Forms.Button();
            this.RetrieveButton = new System.Windows.Forms.Button();
            this.PrintButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ReportSpread)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportSpread_Sheet1)).BeginInit();
            this.TypeGroupBox.SuspendLayout();
            this.LevelGroupBox.SuspendLayout();
            this.CollapsibleGroupBox1.SuspendLayout();
            this.ButtonPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ReportSpread
            // 
            this.ReportSpread.About = "3.0.2004.2005";
            this.ReportSpread.AccessibleDescription = "ReportSpread, Sheet1, Row 0, Column 0, ";
            this.ReportSpread.AllowCellOverflow = true;
            this.ReportSpread.BackColor = System.Drawing.SystemColors.Control;
            this.ReportSpread.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReportSpread.Location = new System.Drawing.Point(0, 87);
            this.ReportSpread.Name = "ReportSpread";
            this.ReportSpread.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ReportSpread.Sheets.AddRange(new FarPoint.Win.Spread.SheetView[] {
            this.ReportSpread_Sheet1});
            this.ReportSpread.Size = new System.Drawing.Size(721, 299);
            this.ReportSpread.TabIndex = 11;
            tipAppearance1.BackColor = System.Drawing.SystemColors.Info;
            tipAppearance1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tipAppearance1.ForeColor = System.Drawing.SystemColors.InfoText;
            this.ReportSpread.TextTipAppearance = tipAppearance1;
            this.ReportSpread.ColumnWidthChanged += new FarPoint.Win.Spread.ColumnWidthChangedEventHandler(this.ReportSpread_ColumnWidthChanged);
            this.ReportSpread.PrintMessageBox += new FarPoint.Win.Spread.PrintMessageBoxEventHandler(this.ReportSpread_PrintMessageBox);
            // 
            // ReportSpread_Sheet1
            // 
            this.ReportSpread_Sheet1.Reset();
            this.ReportSpread_Sheet1.SheetName = "Sheet1";
            // Formulas and custom names must be loaded with R1C1 reference style
            this.ReportSpread_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1;
            this.ReportSpread_Sheet1.ColumnCount = 0;
            this.ReportSpread_Sheet1.RowCount = 0;
            this.ReportSpread_Sheet1.RowHeader.ColumnCount = 0;
            this.ReportSpread_Sheet1.PrintInfo.Margin.Bottom = 30;
            this.ReportSpread_Sheet1.PrintInfo.Margin.Header = 30;
            this.ReportSpread_Sheet1.PrintInfo.Margin.Left = 30;
            this.ReportSpread_Sheet1.PrintInfo.Margin.Right = 30;
            this.ReportSpread_Sheet1.PrintInfo.Margin.Top = 40;
            this.ReportSpread_Sheet1.PrintInfo.ShowBorder = false;
            this.ReportSpread_Sheet1.PrintInfo.ShowGrid = false;
            this.ReportSpread_Sheet1.RowHeader.Columns.Default.Resizable = false;
            this.ReportSpread_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1;
            this.ReportSpread.SetActiveViewport(0, 1, 1);
            // 
            // TypeGroupBox
            // 
            this.TypeGroupBox.Controls.Add(this.TillTypeRadioButton);
            this.TypeGroupBox.Controls.Add(this.CashierTypeRadioButton);
            this.TypeGroupBox.Location = new System.Drawing.Point(6, 19);
            this.TypeGroupBox.Name = "TypeGroupBox";
            this.TypeGroupBox.Size = new System.Drawing.Size(78, 60);
            this.TypeGroupBox.TabIndex = 12;
            this.TypeGroupBox.TabStop = false;
            this.TypeGroupBox.Text = "Type";
            // 
            // TillTypeRadioButton
            // 
            this.TillTypeRadioButton.AutoSize = true;
            this.TillTypeRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TillTypeRadioButton.Location = new System.Drawing.Point(6, 37);
            this.TillTypeRadioButton.Name = "TillTypeRadioButton";
            this.TillTypeRadioButton.Size = new System.Drawing.Size(42, 17);
            this.TillTypeRadioButton.TabIndex = 13;
            this.TillTypeRadioButton.Text = "Till";
            this.TillTypeRadioButton.UseVisualStyleBackColor = true;
            // 
            // CashierTypeRadioButton
            // 
            this.CashierTypeRadioButton.AutoSize = true;
            this.CashierTypeRadioButton.Checked = true;
            this.CashierTypeRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CashierTypeRadioButton.Location = new System.Drawing.Point(6, 15);
            this.CashierTypeRadioButton.Name = "CashierTypeRadioButton";
            this.CashierTypeRadioButton.Size = new System.Drawing.Size(67, 17);
            this.CashierTypeRadioButton.TabIndex = 0;
            this.CashierTypeRadioButton.TabStop = true;
            this.CashierTypeRadioButton.Text = "Cashier";
            this.CashierTypeRadioButton.UseVisualStyleBackColor = true;
            this.CashierTypeRadioButton.CheckedChanged += new System.EventHandler(this.ListingTypeRadioButton_CheckedChanged);
            // 
            // LevelGroupBox
            // 
            this.LevelGroupBox.Controls.Add(this.SummaryLevelRadioButton);
            this.LevelGroupBox.Controls.Add(this.DetailsLevelRadioButton);
            this.LevelGroupBox.Location = new System.Drawing.Point(90, 19);
            this.LevelGroupBox.Name = "LevelGroupBox";
            this.LevelGroupBox.Size = new System.Drawing.Size(86, 60);
            this.LevelGroupBox.TabIndex = 13;
            this.LevelGroupBox.TabStop = false;
            this.LevelGroupBox.Text = "Level";
            // 
            // SummaryLevelRadioButton
            // 
            this.SummaryLevelRadioButton.AutoSize = true;
            this.SummaryLevelRadioButton.Checked = true;
            this.SummaryLevelRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SummaryLevelRadioButton.Location = new System.Drawing.Point(6, 16);
            this.SummaryLevelRadioButton.Name = "SummaryLevelRadioButton";
            this.SummaryLevelRadioButton.Size = new System.Drawing.Size(75, 17);
            this.SummaryLevelRadioButton.TabIndex = 13;
            this.SummaryLevelRadioButton.TabStop = true;
            this.SummaryLevelRadioButton.Text = "Summary";
            this.SummaryLevelRadioButton.UseVisualStyleBackColor = true;
            this.SummaryLevelRadioButton.CheckedChanged += new System.EventHandler(this.SummaryLevelRadioButton_CheckedChanged);
            // 
            // DetailsLevelRadioButton
            // 
            this.DetailsLevelRadioButton.AutoSize = true;
            this.DetailsLevelRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DetailsLevelRadioButton.Location = new System.Drawing.Point(6, 37);
            this.DetailsLevelRadioButton.Name = "DetailsLevelRadioButton";
            this.DetailsLevelRadioButton.Size = new System.Drawing.Size(64, 17);
            this.DetailsLevelRadioButton.TabIndex = 0;
            this.DetailsLevelRadioButton.Text = "Details";
            this.DetailsLevelRadioButton.UseVisualStyleBackColor = true;
            // 
            // lblStartDate
            // 
            this.lblStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartDate.Location = new System.Drawing.Point(435, 36);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(85, 13);
            this.lblStartDate.TabIndex = 15;
            this.lblStartDate.Text = "Trading Date:";
            // 
            // dtpReportDate
            // 
            this.dtpReportDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpReportDate.CustomFormat = "";
            this.dtpReportDate.Location = new System.Drawing.Point(526, 32);
            this.dtpReportDate.MaxDate = new System.DateTime(2050, 12, 31, 0, 0, 0, 0);
            this.dtpReportDate.MinDate = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            this.dtpReportDate.Name = "dtpReportDate";
            this.dtpReportDate.Size = new System.Drawing.Size(189, 20);
            this.dtpReportDate.TabIndex = 18;
            this.dtpReportDate.Value = new System.DateTime(2008, 12, 19, 0, 0, 0, 0);
            this.dtpReportDate.ValueChanged += new System.EventHandler(this.dtpReportDate_ValueChanged);
            // 
            // CollapsibleGroupBox1
            // 
            this.CollapsibleGroupBox1.Controls.Add(this.TypeGroupBox);
            this.CollapsibleGroupBox1.Controls.Add(this.LevelGroupBox);
            this.CollapsibleGroupBox1.Controls.Add(this.dtpReportDate);
            this.CollapsibleGroupBox1.Controls.Add(this.lblStartDate);
            this.CollapsibleGroupBox1.Controls.Add(this.ChoiceLabel);
            this.CollapsibleGroupBox1.Controls.Add(this.ChoiceComboBox);
            this.CollapsibleGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.CollapsibleGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.CollapsibleGroupBox1.MinHeight = 18;
            this.CollapsibleGroupBox1.MinWidth = 18;
            this.CollapsibleGroupBox1.Name = "CollapsibleGroupBox1";
            this.CollapsibleGroupBox1.Size = new System.Drawing.Size(721, 87);
            this.CollapsibleGroupBox1.Style = CtsControls.CollapsibleGroupBox.Styles.Height;
            this.CollapsibleGroupBox1.TabIndex = 19;
            this.CollapsibleGroupBox1.TabStop = false;
            this.CollapsibleGroupBox1.Text = "Selection Criteria";
            // 
            // ChoiceLabel
            // 
            this.ChoiceLabel.AutoSize = true;
            this.ChoiceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChoiceLabel.Location = new System.Drawing.Point(182, 58);
            this.ChoiceLabel.Name = "ChoiceLabel";
            this.ChoiceLabel.Size = new System.Drawing.Size(53, 13);
            this.ChoiceLabel.TabIndex = 19;
            this.ChoiceLabel.Text = "Cashier:";
            this.ChoiceLabel.Visible = false;
            // 
            // ChoiceComboBox
            // 
            this.ChoiceComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ChoiceComboBox.DroppedDown = false;
            this.ChoiceComboBox.ItemTable = null;
            this.ChoiceComboBox.Location = new System.Drawing.Point(241, 56);
            this.ChoiceComboBox.MaxLength = 0;
            this.ChoiceComboBox.Name = "ChoiceComboBox";
            this.ChoiceComboBox.SelectedIndex = -1;
            this.ChoiceComboBox.Size = new System.Drawing.Size(259, 21);
            this.ChoiceComboBox.TabIndex = 14;
            this.ChoiceComboBox.Visible = false;
            this.ChoiceComboBox.SelectedIndexChanged += new System.EventHandler<System.EventArgs>(this.ChoiceComboBox_SelectedIndexChanged);
            // 
            // ButtonPanel
            // 
            this.ButtonPanel.Controls.Add(this.ExitButton);
            this.ButtonPanel.Controls.Add(this.RetrieveButton);
            this.ButtonPanel.Controls.Add(this.PrintButton);
            this.ButtonPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ButtonPanel.Location = new System.Drawing.Point(0, 386);
            this.ButtonPanel.Name = "ButtonPanel";
            this.ButtonPanel.Size = new System.Drawing.Size(721, 50);
            this.ButtonPanel.TabIndex = 21;
            // 
            // ExitButton
            // 
            this.ExitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ExitButton.Location = new System.Drawing.Point(643, 8);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(75, 39);
            this.ExitButton.TabIndex = 11;
            this.ExitButton.Text = "F12 Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // RetrieveButton
            // 
            this.RetrieveButton.Location = new System.Drawing.Point(3, 8);
            this.RetrieveButton.Name = "RetrieveButton";
            this.RetrieveButton.Size = new System.Drawing.Size(75, 39);
            this.RetrieveButton.TabIndex = 0;
            this.RetrieveButton.Text = "F5 Retrieve";
            this.RetrieveButton.UseVisualStyleBackColor = true;
            this.RetrieveButton.Click += new System.EventHandler(this.RetrieveButton_Click);
            // 
            // PrintButton
            // 
            this.PrintButton.Location = new System.Drawing.Point(84, 8);
            this.PrintButton.Name = "PrintButton";
            this.PrintButton.Size = new System.Drawing.Size(75, 39);
            this.PrintButton.TabIndex = 10;
            this.PrintButton.Text = "F9 Print";
            this.PrintButton.UseVisualStyleBackColor = true;
            this.PrintButton.Click += new System.EventHandler(this.PrintButton_Click);
            // 
            // RetailSalesReports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ReportSpread);
            this.Controls.Add(this.CollapsibleGroupBox1);
            this.Controls.Add(this.ButtonPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RetailSalesReports";
            this.Size = new System.Drawing.Size(721, 436);
            ((System.ComponentModel.ISupportInitialize)(this.ReportSpread)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportSpread_Sheet1)).EndInit();
            this.TypeGroupBox.ResumeLayout(false);
            this.TypeGroupBox.PerformLayout();
            this.LevelGroupBox.ResumeLayout(false);
            this.LevelGroupBox.PerformLayout();
            this.CollapsibleGroupBox1.ResumeLayout(false);
            this.CollapsibleGroupBox1.PerformLayout();
            this.ButtonPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        internal FarPoint.Win.Spread.FpSpread ReportSpread;
        internal FarPoint.Win.Spread.SheetView ReportSpread_Sheet1;
        internal System.Windows.Forms.GroupBox TypeGroupBox;
        internal System.Windows.Forms.RadioButton CashierTypeRadioButton;
        internal System.Windows.Forms.RadioButton TillTypeRadioButton;
        internal System.Windows.Forms.GroupBox LevelGroupBox;
        internal System.Windows.Forms.RadioButton SummaryLevelRadioButton;
        internal System.Windows.Forms.RadioButton DetailsLevelRadioButton;
        internal System.Windows.Forms.Label lblStartDate;
        internal System.Windows.Forms.DateTimePicker dtpReportDate;
        internal CtsControls.CollapsibleGroupBox CollapsibleGroupBox1;
        internal System.Windows.Forms.Panel ButtonPanel;
        internal System.Windows.Forms.Button ExitButton;
        internal System.Windows.Forms.Button RetrieveButton;
        internal System.Windows.Forms.Button PrintButton;
        internal System.Windows.Forms.Label ChoiceLabel;

        internal PCI.Controls.ComboBoxWithLabel ChoiceComboBox;

        #endregion
    }
}