﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Data.SqlClient;
using RetailSalesReports.DAL;
using FarPoint.Win.Spread;

namespace RetailSalesReports
{
    public partial class RetailSalesReports : Cts.Oasys.WinForm.Form
    {
        private Oasys.DataProxy dataProxy;
        private bool printNotPrinted;
        private DateTime reportDate;
        private bool nightJob;
        private string storeNumber;
        private string storeName;
        private string shortAssemblyName;

        private DataAccessor accessor;

        private string _FileVersion;
        public RetailSalesReports(int userId, int workstationId, int securityLevel, string RunParameters)
            : base(userId, workstationId, securityLevel, RunParameters)
        {
            SetupEnvironment();
            SetupPrint();
        }

        private void SetupEnvironment()
        {
            var version = FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetAssembly(this.GetType()).Location);
            shortAssemblyName = version.FileName.Substring(version.FileName.LastIndexOf('\\') + 1, version.FileName.LastIndexOf('.') - version.FileName.LastIndexOf('\\') - 1);
            _FileVersion = version.FileVersion;

            MyTrace("Initialising");
            // This call is required by the Windows Form Designer.
            InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            OasysDBBO.Oasys3.DB.clsOasys3DB db = new OasysDBBO.Oasys3.DB.clsOasys3DB();
            dataProxy = (Oasys.DataProxy)Activator.CreateInstance(db.GetConfigValue("ProxyAssembly"), db.GetConfigValue("ProxyTypeName"), false, System.Reflection.BindingFlags.CreateInstance, null, new object[] { db.GetConfigValue("sqlConnection") }, System.Globalization.CultureInfo.InvariantCulture, null, null).Unwrap();

            reportDate = Convert.ToDateTime(dataProxy.ExecuteScalar(new SqlCommand("SELECT TODT FROM SYSDAT WHERE FKEY = '01'")));
            dtpReportDate.Value = reportDate;

            var row = dataProxy.ExecuteDataTable(new SqlCommand("SELECT STOR, SNAM FROM RETOPT WHERE FKEY='01'")).Rows[0];
            storeNumber = Convert.ToString(row["STOR"]);
            storeName = Convert.ToString(row["SNAM"]);

            nightJob = RunParameters.Contains(",CFC");

            accessor = new DataAccessor(shortAssemblyName, _FileVersion);
        }

        private void SetupPrint()
        {
            ReportSpread_Sheet1.PrintInfo.UseSmartPrint = true;
            SmartPrintRulesCollection printrules = new SmartPrintRulesCollection();
            printrules.Add(new LandscapeRule(ResetOption.None));
            printrules.Add(new ScaleRule(ResetOption.All, 1.0f, 1.0f, .5f));
            printrules.Add(new BestFitColumnRule(ResetOption.None));
            ReportSpread_Sheet1.PrintInfo.SmartPrintRules = printrules;
        }

        private void Print()
        {
            if (SummaryLevelRadioButton.Checked)
            {
                if (CashierTypeRadioButton.Checked)
                    PrintCashierSummary();
                else
                    PrintTillSummary();
            }
            else
            {
                if (CashierTypeRadioButton.Checked)
                    PrintCashierDetail();
                else
                    PrintTillDetail();
            }
        }

        private void EmitCashierTotals(ref int row, CashierSummary.Totals cashier)
        {
            ReportSpread_Sheet1.SetText(row, 0, string.Format("{0} {1}", cashier.Id, cashier.Name));

            ReportSpread_Sheet1.SetText(row, 1, cashier.TotalValue.ToString("0.00"));
            ReportSpread_Sheet1.SetText(row, 2, cashier.TotalVat.ToString("0.00"));
            ReportSpread_Sheet1.SetText(row, 3, cashier.TotalMerch.ToString("0.00"));
            ReportSpread_Sheet1.SetText(row, 4, cashier.TotalNonMerch.ToString("0.00"));
            ReportSpread_Sheet1.SetText(row, 5, cashier.TotalDiscount.ToString("0.00"));

            ReportSpread_Sheet1.SetText(row, 6, "Sale");
            ReportSpread_Sheet1.SetText(row, 7, cashier.SalesCount.ToString("0"));
            ReportSpread_Sheet1.SetText(row, 8, cashier.SalesValue.ToString("0.00"));
            ReportSpread_Sheet1.SetText(row, 9, "S/On");
            ReportSpread_Sheet1.SetText(row, 10, cashier.SignOnCount.ToString("0"));
            ReportSpread_Sheet1.RowCount++;
            row++;

            ReportSpread_Sheet1.SetText(row, 6, "Refund");
            ReportSpread_Sheet1.SetText(row, 7, cashier.RefundCount.ToString("0"));
            ReportSpread_Sheet1.SetText(row, 8, cashier.RefundValue.ToString("0.00"));
            ReportSpread_Sheet1.SetText(row, 9, "S/Off");
            ReportSpread_Sheet1.SetText(row, 10, cashier.SignOffCount.ToString("0"));
            ReportSpread_Sheet1.RowCount++;
            row++;

            ReportSpread_Sheet1.SetText(row, 6, "Exchg.");
            ReportSpread_Sheet1.SetText(row, 7, cashier.ExchangeCount.ToString("0"));
            ReportSpread_Sheet1.SetText(row, 8, cashier.ExchangeSaleValue.ToString("0.00"));
            ReportSpread_Sheet1.SetText(row, 9, "O/Drwr");
            ReportSpread_Sheet1.SetText(row, 10, cashier.OpenDrawerCount.ToString("0"));
            ReportSpread_Sheet1.RowCount++;
            row++;

            ReportSpread_Sheet1.SetText(row, 8, cashier.ExchangeRefundValue.ToString("0.00"));
            ReportSpread_Sheet1.SetText(row, 9, "Voided");
            ReportSpread_Sheet1.SetText(row, 10, cashier.VoidCount.ToString("0"));
            ReportSpread_Sheet1.RowCount++;
            row++;

            ReportSpread_Sheet1.SetText(row, 5, "Net");
            ReportSpread_Sheet1.SetText(row, 6, "Refund");
            ReportSpread_Sheet1.SetText(row, 7, cashier.NetRefundCount.ToString("0"));
            ReportSpread_Sheet1.SetText(row, 8, cashier.NetRefundValue.ToString("0.00"));
            ReportSpread_Sheet1.RowCount++;
            row++;

            ReportSpread_Sheet1.SetText(row, 6, "Misc +");
            ReportSpread_Sheet1.SetText(row, 7, cashier.MiscInCount.ToString("0"));
            ReportSpread_Sheet1.SetText(row, 8, cashier.MiscInValue.ToString("0.00"));
            ReportSpread_Sheet1.RowCount++;
            row++;

            ReportSpread_Sheet1.SetText(row, 6, "+/Corr");
            ReportSpread_Sheet1.SetText(row, 7, cashier.CorrInCount.ToString("0"));
            ReportSpread_Sheet1.SetText(row, 8, cashier.CorrInValue.ToString("0.00"));
            ReportSpread_Sheet1.RowCount++;
            row++;

            ReportSpread_Sheet1.SetText(row, 6, "Misc -");
            ReportSpread_Sheet1.SetText(row, 7, cashier.MiscOutCount.ToString("0"));
            ReportSpread_Sheet1.SetText(row, 8, cashier.MiscOutValue.ToString("0.00"));
            ReportSpread_Sheet1.RowCount++;
            row++;

            ReportSpread_Sheet1.SetText(row, 6, "-/Corr");
            ReportSpread_Sheet1.SetText(row, 7, cashier.CorrOutCount.ToString("0"));
            ReportSpread_Sheet1.SetText(row, 8, cashier.CorrOutValue.ToString("0.00"));
            ReportSpread_Sheet1.RowCount += 2;
            row += 2;
        }

        private void PrintCashierSummary()
        {
            ReportSpread_Sheet1.PrintInfo.ShowColumnHeaders = true;
            ReportSpread_Sheet1.ColumnHeaderVisible = true;

            try
            {
                int row = 0;
                ReportSpread.Font = new System.Drawing.Font("Microsoft Sans Serif", 10f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));

                if (ReportSpread_Sheet1.RowCount > 0)
                    ReportSpread_Sheet1.RemoveRows(0, ReportSpread_Sheet1.RowCount);

                ReportSpread_Sheet1.ColumnCount = 11;

                for (int i = 0; i < ReportSpread_Sheet1.ColumnCount; i++)
                    ReportSpread_Sheet1.Columns[i].CellType = new FarPoint.Win.Spread.CellType.TextCellType();

                ReportSpread_Sheet1.Columns[0].Width = 189;
                ReportSpread_Sheet1.Columns[1].Width = 102;
                ReportSpread_Sheet1.Columns[2].Width = 80;
                ReportSpread_Sheet1.Columns[3].Width = 102;
                ReportSpread_Sheet1.Columns[4].Width = 102;
                ReportSpread_Sheet1.Columns[5].Width = 89;
                ReportSpread_Sheet1.Columns[6].Width = 54;
                ReportSpread_Sheet1.Columns[7].Width = 67;
                ReportSpread_Sheet1.Columns[8].Width = 78;
                ReportSpread_Sheet1.Columns[9].Width = 54;
                ReportSpread_Sheet1.Columns[10].Width = 67;

                //set up headings 
                ReportSpread_Sheet1.Columns[0].Label = "CASHIER";
                ReportSpread_Sheet1.Columns[1].Label = "TOTAL";
                ReportSpread_Sheet1.Columns[2].Label = "V.A.T.";
                ReportSpread_Sheet1.Columns[3].Label = "MERCH";
                ReportSpread_Sheet1.Columns[4].Label = "N-MERCH";
                ReportSpread_Sheet1.Columns[5].Label = "DISC";
                ReportSpread_Sheet1.Columns[6].Label = "TYPE";
                ReportSpread_Sheet1.Columns[7].Label = "COUNT";
                ReportSpread_Sheet1.Columns[8].Label = "AMOUNT";
                ReportSpread_Sheet1.Columns[9].Label = "TYPE";
                ReportSpread_Sheet1.Columns[10].Label = "COUNT";

                ReportSpread_Sheet1.Columns[0].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[1].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[2].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[3].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[4].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[5].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[6].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[7].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[8].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[9].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[10].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;

                ReportSpread_Sheet1.RowCount = 1;

                CashierSummary res = accessor.GetCashierSummary(reportDate);
                foreach (CashierSummary.Totals item in res.Cashiers)
                    EmitCashierTotals(ref row, item);
                EmitCashierTotals(ref row, res.GrandTotals);

                row -= 3;
                ReportSpread_Sheet1.SetText(row, 0, string.Format("Data Date = {0}", reportDate.ToString("d")));
                ReportSpread_Sheet1.SetText(row + 1, 0, storeName);
                row += 3;

                ReportSpread_Sheet1.PrintInfo.Header = string.Concat(shortAssemblyName, "  /  v", _FileVersion, new string(' ', 10), "Retail Cashier Summary", new string(' ', 10), "Store: ", storeNumber, '-', storeName, new string(' ', 10), "Date Printed: ", DateTime.Now.Date.ToString("d"), "/rPage: /p");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void PrintCashierDetail()
        {
            try
            {
                ReportSpread.Font = new System.Drawing.Font("Microsoft Sans Serif", 10f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));

                if (ReportSpread_Sheet1.RowCount > 0)
                    ReportSpread_Sheet1.RemoveRows(0, ReportSpread_Sheet1.RowCount);

                ReportSpread_Sheet1.ColumnCount = 15;

                for (int i = 0; i < ReportSpread_Sheet1.ColumnCount; i++)
                    ReportSpread_Sheet1.Columns[i].CellType = new FarPoint.Win.Spread.CellType.TextCellType();

                ReportSpread_Sheet1.Columns[0].Width = 47;
                ReportSpread_Sheet1.Columns[1].Width = 30;
                ReportSpread_Sheet1.Columns[2].Width = 57;
                ReportSpread_Sheet1.Columns[3].Width = 67;
                ReportSpread_Sheet1.Columns[4].Width = 102;
                ReportSpread_Sheet1.Columns[5].Width = 56;
                ReportSpread_Sheet1.Columns[6].Width = 120;
                ReportSpread_Sheet1.Columns[7].Width = 83;
                ReportSpread_Sheet1.Columns[8].Width = 80;
                ReportSpread_Sheet1.Columns[9].Width = 57;
                ReportSpread_Sheet1.Columns[10].Width = 67;
                ReportSpread_Sheet1.Columns[11].Width = 57;
                ReportSpread_Sheet1.Columns[12].Width = 83;
                ReportSpread_Sheet1.Columns[13].Width = 113;
                ReportSpread_Sheet1.Columns[14].Width = 83;

                //set up headings 
                ReportSpread_Sheet1.Columns[0].Label = "CSH";
                ReportSpread_Sheet1.Columns[1].Label = "TL";
                ReportSpread_Sheet1.Columns[2].Label = "TIME";
                ReportSpread_Sheet1.Columns[3].Label = "TYPE";
                ReportSpread_Sheet1.Columns[4].Label = "COMMENT";
                ReportSpread_Sheet1.Columns[5].Label = "TRAN";
                ReportSpread_Sheet1.Columns[6].Label = "OVC TRAN";
                ReportSpread_Sheet1.Columns[7].Label = "TOTAL";
                ReportSpread_Sheet1.Columns[8].Label = "V.A.T.";
                ReportSpread_Sheet1.Columns[9].Label = "SKU";
                ReportSpread_Sheet1.Columns[10].Label = "PRICE";
                ReportSpread_Sheet1.Columns[11].Label = "QUAN";
                ReportSpread_Sheet1.Columns[12].Label = "EXT VAL";
                ReportSpread_Sheet1.Columns[13].Label = "TENDER";
                ReportSpread_Sheet1.Columns[14].Label = "AMOUNT";

                ReportSpread_Sheet1.Columns[0].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[1].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[2].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[3].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[4].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[5].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[6].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[7].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[8].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[9].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[10].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[11].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[12].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[13].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[14].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;

                ReportSpread_Sheet1.RowCount = 1;

                int cashierId;
                if (!int.TryParse(ChoiceComboBox.Text, out cashierId))
                {
                    cashierId = 0;
                }

                CashierDetails res = accessor.GetCashierDetails(reportDate, cashierId);

                if (res.DLTOTS.Rows.Count > 0)
                {
                    int row = 0;
                    foreach (DataRow reportRow in res.DLTOTS.Rows)
                    {
                        string currentTill = (string)reportRow["TILL"];
                        string currentTran = (string)reportRow["TRAN"];
                        string currentKey = currentTill + '.' + currentTran;

                        int tenderIndex = 0;
                        decimal[] tenderAmounts = res.TenderAmounts[currentKey];

                        ReportSpread_Sheet1.SetText(row, 0, ChoiceComboBox.Text);
                        ReportSpread_Sheet1.SetText(row, 1, (string)reportRow["TILL"]);
                        ReportSpread_Sheet1.SetText(row, 2, (string)reportRow["TIME"]);
                        ReportSpread_Sheet1.SetText(row, 3, GetTransactionType((string)reportRow["TCOD"]));
                        ReportSpread_Sheet1.SetText(row, 4, GetComment((string)reportRow["TCOD"], (string)reportRow["DESCR"], (bool)reportRow["VOID"], (bool)reportRow["TMOD"], (bool)reportRow["PARK"], (bool)reportRow["PKRC"]));
                        ReportSpread_Sheet1.SetText(row, 5, (string)reportRow["TRAN"]);
                        if (reportRow["OVCTranNumber"] != DBNull.Value)
                        {
                            ReportSpread_Sheet1.SetText(row, 6, (string)reportRow["OVCTranNumber"]);
                        }
                        ReportSpread_Sheet1.SetText(row, 7, Convert.ToDecimal(reportRow["TOTL"]).ToString("0.00"));
                        if (Convert.ToDecimal(reportRow["TAXA"]) != 0)
                            ReportSpread_Sheet1.SetText(row, 8, Convert.ToDecimal(reportRow["TAXA"]).ToString("0.00"));

                        row++;
                        ReportSpread_Sheet1.RowCount++;

                        bool firstRow = true;
                        decimal eventsDiscount = 0;
                        foreach (DataRow detailRow in res.DLLINE[currentKey].Rows)
                        {
                            if (firstRow)
                            {
                                firstRow = false;
                                if (Convert.ToString(reportRow["ORDN"]) != "000000")
                                    ReportSpread_Sheet1.SetText(row, 1, string.Format("Quote/Order No. : {0}", Convert.ToString(reportRow["ORDN"])));
                            }

                            if ((int)(decimal)detailRow["QUAN"] < 0 && detailRow["RefundCode"] != DBNull.Value)
                            {
                                string refundCode = (string)detailRow["RefundCode"];
                                if (res.RefundCodeDescriptions.ContainsKey(refundCode))
                                    ReportSpread_Sheet1.SetText(row, 1, string.Format("Refund Reason : {0} {1}", refundCode, res.RefundCodeDescriptions[refundCode]));
                            }

                            // Attempt to print a tender and amount
                            while (tenderIndex < 21 && tenderAmounts[tenderIndex] == 0)
                                tenderIndex++;

                            if (tenderIndex < 21)
                            {
                                if (tenderIndex < 13)
                                    ReportSpread_Sheet1.SetText(row, 13, res.TenderDescriptions[tenderIndex]);
                                else if (tenderIndex < 20)
                                    ReportSpread_Sheet1.SetText(row, 13, string.Concat("M-", res.TenderDescriptions[tenderIndex]));
                                else
                                    ReportSpread_Sheet1.SetText(row, 13, "CHANGE");
                                ReportSpread_Sheet1.SetText(row, 14, tenderAmounts[tenderIndex].ToString("0.00"));
                                tenderIndex++;
                            }

                            if (Convert.ToString(detailRow["SKUN"]) != "000000")
                                ReportSpread_Sheet1.SetText(row, 9, Convert.ToString(detailRow["SKUN"]));

                            ReportSpread_Sheet1.SetText(row, 10, Convert.ToDecimal(detailRow["PRIC"]).ToString("0.00"));
                            ReportSpread_Sheet1.SetText(row, 11, Convert.ToInt32(detailRow["QUAN"]).ToString());
                            ReportSpread_Sheet1.SetText(row, 12, Convert.ToDecimal(detailRow["EXTP"]).ToString("0.00"));

                            eventsDiscount += Convert.ToDecimal(detailRow["QBPD"]);
                            eventsDiscount += Convert.ToDecimal(detailRow["DGPD"]);
                            eventsDiscount += Convert.ToDecimal(detailRow["MBPD"]);
                            eventsDiscount += Convert.ToDecimal(detailRow["HSPD"]);
                            eventsDiscount += Convert.ToDecimal(detailRow["ESEV"]);

                            row++;
                            ReportSpread_Sheet1.RowCount++;
                        }

                        // finish printing tenders, if any left
                        if (tenderIndex < 21)
                        {
                            while (tenderIndex < 21)
                            {
                                if (tenderAmounts[tenderIndex] != 0)
                                {
                                    if (tenderIndex < 13)
                                        ReportSpread_Sheet1.SetText(row, 13, res.TenderDescriptions[tenderIndex]);
                                    else if (tenderIndex < 20)
                                        ReportSpread_Sheet1.SetText(row, 13, string.Concat("M-", res.TenderDescriptions[tenderIndex - 10]));
                                    else
                                        ReportSpread_Sheet1.SetText(row, 13, "CHANGE");
                                    ReportSpread_Sheet1.SetText(row, 14, tenderAmounts[tenderIndex].ToString("0.00"));

                                    row++;
                                    ReportSpread_Sheet1.RowCount++;
                                }
                                tenderIndex++;
                            }
                        }

                        if (eventsDiscount != 0)
                        {
                            ReportSpread_Sheet1.SetText(row, 11, "( Event Discounts:");
                            ReportSpread_Sheet1.SetText(row, 12, eventsDiscount.ToString("0.00"));
                            ReportSpread_Sheet1.SetText(row, 13, ")");
                            ReportSpread_Sheet1.RowCount++;
                            row++;
                        }

                        ReportSpread_Sheet1.RowCount++;
                        row++;
                    }


                    ReportSpread_Sheet1.SetText(row, 1, string.Concat("Data Date = ", reportDate.ToString("d")));

                    row++;
                    ReportSpread_Sheet1.RowCount++;
                    ReportSpread_Sheet1.SetText(row, 1, storeName);

                    row++;
                    ReportSpread_Sheet1.RowCount++;
                    ReportSpread_Sheet1.SetText(row, 1, Convert.ToString(ChoiceComboBox.ItemTable.Rows[ChoiceComboBox.SelectedIndex][1]));

                    ReportSpread_Sheet1.PrintInfo.Header = string.Concat(shortAssemblyName, "  /  v", _FileVersion, new string(' ', 10), "Detail By Selected Cashier", new string(' ', 10), "Store: ", storeNumber, '-', storeName, new string(' ', 10), "Date Printed: ", DateTime.Now.Date.ToString("d"), "/rPage: /p");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void EmitTillTotals(ref int row, TillSummary.Totals item)
        {
            ReportSpread_Sheet1.SetText(row, 0, item.Id);
            ReportSpread_Sheet1.SetText(row, 1, item.FirstTran);
            ReportSpread_Sheet1.SetText(row, 2, item.FirstOVCTran);

            ReportSpread_Sheet1.SetText(row, 3, item.TotalValue.ToString("0.00"));
            ReportSpread_Sheet1.SetText(row, 4, item.TotalVat.ToString("0.00"));
            ReportSpread_Sheet1.SetText(row, 5, item.TotalMerch.ToString("0.00"));
            ReportSpread_Sheet1.SetText(row, 6, item.TotalNonMerch.ToString("0.00"));
            ReportSpread_Sheet1.SetText(row, 7, item.TotalDiscount.ToString("0.00"));
            ReportSpread_Sheet1.SetText(row, 8, item.TotalTraining.ToString("0.00"));

            ReportSpread_Sheet1.SetText(row, 9, "Sale");
            ReportSpread_Sheet1.SetText(row, 10, item.SalesCount.ToString("0"));
            ReportSpread_Sheet1.SetText(row, 11, item.SalesValue.ToString("0.00"));
            ReportSpread_Sheet1.SetText(row, 12, "S/On");
            ReportSpread_Sheet1.SetText(row, 13, item.SignOnCount.ToString("0"));
            ReportSpread_Sheet1.RowCount++;
            row++;

            ReportSpread_Sheet1.SetText(row, 1, item.LastTran);
            ReportSpread_Sheet1.SetText(row, 2, item.LastOVCTran);
            ReportSpread_Sheet1.SetText(row, 8, item.TrainingCount.ToString());
            ReportSpread_Sheet1.SetText(row, 9, "Refund");
            ReportSpread_Sheet1.SetText(row, 10, item.RefundCount.ToString("0"));
            ReportSpread_Sheet1.SetText(row, 11, item.RefundValue.ToString("0.00"));
            ReportSpread_Sheet1.SetText(row, 12, "S/Off");
            ReportSpread_Sheet1.SetText(row, 13, item.SignOffCount.ToString("0"));
            ReportSpread_Sheet1.RowCount++;
            row++;

            ReportSpread_Sheet1.SetText(row, 9, "Exchg.");
            ReportSpread_Sheet1.SetText(row, 10, item.ExchangeCount.ToString("0"));
            ReportSpread_Sheet1.SetText(row, 11, item.ExchangeSaleValue.ToString("0.00"));
            ReportSpread_Sheet1.SetText(row, 12, "O/Drwr");
            ReportSpread_Sheet1.SetText(row, 13, item.OpenDrawerCount.ToString("0"));
            ReportSpread_Sheet1.RowCount++;
            row++;

            ReportSpread_Sheet1.SetText(row, 11, item.ExchangeRefundValue.ToString("0.00"));
            ReportSpread_Sheet1.SetText(row, 12, "Voided");
            ReportSpread_Sheet1.SetText(row, 13, item.VoidCount.ToString("0"));
            ReportSpread_Sheet1.RowCount++;
            row++;

            ReportSpread_Sheet1.SetText(row, 9, "Misc +");
            ReportSpread_Sheet1.SetText(row, 10, item.MiscInCount.ToString("0"));
            ReportSpread_Sheet1.SetText(row, 11, item.MiscInValue.ToString("0.00"));
            ReportSpread_Sheet1.RowCount++;
            row++;

            ReportSpread_Sheet1.SetText(row, 9, "+/Corr");
            ReportSpread_Sheet1.SetText(row, 10, item.CorrInCount.ToString("0"));
            ReportSpread_Sheet1.SetText(row, 11, item.CorrInValue.ToString("0.00"));
            ReportSpread_Sheet1.RowCount++;
            row++;

            ReportSpread_Sheet1.SetText(row, 9, "Misc -");
            ReportSpread_Sheet1.SetText(row, 10, item.MiscOutCount.ToString("0"));
            ReportSpread_Sheet1.SetText(row, 11, item.MiscOutValue.ToString("0.00"));
            ReportSpread_Sheet1.RowCount++;
            row++;

            ReportSpread_Sheet1.SetText(row, 9, "-/Corr");
            ReportSpread_Sheet1.SetText(row, 10, item.CorrOutCount.ToString("0"));
            ReportSpread_Sheet1.SetText(row, 11, item.CorrOutValue.ToString("0.00"));
            ReportSpread_Sheet1.RowCount += 2;
            row += 2;
        }

        private void PrintTillSummary()
        {
            ReportSpread_Sheet1.PrintInfo.ShowColumnHeaders = true;
            ReportSpread_Sheet1.ColumnHeaderVisible = true;

            try
            {
                int row = 0;

                ReportSpread.Font = new System.Drawing.Font("Microsoft Sans Serif", 10f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));

                if (ReportSpread_Sheet1.RowCount > 0)
                    ReportSpread_Sheet1.RemoveRows(0, ReportSpread_Sheet1.RowCount);

                ReportSpread_Sheet1.ColumnCount = 14;

                for (int i = 0; i < ReportSpread_Sheet1.ColumnCount; i++)
                    ReportSpread_Sheet1.Columns[i].CellType = new FarPoint.Win.Spread.CellType.TextCellType();

                ReportSpread_Sheet1.Columns[0].Width = 45;
                ReportSpread_Sheet1.Columns[1].Width = 56;
                ReportSpread_Sheet1.Columns[2].Width = 120;
                ReportSpread_Sheet1.Columns[3].Width = 83;
                ReportSpread_Sheet1.Columns[4].Width = 83;
                ReportSpread_Sheet1.Columns[5].Width = 83;
                ReportSpread_Sheet1.Columns[6].Width = 83;
                ReportSpread_Sheet1.Columns[7].Width = 83;
                ReportSpread_Sheet1.Columns[8].Width = 83;
                ReportSpread_Sheet1.Columns[9].Width = 54;
                ReportSpread_Sheet1.Columns[10].Width = 67;
                ReportSpread_Sheet1.Columns[11].Width = 78;
                ReportSpread_Sheet1.Columns[12].Width = 54;
                ReportSpread_Sheet1.Columns[13].Width = 67;

                //set up headings 
                ReportSpread_Sheet1.Columns[0].Label = "TILL";
                ReportSpread_Sheet1.Columns[1].Label = "TRAN";
                ReportSpread_Sheet1.Columns[2].Label = "OVC TRAN";
                ReportSpread_Sheet1.Columns[3].Label = "TOTAL";
                ReportSpread_Sheet1.Columns[4].Label = "V.A.T.";
                ReportSpread_Sheet1.Columns[5].Label = "MERCH";
                ReportSpread_Sheet1.Columns[6].Label = "N-MERCH";
                ReportSpread_Sheet1.Columns[7].Label = "DISC";
                ReportSpread_Sheet1.Columns[8].Label = "TRAINING";
                ReportSpread_Sheet1.Columns[9].Label = "TYPE";
                ReportSpread_Sheet1.Columns[10].Label = "COUNT";
                ReportSpread_Sheet1.Columns[11].Label = "AMOUNT";
                ReportSpread_Sheet1.Columns[12].Label = "TYPE";
                ReportSpread_Sheet1.Columns[13].Label = "COUNT";

                ReportSpread_Sheet1.Columns[0].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[1].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[2].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[3].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[4].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[5].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[6].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[7].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[8].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[9].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[10].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[11].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[12].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[13].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;

                ReportSpread_Sheet1.RowCount = 1;

                TillSummary res = accessor.GetTillSummary(reportDate);
                foreach (TillSummary.Totals item in res.Tills)
                    EmitTillTotals(ref row, item);
                EmitTillTotals(ref row, res.GrandTotals);

                row -= 3;
                ReportSpread_Sheet1.SetText(row, 0, string.Format("Data Date = {0}", reportDate.ToString("d")));
                ReportSpread_Sheet1.Cells[row, 0].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;

                ReportSpread_Sheet1.SetText(row + 1, 0, storeName);
                ReportSpread_Sheet1.Cells[row + 1, 0].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                row += 3;

                ReportSpread_Sheet1.PrintInfo.Header = string.Concat(shortAssemblyName, "  /  v", _FileVersion, new string(' ', 10), "Retail Till Summary", new string(' ', 10), "Store: ", storeNumber, '-', storeName, new string(' ', 10), "Date Printed: ", DateTime.Now.Date.ToString("d"), "/rPage: /p");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void PrintTillDetail()
        {
            try
            {
                ReportSpread.Font = new System.Drawing.Font("Microsoft Sans Serif", 10f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));

                if (ReportSpread_Sheet1.RowCount > 0)
                    ReportSpread_Sheet1.RemoveRows(0, ReportSpread_Sheet1.RowCount);

                ReportSpread_Sheet1.ColumnCount = 15;

                for (int i = 0; i < ReportSpread_Sheet1.ColumnCount; i++)
                    ReportSpread_Sheet1.Columns[i].CellType = new FarPoint.Win.Spread.CellType.TextCellType();

                ReportSpread_Sheet1.Columns[0].Width = 30;
                ReportSpread_Sheet1.Columns[1].Width = 57;
                ReportSpread_Sheet1.Columns[2].Width = 67;
                ReportSpread_Sheet1.Columns[3].Width = 47;
                ReportSpread_Sheet1.Columns[4].Width = 102;
                ReportSpread_Sheet1.Columns[5].Width = 56;
                ReportSpread_Sheet1.Columns[6].Width = 120;
                ReportSpread_Sheet1.Columns[7].Width = 83;
                ReportSpread_Sheet1.Columns[8].Width = 83;
                ReportSpread_Sheet1.Columns[9].Width = 57;
                ReportSpread_Sheet1.Columns[10].Width = 67;
                ReportSpread_Sheet1.Columns[11].Width = 57;
                ReportSpread_Sheet1.Columns[12].Width = 83;
                ReportSpread_Sheet1.Columns[13].Width = 113;
                ReportSpread_Sheet1.Columns[14].Width = 83;

                //set up headings 
                ReportSpread_Sheet1.Columns[0].Label = "TL";
                ReportSpread_Sheet1.Columns[1].Label = "TIME";
                ReportSpread_Sheet1.Columns[2].Label = "TYPE";
                ReportSpread_Sheet1.Columns[3].Label = "CSH";
                ReportSpread_Sheet1.Columns[4].Label = "COMMENT";
                ReportSpread_Sheet1.Columns[5].Label = "TRAN";
                ReportSpread_Sheet1.Columns[6].Label = "OVC TRAN";
                ReportSpread_Sheet1.Columns[7].Label = "TOTAL";
                ReportSpread_Sheet1.Columns[8].Label = "V.A.T.";
                ReportSpread_Sheet1.Columns[9].Label = "SKU";
                ReportSpread_Sheet1.Columns[10].Label = "PRICE";
                ReportSpread_Sheet1.Columns[11].Label = "QUAN";
                ReportSpread_Sheet1.Columns[12].Label = "EXT VAL";
                ReportSpread_Sheet1.Columns[13].Label = "TENDER";
                ReportSpread_Sheet1.Columns[14].Label = "AMOUNT";

                ReportSpread_Sheet1.Columns[0].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[1].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[2].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[3].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[4].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[5].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[6].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[7].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[8].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[9].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[10].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[11].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[12].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;
                ReportSpread_Sheet1.Columns[13].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
                ReportSpread_Sheet1.Columns[14].HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right;

                ReportSpread_Sheet1.RowCount = 1;

                int tillId;
                if (!int.TryParse(ChoiceComboBox.Text, out tillId))
                {
                    tillId = 0;
                }

                TillDetails res = accessor.GetTillDetails(reportDate, tillId);

                if (res.DLTOTS.Rows.Count > 0)
                {
                    int row = 0;
                    foreach (DataRow reportRow in res.DLTOTS.Rows)
                    {
                        string currentTill = (string)reportRow["TILL"];
                        string currentTran = (string)reportRow["TRAN"];
                        string currentKey = currentTill + '.' + currentTran;

                        int tenderIndex = 0;
                        decimal[] tenderAmounts = res.TenderAmounts[currentKey];

                        ReportSpread_Sheet1.SetText(row, 0, ChoiceComboBox.Text);
                        ReportSpread_Sheet1.SetText(row, 1, (string)reportRow["TIME"]);
                        ReportSpread_Sheet1.SetText(row, 2, GetTransactionType((string)reportRow["TCOD"]));
                        ReportSpread_Sheet1.SetText(row, 3, Convert.ToString(reportRow["CASH"]));
                        ReportSpread_Sheet1.SetText(row, 4, GetComment((string)reportRow["TCOD"], (string)reportRow["DESCR"], (bool)reportRow["VOID"], (bool)reportRow["TMOD"], (bool)reportRow["PARK"], (bool)reportRow["PKRC"]));
                        ReportSpread_Sheet1.SetText(row, 5, (string)reportRow["TRAN"]);
                        if (reportRow["OVCTranNumber"] != DBNull.Value)
                        {
                            ReportSpread_Sheet1.SetText(row, 6, (string)reportRow["OVCTranNumber"]);
                        }
                        ReportSpread_Sheet1.SetText(row, 7, Convert.ToDecimal(reportRow["TOTL"]).ToString("0.00"));
                        if ((decimal)reportRow["TAXA"] != 0)
                            ReportSpread_Sheet1.SetText(row, 8, Convert.ToDecimal(reportRow["TAXA"]).ToString("0.00"));

                        row++;
                        ReportSpread_Sheet1.RowCount++;

                        bool firstRow = true;
                        decimal eventsDiscount = 0;
                        foreach (DataRow detailRow in res.DLLINE[currentKey].Rows)
                        {
                            if (firstRow)
                            {
                                firstRow = false;
                                if (Convert.ToString(reportRow["ORDN"]) != "000000")
                                    ReportSpread_Sheet1.SetText(row, 1, string.Format("Quote/Order No. : {0}", (string)reportRow["ORDN"]));
                            }

                            if ((int)(decimal)detailRow["QUAN"] < 0 && detailRow["RefundCode"] != DBNull.Value)
                            {
                                string refundCode = (string)detailRow["RefundCode"];
                                if (res.RefundCodeDescriptions.ContainsKey(refundCode))
                                    ReportSpread_Sheet1.SetText(row, 1, string.Format("Refund Reason : {0} {1}", refundCode, res.RefundCodeDescriptions[refundCode]));
                            }

                            // Attempt to print a tender and amount
                            while (tenderIndex < 21 && tenderAmounts[tenderIndex] == 0)
                                tenderIndex++;

                            if (tenderIndex < 21)
                            {
                                if (tenderIndex < 13)
                                    ReportSpread_Sheet1.SetText(row, 13, res.TenderDescriptions[tenderIndex]);
                                else if (tenderIndex < 20)
                                    ReportSpread_Sheet1.SetText(row, 13, string.Concat("M-", res.TenderDescriptions[tenderIndex]));
                                else
                                    ReportSpread_Sheet1.SetText(row, 13, "CHANGE");

                                ReportSpread_Sheet1.SetText(row, 14, tenderAmounts[tenderIndex].ToString("0.00"));
                                tenderIndex++;
                            }

                            if (Convert.ToString(detailRow["SKUN"]) != "000000")
                                ReportSpread_Sheet1.SetText(row, 9, Convert.ToString(detailRow["SKUN"]));

                            ReportSpread_Sheet1.SetText(row, 10, Convert.ToDecimal(detailRow["PRIC"]).ToString("0.00"));
                            ReportSpread_Sheet1.SetText(row, 11, Convert.ToInt32(detailRow["QUAN"]).ToString());
                            ReportSpread_Sheet1.SetText(row, 12, Convert.ToDecimal(detailRow["EXTP"]).ToString("0.00"));

                            eventsDiscount += (decimal)detailRow["QBPD"];
                            eventsDiscount += (decimal)detailRow["DGPD"];
                            eventsDiscount += (decimal)detailRow["MBPD"];
                            eventsDiscount += (decimal)detailRow["HSPD"];
                            eventsDiscount += (decimal)detailRow["ESEV"];

                            row++;
                            ReportSpread_Sheet1.RowCount++;
                        }

                        // finish printing tenders, if any left
                        if (tenderIndex < 21)
                        {
                            while (tenderIndex < 21)
                            {
                                if (tenderAmounts[tenderIndex] != 0)
                                {
                                    if (tenderIndex < 13)
                                        ReportSpread_Sheet1.SetText(row, 13, res.TenderDescriptions[tenderIndex]);
                                    else if (tenderIndex < 20)
                                        ReportSpread_Sheet1.SetText(row, 13, string.Concat("M-", res.TenderDescriptions[tenderIndex - 10]));
                                    else
                                        ReportSpread_Sheet1.SetText(row, 13, "CHANGE");

                                    ReportSpread_Sheet1.SetText(row, 14, tenderAmounts[tenderIndex].ToString("0.00"));
                                    row++;
                                    ReportSpread_Sheet1.RowCount++;
                                }
                                tenderIndex += 1;
                            }
                        }

                        if (eventsDiscount != 0)
                        {
                            ReportSpread_Sheet1.SetText(row, 11, "( Event Discounts:");
                            ReportSpread_Sheet1.SetText(row, 12, eventsDiscount.ToString("0.00"));
                            ReportSpread_Sheet1.SetText(row, 13, ")");
                            ReportSpread_Sheet1.RowCount++;
                            row++;
                        }

                        ReportSpread_Sheet1.RowCount++;
                        row++;
                    }

                    ReportSpread_Sheet1.SetText(row, 1, string.Concat("Data Date = ", reportDate.ToString("d")));
                    row++;
                    ReportSpread_Sheet1.RowCount++;
                    ReportSpread_Sheet1.SetText(row, 1, storeName);

                    ReportSpread_Sheet1.PrintInfo.Header = string.Concat(shortAssemblyName, "  /  v", _FileVersion, new string(' ', 10), "Detail By Selected Till", new string(' ', 10), "Store: ", storeNumber, '-', storeName, new string(' ', 10), "Date Printed: ", DateTime.Now.Date.ToString("d"), "/rPage: /p");
                }
                }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private string GetTransactionType(string transactionCode)
        {
            switch (transactionCode)
            {
                case "ZR":
                    return "Z-READ";
                case "XR":
                    return "X-READ";
                case "SA":
                    return "SALE  ";
                case "SC":
                    return "S/CORR";
                case "RF":
                    return "REFUND";
                case "RC":
                    return "R/CORR";
                case "OD":
                    return "O/DRWR";
                case "M+":
                    return "MISC +";
                case "C+":
                    return "+/CORR";
                case "M-":
                    return "MISC -";
                case "C-":
                    return "-/CORR";
                case "CO":
                    return "S/ON  ";
                case "CC":
                    return "S/OFF ";
                default:
                    return "??????";
            }
        }

        private string GetComment(string transactionCode, string transactionDescription, bool @void, bool trainingMode, bool park, bool recall)
        {
            if (trainingMode & @void)
                return "T/MOD VOID";

            if (recall)
                return "* RECALLED";

            if (park)
                return "* PARKED";

            if (trainingMode)
                return "Training";

            if (@void)
                return "* V O I D";

            switch (transactionCode)
            {
                case "OD":
                case "M+":
                case "C+":
                case "M-":
                case "C-":
                    return transactionDescription;
                default:
                    return string.Empty;
            }
        }

        private void FillChoices()
        {
            if (ChoiceComboBox.ItemTable != null)
            {
                ChoiceComboBox.ItemTable.Dispose();
                ChoiceComboBox.ItemTable = null;
            }

            if (DetailsLevelRadioButton.Checked)
            {
                using (SqlCommand choicesCommand = new SqlCommand())
                {
                    if (CashierTypeRadioButton.Checked)
                        choicesCommand.CommandText = "SELECT DISTINCT CASH, CASE CASH WHEN 499 THEN 'Click and Collect' ELSE Name END FROM DLTOTS LEFT OUTER JOIN SystemUsers ON SystemUsers.EmployeeCode = CASH WHERE Date1 = @reportingDate UNION SELECT DISTINCT CASH, CASE CASH WHEN 499 THEN 'Click and Collect' ELSE Name END FROM PVTOTS LEFT OUTER JOIN SystemUsers ON EmployeeCode = Cash WHERE Date1 = @pvtotsDate ORDER BY CASH";
                    else
                        choicesCommand.CommandText = "SELECT DISTINCT TILL, WSOCTL.DESCR FROM DLTOTS LEFT OUTER JOIN WSOCTL ON WSID = TILL WHERE Date1 = @reportingDate UNION SELECT DISTINCT TILL, WSOCTL.Descr FROM PVTOTS LEFT OUTER JOIN WSOCTL ON WSID = TILL WHERE Date1 = @pvtotsDate ORDER BY TILL";

                    choicesCommand.Parameters.Add("@reportingDate", SqlDbType.DateTime).Value = reportDate;
                    choicesCommand.Parameters.Add("@pvtotsDate", SqlDbType.Char, 8).Value = reportDate.ToString("dd/MM/yy");
                    ChoiceComboBox.ItemTable = dataProxy.ExecuteDataTable(choicesCommand);
                    if (ChoiceComboBox.ItemTable.Rows.Count > 0)
                    {
                        if (CashierTypeRadioButton.Checked)
                            ChoiceLabel.Text = "Cashier:";
                        else
                            ChoiceLabel.Text = "Till:";
                        ChoiceComboBox.SelectedIndex = 0;
                        ChoiceComboBox.Visible = true;
                    }
                    else
                    {
                        ChoiceLabel.Text = "No Data";
                        ChoiceComboBox.Visible = false;
                    }
                }
            }
        }

        private void ListingTypeRadioButton_CheckedChanged(System.Object sender, System.EventArgs e)
        {
            PrintButton.Enabled = false;
            if (CashierTypeRadioButton.Checked)
                ChoiceLabel.Text = "Cashier:";
            else
                ChoiceLabel.Text = "Till:";
            FillChoices();
        }

        private void SummaryLevelRadioButton_CheckedChanged(System.Object sender, System.EventArgs e)
        {
            PrintButton.Enabled = false;
            ChoiceComboBox.Visible = DetailsLevelRadioButton.Checked;
            ChoiceLabel.Visible = ChoiceComboBox.Visible;
            FillChoices();
        }

        private void ReportSpread_ColumnWidthChanged(object sender, FarPoint.Win.Spread.ColumnWidthChangedEventArgs e)
        {
            Debug.WriteLine("Report Column Widths:");
            for (int i = 0; i < ReportSpread_Sheet1.ColumnCount; i++)
                Debug.WriteLine(string.Format("ReportSpread_Sheet1.Columns({0}).Width = {1}", i, ReportSpread_Sheet1.Columns[i].Width));
        }

        private void ReportSpread_PrintMessageBox(object sender, FarPoint.Win.Spread.PrintMessageBoxEventArgs e)
        {
            if (e.BeginPrinting == false)
                printNotPrinted = false;
        }

        private void Finish()
        {
            printNotPrinted = true;
            while (printNotPrinted)
            {
                Application.DoEvents();
                System.Threading.Thread.Sleep(1);
            }
        }

        private void ParentForm_Shown(System.Object sender, System.EventArgs e)
        {
            RetrieveButton.PerformClick();
        }

        private void RetrieveButton_Click(System.Object sender, System.EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            ButtonPanel.Enabled = false;
            CollapsibleGroupBox1.Enabled = false;
            Print();
            ButtonPanel.Enabled = true;
            CollapsibleGroupBox1.Enabled = true;
            Cursor = Cursors.Default;
            PrintButton.Enabled = true;

            if (nightJob)
                PrintButton.PerformClick();
        }

        private void PrintButton_Click(System.Object sender, System.EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            ButtonPanel.Enabled = false;
            CollapsibleGroupBox1.Enabled = false;

            ReportSpread.PrintSheet(0);
            //wait until printing finished
            Finish();
            ButtonPanel.Enabled = true;
            CollapsibleGroupBox1.Enabled = true;
            Cursor = Cursors.Default;

            if (nightJob)
                ExitButton.PerformClick();
        }

        private void ExitButton_Click(System.Object sender, System.EventArgs e)
        {
            FindForm().Close();
        }

        private void _KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F5:
                    RetrieveButton.PerformClick();
                    break;
                case Keys.F9:
                    PrintButton.PerformClick();
                    break;
                case Keys.F12:
                    ExitButton.PerformClick();
                    break;
            }
        }

        private void dtpReportDate_ValueChanged(object sender, EventArgs e)
        {
            reportDate = ((DateTimePicker)sender).Value.Date;
            PrintButton.Enabled = false;
            FillChoices();
        }

        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            FindForm().Shown += ParentForm_Shown;
            FindForm().KeyDown += _KeyDown;
            FindForm().KeyPreview = true;
            ReportSpread_Sheet1.OperationMode = FarPoint.Win.Spread.OperationMode.ReadOnly;
        }

        private void ChoiceComboBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            PrintButton.Enabled = false;
        }

        private void MyTrace(string message)
        {
            Trace.WriteLine(string.Format("{0}/v{1} - {2}", shortAssemblyName, _FileVersion, message));
        }
    }
}
