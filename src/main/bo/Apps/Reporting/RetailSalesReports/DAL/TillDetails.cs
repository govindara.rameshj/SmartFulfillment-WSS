﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace RetailSalesReports.DAL
{
    public class TillDetails
    {
        public string[] TenderDescriptions;
        public Dictionary<string, string> RefundCodeDescriptions;
        public DataTable DLTOTS;

        /// <summary>
        /// Key is till.tran
        /// </summary>
        public Dictionary<string, DataTable> DLLINE;
        /// <summary>
        /// Key is till.tran
        /// </summary>
        public Dictionary<string, decimal[]> TenderAmounts;
    }
}
