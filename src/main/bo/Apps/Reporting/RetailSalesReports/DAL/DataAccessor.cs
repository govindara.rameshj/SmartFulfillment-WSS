﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;

namespace RetailSalesReports.DAL
{
    public class DataAccessor
    {
        private Oasys.DataProxy dataProxy;
        private string shortAssemblyName;
        private string fileVersion;

        public DataAccessor(string shortAssemblyName, string fileVersion)
        {
            OasysDBBO.Oasys3.DB.clsOasys3DB db = new OasysDBBO.Oasys3.DB.clsOasys3DB();
            dataProxy = (Oasys.DataProxy)Activator.CreateInstance(db.GetConfigValue("ProxyAssembly"), db.GetConfigValue("ProxyTypeName"), false, System.Reflection.BindingFlags.CreateInstance, null, new object[] { db.GetConfigValue("sqlConnection") }, System.Globalization.CultureInfo.InvariantCulture, null, null).Unwrap();

            this.shortAssemblyName = shortAssemblyName;
            this.fileVersion = fileVersion;
        }

        private void MyTrace(string message)
        {
            Trace.WriteLine(string.Format("{0}/v{1} - {2}", shortAssemblyName, fileVersion, message));
        }

        private decimal[] GetDLTenders(DateTime date, string tranTill, string tranNumb)
        {
            decimal[] tenderAmounts = new decimal[21];
            DataTable detailData = null;

            MyTrace("Counting tenders");
            using (SqlCommand cmd = new SqlCommand("SELECT DP.[AMNT], DP.[TYPE] FROM DLPAID DP WHERE DATE1 = @reportingDate AND TILL = @reportTill AND [TRAN] = @reportTran"))
            {
                cmd.Parameters.Add("@reportingDate", SqlDbType.DateTime).Value = date;
                cmd.Parameters.Add("@reportTill", SqlDbType.Char, 2).Value = tranTill;
                cmd.Parameters.Add("@reportTran", SqlDbType.Char, 4).Value = tranNumb;

                detailData = dataProxy.ExecuteDataTable(cmd);
            }

            for (int i = 0; i <= 20; i++)
                tenderAmounts[i] = 0;

            foreach (DataRow detailRow in detailData.Rows)
            {
                int type = (int)(decimal)detailRow["TYPE"] - 1;
                if (type == 98)
                    type = 20;
                tenderAmounts[type] -= (decimal)detailRow["AMNT"];
            }
            return tenderAmounts;
        }

        private string[] GetTenderDescriptions()
        {
            string[] res = null;
            using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
            {
                MyTrace("Obtaining tender descriptions");
                cmd.CommandText = "SELECT TTDE1, TTDE2, TTDE3, TTDE4, TTDE5, TTDE6, TTDE7, TTDE8, TTDE9, TTDE10, TTDE11, TTDE12, TTDE13 FROM RETOPT WHERE FKEY = '01'";
                var table = dataProxy.ExecuteDataTable(cmd);
                var row = table.Rows[0];
                res = new string[13];
                for (int i = 0; i < res.Length; i++)
                    res[i] = (row["TTDE" + (i + 1).ToString()]).ToString().Trim();
                table.Dispose();
            }
            return res;
        }

        private Dictionary<string, string> GetRefundReasonDescriptions()
        {
            var res = new Dictionary<string, string>();
            using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
            {
                cmd.CommandText = "SELECT CODE, DESCR FROM SYSCOD WHERE TYPE = 'RF'";
                var table = dataProxy.ExecuteDataTable(cmd);
                foreach (DataRow row in table.Rows)
                    res.Add((string)row["CODE"], (string)row["DESCR"]);
                table.Dispose();
            }
            return res;
        }

        public CashierDetails GetCashierDetails(DateTime date, int cashierId)
        {
            var res = new CashierDetails();
            res.TenderDescriptions = GetTenderDescriptions();
            res.RefundCodeDescriptions = GetRefundReasonDescriptions();

            using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
            {
                MyTrace("Getting transactions from DLTOTS");
                cmd.CommandText = @"
SELECT [TILL], [TIME], [TCOD], [DESCR], [TRAN], [OVCTranNumber], [TOTL], [TAXA], [ORDN], [TMOD], [PARK], [PKRC], [VOID]
FROM vwDLTOTS DT
WHERE [DATE1] = @reportingDate AND CASH = @reportCashier
ORDER BY [TILL], [TIME], [TRAN]";

                cmd.Parameters.Add("@reportingDate", SqlDbType.DateTime).Value = date;
                cmd.Parameters.Add("@reportCashier", SqlDbType.Char, 3).Value = cashierId.ToString("000");
                res.DLTOTS = dataProxy.ExecuteDataTable(cmd);
                cmd.Parameters.Clear();

                res.TenderAmounts = new Dictionary<string, decimal[]>();
                res.DLLINE = new Dictionary<string, DataTable>();

                foreach (DataRow row in res.DLTOTS.Rows)
                {
                    string currentTill = (string)row["TILL"];
                    string currentTran = (string)row["TRAN"];
                    string currentKey = currentTill + '.' + currentTran;

                    res.TenderAmounts.Add(currentKey, GetDLTenders(date, currentTill, currentTran));

                    MyTrace("Getting transaction lines from DLLINE");
                    cmd.CommandText = @"
SELECT DL.[NUMB], DL.[SKUN], DL.[PRIC], DL.[QUAN], DL.[EXTP], DL.[QBPD], DL.[DGPD], DL.[MBPD], DL.[HSPD], DL.[ESEV],
    DLR.[RCOD] AS RefundCode
FROM DLLINE DL
    LEFT JOIN DLRCUS DLR ON DLR.DATE1 = DL.DATE1 AND DLR.[TILL] = DL.[TILL] AND DLR.[TRAN] = DL.[TRAN] AND DLR.[NUMB] = DL.[NUMB]
WHERE DL.[DATE1] = @reportingDate
    AND DL.[TILL] = @reportTill
    AND DL.[TRAN] = @reportTran
ORDER BY DL.NUMB";
                    cmd.Parameters.Add("@reportingDate", SqlDbType.DateTime).Value = date;
                    cmd.Parameters.Add("@reportTill", SqlDbType.Char, 2).Value = currentTill;
                    cmd.Parameters.Add("@reportTran", SqlDbType.Char, 4).Value = currentTran;
                    var detailData = dataProxy.ExecuteDataTable(cmd);
                    cmd.Parameters.Clear();
                    res.DLLINE.Add(currentKey, detailData);
                }
            }

            return res;
        }

        public CashierSummary GetCashierSummary(DateTime date)
        {
            var res = new CashierSummary();

            using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
            {
                cmd.CommandText = @"
SELECT [CASH], [TILL], [TRAN], [OVCTranNumber], [Name], [VOID], [PKRC], [TMOD], [TCOD], [IEMP], [MERC], [NMER], [TAXA], [DISC], [TOTL]
FROM vwDLTOTS DT
    LEFT OUTER JOIN SystemUsers CM ON CM.EmployeeCode = DT.[CASH]
WHERE
    [DATE1] = @reportingDate
    AND CASH <> '000'
    AND TCOD <> 'XR' AND TCOD <> 'ZR'
ORDER BY CASH";

                cmd.Parameters.Add("@reportingDate", SqlDbType.DateTime).Value = date;
                var table = dataProxy.ExecuteDataTable(cmd);
                cmd.Parameters.Clear();

                res.Cashiers = new List<CashierSummary.Totals>();
                CashierSummary.Totals item = null;
                foreach (DataRow reportRow in table.Rows)
                {
                    if (item == null || item.Id != (string)reportRow["CASH"])
                    {
                        // create new cashier
                        if (item != null)
                            res.Cashiers.Add(item);

                        item = new CashierSummary.Totals();
                        item.Id = (string)reportRow["CASH"];
                        item.Name = (reportRow["Name"] == DBNull.Value) ? "* *  NOT FOUND  * *" : ((string)reportRow["Name"]).Trim();
                    }

                    if ((bool)reportRow["VOID"])
                    {
                        if (!((bool)reportRow["PKRC"]))
                            item.VoidCount++;
                        continue;
                    }

                    if ((bool)reportRow["TMOD"])
                        continue;

                    string transactionType = (string)reportRow["TCOD"];
                    decimal transactionTotal = (decimal)reportRow["TOTL"];

                    item.TotalMerch += (decimal)reportRow["MERC"];
                    item.TotalNonMerch += (decimal)reportRow["NMER"];
                    item.TotalVat += (decimal)reportRow["TAXA"];
                    item.TotalDiscount += (decimal)reportRow["DISC"];
                    item.TotalValue += transactionTotal;

                    // Check For and Process Exchanges
                    bool isExchange = false;
                    decimal lineSales = 0;
                    decimal lineRefund = 0;

                    if (transactionType == "SA" || transactionType == "RF")
                    {
                        cmd.CommandText = @"
SELECT [EXTP], [QBPD], [DGPD], [MBPD], [HSPD], [ESEV]
FROM DLLINE
WHERE [DATE1] = @reportingDate AND [TILL] = @reportingTill AND [TRAN] = @reportingTran AND LREV = 0";

                        cmd.Parameters.Add("@reportingDate", SqlDbType.DateTime).Value = date;
                        cmd.Parameters.Add("@reportingTill", SqlDbType.Char, 2).Value = (string)reportRow["TILL"];
                        cmd.Parameters.Add("@reportingTran", SqlDbType.Char, 4).Value = (string)reportRow["TRAN"];
                        var lineTable = dataProxy.ExecuteDataTable(cmd);
                        cmd.Parameters.Clear();

                        foreach (DataRow lineRow in lineTable.Rows)
                        {
                            if ((decimal)lineRow["EXTP"] >= 0)
                            {
                                lineSales += (decimal)lineRow["EXTP"];
                                lineSales -= (decimal)lineRow["QBPD"];
                                lineSales -= (decimal)lineRow["DGPD"];
                                lineSales -= (decimal)lineRow["MBPD"];
                                lineSales -= (decimal)lineRow["HSPD"];

                                if (!((bool)reportRow["IEMP"]))
                                    lineSales -= (decimal)lineRow["ESEV"];
                            }
                            else
                            {
                                lineRefund += (decimal)lineRow["EXTP"];
                                lineRefund -= (decimal)lineRow["QBPD"];
                                lineRefund -= (decimal)lineRow["DGPD"];
                                lineRefund -= (decimal)lineRow["MBPD"];
                                lineRefund -= (decimal)lineRow["HSPD"];

                                if (!((bool)reportRow["IEMP"]))
                                    lineRefund -= (decimal)lineRow["ESEV"];
                            }
                        }
                    }

                    if (lineSales != 0 & lineRefund != 0)
                    {
                        isExchange = true;

                        item.ExchangeSaleValue += lineSales;
                        item.ExchangeRefundValue += lineRefund;
                        item.NetRefundValue += lineRefund;
                        item.NetRefundValue += lineSales;

                        item.ExchangeCount++;
                        item.NetRefundCount++;
                    }

                    switch (transactionType)
                    {
                        case "SA":
                            if (!isExchange)
                            {
                                item.SalesCount++;
                                item.SalesValue += transactionTotal;
                            }
                            break;
                        case "RF":
                            if (!isExchange)
                            {
                                item.RefundCount++;
                                item.NetRefundCount++;
                                item.RefundValue += transactionTotal;
                                item.NetRefundValue += transactionTotal;
                            }
                            break;
                        case "M+":
                            item.MiscInCount++;
                            item.MiscInValue += transactionTotal;
                            break;
                        case "C+":
                            item.CorrInCount++;
                            item.CorrInValue += transactionTotal;
                            break;
                        case "CO":
                            item.SignOnCount++;
                            break;
                        case "CC":
                            item.SignOffCount++;
                            break;
                        case "M-":
                            item.MiscOutCount++;
                            item.MiscOutValue += transactionTotal;
                            break;
                        case "C-":
                            item.CorrOutCount++;
                            item.CorrOutValue += transactionTotal;
                            break;
                        case "OD":
                            item.OpenDrawerCount++;
                            break;
                    }
                }
                if (item != null)
                    res.Cashiers.Add(item);
            }

            // calculate grand totals
            res.GrandTotals = new CashierSummary.Totals()
            {
                Id = null,
                Name = "All          Grand Totals -->"
            };

            foreach (CashierSummary.Totals cashier in res.Cashiers)
            {
                res.GrandTotals.SalesCount += cashier.SalesCount;
                res.GrandTotals.RefundCount += cashier.RefundCount;
                res.GrandTotals.MiscInCount += cashier.MiscInCount;
                res.GrandTotals.CorrInCount += cashier.CorrInCount;
                res.GrandTotals.SignOnCount += cashier.SignOnCount;
                res.GrandTotals.SignOffCount += cashier.SignOffCount;
                res.GrandTotals.MiscOutCount += cashier.MiscOutCount;
                res.GrandTotals.CorrOutCount += cashier.CorrOutCount;
                res.GrandTotals.OpenDrawerCount += cashier.OpenDrawerCount;
                res.GrandTotals.VoidCount += cashier.VoidCount;
                res.GrandTotals.ExchangeCount += cashier.ExchangeCount;
                res.GrandTotals.NetRefundCount += cashier.NetRefundCount;

                res.GrandTotals.SalesValue += cashier.SalesValue;
                res.GrandTotals.RefundValue += cashier.RefundValue;
                res.GrandTotals.MiscInValue += cashier.MiscInValue;
                res.GrandTotals.CorrInValue += cashier.CorrInValue;
                res.GrandTotals.MiscOutValue += cashier.MiscOutValue;
                res.GrandTotals.CorrOutValue += cashier.CorrOutValue;
                res.GrandTotals.ExchangeSaleValue += cashier.ExchangeSaleValue;
                res.GrandTotals.ExchangeRefundValue += cashier.ExchangeRefundValue;
                res.GrandTotals.NetRefundValue += cashier.NetRefundValue;

                res.GrandTotals.TotalMerch += cashier.TotalMerch;
                res.GrandTotals.TotalNonMerch += cashier.TotalNonMerch;
                res.GrandTotals.TotalVat += cashier.TotalVat;
                res.GrandTotals.TotalDiscount += cashier.TotalDiscount;
                res.GrandTotals.TotalValue += cashier.TotalValue;
            }

            return res;
        }

        public TillDetails GetTillDetails(DateTime date, int tillId)
        {
            var res = new TillDetails();
            res.TenderDescriptions = GetTenderDescriptions();
            res.RefundCodeDescriptions = GetRefundReasonDescriptions();

            using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
            {
                MyTrace("Getting transactions from DLTOTS");
                cmd.CommandText = @"
SELECT [CASH], [TIME], [TCOD], [DESCR], [TRAN], [OVCTranNumber], [TOTL], [TAXA], [ORDN], [TMOD], [PARK], [PKRC], [VOID],
    [TILL]
FROM vwDLTOTS DT
WHERE [DATE1] = @reportingDate AND TILL = @reportTill
ORDER BY [TILL], [TIME], [TRAN]";

                cmd.Parameters.Add("@reportingDate", SqlDbType.DateTime).Value = date;
                cmd.Parameters.Add("@reportTill", SqlDbType.Char, 2).Value = tillId.ToString("00");
                res.DLTOTS = dataProxy.ExecuteDataTable(cmd);
                cmd.Parameters.Clear();

                res.TenderAmounts = new Dictionary<string, decimal[]>();
                res.DLLINE = new Dictionary<string, DataTable>();

                foreach (DataRow row in res.DLTOTS.Rows)
                {
                    string currentTill = (string)row["TILL"];
                    string currentTran = (string)row["TRAN"];
                    string currentKey = currentTill + '.' + currentTran;

                    res.TenderAmounts.Add(currentKey, GetDLTenders(date, currentTill, currentTran));

                    MyTrace("Getting transaction lines from DLLINE");
                    cmd.CommandText = @"
SELECT DL.[NUMB], DL.[SKUN], DL.[PRIC], DL.[QUAN], DL.[EXTP], DL.[QBPD], DL.[DGPD], DL.[MBPD], DL.[HSPD], DL.[ESEV],
    DLR.[RCOD] AS RefundCode
FROM DLLINE DL
    LEFT JOIN DLRCUS DLR ON DLR.DATE1 = DL.DATE1 AND DLR.[TILL] = DL.[TILL] AND DLR.[TRAN] = DL.[TRAN] AND DLR.[NUMB] = DL.[NUMB]
WHERE DL.DATE1 = @reportingDate
    AND DL.TILL = @reportTill
    AND DL.[TRAN] = @reportTran
ORDER BY DL.NUMB";
                    cmd.Parameters.Add("@reportingDate", SqlDbType.DateTime).Value = date;
                    cmd.Parameters.Add("@reportTill", SqlDbType.Char, 2).Value = currentTill;
                    cmd.Parameters.Add("@reportTran", SqlDbType.Char, 4).Value = currentTran;
                    var detailData = dataProxy.ExecuteDataTable(cmd);
                    cmd.Parameters.Clear();
                    res.DLLINE.Add(currentKey, detailData);
                }
            }

            return res;
        }

        public TillSummary GetTillSummary(DateTime date)
        {
            var res = new TillSummary();

            using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
            {
                cmd.CommandText = @"
SELECT [TILL], [TRAN], [OVCTranNumber],[VOID], [PKRC], [TMOD], [TCOD], [IEMP], [MERC], [NMER], [TAXA], [DISC], [TOTL]
FROM vwDLTOTS DT
WHERE [DATE1] = @reportingDate AND TILL <> '00'
ORDER BY [TILL], [TIME], [TRAN]";
                cmd.Parameters.Add("@reportingDate", SqlDbType.DateTime).Value = date;
                var table = dataProxy.ExecuteDataTable(cmd);
                cmd.Parameters.Clear();

                res.Tills = new List<TillSummary.Totals>();
                TillSummary.Totals item = null;
                foreach (DataRow reportRow in table.Rows)
                {
                    if (item == null || item.Id != (string)reportRow["Till"])
                    {
                        // create new cashier
                        if (item != null)
                            res.Tills.Add(item);

                        item = new TillSummary.Totals();
                        item.Id = (string)reportRow["TILL"];
                        item.FirstTran = (string)reportRow["TRAN"];
                        if (reportRow["OVCTranNumber"] != DBNull.Value)
                        {
                            item.FirstOVCTran = (string)reportRow["OVCTranNumber"];
                        }
                    }

                    item.LastTran = (string)reportRow["TRAN"];
                    if (reportRow["OVCTranNumber"] != DBNull.Value)
                    {
                        item.LastOVCTran = (string)reportRow["OVCTranNumber"];
                    }
                    // accumulate values
                    if ((bool)reportRow["VOID"])
                    {
                        if (!((bool)reportRow["PKRC"]))
                            item.VoidCount++;
                        continue;
                    }

                    string transactionType = (string)reportRow["TCOD"];
                    decimal transactionTotal = (decimal)reportRow["TOTL"];

                    if ((bool)reportRow["TMOD"])
                    {
                        item.TrainingCount++;
                        item.TrainingValue += transactionTotal;
                        item.TotalTraining = item.TrainingValue;
                        continue;
                    }

                    switch (transactionType)
                    {
                        case "ZR":
                            item.ZReadCount++;
                            item.ZReadValue += transactionTotal;
                            continue;
                        case "XR":
                            item.XReadCount++;
                            item.XReadValue += transactionTotal;
                            continue;
                    }

                    item.TotalMerch += (decimal)reportRow["MERC"];
                    item.TotalNonMerch += (decimal)reportRow["NMER"];
                    item.TotalVat += (decimal)reportRow["TAXA"];
                    item.TotalDiscount += (decimal)reportRow["DISC"];
                    item.TotalValue += transactionTotal;

                    // Check For and Process Exchanges
                    bool isExchange = false;
                    decimal lineSales = 0;
                    decimal lineRefund = 0;

                    if (transactionType == "SA" | transactionType == "RF")
                    {
                        cmd.CommandText = @"
SELECT [EXTP], [QBPD], [DGPD], [MBPD], [HSPD], [ESEV]
FROM DLLINE
WHERE [DATE1] = @reportingDate AND [TILL] = @reportingTill AND [TRAN] = @reportingTran AND LREV = 0";

                        cmd.Parameters.Add("@reportingDate", SqlDbType.DateTime).Value = date;
                        cmd.Parameters.Add("@reportingTill", SqlDbType.Char, 2).Value = item.Id;
                        cmd.Parameters.Add("@reportingTran", SqlDbType.Char, 4).Value = (string)reportRow["TRAN"];
                        var lineData = dataProxy.ExecuteDataTable(cmd);
                        cmd.Parameters.Clear();

                        foreach (DataRow lineRow in lineData.Rows)
                        {
                            if ((decimal)lineRow["EXTP"] >= 0)
                            {
                                lineSales += (decimal)lineRow["EXTP"];
                                lineSales -= (decimal)lineRow["QBPD"];
                                lineSales -= (decimal)lineRow["DGPD"];
                                lineSales -= (decimal)lineRow["MBPD"];
                                lineSales -= (decimal)lineRow["HSPD"];

                                if (!((bool)reportRow["IEMP"]))
                                    lineSales -= (decimal)lineRow["ESEV"];
                            }
                            else
                            {
                                lineRefund += (decimal)lineRow["EXTP"];
                                lineRefund -= (decimal)lineRow["QBPD"];
                                lineRefund -= (decimal)lineRow["DGPD"];
                                lineRefund -= (decimal)lineRow["MBPD"];
                                lineRefund -= (decimal)lineRow["HSPD"];

                                if (!((bool)reportRow["IEMP"]))
                                    lineRefund -= (decimal)lineRow["ESEV"];
                            }
                        }
                    }

                    if (lineSales != 0 & lineRefund != 0)
                    {
                        isExchange = true;

                        item.ExchangeSaleValue += lineSales;
                        item.ExchangeRefundValue += lineRefund;
                        item.NetRefundValue += lineRefund;
                        item.NetRefundValue += lineSales;

                        item.ExchangeCount++;
                        item.NetRefundCount++;
                    }

                    switch (transactionType)
                    {
                        case "SA":
                            if (!isExchange)
                            {
                                item.SalesCount++;
                                item.SalesValue += transactionTotal;
                            }
                            break;
                        case "RF":
                            if (!isExchange)
                            {
                                item.RefundCount++;
                                item.NetRefundCount++;
                                item.RefundValue += transactionTotal;
                                item.NetRefundValue += transactionTotal;
                            }
                            break;
                        case "M+":
                            item.MiscInCount++;
                            item.MiscInValue += transactionTotal;
                            break;
                        case "C+":
                            item.CorrInCount++;
                            item.CorrInValue += transactionTotal;
                            break;
                        case "CO":
                            item.SignOnCount++;
                            break;
                        case "CC":
                            item.SignOffCount++;
                            break;
                        case "M-":
                            item.MiscOutCount++;
                            item.MiscOutValue += transactionTotal;
                            break;
                        case "C-":
                            item.CorrOutCount++;
                            item.CorrOutValue += transactionTotal;
                            break;
                        case "OD":
                            item.OpenDrawerCount++;
                            break;
                    }
                }

                if (item != null)
                    res.Tills.Add(item);
            }

            // calculate grand totals
            res.GrandTotals = new TillSummary.Totals()
            {
                Id = "**",
                FirstTran = "All",
                LastTran = null
            };

            foreach (TillSummary.Totals item in res.Tills)
            {
                res.GrandTotals.SalesCount += item.SalesCount;
                res.GrandTotals.RefundCount += item.RefundCount;
                res.GrandTotals.MiscInCount += item.MiscInCount;
                res.GrandTotals.CorrInCount += item.CorrInCount;
                res.GrandTotals.SignOnCount += item.SignOnCount;
                res.GrandTotals.SignOffCount += item.SignOffCount;
                res.GrandTotals.MiscOutCount += item.MiscOutCount;
                res.GrandTotals.CorrOutCount += item.CorrOutCount;
                res.GrandTotals.OpenDrawerCount += item.OpenDrawerCount;
                res.GrandTotals.VoidCount += item.VoidCount;
                res.GrandTotals.ExchangeCount += item.ExchangeCount;
                res.GrandTotals.NetRefundCount += item.NetRefundCount;

                res.GrandTotals.SalesValue += item.SalesValue;
                res.GrandTotals.RefundValue += item.RefundValue;
                res.GrandTotals.MiscInValue += item.MiscInValue;
                res.GrandTotals.CorrInValue += item.CorrInValue;
                res.GrandTotals.MiscOutValue += item.MiscOutValue;
                res.GrandTotals.CorrOutValue += item.CorrOutValue;
                res.GrandTotals.ExchangeSaleValue += item.ExchangeSaleValue;
                res.GrandTotals.ExchangeRefundValue += item.ExchangeRefundValue;
                res.GrandTotals.NetRefundValue += item.NetRefundValue;

                res.GrandTotals.XReadCount += item.XReadCount;
                res.GrandTotals.XReadValue += item.XReadValue;

                res.GrandTotals.ZReadCount += item.ZReadCount;
                res.GrandTotals.ZReadValue += item.ZReadValue;

                res.GrandTotals.TrainingCount += item.TrainingCount;
                res.GrandTotals.TrainingValue += item.TrainingValue;

                res.GrandTotals.TotalMerch += item.TotalMerch;
                res.GrandTotals.TotalNonMerch += item.TotalNonMerch;
                res.GrandTotals.TotalVat += item.TotalVat;
                res.GrandTotals.TotalDiscount += item.TotalDiscount;
                res.GrandTotals.TotalValue += item.TotalValue;
                res.GrandTotals.TotalTraining += item.TotalTraining;
            }

            return res;
        }
    }
}
