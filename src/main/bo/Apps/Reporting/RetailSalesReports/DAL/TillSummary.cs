﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RetailSalesReports.DAL
{
    public class TillSummary
    {
        public class Totals
        {
            public string Id;
            public string FirstTran;
            public string FirstOVCTran;
            public string LastOVCTran;
            public string LastTran;

            public decimal TotalValue = 0;
            public decimal TotalVat = 0;
            public decimal TotalMerch = 0;
            public decimal TotalNonMerch = 0;
            public decimal TotalDiscount = 0;
            public decimal TotalTraining = 0;

            public int SalesCount = 0;
            public decimal SalesValue = 0;

            public int SignOnCount = 0;

            public int RefundCount = 0;
            public decimal RefundValue = 0;

            public int SignOffCount = 0;

            public int ExchangeCount = 0;
            public decimal ExchangeSaleValue = 0;
            public decimal ExchangeRefundValue = 0;

            public int OpenDrawerCount = 0;

            public int VoidCount = 0;

            public int NetRefundCount = 0;
            public decimal NetRefundValue = 0;

            public int MiscInCount = 0;
            public decimal MiscInValue = 0;

            // Miscellaneous In Corrections
            public int CorrInCount = 0;
            public decimal CorrInValue = 0;

            public int MiscOutCount = 0;
            public decimal MiscOutValue = 0;

            public int CorrOutCount = 0;
            public decimal CorrOutValue = 0;

            public int XReadCount = 0;
            public decimal XReadValue = 0;

            public int ZReadCount = 0;
            public decimal ZReadValue = 0;

            public int TrainingCount = 0;
            public decimal TrainingValue = 0;

            public Totals()
            {
            }
        }

        public List<Totals> Tills;
        public Totals GrandTotals;
    }
}
