﻿Imports FarPoint.Win
Imports System.Data.SqlClient

Public Class ShortDeliveryReport
    Private _Proxy As Oasys.DataProxy
    Private _PrintNotPrinted As Boolean
    Private _ReportDate As DateTime
    Private _CalledFromNight As Boolean
    Private _StoreNumber As String
    Private _StoreName As String
    Private _ShortAssemblyName As String
    Private _FileVersion As String

    Private Sub New()
        MyBase.New()
        SetupEnvironment()
    End Sub

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        SetupEnvironment()
    End Sub
    Private Sub SetupEnvironment()

        With FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetAssembly(Me.GetType).Location)
            _ShortAssemblyName = .FileName.Substring(.FileName.LastIndexOf("\"c) + 1, .FileName.LastIndexOf("."c) - .FileName.LastIndexOf("\"c) - 1)
            _FileVersion = .FileVersion
        End With

        MyTrace("Initialising")
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Dim db As New OasysDBBO.Oasys3.DB.clsOasys3DB()
        _Proxy = CType(Activator.CreateInstance(db.GetConfigValue("ProxyAssembly"), db.GetConfigValue("ProxyTypeName"), False, Reflection.BindingFlags.CreateInstance, Nothing, New Object() {db.GetConfigValue("sqlConnection")}, Globalization.CultureInfo.InvariantCulture, Nothing, Nothing).Unwrap, Oasys.DataProxy)
        _ReportDate = CDate(_Proxy.ExecuteScalar(New SqlCommand("SELECT TODT FROM SYSDAT")))
        dtpReportDate.Value = _ReportDate

        With _Proxy.ExecuteDataTable(New SqlCommand("SELECT STOR, SNAM FROM RETOPT WHERE FKEY='01'")).Rows(0)
            _StoreNumber = CStr(.Item("STOR"))
            _StoreName = CStr(.Item("SNAM"))
        End With

        _CalledFromNight = RunParameters.Contains(",CFC")
    End Sub

    Private Sub Print()
        Dim row As Integer = 0
        Dim reportData As DataTable = Nothing

        Dim shortCount As Integer
        Dim shortValue As Decimal

        Dim totalShortCount As Integer
        Dim totalShortValue As Decimal = 0D

        Dim currentPO As String = String.Empty
        Dim currentSupplier As String = String.Empty

        Try
            If ReportSpread_Sheet1.RowCount > 0 Then
                ReportSpread_Sheet1.RemoveRows(0, ReportSpread_Sheet1.RowCount)
            End If

            Using reportCommand As New SqlClient.SqlCommand

                ReportSpread.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                ReportSpread_Sheet1.ColumnCount = 9

                For i As Integer = 0 To ReportSpread_Sheet1.ColumnCount - 1
                    ReportSpread_Sheet1.Columns(i).CellType = New Spread.CellType.TextCellType
                Next

                ReportSpread_Sheet1.Columns(0).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(1).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(2).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(3).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(4).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
                ReportSpread_Sheet1.Columns(5).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
                ReportSpread_Sheet1.Columns(6).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
                ReportSpread_Sheet1.Columns(7).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
                ReportSpread_Sheet1.Columns(8).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right

                ReportSpread_Sheet1.Columns(0).Width = 61
                ReportSpread_Sheet1.Columns(1).Width = 250
                ReportSpread_Sheet1.Columns(2).Width = 52
                ReportSpread_Sheet1.Columns(3).Width = 349
                ReportSpread_Sheet1.Columns(4).Width = 71
                ReportSpread_Sheet1.Columns(5).Width = 71
                ReportSpread_Sheet1.Columns(6).Width = 67
                ReportSpread_Sheet1.Columns(7).Width = 76
                ReportSpread_Sheet1.Columns(8).Width = 95

                ReportSpread_Sheet1.Columns(0).Label = "P/O NO"
                ReportSpread_Sheet1.Columns(1).Label = "Supplier"
                ReportSpread_Sheet1.Columns(2).Label = "ITEM"
                ReportSpread_Sheet1.Columns(3).Label = "DESCRIPTION"
                ReportSpread_Sheet1.Columns(4).Label = "O QUAN"
                ReportSpread_Sheet1.Columns(5).Label = "R QUAN"
                ReportSpread_Sheet1.Columns(6).Label = "SHORT"
                ReportSpread_Sheet1.Columns(7).Label = "PRICE"
                ReportSpread_Sheet1.Columns(8).Label = "SHORT VAL"

                'set up headings 
                ReportSpread_Sheet1.RowCount = 1
                row = 0


                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ' Author      : Dhanesh Ramachandran
                ' Date        : 11/11/2010
                ' Referral No : 443
                ' Notes       : Modified to display proper records
                '
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                reportCommand.CommandText = String.Concat( _
                    "SELECT ", _
                        "ds.[0PON], ds.[0SUP], Coalesce(vm.Name, '***** NOT ON FILE *****') [SupplierName], ", _
                        "dd.[SKUN], Coalesce(im.Descr, '***** NOT FOUND *****') Descr, ", _
                        "dd.[ORDQ], dd.[RECQ], dd.[PRIC], ", _
                        "(dd.[ORDQ] - dd.[RECQ]) ShortQuantity, ((dd.[ORDQ] - dd.[RECQ]) * dd.[PRIC]) ShortValue ", _
                    "FROM DRLSUM ds ", _
                    "INNER JOIN DRLDET dd ON ", _
                        "dd.[NUMB] = ds.[NUMB] AND ", _
                        "dd.[RECQ] < dd.[ORDQ] ", _
                    "INNER JOIN PURLIN pl ON ", _
                        "pl.[HKEY] = ds.[TKEY] AND ", _
                        "pl.[SKUN] = dd.[SKUN]", _
                    "LEFT OUTER JOIN SUPMAS vm ON ", _
                        "vm.[SUPN] = ds.[0SUP] ", _
                    "LEFT OUTER JOIN STKMAS im ON ", _
                        "im.[SKUN] = dd.[SKUN] ", _
                    "WHERE ", _
                        "ds.[DATE1] = @reportingDate AND ", _
                        "ds.[TYPE] = '0' AND ", _
                        "ds.IPSO = 0 AND ", _
                        "RTrim([0DL3]) <> 'IMPORT' ", _
                    "ORDER BY ds.[NUMB], dd.[SKUN], dd.SEQN " _
                )
                reportCommand.Parameters.Add("@reportingDate", SqlDbType.DateTime).Value = _ReportDate

                reportData = _Proxy.ExecuteDataTable(reportCommand)
                reportCommand.Parameters.Clear()

                MyTrace("Generating report")

                ReportSpread_Sheet1.SetText(row, 0, String.Format("DRL DATE: {0}", _ReportDate.ToString("d")))

                ReportSpread_Sheet1.RowCount += 2
                row += 2

                For Each reportRow As DataRow In reportData.Rows
                    If currentPO <> CStr(reportRow("0PON")) Or _
                       currentSupplier <> CStr(reportRow("0SUP")) Then
                        currentPO = CStr(reportRow("0PON"))
                        currentSupplier = CStr(reportRow("0SUP"))

                        ReportSpread_Sheet1.SetText(row, 0, currentPO)
                        ReportSpread_Sheet1.SetText(row, 1, String.Format("{0} {1}", currentSupplier, CStr(reportRow("SupplierName"))))
                    End If
                    shortCount = CInt(reportRow("ShortQuantity"))
                    shortValue = CDec(reportRow("ShortValue"))
                    ReportSpread_Sheet1.SetText(row, 2, CStr(reportRow("SKUN")))
                    ReportSpread_Sheet1.SetText(row, 3, CStr(reportRow("Descr")))
                    ReportSpread_Sheet1.SetText(row, 4, CStr(reportRow("ORDQ")))
                    ReportSpread_Sheet1.SetText(row, 5, CStr(reportRow("RECQ")))

                    ReportSpread_Sheet1.SetText(row, 6, shortCount.ToString())
                    ReportSpread_Sheet1.SetText(row, 7, CDec(reportRow("PRIC")).ToString("0.00"))

                    ReportSpread_Sheet1.SetText(row, 8, shortValue.ToString("0.00"))

                    totalShortCount += shortCount
                    totalShortValue += shortValue

                    row += 1
                    ReportSpread_Sheet1.RowCount += 1
                Next

                ReportSpread_Sheet1.SetText(row, 0, "GRAND TOTAL")

                ReportSpread_Sheet1.SetText(row, 6, totalShortCount.ToString())
                ReportSpread_Sheet1.SetText(row, 8, totalShortValue.ToString("0.00"))

                ReportSpread_Sheet1.Cells(row, 6, row, 6).Border = New LineBorder(Drawing.Color.Black, 1, False, True, False, True)
                ReportSpread_Sheet1.Cells(row, 8, row, 8).Border = New LineBorder(Drawing.Color.Black, 1, False, True, False, True)
            End Using

            ReportSpread_Sheet1.PrintInfo.Header = _
                    String.Concat( _
                        _ShortAssemblyName, "  /  v", _FileVersion, _
                        New String(" "c, 10), "Short-Delivery Report", _
                        New String(" "c, 10), "Store: ", _StoreNumber, "-"c, _StoreName, _
                        New String(" "c, 10), "Date Printed: ", DateTime.Now.Date.ToString("d"), _
                        "/rPage: /p" _
                   )
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        Finally
            If reportData IsNot Nothing Then
                reportData.Dispose()
            End If
        End Try
    End Sub

    Private Sub ReportSpread_ColumnWidthChanged(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.ColumnWidthChangedEventArgs) Handles ReportSpread.ColumnWidthChanged
        Debug.WriteLine("Report Column Widths:")
        For i As Integer = 0 To ReportSpread_Sheet1.ColumnCount - 1
            Debug.WriteLine(String.Format("ReportSpread_Sheet1.Columns({0}).Width = {1}", i, ReportSpread_Sheet1.Columns(i).Width))
        Next
    End Sub

    Private Sub ReportSpread_PrintMessageBox(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.PrintMessageBoxEventArgs) Handles ReportSpread.PrintMessageBox
        If e.BeginPrinting = False Then _PrintNotPrinted = False
    End Sub

    Private Sub Finish()
        _PrintNotPrinted = True
        Do While _PrintNotPrinted
            Application.DoEvents()
            System.Threading.Thread.Sleep(1)
        Loop
    End Sub

    Private Sub ParentForm_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs)
        RetrieveButton.PerformClick()
    End Sub

    Private Sub RetrieveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RetrieveButton.Click
        Cursor = Cursors.WaitCursor
        ButtonPanel.Enabled = False
        CollapsibleGroupBox1.Enabled = False
        Print()
        ButtonPanel.Enabled = True
        CollapsibleGroupBox1.Enabled = True
        Cursor = Cursors.Default
        PrintButton.Enabled = True

        If _CalledFromNight Then PrintButton.PerformClick()
    End Sub

    Private Sub PrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintButton.Click
        Cursor = Cursors.WaitCursor
        ButtonPanel.Enabled = False
        CollapsibleGroupBox1.Enabled = False

        ReportSpread_Sheet1.PrintInfo.Margin.Header = CInt(ReportSpread_Sheet1.Rows(-1).Height)
        ReportSpread.PrintSheet(0)
        'wait until printing finished
        Finish()
        ButtonPanel.Enabled = True
        CollapsibleGroupBox1.Enabled = True
        Cursor = Cursors.Default

        If _CalledFromNight Then ExitButton.PerformClick()
    End Sub

    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click
        FindForm.Close()
    End Sub

    Private Sub _KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs)
        Select Case e.KeyCode
            Case Keys.F5
                RetrieveButton.PerformClick()
            Case Keys.F9
                PrintButton.PerformClick()
            Case Keys.F12
                ExitButton.PerformClick()
        End Select
    End Sub

    Private Sub dtpReportDate_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dtpReportDate.ValueChanged
        _ReportDate = CType(sender, DateTimePicker).Value.Date
        PrintButton.Enabled = False
    End Sub

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        AddHandler FindForm.Shown, AddressOf ParentForm_Shown
        AddHandler FindForm.KeyDown, AddressOf _KeyDown
        FindForm.KeyPreview = True
        ReportSpread_Sheet1.OperationMode = Spread.OperationMode.ReadOnly
    End Sub

    Private Sub MyTrace(ByVal message As String)
        Trace.WriteLine(String.Format("{0}/v{1} - {2}", _ShortAssemblyName, _FileVersion, message))
    End Sub
End Class
