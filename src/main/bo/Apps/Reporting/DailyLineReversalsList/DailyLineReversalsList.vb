﻿Imports FarPoint.Win
Imports System.Data.SqlClient

Public Class DailyLineReversalsList
    Private _Proxy As Oasys.DataProxy
    Private _PrintNotPrinted As Boolean
    Private _ReportDate As DateTime
    Private _CalledFromNight As Boolean
    Private _StoreNumber As String
    Private _StoreName As String
    Private _ShortAssemblyName As String
    Private _FileVersion As String

    Private Sub New()
        MyBase.New()
        SetupEnvironment()
    End Sub

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        SetupEnvironment()
    End Sub

    Private Sub SetupEnvironment()
        Dim workdate As String

        With FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetAssembly(Me.GetType).Location)
            _ShortAssemblyName = .FileName.Substring(.FileName.LastIndexOf("\"c) + 1, .FileName.LastIndexOf("."c) - .FileName.LastIndexOf("\"c) - 1)
            _FileVersion = .FileVersion
        End With

        MyTrace("Initialising")
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Dim db As New OasysDBBO.Oasys3.DB.clsOasys3DB()
        _Proxy = CType(Activator.CreateInstance(db.GetConfigValue("ProxyAssembly"), db.GetConfigValue("ProxyTypeName"), False, Reflection.BindingFlags.CreateInstance, Nothing, New Object() {db.GetConfigValue("sqlConnection")}, Globalization.CultureInfo.InvariantCulture, Nothing, Nothing).Unwrap, Oasys.DataProxy)
        workdate = CStr(_Proxy.ExecuteScalar(New SqlCommand("SELECT DOLR FROM RETOPT")))
        _ReportDate = New DateTime( _
            CInt(workdate.Substring(6, 2)) + 2000, _
            CInt(workdate.Substring(3, 2)), _
            CInt(workdate.Substring(0, 2)) _
        )
        dtpReportDate.Value = _ReportDate

        With _Proxy.ExecuteDataTable(New SqlCommand("SELECT STOR, SNAM FROM RETOPT WHERE FKEY='01'")).Rows(0)
            _StoreNumber = CStr(.Item("STOR"))
            _StoreName = CStr(.Item("SNAM"))
        End With

        _CalledFromNight = RunParameters.Contains(",CFC")
    End Sub

    Private Sub Print()
        Dim row As Integer = 0
        Dim reportData As DataTable = Nothing

        Dim saveCashier As String = String.Empty

        Try
            If ReportSpread_Sheet1.RowCount > 0 Then
                ReportSpread_Sheet1.RemoveRows(0, ReportSpread_Sheet1.RowCount)
            End If

            Using reportCommand As New SqlClient.SqlCommand
                ReportSpread.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                ReportSpread_Sheet1.ColumnCount = 10

                For i As Integer = 0 To ReportSpread_Sheet1.ColumnCount - 1
                    ReportSpread_Sheet1.Columns(i).CellType = New Spread.CellType.TextCellType
                Next

                ReportSpread_Sheet1.Columns(0).Width = 173
                ReportSpread_Sheet1.Columns(1).Width = 36
                ReportSpread_Sheet1.Columns(2).Width = 45
                ReportSpread_Sheet1.Columns(3).Width = 56
                ReportSpread_Sheet1.Columns(4).Width = 132
                ReportSpread_Sheet1.Columns(5).Width = 92
                ReportSpread_Sheet1.Columns(6).Width = 69
                ReportSpread_Sheet1.Columns(7).Width = 92
                ReportSpread_Sheet1.Columns(8).Width = 58
                ReportSpread_Sheet1.Columns(9).Width = 92

                ReportSpread_Sheet1.Columns(0).Label = "CASHIER"
                ReportSpread_Sheet1.Columns(1).Label = "TS"
                ReportSpread_Sheet1.Columns(2).Label = "TILL"
                ReportSpread_Sheet1.Columns(3).Label = "TRAN"
                ReportSpread_Sheet1.Columns(4).Label = "OVC TRAN"
                ReportSpread_Sheet1.Columns(5).Label = "AMOUNT"
                ReportSpread_Sheet1.Columns(6).Label = "SKU NO"
                ReportSpread_Sheet1.Columns(7).Label = "PRICE"
                ReportSpread_Sheet1.Columns(8).Label = "QUAN"
                ReportSpread_Sheet1.Columns(9).Label = "EXT VALUE"

                ReportSpread_Sheet1.Columns(0).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left
                ReportSpread_Sheet1.Columns(1).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Center
                ReportSpread_Sheet1.Columns(2).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Center
                ReportSpread_Sheet1.Columns(3).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Center
                ReportSpread_Sheet1.Columns(4).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Center
                ReportSpread_Sheet1.Columns(5).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
                ReportSpread_Sheet1.Columns(6).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Center
                ReportSpread_Sheet1.Columns(7).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
                ReportSpread_Sheet1.Columns(8).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right
                ReportSpread_Sheet1.Columns(9).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Right

                'set up headings 
                ReportSpread_Sheet1.RowCount = 1
                row = 0

                reportCommand.CommandText = String.Concat( _
                    "SELECT ", _
                        "dt.[CASH], Coalesce(cm.[NAME], '* *   NOT FOUND   * *') [CashierName], ", _
                        "dt.[TCOD], dt.[TILL], dt.[TRAN], dt.[TOTL], ", _
                        "dl.[SKUN], dl.[PRIC], dl.[QUAN], dl.[EXTP], dt.[OVCTranNumber] ", _
                    "FROM vwDLTOTS dt ", _
                    "INNER JOIN DLLINE dl ON ", _
                        "dl.[DATE1] = dt.[DATE1] AND ", _
                        "dl.[TILL] = dt.[TILL] AND ", _
                        "dl.[TRAN] = dt.[TRAN] AND ", _
                        "dl.[LREV] = 1 AND ", _
                        "Sign(dl.[EXTP]) <> 0 AND ", _
                        "Sign(dl.[EXTP]) = -Sign(dt.[TOTL]) ", _
                    "LEFT OUTER JOIN SystemUsers cm ON ", _
                        "cm.[EmployeeCode] = dt.[CASH] ", _
                    "WHERE ", _
                        "dt.[DATE1] = @reportingDate AND ", _
                        "dt.[CASH] <> '000' AND ", _
                        "(dt.[TCOD] = 'SA' OR ", _
                        " dt.[TCOD] = 'SC' OR ", _
                        " dt.[TCOD] = 'RF' OR ", _
                        " dt.[TCOD] = 'RC') ", _
                    "ORDER BY dt.[CASH], dt.[TIME], dt.[TILL], dt.[TRAN], dl.[NUMB] " _
                )
                reportCommand.Parameters.Add("@reportingDate", SqlDbType.DateTime).Value = _ReportDate

                MyTrace("Acquiring data")
                reportData = _Proxy.ExecuteDataTable(reportCommand)
                reportCommand.Parameters.Clear()

                MyTrace("Generating report")

                For Each reportRow As DataRow In reportData.Rows
                    If (CStr(reportRow("CASH")) <> saveCashier) Then
                        saveCashier = CStr(reportRow("CASH"))
                        ReportSpread_Sheet1.SetText(row, 0, String.Format("{0} {1}", saveCashier, CStr(reportRow("CashierName"))))
                    End If

                    ReportSpread_Sheet1.SetText(row, 1, CStr(reportRow("TCOD")))
                    ReportSpread_Sheet1.SetText(row, 2, CStr(reportRow("TILL")))
                    ReportSpread_Sheet1.SetText(row, 3, CStr(reportRow("TRAN")))
                    If Not Equals(reportRow("OVCTranNumber"), DBNull.Value) Then
                        ReportSpread_Sheet1.SetText(row, 4, CStr(reportRow("OVCTranNumber")))
                    End If
                    ReportSpread_Sheet1.SetText(row, 5, CDec(reportRow("TOTL")).ToString("0.00"))
                    If CStr(reportRow("SKUN")) <> "000000" Then
                        ReportSpread_Sheet1.SetText(row, 6, CStr(reportRow("SKUN")))
                    End If
                    ReportSpread_Sheet1.SetText(row, 7, CDec(reportRow("PRIC")).ToString("0.00"))
                    ReportSpread_Sheet1.SetText(row, 8, CInt(reportRow("QUAN")).ToString())
                    ReportSpread_Sheet1.SetText(row, 9, CDec(reportRow("EXTP")).ToString("0.00"))
                    
                    row += 1
                    ReportSpread_Sheet1.RowCount += 1
                Next

                row += 1
                ReportSpread_Sheet1.RowCount += 1

                ReportSpread_Sheet1.SetText(row, 0, String.Concat("Data Date = ", _ReportDate.ToShortDateString))
                row += 1
                ReportSpread_Sheet1.RowCount += 1

                ReportSpread_Sheet1.SetText(row, 0, _StoreName)
            End Using

            ReportSpread_Sheet1.PrintInfo.Header = _
                    String.Concat( _
                        _ShortAssemblyName, "  /  v", _FileVersion, _
                        New String(" "c, 10), "Daily Line Reversals List", _
                        New String(" "c, 10), "Store: ", _StoreNumber, "-"c, _StoreName, _
                        New String(" "c, 10), "Date Printed: ", DateTime.Now.Date.ToString("d"), _
                        "/rPage: /p" _
                   )
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        Finally
            If reportData IsNot Nothing Then
                reportData.Dispose()
                reportData = Nothing
            End If
        End Try
    End Sub

    Private Sub ReportSpread_ColumnWidthChanged(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.ColumnWidthChangedEventArgs) Handles ReportSpread.ColumnWidthChanged
        Debug.WriteLine("Report Column Widths:")
        For i As Integer = 0 To ReportSpread_Sheet1.ColumnCount - 1
            Debug.WriteLine(String.Format("ReportSpread_Sheet1.Columns({0}).Width = {1}", i, ReportSpread_Sheet1.Columns(i).Width))
        Next
    End Sub

    Private Sub ReportSpread_PrintMessageBox(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.PrintMessageBoxEventArgs) Handles ReportSpread.PrintMessageBox
        If e.BeginPrinting = False Then _PrintNotPrinted = False
    End Sub

    Private Sub Finish()
        _PrintNotPrinted = True
        Do While _PrintNotPrinted
            Application.DoEvents()
            System.Threading.Thread.Sleep(1)
        Loop
    End Sub

    Private Sub ParentForm_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs)
        RetrieveButton.PerformClick()
    End Sub

    Private Sub RetrieveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RetrieveButton.Click
        Cursor = Cursors.WaitCursor
        ButtonPanel.Enabled = False
        CollapsibleGroupBox1.Enabled = False
        Print()
        ButtonPanel.Enabled = True
        CollapsibleGroupBox1.Enabled = True
        Cursor = Cursors.Default
        PrintButton.Enabled = True

        If _CalledFromNight Then PrintButton.PerformClick()
    End Sub

    Private Sub PrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintButton.Click
        Cursor = Cursors.WaitCursor
        ButtonPanel.Enabled = False
        CollapsibleGroupBox1.Enabled = False

        ReportSpread_Sheet1.PrintInfo.Margin.Header = CInt(ReportSpread_Sheet1.Rows(-1).Height)
        ReportSpread.PrintSheet(0)
        'wait until printing finished
        Finish()
        ButtonPanel.Enabled = True
        CollapsibleGroupBox1.Enabled = True
        Cursor = Cursors.Default

        If _CalledFromNight Then ExitButton.PerformClick()
    End Sub

    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click
        FindForm.Close()
    End Sub

    Private Sub _KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs)
        Select Case e.KeyCode
            Case Keys.F5
                RetrieveButton.PerformClick()
            Case Keys.F9
                PrintButton.PerformClick()
            Case Keys.F12
                ExitButton.PerformClick()
        End Select
    End Sub

    Private Sub dtpReportDate_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dtpReportDate.ValueChanged
        _ReportDate = CType(sender, DateTimePicker).Value.Date
        PrintButton.Enabled = False
    End Sub

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        AddHandler FindForm.Shown, AddressOf ParentForm_Shown
        AddHandler FindForm.KeyDown, AddressOf _KeyDown
        FindForm.KeyPreview = True
        ReportSpread_Sheet1.OperationMode = Spread.OperationMode.ReadOnly
    End Sub

    Private Sub MyTrace(ByVal message As String)
        Trace.WriteLine(String.Format("{0}/v{1} - {2}", _ShortAssemblyName, _FileVersion, message))
    End Sub

End Class
