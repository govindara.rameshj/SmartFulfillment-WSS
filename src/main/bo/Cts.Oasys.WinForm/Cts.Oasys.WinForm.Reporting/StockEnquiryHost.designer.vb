﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StockEnquiryHost
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblSkuLabel = New DevExpress.XtraEditors.LabelControl
        Me.lblSku = New DevExpress.XtraEditors.LabelControl
        Me.lblEanLabel = New DevExpress.XtraEditors.LabelControl
        Me.lblSupplierRef = New DevExpress.XtraEditors.LabelControl
        Me.lblSupplierRefLabel = New DevExpress.XtraEditors.LabelControl
        Me.cmbEan = New System.Windows.Forms.ComboBox
        Me.btnExit = New System.Windows.Forms.Button
        Me.pnlReports = New DevExpress.XtraEditors.XtraScrollableControl
        Me.btnAccept = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'lblSkuLabel
        '
        Me.lblSkuLabel.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold)
        Me.lblSkuLabel.Appearance.Options.UseFont = True
        Me.lblSkuLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblSkuLabel.Location = New System.Drawing.Point(12, 12)
        Me.lblSkuLabel.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
        Me.lblSkuLabel.Name = "lblSkuLabel"
        Me.lblSkuLabel.Size = New System.Drawing.Size(53, 20)
        Me.lblSkuLabel.TabIndex = 0
        Me.lblSkuLabel.Text = "SKU"
        '
        'lblSku
        '
        Me.lblSku.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.lblSku.Appearance.Options.UseFont = True
        Me.lblSku.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblSku.Location = New System.Drawing.Point(71, 12)
        Me.lblSku.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
        Me.lblSku.Name = "lblSku"
        Me.lblSku.Size = New System.Drawing.Size(422, 20)
        Me.lblSku.TabIndex = 1
        Me.lblSku.Text = "sku number and description"
        '
        'lblEanLabel
        '
        Me.lblEanLabel.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold)
        Me.lblEanLabel.Appearance.Options.UseFont = True
        Me.lblEanLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblEanLabel.Location = New System.Drawing.Point(499, 9)
        Me.lblEanLabel.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
        Me.lblEanLabel.Name = "lblEanLabel"
        Me.lblEanLabel.Size = New System.Drawing.Size(52, 20)
        Me.lblEanLabel.TabIndex = 2
        Me.lblEanLabel.Text = "EAN"
        '
        'lblSupplierRef
        '
        Me.lblSupplierRef.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSupplierRef.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.lblSupplierRef.Appearance.Options.UseFont = True
        Me.lblSupplierRef.Appearance.Options.UseTextOptions = True
        Me.lblSupplierRef.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblSupplierRef.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblSupplierRef.Location = New System.Drawing.Point(977, 12)
        Me.lblSupplierRef.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
        Me.lblSupplierRef.Name = "lblSupplierRef"
        Me.lblSupplierRef.Size = New System.Drawing.Size(95, 20)
        Me.lblSupplierRef.TabIndex = 5
        Me.lblSupplierRef.Text = "supplier ref"
        '
        'lblSupplierRefLabel
        '
        Me.lblSupplierRefLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSupplierRefLabel.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold)
        Me.lblSupplierRefLabel.Appearance.Options.UseFont = True
        Me.lblSupplierRefLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblSupplierRefLabel.Location = New System.Drawing.Point(871, 12)
        Me.lblSupplierRefLabel.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
        Me.lblSupplierRefLabel.Name = "lblSupplierRefLabel"
        Me.lblSupplierRefLabel.Size = New System.Drawing.Size(100, 20)
        Me.lblSupplierRefLabel.TabIndex = 4
        Me.lblSupplierRefLabel.Text = "Supplier Ref"
        '
        'cmbEan
        '
        Me.cmbEan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbEan.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbEan.FormattingEnabled = True
        Me.cmbEan.Location = New System.Drawing.Point(558, 9)
        Me.cmbEan.Name = "cmbEan"
        Me.cmbEan.Size = New System.Drawing.Size(218, 26)
        Me.cmbEan.TabIndex = 7
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(996, 661)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 15
        Me.btnExit.TabStop = False
        Me.btnExit.Text = "F10 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'pnlReports
        '
        Me.pnlReports.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlReports.Location = New System.Drawing.Point(12, 44)
        Me.pnlReports.Name = "pnlReports"
        Me.pnlReports.Size = New System.Drawing.Size(1060, 611)
        Me.pnlReports.TabIndex = 0
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnAccept.Location = New System.Drawing.Point(914, 661)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(76, 39)
        Me.btnAccept.TabIndex = 16
        Me.btnAccept.TabStop = False
        Me.btnAccept.Text = "F4 Accept"
        Me.btnAccept.UseVisualStyleBackColor = True
        Me.btnAccept.Visible = False
        '
        'StockEnquiryHost
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1084, 712)
        Me.Controls.Add(Me.btnAccept)
        Me.Controls.Add(Me.pnlReports)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.cmbEan)
        Me.Controls.Add(Me.lblSupplierRef)
        Me.Controls.Add(Me.lblSupplierRefLabel)
        Me.Controls.Add(Me.lblEanLabel)
        Me.Controls.Add(Me.lblSku)
        Me.Controls.Add(Me.lblSkuLabel)
        Me.KeyPreview = True
        Me.Name = "StockEnquiryHost"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Item Enquiry"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblSkuLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblSku As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblEanLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblSupplierRef As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblSupplierRefLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cmbEan As System.Windows.Forms.ComboBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents pnlReports As DevExpress.XtraEditors.XtraScrollableControl
    Friend WithEvents btnAccept As System.Windows.Forms.Button
End Class
