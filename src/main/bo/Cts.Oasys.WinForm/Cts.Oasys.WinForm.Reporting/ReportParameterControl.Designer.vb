﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReportParameterControl
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblName = New System.Windows.Forms.Label
        Me.dtp = New System.Windows.Forms.DateTimePicker
        Me.cmb = New System.Windows.Forms.ComboBox
        Me.txt = New DevExpress.XtraEditors.TextEdit
        Me.chk = New DevExpress.XtraEditors.CheckEdit
        Me.cmbChecked = New DevExpress.XtraEditors.CheckedComboBoxEdit
        CType(Me.txt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmbChecked.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(0, 0)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(165, 22)
        Me.lblName.TabIndex = 0
        Me.lblName.Text = "Label"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtp
        '
        Me.dtp.Location = New System.Drawing.Point(317, 0)
        Me.dtp.Name = "dtp"
        Me.dtp.Size = New System.Drawing.Size(200, 20)
        Me.dtp.TabIndex = 8
        Me.dtp.Visible = False
        '
        'cmb
        '
        Me.cmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb.FormattingEnabled = True
        Me.cmb.Location = New System.Drawing.Point(317, 0)
        Me.cmb.Name = "cmb"
        Me.cmb.Size = New System.Drawing.Size(200, 21)
        Me.cmb.TabIndex = 12
        Me.cmb.Visible = False
        '
        'txt
        '
        Me.txt.Location = New System.Drawing.Point(317, 0)
        Me.txt.Name = "txt"
        Me.txt.Size = New System.Drawing.Size(200, 20)
        Me.txt.TabIndex = 13
        Me.txt.Visible = False
        '
        'chk
        '
        Me.chk.Location = New System.Drawing.Point(315, 0)
        Me.chk.Name = "chk"
        Me.chk.Properties.Caption = ""
        Me.chk.Size = New System.Drawing.Size(21, 19)
        Me.chk.TabIndex = 14
        Me.chk.Visible = False
        '
        'cmbChecked
        '
        Me.cmbChecked.Location = New System.Drawing.Point(317, 0)
        Me.cmbChecked.Name = "cmbChecked"
        Me.cmbChecked.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmbChecked.Size = New System.Drawing.Size(200, 20)
        Me.cmbChecked.TabIndex = 15
        Me.cmbChecked.Visible = False
        '
        'ReportParameterControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.cmbChecked)
        Me.Controls.Add(Me.chk)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.txt)
        Me.Controls.Add(Me.cmb)
        Me.Controls.Add(Me.dtp)
        Me.Name = "ReportParameterControl"
        Me.Size = New System.Drawing.Size(862, 22)
        CType(Me.txt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmbChecked.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmb As System.Windows.Forms.ComboBox
    Friend WithEvents txt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents chk As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents cmbChecked As DevExpress.XtraEditors.CheckedComboBoxEdit

End Class
