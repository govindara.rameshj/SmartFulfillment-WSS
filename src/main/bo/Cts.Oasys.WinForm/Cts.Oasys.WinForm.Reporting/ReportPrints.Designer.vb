<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class ReportPrints
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.xrlDate = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlTitle = New DevExpress.XtraReports.UI.XRLabel
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo
        Me.xrlFooter = New DevExpress.XtraReports.UI.XRLabel
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand
        Me.xrlFooter2 = New DevExpress.XtraReports.UI.XRLabel
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Height = 193
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDate
        '
        Me.xrlDate.Font = New System.Drawing.Font("Times New Roman", 8.0!)
        Me.xrlDate.Location = New System.Drawing.Point(533, 17)
        Me.xrlDate.Name = "xrlDate"
        Me.xrlDate.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDate.Size = New System.Drawing.Size(192, 16)
        Me.xrlDate.StylePriority.UseFont = False
        Me.xrlDate.StylePriority.UseTextAlignment = False
        Me.xrlDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTitle
        '
        Me.xrlTitle.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
        Me.xrlTitle.Location = New System.Drawing.Point(8, 17)
        Me.xrlTitle.Name = "xrlTitle"
        Me.xrlTitle.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTitle.Size = New System.Drawing.Size(267, 16)
        Me.xrlTitle.StylePriority.UseFont = False
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Format = "Page: {0} of {1}"
        Me.XrPageInfo1.Location = New System.Drawing.Point(625, 0)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.Size = New System.Drawing.Size(100, 17)
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlFooter
        '
        Me.xrlFooter.Font = New System.Drawing.Font("Times New Roman", 8.0!)
        Me.xrlFooter.Location = New System.Drawing.Point(0, 0)
        Me.xrlFooter.Name = "xrlFooter"
        Me.xrlFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFooter.Size = New System.Drawing.Size(275, 17)
        Me.xrlFooter.StylePriority.UseFont = False
        Me.xrlFooter.StylePriority.UseTextAlignment = False
        Me.xrlFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMargin
        '
        Me.TopMargin.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlDate, Me.xrlTitle})
        Me.TopMargin.Height = 44
        Me.TopMargin.Name = "TopMargin"
        '
        'BottomMargin
        '
        Me.BottomMargin.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlFooter2, Me.XrPageInfo1, Me.xrlFooter})
        Me.BottomMargin.Height = 42
        Me.BottomMargin.Name = "BottomMargin"
        '
        'xrlFooter2
        '
        Me.xrlFooter2.Font = New System.Drawing.Font("Times New Roman", 8.0!)
        Me.xrlFooter2.Location = New System.Drawing.Point(0, 17)
        Me.xrlFooter2.Name = "xrlFooter2"
        Me.xrlFooter2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFooter2.Size = New System.Drawing.Size(275, 17)
        Me.xrlFooter2.StylePriority.UseFont = False
        Me.xrlFooter2.StylePriority.UseTextAlignment = False
        Me.xrlFooter2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ReportPrints
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin})
        Me.Margins = New System.Drawing.Printing.Margins(100, 2, 44, 42)
        Me.PageHeight = 1169
        Me.PageWidth = 827
        Me.PaperKind = System.Drawing.Printing.PaperKind.A4
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.Version = "9.1"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents xrlDate As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTitle As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFooter As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents xrlFooter2 As DevExpress.XtraReports.UI.XRLabel
End Class
