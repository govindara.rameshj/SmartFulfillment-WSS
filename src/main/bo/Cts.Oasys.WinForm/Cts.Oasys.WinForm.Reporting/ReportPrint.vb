﻿Imports DevExpress.XtraPrintingLinks
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports System.Drawing
Imports DevExpress.XtraGrid.Views.Grid
Imports Cts.Oasys.Core.System

Public Class ReportPrint
    Implements IDisposable
    Private _compLink As CompositeLink
    Private _reportCtl As ReportControl
    Private _storeIdName As String = String.Empty
    Private _header As String = String.Empty
    Private _title As String = String.Empty
    Private _userIdName As String = String.Empty
    Private _workstationId As Integer


    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 21/06/2011
    ' Referral No : 677
    ' Notes       : Modifed to Allow support users to use the support logins provided.
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Public Sub New(ByVal repControl As ReportControl, ByVal userId As Integer, ByVal workstationId As Integer)
        _reportCtl = repControl
        _storeIdName = Store.GetIdName
        _header = _reportCtl.Report.Header
        _title = _reportCtl.Report.Title
        _userIdName = User.GetUserIdName(userId, IncludeExternalIds:=True)
        _workstationId = workstationId
        Initialise()
    End Sub

    Private Sub Initialise()

        'set up supplier copy report
        _compLink = New CompositeLink(New PrintingSystem)
        _compLink.Margins.Bottom = 70
        _compLink.Margins.Top = 70
        _compLink.Margins.Left = 50
        _compLink.Margins.Right = 50
        _compLink.Landscape = _reportCtl.Report.PrintLandscape
        _compLink.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Watermark, CommandVisibility.None)
        _compLink.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.FillBackground, CommandVisibility.None)
        _compLink.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.EditPageHF, CommandVisibility.None)

        AddHandler _compLink.CreateMarginalHeaderArea, AddressOf Report_CreateMarginalHeaderArea
        AddHandler _compLink.CreateMarginalFooterArea, AddressOf Report_CreateMarginalFooterArea

        'set up grid components
        For Each grid As GridControl In _reportCtl.Grids
            Dim reportGrid As New PrintableComponentLink
            reportGrid.Component = grid
            _compLink.Links.Add(reportGrid)

            Dim break As Link = New Link()
            AddHandler break.CreateDetailArea, AddressOf ReportBreak_CreateDetailArea
            _compLink.Links.Add(break)
        Next

    End Sub

    Private Sub Report_CreateMarginalHeaderArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        e.Graph.Modifier = BrickModifier.MarginalHeader
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near, StringAlignment.Far)
        e.Graph.Font = New Font("Tahoma", 9, FontStyle.Bold)
        Dim tb As TextBrick = e.Graph.DrawString(_header, Color.Black, New RectangleF(0, 10, e.Graph.ClientPageSize.Width / 2, 15), DevExpress.XtraPrinting.BorderSide.None)

        e.Graph.Font = New Font("Tahoma", 9, FontStyle.Regular)
        tb = e.Graph.DrawString(_title, Color.Black, New RectangleF(0, 25, e.Graph.ClientPageSize.Width / 2, 15), DevExpress.XtraPrinting.BorderSide.None)

        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Far, StringAlignment.Far)
        tb = e.Graph.DrawString("Date: " & Now.Date.ToShortDateString, Color.Black, New RectangleF(e.Graph.ClientPageSize.Width / 2, 25, e.Graph.ClientPageSize.Width / 2, 15), DevExpress.XtraPrinting.BorderSide.None)

    End Sub

    Private Sub Report_CreateMarginalFooterArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        e.Graph.Modifier = BrickModifier.MarginalFooter

        Dim r As RectangleF = RectangleF.Empty
        r.Height = 30

        Dim sb As New StringBuilder
        sb.Append("Printed: " & _userIdName & Space(2) & "WS: " & _workstationId & Space(2) & "Time: {0:HH:mm}")
        sb.Append(Environment.NewLine)
        sb.Append("Wickes " & _storeIdName)

        Dim pibPrinted As New PageInfoBrick
        pibPrinted = e.Graph.DrawPageInfo(PageInfo.DateTime, sb.ToString, Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pibPrinted.Alignment = BrickAlignment.Near
        pibPrinted.HorzAlignment = DevExpress.Utils.HorzAlignment.Near

        Dim pibPage As New PageInfoBrick
        pibPage = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, "Page: {0} of {1}", Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pibPage.Alignment = BrickAlignment.Far

    End Sub

    Private Sub ReportBreak_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        Dim tb As TextBrick = New TextBrick()
        tb.Rect = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 15)
        tb.BackColor = Color.Transparent
        tb.BorderStyle = BrickBorderStyle.Inset
        tb.BorderColor = Color.White
        e.Graph.DrawBrick(tb)
    End Sub


    Public Sub ShowPreviewDialog()
        _compLink.ShowPreviewDialog()
    End Sub

    Public Sub Print()
        Dim ps As New System.Drawing.Printing.PrinterSettings
        _compLink.Print(ps.PrinterName)
    End Sub

    Public Sub Print(ByVal printerName As String)
        _compLink.Print(printerName)
    End Sub

#Region " IDisposable Support "
    Private disposedValue As Boolean = False

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                _compLink.Dispose()
            End If
        End If
        Me.disposedValue = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
