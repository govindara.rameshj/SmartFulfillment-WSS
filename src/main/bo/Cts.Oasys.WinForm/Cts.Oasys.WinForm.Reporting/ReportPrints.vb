Imports DevExpress.XtraReports.UI
Imports System.Data
Imports DevExpress.Data
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Columns
Imports System.Drawing
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports Cts.Oasys.Core.System

Public Class ReportPrints
    Private _tableWidth As Integer
    Private _printHeight As Integer

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 21/06/2011
    ' Referral No : 677
    ' Notes       : Modifed to Allow support users to use the support logins provided.
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Sub New(ByVal repControls As List(Of ReportControl), ByVal userId As Integer, ByVal workstationId As Integer)
        InitializeComponent()

        xrlTitle.Text = repControls(0).Report.Header
        xrlDate.Text = "Date: " & Now.Date.ToShortDateString
        xrlFooter.Text = "Printed: " & User.GetUserIdName(userId, IncludeExternalIds:=True) & Space(2) & "WS: " & workstationId & Space(2) & "Time: " & Now.ToString("HH:mm")
        xrlFooter2.Text = "Wickes " & Store.GetIdName

        Me.Margins.Top = 50
        Me.Margins.Bottom = 50
        Me.Margins.Left = 50
        Me.Margins.Right = 50

        Dim top As Integer = 0
        Dim left As Integer = 0

        _tableWidth = CInt((Me.PageWidth - Me.Margins.Left - Me.Margins.Right) / 2 - 10)
        _printHeight = Me.PageHeight - Me.Margins.Top - Me.Margins.Bottom

        For Each repControl As ReportControl In repControls
            Dim view As GridView = CType(repControl.Grids(0).MainView, GridView)

            'set column widths
            Dim colWidths As New ArrayList
            Dim sumWidth As Integer = 0
            Dim viewWidth As Integer = 0
            For Each col As GridColumn In view.Columns
                viewWidth += col.Width
            Next

            For colIndex As Integer = view.Columns.Count - 1 To 1 Step -1
                Dim ratio As Decimal = CDec(view.Columns(colIndex).Width / viewWidth)
                Dim colWidth As Integer = CInt(Math.Ceiling(ratio * _tableWidth))
                sumWidth += colWidth
                colWidths.Add(colWidth)
            Next
            colWidths.Add(_tableWidth - sumWidth)
            colWidths.Reverse()

            CreateTitle(view, repControl.Report.Title, left, top)
            CreateHeader(view, colWidths, left, top)
            CreateDetail(view, colWidths, left, top)
        Next

    End Sub


    Private Sub CreateTitle(ByVal view As GridView, ByVal title As String, ByRef left As Integer, ByRef top As Integer)

        'check if going off end of page
        If top + (view.DataRowCount * 20 + 40) > _printHeight Then
            If left = 0 Then
                top = 0
                left = _tableWidth + 15
            Else
                top = Me.PageHeight * Me.Pages.Count
                left = 0
            End If
        End If

        'create table
        Dim table As XRTable = XRTable.CreateTable(New Rectangle(left, top, _tableWidth, 20), 1, 1)
        table.BorderWidth = 1
        table.Borders = DevExpress.XtraPrinting.BorderSide.All
        table.BackColor = Color.LightSteelBlue
        table.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        table.Font = New Font("Tahoma", 8, FontStyle.Bold)
        table.Rows(0).Cells(0).Text = title

        'add to page header band
        top += table.Height - 1
        Me.Bands(BandKind.Detail).Controls.Add(table)

    End Sub

    Private Sub CreateHeader(ByVal view As GridView, ByVal colWidths As ArrayList, ByRef left As Integer, ByRef top As Integer)

        'check if going off end of page
        If top + (view.DataRowCount * 20 + 20) > _printHeight Then
            If left = 0 Then
                top = 0
                left = _tableWidth + 20
            Else
                top = Me.PageHeight * Me.Pages.Count
                left = 0
            End If
        End If

        'create table
        Dim table As XRTable = XRTable.CreateTable(New Rectangle(left, top, _tableWidth, 20), 1, view.Columns.Count)
        table.BorderWidth = 1
        table.Borders = DevExpress.XtraPrinting.BorderSide.All
        table.BackColor = Color.LightSteelBlue
        table.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        table.Font = New Font("Tahoma", 7, FontStyle.Bold)

        'populate headers
        Dim row As XRTableRow = table.Rows(0)
        For colIndex As Integer = 0 To row.Cells.Count - 1
            row.Cells(colIndex).Width = CInt(colWidths(colIndex))
            row.Cells(colIndex).Text = view.Columns(colIndex).Caption
        Next

        'add to page header band
        top += table.Height - 1
        Me.Bands(BandKind.Detail).Controls.Add(table)

    End Sub

    Private Sub CreateDetail(ByVal view As GridView, ByVal colWidths As ArrayList, ByRef left As Integer, ByRef top As Integer)

        'create table
        Dim table As XRTable = XRTable.CreateTable(New Rectangle(left, top, _tableWidth, view.DataRowCount * 20), view.DataRowCount, view.Columns.Count)
        table.BorderWidth = 1
        table.Borders = BorderSide.All
        table.Font = New Font("Tahoma", 7, FontStyle.Regular)
        table.Padding = New PaddingInfo(1, 1, 0, 0)

        'populate details
        For rowIndex As Integer = 0 To table.Rows.Count - 1
            Dim row As XRTableRow = table.Rows(rowIndex)

            For colIndex As Integer = 0 To row.Cells.Count - 1
                If colIndex > 0 Then
                    row.Cells(colIndex).TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
                End If

                Dim value As Object = view.GetRowCellValue(rowIndex, view.Columns(colIndex))
                If value Is Nothing Then value = String.Empty
                If IsDBNull(value) Then value = String.Empty

                row.Cells(colIndex).Width = CInt(colWidths(colIndex))
                row.Cells(colIndex).Text = CStr(value)
            Next
        Next

        'add to detail band
        top += table.Height + 10 - 1
        Me.Bands(BandKind.Detail).Controls.Add(table)

    End Sub


End Class