<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HostForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(HostForm))
        Me.StatusStripPanel = New PanelCustomBorder
        Me.StatusStrip = New System.Windows.Forms.StatusStrip
        Me.VersionLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me.MessageLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me.Progress = New System.Windows.Forms.ToolStripProgressBar
        Me.AppPanel = New PanelCustomBorder
        Me.StatusStripPanel.SuspendLayout()
        Me.StatusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'StatusStripPanel
        '
        resources.ApplyResources(Me.StatusStripPanel, "StatusStripPanel")
        Me.StatusStripPanel.Controls.Add(Me.StatusStrip)
        Me.StatusStripPanel.DrawBottom = False
        Me.StatusStripPanel.DrawLeft = True
        Me.StatusStripPanel.DrawRight = True
        Me.StatusStripPanel.DrawTop = False
        Me.StatusStripPanel.Name = "StatusStripPanel"
        '
        'StatusStrip
        '
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VersionLabel, Me.Progress, Me.MessageLabel})
        resources.ApplyResources(Me.StatusStrip, "StatusStrip")
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.SizingGrip = False
        '
        'VersionLabel
        '
        Me.VersionLabel.Name = "VersionLabel"
        resources.ApplyResources(Me.VersionLabel, "VersionLabel")
        '
        'MessageLabel
        '
        Me.MessageLabel.Name = "MessageLabel"
        resources.ApplyResources(Me.MessageLabel, "MessageLabel")
        '
        'Progress
        '
        Me.Progress.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Progress.Name = "Progress"
        resources.ApplyResources(Me.Progress, "Progress")
        '
        'AppPanel
        '
        resources.ApplyResources(Me.AppPanel, "AppPanel")
        Me.AppPanel.DrawBottom = False
        Me.AppPanel.DrawLeft = True
        Me.AppPanel.DrawRight = True
        Me.AppPanel.DrawTop = True
        Me.AppPanel.Name = "AppPanel"
        '
        'HostForm
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.StatusStripPanel)
        Me.Controls.Add(Me.AppPanel)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.Name = "HostForm"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.StatusStripPanel.ResumeLayout(False)
        Me.StatusStripPanel.PerformLayout()
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents StatusStripPanel As PanelCustomBorder
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents VersionLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents MessageLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Progress As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents AppPanel As PanelCustomBorder

End Class
