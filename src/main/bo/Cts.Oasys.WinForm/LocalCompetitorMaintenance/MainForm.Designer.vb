<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits Cts.Oasys.WinForm.Form

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.uxCompetitorGrid = New DevExpress.XtraGrid.GridControl
        Me.uxCompetitorView = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.uxExitButton = New DevExpress.XtraEditors.SimpleButton
        Me.uxAddButton = New DevExpress.XtraEditors.SimpleButton
        Me.uxDeleteButton = New DevExpress.XtraEditors.SimpleButton
        Me.uxMaintainButton = New DevExpress.XtraEditors.SimpleButton
        Me.chkDeleted = New System.Windows.Forms.CheckBox
        CType(Me.uxCompetitorGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxCompetitorView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'uxCompetitorGrid
        '
        Me.uxCompetitorGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxCompetitorGrid.Cursor = System.Windows.Forms.Cursors.Default
        Me.uxCompetitorGrid.Location = New System.Drawing.Point(3, 2)
        Me.uxCompetitorGrid.MainView = Me.uxCompetitorView
        Me.uxCompetitorGrid.Name = "uxCompetitorGrid"
        Me.uxCompetitorGrid.Size = New System.Drawing.Size(760, 549)
        Me.uxCompetitorGrid.TabIndex = 0
        Me.uxCompetitorGrid.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uxCompetitorView})
        '
        'uxCompetitorView
        '
        Me.uxCompetitorView.Appearance.FocusedRow.BackColor = System.Drawing.Color.Blue
        Me.uxCompetitorView.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.Blue
        Me.uxCompetitorView.Appearance.FocusedRow.BorderColor = System.Drawing.Color.Blue
        Me.uxCompetitorView.Appearance.FocusedRow.Options.UseBackColor = True
        Me.uxCompetitorView.Appearance.FocusedRow.Options.UseBorderColor = True
        Me.uxCompetitorView.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxCompetitorView.Appearance.HeaderPanel.Options.UseFont = True
        Me.uxCompetitorView.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.uxCompetitorView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uxCompetitorView.Appearance.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxCompetitorView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxCompetitorView.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.uxCompetitorView.Appearance.SelectedRow.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.uxCompetitorView.Appearance.SelectedRow.Options.UseBackColor = True
        Me.uxCompetitorView.ColumnPanelRowHeight = 35
        Me.uxCompetitorView.GridControl = Me.uxCompetitorGrid
        Me.uxCompetitorView.Name = "uxCompetitorView"
        Me.uxCompetitorView.OptionsBehavior.Editable = False
        Me.uxCompetitorView.OptionsDetail.ShowDetailTabs = False
        Me.uxCompetitorView.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.uxCompetitorView.OptionsSelection.EnableAppearanceHideSelection = False
        Me.uxCompetitorView.OptionsSelection.InvertSelection = True
        Me.uxCompetitorView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect
        Me.uxCompetitorView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways
        '
        'uxExitButton
        '
        Me.uxExitButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxExitButton.Location = New System.Drawing.Point(675, 558)
        Me.uxExitButton.Name = "uxExitButton"
        Me.uxExitButton.Size = New System.Drawing.Size(88, 39)
        Me.uxExitButton.TabIndex = 5
        Me.uxExitButton.Text = "F12 Exit"
        '
        'uxAddButton
        '
        Me.uxAddButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxAddButton.Appearance.Options.UseTextOptions = True
        Me.uxAddButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxAddButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxAddButton.Location = New System.Drawing.Point(3, 558)
        Me.uxAddButton.Name = "uxAddButton"
        Me.uxAddButton.Size = New System.Drawing.Size(88, 39)
        Me.uxAddButton.TabIndex = 1
        Me.uxAddButton.Text = "F5 Add "
        '
        'uxDeleteButton
        '
        Me.uxDeleteButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxDeleteButton.Appearance.Options.UseTextOptions = True
        Me.uxDeleteButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxDeleteButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxDeleteButton.Location = New System.Drawing.Point(191, 558)
        Me.uxDeleteButton.Name = "uxDeleteButton"
        Me.uxDeleteButton.Size = New System.Drawing.Size(88, 39)
        Me.uxDeleteButton.TabIndex = 3
        Me.uxDeleteButton.TabStop = False
        Me.uxDeleteButton.Text = "F7 Delete "
        '
        'uxMaintainButton
        '
        Me.uxMaintainButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxMaintainButton.Appearance.Options.UseTextOptions = True
        Me.uxMaintainButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxMaintainButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxMaintainButton.Location = New System.Drawing.Point(97, 558)
        Me.uxMaintainButton.Name = "uxMaintainButton"
        Me.uxMaintainButton.Size = New System.Drawing.Size(88, 39)
        Me.uxMaintainButton.TabIndex = 2
        Me.uxMaintainButton.Text = "F6 Maintain "
        '
        'chkDeleted
        '
        Me.chkDeleted.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkDeleted.AutoSize = True
        Me.chkDeleted.Location = New System.Drawing.Point(303, 569)
        Me.chkDeleted.Name = "chkDeleted"
        Me.chkDeleted.Size = New System.Drawing.Size(151, 17)
        Me.chkDeleted.TabIndex = 4
        Me.chkDeleted.Text = "Show Deleted Competitors"
        Me.chkDeleted.UseVisualStyleBackColor = True
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Controls.Add(Me.chkDeleted)
        Me.Controls.Add(Me.uxMaintainButton)
        Me.Controls.Add(Me.uxDeleteButton)
        Me.Controls.Add(Me.uxAddButton)
        Me.Controls.Add(Me.uxExitButton)
        Me.Controls.Add(Me.uxCompetitorGrid)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "MainForm"
        Me.Size = New System.Drawing.Size(766, 600)
        CType(Me.uxCompetitorGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxCompetitorView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents uxCompetitorGrid As DevExpress.XtraGrid.GridControl
    Friend WithEvents uxCompetitorView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents uxExitButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxAddButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxDeleteButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxMaintainButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents chkDeleted As System.Windows.Forms.CheckBox

End Class
