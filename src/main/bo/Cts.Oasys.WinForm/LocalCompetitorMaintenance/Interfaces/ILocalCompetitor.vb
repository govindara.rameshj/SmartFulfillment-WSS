﻿Public Interface ILocalCompetitor
    Function AddCompetitor(ByVal Name As String) As Boolean
    Function MaintainCompetitor(ByVal Id As Integer, ByVal Name As String) As Boolean
    Function DeleteCompetitor(ByVal Id As Integer) As Boolean
    Function GetLocalCompetitors(ByVal ShowDeleted As Boolean) As DataTable
End Interface
