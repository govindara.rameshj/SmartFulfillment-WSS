﻿Public Interface ILocalCompetitorRepository
    Function GetLocalCompetitors(ByVal ShowDeleted As Boolean) As DataTable
    Function InsertLocalCompetitor(ByVal Name As String) As Boolean
    Function UpdateLocalCompetitor(ByVal Id As Integer, ByVal Name As String, ByVal DeleteFlag As Boolean) As Boolean
    Function DeleteLocalCompetitor(ByVal Id As Integer) As Boolean
    Function DoesCompetitorNameExist(ByVal Name As String) As Boolean
End Interface
