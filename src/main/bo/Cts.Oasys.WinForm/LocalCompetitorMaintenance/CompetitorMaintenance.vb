﻿Imports System.Windows.Forms

Public Class CompetitorMaintenance

    Private _CompetitorName As String = ""

    Public Property CompetitorName() As String
        Get
            Return _CompetitorName.Trim
        End Get
        Set(ByVal value As String)
            _CompetitorName = value
        End Set
    End Property

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        _CompetitorName = txtName.Text
        If _CompetitorName.Length > 1 Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Hide()
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Hide()
    End Sub

    Private Sub CompetitorMaintenance_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5 : btnOk.PerformClick()
            Case Keys.F10 : btnCancel.PerformClick()
            Case Else : e.Handled = False
        End Select
    End Sub

    Private Sub CompetitorMaintenance_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        If _CompetitorName.Length > 1 Then
            txtName.Text = _CompetitorName.Trim
            txtName.SelectionStart = 0
            txtName.SelectionLength = txtName.TextLength
            txtName.Focus()
        End If
    End Sub

    Private Sub CompetitorMaintenance_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class