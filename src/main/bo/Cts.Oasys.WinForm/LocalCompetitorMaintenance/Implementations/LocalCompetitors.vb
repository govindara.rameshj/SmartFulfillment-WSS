﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Forms

<Assembly: Runtime.CompilerServices.InternalsVisibleTo("LocalCompetitorMaintenance.WinForm.UnitTest, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
"9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
"3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
"97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
"54e0a4a4")> 
Public Class LocalCompetitors

    Implements ILocalCompetitor

    Friend Overridable Function DoesCompetitorExist(ByVal Name As String) As Boolean
        Dim CompetitorRepository As ILocalCompetitorRepository = LocalCompetitorRepositoryFactory.FactoryGet

        Return CompetitorRepository.DoesCompetitorNameExist(Name)
    End Function

    Friend Overridable Function AddaCompetitor(ByVal Name As String) As Boolean
        Dim add As ILocalCompetitorRepository = LocalCompetitorRepositoryFactory.FactoryGet
        Dim ReturnValue As Boolean = False

        If Not DoesCompetitorExist(Name) Then
            ReturnValue = add.InsertLocalCompetitor(Name)
        End If

        Return ReturnValue
    End Function

    Public Function AddCompetitor(ByVal Name As String) As Boolean Implements ILocalCompetitor.AddCompetitor
        Return AddaCompetitor(Name)
    End Function

    Public Function DeleteCompetitor(ByVal Id As Integer) As Boolean Implements ILocalCompetitor.DeleteCompetitor
        Return DeleteaCompetitor(Id)
    End Function

    Friend Overridable Function DeleteaCompetitor(ByVal Id As Integer) As Boolean
        Dim delete As ILocalCompetitorRepository = LocalCompetitorRepositoryFactory.FactoryGet

        If MessageBox.Show("Are you sure you want to delete this local competitor ?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = DialogResult.Yes Then
            Return delete.DeleteLocalCompetitor(Id)
        Else
            Return False
        End If
    End Function

    Public Function GetLocalCompetitors(ByVal ShowDeleted As Boolean) As System.Data.DataTable Implements ILocalCompetitor.GetLocalCompetitors
        Return GetListOfLocalCompetitors(ShowDeleted)
    End Function

    Friend Overridable Function GetListOfLocalCompetitors(ByVal ShowDeleted As Boolean) As System.Data.DataTable
        Dim loadlist As ILocalCompetitorRepository = LocalCompetitorRepositoryFactory.FactoryGet

        Return loadlist.GetLocalCompetitors(ShowDeleted)
    End Function

    Public Function MaintainCompetitor(ByVal Id As Integer, ByVal Name As String) As Boolean Implements ILocalCompetitor.MaintainCompetitor
        Return MaintainaCompetitor(Id, Name)
    End Function

    Friend Overridable Function MaintainaCompetitor(ByVal Id As Integer, ByVal Name As String) As Boolean
        Dim Maintain As ILocalCompetitorRepository = LocalCompetitorRepositoryFactory.FactoryGet

        Return Maintain.UpdateLocalCompetitor(Id, Name, False)
    End Function

End Class
