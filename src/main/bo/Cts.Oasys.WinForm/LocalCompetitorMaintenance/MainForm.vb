Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.System.User
Imports System.ComponentModel
Imports System.Windows.Forms
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid
Imports System.Drawing

Public Class MainForm

    Private LocalCompetitors As ILocalCompetitor = LocalCompetitorFactory.FactoryGet

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5 : uxAddButton.PerformClick()
            Case Keys.F6 : uxMaintainButton.PerformClick()
            Case Keys.F7 : uxDeleteButton.PerformClick()
            Case Keys.F12 : uxExitButton.PerformClick()
            Case Else : e.Handled = False
        End Select
    End Sub

    Protected Overrides Sub DoProcessing()

        Try
            Cursor = Cursors.WaitCursor
            LoadLocalCompetitors(chkDeleted.Checked)
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub LoadLocalCompetitors(ByVal ShowDeleted As Boolean)

        uxCompetitorGrid.DataSource = LocalCompetitors.GetLocalCompetitors(ShowDeleted)
        uxCompetitorView.Columns(0).Visible = False
        uxCompetitorView.BestFitColumns()
        'uxCompetitorView.SelectedRowsCount = 1
        uxCompetitorGrid.Focus()

    End Sub

    Private Sub uxCompetitorView_RowStyle(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles uxCompetitorView.RowStyle

        If e.RowHandle < 0 Then Exit Sub
        If chkDeleted.Checked Then
            e.Appearance.BackColor = Color.LightSalmon
            e.Appearance.BackColor2 = Color.Red
        Else
            e.Appearance.BackColor = Color.White
            e.Appearance.BackColor2 = Color.White
        End If
    End Sub

    Private Sub uxAddButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxAddButton.Click

        Using uf As New CompetitorMaintenance
            If uf.ShowDialog = DialogResult.OK Then
                If LocalCompetitors.AddCompetitor(uf.CompetitorName) Then
                    LoadLocalCompetitors(chkDeleted.Checked)
                End If
            End If
        End Using

    End Sub

    Private Sub uxMaintainButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxMaintainButton.Click
        Dim myRow As DataRow = uxCompetitorView.GetFocusedDataRow()

        Using uf As New CompetitorMaintenance
            uf.CompetitorName = CStr(myRow.Item(1))
            If uf.ShowDialog = DialogResult.OK Then
                If LocalCompetitors.MaintainCompetitor(CInt(myRow.Item(0)), CStr(uf.CompetitorName)) Then
                    LoadLocalCompetitors(chkDeleted.Checked)
                    uxCompetitorView.SelectRow(CInt(myRow.Item(0)))
                End If
            End If
        End Using

    End Sub

    Private Sub uxDeleteButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxDeleteButton.Click

        Dim myRow As DataRow = uxCompetitorView.GetFocusedDataRow()
        If chkDeleted.Checked = False Then
            If LocalCompetitors.DeleteCompetitor(CInt(myRow.Item(0))) Then
                LoadLocalCompetitors(chkDeleted.Checked)
            End If
        Else
            If LocalCompetitors.MaintainCompetitor(CInt(myRow.Item(0)), CStr(myRow.Item(1))) Then
                LoadLocalCompetitors(chkDeleted.Checked)
            End If
        End If

    End Sub

    Private Sub uxExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxExitButton.Click
        FindForm.Close()
    End Sub

    Private Sub chkDeleted_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDeleted.CheckedChanged
        If chkDeleted.Checked = True Then
            uxDeleteButton.Text = "F7 UnDelete"
            uxCompetitorView.Columns(1).Caption = "Deleted Name"
            uxAddButton.Enabled = False
        Else
            uxDeleteButton.Text = "F7 Delete"
            uxCompetitorView.Columns(1).Caption = "Name"
            uxAddButton.Enabled = True
        End If
        LoadLocalCompetitors(chkDeleted.Checked)
    End Sub
End Class
