﻿Public Class LocalCompetitorRepository

    Implements ILocalCompetitorRepository

    Public Function GetLocalCompetitors(ByVal ShowDeleted As Boolean) As System.Data.DataTable Implements ILocalCompetitorRepository.GetLocalCompetitors
        Dim dt As New DataTable

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .StoredProcedureName = "usp_GetLocalCompetitors"
                    .AddParameter("@Deleted", IIf(ShowDeleted, 1, 0))
                    dt = .ExecuteDataTable()
                End With
            End Using
        End Using

        Return dt

    End Function

    Public Function InsertLocalCompetitor(ByVal Name As String) As Boolean Implements ILocalCompetitorRepository.InsertLocalCompetitor
        Dim linesInserted As Integer = 0

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .StoredProcedureName = "usp_InsertLocalCompetitor"
                    .AddParameter("@Name", Name)
                    linesInserted += com.ExecuteNonQuery()
                End With
            End Using
        End Using

        Return linesInserted > 0

    End Function

    Public Function UpdateLocalCompetitor(ByVal Id As Integer, ByVal Name As String, ByVal DeleteFlag As Boolean) As Boolean Implements ILocalCompetitorRepository.UpdateLocalCompetitor
        Dim linesUpdated As Integer = 0

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .StoredProcedureName = "usp_UpdateLocalCompetitor"
                    .AddParameter("@Id", Id)
                    .AddParameter("@Name", Name)
                    .AddParameter("@DeleteFlag", DeleteFlag)
                    linesUpdated += com.ExecuteNonQuery()
                End With
            End Using
        End Using

        Return linesUpdated > 0

    End Function

    Public Function DeleteLocalCompetitor(ByVal Id As Integer) As Boolean Implements ILocalCompetitorRepository.DeleteLocalCompetitor
        Dim linesUpdated As Integer = 0

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .StoredProcedureName = "usp_DeleteLocalCompetitor"
                    .AddParameter("@Id", Id)
                    .AddParameter("@DeleteFlag", 1)
                    linesUpdated += com.ExecuteNonQuery()
                End With
            End Using
        End Using

        Return linesUpdated > 0

    End Function

    Public Function DoesCompetitorNameExist(ByVal Name As String) As Boolean Implements ILocalCompetitorRepository.DoesCompetitorNameExist

        Dim dt As New DataTable

        Using con As New Connection
            Using com As New Command(con)
                With com
                    .StoredProcedureName = "usp_DoesCompetitorExist"
                    .AddParameter("@Name", Name)
                    dt = .ExecuteDataTable()
                End With
            End Using
        End Using

        Return (dt.Rows.Count > 0)

    End Function
End Class
