<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits Cts.Oasys.WinForm.Form

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.uxUserGrid = New DevExpress.XtraGrid.GridControl()
        Me.uxUserView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.uxExitButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxAddUserButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxDeleteUserButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxMaintainUserButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxPrintButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxSynchronizeUserButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxSelectUserButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxRefreshButton = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.uxUserGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxUserView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'uxUserGrid
        '
        Me.uxUserGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxUserGrid.Location = New System.Drawing.Point(3, 3)
        Me.uxUserGrid.MainView = Me.uxUserView
        Me.uxUserGrid.Name = "uxUserGrid"
        Me.uxUserGrid.Size = New System.Drawing.Size(1094, 549)
        Me.uxUserGrid.TabIndex = 0
        Me.uxUserGrid.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uxUserView})
        '
        'uxUserView
        '
        Me.uxUserView.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxUserView.Appearance.HeaderPanel.Options.UseFont = True
        Me.uxUserView.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.uxUserView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uxUserView.Appearance.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxUserView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxUserView.ColumnPanelRowHeight = 35
        Me.uxUserView.GridControl = Me.uxUserGrid
        Me.uxUserView.Name = "uxUserView"
        Me.uxUserView.OptionsDetail.ShowDetailTabs = False
        '
        'uxExitButton
        '
        Me.uxExitButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxExitButton.Location = New System.Drawing.Point(1022, 558)
        Me.uxExitButton.Name = "uxExitButton"
        Me.uxExitButton.Size = New System.Drawing.Size(75, 39)
        Me.uxExitButton.TabIndex = 2
        Me.uxExitButton.Text = "F12 Exit"
        '
        'uxAddUserButton
        '
        Me.uxAddUserButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxAddUserButton.Appearance.Options.UseTextOptions = True
        Me.uxAddUserButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxAddUserButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxAddUserButton.Location = New System.Drawing.Point(3, 558)
        Me.uxAddUserButton.Name = "uxAddUserButton"
        Me.uxAddUserButton.Size = New System.Drawing.Size(75, 39)
        Me.uxAddUserButton.TabIndex = 3
        Me.uxAddUserButton.Text = "F5 Add User"
        '
        'uxDeleteUserButton
        '
        Me.uxDeleteUserButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxDeleteUserButton.Appearance.Options.UseTextOptions = True
        Me.uxDeleteUserButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxDeleteUserButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxDeleteUserButton.Location = New System.Drawing.Point(165, 558)
        Me.uxDeleteUserButton.Name = "uxDeleteUserButton"
        Me.uxDeleteUserButton.Size = New System.Drawing.Size(75, 39)
        Me.uxDeleteUserButton.TabIndex = 4
        Me.uxDeleteUserButton.Text = "F7 Delete User"
        '
        'uxMaintainUserButton
        '
        Me.uxMaintainUserButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxMaintainUserButton.Appearance.Options.UseTextOptions = True
        Me.uxMaintainUserButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxMaintainUserButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxMaintainUserButton.Location = New System.Drawing.Point(84, 558)
        Me.uxMaintainUserButton.Name = "uxMaintainUserButton"
        Me.uxMaintainUserButton.Size = New System.Drawing.Size(75, 39)
        Me.uxMaintainUserButton.TabIndex = 5
        Me.uxMaintainUserButton.Text = "F6 Maintain User"
        '
        'uxPrintButton
        '
        Me.uxPrintButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxPrintButton.Location = New System.Drawing.Point(941, 558)
        Me.uxPrintButton.Name = "uxPrintButton"
        Me.uxPrintButton.Size = New System.Drawing.Size(75, 39)
        Me.uxPrintButton.TabIndex = 6
        Me.uxPrintButton.Text = "F9 Print"
        '
        'uxSynchronizeUserButton
        '
        Me.uxSynchronizeUserButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxSynchronizeUserButton.Appearance.Options.UseTextOptions = True
        Me.uxSynchronizeUserButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxSynchronizeUserButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxSynchronizeUserButton.Location = New System.Drawing.Point(246, 558)
        Me.uxSynchronizeUserButton.Name = "uxSynchronizeUserButton"
        Me.uxSynchronizeUserButton.Size = New System.Drawing.Size(75, 39)
        Me.uxSynchronizeUserButton.TabIndex = 7
        Me.uxSynchronizeUserButton.Text = "F8 Synchronize"
        '
        'uxSelectUserButton
        '
        Me.uxSelectUserButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxSelectUserButton.Appearance.Options.UseTextOptions = True
        Me.uxSelectUserButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxSelectUserButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxSelectUserButton.Location = New System.Drawing.Point(327, 558)
        Me.uxSelectUserButton.Name = "uxSelectUserButton"
        Me.uxSelectUserButton.Size = New System.Drawing.Size(75, 39)
        Me.uxSelectUserButton.TabIndex = 8
        Me.uxSelectUserButton.Text = "F10 Select All Users"
        '
        'uxRefreshButton
        '
        Me.uxRefreshButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxRefreshButton.Appearance.Options.UseTextOptions = True
        Me.uxRefreshButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxRefreshButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxRefreshButton.Location = New System.Drawing.Point(408, 558)
        Me.uxRefreshButton.Name = "uxRefreshButton"
        Me.uxRefreshButton.Size = New System.Drawing.Size(75, 39)
        Me.uxRefreshButton.TabIndex = 9
        Me.uxRefreshButton.Text = "F11 Refresh"
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Controls.Add(Me.uxRefreshButton)
        Me.Controls.Add(Me.uxSelectUserButton)
        Me.Controls.Add(Me.uxSynchronizeUserButton)
        Me.Controls.Add(Me.uxPrintButton)
        Me.Controls.Add(Me.uxMaintainUserButton)
        Me.Controls.Add(Me.uxDeleteUserButton)
        Me.Controls.Add(Me.uxAddUserButton)
        Me.Controls.Add(Me.uxExitButton)
        Me.Controls.Add(Me.uxUserGrid)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "MainForm"
        Me.Size = New System.Drawing.Size(1100, 600)
        CType(Me.uxUserGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxUserView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents uxUserGrid As DevExpress.XtraGrid.GridControl
    Friend WithEvents uxUserView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents uxExitButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxAddUserButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxDeleteUserButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxMaintainUserButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxPrintButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxSynchronizeUserButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxSelectUserButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxRefreshButton As DevExpress.XtraEditors.SimpleButton

End Class
