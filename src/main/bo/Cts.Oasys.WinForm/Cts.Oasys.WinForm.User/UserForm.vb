﻿Imports Cts.Oasys.Core.System
Imports WSS.BO.DataLayer.Model

Public Class UserForm

    Private _user As Core.System.User.User
    Private _formState As FormState
    Private _PasswordValidation As IPasswordValidation
    Private _ToolTipMessage As ToolTipMessage
    Private ReadOnly _factory As IDataLayerFactory
    Private isSwitchUsersMode As Boolean

#Region "Form"

    Public Sub New(ByRef userItem As Core.System.User.User, ByVal state As FormState, factory As IDataLayerFactory)
        InitializeComponent()
        _user = userItem
        _formState = state
        _factory = factory
        Initialise()

        _PasswordValidation = (New PasswordValidationFactory).GetImplementation
        isSwitchUsersMode = Parameter.GetBoolean(7008)
    End Sub

    Private Sub Initialise()

        'set user and all ux controls (except passwords)
        uxEmployeeCode.EditValue = _user.Code
        uxInitials.EditValue = _user.Initials
        uxName.EditValue = _user.Name
        uxTillReceiptName.EditValue = _user.TillReceiptName
        uxPosition.EditValue = _user.Position
        uxPayrollId.EditValue = _user.PayrollId
        uxOutlet.EditValue = _user.Outlet
        uxLanguageCode.EditValue = _user.LanguageCode
        uxManager.Checked = _user.IsManager
        uxSupervisor.Checked = _user.IsSupervisor
        _ToolTipMessage = New ToolTipMessage

        'load security profiles and set up columns
        uxProfileLookup.Properties.DataSource = Core.System.User.Profile.GetProfiles()
        uxProfileLookup.Properties.ValueMember = GetPropertyName(Function(f As Profile) f.Id)
        uxProfileLookup.Properties.DisplayMember = GetPropertyName(Function(f As Profile) f.Description)

        uxProfileLookup.Properties.Columns.Add(New LookUpColumnInfo(GetPropertyName(Function(f As Profile) f.Id)))
        uxProfileLookup.Properties.Columns.Add(New LookUpColumnInfo(GetPropertyName(Function(f As Profile) f.Description)))
        uxProfileLookup.Properties.Columns.Add(New LookUpColumnInfo(GetPropertyName(Function(f As Profile) f.IsDeleted)))
        uxProfileLookup.Properties.AutoSearchColumnIndex = 0
        uxProfileLookup.Properties.SortColumnIndex = 1
        uxProfileLookup.Properties.PopupWidth = 150

        For Each col As LookUpColumnInfo In uxProfileLookup.Properties.Columns
            Select Case col.FieldName
                Case GetPropertyName(Function(f As Profile) f.Id) : col.Caption = My.Resources.Columns.Id
                Case GetPropertyName(Function(f As Profile) f.Description)
                Case GetPropertyName(Function(f As Profile) f.IsDeleted) : col.Caption = My.Resources.Columns.IsDeleted
                Case Else : col.Visible = False
            End Select
        Next

        uxProfileLookup.Properties.BestFit()
        uxProfileLookup.EditValue = _user.ProfileId
        uxPassExpiry.Properties.MinValue = Now.Date
        uxSuperExpiry.Properties.MinValue = Now.Date

        Select Case _formState
            Case FormState.Add
                Me.Text = My.Resources.Strings.AddUser
                uxPassOld.Enabled = False
                uxSuperOld.Enabled = False
                uxPassExpiry.EditValue = Now.Date
                uxSuperExpiry.EditValue = Now.Date
                uxPassCheck.Enabled = False
                uxPassCheck.Enabled = False

            Case FormState.Maintain
                Me.Text = String.Format(My.Resources.Strings.MaintainUser, _user.ToString)
                uxPassOld.Enabled = False
                uxSuperOld.Enabled = False
                uxPassExpiry.EditValue = Now.Date
                uxSuperExpiry.EditValue = Now.Date

            Case FormState.MaintainOwn
                Me.Text = String.Format(My.Resources.Strings.MaintainUser, _user.ToString)
                uxPassNew.Enabled = False
                uxPassCheck.Checked = False
                uxSuperNew.Enabled = False
                uxSuperCheck.Checked = False
                uxPassExpiry.EditValue = uxPassExpiry.Properties.MaxValue
                uxSuperExpiry.EditValue = uxSuperExpiry.Properties.MaxValue
                uxProfileLookup.Enabled = False

            Case FormState.PasswordChange
                uxPassExpiry.EditValue = uxPassExpiry.Properties.MaxValue
                uxSuperExpiry.EditValue = uxSuperExpiry.Properties.MaxValue
                uxUserGroup.Enabled = False
                uxPassCheck.Checked = True
                uxPassCheck.Enabled = False

        End Select

    End Sub

    Private Sub Me_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5 : uxAcceptButton.PerformClick()
            Case Keys.F10 : uxCancelButton.PerformClick()
            Case Else : e.Handled = False
        End Select
    End Sub

#End Region

#Region "User Details Section"

    Private Sub uxTextEdit_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxInitials.Validated, uxName.Validated, uxTillReceiptName.Validated, uxPosition.Validated, uxPayrollId.Validated, uxOutlet.Validated, uxLanguageCode.Validated
        Dim textedit As DevExpress.XtraEditors.TextEdit = CType(sender, DevExpress.XtraEditors.TextEdit)
        If (textedit.EditValue Is Nothing) OrElse (textedit.EditValue.ToString.Length = 0) Then
            textedit.ErrorText = My.Resources.Strings.InformationRequired
        End If
    End Sub

    Private Sub uxManagerOrSupervisor_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles uxSupervisor.CheckedChanged, uxManager.CheckedChanged
        uxSuperGroup.Visible = (uxSupervisor.Checked OrElse uxManager.Checked)

        If (uxManager.Checked And Not uxSupervisor.Checked) Then
            uxSupervisor.Checked = True
        End If
    End Sub

#End Region

#Region "Profile Dropdown"

    Private Sub uxProfileLookup_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxProfileLookup.EditValueChanged

        'get selected profile
        Dim profile As Profile = CType(uxProfileLookup.Properties.DataSource, ProfileCollection).Find(CInt(uxProfileLookup.EditValue))
        If profile IsNot Nothing Then
            uxPassExpiry.Properties.MaxValue = Now.Date.AddDays(profile.DaysPasswordValid)
            uxSuperExpiry.Properties.MaxValue = Now.Date.AddDays(profile.DaysPasswordValid)
        End If

    End Sub

    Private Sub uxProfileLookup_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles uxProfileLookup.Validating
        uxProfileLookup.ErrorText = String.Empty
    End Sub

#End Region

#Region "User Password Section"

    Private Sub uxPassCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxPassCheck.CheckedChanged
        uxPassPanel.Visible = uxPassCheck.Checked
        uxShowCharacters.Visible = (uxPassPanel.Visible Or uxSuperPanel.Visible)
        If uxPassPanel.Visible Then uxPassOld.Focus()
        CheckAllOk()
    End Sub

    Private Sub uxPassOld_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxPassOld.EditValueChanged
        uxPassOld.ErrorText = String.Empty
        uxPassNew.ResetText()
        uxPassNew.Enabled = False
    End Sub

    Private Sub uxPassOld_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles uxPassOld.Validating

        Dim password As String = CStr(uxPassOld.EditValue)

        'check something entered and is 5 digits long and check if this is user current password
        Select Case True
            Case (password Is Nothing) OrElse (password.Length <> 5)
                uxPassOld.ErrorText = My.Resources.Strings.PasswordNot5digits

            Case password <> _user.Password
                uxPassOld.ErrorText = My.Resources.Strings.PasswordNotOld
        End Select

        _ToolTipMessage.DisplayToolTipMessage(uxPassOld, uxPassOld.ErrorText)
        If uxPassOld.ErrorText <> String.Empty Then
            uxPassNew.Enabled = False
            uxPassNew.ResetText()
            uxPassOld.SelectAll()
        Else
            uxPassNew.Enabled = True
            uxPassNew.Focus()
        End If

    End Sub

    Private Sub uxPassNew_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxPassNew.EditValueChanged
        uxPassNew.ErrorText = String.Empty
        uxPassConfirm.ResetText()
        uxPassConfirm.Enabled = False
    End Sub

    Private Sub uxPassNew_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles uxPassNew.Validating

        Dim password As String = CStr(uxPassNew.EditValue)

        'check something entered and is 5 digits long and not same as existing password
        Select Case True
            Case (password Is Nothing) OrElse (password.Length <> 5)
                uxPassNew.ErrorText = My.Resources.Strings.PasswordNot5digits

            Case (_user.Password IsNot Nothing) AndAlso (_user.Password.Length > 0)
                If password = _user.Password Then
                    uxPassNew.ErrorText = My.Resources.Strings.PasswordSameAsOld
                End If
        End Select

        _ToolTipMessage.DisplayToolTipMessage(uxPassNew, uxPassNew.ErrorText)
        'if error then display
        If uxPassNew.ErrorText <> String.Empty Then
            uxPassConfirm.Enabled = False
            uxPassConfirm.ResetText()
            uxPassNew.SelectAll()
        Else
            uxPassConfirm.Enabled = True
            uxPassConfirm.Focus()
        End If

    End Sub

    Private Sub uxPassConfirm_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxPassConfirm.EditValueChanged
        uxPassConfirm.ErrorText = String.Empty
        uxPassExpiry.Enabled = False
        uxPassGroup.ResetBackColor()
    End Sub

    Private Sub uxPassConfirm_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles uxPassConfirm.Validating

        Dim password As String = CStr(uxPassConfirm.EditValue)

        'check something entered and is 5 digits long and also that passwords match
        Select Case True
            Case (password Is Nothing) OrElse (password.Length <> 5)
                uxPassConfirm.ErrorText = My.Resources.Strings.PasswordNot5digits

            Case password <> CStr(uxPassNew.EditValue)
                uxPassConfirm.ErrorText = My.Resources.Strings.PasswordNotConfirmed
        End Select

        _ToolTipMessage.DisplayToolTipMessage(uxPassConfirm, uxPassConfirm.ErrorText)
        'if error then display
        If uxPassConfirm.ErrorText <> String.Empty Then
            uxPassExpiry.Enabled = False
            uxPassConfirm.SelectAll()
        Else
            uxPassExpiry.Enabled = True
            uxPassExpiry.Focus()
        End If

        CheckAllOk()

    End Sub

#End Region

#Region "Supervisor Password Section"

    Private Sub uxSuperCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxSuperCheck.CheckedChanged
        uxSuperPanel.Visible = uxSuperCheck.Checked
        uxShowCharacters.Visible = (uxPassPanel.Visible Or uxSuperPanel.Visible)
        If uxSuperPanel.Visible Then uxSuperOld.Focus()
        CheckAllOk()
    End Sub

    Private Sub uxSuperOld_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxSuperOld.EditValueChanged
        uxSuperOld.ErrorText = String.Empty
        uxSuperNew.ResetText()
        uxSuperNew.Enabled = False
    End Sub

    Private Sub uxSuperOld_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles uxSuperOld.Validating

        Dim password As String = CStr(uxSuperOld.EditValue)

        'check something entered and is 5 digits long and check if this is user current Superword
        Select Case True
            Case (password Is Nothing) OrElse (password.Length <> 5)
                uxSuperOld.ErrorText = My.Resources.Strings.PasswordNot5digits

            Case password <> _user.SuperPassword
                uxSuperOld.ErrorText = My.Resources.Strings.PasswordNotOld
        End Select

        _ToolTipMessage.DisplayToolTipMessage(uxSuperOld, uxSuperOld.ErrorText)

        If uxSuperOld.ErrorText <> String.Empty Then
            uxSuperNew.Enabled = False
            uxSuperNew.ResetText()
            uxSuperOld.SelectAll()
        Else
            uxSuperNew.Enabled = True
            uxSuperNew.Focus()
        End If

    End Sub

    Private Sub uxSuperNew_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxSuperNew.EditValueChanged
        uxSuperNew.ErrorText = String.Empty
        uxSuperConfirm.ResetText()
        uxSuperConfirm.Enabled = False
    End Sub

    Private Sub uxSuperNew_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles uxSuperNew.Validating

        Dim password As String = CStr(uxSuperNew.EditValue)

        'check something entered and is 5 digits long and not same as existing Superword
        Select Case True
            Case (password Is Nothing) OrElse (password.Length <> 5)
                uxSuperNew.ErrorText = My.Resources.Strings.PasswordNot5digits


                'uxSuperNew.ErrorText = "12"

            Case Not _PasswordValidation.AcceptSupervisorPassword(uxPassNew.EditValue.ToString, password)
                uxSuperNew.ErrorText = My.Resources.Strings.IdenticalPasswords

            Case (_user.SuperPassword IsNot Nothing) AndAlso (_user.SuperPassword.Length > 0)
                If password = _user.SuperPassword Then
                    uxSuperNew.ErrorText = My.Resources.Strings.PasswordSameAsOld
                End If

        End Select

        _ToolTipMessage.DisplayToolTipMessage(uxSuperNew, uxSuperNew.ErrorText)

        'if error then display
        If uxSuperNew.ErrorText <> String.Empty Then
            uxSuperConfirm.Enabled = False
            uxSuperConfirm.ResetText()
            uxSuperNew.SelectAll()
        Else
            uxSuperConfirm.Enabled = True
            uxSuperConfirm.Focus()
        End If

    End Sub

    Private Sub uxSuperConfirm_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxSuperConfirm.EditValueChanged
        uxSuperConfirm.ErrorText = String.Empty
        uxSuperExpiry.Enabled = False
        uxSuperGroup.ResetBackColor()
    End Sub

    Private Sub uxSuperConfirm_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles uxSuperConfirm.Validating

        Dim password As String = CStr(uxSuperConfirm.EditValue)

        'check something entered and is 5 digits long and also that Superwords match
        Select Case True
            Case (password Is Nothing) OrElse (password.Length <> 5)
                uxSuperConfirm.ErrorText = My.Resources.Strings.PasswordNot5digits

            Case password <> CStr(uxSuperNew.EditValue)
                uxSuperConfirm.ErrorText = My.Resources.Strings.PasswordNotConfirmed
        End Select

        _ToolTipMessage.DisplayToolTipMessage(uxSuperConfirm, uxSuperConfirm.ErrorText)
        'if error then display
        If uxSuperConfirm.ErrorText <> String.Empty Then
            uxSuperExpiry.Enabled = False
            uxSuperConfirm.SelectAll()
        Else
            uxSuperExpiry.Enabled = True
            uxSuperExpiry.Focus()
        End If

        CheckAllOk()

    End Sub

#End Region

#Region "Show Password"

    Private Sub uxShowCharacters_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxShowCharacters.CheckedChanged

        Dim passChar As Char = "*"c
        If uxShowCharacters.Checked Then passChar = Nothing

        uxPassOld.Properties.PasswordChar = passChar
        uxPassNew.Properties.PasswordChar = passChar
        uxPassConfirm.Properties.PasswordChar = passChar
        uxSuperOld.Properties.PasswordChar = passChar
        uxSuperNew.Properties.PasswordChar = passChar
        uxSuperConfirm.Properties.PasswordChar = passChar

    End Sub

#End Region

#Region "Buttons"

    Private Sub uxAcceptButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxAcceptButton.Click

        'check something entered for all fields
        Dim allfieldsOk As Boolean = True
        CheckFieldEntered(uxLanguageCode, allfieldsOk)
        CheckFieldEntered(uxOutlet, allfieldsOk)
        CheckFieldEntered(uxPayrollId, allfieldsOk)
        CheckFieldEntered(uxPosition, allfieldsOk)
        CheckFieldEntered(uxTillReceiptName, allfieldsOk)
        CheckFieldEntered(uxName, allfieldsOk)
        CheckFieldEntered(uxInitials, allfieldsOk)

        If Not allfieldsOk Then
            Exit Sub
        End If

        If (uxProfileLookup.EditValue Is Nothing) OrElse CInt(uxProfileLookup.EditValue) = 0 Then
            uxProfileLookup.ErrorIconAlignment = ErrorIconAlignment.TopRight
            uxProfileLookup.ErrorText = My.Resources.Strings.InformationRequired
            _ToolTipMessage.DisplayToolTipMessage(CType(uxProfileLookup, DevExpress.XtraEditors.TextEdit), uxProfileLookup.ErrorText)
        End If

        _user.Initials = CStr(uxInitials.EditValue)
        _user.Name = CStr(uxName.EditValue)
        _user.TillReceiptName = CStr(uxTillReceiptName.EditValue)
        _user.Position = CStr(uxPosition.EditValue)
        _user.PayrollId = CStr(uxPayrollId.EditValue)
        _user.Outlet = CStr(uxOutlet.EditValue)
        _user.LanguageCode = CStr(uxLanguageCode.EditValue)
        _user.ProfileId = CInt(uxProfileLookup.EditValue)
        _user.IsManager = CBool(uxManager.Checked)
        _user.IsSupervisor = CBool(uxSupervisor.Checked)

        If uxPassCheck.Checked Then
            _user.Password = CStr(uxPassConfirm.EditValue)
            _user.PasswordExpires = CDate(uxPassExpiry.EditValue)
        End If

        If uxSuperCheck.Checked Then
            _user.SuperPassword = CStr(uxSuperConfirm.EditValue)
            _user.SuperPasswordExpires = CType(uxSuperExpiry.EditValue, Date?)
        End If

        Select Case _formState
            Case FormState.Add
                _user.Insert(_factory, isSwitchUsersMode)
            Case Else
                _user.Update(_factory, isSwitchUsersMode)
        End Select

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()

    End Sub

    Private Sub uxCancelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxCancelButton.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub CheckFieldEntered(ByVal textEdit As DevExpress.XtraEditors.TextEdit, ByRef ok As Boolean)
        If (textEdit.EditValue Is Nothing) OrElse (textEdit.EditValue.ToString.Trim.Length = 0) Then
            textEdit.ErrorText = My.Resources.Strings.InformationRequired
            _ToolTipMessage.DisplayToolTipMessage(textEdit, textEdit.ErrorText)
            textEdit.Focus()
            ok = False
        End If
    End Sub

    Private Sub CheckAllOk()

        'check that user password all ok
        If (uxPassGroup.Visible) AndAlso (uxPassCheck.Checked) AndAlso (Not uxPassExpiry.Enabled) Then
            uxAcceptButton.Enabled = False
            Exit Sub
        End If

        'check that super password all ok
        If (uxSuperGroup.Visible) AndAlso (uxSuperCheck.Checked) AndAlso (Not uxSuperExpiry.Enabled) Then
            uxAcceptButton.Enabled = False
            Exit Sub
        End If

        uxAcceptButton.Enabled = True

    End Sub

#End Region

End Class

Public Class ToolTipMessage
    Public Sub DisplayToolTipMessage(ByRef sender As DevExpress.XtraEditors.TextEdit, ByVal errorMessage As String)
        sender.ToolTip = errorMessage
    End Sub
End Class


Public Enum FormState
    Add
    Maintain
    MaintainOwn
    PasswordChange
End Enum