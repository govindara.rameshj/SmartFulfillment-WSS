﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations

Public Class UserViewModel

#Region "Private Variables"
    Private _user As Core.System.User.User
    Private _isSynchronize As Boolean
#End Region

#Region "Properties"
    Public ReadOnly Property Id() As Integer
        Get
            Return _user.Id
        End Get
    End Property
    Public ReadOnly Property Code() As String
        Get
            Return _user.Code
        End Get
    End Property
    Public ReadOnly Property ProfileId() As Integer
        Get
            Return _user.ProfileId
        End Get
    End Property
    Public ReadOnly Property Name() As String
        Get
            Return _user.Name
        End Get
    End Property
    Public ReadOnly Property Initials() As String
        Get
            Return _user.Initials
        End Get
    End Property
    Public ReadOnly Property Position() As String
        Get
            Return _user.Position
        End Get
    End Property
    Public ReadOnly Property PayrollId() As String
        Get
            Return _user.PayrollId
        End Get
    End Property
    Public ReadOnly Property PasswordExpires() As Date
        Get
            Return _user.PasswordExpires
        End Get
    End Property
    Public ReadOnly Property SuperPasswordExpires() As Nullable(Of Date)
        Get
            Return _user.SuperPasswordExpires
        End Get
    End Property
    Public ReadOnly Property Outlet() As String
        Get
            Return _user.Outlet
        End Get
    End Property
    Public ReadOnly Property IsManager() As Boolean
        Get
            Return _user.IsManager
        End Get
    End Property
    Public ReadOnly Property IsSupervisor() As Boolean
        Get
            Return _user.IsSupervisor
        End Get
    End Property
    Public ReadOnly Property IsDeleted() As Boolean
        Get
            Return _user.IsDeleted
        End Get
    End Property
    Public ReadOnly Property DeletedDate() As Nullable(Of Date)
        Get
            Return _user.DeletedDate
        End Get
    End Property
    Public ReadOnly Property DeletedBy() As String
        Get
            Return _user.DeletedBy
        End Get
    End Property
    Public ReadOnly Property DeletedWhere() As String
        Get
            Return _user.DeletedWhere
        End Get
    End Property
    Public ReadOnly Property TillReceiptName() As String
        Get
            Return _user.TillReceiptName
        End Get
    End Property
    Public ReadOnly Property LanguageCode() As String
        Get
            Return _user.LanguageCode
        End Get
    End Property
    Public ReadOnly Property Profile() As Profile
        Get
            Return _user.Profile
        End Get
    End Property
    Public ReadOnly Property IdName() As String
        Get
            Return _user.IdName
        End Get
    End Property
    Public ReadOnly Property SynchronizedWhen() As Nullable(Of Date)
        Get
            Return _user.SynchronizedWhen
        End Get
    End Property
    Public ReadOnly Property SynchronizationFailedWhen() As Nullable(Of Date)
        Get
            Return _user.SynchronizationFailedWhen
        End Get
    End Property

    Public Property IsSynchronize() As Boolean
        Get
            Return _isSynchronize
        End Get
        Set(ByVal value As Boolean)
            _isSynchronize = value
        End Set
    End Property
#End Region

    Public Sub New(user As Core.System.User.User)
        _user = user
    End Sub

    Public Function GetUser() As Core.System.User.User
        Return _user
    End Function
End Class
