﻿Public Class UniqueSupervisorPasswordEnforce
    Implements IPasswordValidation

    Public Function AcceptSupervisorPassword(ByVal StandardPassword As String, ByVal SupervisorPassword As String) As Boolean Implements IPasswordValidation.AcceptSupervisorPassword

        If System.String.Compare(StandardPassword.ToLower, SupervisorPassword.ToLower) = 0 Then

            Return False

        Else

            Return True

        End If

    End Function

End Class