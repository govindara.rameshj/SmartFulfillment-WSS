﻿Public Class ValidateDeletedUsers
    Implements IValidateDeletedUsers

    Public Function ValidateDeletedUsers(ByVal u As Core.System.User.User) As Boolean Implements IValidateDeletedUsers.ValidateDeletedUsers
        If u.IsDeleted = True Then
            Return True
        End If
        Return False
    End Function
End Class
