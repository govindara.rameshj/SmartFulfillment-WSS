Imports WSS.BO.DataLayer.Model
Imports WSS.BO.Common.Hosting

Public Class UserAuthorise
    Private _user As Core.System.User.User = Nothing
    Private _level As LoginLevel = LoginLevel.User
    Private checkDeletedUser As IValidateDeletedUsers = GetValidateDeletedUsersInstance()

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal level As LoginLevel)
        InitializeComponent()
        _level = level
        Select Case level
            Case LoginLevel.User : Me.Text = My.Resources.Strings.AuthorUser
            Case LoginLevel.Supervisor : Me.Text = My.Resources.Strings.AuthorSuper
            Case LoginLevel.Manager : Me.Text = My.Resources.Strings.AuthorManager
        End Select
    End Sub

    Public Sub New(ByVal Level As LoginLevel, ByVal Header As String)
        InitializeComponent()
        _level = Level
        Select Case Level
            Case LoginLevel.User : Me.Text = My.Resources.Strings.AuthorUser & " - " & Header
            Case LoginLevel.Supervisor : Me.Text = My.Resources.Strings.AuthorSuper & " - " & Header
            Case LoginLevel.Manager : Me.Text = My.Resources.Strings.AuthorManager & " - " & Header
        End Select
    End Sub

    Public ReadOnly Property User() As Core.System.User.User
        Get
            Return _user
        End Get
    End Property

    Public ReadOnly Property UserId() As Integer
        Get
            If _user IsNot Nothing Then Return _user.Id
            Return Nothing
        End Get
    End Property

    Private Sub Me_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyData
            Case Keys.F10 : uxCancelButton.PerformClick()
        End Select
    End Sub


    Private Sub uxUserIdText_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxUserIdText.TextChanged
        uxUserNameLabel.Text = String.Empty
        uxPasswordText.EditValue = Nothing
    End Sub

    Friend Overridable Sub SetErrorMessage()
        uxUserNameLabel.Text = My.Resources.Strings.DeletedUser
        uxUserIdText.Focus()
        uxUserIdText.SelectAll()
    End Sub
    Private Sub uxUserIdText_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles uxUserIdText.Validating

        If uxUserIdText.EditValue Is Nothing Then
            Exit Sub
        End If

        'check user exists
        Dim userId As Integer = CInt(uxUserIdText.EditValue)
        Using u As Core.System.User.User = Core.System.User.GetUser(userId, IncludeExternalIds:=True)
            If u IsNot Nothing Then
                If checkDeletedUser.ValidateDeletedUsers(u) Then
                    SetErrorMessage()
                    Return
                End If

                uxUserNameLabel.Text = u.ToString
                uxPasswordText.Focus()
            Else
                uxUserNameLabel.Text = My.Resources.Strings.UserNotRecognised
                uxUserIdText.Focus()
                uxUserIdText.SelectAll()
            End If
        End Using

    End Sub

    Private Sub uxPasswordText_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxPasswordText.TextChanged
        uxPasswordLabel.Text = String.Empty
        uxChangePasswordButton.Visible = False
    End Sub

    Private Function GetValidateDeletedUsersInstance() As IValidateDeletedUsers
        Return New ValidateDeletedUsersFactory().GetImplementation()
    End Function

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 21/06/2011
    ' Referral No : 677
    ' Notes       : Modifed to Allow support users to use the support logins provided.
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Private Sub uxPasswordText_Validating() Handles uxPasswordText.Validating, uxChangePasswordButton.Click

        'check something entered
        If uxPasswordText.EditValue Is Nothing Then
            Exit Sub
        End If

        'check user exists
        Dim userId As Integer = CInt(uxUserIdText.EditValue)
        Using thisUser As Core.System.User.User = Core.System.User.GetUser(userId, IncludeExternalIds:=True)
            If thisUser IsNot Nothing Then
                'check password and exit out if true
                If thisUser.Password = uxPasswordText.EditValue.ToString Then
                    PasswordExpiryCheck(thisUser)
                    uxChangePasswordButton.Visible = True
                Else
                    uxPasswordLabel.Text = My.Resources.Strings.PasswordNotValid
                    uxPasswordText.Focus()
                    uxPasswordText.SelectAll()
                End If
            Else
                uxUserNameLabel.Text = My.Resources.Strings.UserNotRecognised
                uxUserIdText.Focus()
                uxUserIdText.SelectAll()
            End If
        End Using

    End Sub

    Private Sub PasswordExpiryCheck(ByVal thisUser As Core.System.User.User)

        Dim userOk As Boolean = True
        Dim resolver = New SimpleResolver()
        Dim dlFactory = resolver.Resolve(Of IDataLayerFactory)()

        If Now.Date >= thisUser.PasswordExpires.Date Then
            Using uf As New UserForm(thisUser, FormState.PasswordChange, dlFactory)
                If uf.ShowDialog <> Windows.Forms.DialogResult.OK Then userOk = False
            End Using
        End If

        If userOk Then
            _user = thisUser
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        End If

    End Sub


    Private Sub uxCancelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxCancelButton.Click
        Close()
    End Sub

End Class

Public Enum LoginLevel
    User
    Supervisor
    Manager
End Enum