﻿Public Class ServiceDeskAuthorise

    Private _workstation As Integer = 0
    Private _WKStnDescription As String = String.Empty
    Private _SD_Password As String = String.Empty
    Private _SD_Number As String = String.Empty
    Private _isPasswordValid As Boolean = False

    Property WorkstationID() As Integer
        Get
            WorkstationID = _workstation
        End Get
        Set(ByVal value As Integer)
            _workstation = value
        End Set
    End Property

    Property WKStnDescription() As String
        Get
            WKStnDescription = _WKStnDescription
        End Get
        Set(ByVal value As String)
            _WKStnDescription = value
        End Set
    End Property

    ReadOnly Property IsAuthorized() As Boolean
        Get
            IsAuthorized = _isPasswordValid
        End Get
    End Property

    Private Sub txtPassword_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPassword.KeyDown

        If e.KeyValue >= 48 And e.KeyValue <= 57 Or e.KeyValue >= 96 And e.KeyValue <= 105 Or e.KeyValue = 8 Or e.KeyValue = 37 Or e.KeyValue = 39 Or e.KeyValue = 46 Then 'process keys 0-9
        Else
            e.SuppressKeyPress = True
        End If

        If e.KeyValue = Keys.Enter Then
            btnAccept.PerformClick()
        End If

        If e.KeyValue = Keys.F5 Then
            btnAccept.PerformClick()
        End If

    End Sub 'txtFromStore_KeyDown

    Private Sub PassWord_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblWorkStation.Text = _WKStnDescription
        lblTill.Visible = Not String.IsNullOrEmpty(lblWorkStation.Text)
        If GetServiceDeskPassword() = True Then
            lblGenKey.Text = _SD_Number
        End If
    End Sub

    Public Function GetServiceDeskPassword() As Boolean

        Dim wkRandom As Long
        Dim wkDouble As Double

        Randomize()

        wkRandom = CLng(99999 * Rnd() + 1)
        _SD_Number = Format(wkRandom, "0000#")

        wkDouble = 98765432
        wkDouble = wkDouble - wkRandom
        wkDouble = Fix(wkDouble * 119)
        wkDouble = Fix(wkDouble / 19)
        wkDouble = Fix(wkDouble * 256)
        wkDouble = Fix(wkDouble / 27)
        wkDouble = Fix(wkDouble * 5656)
        wkDouble = Fix(wkDouble / 321)
        wkDouble += wkRandom

        _SD_Password = Format(wkDouble)
        _SD_Password = _SD_Password.Substring(_SD_Password.Length - 5)
        _SD_Password = Format(CLng(_SD_Password), "0000#")

        Return True

    End Function

    Private Function CheckPassWord() As Boolean

        If txtPassword.Text = _SD_Password Then
            Return True
        Else
            Return False
        End If

    End Function 'CheckPassWord

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = False
        Select Case e.KeyData
            Case Keys.F10
                btnExit.PerformClick()
                e.Handled = True
        End Select

    End Sub 'Form_KeyDown

    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click
        _isPasswordValid = CheckPassWord()
        If _isPasswordValid Then
            Me.Close()
        End If
    End Sub
End Class