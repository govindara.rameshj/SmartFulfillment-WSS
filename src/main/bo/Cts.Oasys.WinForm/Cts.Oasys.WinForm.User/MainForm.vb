Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.System.User
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid
Imports WSS.BO.Common.Hosting
Imports WSS.BO.DataLayer.Model
Imports System.Linq
Imports Cts.Oasys.Core.System

Public Class MainForm

    Private ReadOnly dlFactory As IDataLayerFactory
    Private viewModel As IList(Of UserViewModel)
    Private isSelectAllUsersMode As Boolean
    Private isSwitchUsersMode As Boolean

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()

        Dim resolver = New SimpleResolver()
        dlFactory = resolver.Resolve(Of IDataLayerFactory)()
        isSelectAllUsersMode = False
        isSwitchUsersMode = Parameter.GetBoolean(7008)
        uxSynchronizeUserButton.Enabled = isSwitchUsersMode
        uxSelectUserButton.Enabled = isSwitchUsersMode
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5 : uxAddUserButton.PerformClick()
            Case Keys.F6 : uxMaintainUserButton.PerformClick()
            Case Keys.F7 : uxDeleteUserButton.PerformClick()
            Case Keys.F8 : uxSynchronizeUserButton.PerformClick()
            Case Keys.F10 : uxSelectUserButton.PerformClick()
            Case Keys.F11 : uxRefreshButton.PerformClick()
            Case Keys.F12 : uxExitButton.PerformClick()
            Case Else : e.Handled = False
        End Select
    End Sub

    Protected Overrides Sub DoProcessing()
        Try
            Cursor = Cursors.WaitCursor
            LoadUsers()
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub LoadUsers()
        Try
            Dim users As UserCollection = GetUsers()
            viewModel = users.Select(Function(x) New UserViewModel(x)).ToList()
            uxUserGrid.BeginUpdate()
            uxUserGrid.DataSource = viewModel

            For Each col As GridColumn In uxUserView.Columns
                col.OptionsColumn.AllowEdit = False
                Select Case col.FieldName
                    Case GetPropertyName(Function(f As UserViewModel) f.Id)
                        col.Caption = My.Resources.Columns.Id

                    Case GetPropertyName(Function(f As UserViewModel) f.Code)
                        col.Caption = My.Resources.Columns.EmployeeCode

                    Case GetPropertyName(Function(f As UserViewModel) f.Initials)
                    Case GetPropertyName(Function(f As UserViewModel) f.Name)
                    Case GetPropertyName(Function(f As UserViewModel) f.Position)
                    Case GetPropertyName(Function(f As UserViewModel) f.PayrollId)

                    Case GetPropertyName(Function(f As UserViewModel) f.TillReceiptName)
                        col.Caption = My.Resources.Columns.TillReceiptName

                    Case GetPropertyName(Function(f As UserViewModel) f.LanguageCode)
                    Case GetPropertyName(Function(f As UserViewModel) f.ProfileId)
                        col.Caption = My.Resources.Columns.ProfileId

                    Case GetPropertyName(Function(f As UserViewModel) f.IsManager)
                        col.Caption = My.Resources.Columns.IsManager

                    Case GetPropertyName(Function(f As UserViewModel) f.IsSupervisor)
                        col.Caption = My.Resources.Columns.IsSupervisor

                    Case GetPropertyName(Function(f As UserViewModel) f.IsDeleted)
                        col.Caption = My.Resources.Columns.IsDeleted

                    Case GetPropertyName(Function(f As UserViewModel) f.SynchronizedWhen)
                        col.Caption = My.Resources.Columns.SynchronizedWhen
                        col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                        col.DisplayFormat.FormatString = "G"

                    Case GetPropertyName(Function(f As UserViewModel) f.SynchronizationFailedWhen)
                        col.Caption = My.Resources.Columns.SynchronizationFailedWhen
                        col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                        col.DisplayFormat.FormatString = "G"

                    Case GetPropertyName(Function(f As UserViewModel) f.IsSynchronize)
                        col.Caption = My.Resources.Columns.IsSynchronize
                        col.OptionsColumn.AllowEdit = isSwitchUsersMode

                    Case Else
                        col.Visible = False
                End Select
            Next

            uxUserView.BestFitColumns()
        Finally
            uxUserGrid.EndUpdate()
        End Try
    End Sub

    Private Sub uxUserView_RowStyle(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles uxUserView.RowStyle

        'check if any rows loaded first
        If e.RowHandle < 0 Then Exit Sub

        Dim view As GridView = CType(sender, GridView)
        Dim isDeleted As Boolean = CBool(view.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As Core.System.User.User) f.IsDeleted)))
        If isDeleted Then
            e.Appearance.BackColor = Color.LightSalmon
            e.Appearance.BackColor2 = Color.Red
        End If

    End Sub


    Private Sub uxAddUserButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxAddUserButton.Click

        Dim userItem As New Core.System.User.User
        Using uf As New UserForm(userItem, FormState.Add, dlFactory)
            If uf.ShowDialog = DialogResult.OK Then
                viewModel.Add(New UserViewModel(userItem))
            End If
        End Using

    End Sub

    Private Sub uxMaintainUserButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxMaintainUserButton.Click
        'get user item for focused row
        Dim userItem = viewModel(uxUserView.GetDataSourceRowIndex(uxUserView.FocusedRowHandle)).GetUser()

        'load user form
        Using uf As New UserForm(userItem, FormState.Maintain, dlFactory)
            If uf.ShowDialog = DialogResult.OK Then
                uxUserView.RefreshRow(uxUserView.FocusedRowHandle)
            End If
        End Using

    End Sub

    Private Sub uxDeleteUserButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxDeleteUserButton.Click

        If MessageBox.Show(My.Resources.Strings.YesNoDeleteUser, AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Dim userItem = viewModel(uxUserView.GetDataSourceRowIndex(uxUserView.FocusedRowHandle)).GetUser()

            userItem.Delete(UserId, WorkstationId, dlFactory, isSwitchUsersMode)
            uxUserView.RefreshRow(uxUserView.FocusedRowHandle)
        End If

    End Sub

    Private Sub uxPrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxPrintButton.Click
        Dim col = uxUserView.Columns(GetPropertyName(Function(f As UserViewModel) f.IsSynchronize))
        Try
            col.Visible = False
            Using print As New Printing.Print(uxUserGrid, UserId, WorkstationId)
                print.PrintHeader = My.Resources.Strings.PrintTitle
                print.PrintHeaderSub = String.Empty
                print.PrintLandscape = True
                print.ShowPreviewDialog()
            End Using
        Finally
            col.Visible = True
        End Try
    End Sub

    Private Sub uxExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxExitButton.Click
        FindForm.Close()
    End Sub

    Private Sub uxSynchronizeUserButton_Click(sender As System.Object, e As System.EventArgs) Handles uxSynchronizeUserButton.Click
        Dim user = viewModel.Where(Function(x) x.IsSynchronize)
        If user.Any() Then
            If MessageBox.Show(My.Resources.Strings.YesNoSynchronizeUser, AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                For Each item As UserViewModel In user
                    item.GetUser().SynchronizeToExternalSystems(dlFactory)
                Next
                UpdateIsSynchronizeForAllUsers(False)
            End If
        Else
            MessageBox.Show(My.Resources.Strings.NothingSynchronizeUser, AppName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub uxSelectUserButton_Click(sender As System.Object, e As System.EventArgs) Handles uxSelectUserButton.Click
        isSelectAllUsersMode = Not isSelectAllUsersMode
        ChangeButtonState(isSelectAllUsersMode)
        UpdateIsSynchronizeForAllUsers(isSelectAllUsersMode)
    End Sub

    Private Sub ChangeButtonState(state As Boolean)
        If state Then
            uxSelectUserButton.Text = "F10 UnSelect All Users"
        Else
            uxSelectUserButton.Text = "F10 Select All Users"
        End If
    End Sub

    Private Sub UpdateIsSynchronizeForAllUsers(state As Boolean)
        For Each item As UserViewModel In viewModel
            item.IsSynchronize = state
        Next
        uxUserView.RefreshData()
    End Sub

    Private Sub uxRefreshButton_Click(sender As System.Object, e As System.EventArgs) Handles uxRefreshButton.Click
        LoadUsers()
    End Sub
End Class

