﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.uxPassNew = New DevExpress.XtraEditors.TextEdit()
        Me.uxPassConfirm = New DevExpress.XtraEditors.TextEdit()
        Me.uxPassGroup = New DevExpress.XtraEditors.GroupControl()
        Me.uxPassPanel = New DevExpress.XtraEditors.PanelControl()
        Me.uxPassOld = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.uxPassExpiry = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.uxPassCheck = New DevExpress.XtraEditors.CheckEdit()
        Me.uxSuperGroup = New DevExpress.XtraEditors.GroupControl()
        Me.uxSuperCheck = New DevExpress.XtraEditors.CheckEdit()
        Me.uxSuperPanel = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.uxSuperOld = New DevExpress.XtraEditors.TextEdit()
        Me.uxSuperNew = New DevExpress.XtraEditors.TextEdit()
        Me.uxSuperConfirm = New DevExpress.XtraEditors.TextEdit()
        Me.uxSuperExpiry = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.uxAcceptButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxCancelButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxShowCharacters = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.uxEmployeeCode = New DevExpress.XtraEditors.TextEdit()
        Me.uxName = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.uxInitials = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.uxPosition = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.uxPayrollId = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.uxOutlet = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.uxLanguageCode = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.uxTillReceiptName = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.uxUserGroup = New DevExpress.XtraEditors.GroupControl()
        Me.uxSupervisor = New DevExpress.XtraEditors.CheckEdit()
        Me.uxManager = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.uxProfileLookup = New DevExpress.XtraEditors.LookUpEdit()
        CType(Me.uxPassNew.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxPassConfirm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxPassGroup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxPassGroup.SuspendLayout()
        CType(Me.uxPassPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxPassPanel.SuspendLayout()
        CType(Me.uxPassOld.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxPassExpiry.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxPassExpiry.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxPassCheck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxSuperGroup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxSuperGroup.SuspendLayout()
        CType(Me.uxSuperCheck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxSuperPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxSuperPanel.SuspendLayout()
        CType(Me.uxSuperOld.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxSuperNew.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxSuperConfirm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxSuperExpiry.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxSuperExpiry.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxShowCharacters.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxEmployeeCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxInitials.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxPosition.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxPayrollId.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxOutlet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxLanguageCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxTillReceiptName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxUserGroup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxUserGroup.SuspendLayout()
        CType(Me.uxSupervisor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxManager.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxProfileLookup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'uxPassNew
        '
        Me.uxPassNew.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxPassNew.Location = New System.Drawing.Point(96, 26)
        Me.uxPassNew.Name = "uxPassNew"
        Me.uxPassNew.Properties.Mask.EditMask = "\d\d\d\d\d"
        Me.uxPassNew.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uxPassNew.Properties.Mask.ShowPlaceHolders = False
        Me.uxPassNew.Properties.MaxLength = 5
        Me.uxPassNew.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.uxPassNew.Properties.ValidateOnEnterKey = True
        Me.uxPassNew.Size = New System.Drawing.Size(86, 20)
        Me.uxPassNew.TabIndex = 3
        '
        'uxPassConfirm
        '
        Me.uxPassConfirm.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxPassConfirm.Enabled = False
        Me.uxPassConfirm.Location = New System.Drawing.Point(96, 52)
        Me.uxPassConfirm.Name = "uxPassConfirm"
        Me.uxPassConfirm.Properties.Mask.EditMask = "\d\d\d\d\d"
        Me.uxPassConfirm.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uxPassConfirm.Properties.Mask.ShowPlaceHolders = False
        Me.uxPassConfirm.Properties.MaxLength = 5
        Me.uxPassConfirm.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.uxPassConfirm.Properties.ValidateOnEnterKey = True
        Me.uxPassConfirm.Size = New System.Drawing.Size(86, 20)
        Me.uxPassConfirm.TabIndex = 5
        '
        'uxPassGroup
        '
        Me.uxPassGroup.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxPassGroup.AppearanceCaption.Options.UseFont = True
        Me.uxPassGroup.Controls.Add(Me.uxPassPanel)
        Me.uxPassGroup.Controls.Add(Me.uxPassCheck)
        Me.uxPassGroup.Location = New System.Drawing.Point(12, 325)
        Me.uxPassGroup.Name = "uxPassGroup"
        Me.uxPassGroup.Size = New System.Drawing.Size(192, 128)
        Me.uxPassGroup.TabIndex = 1
        Me.uxPassGroup.Text = "User Password"
        '
        'uxPassPanel
        '
        Me.uxPassPanel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxPassPanel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.uxPassPanel.Controls.Add(Me.uxPassOld)
        Me.uxPassPanel.Controls.Add(Me.LabelControl17)
        Me.uxPassPanel.Controls.Add(Me.uxPassNew)
        Me.uxPassPanel.Controls.Add(Me.uxPassConfirm)
        Me.uxPassPanel.Controls.Add(Me.uxPassExpiry)
        Me.uxPassPanel.Controls.Add(Me.LabelControl1)
        Me.uxPassPanel.Controls.Add(Me.LabelControl3)
        Me.uxPassPanel.Controls.Add(Me.LabelControl2)
        Me.uxPassPanel.Location = New System.Drawing.Point(5, 23)
        Me.uxPassPanel.Margin = New System.Windows.Forms.Padding(0)
        Me.uxPassPanel.Name = "uxPassPanel"
        Me.uxPassPanel.Size = New System.Drawing.Size(182, 103)
        Me.uxPassPanel.TabIndex = 22
        '
        'uxPassOld
        '
        Me.uxPassOld.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxPassOld.Location = New System.Drawing.Point(96, 0)
        Me.uxPassOld.Name = "uxPassOld"
        Me.uxPassOld.Properties.Mask.EditMask = "\d\d\d\d\d"
        Me.uxPassOld.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uxPassOld.Properties.Mask.ShowPlaceHolders = False
        Me.uxPassOld.Properties.MaxLength = 5
        Me.uxPassOld.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.uxPassOld.Properties.ValidateOnEnterKey = True
        Me.uxPassOld.Size = New System.Drawing.Size(86, 20)
        Me.uxPassOld.TabIndex = 1
        '
        'LabelControl17
        '
        Me.LabelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl17.Location = New System.Drawing.Point(0, 2)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl17.TabIndex = 0
        Me.LabelControl17.Text = "Old password"
        '
        'uxPassExpiry
        '
        Me.uxPassExpiry.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxPassExpiry.EditValue = Nothing
        Me.uxPassExpiry.Enabled = False
        Me.uxPassExpiry.Location = New System.Drawing.Point(96, 78)
        Me.uxPassExpiry.Name = "uxPassExpiry"
        Me.uxPassExpiry.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.uxPassExpiry.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.uxPassExpiry.Size = New System.Drawing.Size(86, 20)
        Me.uxPassExpiry.TabIndex = 7
        '
        'LabelControl1
        '
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl1.Location = New System.Drawing.Point(0, 28)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl1.TabIndex = 2
        Me.LabelControl1.Text = "New password"
        '
        'LabelControl3
        '
        Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl3.Location = New System.Drawing.Point(0, 80)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl3.TabIndex = 6
        Me.LabelControl3.Text = "Expiry date"
        '
        'LabelControl2
        '
        Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl2.Location = New System.Drawing.Point(0, 54)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl2.TabIndex = 4
        Me.LabelControl2.Text = "Confirm password"
        '
        'uxPassCheck
        '
        Me.uxPassCheck.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxPassCheck.EditValue = True
        Me.uxPassCheck.Location = New System.Drawing.Point(171, 1)
        Me.uxPassCheck.Name = "uxPassCheck"
        Me.uxPassCheck.Properties.Caption = ""
        Me.uxPassCheck.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.uxPassCheck.Size = New System.Drawing.Size(19, 19)
        Me.uxPassCheck.TabIndex = 6
        '
        'uxSuperGroup
        '
        Me.uxSuperGroup.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxSuperGroup.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxSuperGroup.AppearanceCaption.Options.UseFont = True
        Me.uxSuperGroup.Controls.Add(Me.uxSuperCheck)
        Me.uxSuperGroup.Controls.Add(Me.uxSuperPanel)
        Me.uxSuperGroup.Location = New System.Drawing.Point(211, 325)
        Me.uxSuperGroup.Name = "uxSuperGroup"
        Me.uxSuperGroup.Size = New System.Drawing.Size(192, 128)
        Me.uxSuperGroup.TabIndex = 2
        Me.uxSuperGroup.Text = "Supervisor Password"
        Me.uxSuperGroup.Visible = False
        '
        'uxSuperCheck
        '
        Me.uxSuperCheck.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxSuperCheck.EditValue = True
        Me.uxSuperCheck.Location = New System.Drawing.Point(170, 1)
        Me.uxSuperCheck.Name = "uxSuperCheck"
        Me.uxSuperCheck.Properties.Caption = ""
        Me.uxSuperCheck.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.uxSuperCheck.Size = New System.Drawing.Size(19, 19)
        Me.uxSuperCheck.TabIndex = 24
        '
        'uxSuperPanel
        '
        Me.uxSuperPanel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxSuperPanel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.uxSuperPanel.Controls.Add(Me.LabelControl18)
        Me.uxSuperPanel.Controls.Add(Me.uxSuperOld)
        Me.uxSuperPanel.Controls.Add(Me.uxSuperNew)
        Me.uxSuperPanel.Controls.Add(Me.uxSuperConfirm)
        Me.uxSuperPanel.Controls.Add(Me.uxSuperExpiry)
        Me.uxSuperPanel.Controls.Add(Me.LabelControl6)
        Me.uxSuperPanel.Controls.Add(Me.LabelControl5)
        Me.uxSuperPanel.Controls.Add(Me.LabelControl4)
        Me.uxSuperPanel.Location = New System.Drawing.Point(5, 23)
        Me.uxSuperPanel.Margin = New System.Windows.Forms.Padding(0)
        Me.uxSuperPanel.Name = "uxSuperPanel"
        Me.uxSuperPanel.Size = New System.Drawing.Size(182, 103)
        Me.uxSuperPanel.TabIndex = 23
        '
        'LabelControl18
        '
        Me.LabelControl18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl18.Location = New System.Drawing.Point(0, 0)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl18.TabIndex = 0
        Me.LabelControl18.Text = "Old password"
        '
        'uxSuperOld
        '
        Me.uxSuperOld.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxSuperOld.Location = New System.Drawing.Point(96, 0)
        Me.uxSuperOld.Name = "uxSuperOld"
        Me.uxSuperOld.Properties.Mask.EditMask = "\d\d\d\d\d"
        Me.uxSuperOld.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uxSuperOld.Properties.Mask.ShowPlaceHolders = False
        Me.uxSuperOld.Properties.MaxLength = 5
        Me.uxSuperOld.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.uxSuperOld.Properties.ValidateOnEnterKey = True
        Me.uxSuperOld.Size = New System.Drawing.Size(86, 20)
        Me.uxSuperOld.TabIndex = 1
        '
        'uxSuperNew
        '
        Me.uxSuperNew.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxSuperNew.Location = New System.Drawing.Point(96, 26)
        Me.uxSuperNew.Name = "uxSuperNew"
        Me.uxSuperNew.Properties.Mask.EditMask = "\d\d\d\d\d"
        Me.uxSuperNew.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uxSuperNew.Properties.Mask.ShowPlaceHolders = False
        Me.uxSuperNew.Properties.MaxLength = 5
        Me.uxSuperNew.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.uxSuperNew.Properties.ValidateOnEnterKey = True
        Me.uxSuperNew.Size = New System.Drawing.Size(86, 20)
        Me.uxSuperNew.TabIndex = 3
        '
        'uxSuperConfirm
        '
        Me.uxSuperConfirm.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxSuperConfirm.Enabled = False
        Me.uxSuperConfirm.Location = New System.Drawing.Point(96, 52)
        Me.uxSuperConfirm.Name = "uxSuperConfirm"
        Me.uxSuperConfirm.Properties.Mask.EditMask = "\d\d\d\d\d"
        Me.uxSuperConfirm.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uxSuperConfirm.Properties.Mask.ShowPlaceHolders = False
        Me.uxSuperConfirm.Properties.MaxLength = 5
        Me.uxSuperConfirm.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.uxSuperConfirm.Properties.ValidateOnEnterKey = True
        Me.uxSuperConfirm.Size = New System.Drawing.Size(86, 20)
        Me.uxSuperConfirm.TabIndex = 5
        '
        'uxSuperExpiry
        '
        Me.uxSuperExpiry.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxSuperExpiry.EditValue = Nothing
        Me.uxSuperExpiry.Enabled = False
        Me.uxSuperExpiry.Location = New System.Drawing.Point(96, 78)
        Me.uxSuperExpiry.Name = "uxSuperExpiry"
        Me.uxSuperExpiry.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.uxSuperExpiry.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.uxSuperExpiry.Size = New System.Drawing.Size(86, 20)
        Me.uxSuperExpiry.TabIndex = 7
        '
        'LabelControl6
        '
        Me.LabelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl6.Location = New System.Drawing.Point(0, 26)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl6.TabIndex = 2
        Me.LabelControl6.Text = "New password"
        '
        'LabelControl5
        '
        Me.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl5.Location = New System.Drawing.Point(0, 52)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl5.TabIndex = 4
        Me.LabelControl5.Text = "Confirm password"
        '
        'LabelControl4
        '
        Me.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl4.Location = New System.Drawing.Point(0, 78)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl4.TabIndex = 6
        Me.LabelControl4.Text = "Expiry date"
        '
        'uxAcceptButton
        '
        Me.uxAcceptButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxAcceptButton.Enabled = False
        Me.uxAcceptButton.Location = New System.Drawing.Point(247, 460)
        Me.uxAcceptButton.Name = "uxAcceptButton"
        Me.uxAcceptButton.Size = New System.Drawing.Size(75, 37)
        Me.uxAcceptButton.TabIndex = 4
        Me.uxAcceptButton.Text = "F5 Accept"
        '
        'uxCancelButton
        '
        Me.uxCancelButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxCancelButton.Location = New System.Drawing.Point(328, 460)
        Me.uxCancelButton.Name = "uxCancelButton"
        Me.uxCancelButton.Size = New System.Drawing.Size(75, 37)
        Me.uxCancelButton.TabIndex = 5
        Me.uxCancelButton.Text = "F10 Cancel"
        '
        'uxShowCharacters
        '
        Me.uxShowCharacters.Location = New System.Drawing.Point(10, 459)
        Me.uxShowCharacters.Name = "uxShowCharacters"
        Me.uxShowCharacters.Properties.Caption = "&Show password characters"
        Me.uxShowCharacters.Size = New System.Drawing.Size(194, 19)
        Me.uxShowCharacters.TabIndex = 3
        '
        'LabelControl7
        '
        Me.LabelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl7.Location = New System.Drawing.Point(5, 23)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl7.TabIndex = 0
        Me.LabelControl7.Text = "Employee Code"
        '
        'uxEmployeeCode
        '
        Me.uxEmployeeCode.Enabled = False
        Me.uxEmployeeCode.Location = New System.Drawing.Point(101, 23)
        Me.uxEmployeeCode.Name = "uxEmployeeCode"
        Me.uxEmployeeCode.Properties.Mask.EditMask = "\d\d\d"
        Me.uxEmployeeCode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uxEmployeeCode.Properties.Mask.ShowPlaceHolders = False
        Me.uxEmployeeCode.Size = New System.Drawing.Size(86, 20)
        Me.uxEmployeeCode.TabIndex = 1
        '
        'uxName
        '
        Me.uxName.Location = New System.Drawing.Point(101, 75)
        Me.uxName.Name = "uxName"
        Me.uxName.Properties.MaxLength = 35
        Me.uxName.Size = New System.Drawing.Size(193, 20)
        Me.uxName.TabIndex = 5
        '
        'LabelControl8
        '
        Me.LabelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl8.Location = New System.Drawing.Point(5, 75)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl8.TabIndex = 4
        Me.LabelControl8.Text = "Name"
        '
        'uxInitials
        '
        Me.uxInitials.Location = New System.Drawing.Point(101, 49)
        Me.uxInitials.Name = "uxInitials"
        Me.uxInitials.Properties.MaxLength = 3
        Me.uxInitials.Size = New System.Drawing.Size(86, 20)
        Me.uxInitials.TabIndex = 3
        '
        'LabelControl9
        '
        Me.LabelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl9.Location = New System.Drawing.Point(5, 49)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl9.TabIndex = 2
        Me.LabelControl9.Text = "Initials"
        '
        'uxPosition
        '
        Me.uxPosition.Location = New System.Drawing.Point(101, 127)
        Me.uxPosition.Name = "uxPosition"
        Me.uxPosition.Properties.MaxLength = 20
        Me.uxPosition.Size = New System.Drawing.Size(193, 20)
        Me.uxPosition.TabIndex = 9
        '
        'LabelControl10
        '
        Me.LabelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl10.Location = New System.Drawing.Point(5, 127)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl10.TabIndex = 8
        Me.LabelControl10.Text = "Position"
        '
        'uxPayrollId
        '
        Me.uxPayrollId.Location = New System.Drawing.Point(101, 153)
        Me.uxPayrollId.Name = "uxPayrollId"
        Me.uxPayrollId.Properties.MaxLength = 20
        Me.uxPayrollId.Size = New System.Drawing.Size(193, 20)
        Me.uxPayrollId.TabIndex = 11
        '
        'LabelControl11
        '
        Me.LabelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl11.Location = New System.Drawing.Point(5, 153)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl11.TabIndex = 10
        Me.LabelControl11.Text = "Payroll ID"
        '
        'uxOutlet
        '
        Me.uxOutlet.Location = New System.Drawing.Point(101, 179)
        Me.uxOutlet.Name = "uxOutlet"
        Me.uxOutlet.Properties.MaxLength = 2
        Me.uxOutlet.Size = New System.Drawing.Size(86, 20)
        Me.uxOutlet.TabIndex = 13
        '
        'LabelControl12
        '
        Me.LabelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl12.Location = New System.Drawing.Point(5, 179)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl12.TabIndex = 12
        Me.LabelControl12.Text = "Outlet"
        '
        'uxLanguageCode
        '
        Me.uxLanguageCode.Location = New System.Drawing.Point(101, 205)
        Me.uxLanguageCode.Name = "uxLanguageCode"
        Me.uxLanguageCode.Properties.MaxLength = 3
        Me.uxLanguageCode.Size = New System.Drawing.Size(86, 20)
        Me.uxLanguageCode.TabIndex = 17
        '
        'LabelControl14
        '
        Me.LabelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl14.Location = New System.Drawing.Point(5, 205)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl14.TabIndex = 16
        Me.LabelControl14.Text = "Language Code"
        '
        'uxTillReceiptName
        '
        Me.uxTillReceiptName.Location = New System.Drawing.Point(101, 101)
        Me.uxTillReceiptName.Name = "uxTillReceiptName"
        Me.uxTillReceiptName.Properties.MaxLength = 35
        Me.uxTillReceiptName.Size = New System.Drawing.Size(193, 20)
        Me.uxTillReceiptName.TabIndex = 7
        '
        'LabelControl15
        '
        Me.LabelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl15.Location = New System.Drawing.Point(5, 101)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl15.TabIndex = 6
        Me.LabelControl15.Text = "Receipt Name"
        '
        'LabelControl16
        '
        Me.LabelControl16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl16.Location = New System.Drawing.Point(5, 231)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl16.TabIndex = 18
        Me.LabelControl16.Text = "Profile ID"
        '
        'uxUserGroup
        '
        Me.uxUserGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxUserGroup.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxUserGroup.AppearanceCaption.Options.UseFont = True
        Me.uxUserGroup.Controls.Add(Me.uxSupervisor)
        Me.uxUserGroup.Controls.Add(Me.uxManager)
        Me.uxUserGroup.Controls.Add(Me.LabelControl19)
        Me.uxUserGroup.Controls.Add(Me.LabelControl13)
        Me.uxUserGroup.Controls.Add(Me.uxProfileLookup)
        Me.uxUserGroup.Controls.Add(Me.LabelControl7)
        Me.uxUserGroup.Controls.Add(Me.uxEmployeeCode)
        Me.uxUserGroup.Controls.Add(Me.LabelControl8)
        Me.uxUserGroup.Controls.Add(Me.uxName)
        Me.uxUserGroup.Controls.Add(Me.LabelControl16)
        Me.uxUserGroup.Controls.Add(Me.LabelControl9)
        Me.uxUserGroup.Controls.Add(Me.uxTillReceiptName)
        Me.uxUserGroup.Controls.Add(Me.uxInitials)
        Me.uxUserGroup.Controls.Add(Me.LabelControl15)
        Me.uxUserGroup.Controls.Add(Me.LabelControl10)
        Me.uxUserGroup.Controls.Add(Me.uxLanguageCode)
        Me.uxUserGroup.Controls.Add(Me.uxPosition)
        Me.uxUserGroup.Controls.Add(Me.LabelControl14)
        Me.uxUserGroup.Controls.Add(Me.LabelControl11)
        Me.uxUserGroup.Controls.Add(Me.uxPayrollId)
        Me.uxUserGroup.Controls.Add(Me.LabelControl12)
        Me.uxUserGroup.Controls.Add(Me.uxOutlet)
        Me.uxUserGroup.Location = New System.Drawing.Point(12, 12)
        Me.uxUserGroup.Name = "uxUserGroup"
        Me.uxUserGroup.Size = New System.Drawing.Size(391, 307)
        Me.uxUserGroup.TabIndex = 0
        Me.uxUserGroup.Text = "User Details"
        '
        'uxSupervisor
        '
        Me.uxSupervisor.Location = New System.Drawing.Point(99, 283)
        Me.uxSupervisor.Name = "uxSupervisor"
        Me.uxSupervisor.Properties.Caption = ""
        Me.uxSupervisor.Size = New System.Drawing.Size(21, 19)
        Me.uxSupervisor.TabIndex = 26
        '
        'uxManager
        '
        Me.uxManager.Location = New System.Drawing.Point(99, 257)
        Me.uxManager.Name = "uxManager"
        Me.uxManager.Properties.Caption = ""
        Me.uxManager.Size = New System.Drawing.Size(21, 19)
        Me.uxManager.TabIndex = 25
        '
        'LabelControl19
        '
        Me.LabelControl19.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl19.Location = New System.Drawing.Point(5, 282)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl19.TabIndex = 24
        Me.LabelControl19.Text = "Supervisor"
        '
        'LabelControl13
        '
        Me.LabelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl13.Location = New System.Drawing.Point(5, 257)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl13.TabIndex = 23
        Me.LabelControl13.Text = "Manager"
        '
        'uxProfileLookup
        '
        Me.uxProfileLookup.Location = New System.Drawing.Point(101, 231)
        Me.uxProfileLookup.Name = "uxProfileLookup"
        Me.uxProfileLookup.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.uxProfileLookup.Size = New System.Drawing.Size(86, 20)
        Me.uxProfileLookup.TabIndex = 22
        '
        'UserForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(415, 509)
        Me.Controls.Add(Me.uxUserGroup)
        Me.Controls.Add(Me.uxShowCharacters)
        Me.Controls.Add(Me.uxSuperGroup)
        Me.Controls.Add(Me.uxCancelButton)
        Me.Controls.Add(Me.uxAcceptButton)
        Me.Controls.Add(Me.uxPassGroup)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.Name = "UserForm"
        Me.ShowIcon = False
        CType(Me.uxPassNew.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxPassConfirm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxPassGroup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxPassGroup.ResumeLayout(False)
        CType(Me.uxPassPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxPassPanel.ResumeLayout(False)
        CType(Me.uxPassOld.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxPassExpiry.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxPassExpiry.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxPassCheck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxSuperGroup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxSuperGroup.ResumeLayout(False)
        CType(Me.uxSuperCheck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxSuperPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxSuperPanel.ResumeLayout(False)
        CType(Me.uxSuperOld.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxSuperNew.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxSuperConfirm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxSuperExpiry.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxSuperExpiry.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxShowCharacters.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxEmployeeCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxInitials.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxPosition.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxPayrollId.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxOutlet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxLanguageCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxTillReceiptName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxUserGroup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxUserGroup.ResumeLayout(False)
        CType(Me.uxSupervisor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxManager.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxProfileLookup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents uxPassNew As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uxPassConfirm As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uxPassGroup As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxPassExpiry As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxSuperGroup As DevExpress.XtraEditors.GroupControl
    Friend WithEvents uxSuperExpiry As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxSuperConfirm As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uxSuperNew As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uxAcceptButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxCancelButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxShowCharacters As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxEmployeeCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uxName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxInitials As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxPosition As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxPayrollId As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxOutlet As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxLanguageCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxTillReceiptName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxUserGroup As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxPassOld As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxSuperOld As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uxPassCheck As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents uxPassPanel As DevExpress.XtraEditors.PanelControl
    Friend WithEvents uxSuperCheck As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents uxSuperPanel As DevExpress.XtraEditors.PanelControl
    Friend WithEvents uxProfileLookup As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxSupervisor As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents uxManager As DevExpress.XtraEditors.CheckEdit
End Class
