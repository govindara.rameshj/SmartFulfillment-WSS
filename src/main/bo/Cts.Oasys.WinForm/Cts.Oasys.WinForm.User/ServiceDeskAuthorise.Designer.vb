﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ServiceDeskAuthorise
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblTill = New System.Windows.Forms.Label
        Me.lblWorkStation = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.lblGenKey = New System.Windows.Forms.Label
        Me.lblPassword = New System.Windows.Forms.Label
        Me.lblKey = New System.Windows.Forms.Label
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnAccept = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblTill
        '
        Me.lblTill.AutoSize = True
        Me.lblTill.Location = New System.Drawing.Point(12, 18)
        Me.lblTill.Name = "lblTill"
        Me.lblTill.Size = New System.Drawing.Size(109, 13)
        Me.lblTill.TabIndex = 0
        Me.lblTill.Text = "Selected Workstation"
        '
        'lblWorkStation
        '
        Me.lblWorkStation.AutoSize = True
        Me.lblWorkStation.Location = New System.Drawing.Point(139, 18)
        Me.lblWorkStation.Name = "lblWorkStation"
        Me.lblWorkStation.Size = New System.Drawing.Size(0, 13)
        Me.lblWorkStation.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtPassword)
        Me.GroupBox1.Controls.Add(Me.lblGenKey)
        Me.GroupBox1.Controls.Add(Me.lblPassword)
        Me.GroupBox1.Controls.Add(Me.lblKey)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 58)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(209, 113)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Service Desk Password Required"
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(136, 66)
        Me.txtPassword.MaxLength = 5
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(41, 20)
        Me.txtPassword.TabIndex = 3
        '
        'lblGenKey
        '
        Me.lblGenKey.AutoSize = True
        Me.lblGenKey.Location = New System.Drawing.Point(133, 40)
        Me.lblGenKey.Name = "lblGenKey"
        Me.lblGenKey.Size = New System.Drawing.Size(37, 13)
        Me.lblGenKey.TabIndex = 2
        Me.lblGenKey.Text = "99999"
        '
        'lblPassword
        '
        Me.lblPassword.AutoSize = True
        Me.lblPassword.Location = New System.Drawing.Point(35, 73)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(79, 13)
        Me.lblPassword.TabIndex = 1
        Me.lblPassword.Text = "PASSWORD ="
        '
        'lblKey
        '
        Me.lblKey.AutoSize = True
        Me.lblKey.Location = New System.Drawing.Point(35, 40)
        Me.lblKey.Name = "lblKey"
        Me.lblKey.Size = New System.Drawing.Size(79, 13)
        Me.lblKey.TabIndex = 0
        Me.lblKey.Text = "KEY               ="
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(8, 205)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 39)
        Me.btnExit.TabIndex = 3
        Me.btnExit.Text = "F10 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnAccept
        '
        Me.btnAccept.Location = New System.Drawing.Point(157, 205)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(75, 39)
        Me.btnAccept.TabIndex = 4
        Me.btnAccept.Text = "F5 Accept"
        Me.btnAccept.UseVisualStyleBackColor = True
        '
        'PassWord
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(244, 247)
        Me.Controls.Add(Me.btnAccept)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblWorkStation)
        Me.Controls.Add(Me.lblTill)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.Name = "PassWord"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "PassWord"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblTill As System.Windows.Forms.Label
    Friend WithEvents lblWorkStation As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents lblGenKey As System.Windows.Forms.Label
    Friend WithEvents lblPassword As System.Windows.Forms.Label
    Friend WithEvents lblKey As System.Windows.Forms.Label
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnAccept As System.Windows.Forms.Button
End Class
