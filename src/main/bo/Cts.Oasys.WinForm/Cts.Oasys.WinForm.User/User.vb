Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.Security
Imports System.ComponentModel
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid

Public Class User

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        e.Handled = True
        Select Case e.KeyData
            Case Keys.F5 : uxAddUserButton.PerformClick()
            Case Keys.F6 : uxMaintainUserButton.PerformClick()
            Case Keys.F7 : uxDeleteUserButton.PerformClick()
            Case Keys.F12 : uxExitButton.PerformClick()
            Case Else : e.Handled = False
        End Select
    End Sub

    Protected Overrides Sub DoProcessing()

        Try
            Cursor = Cursors.WaitCursor
            LoadUsers()
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub


    Private Sub LoadUsers()

        Dim users As Core.Security.UserCollection = Core.Security.User.GetUsers
        uxUserGrid.DataSource = users

        For Each col As GridColumn In uxUserView.Columns
            Select Case col.FieldName
                Case GetPropertyName(Function(f As Core.Security.User) f.Id)
                    col.Caption = My.Resources.ColumnHeaders.Id
                    col.OptionsColumn.AllowEdit = False

                Case GetPropertyName(Function(f As Core.Security.User) f.EmployeeCode)
                    col.Caption = My.Resources.ColumnHeaders.EmployeeCode

                Case GetPropertyName(Function(f As Core.Security.User) f.Initials)
                Case GetPropertyName(Function(f As Core.Security.User) f.Name)
                Case GetPropertyName(Function(f As Core.Security.User) f.Position)
                Case GetPropertyName(Function(f As Core.Security.User) f.PayrollID)

                Case GetPropertyName(Function(f As Core.Security.User) f.IsSupervisor)
                    col.Caption = My.Resources.ColumnHeaders.IsSupervisor

                Case GetPropertyName(Function(f As Core.Security.User) f.IsManager)
                    col.Caption = My.Resources.ColumnHeaders.IsManager

                Case GetPropertyName(Function(f As Core.Security.User) f.TillReceiptName)
                    col.Caption = My.Resources.ColumnHeaders.TillReceiptName

                Case GetPropertyName(Function(f As Core.Security.User) f.LanguageCode)
                Case GetPropertyName(Function(f As Core.Security.User) f.SecurityProfileId)
                    col.Caption = My.Resources.ColumnHeaders.SecurityProfileId

                Case GetPropertyName(Function(f As Core.Security.User) f.IsDeleted)
                    col.Caption = My.Resources.ColumnHeaders.IsDeleted
                    col.OptionsColumn.AllowEdit = False

                Case GetPropertyName(Function(f As Core.Security.User) f.Password)
                    col.Visible = False
                    col.OptionsColumn.AllowShowHide = False

                Case GetPropertyName(Function(f As Core.Security.User) f.SuperPassword)
                    col.Visible = False
                    col.OptionsColumn.AllowShowHide = False

                Case Else
                    col.Visible = False
            End Select
        Next

        uxUserView.BestFitColumns()

    End Sub

    Private Sub uxUserView_RowStyle(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles uxUserView.RowStyle

        'check if any rows loaded first
        If e.RowHandle < 0 Then Exit Sub

        Dim view As GridView = sender
        Dim isDeleted As Boolean = view.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As Core.Security.User) f.IsDeleted))
        If isDeleted Then
            e.Appearance.BackColor = Color.LightSalmon
            e.Appearance.BackColor2 = Color.Red
        End If

    End Sub


    Private Sub uxAddUserButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxAddUserButton.Click

        'create new user (automatically gets net available user id and employee code from table)
        Dim userItem As New Core.Security.User

        'load user form
        Using uf As New WinForm.User.Maintain.UserForm(userItem, Maintain.FormState.Add)
            If uf.ShowDialog = DialogResult.OK Then
                Dim users As Core.Security.UserCollection = uxUserGrid.DataSource
                users.Add(userItem)
            End If
        End Using

    End Sub

    Private Sub uxMaintainUserButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxMaintainUserButton.Click

        'get user item for focused row
        Dim users As Core.Security.UserCollection = uxUserGrid.DataSource
        Dim userItem As Core.Security.User = users(uxUserView.GetDataSourceRowIndex(uxUserView.FocusedRowHandle))

        'load user form
        Using uf As New WinForm.User.Maintain.UserForm(userItem, Maintain.FormState.Maintain)
            If uf.ShowDialog = DialogResult.OK Then
                userItem.Update()
            End If
        End Using

    End Sub

    Private Sub uxDeleteUserButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxDeleteUserButton.Click

        If MessageBox.Show(My.Resources.Strings.YesNoDeleteUser, AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Dim users As Core.Security.UserCollection = uxUserGrid.DataSource
            Dim userItem As Core.Security.User = users(uxUserView.GetDataSourceRowIndex(uxUserView.FocusedRowHandle))

            userItem.Delete(UserId, WorkstationId)
            uxUserView.RefreshRow(uxUserView.FocusedRowHandle)
        End If

    End Sub

    Private Sub uxExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxExitButton.Click
        FindForm.Close()
    End Sub

End Class
