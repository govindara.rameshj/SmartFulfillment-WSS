﻿Imports TpWickes.Library

Public Class ValidateDeletedUsersFactory
    Inherits RequirementSwitchFactory(Of IValidateDeletedUsers)

    Private Const _RequirementId As Integer = -845

    Public Overrides Function ImplementationA() As IValidateDeletedUsers
        Return New ValidateDeletedUsers()
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim RequirementSwitch As IRequirementRepository

        RequirementSwitch = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = RequirementSwitch.IsSwitchPresentAndEnabled(_RequirementId)
    End Function

    Public Overrides Function ImplementationB() As IValidateDeletedUsers
        Return New DeletedUsersNotValidated()
    End Function
End Class
