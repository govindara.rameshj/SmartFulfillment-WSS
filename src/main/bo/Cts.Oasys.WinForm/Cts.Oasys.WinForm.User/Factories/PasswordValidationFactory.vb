﻿Public Class PasswordValidationFactory
    Inherits RequirementSwitchFactory(Of IPasswordValidation)

    Private Const _RequirementSwitchRF0844 As Integer = -844

    Public Overrides Function ImplementationA() As IPasswordValidation

        Return New UniqueSupervisorPasswordEnforce

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim RequirementSwitch As IRequirementRepository

        RequirementSwitch = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = RequirementSwitch.IsSwitchPresentAndEnabled(_RequirementSwitchRF0844)

    End Function

    Public Overrides Function ImplementationB() As IPasswordValidation

        Return New UniqueSupervisorPasswordIgnore

    End Function

End Class