﻿
namespace WSS.BO.BOReceiverService.Host.Contracts
{
    public interface IServiceCallResult
    {
        object GetResultToSerialize();
    }
}
