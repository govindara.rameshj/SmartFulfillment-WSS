﻿using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace WSS.BO.BOReceiverService.Host.Contracts
{
    [ServiceContract]
    public interface IReceiverServiceContract
    {
        // Request: Json for WSS.BO.Model.Entity.Transactions.Transaction
        // Response: Json for WSS.BO.BOReceiverService.BL.ServiceInfoProcessing.ServiceCallResult
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/transactions/")]
        Stream AddTransaction(Stream transactionJson);

        // Request: void
        // Response: WSS.BO.BOReceiverService.BL.ServiceInfoProcessing.ServiceCallResult<ServiceInfo>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/info/")]
        Stream GetServiceInfo();

        // Request: Receipt Barcode
        // Response: WSS.BO.Model.Entity.Transactions.RetailTransaction
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/transactions?receiptBarcode={receiptBarcode}")]
        Stream FindTransaction(string receiptBarcode);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/statistics/")]
        Stream GetStatistics();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/statistics/clear/")]
        Stream ClearStatistics();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/statistics/apply/")]
        Stream ApplyCounters(Stream countersJson);
    }
}
