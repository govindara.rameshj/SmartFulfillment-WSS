﻿namespace WSS.BO.BOReceiverService.Host.Contracts
{
    public class StatusDescription
    {
        public string Status { get; set; }
        public string Details { get; set; }

        public override string ToString()
        {
            return string.Format("StatusDescription: Status = '{0}', Details = '{1}'", Status, Details);
        }
    }
}
