﻿
using System.Runtime.Serialization;

namespace WSS.BO.BOReceiverService.Host.Contracts
{
    [DataContract]
    public class ServiceCallResult<TMethodResult> : IServiceCallResult
        where TMethodResult : class
    {
        public ServiceCallResult(TMethodResult result)
        {
            Result = result;
        }

        public ServiceCallResult(StatusDescription errorDescription)
        {
            ErrorDescription = errorDescription;
        }

        public TMethodResult Result { get; private set; }
        public StatusDescription ErrorDescription { get; private set; }

        public object GetResultToSerialize()
        {
            return (object)Result ?? ErrorDescription;
        }

        public override string ToString()
        {
            return Result != null ? Result.ToString() : ErrorDescription.ToString();
        }
    }

    [DataContract]
    public class ServiceCallResult : IServiceCallResult
    {
        public ServiceCallResult(StatusDescription errorDescription)
        {
            ErrorDescription = errorDescription;
        }

        public StatusDescription ErrorDescription { get; private set; }

        public object GetResultToSerialize()
        {
            return ErrorDescription;
        }

        public override string ToString()
        {
            return ErrorDescription.ToString();
        }
    }
}