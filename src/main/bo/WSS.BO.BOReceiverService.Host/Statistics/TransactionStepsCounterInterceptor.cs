﻿using Ninject.Extensions.Interception.Request;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing;
using WSS.BO.Common.Hosting.Statistics;

namespace WSS.BO.BOReceiverService.Host.Statistics
{
    public class TransactionStepsCounterInterceptor : CounterInterceptor
    {
        public TransactionStepsCounterInterceptor(CountersCollection counters, string defaultContext)
            :base(counters, defaultContext)
        {
        }

        protected override string GetContext(IProxyRequest invocationRequest)
        {
            var steps = (ITransactionProcessingSteps) invocationRequest.Target;

            var transaction = steps.GetTransaction();
            return transaction != null ? transaction.GetType().Name : base.GetContext(invocationRequest);
        }
    }
}
