﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using Ninject;
using slf4net;

namespace WSS.BO.BOReceiverService.Host.Hosting
{
    public class ContainerResolvableServiceBehavior : IServiceBehavior
    {
        private readonly IKernel kernel;

        public ContainerResolvableServiceBehavior(IKernel kernel)
        {
            this.kernel = kernel;
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            try
            {
                foreach (var cdb in serviceHostBase.ChannelDispatchers)
                {
                    ChannelDispatcher cd = cdb as ChannelDispatcher;
                    if (cd != null)
                    {
                        foreach (EndpointDispatcher ed in cd.Endpoints)
                        {
                            ed.DispatchRuntime.InstanceProvider = new ContainerResolvableInstanceProvider(kernel, serviceDescription.ServiceType);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerFactory.GetLogger(typeof(ContainerResolvableServiceBehavior)).Error(ex, "Error during applying service behavior");
                throw;
            }
        }

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }
    }
}