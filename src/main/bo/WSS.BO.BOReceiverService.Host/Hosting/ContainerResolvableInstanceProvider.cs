﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using Ninject;
using slf4net;

namespace WSS.BO.BOReceiverService.Host.Hosting
{
    public class ContainerResolvableInstanceProvider : IInstanceProvider
    {
        private readonly IKernel kernel;
        private readonly Type _serviceType;

        public ContainerResolvableInstanceProvider(IKernel kernel, Type serviceType)
        {
            this.kernel = kernel;
            _serviceType = serviceType;
        }

        public object GetInstance(InstanceContext instanceContext)
        {
            return GetInstance(instanceContext, null);
        }

        public object GetInstance(InstanceContext instanceContext, Message message)
        {
            try
            {
                return kernel.Get(_serviceType);
            }
            catch (Exception ex)
            {
                LoggerFactory.GetLogger(typeof(ContainerResolvableInstanceProvider)).Error(ex, "Error during service instance creation");
                throw;
            }
        }

        public void ReleaseInstance(InstanceContext instanceContext, object instance)
        {
        }
    }
}