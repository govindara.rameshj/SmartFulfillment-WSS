﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using slf4net;

namespace WSS.BO.BOReceiverService.Host.Hosting
{
    public class ReceiverServiceHostFactory : ServiceHostFactory
    {
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            try
            {
                return new ReceiverServiceHost(serviceType, baseAddresses);
            }
            catch (Exception ex)
            {
                LoggerFactory.GetLogger(typeof(ReceiverServiceHostFactory)).Error(ex, "Error during service host creation");
                throw;
            }
        }
    }
}