﻿using System;
using System.Reflection;
using System.ServiceModel;
using slf4net;
using WSS.BO.BOReceiverService.Host.Configuration;
using WSS.BO.Common.Hosting.Statistics;

namespace WSS.BO.BOReceiverService.Host.Hosting
{
    // It is necessary for 1) setting up DI container on service host start, 2) Resolving service instances via DI container.
    public class ReceiverServiceHost : ServiceHost
    {
        private static readonly ILogger Log = LoggerFactory.GetLogger(typeof(ReceiverServiceHost));
        public CountersCollection CountersDictionary { get; private set; }

        public ReceiverServiceHost(Type serviceType, Uri[] baseAddresses)
            : base(serviceType, baseAddresses)
        {
            CountersDictionary = new CountersCollection();
        }

        public static ReceiverServiceHost Instance
        {
            get
            {
                return (ReceiverServiceHost)OperationContext.Current.Host;
            }
        }

        public Version GetVersion()
        {
            return Assembly.GetAssembly(GetType()).GetName().Version;
        }

        protected override void OnOpening()
        {
            Log.Info("Start initialization of service host for BOReceiverService version {0}", GetVersion());
            try
            {
                var config = new DependencyInjectionConfig(CountersDictionary);
                var kernel = config.GetKernel();

                Description.Behaviors.Add(new ContainerResolvableServiceBehavior(kernel));
            }
            catch (Exception ex)
            {
                LoggerFactory.GetLogger(typeof(ReceiverServiceHost)).Error(ex, "Error during service initialization");
                throw;
            }
            base.OnOpening();
        }

        protected override void OnOpened()
        {
            base.OnOpened();
            Log.Info("Service host for BOReceiverService version {0} initialized successfully.", GetVersion());
        }
    }
}