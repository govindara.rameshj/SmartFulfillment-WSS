using System;
using System.Collections.Specialized;
using System.Configuration;
using WSS.BO.BOReceiverService.BL.Configuration;
using WSS.BO.BOReceiverService.Host.Hosting;

namespace WSS.BO.BOReceiverService.Host.Configuration
{
    public class ServiceConfiguration : IConfiguration
    {
        private readonly NameValueCollection appSettings = ConfigurationManager.AppSettings;

        public string ServiceName
        {
            get
            {
                return appSettings.Get("serviceName");
            }
        }

        public Version ServiceVersion
        {
            get
            {
                return ReceiverServiceHost.Instance.GetVersion();
            }
        }

        public string DeadlockPriorityForSavingTransaction
        {
            get
            {
                return appSettings.Get("deadlockPriorityForSavingTransaction");
            }
        }

        public bool ValidateTotalAmounts
        {
            get
            {
                return Convert.ToBoolean(appSettings.Get("validateTotalAmounts"));
            }
        }

        public bool TruncateTooLongStrings
        {
            get
            {
                return Convert.ToBoolean(appSettings.Get("truncateTooLongStrings"));
            }
        }

        public bool GatherStatistics 
        {
            get 
            {
                return Convert.ToBoolean(appSettings.Get("gatherStatistics"));
            }
        }

        public int CacheAbsoluteExpirationMinutes
        {
            get
            {
                return Convert.ToInt32(appSettings.Get("cacheAbsoluteExpirationMinutes"));
            }
        }
    }
}