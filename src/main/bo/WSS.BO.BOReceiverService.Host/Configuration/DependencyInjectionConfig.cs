using System;
using System.Configuration;
using Cts.Oasys.Core.SystemEnvironment;
using Ninject;
using Ninject.Activation;
using Ninject.Extensions.Factory;
using Ninject.Extensions.Interception.Infrastructure.Language;
using WSS.BO.BOReceiverService.BL.Configuration;
using WSS.BO.BOReceiverService.BL.Facade;
using WSS.BO.BOReceiverService.BL.ServiceInfoProcessing;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;
using WSS.BO.BOReceiverService.Host.Contracts;
using WSS.BO.BOReceiverService.Host.JsonSerialization;
using WSS.BO.BOReceiverService.Host.Statistics;
using WSS.BO.Common.Hosting;
using WSS.BO.Common.Hosting.Statistics;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.DataLayer.Database;

namespace WSS.BO.BOReceiverService.Host.Configuration
{
    public class DependencyInjectionConfig : BaseDependencyInjectionConfig
    {
        private readonly CountersCollection _countersDictionary;

        public DependencyInjectionConfig(CountersCollection countersDictionary)
        {
            _countersDictionary = countersDictionary;
        }

        protected override void InitOasysDbConfiguration(IKernel kernel)
        {
            kernel.Bind<IOasysDbConfiguration>().To<AppConfigBasedOasysDbConfiguration>();
        }

        protected bool IsRepository(IContext ctx)
        {
            Type baseRepositoryType = typeof (IRepository);
            return baseRepositoryType.IsAssignableFrom(ctx.Request.Service);
        }

        public override IKernel GetKernel()
        {
            var kernel = base.GetKernel();

            kernel.Bind<ISystemEnvironment>().To<LiveSystemEnvironment>();
            var debugInterceptor = new LoggingInterceptor(LoggingInterceptor.LogLevel.Debug);
            kernel.Bind<IConfiguration>().To<ServiceConfiguration>();

            // log all repo implementations
            kernel.Intercept(IsRepository).With(debugInterceptor);

            var infoInterceptor = new LoggingInterceptor(LoggingInterceptor.LogLevel.Info);
            kernel.Bind<ITransactionProcessor>().To<TransactionProcessor>().Intercept().With(infoInterceptor);
            kernel.Bind<IServiceInfoProcessor>().To<ServiceInfoProcessor>().Intercept().With(infoInterceptor);
            kernel.Bind<IProcessorsFactory>().ToFactory();

            kernel.Bind<IBusinessLogicFacade>().To<BusinessLogicFacade>().Intercept().With(infoInterceptor);

            // multiple interceptors for IDataRetriever, higher priority for logging
            var binding = kernel.Bind<IReadOnlyDataRetriever>().To<DataRetriever>();
            binding.Intercept().With(debugInterceptor).InOrder(1);

            var configuration = new ServiceConfiguration();
            var cacheExpirationMinutes = configuration.CacheAbsoluteExpirationMinutes;
            binding.Intercept().With(new CachingInterceptor(TimeSpan.FromMinutes(cacheExpirationMinutes), null)).InOrder(2);

            var bindingNonCachable = kernel.Bind<IRuntimeDataRetriever>().To<DataRetriever>();
            bindingNonCachable.Intercept().With(debugInterceptor).InOrder(1);

            var transactionProcessingStepsBinding = kernel.Bind<ITransactionProcessingSteps>().To<TransactionProcessingSteps>();
            var receiverServiceProcessingBinding = kernel.Bind<IReceiverServiceContract>().To<ReceiverServiceImplementation>();
            var serializerBinding = kernel.Bind<IJsonSerializerFacade>().To<JsonSerializerFacade>();

            transactionProcessingStepsBinding.Intercept().With(infoInterceptor);

            var gatherStatistics = configuration.GatherStatistics;
            if (gatherStatistics)
            {
                const string allTransactionsContext = "All";

                var transactionStepsInterceptor = new TransactionStepsCounterInterceptor(_countersDictionary, allTransactionsContext);
                transactionProcessingStepsBinding.Intercept().With(transactionStepsInterceptor);

                var commonOperationsInterceptor = new CounterInterceptor(_countersDictionary, allTransactionsContext);
                receiverServiceProcessingBinding.Intercept().With(commonOperationsInterceptor);
                serializerBinding.Intercept().With(commonOperationsInterceptor);
            }

            return kernel;
        }
    }
}