﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.ServiceModel.Web;
using System.Text;
using Cts.Oasys.Core.Export;
using slf4net;
using WSS.BO.BOReceiverService.BL.Facade;
using WSS.BO.BOReceiverService.BL.ServiceInfoProcessing;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.Exceptions;
using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.BOReceiverService.Host.Contracts;
using WSS.BO.BOReceiverService.Host.Hosting;
using WSS.BO.BOReceiverService.Host.JsonSerialization;
using WSS.BO.Common.Hosting.Statistics;
using WSS.BO.Model.Entity.Transactions;
using Newtonsoft.Json;

namespace WSS.BO.BOReceiverService.Host
{
    public class ReceiverServiceImplementation : IReceiverServiceContract
    {
        private const int StoreNumberLength = 3;

        private static readonly ILogger Log = LoggerFactory.GetLogger(typeof(ReceiverServiceImplementation));
        private readonly IBusinessLogicFacade _businessLogicFacade;
        private readonly IJsonSerializerFacade _jsonSerializerFacade;

        public ReceiverServiceImplementation(IBusinessLogicFacade businessLogicFacade, IJsonSerializerFacade jsonSerializerFacade)
        {
            _businessLogicFacade = businessLogicFacade;
            _jsonSerializerFacade = jsonSerializerFacade;
        }

        public Stream AddTransaction(Stream transactionJson)
        {
            return RunOperation<Transaction>("AddTransaction", InnerAddTansaction, transactionJson);
        }

        public Stream GetServiceInfo()
        {
            return RunParameterlessOperation("GetServiceInfo", InnerGetServiceInfo);
        }

        public Stream FindTransaction(string receiptBarcode)
        {
            var resultTransaction = RunParameterlessOperation("FindTransaction", () => InnerFindTransaction(receiptBarcode));
            return resultTransaction;
        }

        public Stream GetStatistics()
        {
            string textToReturn;
            try
            {
                var stats =  ReceiverServiceHost.Instance.CountersDictionary.Counters;
                var exporter = new CsvDictionaryExport<Counter>(stats, x => x.Key);
                exporter.DoubleFormat = "0.####";
                textToReturn = exporter.Export();
            }
            catch (Exception ex)
            {
                textToReturn = ex.StackTrace;
            }

            if (WebOperationContext.Current != null)
            {
                var response = WebOperationContext.Current.OutgoingResponse;
                response.ContentType = "text/plain; charset=utf-8";
            }

            return new MemoryStream(Encoding.UTF8.GetBytes(textToReturn));
        }

        public Stream ClearStatistics()
        {
            string textToReturn;
            try
            {
                ReceiverServiceHost.Instance.CountersDictionary.Clear();
                textToReturn = "Statistic was successfully cleared";
            }

            catch (Exception ex)
            {
                textToReturn = ex.StackTrace;
            }

            if (WebOperationContext.Current != null)
            {
                var response = WebOperationContext.Current.OutgoingResponse;
                response.ContentType = "text/plain; charset=utf-8";
            }

            return new MemoryStream(Encoding.UTF8.GetBytes(textToReturn));
        }

        public Stream ApplyCounters(Stream countersJson)
        {
            return RunOperation<List<Counter>>("ApplyCounters", InnerApplyCounters, countersJson);
        }

        #region Interface implementation

        private IServiceCallResult InnerAddTansaction(Transaction transaction)
        {
            string sourceTransactionId = transaction.SourceTransactionId;
            StatusDescription statusDescription;

            try
            {
                _businessLogicFacade.AddTransaction(transaction);

                statusDescription = new StatusDescription
                {
                    Status = "Successful",
                    Details = String.Format("Transaction '{0}' was added", transaction.SourceTransactionId),
                };
            }
            catch (DuplicateTransactionException)
            {
                Log.Warn("Duplicate transaction ID=[0]", sourceTransactionId);
                statusDescription = new StatusDescription
                {
                    Status = "Duplicate",
                    Details = String.Format("Transaction with ID=[{0}] already exists", sourceTransactionId)
                };
            }
            catch (ValidationException ex)
            {
                Log.Error("Validation of incoming request failed: {0}", ex.Message);
                SetupOutgoingResultParameters(HttpStatusCode.BadRequest);
                statusDescription = new StatusDescription
                {
                    Status = "Validation failed",
                    Details = ex.Message
                };
            }
            catch (EodLockException)
            {
                Log.Warn("Accept Request: Failed; Overnight Run In Progress");
                SetupOutgoingResultParameters(HttpStatusCode.ServiceUnavailable);
                statusDescription = new StatusDescription
                {
                    Status = "EoD lock",
                    Details =
                        String.Format("Unable to process transaction with ID=[{0}]: Overnight Run In Progress",
                            sourceTransactionId)
                };
            }
            catch (PickupException)
            {
                Log.Warn("Accept Request: Failed; Pickup Process For Cashier Is In Progress");
                SetupOutgoingResultParameters(HttpStatusCode.ServiceUnavailable);
                statusDescription = new StatusDescription
                {
                    Status = "Pickup in progress",
                    Details =
                        String.Format("Unable to process transaction with ID=[{0}]: Pickup Process For Cashier Is In Progress",
                            sourceTransactionId)
                };
            }
            catch (BankingException)
            {
                Log.Warn("Accept Request: Failed; Banking In Progress");
                SetupOutgoingResultParameters(HttpStatusCode.ServiceUnavailable);
                statusDescription = new StatusDescription
                {
                    Status = "Banking lock",
                    Details =
                        String.Format("Unable to process transaction with ID=[{0}]: Banking Is In Progress",
                            sourceTransactionId)
                };
            }

            return new ServiceCallResult(statusDescription);
        }

        private IServiceCallResult InnerGetServiceInfo()
        {
            ServiceInfo serviceInfo = _businessLogicFacade.GetServiceInfo();
            return new ServiceCallResult<ServiceInfo>(serviceInfo);
        }

        private IServiceCallResult InnerFindTransaction(string receiptBarcode)
        {
            StatusDescription statusDescription;

            try
            {
                RetailTransaction tran = _businessLogicFacade.FindTransaction(receiptBarcode, receiptBarcode.Substring(0, StoreNumberLength));
                return new ServiceCallResult<RetailTransaction>(tran);
            }
            catch (TransactionNotFoundException)
            {
                Log.Warn("Unable to find transaction with given filters");
                SetupOutgoingResultParameters(HttpStatusCode.NotFound);
                statusDescription = new StatusDescription
                {
                    Status = "Unable to find transaction",
                    Details = String.Format("Unable to find transaction with given Receipt Barcode [{0}]", receiptBarcode)
                };

            }
            catch (ValidationException ex)
            {
                Log.Error("Validation of incoming request failed: {0}", ex.Message);
                SetupOutgoingResultParameters(HttpStatusCode.BadRequest);
                statusDescription = new StatusDescription
                {
                    Status = "Validation failed",
                    Details = ex.Message
                };
            }
            return new ServiceCallResult<StatusDescription>(statusDescription);
        }

        private IServiceCallResult InnerApplyCounters(List<Counter> counters)
        {
            StatusDescription statusDescription;

            try
            {
                foreach (var counterToApply in counters)
                {
                    ReceiverServiceHost.Instance.CountersDictionary.ApplyUpdates(counterToApply);
                }

                statusDescription = new StatusDescription
                {
                    Status = "Successful",
                    Details = "Counters were successfully applied"
                };
            }
            catch (Exception ex)
            {
                Log.Warn("Counters were not applied");
                statusDescription = new StatusDescription
                {
                    Status = "Counters were not applied",
                    Details = String.Format("Perfomance counters were not applied: {0}", ex.StackTrace)
                };
            }

            return new ServiceCallResult(statusDescription);
        }

        #endregion

        #region Common operation processing

        private Stream RunOperation<TRequest>(string name, Func<TRequest, IServiceCallResult> action, Stream requestStream)
            where TRequest : class
        {
            var methodName = String.Format("{0}.{1}", GetType(), name);

            Log.Info("{0} is called. Incoming request will be logged below.", methodName);

            return RunCommonOperationAction(name, () => DeserializeJsonAndRunAction<TRequest>(action, requestStream));
        }

        private IServiceCallResult DeserializeJsonAndRunAction<TRequest>(Func<TRequest, IServiceCallResult> action, Stream requestStream) where TRequest : class
        {
            TRequest request;

            try
            {
                request = _jsonSerializerFacade.DeserializeRequestFromJson<TRequest>(requestStream);
            }
            catch (JsonReaderException ex)
            {
                SetupOutgoingResultParameters(HttpStatusCode.BadRequest);
                var status = new StatusDescription
                {
                    Status = "Incoming Json syntax is invalid",
                    Details = ex.Message
                };

                return new ServiceCallResult(status);
            }
            catch (JsonSerializationException ex)
            {
                SetupOutgoingResultParameters(HttpStatusCode.BadRequest);
                var status = new StatusDescription
                {
                    Status = "Incoming Json has a value that is not convertible to the model field",
                    Details = ex.Message
                };

                return new ServiceCallResult(status);
            }
            
            return action(request);
        }

        private Stream RunParameterlessOperation(string name, Func<IServiceCallResult> action)
        {
            var methodName = String.Format("{0}.{1}", GetType(), name);
            Log.Info("{0} is called.", methodName);

            return RunCommonOperationAction(name, action);
        }

        private Stream RunCommonOperationAction(string methodName, Func<IServiceCallResult> action)
        {
            IServiceCallResult result;
            try
            {
                result = action();
            }
            catch (Exception ex)
            {
                var status = ProcessUnhandledExceptionAndPrepareStatusDescription(ex,
                    "An error occured while processing incoming request");
                result = new ServiceCallResult(status);
            }

            if (WebOperationContext.Current != null)
            {
                var response = WebOperationContext.Current.OutgoingResponse;
                response.ContentType = "application/json; charset=utf-8";
            }

            var resultJson = _jsonSerializerFacade.SerializeResponseToJson(result);
            Log.Info("{0} is completed.\t\nResult Json:\t\n {1}", methodName, resultJson);

            return new MemoryStream(Encoding.UTF8.GetBytes(resultJson));
        }

        private StatusDescription ProcessUnhandledExceptionAndPrepareStatusDescription(Exception ex, string endUserMessage)
        {
            SetupOutgoingResultParameters(HttpStatusCode.InternalServerError);

            const string advancedFormat = "{0}: {1}";

            // Depending on flag log either exception or it's reference key
            object exReference;
            if (Convert.ToBoolean(ConfigurationManager.AppSettings.Get("returnErrorInfoToClient")))
            {
                exReference = ex;
                Log.Error(ex, endUserMessage);
            }
            else
            {
                exReference = Guid.NewGuid();
                Log.Error(ex, advancedFormat, endUserMessage, exReference);
            }

            var errorDescription = new StatusDescription
            {
                Status = "Failed",
                Details = String.Format(advancedFormat, endUserMessage, exReference)
            };

            return errorDescription;
        }

        private static void SetupOutgoingResultParameters(HttpStatusCode code)
        {
            if (WebOperationContext.Current != null)
            {
                var response = WebOperationContext.Current.OutgoingResponse;
                response.StatusCode = code;
            }
        }

        #endregion
    }
}