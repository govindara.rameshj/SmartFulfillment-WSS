﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace WSS.BO.BOReceiverService.Host.JsonSerialization
{
    public class CustomContractResolver : CamelCasePropertyNamesContractResolver
    {
        private readonly Required itemRequiredMode;

        public CustomContractResolver(Required itemRequiredMode)
        {
            this.itemRequiredMode = itemRequiredMode;
        }

        protected override JsonObjectContract CreateObjectContract(Type objectType)
        {
            var contract = base.CreateObjectContract(objectType);
            contract.ItemRequired = itemRequiredMode;
            return contract;
        }
    }
}