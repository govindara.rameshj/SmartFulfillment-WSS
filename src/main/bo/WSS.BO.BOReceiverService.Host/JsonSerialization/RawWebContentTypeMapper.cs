﻿using System.ServiceModel.Channels;

namespace WSS.BO.BOReceiverService.Host.JsonSerialization
{
    public class RawWebContentTypeMapper : WebContentTypeMapper
    {
        public override WebContentFormat GetMessageFormatForContentType(string contentType)
        {
            return WebContentFormat.Raw;
        }
    }
}