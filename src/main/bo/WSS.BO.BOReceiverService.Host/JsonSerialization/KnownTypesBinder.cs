﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WSS.BO.BOReceiverService.Host.JsonSerialization
{
    public class KnownTypesBinder : SerializationBinder
    {
        private readonly IDictionary<string, Type> knownTypes = new Dictionary<string, Type>();

        public override Type BindToType(string assemblyName, string typeName)
        {
            Type result;
            bool found = knownTypes.TryGetValue(typeName, out result);
            if (!found)
            {
                throw new ApplicationException(String.Format("KnownTypesBinder did not found type [{0}] in the list of known types.", typeName));
            }
            return result;
        }

        internal void AddType<T>()
        {
            AddType(typeof(T));
        }

        internal void AddType(Type type)
        {
            knownTypes.Add(type.Name, type);
        }
    }
}