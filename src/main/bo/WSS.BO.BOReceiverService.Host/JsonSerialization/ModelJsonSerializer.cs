﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using WSS.BO.Common.Hosting.Statistics;
using WSS.BO.Model.Metainfo;

namespace WSS.BO.BOReceiverService.Host.JsonSerialization
{
    public class ModelJsonSerializer
    {
        private readonly JsonSerializer innerSerializer;
        private readonly JsonSerializer innerDeserializer;

        public ModelJsonSerializer(MissingMemberHandling missingMemberHandling, Required itemRequiredMode)
        {
            KnownTypesBinder binder = new KnownTypesBinder();
            AddModelClassesToBinder(binder);

            innerDeserializer = new JsonSerializer
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,

                TypeNameHandling = TypeNameHandling.Objects, // to handle $type property
                Binder = binder, // to resolve non-full type names from $type property value
                MissingMemberHandling = missingMemberHandling,
                ContractResolver = new CustomContractResolver(itemRequiredMode),
            };

            innerSerializer = new JsonSerializer
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                DateTimeZoneHandling = DateTimeZoneHandling.Local, // all dates which are to be serialized are local as they usually come from Oasys.

                TypeNameHandling = TypeNameHandling.None,
                MissingMemberHandling = missingMemberHandling,
                ContractResolver = new TypeNameContractResolver(itemRequiredMode), // to serialize non-full type names into $type property
                Converters = { new StringEnumConverter() }
            };
        }

        public T Deserialize<T>(TextReader textReader)
        {
            return (T)Deserialize(textReader, typeof(T));
        }

        public object Deserialize(TextReader textReader, Type modelClassType)
        {
            using (var reader = new JsonTextReader(textReader))
            {
                return innerDeserializer.Deserialize(reader, modelClassType);
            }
        }

        private static void AddModelClassesToBinder(KnownTypesBinder binder)
        {
            Assembly modelAssembly = typeof(ModelMetaInfo).Assembly;
            var types = modelAssembly.GetTypes().Where(t => t.IsClass && t.IsPublic && !t.IsAbstract);
            foreach (var type in types)
            {
                binder.AddType(type);
            }
            binder.AddType(typeof(Counter));
        }

        public void Serialize(TextWriter writer, object obj)
        {
            using (JsonWriter jsonWriter = new JsonTextWriter(writer))
            {
                jsonWriter.Formatting = Formatting.Indented;
                innerSerializer.Serialize(jsonWriter, obj);
            }
        }
    }
}