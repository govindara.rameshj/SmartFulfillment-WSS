﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace WSS.BO.BOReceiverService.Host.JsonSerialization
{
    public class TypeNameContractResolver : CustomContractResolver
    {
        static readonly JsonProperty Property = new JsonProperty
        {
            PropertyName = "$type",
            Readable = true,
            Ignored = false,
            PropertyType = typeof(string),
            ValueProvider = new TypeNameValueProvider()
        };

        public TypeNameContractResolver(Required itemRequiredMode) : base(itemRequiredMode)
        {
        }

        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            var result = base.CreateProperties(type, memberSerialization);
            result.Insert(0, Property);
            return result;
        }
    }
}