﻿using System.Configuration;
using System.IO;
using System.Text;
using Cts.Oasys.Core.Helpers;
using Cts.Oasys.Core.Net4Replacements;
using Newtonsoft.Json;
using slf4net;
using WSS.BO.BOReceiverService.Host.Contracts;

namespace WSS.BO.BOReceiverService.Host.JsonSerialization
{
    public class JsonSerializerFacade : IJsonSerializerFacade
    {
        private static readonly ILogger Log = LoggerFactory.GetLogger(typeof (JsonSerializerFacade));

        #region Json serialization

        private static readonly Lazy<ModelJsonSerializer> JsonModelSerializer =
            new Lazy<ModelJsonSerializer>(CreateModelJsonSerializer);

        private static ModelJsonSerializer CreateModelJsonSerializer()
        {
            return new ModelJsonSerializer(
                EnumParser.Parse<MissingMemberHandling>(
                    ConfigurationManager.AppSettings.Get("IncomingRequestJsonMissingMemberHandling")),
                EnumParser.Parse<Required>(ConfigurationManager.AppSettings.Get("IncomingRequestJsonItemRequired")));
        }

        public T DeserializeRequestFromJson<T>(Stream incomingStream)
        {
            string incomingJson;

            using (var reader = new StreamReader(incomingStream))
            {
                incomingJson = reader.ReadToEnd();
            }

            Log.Info("Incoming request:\t\n {0}", incomingJson);

            using (var reader = new StringReader(incomingJson))
            {
                return JsonModelSerializer.Value.Deserialize<T>(reader);
            }
        }

        public string SerializeResponseToJson(IServiceCallResult result)
        {
            StringBuilder sb = new StringBuilder();
            using (var writer = new StringWriter(sb))
            {
                JsonModelSerializer.Value.Serialize(writer, result.GetResultToSerialize());
            }
            return sb.ToString();
        }

        #endregion
    }
}