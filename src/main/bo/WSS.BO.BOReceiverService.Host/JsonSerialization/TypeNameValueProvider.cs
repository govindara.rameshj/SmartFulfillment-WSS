﻿using System;
using Newtonsoft.Json.Serialization;

namespace WSS.BO.BOReceiverService.Host.JsonSerialization
{
    public class TypeNameValueProvider : IValueProvider
    {
        public void SetValue(object target, object value)
        {
            throw new InvalidOperationException("THis method should not be called as TypeNameValueProvider is not supposed to be used for deserialization.");
        }

        public object GetValue(object target)
        {
            return target.GetType().Name;
        }
    }
}