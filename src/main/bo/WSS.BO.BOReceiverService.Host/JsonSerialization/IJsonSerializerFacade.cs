﻿using System.IO;
using WSS.BO.BOReceiverService.Host.Contracts;

namespace WSS.BO.BOReceiverService.Host.JsonSerialization
{
    public interface IJsonSerializerFacade
    {
        T DeserializeRequestFromJson<T>(Stream incomingStream);
        string SerializeResponseToJson(IServiceCallResult result);
    }
}