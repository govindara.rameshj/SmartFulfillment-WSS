﻿using System.IO;
using System.ServiceModel;
using WSS.BO.BOReceiverService.Host.Contracts;

namespace WSS.BO.BOReceiverService.Host
{
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)] // TODO: Minor. try to move it into web.config
    public class ReceiverService : IReceiverServiceContract
    {
        private readonly IReceiverServiceContract _implementation;

        public ReceiverService(IReceiverServiceContract implementation)
        {
            _implementation = implementation;
        }

        #region Interface

        public Stream AddTransaction(Stream transactionJson)
        {
            return _implementation.AddTransaction(transactionJson);
        }

        public Stream GetServiceInfo()
        {
            return _implementation.GetServiceInfo();
        }

        public Stream FindTransaction(string receiptBarcode)
        {
            return _implementation.FindTransaction(receiptBarcode);
        }

        public Stream GetStatistics()
        {
            return _implementation.GetStatistics();
        }

        public Stream ClearStatistics()
        {
            return _implementation.ClearStatistics();
        }

        public Stream ApplyCounters(Stream countersJson)
        {
            return _implementation.ApplyCounters(countersJson);
        }

        #endregion
    }
}
