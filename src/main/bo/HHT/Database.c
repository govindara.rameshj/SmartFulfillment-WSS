//---------------------------------------------------------------------------
// Application: WixStep.nlm. Hand-Held Terminal (HHT) Server for Novell Server
//							and Socket Services. Developed for Wickes.
// Project: WickesRFDeviceNLMSourceEdit - Developed with MS Visual Studio
//					2003 C++ and compiled with Watcom C/C++ and Watcom linker V 11.0
//					using NMAKE.
// --------------------------------------------------------------------------
// Module:Database.c -Routines used by lApp() to access SysDat, SaCode, StkMas,
//										EanMas,StkAdj, StkLog, HhtHdr, and HhtDet tables.
//
//	The names of these functions are a little misleading and the int return
//	value is usually not used. Look for how they are used in app.c to find out
//	what they really do.
//----------------------------------------------------------------------------
#undef _THIS_SOURCE_HAS_THE_DATA_
#include "wixstep.h"
/***************************************************************************
Function:    Read System Dates Database
Description: Get the System dates record
Passed:      User Number
Return:      0  = Failed,  1  = Success,
LastInput:  "0" = Failed, "1" = Success,
****************************************************************************/
/*
int iReadSysDatDatabase( int sessionNumber )
{
BTI_CHAR keyBuf[21];
  memset(&sysdat, 0, sizeof(sysdat) );
  strcpy(keyBuf, "01");
  if (DBAccessTableData(&dbtSysDat,B_GET_EQUAL,keyBuf,0)==B_NO_ERROR){
		GetDBCharFieldToStr(sUser[i].sdFkey, sysdat.fkey, sizeof(sysdat.fkey));
		GetDBDecFieldToStr(sUser[i].sdTodw, sysdat.todw, sizeof(sysdat.todw),0);
		GetDBDecFieldToStr(sUser[i].sdTmdw, sysdat.tmdw, sizeof(sysdat.tmdw),0);
    strcpy(sUser[i].pcLastInput, "1");
   return(1);
	}
  strcpy(sUser[i].pcLastInput, "0");
  return(0);
return(0);
}
*/
/***************************************************************************
Function:	 Read SACODE Fields
Description: This function reads the SACODE record and sets the user
				 fields.
Passed:		 SACODE record
				 User Number
Return:		 None
****************************************************************************/

void vReadSaCode(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	cn.SQLGetColumn(1, SQL_C_CHAR, user->pcSaNumb, 3);
	cn.SQLGetColumn(2, SQL_C_CHAR, user->pcSaDesc, 26);
	cn.SQLGetColumn(3, SQL_C_CHAR, user->pcSaType, 2);
	cn.SQLGetColumn(4, SQL_C_CHAR, user->pcSaSign, 2);
	cn.SQLGetColumn(5, SQL_C_CHAR, user->pcSaSecl, 2);
	cn.SQLGetColumn(6, SQL_C_SHORT,&user->pcSaImdn, 1);
	cn.SQLGetColumn(7, SQL_C_SHORT,&user->pcSaIwtf, 1);
}

/***************************************************************************
Function:    First SACODE Database
Description: Position on SACODE at BOF
Passed:      User Number
Return:      0  = Failed,  1  = Success,
LastInput:   0   = Failed (or EOF),  1  = Success (Imdn or Iwtf)
             2   = Success (but not Imdn or Iwtf),  3 = EOF
****************************************************************************/
/*
int iFirstSaCodeDatabase( int i )
{
 if (DBAccessTableData(&dbtSaCode,B_GET_FIRST,NULL,0)==B_NO_ERROR) {
    strcpy(sUser[i].pcLastInput, "1");
    return(1);
  }
  strcpy(sUser[i].pcLastInput, "0");
  return(0);
return(0);
}
*/
/***************************************************************************
Function:    Read next SACODE Database
Description: Read next record in the SACODE btrieve database file
Passed:      User Number
Return:      0  = Failed,  1  = Success,
LastInput:   0   = Failed (or EOF),  1  = Success (Imdn or Iwtf)
             2   = Success (but not Imdn or Iwtf),  3 = EOF
****************************************************************************/
/*
int iReadSaCodeDatabase( int i )
{
BTI_SINT iDataBaseStatus=B_NO_ERROR;

  iDataBaseStatus=DBAccessTableData(&dbtSaCode,B_GET_NEXT,NULL,0);
  if (iDataBaseStatus == B_END_OF_FILE) {
    strcpy(sUser[i].pcLastInput, "3");
    return(0);
  }
  if (iDataBaseStatus==B_NO_ERROR) {
    vReadSaCode(&sacode, i);
		if ((sUser[i].pcSaImdn) || (sUser[i].pcSaIwtf)) {
			DBstrncpy(sUser[i].pcSaStart,sUser[i].pcSaNumb,sizeof(sUser[i].pcSaStart));
      strcpy(sUser[i].pcLastInput, "1");
      return(1) ;
      }
    strcpy(sUser[i].pcLastInput, "1");
    return(1) ;
    }
  strcpy(sUser[i].pcLastInput, "0");
  return(0);
return(0);
}
*/
/***************************************************************************
Function:    Read Prior SACODE Database
Description: Read prior record in the SACODE btrieve database file
Passed:      User Number
Return:          0   = Failed ,  1  = Success (Imdn or Iwtf)
             2   = Success (but not Imdn or Iwtf), EOF
****************************************************************************/
int iPriorSaCodeDatabase(int sessionNumber)
{
	return iSeekSaCodeDatabase(sessionNumber, 3);
/*
	USER_TYPE *user = &sUser[sessionNumber];

	if (cnRS[sessionNumber].DoSQLFetch(SQL_FETCH_PRIOR)) {
	  vReadSaCode(sessionNumber);
		if ((user->pcSaImdn) || (user->pcSaIwtf)) {
			//strcpy(user->pcLastInput, "1");
			return(1);
			}
		//strcpy(user->pcLastInput, "2");
		return(2);
    }
  strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
  return(0);
	*/
}
/***************************************************************************
Function:    Seek SACODE Database
Description: Seek the Sacode for the specific Key
Passed:      0 = Begin Search
             1 = Get Exact
						 2 = Get Next
						 3 = Get Prior
LastInput:   "0"  = Key Value Not found,  "1"  = Key Value found,
Return:       0   = Failed (or EOF),  1  = Success (Imdn or Iwtf)
              2   = Success (but not Imdn or Iwtf)
****************************************************************************/
int iSeekSaCodeDatabase(int sessionNumber, int iOption)
{
	USER_TYPE *user = &sUser[sessionNumber];

	char cmd[255];
	char pcSaStart[2];
  // Setup The Fkey Value
  memset(user->pcSaKey, 0,sizeof(user->pcSaKey));
  memset(pcSaStart, 0, sizeof(pcSaStart));
	strcpy_s(cmd, sizeof(cmd), "SELECT TOP 1 NUMB, DESCR, TYPE, SIGN, SECL, IMDN, IWTF from sacode where NUMB ");
	switch (iOption) {
		case 0:
			DBstrncpy(user->pcSaKey, user->pcSaStart,sizeof(user->pcSaKey));
			strcat_s(cmd, sizeof(cmd), ">= ");
			cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSaKey);
			strcat_s(cmd, sizeof(cmd), " order by NUMB ASC");
			break;
		case 1:
	    DBstrncpy(user->pcSaKey, user->pcLastInput,sizeof(user->pcSaKey));
			strcat_s(cmd, sizeof(cmd), "= ");
			cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSaKey);
			strcat_s(cmd, sizeof(cmd), " order by NUMB ASC");
			break;
		case 2:
			DBstrncpy(user->pcSaKey, user->pcSaNumb,sizeof(user->pcSaKey));
			strcat_s(cmd, sizeof(cmd), "> ");
			cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSaKey);
			strcat_s(cmd, sizeof(cmd), " order by NUMB ASC");
			break;
		case 3:
			DBstrncpy(user->pcSaKey, user->pcSaNumb,sizeof(user->pcSaKey));
			strcat_s(cmd, sizeof(cmd), "< ");
			cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSaKey);
			strcat_s(cmd, sizeof(cmd), " order by NUMB DESC");
			break;
	}
	if (cn.DoSQLCommand(cmd, SQL_SCROLLABLE)) {
		if (cn.DoSQLFetch(SQL_FETCH_NEXT)) {
	    vReadSaCode(sessionNumber);
			if ((user->pcSaImdn) || (user->pcSaIwtf)) {
				strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
				return(1);
			}
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "2");
			return(2);
		}
	}
  strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
  return(0);
}
/***************************************************************************
Function:    Get Next SACODE Database
Description: Get the next SaCode record in the SACODE file
                         for MarkDown and Write Off Reasons
Passed:      User Number
Return:      0 = Failed, 1 = Success(mdwn or iwtf), 2 = (not imdn or iwtdf)
                         3 = EOF
****************************************************************************/
int iGetNextSaCodeDatabase(int sessionNumber)
{
	return iSeekSaCodeDatabase(sessionNumber, 2);
	/*
	USER_TYPE *user = &sUser[sessionNumber];

	if (cnRS[sessionNumber].DoSQLFetch(SQL_FETCH_NEXT)) {
		vReadSaCode(sessionNumber);
		if ((user->pcSaImdn) || (user->pcSaIwtf)) {
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
			return(1);
		}
		strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "2");
		return(2);
	}
	strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
	return(0);
	*/
}
/***************************************************************************
Function:	 Read STKMAS Fields
Description: This function reads the STKMAS record and sets the user
				 fields.
Passed:		 STKMAS record
				 User Number
Return:		 None
****************************************************************************/
void vReadStkMas(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	char INON[3];
	cn.SQLGetColumn( 1, SQL_C_CHAR, user->pcSkun, sizeof(user->pcSkun));
	cn.SQLGetColumn( 2, SQL_C_CHAR, user->pcDesc, sizeof(user->pcDesc));
	cn.SQLGetColumn( 3, SQL_C_CHAR, user->pcProd, sizeof(user->pcProd));
	cn.SQLGetColumn( 4, SQL_C_CHAR, user->pcSupp, sizeof(user->pcSupp));
	cn.SQLGetColumn( 5, SQL_C_CHAR, user->imPric, sizeof(user->imPric));
	strcpy_s(user->pcPric, sizeof(user->pcPric), user->imPric);
	cn.SQLGetColumn( 6, SQL_C_CHAR, user->pcCost, sizeof(user->pcCost));
	cn.SQLGetColumn( 7, SQL_C_CHAR, user->imOnha, sizeof(user->imOnha));
	cn.SQLGetColumn( 8, SQL_C_CHAR, user->pcMini, sizeof(user->pcMini));
	cn.SQLGetColumn( 9, SQL_C_CHAR, user->pcMaxi, sizeof(user->pcMaxi));
	cn.SQLGetColumn(10, SQL_C_CHAR, user->pcIsta, sizeof(user->pcIsta));
	cn.SQLGetColumn(11, SQL_C_CHAR, user->pcIobs, sizeof(user->pcIobs));
	cn.SQLGetColumn(12, SQL_C_CHAR, user->pcIdel, sizeof(user->pcIdel));
	cn.SQLGetColumn(13, SQL_C_CHAR, user->imNoor, sizeof(user->imNoor));
	cn.SQLGetColumn(14, SQL_C_CHAR, INON, sizeof(INON));
	user->pcImStatus[0]=0;
	if (INON[0] != '0')
	 	strcpy_s(user->pcImStatus, sizeof(user->pcImStatus),"Status: Non Stocked");
	if (user->pcIobs[0] != '0')
	 	strcpy_s(user->pcImStatus, sizeof(user->pcImStatus),"Status: Obsolete");
	if (user->pcIdel[0] != '0')
	 	strcpy_s(user->pcImStatus, sizeof(user->pcImStatus),"Status: Deleted");
	if (user->imNoor[0] != '0')
	 	strcpy_s(user->pcImStatus, sizeof(user->pcImStatus),"Status:Non Orderable");
	cn.SQLGetColumn(15, SQL_C_CHAR, user->pcLabn, sizeof(user->pcLabn));
	cn.SQLGetColumn(16, SQL_C_CHAR, user->pcLabm, sizeof(user->pcLabm));
	cn.SQLGetColumn(17, SQL_C_CHAR, user->pcLabl, sizeof(user->pcLabl));
	cn.SQLGetColumn(18, SQL_C_CHAR, user->imRetq, sizeof(user->imRetq));
	cn.SQLGetColumn(19, SQL_C_CHAR, user->imMdnq, sizeof(user->imMdnq));
	cn.SQLGetColumn(20, SQL_C_CHAR, user->imWtfq, sizeof(user->imWtfq));
	cn.SQLGetColumn(21, SQL_C_CHAR, user->imEqpm, sizeof(user->imEqpm));
	cn.SQLGetColumn(22, SQL_C_CHAR, user->imEqpu, sizeof(user->imEqpu));
	cn.SQLGetColumn(23, SQL_C_BIT, user->imAapc, 1);
	cn.SQLGetColumn(24, SQL_C_CHAR, user->imOnor, sizeof(user->imOnor));
	vGetEquivPrice(user->pcPric, sessionNumber);
}

/***************************************************************************
Function:    Read next STKMAS Database
Description: Read next record in the stock btrieve database file
Passed:      User Number
Return:      0 = Failed, 1 = Success,
LastInput:  "0" = Failed, else a "1" = Success,
****************************************************************************/
/*
int iReadStkMasDatabase( int i )
{
BTI_CHAR keyBuf[21];
	BTI_SINT iDataBaseStatus = B_NO_ERROR;

  // GET THE RECORD WITH GET_EQUAL AND UPDATE IT
	memset( &stkmas, 0, sizeof(stkmas) );
	iDataBaseStatus=DBAccessTableData(&dbtStkMas,B_GET_NEXT,keyBuf,0);
  if ((iDataBaseStatus==B_END_OF_FILE) || (iDataBaseStatus == B_POSITION_NOT_SET)) {
		iDataBaseStatus=DBAccessTableData(&dbtStkMas,B_GET_FIRST,keyBuf,0);
  }
  if ( iDataBaseStatus == B_NO_ERROR ) {
		vReadStkMas(&stkmas, i);
    strcpy(sUser[i].pcLastInput, "1");
    return(1);
		}
	strcpy(sUser[i].pcLastInput, "0");
	return(0);
return(0);
}
*/

/***************************************************************************
Function:    Seek STKMAS Database
Description: Seek a record in the Stock btrieve database file
             Also reads the database if SKU found.
Passed:      User Number
             Options 0 = Use Last Input, 1 = Use current SKU (as Key)
             Options 2.Current SKU (key) & Lock Record for Update
Return:      0 = Failed, 1 = Success,
LastInput:  "0" = Failed, "1" = Success, 2 -> Locked Record
****************************************************************************/
int iSeekStkMasDatabase(int sessionNumber, int iOption)
{
	USER_TYPE *user = &sUser[sessionNumber];

	char s[12], cmd[255] = 
		"select SKUN, DESCR, PROD, SUPP, PRIC, COST, ONHA, MINI, MAXI, ISTA, IOBS, IDEL, NOOR, INON, LABN, LABM, LABL, RETQ, MDNQ, WTFQ, EQPM, EQPU, AAPC, ONOR from stkmas where SKUN = ";
  if (iOption == 0) {
    strcpy_s(s, sizeof(s), ZeroPadField(user->pcLastInput, 6));
  } else {
		strcpy_s(s, sizeof(s), ZeroPadField(user->pcSkun, 6));
	}
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), s);
	if (cn.DoSQLCommand(cmd)) {
		if (cn.DoSQLFetch()) {
			vReadStkMas(sessionNumber);
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
			return(1);
		}
	}
	if (iOption == 2) {
    strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "2");
	} else {
    strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
	}
  return(0);
}
/***************************************************************************
Function:    Read next EANMAS Database
Description: Read next record in the EAN lookup btrieve database file
Passed:      User Number
Return:      0 = Failed, 1 = Success,
LastInput:  "0" = Failed, else a "SKU" number = Success,
****************************************************************************/
bool iReadEanMasDatabase(int sessionNumber)
{
	// not supported
  return false;
}
/***************************************************************************
Function:    Seek EANMAS Database
Description: Seek a record in the EAN lookup btrieve database file
             This function also checks for Wickes Velocity codes
Passed:      User Number
Return:      0 = Failed, 1 = Success,
LastInput:  "0" = Failed, else a "SKU" number = Success,
****************************************************************************/
int iSeekEanMasDatabase(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	char cmd[255] = "select SKUN from EANMAS where NUMB = ";
  char pcSeek[20];
  //char pcSkun[7];
  strcpy_s(pcSeek, sizeof(pcSeek), user->pcLastInput);
  // First check if SKU is infact an EAN
	if ((strlen(pcSeek) > 6) || (strlen(pcSeek) < 5)) {
    if ((pcSeek[0] == '2') && (strlen(pcSeek) == 8)) { // Look for Velocity Code
      sprintf_s(user->pcLastInput, sizeof(user->pcLastInput),"%6.6s",pcSeek+1);
			return(true);
		} else { // If not a Velocity code, it could be an EAN
			cn.SQLQueryAddVarChar(cmd, sizeof(cmd), ZeroPadField(pcSeek,16));
			if (cn.DoSQLCommand(cmd)) {
				if (cn.DoSQLFetch()) {
					cn.SQLGetColumn(1, SQL_C_CHAR, user->pcLastInput, 7);
					return(true);
				}
			}
		}
	}
	if (strlen(pcSeek) <= 6) {
	  strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), ZeroPadField(pcSeek,6));
	  return(true);
	}
  strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
  return false;
}
/***************************************************************************
Function:	 Read STKADJ Fields
Description: This function reads the STKADJ record and sets the user
				 fields.
Passed:		 STKADJ record
				 User Number
Return:		 None
****************************************************************************/

void vReadStkAdj(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

//char *s="select DATE1, CODE, SKUN, SEQN from stkadj";
	SQL_DATE_STRUCT d;

	cn.SQLGetColumn(1, SQL_C_TYPE_DATE, &d, sizeof(d));
	sprintf_s(user->pcAaDate,sizeof(user->pcAaDate),"%2d/%2d/%4d", d.day, d.month, d.year);

	cn.SQLGetColumn(2, SQL_C_CHAR, user->pcAaCode,3);
	cn.SQLGetColumn(3, SQL_C_CHAR, user->pcAaSkun,7);
	cn.SQLGetColumn(4, SQL_C_CHAR, user->pcAaSeqn,3);
}


/***************************************************************************
Function:    Seek STKADJ Database
Description: Seek the Stk Adj for the Date/Code/Skun
Passed:      User Number
Return:      0   = Failed ,  1  = Success,  2 = Key Not Found
****************************************************************************/
int iSeekStkAdjDatabase(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	char cmd[255];
	char s[21] = "";

	strcpy_s(cmd, sizeof(cmd), "SELECT DATE1, CODE, SKUN, SEQN FROM stkadj WHERE DATE1=");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), cn.SQLGetCurrentDateString(s, sizeof(s)));
	strcat_s(cmd, sizeof(cmd), " AND CODE = ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSaNumb);
	strcat_s(cmd, sizeof(cmd), " AND SKUN = ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSkun);
	strcat_s(cmd, sizeof(cmd), " ORDER BY SEQN ");

	if (cn.DoSQLCommand(cmd)) {
		if (cn.DoSQLFetch()) {
			vReadStkAdj(sessionNumber);
	    user->bAdjExists = TRUE;
			strcpy_s(user->pcLastSeqn, sizeof(user->pcLastSeqn), user->pcAaSeqn);	// save seq for next stkadj
			return (1); // Found a record
		}
		return (0); // No record
	}
return (-1); // DB error
}
/***************************************************************************
Function:    Get Next STKADJ Database
Description: Get the next StkAdj record in the STKADJ file
             ( Used to get the next seq. no for Date\Code\SKU index )
Passed:      User Number
Return:      0 = Failed, 1 = Success( found key ), 2 = (no matching key )
                         3 = EOF
****************************************************************************/
int iGetNextStkAdjDatabase(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	if (cn.DoSQLFetch(SQL_FETCH_NEXT)) {
	  vReadStkAdj(sessionNumber);
      user->bAdjExists = TRUE;
			strcpy_s(user->pcLastSeqn, sizeof(user->pcLastSeqn), user->pcAaSeqn);	// save seq for next stkadj
      return(1); // Found Matching Key fields
	}
  strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
  return(0); // end of list
}
/****************************************************************************
Function   : Insert a STKADJ record                                              *W2.06*
Description: Insert a Stock Adjustment record in the Database file
Passed:      User Number
Return:      0  = Failed, 1  = Success
LastInput:   0  = Failed, 1  = Success; 2 = Duplicate Entry
****************************************************************************/
bool bInsertStkAdjDatabase(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	int  workSeqn;
	char s[21] = "";
	char cmd[511] = 
		"INSERT INTO StkAdj (DATE1, CODE, SKUN, SEQN, INIT, DEPT, SSTK, QUAN, PRIC, COST, COMM, TYPE, TSKU, TVAL, INFO, DRLN, RCOD, MOWT, WAUT) VALUES (";

	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), cn.SQLGetCurrentDateString(s, sizeof(s))); // DATE1
	strcat_s(cmd, sizeof(cmd), ", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSaNumb);
	strcat_s(cmd, sizeof(cmd), ", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSkun);
	strcat_s(cmd, sizeof(cmd), ", ");

	workSeqn = atoi(user->pcLastSeqn);
	if (user->bAdjExists) {
		workSeqn++;
	}
  sprintf_s(user->pcAaSeqn, sizeof(user->pcAaSeqn), "%02d", workSeqn);
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcAaSeqn);
	strcat_s(cmd, sizeof(cmd), ", ");

	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), "HHT  "); //INIT
	strcat_s(cmd, sizeof(cmd), ", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), "00");    //DEPT
	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->imOnha);
	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->pcAdjQty);
	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->pcPric);
	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->pcCost);
	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), "0");                   //COMM
	strcat_s(cmd, sizeof(cmd), ", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSaType);
	strcat_s(cmd, sizeof(cmd), ", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), "000000"); //TSKU
	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), "0");                   //TVAL
	strcat_s(cmd, sizeof(cmd), ", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), "");       //INFO
	strcat_s(cmd, sizeof(cmd), ", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), "000000"); //DRLN
	strcat_s(cmd, sizeof(cmd), ", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), "");       //RCOD
	strcat_s(cmd, sizeof(cmd), ", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSaIwtf ? "W" : (user->pcSaImdn ? "M" : " "));    // MOUT
	strcat_s(cmd, sizeof(cmd), ", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), "000"); //WAUT
	strcat_s(cmd, sizeof(cmd), ")");

	if (cn.DoSQLCommand(cmd)) {
		strcpy_s(user->pcLastInput, sizeof(user->pcLastInput),"1");
		return(true); // Insert Passed
	}
  strcpy_s(user->pcLastInput, sizeof(user->pcLastInput),"0");
  return(false);
}

/***************************************************************************
Function:	 Read STKLOG Fields
Description: This function reads the STKLOG record and sets the user
				 fields.
Passed:		 STKLOG record
				 User Number
Return:		 None
****************************************************************************/
void vReadStkLog(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	//char cmd[511] = "SELECT TOP 1 SKUN, DAYN, TYPE, DATE1, SSTK, ESTK, SMDN, EMDN, SWTF, EWTF, SRET, ERET, SPRI, EPRI "
	//							  "FROM STKLOG WHERE SKUN = ";

	SQL_DATE_STRUCT d;

	cn.SQLGetColumn(1, SQL_C_CHAR, user->pcLsku, sizeof(user->pcLsku));
	cn.SQLGetColumn(2, SQL_C_CHAR, user->pcDayn, sizeof(user->pcDayn));
	cn.SQLGetColumn(3, SQL_C_CHAR, user->pcType, sizeof(user->pcType));

	cn.SQLGetColumn(4, SQL_C_TYPE_DATE, &d, sizeof(d));
	sprintf_s(user->pcLdat, sizeof(user->pcLdat), "%d-%d-%d", d.year, d.month, d.day);
	user->dslDdat.year = d.year;
	user->dslDdat.month = (char)d.month;
	user->dslDdat.day = (char)d.day;
	
	cn.SQLGetColumn(5, SQL_C_CHAR, user->slSstk, sizeof(user->slSstk));
	strcpy_s(user->pcSstk, sizeof(user->pcSstk), user->slSstk);

	cn.SQLGetColumn(6, SQL_C_CHAR, user->slEstk, sizeof(user->slEstk));
	strcpy_s(user->pcSstk, sizeof(user->pcEstk), user->slEstk);

	sprintf_s(user->pcSmov, sizeof(user->pcSmov), "%6d", atol(user->pcSstk) - atol(user->pcEstk));

	cn.SQLGetColumn(7, SQL_C_CHAR, user->slSmdn, sizeof(user->slSmdn));
	cn.SQLGetColumn(8, SQL_C_CHAR, user->slEmdn, sizeof(user->slEmdn));
	sprintf_s(user->pcSmdn, sizeof(user->pcSmdn), "%6d",atol(user->slSmdn)-atol(user->slEmdn));

	cn.SQLGetColumn(9, SQL_C_CHAR, user->slSwtf, sizeof(user->slSwtf));
	cn.SQLGetColumn(10, SQL_C_CHAR, user->slEwtf, sizeof(user->slEwtf));
	cn.SQLGetColumn(11, SQL_C_CHAR, user->slSret, sizeof(user->slSret));
	cn.SQLGetColumn(12, SQL_C_CHAR, user->slEret, sizeof(user->slEret));
	cn.SQLGetColumn(13, SQL_C_CHAR, user->slSpri, sizeof(user->slSpri));
	cn.SQLGetColumn(14, SQL_C_CHAR, user->slEpri, sizeof(user->slEpri));

/*
//ProcessLogDumpBuffer("STK=",stklog,sizeof(STKLOG));
	//ProcessLogDumpBuffer("STK=",stklog->skun,sizeof(STKLOG));
	//ProcessLogDumpBuffer("STK=",stklog->type,sizeof(STKLOG));

	GetDBCharFieldToStr(sUser[i].pcLsku, stklog->skun, sizeof(stklog->skun));
	GetDBCharFieldToStr(sUser[i].pcType, stklog->type, sizeof(stklog->type));
	GetDBLongDateFieldToStr(sUser[i].pcLdat, &stklog->date);
	GetDBDecFieldToStr(sUser[i].pcDayn, stklog->dayn, sizeof(stklog->dayn), 0);
	//		 Calculate the Movement
	//W1.05 Form the Stock Movement = ( Starting Stock - Ending Stock )
	GetDBDecFieldToStr(sUser[i].pcSstk, stklog->sstk, sizeof(stklog->sstk), 0);
	GetDBDecFieldToStr(sUser[i].slSstk, stklog->sstk, sizeof(stklog->sstk), 0);
	GetDBDecFieldToStr(sUser[i].pcEstk, stklog->estk, sizeof(stklog->estk), 0);
	sprintf_s(sUser[i].pcSmov, "%6ld",atol(sUser[i].pcSstk)-atol(sUser[i].pcEstk));
	// Calculate the MarkDown Movement
	GetDBDecFieldToStr(sUser[i].slSmdn, stklog->smdn, sizeof(stklog->smdn), 0);
	GetDBDecFieldToStr(sUser[i].slEmdn, stklog->emdn, sizeof(stklog->emdn), 0);
	sprintf_s(sUser[i].pcSmdn, "%6ld",atol(sUser[i].slSmdn)-atol(sUser[i].slEmdn));
	GetDBDecFieldToStr(sUser[i].slSwtf, stklog->swtf, sizeof(stklog->swtf), 0);
	GetDBDecFieldToStr(sUser[i].slEwtf, stklog->ewtf, sizeof(stklog->ewtf), 0);
	GetDBDecFieldToStr(sUser[i].slSret, stklog->sret, sizeof(stklog->sret), 0);
	GetDBDecFieldToStr(sUser[i].slEret, stklog->eret, sizeof(stklog->eret), 0);
	GetDBDecFieldToStr(sUser[i].slSpri, stklog->spri, sizeof(stklog->spri), 2);
	GetDBDecFieldToStr(sUser[i].slEpri, stklog->epri, sizeof(stklog->epri), 2);
	memcpy(&sUser[i].dslDdat, &stklog->date, sizeof(stklog->date));
*/
	fprintf(ProcessLogHandle(),"**STKLOG: pcLsku='%s', pcType='%s', pcLdat='%s', pcDayn='%s'\n",
					user->pcLsku,user->pcType,user->pcLdat,user->pcDayn);
}
/***************************************************************************
Function:    Seek latest STKLOG Database
Description: Seek the last record in the STKLOG file for the day number
                         and set the bGotToDay TRUE (got valid number for current day)
Passed:      User Number
Return:      None
****************************************************************************/
/*
void vLastStkLogDatabase( int i )
{

	// Get last entry with DAYN/SKUN
  memset(&stklog, 0, sizeof(stklog));
	if (DBAccessTableData(&dbtStkLog,B_GET_LAST,NULL,3)==B_NO_ERROR) {
    vReadStkLog(&stklog, i);

  }
}
*/
/***************************************************************************
Function:    Seek STKLOG Database
Description: Seek a record in the STKLOG btrieve database file
Passed:      User Number
             Options 0 = Last Input, 1 = Skun
Return:      0 = Failed, 1 = Success,
LastInput:   None
****************************************************************************/
/*
int iSeekStkLogDatabase( long lItemsSold, int i )
{
	BTI_CHAR keyBuf[21];
	char s[161];
	char dateStr[12];
	BTI_SINT iDataBaseStatus;

	iDataBaseStatus=DBAccessTableData(&dbtStkLog,B_GET_FIRST,NULL,3);
  GetLongToDBDecField(keyBuf,DateStrToDayNumber(GetCurrentDateInDDMMYYYY(dateStr)), sizeof(stklog.dayn));
  GetStrToDBCharField(keyBuf+sizeof(stklog.dayn),stklog.skun,sizeof(stklog.skun));
	if (DBAccessTableData(&dbtStkLog,B_GET_GE,keyBuf,3)==B_NO_ERROR) {
		vReadStkLog(&stklog,i);
    //fprintf(hLogFile,"pcLdat is %s & pcDate is %s\n",sUser[i].pcLdat,sUser[i].pcDate);
    //fprintf(hLogFile,"pcLsku is %s & pcSkun is %s\n",sUser[i].pcLsku,sUser[i].pcSkun);
		sprintf_s(s,"Seek STKLOG comp\n '%s'='%s'\n'%s'='%s'\n",
						sUser[i].pcLsku,sUser[i].pcSkun,
						sUser[i].pcLdat,sUser[i].pcDate);
		ProcessLogWriteLn(s);
		if ((strcmp(sUser[i].pcLsku,sUser[i].pcSkun)!=0) || (strcmp(sUser[i].pcLdat,sUser[i].pcDate)!=0)) {
			return(0);
 		}
 		lItemsSold = lSkuMoves(lItemsSold, i);
		//fprintf(hLogFile,"lItemsSold is %6ld\n ",lItemsSold );
		sprintf_s(sUser[i].pcSold, "%6ld", lItemsSold);
		return(1);         // Seek Passed
		}
	//fprintf(hLogFile,"No log record found\n");
	return(0);    // Seek Failed
}
*/
/***************************************************************************
Function:    Sequential read of STKLOG Database
Description: Scan (via sequential read ) for a SKU's Movement records
Passed:      User Number
Return:      0 = Failed, 1 = Success,
LastInput:   None
****************************************************************************/
/*
int iScanStkLogDatabase( long lItemsSold, int i )
{
	char s[161];

	// GET THE RECORD WITH GET_EQUAL AND UPDATE IT
  memset( &stklog, 0, sizeof(stklog) );
 	if (DBAccessTableData(&dbtStkLog,B_GET_NEXT,NULL,3)==B_NO_ERROR) {
    vReadStkLog(&stklog, i);

		sprintf_s(s,"Scan STKLOG comp\n '%s'='%s'\n'%s'='%s'\n",
						sUser[i].pcLsku,sUser[i].pcSkun,
						sUser[i].pcLdat,sUser[i].pcDate);
		ProcessLogWriteLn(s);

		if ((strcmp(sUser[i].pcLsku,sUser[i].pcSkun) != 0) ||
			(strcmp(sUser[i].pcLdat,sUser[i].pcDate) != 0 )) {
      return(0);
		}
    lItemsSold = lSkuMoves(lItemsSold, i);
    sprintf_s(sUser[i].pcSold, "%6ld", lItemsSold);
    //fprintf(hLogFile,"sUser[i].pcSold is %6ld\n", lItemsSold);
    return(1);         // Seek Passed
  }
  //fprintf(hLogFile,"No log record found\n");
  return(0);    // Seek Failed
}
*/
//-----------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Author      : Partha Dutta
// Date        : 13/10/2010
// Referral No : 290
// Notes       : Use the system date held in sysdat->tmdt and not today's date
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
long GetStkLogSoldToday(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];
	long      soldToday;
	char      dayNumber[7];
  //char      s[21];
	char      cmd[255] = "exec dbo.GetStkLogSoldPerDay @skun = ";

    cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSkun);
    strcat_s(cmd, sizeof(cmd), ", @dayn = ");
	//use system date: the date has already been calculated by this point
    //old code - it uses today
	//_itoa_s(DateStrToDayNumber(GetCurrentDateInDDMMYYYY(s)), dayNumber, sizeof(dayNumber), 10);
    _itoa_s(DBLongDateToDayNumber(&user->SystemDate), dayNumber, sizeof(dayNumber), 10);
	strcat_s(cmd, sizeof(cmd), dayNumber);

	// First check if SKU is infact an EAN
	if (cn.DoSQLCommand(cmd)) {
		if (cn.DoSQLFetch()) {
			cn.SQLGetColumn(1, SQL_C_LONG, &soldToday, NULL);
			return soldToday;
		}
	}
	
/*
unsigned long slDayn,dayn;
	long itemsSold=0;
	int logType,result;
	char s[161];
	char slSkun[7];
	char *keyBufPtr;
	// Get todays date in day number format
	dayn=DateStrToDayNumber(GetCurrentDateInDDMMYYYY(s));
	// Setup the key for skun/dayn (index 1)
	DBClearAccessKeyBuffer();
	keyBufPtr=DBGetAccessKeyBufferPtr();
  GetStrToDBCharField(keyBufPtr,skun,sizeof(stklog.skun));
	GetLongToDBDecField(keyBufPtr+sizeof(stklog.skun),dayn,sizeof(stklog.dayn));
	// Read the first one
	result=DBAccessTableData(&dbtStkLog,B_GET_GE,NULL,1);
	// Go through stklog for this skun/dayn
	while (result==B_NO_ERROR) {
		GetDBCharFieldToStr(slSkun,stklog.skun, sizeof(stklog.skun));
		// No more for this skun
		if (strcmp(slSkun,skun)) {
			return(itemsSold);
		}
		// Look for today
		slDayn=GetDBDecFieldToLong(stklog.dayn,sizeof(stklog.dayn));
		if (slDayn==dayn) {
			logType=atol(GetDBCharFieldToStr(s,stklog.type, sizeof(stklog.type)));
			// Add onhand diff. if it is a sale, refund, PSS instore, PSS sale, or PSS refund
			if ((logType==1) || (logType==2) || (logType==3) || (logType==11) || (logType==12)) {
				itemsSold+=(GetDBDecFieldToLong(stklog.sstk,sizeof(stklog.sstk))-GetDBDecFieldToLong(stklog.estk,sizeof(stklog.estk)));
			// Add markdown onhand diff. if markdown stock sale
			} else if (logType==4) {
				itemsSold+=(GetDBDecFieldToLong(stklog.smdn,sizeof(stklog.smdn))-GetDBDecFieldToLong(stklog.emdn,sizeof(stklog.emdn)));
			}
		}	// Read next stklog
		result=DBAccessTableData(&dbtStkLog,B_GET_NEXT,NULL,1);
	}
	return(itemsSold);
*/
	return 0;
}

/***************************************************************************
Function:    Read next STKLOG Database (used by the HHT database dump utility)
Description: Read next record in the STKLOG btrieve database file
Passed:      Number sold today running total, User Number
Return:      0 = Failed, 1 = Success,
LastInput:   None
****************************************************************************/
int iReadStkLogDatabase( int sessionNumber )
{
/*
BTI_SINT iDataBaseStatus = B_NO_ERROR;

	// GET THE RECORD WITH GET_EQUAL AND UPDATE IT
  memset( &stklog, 0, sizeof(stklog) );
	iDataBaseStatus=DBAccessTableData(&dbtStkLog,B_GET_NEXT,NULL,3);
  if ((iDataBaseStatus == B_END_OF_FILE) || (iDataBaseStatus == B_POSITION_NOT_SET)) {
		iDataBaseStatus=DBAccessTableData(&dbtStkLog,B_GET_FIRST,NULL,3);
  }
  if ( iDataBaseStatus == B_NO_ERROR ) {
    vReadStkLog(&stklog, i);
    return(1);
  }
  return(0);
*/
return(0);
}
/***************************************************************************
Function:    Get Previous STKLOG Record (used while inserting type 91 stklog)
Description: Get the Previous STKLOG btrieve database record
Passed:      User Number
Return:      0 = Failed, 1 = Success,
LastInput:   None
****************************************************************************/
int iPriorStkLogDatabase( int sessionNumber )
{
	USER_TYPE *user = &sUser[sessionNumber];

	char s[21] = "";

	char cmd[511] = "SELECT TOP 1 SKUN, DAYN, TYPE, DATE1, "
															 "SSTK, ESTK, SMDN, EMDN, SWTF, EWTF, SRET, ERET, SPRI, EPRI "
								  "FROM STKLOG WHERE SKUN = ";

	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSkun);
	strcat_s(cmd, sizeof(cmd), " AND DAYN = ");
	_itoa_s(DateStrToDayNumber(GetCurrentDateInDDMMYYYY(s)), s, sizeof(s), 10);
	strcat_s(cmd, sizeof(cmd), s);

	strcat_s(cmd, sizeof(cmd), " ORDER BY DAYN DESC, TKEY DESC");

	if (cn.DoSQLCommand(cmd) && cn.DoSQLFetch()) {
		vReadStkLog(sessionNumber);
		user->noPreSlRec = false;
		return 1;
	}
	user->noPreSlRec = true;
	return 0;

/*
BTI_CHAR keyBuf[21];
	BTI_SINT iDataBaseStatus = B_NO_ERROR;
	// STKLOG key fields
  long lpcDayn = 999999;
  long lpcTkey = 999999;
  char pcNext[7];

	sUser[i].noPreSlRec=0;
	// Date fields
	memset( &stklog, 0, sizeof(stklog));
  memset( keyBuf, 0, sizeof(keyBuf));
	// Reposition for new Index
	iDataBaseStatus=DBAccessTableData(&dbtStkLog,B_GET_FIRST,NULL,1);
	// SKUN
  sprintf_s(pcNext,"%6ld",atol(sUser[i].pcSkun) +1);
	GetStrToDBCharField(keyBuf,pcNext,sizeof(stklog.skun));
	// DAYN
	GetLongToDBDecField(keyBuf+sizeof(stklog.skun),lpcDayn,sizeof(stklog.dayn));
	// TKEY
	GetLongToDBDecField(keyBuf+sizeof(stklog.skun)+sizeof(stklog.dayn),lpcTkey,sizeof(stklog.tkey));
	iDataBaseStatus=DBAccessTableData(&dbtStkLog,B_GET_GE,keyBuf,1);
	if ( iDataBaseStatus == B_END_OF_FILE )	{
		iDataBaseStatus=DBAccessTableData(&dbtStkLog,B_GET_LAST,keyBuf,1);
	} else  {
		iDataBaseStatus=DBAccessTableData(&dbtStkLog,B_GET_PREVIOUS,keyBuf,1);
	}
  if ( iDataBaseStatus == B_NO_ERROR ) {
    vReadStkLog(&stklog, i);
    if (strcmp(sUser[i].pcLsku,sUser[i].pcSkun) != 0) {
      memset( &stklog, 0, sizeof(stklog) );
      sUser[i].noPreSlRec  = 1 ;
      return(0);
      }
    return(1);
    }
  sUser[i].noPreSlRec  = 1 ;
  //fprintf(hLogFile,"No log record found\n");
  return(0);
*/
}
/****************************************************************************
Function   : Insert a STKLOG record
Description: Insert a Stock Log record in the Database file
Passed:      User Number & Option = 0 -> for MarkDown/WriteOff
                           Option = 1 -> for Price Changes
Return:      0  = Failed, 1  = Success
LastInput:   0  = Failed, 1  = Success; 2 = Duplicate Entry
****************************************************************************/
int iInsertStkLogDatabase( int sessionNumber, int iOption )
{
	USER_TYPE *user = &sUser[sessionNumber];

	char s[21] = "";

	char cmd[511] = "INSERT INTO STKLOG (SKUN, DAYN, TYPE, DATE1, TIME, KEYS, EEID, ICOM, "
		                                  "SSTK, ESTK, SRET, ERET, SPRI, EPRI, SMDN, EMDN, SWTF, EWTF, RTI) VALUES (";

	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSkun);                                     //SKUN

	strcat_s(cmd, sizeof(cmd), ", ");
	_itoa_s(DateStrToDayNumber(GetCurrentDateInDDMMYYYY(s)), s, sizeof(s), 10);                //DAYN
	strcat_s(cmd, sizeof(cmd), s);

	strcat_s(cmd, sizeof(cmd), ", ");
	// writeoff ? negative ? "'68'" : "'69'" : markdown ? negative ? "'66'" : "'67'" : "'61'")
	strcat_s(cmd, sizeof(cmd), user->pcSaIwtf ? (user->pcAdjQty < 0 ? "'68'" : "'69'") : (user->pcSaImdn ? (user->pcAdjQty < 0 ? "'66'" : "'67'") : "'61'")) ; //TYPE

	strcat_s(cmd, sizeof(cmd), ", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), cn.SQLGetCurrentDateString(s, sizeof(s))); //DATE1

	strcat_s(cmd, sizeof(cmd), ", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), cn.SQLGetCurrentTimeString(s, sizeof(s))); //TIME

	strcat_s(cmd, sizeof(cmd), ", '"); //KEYS
	strcat_s(cmd, sizeof(cmd), GetCurrentDateInDDMMYY(NULL));
	strcat_s(cmd, sizeof(cmd), "  ");
	strcat_s(cmd, sizeof(cmd), user->pcSaNumb);
	strcat_s(cmd, sizeof(cmd), "  ");
	strcat_s(cmd, sizeof(cmd), user->pcSkun);
	strcat_s(cmd, sizeof(cmd), "  ");
	strcat_s(cmd, sizeof(cmd), user->pcAaSeqn);
	strcat_s(cmd, sizeof(cmd), "'");

	strcat_s(cmd, sizeof(cmd), ", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcEeid); //EEID

	strcat_s(cmd, sizeof(cmd), ", 0"); //ICOM

	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->imOnha); //SSTK

	strcat_s(cmd, sizeof(cmd), ", ");
	if (iOption == 0) {												//ESTK
		_itoa_s(atol(user->imOnha) + atol(user->pcAdjQty), s, sizeof(s), 10);
		strcat_s(cmd, sizeof(cmd), s);            
	} else {
		strcat_s(cmd, sizeof(cmd), user->imOnha); 
	}

	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->imRetq); //SRET

	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->imRetq); //ERET

	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->imPric); //SPRI

	if (iOption == 1) {												//EPRI
		strcat_s(cmd, sizeof(cmd), ", ");
		strcat_s(cmd, sizeof(cmd), user->ipPric); 
	} else {
		strcat_s(cmd, sizeof(cmd), ", ");
		strcat_s(cmd, sizeof(cmd), user->imPric); 
	}

	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->imMdnq); //SMDN

	strcat_s(cmd, sizeof(cmd), ", ");
	if ((iOption == 0) 												//EMDN
	   && (user->pcSaImdn)) {
		_itoa_s(atol(user->imMdnq) - atol(user->pcAdjQty), s, sizeof(s), 10);
		strcat_s(cmd, sizeof(cmd), s);
	} else {
		strcat_s(cmd, sizeof(cmd), user->imMdnq); 
	}

	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->imWtfq); //SWTF

	strcat_s(cmd, sizeof(cmd), ", ");
	if ((iOption == 0) 												//EWTF
		 && (user->pcSaIwtf)) {
		_itoa_s(atol(user->imWtfq) - atol(user->pcAdjQty), s, sizeof(s), 10);
		strcat_s(cmd, sizeof(cmd), s);
	} else {
		strcat_s(cmd, sizeof(cmd), user->imWtfq); 
	}

	strcat_s(cmd, sizeof(cmd), ", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), "S");                                     //RTI

	strcat_s(cmd, sizeof(cmd), ")");

	if (cn.DoSQLCommand(cmd)) {
    strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
    return(1);
	}
	strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
	return(0);

	/*
BTI_CHAR keyBuf[21];
	BTI_SINT iDataBaseStatus = B_NO_ERROR;
	char dateStr[12],pcType[3];
  long lTempLong;
	// init... the output record
	memset(&stklog, 0, sizeof(stklog) );
	memset( keyBuf, 0, sizeof(keyBuf) );
	// SKUN
	GetStrToDBCharField(stklog.skun,sUser[i].pcSkun,sizeof(stklog.skun));
	// DAYN
	GetLongToDBDecField(stklog.dayn,DateStrToDayNumber(GetCurrentDateInDDMMYYYY(dateStr)),sizeof(stklog.dayn));
	// TYPE
	strcpy(pcType,"61");		// Default to price change
	if (iOption == 0) {			// MarkDown WriteOff STKLOG record
   	if (sUser[i].pcSaImdn) {
			if (atol(sUser[i].pcAdjQty) < 0L) {
				strcpy(pcType,"66");
			} else {
   	   	strcpy(pcType,"67");
			}
		}
   	if (sUser[i].pcSaIwtf) {
			if (atol(sUser[i].pcAdjQty) < 0L) {
       	strcpy(pcType,"68");
			}	else {
     		strcpy(pcType,"69");
			}
		}
	}
	//GetStrToDBCharField(keyBuf+sizeof(stklog.tkey)+sizeof(stklog.skun)+sizeof(stklog.dayn),pcType,sizeof(stklog.type));
	GetStrToDBCharField(stklog.type,pcType,sizeof(stklog.type));
	// Date
 	GetCurrentDateInDBLongDateFormat(&stklog.date);
	// Time
	GetStrToDBCharField(stklog.time,GetCurrentTimeStr(NULL),sizeof(stklog.time));
	// KEYS
  memset(stklog.keys,' ',sizeof(stklog.keys));
	GetStrToDBCharField(stklog.keys,GetCurrentDateInDDMMYY(NULL),8);
  GetStrToDBCharField(stklog.keys+10,sUser[i].pcSaNumb, sizeof(sUser[i].pcSaNumb)-1);
  GetStrToDBCharField(stklog.keys+14,sUser[i].pcSkun, sizeof(sUser[i].pcSkun)-1);
  GetStrToDBCharField(stklog.keys+22,sUser[i].pcAaSeqn, sizeof(sUser[i].pcAaSeqn)-1);
	// EEID
	GetStrToDBCharField(stklog.eeid,sUser[i].pcEeid,sizeof(stklog.eeid));
	//  Starting Stock
 	GetStrToDBDecField(stklog.sstk,sUser[i].imOnha,sizeof(stklog.sstk),0);
	//  Ending Stock
  lTempLong = 0L;
	if (iOption == 0) {	// Stock Adjustment
		lTempLong = (atol(sUser[i].imOnha) + atol(sUser[i].pcAdjQty));
	} else {				// Price Change
		lTempLong = atol(sUser[i].imOnha) ;
	}
	GetLongToDBDecField(stklog.estk,lTempLong,sizeof(stklog.estk));
	//  Starting Returns
	GetStrToDBDecField(stklog.sret,sUser[i].imRetq,sizeof(stklog.sret),0);
	//  Ending Returns
	GetStrToDBDecField(stklog.eret,sUser[i].imRetq,sizeof(stklog.eret),0);
	//  Starting Price
	GetStrToDBDecField(stklog.spri,sUser[i].imPric,sizeof(stklog.spri),2);
	//  Ending Price
	if (iOption == 1) { 	// Price Change
 		GetStrToDBDecField(stklog.epri,sUser[i].ipPric,sizeof(stklog.epri),2);
	} else {
 		GetStrToDBDecField(stklog.epri,sUser[i].imPric,sizeof(stklog.epri),2);
	}
	//  Starting MarkDown Quantity
 	GetStrToDBDecField(stklog.smdn,sUser[i].imMdnq,sizeof(stklog.smdn),0);
	//  Ending MarkDown Quantity
  // This is a MarkDown Update
  lTempLong = atol(sUser[i].imMdnq);
	if (iOption == 0) {				// Stock Adjustment
		if (sUser[i].pcSaImdn) {
     	lTempLong = (atol(sUser[i].imMdnq) - atol(sUser[i].pcAdjQty));
		}
	}
	GetLongToDBDecField(stklog.emdn,lTempLong,sizeof(stklog.emdn));
	//  Starting Write Off Quantity
 	GetStrToDBDecField(stklog.swtf,sUser[i].imWtfq,sizeof(stklog.swtf),0);
	//  Ending Write Off Quantity
  lTempLong = atol(sUser[i].imWtfq);
	if (iOption == 0) {	// Stock Adjustment
		if (sUser[i].pcSaIwtf)  {
     	lTempLong = (atol(sUser[i].imWtfq) - atol(sUser[i].pcAdjQty));
		}
	}
	GetLongToDBDecField(stklog.ewtf,lTempLong,sizeof(stklog.ewtf));
	//  Insert a new StkLog record
	iDataBaseStatus=DBAccessTableData(&dbtStkLog,B_INSERT,keyBuf,0);
	if ( iDataBaseStatus == B_NO_ERROR ) {
    strcpy(sUser[i].pcLastInput,"1");
    return(1);
  }
  if ( iDataBaseStatus == B_DUPLICATE_KEY_VALUE ) {
		strcpy(sUser[i].pcLastInput,"2");
    return(1);
	}
  strcpy(sUser[i].pcLastInput,"0");
  return(0) ;
*/
}
/**************************************************************************
Function:    Insert A Stklog Adustment 91
Description: This function Inserts a STKLOG Adjustment 91 record if
                         there is a mismatch between STKMAS fields after last adjustment
Passed:      User Number
Return:      0  = Failed, 1  = Success
LastInput:   0  = Failed, 1  = Success; 2 = Duplicate Entry
****************************************************************************/
int iInsertStkLogAdjust91(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

  if ((atol(user->imOnha) == atol(user->slEstk)) &&
      (atol(user->imRetq) == atol(user->slEret)) &&
      (atol(user->imMdnq) == atol(user->slEmdn)) &&
      (atol(user->imWtfq) == atol(user->slEwtf))) {
    strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
    return(1);
	}

	char s[21] = "";

	char cmd[511] = "INSERT INTO STKLOG (SKUN, DAYN, TYPE, DATE1, TIME, KEYS, EEID, ICOM, "
		                                  "SSTK, ESTK, SRET, ERET, SPRI, EPRI, SMDN, EMDN, SWTF, EWTF) VALUES (";

	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSkun);                                     //SKUN

	strcat_s(cmd, sizeof(cmd), ", ");
	_itoa_s(DateStrToDayNumber(GetCurrentDateInDDMMYYYY(s)), s, sizeof(s), 10);                //DAYN
	strcat_s(cmd, sizeof(cmd), s);

	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), "'91'"); //TYPE

	strcat_s(cmd, sizeof(cmd), ", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), cn.SQLGetCurrentDateString(s, sizeof(s))); //DATE1

	strcat_s(cmd, sizeof(cmd), ", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), cn.SQLGetCurrentTimeString(s, sizeof(s))); //TIME

	strcat_s(cmd, sizeof(cmd), ", '"); //KEYS
	strcat_s(cmd, sizeof(cmd), GetCurrentDateInDDMMYY(NULL));
	strcat_s(cmd, sizeof(cmd), "  ");
	strcat_s(cmd, sizeof(cmd), user->pcSaNumb);
	strcat_s(cmd, sizeof(cmd), "  ");
	strcat_s(cmd, sizeof(cmd), user->pcSkun);
	strcat_s(cmd, sizeof(cmd), "  ");
	strcat_s(cmd, sizeof(cmd), user->pcAaSeqn);
	strcat_s(cmd, sizeof(cmd), "'");

	strcat_s(cmd, sizeof(cmd), ", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcEeid); //EEID

	strcat_s(cmd, sizeof(cmd), ", 0"); //ICOM

	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->slEstk); //SSTK

	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->imOnha); //ESTK

	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->slEret); //SRET

	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->imRetq); //ERET

	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->slSpri); //SPRI

	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->imPric); //EPRI

	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->slEmdn); //SMDN

	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->imMdnq); //EMDN

	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->slEwtf); //SWTF

	strcat_s(cmd, sizeof(cmd), ", ");
	strcat_s(cmd, sizeof(cmd), user->imWtfq); //EWTF
	
	strcat_s(cmd, sizeof(cmd), ")");

	if (cn.DoSQLCommand(cmd)) {
    strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
    return(1);
	}
	strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
	return(0);

	
	/*
BTI_CHAR keyBuf[21];
	char dateStr[12];
	BTI_SINT iDataBaseStatus = B_NO_ERROR;

	// Compare if Adjustment is needed ?
  if ((strcmp(sUser[i].imOnha,sUser[i].slEstk) == 0 ) &&
      (strcmp(sUser[i].imRetq,sUser[i].slEret) == 0 ) &&
      (strcmp(sUser[i].imMdnq,sUser[i].slEmdn) == 0 ) &&
      (strcmp(sUser[i].imWtfq,sUser[i].slEwtf) == 0 )) {
    strcpy(sUser[i].pcLastInput, "1");
    return(1);
    }
	// Insert Adjustment 91
  memset(&stklog, 0, sizeof(stklog) );
  memset( keyBuf, 0, sizeof(keyBuf) );
	// SKUN
	GetStrToDBCharField(stklog.skun,sUser[i].pcSkun,sizeof(stklog.skun));
	// DAYN
	GetLongToDBDecField(stklog.dayn,DateStrToDayNumber(GetCurrentDateInDDMMYYYY(dateStr)),sizeof(stklog.dayn));
	// TYPE
	GetStrToDBCharField(stklog.type,"91",sizeof(stklog.type));
	// Date
	GetCurrentDateInDBLongDateFormat(&stklog.date);
  // Time
  GetStrToDBCharField(stklog.time,GetCurrentTimeStr(NULL),sizeof(stklog.time));
	// KEYS
  memset(stklog.keys,' ',sizeof(stklog.keys));
	GetStrToDBCharField(stklog.keys,GetCurrentDateInDDMMYY(NULL),8);
  GetStrToDBCharField(stklog.keys+10,sUser[i].pcSaNumb, sizeof(sUser[i].pcSaNumb)-1);
  GetStrToDBCharField(stklog.keys+14,sUser[i].pcSkun, sizeof(sUser[i].pcSkun)-1);
  GetStrToDBCharField(stklog.keys+22,sUser[i].pcAaSeqn, sizeof(sUser[i].pcAaSeqn)-1);
	// EEID
	GetStrToDBCharField(stklog.eeid,sUser[i].pcEeid,sizeof(stklog.eeid));
	//  Starting Stock
	GetStrToDBDecField(stklog.sstk,sUser[i].slEstk,sizeof(stklog.sstk),0);
	//  Ending Stock
	GetStrToDBDecField(stklog.estk,sUser[i].imOnha,sizeof(stklog.estk),0);
	//  Starting Returns
	GetStrToDBDecField(stklog.sret,sUser[i].slEret,sizeof(stklog.sret),0);
	//  Ending Returns
	GetStrToDBDecField(stklog.eret,sUser[i].imRetq,sizeof(stklog.eret),0);
	//  Starting Price
	GetStrToDBDecField(stklog.spri,sUser[i].slEpri,sizeof(stklog.spri),2);
	//  Ending Price
	GetStrToDBDecField(stklog.epri,sUser[i].imPric,sizeof(stklog.epri),2);
	//  Starting MarkDown Quantity
	GetStrToDBDecField(stklog.smdn,sUser[i].slEmdn,sizeof(stklog.smdn),0);
	//  Ending MarkDown Quantity
	GetStrToDBDecField(stklog.emdn,sUser[i].imMdnq,sizeof(stklog.emdn),0);
	//  Starting Write Off Quantity
	GetStrToDBDecField(stklog.swtf,sUser[i].slEwtf,sizeof(stklog.swtf),0);
	//  Ending Write Off Quantity
	GetStrToDBDecField(stklog.ewtf,sUser[i].imWtfq,sizeof(stklog.ewtf),0);
	//  Insert a new StkLog record
	iDataBaseStatus=DBAccessTableData(&dbtStkLog,B_INSERT,keyBuf,0);
  if ( iDataBaseStatus == B_NO_ERROR ) {
    strcpy(sUser[i].pcLastInput,"1");
    return(1);
  }
  if ( iDataBaseStatus == B_DUPLICATE_KEY_VALUE ) {
    strcpy(sUser[i].pcLastInput,"2");
    return(1);
  }
  strcpy(sUser[i].pcLastInput,"0");
  return(0) ;
*/
}

/***************************************************************************
Function:    Update STKMAS Database
Description: Update record in the stock btrieve database file
Passed:      User Number and Option 0 -> Label Qty Update
                             Option 1 -> Update following MarkDown Adj.
                             Option 2 -> Upd.Price with Price Change(IP:PRIC)
Return:      0  = Failed, 1  = Success,
LastInput:  "0" = Failed, else "1" = Success,
****************************************************************************/
int iUpdateStkMasDatabase( int sessionNumber, int iOption )
{
	USER_TYPE *user = &sUser[sessionNumber];

	char s[21];
	char cmd[255] = "UPDATE STKMAS SET";

	switch (iOption) {
		case 0:
			strcat_s(cmd, sizeof(cmd), " LABM = ");
			strcat_s(cmd, sizeof(cmd), user->pcLabm);
			strcat_s(cmd, sizeof(cmd), ", LABN = ");
			strcat_s(cmd, sizeof(cmd), user->pcLabn);
			strcat_s(cmd, sizeof(cmd), ", LABL = ");
			strcat_s(cmd, sizeof(cmd), user->pcLabl);
			/*
    if (iOption == 0) {
			GetStrToDBDecField(stkmas.labm,sUser[i].pcLabm,sizeof(stkmas.labm),0);
			GetStrToDBDecField(stkmas.labn,sUser[i].pcLabn,sizeof(stkmas.labn),0);
			GetStrToDBDecField(stkmas.labl,sUser[i].pcLabl,sizeof(stkmas.labl),0);
			printf("STKMAS update label counts: %s, %s, %s\n",sUser[i].pcLabm,sUser[i].pcLabn,sUser[i].pcLabl);
		}
			*/
			break;
		case 1:
			strcat_s(cmd, sizeof(cmd), " ONHA = ONHA + ");
			strcat_s(cmd, sizeof(cmd), user->pcAdjQty);

			if (user->pcSaImdn) {
				strcat_s(cmd, sizeof(cmd), ", MDNQ = MDNQ - ");
				strcat_s(cmd, sizeof(cmd), user->pcAdjQty);
			}

			if (user->pcSaIwtf) {
				strcat_s(cmd, sizeof(cmd), ", WTFQ = WTFQ - ");
				strcat_s(cmd, sizeof(cmd), user->pcAdjQty);
			}
			/*
    if (iOption == 1) {
	    vReadStkMas(&stkmas, i);
      // Update the OnHand with the transfer value
      lTempLong = (atol(sUser[i].imOnha) + atol(sUser[i].pcAdjQty));
      sprintf_s(sUser[i].imOnha,"%6ld",lTempLong);
      GetLongToDBDecField(stkmas.onha, lTempLong,sizeof(stkmas.onha));
      // Update the MarkDown Quantity
      if (sUser[i].pcSaImdn) {
        lTempLong = (atol(sUser[i].imMdnq) - atol(sUser[i].pcAdjQty));
        sprintf_s(sUser[i].imMdnq,"%6ld",lTempLong);
				GetLongToDBDecField(stkmas.mdnq,lTempLong,sizeof(stkmas.mdnq));
      }
      // Update the WriteOff Quantity
      if (sUser[i].pcSaIwtf) {
        lTempLong = (atol(sUser[i].imWtfq) - atol(sUser[i].pcAdjQty));
        sprintf_s(sUser[i].imWtfq,"%6ld",lTempLong);
	      GetLongToDBDecField(stkmas.wtfq,lTempLong,sizeof(stkmas.wtfq));
        }
      }
			*/
			break;
		case 2:
			strcat_s(cmd, sizeof(cmd), " PPRI = ");
			strcat_s(cmd, sizeof(cmd), user->imPric);
			strcat_s(cmd, sizeof(cmd), ", PRIC = ");
			strcat_s(cmd, sizeof(cmd), user->ipPric);

			strcat_s(cmd, sizeof(cmd), ", DPRC =" );
			cn.SQLGetCurrentDateString(s, sizeof(s));
			cn.SQLQueryAddVarChar(cmd, sizeof(cmd), s);

			strcat_s(cmd, sizeof(cmd), ", REVT = " );
			cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->ipEvnt);

			strcat_s(cmd, sizeof(cmd), ", RPRI = ");
			cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->ipPrio);


			/*
    if (iOption == 2) {
			GetStrToDBDecField(stkmas.ppri,sUser[i].imPric,sizeof(stkmas.ppri),2);
			GetStrToDBDecField(stkmas.pric,sUser[i].ipPric,sizeof(stkmas.pric),2);
			// Date
			GetCurrentDateInDBLongDateFormat(&stkmas.dprc);
			// Event Number
			GetStrToDBCharField(stkmas.revt,sUser[i].ipEvnt,sizeof(stkmas.revt));
			// Event Priority
			GetStrToDBCharField(stkmas.rpri,sUser[i].ipPrio,sizeof(stkmas.rpri));
		  }
			*/
			break;
		default:
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
			return(0);
	}
	
	strcat_s(cmd, sizeof(cmd), " WHERE SKUN=");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSkun);
	if (cn.DoSQLCommand(cmd)) {
    strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
    return(1);
	}
	strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
	return(0);
/*
BTI_CHAR keyBuf[21];
	char pcSeek[20];
  long lTempLong;
  BTI_SINT iDataBaseStatus = B_NO_ERROR;

	strcpy(pcSeek, sUser[i].pcSkun );
  // GET THE RECORD WITH GET_EQUAL AND UPDATE IT
  memset( &stkmas, 0, sizeof(stkmas) );
	GetStrToDBCharField(keyBuf, sUser[i].pcSkun, sizeof(stkmas.skun));
	iDataBaseStatus=DBAccessTableData(&dbtStkMas,B_GET_EQUAL | S_NOWAIT_LOCK,keyBuf,0);
  if (iDataBaseStatus == B_NO_ERROR ) {
    //fprintf(hLogFile, "iOption is %d\n",iOption);
		iDataBaseStatus=DBAccessTableData(&dbtStkMas,B_UPDATE,keyBuf,0);
		iDataBaseStatus=DBAccessTableData(&dbtStkMas,B_UNLOCK,keyBuf,0);
  }
  if (iDataBaseStatus == B_NO_ERROR) {
    strcpy(sUser[i].pcLastInput, "1");
    return(1);
	}
  strcpy(sUser[i].pcLastInput, "0");
  return(0);
*/
}
/***************************************************************************
Function:	 Read HHTDET Fields
Description: This function reads the HHTDET record and sets the user
				 fields.
Passed:		 HHTDET record
				 User Number
Return:		 None
****************************************************************************/
void vReadHhtDet(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	bool lbok;

	cn.SQLGetColumn(1, SQL_C_CHAR, user->pcDate, sizeof(user->pcDate));
	cn.SQLGetColumn(2, SQL_C_CHAR, user->pcSkun, sizeof(user->pcSkun));
	cn.SQLGetColumn(3, SQL_C_CHAR, user->pcIcnt, sizeof(user->pcIcnt));
	cn.SQLGetColumn(4, SQL_C_CHAR, user->pcInon, sizeof(user->pcInon));
	cn.SQLGetColumn(5, SQL_C_CHAR, user->pcOnha, sizeof(user->pcOnha));
	cn.SQLGetColumn(6, SQL_C_CHAR, user->pcTscn, sizeof(user->pcTscn));
	cn.SQLGetColumn(7, SQL_C_CHAR, user->pcTwcn, sizeof(user->pcTwcn));
	cn.SQLGetColumn(8, SQL_C_CHAR, user->pcTpre, sizeof(user->pcTpre));
	cn.SQLGetColumn(9, SQL_C_CHAR, user->pcMdnq, sizeof(user->pcMdnq));
	cn.SQLGetColumn(10, SQL_C_CHAR, user->pcTmdc, sizeof(user->pcTmdc));
	cn.SQLGetColumn(11, SQL_C_CHAR, user->pcTcnt, sizeof(user->pcTcnt));

	/*
	Author      : Partha Dutta
	Date        : 17/08/2011
	Referral No : RF0864
	Notes       : Multiple guns using option 2: Primary Count can cause the HHT server to crash in certain situations
                  When each gun logs on a unique session object is created; these objects held in an session array
                  Gun 1 logs on will get session id 0
				  Gun 2 logs on will get session id 1
                  If gun 2 accesses the "primary count" first and then gun 1 next then everything is fine
				  Reverse the operation and the HHT server crashes because it enteres an infinite loop

				  This is because the session array is accessed with no subscript selected and defaults to the first entry
				  The amended code will use the correct session array element
	*/

	//strcpy_s(user->hdSkun, sizeof(user->hdSkun), sUser->pcSkun);
	//strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), sUser->pcSkun);

	strcpy_s(user->hdSkun, sizeof(user->hdSkun), user->pcSkun);
	strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), user->pcSkun);

	/////////////////////////////////////////////////////////////////////////


	cn.SQLGetColumn(12, SQL_C_BIT, &lbok, 0);

	strcpy_s(user->pcPicLabelOk, sizeof(user->pcPicLabelOk),"NO");
	if (lbok)	{
			strcpy_s(user->pcPicLabelOk, sizeof(user->pcPicLabelOk),"YES");
	}
}
/***************************************************************************
Function:    Seek HHTDET Database
Description: Seek a record in the HHT detail btrieve database file
             Also reads the database if SKU found.
Passed:      User Number
             Options 0 = Use first index Date and SKU
             Options 1 = Use first index Date, Icnt flag and SKU
Return:      0 = Failed, 1 = Success,
LastInput:  "0" = Failed, else a "1" = Success,
****************************************************************************/
int iSeekHhtDetDatabase(int sessionNumber, int option)
{
	USER_TYPE *user = &sUser[sessionNumber];

	char cmd[255] = "SELECT TOP 1 DATE1, SKUN, ICNT, INON, ONHA, TSCN, TWCN, TPRE, MDNQ, TMDC, TCNT, LBOK FROM HHTDET WHERE DATE1 = ";
	char workDate[11];

	DBLongDate d = user->dDateHdr;
	sprintf_s(workDate, sizeof(workDate), "%d-%d-%d", d.year, d.month, d.day);
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd),  workDate);

	switch (option) {
		case 0:
			strcat_s(cmd, sizeof(cmd), " AND SKUN = ");
			cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSkun);
			break;
		case 1:
			strcat_s(cmd, sizeof(cmd), " AND (SKUN >= ");
			cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSkun);
			strcat_s(cmd, sizeof(cmd), " OR ICNT = 1)");
			strcat_s(cmd, sizeof(cmd), " ORDER BY ICNT, SKUN ");
			break;
		case 2:
			strcat_s(cmd, sizeof(cmd), " AND SKUN >= ");
			cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSkun);
			strcat_s(cmd, sizeof(cmd), " ORDER BY SKUN ");
			break;
	}

	if (cn.DoSQLCommand(cmd)) {
		if (cn.DoSQLFetch()) {
			vReadHhtDet(sessionNumber);

			if (option==2) {
				// If Counted then Return Retry for Next SKU
				if (user->pcIcnt[0] != '0') {
					strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "2");
					// Increment key for next HHTDET SKU
					sprintf_s(user->pcSkun, sizeof(user->pcSkun), "%06d", atoi(user->pcSkun) + 1);
					return(1);
				}
			}
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
			return(1);
		}
	}
	strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
	return(0);
}
/***************************************************************************
Function:    Read next HHTDET Database
Description: Read next record in the HHT detail btrieve database file
Passed:      User Number
Return:      0 = Failed, 1 = Success,
LastInput:  "0" = Failed, else "1" = Success,
****************************************************************************/
int ReadNextHhtDetRecord(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	if (cn.DoSQLFetch(SQL_FETCH_NEXT) || cn.DoSQLFetch(SQL_FD_FETCH_FIRST)) {
		vReadHhtDet(sessionNumber);
		strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
		return 1;
	}

	strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
	return 0;
}
/***************************************************************************
Function:    Update HHTDET Database
Description: Update record in the HHT detail btrieve database file
Passed:      User Number
Return:      0  = Failed, 1  = Success,
LastInput:  "0" = Failed, else "1" = Success,
****************************************************************************/
int iUpdateHhtDetDatabase(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];
	
// vvv holding fields for existing record
	int  sCnt[10]; // shelf Count
	int  tSCn;     // total shelf Count
	int  wCnt[10]; // warehouse Count
	int  tWCn;     // total warehouse Count
	int  tPre; 
	int  tCnt;
	bool iCnt;
	int  mDnC[3];
	int  tMDC;
	bool lbOk;
// ^^^

	int i;
	int  thisCount;
	char s[21];
	char hhtLocationCmd[1024];

	char cmd[1024] = "SELECT "
										 "SCNT1, SCNT2, SCNT3, SCNT4, SCNT5, SCNT6, SCNT7, SCNT8, SCNT9, SCNT10, TSCN, "
										 "WCNT1, WCNT2, WCNT3, WCNT4, WCNT5, WCNT6, WCNT7, WCNT8, WCNT9, WCNT10, TWCN, "
										 "TPRE, TCNT, ICNT, MDNC1, MDNC2, MDNC3, TMDC, LBOK "
									 "FROM HHTDET ";
	strcat_s(cmd, sizeof(cmd), "WHERE DATE1 = ");	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcDate);
	strcat_s(cmd, sizeof(cmd), " AND SKUN = "); 	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSkun);

	if (!cn.DoSQLCommand(cmd) || !cn.DoSQLFetch())  {
		strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0"); // Epic FAIL
		return(0);
	}

	int columnIndex = 1;

	for (i = 0; i < 10; i++) {
		cn.SQLGetColumn(columnIndex++, SQL_C_SLONG, &sCnt[i], 0);
	}
	cn.SQLGetColumn(columnIndex++, SQL_C_SLONG, &tSCn, 0);

	for (i = 0; i < 10; i++) {
		cn.SQLGetColumn(columnIndex++, SQL_C_SLONG, &wCnt[i], 0);
	}
	cn.SQLGetColumn(columnIndex++, SQL_C_SLONG, &tWCn, 0);

	cn.SQLGetColumn(columnIndex++, SQL_C_SLONG, &tPre, 0);
	cn.SQLGetColumn(columnIndex++, SQL_C_SLONG, &tCnt, 0);
	cn.SQLGetColumn(columnIndex++, SQL_C_BIT, &iCnt, 0);

	for (i = 0; i < 3; i++) {
		cn.SQLGetColumn(columnIndex++, SQL_C_SLONG, &mDnC[i], 0);
	}
	cn.SQLGetColumn(columnIndex++, SQL_C_LONG, &tMDC, 0);
	cn.SQLGetColumn(columnIndex++, SQL_C_BIT, &lbOk, 0);

	strcpy_s(cmd, sizeof(cmd), "UPDATE HHTDET SET ");

	if (user->iFlag) {
		if (iCnt) {
			user->iFlag = 0;
		}
		strcat_s(cmd, sizeof(cmd), "ICNT = 1, ");

		// Counted something. Output HHTLocation records for all locations.
		strcpy_s(hhtLocationCmd, sizeof(hhtLocationCmd), "INSERT INTO HHTLocation (IsDeleted, IslandId, IslandSegment, PlanogramId, IslandType, Sequence, DateCreated, SkuNumber, StockCount) ");
		strcat_s(hhtLocationCmd, sizeof(hhtLocationCmd), "VALUES (0, 0, 0, 0, 'S', Coalesce((SELECT MAX(Sequence) FROM HHTLocation WHERE IslandId = 0 AND IslandSegment = 0 AND PlanoGramId = 0 AND IslandType = 'S' AND SkuNumber = ");
		cn.SQLQueryAddVarChar(hhtLocationCmd, sizeof(hhtLocationCmd), user->pcSkun); // used to get next sequence number
		strcat_s(hhtLocationCmd, sizeof(hhtLocationCmd), "), 0) + 1,");
		cn.SQLQueryAddVarChar(hhtLocationCmd, sizeof(hhtLocationCmd), user->pcDate); // DateCreated
		strcat_s(hhtLocationCmd, sizeof(hhtLocationCmd), ", ");
		cn.SQLQueryAddVarChar(hhtLocationCmd, sizeof(hhtLocationCmd), user->pcSkun); // SkuNumber
		strcat_s(hhtLocationCmd, sizeof(hhtLocationCmd), ", ");
		_itoa_s(atol(user->pcTscn) - tSCn, s, sizeof(s), 10);
		strcat_s(hhtLocationCmd, sizeof(hhtLocationCmd), s); // StockCount
		strcat_s(hhtLocationCmd, sizeof(hhtLocationCmd), ")");

		if (!cn.DoSQLCommand(hhtLocationCmd)) {
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput),"0");
			return(0);
		}

		strcpy_s(hhtLocationCmd, sizeof(hhtLocationCmd), "INSERT INTO HHTLocation (IsDeleted, IslandId, IslandSegment, PlanogramId, IslandType, Sequence, DateCreated, SkuNumber, StockCount) ");
		strcat_s(hhtLocationCmd, sizeof(hhtLocationCmd), "VALUES (0, 0, 0, 0, 'W', Coalesce((SELECT MAX(Sequence) FROM HHTLocation WHERE IslandId = 0 AND IslandSegment = 0 AND PlanoGramId = 0 AND IslandType = 'W' AND SkuNumber = ");
		cn.SQLQueryAddVarChar(hhtLocationCmd, sizeof(hhtLocationCmd), user->pcSkun); // used to get next sequence number
		strcat_s(hhtLocationCmd, sizeof(hhtLocationCmd), "), 0) + 1,");
		cn.SQLQueryAddVarChar(hhtLocationCmd, sizeof(hhtLocationCmd), user->pcDate); // DateCreated
		strcat_s(hhtLocationCmd, sizeof(hhtLocationCmd), ", ");
		cn.SQLQueryAddVarChar(hhtLocationCmd, sizeof(hhtLocationCmd), user->pcSkun); // SkuNumber
		strcat_s(hhtLocationCmd, sizeof(hhtLocationCmd), ", ");
		_itoa_s(atol(user->pcTwcn) - tWCn, s, sizeof(s), 10);
		strcat_s(hhtLocationCmd, sizeof(hhtLocationCmd), s); // StockCount
		strcat_s(hhtLocationCmd, sizeof(hhtLocationCmd), ")");

		if (!cn.DoSQLCommand(hhtLocationCmd)) {
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput),"0");
			return(0);
		}

		strcpy_s(hhtLocationCmd, sizeof(hhtLocationCmd), "INSERT INTO HHTLocation (IsDeleted, IslandId, IslandSegment, PlanogramId, IslandType, Sequence, DateCreated, SkuNumber, StockCount) ");
		strcat_s(hhtLocationCmd, sizeof(hhtLocationCmd), "VALUES (0, 0, 0, 0, 'M', Coalesce((SELECT MAX(Sequence) FROM HHTLocation WHERE IslandId = 0 AND IslandSegment = 0 AND PlanoGramId = 0 AND IslandType = 'M' AND SkuNumber = ");
		cn.SQLQueryAddVarChar(hhtLocationCmd, sizeof(hhtLocationCmd), user->pcSkun); // used to get next sequence number
		strcat_s(hhtLocationCmd, sizeof(hhtLocationCmd), "), 0) + 1,");
		cn.SQLQueryAddVarChar(hhtLocationCmd, sizeof(hhtLocationCmd), user->pcDate); // DateCreated
		strcat_s(hhtLocationCmd, sizeof(hhtLocationCmd), ", ");
		cn.SQLQueryAddVarChar(hhtLocationCmd, sizeof(hhtLocationCmd), user->pcSkun); // SkuNumber
		strcat_s(hhtLocationCmd, sizeof(hhtLocationCmd), ", ");
		_itoa_s(atol(user->pcTmdc) - tMDC, s, sizeof(s), 10);
		strcat_s(hhtLocationCmd, sizeof(hhtLocationCmd), s); // StockCount
		strcat_s(hhtLocationCmd, sizeof(hhtLocationCmd), ")");

		if (!cn.DoSQLCommand(hhtLocationCmd)) {
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput),"0");
			return(0);
		}

	}
	
	//cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcDate);
	//strcat_s(cmd, sizeof(cmd), 0);
	//cn.SQLQueryAddVarChar(cmd, sizeof(cmd), 0);

	thisCount = atoi(user->pcTscn);
	if (tSCn != thisCount) {
		strcat_s(cmd, sizeof(cmd), "TSCN = ");
		strcat_s(cmd, sizeof(cmd), user->pcTscn);
		strcat_s(cmd, sizeof(cmd), ", ");

		thisCount = thisCount - tSCn; // thisCount is now the difference
		for (i = 0; i < 9; i++) {
			if (sCnt[i] == 0) {
				break;
			}
		}
		sCnt[i] += thisCount;
		strcat_s(cmd, sizeof(cmd), "SCNT");
		_itoa_s(i + 1, s, sizeof(s), 10);
		strcat_s(cmd, sizeof(cmd), s);
		strcat_s(cmd, sizeof(cmd), " = ");
		_itoa_s(sCnt[i], s, sizeof(s), 10);
		strcat_s(cmd, sizeof(cmd), s);
		strcat_s(cmd, sizeof(cmd), ", ");
	}

	thisCount = atoi(user->pcTwcn);
	if (tWCn != thisCount) {
		strcat_s(cmd, sizeof(cmd), "TWCN = ");
		strcat_s(cmd, sizeof(cmd), user->pcTwcn);
		strcat_s(cmd, sizeof(cmd), ", ");

		thisCount = thisCount - tWCn; // thisCount is now the difference
		for (i = 0; i < 9; i++) {
			if (wCnt[i] == 0) {
				break;
			}
		}
		wCnt[i] += thisCount;
		strcat_s(cmd, sizeof(cmd), "WCNT");
		_itoa_s(i + 1, s, sizeof(s), 10);
		strcat_s(cmd, sizeof(cmd), s);
		strcat_s(cmd, sizeof(cmd), " = ");
		_itoa_s(wCnt[i], s, sizeof(s), 10);
		strcat_s(cmd, sizeof(cmd), s);
		strcat_s(cmd, sizeof(cmd), ", ");
	}

	thisCount = atoi(user->pcTmdc);
	if (tMDC != thisCount) {
		strcat_s(cmd, sizeof(cmd), "TMDC = ");
		strcat_s(cmd, sizeof(cmd), user->pcTmdc);
		strcat_s(cmd, sizeof(cmd), ", ");

		thisCount = thisCount - tMDC; // thisCount is now the difference
		for (i = 0; i < 2; i++) {
			if (mDnC[i] == 0) {
				break;
			}
		}
		mDnC[i] += thisCount;
		strcat_s(cmd, sizeof(cmd), "MDNC");
		_itoa_s(i + 1, s, sizeof(s), 10);
		strcat_s(cmd, sizeof(cmd), s);
		strcat_s(cmd, sizeof(cmd), " = ");
		_itoa_s(mDnC[i], s, sizeof(s), 10);
		strcat_s(cmd, sizeof(cmd), s);
		strcat_s(cmd, sizeof(cmd), ", ");
	}



	strcat_s(cmd, sizeof(cmd), "TPRE = ");
	strcat_s(cmd, sizeof(cmd), user->pcTpre);
	strcat_s(cmd, sizeof(cmd), ", ");

	strcat_s(cmd, sizeof(cmd), "TCNT = ");
	strcat_s(cmd, sizeof(cmd), user->pcTcnt);
	strcat_s(cmd, sizeof(cmd), ", ");

	strcat_s(cmd, sizeof(cmd), "LBOK = ");
	strcat_s(cmd, sizeof(cmd), !memcmp(user->pcPicLabelOk, "YES", 2) ? "1" : "0");

#pragma region count flag
/*
	BTI_CHAR keyBuf[KEY_MAX_LENGTH];
	int  iPut;
  long lTempLong;
  long lTempLong2;
  long lTempPrev;
  BTI_SINT iDataBaseStatus;
  // GET THE RECORD WITH GET_EQUAL AND UPDATE IT
	GetDBLongDateToDBField(keyBuf,&sUser[i].dDateHdr);
	GetStrToDBCharField(keyBuf+sizeof(DBLongDate),sUser[i].pcSkun,sizeof(hhtdet.skun));
	iDataBaseStatus=DBAccessTableData(&dbtHhtDet,B_GET_EQUAL | S_NOWAIT_LOCK,keyBuf,0);
  if (iDataBaseStatus == B_NO_ERROR ){
	  GetLongToDBDecField(hhtdet.tscn, lTempLong, sizeof(hhtdet.tscn));
	  if (sUser[i].iFlag) { // Update Data & header flag
		  if (hhtdet.icnt[0]!=0)  // test if Icnt set
				sUser[i].iFlag = 0; // Reset update header flag
      hhtdet.icnt[0] = 32; // If SF entered set Icnt 32=BLANK=True
    }
*/
#pragma endregion
#pragma region shop floor
/*    // First update the Shop Floor Location
	  lTempPrev=GetDBDecFieldToLong(hhtdet.tscn, sizeof(hhtdet.tscn));
    lTempLong=atol(sUser[i].pcTscn);
		if ((lTempLong - lTempPrev) != 0) {
      lTempPrev = lTempLong - lTempPrev;		// Record difference only
		  for (iPut=0; iPut<10*sizeof(hhtdet.tscn); iPut+=sizeof(hhtdet.tscn)) {
				if (GetDBDecFieldToLong(hhtdet.scnt+iPut,sizeof(hhtdet.tscn)) == 0L) {
					break;
				}
		  }
      if (iPut>9*sizeof(hhtdet.tscn)) {	// Add to last Field
        iPut=9*sizeof(hhtdet.tscn);
			  lTempLong2=GetDBDecFieldToLong(hhtdet.scnt+iPut,sizeof(hhtdet.tscn));
        lTempLong2 = lTempLong2 + lTempPrev; // Add
			  GetLongToDBDecField(hhtdet.scnt+iPut,lTempLong2,sizeof(hhtdet.tscn));
      } else  {// Change Field
	 			GetLongToDBDecField(hhtdet.scnt+iPut,lTempPrev,sizeof(hhtdet.tscn));
		  }
	  }
*/
#pragma endregion
#pragma region warehouse
/*
		lTempPrev=GetDBDecFieldToLong(hhtdet.twcn, sizeof(hhtdet.twcn));
    lTempLong=atol(sUser[i].pcTwcn);
	  GetLongToDBDecField(hhtdet.twcn,lTempLong,sizeof(hhtdet.twcn));
    if ((lTempLong - lTempPrev) != 0) {
      lTempPrev = lTempLong - lTempPrev;		// Record difference only
			for (iPut=0; iPut<10*sizeof(hhtdet.twcn); iPut+=sizeof(hhtdet.twcn)) {
				if (GetDBDecFieldToLong(hhtdet.wcnt+iPut,sizeof(hhtdet.twcn)) == 0L) {
					break;
				}
			}
      if (iPut>9*sizeof(hhtdet.twcn)) {	// Add to last Field
        iPut=9*sizeof(hhtdet.twcn);
				lTempLong2=GetDBDecFieldToLong(hhtdet.wcnt+iPut,sizeof(hhtdet.twcn));
        lTempLong2 = lTempLong2 + lTempPrev; // Add
				GetLongToDBDecField(hhtdet.wcnt+iPut,lTempLong2,sizeof(hhtdet.twcn));
      } else  {// Change Field
  			GetLongToDBDecField(hhtdet.wcnt+iPut,lTempPrev,sizeof(hhtdet.twcn));
			}
		}
*/
#pragma endregion
#pragma region markdown count
/*
	  // Next update the MarkDown Count
	  lTempPrev=GetDBDecFieldToLong(hhtdet.tmdc, sizeof(hhtdet.tmdc));
    lTempLong=atol(sUser[i].pcTmdc);
	  GetLongToDBDecField(hhtdet.tmdc,lTempLong,sizeof(hhtdet.tmdc));
    if ((lTempLong - lTempPrev) != 0) {
      lTempPrev = lTempLong - lTempPrev;		// Record difference only
		  for (iPut=0; iPut<3*sizeof(hhtdet.tmdc); iPut+=sizeof(hhtdet.tmdc)) {
				if (GetDBDecFieldToLong(hhtdet.mdnc+iPut,sizeof(hhtdet.tmdc)) == 0L) {
					break;
				}
		  }
			if (iPut>2*sizeof(hhtdet.tmdc)) {	// Add to last Field
	      iPut=2*sizeof(hhtdet.tmdc);
				lTempLong2=GetDBDecFieldToLong(hhtdet.mdnc+iPut,sizeof(hhtdet.tmdc));
				lTempLong2 = lTempLong2 + lTempPrev; // Add
				GetLongToDBDecField(hhtdet.mdnc+iPut,lTempLong2,sizeof(hhtdet.tmdc));
			} else  {// Change Field
	   		GetLongToDBDecField(hhtdet.mdnc+iPut,lTempPrev,sizeof(hhtdet.tmdc));
			}
	  }
*/
#pragma endregion
#pragma region the rest
	/*
    // Next update the Presold Stock Location
	  GetLongToDBDecField(hhtdet.tpre,atol(sUser[i].pcTpre),sizeof(hhtdet.tpre));
	  // Next update the Total Stock
	  GetLongToDBDecField(hhtdet.tcnt,atol(sUser[i].pcTcnt),sizeof(hhtdet.tcnt));
		if (memcmp(sUser[i].pcPicLabelOk,"NO",2) == 0) {
			hhtdet.lbok[0] = 0;
		}
  	iDataBaseStatus=DBAccessTableData(&dbtHhtDet,B_UPDATE,keyBuf,0);
  	iDataBaseStatus=DBAccessTableData(&dbtHhtDet,B_UNLOCK,keyBuf,0);
  }
  if ( iDataBaseStatus == B_NO_ERROR ) {
    strcpy(sUser[i].pcLastInput, "1");
    return(1);
  }
  strcpy(sUser[i].pcLastInput, "0");
  return(0);
*/
#pragma endregion

	strcat_s(cmd, sizeof(cmd), " WHERE DATE1 = ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcDate);
	strcat_s(cmd, sizeof(cmd), " AND SKUN = ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSkun);

	if (cn.DoSQLCommand(cmd)) {
		strcpy_s(user->pcLastInput, sizeof(user->pcLastInput),"1");
		return(1); // Insert Passed
	}
  strcpy_s(user->pcLastInput, sizeof(user->pcLastInput),"0");
  return(0);
}
//----------------------------------------------------------------------------
//	HHTHDR Table Functions
//----------------------------------------------------------------------------
void vReadHhtHdr(int sessionNumber)
// Purpose: Moves table fields to the global user variables
//   Usage: Standard routine used by the other Hhthdr routines
{
	USER_TYPE *user = &sUser[sessionNumber];
	
	SQL_DATE_STRUCT workDate;
	cn.SQLGetColumn( 1, SQL_C_TYPE_DATE, &workDate, sizeof(workDate));
	user->dDateHdr.day = (char)workDate.day;
	user->dDateHdr.month = (char)workDate.month;
	user->dDateHdr.year = workDate.year;
	sprintf_s(user->pcDate, sizeof(user->pcDate), "%d-%d-%d", user->dDateHdr.year, user->dDateHdr.month, user->dDateHdr.day);

	cn.SQLGetColumn( 2, SQL_C_CHAR, user->pcNumb, 17);
	cn.SQLGetColumn( 3, SQL_C_CHAR, user->pcDone, 9);
	sprintf_s(user->pcToDo, sizeof(user->pcToDo), "%d", atoi(user->pcNumb) - atoi(user->pcDone));

	cn.SQLGetColumn( 4, SQL_C_CHAR, user->pcIadj, 3);
	cn.SQLGetColumn( 5, SQL_C_CHAR, user->pcAuth, 9);

	fprintf_s(ProcessLogHandle(), "**HHTHDR: Date=%s, Numb=%s, Done=%s, ToDo=%s, Iadj=%s, Auth=%s\n",
										user->pcDate, user->pcNumb, user->pcDone, user->pcToDo, user->pcIadj, user->pcAuth);
	strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), user->pcSkun);
/*


	GetDBLongDateFieldToStr(sUser[i].pcDate, &hhthdr->date);
	memcpy(&sUser[i].dDateHdr,&hhthdr->date,sizeof(DBLongDate));
	GetDBDecFieldToStr(sUser[i].pcNumb, hhthdr->numb, sizeof(hhthdr->numb), 0);
	GetDBDecFieldToStr(sUser[i].pcDone, hhthdr->done, sizeof(hhthdr->done), 0);
	sprintf_s(sUser[i].pcToDo, "%6ld",atol(sUser[i].pcNumb)-atol(sUser[i].pcDone));
	GetDBLogicalFieldToStr(sUser[i].pcIadj, hhthdr->iadj);
	GetDBCharFieldToStr(sUser[i].pcAuth, hhthdr->auth, sizeof(hhthdr->auth));

	fprintf(ProcessLogHandle(),"**HHTHDR: Date=%s, Numb=%s, Done=%s, ToDo=%s, Iadj=%s, Auth=%s\n",
										sUser[i].pcDate,sUser[i].pcNumb,sUser[i].pcDone,sUser[i].pcToDo,sUser[i].pcIadj,sUser[i].pcAuth);

	strcpy(sUser[i].pcLastInput, sUser[i].pcSkun);
*/
}
//----------------------------------------------------------------------------
int iUpdateHhtHdrDatabase(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	if (SeekLastHhtHdrDatabase(sessionNumber)) {

		if (atoi(user->pcToDo) <= 0) {
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
			return(1);						// Seek Passed && Todo = 0 So Don't Update
		}
		if (user->iFlag) {
			char cmd[256] = "UPDATE HHTHDR SET DONE = ";
			strcat_s(cmd, sizeof(cmd), user->pcDone);
			strcat_s(cmd, sizeof(cmd), " + 1");

			strcat_s(cmd, sizeof(cmd), " WHERE DATE1 = ");
			cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcDate);

			if (!cn.DoSQLCommand(cmd)) {
				strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
				return(0);
			}
		}

		if (SeekLastHhtHdrDatabase(sessionNumber)) {
			if (atoi(user->pcToDo) <= 0) {
				strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
				return(1);						// Seek Passed && Todo = 0 So return Done
			} 
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1" );
			return(1);
		}
	}

/*
BTI_CHAR keyBuf[21];
	//long lTempLong;
  BTI_SINT iDataBaseStatus = B_NO_ERROR;

	// GET THE RECORD WITH GET_EQUAL AND UPDATE IT
	iDataBaseStatus=DBAccessTableData(&dbtHhtHdr,B_GET_LAST,keyBuf,0);
  if (iDataBaseStatus == B_NO_ERROR) {
    vReadHhtHdr(&hhthdr, i);
    if( atoi(sUser[i].pcToDo) > 0) {
			strcpy(sUser[i].pcLastInput, "1");
		} else {
		  strcpy(sUser[i].pcLastInput, "0");
		  return(1);						// Seek Passed && Todo = 0 So Don't Update
    }
		if (sUser[i].iFlag) {		// Update Data & header flag set
			GetLongToDBDecField(hhthdr.done, atol(sUser[i].pcDone)+1,sizeof(hhthdr.done));
			ProcessLogWriteLn("(UPDATE HHTHDR - FLAG SET, done++)");
		} else {
			ProcessLogWriteLn("(UPDATE HHTHDR - FLAG NOT SET)");
		}
		iDataBaseStatus=DBAccessTableData(&dbtHhtHdr,B_UPDATE,keyBuf,0);
		iDataBaseStatus=DBAccessTableData(&dbtHhtHdr,B_UNLOCK,keyBuf,0);
	}
  if (iDataBaseStatus == B_NO_ERROR) {
		if (atoi(sUser[i].pcToDo) == 0) {					//W3.01
      strcpy(sUser[i].pcLastInput, "0");
      return(1) ;     // Seek Passed && Todo = 0 So Don't Update
    }
    strcpy(sUser[i].pcLastInput, "1" );
    return(1);
    }
  strcpy(sUser[i].pcLastInput, "0");
  return(0);
*/
  strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0"); // error occurred
	return(0);
}

//----------------------------------------------------------------------------

/***************************************************************************
Function:    Seek latest HHTHDR Database
Description: Special Seek the last record in the HHT header
             using the date field btrieve database file
Passed:      User Number
Return:      0  = Failed,  1  = Success,
LastInput:  "0" = Failed, "1" = Success,
****************************************************************************/
int SeekLastHhtHdrDatabase(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];
	
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Author      : Partha Dutta
    // Date        : 13/10/2010
    // Referral No : 299
    // Notes       : Use the system date held in sysdat->tmdt and not the latest date entry in HHTHDR
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//get system date
    char cmdSYSDAT[255] = "select tmdt from SYSDAT";
    char FormattedDate[12];

	char cmdHHTHDR[255];
	char SqlDate[12];

	if (cn.DoSQLCommand(cmdSYSDAT))
	{
        if (cn.DoSQLFetch())
		{
            SQL_DATE_STRUCT workDate;
            cn.SQLGetColumn( 1, SQL_C_TYPE_DATE, &workDate, sizeof(workDate));
            user->SystemDate.day   = (char)workDate.day;
            user->SystemDate.month = (char)workDate.month;
            user->SystemDate.year  = workDate.year;

			//log entry
			sprintf_s(FormattedDate, sizeof(FormattedDate), "%04d-%02d-%02d", user->SystemDate.year, user->SystemDate.month, user->SystemDate.day);
            fprintf_s(ProcessLogHandle(), "**SYSDAT: Date=%s \n", FormattedDate);
        }
		else
		{
            //failed to retrieve system date - no record found
            fprintf_s(ProcessLogHandle(), "**SYSDAT: Failed to find record \n");

			return 0;
		}
    }

    //use system date to retrive data
	strcpy_s(cmdHHTHDR, sizeof(cmdHHTHDR), "SELECT DATE1, NUMB, DONE, IADJ, AUTH FROM HHTHDR WHERE DATE1=");
    sprintf_s(SqlDate, sizeof(SqlDate), "%04d-%02d-%02d", user->SystemDate.year, user->SystemDate.month, user->SystemDate.day);
	cn.SQLQueryAddVarChar(cmdHHTHDR, sizeof(cmdHHTHDR), SqlDate);

	if (cn.DoSQLCommand(cmdHHTHDR)) {
		if (cn.DoSQLFetch()) {
			vReadHhtHdr(sessionNumber);
			if (user->pcIadj == "1") {
			  strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "2"); // No detail records
			} else if (atol(user->pcToDo) > 0) {
			  strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1"); // More to do
			} else {
			  strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "3"); // Done
			}
			return 1;
		}
	}
	strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
	return 0;
}
/***************************************************************************
Function:    Read next HHTHDR Database
Description: Read next record in the HHT header btrieve database file
Passed:      User Number
Return:      0  = Failed,  1  = Success,
LastInput:  "0" = Failed, "1" = Success,
****************************************************************************/
//----------------------------------------------------------------------------

int ReadNextHhtHdrRecord(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	if (cn.DoSQLFetch(SQL_FETCH_NEXT) || cn.DoSQLFetch(SQL_FD_FETCH_FIRST)) {
		vReadHhtHdr(sessionNumber);
		strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
		return 1;
	}

	strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
	return 0;
}
/***************************************************************************
Function:    Seek Price Change Database
Description: Seek the (Price Change Start Date) in STKLOG
			 (Using Current Day Number)
Passed:      User Number
             Options 0 = Last Input, 1 = Skun
Return:      0 = Failed, 1 = Success,
LastInput:   None
****************************************************************************/
/*
int iSeekPriceChangeDate( int i )
{
	BTI_CHAR keyBuf[21];
  BTI_SINT iDataBaseStatus = B_NO_ERROR;
  long lDayNumber;
  char slSkun[7];

	memset( keyBuf, 0, sizeof( keyBuf ) );
  // Initialise SKUN Key
  memset( slSkun, 0, sizeof(slSkun) );
  memset( slSkun, '0', sizeof(slSkun)-1);
  // Seek the SKU record
	lDayNumber=DateStrToDayNumber(GetCurrentDateInDDMMYYYY(NULL))-2;
	GetLongToDBDecField(keyBuf,lDayNumber,sizeof(stklog.dayn));
	GetStrToDBCharField(keyBuf+sizeof(stklog.dayn),slSkun,sizeof(stklog.skun));
	iDataBaseStatus=DBAccessTableData(&dbtStkLog,B_GET_GE,keyBuf,3);
  if (iDataBaseStatus==B_NO_ERROR) {
    vReadStkLog(&stklog, i);
		return(1);         // Seek Passed
	}
  return(0);    // Seek Failed
}
*/
/***************************************************************************
Function:    Unlock STKMAS Database
Description: Unlock Current User SKU in Stock btrieve database file
Passed:      User Number
Return:      0  = Failed, 1  = Success,
LastInput:  "0" = Failed, else "1" = Success,
****************************************************************************/
int iUnLockStkMasDatabase( int sessionNumber ) // not aupported
{
/*
BTI_CHAR keyBuf[21];
  BTI_SINT iDataBaseStatus = B_NO_ERROR;

	// UNLOCKE RECORD WITH GET_EQUAL AND UPDATE IT
  memset( &stkmas, 0, sizeof(stkmas) );
	GetStrToDBCharField(keyBuf,sUser[i].pcSkun,sizeof(stkmas.skun));
	iDataBaseStatus=DBAccessTableData(&dbtStkMas,B_UNLOCK,keyBuf,0);
  if ( iDataBaseStatus == B_NO_ERROR ) {
    strcpy(sUser[i].pcLastInput, "1");
    return(1); // Unlock Passed
	  }
	strcpy(sUser[i].pcLastInput, "0");
	return(0); // UnLock Failed
*/
	return(0);
}
