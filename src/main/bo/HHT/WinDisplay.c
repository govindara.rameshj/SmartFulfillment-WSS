//---------------------------------------------------------------------------
// Application: Price Checker Server and Client for Windows Socket Services
//							Developed for Atlantic Homecare BSD ATL702.
// Project: PriceCheckerServer and Client - Developed with MS Visual Studio
//						2003, C++
// --------------------------------------------------------------------------
// Module: WinDisplay.c - Display routines.
//						Manages rectangular areas on the client window. Provides border,
//						Erase, and text resizing and display
// --------------------------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <windows.h>
#include <winerror.h>
#include <winGDI.h>
#include "WinDisplay.h"
// --------------------------------------------------------------------------
// Local Data Structures
// --------------------------------------------------------------------------
#ifdef WIN_CE		// Compiling for Windows CE (the client)

#define FONT_TYPE SYSTEM_FONT
#define BORDER_SIZE 2

#else
#define FONT_TYPE ANSI_FIXED_FONT
#define BORDER_SIZE 10

#endif

static WinDisplayFrame near curWf;	// Current area information
static HFONT hFnt,hOldFnt;					// Holds fonts between begin and end paint
static double _fontSizeMultiplier=1.0;	// Fond resizing
// --------------------------------------------------------------------------
// Local Function Prototypes
// --------------------------------------------------------------------------
static void SetArea(WinDisplayFrame *passedWf);
static void ReturnArea(WinDisplayFrame *passedWf);
static void SetupFont(void);
static void ActivateFrame(WinDisplayFrame *Wf);
static void DeactivateFrame(WinDisplayFrame *Wf);
static void DisplayTextAtXY(int x, int y, char *str);
static void DisplayTextAtColRow(int charCol, int charRow, char *str);

// --------------------------------------------------------------------------
// Local Functions
// --------------------------------------------------------------------------
static void SetArea(WinDisplayFrame *passedWf)
{
	if (passedWf!=NULL) {
		memcpy(&curWf,passedWf,sizeof(WinDisplayFrame));
	}
}
// --------------------------------------------------------------------------
static void ReturnArea(WinDisplayFrame *passedWf)
{
	if (passedWf!=NULL) {
		memcpy(passedWf,&curWf,sizeof(WinDisplayFrame));
	}
}
// --------------------------------------------------------------------------
static void SetupFont()
{
	TEXTMETRIC tm;
	LOGFONT lf;  // structure for font information
	HFONT hStockFont;

	hStockFont = (HFONT)(GetStockObject(FONT_TYPE));			// Get a fixed font
  hOldFnt = (HFONT)(SelectObject(curWf.hdc, hStockFont));
	GetTextMetrics(curWf.hdc,&tm);						// Get its size
	memset(&lf,0,sizeof(LOGFONT));
	if (curWf.fontSizeMultiplier!=1.0) {
		int i;
		for (i=0 ; i<10 ; i++) {
			lf.lfWeight=FW_SEMIBOLD;
			lf.lfWidth=(long)(tm.tmAveCharWidth*curWf.fontSizeMultiplier);
			lf.lfHeight=(long)(tm.tmHeight*curWf.fontSizeMultiplier);
			if ((hFnt=CreateFontIndirect(&lf))!=NULL) {
				SelectObject(curWf.hdc,hFnt);				// Select it to DC
				break;
			}
			if (curWf.fontSizeMultiplier>1) {
				curWf.fontSizeMultiplier-=0.10;			// decrease size
			} else {
				curWf.fontSizeMultiplier+=0.10;			// increase size
			}
		}
	}
	GetTextMetrics(curWf.hdc,&tm);						// Get what CE actually made
	curWf.charWidth=tm.tmAveCharWidth;
	curWf.charHeight=tm.tmHeight;
	curWf.width=curWf.displayArea.right-curWf.displayArea.left;
	curWf.height=curWf.displayArea.bottom-curWf.displayArea.top;
	curWf.cols=curWf.width/curWf.charWidth;
	curWf.rows=curWf.height/curWf.charHeight;
	DeleteObject(hStockFont);

}
// --------------------------------------------------------------------------
static void ActivateFrame(WinDisplayFrame *Wf)
{
	SetArea(Wf);
	if (curWf.beginDrawCounter==0) {
		// Get Text rows and cols
		curWf.hdc=GetDC(curWf.hWnd);
		SetupFont();
	}
	curWf.beginDrawCounter++;
	ReturnArea(Wf);
}
// --------------------------------------------------------------------------
static void DeactivateFrame(WinDisplayFrame *Wf)
{

	SetArea(Wf);
	if (curWf.beginDrawCounter>0) {
		curWf.beginDrawCounter--;
		if (curWf.beginDrawCounter==0) {
		  SelectObject(curWf.hdc,hOldFnt);
			DeleteObject(hFnt);
			ReleaseDC(curWf.hWnd,curWf.hdc);
		}
	}
	ReturnArea(Wf);
}
// --------------------------------------------------------------------------
static void DisplayTextAtXY(int x, int y, char *str)
{
	HPEN hpen, hpenOld;
#ifdef WIN_CE		// Compiling for Windows CE
	LPWSTR wStr;
#endif

	hpen = CreatePen(PS_SOLID,2,RGB(255,255,255));
  hpenOld = (HPEN)(SelectObject(curWf.hdc, hpen));

	SetTextAlign(curWf.hdc,TA_UPDATECP);
	MoveToEx(curWf.hdc,x,y,NULL);

#ifdef WIN_CE		// Compiling for Windows CE

	wStr=WinDisplay_StrTowStrPtr(str);
	DrawText(curWf.hdc,wStr,strlen(str), &curWf.displayArea,0);
	free(wStr);
#else
	DrawText(curWf.hdc,str,-1, &curWf.displayArea,0);
#endif
  SelectObject(curWf.hdc, hpenOld);
  DeleteObject(hpen);

}
// --------------------------------------------------------------------------
static void DisplayTextAtColRow(int charCol, int charRow, char *str)
{
	DisplayTextAtXY(curWf.displayArea.left+charCol*curWf.charWidth,
											 curWf.displayArea.top+charRow*curWf.charHeight,str);

}
// --------------------------------------------------------------------------
// Global Functions
// --------------------------------------------------------------------------
void WinDisplay_SetFontSizeMultiplier(double value)
{
	_fontSizeMultiplier=value;
}
// --------------------------------------------------------------------------
void WinDisplay_CreateFrame(WinDisplayFrame *retWf,HWND hWnd, RECT *areaSize)
{
	memset(&curWf,0,sizeof(WinDisplayFrame));
	curWf.hWnd=hWnd;
	// Set frame size
	memcpy(&curWf.frameArea,areaSize,sizeof(RECT));
	memcpy(&curWf.borderArea,areaSize,sizeof(RECT));
	// Set border size
	curWf.borderArea.left+=BORDER_SIZE/2;
	curWf.borderArea.right-=BORDER_SIZE/2;
	curWf.borderArea.top+=BORDER_SIZE/2;
	curWf.borderArea.bottom-=BORDER_SIZE/2;
	// Get Text area size
	memcpy(&curWf.displayArea,areaSize,sizeof(RECT));
	curWf.displayArea.left+=BORDER_SIZE;
	curWf.displayArea.right-=BORDER_SIZE;
	curWf.displayArea.top+=BORDER_SIZE;
	curWf.displayArea.bottom-=BORDER_SIZE;
	// Get Text rows and cols
	curWf.fontSizeMultiplier=_fontSizeMultiplier;
	curWf.hdc=GetDC(curWf.hWnd);
	SetupFont();
	ReleaseDC(curWf.hWnd,curWf.hdc);
	ReturnArea(retWf);
}
// --------------------------------------------------------------------------
void WinDisplay_ResizeFrame(WinDisplayFrame *Wf, RECT *newArea)
{
	WinDisplay_CreateFrame(Wf,Wf->hWnd,newArea);
}
// --------------------------------------------------------------------------
void WinDisplay_EraseFrame(WinDisplayFrame *Wf)
{
  HPEN hpen, hpenOld;
  HBRUSH hbrush, hbrushOld;
	//int i;

	ActivateFrame(Wf);
  hpen = CreatePen(PS_SOLID,BORDER_SIZE,RGB(255,255,255));
  hbrush = CreateSolidBrush(RGB(255,255,255));

  // Select the new pen and brush, and then draw.
  hpenOld = (HPEN)(SelectObject(curWf.hdc, hpen));
  hbrushOld = (HBRUSH)(SelectObject(curWf.hdc, hbrush));
	Rectangle(curWf.hdc,curWf.frameArea.left,curWf.frameArea.top,curWf.frameArea.right,curWf.frameArea.bottom);

  // Do not forget to clean up.
  SelectObject(curWf.hdc, hpenOld);
  DeleteObject(hpen);
  SelectObject(curWf.hdc, hbrushOld);
  DeleteObject(hbrush);

	DeactivateFrame(Wf);

	/*
	WinDisplay_DisplayLeftJustified(&curWf,0,0,"1234567890123456789012345678901234567890");
	WinDisplay_DisplayInt(&curWf,5,1,curWf.charWidth);
	WinDisplay_DisplayInt(&curWf,9,1,curWf.charHeight);
	WinDisplay_DisplayInt(&curWf,5,2,curWf.cols);
	WinDisplay_DisplayInt(&curWf,9,2,curWf.rows);
	for (i=0 ; i<15 ; i++) {
		WinDisplay_DisplayInt(&curWf,0,i,i);
	}
*/
}
// --------------------------------------------------------------------------
#ifdef WIN_CE		// Compiling for Windows CE

void WinDisplay_DrawFrame(WinDisplayFrame *Wf)
{
  HPEN hpen, hpenOld;
  HBRUSH hbrush, hbrushOld;

	ActivateFrame(Wf);
  hpen = CreatePen(PS_SOLID,BORDER_SIZE,RGB(64,64,64));
  hbrush = CreateSolidBrush(RGB(255,255,255));

  // Select the new pen and brush, and then draw.
  hpenOld = SelectObject(curWf.hdc, hpen);
  hbrushOld = SelectObject(curWf.hdc, hbrush);
	Rectangle(curWf.hdc,curWf.frameArea.left,curWf.frameArea.top,curWf.frameArea.right,curWf.frameArea.bottom);

  // Do not forget to clean up.
  SelectObject(curWf.hdc, hpenOld);
  DeleteObject(hpen);
  SelectObject(curWf.hdc, hbrushOld);
  DeleteObject(hbrush);

	DeactivateFrame(Wf);

}
#else

void WinDisplay_DrawFrame(WinDisplayFrame *Wf)
{
	int i,ofs=-BORDER_SIZE/2,color=192,colorInc=(256-color)/BORDER_SIZE;
  HBRUSH hbrush, hbrushOld;
	RECT rect;
	ActivateFrame(Wf);
	for (i=0 ; i<BORDER_SIZE ; i++, ofs++, color+=colorInc) {
    hbrush = (HBRUSH)(CreateSolidBrush(RGB(color,color,color)));
		hbrushOld = (HBRUSH)(SelectObject(curWf.hdc, hbrush));
		memcpy(&rect,&curWf.borderArea,sizeof(RECT));
		rect.top+=ofs;
		rect.left+=ofs;
		rect.right-=ofs;
		rect.bottom-=ofs;
		FrameRect(curWf.hdc,&rect,hbrush);
	  SelectObject(curWf.hdc, hbrushOld);
	  DeleteObject(hbrush);
	}
	DeactivateFrame(Wf);
}
#endif
// --------------------------------------------------------------------------
void WinDisplay_ChangeCurrentFontSizeMultiplier(WinDisplayFrame *Wf, double multiplyBy)
{
	Wf->fontSizeMultiplier*=multiplyBy;
	ActivateFrame(Wf);
	SetupFont();
	DeactivateFrame(Wf);
}
// --------------------------------------------------------------------------
void WinDisplay_ResetCurrentFontSizeMultiplier(WinDisplayFrame *Wf)
{
	Wf->fontSizeMultiplier=_fontSizeMultiplier;
	ActivateFrame(Wf);
	SetupFont();
	DeactivateFrame(Wf);
}
// --------------------------------------------------------------------------
int WinDisplay_GetStrPelWidth(WinDisplayFrame *Wf, char *str)
{
#ifdef WIN_CE		// Compiling for Windows CE
	LPWSTR wStr;
#endif
	SIZE sz;
	ActivateFrame(Wf);

#ifdef WIN_CE		// Compiling for Windows CE
	wStr=WinDisplay_StrTowStrPtr(str);
	if (!GetTextExtentPoint32(curWf.hdc,wStr,strlen(str),&sz)) {
		sz.cx=0;
	}
	free(wStr);
#else
	if (!GetTextExtentPoint32(curWf.hdc,str,strlen(str),&sz)) {
		sz.cx=0;
	}
#endif
	DeactivateFrame(Wf);
	return(sz.cx);

}
// --------------------------------------------------------------------------
void WinDisplay_EraseLine(WinDisplayFrame *Wf, int y)
{
	char s[301];
	ActivateFrame(Wf);
	memset(s,' ',300);
	s[300]=0;
	DisplayTextAtColRow(0,y,s);
	DeactivateFrame(Wf);

}
// --------------------------------------------------------------------------
void WinDisplay_DisplayCentered(WinDisplayFrame *Wf, int x, int y, char *str)
{
	ActivateFrame(Wf);
	DisplayTextAtColRow(x-(strlen(str)>>1),y,str);
	DeactivateFrame(Wf);
}
// --------------------------------------------------------------------------
void WinDisplay_DisplayRowCentered(WinDisplayFrame *Wf, int y, char *str)
{
	int x;

	ActivateFrame(Wf);
	x=(curWf.width>>1)-(WinDisplay_GetStrPelWidth(Wf,str)>>1);
	DisplayTextAtXY(curWf.displayArea.left+x,curWf.displayArea.top+y*curWf.charHeight,str);
	DeactivateFrame(Wf);

}
// --------------------------------------------------------------------------
void WinDisplay_DisplayLeftJustified(WinDisplayFrame *Wf, int x, int y, char *str)
{
	ActivateFrame(Wf);
	DisplayTextAtColRow(x,y,str);
	DeactivateFrame(Wf);

}
// --------------------------------------------------------------------------
void WinDisplay_DisplayRightJustified(WinDisplayFrame *Wf, int x, int y, char *str)
{

#ifdef WIN_CE		// Compiling for Windows CE
	LPWSTR wStr;
#endif
	SIZE sz;

	ActivateFrame(Wf);

#ifdef WIN_CE		// Compiling for Windows CE
	wStr=WinDisplay_StrTowStrPtr(str);
	if (GetTextExtentPoint32(curWf.hdc,wStr,strlen(str),&sz)) {
		x-=(sz.cx/curWf.charWidth);
	} else {
		x-=strlen(str);
	}
	free(wStr);
#else
	if (GetTextExtentPoint32(curWf.hdc,str,strlen(str),&sz)) {
		x-=(sz.cx/curWf.charWidth);
	} else {
		x-=strlen(str);
	}
#endif

	DisplayTextAtColRow(x,y,str);
	DeactivateFrame(Wf);
}
// --------------------------------------------------------------------------
void WinDisplay_DisplayInt(WinDisplayFrame *Wf, int x, int y, int i)
{
	char s[21];
	sprintf_s(s, sizeof(s), "%d",i);

	ActivateFrame(Wf);
	DisplayTextAtColRow(x,y,s);
	DeactivateFrame(Wf);
}
// --------------------------------------------------------------------------
unsigned short *WinDisplay_StrTowStrPtr(char *str)
{
	LPWSTR wStr;
	LPWSTR wChr;
	wStr=(LPWSTR) malloc(MAX_STRING_LENGTH);
	MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED,str,strlen(str),wStr,MAX_STRING_LENGTH);
	wChr=wStr;
	while ((wChr=wcschr(wChr,EuroCharacter))!=NULL) {
		wChr[0]=0x20ac;
	}
	return((unsigned short *)(wStr));

}
// --------------------------------------------------------------------------
void WinDisplay_wStrToStr(unsigned short *wStr,int wMaxLength, char *str, int maxLength)
{

	WideCharToMultiByte(CP_ACP,0,(LPCWSTR)(wStr),wMaxLength,str,maxLength,NULL,NULL);
	str[wMaxLength]=0;
}
// --------------------------------------------------------------------------
