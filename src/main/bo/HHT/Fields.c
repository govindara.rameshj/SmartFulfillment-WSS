//---------------------------------------------------------------------------
// Application: WixStep.nlm. Hand-Held Terminal (HHT) Server for Novell Server
//							and Socket Services. Developed for Wickes.
// Project: WickesRFDeviceNLMSourceEdit - Developed with MS Visual Studio
//					2003 C++ and compiled with Watcom C/C++ and Watcom linker V 11.0
//					using NMAKE.
// --------------------------------------------------------------------------
// Module:Fields.c - Setters and Getters that work with the global sUser[]
//									 variables.
//	Each HHT has its own index in sUser[], which is a global structure used
//	to store SysPas info, to pass information between functions (instead of
//	using the stack), and to store things between input packets. Most sUser[]
//	variables have text labels enclosed in brackets associated with them, which
//	are used by the scripting file to set and get the variables. (This code is
//	kind of embarrassing... If someone who has the time to retest everything
//	wants	to finish rewriting it, that would be great.)
//----------------------------------------------------------------------------
#undef _THIS_SOURCE_HAS_THE_DATA_
#include "wixstep.h"
/***************************************************************************
Function:	 Get Field
Description: This function checks a Field name and if found returns the
				 contents in pcData.
Passed:		 Field Name string
				 User Number
Return:		 Data string in passed Field Name.
****************************************************************************/
void vGetField ( char *pcData, size_t pcDataSize, int sessionNumber )
{
	USER_TYPE *user = &sUser[sessionNumber];

	if (strlen(pcData)>12) {
		ProcessLogWriteLn("vGetField parameter error\n");
		return;
	}
	if(strcmp(pcData,"[LAST]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcLastInput);
	else if(strcmp(pcData,"[TOGG]") == 0)
		sprintf_s(pcData, pcDataSize, "%d",user->iToggle);
	else if(strcmp(pcData,"[GOTO]") == 0)
		sprintf_s(pcData, pcDataSize, "%d",user->iGoto);
	else if(strcmp(pcData,"[FLAG]") == 0)
		sprintf_s(pcData, pcDataSize, "%d",user->iFlag);
	else if(strcmp(pcData,"[SAVE]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcSave);
	else if(strcmp(pcData,"[SKUN]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcSkun);
	else if(strcmp(pcData,"[DESC]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcDesc);
	else if(strcmp(pcData,"[PRIC]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcPric);
	else if(strcmp(pcData,"[SUPP]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcSupp);
	else if(strcmp(pcData,"[PROD]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcProd);
	else if(strcmp(pcData,"[ONHA]") == 0)
		sprintf_s(pcData, pcDataSize, "%6ld",atol(user->pcOnha));
	else if(strcmp(pcData,"[SOLD]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcSold);
	else if(strcmp(pcData,"[MINI]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcMini);
	else if(strcmp(pcData,"[MAXI]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcMaxi);
	else if(strcmp(pcData,"[ISTA]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcIsta);
	else if(strcmp(pcData,"[IOBS]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcIobs);
	else if(strcmp(pcData,"[IDEL]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcIdel);
	else if(strcmp(pcData,"[LABN]") == 0)
		sprintf_s(pcData, pcDataSize, "%d",atol(user->pcLabn));
	else if(strcmp(pcData,"[LABM]") == 0)
		sprintf_s(pcData, pcDataSize, "%d",atol(user->pcLabm));
	else if(strcmp(pcData,"[LABL]") == 0)
		sprintf_s(pcData, pcDataSize, "%d",atol(user->pcLabl));
	else if(strcmp(pcData,"[TSCN]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcTscn);
	else if(strcmp(pcData,"[TWCN]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcTwcn);
	else if(strcmp(pcData,"[TPRE]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcTpre);
	else if(strcmp(pcData,"[TCNT]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcTcnt);
	else if(strcmp(pcData,"[DATE]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcDate);
	else if(strcmp(pcData,"[NUMB]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcNumb);
	else if(strcmp(pcData,"[DONE]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcDone);
	else if(strcmp(pcData,"[IADJ]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcIadj);
	else if(strcmp(pcData,"[ICNT]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcIcnt);
	else if(strcmp(pcData,"[INON]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcInon);
	else if(strcmp(pcData,"[TODO]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcToDo);
	else if(strcmp(pcData,"[AUTH]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcAuth);
	else if(strcmp(pcData,"[SONH]") == 0) /* W1.03	*/
		strcpy_s(pcData, pcDataSize, user->imOnha);				 /* W1.03	*/
	else if(strcmp(pcData,"[LSKU]") == 0) /* W1.04	*/
		strcpy_s(pcData, pcDataSize, user->pcLsku);				 /* W1.04	*/
	else if(strcmp(pcData,"[LDAT]") == 0) /* W1.04	*/
		strcpy_s(pcData, pcDataSize, user->pcLdat);				 /* W1.04	*/
	else if(strcmp(pcData,"[DAYN]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcDayn);
	else if(strcmp(pcData,"[TYPE]") == 0) /* W1.04	*/
		strcpy_s(pcData, pcDataSize, user->pcType);		/* W1.04	*/
	else if(strcmp(pcData,"[SSTK]") == 0) /* W1.04	*/
		strcpy_s(pcData, pcDataSize, user->pcSstk);		/* W1.04	*/
	else if(strcmp(pcData,"[ESTK]") == 0) /* W1.04	*/
		strcpy_s(pcData, pcDataSize, user->pcEstk);		/* W1.04	*/
	else if(strcmp(pcData,"[SMOV]") == 0) /* W1.04	*/
		strcpy_s(pcData, pcDataSize, user->pcSmov);		/* W1.04	*/
	else if(strcmp(pcData,"[TMDC]") == 0) /* W2.02	*/
		strcpy_s(pcData, pcDataSize, user->pcTmdc);		/* W2.02	*/
	else if(strcmp(pcData,"[SEL1]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcSel1);
	else if(strcmp(pcData,"[SEL2]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcSel2);
	else if(strcmp(pcData,"[SEL3]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcSel3);
	else if(strcmp(pcData,"[SEL4]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcSel4);
	else if(strcmp(pcData,"[SEL5]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcSel5);
	else if(strcmp(pcData,"[REAS]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcSaNumb);
	else if(strcmp(pcData,"[SDES]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcSaDesc);
	else if(strcmp(pcData,"[ADJQ]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcAdjQty);
	else if(strcmp(pcData,"[RDES]") == 0){
		if (user->pcSaImdn)
			strcpy_s(pcData, pcDataSize, "Mark Down ");
		if (user->pcSaIwtf)
			strcpy_s(pcData, pcDataSize, "Write Off ");
	}
	else if(strcmp(pcData,"[MDOH]") == 0)
		sprintf_s(pcData, pcDataSize, "%6ld",atol(user->pcMdnq));
	else if(strcmp(pcData,"[ADES]") == 0) {
		memset(user->pcAdes, 0, sizeof(user->pcAdes));
		strncat_s(user->pcAdes, sizeof(user->pcAdes),user->gcDesc,19);
		strcpy_s(pcData, pcDataSize, user->pcAdes);
	}
	else if(strcmp(pcData,"[MDES]") == 0) {
		memset(user->pcMdes, 0, sizeof(user->pcMdes));
		if(atoi(user->pcAfpn) > 10)
			sprintf_s(user->pcMdes, sizeof(user->pcMdes),"%d ",atoi(user->pcAfpn)-10);
		else
			sprintf_s(user->pcMdes, sizeof(user->pcMdes),"%d ",atoi(user->pcAfpn));
		strncat_s(user->pcMdes, sizeof(user->pcMdes),user->fpDesc,17);
		//fprintf(hLogFile,"sUser[%d].pcMdes is %s\n",i, user->pcMdes);
		strcpy_s(pcData, pcDataSize, user->pcMdes);
	}
	else if(strcmp(pcData,"[EEID]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcEeid);
	else if(strcmp(pcData,"[ENAM]") == 0)	/*W3.02*/
		strcpy_s(pcData, pcDataSize, user->pcEnam);					/*W3.02*/
	else if(strcmp(pcData,"[EQPU]") == 0) {	/*W3.03*/
		if((memcmp(user->imEqpu,"EACH",4) == 0)||		/*W3.03*/
			 (memcmp(user->imEqpu,"each",4) == 0))		/*W3.03*/
			strcpy_s(pcData, pcDataSize, user->imEqpu);			/*W3.03*/
		else {
			sprintf_s(pcData, pcDataSize, "per %s",user->imEqpu);
		}
	}	else if(strcmp(pcData,"[IPRI]") == 0) { /*W3.03*/
		//fprintf(hLogFile,"Fields: user->pcIpri is %s\n",user->pcIpri);
		strcpy_s(pcData, pcDataSize, user->pcIpri);			/*W3.03*/
	}	else if(strcmp(pcData,"[EQPR]") == 0)	/*W3.03*/
		strcpy_s(pcData, pcDataSize, user->pcEqpr);		/*W3.03*/
	else if(strcmp(pcData,"[SLAB]") == 0) {	/*W3.03*/
		strcpy_s(pcData, pcDataSize, user->pcSlab);			/*W3.03*/
		//fprintf(hLogFile,"GetField: user->pcSlab is %s\n",user->pcSlab);
	}
	else if(strcmp(pcData,"[MLAB]") == 0)	/*W3.03*/
		strcpy_s(pcData, pcDataSize, user->pcMlab);		/*W3.03*/
	else if(strcmp(pcData,"[LLAB]") == 0)	/*W3.03*/
		strcpy_s(pcData, pcDataSize, user->pcLlab);		/*W3.03*/
	else if(strcmp(pcData,"[ONHA+MDNQ]") == 0)
		sprintf_s(pcData, pcDataSize, "%6ld",(atol(user->imOnha) + atol(user->imMdnq)));
	else if(strcmp(pcData,"[IMSTATUS]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcImStatus);
	else if(strcmp(pcData,"[QREQ]") == 0)
		strcpy_s(pcData, pcDataSize, user->pcReqQty);
	else if(strcmp(pcData,"[ADL1]") == 0)			/*W3.04*/
		strcpy_s(pcData, pcDataSize, user->pcAdl1);		/*W3.04*/
	else if(strcmp(pcData,"[ADL2]") == 0)			/*W3.04*/
		strcpy_s(pcData, pcDataSize, user->pcAdl2);		/*W3.04*/
	else if(strcmp(pcData,"[ADL3]") == 0)			/*W3.04*/
		strcpy_s(pcData, pcDataSize, user->pcAdl3);		/*W3.04*/
	else if(strcmp(pcData,"[ONOR]") == 0)			/*W3.04*/
		strcpy_s(pcData, pcDataSize, user->imOnor);		/*W3.04*/
	else if(strcmp(pcData,"[IM:STATUS]") == 0)		/*W3.04*/
		strcpy_s(pcData, pcDataSize, user->pcImStatus);		/*W3.04*/
	else if(strcmp(pcData,"[OPT1]") == 0)			/*W3.06*/
		strcpy_s(pcData, pcDataSize, user->pcMenuLine1);		/*W3.06*/
	else if(strcmp(pcData,"[OPT2]") == 0)			/*W3.06*/
		strcpy_s(pcData, pcDataSize, user->pcMenuLine2);		/*W3.06*/
	else if(strcmp(pcData,"[OPT3]") == 0)			/*W3.06*/
		strcpy_s(pcData, pcDataSize, user->pcMenuLine3);		/*W3.06*/
	else if(strcmp(pcData,"[OPT4]") == 0)			/*W3.06*/
		strcpy_s(pcData, pcDataSize, user->pcMenuLine4);		/*W3.06*/
	else if(strcmp(pcData,"[OPT5]") == 0)			/*W3.06*/
		strcpy_s(pcData, pcDataSize, user->pcMenuLine5);		/*W3.06*/
}

/***************************************************************************
Function:	 Set Field
Description: This function checks a Field name and if found
				 sets the contents in pcSet.
Passed:		 Field Name string
				 Data string
				 User Number
Return:		 None
****************************************************************************/
void vSetField ( char *pcData, char *pcSet, int sessionNumber )
{
	USER_TYPE *user = &sUser[sessionNumber];

	long lTempLong ;
	int j,k;														/*W3.03*/
	if(memcmp(pcData,"[LAST]",strlen(pcData)) == 0)
		sprintf_s(user->pcLastInput, sizeof(user->pcLastInput),pcSet);
	else if(memcmp(pcData,"[TOGG]",strlen(pcData)) == 0)
		user->iToggle = atoi(pcSet);
	else if(memcmp(pcData,"[GOTO]",strlen(pcData)) == 0)
		user->iGoto = atoi(pcSet);
	else if(memcmp(pcData,"[FLAG]",strlen(pcData)) == 0)
		user->iFlag = atoi(pcSet);
	else if(memcmp(pcData,"[SAVE]",strlen(pcData)) == 0)
		sprintf_s(user->pcSave, sizeof(user->pcSave),pcSet);
	else if(memcmp(pcData,"[SKUN]",strlen(pcData)) == 0)
		sprintf_s(user->pcSkun, sizeof(user->pcSkun),pcSet);
	else if(memcmp(pcData,"[DESC]",strlen(pcData)) == 0)
		sprintf_s(user->pcDesc, sizeof(user->pcDesc),pcSet);
	else if(memcmp(pcData,"[PRIC]",strlen(pcData)) == 0)
		sprintf_s(user->pcPric, sizeof(user->pcPric),pcSet);
	else if(memcmp(pcData,"[SUPP]",strlen(pcData)) == 0)
		sprintf_s(user->pcSupp, sizeof(user->pcSupp),pcSet);
	else if(memcmp(pcData,"[PROD]",strlen(pcData)) == 0)
		sprintf_s(user->pcProd, sizeof(user->pcProd),pcSet);
	else if(memcmp(pcData,"[ONHA]",strlen(pcData)) == 0)
		sprintf_s(user->pcOnha, sizeof(user->pcOnha),pcSet);
	else if(memcmp(pcData,"[SOLD]",strlen(pcData)) == 0)
		sprintf_s(user->pcSold, sizeof(user->pcSold),"%6ld",atol(pcSet));
	else if(memcmp(pcData,"[MINI]",strlen(pcData)) == 0)
		sprintf_s(user->pcMini, sizeof(user->pcMini),pcSet);
	else if(memcmp(pcData,"[MAXI]",strlen(pcData)) == 0)
		sprintf_s(user->pcMaxi, sizeof(user->pcMaxi),pcSet);
	else if(memcmp(pcData,"[ISTA]",strlen(pcData)) == 0)
		sprintf_s(user->pcIsta, sizeof(user->pcIsta),pcSet);
	else if(memcmp(pcData,"[IOBS]",strlen(pcData)) == 0)
		sprintf_s(user->pcIobs, sizeof(user->pcIobs),pcSet);
	else if(memcmp(pcData,"[IDEL]",strlen(pcData)) == 0)
		sprintf_s(user->pcIdel, sizeof(user->pcIdel),pcSet);
	else if(memcmp(pcData,"[LABN]",strlen(pcData)) == 0)
		sprintf_s(user->pcLabn, sizeof(user->pcLabn),"%d",atol(pcSet));
	else if(memcmp(pcData,"[LABM]",strlen(pcData)) == 0)
		sprintf_s(user->pcLabm, sizeof(user->pcLabm),"%d",atol(pcSet));
	else if(memcmp(pcData,"[LABL]",strlen(pcData)) == 0)
		sprintf_s(user->pcLabl, sizeof(user->pcLabl),"%d",atol(pcSet));
	else if(memcmp(pcData,"[TSCN]",strlen(pcData)) == 0)
		sprintf_s(user->pcTscn, sizeof(user->pcTscn),pcSet);
	else if(memcmp(pcData,"[TWCN]",strlen(pcData)) == 0)
		sprintf_s(user->pcTwcn, sizeof(user->pcTwcn),pcSet);
	else if(memcmp(pcData,"[TPRE]",strlen(pcData)) == 0)
		sprintf_s(user->pcTpre, sizeof(user->pcTpre),pcSet);
	else if(memcmp(pcData,"[TCNT]",strlen(pcData)) == 0)
		sprintf_s(user->pcTcnt, sizeof(user->pcTcnt),pcSet);
	else if(memcmp(pcData,"[DATE]",strlen(pcData)) == 0)
		sprintf_s(user->pcDate, sizeof(user->pcDate),pcSet);
	else if(memcmp(pcData,"[NUMB]",strlen(pcData)) == 0)
		sprintf_s(user->pcNumb, sizeof(user->pcNumb),pcSet);
	else if(memcmp(pcData,"[DONE]",strlen(pcData)) == 0)
		sprintf_s(user->pcDone, sizeof(user->pcDone),pcSet);
	else if(memcmp(pcData,"[IADJ]",strlen(pcData)) == 0)
		sprintf_s(user->pcIadj, sizeof(user->pcIadj),pcSet);
	else if(memcmp(pcData,"[ICNT]",strlen(pcData)) == 0)
		sprintf_s(user->pcIcnt, sizeof(user->pcIcnt),pcSet);
	else if(memcmp(pcData,"[INON]",strlen(pcData)) == 0)
		sprintf_s(user->pcInon, sizeof(user->pcInon),pcSet);
	else if(memcmp(pcData,"[TODO]",strlen(pcData)) == 0)
		sprintf_s(user->pcToDo, sizeof(user->pcToDo),pcSet);
	else if(memcmp(pcData,"[AUTH]",strlen(pcData)) == 0)
		sprintf_s(user->pcAuth, sizeof(user->pcAuth),pcSet);
	else if(memcmp(pcData,"[SONH]",strlen(pcData)) == 0)
		sprintf_s(user->imOnha, sizeof(user->imOnha),pcSet);
	else if(memcmp(pcData,"[LSKU]",strlen(pcData)) == 0)
		sprintf_s(user->pcLsku, sizeof(user->pcLsku),pcSet);
	else if(memcmp(pcData,"[LDAT]",strlen(pcData)) == 0)
		sprintf_s(user->pcLdat, sizeof(user->pcLdat),pcSet);
	else if(memcmp(pcData,"[DAYN]",strlen(pcData)) == 0)
		sprintf_s(user->pcDayn, sizeof(user->pcDayn),pcSet);
	else if(memcmp(pcData,"[TYPE]",strlen(pcData)) == 0)
		sprintf_s(user->pcType, sizeof(user->pcType),pcSet);
	else if(memcmp(pcData,"[STTK]",strlen(pcData)) == 0)
		sprintf_s(user->pcSstk, sizeof(user->pcSstk),pcSet);
	else if(memcmp(pcData,"[ESTK]",strlen(pcData)) == 0)
		sprintf_s(user->pcEstk, sizeof(user->pcEstk),pcSet);
	else if(memcmp(pcData,"[SMOV]",strlen(pcData)) == 0)
		sprintf_s(user->pcSmov, sizeof(user->pcSmov),pcSet);
	else if(memcmp(pcData,"[TMDC]",strlen(pcData)) == 0)
		sprintf_s(user->pcTmdc, sizeof(user->pcTmdc),pcSet);
	else if(memcmp(pcData,"[SNUM]",strlen(pcData)) == 0)
		sprintf_s(user->pcSaStart, sizeof(user->pcSaStart),pcSet);
	else if(memcmp(pcData,"[ADJQ]",strlen(pcData)) == 0) {
		/* Reject either multiple "-" or "." */
		if((strchr(pcSet,'.') != 0) ||
		   (strchr(pcSet+1,'-') != 0)) {
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
		}	else {
			sprintf_s(user->pcAdjQty, sizeof(user->pcAdjQty),pcSet);
			lTempLong = atoi(user->pcAdjQty);
			if (memcmp(user->pcSaSign,"-",1) == 0)
				lTempLong = lTempLong * -1;
			sprintf_s(user->pcAdjQty, sizeof(user->pcAdjQty),"%d",lTempLong);
		}
	}
	else if(memcmp(pcData,"[TOPS]",strlen(pcData)) == 0)
		user->bScreenTop = atoi(pcSet);
	else if(memcmp(pcData,"[BOTS]",strlen(pcData)) == 0)
		user->bScreenBot = atoi(pcSet);
	else if(memcmp(pcData,"[SACN]",strlen(pcData)) == 0)
		sprintf_s(user->pcSaNumb, sizeof(user->pcSaNumb),pcSet);
	else if(memcmp(pcData,"[LANG]",strlen(pcData)) == 0)
		sprintf_s(user->pcLang, sizeof(user->pcLang),pcSet);
	else if(memcmp(pcData,"[WSID]",strlen(pcData)) == 0)
		sprintf_s(user->pcWsid, sizeof(user->pcWsid),pcSet);
	else if(memcmp(pcData,"[AFGN]",strlen(pcData)) == 0)
		sprintf_s(user->pcAfgn, sizeof(user->pcAfgn),pcSet);
	else if(memcmp(pcData,"[AFPN]",strlen(pcData)) == 0) {
		if (strlen(pcSet) == 1)
			sprintf_s(user->pcAfpn, sizeof(user->pcAfpn),"0%s",pcSet);
		else
			sprintf_s(user->pcAfpn, sizeof(user->pcAfpn),"%s",pcSet);
	}
	else if(memcmp(pcData,"[EEID]",strlen(pcData)) == 0)
		sprintf_s(user->pcEeid, sizeof(user->pcEeid),"%03d",atoi(pcSet));
	else if(memcmp(pcData,"[EPAS]",strlen(pcData)) == 0) {
		strcpy_s(user->pcPass, sizeof(user->pcPass),pcSet);
		fprintf(ProcessLogHandle(),"Password entered [EPAS] = %s\n",user->pcPass);
	}
	else if(memcmp(pcData,"[STIM]",strlen(pcData)) == 0)
    GetCurrentTimeStr(user->pcStartTime);
	else if(memcmp(pcData,"[ETIM]",strlen(pcData)) == 0)
    GetCurrentTimeStr(user->pcEndTime);
	else if(memcmp(pcData,"[LOGI]",strlen(pcData)) == 0) {
		memset(user->pcLogin,0,sizeof(user->pcLogin));
		sprintf_s(user->pcLogin, sizeof(user->pcLogin),pcSet);
	}	else if(memcmp(pcData,"[MTIME]",strlen(pcData)) == 0){
   	if(memcmp(pcSet,"RESET",strlen(pcSet)) == 0){
			user->lTimeOut = 99999;
		}	else {
			//vGetCurrentTime(); /* Gets System Date and Time */
			/* Set for 20secs delay*/
			user->lTimeOut = GetCurrentSeconds() + lTimeOutValue;	/*W3.02*/
		}
	} else if(memcmp(pcData,"[FORC]",strlen(pcData)) == 0) {
		strcpy_s(user->pcSystemOut, sizeof(user->pcSystemOut),pcSet);
	}	else if(memcmp(pcData,"[MTIMEOUT]",strlen(pcData)) == 0) {
		lTimeOutValue = 0L;
		lTimeOutValue = atol(pcSet);
	}	else if(memcmp(pcData,"[SLAB]",strlen(pcData)) == 0) {
		for(j=strlen(pcSet),k=0;j > 0;j--)	{
	 		if (pcSet[j-1] != ' ')	{
				k++;
			}
		}
 		if (pcSet[k-1] == '-')
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "X");
		else {
			sprintf_s(user->pcSlab, sizeof(user->pcSlab),pcSet);
		}
	}	else if(memcmp(pcData,"[MLAB]",strlen(pcData)) == 0) {
		for(j=strlen(pcSet),k=0;j > 0;j--)	{
	 		if (pcSet[j-1] != ' ')	{
				k++;
			}
		}
		/* Allow zero input */
 		if (pcSet[k-1] == '-')
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "X");
		else {
			sprintf_s(user->pcMlab, sizeof(user->pcMlab),pcSet);
		}
	}
	else if(memcmp(pcData,"[LLAB]",strlen(pcData)) == 0) {		/*W3.03*/
		for(j=strlen(pcSet),k=0;j > 0;j--)	{					/*W3.03*/
	 		if (pcSet[j-1] != ' ')	{ 							/*W3.03*/
				k++;											/*W3.03*/
			}													/*W3.03*/
		}														/*W3.03*/
 		if (pcSet[k-1] == '-')									/*W3.03*/
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "X"); 				/*W3.03*/
		else {													/*W3.03*/
			sprintf_s(user->pcLlab, sizeof(user->pcLlab),pcSet);						/*W3.03*/
			//fprintf(hLogFile,"Set:user->pcLlab is %s\n",user->pcLlab);
		}														/*W3.03*/
	}															/*W3.03*/
	else if(memcmp(pcData,"[PRCCHG]",strlen(pcData)) == 0) {
		user->pcDoingPriceChange=FALSE;
		if (pcSet[0]=='Y') {
			user->pcDoingPriceChange=TRUE;
		}
	}
	else if(memcmp(pcData,"[LTYPE]",strlen(pcData)) == 0) {		/*W3.03*/
		memset(user->pcThisLabel,0,sizeof(user->pcThisLabel));/*W3.03*/
		sprintf_s(user->pcThisLabel, sizeof(user->pcThisLabel),pcSet);					/*W3.03*/
		//fprintf(hLogFile,"user->pcThisLabel is %s\n",user->pcThisLabel);
	}
	else if(memcmp(pcData,"[WALKABOUT]",strlen(pcData)) == 0) {	/*W3.04*/
		memset(user->pcWalkAbout,0,sizeof(user->pcWalkAbout));/*W3.04*/
		sprintf_s(user->pcWalkAbout, sizeof(user->pcWalkAbout),pcSet);					/*W3.04*/
		//fprintf(hLogFile,"user->pcWalkAbout %s\n",user->pcWalkAbout);
	}
	else if(memcmp(pcData,"[QREQ]",strlen(pcData)) == 0) {		/*W3.04*/
		//fprintf(hLogFile,"pcSet is %s and strlen is %d\n",pcSet,strlen(pcSet)); /*W3.04*/
	 	for(j=0;j < (int)strlen(pcSet);j++) {
			if ((pcSet[j] != 32) && (pcSet[j] != 45)) {
				if ((pcSet[j] < 48) || (pcSet[j] > 57))
					strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
			}
 		}
		if (pcSet[j] == 45) {
				if ((pcSet[j+1] < 48) || (pcSet[j+1] > 57))
					strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
		}
		if (user->bNewGapSect)
			sprintf_s(user->pcReqQty, sizeof(user->pcReqQty),"%6ld",atol(pcSet));/*W3.05a*/
		else
			sprintf_s(user->pcReqQty, sizeof(user->pcReqQty),"%6ld",atol(pcSet)+atol(user->pcReqQty));/*W3.05a*/
		//fprintf(hLogFile,"SetField pcReqQty is %s\n",user->pcReqQty); /*W3.04*/
	}
	else if(memcmp(pcData,"[SUBMENU]",strlen(pcData)) == 0) {	/*W3.04*/
		memset(user->pcSubMenu,0,sizeof(user->pcSubMenu));/*W3.04*/
		sprintf_s(user->pcSubMenu, sizeof(user->pcSubMenu),pcSet);						/*W3.04*/
		//fprintf(hLogFile,"user->pcSubMenu %s\n",user->pcSubMenu);
	}
	else if(memcmp(pcData,"[FIRSTWALK]",strlen(pcData)) == 0) {	/*W3.04*/
		memset(user->pcFirstWalk,0,sizeof(user->pcWalkAbout));/*W3.04*/
		sprintf_s(user->pcFirstWalk, sizeof(user->pcFirstWalk),pcSet);					/*W3.04*/
		//fprintf(hLogFile,"user->pcFirstWalk %s\n",user->pcFirstWalk);
	}
	else if(memcmp(pcData,"[PIC]",strlen(pcData)) == 0) {	/*W3.06*/
		memset(user->pcPIC,0,sizeof(user->pcPIC));
		sprintf_s(user->pcPIC, sizeof(user->pcPIC),pcSet);
	}
	else if(memcmp(pcData,"[SHOPFLOOR]",strlen(pcData)) == 0) {	/*W3.06*/
		memset(user->pcShopFloor,0,sizeof(user->pcShopFloor));
		sprintf_s(user->pcShopFloor, sizeof(user->pcShopFloor),pcSet);
	}
	else if(memcmp(pcData,"[PIC_LAB_OK?]",strlen(pcData)) == 0) {	/*W3.06*/
		memset(user->pcPicLabelOk,0,sizeof(user->pcPicLabelOk));
		sprintf_s(user->pcPicLabelOk, sizeof(user->pcPicLabelOk),pcSet);
	}
	else if(memcmp(pcData,"[LABEL_CHK]",strlen(pcData)) == 0) {		/*W3.06*/
		memset(user->pcLabelCheck,0,sizeof(user->pcLabelCheck));
		sprintf_s(user->pcLabelCheck, sizeof(user->pcLabelCheck),pcSet);
	}
	else if(memcmp(pcData,"[LABEL_MENU]",strlen(pcData)) == 0) {	/*W3.06*/
		memset(user->pcLabelMenu,0,sizeof(user->pcLabelMenu));
		sprintf_s(user->pcLabelMenu, sizeof(user->pcLabelMenu),pcSet);
	}
	else if(memcmp(pcData,"[SY:TYPE]",strlen(pcData)) == 0) {	/*W3.06*/
		memset(user->pcLrType,0,sizeof(user->pcLrType));
		sprintf_s(user->pcLrType, sizeof(user->pcLrType),pcSet);
	}
	else if(memcmp(pcData,"[SY:CODE]",strlen(pcData)) == 0) {	/*W3.06*/
		memset(user->pcSyStart,0,sizeof(user->pcSyStart));
		sprintf_s(user->pcSyStart, sizeof(user->pcSyStart),pcSet);
	}
	else if(memcmp(pcData,"[SY:ATTR]",strlen(pcData)) == 0) {	/*W3.06*/
		memset(user->pcLrAttr,0,sizeof(user->pcLrAttr));
		sprintf_s(user->pcLrAttr, sizeof(user->pcLrAttr),pcSet);
	}
	else if(memcmp(pcData,"[PC_OPT]",strlen(pcData)) == 0) {	/*W3.06*/
		memset(user->pcPcOption,0,sizeof(user->pcPcOption));
		sprintf_s(user->pcPcOption, sizeof(user->pcPcOption),pcSet);
		//fprintf(hLogFile,"user->pcPcOption %s\n",user->pcPcOption);
	}
	else if(memcmp(pcData,"[ENQ_LINE]",strlen(pcData)) == 0) {	/*W3.06*/
		user->iEnquiryLine = atoi(pcSet);
		//fprintf(hLogFile,"user->iEnquiryLine %d\n",user->iEnquiryLine);
	}
	else if(memcmp(pcData,"[PIC_NOF]",strlen(pcData)) == 0) {	/*W3.07*/
		memset(user->pcPicSkuNof,0,sizeof(user->pcPicSkuNof));
		sprintf_s(user->pcPicSkuNof, sizeof(user->pcPicSkuNof),pcSet);
		//fprintf(hLogFile,"user->pcPicSkuNof %s\n",user->pcPicSkuNof);
	}
}
/***************************************************************************
Function:	 Add Field A to field B
Description: This function Adds to Field name
			 Specifically for Label Field Updates
Passed:		 Field Name string
				 Data string
				 User Number
Return:		 None
****************************************************************************/
void vAddField (char *pcData, int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	int iTempInt ;

	if(memcmp(pcData,"[LABN]",strlen(pcData)) == 0) {
		iTempInt	=	(atoi(user->pcLabn) + atoi(user->pcSlab));
		if ((iTempInt == 100)||(iTempInt > 100)) iTempInt = 99 ;
		if (iTempInt < 0) iTempInt = 0 ;
		sprintf_s(user->pcLabn, sizeof(user->pcLabn),"%d",iTempInt);
		//fprintf(hLogFile,"user->pcLabn is %s\n",user->pcLabn);
	}
	if(memcmp(pcData,"[LABM]",strlen(pcData)) == 0) {
		iTempInt	=	(atoi(user->pcLabm) + atoi(user->pcMlab));
		if ((iTempInt == 100)||(iTempInt > 100)) iTempInt = 99 ;
		if (iTempInt < 0) iTempInt = 0 ;
		sprintf_s(user->pcLabm, sizeof(user->pcLabm),"%d",iTempInt);
		//fprintf(hLogFile,"user->pcLabm is %s\n",user->pcLabm);
	}
	if(memcmp(pcData,"[LABL]",strlen(pcData)) == 0) {
		iTempInt	=	(atoi(user->pcLabl) + atoi(user->pcLlab));
		if ((iTempInt == 100)||(iTempInt > 100)) iTempInt = 99 ;
		if (iTempInt < 0) iTempInt = 0 ;
		sprintf_s(user->pcLabl, sizeof(user->pcLabl),"%d",iTempInt);
		//fprintf(hLogFile,"user->pcLabl is %s\n",user->pcLabl);
	}
}

	/***************************************************************************
Function:    Derive the Equivalent Price
Description: This function Sets Up the Equivalent Price for either IM:PRIC
			 or IP:PRIC
Passed:      None
Return:      None
****************************************************************************/
void vGetEquivPrice(char *pcPrice, int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	double prc = atof(pcPrice),
				eqpm = atof(user->imEqpm);
	if (eqpm != 0) {
		sprintf_s(user->pcEqpr, sizeof(user->pcEqpr), "%0.2f", prc / eqpm);
	} else {
		user->pcEqpr[0] = '\0';
		//strcpy_s(user->pcEqpr, sizeof(user->pcEqpr), "0");
	}
	//printf("pcPrice=%s, imEqpm=%s pcEqpr=%s\n", pcPrice, sUser[i].imEqpm, sUser[i].pcEqpr);
}
/***************************************************************************
Function:	 Read EANMAS Fields
Description: This function reads the EANMAS record and sets the user
				 fields.
Passed:		 EANMAS record
				 User Number
Return:		 None
****************************************************************************/
/*
void vReadEanMas(EANMAS *eanmas, int i)
{
	GetDBCharFieldToStr(sUser[i].pcSkun, eanmas->skun, sizeof(eanmas->skun));
	GetDBCharFieldToStr(sUser[i].pcNumb, eanmas->numb, sizeof(eanmas->numb));
	strcpy(sUser[i].pcLastInput, sUser[i].pcSkun);
}
*/
/***************************************************************************
Function:	 Form the Sold Today value				 				W1.04
Description: Accumulate the movement records for today
Passed:		 Movement Running total & User Number
Return:		 none
****************************************************************************/
/*
long lSkuMoves(long lTempSold, int i)
{
	long lTempSmov=atol(sUser[i].pcSmov);
	long lTempSmdn=atol(sUser[i].pcSmdn);
//	fprintf(hLogFile,"Sold so far is %6ld\n",lTempSold);

//	Sale types
	if ((strcmp(sUser[i].pcType,"01")==0) ||
			(strcmp(sUser[i].pcType,"02")==0) ||
			(strcmp(sUser[i].pcType,"03")==0) ||
			(strcmp(sUser[i].pcType,"11")==0) ||
			(strcmp(sUser[i].pcType,"12")==0)) {
			//fprintf(hLogFile,"Movement Type %s is %6ld\n",sUser[i].pcType,lTempSmov);
		lTempSold=lTempSold+lTempSmov;
		return(lTempSold);
	}
//W2.05 For Type (04) add in the MarkDown to the Sold
	if (strcmp(sUser[i].pcType,"04")==0) {
		//fprintf(hLogFile,"Movement Type %s is %6ld\n",sUser[i].pcType,lTempSmdn);
		lTempSold=lTempSold+lTempSmdn;
		return(lTempSold);
	}
	//fprintf(hLogFile,"Other Type detected %s\n",sUser[i].pcType);
	return(lTempSold);
}
*/

/***************************************************************************
Function:	 Update Count Fields
Description: Special function to update HHTDET count fields
Passed:		 User Number
Return:		 none
Last Input:  Set to "0" if it fails else unchanged.
****************************************************************************/
void vUpdateCountField( int sessionNumber )
{
	USER_TYPE *user = &sUser[sessionNumber];

	long lTempTscn=atol(user->pcTscn);
	long lTempTwcn=atol(user->pcTwcn);
	long lTempTpre=atol(user->pcTpre);
	long lTempTmdc=atol(user->pcTmdc);

	switch (user->iToggle)
	{
		case 0:
			lTempTscn=lTempTscn + atol(user->pcLastInput);
			break;
		case 1:
			lTempTmdc=lTempTmdc + atol(user->pcLastInput);
			break;
		case 2:
			lTempTwcn=lTempTwcn + atol(user->pcLastInput);
			break;
		case 3:
			lTempTpre=lTempTpre + atol(user->pcLastInput);
			break;
		default:
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "\xC1");
			break;
	}
// W1.03 - Only allow negative for Presales entry
	if (((lTempTscn>=0L)  && (lTempTwcn>=0L) && (lTempTmdc>=0L)) ||	(user->iToggle==2))	{
		fprintf(ProcessLogHandle(),"UpdateCountField %d\n",user->iToggle);
		switch (user->iToggle) {
			case 0:
				user->iFlag = 1; /* Indicate Update */
				sprintf_s(user->pcTscn, sizeof(user->pcTscn), "%6ld", lTempTscn);
				sprintf_s(user->pcTcnt, sizeof(user->pcTcnt), "%6ld", (lTempTscn + lTempTwcn + lTempTmdc - lTempTpre));
				ProcessLogWriteLn("**SETTING iFlag to 1");
				break;
			case 1:
				user->iFlag = 1; /* Indicate Update */
				sprintf_s(user->pcTmdc, sizeof(user->pcTmdc), "%6ld", lTempTmdc);
				sprintf_s(user->pcTcnt, sizeof(user->pcTcnt), "%6ld", (lTempTscn + lTempTwcn + lTempTmdc - lTempTpre));
				ProcessLogWriteLn("**SETTING iFlag to 1");
				break;
			case 2:
				user->iFlag = 1; /* Indicate Update */
				sprintf_s(user->pcTwcn, sizeof(user->pcTwcn), "%6ld", lTempTwcn);
				sprintf_s(user->pcTcnt, sizeof(user->pcTcnt), "%6ld", (lTempTscn + lTempTwcn + lTempTmdc - lTempTpre));
				ProcessLogWriteLn("**SETTING iFlag to 1");
				break;
			case 3:
				user->iFlag = 1; /* Indicate Update */
				sprintf_s(user->pcTpre, sizeof(user->pcTpre), "%6ld", lTempTpre);
				sprintf_s(user->pcTcnt, sizeof(user->pcTcnt), "%6ld", (lTempTscn + lTempTwcn + lTempTmdc - lTempTpre));
				ProcessLogWriteLn("**SETTING iFlag to 1");
				break;
		}
	}	else {
		strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "\xC1"); /* FUNC A Fail */
	}
}

/***************************************************************************
Function:	 Test High Field
Description: Special function to check if input is greater than 99
Passed:		 User Number
Return:		 none
****************************************************************************/
void vTestHighField(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	long lTempLong=atol(user->pcLastInput);

	/* Test is input is 0 and toggle is zero */
	if ((user->iToggle == 0) && (strcmp(user->pcLastInput,"0") == 0)) {
		user->iFlag=1;
		strcpy_s(user->pcSave, sizeof(user->pcSave), user->pcLastInput);
		user->pcLastInput[0] = '\0'; /* [CR] */
	}
	/* Test is input greater than 99 */
	if (lTempLong >= 100) {
		strcpy_s(user->pcSave, sizeof(user->pcSave), user->pcLastInput);
		strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "\xC1"); /* FUNC A */
	}
}

/***************************************************************************
Function:    Setup the Stk Adj Reason Line
			 or the Label Reason 									*W3.06
Description: This function Sets up the Stock Adj Reason Line to be displayed
			 or the System Codes "LR" Reason Line 									*W3.06
Passed:      Line Number
             User Number
Return:      None
****************************************************************************/
void vSetReasonLine( int iReasonLine, int sessionNumber)						/*A2.06 */
{
	USER_TYPE *user = &sUser[sessionNumber];

	//printf("**Reason line %d\n",iReasonLine);

	if (iReasonLine == 1) {
		if (memcmp(user->pcLabelCheck,"YES",3) == 0) {
			memset(user->pcSel1, 0, sizeof(user->pcSel1));
			memset(user->pcSel1,' ', sizeof(user->pcSel1)-1);
			strncpy_s(user->pcSel1, sizeof(user->pcSel1),user->syCode+1,sizeof(user->syCode)-2);
			strcat_s(user->pcSel1, sizeof(user->pcSel1), " ");
			strncpy_s(user->pcSel1 + 2, sizeof(user->pcSel1)-2, user->syDesc,17);

			fprintf(ProcessLogHandle(),"user->syCode is %s \n",user->syCode);
			fprintf(ProcessLogHandle(),"pcSel1 is %s \n",user->pcSel1);
			fprintf(ProcessLogHandle(),"user->syDesc is %s \n",user->syDesc);
			fprintf(ProcessLogHandle(),"pcSel1 is %s \n",user->pcSel1);
		} else {
			strcpy_s(user->pcReas1, sizeof(user->pcReas1),user->pcSaNumb);
			user->pcImdn1=user->pcSaImdn;
			user->pcIwtf1=user->pcSaIwtf;
			strcpy_s(user->pcType1, sizeof(user->pcType1),user->pcSaType);
			memset(user->pcSel1, 0, sizeof(user->pcSel1));
			memset(user->pcSel1,' ', sizeof(user->pcSel1)-1);
			strncpy_s(user->pcSel1, sizeof(user->pcSel1),user->pcSaNumb,sizeof(user->pcSaNumb)-1);
			strcat_s(user->pcSel1, sizeof(user->pcSel1), " ");
			strncpy_s(user->pcSel1 + 3, sizeof(user->pcSel1)-3, user->pcSaDesc,sizeof(user->pcSel1)-4);

			fprintf(ProcessLogHandle(),"pcReas1 is %s \n",user->pcReas1);
			fprintf(ProcessLogHandle(),"pcImdn1 is %x \n",user->pcImdn1);
			fprintf(ProcessLogHandle(),"pcIwtf1 is %x \n",user->pcIwtf1);
			fprintf(ProcessLogHandle(),"pcType1 is %s \n",user->pcType1);
			fprintf(ProcessLogHandle(),"pcSel1 is %s \n",user->pcSel1);
		}
	}
	if (iReasonLine == 2) {
		if (memcmp(user->pcLabelCheck,"YES",3) == 0) {
			memset(user->pcSel2, 0, sizeof(user->pcSel2));
			memset(user->pcSel2,' ', sizeof(user->pcSel2)-1);
			strncpy_s(user->pcSel2, sizeof(user->pcSel2),user->syCode+1,sizeof(user->syCode)-2);
			strcat_s(user->pcSel2, sizeof(user->pcSel1), " ");
			strncpy_s(user->pcSel2+2, sizeof(user->pcSel2) - 2, user->syDesc,17);
			//fprintf(hLogFile,"pcSel2 is %s \n",user->pcSel2);
		} else {
			strcpy_s(user->pcReas2, sizeof(user->pcReas2),user->pcSaNumb);
			user->pcImdn2=user->pcSaImdn;
			user->pcIwtf2=user->pcSaIwtf;
			strcpy_s(user->pcType2, sizeof(user->pcType2),user->pcSaType);
			memset(user->pcSel2, 0, sizeof(user->pcSel2));
			memset(user->pcSel2,' ', sizeof(user->pcSel2)-1);
			strncpy_s(user->pcSel2, sizeof(user->pcSel2),user->pcSaNumb,sizeof(user->pcSaNumb)-1);
			strcat_s(user->pcSel2, sizeof(user->pcSel1), " ");
			strncpy_s(user->pcSel2+3, sizeof(user->pcSel2)-3, user->pcSaDesc,sizeof(user->pcSel2)-4);
		}
	}
	if (iReasonLine == 3) {
		if (memcmp(user->pcLabelCheck,"YES",3) == 0) {
			memset(user->pcSel3, 0, sizeof(user->pcSel3));
			memset(user->pcSel3,' ', sizeof(user->pcSel3)-1);
			strncpy_s(user->pcSel3, sizeof(user->pcSel3),user->syCode+1,sizeof(user->syCode)-2);
			strcat_s(user->pcSel3, sizeof(user->pcSel1), " ");
			strncpy_s(user->pcSel3+2, sizeof(user->pcSel3)-2, user->syDesc,17);
			//fprintf(hLogFile,"pcSel3 is %s \n",user->pcSel3);
		} else {
			strcpy_s(user->pcReas3, sizeof(user->pcReas3),user->pcSaNumb);
			user->pcImdn3=user->pcSaImdn;
			user->pcIwtf3=user->pcSaIwtf;
			strcpy_s(user->pcType3, sizeof(user->pcType3),user->pcSaType);
			memset(user->pcSel3, 0, sizeof(user->pcSel3));
			memset(user->pcSel3,' ', sizeof(user->pcSel3)-1);
			strncpy_s(user->pcSel3, sizeof(user->pcSel3),user->pcSaNumb,sizeof(user->pcSaNumb)-1);
			strcat_s(user->pcSel3, sizeof(user->pcSel1), " ");
			strncpy_s(user->pcSel3+3, sizeof(user->pcSel3)-3, user->pcSaDesc,sizeof(user->pcSel3)-4);
		}
	}
	if (iReasonLine == 4) {
		if (memcmp(user->pcLabelCheck,"YES",3) == 0) {
			memset(user->pcSel4, 0, sizeof(user->pcSel4));
			memset(user->pcSel4,' ', sizeof(user->pcSel4)-1);
			strncpy_s(user->pcSel4, sizeof(user->pcSel4),user->syCode+1,sizeof(user->syCode)-2);
			strcat_s(user->pcSel4, sizeof(user->pcSel1), " ");
			strncpy_s(user->pcSel4+2, sizeof(user->pcSel4)-2, user->syDesc,17);
			//fprintf(hLogFile,"pcSel4 is %s \n",user->pcSel3);
		} else {
			strcpy_s(user->pcReas4, sizeof(user->pcReas4),user->pcSaNumb);
			user->pcImdn4=user->pcSaImdn;
			user->pcIwtf4=user->pcSaIwtf;
			strcpy_s(user->pcType4, sizeof(user->pcType4),user->pcSaType);
			memset(user->pcSel4, 0, sizeof(user->pcSel4));
			memset(user->pcSel4,' ', sizeof(user->pcSel4)-1);
			strncpy_s(user->pcSel4, sizeof(user->pcSel4),user->pcSaNumb,sizeof(user->pcSaNumb)-1);
			strcat_s(user->pcSel4, sizeof(user->pcSel1), " ");
			strncpy_s(user->pcSel4+3, sizeof(user->pcSel4)-3, user->pcSaDesc,sizeof(user->pcSel4)-4);
		}
	}
	if (iReasonLine == 5) {
		if (memcmp(user->pcLabelCheck,"YES",3) == 0) {
			memset(user->pcSel5, 0, sizeof(user->pcSel5));
			memset(user->pcSel5,' ', sizeof(user->pcSel5)-1);
			strncpy_s(user->pcSel5, sizeof(user->pcSel5),user->syCode+1,sizeof(user->syCode)-2);
			strcat_s(user->pcSel5, sizeof(user->pcSel1), " ");
			strncpy_s(user->pcSel5+2, sizeof(user->pcSel5)-2, user->syDesc,17);
			//fprintf(hLogFile,"pcSel5 is %s \n",user->pcSel5);
		} else {
			strcpy_s(user->pcReas5, sizeof(user->pcReas5),user->pcSaNumb);
			user->pcImdn5=user->pcSaImdn;
			user->pcIwtf5=user->pcSaIwtf;
			strcpy_s(user->pcType5, sizeof(user->pcType5),user->pcSaType);
			memset(user->pcSel5, 0, sizeof(user->pcSel5));
			memset(user->pcSel5,' ', sizeof(user->pcSel5)-1);
			strncpy_s(user->pcSel5, sizeof(user->pcSel5),user->pcSaNumb,sizeof(user->pcSaNumb)-1);
			strcat_s(user->pcSel5, sizeof(user->pcSel1), " ");
			strncpy_s(user->pcSel5+3, sizeof(user->pcSel5)-3,user->pcSaDesc,sizeof(user->pcSel5)-4);
			//fprintf(hLogFile,"pcSel5 is %s \n",user->pcSel5);
		}
	}
}
