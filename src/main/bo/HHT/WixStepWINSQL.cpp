//---------------------------------------------------------------------------
// Application: WixStep.nlm. Hand-Held Terminal (HHT) Server for Novell Server
//							and Socket Services. Developed for Wickes.
// Project: WickesRFDeviceNLMSourceEdit - Developed with MS Visual Studio
//					2003 C++ and compiled with Watcom C/C++ and Watcom linker V 11.0
//					using NMAKE.
// --------------------------------------------------------------------------
// About this solution: This is the current WIXSTEP.NLM source code imported
// into a .NET solution so that the intellisense and IDE can be used to work
// on the code. This project does not compile through the IDE. To make the .NLM,
// open a DOS window, CD to this directory, run AUTOEXEC.BAT to setup the paths
// and env variables, and then run NMAKE, a WATCOM utility which will use the
// files MAKEINIT and MAKEFILE to build WIXSTEP.NLM. Errors and warnings can be
// seen in the command line window. The WATCOM and Novell NLM SDK locations are
// referenced in AUTOEXEC. Current default locations are C:\SDKCD15\ and
// C:\WHATCOM
// --------------------------------------------------------------------------
// Module:WixStep.c - Main for the HHT Server with NLM Console, init, socket
//										listener, and cleanup.
//	Main() initializes everything and then enters a loop that checks keyboard
//	input (F8 exits the NLM) and calls iMainProgramLogic(), which is the socket
//	listener. When a socket has activity, iMainProgramLogic() handles connects,
//	disconnects, and packets (by calling lApp()).
//-----------------------------------------------------------------------------
// Old documentation:
// * A WinSock Step emulation program for Windows NT/95
// * 6/1/98 by Mark Heath & David Clavey, Lowther Consultants
// * Main Program and Socket Start
// * Win32-Console mode and 16bit DOS Versions
// * Converted to NLM in June 98 (JG)
//-----------------------------------------------------------------------------
// Revision History
//-----------------------------------------------------------------------------
// W4.06  JG - 17/11/09 - Repair conversion up HHTHDR update code.
// W4.00  JG - 07/07/09 - Finish up version for SQL Server.
// W3.08b GS - 15/06/05 - Verbal - Fixed bug with STKLOG Lookup- SEEK [STKLOG]
// W3.08a GS - 19/04/05 - Verbal - Fixed bug with sold today in primary count
// W3.08	GS - 11/01/05 - AL1129 - Changes to Support .dta files
// A3.07e14.10.04 JR  AL1129  Changes to Support .dta files
// A3.07c26.11.01 JR  2001-041 Added Calculation for Price change Markup/Down
// W3.07b20.04.01 JR  a.Ref: Changes to resolve Overnight "Sold Today" problem
//                    b.Verbal Request to Close Resources via UNLOAD NLM
// W3.07 19.12.00 JR  WIX979 : Changes to WIXSTEP.TXT for menu changes
//                    and identify non orderable items (for Gap Walk).
// W3.06 17.11.00 JR  WIX729 : Added Label request Functionality
// W3.05 08.11.00 JR  Verbal Req: Menu Changes for Main Menu GAP WALK option
// W3.04 25.10.00 JR  WIX925 : Added Gap Walk & further Menu Changes
// W3.03 10.10.00 JR  WIX933 : Added Price Change Functionality
// W3.02 20.09.00 JR  WIX950 : Added Menu & Security Functionality
// W3.01 19.09.00 JR  WIX732 : MultiUser Changes
// W2.08 25.08.00 JR  WIX732 : MultiUser Changes
// W2.07 25.07.00 JR  WIX947 : Remove from SOH and display MOH (MD On-Hand)
//                    and rearrange input fields
// W2.06 05.07.00 JR  WIX941 : Added fuctionality to support MarkDowns
//                    and Write Off Stock Adjustnments
// W2.05 09.02.00 JR  CTS869 : Changes to "Sold Today" to include Markdown
//                    stock movement type "04"
// W2.04 07.02.00 JR  CTS869 : Changes to Database.c to support entered
//										scanned SKU's less than 6 digits in length.
// W2.02 17.01.00 JR  CTS869 : Changes to Fields.C and Database.c to support
//                    stock take of MarkDown Stock.
// W2.01 13.12.99 JR  CTS051 : Rename TCPSTEP(1.08) as WIXSTEP.NLM(2.01)
//-----------------------------------------------------------------------------
#undef _THIS_SOURCE_HAS_THE_DATA_
#include "wixstep.h"

#define ID_TIMER    1

char programDesc[50] ;
char clientName[] = { "WICKES" };
HWND hMainWnd;

LRESULT CALLBACK WndProc (HWND, UINT, WPARAM, LPARAM) ;
VOID AlertUser (HWND, LPSTR);

/* Function prototypes */
int wInitialise(HWND, HDC, RECT, PAINTSTRUCT );
int iInitialSequence(HWND);

/* Global Socket definition */
SOCKET sSocket;

char szAppName[] = "WinStep" ;

// Local Data structures

UINT_PTR tmrSocketTimerID=0;

// Local function prototypes

int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int iCmdShow);
void StartSocketListenTimer(HWND hwnd);
void StopSocketListenTimer(HWND hwnd);
VOID CALLBACK iMainProgramLogic(HWND hwnd, UINT uMsf, UINT idEvent, DWORD deTime);
void DisplayStr(HWND hwnd, int x, int y, char *str);
void StartSocketListenTimer(HWND hwnd);
void StopSocketListenTimer(HWND hwnd);
void MenuDOSQLStatement(HWND hWnd);
VOID CALLBACK iMainProgramLogic(HWND hwnd, UINT uMsf, UINT idEvent, DWORD deTime);
void DisplayStr(HWND hwnd, int x, int y, char *str);
int wInitialise(HWND hwnd ,HDC hdc ,RECT rect , PAINTSTRUCT ps);
void vCloseDown( HWND hwnd) ;
int iInitialSequence(HWND hwnd);
void vCloseSequence(HWND hwnd);
void vCloseOutHandles(void);

// Program entry point

int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int iCmdShow)
{
	HWND       hwnd;
  MSG        msg;
  WNDCLASSEX wndclass;
  HMENU      hMenu;

  wndclass.cbSize        = sizeof (wndclass);
  wndclass.style         = CS_HREDRAW | CS_VREDRAW;
  wndclass.lpfnWndProc   = WndProc;
  wndclass.cbClsExtra    = 0;
  wndclass.cbWndExtra    = 0;
  wndclass.hInstance     = hInstance;
  wndclass.hIcon         = LoadIcon (NULL, IDI_APPLICATION);
  wndclass.hCursor       = LoadCursor (NULL, IDC_ARROW);
  wndclass.hbrBackground = (HBRUSH) GetStockObject (WHITE_BRUSH);
  wndclass.lpszMenuName  = "ID_MAIN_MENU";//szAppName
  wndclass.lpszClassName = szAppName;
  wndclass.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);

  RegisterClassEx (&wndclass);
  memset(programDesc,0,sizeof(programDesc));
	sprintf_s(programDesc, sizeof(programDesc), "%s HHT Server, Version %s",clientName,APP_VERSION);
  hwnd=CreateWindow(szAppName, programDesc,
  										WS_OVERLAPPEDWINDOW,
                      CW_USEDEFAULT, CW_USEDEFAULT,
                      CW_USEDEFAULT, CW_USEDEFAULT,
                      NULL, NULL , hInstance, NULL) ;

  /* Auto Start */
  SendMessage (hwnd, WM_COMMAND,IDM_FILE_INITIALIZE_SERVER, 0L);

	hMenu = GetMenu(hwnd);
  EnableMenuItem(hMenu, IDM_FILE_END_LOGGING, MF_GRAYED);

	ShowWindow (hwnd, iCmdShow) ;
  UpdateWindow (hwnd) ;
  while (GetMessage (&msg, NULL, 0, 0)) {
    TranslateMessage (&msg) ;
    DispatchMessage (&msg) ;
  }
  return (int)msg.wParam;
}

void AlertUser(HWND hwnd, LPSTR lpszWarning)
{
	MessageBox(hwnd,lpszWarning,"Help",MB_OK | MB_ICONEXCLAMATION);
}				/* end Warning () */

HWND GetMainWindowHandle()
{
	return(hMainWnd);
}

LRESULT CALLBACK WndProc (HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
  PAINTSTRUCT ps;
  RECT rect;
	//char msg[128];
  HMENU hMenu;
	hMainWnd=hWnd;
	hdc = BeginPaint (hWnd, &ps);
  GetClientRect (hWnd, &rect);
  DrawText (hdc, programDesc, -1, &rect,DT_SINGLELINE | DT_CENTER | DT_VCENTER);
  EndPaint (hWnd, &ps);
	//fprintf(ProcessLogHandle(),"WndProc iMsg=%04X, wParam=%04X, wParam=%04X\n",iMsg, HIWORD(wParam),LOWORD(wParam));
  switch (iMsg)
		{
    case WM_COMMAND:
      hMenu=GetMenu(hWnd);
      switch (LOWORD (wParam))
        {
				// File Menu
        case IDM_FILE_INITIALIZE_SERVER:
//	char s[21];
//	SQLGetCurrentTimeString(s);
//	SQLGetCurrentDateString(s);
					ProcessLogSetFilename("WIXSTEP.LOG");
					ProcessLogInitialize();
					ProcessLogStartToFile(FALSE);
					if (wInitialise(hWnd,hdc,rect,ps)==0) {
						AlertUser(hWnd,"Failed on Initialise");
					} else {
					  EnableMenuItem (hMenu, IDM_FILE_INITIALIZE_SERVER, MF_GRAYED);
					  EnableMenuItem (hMenu, IDM_FILE_SHUTDOWN_SERVER, MF_ENABLED);
					}
					//do some work
					return(0);
				case IDM_FILE_SHUTDOWN_SERVER:
					vCloseDown(hWnd);
					EnableMenuItem (hMenu, IDM_FILE_INITIALIZE_SERVER, MF_ENABLED);
					EnableMenuItem (hMenu, IDM_FILE_SHUTDOWN_SERVER, MF_GRAYED);
					return(0);
				case IDM_FILE_BEGIN_LOGGING:
					ProcessLogStartToFile(FALSE);
					//MessageBox(hWnd,"Log file Started - C:\\TOPWINS.LOG","Logging",MB_OK);
					return(0);
				case IDM_FILE_END_LOGGING:
					ProcessLogStop();
					//MessageBox(hWnd,"Log file is closed","Logging",MB_OK);
					return(0);
				case ID_FILE_SQLSTATEMENT:
					//MenuDOSQLStatement(hWnd);
					return(0);
				case IDM_FILE_EXIT_SHUTDOWN_SERVER:
					//vCloseDown(hWnd);
					//EnableMenuItem (hMenu,IDM_FILE_INITIALIZE_SERVER, MF_GRAYED);
					//EnableMenuItem (hMenu,IDM_FILE_SHUTDOWN_SERVER, MF_GRAYED);
	     	  SendMessage (hWnd, WM_CLOSE, 0, 0L);
					return(0);
				// View Menu
				case IDM_VIEW_LOGTOSCREEN:
					return(0);
				// About Menu
				case IDM_ABOUT :
          MessageBox (hWnd,programDesc,szAppName, MB_ICONINFORMATION | MB_OK);
					return(0);
				}
        break;
		case WM_PAINT:
			{
			PAINTSTRUCT ps;
			HDC hdc;
			hdc=BeginPaint(hWnd,&ps);
			EndPaint(hWnd,&ps);
			}
			return(1);
    case WM_SIZE:
			return(0);
    case WM_DESTROY:
      KillTimer (hWnd, ID_TIMER);
			vCloseDown(hWnd);
      PostQuitMessage(0);
			return(0);
		}
	return(DefWindowProc (hWnd, iMsg, wParam, lParam));
}

void StartSocketListenTimer(HWND hWnd)
{
	if (tmrSocketTimerID) {
		 StopSocketListenTimer(hWnd);
	}
	tmrSocketTimerID=SetTimer(hWnd, ID_TIMER, 100, iMainProgramLogic);
}

void StopSocketListenTimer(HWND hWnd)
{
	KillTimer(hWnd,tmrSocketTimerID);
	tmrSocketTimerID=0;
}

/*
SQLRETURN rc;
	char field[255];

void MenuDOSQLStatement(HWND hWnd)
{
//extern SQLHSTMT globalHstmt;       // Statement handle

	//hstmt =
	DoSQLCommand("select * from SYSPAS");
	for (rc = DoSQLFetch() ; rc == SQL_SUCCESS; rc=DoSQLFetch()) {
		int c = 1;
		while (SQLSUCCESS(SQLGetData(hstmt,c++,SQL_C_CHAR,field,sizeof(field),&cbData))) {
			ProcessLogWrite(field);
		}
	}

}
*/


/***************************************************************************
Function:    Main Program Logic
Description: Main program logic scanning sockets application function
Passed:      none
Return:      1 = Success, 0 = Failed
****************************************************************************/
static int iSocketsBusy=FALSE;

VOID CALLBACK iMainProgramLogic(HWND hWnd, UINT uMsf, UINT idEvent, DWORD deTime)
{
	int i;

	if (iSocketsBusy) {
		return;
	}
	iSocketsBusy=TRUE;
  MyTime=time(NULL);
  localtime_s(&pNow, &MyTime);
  strftime(buffer,sizeof(buffer),"%H:%M:%S on %d/%m/%y " ,&pNow);

  FD_ZERO(&readfds);
  sSelTime.tv_sec = 0;
  sSelTime.tv_usec = 10000;
  /* Always check the main socket for activity */
  FD_SET (sSocket, &readfds);

  /* Check all sInUse sockets for activity. */
  for (i = 0; i < MAX_SOCKETS; i++) {
    if (sInUse[i].uiEndOctet) {
			FD_SET(sInUse[i].sSocketId, &readfds);
    }
  }
  /* if there's not any activity, go back to the top of the for loop */
  if (!select(MAX_SOCKETS,&readfds, (fd_set *) 0, (fd_set *) 0,&sSelTime)) {
   	iSocketsBusy=FALSE;
		return;
  }

	/* If you get here, there's some data on a socket to process. */
	/* look for new incoming connections. */
	for (i = 0; i < (int) readfds.fd_count; i++) {
		/* if the readfds structure has a socket that is not in our sInUse array, */
		/* it must be a new incoming connection. */
		if (getInUseIdx(readfds.fd_array[i]) < 0) {
			if (Make_New(sSocket) == - 1) {
		   	iSocketsBusy=FALSE;
	   		return;			/* return on error */
			}
 		}
	}
	/* loop through all the sockets with activity and do a read_echo */
	for(i = 0; i < MAX_SOCKETS; i++) {
		/* if the socket id is not zero (i.e. if is in use) */
		/*   AND it has activity. */
		if(sInUse[i].sSocketId && FD_ISSET(sInUse[i].sSocketId, &readfds)) {
			/* read & echo to the socket iff it is not the main socket. */
			if(sInUse[i].sSocketId != sSocket) {
				if(sUser[i].fLoggedOn == TRUE) {
					sUser[i].lCurrentPos = lApp(sUser[i].lCurrentPos,sInUse[i].sSocketId,i);
				} else {
					sUser[i].fLoggedOn = fLogonAttempt(sInUse[i].sSocketId);
				}
			}
		}
	}
  iSocketsBusy=FALSE;
	return;
}

void DisplayStr(HWND hWnd, int x, int y, char *str)
{
	PAINTSTRUCT ps;
	BeginPaint(hWnd, &ps);
	if (ps.hdc) {
		TextOut(ps.hdc, x*10, y*10, str, strlen(str));
		EndPaint(hWnd, &ps);
	}
}


/***************************************************************************
Function:    Open
Description: Open /Initialise routine
Passed:      windows handle
Return:      1 = Success, 0 = Failed
****************************************************************************/
int wInitialise(HWND hWnd ,HDC hdc ,RECT rect , PAINTSTRUCT ps)
{
	char title[] = "*** TOPPS - RF WINDOWS 95 STEP Program ***";
  hdc = BeginPaint (hWnd, &ps);
  TextOut(hdc,0,0,(LPCSTR)&title,strlen(title));
  EndPaint (hWnd, &ps);
	if (!cn.Connect())
		return 0;
  /* Initial Sequence */
	fprintf (ProcessLogHandle(),"\nWINDOWS/SQL RF STEP program started,version %s\n\n",APP_VERSION);
	if (initialiseSockets(hWnd, &sSocket) < 0) {
    fprintf (ProcessLogHandle(),"\nSocket failed to start, stopping Server.\n");
	  AlertUser(hWnd, (LPSTR) "Socket failed to Start" );
    return(0);
  }
	if(initialiseUsers(hWnd) < 0)
		return(0);
  /* ___if you get here, we're ready to accept incoming messages */
  fprintf (ProcessLogHandle(),"\nServer Ready, Ready To accept messages\n");
	StartSocketListenTimer(hWnd);
  return(1);
}

/***************************************************************************
Function:    Close
Description: Open /Initialise routine
Passed:      windows handle
Return:
****************************************************************************/
void vCloseDown( HWND hWnd)
{
	StopSocketListenTimer(hWnd);
	vCloseSequence(hWnd);
	vCloseOutHandles();
	cn.Disconnect();
	ProcessLogStop();
}
/***************************************************************************
Function:    Close Sequence
Description: Close Sequence of application function
Passed:      none
Return:      none
****************************************************************************/
void vCloseSequence(HWND hWnd)
{
	int i;
	fprintf(ProcessLogHandle(),"\nClosing Sockets\n");
  closesocket(sSocket);    /* close the main socket for the host */
  WSACleanup();
	for (i = 0; i < MAX_SOCKETS; i++) {
    if(sInUse[i].sSocketId) {
      fprintf(ProcessLogHandle(),"\nSocket %u still in use (1)\n",sInUse[i].uiEndOctet);
	    closesocket(sInUse[i].sSocketId);    /* close the socket */
    }
    if(FD_ISSET(sInUse[i].sSocketId, &readfds)) {
			fprintf(ProcessLogHandle(),"\nReceived on Socket %u (2)\n",sInUse[i].uiEndOctet);
	    closesocket(sInUse[i].sSocketId);    /* close the socket */
    }
		cnRS[i].Disconnect();
  }
  fprintf(ProcessLogHandle(),"\nShutdown completed....\n");
}

/***************************************************************************
Function:    Open Log File
Description: Open Log File
Passed:      none
Return:      none
****************************************************************************/
/*
void vOpenLogFile(void)
{
 	MyTime=time(NULL);
  localtime_s(&pNow, &MyTime);
 	strftime(buffer,sizeof(buffer),"%H:%M:%S on %d/%m/%y " ,pNow);
	ProcessLogHandle()	=	fopen("C:\\TOPWINS.LOG","w+");
	fprintf(ProcessLogHandle(),"\nLogging Started @ %s\n",buffer);
}
*/
/***************************************************************************
Function:    Close Log File
Description: Close Log File
Passed:      none
Return:      none
****************************************************************************/
/*
void vCloseLogFile(void)
{
		fprintf(ProcessLogHandle(),"Logging Ends @ %s\n",buffer);
		fclose(ProcessLogHandle());
}
*/
