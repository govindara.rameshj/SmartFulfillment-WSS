//---------------------------------------------------------------------------
// Application: WixStep.nlm. Hand-Held Terminal (HHT) Server for Novell Server
//							and Socket Services. Developed for Wickes.
// Project: WickesRFDeviceNLMSourceEdit - Developed with MS Visual Studio
//					2003 C++ and compiled with Watcom C/C++ and Watcom linker V 11.0
//					using NMAKE.
// --------------------------------------------------------------------------
// Module:Menubase.c -Functions used by lApp() to access AfgCtl, AfpCtl, AfdCtl,
//										SysPas, SysCod, and ActLog tables.
//	These functions are used during loggin, loggout, and for validating user's
//	security level at the main menu. The int return value is rarely used.
//----------------------------------------------------------------------------
#undef _THIS_SOURCE_HAS_THE_DATA_
#include "wixstep.h"

/***************************************************************************
Function:    Read AfgCtl Database
Description: ReadSeek a record in the PICOPT btrieve database file
Passed:      User Number
Return:      0 = Failed, 1 = Success,
LastInput:  "0" = Failed, else "1" = Success
****************************************************************************/
bool ReadAfgCtlTable(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];
	char cmd[255];

// build the column name for the SECL
	strcpy_s(cmd, sizeof(cmd), "select LANG, NUMB, DSCL, DESCR from afgctl where LANG = ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcLang);
	strcat_s(cmd, sizeof(cmd), " and NUMB = ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcAfgn);
	if (cn.DoSQLCommand(cmd)) {
		if (cn.DoSQLFetch()) {
			cn.SQLGetColumn(1, SQL_C_CHAR,user->gcLang,4);
			cn.SQLGetColumn(2, SQL_C_CHAR,user->gcNumb,3);
			cn.SQLGetColumn(3, SQL_C_CHAR,user->gcDscl,3);
			cn.SQLGetColumn(4, SQL_C_CHAR,user->gcDesc,36);
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
			return true;
		}
	}
  strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
  return false;
}
/***************************************************************************
Function:    Read AfpCtl Database
Description: Read a record in the AFPCTL btrieve database file
Passed:      User Number
Return:      0 = Failed, 1 = Success,
LastInput:  "0" = Failed, else "1" = Success
****************************************************************************/
bool ReadAfpCtlTable(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	char cmd[255];
	strcpy_s(cmd, sizeof(cmd), "select LANG, AFGN, NUMB, DESCR, SECL from afpctl where LANG = ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcLang);
	strcat_s(cmd, sizeof(cmd), " and AFGN = ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcAfgn);
	strcat_s(cmd, sizeof(cmd), " and NUMB = ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcAfpn);
	if (cn.DoSQLCommand(cmd)) {
		if (cn.DoSQLFetch()) {
			cn.SQLGetColumn(1, SQL_C_CHAR, user->fpLang, 4);
			cn.SQLGetColumn(2, SQL_C_CHAR, user->fpAfgn, 3);
			cn.SQLGetColumn(3, SQL_C_CHAR, user->fpNumb, 3);
			cn.SQLGetColumn(4, SQL_C_CHAR, user->fpDesc, 36);
			cn.SQLGetColumn(5, SQL_C_CHAR, user->fpSecl, 3);
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
			return true;
		}
	}
  strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
  return false;
}
/***************************************************************************
Function:    Read AfdCtl Database
Description: Read a record in the AFDCTL btrieve database file
Passed:      User Number
Return:      0 = Failed, 1 = Success,
LastInput:  "0" = Failed, else "1" = Success
****************************************************************************/
bool ReadAfdCtlTable(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	char cmd[255];
	strcpy_s(cmd, sizeof(cmd), "select LANG, AFGN, AFGP, TEXT1, TEXT2, TEXT3 from afdctl where LANG = ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcLang);
	strcat_s(cmd, sizeof(cmd), " and AFGN = ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcAfgn);
	strcat_s(cmd, sizeof(cmd), " and AFPN = ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcAfpn);
	if (cn.DoSQLCommand(cmd)) {
		if (cn.DoSQLFetch()) {
			cn.SQLGetColumn(1, SQL_C_CHAR,user->dcLang,4);
			cn.SQLGetColumn(2, SQL_C_CHAR,user->dcAfgn,3);
			cn.SQLGetColumn(3, SQL_C_CHAR,user->dcAfgp,3);
			strcpy_s(user->pcAdl1, sizeof(user->pcAdl1),"1.");
			cn.SQLGetColumn(3, SQL_C_CHAR, &user->pcAdl1[2],18);
			strcpy_s(user->pcAdl2, sizeof(user->pcAdl2),"2.");
			cn.SQLGetColumn(3, SQL_C_CHAR, &user->pcAdl2[2],18);
			strcpy_s(user->pcAdl3, sizeof(user->pcAdl3),"3.");
			cn.SQLGetColumn(3, SQL_C_CHAR, &user->pcAdl3[2],18);
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
			return true;
		}
	}
  strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
  return false;
}

/***************************************************************************
Function:    Read SYSPAS Database
Description: Read a record in the SYSPAS btrieve database file
Passed:      User Number
Return:      0 = Failed, 1 = Success
LastInput:  "0" = Failed, else "1" = Success, 2 = User deleted(3.07)
****************************************************************************/
bool ReadSysPasTable(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	char SECLxx[12];
	char DELC;
	char cmd[255];
	// build the column name for the SECL
	strcpy_s(SECLxx, sizeof(SECLxx), "SECL");
	_itoa_s(atoi(user->pcAfgn), SECLxx+4, sizeof(SECLxx)-4, 10);
	strcpy_s(cmd, sizeof(cmd), "SELECT NAME, PASS, DELC, ");
	strcat_s(cmd, sizeof(cmd), SECLxx);
	strcat_s(cmd, sizeof(cmd), " FROM SYSPAS WHERE EEID = ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcEeid);
	if (cn.DoSQLCommand(cmd)) {
		if (cn.DoSQLFetch()) {
			cn.SQLGetColumn( 1, SQL_C_CHAR, user->pcEnam, 20);
			cn.SQLGetColumn( 2, SQL_C_CHAR, user->spPass, 6);
			cn.SQLGetColumn( 3, SQL_C_BIT, &DELC, 1);
			cn.SQLGetColumn( 4, SQL_C_CHAR, user->pcSecl, 3);
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
			if(DELC != 0) {
				strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "2");
			}
			return true;
		}
	}
	//MessageBox(NULL, itoa(SQLLastReturnCode(), cmd, 10), "", MB_OK);
  strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
  return false;
}
/***************************************************************************
Function:	 Read SYSCOD Fields
Description: This function reads the SYSCOD record and sets the user
				 fields.
Passed:		 SYSCOD record
				 User Number
Return:		 None
****************************************************************************/
int near ReadSysCod(int sessionNumber, char *pType, char *pAttr, char *pCode, char *codeCompareType)
{
	USER_TYPE *user = &sUser[sessionNumber];

	char cmd[255];
	strcpy_s(cmd, sizeof(cmd), "select TYPE, CODE, DESCR, ATTR from SYSCOD where TYPE = ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), pType);
	if (pCode != NULL) {
		strcat_s(cmd, sizeof(cmd), " and CODE");
		strcat_s(cmd, sizeof(cmd), codeCompareType);
		cn.SQLQueryAddVarChar(cmd, sizeof(cmd), pCode);
		}
	if (pAttr != NULL) {
		strcat_s(cmd, sizeof(cmd), " and ATTR = ");
		cn.SQLQueryAddVarChar(cmd, sizeof(cmd), pAttr);
		}
	strcat_s(cmd, sizeof(cmd), " order by TYPE asc, CODE asc");
	if (cn.DoSQLCommand(cmd)) {
		if (cn.DoSQLFetch()) {
			cn.SQLGetColumn(1, SQL_C_CHAR,user->syType,3);
			cn.SQLGetColumn(2, SQL_C_CHAR,user->syCode,3);
			cn.SQLGetColumn(3, SQL_C_CHAR,user->syDesc,36);
			cn.SQLGetColumn(4, SQL_C_CHAR,user->syAttr,2);
			return (1); // Found a record
		}
		return (0); // No record
	}
return (-1); // DB error
}
/***************************************************************************
Function:    Seek SYSCOD Database
Description: Seek a "LR" record in the SYSCOD btrieve database file
Passed:      User Number , Option = 1  Seek via LastInput
                           Option = 2  Seek via Saved Code
                           Option = 0  Seek for ATTR = "?"
Return:      0 = Failed, 1 = Success("LR" found & ATTR) 2 = ("LR" not found)
                         3 = EOF ; 4 =Success("LR" found Not "ATTR")
****************************************************************************/
bool iSeekSysCodDatabase(int sessionNumber, int iOption)
{
	USER_TYPE *user = &sUser[sessionNumber];

	int result = -1;
	char s[12];
	switch (iOption) {
		case 0:	// Start seek for Type/Attr/Code
			result = ReadSysCod(sessionNumber, user->pcLrType, user->pcLrAttr, NULL, NULL);
			break;
		case 1: // Seek for Type/(User entered) Code
			ZeroPadField(s,user->pcLastInput,2);
			result = ReadSysCod(sessionNumber, user->pcLrType, NULL, s, " = ");
			break;
		case 2: // Seek for Type/(first) Code
			result = ReadSysCod(sessionNumber, user->pcLrType, NULL, user->pcSyStart, " = ");
			break;
		}
	if (result <= 0) {
    strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
    return(0);
		}
	if (strcmp(user->syType,user->pcLrType) == 0) {
		if (strcmp(user->syAttr,user->pcLrAttr) == 0) {
      strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
	  } else {
     	strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "2");
		}
		return(true);
	}
	return(false);
}

/***************************************************************************
Function:    Get Next SYSCOD Database
Description: Get the next SYSCOD record in the SYSCOD file
                         for Label Request Reasons
Passed:      User Number
Return:      0 = Failed, 1 = Success("LR" found & ATTR) 2 = ("LR" not found)
                         3 = EOF ;
****************************************************************************/
int iGetNextSysCodDatabase(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	if (ReadSysCod(sessionNumber, user->pcLrType, NULL, user->pcSyStart, " > ")) {
		if (!memcmp(user->syType, user->pcLrType, sizeof(user->pcLrType)) &&
			 !memcmp(user->syAttr, user->pcLrAttr, sizeof(user->pcLrAttr))) {
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
			return(1);
		}
		strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "2");
		return(2);
	}
	strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
	return(0);

/*

char keyBuf[21];
	BTI_SINT iDataBaseStatus = B_NO_ERROR;
	iDataBaseStatus=DBAccessTableData(&dbtSysCod,B_GET_NEXT,keyBuf,0);
  if ( iDataBaseStatus == B_END_OF_FILE ) {
		strcpy(sUser[i].pcLastInput, "0");
    return(0);
	}
  if (iDataBaseStatus==B_NO_ERROR){
    vReadSysCod(&syscod, i);
		if ((memcmp(sUser[i].syType,sUser[i].pcLrType,sizeof(sUser[i].syType)) == 0) &&
				(memcmp(sUser[i].syAttr,sUser[i].pcLrAttr,sizeof(sUser[i].syAttr)) == 0)) {
        strcpy(sUser[i].pcLastInput, "1");
	      return(1);
				}
    strcpy(sUser[i].pcLastInput, "2");
    return(2) ;
  }
  if ( iDataBaseStatus == B_END_OF_FILE ) {
		strcpy(sUser[i].pcLastInput, "3");
	  return(3);
  }
  return(0); // Seek Failed
*/
//return(0);
}
/***************************************************************************
Function:    Read Prior SYSCOD Database
Description: Read prior record in the SYSCOD btrieve database file
Passed:      User Number
Return:      0 = Failed, 1 = Success("LR" & ATTR found) 2 = ("LR" or "ATTR" not found)
                         3 = EOF ;
****************************************************************************/
int iPriorSysCodDatabase(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	if (ReadSysCod(sessionNumber, user->pcLrType, NULL, user->pcSyStart, " < ")) {
		if (!memcmp(user->syType, user->pcLrType, sizeof(user->pcLrType)) &&
			 !memcmp(user->syAttr, user->pcLrAttr, sizeof(user->pcLrAttr))) {
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
			return(1);
		}
		strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "2");
		return(2);
	}
	strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
	return(0);
/*
char keyBuf[21];
	BTI_SINT iDataBaseStatus = B_NO_ERROR;
  // GET THE RECORD WITH GET_EQUAL AND UPDATE IT
 	iDataBaseStatus=DBAccessTableData(&dbtSysCod,B_GET_PREVIOUS,keyBuf,0);
  if ( iDataBaseStatus == B_END_OF_FILE ) {
    return(3); // BOF detected
  }
  if ( iDataBaseStatus == B_NO_ERROR ) {
    vReadSysCod(&syscod, i);
    if (memcmp(sUser[i].syType,sUser[i].pcLrType,sizeof(sUser[i].syType)) == 0) {
	    if (memcmp(sUser[i].syAttr,sUser[i].pcLrAttr,sizeof(sUser[i].syAttr)) == 0) {
        strcpy(sUser[i].pcLastInput, "1");
	      return(1);
		  } else {
        strcpy(sUser[i].pcLastInput, "2");
	      return(2) ;
		  }
	  }
    strcpy(sUser[i].pcLastInput, "2");
    return(2) ;
   }
 return(0); // Seek Failed
*/
return(0);
}

/****************************************************************************
Function   : Insert a ActLog record                                                                     *W2.06*
Description: Insert a Activity Log record in the Database file
Passed:      User Number and Login/Out identifier
Return:      0  = Failed, 1  = Success
LastInput:   0  = Failed, 1  = Success; 2 = Duplicate Entry
****************************************************************************/
bool PutInsertActivityLog(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	char cmd[255];
	char s[21];
	unsigned long int pk = 0;
	if (cn.DoSQLCommand("select max(TKEY) from actlog")) {
		if (cn.DoSQLFetch()) {
			cn.SQLGetColumn(1, SQL_C_ULONG,&pk,sizeof(pk));
		}
	}
	pk+=1;
	cn.SQLGetCurrentTimeString(s, sizeof(s));
	strcpy_s(cmd, sizeof(cmd), "INSERT INTO ACTLOG (DATE1, EEID, WSID, AFGN, AFPN, LOGI, FORC, STIM, ETIM, EDAT) VALUES (");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), cn.SQLGetCurrentDateString(s, sizeof(s)));
	strcat_s(cmd, sizeof(cmd),", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcEeid);
	strcat_s(cmd, sizeof(cmd),", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcWsid);
	strcat_s(cmd, sizeof(cmd),", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcAfgn);
	strcat_s(cmd, sizeof(cmd),", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcAfpn);
	strcat_s(cmd, sizeof(cmd),", ");
	if (user->pcLogin[0]=='I') {
		strcat_s(cmd, sizeof(cmd), "1");
	} else {
	  strcat_s(cmd, sizeof(cmd), "0");
	}
	strcat_s(cmd, sizeof(cmd),", ");
	if (user->pcSystemOut[0]=='Y') {
    strcat_s(cmd, sizeof(cmd), "1");
	} else {
    strcat_s(cmd, sizeof(cmd), "0");
	}
	strcat_s(cmd, sizeof(cmd),", ");
	if (user->pcLogin[0]=='O') {
	  cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcEndTime);
	} else {
	  cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcStartTime);
	}
	strcat_s(cmd, sizeof(cmd),", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), GetCurrentTimeStr(NULL));
	strcat_s(cmd, sizeof(cmd),", ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), cn.SQLGetCurrentDateString(s, sizeof(s)));
	strcat_s(cmd, sizeof(cmd),")");
	if (cn.DoSQLCommand(cmd)) {
		if (memcmp(user->pcSubMenu,"NO",2) == 0) {
			user->alTkey=pk;
		}	else {	// else Sub Menu Option
	  	user->alSubTkey=pk;
		}
		strcpy_s(user->pcLastInput, sizeof(user->pcLastInput),"1");
		return(true); // Seek Passed
	}
  strcpy_s(user->pcLastInput, sizeof(user->pcLastInput),"0");
  return(false);
}
/****************************************************************************
Function   : Update the ActLog record                                                                     *W2.06*
Description: Update the Activity Log record in the Database file
Passed:      User Number and Login/Out identifier
Return:      0  = Failed, 1  = Success
LastInput:   0  = Failed, 1  = Success; 2 = Duplicate Entry
****************************************************************************/
bool iUpdateActLogDatabase(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	char cmd[255];
	char s[21];
	unsigned long int pk = 0;
	strcpy_s(cmd, sizeof(cmd), "update actlog set LOGI = 0, EDAT = ");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), cn.SQLGetCurrentDateString(s, sizeof(s)));
	if (memcmp(user->pcLogin,"OUT",strlen(user->pcLogin)) == 0) {
		strcat_s(cmd, sizeof(cmd), ", ETIM = ");
		cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcEndTime);
		}
	strcat_s(cmd, sizeof(cmd), " where TKEY = ");
	if(memcmp(user->pcSubMenu, "NO", 2) == 0) {
		_itoa_s(user->alTkey, s, sizeof(s), 10);
		strcat_s(cmd, sizeof(cmd), s);
	}	else {	  // else Update is for Sub Menu Option
		_itoa_s(user->alSubTkey, s, sizeof(s), 10);
	  strcat_s(cmd, sizeof(cmd), s);
	}
	if (cn.DoSQLCommand(cmd)) {
		strcpy_s(user->pcLastInput, sizeof(user->pcLastInput),"1");
		return(true); // Seek Passed
	}
  strcpy_s(user->pcLastInput, sizeof(user->pcLastInput),"0");
  return(false);
}


