#undef _THIS_SOURCE_HAS_THE_DATA_
#include "wixstep.h"
#include "afx.h"
//#include <configuration>

SQLCHAR SQL_CONNECTION_STRING[256] = "DSN=WIXSTEP;UID=sa;PWD=W1ckes";

/*
class SQLConnection
{
	HWND hWnd;
  HENV henv;         // Environment
  HDBC hdbc;         // Connection handle
  SDWORD cbData;     // Output length of data
	SQLHSTMT hstmt;    // Statement handle

public:
	RETCODE rc;        // ODBC return code
  SQLConnection();           // Constructor
	bool Connect(HWND hWnd);
	void DisplaySQLError(char *cmd);
	bool Disconnect(void);
	bool DoSQLCommand(char *cmdstr);
	bool DoSQLFetch(int fetchType);
	bool SQLGetColumn(int columnNumber, int C_columnType, void *buffer, int length);
	char *SQLGetCurrentDateString(char *s);
	char *SQLGetCurrentTimeString(char *s);
	char *SQLQueryAddVarChar(char *cmd, char *varChar);
};
*/
SQLConnection::SQLConnection()
{
	henv = 0;         // Environment
  hdbc = 0;         // Connection handle
	hstmt = 0;    // Statement handle

}

// Allocate environment handle, allocate connection handle,
// connect to data source, and allocate statement handle.
bool SQLConnection::Connect(void)
{
	int ConnStrOutLen;
	char ConnStrOut[1024];
	SQLCHAR *s = SQL_CONNECTION_STRING;
	//SQLConnection::hWnd = hWnd;
  //SQLAllocEnv(&henv);
	SQLAllocHandle(SQL_HANDLE_ENV, 0, &henv);
	SQLRETURN ret;
	ret = SQLSetEnvAttr(henv, SQL_ATTR_ODBC_VERSION, (SQLPOINTER)SQL_OV_ODBC3, 0);
	if (!SQLSUCCESS(ret)) {
		SQLSTATE state;
		SQLCHAR stateText[4096];
		SQLGetDiagRec(SQL_HANDLE_ENV, henv, 1, state, NULL, stateText, sizeof(stateText), NULL);
		return false;
	}

	SQLCHAR errorCode[20];
	SQLCHAR errorMessage[1024];
	SQLSMALLINT messageLength;
	SQLINTEGER errorNumber;
  SQLAllocConnect(henv,&hdbc);
	rc=SQLDriverConnect(hdbc, NULL, s, SQL_NTS, (SQLCHAR *)(ConnStrOut), sizeof(ConnStrOut), (SQLSMALLINT *)(&ConnStrOutLen), SQL_DRIVER_NOPROMPT);
	if (rc == SQL_ERROR) {
		int i = 1;
		int rc2;
		while ((rc2 = SQLGetDiagRec(SQL_HANDLE_DBC, hdbc, i, errorCode, &errorNumber, errorMessage, 1024, &messageLength)) != SQL_NO_DATA) {
			MessageBox(NULL, (LPCSTR)errorMessage, (LPSTR)errorCode, MB_OK);
			i++;
		}
    MessageBox(NULL,(LPCSTR)(SQL_CONNECTION_STRING),"SQL Connection Error",MB_OK);
	}
  if (!SQLSUCCESS(rc))  {
		// Deallocate handles, display error message, and exit.
    SQLFreeEnv(henv);
    SQLFreeConnect(hdbc);
    MessageBox(NULL,(LPCSTR)(SQL_CONNECTION_STRING),"SQL Connection Error",MB_OK);
		return false;
	}
connected = true;
return true;
}
// Display error message in a message box that has an OK button.
void SQLConnection::DisplaySQLError(char *cmd)
{
  unsigned char szSQLSTATE[10];
  SDWORD nErr=0;
  unsigned char msg[SQL_MAX_MESSAGE_LENGTH+1];
  SWORD cbmsg;
	bool errFound = false;
	char s[256];
  while(SQLError(0,0,hstmt,szSQLSTATE,&nErr,msg,sizeof(msg),&cbmsg)==SQL_SUCCESS) {
    wsprintf((char *)s,"Error:\nSQLSTATE=%s, Native error=%ld, msg='%s'\n\n%s", szSQLSTATE, nErr, msg, cmd);
    MessageBox(NULL,(const char *)s,"SQL Error",MB_OK);
		errFound = true;
  }
	if (!errFound) {
		MessageBox(NULL,"Unknown Error", "SQL Error", MB_OK);
	}
}
// Free the statement handle, disconnect, free the connection handle, and
// free the environment handle.
bool SQLConnection::Disconnect(void)
{
 	SQLFreeStmt(hstmt,SQL_RESET_PARAMS);
	SQLDisconnect(hdbc);
  SQLFreeConnect(hdbc);
  SQLFreeEnv(henv);
	connected = false;
	return true;
}

bool SQLConnection::DoSQLCommand(char *cmdstr)
{
	if (!connected) {
			Connect();
	}
	if (hstmt != 0) {
		SQLFreeStmt(hstmt, SQL_DROP);
		hstmt = 0;
	}
	SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);
  rc=SQLExecDirect(hstmt, (SQLCHAR *)(cmdstr),SQL_NTS);
	if (!SQLSUCCESS(rc)) { //Error
		DisplaySQLError(cmdstr);
    // Deallocate handles and disconnect.
    //SQLDisconnect(hdbc);
    //SQLFreeConnect(hdbc);
    //SQLFreeEnv(henv);
    return false;
		}
	return true;
}


bool SQLConnection::DoSQLCommand(char *cmdstr, int cursorType)
{
	if (!connected) {
			Connect();
	}
	if (hstmt != 0) {
		SQLFreeStmt(hstmt, SQL_DROP);
		hstmt = 0;
	}
	SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);
	cursorType = SQL_SCROLLABLE;
	SQLSetStmtAttr(hstmt, SQL_ATTR_CURSOR_TYPE, (void *)(SQL_CURSOR_DYNAMIC), SQL_IS_INTEGER);
  rc=SQLExecDirect(hstmt, (SQLCHAR *)(cmdstr),SQL_NTS);
	if (!SQLSUCCESS(rc)) { //Error
		DisplaySQLError(cmdstr);
    // Deallocate handles and disconnect.
    //SQLDisconnect(hdbc);
    //SQLFreeConnect(hdbc);
    //SQLFreeEnv(henv);
    return false;
		}
	return true;
}

bool SQLConnection::DoSQLFetch()
{
	int ret;
	ret = SQLFetch(hstmt);
	return(SQLSUCCESS(ret));
}

bool SQLConnection::DoSQLFetch(int fetchType)
{
	int ret;
	ret = SQLFetchScroll(hstmt, fetchType, 0);
	//if (!SQLSUCCESS(ret)) {	DisplaySQLError(""); }
	return(SQLSUCCESS(ret));
}

bool SQLConnection::SQLGetColumn(int columnNumber, int C_columnType, void *buffer, int length)
{
	SQLRETURN ret;
	SQLINTEGER valueNull;

	if (length > 0) memset(buffer, 0, length);
	ret = SQLGetData(hstmt, columnNumber, C_columnType, buffer, length, &valueNull);
	if (SQLSUCCESS(ret) == false) {
		SQLSTATE state;
		SQLCHAR stateText[4096];
		SQLGetDiagRec(SQL_HANDLE_STMT, hstmt, 1, state, NULL, stateText, sizeof(stateText), NULL);
		return false;
	}
	switch (C_columnType) {
		case SQL_C_DATE:
			break;
		case SQL_C_CHAR:
/*
			char *p;
			p = (char *)(buffer);
			while ((strlen(p)>0) && (p[strlen(p) - 1] = 32)) {
				p[strlen(p) - 1] = 0;
			}
*/
			break;
		case SQL_C_BIT:
			break;
		default:
			break;
	}
	return true;
}

char *SQLConnection::SQLGetCurrentDateString(char *s, size_t sSize)
{
	struct tm pNow;
	time_t MyTime;
	MyTime=time(NULL);
  localtime_s(&pNow, &MyTime);
	sprintf_s(s, sSize, "%04d-%02d-%02d", pNow.tm_year+1900, pNow.tm_mon+1, pNow.tm_mday);
	return(s);
}

char *SQLConnection::SQLGetCurrentTimeString(char *s, size_t sSize)
{
	struct tm pNow;
	time_t MyTime;
	//char h[4],m[4],c[4];
	MyTime=time(NULL);
  localtime_s(&pNow, &MyTime);
	sprintf_s(s, sSize, "%02d%02d%02d", pNow.tm_hour, pNow.tm_min, pNow.tm_sec);
	//sprintf_s(s, sSize, "%s%s%s", ZeroPadField(h, pNow.tm_hour,2), ZeroPadField(m, pNow.tm_min,2), ZeroPadField(c, pNow.tm_sec,2));
	return(s);
}

char *SQLConnection::SQLQueryAddVarChar(char *cmd, rsize_t cmdSizeInBytes, char *varChar)
{
	CString workVarChar(varChar);
	workVarChar.Replace("'", "''");
	workVarChar.Insert(0, '\'');
	workVarChar.Append(CString('\''));

	strcat_s(cmd, cmdSizeInBytes, workVarChar);
	return cmd;
}



