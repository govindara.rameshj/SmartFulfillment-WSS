//---------------------------------------------------------------------------
// Application: WixStep.nlm. Hand-Held Terminal (HHT) Server for Novell Server
//							and Socket Services. Developed for Wickes.
// Project: WickesRFDeviceNLMSourceEdit - Developed with MS Visual Studio
//					2003 C++ and compiled with Watcom C/C++ and Watcom linker V 11.0
//					using NMAKE.
// --------------------------------------------------------------------------
// Module:Utils.c - Socket and user routines.
//	Each HHT has an index in sInUse[], which holds socket information, and in
//	sSvUse[], which holds more socket info and some flags that tell which tables
//	have been opened. There are also arrays to hold inuse skun's and menu
//	security levels.
//----------------------------------------------------------------------------
#include "wixstep.h"

/* W1.06  Save the Socket detail */
typedef struct {
  SOCKET sSocketId;             /*   Socket identification        */
  unsigned int uiEndOctet;      /*   TCP type D address           */
  int	iHandles;							    /*   Handles open for this user	  */
} SVUSE_TYPE;

SVUSE_TYPE sSvUse[MAX_SOCKETS];					/*W1.06*/

//extern iNumberUsers;
extern void  AlertUser(HWND,LPSTR);

/***************************************************************************
Function:	 Initialise
Description: Initialise and startup socket
Passed:		 Socket handle
Return:		 0 = Success else error number
****************************************************************************/
int initialiseSockets(HWND hWnd, SOCKET *pSocket)
{
	int i;
	char message[70];
	struct sockaddr_in sAddr;

	/* Initialise sInUse array */
	memset((char *)sInUse, 0, sizeof(sInUse));
	for (i = 0; i < MAX_SOCKETS; i++) {
		sInUse[i].sSocketId = 0;
		sInUse[i].uiEndOctet = 0;
		sSvUse[i].sSocketId = 0;						/* W1.06	*/
		sSvUse[i].uiEndOctet = 0;						/* W1.06	*/
		sSvUse[i].iHandles = 0;							/* W1.06	*/
		/* Reset all File Open flags per User			  T1.02*/
		iCurrentState[i] = 0;
	}

   /* Initialize the Windows Sockets DLL v1.1 */
	i = WSAStartup(0x0101, &WsaData);
	if(i == SOCKET_ERROR) {
		sprintf_s(message,sizeof(message),"ERROR WSAStartup() failed: %ld\n", GetLastError());
		fprintf(ProcessLogHandle(),message);
		AlertUser(hWnd, message);
		return(-1);
	}
   /* fprintf(ProcessLogHandle(),"WSAStartup loaded\n"); */
	MyTime=time(NULL);
	localtime_s(&pNow, &MyTime);
  strftime(buffer,sizeof(buffer),"%H:%M:%S on %d/%m/%y",&pNow);
	fprintf(ProcessLogHandle(),"WSAStartup loaded @ %s\n",buffer);

   /* Get a socket for incoming messages */
  *pSocket = socket(AF_INET, SOCK_STREAM, 0);
  if(*pSocket == INVALID_SOCKET) {
		sprintf_s(message,sizeof(message),"ERROR socket() failed: %ld\n", GetLastError());
		fprintf(ProcessLogHandle(),message);
		AlertUser(hWnd, message);
		return(-2);
	}

	ZeroMemory(&sAddr,sizeof(sAddr));
	sAddr.sin_family = AF_INET;
	sAddr.sin_port = htons(MAIN_PORT);

	i = bind(*pSocket, (struct sockaddr *)&sAddr, sizeof(sAddr));
	if(i == SOCKET_ERROR) {
		closesocket(*pSocket);
		sprintf_s(message, sizeof(message),"ERROR bind() failed: %ld\n", GetLastError());
		fprintf(ProcessLogHandle(),message);
		WSACleanup();
 	  AlertUser(hWnd, message);
		return(-3);
	}

	/* prepare to accept upto 5 pending client connections */

	i = listen(*pSocket,5);
	if(i == SOCKET_ERROR) {
		closesocket(*pSocket);
		sprintf_s(message,sizeof(message),"ERROR listen() failed: %ld\n", GetLastError());
		fprintf(ProcessLogHandle(),message);
		WSACleanup();
 	  AlertUser(hWnd, message);
		return(-4);
	}
	return(0);
}

/***************************************************************************
Function:	 Initialise Users
Description: Initialise the user fields, open Macro file, and set up the 
             data access connection string.
Passed:		 none
Return:		 0 = Success else error number
****************************************************************************/
int initialiseUsers(HWND hWnd)
{
	int i;
	char message[70];
	for (i = 0; i < MAX_SOCKETS; i++) {
		sUser[i].lCurrentPos = 0L;
		sUser[i].fLoggedOn = FALSE;
		memset(sUser[i].pcLastInput,0,sizeof(sUser->pcLastInput));
	}
	fopen_s(&hCommandFile, "WIXSTEP.TXT","rt");
	if (hCommandFile == NULL) {
		sprintf_s(message,sizeof(message),"ERROR Unable to open script file at WIXSTEP.TXT\n");
		AlertUser(hWnd, message);
		fprintf(ProcessLogHandle(),message);
		return(-1);
	}
	memset(pcSpaces,' ',SCREEN_COLS);
	pcSpaces[SCREEN_COLS] = 0;

	/* new code below to obtain the connection string from Oasys3.config */
	/*
	FILE *configFile;
	char configLine[1024];
	char linePart[1024];
	bool insideConnectStrings = false;
	
	if (fopen_s(&configFile, "Oasys3.config", "rt")) {
		sprintf_s(message,sizeof(message),"ERROR Unable to open configuration file at Oasys3.config\n");
		AlertUser(hWnd, message);
		fprintf(ProcessLogHandle(),message);
		return(-1);
	}

	while (!feof(configFile)) {
		if (fgets(configLine, sizeof(configLine), configFile) == NULL) {
			sprintf_s(message,sizeof(message),"ERROR Read failure in configuration file at Oasys3.config\n");
			AlertUser(hWnd, message);
			fprintf(ProcessLogHandle(),message);
			return(-1);
		}
		if (!insideConnectStrings) {
			if (strstr(configLine, "ConnectStrings") != NULL) {
				insideConnectStrings = true;
			}
			continue;
		}
		if (strstr(configLine, "/ConnectStrings") != NULL) {
			insideConnectStrings = false;
			continue;
		}
		if (strstr(configLine, "/ConnectStrings") != NULL) {
		}
	}
	//"DSN=WIXSTEP;UID=sa;PWD=W1ckes"
  */

	return(0);
}

/***************************************************************************
Function:	 Close Socket
Description: Close the sockets
Passed:		 Message string
				 Socket Number
				 code debug
Return:		 none
****************************************************************************/
void vCloseSocket( char * msg, SOCKET socket, int code)
{
  char szDate [10];
  char szTime [10];
  fprintf(ProcessLogHandle(),"%s %u %s %s rtn=%u\n", msg, getInUseOctet(socket),
	             _strdate_s(szDate, sizeof(szDate)), _strtime_s(szTime, sizeof(szTime)), code);
  /*___wipe out it's entry in the sInUse array &mark this guy as a goner... */
  setInUseOctet(socket, 0); /* 0 to wipe out it's entry in the sInUse array */
  closesocket(socket);
}

/***************************************************************************
Function:	 Get in use Index
Description: Given a socket, lookup its index into the sInUse array
Passed:		 Socket Number
Return:		 User number or -1 if failed
****************************************************************************/
int getInUseIdx(SOCKET s)
{
	int ii;

	for (ii = 0; ii < MAX_SOCKETS; ii++) {
		if (s == sInUse[ii].sSocketId){
			return ii;
		}
	}

	return -1;
}


/***************************************************************************
Function:	 Get in use Octet
Description: Given a socket, return the last octet of the IP address
Passed:		 Socket Number
Return:		 Octet number or -1 if failed
****************************************************************************/
unsigned int getInUseOctet(SOCKET s)
{
	int idx = getInUseIdx(s);

	if (idx >= 0) {
		return sInUse[idx].uiEndOctet;
	} else {
		return 0;
	}
}

/***************************************************************************
Function:	 Get in use Octet Index
Description: Given an Octet number, lookup its index into the sSvUse array
Passed:		 Octet Number
Return:		 User number or -1 if failed
****************************************************************************/
int getInUseOidx(unsigned int uiOctet) {
	int ii;
//	fprintf(ProcessLogHandle(),"enter getInUseOidx: Octet is %d \n",uiOctet);
	for (ii = 0; ii < MAX_SOCKETS; ii++) {
//	fprintf(ProcessLogHandle(),"checking %d for in Use : sSvUse[ii].uiEndOctet: %d \n",
//						ii,sSvUse[ii].uiEndOctet);
		if (uiOctet == sSvUse[ii].uiEndOctet) {
//		fprintf(ProcessLogHandle(),"Saved index is %d & Octet In Use is %d \n",ii,
//										 uiOctet);
			return ii;
		}
	}

	return -1;
}


/***************************************************************************
Function:	 Set in use Octet
Description: Given a Socket & the last octet of the IP address,
				 mark it sInUse Called by vCloseSocket with octet=0 to
				 remove a sockets entry in sInUse
Passed:		 Socket Number
				 Octet number
Return:		 none
****************************************************************************/
void setInUseOctet(SOCKET sSocket, unsigned int uiOctet)
{
// clear down the Socket In Use
// leave the Saved Socket open.....

  int ii;
  /* look up the index for this socket */
  int iIndex = getInUseIdx(sSocket);

	if (iIndex >= 0) { /* implies it's sInUse or active so zero out the entry */
		sInUse[iIndex].uiEndOctet = uiOctet;	/* will be zero to close a socket */
		if (uiOctet == 0){
			fprintf(ProcessLogHandle(),"Init socket %d \n ",sInUse[iIndex].sSocketId);
			sInUse[iIndex].sSocketId = 0;
			/* and clear down the Saved values W1.06 */
//			sSvUse[iIndex].sSocketId = 0;
//			sSvUse[iIndex].uiEndOctet = 0;
		}
		return;
	} else { /* got a -1 so this is a new socket */
		for(ii = 0; ii < MAX_SOCKETS; ii++) {
			if(sInUse[ii].sSocketId == 0) {
				sInUse[ii].sSocketId = sSocket;
				sInUse[ii].uiEndOctet = uiOctet;
				return;
			}
		}
	}
}

/***************************************************************************
Function:	 Clear Outstanding Connections (inevent of Poweroff )
Description: Given a Socket & the last octet of the IP address,
				 Check the Address matches in the Save InUse Array
				 then, close the associated socket and close the files
Passed:		 Socket Number
				 Octet number
Return:		 none
****************************************************************************/
void clearOldConnects(SOCKET sSocket, unsigned int uiOctet)
{
  SOCKET ds;
  /* look up the index for outstanding socket connection*/
  int iOctIndex = getInUseOidx(uiOctet);
  /* look up the index for this socket */
  int iIndex = getInUseIdx(sSocket);
  fprintf(ProcessLogHandle(),"iOctIndex is %d Saved Octet is %d and Current Octet is %d\n",
		iOctIndex, sSvUse[iOctIndex].uiEndOctet,uiOctet);
     /* Connection already open? */
	if (sSvUse[iOctIndex].uiEndOctet == uiOctet) {
		if (sSvUse[iOctIndex].iHandles == 1) { 				/* T1.02 */
			MyTime=time(NULL);								/* T1.02 */
			localtime_s(&pNow, &MyTime);
			strftime(buffer,sizeof(buffer),"%H:%M:%S on %d/%m/%y",&pNow);
			fprintf(ProcessLogHandle(),"Closing files after Reboot @ %s\n",buffer);
		}
		ds = sSvUse[iOctIndex].sSocketId ;
		fprintf(ProcessLogHandle(),"iOctIndex is %d & ds is %d & sSocket is %d \n",
		 						iOctIndex,ds,sSocket);
		if (ds != sSocket ) {
			fprintf(ProcessLogHandle(),"closing socket %d for unit %d \n",ds,iOctIndex);
			closesocket(ds);
			setInUseOctet(ds, 0);					/*TO1.02*/
		}
		sSvUse[iOctIndex].sSocketId = 0 ;
		sSvUse[iOctIndex].uiEndOctet = 0;
		sSvUse[iOctIndex].iHandles = 0;							/*T1.02*/
	}
	/* Save the new Connection detail	*/
	sSvUse[iIndex].sSocketId  = sInUse[iIndex].sSocketId ;
	sSvUse[iIndex].uiEndOctet =	sInUse[iIndex].uiEndOctet;
	fprintf(ProcessLogHandle(),"Saving :: Index is %d Saved Octet is %d & Saved Socket is %d \n",
	iIndex, sSvUse[iIndex].uiEndOctet, sSvUse[iIndex].sSocketId);
}
/***************************************************************************
Function:	 Strimid
Description: extract a sub-string given a starting point and length
Passed:		 String Input
				 String Output
				 Start position
				 Length
Return:		 none
****************************************************************************/
/*
void strmid( char *input,	char *out, char start, char length )
{
  char *strptr;
  strptr = input + (long)start;
  strncpy (out, strptr, length);
  out [length] = '\0';
}
*/
	/***************************************************************************
Function:	 Make New
Description: When a client establishs his connect, it comes through the
				 main host port s.  This routine creates a unique socket for
				 output to the new client with the accept() call. It then
				 stores the returned new socket number ns and the last octet
				 of the ip address in our sInUse array.
Passed:		 Socket Number
Return:		 0 = Success, else error number.
****************************************************************************/
int Make_New(SOCKET s)
{
  SOCKET ns;
  int  result;
  struct sockaddr_in peer;
  int  peersize = sizeof(peer);
  char unit[6];
  char ip_addr [20];
  char * pdest;

	fprintf(ProcessLogHandle(),"enter Make_New for Socket %d\n",s);
  ns = accept(s, (struct sockaddr *)&peer, &peersize);
  if (ns == INVALID_SOCKET)	{
		vCloseSocket("Close Socket Accept failed new on connection", ns, 0);
		return -1;
  }

  /*___strip off the last byte of the ip address */
  strcpy_s(ip_addr, sizeof(ip_addr), inet_ntoa(peer.sin_addr));
	_strrev(ip_addr);
	pdest = strchr (ip_addr, '.');
	result = pdest - ip_addr;
	strncpy_s(unit, sizeof(unit), ip_addr, result);
	unit [result] = '\0';
	_strrev(unit);

	MyTime=time(NULL);
  localtime_s(&pNow, &MyTime);
	strftime(buffer,sizeof(buffer),"%H:%M:%S on %d/%m/%y",&pNow);
	fprintf (ProcessLogHandle(),"\n%s ###### Accepted unit on port %d ###### @ %s\n\n",
					 inet_ntoa(peer.sin_addr), ns, buffer);
  /*__Store the last octet of the ip_address in the sInUse array. This will */
  /*	cause it to be picked up for the echo later. */
  setInUseOctet(ns, atoi(unit));
  clearOldConnects(ns, atoi(unit));
  return 0;
}
/***************************************************************************
Function:	 Clear Saved User
Description: Clear the saved user connection detail
Passed:		 Socket Number
Return:		 none
****************************************************************************/
void vClearSvUser( SOCKET s)
{
	unsigned int uiCurrentOctet;
	int iSvIndex ;
	/* get the Current Octet */
	uiCurrentOctet = getInUseOctet(s) ;
	fprintf(ProcessLogHandle(),"Checking (%d)/(%d)\n",s,uiCurrentOctet);
	/* get the Index into the sSvUse  array */
	if((iSvIndex = getInUseOidx(uiCurrentOctet)) == -1) {
		fprintf(ProcessLogHandle(),"No Saved Detail : Index is %d\n",iSvIndex);
	}
	else {
	/* clear the connection detail */
		fprintf(ProcessLogHandle(),"Saved Detail : Index is %d\n",iSvIndex);
		fprintf(ProcessLogHandle(),"Clearing Detail for %d\n",sSvUse[iSvIndex].uiEndOctet );
		sSvUse[iSvIndex].sSocketId = 0;
		sSvUse[iSvIndex].uiEndOctet = 0;
		sSvUse[iSvIndex].iHandles = 0;
		// Reset all File Open flags for User
	}
}
/***************************************************************************
Function:	 Close Outstanding handles
Description: Close Outstanding handles
Passed:		 none
Return:		 none
****************************************************************************/
void vCloseOutHandles(void)
{
	int i;
	for(i = 0; i < MAX_SOCKETS; i++) {
		if(sSvUse[i].iHandles) {
			fprintf(ProcessLogHandle(),"Closing Handles for Socket %d / Unit %u\n",	sSvUse[i].sSocketId,sSvUse[i].uiEndOctet);
			sSvUse[i].iHandles= 0;
		}
	}
}

