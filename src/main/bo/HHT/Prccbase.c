//---------------------------------------------------------------------------
// Application: WixStep.nlm. Hand-Held Terminal (HHT) Server for Novell Server
//							and Socket Services. Developed for Wickes.
// Project: WickesRFDeviceNLMSourceEdit - Developed with MS Visual Studio
//					2003 C++ and compiled with Watcom C/C++ and Watcom linker V 11.0
//					using NMAKE.
// --------------------------------------------------------------------------
// Module:Prccbase.c -Functions used by lApp() to access PrcChg and TmpFil
//										tables.
//----------------------------------------------------------------------------
#undef _THIS_SOURCE_HAS_THE_DATA_
#include "wixstep.h"
/***************************************************************************
Function:	 Read PRCCHG Fields
Description: This function reads the PRCCHG record and sets the user
				 fields.
Passed:		 PRCCHG record
				 User Number
Return:		 None
****************************************************************************/
void near vReadPrcChg(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	//strcpy(cmd,	"select SKUN, PDAT, PRIC, MARK, EVNT, PRIO, PSTA, LABS, LABM, LABL from prcchg");
	cn.SQLGetColumn(1, SQL_C_CHAR, user->ipSkun, sizeof(user->ipSkun));

	cn.SQLGetColumn(2, SQL_C_TYPE_DATE, &user->dipPldt, sizeof(DATE_STRUCT));
	//sprintf_s(user->dipPldt,sizeof(user->dipPldt),"%2d/%2d/%4d", d.day, d.month, d.year);

	user->dipDpdt.day = (char)user->dipPldt.day;
	user->dipDpdt.month = (char)user->dipPldt.month;
	user->dipDpdt.year = (unsigned short)user->dipPldt.year;

	cn.SQLGetColumn(3, SQL_C_CHAR, user->ipPric, sizeof(user->ipPric));
	strcpy_s(user->pcIpri, sizeof(user->pcIpri), user->ipPric);
	cn.SQLGetColumn(4, SQL_C_CHAR, user->ipMark, sizeof(user->ipMark));
	cn.SQLGetColumn(5, SQL_C_CHAR, user->ipEvnt, 7);
	cn.SQLGetColumn(6, SQL_C_CHAR, user->ipPrio, 3);
	cn.SQLGetColumn(7, SQL_C_CHAR, user->ipPsta, 2);
	cn.SQLGetColumn(8, SQL_C_BIT, &user->ipLabs, 1);
	cn.SQLGetColumn(9, SQL_C_BIT, &user->ipLabm, 1);
	cn.SQLGetColumn(10, SQL_C_BIT, &user->ipLabl, 1);
}
/***************************************************************************
Function:    Seek PRCCHG Database
Description: Seek the next (Unapplied) PRCCHG record in the PRCCHG file
Passed:      User Number
Return:      0 = Failed, 1 = Success(Unapplied), 2 = (not Unapplied)
                         3 = EOF
****************************************************************************/
int iSeekPrcChgDatabase(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	char cmd[255] = "SELECT SKUN, PDAT, PRIC, MARK, EVNT, PRIO, PSTA, LABS, LABM, LABL "
									"FROM PRCCHG "
									"WHERE "
										"DateDiff(day, GetDate(), PDAT) < 3 AND "
										"PSTA='U' AND "
										"SKUN=";
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcSkun);

	if (cn.DoSQLCommand(cmd)) {
		if (cn.DoSQLFetch()) {
			vReadPrcChg(sessionNumber);
			vGetEquivPrice(user->pcIpri, sessionNumber);
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
			return 1;
		} else {
			strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "2");
			return(2); // None Found
		}
	}
	strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0"); // Seek Failed
  return(0); 

/* 
	char keyBuf[64];
  long date1, date2;
	DBLongDate date;
  BTI_SINT iDataBaseStatus = B_NO_ERROR;

	// Determine if this is a SKU or KIT
  // Seek the SKU record
	memset(keyBuf,0,sizeof(keyBuf));
  GetStrToDBCharField(keyBuf,sUser[i].pcSkun,sizeof(sUser[i].pcSkun));
	iDataBaseStatus=DBAccessTableData(&dbtPrcChg,B_GET_GE,keyBuf,0);
	if ( iDataBaseStatus == B_NO_ERROR ) {
    vReadPrcChg(i);
		// Check Price Change if this not Future Date
		date1=DBLongDateToDayNumber(&sUser[i].dipPldt);
		date2=DBLongDateToDayNumber(GetCurrentDateInDBLongDateFormat(&date));
		//printf("%ld, %ld, %s, %s\n",date1,date2,sUser[i].ipSkun,sUser[i].pcSkun);

		if ((date1<date2+3) && (memcmp(sUser[i].ipSkun,sUser[i].pcSkun,6)==0)) {
			if ((!sUser[i].ipLabs) && (!sUser[i].ipLabm) && (!sUser[i].ipLabl)) {
			  //fprintf(hLogFile,"(%d) No Label print indicator is set\n",i);
			  strcpy(sUser[i].pcLastInput, "2");
		  } else {
				if (sUser[i].ipPsta[0]=='U') {	// Unapplied Price Change found
					vGetEquivPrice(sUser[i].pcIpri,i);
					strcpy(sUser[i].pcLastInput, "1");
				} else {												// Applied Price Change found
					strcpy(sUser[i].pcLastInput, "2");
				}
				return(1);
			}
		}
	}
	strcpy(sUser[i].pcLastInput, "0");
  return(0); // Seek Failed
	*/
}
/***************************************************************************
Function:    Get Next PRCCHG Database
Description: Get the next (Unapplied) PRCCHG record in the PRCCHG file
Passed:      User Number
Return:      0 = Failed, 1 = Success(Unapplied), 2 = (not Unapplied)
                         3 = EOF
****************************************************************************/
int iGetNextPrcChgDatabase(int sessionNumber) // don't bother. Seek has already done all the work, just return "not found"
{
	USER_TYPE *user = &sUser[sessionNumber]; 
	strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0"); // None Found
	return(0); 

/* 
BTI_SINT iDataBaseStatus = B_NO_ERROR;
	iDataBaseStatus=DBAccessTableData(&dbtPrcChg,B_GET_NEXT,NULL,0);
  if ( iDataBaseStatus == B_END_OF_FILE ) {
    strcpy(sUser[i].pcLastInput, "3");
    return(0);
  }
  if ( iDataBaseStatus == B_NO_ERROR ) {
    vReadPrcChg(i);
    // Check Price Change if this not Future Date
    if((DBLongDateCompare(&sUser[i].dipPldt,GetCurrentDateInDBLongDateFormat(NULL))==0) &&
			 (memcmp(sUser[i].ipSkun,sUser[i].pcSkun,6) == 0)) {
		  if ((!sUser[i].ipLabs) && (!sUser[i].ipLabm) && (!sUser[i].ipLabl)) {
			  strcpy(sUser[i].pcLastInput, "2");
		  } else {
			  if (memcmp(sUser[i].ipPsta,"U",1) == 0) {	// Unapplied
				  vGetEquivPrice(sUser[i].pcIpri,i);
			    strcpy(sUser[i].pcLastInput, "1");
			  } else {
			    strcpy(sUser[i].pcLastInput, "2");
			  }
		  }
	  } else {
		  strcpy(sUser[i].pcLastInput, "0");
	  }
	  return(1); // Seek Passed
	}
  strcpy(sUser[i].pcLastInput, "0");
  return(0); // Seek Failed
*/
}
/***************************************************************************
Function:    Update PRCCHG Database
Description: Update Current Unapplied Price Change record (to Applied)
Passed:      User Number
Return:      0  = Failed, 1  = Success,
LastInput:  "0" = Failed, else "1" = Success,
****************************************************************************/
int iUpdatePrcChgDatabase(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	char s[21];
	char cmd[255] = "UPDATE PRCCHG SET PSTA='H'";

	strcat_s(cmd, sizeof(cmd), ", EEID=");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->pcEeid);

	strcat_s(cmd, sizeof(cmd), ", AUAP=");
	cn.SQLGetCurrentDateString(s, sizeof(s));
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), s);

	strcat_s(cmd, sizeof(cmd), ", MARK=");
	sprintf_s(user->ipMark, sizeof(user->ipMark), "%0.2f", 
				(atof(user->ipPric) - atof(user->imPric)) * 
				(atol(user->imWtfq) + (atol(user->imMdnq)) + (atol(user->imOnha))));
	strcat_s(cmd, sizeof(cmd), user->ipMark);

	strcat_s(cmd, sizeof(cmd), " WHERE SKUN=");
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), user->ipSkun);
	strcat_s(cmd, sizeof(cmd), " AND PDAT=");
	sprintf_s(s, sizeof(s), "%d-%d-%d", user->dipDpdt.year, user->dipDpdt.month, user->dipDpdt.day);
	cn.SQLQueryAddVarChar(cmd, sizeof(cmd), s);

	if (cn.DoSQLCommand(cmd)) {
		strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
		return 1;
	}
	strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
	return 0;

/*
char keyBuf[64];
  double dTemp0;
  double dTemp1;
  BTI_SINT iDataBaseStatus = B_NO_ERROR;
  // GET THE RECORD WITH GET_EQUAL AND UPDATE IT
	// IP:SKUN
	GetStrToDBCharField(keyBuf,sUser[i].ipSkun,sizeof(prcchg.skun));
	GetDBLongDateToDBField(keyBuf+sizeof(prcchg.skun),&sUser[i].dipDpdt);
	iDataBaseStatus=DBAccessTableData(&dbtPrcChg,B_GET_EQUAL | S_NOWAIT_LOCK,keyBuf,0);
	// PSTA
	prcchg.psta[0]='H';
	// EEID
	GetStrToDBCharField(prcchg.eeid,sUser[i].pcEeid,sizeof(prcchg.eeid));
	// AUAP
	GetDBLongDateToDBField(prcchg.auap,GetCurrentDateInDBLongDateFormat(NULL));
	// MARK
	// PRCCHG.PRIC-STKMAS.PRIC = price change difference
	dTemp0=atof(sUser[i].ipPric)-atof(sUser[i].imPric);
	// STKMAS.WTFQ+STKMAS.MDNQ+STKMAS.ONHA = writeoff+Markdown+onhand quantities
  dTemp1=atol(sUser[i].imWtfq)+atol(sUser[i].imMdnq)+atol(sUser[i].imOnha);
	// price difference*quantities = markdown value
  sprintf_s(sUser[i].ipMark,"%8.2f",dTemp0*dTemp1);
	GetStrToDBDecField(prcchg.mark,sUser[i].ipMark,sizeof(prcchg.mark),2);
	if ( iDataBaseStatus == B_NO_ERROR ) {
		iDataBaseStatus=DBAccessTableData(&dbtPrcChg,B_UPDATE,keyBuf,0);
		iDataBaseStatus=DBAccessTableData(&dbtPrcChg,B_UNLOCK,keyBuf,0);
	}
	if ( iDataBaseStatus == B_NO_ERROR ) {
	  strcpy(sUser[i].pcLastInput, "1");
		return(1); // Seek Passed
	}
  strcpy(sUser[i].pcLastInput, "0");
  return(0); // Seek Failed
*/
}
/***************************************************************************
Function:    Seek/Update TMPFIL Table Label Totals Record
Description: Seek and Update the TmpFil Total record
Passed:      User Number
Return:      0 = Failed, 1 = Success,
LastInput:  "0" = Failed, else "1" = Success
****************************************************************************/
// Updates the labels total record. Key index 2. WSID="00" + FKEY="PCPLRQ00000000"
int UpdateTmpfilLabelTotals(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	ProcessLogWriteLn("TMPFILE Totals");
	char cmd[1024] = "DECLARE @small int, @medium int, @large int, @tkey int;";

	strcat_s(cmd, sizeof(cmd), " SET @small = ");
	strcat_s(cmd, sizeof(cmd), user->pcSlab);
	strcat_s(cmd, sizeof(cmd), "; ");

	strcat_s(cmd, sizeof(cmd), " SET @medium = ");
	strcat_s(cmd, sizeof(cmd), user->pcMlab);
	strcat_s(cmd, sizeof(cmd), "; ");

	strcat_s(cmd, sizeof(cmd), " SET @large = ");
	strcat_s(cmd, sizeof(cmd), user->pcLlab);
	strcat_s(cmd, sizeof(cmd), "; ");

	strcat_s(cmd, sizeof(cmd), " SELECT @tkey = TKEY FROM TmpFil WHERE WSID = '00' AND Left(FKEY, 14) = 'PCPLRQ00000000';");

	strcat_s(cmd, sizeof(cmd), " IF @tkey IS NULL BEGIN");
	strcat_s(cmd, sizeof(cmd), " INSERT INTO TMPFIL (WSID, FKEY, DATA) VALUES (");
	strcat_s(cmd, sizeof(cmd), " '00', 'PCPLRQ00000000',");
	strcat_s(cmd, sizeof(cmd), " Space(13) +");
	strcat_s(cmd, sizeof(cmd), " Right(Space(6) + Convert(VARCHAR(6), @small), 6) +");
	strcat_s(cmd, sizeof(cmd), " Right(Space(6) + Convert(VARCHAR(6), @medium), 6) +");
	strcat_s(cmd, sizeof(cmd), " Right(Space(6) + Convert(VARCHAR(6), @large), 6)");
	strcat_s(cmd, sizeof(cmd), " )");
	strcat_s(cmd, sizeof(cmd), " END");

	strcat_s(cmd, sizeof(cmd), " ELSE BEGIN");
	strcat_s(cmd, sizeof(cmd), " UPDATE TmpFil SET DATA=");
	strcat_s(cmd, sizeof(cmd), " Space(13) +");
	strcat_s(cmd, sizeof(cmd), " Right(Space(6) + Convert(VARCHAR(6), @small + Convert(INT, Substring(DATA, 14, 6))), 6) +");
	strcat_s(cmd, sizeof(cmd), " Right(Space(6) + Convert(VARCHAR(6), @medium + Convert(INT, Substring(DATA, 20, 6))), 6) +");
	strcat_s(cmd, sizeof(cmd), " Right(Space(6) + Convert(VARCHAR(6), @large + Convert(INT, Substring(DATA, 26, 6))), 6)");
	strcat_s(cmd, sizeof(cmd), " WHERE TKEY=@tkey");
	strcat_s(cmd, sizeof(cmd), " END");

	if (cn.DoSQLCommand(cmd)) {
		ProcessLogWriteLn("Updated TMPFILE Totals");
		strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
		return 1;
	}
	ProcessLogWriteLn("Error with TMPFILE Totals");
	strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
	return 0;

	/*
char TOTALS_KEY_VALUE[]="00PCPLRQ00000000";
	char keyBuf[sizeof(tmpfil.wsid)+sizeof(tmpfil.Fkey)];
	char s[21];	// For conversion of totals fields
	BTI_SINT iDataBaseStatus = B_NO_ERROR;

	// Make the totals record key
	DBClearAccessKeyBuffer();
	memcpy(keyBuf,TOTALS_KEY_VALUE,strlen(TOTALS_KEY_VALUE));
	memset(keyBuf+strlen(TOTALS_KEY_VALUE)-6,0,6);	// set SKUN to ""
	memset(&tmpfil,0,sizeof(tmpfil));
	ProcessLogWriteLn("TMPFILE Totals Loop");
	iDataBaseStatus=DBAccessTableData(&dbtTmpFil,B_GET_GE,keyBuf,1);
	while (iDataBaseStatus==B_NO_ERROR) {
		ProcessLogWriteLn(tmpfil.wsid);
		if ((memcmp(&tmpfil.wsid,TOTALS_KEY_VALUE,strlen(TOTALS_KEY_VALUE))==0)) {
			ProcessLogWriteLn("TMPFIL Found totals record");
			// Update totals fields
			GetDBCharFieldToStr(s,tmpfil.Data.Total.Small,6);				// Get the small count
			sprintf_s(s,"%6ld",atol(s) + atol(sUser[i].pcSlab));			// Add new count to it
			GetStrToDBCharField(tmpfil.Data.Total.Small,s,6);				// and then update it
			GetDBCharFieldToStr(s,tmpfil.Data.Total.Medium,6);			// Get the Medium count
			sprintf_s(s,"%6ld",atol(s) + atol(sUser[i].pcMlab));			// Add new count to it
			GetStrToDBCharField(tmpfil.Data.Total.Medium,s,6);			// and then update it
			GetDBCharFieldToStr(s,tmpfil.Data.Total.Large,6);				// Get the Large count
			sprintf_s(s,"%6ld",atol(s) + atol(sUser[i].pcLlab));			// Add new count to it
			GetStrToDBCharField(tmpfil.Data.Total.Large,s,6);				// and then update it
			// add up label totals
			iDataBaseStatus=DBAccessTableData(&dbtTmpFil,B_UPDATE,NULL,1);
			if (iDataBaseStatus==B_NO_ERROR) {
				ProcessLogWriteLn("Updated TMPFILE Totals");
				strcpy(sUser[i].pcLastInput, "1");
				return(1); // Update Passed
			}
			strcpy(sUser[i].pcLastInput, "0");										// DB error
			return(0);
		}
		// Done searching, no totals record found
		if ((memcmp(&tmpfil.wsid,TOTALS_KEY_VALUE,strlen(TOTALS_KEY_VALUE))>0)) {
			break;
		}
		iDataBaseStatus=DBAccessTableData(&dbtTmpFil,B_GET_NEXT,keyBuf,1);
	}
	// Try to add the totals record
	ProcessLogWriteLn("Adding new TMPFILE Totals record");
	memset(&tmpfil,' ',sizeof(tmpfil));					// Initialize all fields to spaces
	memset(&tmpfil.date,0,sizeof(DBLongDate));	// No date
	// init tkey
	memset(tmpfil.tkey,0,sizeof(tmpfil.tkey));
	// Set the WSID as "00" for HHT's, and the FKey
	memcpy(tmpfil.wsid,TOTALS_KEY_VALUE,strlen(TOTALS_KEY_VALUE));
	// Set up the Number of Labels Requested (or initialize totals count)
	sprintf_s(s,"%6ld",atol(sUser[i].pcSlab));
	GetStrToDBCharField(tmpfil.Data.Total.Small,s,6);
	sprintf_s(s,"%6ld",atol(sUser[i].pcMlab));
	GetStrToDBCharField(tmpfil.Data.Total.Medium,s,6);
	sprintf_s(s,"%6ld",atol(sUser[i].pcLlab));
	GetStrToDBCharField(tmpfil.Data.Total.Large,s,6);
	if (DBAccessTableData(&dbtTmpFil,B_INSERT,NULL,1)==B_NO_ERROR) {
		ProcessLogWriteLn("Inserted TMPFILE Totals");
	  strcpy(sUser[i].pcLastInput, "1");
		return(1); // Seek Passed
	}
	ProcessLogWriteLn("Error with TMPFILE Totals");
	strcpy(sUser[i].pcLastInput, "0");	// Default to error
	return(0); // Seek Failed
*/
}
/***************************************************************************
Function:    Insert TMPFIL Database
Description: Insert a TMPFIL label record
Passed:      User Number
Return:      0  = Failed, 1  = Success, 2 = Duplicate
LastInput:  "0" = Failed, else "1" = Success,
**************************************************`**************************/
int InsertTmpfileLabels(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	char s[21];

	char cmd[512] = "INSERT INTO TMPFIL (WSID, FKEY, DATA) VALUES (";
	strcat_s(cmd, sizeof(cmd), "'00',"); //WSID

	strcat_s(cmd, sizeof(cmd), " 'PCPLRQ");    //FKEY
	strcat_s(cmd, sizeof(cmd), user->syCode);
	strcat_s(cmd, sizeof(cmd), user->pcSkun);
	strcat_s(cmd, sizeof(cmd), "',");

	strcat_s(cmd, sizeof(cmd), " '");          //DATA  
	sprintf_s(s, sizeof(s), "%10s", user->pcDoingPriceChange ? user->pcIpri : user->pcPric);
	strcat_s(cmd, sizeof(cmd), s);
	strcat_s(cmd, sizeof(cmd), user->pcEeid);

	sprintf_s(s, sizeof(s), "%6d", atoi(user->pcSlab));
	strcat_s(cmd, sizeof(cmd), s);
	sprintf_s(s, sizeof(s), "%6d", atoi(user->pcMlab));
	strcat_s(cmd, sizeof(cmd), s);
	sprintf_s(s, sizeof(s), "%6d", atoi(user->pcLlab));
	strcat_s(cmd, sizeof(cmd), s);
	strcat_s(cmd, sizeof(cmd), "')");

	if (cn.DoSQLCommand(cmd)) {
		strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "1");
		return 1;
	}
	strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0");
	return 0;

	/*
double price;	// for conversion to tmpfil fields
	char s[21];		//   "            "            "
	BTI_SINT iDataBaseStatus = B_NO_ERROR;

  // INSERT a TMPFIL header record
  memset(&tmpfil,' ',sizeof(tmpfil));					// Initialize all fields to spaces
	memset(&tmpfil.date,0,sizeof(DBLongDate));	// No date
	// init tkey
	memset(tmpfil.tkey,0,sizeof(tmpfil.tkey));
  // Set the WSID as "00" for HHT's
	GetStrToDBCharField(tmpfil.wsid,"00",sizeof(tmpfil.wsid));
  // Set up the FKey
  memcpy(tmpfil.Fkey.PriceChg.Header,"PCPLRQ",sizeof(tmpfil.Fkey.PriceChg.Header));
  GetStrToDBCharField(tmpfil.Fkey.PriceChg.Reas,sUser[i].syCode,sizeof(tmpfil.Fkey.PriceChg.Reas));
  GetStrToDBCharField(tmpfil.Fkey.PriceChg.Skun,sUser[i].pcSkun,sizeof(tmpfil.Fkey.PriceChg.Skun));
 	// Set up the Employee Id
	GetStrToDBCharField(tmpfil.Data.Label.Eeid,sUser[i].pcEeid,sizeof(tmpfil.Data.Label.Eeid));
  GetStrToDBCharField(prcchg.eeid,sUser[i].pcEeid,sizeof(prcchg.eeid));
	// Set up the Price
	if (!sUser[i].pcDoingPriceChange) {
		price=atof(sUser[i].pcPric);
	}	else 	{
		price=atof(sUser[i].pcIpri);
	}
	sprintf_s(s,"%10.2f",price);
	GetStrToDBCharField(tmpfil.Data.Label.Pric,s,sizeof(tmpfil.Data.Label.Pric));
	// Set up the Number of Labels Requested (or initialize totals count)
	sprintf_s(s,"%6ld",atol(sUser[i].pcSlab));
	GetStrToDBCharField(tmpfil.Data.Total.Small,s,6);
	sprintf_s(s,"%6ld",atol(sUser[i].pcMlab));
	GetStrToDBCharField(tmpfil.Data.Total.Medium,s,6);
	sprintf_s(s,"%6ld",atol(sUser[i].pcLlab));
	GetStrToDBCharField(tmpfil.Data.Total.Large,s,6);

	iDataBaseStatus=DBAccessTableData(&dbtTmpFil,B_INSERT,NULL,1);
  if ( iDataBaseStatus == B_NO_ERROR ) {
   	strcpy(sUser[i].pcLastInput, "1");
		return(1); // Seek Passed
  }
  if ( iDataBaseStatus == B_DUPLICATE_KEY_VALUE ) {
   	strcpy(sUser[i].pcLastInput, "2");
		return(1); // Seek Passed
  }
 	strcpy(sUser[i].pcLastInput, "0");
 	return(0) ;
*/
}

/***************************************************************************
Function:    Seek TMPFIL Gap Walk record
Description: Seek the Gap Walk SKU record in TMPFIL
Passed:      User Number
LastInput:   "0"  = Key Value Not found,  "1"  = Key Value found,
Return:       0   = Failed,  1  = Success,
****************************************************************************/
int SeekGapWalkTmpfilRecord(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	char s[201];

	char cmd[255] = "SELECT TOP 1 TKEY, DATA FROM TmpFil WHERE WSID = '00' AND Left(FKEY, 12) = 'IMRGWP";
	strcat_s(cmd, sizeof(cmd), user->pcSkun);
	strcat_s(cmd, sizeof(cmd), "'");

	if (cn.DoSQLCommand(cmd) && cn.DoSQLFetch()) {
		cn.SQLGetColumn(2, SQL_C_CHAR, s, sizeof(s));
		s[6] = '\0';
		strcpy_s(user->pcReqQty, sizeof(user->pcReqQty), s);
		ProcessLogWriteLn("SeekGapWalkTmpfilRecord - Found it and match");
		return 1;
	}
	ProcessLogWriteLn("SeekGapWalkTmpfilRecord - Not found");
	return 0;

/*
union tmpfilFkey TargetFkey;
	char keyBuf[64];

  // Setup The Fkey Value
	memset(&TargetFkey,' ',sizeof(TargetFkey));
	GetStrToDBCharField(TargetFkey.GapWalk.Header,"IMRGWP",sizeof(tmpfil.Fkey.GapWalk.Header));
	GetStrToDBCharField(TargetFkey.GapWalk.Skun,sUser[i].pcSkun,sizeof(tmpfil.Fkey.GapWalk.Skun));
	GetStrToDBCharField(keyBuf,"00",sizeof(tmpfil.wsid));											// WSID = 00 for HHT
	memcpy(keyBuf+sizeof(tmpfil.wsid),&TargetFkey,sizeof(tmpfil.Fkey));			// Plus the Fkey field is the key
	if (DBAccessTableData(&dbtTmpFil,B_GET_EQUAL,keyBuf,1)==B_NO_ERROR) {
		// Get qty
		GetDBCharFieldToStr(sUser[i].pcReqQty,tmpfil.Data.GapWalk.RequestedQty,sizeof(tmpfil.Data.GapWalk.RequestedQty));
		ProcessLogWriteLn("SeekGapWalkTmpfilRecord - Found it and match");
		return(1);
	}
	ProcessLogWriteLn("SeekGapWalkTmpfilRecord - Not found");
	return(0);
*/
}
/***************************************************************************
Function:    Seek/Update TMPFIL Gap Walk Record
Description: Seek and Update the TmpFil (Gap Walk Requested Quantity) record
Passed:      User Number
Return:      0 = Failed, 1 = Success,
LastInput:  "0" = Failed, else "1" = Success "4"-> No Key value Found
****************************************************************************/
int UpdateGapWalkTmpfilRecord(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	ProcessLogWriteLn("GapWalk Totals");
	char s[21];
	char cmd[1024] = "INSERT INTO GapWalk (ID, SkuNumber, Sequence, DateCreated, Location, QuantityRequired, IsPrinted) VALUES (";

	strcat_s(cmd, sizeof(cmd), "Coalesce((SELECT MAX(ID) FROM GapWalk), 0) + 1,'"); // ID

	strcat_s(cmd, sizeof(cmd), user->pcSkun); // SkuNumber

	strcat_s(cmd, sizeof(cmd), "',Coalesce((SELECT MAX(Sequence) FROM GapWalk WHERE SkuNumber = '"); // Sequence
	strcat_s(cmd, sizeof(cmd), user->pcSkun); 
	strcat_s(cmd, sizeof(cmd), "'), 0) + 1, '");

	strcat_s(cmd, sizeof(cmd), cn.SQLGetCurrentDateString(s, sizeof(s))); // DateCreated
	strcat_s(cmd, sizeof(cmd), "','"); 

	strcat_s(cmd, sizeof(cmd), "',"); // Location

	strcat_s(cmd, sizeof(cmd), user->pcReqQty); // QuantityRequired
	strcat_s(cmd, sizeof(cmd), ","); 

	strcat_s(cmd, sizeof(cmd), "0)"); // IsPrinted

	//char cmd[1024] = "DECLARE @skun CHAR(6), @quantity INT, @tkey INT;";

	//strcat_s(cmd, sizeof(cmd), " SET @quantity = ");
	//strcat_s(cmd, sizeof(cmd), user->pcReqQty);
	//strcat_s(cmd, sizeof(cmd), ";");

	//strcat_s(cmd, sizeof(cmd), " SET @skun = '");
	//strcat_s(cmd, sizeof(cmd), user->pcSkun);
	//strcat_s(cmd, sizeof(cmd), "';");

	//strcat_s(cmd, sizeof(cmd), " SELECT @tkey = TKEY FROM TmpFil WHERE WSID = '00' AND Left(FKEY, 12) = 'IMRGWP' + @skun;");

	//strcat_s(cmd, sizeof(cmd), " IF @tkey IS NULL BEGIN");
	//strcat_s(cmd, sizeof(cmd), " INSERT INTO TMPFIL (WSID, FKEY, DATA) VALUES (");
	//strcat_s(cmd, sizeof(cmd), " '00', 'IMRGWP' + @skun,");
	//strcat_s(cmd, sizeof(cmd), " Right(Space(6) + Convert(VARCHAR(6), @quantity), 6)"); //DATA  
	//strcat_s(cmd, sizeof(cmd), " )");
	//strcat_s(cmd, sizeof(cmd), " END");

	//strcat_s(cmd, sizeof(cmd), " ELSE BEGIN");
	//strcat_s(cmd, sizeof(cmd), " UPDATE TmpFil SET DATA=");
	//if (user->bNewGapSect) {
	//	strcat_s(cmd, sizeof(cmd), " Right(Space(6) + Convert(VARCHAR(6), @quantity + Convert(INT, Left(DATA, 6))), 6)");
	//} else {
	//	strcat_s(cmd, sizeof(cmd), " Right(Space(6) + Convert(VARCHAR(6), @quantity), 6)"); //DATA  
	//}

	//strcat_s(cmd, sizeof(cmd), " WHERE TKEY=@tkey");
	//strcat_s(cmd, sizeof(cmd), " END");

	if (cn.DoSQLCommand(cmd)) {
		ProcessLogWriteLn("UpdateGapWalkTmpfilRecord - Inserted/updated");
		return 1;
	}
	ProcessLogWriteLn("UpdateGapWalkTmpfilRecord - Insert/Update failed");
	return 0;
/*
union tmpfilFkey TargetFkey;
	char keyBuf[64];
	BTI_SINT iDataBaseStatus = B_NO_ERROR;
  //Setup The Fkey Value
	memset(&TargetFkey,' ',sizeof(TargetFkey));
  GetStrToDBCharField(TargetFkey.GapWalk.Header,"IMRGWP",sizeof(tmpfil.Fkey.GapWalk.Header));
  GetStrToDBCharField(TargetFkey.GapWalk.Skun,sUser[i].pcSkun,sizeof(tmpfil.Fkey.GapWalk.Skun));
	GetStrToDBCharField(keyBuf,"00",sizeof(tmpfil.wsid));									// WSID = 00 for HHT
	memcpy(keyBuf+sizeof(tmpfil.wsid),&TargetFkey,sizeof(tmpfil.Fkey));		// Plus the Fkey field is the key
	if ((iDataBaseStatus=DBAccessTableData(&dbtTmpFil,B_GET_EQUAL,keyBuf,1))==B_NO_ERROR) {
		iDataBaseStatus=DBAccessTableData(&dbtTmpFil,B_GET_EQUAL | S_NOWAIT_LOCK,NULL,1);
	  GetStrToDBCharField(tmpfil.Data.GapWalk.RequestedQty,sUser[i].pcReqQty,sizeof(tmpfil.Data.GapWalk.RequestedQty));
		iDataBaseStatus=DBAccessTableData(&dbtTmpFil,B_UPDATE,NULL,1);
		iDataBaseStatus=DBAccessTableData(&dbtTmpFil,B_UNLOCK,NULL,1);
		if (iDataBaseStatus==B_NO_ERROR) {
			strcpy(sUser[i].pcLastInput, "1");
			ProcessLogWriteLn("Update gapwalk - Got it and updated(1)");
			return(1);
		}
	}
	strcpy(sUser[i].pcLastInput, "4");
	ProcessLogWriteLn("Update gapwalk - Did not GET_GE(4)");
	return(0);
*/
return(0);
}
/***************************************************************************
Function:    Insert TMPFIL Gap Walk Record
Description: Insert a Gap Walk record in the TMPFIL btrieve database file
Passed:      User Number
Return:      0  = Failed, 1  = Success, 2 = Duplicate
LastInput:  "0" = Failed, else "1" = Success,
****************************************************************************/
int iInsertGapWalkTmpfilRecord(int sessionNumber)
{
	USER_TYPE *user = &sUser[sessionNumber];

	strcpy_s(user->pcLastInput, sizeof(user->pcLastInput), "0"); // Something has gone horribly wrong
	ProcessLogWriteLn("Insert gapwalk - Failed");
	return 0;

/*
//char keyBuf[64];
	BTI_SINT iDataBaseStatus = B_NO_ERROR;

	// Init all fields to space
  memset(&tmpfil,' ',sizeof(tmpfil));
	memset(tmpfil.tkey,0,sizeof(tmpfil.tkey));
	memset(&tmpfil.date,0,sizeof(DBLongDate));
  // Set up the FKey
	GetStrToDBCharField(tmpfil.wsid,"00",sizeof(tmpfil.wsid));
  GetStrToDBCharField(tmpfil.Fkey.GapWalk.Header,"IMRGWP",sizeof(tmpfil.Fkey.GapWalk.Header));
	GetStrToDBCharField(tmpfil.Fkey.GapWalk.Skun,sUser[i].pcSkun,sizeof(tmpfil.Fkey.GapWalk.Skun));
 	// Set up the Price
  GetStrToDBCharField(tmpfil.Data.GapWalk.RequestedQty,sUser[i].pcReqQty,sizeof(tmpfil.Data.GapWalk.RequestedQty));

	iDataBaseStatus=DBAccessTableData(&dbtTmpFil,B_INSERT,NULL,1);
  if (iDataBaseStatus==B_NO_ERROR) {
    strcpy(sUser[i].pcLastInput, "1");
		ProcessLogWriteLn("Insert gapwalk - OK");
		return(1);
  }
	if (iDataBaseStatus==B_DUPLICATE_KEY_VALUE) {
 		strcpy(sUser[i].pcLastInput, "2");
		ProcessLogWriteLn("Insert gapwalk - Dup found");
		return(1);
  }
 	strcpy(sUser[i].pcLastInput,"0");
	ProcessLogWriteLn("Insert gapwalk - Failed");
 	return(0);
*/
return(0);
}

