using System;
using Ninject;

namespace WSS.BO.Common.Hosting
{
    public class SimpleResolver
    {
        private readonly IKernel kernel;

        public SimpleResolver()
        {
            var config = new BaseDependencyInjectionConfig();
            kernel = config.GetKernel();
        }

        public T Resolve<T>()
        {
            return kernel.Get<T>();
        }
    }
}
