﻿using System;
using System.Linq;
using Ninject.Extensions.Interception;
using slf4net;

namespace WSS.BO.Common.Hosting
{
    public class LoggingInterceptor : IInterceptor
    {
        private static readonly ILogger Log = LoggerFactory.GetLogger(typeof(LoggingInterceptor));
        private readonly LogLevel level;

        public LoggingInterceptor(LogLevel level)
        {
            this.level = level;
        }
        
        public void Intercept(IInvocation invocation)
        {
            var methodName = string.Format("{0}.{1}", invocation.Request.Target.GetType(), invocation.Request.Method.Name);
            level.WriteLog("{0} is called", methodName);
            LogParameters(methodName, invocation);

            invocation.Proceed();
            
            LogReturnValue(methodName, invocation);
            level.WriteLog("{0} is completed", methodName);
        }

        private void LogParameters(string methodName, IInvocation invocation)
        {
            if (Log.IsDebugEnabled)
            {
                var parameters = invocation.Request.Method.GetParameters().ToList();
                var parameterValues = invocation.Request.Arguments;

                for (var i = 0; i < parameters.Count; i++)
                {
                    if (IsLogableType(parameters[i].ParameterType))
                    {
                        Log.Debug("{0} parameter {1}='{2}'", methodName, parameters[i].Name, parameterValues[i]);
                    }
                }
            }
        }

        private void LogReturnValue(string methodName, IInvocation invocation)
        {
            if (Log.IsDebugEnabled && invocation.Request.Method.ReturnType != typeof(void) && IsLogableType(invocation.Request.Method.ReturnType))
            {
                Log.Debug("{0} returned: '{1}'", methodName, invocation.ReturnValue);
            }
        }

        private bool IsLogableType(Type T)
        {
            return !T.Name.StartsWith("IQueryable");
        }

        public class LogLevel
        {
            public static readonly LogLevel Debug = new LogLevel(Log.Debug);
            public static readonly LogLevel Info = new LogLevel(Log.Info);

            private readonly Action<string, object[]> action;

            private LogLevel(Action<string, object[]> action)
            {
                this.action = action;
            }

            public void WriteLog(string format, params object[] args)
            {
                action(format, args);
            }
        }
    }
}