using System;
using Cts.Oasys.Core;
using Ninject;
using WSS.BO.DataLayer;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.DataLayer.Repositories;
using Ninject.Extensions.Factory;
using Ninject.Extensions.Interception;
using Ninject.Modules;
using WSS.BO.DataLayer.Database;

namespace WSS.BO.Common.Hosting
{
    public class BaseDependencyInjectionConfig : IDependencyInjectionConfig
    {
        public virtual IKernel GetKernel()
        {
            // IMPORTANT: Have to disable automatic extensions loading and do it explicitly.
            // Reason - automatic extensions loading tries to find modules in FileSystem. For restricted accounts (like for CtsOrderManager), it throws error.
            var modules = new INinjectModule[]
            {
                new FuncModule(), 
                new DynamicProxyModule()
            };

            IKernel kernel = new StandardKernel(
                new NinjectSettings
                {
                    LoadExtensions = false
                }, modules);

            ConfigureDataLayer(kernel);
            return kernel;
        }

        protected virtual void InitOasysDbConfiguration(IKernel kernel)
        {
            kernel.Bind<IOasysDbConfiguration>().To<ConnectionXMLBasedOasysDbConfiguration>();
        }

        private void ConfigureDataLayer(IKernel kernel)
        {
            kernel.Bind<IAlertRepository>().To<AlertRepository>();
            kernel.Bind<ICashierBalanceRepository>().To<CashierBalanceRepository>();
            kernel.Bind<ICustomerOrderRepository>().To<CustomerOrderRepository>();
            kernel.Bind<IDailyTillRepository>().To<DailyTillRepository>();
            kernel.Bind<IDictionariesRepository>().To<DictionariesRepository>();
            kernel.Bind<IExternalRequestRepository>().To<ExternalRequestRepository>();
            kernel.Bind<ISafeRepository>().To<SafeRepository>();
            kernel.Bind<IStockRepository>().To<StockRepository>();
            kernel.Bind<ICouponRepository>().To<CouponRepository>();
            kernel.Bind<IDeliveryChargeRepository>().To<DeliveryChargeRepository>();
            kernel.Bind<IControlFileRepository>().To<ControlFileRepository>();
            kernel.Bind<IUserRepository>().To<UserRepository>();
            kernel.Bind<IRelatedItemRepository>().To<RelatedItemRepository>();

            InitOasysDbConfiguration(kernel);

            kernel.Bind<IDataLayerFactory>().To<DataLayerFactory>();

            kernel.Bind<IRepositoriesFactory>().ToFactory();
        }
    }
}