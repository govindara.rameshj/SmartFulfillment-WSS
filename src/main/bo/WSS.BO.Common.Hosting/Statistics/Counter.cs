﻿namespace WSS.BO.Common.Hosting.Statistics
{
    public class Counter
    {
        private readonly object _lockObject = new object();
        public string Key { get; private set; }
        public double Avg { get; private set; }
        public int Count { get; private set; }
        public double Min { get; private set; }
        public double Max { get; private set; }

        public Counter(string key, double avg, int count, double min, double max)
        {
            Key = key;
            Avg = avg;
            Count = count;
            Min = min;
            Max = max;
        }

        public void Add(double value)
        {
            lock (_lockObject)
            {
                Count++;

                if (value < Min)
                {
                    Min = value;
                }

                if (value > Max)
                {
                    Max = value;
                }

                Avg = Avg + ((value - Avg)/Count);
            }
        }

        public void ApplyUpdates(double updateAvg, int updateCount, double updateMin, double updateMax)
        {
            lock (_lockObject)
            {
                if (updateCount > 0)
                {
                    Avg = (Avg * Count + updateAvg * updateCount) / (Count + updateCount);

                    if (updateMin < Min)
                    {
                        Min = updateMin;
                    }

                    if (updateMax > Max)
                    {
                        Max = updateMax;
                    }

                    Count = Count + updateCount;
                }
            }
        }
    }
}