﻿using System;
using System.Diagnostics;
using Ninject.Extensions.Interception;
using Ninject.Extensions.Interception.Request;

namespace WSS.BO.Common.Hosting.Statistics
{
    public class CounterInterceptor : IInterceptor
    {
        private readonly CountersCollection _countersCollection;
        private readonly string _context;

        public CounterInterceptor(CountersCollection counters, string context)
        {
            _countersCollection = counters;
            _context = context;
        }

        public void Intercept(IInvocation invocation)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            try
            {
                invocation.Proceed();
            }
            finally
            {
                sw.Stop();

                var key = GetCounterKey(invocation.Request);
                var counter = _countersCollection.GetCounter(key);
                counter.Add(sw.Elapsed.TotalSeconds);
            }
        }

        protected virtual string GetContext(IProxyRequest invocationRequest)
        {
            return _context;
        }

        private string GetCounterKey(IProxyRequest invocationRequest)
        {
            var method = invocationRequest.Method;
            var className = method.DeclaringType != null ? method.DeclaringType.Name : "N/A";

            return String.Format("{0}/{1}.{2}", GetContext(invocationRequest), className, method.Name);
        }
    }
}