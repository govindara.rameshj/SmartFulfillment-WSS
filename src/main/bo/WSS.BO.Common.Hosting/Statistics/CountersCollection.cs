﻿using System.Collections.Generic;
using System.Linq;

namespace WSS.BO.Common.Hosting.Statistics
{
    public class CountersCollection
    {
        public List<Counter> Counters = new List<Counter>();
        private readonly object _lockObject = new object();

        public Counter GetCounter(string key)
        {
            lock (_lockObject)
            {
                if (!Counters.Exists(x => x.Key == key))
                {
                    Counters.Add(new Counter(key, 0, 0, double.MaxValue, double.MinValue));
                }
                return Counters.First(x => x.Key == key);
            }
        }

        public void ApplyUpdates(Counter counterToApply)
        {
            lock (_lockObject)
            {
                if (counterToApply.Count > 0)
                {
                    var existingCounter = Counters.FirstOrDefault(x => x.Key == counterToApply.Key);
                    if (existingCounter != null)
                    {
                        existingCounter.ApplyUpdates(counterToApply.Avg, counterToApply.Count, counterToApply.Min,
                            counterToApply.Max);
                    }
                    else
                    {
                        Counters.Add(counterToApply);
                    }
                }
            }
        }

        public void Clear()
        {
            lock (_lockObject)
            {
                Counters.Clear();
            }
        }
    }
}