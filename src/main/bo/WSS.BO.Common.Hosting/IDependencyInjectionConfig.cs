using Ninject;

namespace WSS.BO.Common.Hosting
{
    public interface IDependencyInjectionConfig
    {
        IKernel GetKernel();
    }
}