﻿using System.Configuration;
using WSS.BO.DataLayer.Database;

namespace WSS.BO.Common.Hosting
{
    public class AppConfigBasedOasysDbConfiguration : IOasysDbConfiguration
    {
        public string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["Oasys"].ConnectionString;
            }
        }

        public int? CommandTimeout
        {
            get
            {
                var appSettings = ConfigurationManager.AppSettings;

                var dbQueryTimeoutString = appSettings.Get("dbQueryTimeout");
                int dbQueryTimeout;

                if (!string.IsNullOrEmpty(dbQueryTimeoutString) && int.TryParse(dbQueryTimeoutString, out dbQueryTimeout))
                {
                    return dbQueryTimeout;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
