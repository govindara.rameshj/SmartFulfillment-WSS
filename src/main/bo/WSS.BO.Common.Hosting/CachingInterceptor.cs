﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Ninject.Extensions.Interception;
using System.Web.Caching;
using System.Web;
using slf4net;

namespace WSS.BO.Common.Hosting
{
    public class CachingInterceptor : IInterceptor
    {
        private static readonly ILogger Log = LoggerFactory.GetLogger(typeof(CachingInterceptor));
        private static readonly object DummyNull = new object();
        
        private readonly TimeSpan? absoluteExpirationPeriod;
        private readonly TimeSpan slidingExpiration;

        public CachingInterceptor(TimeSpan? absoluteExpirationPeriod, TimeSpan? slidingExpiration)
        {
            this.absoluteExpirationPeriod = absoluteExpirationPeriod;
            this.slidingExpiration = slidingExpiration ?? Cache.NoSlidingExpiration;
        }

        public void Intercept(IInvocation invocation)
        {
            object objectFromCache;
            string methodName;
            var key = String.Empty;
            try
            {
                methodName = string.Format("{0}.{1}", invocation.Request.Target.GetType(), invocation.Request.Method.Name);
                var parameters = invocation.Request.Method.GetParameters().ToList();
                key = GetKeyValue(invocation, methodName, parameters);
                objectFromCache = HttpRuntime.Cache.Get(key);
            }
            catch (Exception ex)
            {
                Log.Warn("Some error occured with caching for {0} key: {1} ", key, ex);
                invocation.Proceed();
                return;
            }

            if (objectFromCache == DummyNull)   //If we have null in cache - return null or default value
            {
                var returnType = invocation.Request.Method.ReturnType;
                invocation.ReturnValue = GetDefaultValue(returnType);
                return;
            }

            if (objectFromCache == null)  //There is no value in cache. So execute invocation and save value to cache
            {
                Log.Debug("{0} cache miss", methodName);
                invocation.Proceed();
                var value = invocation.ReturnValue ?? DummyNull;
                InsertToCache(key, value);
                Log.Debug("{0} retrieved value has been put to cache", methodName);
            }

            else //Return value from cache
            {
                Log.Debug("{0} returns value from cache", methodName);
                invocation.ReturnValue = objectFromCache;
            }
        }

        private void InsertToCache(string key, object value)
        {
            var absoluteExpiration = absoluteExpirationPeriod != null ? DateTime.Now.Add(absoluteExpirationPeriod.Value) : Cache.NoAbsoluteExpiration;
            HttpRuntime.Cache.Insert(key, value, null, absoluteExpiration, slidingExpiration);
        }

        private object GetDefaultValue(Type t)
        {
            if (t.IsValueType)
                return Activator.CreateInstance(t);

            return null;
        }

        private string GetKeyValue(IInvocation invocation, string methodName, List<ParameterInfo> parameters)
        {
            string result;
           
            if (parameters.Any())
            {
                // check if can cache
                foreach (var param in parameters)
                {
                    if (!IsCachableType(param.ParameterType))
                    {
                        throw new ApplicationException(String.Format("{0} does not support caching because of {1} parameter", methodName, param.ParameterType));
                    }
                }

                var sb = new StringBuilder(methodName);
                foreach (var arg in invocation.Request.Arguments)
                {
                    sb.AppendFormat(".{0}", arg);
                }
                result = sb.Replace(' ', '_').ToString();
            }
            else
            {
                result = methodName;
            }
            return result;
        }

        private bool IsCachableType(Type T)
        {
            return T.IsValueType || T == typeof(string);
        }
    }
}
