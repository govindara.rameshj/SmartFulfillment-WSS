﻿using Cts.Oasys.Core;
using WSS.BO.DataLayer.Database;

namespace WSS.BO.Common.Hosting
{
    public class ConnectionXMLBasedOasysDbConfiguration : IOasysDbConfiguration
    {
        public string ConnectionString
        {
            get
            {
                return ConnectionStringHelper.GetSqlConnectionString();
            }
        }


        public int? CommandTimeout
        {
            get
            {
                return null;
            }
        }
    }
}
