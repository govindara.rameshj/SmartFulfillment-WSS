// ReSharper disable PartialTypeWithSinglePart

namespace WSS.BO.Model.Entity
{
    /// <summary>Fulfillment item.</summary>
    public sealed class FulfillmentItem
    {
        /// <summary>Retail transaction item index, not null, smallint.</summary>
        public int TransactionItemIndex { get; set; }

        /// <summary>Quantity ordered, not null, decimal(5,0).</summary>
        public int AbsQuantity { get; set; }

        /// <summary>Fulfilling store number.</summary>
        public string FulfillerNumber { get; set; }
    }
}
