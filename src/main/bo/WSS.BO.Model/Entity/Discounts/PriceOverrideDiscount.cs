// ReSharper disable PartialTypeWithSinglePart

using WSS.BO.Model.Enums;

namespace WSS.BO.Model.Entity.Discounts
{
    /// <summary>Price Override Discount.</summary>
    public sealed class PriceOverrideDiscount : AmountDiscount
    {
        /// <summary>Price override code, null, smallint.</summary>
        public PriceOverrideReasonCode PriceOverrideCode { get; set; }

        /// <summary>Description for reason code.</summary>
        public string Description { get; set; }
    }
}
