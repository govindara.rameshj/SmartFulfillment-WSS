// ReSharper disable PartialTypeWithSinglePart

namespace WSS.BO.Model.Entity.Discounts
{
    /// <summary>Employee discount.</summary>
    public sealed class EmployeeDiscount : AmountDiscount
    {
        /// <summary>Employee discount card.</summary>
        public string CardNumber { get; set; }
    }
}
