// ReSharper disable PartialTypeWithSinglePart

namespace WSS.BO.Model.Entity.Discounts
{
    /// <summary>Amount Discount.</summary>
    public class AmountDiscount : Discount
    {
        /// <summary>Saving amount of discount.</summary>
        public decimal SavingAmount { get; set; }

        /// <summary>Authorizing cashier, null, char(3).</summary>
        public string SupervisorNumber { get; set; }
    }
}
