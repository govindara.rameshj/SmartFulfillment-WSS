// ReSharper disable PartialTypeWithSinglePart

using System.Collections.Generic;

namespace WSS.BO.Model.Entity.Discounts
{
    /// <summary>Event Discount.</summary>
    public sealed class EventDiscount : Discount
    {
        /// <summary>Event Descriptor.</summary>
        public EventDescriptor EventDescriptor { get; set; }

        /// <summary>Event hit amounts.</summary>
        public IList<decimal> HitAmounts { get; set; }
    }
}
