// ReSharper disable PartialTypeWithSinglePart

namespace WSS.BO.Model.Entity.Discounts
{
    /// <summary>Price Match Discount.</summary>
    public sealed class PriceMatchDiscount : AmountDiscount
    {
        /// <summary>Competitor name.</summary>
        public string CompetitorName { get; set; }

        /// <summary>New price.</summary>
        public decimal NewPrice { get; set; }

        /// <summary>Is VAT included.</summary>
        public bool IsVATIncluded { get; set; }
    }
}
