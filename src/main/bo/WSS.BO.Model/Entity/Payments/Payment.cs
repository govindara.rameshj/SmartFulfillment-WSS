// ReSharper disable PartialTypeWithSinglePart

using WSS.BO.Model.Enums;

namespace WSS.BO.Model.Entity.Payments
{
    /// <summary>Transaction payment.</summary>
    public partial class Payment
    {
        /// <summary>Payment amount, not null, decimal(9,2).</summary>
        public decimal AbsAmount { get; set; }

        /// <summary>Item direction.</summary>
        public Direction Direction { get; set; }
    }
}
