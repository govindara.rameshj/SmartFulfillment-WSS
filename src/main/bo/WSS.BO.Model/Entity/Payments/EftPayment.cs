// ReSharper disable PartialTypeWithSinglePart

using WSS.BO.Model.Enums;

namespace WSS.BO.Model.Entity.Payments
{
    /// <summary>Transaction card payment.</summary>
    public sealed class EftPayment : Payment
    {
        /// <summary>Authorization code, null, char(9).</summary>
        public string AuthCode { get; set; }

        /// <summary>Authorized description, null, char(30).</summary>
        public string AuthDescription { get; set; }

        /// <summary>Card was keyed or swiped, not null, enum.</summary>
        public CardAcceptType CardAcceptType { get; set; }

        /// <summary>Unique transaction ID, null, char(6).</summary>
        public string TransactionUid { get; set; }

        /// <summary>Merchant number, null, char(15).</summary>
        public string MerchantNumber { get; set; }

        /// <summary>Commidea account of file registration ID, null, varchar(50).</summary>
        public string TokenId { get; set; }

        /// <summary>Payment card.</summary>
        public PaymentCard PaymentCard { get; set; }
    }
}
