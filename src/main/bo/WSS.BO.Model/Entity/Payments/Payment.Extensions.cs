﻿using WSS.BO.Model.Enums;

namespace WSS.BO.Model.Entity.Payments
{
    public partial class Payment
    {
        public decimal GetSignedAmount()
        {
            return Direction == Direction.FROM_CUSTOMER ? AbsAmount : -AbsAmount;
        }
    }
}
