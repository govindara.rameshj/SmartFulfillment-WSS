// ReSharper disable PartialTypeWithSinglePart

using WSS.BO.Model.Enums;

namespace WSS.BO.Model.Entity.Payments
{
    /// <summary>Gift card payment.</summary>
    public sealed class GiftCardPayment : Payment
    {
        /// <summary>Card was keyed or swiped, not null, enum.</summary>
        public CardAcceptType CardAcceptType { get; set; }

        /// <summary>Authorization description.</summary>
        public string CardDescription { get; set; }

        /// <summary>Gift card operation.</summary>
        public GiftCardOperation GiftCardOperation { get; set; }
    }
}
