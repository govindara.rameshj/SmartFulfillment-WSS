// ReSharper disable PartialTypeWithSinglePart

using System.Collections.Generic;
using WSS.BO.Model.Entity.Payments;

namespace WSS.BO.Model.Entity.Transactions
{
    /// <summary>Payable Transaction</summary>
    public partial class PayableTransaction : VoidableTransaction
    {
        /// <summary>Payments.</summary>
        public IList<Payment> Payments { get; set; }

        /// <summary>Total transaction amount, not null, decimal(9,2).</summary>
        public decimal TotalAmount { get; set; }
    }
}
