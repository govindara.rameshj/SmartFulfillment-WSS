// ReSharper disable PartialTypeWithSinglePart

using System;
using System.Collections.Generic;

namespace WSS.BO.Model.Entity.Transactions
{
    /// <summary>Transaction.</summary>
    public class Transaction
    {
        /// <summary>Source system name.</summary>
        public string Source { get; set; }

        /// <summary>Source system transaction identifier.</summary>
        public string SourceTransactionId { get; set; }

        /// <summary>Store number.</summary>
        public string StoreNumber { get; set; }

        /// <summary>Creation date and time, not null, 2 fields</summary>
        public DateTime CreateDateTime { get; set; }

        /// <summary>Till number, not null, char(2).</summary>
        public string TillNumber { get; set; }

        /// <summary>Transaction number, not null, char(4).</summary>
        public string TransactionNumber { get; set; }

        /// <summary>Cashier number, not null, char(3).</summary>
        public string CashierNumber { get; set; }

        /// <summary>Brand code.</summary>
        public string BrandCode { get; set; }

        /// <summary>Is training.</summary>
        public bool IsTraining { get; set; }

        /// <summary>Array of related transactions.</summary>
        public IList<Transaction> RelatedTransactions { get; set; }

        /// <summary>barcode.</summary>
        public string ReceiptBarcode { get; set; }

        /// <summary>Additional info.</summary>
        public IList<NameValuePair> AdditionalInfo { get; set; }
    }
}
