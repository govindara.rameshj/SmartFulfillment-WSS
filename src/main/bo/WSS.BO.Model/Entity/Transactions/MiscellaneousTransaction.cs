// ReSharper disable PartialTypeWithSinglePart

namespace WSS.BO.Model.Entity.Transactions
{
    /// <summary>MiscellaneousTransaction.</summary>
    public class MiscellaneousTransaction : PayableTransaction
    {
        /// <summary>Invoice number.</summary>
        public string InvoiceNumber { get; set; }
    }
}
