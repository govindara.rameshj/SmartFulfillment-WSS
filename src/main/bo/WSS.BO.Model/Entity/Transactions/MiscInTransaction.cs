// ReSharper disable PartialTypeWithSinglePart

using WSS.BO.Model.Enums;

namespace WSS.BO.Model.Entity.Transactions
{
    /// <summary>Misc in transaction.</summary>
    public sealed partial class MiscInTransaction : MiscellaneousTransaction
    {
        /// <summary>Reason code.</summary>
        public MiscInReasonCode ReasonCode { get; set; }

        /// <summary>Description for reason code.</summary>
        public string Description { get; set; }
    }
}
