﻿using System.Linq;
using WSS.BO.Model.Enums;

namespace WSS.BO.Model.Entity.Transactions
{
    public partial class PayableTransaction
    {
        public decimal CalculatePaymentsAmount()
        {
            return Payments.Sum(x => x.Direction == Direction.FROM_CUSTOMER ? x.AbsAmount : -x.AbsAmount);
        }
    }
}
