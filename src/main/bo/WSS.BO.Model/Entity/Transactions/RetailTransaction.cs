// ReSharper disable PartialTypeWithSinglePart

using System.Collections.Generic;
using WSS.BO.Model.Entity.Fulfillments;

namespace WSS.BO.Model.Entity.Transactions
{
    /// <summary>Retail transaction.</summary>
    public sealed partial class RetailTransaction : PayableTransaction
    {
        /// <summary>Transaction items.</summary>
        public IList<RetailTransactionItem> Items { get; set; }

        /// <summary>List of contact info.</summary>
        public IList<ContactInfo> Contacts { get; set; }

        /// <summary>Customer.</summary>
        public Customer Customer { get; set; }

        /// <summary>Tax amount, not null, decimal(9,2).</summary>
        public decimal TaxAmount { get; set; }

        /// <summary>Fulfillments.</summary>
        public IList<Fulfillment> Fulfillments { get; set; }

        /// <summary>Cancellation.</summary>
        public Fulfillment Cancellation { get; set; }

        /// <summary>Id of recalled transaction.</summary>
        public string RecalledTransactionId { get; set; }

        /// <summary>Manager.</summary>
        public string Manager { get; set; }

        /// <summary>Refund cashier.</summary>
        public string RefundCashier { get; set; }

        /// <summary>Coupons.</summary>
        public IList<Coupon> Coupons { get; set; }

        /// <summary>Restricted items</summary>
        public IList<RestrictedItem> RestrictedItems { get; set; }

        /// <summary>Survey postcode.</summary>
        public string SurveyPostcode { get; set; }
    }
}
