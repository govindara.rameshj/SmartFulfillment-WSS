// ReSharper disable PartialTypeWithSinglePart

namespace WSS.BO.Model.Entity.Transactions
{
    /// <summary>Open drawer transaction.</summary>
    public sealed class OpenDrawerTransaction : VoidableTransaction
    {
    }
}
