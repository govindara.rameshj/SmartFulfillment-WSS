// ReSharper disable PartialTypeWithSinglePart

using WSS.BO.Model.Enums;

namespace WSS.BO.Model.Entity.Transactions
{
    /// <summary>Audit transaction.</summary>
    public sealed class AuditTransaction : Transaction
    {
        /// <summary>Audit type.</summary>
        public AuditType AuditType { get; set; }
    }
}
