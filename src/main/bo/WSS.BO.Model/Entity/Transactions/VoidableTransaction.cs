// ReSharper disable PartialTypeWithSinglePart

using WSS.BO.Model.Enums;

namespace WSS.BO.Model.Entity.Transactions
{
    /// <summary>Voidable transaction.</summary>
    public class VoidableTransaction : Transaction
    {
        /// <summary>Authorizer number.</summary>
        public string AuthorizerNumber { get; set; }

        /// <summary>Transaction status.</summary>
        public TransactionStatus TransactionStatus { get; set; }

        /// <summary>Void supervisor.</summary>
        public string VoidSupervisor { get; set; }
    }
}
