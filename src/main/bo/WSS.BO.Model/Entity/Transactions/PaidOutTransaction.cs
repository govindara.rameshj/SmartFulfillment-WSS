// ReSharper disable PartialTypeWithSinglePart

using WSS.BO.Model.Enums;

namespace WSS.BO.Model.Entity.Transactions
{
    /// <summary>Paid out transaction.</summary>
    public sealed partial class PaidOutTransaction : MiscellaneousTransaction
    {
        /// <summary>Reason code.</summary>
        public PaidOutReasonCode ReasonCode { get; set; }

        /// <summary>Description for reason code.</summary>
        public string Description { get; set; }
    }
}
