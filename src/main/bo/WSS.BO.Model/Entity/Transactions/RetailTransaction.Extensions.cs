using System;
using System.Collections.Generic;
using System.Linq;
using Cts.Oasys.Core.Helpers;
using WSS.BO.Model.Entity.Products;
using WSS.BO.Model.Enums;
using WSS.BO.Model.Entity.Discounts;
using WSS.BO.Model.Entity.Fulfillments;

namespace WSS.BO.Model.Entity.Transactions
{
    /// <summary>RetailTransaction.</summary>
    public partial class RetailTransaction
    {
        public decimal CalculateExtpAmount(SkuProduct skuProduct)
        {
            var priceWithAppliedPriceOverride = skuProduct.InitialPrice - skuProduct.CalculatePriceOverrideChangePriceDifference();
            return AmountCalculationEngine.ApplyDiscountToValue(priceWithAppliedPriceOverride * skuProduct.AbsQuantity, skuProduct.GetEmployeeDiscountRate(), skuProduct.CalculateEventsDiscount());
        }

        public decimal CalculatePrice(SkuProduct skuProduct)
        {
            return AmountCalculationEngine.ApplyDiscountToValue(skuProduct.InitialPrice - skuProduct.CalculatePriceOverrideChangePriceDifference(), skuProduct.GetEmployeeDiscountRate());
        }

        public decimal CalculateChargedAbsAmount(Product product)
        {
            if (product is SkuProduct)
            {
                var skuProduct = (SkuProduct) product;
                return CalculateExtpAmount(skuProduct) - skuProduct.CalculateEventsDiscount();
            }
            if (product is GiftCardProduct)
            {
                return ((GiftCardProduct) product).AbsAmount;
            }
            if (product is DeliveryProduct)
            {
                return ((DeliveryProduct) product).AbsAmount;
            }

            return 0;
        }

        public decimal CalculateEventDiscountsAmount()
        {
            decimal eventDiscountsAmount = 0;

            var skuProducts = Items.Select(x => x.Product).OfType<SkuProduct>();

            foreach(var skuProduct in skuProducts)
            {
                eventDiscountsAmount += skuProduct.CalculateEventsDiscount();
            }

            return eventDiscountsAmount;
        }

        public decimal CalculateEmployeeDiscountAmount()
        {
            decimal amountDiscountsAmount = 0;

            var skuProducts = Items.Select(x => x.Product).OfType<SkuProduct>();
            foreach (var skuProduct in skuProducts)
            {
                if (skuProduct.Discounts != null && skuProduct.Discounts.Any(x => x is EmployeeDiscount))
                {
                    amountDiscountsAmount += skuProduct.Discounts.OfType<EmployeeDiscount>().Sum(x => x.SavingAmount);
                }
            }
            return amountDiscountsAmount;
        }

        public bool IsRefund()
        {
            return TotalAmount < 0;
        }

        public bool IsSale()
        {
            return TotalAmount >= 0;
        }

        public decimal CalculateItemsAmount()
        {
            var items = Items.Where(x => !x.IsReversed).ToList();

            decimal sum = 0;
            foreach (var item in items)
            {
                decimal absAmount = item.Product.CalculateAbsAmount();

                sum += absAmount * (item.Direction == Direction.TO_CUSTOMER ? 1 : -1);
            }
            return sum;
        }

        public void AddContactIfUnique(ContactInfo contactToAdd)
        {
            CreateContacts();

            if (!Contacts.Any(x =>
                x.ContactAddress.AddressLine1 == contactToAdd.ContactAddress.AddressLine1 &&
                x.ContactAddress.AddressLine2 == contactToAdd.ContactAddress.AddressLine2 &&
                x.ContactAddress.PostCode == contactToAdd.ContactAddress.PostCode &&
                x.ContactAddress.Town == contactToAdd.ContactAddress.Town &&
                x.Email == contactToAdd.Email &&
                x.MobilePhone == contactToAdd.MobilePhone &&
                x.Name == contactToAdd.Name &&
                x.Phone == contactToAdd.Phone &&
                x.WorkPhone == contactToAdd.WorkPhone))
            {
                Contacts.Add(contactToAdd);
            }
        }

        private void CreateContacts()
        {
            if (Contacts == null)
            {
                Contacts = new List<ContactInfo>();
            }
        }

        public decimal GetMerchandiseAmount()
        {
            var skuProductItems = Items.Where(x => !x.IsReversed && x.Product is SkuProduct);
            decimal mercProductAmount = 0;

            foreach (var item in skuProductItems)
            {
                var skuProduct = (SkuProduct)item.Product;
                var initialPrice = skuProduct.InitialPrice;

                var priceAfterOverride = initialPrice - skuProduct.CalculatePriceOverrideChangePriceDifference();
                mercProductAmount += (item.Direction == Direction.TO_CUSTOMER ? 1 : -1) * priceAfterOverride * skuProduct.AbsQuantity;
            }

            var giftCardProducts = Items.Where(x => !x.IsReversed && x.Product is GiftCardProduct);
            foreach (var giftCardProduct in giftCardProducts)
            {
                mercProductAmount += (giftCardProduct.Direction == Direction.TO_CUSTOMER ? 1 : -1) * giftCardProduct.Product.CalculateAbsAmount();
            }

            return mercProductAmount;
        }

        public bool CheckIsSupervisorUsed(string nullSupervisorCode)
        {
            return CheckSupervisorFieldsFulfilled()
                || RestrictedItems.Any(x => !string.IsNullOrEmpty(x.AuthorizerNumber))
                || CheckDiscountsWithSupervisorFulfilled(nullSupervisorCode);
        }

        private bool CheckSupervisorFieldsFulfilled()
        {
            return !string.IsNullOrEmpty(AuthorizerNumber) || !string.IsNullOrEmpty(VoidSupervisor) || !string.IsNullOrEmpty(Manager);
        }

        private bool CheckDiscountsWithSupervisorFulfilled(string nullSupervisorCode)
        {
            var skuProducts = Items.Select(x => x.Product).OfType<SkuProduct>().Where(x => x.Discounts != null).ToList();
            var employeeDiscounts = skuProducts.SelectMany(x => x.Discounts.OfType<EmployeeDiscount>()).ToList();
            var amountDiscounts = skuProducts.SelectMany(x => x.Discounts.OfType<AmountDiscount>()).ToList();

            return employeeDiscounts.Any(x => !String.IsNullOrEmpty(x.SupervisorNumber))
                || CheckDiscountSupervisor(amountDiscounts, nullSupervisorCode);
        }

        private bool CheckDiscountSupervisor(List<AmountDiscount> amountDiscounts, string nullSupervisorCode)
        {
            return amountDiscounts.Any(y => !string.IsNullOrEmpty(y.SupervisorNumber) && y.SupervisorNumber != nullSupervisorCode);
        }

        public string GetDiscountSupervisor(string nullSupervisorCode)
        {
            var transactionEmployeeDiscounts = Items.Select(x => x.Product).OfType<SkuProduct>().SelectMany(x => x.Discounts).OfType<EmployeeDiscount>().ToList();

            if (transactionEmployeeDiscounts.Any(x => x.SupervisorNumber != null))
            {
                return transactionEmployeeDiscounts.Last().SupervisorNumber;
            }

            return nullSupervisorCode;
        }

        public bool CouponEventDiscountExists(string couponEventCode)
        {
            var transactionEventDiscount = Items.Select(x => x.Product).OfType<SkuProduct>().SelectMany(x => x.Discounts).OfType<EventDiscount>();
            return transactionEventDiscount.Any(x => x.EventDescriptor.EventCode == couponEventCode);
        }

        public List<T> GetDiscounts<T>() where T : Discount
        {
            return Items.Select(x => x.Product).OfType<SkuProduct>().SelectMany(x => x.Discounts).OfType<T>().ToList();
        }

        public int GetInstantFulfullmentQuantity(int itemIndex)
        {
            var instantFulfillment = (InstantFulfillment)Fulfillments.FirstOrDefault(x => x is InstantFulfillment);
            if (instantFulfillment != null)
            {
                var instantFulfillmentItem = instantFulfillment.Items.FirstOrDefault(x => x.TransactionItemIndex == itemIndex);
                return instantFulfillmentItem == null ? 0 : instantFulfillmentItem.AbsQuantity;
            }
            return 0;
        }

        public int? GetRequiredFulfuller(int itemIndex)
        {
            var orderFulfillment = (OrderFulfillment)Fulfillments.First(x => x is OrderFulfillment);
            var orderFulfillmentItem = orderFulfillment.Items.FirstOrDefault(x => x.TransactionItemIndex == itemIndex);
            if (orderFulfillmentItem != null && !string.IsNullOrEmpty(orderFulfillmentItem.FulfillerNumber))
            {
                return Int32.Parse(orderFulfillmentItem.FulfillerNumber);
            }
            return null;
        }
    }
}
