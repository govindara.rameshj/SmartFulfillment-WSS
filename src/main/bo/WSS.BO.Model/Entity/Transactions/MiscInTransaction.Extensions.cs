﻿namespace WSS.BO.Model.Entity.Transactions
{
    public partial class MiscInTransaction
    {
        public bool IsNormal()
        {
            return TotalAmount >= 0;
        }

        public bool IsCorrection()
        {
            return TotalAmount < 0;
        }
    }
}
