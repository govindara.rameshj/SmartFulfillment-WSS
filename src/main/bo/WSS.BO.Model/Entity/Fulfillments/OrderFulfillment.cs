// ReSharper disable PartialTypeWithSinglePart

namespace WSS.BO.Model.Entity.Fulfillments
{
    /// <summary>Fulfillment requiring customer info.</summary>
    public class OrderFulfillment : Fulfillment
    {
        /// <summary>Order number.</summary>
        public string Number { get; set; }

        /// <summary>Contact info index in RetailTransaction.contacts, null.</summary>
        public int ContactInfoIndex { get; set; }
    }
}
