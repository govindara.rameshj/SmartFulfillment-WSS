// ReSharper disable PartialTypeWithSinglePart

using System.Collections.Generic;

namespace WSS.BO.Model.Entity.Fulfillments
{
    /// <summary>Fulfillment.</summary>
    public class Fulfillment
    {
        /// <summary>Fulfillment items.</summary>
        public IList<FulfillmentItem> Items { get; set; }
    }
}
