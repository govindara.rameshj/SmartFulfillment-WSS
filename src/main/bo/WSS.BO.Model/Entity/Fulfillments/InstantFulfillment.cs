// ReSharper disable PartialTypeWithSinglePart

namespace WSS.BO.Model.Entity.Fulfillments
{
    /// <summary>Sale without order.</summary>
    public partial class InstantFulfillment : Fulfillment
    {
    }
}
