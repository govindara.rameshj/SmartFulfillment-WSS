// ReSharper disable PartialTypeWithSinglePart

using System;

namespace WSS.BO.Model.Entity.Fulfillments
{
    /// <summary>Collection.</summary>
    public sealed class CollectionFulfillment : OrderFulfillment
    {
        /// <summary>Collection date.</summary>
        public DateTime CollectionDate { get; set; }
    }
}
