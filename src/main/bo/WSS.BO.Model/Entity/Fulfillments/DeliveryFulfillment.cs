// ReSharper disable PartialTypeWithSinglePart

using System;
using System.Collections.Generic;
using WSS.BO.Model.Enums;

namespace WSS.BO.Model.Entity.Fulfillments
{
    /// <summary>Delivery.</summary>
    public sealed class DeliveryFulfillment : OrderFulfillment
    {
        /// <summary>Delivery date, null.</summary>
        public DateTime DeliveryDate { get; set; }

        /// <summary>Delivery type.</summary>
        public DeliveryType DeliveryType { get; set; }

        /// <summary>Authorizing cashier for free delivery, null, char(3).</summary>
        public string FreeDeliveryAuthorizerNumber { get; set; }

        /// <summary>Delivery instructions.</summary>
        public IList<string> DeliveryInstructions { get; set; }

        /// <summary>Delivery slot.</summary>
        public DeliverySlot DeliverySlot { get; set; }
    }
}
