// ReSharper disable PartialTypeWithSinglePart

namespace WSS.BO.Model.Entity
{
    /// <summary>Name-value object for additional info gathering.</summary>
    public sealed partial class NameValuePair
    {
        /// <summary>Name of parameter.</summary>
        public string Name { get; set; }

        /// <summary>Value of parameter.</summary>
        public string Value { get; set; }
    }
}
