// ReSharper disable PartialTypeWithSinglePart

namespace WSS.BO.Model.Entity
{
    /// <summary>Contact info.</summary>
    public sealed class ContactInfo
    {
        /// <summary>Name, not null, char(30).</summary>
        public string Name { get; set; }

        /// <summary>Phone, null, char(15).</summary>
        public string Phone { get; set; }

        /// <summary>Mobile phone, null, char(15).</summary>
        public string MobilePhone { get; set; }

        /// <summary>Work phone, null, varchar(20).</summary>
        public string WorkPhone { get; set; }

        /// <summary>E-mail, null, varchar(100).</summary>
        public string Email { get; set; }

        /// <summary>Contact address.</summary>
        public Address ContactAddress { get; set; }
    }
}
