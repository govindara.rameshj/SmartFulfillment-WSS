// ReSharper disable PartialTypeWithSinglePart

using WSS.BO.Model.Enums;

namespace WSS.BO.Model.Entity
{
    /// <summary>Event descriptor.</summary>
    public sealed class EventDescriptor
    {
        /// <summary>Event type.</summary>
        public EventType EventType { get; set; }

        /// <summary>Event code.</summary>
        public string EventCode { get; set; }
    }
}
