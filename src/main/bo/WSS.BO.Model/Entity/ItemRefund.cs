// ReSharper disable PartialTypeWithSinglePart

using WSS.BO.Model.Enums;

namespace WSS.BO.Model.Entity
{
    /// <summary>Item refund.</summary>
    public sealed class ItemRefund
    {
        /// <summary>Refund reason.</summary>
        public RefundReasonCode RefundReasonCode { get; set; }

        /// <summary>Id of refunded transaction.</summary>
        public string RefundedTransactionId { get; set; }

        /// <summary>Item id in refunded transaction.</summary>
        public string RefundedTransactionItemId { get; set; }

        /// <summary>Description for reason code.</summary>
        public string Description { get; set; }
    }
}
