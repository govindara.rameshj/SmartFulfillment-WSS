// ReSharper disable PartialTypeWithSinglePart

using System;
using WSS.BO.Model.Enums;

namespace WSS.BO.Model.Entity
{
    /// <summary>Payment card.</summary>
    public sealed class PaymentCard
    {
        /// <summary>Card number.</summary>
        public string MaskedCardNumber { get; set; }

        /// <summary>Hashed card number, null, char(28).</summary>
        public string PanHash { get; set; }

        /// <summary>Card type name.</summary>
        public CardType CardType { get; set; }

        /// <summary>Card description.</summary>
        public string CardDescription { get; set; }

        /// <summary>Card start date.</summary>
        public DateTime? CardStartDate { get; set; }

        /// <summary>Card expire date.</summary>
        public DateTime CardExpireDate { get; set; }

        /// <summary>Switch card issue number, null, char(2).</summary>
        public string IssueNumberSwitch { get; set; }
    }
}
