// ReSharper disable PartialTypeWithSinglePart

using System;

namespace WSS.BO.Model.Entity
{
    /// <summary>Delivery slot.</summary>
    public sealed class DeliverySlot
    {
        /// <summary>Slot id.</summary>
        public string Id { get; set; }

        /// <summary>Slot description.</summary>
        public string Description { get; set; }

        /// <summary>Start time.</summary>
        public TimeSpan StartTime { get; set; }

        /// <summary>End time.</summary>
        public TimeSpan EndTime { get; set; }
    }
}
