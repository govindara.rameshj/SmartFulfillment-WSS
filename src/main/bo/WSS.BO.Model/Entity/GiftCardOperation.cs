// ReSharper disable PartialTypeWithSinglePart

namespace WSS.BO.Model.Entity
{
    /// <summary>Gift card operation.</summary>
    public sealed class GiftCardOperation
    {
        /// <summary>Card number, not null, char(19).</summary>
        public string CardNumber { get; set; }

        /// <summary>Authorization Code assigned by Card Commerce.</summary>
        public string AuthCode { get; set; }
    }
}
