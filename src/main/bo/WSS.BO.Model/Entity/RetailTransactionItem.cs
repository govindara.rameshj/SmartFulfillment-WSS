// ReSharper disable PartialTypeWithSinglePart

using WSS.BO.Model.Entity.Products;
using WSS.BO.Model.Enums;

namespace WSS.BO.Model.Entity
{
    /// <summary>Transaction item.</summary>
    public sealed partial class RetailTransactionItem
    {
        /// <summary>Item index.</summary>
        public int ItemIndex { get; set; }

        /// <summary>Is reversed.</summary>
        public bool IsReversed { get; set; }

        /// <summary>Item refund.</summary>
        public ItemRefund ItemRefund { get; set; }

        /// <summary>Item direction.</summary>
        public Direction Direction { get; set; }

        /// <summary>Item product.</summary>
        public Product Product { get; set; }

        /// <summary>Item source item id.</summary>
        public string SourceItemId { get; set; }

        /// <summary>VAT rate.</summary>
        public decimal VatRate { get; set; }

        /// <summary>Absolute VAT amount.</summary>
        public decimal AbsVatAmount { get; set; }
    }
}
