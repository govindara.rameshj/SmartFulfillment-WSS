// ReSharper disable PartialTypeWithSinglePart

using WSS.BO.Model.Enums;

namespace WSS.BO.Model.Entity
{
    /// <summary>Restricted item.</summary>
    public sealed class RestrictedItem
    {
        /// <summary>Brand product identifier, not null, char(6).</summary>
        public string BrandProductId { get; set; }

        /// <summary>Restriction type.</summary>
        public RestrictionType RestrictionType { get; set; }

        /// <summary>Restricted item accept type.</summary>
        public RestrictedItemAcceptType AcceptType { get; set; }

        /// <summary>Confirmation string.</summary>
        public string Confirmation { get; set; }

        /// <summary>Authorizer number.</summary>
        public string AuthorizerNumber { get; set; }
    }
}
