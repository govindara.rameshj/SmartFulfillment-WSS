// ReSharper disable PartialTypeWithSinglePart

namespace WSS.BO.Model.Entity
{
    /// <summary>Contact address.</summary>
    public sealed class Address
    {
        /// <summary>Postcode, null, char(8).</summary>
        public string PostCode { get; set; }

        /// <summary>Address line 1, null, char(30).</summary>
        public string AddressLine1 { get; set; }

        /// <summary>Address line 2, null, char(30).</summary>
        public string AddressLine2 { get; set; }

        /// <summary>Town, null, char(30).</summary>
        public string Town { get; set; }
    }
}
