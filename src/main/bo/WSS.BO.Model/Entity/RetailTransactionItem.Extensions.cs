﻿using WSS.BO.Model.Entity.Products;

namespace WSS.BO.Model.Entity
{
    public sealed partial class RetailTransactionItem
    {
        public T GetTypedProduct<T>()
            where T : Product
        {
            return (T)Product;
        }
    }
}
