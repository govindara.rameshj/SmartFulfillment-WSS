﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.Model.Entity.Products
{
    public partial class Product
    {
        public decimal CalculateAbsAmount()
        {
            if (this is SkuProduct)
            {
                var skuProduct  = this as SkuProduct;

                return skuProduct.AbsQuantity * (skuProduct.InitialPrice - skuProduct.CalculatePriceOverrideChangePriceDifference());
            }
            else if (this is DeliveryProduct)
            {
                var deliveryProduct = this as DeliveryProduct;
                return deliveryProduct.AbsAmount;
            }
            else if (this is GiftCardProduct)
            {
                var giftCardProduct = this as GiftCardProduct;
                return giftCardProduct.AbsAmount;
            }
            else
            {
                return 0;
            }
        }
    }
}
