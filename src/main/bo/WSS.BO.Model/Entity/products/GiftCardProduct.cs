// ReSharper disable PartialTypeWithSinglePart

namespace WSS.BO.Model.Entity.Products
{
    /// <summary>Transaction item.</summary>
    public sealed class GiftCardProduct : Product
    {
        /// <summary>Amount.</summary>
        public decimal AbsAmount { get; set; }

        /// <summary>Gift card operation.</summary>
        public GiftCardOperation GiftCardOperation { get; set; }
    }
}
