// ReSharper disable PartialTypeWithSinglePart

namespace WSS.BO.Model.Entity.Products
{
    /// <summary>Charge of the delivery.</summary>
    public sealed class DeliveryProduct : Product
    {
        /// <summary>Amount.</summary>
        public decimal AbsAmount { get; set; }
    }
}
