using System.Linq;
using Cts.Oasys.Core.Helpers;
using WSS.BO.Model.Enums;
using System;
using WSS.BO.Model.Entity.Discounts;

namespace WSS.BO.Model.Entity.Products
{
    public partial class SkuProduct
    {
        public decimal CalculateEventsDiscount()
        {
            return CalculateQuantityBreakChangePriceDifference()
                   + CalculateDealGroupChangePriceDifference()
                   + CalculateMultibuyChangePriceDifference()
                   + CalculateHierachyChangePriceDifference();
        }

        public decimal CalculateQuantityBreakChangePriceDifference()
        {
            return GetEventDiscount(EventType.QUANTITY_BREAK);
        }

        public decimal CalculateDealGroupChangePriceDifference()
        {
            return GetEventDiscount(EventType.DEAL_GROUP);
        }

        public decimal CalculateMultibuyChangePriceDifference()
        {
            return GetEventDiscount(EventType.MULTIBUY);
        }

        public decimal CalculateHierachyChangePriceDifference()
        {
            return GetEventDiscount(EventType.HIERARCHY_SPEND);
        }

        public decimal CalculatePriceOverrideChangePriceDifference()
        {
            decimal discount = 0;

            if (Discounts != null && (Discounts.Any(x => x is PriceOverrideDiscount) || Discounts.Any(x => x is PriceMatchDiscount)))
            {
                decimal totalPriceOverrideAmount = Discounts.Where(x => x is PriceOverrideDiscount).Sum(x => ((PriceOverrideDiscount) x).SavingAmount) 
                    + Discounts.Where(x => x is PriceMatchDiscount).Sum(x => ((PriceMatchDiscount) x).SavingAmount);

                // As in TransactionPreProcessor we've already processed all discounts and put indivisible discounts' part into new Hierarchy Spend event discount
                // we will always have totalPriceOverrideAmount / AbsQuantity with only two signs after dot and can safely use Math.Round
                discount = Math.Round( totalPriceOverrideAmount / AbsQuantity, 2);
            }

            return discount;
        }

        private decimal GetEventDiscount(EventType type)
        {
            decimal discount = 0;

            if (Discounts != null && Discounts.Any(x => x is EventDiscount && ((EventDiscount) x).EventDescriptor.EventType == type))
            {
                var listOfTypeEventDiscounts = Discounts.Where(x => x is EventDiscount && ((EventDiscount) x).EventDescriptor.EventType == type).Select(x => x as EventDiscount);
                discount = listOfTypeEventDiscounts.Sum(eventDiscount => eventDiscount.HitAmounts.Sum(x => x));
            }

            return discount;
        }

        public decimal GetEmployeeDiscountRate()
        {
            decimal discountRate = 0;

            if (Discounts != null && Discounts.Any(x => x is EmployeeDiscount))
            {
                decimal totalEployeeDiscountAmount = Discounts.Where(x => x is EmployeeDiscount).Sum(x => ((EmployeeDiscount) x).SavingAmount);
                // !Important! Discount Rate here is calculated here as in old till, e.g. Price Override is applied first, then Event Discounts and the last is Employee Discount
                discountRate = Math.Round(totalEployeeDiscountAmount / ((InitialPrice - CalculatePriceOverrideChangePriceDifference()) * AbsQuantity - CalculateEventsDiscount()), 2);
            }

            return discountRate;
        }

        public string GetPriceOverrideCode()
        {
            if (Discounts != null && Discounts.Any(x => x is PriceMatchDiscount || x is PriceOverrideDiscount))
            {
                if (Discounts.OfType<PriceMatchDiscount>().Any(x => !string.IsNullOrEmpty(x.SupervisorNumber)))
                {
                    return Discounts.OfType<PriceMatchDiscount>().Last(x => !string.IsNullOrEmpty(x.SupervisorNumber)).SupervisorNumber;
                }
                if (Discounts.OfType<PriceOverrideDiscount>().Any(x => !string.IsNullOrEmpty(x.SupervisorNumber)))
                {
                    return Discounts.OfType<PriceOverrideDiscount>().Last(x => !string.IsNullOrEmpty(x.SupervisorNumber)).SupervisorNumber;
                }
            }

            return "0";
        }
    }
}
