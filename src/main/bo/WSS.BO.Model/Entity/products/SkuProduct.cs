// ReSharper disable PartialTypeWithSinglePart

using System.Collections.Generic;
using WSS.BO.Model.Entity.Discounts;
using WSS.BO.Model.Enums;

namespace WSS.BO.Model.Entity.Products
{
    /// <summary>Sku product.</summary>
    public sealed partial class SkuProduct : Product
    {
        /// <summary>Group product identifier.</summary>
        public string GroupProductId { get; set; }

        /// <summary>Brand product identifier, not null, char(6).</summary>
        public string BrandProductId { get; set; }

        /// <summary>Scanned or manual input.</summary>
        public ProductInputType ProductInputType { get; set; }

        /// <summary>Quantity sold, not null, decimal(7,0).</summary>
        public int AbsQuantity { get; set; }

        /// <summary>System lookup price, null, decimal(9,2).</summary>
        public decimal InitialPrice { get; set; }

        /// <summary>Discounts.</summary>
        public IList<Discount> Discounts { get; set; }

        /// <summary>Stock Type</summary>
        public StockType StockType { get; set; }
    }
}
