// ReSharper disable PartialTypeWithSinglePart

namespace WSS.BO.Model.Entity
{
    /// <summary>Customer.</summary>
    public sealed class Customer
    {
        /// <summary>Contact info index in RetailTransaction.contacts, null.</summary>
        public int ContactInfoIndex { get; set; }
    }
}
