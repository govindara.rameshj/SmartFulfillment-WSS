// ReSharper disable PartialTypeWithSinglePart

namespace WSS.BO.Model.Entity
{
    /// <summary>Coupon.</summary>
    public sealed class Coupon
    {
        /// <summary>Event Descriptor.</summary>
        public EventDescriptor EventDescriptor { get; set; }

        /// <summary>Coupon Barcode.</summary>
        public string CouponBarcode { get; set; }
    }
}
