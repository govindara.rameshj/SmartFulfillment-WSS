// ReSharper disable InconsistentNaming
namespace WSS.BO.Model.Enums
{
    /// <summary>Restricted item accept type.</summary>
    public enum RestrictedItemAcceptType
    {
        ACCEPTED_COMPLETELY,
        ACCEPTED_WITH_CONFIRMATION,
        REJECTED_COMPLETELY,
        REJECTED_NOT_CONFIRMED,
    }
}
