// ReSharper disable InconsistentNaming
namespace WSS.BO.Model.Enums
{
    /// <summary>Price override reason code.</summary>
    public enum PriceOverrideReasonCode
    {
        DAMAGED_GOODS,
        INCORRECT_PRICE_DISPLAYED,
        REFUND_PRICE_DIFFERENCE,
        HDC_RETURN,
        WEB_RETURN,
        MANAGERS_DISCRETION,
        UNKNOWN,
    }
}
