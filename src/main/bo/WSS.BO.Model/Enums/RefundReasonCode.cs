// ReSharper disable InconsistentNaming
namespace WSS.BO.Model.Enums
{
    /// <summary>Refund reason code.</summary>
    public enum RefundReasonCode
    {
        CUSTOMER_CHANGED_MIND,
        DAMAGED,
        PARTS_MISSING,
        FAULTY,
        WEB_ORDER,
        UNKNOWN,
    }
}
