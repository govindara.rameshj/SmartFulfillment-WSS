// ReSharper disable InconsistentNaming
namespace WSS.BO.Model.Enums
{
    /// <summary>Event Type.</summary>
    public enum EventType
    {
        QUANTITY_BREAK,
        DEAL_GROUP,
        MULTIBUY,
        HIERARCHY_SPEND,
    }
}
