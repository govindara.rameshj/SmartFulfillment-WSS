// ReSharper disable InconsistentNaming
namespace WSS.BO.Model.Enums
{
    /// <summary>Restriction type.</summary>
    public enum RestrictionType
    {
        AGE_CHECK,
        EXPLOSIVES_POISONS,
        QUARANTINE,
    }
}
