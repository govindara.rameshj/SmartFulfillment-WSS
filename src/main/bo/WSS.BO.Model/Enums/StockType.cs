﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.Model.Enums
{
    /// <summary>Audit Type.</summary>
    public enum StockType
    {
        NORMAL,
        MARKDOWN,
    }
}
