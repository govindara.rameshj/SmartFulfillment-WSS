// ReSharper disable InconsistentNaming
namespace WSS.BO.Model.Enums
{
    /// <summary>Transaction status.</summary>
    public enum TransactionStatus
    {
        VOIDED,
        PARKED,
        COMPLETED,
    }
}
