// ReSharper disable InconsistentNaming
namespace WSS.BO.Model.Enums
{
    /// <summary>Misc in reason code.</summary>
    public enum MiscInReasonCode
    {
        BAD_CHEQUE,
        SOCIAL_FUND,
        SUSPENSE,
        CAR_PARK_TRADING,
        SAFE_OVERS,
        VENDING_MACHINE,
        DISPLAY_SALE,
        SAFE_CHANGE,
        COUNCIL_ACCOUNT,
        CHARITY,
        REFUND_TILL,
        H_O_CHEQUE,
        REPAY_PROJECT_LOAN,
        UNKNOWN,
    }
}
