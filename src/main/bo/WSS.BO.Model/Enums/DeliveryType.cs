// ReSharper disable InconsistentNaming
namespace WSS.BO.Model.Enums
{
    /// <summary>Order delivery type.</summary>
    public enum DeliveryType
    {
        NORMAL,
        EXPRESS,
    }
}
