// ReSharper disable InconsistentNaming
namespace WSS.BO.Model.Enums
{
    /// <summary>Paid out reason code.</summary>
    public enum PaidOutReasonCode
    {
        CUSTOMER_CONCERN,
        STATIONERY,
        BREAKFAST_VOUCHER,
        SOCIAL_FUND,
        TRAVEL,
        POSTAGE,
        MEETING_FOOD,
        COLLEAGUE_WELFARE,
        COUNCIL_ACCOUNT,
        SUSPENSE,
        SAFE_UNDERS,
        CLEANING,
        CHARITY,
        REFUND_TILL,
        H_O_CHEQUE,
        FORKLIFT_FUEL,
        PROJECT_LOAN,
        UNKNOWN,
    }
}
