// ReSharper disable InconsistentNaming
namespace WSS.BO.Model.Enums
{
    /// <summary>Product or payment direction.</summary>
    public enum Direction
    {
        FROM_CUSTOMER,
        TO_CUSTOMER,
    }
}
