// ReSharper disable InconsistentNaming
namespace WSS.BO.Model.Enums
{
    /// <summary>Audit Type.</summary>
    public enum AuditType
    {
        SIGN_ON,
        SIGN_OFF,
    }
}
