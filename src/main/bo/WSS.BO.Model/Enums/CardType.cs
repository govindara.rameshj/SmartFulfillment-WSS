// ReSharper disable InconsistentNaming
namespace WSS.BO.Model.Enums
{
    /// <summary>Card Type.</summary>
    public enum CardType
    {
        VISA,
        MASTER,
        MAESTRO,
        AMERICAN_EXPRESS,
        UNKNOWN,
    }
}
