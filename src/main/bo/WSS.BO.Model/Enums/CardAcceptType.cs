// ReSharper disable InconsistentNaming
namespace WSS.BO.Model.Enums
{
    /// <summary>Payment card accept type.</summary>
    public enum CardAcceptType
    {
        SWIPED,
        KEYED,
    }
}
