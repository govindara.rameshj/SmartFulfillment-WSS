// ReSharper disable InconsistentNaming
namespace WSS.BO.Model.Enums
{
    /// <summary>Product input type.</summary>
    public enum ProductInputType
    {
        SCANNED,
        KEYED,
    }
}
