﻿Imports System.Configuration


Public Class AppCollection
    Inherits ConfigurationElementCollection

    Public Sub New()
    End Sub

    Protected Overloads Overrides Function CreateNewElement() As ConfigurationElement
        Return New AppProcess()
    End Function

    Protected Overrides Function GetElementKey(ByVal element As ConfigurationElement) As Object
        Dim key As String = CType(element, AppProcess).AppName & Space(1) & CType(element, AppProcess).AppArguments
        Return key.Trim
    End Function

    Public Overrides ReadOnly Property CollectionType() As ConfigurationElementCollectionType
        Get
            Return ConfigurationElementCollectionType.BasicMap
        End Get
    End Property

    Protected Overrides ReadOnly Property ElementName() As String
        Get
            Return "add"
        End Get
    End Property

    Public Overrides Function IsReadOnly() As Boolean
        Return False
    End Function

End Class
