Imports System.Configuration.ConfigurationManager

Public Class AppService
    Private _timerInterval As Integer = 60000     '1 minute.
    Private _appSection As AppSection

    Protected Overrides Sub OnStart(ByVal args() As String)
        Trace.WriteLine(My.Resources.AppStart, Me.ServiceName)

        Dim config As Configuration.Configuration = OpenExeConfiguration(Configuration.ConfigurationUserLevel.None)
        _appSection = CType(config.GetSection("appProcesses"), AppSection)

        For Each ap As AppProcess In _appSection.AppProcesses
            Trace.WriteLine("appProcess: " & ap.WorkingDirectory & " - " & ap.AppName & Space(1) & ap.AppArguments & " @:" & ap.Interval, Me.ServiceName)
        Next

        tmrCounter.Interval = _timerInterval
        tmrCounter.Enabled = True
        tmrCounter.Start()

    End Sub

    Protected Overrides Sub OnStop()
        tmrCounter.Enabled = False
        Trace.WriteLine(My.Resources.AppEnd, Me.ServiceName)
    End Sub

    Private Sub tmrCounter_Tick(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmrCounter.Elapsed

        Try
            Trace.WriteLine(My.Resources.AppTick, Me.ServiceName)
            tmrCounter.Enabled = False

            For Each ap As AppProcess In _appSection.AppProcesses
                ap.Counter += 1

                If ap.Interval <= ap.Counter Then
                    'check whether process already running first
                    Dim alreadyRunning As Boolean = False
                    Dim runningProcesses() As Process = Process.GetProcesses
                    For Each runningProcess As Process In runningProcesses

                        If runningProcess.ProcessName = ap.AppName.Remove(ap.AppName.Length - 4, 4) Then
                            If runningProcess.StartInfo.Arguments = ap.AppArguments Then
                                alreadyRunning = True
                                Exit For
                            End If
                        End If
                    Next

                    If alreadyRunning Then
                        Trace.WriteLine(String.Format(My.Resources.ProcessRunning, ap.AppName), Me.ServiceName)
                    Else
                        Trace.WriteLine(String.Format(My.Resources.ProcessStart, ap.AppName), Me.ServiceName)
                        Dim startInfo As New ProcessStartInfo
                        startInfo.WorkingDirectory = ap.WorkingDirectory
                        startInfo.FileName = ap.WorkingDirectory & "\" & ap.AppName
                        startInfo.Arguments = ap.AppArguments
                        startInfo.WindowStyle = ProcessWindowStyle.Normal

                        Dim newProc As Diagnostics.Process = Diagnostics.Process.Start(startInfo)
                        ap.Counter = 0
                    End If

                Else
                    Trace.WriteLine(String.Format(My.Resources.ProcessSkipped, ap.AppName, ap.Counter, ap.Interval), Me.ServiceName)
                End If
            Next

        Catch ex As Exception
            Trace.WriteLine(ex.ToString, Me.ServiceName)
        Finally
            tmrCounter.Enabled = True
        End Try

    End Sub

End Class

