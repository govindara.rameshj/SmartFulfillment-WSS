﻿Imports System.Configuration

Public Class AppProcess
    Inherits ConfigurationElement
    Private _counter As Integer

    <ConfigurationProperty("interval", IsKey:=True, isRequired:=True)> Public ReadOnly Property Interval() As Integer
        Get
            Return CInt(Me("interval"))
        End Get
    End Property
    <ConfigurationProperty("workingDirectory", isRequired:=True)> Public ReadOnly Property WorkingDirectory() As String
        Get
            Return CStr(Me("workingDirectory"))
        End Get
    End Property
    <ConfigurationProperty("appName", isRequired:=True)> Public ReadOnly Property AppName() As String
        Get
            Return CStr(Me("appName"))
        End Get
    End Property
    <ConfigurationProperty("appArguments", isRequired:=True)> Public ReadOnly Property AppArguments() As String
        Get
            Return CStr(Me("appArguments"))
        End Get
    End Property

    Public Overrides Function IsReadOnly() As Boolean
        Return False
    End Function

    Public Property Counter() As Integer
        Get
            Return _counter
        End Get
        Set(ByVal value As Integer)
            _counter = value
        End Set
    End Property

End Class