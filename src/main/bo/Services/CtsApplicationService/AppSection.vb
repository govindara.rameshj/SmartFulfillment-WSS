﻿Imports System.Configuration

Public Class AppSection
    Inherits ConfigurationSection

    <ConfigurationProperty("", IsDefaultCollection:=True)> Public ReadOnly Property AppProcesses() As AppCollection
        Get
            Return CType(Me(""), AppCollection)
        End Get
    End Property

    Public Overrides Function IsReadOnly() As Boolean
        Return False
    End Function

End Class
