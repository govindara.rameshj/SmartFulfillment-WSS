﻿Public Class PickInstructionsFactory
    Inherits RequirementSwitchFactory(Of IPickInstructions)

    Public Overrides Function ImplementationA() As IPickInstructions

        Return New PickInstructions
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet()

        ImplementationA_IsActive = CBool(SwitchRepository.RequirementEnabled(-118))
    End Function

    Public Overrides Function ImplementationB() As IPickInstructions

        Return New PickInstructionsOldWay
    End Function

End Class
