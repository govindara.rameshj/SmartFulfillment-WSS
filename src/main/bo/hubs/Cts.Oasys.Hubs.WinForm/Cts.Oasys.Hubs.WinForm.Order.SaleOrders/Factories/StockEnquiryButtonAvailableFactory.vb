﻿Public Class StockEnquiryButtonAvailableFactory

    Inherits RequirementSwitchFactory(Of Boolean)

    Public Overrides Function ImplementationA() As Boolean

        Return True
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet()

        ImplementationA_IsActive = CBool(SwitchRepository.RequirementEnabled(-22005))
    End Function

    Public Overrides Function ImplementationB() As Boolean

        Return False
    End Function

End Class
