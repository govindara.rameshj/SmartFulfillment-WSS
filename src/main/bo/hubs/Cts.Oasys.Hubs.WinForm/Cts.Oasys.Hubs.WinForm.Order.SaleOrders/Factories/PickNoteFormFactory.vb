﻿Imports Cts.Oasys.Hubs.Core.System.Store

Public Class PickNoteFormFactory
    Inherits BaseFactory(Of IPickNoteForm)

    Private _qod As QodHeader
    Private _sellingStore As Store
    Private _fulfillingStoreId As Integer

    Public Overrides Function Implementation() As IPickNoteForm
        Return CType(New DespatchNoteModified(_qod, _sellingStore, _fulfillingStoreId), IPickNoteForm)
    End Function

    Public Sub Initialise(ByVal qod As QodHeader, ByVal sellingStore As Store, ByVal fulfillingStoreId As Integer)
        _qod = qod
        _sellingStore = sellingStore
        _fulfillingStoreId = fulfillingStoreId
    End Sub
End Class
