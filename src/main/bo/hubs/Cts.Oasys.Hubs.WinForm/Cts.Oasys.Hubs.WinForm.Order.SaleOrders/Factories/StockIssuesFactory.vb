﻿Public Class StockIssuesFactory

    Inherits RequirementSwitchFactory(Of IStockIssues)

    Public Overrides Function ImplementationA() As IStockIssues
        Return New StockIssuesNegativeOnHand
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = CBool(SwitchRepository.RequirementEnabled(-121))
    End Function

    Public Overrides Function ImplementationB() As IStockIssues
        Return New StockIssuesOldWay
    End Function

End Class
