﻿Namespace Factories
    Public Class FactoryDespatchNote2109

        Public Shared Function FactoryGet() As Interfaces.IDespatchNote2109

            If Cts.Oasys.Hubs.Core.System.Parameter.GetBoolean(-2109) Then
                'using new implementation
                Return New Implementations.DespatchNote2109
            Else
                'using existing implementation
                Return New Implementations.DespatchNote2109Live
            End If
        End Function
    End Class
End Namespace
