﻿<Assembly: Runtime.CompilerServices.InternalsVisibleTo("Cts.Oasys.Hubs.WinForm.Order.SaleOrders.UnitTest, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                                                                               "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                                                                               "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                                                                               "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                                                                               "54e0a4a4")> 
Public Class GetOrderNumberFactory

    Private Const Parameter As Integer = -1086

    Public Function FactoryGet() As IGetOrderNumber
        If IsEnabled() = True Then
            Return New GetOrderNumber
        Else
            Return New GetOrderNumberDoesNothing
        End If
    End Function

    Friend Overridable Function IsEnabled() As Boolean
        Return Cts.Oasys.Hubs.Core.System.Parameter.GetBoolean(Parameter)
    End Function

End Class

