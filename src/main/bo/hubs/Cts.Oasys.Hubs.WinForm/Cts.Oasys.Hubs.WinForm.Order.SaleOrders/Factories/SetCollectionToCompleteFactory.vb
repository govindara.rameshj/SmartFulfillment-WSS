﻿Public Class SetCollectionToCompleteFactory

    Private Const parameterID As Integer = -22022

    Public Overridable Function IsEnabled() As Boolean
        Return Core.System.Parameter.GetBoolean(parameterID)
    End Function

    Public Overridable Function FactoryGet() As ISetCollectionToComplete
        If IsEnabled() = True Then
            Return New SetCollectionToComplete
        Else
            Return New SetCollectionToCompleteDoesNothing
        End If
    End Function

End Class
