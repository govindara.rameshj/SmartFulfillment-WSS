﻿Public Class GetHeaderOrderFactory
    Inherits RequirementSwitchFactory(Of IGridHeaderOrder)

    Public Overrides Function ImplementationA() As IGridHeaderOrder

        Return New GridHeaderOrder
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet()

        ImplementationA_IsActive = CBool(SwitchRepository.RequirementEnabled(-22004))
    End Function

    Public Overrides Function ImplementationB() As IGridHeaderOrder

        Return New GridHeaderOrderOriginal
    End Function

End Class
