﻿Public Class PickNoteFactory
    Inherits BaseFactory(Of IPickNote)

    Public Overrides Function Implementation() As IPickNote
        Return New PickNote
    End Function
End Class
