﻿Public Class PickConfirmationFactory

    Inherits TpWickes.Hubs.Library.RequirementSwitchFactory(Of IPickconformation)

    Public Overrides Function ImplementationA() As IPickconformation
        Return New PickConfirmationStandardStore
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = (CBool(SwitchRepository.RequirementEnabled(-22021) AndAlso Not CBool(SwitchRepository.RequirementEnabled(970100))))
    End Function

    Public Overrides Function ImplementationB() As IPickconformation
        Return New PickConfirmWarehouse
    End Function

End Class


