﻿Public Class PickConfirmedStatusFactory
    Inherits TpWickes.Hubs.Library.RequirementSwitchFactory(Of IPickConfirmedStatus)

    Public Overrides Function ImplementationA() As IPickConfirmedStatus
        Return New PickConfirmationStatusActivated
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = (CBool(SwitchRepository.RequirementEnabled(-22021) AndAlso Not CBool(SwitchRepository.RequirementEnabled(970100))))
    End Function

    Public Overrides Function ImplementationB() As IPickConfirmedStatus
        Return New PickConfirmationStatusInActive
    End Function
End Class
