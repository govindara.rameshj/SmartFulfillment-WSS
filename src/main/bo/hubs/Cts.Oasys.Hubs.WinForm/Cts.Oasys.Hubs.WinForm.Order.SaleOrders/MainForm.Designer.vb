<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits Cts.Oasys.WinForm.Form

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.uxOpenOrderGrid = New DevExpress.XtraGrid.GridControl()
        Me.uxOpenOrderView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.uxExitButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxRetrieveButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxMaintainButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxOrderTabControl = New DevExpress.XtraTab.XtraTabControl()
        Me.uxNonZeroStockTab = New DevExpress.XtraTab.XtraTabPage()
        Me.uxNonZeroGrid = New DevExpress.XtraGrid.GridControl()
        Me.uxNonZeroView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.uxOpenOrderTab = New DevExpress.XtraTab.XtraTabPage()
        Me.txtOvcNumberOpenOrders = New DevExpress.XtraEditors.TextEdit()
        Me.lblOvcNumberOpenOrders = New DevExpress.XtraEditors.LabelControl()
        Me.panelOpenOrdersCustomerSearch = New System.Windows.Forms.Panel()
        Me.lblTelephoneNumberOpenOrders = New DevExpress.XtraEditors.LabelControl()
        Me.lblCustomerNameOpenOrders = New DevExpress.XtraEditors.LabelControl()
        Me.lblPostCodeOpenOrders = New DevExpress.XtraEditors.LabelControl()
        Me.txtTelephoneNumberOpenOrders = New DevExpress.XtraEditors.TextEdit()
        Me.txtCustomerNameOpenOrders = New DevExpress.XtraEditors.TextEdit()
        Me.txtPostCodeOpenOrders = New DevExpress.XtraEditors.TextEdit()
        Me.uxOMOrderNumber = New DevExpress.XtraEditors.TextEdit()
        Me.lblOMOrderNumber = New DevExpress.XtraEditors.LabelControl()
        Me.uxShowFulfilCheck = New DevExpress.XtraEditors.CheckEdit()
        Me.uxAllOrderTab = New DevExpress.XtraTab.XtraTabPage()
        Me.txtOvcNumberAllOrders = New DevExpress.XtraEditors.TextEdit()
        Me.lblOvcNumberAllOrders = New DevExpress.XtraEditors.LabelControl()
        Me.panelAllOrdersCustomerSearch = New System.Windows.Forms.Panel()
        Me.lblPhoneNumberAllOrders = New DevExpress.XtraEditors.LabelControl()
        Me.lblNameAllOrders = New DevExpress.XtraEditors.LabelControl()
        Me.lblPostCodeAllOrders = New DevExpress.XtraEditors.LabelControl()
        Me.txtPhoneNumberAllOrders = New DevExpress.XtraEditors.TextEdit()
        Me.txtNameAllOrders = New DevExpress.XtraEditors.TextEdit()
        Me.txtPostCodeAllOrders = New DevExpress.XtraEditors.TextEdit()
        Me.uxEndDateLabel = New DevExpress.XtraEditors.LabelControl()
        Me.uxEndDate = New DevExpress.XtraEditors.DateEdit()
        Me.uxStartDateLabel = New DevExpress.XtraEditors.LabelControl()
        Me.uxStartDate = New DevExpress.XtraEditors.DateEdit()
        Me.uxAllOrderGrid = New DevExpress.XtraGrid.GridControl()
        Me.uxAllOrderView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.uxFullOrderTab = New DevExpress.XtraTab.XtraTabPage()
        Me.uxFullTillOrderReferenceErrorLabel = New DevExpress.XtraEditors.LabelControl()
        Me.uxFullTillOrderReferenceLabel = New DevExpress.XtraEditors.LabelControl()
        Me.uxFullTillOrderReferenceText = New DevExpress.XtraEditors.TextEdit()
        Me.uxFullStoreOrderErrorLabel = New DevExpress.XtraEditors.LabelControl()
        Me.uxFullStoreIdErrorLabel = New DevExpress.XtraEditors.LabelControl()
        Me.uxFullOmOrderErrorLabel = New DevExpress.XtraEditors.LabelControl()
        Me.uxFullStoreOrderLabel = New DevExpress.XtraEditors.LabelControl()
        Me.uxFullStoreIdLabel = New DevExpress.XtraEditors.LabelControl()
        Me.uxFullOmOrderLabel = New DevExpress.XtraEditors.LabelControl()
        Me.uxFullStoreOrderText = New DevExpress.XtraEditors.TextEdit()
        Me.uxFullStoreIdText = New DevExpress.XtraEditors.TextEdit()
        Me.uxFullOmOrderText = New DevExpress.XtraEditors.TextEdit()
        Me.uxFullOrderGrid = New DevExpress.XtraGrid.GridControl()
        Me.uxFullOrderView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.uxPrintButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxConfirmOrderButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxPrintDespatchButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxDespatchButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxConfirmPickingButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxDeliveredButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxResetButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxRePrintDespatchNote = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStockEnquiry = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.uxOpenOrderGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxOpenOrderView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxOrderTabControl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxOrderTabControl.SuspendLayout()
        Me.uxNonZeroStockTab.SuspendLayout()
        CType(Me.uxNonZeroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxNonZeroView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxOpenOrderTab.SuspendLayout()
        CType(Me.txtOvcNumberOpenOrders.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelOpenOrdersCustomerSearch.SuspendLayout()
        CType(Me.txtTelephoneNumberOpenOrders.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCustomerNameOpenOrders.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPostCodeOpenOrders.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxOMOrderNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxShowFulfilCheck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxAllOrderTab.SuspendLayout()
        CType(Me.txtOvcNumberAllOrders.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelAllOrdersCustomerSearch.SuspendLayout()
        CType(Me.txtPhoneNumberAllOrders.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNameAllOrders.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPostCodeAllOrders.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxEndDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxEndDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxStartDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxStartDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxAllOrderGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxAllOrderView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxFullOrderTab.SuspendLayout()
        CType(Me.uxFullTillOrderReferenceText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxFullStoreOrderText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxFullStoreIdText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxFullOmOrderText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxFullOrderGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxFullOrderView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'uxOpenOrderGrid
        '
        Me.uxOpenOrderGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxOpenOrderGrid.Location = New System.Drawing.Point(3, 89)
        Me.uxOpenOrderGrid.MainView = Me.uxOpenOrderView
        Me.uxOpenOrderGrid.Name = "uxOpenOrderGrid"
        Me.uxOpenOrderGrid.Size = New System.Drawing.Size(942, 360)
        Me.uxOpenOrderGrid.TabIndex = 1
        Me.uxOpenOrderGrid.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uxOpenOrderView})
        '
        'uxOpenOrderView
        '
        Me.uxOpenOrderView.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxOpenOrderView.Appearance.HeaderPanel.Options.UseFont = True
        Me.uxOpenOrderView.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.uxOpenOrderView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uxOpenOrderView.Appearance.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxOpenOrderView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxOpenOrderView.Appearance.Row.Font = New System.Drawing.Font("Tahoma", 7.5!)
        Me.uxOpenOrderView.Appearance.Row.Options.UseFont = True
        Me.uxOpenOrderView.ColumnPanelRowHeight = 35
        Me.uxOpenOrderView.GridControl = Me.uxOpenOrderGrid
        Me.uxOpenOrderView.Name = "uxOpenOrderView"
        Me.uxOpenOrderView.OptionsBehavior.Editable = False
        Me.uxOpenOrderView.OptionsFilter.AllowColumnMRUFilterList = False
        Me.uxOpenOrderView.OptionsFilter.AllowFilterEditor = False
        Me.uxOpenOrderView.OptionsFilter.AllowMRUFilterList = False
        Me.uxOpenOrderView.OptionsPrint.UsePrintStyles = True
        Me.uxOpenOrderView.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.uxOpenOrderView.OptionsSelection.MultiSelect = True
        Me.uxOpenOrderView.OptionsView.ColumnAutoWidth = False
        Me.uxOpenOrderView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.uxOpenOrderView.OptionsView.ShowGroupedColumns = True
        '
        'uxExitButton
        '
        Me.uxExitButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxExitButton.Location = New System.Drawing.Point(885, 492)
        Me.uxExitButton.Name = "uxExitButton"
        Me.uxExitButton.Size = New System.Drawing.Size(75, 39)
        Me.uxExitButton.TabIndex = 14
        Me.uxExitButton.Text = "F12 Exit"
        '
        'uxRetrieveButton
        '
        Me.uxRetrieveButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxRetrieveButton.Appearance.Options.UseTextOptions = True
        Me.uxRetrieveButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxRetrieveButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxRetrieveButton.Location = New System.Drawing.Point(3, 492)
        Me.uxRetrieveButton.Name = "uxRetrieveButton"
        Me.uxRetrieveButton.Size = New System.Drawing.Size(75, 39)
        Me.uxRetrieveButton.TabIndex = 5
        Me.uxRetrieveButton.Text = "F2 Retrieve Orders"
        '
        'uxMaintainButton
        '
        Me.uxMaintainButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxMaintainButton.Appearance.Options.UseTextOptions = True
        Me.uxMaintainButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxMaintainButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxMaintainButton.Location = New System.Drawing.Point(84, 492)
        Me.uxMaintainButton.Name = "uxMaintainButton"
        Me.uxMaintainButton.Size = New System.Drawing.Size(75, 39)
        Me.uxMaintainButton.TabIndex = 6
        Me.uxMaintainButton.Text = "F3 Maintain Order"
        '
        'uxOrderTabControl
        '
        Me.uxOrderTabControl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxOrderTabControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.uxOrderTabControl.Location = New System.Drawing.Point(3, 3)
        Me.uxOrderTabControl.Name = "uxOrderTabControl"
        Me.uxOrderTabControl.SelectedTabPage = Me.uxNonZeroStockTab
        Me.uxOrderTabControl.Size = New System.Drawing.Size(957, 483)
        Me.uxOrderTabControl.TabIndex = 0
        Me.uxOrderTabControl.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.uxOpenOrderTab, Me.uxAllOrderTab, Me.uxFullOrderTab, Me.uxNonZeroStockTab})
        '
        'uxNonZeroStockTab
        '
        Me.uxNonZeroStockTab.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.uxNonZeroStockTab.Controls.Add(Me.uxNonZeroGrid)
        Me.uxNonZeroStockTab.Name = "uxNonZeroStockTab"
        Me.uxNonZeroStockTab.PageVisible = False
        Me.uxNonZeroStockTab.Size = New System.Drawing.Size(948, 452)
        Me.uxNonZeroStockTab.Text = "Non Zero Stock"
        '
        'uxNonZeroGrid
        '
        Me.uxNonZeroGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxNonZeroGrid.Location = New System.Drawing.Point(3, 55)
        Me.uxNonZeroGrid.MainView = Me.uxNonZeroView
        Me.uxNonZeroGrid.Name = "uxNonZeroGrid"
        Me.uxNonZeroGrid.Size = New System.Drawing.Size(942, 394)
        Me.uxNonZeroGrid.TabIndex = 7
        Me.uxNonZeroGrid.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uxNonZeroView})
        '
        'uxNonZeroView
        '
        Me.uxNonZeroView.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxNonZeroView.Appearance.HeaderPanel.Options.UseFont = True
        Me.uxNonZeroView.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.uxNonZeroView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uxNonZeroView.Appearance.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxNonZeroView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxNonZeroView.ColumnPanelRowHeight = 35
        Me.uxNonZeroView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.uxNonZeroView.GridControl = Me.uxNonZeroGrid
        Me.uxNonZeroView.Name = "uxNonZeroView"
        Me.uxNonZeroView.OptionsBehavior.Editable = False
        Me.uxNonZeroView.OptionsDetail.ShowDetailTabs = False
        Me.uxNonZeroView.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.uxNonZeroView.OptionsSelection.MultiSelect = True
        Me.uxNonZeroView.OptionsView.ColumnAutoWidth = False
        Me.uxNonZeroView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        '
        'uxOpenOrderTab
        '
        Me.uxOpenOrderTab.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.uxOpenOrderTab.Controls.Add(Me.txtOvcNumberOpenOrders)
        Me.uxOpenOrderTab.Controls.Add(Me.lblOvcNumberOpenOrders)
        Me.uxOpenOrderTab.Controls.Add(Me.panelOpenOrdersCustomerSearch)
        Me.uxOpenOrderTab.Controls.Add(Me.uxOMOrderNumber)
        Me.uxOpenOrderTab.Controls.Add(Me.lblOMOrderNumber)
        Me.uxOpenOrderTab.Controls.Add(Me.uxOpenOrderGrid)
        Me.uxOpenOrderTab.Controls.Add(Me.uxShowFulfilCheck)
        Me.uxOpenOrderTab.Name = "uxOpenOrderTab"
        Me.uxOpenOrderTab.Size = New System.Drawing.Size(948, 452)
        Me.uxOpenOrderTab.Text = "Open Orders"
        '
        'txtOvcNumberOpenOrders
        '
        Me.txtOvcNumberOpenOrders.Location = New System.Drawing.Point(836, 34)
        Me.txtOvcNumberOpenOrders.Name = "txtOvcNumberOpenOrders"
        Me.txtOvcNumberOpenOrders.Properties.MaxLength = 14
        Me.txtOvcNumberOpenOrders.Size = New System.Drawing.Size(100, 20)
        Me.txtOvcNumberOpenOrders.TabIndex = 4
        '
        'lblOvcNumberOpenOrders
        '
        Me.lblOvcNumberOpenOrders.Location = New System.Drawing.Point(718, 37)
        Me.lblOvcNumberOpenOrders.Name = "lblOvcNumberOpenOrders"
        Me.lblOvcNumberOpenOrders.Size = New System.Drawing.Size(92, 13)
        Me.lblOvcNumberOpenOrders.TabIndex = 5
        Me.lblOvcNumberOpenOrders.Text = "OVC Order Number"
        '
        'panelOpenOrdersCustomerSearch
        '
        Me.panelOpenOrdersCustomerSearch.Controls.Add(Me.lblTelephoneNumberOpenOrders)
        Me.panelOpenOrdersCustomerSearch.Controls.Add(Me.lblCustomerNameOpenOrders)
        Me.panelOpenOrdersCustomerSearch.Controls.Add(Me.lblPostCodeOpenOrders)
        Me.panelOpenOrdersCustomerSearch.Controls.Add(Me.txtTelephoneNumberOpenOrders)
        Me.panelOpenOrdersCustomerSearch.Controls.Add(Me.txtCustomerNameOpenOrders)
        Me.panelOpenOrdersCustomerSearch.Controls.Add(Me.txtPostCodeOpenOrders)
        Me.panelOpenOrdersCustomerSearch.Location = New System.Drawing.Point(350, 0)
        Me.panelOpenOrdersCustomerSearch.Name = "panelOpenOrdersCustomerSearch"
        Me.panelOpenOrdersCustomerSearch.Size = New System.Drawing.Size(350, 85)
        Me.panelOpenOrdersCustomerSearch.TabIndex = 2
        '
        'lblTelephoneNumberOpenOrders
        '
        Me.lblTelephoneNumberOpenOrders.Location = New System.Drawing.Point(28, 63)
        Me.lblTelephoneNumberOpenOrders.Name = "lblTelephoneNumberOpenOrders"
        Me.lblTelephoneNumberOpenOrders.Size = New System.Drawing.Size(30, 13)
        Me.lblTelephoneNumberOpenOrders.TabIndex = 5
        Me.lblTelephoneNumberOpenOrders.Text = "Phone"
        '
        'lblCustomerNameOpenOrders
        '
        Me.lblCustomerNameOpenOrders.Location = New System.Drawing.Point(28, 37)
        Me.lblCustomerNameOpenOrders.Name = "lblCustomerNameOpenOrders"
        Me.lblCustomerNameOpenOrders.Size = New System.Drawing.Size(27, 13)
        Me.lblCustomerNameOpenOrders.TabIndex = 4
        Me.lblCustomerNameOpenOrders.Text = "Name"
        '
        'lblPostCodeOpenOrders
        '
        Me.lblPostCodeOpenOrders.Location = New System.Drawing.Point(28, 11)
        Me.lblPostCodeOpenOrders.Name = "lblPostCodeOpenOrders"
        Me.lblPostCodeOpenOrders.Size = New System.Drawing.Size(49, 13)
        Me.lblPostCodeOpenOrders.TabIndex = 3
        Me.lblPostCodeOpenOrders.Text = "Post Code"
        '
        'txtTelephoneNumberOpenOrders
        '
        Me.txtTelephoneNumberOpenOrders.Location = New System.Drawing.Point(102, 60)
        Me.txtTelephoneNumberOpenOrders.Name = "txtTelephoneNumberOpenOrders"
        Me.txtTelephoneNumberOpenOrders.Properties.MaxLength = 20
        Me.txtTelephoneNumberOpenOrders.Size = New System.Drawing.Size(160, 20)
        Me.txtTelephoneNumberOpenOrders.TabIndex = 2
        '
        'txtCustomerNameOpenOrders
        '
        Me.txtCustomerNameOpenOrders.Location = New System.Drawing.Point(102, 34)
        Me.txtCustomerNameOpenOrders.Name = "txtCustomerNameOpenOrders"
        Me.txtCustomerNameOpenOrders.Properties.MaxLength = 50
        Me.txtCustomerNameOpenOrders.Size = New System.Drawing.Size(227, 20)
        Me.txtCustomerNameOpenOrders.TabIndex = 1
        '
        'txtPostCodeOpenOrders
        '
        Me.txtPostCodeOpenOrders.Location = New System.Drawing.Point(102, 8)
        Me.txtPostCodeOpenOrders.Name = "txtPostCodeOpenOrders"
        Me.txtPostCodeOpenOrders.Properties.MaxLength = 10
        Me.txtPostCodeOpenOrders.Size = New System.Drawing.Size(100, 20)
        Me.txtPostCodeOpenOrders.TabIndex = 0
        '
        'uxOMOrderNumber
        '
        Me.uxOMOrderNumber.Location = New System.Drawing.Point(836, 8)
        Me.uxOMOrderNumber.Name = "uxOMOrderNumber"
        Me.uxOMOrderNumber.Properties.MaxLength = 11
        Me.uxOMOrderNumber.Size = New System.Drawing.Size(100, 20)
        Me.uxOMOrderNumber.TabIndex = 3
        '
        'lblOMOrderNumber
        '
        Me.lblOMOrderNumber.Location = New System.Drawing.Point(718, 11)
        Me.lblOMOrderNumber.Name = "lblOMOrderNumber"
        Me.lblOMOrderNumber.Size = New System.Drawing.Size(100, 13)
        Me.lblOMOrderNumber.TabIndex = 2
        Me.lblOMOrderNumber.Text = "OM Order Reference"
        '
        'uxShowFulfilCheck
        '
        Me.uxShowFulfilCheck.Location = New System.Drawing.Point(3, 6)
        Me.uxShowFulfilCheck.Name = "uxShowFulfilCheck"
        Me.uxShowFulfilCheck.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uxShowFulfilCheck.Properties.Appearance.Options.UseFont = True
        Me.uxShowFulfilCheck.Properties.Caption = "Only show orders and lines fulfilled by this store"
        Me.uxShowFulfilCheck.Size = New System.Drawing.Size(341, 21)
        Me.uxShowFulfilCheck.TabIndex = 0
        '
        'uxAllOrderTab
        '
        Me.uxAllOrderTab.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.uxAllOrderTab.Controls.Add(Me.txtOvcNumberAllOrders)
        Me.uxAllOrderTab.Controls.Add(Me.lblOvcNumberAllOrders)
        Me.uxAllOrderTab.Controls.Add(Me.panelAllOrdersCustomerSearch)
        Me.uxAllOrderTab.Controls.Add(Me.uxEndDateLabel)
        Me.uxAllOrderTab.Controls.Add(Me.uxEndDate)
        Me.uxAllOrderTab.Controls.Add(Me.uxStartDateLabel)
        Me.uxAllOrderTab.Controls.Add(Me.uxStartDate)
        Me.uxAllOrderTab.Controls.Add(Me.uxAllOrderGrid)
        Me.uxAllOrderTab.Name = "uxAllOrderTab"
        Me.uxAllOrderTab.Size = New System.Drawing.Size(948, 452)
        Me.uxAllOrderTab.Text = "All Orders"
        '
        'txtOvcNumberAllOrders
        '
        Me.txtOvcNumberAllOrders.Location = New System.Drawing.Point(121, 60)
        Me.txtOvcNumberAllOrders.Name = "txtOvcNumberAllOrders"
        Me.txtOvcNumberAllOrders.Properties.MaxLength = 14
        Me.txtOvcNumberAllOrders.Size = New System.Drawing.Size(100, 20)
        Me.txtOvcNumberAllOrders.TabIndex = 5
        '
        'lblOvcNumberAllOrders
        '
        Me.lblOvcNumberAllOrders.Location = New System.Drawing.Point(6, 63)
        Me.lblOvcNumberAllOrders.Name = "lblOvcNumberAllOrders"
        Me.lblOvcNumberAllOrders.Size = New System.Drawing.Size(92, 13)
        Me.lblOvcNumberAllOrders.TabIndex = 8
        Me.lblOvcNumberAllOrders.Text = "OVC Order Number"
        '
        'panelAllOrdersCustomerSearch
        '
        Me.panelAllOrdersCustomerSearch.Controls.Add(Me.lblPhoneNumberAllOrders)
        Me.panelAllOrdersCustomerSearch.Controls.Add(Me.lblNameAllOrders)
        Me.panelAllOrdersCustomerSearch.Controls.Add(Me.lblPostCodeAllOrders)
        Me.panelAllOrdersCustomerSearch.Controls.Add(Me.txtPhoneNumberAllOrders)
        Me.panelAllOrdersCustomerSearch.Controls.Add(Me.txtNameAllOrders)
        Me.panelAllOrdersCustomerSearch.Controls.Add(Me.txtPostCodeAllOrders)
        Me.panelAllOrdersCustomerSearch.Location = New System.Drawing.Point(350, 0)
        Me.panelAllOrdersCustomerSearch.Name = "panelAllOrdersCustomerSearch"
        Me.panelAllOrdersCustomerSearch.Size = New System.Drawing.Size(350, 85)
        Me.panelAllOrdersCustomerSearch.TabIndex = 2
        '
        'lblPhoneNumberAllOrders
        '
        Me.lblPhoneNumberAllOrders.Location = New System.Drawing.Point(28, 63)
        Me.lblPhoneNumberAllOrders.Name = "lblPhoneNumberAllOrders"
        Me.lblPhoneNumberAllOrders.Size = New System.Drawing.Size(30, 13)
        Me.lblPhoneNumberAllOrders.TabIndex = 5
        Me.lblPhoneNumberAllOrders.Text = "Phone"
        '
        'lblNameAllOrders
        '
        Me.lblNameAllOrders.Location = New System.Drawing.Point(28, 37)
        Me.lblNameAllOrders.Name = "lblNameAllOrders"
        Me.lblNameAllOrders.Size = New System.Drawing.Size(27, 13)
        Me.lblNameAllOrders.TabIndex = 4
        Me.lblNameAllOrders.Text = "Name"
        '
        'lblPostCodeAllOrders
        '
        Me.lblPostCodeAllOrders.Location = New System.Drawing.Point(28, 11)
        Me.lblPostCodeAllOrders.Name = "lblPostCodeAllOrders"
        Me.lblPostCodeAllOrders.Size = New System.Drawing.Size(49, 13)
        Me.lblPostCodeAllOrders.TabIndex = 3
        Me.lblPostCodeAllOrders.Text = "Post Code"
        '
        'txtPhoneNumberAllOrders
        '
        Me.txtPhoneNumberAllOrders.Location = New System.Drawing.Point(102, 60)
        Me.txtPhoneNumberAllOrders.Name = "txtPhoneNumberAllOrders"
        Me.txtPhoneNumberAllOrders.Properties.MaxLength = 20
        Me.txtPhoneNumberAllOrders.Size = New System.Drawing.Size(160, 20)
        Me.txtPhoneNumberAllOrders.TabIndex = 2
        '
        'txtNameAllOrders
        '
        Me.txtNameAllOrders.Location = New System.Drawing.Point(102, 34)
        Me.txtNameAllOrders.Name = "txtNameAllOrders"
        Me.txtNameAllOrders.Properties.MaxLength = 50
        Me.txtNameAllOrders.Size = New System.Drawing.Size(227, 20)
        Me.txtNameAllOrders.TabIndex = 1
        '
        'txtPostCodeAllOrders
        '
        Me.txtPostCodeAllOrders.Location = New System.Drawing.Point(102, 8)
        Me.txtPostCodeAllOrders.Name = "txtPostCodeAllOrders"
        Me.txtPostCodeAllOrders.Properties.MaxLength = 10
        Me.txtPostCodeAllOrders.Size = New System.Drawing.Size(100, 20)
        Me.txtPostCodeAllOrders.TabIndex = 0
        '
        'uxEndDateLabel
        '
        Me.uxEndDateLabel.Location = New System.Drawing.Point(6, 37)
        Me.uxEndDateLabel.Name = "uxEndDateLabel"
        Me.uxEndDateLabel.Size = New System.Drawing.Size(75, 13)
        Me.uxEndDateLabel.TabIndex = 5
        Me.uxEndDateLabel.Text = "Order End Date"
        '
        'uxEndDate
        '
        Me.uxEndDate.EditValue = Nothing
        Me.uxEndDate.Location = New System.Drawing.Point(121, 34)
        Me.uxEndDate.Name = "uxEndDate"
        Me.uxEndDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.uxEndDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.uxEndDate.Size = New System.Drawing.Size(100, 20)
        Me.uxEndDate.TabIndex = 4
        '
        'uxStartDateLabel
        '
        Me.uxStartDateLabel.Location = New System.Drawing.Point(6, 11)
        Me.uxStartDateLabel.Name = "uxStartDateLabel"
        Me.uxStartDateLabel.Size = New System.Drawing.Size(81, 13)
        Me.uxStartDateLabel.TabIndex = 3
        Me.uxStartDateLabel.Text = "Order Start Date"
        '
        'uxStartDate
        '
        Me.uxStartDate.EditValue = Nothing
        Me.uxStartDate.Location = New System.Drawing.Point(121, 8)
        Me.uxStartDate.Name = "uxStartDate"
        Me.uxStartDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.uxStartDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.uxStartDate.Size = New System.Drawing.Size(100, 20)
        Me.uxStartDate.TabIndex = 3
        '
        'uxAllOrderGrid
        '
        Me.uxAllOrderGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxAllOrderGrid.Location = New System.Drawing.Point(3, 89)
        Me.uxAllOrderGrid.MainView = Me.uxAllOrderView
        Me.uxAllOrderGrid.Name = "uxAllOrderGrid"
        Me.uxAllOrderGrid.Size = New System.Drawing.Size(942, 360)
        Me.uxAllOrderGrid.TabIndex = 1
        Me.uxAllOrderGrid.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uxAllOrderView})
        '
        'uxAllOrderView
        '
        Me.uxAllOrderView.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxAllOrderView.Appearance.HeaderPanel.Options.UseFont = True
        Me.uxAllOrderView.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.uxAllOrderView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uxAllOrderView.Appearance.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxAllOrderView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxAllOrderView.ColumnPanelRowHeight = 35
        Me.uxAllOrderView.GridControl = Me.uxAllOrderGrid
        Me.uxAllOrderView.Name = "uxAllOrderView"
        Me.uxAllOrderView.OptionsBehavior.Editable = False
        Me.uxAllOrderView.OptionsFilter.AllowColumnMRUFilterList = False
        Me.uxAllOrderView.OptionsFilter.AllowFilterEditor = False
        Me.uxAllOrderView.OptionsFilter.AllowMRUFilterList = False
        Me.uxAllOrderView.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.uxAllOrderView.OptionsView.ColumnAutoWidth = False
        Me.uxAllOrderView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        '
        'uxFullOrderTab
        '
        Me.uxFullOrderTab.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.uxFullOrderTab.Controls.Add(Me.uxFullTillOrderReferenceErrorLabel)
        Me.uxFullOrderTab.Controls.Add(Me.uxFullTillOrderReferenceLabel)
        Me.uxFullOrderTab.Controls.Add(Me.uxFullTillOrderReferenceText)
        Me.uxFullOrderTab.Controls.Add(Me.uxFullStoreOrderErrorLabel)
        Me.uxFullOrderTab.Controls.Add(Me.uxFullStoreIdErrorLabel)
        Me.uxFullOrderTab.Controls.Add(Me.uxFullOmOrderErrorLabel)
        Me.uxFullOrderTab.Controls.Add(Me.uxFullStoreOrderLabel)
        Me.uxFullOrderTab.Controls.Add(Me.uxFullStoreIdLabel)
        Me.uxFullOrderTab.Controls.Add(Me.uxFullOmOrderLabel)
        Me.uxFullOrderTab.Controls.Add(Me.uxFullStoreOrderText)
        Me.uxFullOrderTab.Controls.Add(Me.uxFullStoreIdText)
        Me.uxFullOrderTab.Controls.Add(Me.uxFullOmOrderText)
        Me.uxFullOrderTab.Controls.Add(Me.uxFullOrderGrid)
        Me.uxFullOrderTab.Name = "uxFullOrderTab"
        Me.uxFullOrderTab.Size = New System.Drawing.Size(948, 452)
        Me.uxFullOrderTab.Text = "OM Enquiry"
        '
        'uxFullTillOrderReferenceErrorLabel
        '
        Me.uxFullTillOrderReferenceErrorLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.uxFullTillOrderReferenceErrorLabel.Location = New System.Drawing.Point(262, 72)
        Me.uxFullTillOrderReferenceErrorLabel.Name = "uxFullTillOrderReferenceErrorLabel"
        Me.uxFullTillOrderReferenceErrorLabel.Size = New System.Drawing.Size(316, 20)
        Me.uxFullTillOrderReferenceErrorLabel.TabIndex = 12
        '
        'uxFullTillOrderReferenceLabel
        '
        Me.uxFullTillOrderReferenceLabel.Location = New System.Drawing.Point(6, 75)
        Me.uxFullTillOrderReferenceLabel.Name = "uxFullTillOrderReferenceLabel"
        Me.uxFullTillOrderReferenceLabel.Size = New System.Drawing.Size(92, 13)
        Me.uxFullTillOrderReferenceLabel.TabIndex = 10
        Me.uxFullTillOrderReferenceLabel.Text = "OVC Order Number"
        '
        'uxFullTillOrderReferenceText
        '
        Me.uxFullTillOrderReferenceText.Location = New System.Drawing.Point(159, 72)
        Me.uxFullTillOrderReferenceText.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.uxFullTillOrderReferenceText.Name = "uxFullTillOrderReferenceText"
        Me.uxFullTillOrderReferenceText.Properties.Mask.EditMask = "\d{0,14}"
        Me.uxFullTillOrderReferenceText.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uxFullTillOrderReferenceText.Properties.Mask.ShowPlaceHolders = False
        Me.uxFullTillOrderReferenceText.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.uxFullTillOrderReferenceText.Size = New System.Drawing.Size(97, 20)
        Me.uxFullTillOrderReferenceText.TabIndex = 4
        '
        'uxFullStoreOrderErrorLabel
        '
        Me.uxFullStoreOrderErrorLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.uxFullStoreOrderErrorLabel.Location = New System.Drawing.Point(262, 49)
        Me.uxFullStoreOrderErrorLabel.Name = "uxFullStoreOrderErrorLabel"
        Me.uxFullStoreOrderErrorLabel.Size = New System.Drawing.Size(316, 20)
        Me.uxFullStoreOrderErrorLabel.TabIndex = 9
        '
        'uxFullStoreIdErrorLabel
        '
        Me.uxFullStoreIdErrorLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.uxFullStoreIdErrorLabel.Location = New System.Drawing.Point(262, 26)
        Me.uxFullStoreIdErrorLabel.Name = "uxFullStoreIdErrorLabel"
        Me.uxFullStoreIdErrorLabel.Size = New System.Drawing.Size(316, 20)
        Me.uxFullStoreIdErrorLabel.TabIndex = 8
        '
        'uxFullOmOrderErrorLabel
        '
        Me.uxFullOmOrderErrorLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.uxFullOmOrderErrorLabel.Location = New System.Drawing.Point(262, 3)
        Me.uxFullOmOrderErrorLabel.Name = "uxFullOmOrderErrorLabel"
        Me.uxFullOmOrderErrorLabel.Size = New System.Drawing.Size(340, 20)
        Me.uxFullOmOrderErrorLabel.TabIndex = 7
        '
        'uxFullStoreOrderLabel
        '
        Me.uxFullStoreOrderLabel.Location = New System.Drawing.Point(6, 52)
        Me.uxFullStoreOrderLabel.Name = "uxFullStoreOrderLabel"
        Me.uxFullStoreOrderLabel.Size = New System.Drawing.Size(130, 13)
        Me.uxFullStoreOrderLabel.TabIndex = 4
        Me.uxFullStoreOrderLabel.Text = "Selling Store Order &Number"
        '
        'uxFullStoreIdLabel
        '
        Me.uxFullStoreIdLabel.Location = New System.Drawing.Point(6, 29)
        Me.uxFullStoreIdLabel.Name = "uxFullStoreIdLabel"
        Me.uxFullStoreIdLabel.Size = New System.Drawing.Size(73, 13)
        Me.uxFullStoreIdLabel.TabIndex = 2
        Me.uxFullStoreIdLabel.Text = "Selling Store &ID"
        '
        'uxFullOmOrderLabel
        '
        Me.uxFullOmOrderLabel.Location = New System.Drawing.Point(6, 6)
        Me.uxFullOmOrderLabel.Name = "uxFullOmOrderLabel"
        Me.uxFullOmOrderLabel.Size = New System.Drawing.Size(100, 13)
        Me.uxFullOmOrderLabel.TabIndex = 0
        Me.uxFullOmOrderLabel.Text = "&OM Order Reference"
        '
        'uxFullStoreOrderText
        '
        Me.uxFullStoreOrderText.Location = New System.Drawing.Point(159, 49)
        Me.uxFullStoreOrderText.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.uxFullStoreOrderText.Name = "uxFullStoreOrderText"
        Me.uxFullStoreOrderText.Properties.Mask.EditMask = "\d{0,6}"
        Me.uxFullStoreOrderText.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uxFullStoreOrderText.Properties.Mask.ShowPlaceHolders = False
        Me.uxFullStoreOrderText.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.uxFullStoreOrderText.Size = New System.Drawing.Size(97, 20)
        Me.uxFullStoreOrderText.TabIndex = 3
        '
        'uxFullStoreIdText
        '
        Me.uxFullStoreIdText.Location = New System.Drawing.Point(159, 26)
        Me.uxFullStoreIdText.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.uxFullStoreIdText.Name = "uxFullStoreIdText"
        Me.uxFullStoreIdText.Properties.Mask.EditMask = "\d{0,4}"
        Me.uxFullStoreIdText.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uxFullStoreIdText.Properties.Mask.ShowPlaceHolders = False
        Me.uxFullStoreIdText.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.uxFullStoreIdText.Size = New System.Drawing.Size(97, 20)
        Me.uxFullStoreIdText.TabIndex = 2
        '
        'uxFullOmOrderText
        '
        Me.uxFullOmOrderText.Location = New System.Drawing.Point(159, 3)
        Me.uxFullOmOrderText.Name = "uxFullOmOrderText"
        Me.uxFullOmOrderText.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.uxFullOmOrderText.Properties.Appearance.Options.UseBackColor = True
        Me.uxFullOmOrderText.Properties.Mask.EditMask = "\d{0,9}"
        Me.uxFullOmOrderText.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uxFullOmOrderText.Properties.Mask.SaveLiteral = False
        Me.uxFullOmOrderText.Properties.Mask.ShowPlaceHolders = False
        Me.uxFullOmOrderText.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.uxFullOmOrderText.Size = New System.Drawing.Size(97, 20)
        Me.uxFullOmOrderText.TabIndex = 1
        '
        'uxFullOrderGrid
        '
        Me.uxFullOrderGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxFullOrderGrid.Location = New System.Drawing.Point(3, 98)
        Me.uxFullOrderGrid.MainView = Me.uxFullOrderView
        Me.uxFullOrderGrid.Name = "uxFullOrderGrid"
        Me.uxFullOrderGrid.Size = New System.Drawing.Size(942, 351)
        Me.uxFullOrderGrid.TabIndex = 6
        Me.uxFullOrderGrid.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uxFullOrderView})
        '
        'uxFullOrderView
        '
        Me.uxFullOrderView.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxFullOrderView.Appearance.HeaderPanel.Options.UseFont = True
        Me.uxFullOrderView.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.uxFullOrderView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uxFullOrderView.Appearance.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxFullOrderView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxFullOrderView.ColumnPanelRowHeight = 35
        Me.uxFullOrderView.GridControl = Me.uxFullOrderGrid
        Me.uxFullOrderView.Name = "uxFullOrderView"
        Me.uxFullOrderView.OptionsBehavior.Editable = False
        Me.uxFullOrderView.OptionsFilter.AllowColumnMRUFilterList = False
        Me.uxFullOrderView.OptionsFilter.AllowFilterEditor = False
        Me.uxFullOrderView.OptionsFilter.AllowMRUFilterList = False
        Me.uxFullOrderView.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.uxFullOrderView.OptionsView.ColumnAutoWidth = False
        Me.uxFullOrderView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        '
        'uxPrintButton
        '
        Me.uxPrintButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxPrintButton.Location = New System.Drawing.Point(723, 492)
        Me.uxPrintButton.Name = "uxPrintButton"
        Me.uxPrintButton.Size = New System.Drawing.Size(75, 39)
        Me.uxPrintButton.TabIndex = 12
        Me.uxPrintButton.Text = "F9 Print"
        '
        'uxConfirmOrderButton
        '
        Me.uxConfirmOrderButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxConfirmOrderButton.Appearance.Options.UseTextOptions = True
        Me.uxConfirmOrderButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxConfirmOrderButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxConfirmOrderButton.Enabled = False
        Me.uxConfirmOrderButton.Location = New System.Drawing.Point(165, 492)
        Me.uxConfirmOrderButton.Name = "uxConfirmOrderButton"
        Me.uxConfirmOrderButton.Size = New System.Drawing.Size(97, 39)
        Me.uxConfirmOrderButton.TabIndex = 3
        Me.uxConfirmOrderButton.Text = "F4 Confirm New Fulfilment"
        Me.uxConfirmOrderButton.Visible = False
        '
        'uxPrintDespatchButton
        '
        Me.uxPrintDespatchButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxPrintDespatchButton.Appearance.Options.UseTextOptions = True
        Me.uxPrintDespatchButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxPrintDespatchButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxPrintDespatchButton.Location = New System.Drawing.Point(165, 492)
        Me.uxPrintDespatchButton.Name = "uxPrintDespatchButton"
        Me.uxPrintDespatchButton.Size = New System.Drawing.Size(97, 39)
        Me.uxPrintDespatchButton.TabIndex = 7
        Me.uxPrintDespatchButton.Text = "F5 Print Pick Despatch Note"
        '
        'uxDespatchButton
        '
        Me.uxDespatchButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxDespatchButton.Appearance.Options.UseTextOptions = True
        Me.uxDespatchButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxDespatchButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxDespatchButton.Location = New System.Drawing.Point(349, 492)
        Me.uxDespatchButton.Name = "uxDespatchButton"
        Me.uxDespatchButton.Size = New System.Drawing.Size(75, 39)
        Me.uxDespatchButton.TabIndex = 5
        Me.uxDespatchButton.Text = "F7 Despatch Order"
        '
        'uxConfirmPickingButton
        '
        Me.uxConfirmPickingButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxConfirmPickingButton.Appearance.Options.UseTextOptions = True
        Me.uxConfirmPickingButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxConfirmPickingButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxConfirmPickingButton.Location = New System.Drawing.Point(268, 492)
        Me.uxConfirmPickingButton.Name = "uxConfirmPickingButton"
        Me.uxConfirmPickingButton.Size = New System.Drawing.Size(75, 39)
        Me.uxConfirmPickingButton.TabIndex = 8
        Me.uxConfirmPickingButton.Text = "F6 Confirm Picking"
        '
        'uxDeliveredButton
        '
        Me.uxDeliveredButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxDeliveredButton.Appearance.Options.UseTextOptions = True
        Me.uxDeliveredButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxDeliveredButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxDeliveredButton.Location = New System.Drawing.Point(430, 492)
        Me.uxDeliveredButton.Name = "uxDeliveredButton"
        Me.uxDeliveredButton.Size = New System.Drawing.Size(75, 39)
        Me.uxDeliveredButton.TabIndex = 10
        Me.uxDeliveredButton.Text = "F8 Confirm or Fail Delivery"
        '
        'uxResetButton
        '
        Me.uxResetButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxResetButton.Location = New System.Drawing.Point(804, 492)
        Me.uxResetButton.Name = "uxResetButton"
        Me.uxResetButton.Size = New System.Drawing.Size(75, 39)
        Me.uxResetButton.TabIndex = 13
        Me.uxResetButton.Text = "F11 Reset"
        '
        'uxRePrintDespatchNote
        '
        Me.uxRePrintDespatchNote.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxRePrintDespatchNote.Appearance.Options.UseTextOptions = True
        Me.uxRePrintDespatchNote.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxRePrintDespatchNote.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxRePrintDespatchNote.Location = New System.Drawing.Point(349, 492)
        Me.uxRePrintDespatchNote.Name = "uxRePrintDespatchNote"
        Me.uxRePrintDespatchNote.Size = New System.Drawing.Size(75, 39)
        Me.uxRePrintDespatchNote.TabIndex = 9
        Me.uxRePrintDespatchNote.Text = "F7 Re-Print Despatch Note"
        '
        'btnStockEnquiry
        '
        Me.btnStockEnquiry.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStockEnquiry.Appearance.Options.UseTextOptions = True
        Me.btnStockEnquiry.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.btnStockEnquiry.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btnStockEnquiry.Location = New System.Drawing.Point(642, 492)
        Me.btnStockEnquiry.Name = "btnStockEnquiry"
        Me.btnStockEnquiry.Size = New System.Drawing.Size(75, 39)
        Me.btnStockEnquiry.TabIndex = 11
        Me.btnStockEnquiry.Text = "F4 Stock Enquiry"
        Me.btnStockEnquiry.Visible = False
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Controls.Add(Me.btnStockEnquiry)
        Me.Controls.Add(Me.uxRePrintDespatchNote)
        Me.Controls.Add(Me.uxResetButton)
        Me.Controls.Add(Me.uxDeliveredButton)
        Me.Controls.Add(Me.uxConfirmPickingButton)
        Me.Controls.Add(Me.uxDespatchButton)
        Me.Controls.Add(Me.uxPrintDespatchButton)
        Me.Controls.Add(Me.uxConfirmOrderButton)
        Me.Controls.Add(Me.uxPrintButton)
        Me.Controls.Add(Me.uxOrderTabControl)
        Me.Controls.Add(Me.uxMaintainButton)
        Me.Controls.Add(Me.uxRetrieveButton)
        Me.Controls.Add(Me.uxExitButton)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "MainForm"
        Me.Size = New System.Drawing.Size(963, 534)
        CType(Me.uxOpenOrderGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxOpenOrderView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxOrderTabControl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxOrderTabControl.ResumeLayout(False)
        Me.uxNonZeroStockTab.ResumeLayout(False)
        CType(Me.uxNonZeroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxNonZeroView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxOpenOrderTab.ResumeLayout(False)
        Me.uxOpenOrderTab.PerformLayout()
        CType(Me.txtOvcNumberOpenOrders.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelOpenOrdersCustomerSearch.ResumeLayout(False)
        Me.panelOpenOrdersCustomerSearch.PerformLayout()
        CType(Me.txtTelephoneNumberOpenOrders.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCustomerNameOpenOrders.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPostCodeOpenOrders.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxOMOrderNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxShowFulfilCheck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxAllOrderTab.ResumeLayout(False)
        Me.uxAllOrderTab.PerformLayout()
        CType(Me.txtOvcNumberAllOrders.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelAllOrdersCustomerSearch.ResumeLayout(False)
        Me.panelAllOrdersCustomerSearch.PerformLayout()
        CType(Me.txtPhoneNumberAllOrders.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNameAllOrders.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPostCodeAllOrders.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxEndDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxEndDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxStartDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxStartDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxAllOrderGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxAllOrderView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxFullOrderTab.ResumeLayout(False)
        Me.uxFullOrderTab.PerformLayout()
        CType(Me.uxFullTillOrderReferenceText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxFullStoreOrderText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxFullStoreIdText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxFullOmOrderText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxFullOrderGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxFullOrderView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents uxOpenOrderGrid As DevExpress.XtraGrid.GridControl
    Friend WithEvents uxOpenOrderView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents uxExitButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxMaintainButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxOpenOrderTab As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uxAllOrderGrid As DevExpress.XtraGrid.GridControl
    Friend WithEvents uxAllOrderView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents uxEndDateLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxStartDateLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxPrintButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxConfirmOrderButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxPrintDespatchButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxDespatchButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxConfirmPickingButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxDeliveredButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxShowFulfilCheck As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents uxFullOrderTab As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uxFullOrderGrid As DevExpress.XtraGrid.GridControl
    Friend WithEvents uxFullOrderView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents uxFullOmOrderLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxFullStoreOrderText As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uxFullStoreIdText As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uxFullOmOrderText As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uxFullStoreOrderLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxFullStoreIdLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxResetButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxFullStoreOrderErrorLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxFullStoreIdErrorLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxFullOmOrderErrorLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxNonZeroStockTab As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uxNonZeroGrid As DevExpress.XtraGrid.GridControl
    Friend WithEvents uxNonZeroView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents uxRePrintDespatchNote As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxOMOrderNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblOMOrderNumber As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnStockEnquiry As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents panelOpenOrdersCustomerSearch As System.Windows.Forms.Panel
    Friend WithEvents txtTelephoneNumberOpenOrders As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCustomerNameOpenOrders As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtPostCodeOpenOrders As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTelephoneNumberOpenOrders As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblCustomerNameOpenOrders As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblPostCodeOpenOrders As DevExpress.XtraEditors.LabelControl
    Friend WithEvents panelAllOrdersCustomerSearch As System.Windows.Forms.Panel
    Friend WithEvents lblPhoneNumberAllOrders As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblNameAllOrders As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblPostCodeAllOrders As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtPhoneNumberAllOrders As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtPostCodeAllOrders As DevExpress.XtraEditors.TextEdit
    Public WithEvents uxEndDate As DevExpress.XtraEditors.DateEdit
    Public WithEvents uxStartDate As DevExpress.XtraEditors.DateEdit
    Public WithEvents uxRetrieveButton As DevExpress.XtraEditors.SimpleButton
    Public WithEvents uxAllOrderTab As DevExpress.XtraTab.XtraTabPage
    Public WithEvents txtNameAllOrders As DevExpress.XtraEditors.TextEdit
    Public WithEvents uxOrderTabControl As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents uxFullTillOrderReferenceErrorLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxFullTillOrderReferenceLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxFullTillOrderReferenceText As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtOvcNumberOpenOrders As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblOvcNumberOpenOrders As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblOvcNumberAllOrders As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtOvcNumberAllOrders As DevExpress.XtraEditors.TextEdit

End Class
