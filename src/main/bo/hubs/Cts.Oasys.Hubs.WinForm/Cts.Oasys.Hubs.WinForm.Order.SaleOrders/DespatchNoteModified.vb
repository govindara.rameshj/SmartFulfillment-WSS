Imports Cts.Oasys.Hubs.Core
Imports Cts.Oasys.Hubs.Core.Order
Imports Cts.Oasys.Hubs.Core.Order.Qod
Imports Cts.Oasys.Hubs.Core.Order.Qod.State
Imports Cts.Oasys.Hubs.Core.System.Store
Imports Cts.Oasys.Hubs.WinForm.Order.SaleOrders.Factories
Imports Cts.Oasys.Hubs.WinForm.Order.SaleOrders.Interfaces
Imports System.Data

Public Class DespatchNoteModified
    Implements IPickNoteForm

    Private pickNoteInstance As IPickNote = (New PickNoteFactory).GetImplementation()
    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal qod As QodHeader, ByVal sellingStore As Store, ByVal fulfillStoreId As Integer)
        InitializeComponent()
        Dim TotalWeight As Decimal = 0
        Dim TotalItems As Integer = 0

        'add header details
        xrSellingStoreId.Text = qod.SellingStoreId.ToString("0000")
        xrSellingStoreOrderId.Text = CStr(IIf(String.IsNullOrEmpty(qod.OvcOrderNumber), qod.SellingStoreOrderId.ToString("000000"), qod.OvcOrderNumber))
        xrStoreId.Text = fulfillStoreId.ToString("0000")
        xrOrderNumber.Text = qod.Number
        'Check if this is a delivery or a collection
        If qod.IsForDelivery Then
            XrLabelCollectionId.Text = "Delivery Id:"
            XrDelColNew.Text = "Delivery Date:"
        End If
        xrOmNumber.Text = qod.OmOrderNumber.ToString()

        'add customer details
        xrCustName.Text = qod.CustomerName
        xrCustAddress.Text = ConcetenateNewLines(New String() {qod.DeliveryAddress1, qod.DeliveryAddress2, qod.DeliveryAddress3, qod.DeliveryAddress4, qod.DeliveryPostCode})
        xrCustPhone.Text = qod.PhoneNumber
        xrCustPhoneMobile.Text = qod.PhoneNumberMobile
        xrCustPhoneWork.Text = qod.PhoneNumberWork
        xrCustEmail.Text = qod.CustomerEmail

        xrDateOrder.Text = qod.DateOrder.ToShortDateString
        If qod.DateDelivery.HasValue Then
            xrDateDelivery.Text = qod.DateDelivery.Value.ToShortDateString
            xrDateDeliveryDay.Text = qod.DateDelivery.Value.DayOfWeek.ToString
        End If
        ' Change request 41/User Story 2109 - account for refunds in delivery & order values
        Dim despatchNote2109 As IDespatchNote2109 = FactoryDespatchNote2109.FactoryGet()

        'xrOrderValue.Text = qod.MerchandiseValue.ToString("c2")
        xrOrderValue.Text = despatchNote2109.GetOrderValue(qod).ToString("c2")
        ' End of Change request 41/User Story 2109


        'add store info and rest of order totals
        xrStoreName.Text = sellingStore.Address1
        xrStoreAddr1.Text = sellingStore.Address2
        xrStoreAddr2.Text = sellingStore.Address3
        xrStoreAddr3.Text = sellingStore.Address4
        xrStorePhone.Text = sellingStore.PhoneNumber
        xrStoreFax.Text = sellingStore.FaxNumber
        xrDatePrinted.Text = Format(Now, "dd/MM/yyyy")
        ' Change request 41/User Story 2109 - account for refunds in delivery & order values
        'xrDeliveryCharge.Text = qod.DeliveryCharge.ToString("c2")
        xrDeliveryCharge.Text = despatchNote2109.GetDeliveryCharge(qod).ToString("c2")
        xrTotalCost.Text = despatchNote2109.GetOrderValue(qod).ToString("c2")
        ' End of Change request 41/User Story 2109

        ' Merge lines for same SKUs and sort them for printing
        Dim PrintLineList As New QodPrintLineList
        For Each QodLine As QodLine In qod.Lines
            If (Not QodLine.IsDeliveryChargeItem) And QodLine.DeliverySource = fulfillStoreId.ToString("0000") And _
            QodLine.QtyToDeliver > 0 Then
                PrintLineList.Add(QodLine)
                TotalWeight += (QodLine.QtyToDeliver * QodLine.Weight)
                TotalItems += QodLine.QtyToDeliver
            End If
        Next


        For Each QodLine As QodPrintLine In PrintLineList
            QodLine.Volume = pickNoteInstance.UpdateVolume(QodLine.Quantity, QodLine.Weight)
        Next


        SameStoreDeliveryReport.DataSource = PrintLineList
        xrSkuNumber.DataBindings.Add("Text", PrintLineList, GetPropertyName(Function(f As QodPrintLine) f.SkuNumberDisplay))
        xrSkuDescription.DataBindings.Add("Text", PrintLineList, GetPropertyName(Function(f As QodPrintLine) f.Description))
        xrSkuQty.DataBindings.Add("Text", PrintLineList, GetPropertyName(Function(f As QodPrintLine) f.QuantityDisplay))
        XrSkuPicked.DataBindings.Add("Text", PrintLineList, GetPropertyName(Function(f As QodPrintLine) f.DisplayQtyPicked))
        xrSkuPrice.DataBindings.Add("Text", PrintLineList, GetPropertyName(Function(f As QodPrintLine) f.PriceDisplay))
        xrSkuWeight.DataBindings.Add("Text", PrintLineList, GetPropertyName(Function(f As QodPrintLine) f.WeightDisplay))
        xrSkuVolume.DataBindings.Add("Text", PrintLineList, GetPropertyName(Function(f As QodPrintLine) f.VolumeDisplay))

        Dim factory As IPickInstructions = (New PickInstructionsFactory).GetImplementation
        XrPanelPickInstructions.Visible = factory.SetControlsVisibleProperty

        'add delivery instructions and order totals
        xrInstructions.Text = factory.GetDeliveryInstructions(qod)
        XrPickInstructions.Text = factory.GetPickingInstructions(qod)
        XrPickInstructions.Font = New Font("Arial", 7)
        XrPanelPickInstructions.Visible = factory.HasPickingInstructionsAnyText(XrPickInstructions.Text)
        If XrPanelPickInstructions.Visible = False Then
            XrPanelPickInstructions.Top = 0 - XrPanelPickInstructions.Height
        End If
        ReportFooterPickInstruction.Height = factory.GetFooterHeight(XrPickInstructions.Text)
        ReportFooterPickInstruction.PrintAtBottom = True
        ReportFooterPickInstruction.PerformLayout()

        'add watermark if reprint
        If qod.RevisionNumber > 0 Then
            xrRevisionNumber.Text = qod.RevisionNumber.ToString("n0")
            xrRevisionNumber.Visible = True
            xrRevisionNumberLabel.Visible = True

            Watermark.Text = "REVISION " & qod.RevisionNumber
            Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.Vertical
            Watermark.Font = New Font(Me.Watermark.Font.FontFamily, 40)
            Watermark.ForeColor = Color.Black
            Watermark.TextTransparency = pickNoteInstance.SetWaterMarkTransparency
            Watermark.ShowBehind = True
            Watermark.PageRange = "1-5"
        End If

        'Add barcode info
        xrOrderBarCode.Text = Microsoft.VisualBasic.Right(qod.SellingStoreId.ToString("000"), 3) & _
        qod.TranTill & qod.TranNumber & Microsoft.VisualBasic.Format(qod.TranDate, "ddMMyy")
        If xrOrderBarCode.Text.Length < 15 Then 'Some info is missing - may be an old order, so don't print the barcode
            xrOrderBarCode.Visible = False
        End If

        XrItemsTotal.Text = TotalItems.ToString
        If Cts.Oasys.Hubs.Core.System.Parameter.GetBoolean(-922) Then
            XrTotalWeight.Text = TotalWeight.ToString + " kg"
        Else
            XrTotalWeight.Visible = False
            XrWeightLabel.Visible = False
        End If

        If qod.ExtendedLeadTime Then XrLabel3.Visible = True

        Dim PrintLinesDifStore As New QodPrintLineList
        For Each QodLine As QodLine In qod.Lines
            If (Not QodLine.IsDeliveryChargeItem) And QodLine.DeliverySource <> fulfillStoreId.ToString("0000") And _
            QodLine.QtyToDeliver > 0 And QodLine.DeliveryStatus < Delivery.DeliveredStatusOk Then
                PrintLinesDifStore.Add(QodLine)
            End If
        Next

        'Merge lines for same SKUs and sort them for printing
        OtherStoreDeliveryReport.DataSource = PrintLinesDifStore
        XrSkuDifStore.DataBindings.Add("Text", PrintLinesDifStore, GetPropertyName(Function(f As QodPrintLine) f.SkuNumberDisplay))
        XrDescDifStore.DataBindings.Add("Text", PrintLinesDifStore, GetPropertyName(Function(f As QodPrintLine) f.Description))
        XrQtyDifStore.DataBindings.Add("Text", PrintLinesDifStore, GetPropertyName(Function(f As QodPrintLine) f.QuantityDisplay))
        XrPriceDifStore.DataBindings.Add("Text", PrintLinesDifStore, GetPropertyName(Function(f As QodPrintLine) f.PriceDisplay))
    End Sub

    Public Sub PrintDespatch() Implements IPickNoteForm.Print
        Me.Print()
    End Sub
End Class