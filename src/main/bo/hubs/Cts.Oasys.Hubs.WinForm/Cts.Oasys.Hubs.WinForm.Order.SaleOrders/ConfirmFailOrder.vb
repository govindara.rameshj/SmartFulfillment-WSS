Public Class ConfirmFailOrder 

    Private Sub uxConfirmDelivery_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxConfirmDelivery.Click
        Me.DialogResult = Windows.Forms.DialogResult.Yes
        Me.Close()
    End Sub

    Private Sub uxFailDelivery_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxFailDelivery.Click
        Me.DialogResult = Windows.Forms.DialogResult.No
        Me.Close()
    End Sub

    Private Sub uxCancelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxCancelButton.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

End Class