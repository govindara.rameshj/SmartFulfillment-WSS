﻿
Public Interface IPickInstructions
    Function SetControlsVisibleProperty() As Boolean
    Function GetDeliveryInstructions(ByVal qod As QodHeader) As String
    Function GetPickingInstructions(ByVal qod As QodHeader) As String
    Function HasPickingInstructionsAnyText(ByVal PickInstructions As String) As Boolean
    Function GetFooterHeight(ByVal PickInstructions As String) As Integer
End Interface
