﻿
Namespace Interfaces
    Public Interface IQodLine2109

        Property IsDeliveryChargeItem() As Boolean
        Property QtyOrdered() As Integer
        Property Price() As Decimal
        Property QtyRefunded() As Integer
        Property QtyTaken() As Integer
    End Interface
End Namespace
