﻿Imports Cts.Oasys.Hubs.Core.Order.Qod

Public Interface IGridHeaderOrder
    Sub GridHeaderOrder(ByRef view As DevExpress.XtraGrid.Views.Grid.GridView, ByVal nonZeroStockPageVisible As Boolean, ByVal localPhase As String, ByVal localState As String)
    Function UseExtendedColours() As Boolean
    Function ShowHeaderIssues(ByVal Qod As QodHeader) As Boolean
    Sub ShowLineIssues(ByRef view As DevExpress.XtraGrid.Views.Grid.GridView, ByVal e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs, ByVal storeId As Integer)
End Interface
