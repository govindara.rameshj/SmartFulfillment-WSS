﻿Public Interface ICustomerSearch
    Function SearchCriteriaParameters(ByVal OrderManagerOrderNumber As Integer, _
                                      ByVal PostCode As String, _
                                      ByVal TelephoneNumber As String, _
                                      ByVal CustomerName As String, _
                                      ByVal OvcOrderNumber As String) As SaleOrderSearchParameters
    Function GetOrders(ByVal MinimumDeliveryStatus As Integer, ByVal MaximumDeliveryStatus As Integer) As QodHeaderCollection
    Function GetOrders(ByVal OrderStartDate As Date,ByVal OrderEndDate As Date) As QodHeaderCollection
    Function GetWarehouseOrders() As QodHeaderCollection
End Interface
