﻿Public Interface IGetOrderNumber
    Function GetOrderNumber(ByVal qod As QodHeader) As String
    Function RemoveGroupingByPhase(ByRef view As GridView, ByVal localPhase As String) As Boolean
    Function RemoveGroupingByDateAndPhase(ByRef view As GridView, ByVal localPhase As String) As Boolean
End Interface
