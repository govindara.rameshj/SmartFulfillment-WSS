﻿Imports Cts.Oasys.Hubs.Core.Order.Qod

Namespace Interfaces
    Public Interface IDespatchNote2109

        Function GetOrderValue(ByRef orderHeader As QodHeader) As Decimal
        Function GetDeliveryCharge(ByRef orderHeader As QodHeader) As Decimal
    End Interface
End Namespace
