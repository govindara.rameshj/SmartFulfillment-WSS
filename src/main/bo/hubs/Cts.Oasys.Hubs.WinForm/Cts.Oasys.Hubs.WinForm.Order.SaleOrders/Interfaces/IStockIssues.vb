﻿Public Interface IStockIssues
    Function IsStockAvailable(ByVal s As StockIssues) As Boolean
    Function IsStockAvailableForThisSkun(ByVal s As QodLine, ByVal deliveryQty As Integer) As Boolean
End Interface
