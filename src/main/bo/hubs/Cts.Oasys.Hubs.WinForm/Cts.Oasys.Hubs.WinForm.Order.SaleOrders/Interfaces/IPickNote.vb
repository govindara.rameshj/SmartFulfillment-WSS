﻿Public Interface IPickNote
    Function SetWeightDescription() As String
    Function SetVolumeDescription() As String
    Function SetQuantityDescription() As String
    Function SetPickedDescription() As String
    Function SetWaterMarkTransparency() As Integer
    Function ReDrawForm() As Boolean
    Function UpdateVolume(ByVal quantity As Integer, ByVal weight As Decimal) As Decimal
End Interface
