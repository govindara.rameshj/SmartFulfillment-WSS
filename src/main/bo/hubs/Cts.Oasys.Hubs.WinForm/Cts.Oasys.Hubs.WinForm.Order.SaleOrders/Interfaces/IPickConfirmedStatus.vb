﻿Public Interface IPickConfirmedStatus
    Event ConfirmDeliveryButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean)
    Event ConfirmPickingButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean)
    Event DespatchButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean)
    Event MaintainButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean)
    Event PrintPickNoteButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean)
    Event RePrintDespatchButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean)
    Event RetreiveOrdersButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean)
    Event StockEnquiryButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean)
    Event PrintButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean)
    Event ResetButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean)
    Event ExitButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean)
    Function IsPickConfimedStatusActivated() As Boolean
    Function GetPhase(ByVal status As Delivery) As String
    Function SetButtonStatus(ByVal qod As QodHeaderCollection, ByVal ConfirmPickingAvailable As Boolean) As Boolean
    Function StateBeforeDespatch() As Integer
    Function StateCreatedBeforeDespatch() As Integer
End Interface

