﻿Imports Cts.Oasys.Hubs.Core.Order.Qod
Imports Cts.Oasys.Hubs.WinForm.Order.SaleOrders.Implementations
Imports System.Collections.ObjectModel

Namespace Interfaces
    Public Interface IDespatchNote2109InternalMethods

        Function NetValue(ByVal line As QodLine2109) As Decimal
        Function NetQuantity(ByVal line As QodLine2109) As Integer
        Function ConvertQodLineCollectionToCollectionOfQodLine2109(ByVal toConvert As QodLineCollection) As Collection(Of QodLine2109)
        Function ConvertQodLineToQodLine2109(ByVal line As QodLine) As QodLine2109
    End Interface
End Namespace
