﻿Imports Cts.Oasys.Hubs.Core.Order

Public Class PickConfirmationStatusInActive

    Implements IPickConfirmedStatus

    Public Event ConfirmDeliveryButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.ConfirmDeliveryButton
    Public Event ConfirmPickingButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.ConfirmPickingButton
    Public Event DespatchButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.DespatchButton
    Public Event ExitButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.ExitButton
    Public Event MaintainButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.MaintainButton
    Public Event PrintButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.PrintButton
    Public Event PrintPickNoteButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.PrintPickNoteButton
    Public Event RePrintDespatchButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.RePrintDespatchButton
    Public Event ResetButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.ResetButton
    Public Event RetreiveOrdersButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.RetreiveOrdersButton
    Public Event StockEnquiryButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.StockEnquiryButton

    Friend _ConfirmDeliveryButtonOn As Boolean = False
    Friend _ConfirmPickingButtonOn As Boolean = False
    Friend _DespatchButtonOn As Boolean = False
    Friend _MaintainButtonOn As Boolean = False
    Friend _PrintPickNoteButtonOn As Boolean = False
    Friend _RePrintDespatchButtonOn As Boolean = False

    Friend _RePrintDespatchButtonVisible As Boolean = False
    Friend _DespatchButtonVisible As Boolean = False
    Friend _ConfirmPickButtonVisible As Boolean = False

    Public Function IsPickConfimedStatusActivated() As Boolean Implements IPickConfirmedStatus.IsPickConfimedStatusActivated
        Return False
    End Function

    Public Function GetPhase(ByVal status As Core.Order.Qod.State.Delivery) As String Implements IPickConfirmedStatus.GetPhase
        Return DeliveryDescription.GetPhase(status)
    End Function

    Friend Function IsConfirmDeliveryOn(ByVal DeliveryStatePhase As String, ByVal Suspended As Boolean) As Boolean
        Dim result As Boolean = False

        If Suspended Then Return False

        If DeliveryStatePhase = Qod.State.DeliveryDescription.Phase7 Then
            result = True
        End If
        Return result
    End Function

    Friend Function IsConfirmPickingButtonOn(ByVal DeliveryStatePhase As String, ByVal ConfirmPickingAvailable As Boolean, ByVal Suspended As Boolean) As Boolean
        Dim result As Boolean = False

        If Suspended Then Return False

        If DeliveryStatePhase = Qod.State.DeliveryDescription.Phase5 And ConfirmPickingAvailable Then
            result = True
        End If
        Return result
    End Function

    Friend Function IsDespatchButtonOn(ByVal DeliveryStatePhase As String, ByVal ConfirmPickingAvailable As Boolean, ByVal Suspended As Boolean) As Boolean
        Dim result As Boolean = False

        If Suspended Then Return False

        If DeliveryStatePhase = Qod.State.DeliveryDescription.Phase6 Or _
        (DeliveryStatePhase = Qod.State.DeliveryDescription.Phase8 And ConfirmPickingAvailable = False) Then
            result = True
        End If
        Return result
    End Function

    Private Function IsMaintainButtonOn(ByVal deliveryStatePhase As String, ByVal Suspended As Boolean) As Boolean
        Dim result As Boolean = False

        If Suspended Then Return True

        If deliveryStatePhase = Qod.State.DeliveryDescription.Phase3 Or _
            deliveryStatePhase = Qod.State.DeliveryDescription.Phase4 Or _
            deliveryStatePhase = Qod.State.DeliveryDescription.Phase5 Or _
            deliveryStatePhase = Qod.State.DeliveryDescription.Phase8 Then
            result = True
        End If
        Return result
    End Function

    Friend Function IsPrintButtonOn(ByVal DeliveryStatePhase As String, ByVal Suspended As Boolean) As Boolean
        Dim result As Boolean = False

        If Suspended Then Return False

        If DeliveryStatePhase = Qod.State.DeliveryDescription.Phase3 Or _
            DeliveryStatePhase = Qod.State.DeliveryDescription.Phase4 Or _
            DeliveryStatePhase = Qod.State.DeliveryDescription.Phase5 Or _
            DeliveryStatePhase = Qod.State.DeliveryDescription.Phase8 Then
            result = True
        End If
        Return result
    End Function

    Friend Function IsRePrintDispatchButtonOnButtonOn(ByVal DeliveryStatePhase As String, ByVal ConfirmPickingAvailable As Boolean, ByVal Suspended As Boolean) As Boolean
        Dim result As Boolean = False

        If Suspended Then Return False

        If (DeliveryStatePhase = Qod.State.DeliveryDescription.Phase6 Or _
            DeliveryStatePhase = Qod.State.DeliveryDescription.Phase8) And ConfirmPickingAvailable Then
            result = True
        End If
        Return result

    End Function

    Private Sub RaiseAppropriateEvents()
        RaiseEvent RetreiveOrdersButton(True, True)
        RaiseEvent MaintainButton(True, _MaintainButtonOn)
        RaiseEvent PrintPickNoteButton(True, _PrintPickNoteButtonOn)
        RaiseEvent ConfirmPickingButton(_ConfirmPickButtonVisible, _ConfirmPickingButtonOn)
        RaiseEvent DespatchButton(_DespatchButtonVisible, _DespatchButtonOn)
        RaiseEvent RePrintDespatchButton(_RePrintDespatchButtonVisible, _RePrintDespatchButtonOn)
        RaiseEvent ConfirmDeliveryButton(True, _ConfirmDeliveryButtonOn)
        RaiseEvent StockEnquiryButton(True, True)
        RaiseEvent PrintButton(True, True)
        RaiseEvent ResetButton(True, True)
        RaiseEvent ExitButton(True, True)
    End Sub

    Private Sub ResetFlags(ByVal ConfirmPickingAvailable As Boolean)
        If ConfirmPickingAvailable Then
            _ConfirmPickButtonVisible = True
            _DespatchButtonVisible = False
            _RePrintDespatchButtonVisible = True
        Else
            _ConfirmPickButtonVisible = False
            _DespatchButtonVisible = True
            _RePrintDespatchButtonVisible = False
        End If
        _ConfirmDeliveryButtonOn = False
        _ConfirmPickingButtonOn = False
        _DespatchButtonOn = False
        _MaintainButtonOn = False
        _PrintPickNoteButtonOn = False
        _RePrintDespatchButtonOn = False
    End Sub

    Public Function SetButtonStatus(ByVal qods As Core.Order.Qod.QodHeaderCollection, ByVal ConfirmPickingAvailable As Boolean) As Boolean Implements IPickConfirmedStatus.SetButtonStatus
        ResetFlags(ConfirmPickingAvailable)

        If qods.Count > 0 Then

            For Each QodHeader As QodHeader In qods

                If QodHeader.DespatchSite.Contains(ThisStore.Id4.ToString) Then

                    For Each QodLine As QodLine In QodHeader.Lines
                        If QodLine.DeliverySource = ThisStore.Id4.ToString Then

                            _MaintainButtonOn = _MaintainButtonOn Or IsMaintainButtonOn(QodLine.DeliveryStatePhase, QodHeader.IsSuspended)
                            _PrintPickNoteButtonOn = _PrintPickNoteButtonOn Or IsPrintButtonOn(QodLine.DeliveryStatePhase, QodHeader.IsSuspended)
                            _ConfirmPickingButtonOn = _ConfirmPickingButtonOn Or IsConfirmPickingButtonOn(QodLine.DeliveryStatePhase, ConfirmPickingAvailable, QodHeader.IsSuspended)
                            _DespatchButtonOn = _DespatchButtonOn Or IsDespatchButtonOn(QodLine.DeliveryStatePhase, ConfirmPickingAvailable, QodHeader.IsSuspended)
                            _RePrintDespatchButtonOn = _RePrintDespatchButtonOn Or IsRePrintDispatchButtonOnButtonOn(QodLine.DeliveryStatePhase, ConfirmPickingAvailable, QodHeader.IsSuspended)
                            _ConfirmDeliveryButtonOn = _ConfirmDeliveryButtonOn Or IsConfirmDeliveryOn(QodLine.DeliveryStatePhase, QodHeader.IsSuspended)

                        End If
                    Next

                End If
            Next

        End If
        RaiseAppropriateEvents()
    End Function

    Friend Sub SetButtonStatusOld(ByRef PrintButtonOn As Boolean, ByRef DespatchButtonOn As Boolean, ByRef ConfirmPickingButtonOn As Boolean, ByRef ConfirmDeliveryButtonOn As Boolean, ByRef RePrintDispatchButtonOn As Boolean, ByVal QodHeader As Core.Order.Qod.QodHeader, ByVal ConfirmPickingAvailable As Boolean)
        For Each QodLine As QodLine In QodHeader.Lines
            If QodLine.DeliverySource = ThisStore.Id4.ToString Then
                Select Case QodLine.DeliveryStatePhase
                    Case Qod.State.DeliveryDescription.Phase3
                        PrintButtonOn = True
                    Case Qod.State.DeliveryDescription.Phase4
                        PrintButtonOn = True
                    Case Qod.State.DeliveryDescription.Phase5
                        PrintButtonOn = True
                        If ConfirmPickingAvailable = True Then
                            ConfirmPickingButtonOn = True
                        End If
                        DespatchButtonOn = True
                    Case Qod.State.DeliveryDescription.Phase6
                        ConfirmDeliveryButtonOn = True
                        If ConfirmPickingAvailable = True Then
                            RePrintDispatchButtonOn = True
                        End If
                    Case Qod.State.DeliveryDescription.Phase7
                        If ConfirmPickingAvailable = True Then     'MO'C Change Request 0038
                            ConfirmPickingButtonOn = True
                            RePrintDispatchButtonOn = True
                        Else
                            DespatchButtonOn = True
                            PrintButtonOn = True                    'MO'C Change Request 0038
                        End If
                End Select
            End If
        Next
    End Sub

    Public Function StateBeforeDespatch() As Integer Implements IPickConfirmedStatus.StateBeforeDespatch
        Return Qod.State.Delivery.PickingStatusOk
    End Function

    Public Function StateCreatedBeforeDespatch() As Integer Implements IPickConfirmedStatus.StateCreatedBeforeDespatch
        Return Qod.State.Delivery.PickingListCreated
    End Function
End Class
