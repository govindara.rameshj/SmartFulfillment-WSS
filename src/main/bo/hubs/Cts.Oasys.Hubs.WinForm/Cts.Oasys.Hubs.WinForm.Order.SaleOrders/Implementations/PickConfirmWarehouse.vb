﻿Public Class PickConfirmWarehouse

    Implements IPickconformation

    Public Function PickConfirm(ByVal qods As Core.Order.Qod.QodHeaderCollection) As Boolean Implements IPickconformation.PickConfirm

        Dim printCount As Integer = -2147483648  'Set this to the lowest value so we do not show a number of prints dialog when not needed

        For Each QodHeader As QodHeader In qods

            If QodHeader.IsSuspended = True Then
                MessageBox.Show("Order is suspended, can't pick yet.", "Suspended Order", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                Continue For
            End If

            Trace.WriteLine("About to Clone Data.")
            Dim _tempqod As New QodHeader
            _tempqod = QodHeader.Clone()
            Trace.WriteLine("About to read from the clone.")
            If _tempqod Is Nothing Then
                Trace.WriteLine("_tempqod is nothing.")
            Else
                Trace.WriteLine("_tempqod is not nothing *(Cloned ok).")
            End If
            Trace.WriteLine("Cloned Data. (Order No: " & _tempqod.Number & ")")
            Using Form As New PickConfirmation(_tempqod)
                If Form.ShowDialog = DialogResult.OK Then
                    'Need to re-print the dispatch Note if Allowed and set the status to 700 dispatched
                    If Form._Completed Then
                        'Check the local store id
                        Using store As Core.System.Store.Store = Core.System.Store.GetStore(_tempqod.SellingStoreId)
                            If store Is Nothing Then
                                MessageBox.Show(String.Format(My.Resources.Strings.NoStoreExists, _tempqod.SellingStoreId))
                            Else
                                'Print the Qods and update the flag and counters as required
                                Dim despatch As New DespatchNoteModified
                                despatch.Print()
                                printCount += 1
                                If Not _tempqod.IsPrinted Then
                                    _tempqod.IsPrinted = True
                                Else
                                    _tempqod.NumberReprints += 1
                                End If
                            End If
                        End Using
                        'Update the Delivery Status to Dispatched awaiting Conformation
                        _tempqod.Lines.DeliveryStatus(ThisStore.Id4) = Delivery.DespatchCreated
                        _tempqod.DeliveryStatus = Delivery.DespatchCreated
                        _tempqod.DateDespatch = Now.Date
                        _tempqod.DeliveryConfirmed = True
                    End If

                    'Save the existing scanned info
                    For Each line As QodLine In _tempqod.Lines
                        line.SelfPersist()
                    Next
                    _tempqod.SelfPersist(False)

                    'Store the new data - update the recordset
                    For Each line As QodLine In QodHeader.Lines
                        line.QuantityScanned = _tempqod.Lines(CInt(line.Number.ToString) - 1).QuantityScanned
                        line.DeliveryStatus = _tempqod.Lines(CInt(line.Number.ToString) - 1).DeliveryStatus
                    Next
                    QodHeader.DeliveryStatus = _tempqod.DeliveryStatus
                End If
            End Using
        Next
    End Function

    Public Event RefreshGrid() Implements IPickconformation.RefreshGrid
End Class
