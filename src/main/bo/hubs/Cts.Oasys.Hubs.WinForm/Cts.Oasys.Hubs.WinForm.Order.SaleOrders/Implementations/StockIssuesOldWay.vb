﻿Public Class StockIssuesOldWay

    Implements IStockIssues

    Public Function IsStockAvailable(ByVal s As StockIssues) As Boolean Implements IStockIssues.IsStockAvailable
        Return (s.QtyToDeliver > s.QtyOnHand)
    End Function

    Public Function IsStockAvailableForThisSkun(ByVal qodLine As Core.Order.Qod.QodLine, ByVal qtyToDeliver As Integer) As Boolean Implements IStockIssues.IsStockAvailableForThisSkun
        Return (qodLine.DeliveryStatus < Delivery.DespatchCreated _
                AndAlso (QtyToDeliver > 0) _
                AndAlso (QtyToDeliver > (qodLine.QtyOnHand + qodLine.QtyOnOrder)))
    End Function
End Class
