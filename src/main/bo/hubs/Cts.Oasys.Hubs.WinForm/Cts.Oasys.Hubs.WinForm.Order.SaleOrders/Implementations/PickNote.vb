﻿Public Class PickNote
    Implements IPickNote


    Private Const watermarkTransparencyLevel As Integer = 170
    Private Const descriptionPicked As String = "Quantity Picked"
    Private Const descriptionQuantity As String = "Quantity Ordered"
    Private Const descriptionVolume As String = "Line Weight"
    Private Const descriptionWeight As String = "Unit Weight"

    Public Function SetPickedDescription() As String Implements IPickNote.SetPickedDescription
        Return descriptionPicked
    End Function

    Public Function SetQuantityDescription() As String Implements IPickNote.SetQuantityDescription
        Return descriptionQuantity
    End Function

    Public Function SetVolumeDescription() As String Implements IPickNote.SetVolumeDescription
        Return descriptionVolume
    End Function

    Public Function SetWeightDescription() As String Implements IPickNote.SetWeightDescription
        Return descriptionWeight
    End Function

    Public Function UpdateVolume(ByVal quantity As Integer, ByVal weight As Decimal) As Decimal Implements IPickNote.UpdateVolume
        Return quantity * weight
    End Function

    Public Function SetWaterMarkTransparency() As Integer Implements IPickNote.SetWaterMarkTransparency
        Return watermarkTransparencyLevel
    End Function

    Public Function ReDrawForm() As Boolean Implements IPickNote.ReDrawForm
        Return True
    End Function
End Class
