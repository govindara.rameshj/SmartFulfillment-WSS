﻿Imports Cts.Oasys.Hubs.Core.Order.Qod
Imports Cts.Oasys.Hubs.WinForm.Order.SaleOrders.Implementations
Imports System.Collections.ObjectModel
Imports System.Runtime.CompilerServices
Imports Cts.Oasys.Hubs.WinForm.Order.SaleOrders.Interfaces

Namespace Implementations
    Public Class DespatchNote2109
        Implements IDespatchNote2109
        Implements IDespatchNote2109InternalMethods

        Public Function GetOrderValue(ByRef orderHeader As Core.Order.Qod.QodHeader) As Decimal Implements IDespatchNote2109.GetOrderValue
            Dim lines As Collection(Of QodLine2109)

            GetOrderValue = 0
            lines = ConvertQodLineCollectionToCollectionOfQodLine2109(orderHeader.Lines)
            For Each line As QodLine2109 In lines
                With line
                    GetOrderValue += NetValue(line)
                End With
            Next
        End Function

        Public Function GetDeliveryCharge(ByRef orderHeader As Core.Order.Qod.QodHeader) As Decimal Implements IDespatchNote2109.GetDeliveryCharge
            Dim lines As Collection(Of QodLine2109)

            GetDeliveryCharge = 0
            lines = ConvertQodLineCollectionToCollectionOfQodLine2109(orderHeader.Lines)
            For Each line As QodLine2109 In lines
                With line
                    If .IsDeliveryChargeItem Then
                        GetDeliveryCharge += NetValue(line)
                    End If
                End With
            Next
            GetDeliveryCharge = orderHeader.GetCurrentDeliveryCharge
        End Function

        Public Function NetValue(ByVal line As QodLine2109) As Decimal Implements IDespatchNote2109InternalMethods.NetValue

            Return line.Price * NetQuantity(line)
        End Function

        Public Function NetQuantity(ByVal line As QodLine2109) As Integer Implements IDespatchNote2109InternalMethods.NetQuantity

            ' NB QtyRefunded is stored as a -ve value
            Return (line.QtyOrdered - line.QtyTaken + line.QtyRefunded)
        End Function

        Public Function ConvertQodLineCollectionToCollectionOfQodLine2109(ByVal toConvert As QodLineCollection) As Collection(Of QodLine2109) Implements IDespatchNote2109InternalMethods.ConvertQodLineCollectionToCollectionOfQodLine2109
            Dim newQodLine2109 As QodLine2109

            ConvertQodLineCollectionToCollectionOfQodLine2109 = New Collection(Of QodLine2109)()
            If toConvert IsNot Nothing Then
                For Each line As QodLine In toConvert
                    newQodLine2109 = ConvertQodLineToQodLine2109(line)
                    ConvertQodLineCollectionToCollectionOfQodLine2109.Add(newQodLine2109)
                Next
            End If
        End Function

        Public Function ConvertQodLineToQodLine2109(ByVal line As QodLine) As QodLine2109 Implements IDespatchNote2109InternalMethods.ConvertQodLineToQodLine2109
            ConvertQodLineToQodLine2109 = New QodLine2109()

            If line IsNot Nothing Then
                With ConvertQodLineToQodLine2109
                    .IsDeliveryChargeItem = line.IsDeliveryChargeItem
                    .Price = line.Price
                    .QtyOrdered = line.QtyOrdered
                    .QtyRefunded = line.QtyRefunded
                    .QtyTaken = line.QtyTaken
                End With
            End If
        End Function
    End Class
End Namespace