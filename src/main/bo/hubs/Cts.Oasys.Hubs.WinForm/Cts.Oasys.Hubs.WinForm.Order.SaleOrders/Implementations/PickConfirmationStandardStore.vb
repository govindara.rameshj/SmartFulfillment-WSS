﻿Imports System.Xml
Imports Cts.Oasys.Hubs.Core.Order.Qod

Public Class PickConfirmationStandardStore
    Implements IPickconformation

    Public Event RefreshGrid() Implements IPickconformation.RefreshGrid

    Public Function PickConfirm(ByVal qods As Core.Order.Qod.QodHeaderCollection) As Boolean Implements IPickconformation.PickConfirm

        Dim ConfigXml As XmlNode = ConfigXMLDoc.SelectSingleNode("Configuration/DataCheck/DespatchOrder")

        For Each Header As QodHeader In qods
            If Header.DeliveryStatus < Delivery.PickingStatusOk Then
                MessageBox.Show("OM has not confirmed Pick List Printed. Please wait 10 minutes and try again.", "Invalid Delivery Status", MessageBoxButtons.OK)
                RaiseEvent RefreshGrid()
                Return False
            End If

            If Header.HasChanged(ConfigXml) Then
                MessageBox.Show(My.Resources.Strings.DataChanged, "Data has changed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                RaiseEvent RefreshGrid()
                Return False
            End If
        Next

        For Each QodHeader As QodHeader In qods
            QodHeader.Lines.DeliveryStatus(ThisStore.Id4) = Delivery.PickingConfirmCreated
            QodHeader.Lines.DeliveryStatus = Delivery.PickingConfirmCreated
            QodHeader.DeliveryStatus = Delivery.PickingConfirmCreated
            QodHeader.SelfPersistTree()
        Next
        Return True
    End Function
End Class
