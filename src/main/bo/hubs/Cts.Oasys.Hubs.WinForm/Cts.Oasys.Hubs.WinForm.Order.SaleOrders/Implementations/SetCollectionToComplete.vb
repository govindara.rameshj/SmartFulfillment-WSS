﻿Public Class SetCollectionToComplete
    Implements ISetCollectionToComplete

    Public Sub SetCollectionToComplete(ByVal QodHeaderActionFilter As Core.Order.Qod.QodHeaderActionFilter, ByVal StoreId As Integer) Implements ISetCollectionToComplete.SetCollectionToComplete
        For Each QodHeader As QodHeader In QodHeaderActionFilter.PositiveActionList
            If QodHeader.IsForDelivery Then
                QodHeader.Lines.DeliveryStatus(StoreId) = Delivery.DespatchCreated
                QodHeader.DeliveryStatus = Delivery.DespatchCreated
            Else
                QodHeader.Lines.DeliveryStatus(StoreId) = Delivery.DeliveredCreated
                QodHeader.DeliveryStatus = Delivery.DeliveredCreated
            End If
            QodHeader.DateDespatch = Now.Date
            QodHeader.DeliveryConfirmed = True
            QodHeader.SelfPersistTree()
        Next

    End Sub
End Class
