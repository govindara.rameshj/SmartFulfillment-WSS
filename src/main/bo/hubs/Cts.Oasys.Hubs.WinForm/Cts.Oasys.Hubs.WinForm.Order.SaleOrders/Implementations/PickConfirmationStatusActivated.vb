﻿Imports Cts.Oasys.Hubs.Core.Order

Public Class PickConfirmationStatusActivated

    Implements IPickConfirmedStatus

    Friend _ConfirmDeliveryButtonOn As Boolean = False
    Friend _ConfirmPickingButtonOn As Boolean = False
    Friend _DespatchButtonOn As Boolean = False
    Friend _MaintainButtonOn As Boolean = False
    Friend _PrintPickNoteButtonOn As Boolean = False
    Friend _RePrintDispatchButtonOn As Boolean = False
    Friend _RePrintDespatchButtonVisible As Boolean = False
    Friend _DespatchButtonVisible As Boolean = False
    Friend _ConfirmPickButtonVisible As Boolean = False

    Public Event ConfirmDeliveryButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.ConfirmDeliveryButton
    Public Event ConfirmPickingButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.ConfirmPickingButton
    Public Event DespatchButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.DespatchButton
    Public Event ExitButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.ExitButton
    Public Event MaintainButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.MaintainButton
    Public Event PrintButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.PrintButton
    Public Event PrintPickNoteButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.PrintPickNoteButton
    Public Event RePrintDespatchButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.RePrintDespatchButton
    Public Event ResetButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.ResetButton
    Public Event RetreiveOrdersButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.RetreiveOrdersButton
    Public Event StockEnquiryButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Implements IPickConfirmedStatus.StockEnquiryButton

#Region "Interface Functions"

    Public Function IsPickConfimedStatusActivated() As Boolean Implements IPickConfirmedStatus.IsPickConfimedStatusActivated
        Return True
    End Function

    Public Function GetPhase(ByVal status As Core.Order.Qod.State.Delivery) As String Implements IPickConfirmedStatus.GetPhase
        Return DeliveryDescription.GetPhaseIncludingPickConfirmStatus(status)
    End Function

    Public Function SetButtonStatus(ByVal qods As Core.Order.Qod.QodHeaderCollection, ByVal ConfirmPickingAvailable As Boolean) As Boolean Implements IPickConfirmedStatus.SetButtonStatus

        ResetToDefaultPosition(ConfirmPickingAvailable)

        If qods.Count > 0 Then
            For Each QodHeader As QodHeader In qods

                If QodHeader.DespatchSite.Contains(ThisStore.Id4.ToString) Then

                    For Each QodLine As QodLine In QodHeader.Lines
                        If IsLineItemToBeFulfilledByThisHub(QodLine) Then

                            _MaintainButtonOn = _MaintainButtonOn Or IsMaintainButtonOn(QodLine.DeliveryStatus, QodHeader.IsSuspended)
                            _PrintPickNoteButtonOn = _PrintPickNoteButtonOn Or IsPrintPickNoteButtonOn(QodLine.DeliveryStatus, QodHeader.IsSuspended)
                            _ConfirmPickingButtonOn = _ConfirmPickingButtonOn Or IsConfirmPickingButtonOn(QodLine.DeliveryStatus, QodHeader.IsSuspended)
                            _DespatchButtonOn = _DespatchButtonOn Or IsDespatchButtonOn(QodLine.DeliveryStatus, ConfirmPickingAvailable, QodHeader.IsSuspended)
                            _RePrintDispatchButtonOn = _RePrintDispatchButtonOn Or IsRePrintDispatchButtonOnButtonOn(QodLine.DeliveryStatus, ConfirmPickingAvailable, QodHeader.IsSuspended)
                            _ConfirmDeliveryButtonOn = _ConfirmDeliveryButtonOn Or IsConfirmDeliveryOn(QodLine.DeliveryStatus, QodHeader.IsSuspended)

                        End If
                    Next

                End If

            Next

        End If

        RaiseAppropriateEvents()

    End Function

#End Region

#Region "Class Working Functions"

    Private Function IsLineItemToBeFulfilledByThisHub(ByVal QodLine As QodLine) As Boolean

        Return QodLine.DeliverySource = ThisStore.Id4.ToString

    End Function

    Private Sub RaiseAppropriateEvents()
        RaiseEvent RetreiveOrdersButton(True, True)
        RaiseEvent MaintainButton(True, _MaintainButtonOn)
        RaiseEvent PrintPickNoteButton(True, _PrintPickNoteButtonOn)
        RaiseEvent ConfirmPickingButton(_ConfirmPickButtonVisible, _ConfirmPickingButtonOn)
        RaiseEvent DespatchButton(_DespatchButtonVisible, _DespatchButtonOn)
        RaiseEvent RePrintDespatchButton(_RePrintDespatchButtonVisible, _RePrintDispatchButtonOn)
        RaiseEvent ConfirmDeliveryButton(True, _ConfirmDeliveryButtonOn)
        RaiseEvent StockEnquiryButton(True, True)
        RaiseEvent PrintButton(True, True)
        RaiseEvent ResetButton(True, True)
        RaiseEvent ExitButton(True, True)
    End Sub

    Private Sub ResetToDefaultPosition(ByVal ConfirmPickingAvailable As Boolean)
        If ConfirmPickingAvailable Then
            _DespatchButtonVisible = False
            _RePrintDespatchButtonVisible = True
        Else
            _DespatchButtonVisible = True
            _RePrintDespatchButtonVisible = False
        End If
        _ConfirmPickButtonVisible = True


        _ConfirmDeliveryButtonOn = False
        _ConfirmPickingButtonOn = False
        _DespatchButtonOn = False
        _MaintainButtonOn = False
        _PrintPickNoteButtonOn = False
        _RePrintDispatchButtonOn = False
    End Sub

#End Region

#Region "Phase Selector Area"

    Friend Function IsPhase3(ByVal deliveryStatus As Integer) As Boolean
        Return (deliveryStatus >= Qod.State.Delivery.IbtOutAllStock And deliveryStatus <= Qod.State.Delivery.IbtInAllStock)
    End Function

    Friend Function IsPhase4(ByVal deliveryStatus As Integer) As Boolean
        Return (deliveryStatus >= Qod.State.Delivery.ReceiptNotifyCreate And deliveryStatus <= Qod.State.Delivery.ReceiptStatusOk)
    End Function

    Friend Function IsPhase5(ByVal deliveryStatus As Integer) As Boolean
        Return (deliveryStatus >= Qod.State.Delivery.PickingListCreated And deliveryStatus <= Qod.State.Delivery.PickingStatusOk)
    End Function

    Friend Function IsPhase6(ByVal deliveryStatus As Integer) As Boolean
        Return (deliveryStatus >= Qod.State.Delivery.PickingConfirmCreated And deliveryStatus <= Qod.State.Delivery.PickingConfirmStatusOk)
    End Function

    Friend Function IsPhase7(ByVal deliveryStatus As Integer) As Boolean
        Return (deliveryStatus >= Qod.State.Delivery.DespatchCreated And deliveryStatus <= Qod.State.Delivery.DespatchStatusOk)
    End Function

    Friend Function IsPhase8(ByVal deliveryStatus As Integer) As Boolean
        Return (deliveryStatus >= Qod.State.Delivery.UndeliveredCreated And deliveryStatus <= Qod.State.Delivery.UndeliveredStatusOk)
    End Function

    Friend Function IsPhase9(ByVal deliveryStatus As Integer) As Boolean
        Return (deliveryStatus >= Qod.State.Delivery.DeliveredCreated And deliveryStatus <= Qod.State.Delivery.DeliveredStatusOk)
    End Function

#End Region

#Region "Button On Checks"

    Friend Function IsPrintPickNoteButtonOn(ByVal DeliveryStatus As Integer, ByVal Suspended As Boolean) As Boolean
        Dim result As Boolean = False

        If Suspended Then Return False

        If IsPhase3(DeliveryStatus) Or _
            IsPhase4(DeliveryStatus) Or _
            IsPhase5(DeliveryStatus) Or _
            IsPhase6(DeliveryStatus) Or _
            IsPhase8(DeliveryStatus) Then
            result = True
        End If
        Return result
    End Function

    Friend Function IsMaintainButtonOn(ByVal DeliveryStatus As Integer, ByVal Suspended As Boolean) As Boolean
        Dim result As Boolean = False

        If Suspended Then Return True

        If IsPhase3(DeliveryStatus) Or _
            IsPhase4(DeliveryStatus) Or _
            IsPhase5(DeliveryStatus) Or _
            IsPhase6(DeliveryStatus) Or _
            IsPhase8(DeliveryStatus) Then

            result = True
        End If

        Return result

    End Function

    Friend Function IsConfirmPickingButtonOn(ByVal DeliveryStatus As Integer, ByVal Suspended As Boolean) As Boolean
        Dim result As Boolean = False

        If Suspended Then Return False

        If IsPhase5(DeliveryStatus) Or _
            IsPhase6(DeliveryStatus) Or _
            IsPhase8(DeliveryStatus) Then
            result = True
        End If
        Return result
    End Function

    Friend Function IsDespatchButtonOn(ByVal DeliveryStatus As Integer, ByVal ConfirmPickingAvailable As Boolean, ByVal Suspended As Boolean) As Boolean
        Dim result As Boolean = False

        If Suspended Then Return False

        If (IsPhase6(DeliveryStatus) And Not ConfirmPickingAvailable) Or _
            IsPhase8(DeliveryStatus) Then
            result = True
        End If
        Return result
    End Function

    Friend Function IsConfirmDeliveryOn(ByVal DeliveryStatus As Integer, ByVal Suspended As Boolean) As Boolean
        Dim result As Boolean = False

        If Suspended Then Return False

        If IsPhase6(DeliveryStatus) Or IsPhase7(DeliveryStatus) Then
            result = True
        End If
        Return result
    End Function

    Friend Function IsRePrintDispatchButtonOnButtonOn(ByVal DeliveryStatus As Integer, ByVal ConfirmPickingAvailable As Boolean, ByVal Suspended As Boolean) As Boolean
        Dim result As Boolean = False

        If Suspended Then Return False

        If (IsPhase7(DeliveryStatus) Or _
            IsPhase8(DeliveryStatus)) And ConfirmPickingAvailable Then
            result = True
        End If
        Return result
    End Function
#End Region

    Public Function StateBeforeDespatch() As Integer Implements IPickConfirmedStatus.StateBeforeDespatch
        Return Qod.State.Delivery.PickingConfirmStatusOk
    End Function

    Public Function StateCreatedBeforeDespatch() As Integer Implements IPickConfirmedStatus.StateCreatedBeforeDespatch
        Return Qod.State.Delivery.PickingConfirmCreated
    End Function
End Class
