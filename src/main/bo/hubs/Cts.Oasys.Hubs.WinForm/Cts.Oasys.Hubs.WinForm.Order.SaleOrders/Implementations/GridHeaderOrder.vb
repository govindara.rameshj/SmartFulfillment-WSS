﻿Public Class StockIssues
    Public Skun As String
    Public QtyToDeliver As Integer
    Public QtyOnHand As Integer
    Public QtyOnOrder As Integer
    Public DeliveryStatus As Integer
End Class

Public Class GridHeaderOrder

    Implements IGridHeaderOrder
    Private _StockCheck As IStockIssues = (New StockIssuesFactory).GetImplementation
    Private _SkuNumber As String = String.Empty

#Region "Interface"

    Public Sub GridHeaderOrder(ByRef view As GridView, ByVal nonZeroStockPageVisible As Boolean, ByVal localPhase As String, ByVal localState As String) Implements IGridHeaderOrder.GridHeaderOrder
        view.ColumnPanelRowHeight = 60
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.OmOrderNumber), My.Resources.Columns.OMOrderNumber, 40, 80)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.OvcOrderNumber), My.Resources.Columns.OVCOrderNumber, 90, 100)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.Number), My.Resources.Columns.DeliveryNumber, 55, 70)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.IsForDelivery), My.Resources.Columns.DeliveryOrder, 60, 68)
        If nonZeroStockPageVisible Then GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.VendaNumber))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.NumberStockIssues), My.Resources.Columns.StockIssues, 65, 70)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.IsSuspended), My.Resources.Columns.IsSuspended, 83, 83)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.RevisionNumber), , 65, 70)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.SellingStoreId), , 65, 70)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.SellingStoreName))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.SellingStorePhone), , 75, 80)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.SellingStoreOrderId), My.Resources.Columns.SellingStoreOrderId, 65, 70)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.DespatchSite), , 70, 75)
        GridViewSetColumn(view, localPhase, My.Resources.Columns.DeliveryPhase)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.CustomerName))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.PhoneNumber), My.Resources.Columns.PhoneNumber, 75, 80)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.PhoneNumberMobile), My.Resources.Columns.PhoneMobile, 80, 85)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.CustomerAddress))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.DeliveryAddress))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.PhoneNumberWork), My.Resources.Columns.PhoneWork)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.CustomerEmail), My.Resources.Columns.CustomerEmail)
        GridViewSetColumn(view, localState, My.Resources.Columns.DeliveryState)
    End Sub

    Public Function UseExtendedColours() As Boolean Implements IGridHeaderOrder.UseExtendedColours
        Return True
    End Function

    Public Sub ShowLineIssues(ByRef view As GridView, ByVal e As RowStyleEventArgs, ByVal storeId As Integer) Implements IGridHeaderOrder.ShowLineIssues


        Dim deliverySource As String = String.Empty
        Dim deliveryStoreLineIndex As Integer = 0
        Dim RowIndexArray(0) As Integer

        deliverySource = view.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As QodLine) f.DeliverySource)).ToString
        If view.ActiveFilterEnabled = False Then
            deliveryStoreLineIndex = e.RowHandle
        Else
            Dim LineCount As Integer = 0
            For Each Line As QodLine In CType(view.DataSource, QodLineCollection)
                If Line.DeliverySource = storeId.ToString Then
                    ReDim Preserve RowIndexArray(deliveryStoreLineIndex)
                    RowIndexArray(deliveryStoreLineIndex) = LineCount
                    deliveryStoreLineIndex += 1
                    If deliveryStoreLineIndex > e.RowHandle Then Exit For
                End If
                LineCount += 1
            Next
            If deliveryStoreLineIndex > e.RowHandle Then
                deliveryStoreLineIndex = RowIndexArray(e.RowHandle)
            End If
        End If


        Dim SKUN As String = CStr(view.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As QodLine) f.SkuNumber)))

        Dim deliveryStore As Integer = 0
        If Integer.TryParse(deliverySource, deliveryStore) Then
            'check for sale line IsDependentOnOrders
            If IsDepenedentOnOrders(view, SKUN, storeId) Then
                e.Appearance.BackColor = Color.Gold
            End If

            'check for sale line IsNotFilfullable
            If IsNotFulFillable(view, SKUN, storeId) Then
                e.Appearance.BackColor = Color.Red
            End If
        End If


    End Sub

    Public Function ShowHeaderIssues(ByVal Qod As QodHeader) As Boolean Implements IGridHeaderOrder.ShowHeaderIssues
        Dim result As Boolean = False

        If NumberStockIssues(Qod) > 0 Then
            result = True
        End If
        Return result
    End Function

#End Region

#Region "Private Procedures & Functions"

    Private Sub GridViewSetColumn(ByRef view As GridView, ByVal columnName As String, Optional ByVal caption As String = "", Optional ByVal minWidth As Integer = 0, Optional ByVal maxWidth As Integer = 0)
        view.Columns(columnName).VisibleIndex = view.VisibleColumns.Count
        If caption.Length > 0 Then view.Columns(columnName).Caption = caption
        If minWidth > 0 Then view.Columns(columnName).MinWidth = minWidth
        If maxWidth > 0 Then view.Columns(columnName).MaxWidth = maxWidth
        view.Columns(columnName).GetBestWidth()
    End Sub

    Friend Function IsDepenedentOnOrders(ByRef view As GridView, ByVal SKUN As String, ByVal storeId As Integer) As Boolean
        Dim CurrentLine As QodLine
        Dim QtyToDeliver As Integer = 0

        CurrentLine = FindQodLine(view, SKUN)

        If CurrentLine.DeliveryStatus < Delivery.DespatchCreated Then

            For Each line As QodLine In CType(view.DataSource, QodLineCollection)
                If line.SkuNumber = CurrentLine.SkuNumber AndAlso CInt(line.DeliverySource) = storeId Then
                    QtyToDeliver += line.QtyToDeliver
                End If
            Next
        End If

        Return (CurrentLine.DeliveryStatus < Delivery.DespatchCreated _
               AndAlso (QtyToDeliver > 0) _
               AndAlso QtyToDeliver <= (CurrentLine.QtyOnHand + CurrentLine.QtyOnOrder) _
               AndAlso QtyToDeliver > CurrentLine.QtyOnHand)


    End Function

    Friend Function IsNotFulFillable(ByRef view As GridView, ByVal SKUN As String, ByVal storeId As Integer) As Boolean
        Dim CurrentLine As QodLine
        Dim QtyToDeliver As Integer = 0

        CurrentLine = FindQodLine(view, SKUN)

        If CurrentLine.DeliveryStatus < Delivery.DespatchCreated Then
            For Each line As QodLine In CType(view.DataSource, QodLineCollection)
                If line.SkuNumber = CurrentLine.SkuNumber AndAlso CInt(line.DeliverySource) = storeId Then
                    QtyToDeliver += line.QtyToDeliver
                End If
            Next
        End If

        Return _StockCheck.IsStockAvailableForThisSkun(CurrentLine, QtyToDeliver)
    End Function

    Private Function FindQodLine(ByRef view As GridView, ByVal SKUN As String) As QodLine

        For Each Line As QodLine In CType(view.DataSource, QodLineCollection)
            If Line.SkuNumber = SKUN Then
                Return Line
            End If
        Next
        FindQodLine = Nothing
    End Function

    Friend Function NumberStockIssues(ByVal Qod As QodHeader) As Integer
        Dim total As Integer = 0
        Dim SkunStockIssues As New List(Of StockIssues)

        SkunStockIssues = CombineLinesOfSameSku(Qod)
        For Each s As StockIssues In SkunStockIssues
            If _StockCheck.IsStockAvailable(s) Then total += 1
        Next
        Return total

    End Function

    Friend Function CombineLinesOfSameSku(ByVal Qod As QodHeader) As List(Of StockIssues)
        Dim SkunStockIssues As New List(Of StockIssues)

        For Each line As QodLine In Qod.Lines
            If line.IsDeliveryChargeItem Then Continue For
            If line.DeliverySource <> ThisStore.Id4.ToString Then Continue For
            _SkuNumber = line.SkuNumber
            Dim s As StockIssues = SkunStockIssues.Find(AddressOf FindSku)
            If s Is Nothing Then
                Dim x As New StockIssues
                x.QtyToDeliver = line.QtyToDeliver
                x.QtyOnHand = line.QtyOnHand
                x.Skun = line.SkuNumber
                x.DeliveryStatus = line.DeliveryStatus
                x.QtyOnOrder = line.QtyOnOrder
                SkunStockIssues.Add(x)
            Else
                s.QtyToDeliver += line.QtyToDeliver
                If line.DeliveryStatus > s.DeliveryStatus Then s.DeliveryStatus = line.DeliveryStatus
            End If
        Next
        Return SkunStockIssues
    End Function

    Private Function FindSku(ByVal Sku As StockIssues) As Boolean
        If Sku.Skun = _SkuNumber Then
            Return True
        Else
            Return False
        End If
    End Function

#End Region

End Class