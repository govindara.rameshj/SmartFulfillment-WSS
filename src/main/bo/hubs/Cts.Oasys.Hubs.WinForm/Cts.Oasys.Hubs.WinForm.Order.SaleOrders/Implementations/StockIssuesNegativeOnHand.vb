﻿Public Class StockIssuesNegativeOnHand

    Implements IStockIssues

    Public Function IsStockAvailable(ByVal s As StockIssues) As Boolean Implements IStockIssues.IsStockAvailable
        Return (s.QtyOnHand < 0 And s.DeliveryStatus >= 399)
    End Function

    Public Function IsStockAvailableForThisSkun(ByVal s As Core.Order.Qod.QodLine, ByVal qty As Integer) As Boolean Implements IStockIssues.IsStockAvailableForThisSkun
        Return (s.QtyOnHand < 0 And s.DeliveryStatus >= 399)
    End Function
End Class
