﻿Public Class GetOrderNumber
    Implements IGetOrderNumber

    Public Function GetOrderNumber(ByVal qod As Core.Order.Qod.QodHeader) As String Implements IGetOrderNumber.GetOrderNumber
        Return qod.Number
    End Function

    Public Function RemoveGroupingByDateAndPhase(ByRef view As DevExpress.XtraGrid.Views.Grid.GridView, ByVal localPhase As String) As Boolean Implements IGetOrderNumber.RemoveGroupingByDateAndPhase
        view.GroupedColumns(0).Visible = False
        view.GroupedColumns(1).Visible = False
    End Function

    Public Function RemoveGroupingByPhase(ByRef view As DevExpress.XtraGrid.Views.Grid.GridView, ByVal localPhase As String) As Boolean Implements IGetOrderNumber.RemoveGroupingByPhase
        view.Columns(localPhase).GroupIndex = 0
    End Function
End Class
