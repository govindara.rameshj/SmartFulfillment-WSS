﻿Imports Cts.Oasys.Hubs.Core.Order

Public Class CustomerSearch

    Implements ICustomerSearch

    Friend _SearchCriteria As Qod.SaleOrderSearchParameters

    Friend Sub ClearLastSearchCriteria()
        Dim SearchCriteria As New Qod.SaleOrderSearchParameters
        _SearchCriteria = SearchCriteria
    End Sub

    Public Function SearchCriteriaParameters(ByVal OrderManagerOrderNumber As Integer, _
                                             ByVal PostCode As String, _
                                             ByVal TelephoneNumber As String, _
                                             ByVal CustomerName As String, _
                                             ByVal OvcOrderNumber As String) As Qod.SaleOrderSearchParameters Implements ICustomerSearch.SearchCriteriaParameters
        ClearLastSearchCriteria()
        If OrderManagerOrderNumber > 0 Then _SearchCriteria.OrderManagerOrderNumber = OrderManagerOrderNumber
        If PostCode.Length > 0 Then _SearchCriteria.PostCode = PostCode
        If TelephoneNumber.Length > 0 Then _SearchCriteria.TelephoneNumber = TelephoneNumber
        If CustomerName.Length > 0 Then _SearchCriteria.CustomerName = CustomerName
        If OvcOrderNumber.Length > 0 Then _SearchCriteria.OvcOrderNumber = OvcOrderNumber
        Return _SearchCriteria
    End Function

    Public Function GetOrders(ByVal MinimumDeliveryStatus As Integer, ByVal MaximumDeliveryStatus As Integer) As Qod.QodHeaderCollection Implements ICustomerSearch.GetOrders
        If MinimumDeliveryStatus > 0 Then _SearchCriteria.MinimumDeliveryStatus = MinimumDeliveryStatus
        If MaximumDeliveryStatus > 0 Then _SearchCriteria.MaximumDeliveryStatus = MaximumDeliveryStatus
        Return GetOrders()
    End Function

    Public Function GetOrders(ByVal OrderStartDate As Date, ByVal OrderEndDate As Date) As Qod.QodHeaderCollection Implements ICustomerSearch.GetOrders
        If Not OrderStartDate = DateTime.MinValue Then _SearchCriteria.OrderStartDate = OrderStartDate
        If Not OrderEndDate = DateTime.MinValue Then _SearchCriteria.OrderEndDate = OrderEndDate
        Return GetOrders()
    End Function

    Friend Function IsSearchCriteriaEmpty() As Boolean
        Dim result As Boolean = False
        If _SearchCriteria.OrderManagerOrderNumber = 0 AndAlso _
            _SearchCriteria.CustomerName Is Nothing AndAlso _
            _SearchCriteria.TelephoneNumber Is Nothing AndAlso _
            _SearchCriteria.PostCode Is Nothing AndAlso _
            _SearchCriteria.OvcOrderNumber Is Nothing Then
            result = True
        End If
        Return result
    End Function

    Public Function GetWarehouseOrders() As Qod.QodHeaderCollection Implements ICustomerSearch.GetWarehouseOrders
        If IsSearchCriteriaEmpty() Then
            Return New QodHeaderCollection
        Else
            Return GetOrders()
        End If
    End Function

    Friend Function GetOrders() As Qod.QodHeaderCollection
        Return Qod.GetHeader(_SearchCriteria)
    End Function

End Class
