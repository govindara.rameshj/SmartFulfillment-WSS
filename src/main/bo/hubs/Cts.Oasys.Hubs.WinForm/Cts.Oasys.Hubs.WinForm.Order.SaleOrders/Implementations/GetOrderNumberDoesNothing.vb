﻿Public Class GetOrderNumberDoesNothing
    Implements IGetOrderNumber

    Public Function GetOrderNumber(ByVal qod As Core.Order.Qod.QodHeader) As String Implements IGetOrderNumber.GetOrderNumber
        Return String.Empty
    End Function

    Public Function RemoveGroupingByDateAndPhase(ByRef view As DevExpress.XtraGrid.Views.Grid.GridView, ByVal localPhase As String) As Boolean Implements IGetOrderNumber.RemoveGroupingByDateAndPhase
        Return False
    End Function

    Public Function RemoveGroupingByPhase(ByRef view As DevExpress.XtraGrid.Views.Grid.GridView, ByVal localPhase As String) As Boolean Implements IGetOrderNumber.RemoveGroupingByPhase
        Return False
    End Function
End Class
