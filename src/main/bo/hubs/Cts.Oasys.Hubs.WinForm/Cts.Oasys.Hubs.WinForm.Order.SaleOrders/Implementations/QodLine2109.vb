﻿Imports Cts.Oasys.Hubs.WinForm.Order.SaleOrders.Interfaces

Namespace Implementations
    Public Class QodLine2109
        Implements IQodLine2109

        Private _isDeliveryChargeItem As Boolean
        Private _price As Decimal
        Private _qtyOrdered As Integer
        Private _qtyTaken As Integer
        Private _qtyRefunded As Integer

        Public Property IsDeliveryChargeItem() As Boolean Implements IQodLine2109.IsDeliveryChargeItem
            Get
                IsDeliveryChargeItem = _isDeliveryChargeItem
            End Get
            Set(ByVal value As Boolean)
                _isDeliveryChargeItem = value
            End Set
        End Property

        Public Property Price() As Decimal Implements Interfaces.IQodLine2109.Price
            Get
                Price = _price
            End Get
            Set(ByVal value As Decimal)
                _price = value
            End Set
        End Property

        Public Property QtyOrdered() As Integer Implements Interfaces.IQodLine2109.QtyOrdered
            Get
                QtyOrdered = _qtyOrdered
            End Get
            Set(ByVal value As Integer)
                _qtyOrdered = value
            End Set
        End Property

        Public Property QtyRefunded() As Integer Implements Interfaces.IQodLine2109.QtyRefunded
            Get
                QtyRefunded = _qtyRefunded
            End Get
            Set(ByVal value As Integer)
                _qtyRefunded = value
            End Set
        End Property

        Public Property QtyTaken() As Integer Implements Interfaces.IQodLine2109.QtyTaken
            Get
                QtyTaken = _qtyTaken
            End Get
            Set(ByVal value As Integer)
                _qtyTaken = value
            End Set
        End Property
    End Class
End Namespace