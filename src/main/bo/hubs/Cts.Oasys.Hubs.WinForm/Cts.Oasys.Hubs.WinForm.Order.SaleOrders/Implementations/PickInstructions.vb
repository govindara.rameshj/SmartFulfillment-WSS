﻿Public Class PickInstructions

    Implements IPickInstructions

    Public Function GetDeliveryInstructions(ByVal qod As QodHeader) As String Implements IPickInstructions.GetDeliveryInstructions
        Dim sb As New StringBuilder
        For Each txt As QodText In qod.Texts.GetTexts(QodTextType.Type.DeliveryInstruction)
            sb.Append(txt.Text.Trim & Space(1))
        Next
        Return sb.ToString
    End Function

    Public Function GetPickingInstructions(ByVal qod As QodHeader) As String Implements IPickInstructions.GetPickingInstructions
        Dim PickInstructionsArea As Boolean = False
        Dim sb As New StringBuilder
        For Each txt As QodText In qod.Texts.GetTexts(QodTextType.Type.PickInstruction)
            sb.Append(txt.Text.Trim & Space(1) & vbCrLf)
        Next
        Return sb.ToString
    End Function

    Public Function SetControlsVisibleProperty() As Boolean Implements IPickInstructions.SetControlsVisibleProperty
        Return True
    End Function

    Public Function HasPickingInstructionsAnyText(ByVal PickInstructions As String) As Boolean Implements IPickInstructions.HasPickingInstructionsAnyText
        Return PickInstructions.Length > 0
    End Function

    Public Function GetFooterHeight(ByVal PickInstructions As String) As Integer Implements IPickInstructions.GetFooterHeight
        Dim result As Integer = 325
        If PickInstructions.Length = 0 Then result = 225
        Return result
    End Function

End Class
