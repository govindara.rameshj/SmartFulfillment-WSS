﻿Public Class PickInstructionsOldWay

    Implements IPickInstructions

    Public Function GetDeliveryInstructions(ByVal qod As QodHeader) As String Implements IPickInstructions.GetDeliveryInstructions
        Dim sb As New StringBuilder
        For Each txt As QodText In qod.Texts.GetTexts(QodTextType.Type.DeliveryInstruction)
            sb.Append(txt.Text.Trim & Space(1))
        Next
        Return sb.ToString
    End Function

    Public Function GetPickingInstructions(ByVal qod As QodHeader) As String Implements IPickInstructions.GetPickingInstructions
        Return String.Empty
    End Function

    Public Function SetControlsVisibleProperty() As Boolean Implements IPickInstructions.SetControlsVisibleProperty
        Return False
    End Function

    Public Function HasPickingInstructionsAnyText(ByVal PickInstructions As String) As Boolean Implements IPickInstructions.HasPickingInstructionsAnyText
        Return False
    End Function

    Public Function GetFooterHeight(ByVal PickInstructions As String) As Integer Implements IPickInstructions.GetFooterHeight
        Return 225
    End Function

End Class
