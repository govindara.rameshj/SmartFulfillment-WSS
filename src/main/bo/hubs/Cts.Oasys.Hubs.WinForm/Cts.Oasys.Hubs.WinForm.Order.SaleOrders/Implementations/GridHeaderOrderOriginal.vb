﻿Imports Cts.Oasys.Hubs.Core.Order.Qod
Imports Cts.Oasys.Hubs.Core.Common

Public Class GridHeaderOrderOriginal

    Implements IGridHeaderOrder

    Public Sub GridHeaderOrder(ByRef view As DevExpress.XtraGrid.Views.Grid.GridView, ByVal nonZeroStockPageVisible As Boolean, ByVal localPhase As String, ByVal localState As String) Implements IGridHeaderOrder.GridHeaderOrder
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.OmOrderNumber), My.Resources.Columns.OMOrderNumber, 40, 80)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.Number), My.Resources.Columns.DeliveryNumber, 55, 70)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.IsForDelivery), My.Resources.Columns.DeliveryOrder)
        If nonZeroStockPageVisible Then GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.VendaNumber))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.SellingStoreId))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.SellingStoreName))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.SellingStorePhone))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.SellingStoreOrderId), My.Resources.Columns.SellingStoreOrderId)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.DespatchSite))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.CustomerName))
        GridViewSetColumn(view, localPhase, My.Resources.Columns.DeliveryPhase)
        GridViewSetColumn(view, localState, My.Resources.Columns.DeliveryState)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.NumberStockIssues), My.Resources.Columns.StockIssues)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.IsSuspended), My.Resources.Columns.IsSuspended, 80)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.RevisionNumber))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.PhoneNumber), My.Resources.Columns.PhoneNumber)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.PhoneNumberMobile), My.Resources.Columns.PhoneMobile)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.CustomerAddress))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.DeliveryAddress))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.PhoneNumberWork), My.Resources.Columns.PhoneWork)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.CustomerEmail), My.Resources.Columns.CustomerEmail)
    End Sub

    Private Sub GridViewSetColumn(ByRef view As DevExpress.XtraGrid.Views.Grid.GridView, ByVal columnName As String, Optional ByVal caption As String = "", Optional ByVal minWidth As Integer = 0, Optional ByVal maxWidth As Integer = 0)
        view.Columns(columnName).VisibleIndex = view.VisibleColumns.Count
        If caption.Length > 0 Then view.Columns(columnName).Caption = caption
        If minWidth > 0 Then view.Columns(columnName).MinWidth = minWidth
        If maxWidth > 0 Then view.Columns(columnName).MaxWidth = maxWidth
    End Sub

    Public Function UseExtendedColours() As Boolean Implements IGridHeaderOrder.UseExtendedColours
        Return False
    End Function

    Public Sub ShowLineIssues(ByRef view As DevExpress.XtraGrid.Views.Grid.GridView, ByVal e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs, ByVal storeId As Integer) Implements IGridHeaderOrder.ShowLineIssues
        Dim deliverySource As String = view.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As QodLine) f.DeliverySource)).ToString
        Dim deliveryStore As Integer = 0
        If Integer.TryParse(deliverySource, deliveryStore) Then
            If deliveryStore = ThisStore.Id4 Then
                'check for sale line IsDependentOnOrders
                If CBool(view.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As QodLine) f.IsDependentOnOrders))) Then
                    e.Appearance.BackColor = Color.Gold
                End If

                'check for sale line IsNotFilfullable
                If CBool(view.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As QodLine) f.IsNotFulfillable))) Then
                    e.Appearance.BackColor = Color.Red
                End If

            End If
        End If
    End Sub

    Public Function ShowHeaderIssues(ByVal Qod As Core.Order.Qod.QodHeader) As Boolean Implements IGridHeaderOrder.ShowHeaderIssues
        Return False
    End Function
End Class
