﻿Public Class SetCollectionToCompleteDoesNothing
    Implements ISetCollectionToComplete

    Public Sub SetCollectionToComplete(ByVal QodHeaderActionFilter As Core.Order.Qod.QodHeaderActionFilter, ByVal StoreId As Integer) Implements ISetCollectionToComplete.SetCollectionToComplete
        For Each QodHeader As QodHeader In QodHeaderActionFilter.PositiveActionList
            QodHeader.Lines.DeliveryStatus(StoreId) = Delivery.DespatchCreated
            QodHeader.DeliveryStatus = Delivery.DespatchCreated
            QodHeader.DateDespatch = Now.Date
            QodHeader.DeliveryConfirmed = True
            QodHeader.SelfPersistTree()
        Next
    End Sub
End Class
