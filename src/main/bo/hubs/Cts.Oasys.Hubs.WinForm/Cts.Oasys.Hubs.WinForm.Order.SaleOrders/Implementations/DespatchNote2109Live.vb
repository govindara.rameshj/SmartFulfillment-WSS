﻿Imports Cts.Oasys.Hubs.WinForm.Order.SaleOrders.Interfaces

Namespace Implementations
    Public Class DespatchNote2109Live
        Implements IDespatchNote2109

        Public Function GetOrderValue(ByRef orderHeader As Core.Order.Qod.QodHeader) As Decimal Implements IDespatchNote2109.GetOrderValue

            GetOrderValue = orderHeader.MerchandiseValue
        End Function

        Public Function GetDeliveryCharge(ByRef orderHeader As Core.Order.Qod.QodHeader) As Decimal Implements IDespatchNote2109.GetDeliveryCharge

            GetDeliveryCharge = orderHeader.DeliveryCharge
        End Function
    End Class
End Namespace
