<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConfirmFailOrder
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.uxConfirmDelivery = New DevExpress.XtraEditors.SimpleButton
        Me.uxFailDelivery = New DevExpress.XtraEditors.SimpleButton
        Me.uxCancelButton = New DevExpress.XtraEditors.SimpleButton
        Me.uxQuestionLabel = New DevExpress.XtraEditors.LabelControl
        Me.SuspendLayout()
        '
        'uxConfirmDelivery
        '
        Me.uxConfirmDelivery.Location = New System.Drawing.Point(12, 47)
        Me.uxConfirmDelivery.Name = "uxConfirmDelivery"
        Me.uxConfirmDelivery.Size = New System.Drawing.Size(75, 39)
        Me.uxConfirmDelivery.TabIndex = 1
        Me.uxConfirmDelivery.Text = "Confirm"
        '
        'uxFailDelivery
        '
        Me.uxFailDelivery.Location = New System.Drawing.Point(94, 47)
        Me.uxFailDelivery.Name = "uxFailDelivery"
        Me.uxFailDelivery.Size = New System.Drawing.Size(75, 39)
        Me.uxFailDelivery.TabIndex = 2
        Me.uxFailDelivery.Text = "Fail"
        '
        'uxCancelButton
        '
        Me.uxCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxCancelButton.Location = New System.Drawing.Point(225, 47)
        Me.uxCancelButton.Name = "uxCancelButton"
        Me.uxCancelButton.Size = New System.Drawing.Size(75, 39)
        Me.uxCancelButton.TabIndex = 0
        Me.uxCancelButton.Text = "Cancel"
        '
        'uxQuestionLabel
        '
        Me.uxQuestionLabel.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxQuestionLabel.Appearance.Options.UseFont = True
        Me.uxQuestionLabel.Location = New System.Drawing.Point(13, 13)
        Me.uxQuestionLabel.Name = "uxQuestionLabel"
        Me.uxQuestionLabel.Size = New System.Drawing.Size(245, 13)
        Me.uxQuestionLabel.TabIndex = 3
        Me.uxQuestionLabel.Text = "Do you want to confirm or fail this delivery?"
        '
        'ConfirmFailOrder
        '
        Me.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.uxCancelButton
        Me.ClientSize = New System.Drawing.Size(312, 98)
        Me.Controls.Add(Me.uxQuestionLabel)
        Me.Controls.Add(Me.uxCancelButton)
        Me.Controls.Add(Me.uxFailDelivery)
        Me.Controls.Add(Me.uxConfirmDelivery)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "ConfirmFailOrder"
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Confirm or Fail Order Delivery"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents uxConfirmDelivery As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxFailDelivery As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxCancelButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxQuestionLabel As DevExpress.XtraEditors.LabelControl
End Class
