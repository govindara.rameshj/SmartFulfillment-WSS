<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ActionConfirmationForm
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.uxAcceptButton = New DevExpress.XtraEditors.SimpleButton
        Me.uxCancelButton = New DevExpress.XtraEditors.SimpleButton
        Me.uxPositiveActionGroup = New DevExpress.XtraEditors.GroupControl
        Me.uxPositiveActionGrid = New DevExpress.XtraGrid.GridControl
        Me.uxPositiveActionView = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.uxNegativeActionGroup = New DevExpress.XtraEditors.GroupControl
        Me.uxNegativeActionGrid = New DevExpress.XtraGrid.GridControl
        Me.uxNegativeActionView = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.uxTextLabel = New DevExpress.XtraEditors.LabelControl
        Me.ParentPanel = New DevExpress.XtraEditors.PanelControl
        Me.uxSplitContainer = New DevExpress.XtraEditors.SplitContainerControl
        CType(Me.uxPositiveActionGroup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxPositiveActionGroup.SuspendLayout()
        CType(Me.uxPositiveActionGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxPositiveActionView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxNegativeActionGroup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxNegativeActionGroup.SuspendLayout()
        CType(Me.uxNegativeActionGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxNegativeActionView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ParentPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ParentPanel.SuspendLayout()
        CType(Me.uxSplitContainer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxSplitContainer.SuspendLayout()
        Me.SuspendLayout()
        '
        'uxAcceptButton
        '
        Me.uxAcceptButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxAcceptButton.Enabled = False
        Me.uxAcceptButton.Location = New System.Drawing.Point(747, 664)
        Me.uxAcceptButton.Name = "uxAcceptButton"
        Me.uxAcceptButton.Size = New System.Drawing.Size(75, 37)
        Me.uxAcceptButton.TabIndex = 6
        Me.uxAcceptButton.Text = "F8 Accept"
        '
        'uxCancelButton
        '
        Me.uxCancelButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxCancelButton.Location = New System.Drawing.Point(828, 664)
        Me.uxCancelButton.Name = "uxCancelButton"
        Me.uxCancelButton.Size = New System.Drawing.Size(75, 37)
        Me.uxCancelButton.TabIndex = 7
        Me.uxCancelButton.Text = "F10 Cancel"
        '
        'uxPositiveActionGroup
        '
        Me.uxPositiveActionGroup.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxPositiveActionGroup.AppearanceCaption.Options.UseFont = True
        Me.uxPositiveActionGroup.Controls.Add(Me.uxPositiveActionGrid)
        Me.uxPositiveActionGroup.Dock = System.Windows.Forms.DockStyle.Fill
        Me.uxPositiveActionGroup.Location = New System.Drawing.Point(0, 0)
        Me.uxPositiveActionGroup.Name = "uxPositiveActionGroup"
        Me.uxPositiveActionGroup.Size = New System.Drawing.Size(887, 321)
        Me.uxPositiveActionGroup.TabIndex = 8
        Me.uxPositiveActionGroup.Text = "Action WILL be completed for these QODs"
        '
        'uxPositiveActionGrid
        '
        Me.uxPositiveActionGrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.uxPositiveActionGrid.Location = New System.Drawing.Point(2, 20)
        Me.uxPositiveActionGrid.MainView = Me.uxPositiveActionView
        Me.uxPositiveActionGrid.Name = "uxPositiveActionGrid"
        Me.uxPositiveActionGrid.Size = New System.Drawing.Size(883, 299)
        Me.uxPositiveActionGrid.TabIndex = 0
        Me.uxPositiveActionGrid.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uxPositiveActionView})
        '
        'uxPositiveActionView
        '
        Me.uxPositiveActionView.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxPositiveActionView.Appearance.HeaderPanel.Options.UseFont = True
        Me.uxPositiveActionView.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.uxPositiveActionView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uxPositiveActionView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxPositiveActionView.ColumnPanelRowHeight = 35
        Me.uxPositiveActionView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.uxPositiveActionView.GridControl = Me.uxPositiveActionGrid
        Me.uxPositiveActionView.Name = "uxPositiveActionView"
        Me.uxPositiveActionView.OptionsBehavior.KeepGroupExpandedOnSorting = False
        Me.uxPositiveActionView.OptionsCustomization.AllowColumnMoving = False
        Me.uxPositiveActionView.OptionsCustomization.AllowFilter = False
        Me.uxPositiveActionView.OptionsCustomization.AllowGroup = False
        Me.uxPositiveActionView.OptionsCustomization.AllowSort = False
        Me.uxPositiveActionView.OptionsFilter.AllowColumnMRUFilterList = False
        Me.uxPositiveActionView.OptionsFilter.AllowMRUFilterList = False
        Me.uxPositiveActionView.OptionsMenu.EnableColumnMenu = False
        Me.uxPositiveActionView.OptionsMenu.EnableFooterMenu = False
        Me.uxPositiveActionView.OptionsMenu.EnableGroupPanelMenu = False
        Me.uxPositiveActionView.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.uxPositiveActionView.OptionsSelection.EnableAppearanceFocusedRow = False
        Me.uxPositiveActionView.OptionsSelection.UseIndicatorForSelection = False
        Me.uxPositiveActionView.OptionsView.ColumnAutoWidth = False
        Me.uxPositiveActionView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.uxPositiveActionView.OptionsView.ShowGroupPanel = False
        Me.uxPositiveActionView.OptionsView.ShowIndicator = False
        '
        'uxNegativeActionGroup
        '
        Me.uxNegativeActionGroup.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxNegativeActionGroup.AppearanceCaption.Options.UseFont = True
        Me.uxNegativeActionGroup.Controls.Add(Me.uxNegativeActionGrid)
        Me.uxNegativeActionGroup.Dock = System.Windows.Forms.DockStyle.Fill
        Me.uxNegativeActionGroup.Location = New System.Drawing.Point(0, 0)
        Me.uxNegativeActionGroup.Name = "uxNegativeActionGroup"
        Me.uxNegativeActionGroup.Size = New System.Drawing.Size(887, 315)
        Me.uxNegativeActionGroup.TabIndex = 9
        Me.uxNegativeActionGroup.Text = "Action WILL NOT be completed for these QODs"
        '
        'uxNegativeActionGrid
        '
        Me.uxNegativeActionGrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.uxNegativeActionGrid.Location = New System.Drawing.Point(2, 20)
        Me.uxNegativeActionGrid.MainView = Me.uxNegativeActionView
        Me.uxNegativeActionGrid.Name = "uxNegativeActionGrid"
        Me.uxNegativeActionGrid.Size = New System.Drawing.Size(883, 293)
        Me.uxNegativeActionGrid.TabIndex = 1
        Me.uxNegativeActionGrid.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uxNegativeActionView})
        '
        'uxNegativeActionView
        '
        Me.uxNegativeActionView.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxNegativeActionView.Appearance.HeaderPanel.Options.UseFont = True
        Me.uxNegativeActionView.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.uxNegativeActionView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uxNegativeActionView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxNegativeActionView.ColumnPanelRowHeight = 35
        Me.uxNegativeActionView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.uxNegativeActionView.GridControl = Me.uxNegativeActionGrid
        Me.uxNegativeActionView.Name = "uxNegativeActionView"
        Me.uxNegativeActionView.OptionsBehavior.Editable = False
        Me.uxNegativeActionView.OptionsBehavior.KeepGroupExpandedOnSorting = False
        Me.uxNegativeActionView.OptionsCustomization.AllowColumnMoving = False
        Me.uxNegativeActionView.OptionsCustomization.AllowFilter = False
        Me.uxNegativeActionView.OptionsCustomization.AllowGroup = False
        Me.uxNegativeActionView.OptionsCustomization.AllowSort = False
        Me.uxNegativeActionView.OptionsFilter.AllowColumnMRUFilterList = False
        Me.uxNegativeActionView.OptionsFilter.AllowMRUFilterList = False
        Me.uxNegativeActionView.OptionsMenu.EnableColumnMenu = False
        Me.uxNegativeActionView.OptionsMenu.EnableFooterMenu = False
        Me.uxNegativeActionView.OptionsMenu.EnableGroupPanelMenu = False
        Me.uxNegativeActionView.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.uxNegativeActionView.OptionsSelection.EnableAppearanceFocusedRow = False
        Me.uxNegativeActionView.OptionsSelection.UseIndicatorForSelection = False
        Me.uxNegativeActionView.OptionsView.ColumnAutoWidth = False
        Me.uxNegativeActionView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.uxNegativeActionView.OptionsView.ShowGroupPanel = False
        Me.uxNegativeActionView.OptionsView.ShowIndicator = False
        Me.uxNegativeActionView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.[Default]
        '
        'uxTextLabel
        '
        Me.uxTextLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxTextLabel.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxTextLabel.Appearance.Options.UseFont = True
        Me.uxTextLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.uxTextLabel.Location = New System.Drawing.Point(18, 665)
        Me.uxTextLabel.Name = "uxTextLabel"
        Me.uxTextLabel.Size = New System.Drawing.Size(723, 36)
        Me.uxTextLabel.TabIndex = 10
        '
        'ParentPanel
        '
        Me.ParentPanel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ParentPanel.Controls.Add(Me.uxSplitContainer)
        Me.ParentPanel.Location = New System.Drawing.Point(12, 12)
        Me.ParentPanel.Name = "ParentPanel"
        Me.ParentPanel.Size = New System.Drawing.Size(891, 646)
        Me.ParentPanel.TabIndex = 11
        '
        'uxSplitContainer
        '
        Me.uxSplitContainer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxSplitContainer.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.None
        Me.uxSplitContainer.Horizontal = False
        Me.uxSplitContainer.Location = New System.Drawing.Point(2, 2)
        Me.uxSplitContainer.Name = "uxSplitContainer"
        Me.uxSplitContainer.Panel1.Controls.Add(Me.uxPositiveActionGroup)
        Me.uxSplitContainer.Panel1.MinSize = 81
        Me.uxSplitContainer.Panel1.Text = "Panel1"
        Me.uxSplitContainer.Panel2.Controls.Add(Me.uxNegativeActionGroup)
        Me.uxSplitContainer.Panel2.MinSize = 81
        Me.uxSplitContainer.Panel2.Text = "Panel2"
        Me.uxSplitContainer.Size = New System.Drawing.Size(887, 642)
        Me.uxSplitContainer.SplitterPosition = 321
        Me.uxSplitContainer.TabIndex = 0
        '
        'ActionConfirmationForm
        '
        Me.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(915, 713)
        Me.Controls.Add(Me.uxTextLabel)
        Me.Controls.Add(Me.uxCancelButton)
        Me.Controls.Add(Me.uxAcceptButton)
        Me.Controls.Add(Me.ParentPanel)
        Me.Name = "ActionConfirmationForm"
        Me.ShowIcon = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Action Confirmation Form"
        CType(Me.uxPositiveActionGroup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxPositiveActionGroup.ResumeLayout(False)
        CType(Me.uxPositiveActionGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxPositiveActionView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxNegativeActionGroup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxNegativeActionGroup.ResumeLayout(False)
        CType(Me.uxNegativeActionGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxNegativeActionView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ParentPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ParentPanel.ResumeLayout(False)
        CType(Me.uxSplitContainer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxSplitContainer.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents uxAcceptButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxCancelButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxPositiveActionGroup As DevExpress.XtraEditors.GroupControl
    Friend WithEvents uxNegativeActionGroup As DevExpress.XtraEditors.GroupControl
    Friend WithEvents uxPositiveActionGrid As DevExpress.XtraGrid.GridControl
    Friend WithEvents uxPositiveActionView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents uxNegativeActionGrid As DevExpress.XtraGrid.GridControl
    Friend WithEvents uxNegativeActionView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents uxTextLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ParentPanel As DevExpress.XtraEditors.PanelControl
    Friend WithEvents uxSplitContainer As DevExpress.XtraEditors.SplitContainerControl
End Class
