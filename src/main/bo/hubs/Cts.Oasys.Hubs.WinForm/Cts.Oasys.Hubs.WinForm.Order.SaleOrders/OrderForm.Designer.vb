﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class OrderForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.uxContactGroup = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.uxEmailText = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.uxPhoneMobileText = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.uxPhoneWorkText = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.uxPhoneHomeText = New DevExpress.XtraEditors.TextEdit()
        Me.uxAcceptButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxCancelButton = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.uxDelivery1Text = New DevExpress.XtraEditors.TextEdit()
        Me.uxDelivery3Text = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.uxDelivery2Text = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.uxDeliveryPostText = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.uxDelivery4Text = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.uxDeliveryGroup = New DevExpress.XtraEditors.GroupControl()
        Me.uxDeliverySlot = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.uxDeliveryContactNumber = New DevExpress.XtraEditors.TextEdit()
        Me.uxDeliveryContactName = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.uxDeliveryDate = New DevExpress.XtraEditors.DateEdit()
        Me.uxInstructionsList = New DevExpress.XtraEditors.LookUpEdit()
        Me.uxInstructionsGroup = New DevExpress.XtraEditors.GroupControl()
        Me.uxRemoveButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uxInstructionsText = New DevExpress.XtraEditors.TextEdit()
        Me.uxAddNewButton = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.uxDeliveryStatusCheck = New DevExpress.XtraEditors.CheckEdit()
        Me.uxDeliveryStatusLabel = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.uxResetButton = New DevExpress.XtraEditors.SimpleButton()
        Me.Err = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me.uxContactGroup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxContactGroup.SuspendLayout()
        CType(Me.uxEmailText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxPhoneMobileText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxPhoneWorkText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxPhoneHomeText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxDelivery1Text.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxDelivery3Text.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxDelivery2Text.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxDeliveryPostText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxDelivery4Text.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxDeliveryGroup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxDeliveryGroup.SuspendLayout()
        CType(Me.uxDeliveryContactNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxDeliveryContactName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxDeliveryDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxDeliveryDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxInstructionsList.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxInstructionsGroup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxInstructionsGroup.SuspendLayout()
        CType(Me.uxInstructionsText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.uxDeliveryStatusCheck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Err, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'uxContactGroup
        '
        Me.uxContactGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxContactGroup.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxContactGroup.AppearanceCaption.Options.UseFont = True
        Me.uxContactGroup.Controls.Add(Me.LabelControl4)
        Me.uxContactGroup.Controls.Add(Me.uxEmailText)
        Me.uxContactGroup.Controls.Add(Me.LabelControl3)
        Me.uxContactGroup.Controls.Add(Me.uxPhoneMobileText)
        Me.uxContactGroup.Controls.Add(Me.LabelControl2)
        Me.uxContactGroup.Controls.Add(Me.uxPhoneWorkText)
        Me.uxContactGroup.Controls.Add(Me.LabelControl1)
        Me.uxContactGroup.Controls.Add(Me.uxPhoneHomeText)
        Me.uxContactGroup.Location = New System.Drawing.Point(12, 278)
        Me.uxContactGroup.Name = "uxContactGroup"
        Me.uxContactGroup.Size = New System.Drawing.Size(397, 130)
        Me.uxContactGroup.TabIndex = 1
        Me.uxContactGroup.Text = "Customer Contact Details"
        '
        'LabelControl4
        '
        Me.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl4.Location = New System.Drawing.Point(5, 101)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(104, 20)
        Me.LabelControl4.TabIndex = 6
        Me.LabelControl4.Text = "&Email"
        '
        'uxEmailText
        '
        Me.uxEmailText.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxEmailText.Location = New System.Drawing.Point(115, 101)
        Me.uxEmailText.Name = "uxEmailText"
        Me.uxEmailText.Properties.Mask.EditMask = "([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)" & _
    "+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)"
        Me.uxEmailText.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uxEmailText.Properties.Mask.ShowPlaceHolders = False
        Me.uxEmailText.Properties.MaxLength = 50
        Me.uxEmailText.Size = New System.Drawing.Size(249, 20)
        Me.uxEmailText.TabIndex = 7
        '
        'LabelControl3
        '
        Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl3.Location = New System.Drawing.Point(5, 75)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(104, 20)
        Me.LabelControl3.TabIndex = 4
        Me.LabelControl3.Text = "&Mobile Phone Number"
        '
        'uxPhoneMobileText
        '
        Me.uxPhoneMobileText.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxPhoneMobileText.Location = New System.Drawing.Point(115, 75)
        Me.uxPhoneMobileText.Name = "uxPhoneMobileText"
        Me.uxPhoneMobileText.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None
        Me.uxPhoneMobileText.Properties.Mask.ShowPlaceHolders = False
        Me.uxPhoneMobileText.Properties.MaxLength = 15
        Me.uxPhoneMobileText.Size = New System.Drawing.Size(112, 20)
        Me.uxPhoneMobileText.TabIndex = 5
        '
        'LabelControl2
        '
        Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl2.Location = New System.Drawing.Point(5, 49)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(104, 20)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "&Work Phone Number"
        '
        'uxPhoneWorkText
        '
        Me.uxPhoneWorkText.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxPhoneWorkText.Location = New System.Drawing.Point(115, 49)
        Me.uxPhoneWorkText.Name = "uxPhoneWorkText"
        Me.uxPhoneWorkText.Properties.Mask.ShowPlaceHolders = False
        Me.uxPhoneWorkText.Properties.MaxLength = 20
        Me.uxPhoneWorkText.Size = New System.Drawing.Size(112, 20)
        Me.uxPhoneWorkText.TabIndex = 3
        '
        'LabelControl1
        '
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl1.Location = New System.Drawing.Point(5, 23)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(104, 20)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "&Home Phone Number"
        '
        'uxPhoneHomeText
        '
        Me.uxPhoneHomeText.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxPhoneHomeText.Location = New System.Drawing.Point(115, 23)
        Me.uxPhoneHomeText.Name = "uxPhoneHomeText"
        Me.uxPhoneHomeText.Properties.Mask.ShowPlaceHolders = False
        Me.uxPhoneHomeText.Properties.MaxLength = 20
        Me.uxPhoneHomeText.Size = New System.Drawing.Size(112, 20)
        Me.uxPhoneHomeText.TabIndex = 1
        '
        'uxAcceptButton
        '
        Me.uxAcceptButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxAcceptButton.Enabled = False
        Me.uxAcceptButton.Location = New System.Drawing.Point(253, 590)
        Me.uxAcceptButton.Name = "uxAcceptButton"
        Me.uxAcceptButton.Size = New System.Drawing.Size(75, 37)
        Me.uxAcceptButton.TabIndex = 5
        Me.uxAcceptButton.Text = "F8 Accept"
        '
        'uxCancelButton
        '
        Me.uxCancelButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxCancelButton.Location = New System.Drawing.Point(334, 590)
        Me.uxCancelButton.Name = "uxCancelButton"
        Me.uxCancelButton.Size = New System.Drawing.Size(75, 37)
        Me.uxCancelButton.TabIndex = 6
        Me.uxCancelButton.Text = "F10 Cancel"
        '
        'LabelControl7
        '
        Me.LabelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl7.Location = New System.Drawing.Point(6, 75)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(104, 20)
        Me.LabelControl7.TabIndex = 0
        Me.LabelControl7.Text = "Address Line &1"
        '
        'uxDelivery1Text
        '
        Me.uxDelivery1Text.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxDelivery1Text.Location = New System.Drawing.Point(131, 75)
        Me.uxDelivery1Text.Name = "uxDelivery1Text"
        Me.uxDelivery1Text.Properties.MaxLength = 30
        Me.uxDelivery1Text.Size = New System.Drawing.Size(237, 20)
        Me.uxDelivery1Text.TabIndex = 1
        '
        'uxDelivery3Text
        '
        Me.uxDelivery3Text.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxDelivery3Text.Location = New System.Drawing.Point(131, 127)
        Me.uxDelivery3Text.Name = "uxDelivery3Text"
        Me.uxDelivery3Text.Properties.MaxLength = 30
        Me.uxDelivery3Text.Size = New System.Drawing.Size(237, 20)
        Me.uxDelivery3Text.TabIndex = 5
        '
        'LabelControl8
        '
        Me.LabelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl8.Location = New System.Drawing.Point(6, 127)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(104, 20)
        Me.LabelControl8.TabIndex = 4
        Me.LabelControl8.Text = "Address &Town"
        '
        'uxDelivery2Text
        '
        Me.uxDelivery2Text.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxDelivery2Text.Location = New System.Drawing.Point(131, 101)
        Me.uxDelivery2Text.Name = "uxDelivery2Text"
        Me.uxDelivery2Text.Properties.MaxLength = 30
        Me.uxDelivery2Text.Size = New System.Drawing.Size(237, 20)
        Me.uxDelivery2Text.TabIndex = 3
        '
        'LabelControl9
        '
        Me.LabelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl9.Location = New System.Drawing.Point(6, 101)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(104, 20)
        Me.LabelControl9.TabIndex = 2
        Me.LabelControl9.Text = "Address Line &2"
        '
        'uxDeliveryPostText
        '
        Me.uxDeliveryPostText.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxDeliveryPostText.Location = New System.Drawing.Point(131, 179)
        Me.uxDeliveryPostText.Name = "uxDeliveryPostText"
        Me.uxDeliveryPostText.Properties.Mask.EditMask = "(GIR 0AA)|((([A-PR-UWYZ][0-9][0-9]?)|(([A-PR-UWYZ][A-HK-Y][0-9][0-9]?)|(([A-PR-UW" & _
    "YZ][0-9][A-HJKSTUW])|([A-PR-UWYZ][A-HK-Y][0-9][ABEHMNPRVWXY])))) [0-9][ABD-HJLNP" & _
    "-UW-Z]{2})|( ?)"
        Me.uxDeliveryPostText.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uxDeliveryPostText.Properties.Mask.ShowPlaceHolders = False
        Me.uxDeliveryPostText.Properties.MaxLength = 8
        Me.uxDeliveryPostText.Size = New System.Drawing.Size(112, 20)
        Me.uxDeliveryPostText.TabIndex = 9
        '
        'LabelControl10
        '
        Me.LabelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl10.Location = New System.Drawing.Point(6, 179)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(104, 20)
        Me.LabelControl10.TabIndex = 8
        Me.LabelControl10.Text = "&Post Code"
        '
        'uxDelivery4Text
        '
        Me.uxDelivery4Text.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxDelivery4Text.Location = New System.Drawing.Point(131, 153)
        Me.uxDelivery4Text.Name = "uxDelivery4Text"
        Me.uxDelivery4Text.Properties.MaxLength = 30
        Me.uxDelivery4Text.Size = New System.Drawing.Size(237, 20)
        Me.uxDelivery4Text.TabIndex = 7
        '
        'LabelControl15
        '
        Me.LabelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl15.Location = New System.Drawing.Point(6, 153)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(104, 20)
        Me.LabelControl15.TabIndex = 6
        Me.LabelControl15.Text = "Address Line &4"
        '
        'uxDeliveryGroup
        '
        Me.uxDeliveryGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxDeliveryGroup.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxDeliveryGroup.AppearanceCaption.Options.UseFont = True
        Me.uxDeliveryGroup.Controls.Add(Me.uxDeliverySlot)
        Me.uxDeliveryGroup.Controls.Add(Me.LabelControl14)
        Me.uxDeliveryGroup.Controls.Add(Me.uxDeliveryContactNumber)
        Me.uxDeliveryGroup.Controls.Add(Me.uxDeliveryContactName)
        Me.uxDeliveryGroup.Controls.Add(Me.LabelControl12)
        Me.uxDeliveryGroup.Controls.Add(Me.LabelControl6)
        Me.uxDeliveryGroup.Controls.Add(Me.LabelControl5)
        Me.uxDeliveryGroup.Controls.Add(Me.uxDeliveryDate)
        Me.uxDeliveryGroup.Controls.Add(Me.LabelControl7)
        Me.uxDeliveryGroup.Controls.Add(Me.uxDelivery1Text)
        Me.uxDeliveryGroup.Controls.Add(Me.LabelControl8)
        Me.uxDeliveryGroup.Controls.Add(Me.uxDelivery3Text)
        Me.uxDeliveryGroup.Controls.Add(Me.LabelControl9)
        Me.uxDeliveryGroup.Controls.Add(Me.uxDelivery4Text)
        Me.uxDeliveryGroup.Controls.Add(Me.uxDelivery2Text)
        Me.uxDeliveryGroup.Controls.Add(Me.LabelControl15)
        Me.uxDeliveryGroup.Controls.Add(Me.LabelControl10)
        Me.uxDeliveryGroup.Controls.Add(Me.uxDeliveryPostText)
        Me.uxDeliveryGroup.Location = New System.Drawing.Point(12, 12)
        Me.uxDeliveryGroup.Name = "uxDeliveryGroup"
        Me.uxDeliveryGroup.Size = New System.Drawing.Size(397, 260)
        Me.uxDeliveryGroup.TabIndex = 0
        Me.uxDeliveryGroup.Text = "Delivery Details"
        '
        'uxDeliverySlot
        '
        Me.uxDeliverySlot.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxDeliverySlot.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.uxDeliverySlot.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.uxDeliverySlot.Location = New System.Drawing.Point(131, 231)
        Me.uxDeliverySlot.Name = "uxDeliverySlot"
        Me.uxDeliverySlot.Padding = New System.Windows.Forms.Padding(1)
        Me.uxDeliverySlot.Size = New System.Drawing.Size(237, 20)
        Me.uxDeliverySlot.TabIndex = 8
        '
        'LabelControl14
        '
        Me.LabelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl14.Location = New System.Drawing.Point(6, 231)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(104, 20)
        Me.LabelControl14.TabIndex = 16
        Me.LabelControl14.Text = "Delivery Slot"
        '
        'uxDeliveryContactNumber
        '
        Me.uxDeliveryContactNumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxDeliveryContactNumber.Location = New System.Drawing.Point(131, 49)
        Me.uxDeliveryContactNumber.Name = "uxDeliveryContactNumber"
        Me.uxDeliveryContactNumber.Properties.MaxLength = 30
        Me.uxDeliveryContactNumber.Size = New System.Drawing.Size(237, 20)
        Me.uxDeliveryContactNumber.TabIndex = 15
        '
        'uxDeliveryContactName
        '
        Me.uxDeliveryContactName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxDeliveryContactName.Location = New System.Drawing.Point(131, 23)
        Me.uxDeliveryContactName.Name = "uxDeliveryContactName"
        Me.uxDeliveryContactName.Properties.MaxLength = 30
        Me.uxDeliveryContactName.Size = New System.Drawing.Size(237, 20)
        Me.uxDeliveryContactName.TabIndex = 14
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(6, 52)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(120, 13)
        Me.LabelControl12.TabIndex = 13
        Me.LabelControl12.Text = "Delivery Contact Number"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(6, 26)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(110, 13)
        Me.LabelControl6.TabIndex = 12
        Me.LabelControl6.Text = "Delivery Contact Name"
        '
        'LabelControl5
        '
        Me.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl5.Location = New System.Drawing.Point(6, 205)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(104, 20)
        Me.LabelControl5.TabIndex = 10
        Me.LabelControl5.Text = "&Delivery Date"
        '
        'uxDeliveryDate
        '
        Me.uxDeliveryDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxDeliveryDate.EditValue = Nothing
        Me.uxDeliveryDate.Location = New System.Drawing.Point(131, 205)
        Me.uxDeliveryDate.Name = "uxDeliveryDate"
        Me.uxDeliveryDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.uxDeliveryDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.uxDeliveryDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.uxDeliveryDate.Size = New System.Drawing.Size(112, 20)
        Me.uxDeliveryDate.TabIndex = 11
        '
        'uxInstructionsList
        '
        Me.uxInstructionsList.Location = New System.Drawing.Point(115, 23)
        Me.uxInstructionsList.Name = "uxInstructionsList"
        Me.uxInstructionsList.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.uxInstructionsList.Properties.NullText = ""
        Me.uxInstructionsList.Size = New System.Drawing.Size(37, 20)
        Me.uxInstructionsList.TabIndex = 1
        '
        'uxInstructionsGroup
        '
        Me.uxInstructionsGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxInstructionsGroup.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxInstructionsGroup.AppearanceCaption.Options.UseFont = True
        Me.uxInstructionsGroup.Controls.Add(Me.uxRemoveButton)
        Me.uxInstructionsGroup.Controls.Add(Me.uxInstructionsList)
        Me.uxInstructionsGroup.Controls.Add(Me.uxInstructionsText)
        Me.uxInstructionsGroup.Controls.Add(Me.uxAddNewButton)
        Me.uxInstructionsGroup.Controls.Add(Me.LabelControl11)
        Me.uxInstructionsGroup.Location = New System.Drawing.Point(12, 414)
        Me.uxInstructionsGroup.Name = "uxInstructionsGroup"
        Me.uxInstructionsGroup.Size = New System.Drawing.Size(397, 82)
        Me.uxInstructionsGroup.TabIndex = 2
        Me.uxInstructionsGroup.Text = "Delivery Instructions"
        '
        'uxRemoveButton
        '
        Me.uxRemoveButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxRemoveButton.Appearance.Options.UseTextOptions = True
        Me.uxRemoveButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxRemoveButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxRemoveButton.Location = New System.Drawing.Point(245, 49)
        Me.uxRemoveButton.Name = "uxRemoveButton"
        Me.uxRemoveButton.Size = New System.Drawing.Size(123, 26)
        Me.uxRemoveButton.TabIndex = 5
        Me.uxRemoveButton.Text = "F5 Remove Instruction"
        '
        'uxInstructionsText
        '
        Me.uxInstructionsText.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxInstructionsText.Location = New System.Drawing.Point(179, 23)
        Me.uxInstructionsText.Name = "uxInstructionsText"
        Me.uxInstructionsText.Properties.MaxLength = 100
        Me.uxInstructionsText.Size = New System.Drawing.Size(189, 20)
        Me.uxInstructionsText.TabIndex = 3
        '
        'uxAddNewButton
        '
        Me.uxAddNewButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxAddNewButton.Appearance.Options.UseTextOptions = True
        Me.uxAddNewButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxAddNewButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxAddNewButton.Location = New System.Drawing.Point(116, 49)
        Me.uxAddNewButton.Name = "uxAddNewButton"
        Me.uxAddNewButton.Size = New System.Drawing.Size(123, 26)
        Me.uxAddNewButton.TabIndex = 4
        Me.uxAddNewButton.Text = "F4 Add Instruction"
        '
        'LabelControl11
        '
        Me.LabelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl11.Location = New System.Drawing.Point(6, 23)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(104, 20)
        Me.LabelControl11.TabIndex = 0
        Me.LabelControl11.Text = "&Line"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.uxDeliveryStatusCheck)
        Me.GroupControl1.Controls.Add(Me.uxDeliveryStatusLabel)
        Me.GroupControl1.Controls.Add(Me.LabelControl13)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 502)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(397, 82)
        Me.GroupControl1.TabIndex = 3
        Me.GroupControl1.Text = "Order Status"
        '
        'uxDeliveryStatusCheck
        '
        Me.uxDeliveryStatusCheck.Location = New System.Drawing.Point(5, 49)
        Me.uxDeliveryStatusCheck.Name = "uxDeliveryStatusCheck"
        Me.uxDeliveryStatusCheck.Properties.Caption = "&Suspend Order"
        Me.uxDeliveryStatusCheck.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.uxDeliveryStatusCheck.Size = New System.Drawing.Size(127, 19)
        Me.uxDeliveryStatusCheck.TabIndex = 2
        '
        'uxDeliveryStatusLabel
        '
        Me.uxDeliveryStatusLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxDeliveryStatusLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.uxDeliveryStatusLabel.Location = New System.Drawing.Point(116, 23)
        Me.uxDeliveryStatusLabel.Name = "uxDeliveryStatusLabel"
        Me.uxDeliveryStatusLabel.Size = New System.Drawing.Size(275, 20)
        Me.uxDeliveryStatusLabel.TabIndex = 1
        Me.uxDeliveryStatusLabel.Text = "Status"
        '
        'LabelControl13
        '
        Me.LabelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl13.Location = New System.Drawing.Point(6, 23)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(104, 20)
        Me.LabelControl13.TabIndex = 0
        Me.LabelControl13.Text = "Delivery Status"
        '
        'uxResetButton
        '
        Me.uxResetButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxResetButton.Appearance.Options.UseTextOptions = True
        Me.uxResetButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxResetButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxResetButton.Location = New System.Drawing.Point(12, 590)
        Me.uxResetButton.Name = "uxResetButton"
        Me.uxResetButton.Size = New System.Drawing.Size(75, 37)
        Me.uxResetButton.TabIndex = 7
        Me.uxResetButton.Text = "F3 Reset"
        '
        'Err
        '
        Me.Err.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink
        Me.Err.ContainerControl = Me
        '
        'OrderForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(421, 639)
        Me.Controls.Add(Me.uxResetButton)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.uxInstructionsGroup)
        Me.Controls.Add(Me.uxDeliveryGroup)
        Me.Controls.Add(Me.uxCancelButton)
        Me.Controls.Add(Me.uxAcceptButton)
        Me.Controls.Add(Me.uxContactGroup)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.Name = "OrderForm"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Maintain Order"
        CType(Me.uxContactGroup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxContactGroup.ResumeLayout(False)
        CType(Me.uxEmailText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxPhoneMobileText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxPhoneWorkText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxPhoneHomeText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxDelivery1Text.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxDelivery3Text.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxDelivery2Text.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxDeliveryPostText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxDelivery4Text.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxDeliveryGroup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxDeliveryGroup.ResumeLayout(False)
        Me.uxDeliveryGroup.PerformLayout()
        CType(Me.uxDeliveryContactNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxDeliveryContactName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxDeliveryDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxDeliveryDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxInstructionsList.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxInstructionsGroup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxInstructionsGroup.ResumeLayout(False)
        CType(Me.uxInstructionsText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.uxDeliveryStatusCheck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Err, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents uxContactGroup As DevExpress.XtraEditors.GroupControl
    Public WithEvents uxAcceptButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxCancelButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxDelivery3Text As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxDelivery2Text As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxDeliveryPostText As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxDelivery4Text As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxDeliveryGroup As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxPhoneMobileText As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxPhoneWorkText As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Public WithEvents uxDeliveryDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents uxEmailText As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uxInstructionsList As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents uxInstructionsGroup As DevExpress.XtraEditors.GroupControl
    Friend WithEvents uxInstructionsText As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxAddNewButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents uxDeliveryStatusCheck As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents uxDeliveryStatusLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxResetButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Err As System.Windows.Forms.ErrorProvider
    Friend WithEvents uxRemoveButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxDeliveryContactNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uxDeliveryContactName As DevExpress.XtraEditors.TextEdit
    Public WithEvents uxPhoneHomeText As DevExpress.XtraEditors.TextEdit
    Public WithEvents uxDelivery1Text As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Public WithEvents uxDeliverySlot As DevExpress.XtraEditors.LabelControl
End Class
