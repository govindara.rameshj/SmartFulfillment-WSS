<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PickConfirmation
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PickConfirmation))
        Me.uxCancelButton = New DevExpress.XtraEditors.SimpleButton
        Me.uxAcceptButton = New DevExpress.XtraEditors.SimpleButton
        Me.ParentPanel = New DevExpress.XtraEditors.PanelControl
        Me.uxDeliveryContactNumber = New System.Windows.Forms.Label
        Me.uxDeliveryAddress = New System.Windows.Forms.Label
        Me.uxDeliveryContactName = New System.Windows.Forms.Label
        Me.uxCustomerName = New System.Windows.Forms.Label
        Me.uxDeliveryDate = New System.Windows.Forms.Label
        Me.uxOrderNumber = New System.Windows.Forms.Label
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.uxScanItemGrid = New DevExpress.XtraGrid.GridControl
        Me.uxScanItemView = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.RepositoryItemButtonEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
        Me.EditParentPanel = New DevExpress.XtraEditors.PanelControl
        Me.LabelQTY = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.uxScannedList = New DevExpress.XtraEditors.ListBoxControl
        Me.LabelMain = New DevExpress.XtraEditors.LabelControl
        Me.uxScanEntry = New DevExpress.XtraEditors.TextEdit
        Me.uxResetButton = New DevExpress.XtraEditors.SimpleButton
        Me.AxComm = New AxMSCommLib.AxMSComm
        Me.uxButtonQuantity = New DevExpress.XtraEditors.SimpleButton
        CType(Me.ParentPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ParentPanel.SuspendLayout()
        CType(Me.uxScanItemGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxScanItemView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemButtonEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EditParentPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EditParentPanel.SuspendLayout()
        CType(Me.uxScannedList, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxScanEntry.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AxComm, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'uxCancelButton
        '
        Me.uxCancelButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxCancelButton.Location = New System.Drawing.Point(705, 524)
        Me.uxCancelButton.Name = "uxCancelButton"
        Me.uxCancelButton.Size = New System.Drawing.Size(75, 37)
        Me.uxCancelButton.TabIndex = 9
        Me.uxCancelButton.Text = "F10 Cancel"
        '
        'uxAcceptButton
        '
        Me.uxAcceptButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxAcceptButton.Location = New System.Drawing.Point(624, 524)
        Me.uxAcceptButton.Name = "uxAcceptButton"
        Me.uxAcceptButton.Size = New System.Drawing.Size(75, 37)
        Me.uxAcceptButton.TabIndex = 8
        Me.uxAcceptButton.Text = "F8 Accept"
        '
        'ParentPanel
        '
        Me.ParentPanel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ParentPanel.Controls.Add(Me.uxDeliveryContactNumber)
        Me.ParentPanel.Controls.Add(Me.uxDeliveryAddress)
        Me.ParentPanel.Controls.Add(Me.uxDeliveryContactName)
        Me.ParentPanel.Controls.Add(Me.uxCustomerName)
        Me.ParentPanel.Controls.Add(Me.uxDeliveryDate)
        Me.ParentPanel.Controls.Add(Me.uxOrderNumber)
        Me.ParentPanel.Controls.Add(Me.LabelControl8)
        Me.ParentPanel.Controls.Add(Me.LabelControl7)
        Me.ParentPanel.Controls.Add(Me.LabelControl6)
        Me.ParentPanel.Controls.Add(Me.LabelControl5)
        Me.ParentPanel.Controls.Add(Me.LabelControl4)
        Me.ParentPanel.Controls.Add(Me.LabelControl3)
        Me.ParentPanel.Controls.Add(Me.uxScanItemGrid)
        Me.ParentPanel.Location = New System.Drawing.Point(201, 12)
        Me.ParentPanel.Name = "ParentPanel"
        Me.ParentPanel.Size = New System.Drawing.Size(579, 506)
        Me.ParentPanel.TabIndex = 10
        '
        'uxDeliveryContactNumber
        '
        Me.uxDeliveryContactNumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxDeliveryContactNumber.BackColor = System.Drawing.Color.Gainsboro
        Me.uxDeliveryContactNumber.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.uxDeliveryContactNumber.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxDeliveryContactNumber.Location = New System.Drawing.Point(411, 67)
        Me.uxDeliveryContactNumber.Margin = New System.Windows.Forms.Padding(100, 0, 3, 0)
        Me.uxDeliveryContactNumber.Name = "uxDeliveryContactNumber"
        Me.uxDeliveryContactNumber.Size = New System.Drawing.Size(163, 31)
        Me.uxDeliveryContactNumber.TabIndex = 19
        '
        'uxDeliveryAddress
        '
        Me.uxDeliveryAddress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxDeliveryAddress.BackColor = System.Drawing.Color.Gainsboro
        Me.uxDeliveryAddress.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.uxDeliveryAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxDeliveryAddress.Location = New System.Drawing.Point(411, 35)
        Me.uxDeliveryAddress.Margin = New System.Windows.Forms.Padding(100, 0, 3, 0)
        Me.uxDeliveryAddress.Name = "uxDeliveryAddress"
        Me.uxDeliveryAddress.Size = New System.Drawing.Size(163, 32)
        Me.uxDeliveryAddress.TabIndex = 18
        '
        'uxDeliveryContactName
        '
        Me.uxDeliveryContactName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxDeliveryContactName.BackColor = System.Drawing.Color.Gainsboro
        Me.uxDeliveryContactName.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.uxDeliveryContactName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxDeliveryContactName.Location = New System.Drawing.Point(412, 6)
        Me.uxDeliveryContactName.Margin = New System.Windows.Forms.Padding(100, 0, 3, 0)
        Me.uxDeliveryContactName.Name = "uxDeliveryContactName"
        Me.uxDeliveryContactName.Size = New System.Drawing.Size(162, 20)
        Me.uxDeliveryContactName.TabIndex = 17
        '
        'uxCustomerName
        '
        Me.uxCustomerName.BackColor = System.Drawing.Color.Gainsboro
        Me.uxCustomerName.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.uxCustomerName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxCustomerName.Location = New System.Drawing.Point(91, 67)
        Me.uxCustomerName.Margin = New System.Windows.Forms.Padding(100, 0, 3, 0)
        Me.uxCustomerName.Name = "uxCustomerName"
        Me.uxCustomerName.Size = New System.Drawing.Size(188, 31)
        Me.uxCustomerName.TabIndex = 16
        '
        'uxDeliveryDate
        '
        Me.uxDeliveryDate.BackColor = System.Drawing.Color.Gainsboro
        Me.uxDeliveryDate.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.uxDeliveryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxDeliveryDate.Location = New System.Drawing.Point(91, 35)
        Me.uxDeliveryDate.Margin = New System.Windows.Forms.Padding(100, 0, 3, 0)
        Me.uxDeliveryDate.Name = "uxDeliveryDate"
        Me.uxDeliveryDate.Size = New System.Drawing.Size(188, 20)
        Me.uxDeliveryDate.TabIndex = 15
        '
        'uxOrderNumber
        '
        Me.uxOrderNumber.BackColor = System.Drawing.Color.Gainsboro
        Me.uxOrderNumber.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.uxOrderNumber.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxOrderNumber.Location = New System.Drawing.Point(91, 6)
        Me.uxOrderNumber.Margin = New System.Windows.Forms.Padding(100, 0, 3, 0)
        Me.uxOrderNumber.Name = "uxOrderNumber"
        Me.uxOrderNumber.Size = New System.Drawing.Size(188, 20)
        Me.uxOrderNumber.TabIndex = 14
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(285, 68)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(120, 13)
        Me.LabelControl8.TabIndex = 6
        Me.LabelControl8.Text = "Delivery Contact Number"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(16, 36)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl7.TabIndex = 5
        Me.LabelControl7.Text = "Delivery Date"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(5, 68)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl6.TabIndex = 4
        Me.LabelControl6.Text = "Customer Name"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(295, 7)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(110, 13)
        Me.LabelControl5.TabIndex = 3
        Me.LabelControl5.Text = "Delivery Contact Name"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(324, 36)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(81, 13)
        Me.LabelControl4.TabIndex = 2
        Me.LabelControl4.Text = "Delivery Address"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(16, 7)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(68, 13)
        Me.LabelControl3.TabIndex = 1
        Me.LabelControl3.Text = "Order Number"
        '
        'uxScanItemGrid
        '
        Me.uxScanItemGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxScanItemGrid.Location = New System.Drawing.Point(6, 101)
        Me.uxScanItemGrid.MainView = Me.uxScanItemView
        Me.uxScanItemGrid.Name = "uxScanItemGrid"
        Me.uxScanItemGrid.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemButtonEdit1})
        Me.uxScanItemGrid.Size = New System.Drawing.Size(568, 400)
        Me.uxScanItemGrid.TabIndex = 0
        Me.uxScanItemGrid.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uxScanItemView})
        '
        'uxScanItemView
        '
        Me.uxScanItemView.Appearance.FocusedRow.BackColor = System.Drawing.Color.ForestGreen
        Me.uxScanItemView.Appearance.FocusedRow.Options.UseBackColor = True
        Me.uxScanItemView.GridControl = Me.uxScanItemGrid
        Me.uxScanItemView.Name = "uxScanItemView"
        Me.uxScanItemView.OptionsBehavior.Editable = False
        Me.uxScanItemView.OptionsBehavior.KeepGroupExpandedOnSorting = False
        Me.uxScanItemView.OptionsCustomization.AllowColumnMoving = False
        Me.uxScanItemView.OptionsCustomization.AllowColumnResizing = False
        Me.uxScanItemView.OptionsCustomization.AllowFilter = False
        Me.uxScanItemView.OptionsCustomization.AllowGroup = False
        Me.uxScanItemView.OptionsCustomization.AllowSort = False
        Me.uxScanItemView.OptionsFilter.AllowColumnMRUFilterList = False
        Me.uxScanItemView.OptionsFilter.AllowMRUFilterList = False
        Me.uxScanItemView.OptionsMenu.EnableColumnMenu = False
        Me.uxScanItemView.OptionsMenu.EnableFooterMenu = False
        Me.uxScanItemView.OptionsMenu.EnableGroupPanelMenu = False
        Me.uxScanItemView.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.uxScanItemView.OptionsSelection.MultiSelect = True
        Me.uxScanItemView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.uxScanItemView.PaintStyleName = "Skin"
        '
        'RepositoryItemButtonEdit1
        '
        Me.RepositoryItemButtonEdit1.AutoHeight = False
        Me.RepositoryItemButtonEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.RepositoryItemButtonEdit1.Name = "RepositoryItemButtonEdit1"
        Me.RepositoryItemButtonEdit1.Tag = "Scan"
        '
        'EditParentPanel
        '
        Me.EditParentPanel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.EditParentPanel.Controls.Add(Me.LabelQTY)
        Me.EditParentPanel.Controls.Add(Me.LabelControl2)
        Me.EditParentPanel.Controls.Add(Me.uxScannedList)
        Me.EditParentPanel.Controls.Add(Me.LabelMain)
        Me.EditParentPanel.Controls.Add(Me.uxScanEntry)
        Me.EditParentPanel.Location = New System.Drawing.Point(13, 13)
        Me.EditParentPanel.Name = "EditParentPanel"
        Me.EditParentPanel.Size = New System.Drawing.Size(182, 505)
        Me.EditParentPanel.TabIndex = 11
        '
        'LabelQTY
        '
        Me.LabelQTY.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.LabelQTY.Appearance.Options.UseFont = True
        Me.LabelQTY.Location = New System.Drawing.Point(129, 29)
        Me.LabelQTY.Name = "LabelQTY"
        Me.LabelQTY.Size = New System.Drawing.Size(0, 16)
        Me.LabelQTY.TabIndex = 4
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(6, 78)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(131, 16)
        Me.LabelControl2.TabIndex = 3
        Me.LabelControl2.Text = "Previously Scanned:"
        '
        'uxScannedList
        '
        Me.uxScannedList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxScannedList.Location = New System.Drawing.Point(7, 100)
        Me.uxScannedList.LookAndFeel.UseDefaultLookAndFeel = False
        Me.uxScannedList.LookAndFeel.UseWindowsXPTheme = True
        Me.uxScannedList.Name = "uxScannedList"
        Me.uxScannedList.Size = New System.Drawing.Size(170, 400)
        Me.uxScannedList.TabIndex = 2
        '
        'LabelMain
        '
        Me.LabelMain.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.LabelMain.Appearance.Options.UseFont = True
        Me.LabelMain.Location = New System.Drawing.Point(6, 6)
        Me.LabelMain.Name = "LabelMain"
        Me.LabelMain.Size = New System.Drawing.Size(106, 16)
        Me.LabelMain.TabIndex = 1
        Me.LabelMain.Text = "Scan SKU or EAN"
        '
        'uxScanEntry
        '
        Me.uxScanEntry.Location = New System.Drawing.Point(5, 28)
        Me.uxScanEntry.Name = "uxScanEntry"
        Me.uxScanEntry.Size = New System.Drawing.Size(118, 20)
        Me.uxScanEntry.TabIndex = 0
        '
        'uxResetButton
        '
        Me.uxResetButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxResetButton.Enabled = False
        Me.uxResetButton.Location = New System.Drawing.Point(543, 524)
        Me.uxResetButton.Name = "uxResetButton"
        Me.uxResetButton.Size = New System.Drawing.Size(75, 37)
        Me.uxResetButton.TabIndex = 12
        Me.uxResetButton.Text = "F3 Reset"
        '
        'AxComm
        '
        Me.AxComm.Enabled = True
        Me.AxComm.Location = New System.Drawing.Point(13, 524)
        Me.AxComm.Name = "AxComm"
        Me.AxComm.OcxState = CType(resources.GetObject("AxComm.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxComm.Size = New System.Drawing.Size(38, 38)
        Me.AxComm.TabIndex = 14
        '
        'uxButtonQuantity
        '
        Me.uxButtonQuantity.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxButtonQuantity.Location = New System.Drawing.Point(462, 525)
        Me.uxButtonQuantity.Name = "uxButtonQuantity"
        Me.uxButtonQuantity.Size = New System.Drawing.Size(75, 37)
        Me.uxButtonQuantity.TabIndex = 15
        Me.uxButtonQuantity.Text = "&Quantity"
        '
        'PickConfirmation
        '
        Me.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(792, 573)
        Me.Controls.Add(Me.uxButtonQuantity)
        Me.Controls.Add(Me.AxComm)
        Me.Controls.Add(Me.uxResetButton)
        Me.Controls.Add(Me.EditParentPanel)
        Me.Controls.Add(Me.ParentPanel)
        Me.Controls.Add(Me.uxCancelButton)
        Me.Controls.Add(Me.uxAcceptButton)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "PickConfirmation"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pick Confirmation"
        CType(Me.ParentPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ParentPanel.ResumeLayout(False)
        Me.ParentPanel.PerformLayout()
        CType(Me.uxScanItemGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxScanItemView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemButtonEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EditParentPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EditParentPanel.ResumeLayout(False)
        Me.EditParentPanel.PerformLayout()
        CType(Me.uxScannedList, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxScanEntry.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AxComm, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents uxCancelButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxAcceptButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ParentPanel As DevExpress.XtraEditors.PanelControl
    Friend WithEvents EditParentPanel As DevExpress.XtraEditors.PanelControl
    Friend WithEvents uxScanEntry As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelMain As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxScanItemGrid As DevExpress.XtraGrid.GridControl
    Friend WithEvents uxScanItemView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxScannedList As DevExpress.XtraEditors.ListBoxControl
    Friend WithEvents uxResetButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxOrderNumber As System.Windows.Forms.Label
    Friend WithEvents uxDeliveryContactName As System.Windows.Forms.Label
    Friend WithEvents uxCustomerName As System.Windows.Forms.Label
    Friend WithEvents uxDeliveryDate As System.Windows.Forms.Label
    Friend WithEvents uxDeliveryContactNumber As System.Windows.Forms.Label
    Friend WithEvents uxDeliveryAddress As System.Windows.Forms.Label
    Friend WithEvents RepositoryItemButtonEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents AxComm As AxMSCommLib.AxMSComm
    Friend WithEvents uxButtonQuantity As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelQTY As DevExpress.XtraEditors.LabelControl
End Class
