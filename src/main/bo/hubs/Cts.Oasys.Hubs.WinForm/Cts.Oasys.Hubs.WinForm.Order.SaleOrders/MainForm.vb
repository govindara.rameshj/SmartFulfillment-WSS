Imports Cts.Oasys.Hubs.Core
Imports Cts.Oasys.Hubs.Core.Order
Imports Cts.Oasys.Hubs.Core.Order.Qod
Imports Cts.Oasys.Hubs.Core.Order.Qod.State
Imports Cts.Oasys.Hubs.Core.System
Imports Cts.Oasys.Hubs.Core.System.Store
Imports System.Xml
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports System.ComponentModel
Imports DevExpress.XtraTab
Imports Cts.Oasys.Hubs.Core.Order.WebService
Imports System.IO
Imports System.Data
Imports Cts.Oasys.Core.Helpers
Imports System.Reflection
Imports WSS.BO.DataLayer.Model
Imports WSS.BO.DataLayer.Model.Entities.ExternalRequestDetails
Imports WSS.BO.DataLayer.Model.Repositories
Imports WSS.BO.DataLayer.Model.Entities.NonPersistent
Imports Cts.Oasys.Core.Net4Replacements
Imports DevExpress.XtraEditors

Public Class MainForm
    Private _storeId As Integer
    Private _localState As String = "LocalState"
    Private _localPhase As String = "LocalPhase"
    Private _localCount As String = "LocalCount"
    Private _DeliverySourceMissing As String = "DeliverySourceMissing"
    Private Const PICKCONFIRM_BUTTON As Long = 970100
    Private Const STORE8120 As Long = 3020

    Private _COLOR_PASTEL_GREEN As System.Drawing.Color = Color.PaleGreen ' System.Drawing.ColorTranslator.FromHtml("#92CCA6")
    Private _COLOR_DEFAULT_BACKCOLOR As System.Drawing.Color = Color.White
    Private _ConfirmPickingAvailable As Boolean = False
    Private _Store8120 As Integer = 0
    Private _ParentQod As QodHeader = Nothing
    Private _GridOrder As IGridHeaderOrder
    Private _GetGrouping As IGetOrderNumber
    Private _pickNoteFormInstance As IPickNoteForm
    Friend _CustomerSearch As ICustomerSearch
    Friend WithEvents _PickConfirm As IPickconformation
    Friend WithEvents _PickConfirmedStatus As IPickConfirmedStatus

    Private ReadOnly _dataLayerFactory As IDataLayerFactory

    Private _deliverySlots As Lazy(Of IEnumerable(Of DeliverySlot)) = New Lazy(Of IEnumerable(Of DeliverySlot))(Function() GetDeliverySlots())

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String, ByVal dataLayerFactory As IDataLayerFactory)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        _dataLayerFactory = dataLayerFactory
        InitializeComponent()

        If Me.AppName Is Nothing OrElse Me.AppName = String.Empty Then Me.AppName = My.Resources.Strings.AppName
        uxStartDate.EditValue = Nothing     ' Now.Date
        uxEndDate.EditValue = Nothing       ' Now.Date
        uxRePrintDespatchNote.Visible = False

        ' determine if pick confirmation functionality is available
        _ConfirmPickingAvailable = Parameter.GetBoolean(PICKCONFIRM_BUTTON)
        _PickConfirmedStatus = (New PickConfirmedStatusFactory).GetImplementation
        _PickConfirm = (New PickConfirmationFactory).GetImplementation
        btnStockEnquiry.Visible = (New StockEnquiryButtonAvailableFactory).GetImplementation()

        _GridOrder = (New GetHeaderOrderFactory).GetImplementation()
        _GetGrouping = (New GetOrderNumberFactory).FactoryGet

        _CustomerSearch = New CustomerSearch

        ' default focus to OM Order Number text box
        uxOMOrderNumber.Focus()
        SetButtonStatus()
    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        e.Handled = True

        'ref 777 : if on "om enquiry" tab then ignore F3, F5, F6, F7, F8; these buttons will be disabled, this is just a safety backup
        Select Case uxOrderTabControl.SelectedTabPage.Name
            Case uxOpenOrderTab.Name, uxAllOrderTab.Name, uxNonZeroStockTab.Name

                Select Case e.KeyData
                    Case Keys.F2 : uxRetrieveButton.PerformClick()
                    Case Keys.F3 : uxMaintainButton.PerformClick()
                    Case Keys.F4 : If btnStockEnquiry.Visible Then btnStockEnquiry.PerformClick()
                    Case Keys.F5 : uxPrintDespatchButton.PerformClick()
                    Case Keys.F6 : uxConfirmPickingButton.PerformClick()
                    Case Keys.F7
                        If uxConfirmPickingButton.Visible = True Then
                            uxRePrintDespatchNote.PerformClick()
                        Else
                            uxDespatchButton.PerformClick()
                        End If

                    Case Keys.F8 : uxDeliveredButton.PerformClick()
                    Case Keys.F9 : uxPrintButton.PerformClick()
                    Case Keys.F11 : uxResetButton.PerformClick()
                    Case Keys.F12 : uxExitButton.PerformClick()
                    Case Else
                        e.Handled = False
                End Select

            Case uxFullOrderTab.Name

                'ignore function keys F3, F5, F6, F7, F8
                Select Case e.KeyData
                    Case Keys.F2 : uxRetrieveButton.PerformClick()
                    Case Keys.F4 : If btnStockEnquiry.Visible Then btnStockEnquiry.PerformClick()
                    Case Keys.F9 : uxPrintButton.PerformClick()
                    Case Keys.F11 : uxResetButton.PerformClick()
                    Case Keys.F12 : uxExitButton.PerformClick()
                    Case Else
                        e.Handled = False
                End Select

        End Select

    End Sub

    Protected Overrides Sub DoProcessing()
        _storeId = ThisStore.Id4
        _Store8120 = Parameter.GetInteger(STORE8120)

        uxNonZeroStockTab.PageVisible = Core.System.Parameter.IsQodVendaStore
        If uxNonZeroStockTab.PageVisible = True AndAlso _storeId = _Store8120 Then
            uxOrderTabControl.SelectedTabPageIndex = 3
        End If
        RetrieveOrders()
    End Sub

    Public temp_uxRetrieveClicked As Boolean 'for fitnesse acceptance test ONLY
    Private Sub RetrieveOrders() Handles uxRetrieveButton.Click
        temp_uxRetrieveClicked = True

        Try
            Cursor = Cursors.WaitCursor

            Dim grid As GridControl = GetFocusedGrid()
            If grid.Name = uxNonZeroGrid.Name Then
                Dim ds As Data.DataSet = Qod.GetNonZeroStockOrOutstandingIbt
                grid.DataSource = ds
                grid.DataMember = ds.Tables(0).TableName

                Dim mainView As GridView = CType(grid.MainView, GridView)
                mainView.Columns(0).Caption = My.Resources.Columns.SkuNumber
                mainView.BestFitColumns()
                'uxNonZeroVarianceZero.Checked = True     'Ref 815 - Removed Checkbox as required

            Else

                If Not IsSearchAllowed() Then
                    MessageBox.Show(My.Resources.Strings.MessageCriteriaRequired, My.Resources.Strings.AppName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

                Dim qods As QodHeaderCollection = GetQods()
                grid.DataSource = qods

                GridLoadQods(grid)
                GridLoadQodLines(grid)
                GridLoadQodTexts(grid)
                GridLoadQodRefunds(grid)

                If grid.Name = uxFullOrderGrid.Name Then
                    Dim view As GridView = CType(grid.MainView, GridView)
                    If view.RowCount > 0 Then
                        view.ExpandAllGroups()
                        For rowHandle As Integer = 0 To view.RowCount - 1
                            view.SetMasterRowExpanded(rowHandle, True)
                        Next
                    End If
                End If

                If Not StringIsSomething(RunParameters) Then uxShowFulfilCheck.Checked = True

                CheckButtonEnableStatus()

                If grid.Name = uxOpenOrderGrid.Name Then
                    Dim view As GridView = CType(grid.MainView, GridView)
                    view.Columns(73).VisibleIndex = -1
                End If
            End If
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Function GetQodsFromOrderManager(ByVal grid As GridControl) As QodHeaderCollection
        Dim omOrder As Nullable(Of Integer)
        Dim storeId As Nullable(Of Integer)
        Dim storeOrder As Nullable(Of Integer)
        Dim tillOrderReference As String
        tillOrderReference = String.Empty
        If (uxFullOmOrderText.EditValue IsNot Nothing AndAlso uxFullOmOrderText.EditValue.ToString.Length > 0) Then _
            omOrder = CInt(uxFullOmOrderText.EditValue)
        If (uxFullStoreIdText.EditValue IsNot Nothing AndAlso uxFullStoreIdText.EditValue.ToString.Length > 0) Then _
            storeId = CInt(uxFullStoreIdText.EditValue)
        If (uxFullStoreOrderText.EditValue IsNot Nothing AndAlso uxFullStoreOrderText.EditValue.ToString.Length > 0) Then _
            storeOrder = CInt(uxFullStoreOrderText.EditValue)
        If (uxFullTillOrderReferenceText.EditValue IsNot Nothing AndAlso uxFullTillOrderReferenceText.EditValue.ToString.Length > 0) Then _
            tillOrderReference = CType(uxFullTillOrderReferenceText.EditValue, String)

        If omOrder.HasValue OrElse (storeId.HasValue AndAlso storeOrder.HasValue) OrElse (Not String.IsNullOrEmpty(tillOrderReference)) Then
            Dim qods As New QodHeaderCollection
            My.Application.Log.WriteEntry("Is grid.DataSource not equal to Nothing: " & (grid.DataSource IsNot Nothing).ToString)
            If grid.DataSource IsNot Nothing Then qods = CType(grid.DataSource, QodHeaderCollection)
            My.Application.Log.WriteEntry("Was Qods initiallized: " & (qods IsNot Nothing).ToString)

            Dim orderManagerService As New FullStatusService
            Dim request As OMSendFullStatusUpdateRequest = Nothing

            request = CType(orderManagerService.GetRequest, OMSendFullStatusUpdateRequest)
            request.SetFields(omOrder, storeId, storeOrder, tillOrderReference)

            Dim requestXml As String = request.Serialise
            Dim responseXml As String = orderManagerService.GetResponseXml(requestXml)

            My.Application.Log.WriteEntry("Did we get response: " & (responseXml IsNot Nothing).ToString())

            If responseXml IsNot Nothing AndAlso responseXml.Length > 0 Then

                My.Application.Log.WriteEntry("Trying to get Full Status Update Response...")

                Dim response As New OMSendFullStatusUpdateResponse
                response = CType(response.Deserialise(responseXml), OMSendFullStatusUpdateResponse)
                My.Application.Log.WriteEntry("Full Status Update Response was got.")

                My.Application.Log.WriteEntry("Did we get Full Status Update Response: " & (response IsNot Nothing).ToString())

                My.Application.Log.WriteEntry("Was the response successfull: " & response.IsSuccessful.ToString)
                If response.IsSuccessful Then
                    My.Application.Log.WriteEntry("Trying to get Qod from the Full Status Update Response...")
                    Dim qod As QodHeader = response.GetQod
                    My.Application.Log.WriteEntry("Did we get a Qod from the Full Status Update Response: " & (qod IsNot Nothing).ToString)
                    My.Application.Log.WriteEntry("Trying to add the Qod to the Qods list...")
                    If qod IsNot Nothing Then qods.Add(qod)
                    My.Application.Log.WriteEntry("Qod was successfully added to the Qods list")

                    uxFullOmOrderErrorLabel.Text = String.Empty
                    uxFullStoreIdErrorLabel.Text = String.Empty
                    uxFullStoreOrderErrorLabel.Text = String.Empty
                    uxFullTillOrderReferenceErrorLabel.Text = String.Empty
                Else
                    My.Application.Log.WriteEntry("Trying to get the Full Status Update Response error details")
                    Dim errorDetail As OMSendFullStatusUpdateResponseOrderStatusErrorDetail = response.GetErrorDetail
                    My.Application.Log.WriteEntry("Did we get he Full Status Update Response error details: " & (errorDetail IsNot Nothing).ToString)
                    If errorDetail IsNot Nothing Then

                        My.Application.Log.WriteEntry("Did we get OMOrderNumber error details: " & (errorDetail.OMOrderNumber IsNot Nothing).ToString)
                        If errorDetail.OMOrderNumber IsNot Nothing AndAlso errorDetail.OMOrderNumber.ValidationStatus IsNot Nothing Then
                            uxFullOmOrderErrorLabel.Text = errorDetail.OMOrderNumber.ValidationStatus
                        End If

                        My.Application.Log.WriteEntry("Did we get SellingStoreCode error details: " & (errorDetail.SellingStoreCode IsNot Nothing).ToString)
                        If errorDetail.SellingStoreCode IsNot Nothing AndAlso errorDetail.SellingStoreCode.ValidationStatus IsNot Nothing Then
                            uxFullStoreIdErrorLabel.Text = errorDetail.SellingStoreCode.ValidationStatus
                        End If

                        My.Application.Log.WriteEntry("Did we get SellingStoreCode error details: " & (errorDetail.SellingStoreOrderNumber IsNot Nothing).ToString)
                        If errorDetail.SellingStoreOrderNumber IsNot Nothing AndAlso errorDetail.SellingStoreOrderNumber.ValidationStatus IsNot Nothing Then
                            uxFullStoreOrderErrorLabel.Text = errorDetail.SellingStoreOrderNumber.ValidationStatus
                        End If

                        My.Application.Log.WriteEntry("Did we get TillOrderReference error details: " & (errorDetail.TillOrderReference IsNot Nothing).ToString)
                        If errorDetail.TillOrderReference IsNot Nothing AndAlso errorDetail.TillOrderReference.ValidationStatus IsNot Nothing Then
                            uxFullTillOrderReferenceErrorLabel.Text = errorDetail.TillOrderReference.ValidationStatus()
                        End If


                    End If
                End If

            End If
            My.Application.Log.WriteEntry("Response was successfully processed")
            Return qods

        End If
        Return Nothing
    End Function

    Private Overloads Function GetQods() As QodHeaderCollection
        Dim grid As GridControl = GetFocusedGrid()

        If grid IsNot Nothing Then
            Select Case grid.Name
                Case uxOpenOrderGrid.Name
                    Dim OMOrderNumber As Integer = 0
                    If Val(uxOMOrderNumber.Text) > 0 Then OMOrderNumber = CInt(uxOMOrderNumber.Text)
                    Dim SearchCriteriaOpenOrders As Qod.SaleOrderSearchParameters = _CustomerSearch.SearchCriteriaParameters( _
                                                         OMOrderNumber, _
                                                         txtPostCodeOpenOrders.Text, _
                                                         txtTelephoneNumberOpenOrders.Text, _
                                                         txtCustomerNameOpenOrders.Text,
                                                         txtOvcNumberOpenOrders.Text)

                    If _ConfirmPickingAvailable Then
                        Return _CustomerSearch.GetWarehouseOrders()
                    Else
                        Return _CustomerSearch.GetOrders(Delivery.DeliveryRequest, Delivery.DeliveredStatusFailedData)
                    End If

                Case uxAllOrderGrid.Name
                    Dim SearchCriteriaAllOrders As Qod.SaleOrderSearchParameters = _CustomerSearch.SearchCriteriaParameters( _
                                                         0, _
                                                         txtPostCodeAllOrders.Text, _
                                                         txtPhoneNumberAllOrders.Text, _
                                                         txtNameAllOrders.Text,
                                                         txtOvcNumberAllOrders.Text)
                    Return _CustomerSearch.GetOrders(uxStartDate.DateTime.Date, uxEndDate.DateTime.Date)

                Case uxFullOrderGrid.Name
                    Return GetQodsFromOrderManager(grid)

            End Select
        End If
        Return Nothing

    End Function

    Private Overloads Function GetQods(ByVal textEdit As TextEdit) As QodHeaderCollection
        Dim col As New QodHeaderCollection
        Dim searchText As String = textEdit.Text.Replace(" ", "") ' remove spaces
        If searchText.Length > 0 Then
            If textEdit Is uxOMOrderNumber Then
                Dim integerOMNumber As Integer
                If Integer.TryParse(searchText, integerOMNumber) Then
                    Dim order As QodHeader = Qod.GetForOmOrderNumber(integerOMNumber)
                    If Not order Is Nothing Then
                        col.Add(order)
                    End If
                End If
            ElseIf textEdit Is txtOvcNumberOpenOrders Then
                Dim SearchCriteria As Qod.SaleOrderSearchParameters = _CustomerSearch.SearchCriteriaParameters(0, "", "", "", searchText)
                col = _CustomerSearch.GetWarehouseOrders()
            End If
        End If
        Return col
    End Function

    Private Sub GridLoadQods(ByVal grid As GridControl)

        Dim view As GridView = CType(grid.MainView, GridView)
        For Each col As GridColumn In view.Columns
            col.VisibleIndex = -1
        Next

        view.Columns.AddField(_localState).UnboundType = DevExpress.Data.UnboundColumnType.String
        view.Columns.AddField(_localPhase).UnboundType = DevExpress.Data.UnboundColumnType.String
        view.Columns.AddField(_localCount).UnboundType = DevExpress.Data.UnboundColumnType.Integer

        _GridOrder.GridHeaderOrder(view, uxNonZeroStockTab.PageVisible, _localPhase, _localState)
        view.BestFitColumns()

        'add view sorting (if not order manager enquiry)
        If grid.Name <> uxFullOrderGrid.Name Then
            Try
                view.BeginSort()
                view.ClearGrouping()

                If StringIsSomething(RunParameters) Then
                    view.Columns(_localPhase).GroupIndex = 0
                    view.GroupedColumns(0).SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
                    _GetGrouping.RemoveGroupingByPhase(view, _localPhase)
                Else
                    If _ConfirmPickingAvailable = False Then
                        ' standard route
                        view.Columns(GetPropertyName(Function(f As QodHeader) f.DateDelivery)).GroupIndex = 0
                        view.Columns(_localPhase).GroupIndex = 1
                        _GetGrouping.RemoveGroupingByDateAndPhase(view, _localPhase)
                    Else
                        ' eStore route 
                        view.Columns(_localPhase).GroupIndex = 0
                        view.Columns(GetPropertyName(Function(f As QodHeader) f.DateDelivery)).GroupIndex = 1
                        'view.GroupedColumns(1).Visible = False
                    End If

                    view.GroupedColumns(0).SortOrder = DevExpress.Data.ColumnSortOrder.Descending
                End If
            Finally
                view.EndSort()
            End Try

            If StringIsSomething(RunParameters) Then
                Dim groupCount As Integer = GetGroupRowCount(view)
                If groupCount > 0 Then
                    For groupHandle As Integer = 1 To groupCount
                        If Not view.GetGroupRowValue(groupHandle - 1) Is Nothing Then
                            Dim text As String = view.GetGroupRowValue(groupHandle - 1).ToString.ToLower
                            If text = RunParameters.ToLower Then
                                view.SetRowExpanded(groupHandle * -1, True)
                                Exit For
                            End If
                        End If
                    Next
                End If
            End If
            view.Columns(_localPhase).Visible = False
        End If

    End Sub

    Private Sub GridLoadQodLines(ByVal grid As GridControl)

        Dim view As New GridView(grid)
        grid.LevelTree.Nodes.Add(GetPropertyName(Function(f As QodHeader) f.Lines), view)

        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.SkuNumber), My.Resources.Columns.SkuNumber, 45, 45)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.SkuDescription), My.Resources.Columns.SkuDescription, 190)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.DeliveryPhase), My.Resources.Columns.DeliveryPhase, 130)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.DeliveryStatus), My.Resources.Columns.DeliveryState, 55, 55)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.QtyOrdered), My.Resources.Columns.QtyOrdered)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.QtyTaken), My.Resources.Columns.QtyTaken)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.QtyRefunded), My.Resources.Columns.Refunded, 55)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.QtyToDeliver), My.Resources.Columns.QtyToDeliver)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.QtyOnHand), My.Resources.Columns.QtyOnHand, 50, 50)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.QtyOnOrder), My.Resources.Columns.QtyOnOrder, 55, 55)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.NextPoDateDelivery), My.Resources.Columns.NextPoDeliveryDate, 55)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.DeliverySource), My.Resources.Columns.DeliverySource, 55)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.FulfillingStoreName), My.Resources.Columns.DeliverySource, 55)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.FulfillingStorePhone), My.Resources.Columns.FulfillingStorePhone, 55)

        view.Columns(GetPropertyName(Function(f As QodLine) f.NextPoDateDelivery)).Visible = (grid.Name = uxOpenOrderGrid.Name)
        If grid.Name <> uxFullOrderGrid.Name Then
            view.Columns(GetPropertyName(Function(f As QodLine) f.DeliverySource)).FilterInfo = New ColumnFilterInfo("[" & GetPropertyName(Function(f As QodLine) f.DeliverySource) & "]=" & _storeId)
        End If

        view.Appearance.Assign(grid.MainView.Appearance)
        view.OptionsView.ShowGroupPanel = False
        view.OptionsBehavior.Editable = False
        view.ColumnPanelRowHeight = 35
        view.OptionsFilter.AllowFilterEditor = False
        view.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.ShowAlways
        view.OptionsView.ShowIndicator = False
        view.OptionsSelection.EnableAppearanceFocusedCell = False
        view.OptionsSelection.EnableAppearanceFocusedRow = False
        view.FocusRectStyle = DrawFocusRectStyle.None

        AddHandler view.RowStyle, AddressOf uxViewLines_RowStyle

    End Sub

    Private Sub GridLoadQodTexts(ByVal grid As GridControl)

        Dim view As New GridView(grid)
        grid.LevelTree.Nodes.Add(GetPropertyName(Function(f As QodHeader) f.Texts), view)

        GridViewAddColumn(view, GetPropertyName(Function(f As QodText) f.TextType), "", 150, 150)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodText) f.Number), "", 80, 80)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodText) f.Text))

        view.Appearance.Assign(grid.MainView.Appearance)
        view.OptionsView.ShowGroupPanel = False
        view.OptionsBehavior.Editable = False
        view.ColumnPanelRowHeight = 35
        view.OptionsFilter.AllowFilterEditor = False
        view.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Never
        view.OptionsView.ShowIndicator = False
        view.OptionsSelection.EnableAppearanceFocusedCell = False
        view.OptionsSelection.EnableAppearanceFocusedRow = False
        view.FocusRectStyle = DrawFocusRectStyle.None

    End Sub

    Private Sub GridLoadQodRefunds(ByVal grid As GridControl)

        Dim view As New GridView(grid)
        grid.LevelTree.Nodes.Add(GetPropertyName(Function(f As QodHeader) f.Refunds), view)

        GridViewAddColumn(view, GetPropertyName(Function(f As QodRefund) f.SkuNumber), My.Resources.Columns.SkuNumber)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodRefund) f.SkuDescription), My.Resources.Columns.SkuDescription)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodRefund) f.RefundStoreId))
        GridViewAddColumn(view, GetPropertyName(Function(f As QodRefund) f.RefundDate))
        GridViewAddColumn(view, GetPropertyName(Function(f As QodRefund) f.RefundTill))
        GridViewAddColumn(view, GetPropertyName(Function(f As QodRefund) f.RefundTransaction))
        GridViewAddColumn(view, GetPropertyName(Function(f As QodRefund) f.QtyReturned))
        GridViewAddColumn(view, GetPropertyName(Function(f As QodRefund) f.QtyCancelled))
        GridViewAddColumn(view, GetPropertyName(Function(f As QodRefund) f.ValueRefunded))

        view.Appearance.Assign(grid.MainView.Appearance)
        view.OptionsView.ShowGroupPanel = False
        view.OptionsBehavior.Editable = False
        view.ColumnPanelRowHeight = 35
        view.OptionsFilter.AllowFilterEditor = False
        view.OptionsView.ShowIndicator = False
        view.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Never
        view.OptionsSelection.EnableAppearanceFocusedCell = False
        view.OptionsSelection.EnableAppearanceFocusedRow = False
        view.FocusRectStyle = DrawFocusRectStyle.None

    End Sub

    Private Sub GridViewSetColumn(ByRef view As GridView, ByVal columnName As String, Optional ByVal caption As String = "", Optional ByVal minWidth As Integer = 0, Optional ByVal maxWidth As Integer = 0)
        view.Columns(columnName).VisibleIndex = view.VisibleColumns.Count
        If caption.Length > 0 Then view.Columns(columnName).Caption = caption
        If minWidth > 0 Then view.Columns(columnName).MinWidth = minWidth
        If maxWidth > 0 Then view.Columns(columnName).MaxWidth = maxWidth
    End Sub

    Private Sub GridViewAddColumn(ByRef view As GridView, ByVal columnName As String, Optional ByVal caption As String = "", Optional ByVal minWidth As Integer = 0, Optional ByVal maxWidth As Integer = 0)
        view.Columns.AddField(columnName).VisibleIndex = view.VisibleColumns.Count
        If caption.Length > 0 Then view.Columns(columnName).Caption = caption
        If minWidth > 0 Then view.Columns(columnName).MinWidth = minWidth
        If maxWidth > 0 Then view.Columns(columnName).MaxWidth = maxWidth
    End Sub

    Private Sub SetButtonStatus() Handles uxAllOrderView.SelectionChanged, uxOpenOrderView.SelectionChanged
        Dim grid As GridControl = GetFocusedGrid()
        Dim qods As QodHeaderCollection = GetSelectedQods(grid)
        _PickConfirmedStatus.SetButtonStatus(qods, _ConfirmPickingAvailable)
    End Sub

    Private Sub uxView_CustomUnboundColumnData(ByVal sender As Object, ByVal e As CustomColumnDataEventArgs) Handles uxOpenOrderView.CustomUnboundColumnData

        If e.RowHandle = GridControl.InvalidRowHandle Then Exit Sub
        If e.ListSourceRowIndex = GridControl.InvalidRowHandle Then Exit Sub

        'get order in question
        Dim view As GridView = CType(sender, GridView)
        Dim qods As QodHeaderCollection = CType(view.GridControl.DataSource, QodHeaderCollection)
        Dim qod As QodHeader = qods(e.ListSourceRowIndex)

        If uxShowFulfilCheck.Checked Then
            Dim status As Delivery = qod.Lines.DeliveryStatus(_storeId)
            Select Case e.Column.FieldName
                Case _localState : e.Value = DeliveryDescription.GetDescription(status)
                Case _localPhase : e.Value = _PickConfirmedStatus.GetPhase(status)
                Case _localCount : e.Value = qod.Lines.CountDeliverySource(_storeId)
            End Select
        Else
            Select Case e.Column.FieldName
                Case _localState : e.Value = qod.DeliveryStateDescription
                Case _localPhase : e.Value = qod.DeliveryStatePhase
                Case _localCount : e.Value = qod.Lines.Count
            End Select
        End If

    End Sub

    Private Sub uxView_RowStyle(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles uxOpenOrderView.RowStyle, uxAllOrderView.RowStyle

        If e.RowHandle < 0 Then Exit Sub

        Dim view As GridView = CType(sender, GridView)
        Dim qods As QodHeaderCollection = CType(view.GridControl.DataSource, QodHeaderCollection)
        Dim qod As QodHeader = qods(view.GetDataSourceRowIndex(e.RowHandle))
        Dim col As GridColumn = view.Columns(_localState)

        If _GridOrder.UseExtendedColours() And qod.IsForDelivery = False Then
            e.Appearance.BackColor = Color.Plum
        End If

        If qod.ExtendedLeadTime Then
            e.Appearance.BackColor = Color.Cyan
        End If

        If qod.IsDeliveryFailure Then
            e.Appearance.ForeColor = Color.Red
        End If

        If _GridOrder.UseExtendedColours() Then
            If _GridOrder.ShowHeaderIssues(qod) Then
                e.Appearance.BackColor = Color.Red
                If e.Appearance.ForeColor = Color.Red Then
                    e.Appearance.ForeColor = Color.White
                End If
            End If
        End If

    End Sub

    Private Sub uxViewLines_RowStyle(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs)

        'check if any rows loaded first
        If e.RowHandle < 0 Then Exit Sub

        'get view and sale order line for this row
        Dim view As GridView = CType(sender, GridView)


        'if delivery charge item then exit
        If CBool(view.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As QodLine) f.IsDeliveryChargeItem))) Then
            Exit Sub
        End If

        'get delivery source and try to convert to integer
        If view.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As QodLine) f.DeliverySource)) Is Nothing Then
            Exit Sub
        End If

        _GridOrder.ShowLineIssues(view, e, _storeId)

    End Sub

    Private Sub CheckButtonEnableStatus() Handles uxOrderTabControl.SelectedPageChanged, uxFullOmOrderText.EditValueChanged, uxFullStoreIdText.EditValueChanged, uxFullStoreOrderText.EditValueChanged, uxFullTillOrderReferenceText.EditValueChanged

        SetButtonStatus()
        uxRetrieveButton.Enabled = False
        uxPrintButton.Enabled = False


        'ref 777 : reset button visibility incase user is coming from the om enquiry tab
        uxMaintainButton.Visible = True         'F3
        uxPrintDespatchButton.Visible = True    'F5
        uxConfirmPickingButton.Visible = True   'F6
        uxRePrintDespatchNote.Visible = True    'F7
        uxDespatchButton.Visible = True         'F7
        uxDeliveredButton.Visible = True        'F8
        ConfirmPickingAvailableButtonVisibility(_ConfirmPickingAvailable)


        'check which tab is open
        Dim rowCount As Integer = 0
        Select Case uxOrderTabControl.SelectedTabPage.Name
            Case uxOpenOrderTab.Name
                rowCount = uxOpenOrderView.RowCount
                uxRetrieveButton.Enabled = True
                txtPostCodeOpenOrders.Focus()

            Case uxAllOrderTab.Name
                uxRetrieveButton.Enabled = True
                uxPrintDespatchButton.Enabled = False
                rowCount = uxAllOrderView.RowCount
                txtPostCodeAllOrders.Focus()

            Case uxFullOrderTab.Name
                uxPrintDespatchButton.Enabled = False
                rowCount = uxFullOrderView.RowCount
                If ((uxFullOmOrderText.EditValue IsNot Nothing) AndAlso (uxFullOmOrderText.EditValue.ToString.Length > 0) _
                    AndAlso ((uxFullStoreOrderText.EditValue Is Nothing) OrElse (uxFullStoreOrderText.EditValue.ToString.Length = 0)) _
                    AndAlso ((uxFullStoreIdText.EditValue Is Nothing) OrElse (uxFullStoreIdText.EditValue.ToString.Length = 0)) _
                    AndAlso ((uxFullTillOrderReferenceText.EditValue Is Nothing) OrElse (uxFullTillOrderReferenceText.EditValue.ToString.Length = 0))) _
                OrElse ((uxFullStoreIdText.EditValue IsNot Nothing) AndAlso (uxFullStoreIdText.EditValue.ToString.Length > 0) _
                        AndAlso (uxFullStoreOrderText.EditValue IsNot Nothing) AndAlso (uxFullStoreOrderText.EditValue.ToString.Length > 0) _
                        AndAlso ((uxFullTillOrderReferenceText.EditValue Is Nothing) OrElse (uxFullTillOrderReferenceText.EditValue.ToString.Length = 0)) _
                        AndAlso ((uxFullOmOrderText.EditValue Is Nothing) OrElse (uxFullOmOrderText.EditValue.ToString.Length = 0))) _
                OrElse ((uxFullTillOrderReferenceText.EditValue IsNot Nothing) AndAlso (uxFullTillOrderReferenceText.EditValue.ToString.Length > 0) _
                        AndAlso ((uxFullStoreIdText.EditValue Is Nothing) OrElse (uxFullStoreIdText.EditValue.ToString.Length = 0)) _
                        AndAlso ((uxFullOmOrderText.EditValue Is Nothing) OrElse (uxFullOmOrderText.EditValue.ToString.Length = 0)) _
                        AndAlso ((uxFullStoreOrderText.EditValue Is Nothing) OrElse (uxFullStoreOrderText.EditValue.ToString.Length = 0))) Then
                    uxRetrieveButton.Enabled = True
                End If

                'ref 777 : if on "om enquiry" tab then hide following buttons
                uxMaintainButton.Visible = False         'F3
                uxPrintDespatchButton.Visible = False    'F5
                uxConfirmPickingButton.Visible = False   'F6
                uxRePrintDespatchNote.Visible = False    'F7
                uxDespatchButton.Visible = False         'F7
                uxDeliveredButton.Visible = False        'F8

            Case uxNonZeroStockTab.Name
                uxPrintDespatchButton.Enabled = False
                rowCount = uxNonZeroView.RowCount
                uxRetrieveButton.Enabled = True
        End Select

        uxPrintButton.Enabled = (rowCount > 0) AndAlso _ConfirmPickingAvailable = False

    End Sub

    Private Sub uxShowFulfilCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxShowFulfilCheck.CheckedChanged

        'check which tab is open to get grid (and view) in question
        Dim grid As GridControl = GetFocusedGrid()
        If grid Is Nothing Then Exit Sub
        Dim view As GridView = CType(grid.MainView, GridView)

        If uxShowFulfilCheck.Checked Then
            view.Columns(_localCount).FilterInfo = New ColumnFilterInfo("[" & _localCount & "]>0 OR [" & _localPhase & "] Like '1%' OR [" & _
                                                                        _localPhase & "] Like '2%' And [" & _DeliverySourceMissing & "] = True")
        Else
            view.Columns(_localCount).ClearFilter()
        End If

        'Now make sure the correct buttons are showing
        SetButtonStatus()

    End Sub

    Public Function GetFocusedGrid() As GridControl
        For Each ctl As Control In uxOrderTabControl.SelectedTabPage.Controls
            If TypeOf ctl Is GridControl Then Return CType(ctl, GridControl)
        Next
        Return Nothing
    End Function

    Private Function GetRowCount(ByVal grid As GridControl) As Integer
        Dim view As GridView = CType(grid.MainView, GridView)
        Return view.RowCount
    End Function

    Private Function GetSelectedQods(ByVal grid As GridControl) As QodHeaderCollection
        Dim qods As New QodHeaderCollection
        If grid.Name <> uxNonZeroGrid.Name Then
            Dim gridQods As QodHeaderCollection = CType(grid.DataSource, QodHeaderCollection)
            Dim view As GridView = CType(grid.MainView, GridView)

            If view.DataRowCount > 0 Then
                For Each rowHandle As Integer In view.GetSelectedRows
                    If rowHandle < 0 Then Continue For
                    Dim qod As QodHeader = gridQods(view.GetDataSourceRowIndex(rowHandle))
                    qods.Add(qod)
                Next
            End If
        End If
        Return qods

    End Function

    Private Function GetUnprintedPickNoteQods(ByVal grid As GridControl) As QodHeaderCollection
        Dim qods As New QodHeaderCollection
        If grid.Name <> uxNonZeroGrid.Name Then
            Dim allqods As QodHeaderCollection = CType(grid.DataSource, QodHeaderCollection)
            For Each qod As QodHeader In allqods
                If (Not qod.IsPrinted) AndAlso (Not qod.IsSuspended) AndAlso (qod.IsAwaitingDespatch OrElse qod.IsAwaitingPicking) Then qods.Add(qod)
            Next
        End If
        Return qods
    End Function

    Private Function GetGroupRowCount(ByVal view As GridView) As Integer
        For index As Integer = 0 To Integer.MinValue Step -1
            If Not view.IsValidRowHandle(index) Then Return ((index + 1) * -1)
        Next
        Return 0
    End Function

    Private Sub ResetFullOmOrderErrorLabel() Handles uxFullOmOrderText.EditValueChanged
        uxFullOmOrderErrorLabel.Text = String.Empty
    End Sub

    Private Sub ResetFullStoreErrorLabel() Handles uxFullStoreIdText.EditValueChanged, uxFullStoreOrderText.EditValueChanged
        uxFullStoreIdErrorLabel.Text = String.Empty
        uxFullStoreOrderErrorLabel.Text = String.Empty
    End Sub

    Public Shared Sub HandleChanges(qodPrevious As QodHeader, qodCurrent As QodHeader, dataLayerFactory As IDataLayerFactory)
        Dim dateDeliveryInfo As PropertyInfo = PropertyUtils.GetPropertyInfo(Function(f As QodHeader) f.DateDelivery)
        Dim deliverySlotInfo As PropertyInfo = PropertyUtils.GetPropertyInfo(Function(f As QodHeader) f.RequiredDeliverySlotID)

        Dim trackingProperties As List(Of PropertyInfo) = New List(Of PropertyInfo) From {
            dateDeliveryInfo,
            deliverySlotInfo,
            PropertyUtils.GetPropertyInfo(Function(f As QodHeader) f.PhoneNumberMobile),
            PropertyUtils.GetPropertyInfo(Function(f As QodHeader) f.PhoneNumber),
            PropertyUtils.GetPropertyInfo(Function(f As QodHeader) f.PhoneNumberHome),
            PropertyUtils.GetPropertyInfo(Function(f As QodHeader) f.PhoneNumberWork),
            PropertyUtils.GetPropertyInfo(Function(f As QodHeader) f.CustomerName),
            PropertyUtils.GetPropertyInfo(Function(f As QodHeader) f.CustomerPostcode),
            PropertyUtils.GetPropertyInfo(Function(f As QodHeader) f.DeliveryAddress1),
            PropertyUtils.GetPropertyInfo(Function(f As QodHeader) f.DeliveryAddress2),
            PropertyUtils.GetPropertyInfo(Function(f As QodHeader) f.DeliveryAddress3),
            PropertyUtils.GetPropertyInfo(Function(f As QodHeader) f.DeliveryAddress4)
        }

        Dim changesHolder = New ChangesHolder(Of QodHeader)(qodPrevious, qodCurrent, trackingProperties)
        If changesHolder.HasChanges Then
            Dim header As QodHeader = changesHolder.CurrentState()
            If header.IsForDelivery Then

                Dim repo = dataLayerFactory.Create(Of IExternalRequestRepository)()

                'capacity
                If header.WasCreatedInStore AndAlso _
                    (changesHolder.HasChange(dateDeliveryInfo) OrElse changesHolder.HasChange(deliverySlotInfo)) Then

                    header.GenerateAllocateDeallocateUpdateCapacityRequest( _
                        repo, changesHolder.PreviousState.DateDelivery.Value, changesHolder.PreviousState.RequiredDeliverySlotID, _
                        header.DateDelivery.Value, header.RequiredDeliverySlotID)
                End If

                'consignment (do not notify if only slot was changed)
                changesHolder.TrackingProperties.Remove(deliverySlotInfo)
                If changesHolder.HasChanges AndAlso Parameter.ShouldNotifyDeliveryService Then
                    header.GenerateUpdateConsignmentRequest(repo)
                End If
            End If
        End If
    End Sub

    Private Sub uxMaintainButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxMaintainButton.Click

        Dim grid As GridControl = GetFocusedGrid()
        Dim view As GridView = CType(grid.MainView, GridView)
        Dim _qods As QodHeaderCollection = CType(grid.DataSource, QodHeaderCollection)
        Dim _qod As QodHeader = _qods(view.GetDataSourceRowIndex(view.FocusedRowHandle))

        Trace.WriteLine("About to Cloned Data.")
        Dim _tempqod As QodHeader = _qod.Clone
        Trace.WriteLine("Cloned Data.")
        Using form As New OrderForm(_tempqod, _deliverySlots.Value)
            If form.ShowDialog = DialogResult.OK Then
                Dim qodOriginal As QodHeader = _qod.Clone
                _qods(view.GetDataSourceRowIndex(view.FocusedRowHandle)) = _tempqod
                _tempqod.SelfPersistTree()

                HandleChanges(qodOriginal, _tempqod, _dataLayerFactory)

                grid.RefreshDataSource()
                view.CollapseMasterRow(view.FocusedRowHandle)
                view.ExpandMasterRow(view.FocusedRowHandle)
                SetButtonStatus()
            End If
        End Using
        'End If

    End Sub

    Private Sub uxPrintDespatchButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxPrintDespatchButton.Click

        Dim grid As GridControl = GetFocusedGrid()
        Dim qods As QodHeaderCollection = GetSelectedQods(grid)
        Dim printCount As Integer = -2147483648  'Set this to the lowest value so we do not show a number of prints dialog when not needed

        'First check the database to see if anything else (till, webservice, etc) has updated the data while we've been making tea!
        Dim ConfigXml As XmlNode = ConfigXMLDoc.SelectSingleNode("Configuration/DataCheck/PrintPickNote")
        For Each Header As QodHeader In qods
            If Header.HasChanged(ConfigXml) Then
                MessageBox.Show(My.Resources.Strings.DataChanged, "Data has changed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                uxRetrieveButton.PerformClick()
                Exit Sub
            End If
        Next

        'Check to see if more than one order is selected
        If qods.Count > 1 Then
            printCount = 0  ' Set this to 0 now as we want to show a number of prints dialog when done
        End If

        'Now we must check the action statuses and show the correct dialog, if needed
        'Firstly including Qods that have already been printed and could be printed again as well as Qods that have not yet been printed and are ready
        'CR0038(Point 2) MO'C 19/07/2011: This has been changed to allow reprinting of a pick note at failed delivery Status (800-899)
        'Dim QodHeaderActionFilterFull As New QodHeaderActionFilter(qods, Delivery.IbtOutAllStock, Delivery.PickingStatusOk, _storeId.ToString)
        Dim QodHeaderActionFilterFull As New QodHeaderActionFilter(qods, Delivery.IbtOutAllStock, Delivery.UndeliveredStatusOk, _storeId.ToString)

        'Secondly including Qods that have not yet been printed and are ready to be printed
        Dim QodHeaderActionFilter As New QodHeaderActionFilter(qods, Delivery.IbtOutAllStock, Delivery.ReceiptStatusOk, _storeId.ToString)
        'Check if any qods are in the non-print list or if any qods have already been printed
        If QodHeaderActionFilterFull.NegativeActionList.Count > 0 OrElse _
        (qods.Count > 1 And QodHeaderActionFilterFull.PositiveActionList.Count > QodHeaderActionFilter.PositiveActionList.Count) Then
            'Show the dialog and allow the user to select qods for printing and whether to continue or not
            Using Form As New ActionConfirmationForm(QodHeaderActionFilterFull, ActionFormBehaviour.PrintDialog)
                If Not Form.ShowDialog = DialogResult.OK Then
                    Exit Sub
                End If
            End Using
        End If


        Dim pickFactory As New PickNoteFormFactory
        'Now we must print the positive action QODs
        For Each qod As QodHeader In QodHeaderActionFilterFull.PositiveActionList

            'If there was only one selected initially then the rules must be applied to that one (the dialog would not have been called)
            If qods.Count = 1 Then
                If qod.IsSuspended Then Continue For
                If qod.IsPrinted AndAlso YesNoWarning(My.Resources.Strings.PickingNotePrinted) = DialogResult.No Then Continue For
            End If

            'Check the local store id
            Using store As Store = AllStores.GetStore(qod.SellingStoreId)
                If store Is Nothing Then
                    DisplayWarning(String.Format(My.Resources.Strings.NoStoreExists, qod.SellingStoreId))
                Else
                    pickFactory.Initialise(qod, store, _storeId)
                    _pickNoteFormInstance = pickFactory.GetImplementation
                    'Print the Qods and update the flag and counters as required
                    For NoCopies As Integer = 0 To 1
                        _pickNoteFormInstance.Print()
                        printCount += 1
                        If Not qod.IsPrinted Then
                            qod.IsPrinted = True
                        Else
                            qod.NumberReprints += 1
                        End If

                    Next

                    'Live store issue reprinting of the picking note caused a reset of the header to 500
                    'This caused an error 530, as the lines were not included in the xml for a status
                    'notification.
                    If qod.Lines.DeliveryStatus(_storeId) < Delivery.PickingStatusOk Then
                        'Update the delivery status!
                        qod.Lines.DeliveryStatus(_storeId) = Delivery.PickingListCreated
                        qod.DeliveryStatus = Delivery.PickingListCreated
                    End If
                    qod.SelfPersistTree()
                End If
            End Using
        Next

        'If this was a "print multiple" action then tell the user how many pick notes they should lift from the printer
        If printCount > -1 Then
            If printCount = 0 Then
                FriendlyMessage(My.Resources.Strings.PrintPickNotesNone)
            ElseIf printCount = 1 Then
                FriendlyMessage(My.Resources.Strings.PrintPickNotesOne)
            Else
                FriendlyMessage(String.Format(My.Resources.Strings.PrintPickNotesNumber, printCount))
            End If
        End If

        'Clear the action filter, as it is no longer needed!
        QodHeaderActionFilter = Nothing
        grid.RefreshDataSource()
        Dim view As GridView = CType(grid.MainView, GridView)
        For Each rowHandle As Integer In view.GetSelectedRows
            If rowHandle < 0 Then Continue For
            view.CollapseMasterRow(rowHandle)
            view.ExpandMasterRow(rowHandle)
        Next
        SetButtonStatus()

    End Sub

    ''' <summary>
    ''' Controls what happens when the despatch button or hotkey (F6) is pressed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <history>
    ''' <updated author="Charles McMahon" date="07/10/2010"></updated>
    ''' </history>
    ''' <remarks>
    ''' Creates a QodHeaderActionFilter to filter Qods that are not ready to be despatched.  Then calls the ActionConfirmationForm to allow
    ''' the user to de-select Qods for despatching.
    ''' </remarks>
    Private Sub uxDespatchButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxDespatchButton.Click

        Dim grid As GridControl = GetFocusedGrid()
        Dim qods As QodHeaderCollection = GetSelectedQods(grid)

        'First check the database to see if anything else (till, webservice, etc) has updated the data while we've been making tea!
        Dim ConfigXml As XmlNode = ConfigXMLDoc.SelectSingleNode("Configuration/DataCheck/DespatchOrder")
        For Each Header As QodHeader In qods
            If Header.DeliveryStatus < _PickConfirmedStatus.StateBeforeDespatch Then ' Qod.State.Delivery.PickingStatusOk Then
                MessageBox.Show("OM has not confirmed Picking. Please wait 10 minutes and try again.", "Invalid Delivery Status", MessageBoxButtons.OK)
                RetrieveOrders()
                Exit Sub
            End If

            If Header.HasChanged(ConfigXml) Then
                MessageBox.Show(My.Resources.Strings.DataChanged, "Data has changed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                uxRetrieveButton.PerformClick()
                Exit Sub
            End If
        Next

        'Now we must check the action statuses and show the correct dialog, if needed
        Dim QodHeaderActionFilter As New QodHeaderActionFilter(qods, CType(_PickConfirmedStatus.StateCreatedBeforeDespatch, Delivery), CType(_PickConfirmedStatus.StateBeforeDespatch, Delivery), _storeId.ToString)
        Dim TempActionFilter As New QodHeaderActionFilter(qods, Delivery.UndeliveredCreated, Delivery.UndeliveredStatusOk, _storeId.ToString)
        QodHeaderActionFilter.Union(TempActionFilter)
        'Check if any qods are in the negative action list
        If QodHeaderActionFilter.NegativeActionList.Count > 0 Then
            'Show the dialog and allow the user to select whether to continue or not
            Using Form As New ActionConfirmationForm(QodHeaderActionFilter, ActionFormBehaviour.DespatchDialog)
                If Not Form.ShowDialog = DialogResult.OK Then
                    Exit Sub
                End If
            End Using
        Else
            'Check the number of qods that were selected and confirm if the user wants to continue
            If qods.Count = 1 Then
                If Not MessageBox.Show(My.Resources.Strings.YesNoDespatch, Me.AppName, MessageBoxButtons.YesNo) = DialogResult.Yes Then
                    Exit Sub
                End If
            Else
                If Not MessageBox.Show(My.Resources.Strings.YesNoDespatchMultiple, Me.AppName, MessageBoxButtons.YesNo) = DialogResult.Yes Then
                    Exit Sub
                End If
            End If
        End If

        'If we get this far then we are good to go, so set all the relevant lines to despatched!
        Dim factory As ISetCollectionToComplete = (New SetCollectionToCompleteFactory).FactoryGet
        factory.SetCollectionToComplete(QodHeaderActionFilter, _storeId)

        'Clear the filter as it is no longer required!
        QodHeaderActionFilter = Nothing
        grid.RefreshDataSource()
        Dim view As GridView = CType(grid.MainView, GridView)
        For Each rowHandle As Integer In view.GetSelectedRows
            If rowHandle < 0 Then Continue For
            view.CollapseMasterRow(rowHandle)
            view.ExpandMasterRow(rowHandle)
        Next
        SetButtonStatus()

    End Sub

    Private Sub uxConfirmPickingButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxConfirmPickingButton.Click
        Dim grid As GridControl = GetFocusedGrid()
        Dim qods As QodHeaderCollection = GetSelectedQods(grid)

        If _PickConfirm.PickConfirm(qods) Then
            grid.RefreshDataSource()
            Dim view As GridView = CType(grid.MainView, GridView)
            For Each rowHandle As Integer In view.GetSelectedRows
                If rowHandle < 0 Then Continue For
                view.CollapseMasterRow(rowHandle)
                view.ExpandMasterRow(rowHandle)
            Next
            SetButtonStatus()
        End If

    End Sub

    Private Sub DespatchOrder(ByVal qods As QodHeaderCollection, ByRef shouldReturn As Boolean)
        shouldReturn = False
        Dim QodHeaderActionFilter As New QodHeaderActionFilter(qods, Delivery.DespatchCreated, Delivery.DespatchStatusOk, _storeId.ToString)
        'We should always show the dialog when more than one QOD is selected
        If qods.Count > 1 Then

            'Check Delivery Status of order first to see if we can continue with confirmation
            For Each QodHeader As QodHeader In QodHeaderActionFilter.PositiveActionList
                'Ref 813 - Don't Allow Dispatch if Delivery status < 799
                If QodHeader.DeliveryStatus < Qod.State.Delivery.DespatchStatusOk Then
                    MessageBox.Show("OM has not confirmed Despatch. Please wait 10 minutes and try again.", "Invalid Delivery Status", MessageBoxButtons.OK)
                    RetrieveOrders()
                    shouldReturn = True : Exit Sub
                End If
            Next

            'Show the dialog and allow the user to select whether to continue or not
            Using Form As New ActionConfirmationForm(QodHeaderActionFilter, (ActionFormBehaviour.DeliveryDialog Or ActionFormBehaviour.DeliveryConfirm))
                If Not Form.ShowDialog = DialogResult.OK Then
                    shouldReturn = True : Exit Sub
                End If
            End Using

            'If we get this far then we are good to go, so set all the relevant lines to their appropriate states!
            'First all lines the user has selected to be delivered
            For Each QodHeader As QodHeader In QodHeaderActionFilter.PositiveActionList
                QodHeader.Lines.DeliveryStatus(_storeId) = Delivery.DeliveredCreated
                QodHeader.DeliveryStatus = Delivery.DeliveredCreated
                QodHeader.SelfPersistTree()
            Next

            'Now all lines the user has selected to be undelivered
            For Each QodHeader As QodHeader In QodHeaderActionFilter.PositiveActionNegateList
                QodHeader.Lines.DeliveryStatus(_storeId) = Delivery.UndeliveredCreated
                QodHeader.DeliveryStatus = Delivery.UndeliveredCreated
                QodHeader.DeliveryConfirmed = False
                QodHeader.SelfPersistTree()
            Next

        ElseIf qods.Count = 1 Then

            Dim QodHeader As QodHeader
            QodHeader = qods(0)

            'Ref 813 - Don't Allow Dispatch if Delivery status < 799
            If QodHeader.DeliveryStatus < Qod.State.Delivery.DespatchStatusOk Then
                MessageBox.Show("OM has not confirmed Despatch. Please wait 10 minutes and try again.", "Invalid Delivery Status", MessageBoxButtons.OK)
                RetrieveOrders()
                shouldReturn = True : Exit Sub
            End If

            Using Form As New ConfirmFailOrder
                Select Case Form.ShowDialog
                    Case DialogResult.Yes
                        'Confirm the delivery
                        QodHeader.Lines.DeliveryStatus(_storeId) = Delivery.DeliveredCreated
                        QodHeader.DeliveryStatus = Delivery.DeliveredCreated
                        QodHeader.SelfPersistTree()
                    Case DialogResult.No
                        'Fail the delivery
                        QodHeader.Lines.DeliveryStatus(_storeId) = Delivery.UndeliveredCreated
                        QodHeader.DeliveryStatus = Delivery.UndeliveredCreated
                        QodHeader.DeliveryConfirmed = False
                        QodHeader.SelfPersistTree()
                    Case Else
                        'Cancel the action
                        shouldReturn = True : Exit Sub
                End Select
            End Using
        End If
    End Sub

    Private Sub FailConfirmPicking(ByVal qods As QodHeaderCollection, ByVal ShouldReturn As Boolean)

        ShouldReturn = False
        Dim QodHeaderActionFilter As New QodHeaderActionFilter(qods, Delivery.DespatchCreated, Delivery.DespatchStatusOk, _storeId.ToString)

        For Each QodHeader As QodHeader In qods
            'Ref 813 - Don't Allow Dispatch if Delivery status < 699
            If QodHeader.DeliveryStatus < Qod.State.Delivery.PickingConfirmStatusOk Then
                MessageBox.Show("OM has not confirmed pick list printed. Please wait 10 minutes and try again.", "Invalid Delivery Status", MessageBoxButtons.OK)
                RetrieveOrders()
                ShouldReturn = True : Exit Sub
            End If
        Next

        'If we get this far then we are good to go, so set all the relevant lines to their appropriate states!
        'First all lines the user has selected to be delivered
        For Each QodHeader As QodHeader In qods
            QodHeader.Lines.DeliveryStatus(_storeId) = Delivery.UndeliveredCreated
            QodHeader.DeliveryStatus = Delivery.UndeliveredCreated
            QodHeader.DeliveryConfirmed = False
            QodHeader.SelfPersistTree()
        Next

    End Sub
    ''' <summary>
    ''' Controls what happens when the confirmed delivery button or hotkey (F8) is pressed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <history>
    ''' <updated author="Charles McMahon" date="07/10/2010"></updated>
    ''' </history>
    ''' <remarks>
    ''' Creates a QodHeaderActionFilter to filter Qods that have not been despatched.  Then calls the ActionConfirmationForm to allow
    ''' the user to select the delivery status, Confirmed or Failed, as required.  All Qods are defaulted to "Confirmed" in the delivery dialog.
    ''' </remarks>
    Private Sub uxDeliveredButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxDeliveredButton.Click

        Dim grid As GridControl = GetFocusedGrid()
        Dim qods As QodHeaderCollection = GetSelectedQods(grid)
        Dim DeliveryStatus As Integer = 0

        Dim lShouldReturn As Boolean
        Dim QodHeaderActionFilter As QodHeaderActionFilter
        For Each QodHeader As QodHeader In qods
            If QodHeader.DeliveryStatus > DeliveryStatus Then
                DeliveryStatus = QodHeader.DeliveryStatus
            End If
        Next
        If DeliveryStatus >= Delivery.DespatchCreated Then
            DespatchOrder(qods, lShouldReturn)
        Else
            FailConfirmPicking(qods, lShouldReturn)
        End If
        If lShouldReturn Then
            Return
        End If
        'Clear the filter as it is no longer required!
        QodHeaderActionFilter = Nothing
        grid.RefreshDataSource()
        Dim view As GridView = CType(grid.MainView, GridView)
        For Each rowHandle As Integer In view.GetSelectedRows
            If rowHandle < 0 Then Continue For
            view.CollapseMasterRow(rowHandle)
            view.ExpandMasterRow(rowHandle)
        Next
        SetButtonStatus()

    End Sub

    Private Sub uxPrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxPrintButton.Click

        'check which tab is open to get grid in question
        Dim grid As GridControl = GetFocusedGrid()
        If grid Is Nothing Then Exit Sub

        'get sub header if all order grid
        Dim headerSub As String = My.Resources.Strings.ReportHeaderSubOpen
        If grid.Name = uxAllOrderGrid.Name Then headerSub = String.Format(My.Resources.Strings.ReportHeaderSubDates, uxStartDate.DateTime.ToShortDateString, uxEndDate.DateTime.ToShortDateString)

        'get columns to make non visible
        Dim view As GridView = CType(grid.MainView, GridView)
        If view Is Nothing Then Exit Sub

        Dim deliveryCol As GridColumn = view.Columns(GetPropertyName(Function(f As QodHeader) f.IsForDelivery))
        Dim customerCol As GridColumn = view.Columns(GetPropertyName(Function(f As QodHeader) f.CustomerAddress))
        Dim workPhoneCol As GridColumn = view.Columns(GetPropertyName(Function(f As QodHeader) f.PhoneNumberWork))
        Dim emailCol As GridColumn = view.Columns(GetPropertyName(Function(f As QodHeader) f.CustomerEmail))
        Dim suspendCol As GridColumn = view.Columns(GetPropertyName(Function(f As QodHeader) f.IsSuspended))

        Dim deliveryVisible As Boolean = deliveryCol.Visible
        Dim customerVisible As Boolean = customerCol.Visible
        Dim workPhoneVisible As Boolean = workPhoneCol.Visible
        Dim emailVisible As Boolean = emailCol.Visible
        Dim suspendVisible As Boolean = suspendCol.Visible
        deliveryCol.Visible = False
        customerCol.Visible = False
        workPhoneCol.Visible = False
        emailCol.Visible = False
        suspendCol.Visible = False

        'load print preview
        Using print As New Printing.Print(grid, UserId, WorkstationId)
            print.PrintHeader = My.Resources.Strings.ReportHeader
            print.PrintHeaderSub = headerSub
            print.PrintLandscape = True
            print.ShowPreviewDialog()
        End Using

        'reset column visibility
        deliveryCol.Visible = deliveryVisible
        customerCol.Visible = customerVisible
        workPhoneCol.Visible = workPhoneVisible
        emailCol.Visible = emailVisible
        suspendCol.Visible = suspendVisible

    End Sub

    Private Sub uxResetButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxResetButton.Click
        uxFullOmOrderText.EditValue = Nothing
        uxFullStoreIdText.EditValue = Nothing
        uxFullStoreOrderText.EditValue = Nothing
        uxFullTillOrderReferenceText.EditValue = Nothing

        Dim grid As GridControl = GetFocusedGrid()
        If grid IsNot Nothing Then grid.DataSource = Nothing
        RetrieveOrders()   'CR0020 - After a reset make it re-load the grid (refresh)
        SetButtonStatus()
        CheckButtonEnableStatus()
    End Sub

    Private Sub uxExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxExitButton.Click
        FindForm.Close()
    End Sub

    Private Sub uxRePrintDespatchNote_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxRePrintDespatchNote.Click

        Dim grid As GridControl = GetFocusedGrid()
        Dim qods As QodHeaderCollection = GetSelectedQods(grid)
        Dim printCount As Integer = -2147483648  'Set this to the lowest value so we do not show a number of prints dialog when not needed

        Dim pickFactory As New PickNoteFormFactory

        For Each QodHeader As QodHeader In qods
            'Check the local store id

            Using store As Store = Core.System.Store.GetStore(QodHeader.SellingStoreId)
                If store Is Nothing Then
                    DisplayWarning(String.Format(My.Resources.Strings.NoStoreExists, QodHeader.SellingStoreId))
                Else
                    'Print the Qods and update the flag and counters as required
                    pickFactory.Initialise(QodHeader, store, _storeId)
                    _pickNoteFormInstance = pickFactory.GetImplementation
                    _pickNoteFormInstance.Print()
                    printCount += 1
                    If Not QodHeader.IsPrinted Then
                        QodHeader.IsPrinted = True
                    Else
                        QodHeader.NumberReprints += 1
                    End If

                    'Save the existing scanned info
                    For Each line As QodLine In QodHeader.Lines
                        line.SelfPersist()
                    Next
                    QodHeader.SelfPersist(False)
                End If
            End Using
        Next
    End Sub

    Private Sub GridLoadQodPickInstructions(ByVal grid As GridControl)
        Dim view As New GridView(grid)
        grid.LevelTree.Nodes.Add(GetPropertyName(Function(f As QodHeader) f.Texts), view)

        GridViewAddColumn(view, GetPropertyName(Function(f As QodText) f.TextType), "", 150, 150)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodText) f.Number), "", 80, 80)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodText) f.Text))

        view.Appearance.Assign(grid.MainView.Appearance)
        view.OptionsView.ShowGroupPanel = False
        view.OptionsBehavior.Editable = False
        view.ColumnPanelRowHeight = 35
        view.OptionsFilter.AllowFilterEditor = False
        view.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Never
        view.OptionsView.ShowIndicator = False
        view.OptionsSelection.EnableAppearanceFocusedCell = False
        view.OptionsSelection.EnableAppearanceFocusedRow = False
        view.FocusRectStyle = DrawFocusRectStyle.None
    End Sub

    Private Sub uxOrderNumber_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles uxOMOrderNumber.KeyUp, txtOvcNumberOpenOrders.KeyUp, txtOvcNumberAllOrders.KeyUp
        If e.KeyCode = Keys.Enter Then
            Try
                ' show an hourglass cursor
                Me.Cursor = Cursors.WaitCursor

                Dim grid As GridControl = GetFocusedGrid()

                Dim textEdit As TextEdit = CType(sender, TextEdit)
                textEdit.BackColor = Color.White

                ' establish search value
                Dim searchText As String = textEdit.Text.Replace(" ", "") ' remove spaces

                ' populate the grid with any matching order
                If _ConfirmPickingAvailable AndAlso grid Is uxOpenOrderGrid Then
                    'if ConfirmPickingAvailable is true, this is eStore
                    '   either search by text or return no QOD Headers

                    Dim qods As QodHeaderCollection = GetQods(textEdit)
                    If qods.Count = 0 Then
                        ' no entries found
                        grid.DataSource = Nothing
                        Exit Try
                    End If

                    grid.DataSource = qods
                    GridLoadQods(grid)
                    GridLoadQodLines(grid)
                    GridLoadQodTexts(grid)
                    GridLoadQodPickInstructions(grid)
                    GridLoadQodRefunds(grid)
                End If

                ' select the order in the grid, if found
                ' search for the value in specified column
                Dim view As DevExpress.XtraGrid.Views.Base.ColumnView = CType(grid.MainView, DevExpress.XtraGrid.Views.Base.ColumnView)
                Dim col As DevExpress.XtraGrid.Columns.GridColumn
                If textEdit Is uxOMOrderNumber Then
                    col = view.Columns("OmOrderNumber")
                ElseIf textEdit Is txtOvcNumberOpenOrders OrElse textEdit Is txtOvcNumberAllOrders Then
                    col = view.Columns("OvcOrderNumber")
                Else
                    Exit Try
                End If

                Dim rowHandle As Integer = view.LocateByDisplayText(0, col, searchText)

                If Not rowHandle = DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
                    ' value found - set backcolor of text box to indicate success
                    textEdit.BackColor = Color.LightGreen

                    ' select the order which was found
                    view.ClearSelection()
                    view.SelectRow(rowHandle)
                    view.FocusedRowHandle = rowHandle

                    ' enable/disable buttons according to status
                    SetButtonStatus()
                End If

            Finally
                Me.Cursor = Cursors.Default
            End Try
        End If
    End Sub

    Private Sub MainForm_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus
        If _ConfirmPickingAvailable = True Then
            uxOMOrderNumber.Focus()
        End If
    End Sub

    ''Ref 777 : on "om enquiry" tab setting visibility of these buttons to false in every case, need to reset if moving to different tab
    Private Sub ConfirmPickingAvailableButtonVisibility(ByVal blnVisible As Boolean)
        If blnVisible = True Then
            uxConfirmPickingButton.Visible = True     'F6
            uxRePrintDespatchNote.Visible = True      'F7
            uxDespatchButton.Visible = False          'F7
        Else
            uxConfirmPickingButton.Visible = _PickConfirmedStatus.IsPickConfimedStatusActivated    'F6
            uxRePrintDespatchNote.Visible = False     'F7
            uxDespatchButton.Visible = True           'F7
        End If

    End Sub

    Private Sub btnStockEnquiry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStockEnquiry.Click
        Dim assemblyName As String = "Stock.Enquiry.dll"
        Dim className As String = "Stock.Enquiry.Enquiry"
        Dim appName As String = "Stock Item Enquiry"
        Dim parameters As String = " (/P=',CFC')"
        Dim CurDirectory As String = Directory.GetCurrentDirectory()
        Dim ParentDirectory As String = Directory.GetParent(CurDirectory).ToString

        Trace.WriteLine("Launching Stock Enquiry")
        '_workstationId = "01"
        Trace.WriteLine("Launching : " + ParentDirectory + assemblyName)
        Dim hostForm As New Cts.Oasys.WinForm.HostForm(assemblyName, className, appName, 0, 1, 9, parameters, "", False)
        hostForm.ShowDialog()
    End Sub

    Private Sub uxOpenOrderTab_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxOpenOrderTab.Leave
        txtNameAllOrders.Text = txtCustomerNameOpenOrders.Text
        txtPostCodeAllOrders.Text = txtPostCodeOpenOrders.Text
        txtPhoneNumberAllOrders.Text = txtTelephoneNumberOpenOrders.Text
    End Sub

    Private Sub uxAllOrderTab_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxAllOrderTab.Leave
        txtCustomerNameOpenOrders.Text = txtNameAllOrders.Text
        txtPostCodeOpenOrders.Text = txtPostCodeAllOrders.Text
        txtTelephoneNumberOpenOrders.Text = txtPhoneNumberAllOrders.Text
    End Sub

    Private Sub _PickConfirmedStatus_ConfirmDeliveryButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Handles _PickConfirmedStatus.ConfirmDeliveryButton
        uxDeliveredButton.Visible = IsVisible
        uxDeliveredButton.Enabled = IsEnabled
    End Sub

    Private Sub _PickConfirmedStatus_ConfirmPickingButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Handles _PickConfirmedStatus.ConfirmPickingButton
        uxConfirmPickingButton.Visible = IsVisible
        uxConfirmPickingButton.Enabled = IsEnabled
    End Sub

    Private Sub _PickConfirmedStatus_DespatchButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Handles _PickConfirmedStatus.DespatchButton
        uxDespatchButton.Visible = IsVisible
        uxDespatchButton.Enabled = IsEnabled
    End Sub

    Private Sub _PickConfirmedStatus_ExitButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Handles _PickConfirmedStatus.ExitButton
        uxExitButton.Visible = IsVisible
        uxExitButton.Enabled = IsEnabled
    End Sub

    Private Sub _PickConfirmedStatus_MaintainButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Handles _PickConfirmedStatus.MaintainButton
        uxMaintainButton.Visible = IsVisible
        uxMaintainButton.Enabled = IsEnabled
    End Sub

    Private Sub _PickConfirmedStatus_PrintButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Handles _PickConfirmedStatus.PrintButton
        uxPrintButton.Visible = IsVisible
        uxPrintButton.Enabled = IsEnabled
    End Sub

    Private Sub _PickConfirmedStatus_PrintPickNoteButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Handles _PickConfirmedStatus.PrintPickNoteButton
        uxPrintDespatchButton.Visible = IsVisible
        uxPrintDespatchButton.Enabled = IsEnabled
    End Sub

    Private Sub _PickConfirmedStatus_RePrintDespatchButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Handles _PickConfirmedStatus.RePrintDespatchButton
        uxRePrintDespatchNote.Visible = IsVisible
        uxRePrintDespatchNote.Enabled = IsEnabled
    End Sub

    Private Sub _PickConfirmedStatus_ResetButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Handles _PickConfirmedStatus.ResetButton
        uxResetButton.Visible = IsVisible
        uxResetButton.Enabled = IsEnabled
    End Sub

    Private Sub _PickConfirmedStatus_RetreiveOrdersButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Handles _PickConfirmedStatus.RetreiveOrdersButton
        uxRetrieveButton.Visible = IsVisible
        uxRetrieveButton.Enabled = IsEnabled
    End Sub

    Private Sub _PickConfirmedStatus_StockEnquiryButton(ByVal IsVisible As Boolean, ByVal IsEnabled As Boolean) Handles _PickConfirmedStatus.StockEnquiryButton
        btnStockEnquiry.Visible = IsVisible
        btnStockEnquiry.Enabled = IsEnabled
    End Sub

    Private Sub _PickConfirm_RefreshGrid() Handles _PickConfirm.RefreshGrid
        RetrieveOrders()
    End Sub

    Private Sub txtRetrieveControl_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPostCodeAllOrders.KeyDown, txtNameAllOrders.KeyDown, txtPhoneNumberAllOrders.KeyDown, txtTelephoneNumberOpenOrders.KeyDown, txtPostCodeOpenOrders.KeyDown, txtCustomerNameOpenOrders.KeyDown
        If e.KeyCode = Keys.Enter And uxRetrieveButton.Enabled = True Then RetrieveOrders()
    End Sub

    Public Function IsSearchAllowed() As Boolean
        Dim grid As GridControl = GetFocusedGrid()

        Dim res As Boolean = grid.Name = uxAllOrderGrid.Name _
            And (uxStartDate.EditValue Is Nothing _
            Or uxEndDate.EditValue Is Nothing) _
            And txtOvcNumberAllOrders.Text = Nothing _
            And txtPostCodeAllOrders.Text = Nothing _
            And txtNameAllOrders.Text = Nothing _
            And txtPhoneNumberAllOrders.Text = Nothing

        Return Not res
    End Function

    Private Function GetDeliverySlots() As IEnumerable(Of DeliverySlot)
        Dim repo = _dataLayerFactory.Create(Of IDictionariesRepository)()
        GetDeliverySlots = repo.GetDeliverySlots()
    End Function

End Class

