Imports System
Imports System.Diagnostics
Imports System.ComponentModel

Imports Cts.Oasys.Hubs.Core
Imports Cts.Oasys.Hubs.Core.Order
Imports Cts.Oasys.Hubs.Core.Order.Qod
Imports Cts.Oasys.Hubs.Core.System
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base

Public Class PickConfirmation

    Private Const SKUN_LEN As Integer = 6

    Private Const SKUN_EAN_LEN As Integer = 8

    Private Const SKUN_EAN As Integer = 13

    Private Const SCANNER_PARAMS As Long = 952

    Private Const QTY_SCAN_ERROR_MSG1 As Long = 970101 'CR0043

    Private Const QTY_SCAN_ERROR_MSG2 As Long = 970102 'CR0043

    Public _Completed As Boolean = False

    Private KeyBuffer As String = String.Empty

    Private LastKeyPressed As Date

    Private KeysTime As TimeSpan = New TimeSpan(0, 0, 0, 0, _
            CInt(ConfigXMLDoc.SelectSingleNode("Configuration/PickConfirmation/Scanner").Attributes("KeyTime").Value))

    Private ScanCodeLength() As Integer

    Private _ScannerValue As String = String.Empty

    Private _StoreId As Integer = 0

    Private m_ScanButtonPressed As Boolean = False

    'CR0043 MO'C 25/05/2011 - Add Quantity to scan to the pick confirm screen
    Enum EntryModes
        modeSkuEntry = 0
        modeQTYEntry = 1
    End Enum

    Private _EntryMode As EntryModes = EntryModes.modeSkuEntry

    Private _intQuantity As Integer = 1
    'End of CR0043 Change

    Public Sub New(ByRef QodHeader As QodHeader)

        'This call is required by the Windows Form Designer.
        Trace.WriteLine("About to Initialze Component.")
        InitializeComponent()
        Trace.WriteLine("Initialzed Component.")

        'Add any initialization after the InitializeComponent() call.
        Trace.WriteLine("About to get Order number.")
        uxOrderNumber.Text = QodHeader.Number
        Trace.WriteLine("Got Order number.")
        uxDeliveryDate.Text = QodHeader.DateDelivery.ToString
        uxCustomerName.Text = QodHeader.CustomerName
        uxDeliveryContactName.Text = QodHeader.CustomerName
        uxDeliveryAddress.Text = QodHeader.DeliveryAddress
        uxDeliveryContactNumber.Text = CStr(IIf(QodHeader.PhoneNumber <> String.Empty, QodHeader.PhoneNumber, _
                                           IIf(QodHeader.PhoneNumberMobile <> String.Empty, QodHeader.PhoneNumberMobile, _
                                               QodHeader.PhoneNumberWork)))
        uxResetButton.Enabled = True

        Trace.WriteLine("About to Store Number.")
        _StoreId = ThisStore.Id4
        Trace.WriteLine("Got Store Number " & _StoreId.ToString)
        GridLoadLines(uxScanItemGrid, QodHeader.Lines)

    End Sub

    Public Sub GridLoadLines(ByVal grid As GridControl, ByVal Lines As QodLineCollection)

        Trace.WriteLine("Started GridLoadLines.")
        'Load the grid with the data
        grid.DataSource = Lines
        Trace.WriteLine("Set Grids datasource.")

        'Hide ALL default columns (there will be one fo each property in the Lines)
        Dim view As GridView = CType(grid.MainView, GridView)
        For Each col As GridColumn In view.Columns
            col.VisibleIndex = -1
        Next
        Trace.WriteLine("Hide not used columns.")

        view.Columns(GetPropertyName(Function(f As QodLine) f.DeliverySource)).FilterInfo = New ColumnFilterInfo("[" & GetPropertyName(Function(f As QodLine) f.DeliverySource) & "]=" & _StoreId)
        Trace.WriteLine("Hiding text edits for button manual scan.")

        RepositoryItemButtonEdit1.TextEditStyle = TextEditStyles.HideTextEditor
        Trace.WriteLine("Setting up grid columns.")

        'New select which columns to show (some depending on FormBehaviour)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodLine) f.SkuNumber), My.Resources.Columns.SkuNumber, False, 80, 80)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodLine) f.SkuDescription), My.Resources.Columns.SkuDescription, False)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodLine) f.QtyToDeliver), My.Resources.Columns.QtyOrdered, False, 80, 80)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodLine) f.QuantityScanned), My.Resources.Columns.QtyScanned, False, 80, 80)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodLine) f.IsCancellable), My.Resources.Columns.ManualScan, True, 80, 80)
        view.Columns(GetPropertyName(Function(f As QodLine) f.IsCancellable)).ColumnEdit = RepositoryItemButtonEdit1
        Trace.WriteLine("Set up grid columns.")

        'Adjust the grid to fit the columns
        view.BestFitColumns()
        Trace.WriteLine("Setting up scanned grid.")
        LoadListBoxEntry()
        Trace.WriteLine("Finished GridLoadLines.")

    End Sub

    Private Sub GridViewSetColumn(ByRef view As GridView, ByVal columnName As String, Optional ByVal caption As String = "", Optional ByVal editable As Boolean = False, Optional ByVal minWidth As Integer = 0, Optional ByVal maxWidth As Integer = 0)
        view.Columns(columnName).VisibleIndex = view.VisibleColumns.Count
        view.Columns(columnName).OptionsColumn.AllowEdit = editable
        If caption.Length > 0 Then view.Columns(columnName).Caption = caption
        If minWidth > 0 Then view.Columns(columnName).MinWidth = minWidth
        If maxWidth > 0 Then view.Columns(columnName).MaxWidth = maxWidth
    End Sub

    Private Sub uxCancelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxCancelButton.Click

        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub PickConfirmation_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
    End Sub

    Private Sub PickConfirmation_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        Try
            AxComm.PortOpen = False
            Trace.WriteLine("Closed the COM port.")
        Catch
            Trace.WriteLine("Error Closing COM port.")
        End Try
    End Sub

    Private Sub PickConfirmation_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        e.Handled = True
        Select Case e.KeyData
            Case Keys.Q     'CR0043 MO'C 25/05/2011 - Add Quantity Scanned to Pick Confirm screen
                uxButtonQuantity.PerformClick()
                e.Handled = True
            Case Keys.F3 : uxResetButton.PerformClick()
            Case Keys.F8 : uxAcceptButton.PerformClick()
            Case Keys.F10 : uxCancelButton.PerformClick()
            Case Keys.Enter
                If KeyBuffer.Length > 0 Then
                    If IsValidLength(KeyBuffer.Length) Then
                        If IsScannedAValidSku(KeyBuffer) Then
                            UpdateGrid(KeyBuffer)
                        Else
                            e.Handled = False
                        End If
                    Else
                        e.Handled = False
                    End If
                Else
                    e.Handled = False
                End If
            Case Else
                    If Now.Subtract(LastKeyPressed) < KeysTime Then
                        KeyBuffer += Chr(e.KeyValue)
                    Else
                        KeyBuffer = Chr(e.KeyValue)
                    End If
                    LastKeyPressed = Now
        End Select

    End Sub

    Private Sub PickConfirmation_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Index As Integer = 0
        Dim BufferLengths As Xml.XmlNodeList = ConfigXMLDoc.SelectNodes("Configuration/PickConfirmation/Scanner/BufferLengths/BufferLength")
        For Each BufferLength As Xml.XmlNode In BufferLengths
            If BufferLength.Name = "BufferLength" Then
                ReDim Preserve ScanCodeLength(Index)
                ScanCodeLength(Index) = CInt(BufferLength.InnerText)
                Index += 1
            End If
        Next
    End Sub

    Private Function IsValidLength(ByVal bufferLength As Integer) As Boolean
        For Index As Integer = 0 To UBound(ScanCodeLength)
            If ScanCodeLength(Index) = bufferLength Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Sub PickConfirmation_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        InitializeScanner()
        uxScanEntry.Focus()
    End Sub

    Private Sub UpdateGrid(ByVal SkuNumber As String)

        Trace.WriteLine("Looking for SKUN = " & SkuNumber)
        If IsScannedAValidSku(SkuNumber) = True Then
            SkuNumber = _ScannerValue
            'We can only get a valid SkuNumber, back from the Scanner Class
            'So we need to lookup the skuNumber in the order
            Dim grid As GridControl = uxScanItemGrid
            Dim view As GridView = CType(grid.MainView, GridView)
            Dim blnUpdated As Boolean = False
            Dim blnInOrder As Boolean = False
            Dim SkuQtyToRemove As Integer = _intQuantity

            If view.RowCount > 0 Then
                For rowHandle As Integer = 0 To view.RowCount - 1
                    Dim row As QodLine = CType(view.GetRow(rowHandle), QodLine)
                    If Not row Is Nothing Then
                        If row.SkuNumber = SkuNumber Then 'We have found one
                            Dim AlreadyScanned As Integer = 0
                            blnInOrder = True
                            If row.QuantityScanned < row.QtyToDeliver Then 'CR0043 MO'C 25/05/2011 - Add Quantity Scanned to Pick Confirm screen
                                If _intQuantity >= (row.QtyToDeliver - row.QuantityScanned) Then 'Ref 838 added >= instead of 
                                    AlreadyScanned += row.QuantityScanned
                                    row.QuantityScanned += (row.QtyToDeliver - AlreadyScanned)
                                    _intQuantity -= (row.QtyToDeliver - AlreadyScanned)
                                ElseIf _intQuantity < (row.QtyToDeliver - row.QuantityScanned) Then
                                    row.QuantityScanned += _intQuantity
                                    _intQuantity -= _intQuantity
                                End If
                                blnUpdated = True
                                uxScannedList.Items.Insert(0, SkuNumber)
                                uxScannedList.SelectedIndex = 0 ' (uxScannedList.Items.Count - 1)
                                view.RefreshRow(rowHandle)
                                uxScannedList.Refresh()
                                If _intQuantity = 0 Then Exit For
                            End If
                        End If
                    End If
                Next rowHandle
            End If
            m_ScanButtonPressed = False

            If _intQuantity > 0 And blnUpdated Then 'CR0043 MO'C 25/05/2011 - Add Quantity Scanned to Pick Confirm screen
                Dim sMsg As String = Parameter.GetString(QTY_SCAN_ERROR_MSG1) + Parameter.GetString(QTY_SCAN_ERROR_MSG2) 'CR0043
                MessageBox.Show(sMsg, "Error too many scanned")
                ClearAllScans(SkuNumber, (SkuQtyToRemove - _intQuantity))
                If uxScannedList.SelectedIndex > -1 Then
                    uxScannedList.SelectedIndex = 0
                End If
            End If

            'Report back any error.
            If blnInOrder = False Then
                Beep()
                MessageBox.Show("SkuNumber '" & SkuNumber.ToString & "' not found in order.", "Sku Number not found", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                If blnUpdated = False Then
                    Beep()
                    MessageBox.Show("SkuNumber '" & SkuNumber.ToString & "' has been completed for this order.", "Sku Number Fulfilled", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            End If
            uxScanEntry.Text = ""
            LabelQTY.Text = "" 'CR0043
            _intQuantity = 1 'CR0043
            uxScanEntry.Focus()
        Else
            Trace.WriteLine("Invalid Sku.")
        End If
        _ScannerValue = String.Empty

    End Sub

    Private Sub ShowInvalidSku(ByVal SkuNumber As String)
        Beep()
        MessageBox.Show("Invalid Sku '" & SkuNumber.ToString & "'.", "Invalid Sku", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Trace.WriteLine("Invalid SKUN = " & SkuNumber)
    End Sub

    Private Sub ScannerFeedBack(ByVal strInformation As String)
        Trace.WriteLine("Feed Back = " & strInformation)
    End Sub

    Private Sub uxAcceptButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxAcceptButton.Click

        'Check to see if the order is complete
        Dim view As GridView = CType(uxScanItemGrid.MainView, GridView)

        If view.RowCount > 0 Then
            _Completed = True
            For rowHandle As Integer = 0 To view.RowCount - 1
                Dim row As QodLine = CType(view.GetRow(rowHandle), QodLine)
                If Not row Is Nothing Then
                    If row.QuantityScanned < row.QtyToDeliver Then
                        _Completed = False
                        Exit For
                    End If
                End If
            Next
        End If

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub uxScanEntry_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles uxScanEntry.KeyDown

        Select Case e.KeyData
            Case Keys.Enter
                If _EntryMode = EntryModes.modeQTYEntry Then 'CR0043 MO'C 25/05/2011 - Quantity to be scanned change
                    LabelQTY.Text = "X " & uxScanEntry.Text 'CR0043
                    _intQuantity = CInt(uxScanEntry.Text) 'CR0043
                    _EntryMode = EntryModes.modeSkuEntry 'CR0043
                    LabelMain.Text = "Scan SKU or EAN" 'CR0043
                    uxScanEntry.Text = "" 'CR0043
                Else 'CR0043
                    If uxScanEntry.Text.Length > 0 Then 'CR0043
                        If IsValidLength(uxScanEntry.Text.Length) Then 'CR0043
                            If IsScannedAValidSku(uxScanEntry.Text) Then 'CR0043
                                UpdateGrid(_ScannerValue) 'CR0043
                            Else 'CR0043
                                ShowInvalidSku(_ScannerValue) 'CR0043
                            End If 'CR0043
                        End If 'CR0043
                        uxScanEntry.Text = "" 'CR0043
                        LabelQTY.Text = "" 'CR0043
                        _intQuantity = 1 'CR0043
                    End If
                End If
        End Select
    End Sub

    Private Sub ClearScannedQty(ByVal rowHandle As Integer)
        Dim view As GridView = CType(uxScanItemGrid.MainView, GridView)
        Dim row As QodLine = CType(view.GetRow(rowHandle), QodLine)
        If Not row Is Nothing Then
            row.QuantityScanned = 0
        End If
    End Sub

    Private Sub uxResetButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxResetButton.Click

        'Reset the Grid on specific criteria
        Dim grid As GridControl = uxScanItemGrid
        Dim view As GridView = CType(grid.MainView, GridView)
        Dim strSkuNumber As String = String.Empty

        If view.SelectedRowsCount > 0 Then

            'Anything on the grid selected reset to zero
            Dim rowHandle() As Integer = view.GetSelectedRows
            For Index As Integer = 0 To view.SelectedRowsCount - 1
                Dim row As QodLine = CType(view.GetRow(rowHandle(Index)), QodLine)
                If Not row Is Nothing Then
                    ClearSkuFromList(row.QuantityScanned, row.SkuNumber)
                    row.QuantityScanned = 0
                    view.RefreshRow(rowHandle(Index))
                End If
            Next

        Else

            'Use the list entry highlight to clear the grid.
            If uxScannedList.SelectedIndex > -1 Then
                For Index As Integer = uxScannedList.SelectedIndex To 0 Step -1
                    strSkuNumber = uxScannedList.Items(Index).ToString
                    If view.RowCount > 0 Then
                        For rowHandle As Integer = 0 To view.RowCount - 1
                            Dim row As QodLine = CType(view.GetRow(rowHandle), QodLine)
                            If Not row Is Nothing Then
                                If row.SkuNumber = strSkuNumber Then 'We have found one
                                    If row.QuantityScanned >= 1 Then
                                        row.QuantityScanned = 0
                                        uxScannedList.Items.Remove(uxScannedList.Items(Index))
                                        uxScannedList.SelectedIndex = (uxScannedList.Items.Count - 1)
                                        view.RefreshRow(rowHandle)
                                        uxScannedList.Refresh()
                                        Exit For
                                    End If
                                End If
                            End If
                        Next
                    End If
                Next
            End If
        End If

        'Clear up
        If uxScannedList.SelectedIndex > -1 Then uxScannedList.SelectedIndex = 0
        Application.DoEvents()
        grid.Refresh()
        uxScanEntry.Focus()
        uxScannedList.Refresh()
    End Sub

    ''' <summary>
    ''' New Function ClearAllScans For Sku
    ''' This was added as part of CR0043
    ''' Author: MO'C
    ''' Date:   25/05/2011
    ''' </summary>
    ''' <param name="strSkuNumber"></param>
    ''' <remarks></remarks>
    Private Sub ClearAllScans(ByVal strSkuNumber As String, ByVal NoOfSkusToRemove As Integer)

        Dim grid As GridControl = uxScanItemGrid
        Dim view As GridView = CType(grid.MainView, GridView)
        Dim Count As Integer = 0

        If uxScannedList.SelectedIndex > -1 Then
            For Index As Integer = uxScannedList.SelectedIndex To 0 Step -1
                If view.RowCount > 0 Then
                    For rowHandle As Integer = view.RowCount - 1 To 0 step -1 
                        Dim row As QodLine = CType(view.GetRow(rowHandle), QodLine)
                        If Not row Is Nothing Then
                            If row.SkuNumber = strSkuNumber Then 'We have found one
                                If row.QuantityScanned >= 1 Then
                                    If row.QuantityScanned >= NoOfSkusToRemove Then
                                        row.QuantityScanned -= NoOfSkusToRemove
                                        NoOfSkusToRemove = 0
                                    Else
                                        NoOfSkusToRemove -= row.QuantityScanned
                                        row.QuantityScanned = 0
                                    End If
                                    uxScannedList.Items.Remove(uxScannedList.Items(Index))
                                    uxScannedList.SelectedIndex = (uxScannedList.Items.Count - 1)
                                    view.RefreshRow(rowHandle)
                                    uxScannedList.Refresh()
                                    If NoOfSkusToRemove <= 0 Then Exit For 'Only remove incorrect scan value
                                End If
                            End If
                        End If
                    Next
                End If
            Next
        End If
    End Sub


    Private Sub ClearSkuFromList(ByVal QuantityScanned As Integer, ByVal strSkuNumber As String)

        Dim NumberDeleted As Integer = 0

        While NumberDeleted < QuantityScanned
            For Index As Integer = uxScannedList.Items.Count - 1 To 0 Step -1
                If uxScannedList.Items(Index).ToString = strSkuNumber Then
                    If NumberDeleted <= QuantityScanned Then
                        uxScannedList.Items.Remove(uxScannedList.Items(Index))
                        NumberDeleted += 1
                        Exit For
                    End If
                End If
            Next
        End While

    End Sub

    Private Sub LoadListBoxEntry()
        Dim grid As GridControl = uxScanItemGrid
        Dim view As GridView = CType(grid.MainView, GridView)

        If view.RowCount > 0 Then
            For rowHandle As Integer = 0 To view.RowCount - 1
                Dim row As QodLine = CType(view.GetRow(rowHandle), QodLine)
                If Not row Is Nothing Then
                    If row.QuantityScanned > 0 Then
                        For index As Integer = 0 To row.QuantityScanned - 1
                            uxScannedList.Items.Add(row.SkuNumber)
                            uxScannedList.SelectedIndex = (uxScannedList.Items.Count - 1)
                            uxScannedList.Refresh()
                        Next
                    End If
                End If
            Next
        End If

    End Sub

    Private Sub uxScanItemGrid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxScanItemGrid.Click
        Dim grid As GridControl = uxScanItemGrid
        Dim view As GridView = CType(grid.MainView, GridView)
        If view.FocusedColumn.FieldName = GetPropertyName(Function(f As QodLine) f.IsCancellable) Then
            Dim row As QodLine = CType(view.GetRow(view.FocusedRowHandle), QodLine)
            If Not row Is Nothing Then
                m_ScanButtonPressed = True
                UpdateGrid(row.SkuNumber)
            End If
        End If
    End Sub

    Private Sub uxScannedList_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxScannedList.GotFocus
        uxScanItemView.ClearSelection()
    End Sub

    Public Function IsScannedAValidSku(ByVal strInput As String) As Boolean

        'Dim strReturnValue As String = String.Empty
        Dim strEANValue As String = String.Empty
        Dim strSkuNumber As String = String.Empty
        Trace.WriteLine("Start IsScannedAValidSku: " & strInput.ToString & " len(" & strInput.Length.ToString & ")")
        Select Case Len(strInput)
            Case SKUN_LEN
                _ScannerValue = strInput
                Trace.WriteLine("[6] Sku Interpreted = " & _ScannerValue)
                Return Stock.Stock.IsStockItemInDatabase(strInput)

            Case SKUN_EAN_LEN, SKUN_EAN_LEN + 1
                _ScannerValue = strInput.Substring(1, 6)
                Trace.WriteLine("[8] Sku Interpreted = " & _ScannerValue)
                Return Stock.Stock.IsStockItemInDatabase(_ScannerValue)

            Case SKUN_EAN, SKUN_EAN + 1
                'Lookup sku via 16 digit EAN number
                strInput = strInput.Substring(0, 13)
                strEANValue = strInput.PadLeft(16, Chr(48))
                Trace.WriteLine("[13] EAN = " & strEANValue)

                Dim stock As Stock.Stock = stock.GetStockbyEAN(strEANValue)
                If stock Is Nothing Then
                    _ScannerValue = "INVALID SKU"
                    Return False
                End If

                If stock.SkuNumber <> "" Then
                    _ScannerValue = stock.SkuNumber
                    Trace.WriteLine("[13] Sku Interpreted = " & _ScannerValue)
                    Return stock.IsStockItemInDatabase(_ScannerValue)
                Else
                    _ScannerValue = "INVALID SKU"
                    Trace.WriteLine("No EAN Found." & _ScannerValue)
                    Return False
                End If

        End Select
        Trace.WriteLine("Finish IsScannedAValidSku. ")

        Return False

    End Function

    Private Function InitializeScanner() As Boolean

        Try

            Dim strComPort As String = Parameter.GetString(SCANNER_PARAMS)
            If strComPort.ToLower.Contains("com") Then

                Dim params() As String = strComPort.Split(Chr(44))

                AxComm.CommPort = CShort(params(0).Substring(3, 1))
                AxComm.Settings = params(1) & "," & params(2) & "," & params(3) & "," & params(4)
                AxComm.DTREnable = True
                AxComm.RTSEnable = True
                AxComm.RThreshold = 1
                AxComm.SThreshold = 1
                AxComm.PortOpen = True
                Trace.WriteLine("Scanner port was opened.")
                Return True

            ElseIf strComPort.ToLower.Contains("key") Then
                Trace.WriteLine("Scanner port was already opened.")
                Return True

            Else
                Trace.WriteLine("Scanner port was not opened.")
                Return False

            End If

        Catch ex As Exception
            Trace.WriteLine("Scanner port was not opened: " & ex.Message)
            Return False

        End Try

    End Function

    Private Sub Comm_OnComm(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AxComm.OnComm

        Try
            Trace.WriteLine("Start: COM port Data received.")

            Static strInput As String = String.Empty

            If AxComm.PortOpen = False Then Exit Sub

            strInput = String.Format("{0}{1}", strInput, AxComm.Input)
            If strInput.Contains(vbCr) = False Then
                Exit Sub
            End If
            If Len(strInput) = 0 Then
                Trace.WriteLine("Zero Character(s) available to read.")
                Exit Sub
            End If

            strInput = strInput.Replace(vbCr, vbNullString)
            strInput = strInput.Replace(vbLf, vbNullString)
            Trace.WriteLine("This was received by the scan: " & strInput)

            If IsScannedAValidSku(strInput) = True Then
                'Need to send the result back to the form.
                Trace.WriteLine("Got sku - " & _ScannerValue)
                UpdateGrid(_ScannerValue)
            Else
                'Invalid Sku found
                Trace.WriteLine("No sku - Error. " & strInput)
                ShowInvalidSku(_ScannerValue)
            End If

            Trace.WriteLine("Finish: COM port Data received.")
            strInput = String.Empty

        Catch ex As Exception
            Trace.WriteLine(" COMMS Error: " & ex.Message.ToString)

        End Try

    End Sub

    ''' <summary>
    ''' New Event: uxButtonQuantity_Click
    ''' Added as part of CR0043
    ''' Author: MO'C
    ''' Date:   25/05/2011
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub uxButtonQuantity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxButtonQuantity.Click
        _EntryMode = EntryModes.modeQTYEntry
        LabelMain.Text = "Quantity Picked for Sku"
        uxScanEntry.Focus()
    End Sub

    ''' <summary>
    ''' New Event: 
    ''' Added as part of CR0043
    ''' Author: MO'C
    ''' Date:   25/05/2011
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub uxScanEntry_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles uxScanEntry.KeyPress

        If Asc(e.KeyChar) = 8 Then Exit Sub
        Select Case e.KeyChar
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
            Case Else
                e.KeyChar = CChar("")
        End Select
    End Sub
End Class

'''' <summary>
'''' 
'''' </summary>
'''' <remarks></remarks>
'Public Class Scanner

'    Implements System.IDisposable


'    Public Event ScannerInput(ByVal skuNumber As String)
'    Public Event InvalidSku(ByVal skuNumber As String)
'    Public Event FeedBack(ByVal Information As String)

'    Private WithEvents port As SerialPort = New System.IO.Ports.SerialPort()

'    Private Const SCANNER_PARAMS As Long = 952
'    Private Const SKUN_LEN As Integer = 6
'    Private Const SKUN_EAN_LEN As Integer = 8
'    Private Const SKUN_EAN As Integer = 13

'    Private _ScannerAvailable As Boolean = False
'    Private _ScannerValue As String = String.Empty

'    Public Property ScannerValue() As String
'        Get
'            ScannerValue = _ScannerValue
'        End Get
'        Set(ByVal value As String)
'            _ScannerValue = value
'        End Set
'    End Property

'    Public Property ScannerAvailable() As Boolean
'        Get
'            ScannerAvailable = _ScannerAvailable
'        End Get
'        Set(ByVal value As Boolean)
'            _ScannerAvailable = value
'        End Set
'    End Property

'    Public Sub New()
'        ScannerAvailable = InitializeScanner()
'    End Sub

'    Private Function InitializeScanner() As Boolean

'        Try

'            Dim stopBits As StopBits = stopBits.One
'            Dim parity As Parity = parity.None

'            Dim strComPort As String = Parameter.GetString(SCANNER_PARAMS)
'            If strComPort.ToLower.Contains("com") Then

'                Dim params() As String = strComPort.Split(Chr(44))

'                'CheckForIllegalCrossThreadCalls = False

'                port.PortName = params(0)

'                port.BaudRate = CInt(params(1))

'                Select Case params(2)
'                    Case "e", "E"
'                        port.Parity = IO.Ports.Parity.Even
'                    Case "o", "O"
'                        port.Parity = IO.Ports.Parity.Odd
'                    Case "m", "M"
'                        port.Parity = IO.Ports.Parity.Mark
'                    Case " "
'                        port.Parity = IO.Ports.Parity.Space
'                    Case Else
'                        port.Parity = IO.Ports.Parity.None
'                End Select

'                port.DataBits = CInt(params(3))

'                Select Case params(4)
'                    Case "0"
'                        port.StopBits = stopBits.None
'                    Case "1"
'                        port.StopBits = stopBits.One
'                    Case "1.5"
'                        port.StopBits = stopBits.OnePointFive
'                End Select
'                port.DtrEnable = True
'                port.RtsEnable = True
'                If port.IsOpen = False Then port.Open()
'                Trace.WriteLine("Scanner port was opened.")
'                Return True

'            ElseIf strComPort.ToLower.Contains("key") Then
'                Trace.WriteLine("Scanner port was already opened.")
'                Return True

'            Else
'                Trace.WriteLine("Scanner port was not opened.")
'                Return False

'            End If

'        Catch ex As Exception
'            Trace.WriteLine("Scanner port was not opened: " & ex.Message)
'            Return False

'        End Try

'    End Function

'    Private Sub port_DataReceived(ByVal sender As Object, ByVal e As System.IO.Ports.SerialDataReceivedEventArgs) Handles port.DataReceived

'        Try
'            Trace.WriteLine("Start: COM port Data received.")

'            Dim strInput As String = String.Empty

'            If port.IsOpen = False Then Exit Sub

'            Trace.WriteLine(port.BytesToRead.ToString & " Character(s) available to read.")
'            If port.BytesToRead = 0 Then Exit Sub

'            strInput = port.ReadLine
'            Trace.WriteLine("This was received by the scan: " & strInput)

'            If IsScannedAValidSku(strInput) = True Then
'                'Need to send the result back to the form.
'                Trace.WriteLine("Got sku - " & ScannerValue)
'                RaiseEvent ScannerInput(ScannerValue)
'            Else
'                'Invalid Sku found
'                Trace.WriteLine("No sku - Error. " & strInput)
'                RaiseEvent InvalidSku(strInput)
'            End If
'            port.DiscardInBuffer()

'            Trace.WriteLine("Finish: COM port Data received.")

'        Catch ex As Exception
'            Trace.WriteLine(" COMMS Error: " & ex.Message.ToString)

'        End Try
'    End Sub

'    Public Function IsScannedAValidSku(ByVal strInput As String) As Boolean

'        'Dim strReturnValue As String = String.Empty
'        Dim strEANValue As String = String.Empty
'        Dim strSkuNumber As String = String.Empty
'        Trace.WriteLine("Start IsScannedAValidSku: " & strInput.ToString & " len(" & strInput.Length.ToString & ")")
'        Select Case Len(strInput)
'            Case SKUN_LEN
'                ScannerValue = strInput
'                Trace.WriteLine("[6] Sku Interpreted = " & ScannerValue)
'                Return Stock.Stock.IsStockItemInDatabase(strInput)

'            Case SKUN_EAN_LEN, SKUN_EAN_LEN + 1
'                ScannerValue = strInput.Substring(1, 6)
'                Trace.WriteLine("[8] Sku Interpreted = " & ScannerValue)
'                Return Stock.Stock.IsStockItemInDatabase(ScannerValue)

'            Case SKUN_EAN, SKUN_EAN + 1
'                'Lookup sku via 16 digit EAN number
'                strInput = strInput.Substring(0, 13)
'                strEANValue = strInput.PadLeft(16, Chr(48))
'                Trace.WriteLine("[13] EAN = " & strEANValue)
'                Dim stock As Stock.Stock = stock.GetStockbyEAN(strEANValue)
'                If stock.SkuNumber <> "" Then
'                    ScannerValue = stock.SkuNumber
'                    Trace.WriteLine("[13] Sku Interpreted = " & ScannerValue)
'                    Return stock.IsStockItemInDatabase(ScannerValue)
'                Else
'                    ScannerValue = "INVALID SKU"
'                    Trace.WriteLine("No EAN Found." & ScannerValue)
'                    Return False
'                End If

'        End Select
'        Trace.WriteLine("Finish IsScannedAValidSku. ")

'        Return False

'    End Function

'    Public Sub ClosePort()
'        If port.IsOpen = True Then
'            port.Close()
'            Trace.WriteLine("Scanner port was Closed.")
'        End If
'    End Sub

'    Private disposedValue As Boolean = False        ' To detect redundant calls

'    ' IDisposable
'    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
'        If Not Me.disposedValue Then
'            If disposing Then
'                If port.IsOpen = True Then
'                    port.Close()
'                End If
'            End If

'            ' TODO: free your own state (unmanaged objects).
'            ' TODO: set large fields to null.
'        End If
'        Me.disposedValue = True
'    End Sub

'#Region " IDisposable Support "
'    ' This code added by Visual Basic to correctly implement the disposable pattern.
'    Public Sub Dispose() Implements IDisposable.Dispose
'        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
'        Dispose(True)
'        GC.SuppressFinalize(Me)
'    End Sub
'#End Region

'End Class