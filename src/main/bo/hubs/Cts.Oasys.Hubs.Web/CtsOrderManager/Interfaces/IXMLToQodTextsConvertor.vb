﻿Public Interface IXMLToQodTextsConvertor
    Function ConvertToQodTexts(ByVal deliveryLines As IEnumerable(Of IDeliveryLine), ByVal QodHeader As Qod.QodHeader) As Qod.QodTextCollection
End Interface
