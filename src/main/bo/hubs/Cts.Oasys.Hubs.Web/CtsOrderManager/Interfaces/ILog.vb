﻿Public Interface ILog
    Inherits IDisposable

    Property Header() As String
    Property Log() As System.IO.StreamWriter

    'Sub Create(ByVal Header As String)
    'Sub Create(ByVal Header As String, ByVal Path As String)
    Sub SetLogPath()
    'Sub SetLogPath(ByVal Path As String)
    Sub Write(ByVal Message As String)


    'interface required to maintain existing functionality
    Sub WriteServiceCalled()
    Sub WriteInput(ByVal Input As String)
    Sub WriteReturnEmptyString()
    Sub WriteCannotCreateRequest()
    Sub WriteCannotCreateResponse()
    Sub WriteGettingQod()
    Sub WriteQodNotFound()
    Sub WriteQodAlreadyExists()
    Sub WriteQodLineNotFound(ByVal LineNumber As String)
    Sub WriteCreatingQod()
    Sub WriteCreatingIbtOut()
    Sub WriteCreatingIbtIn()
    Sub WriteSaveSuccessful()
    Sub WriteServiceCompleted()
    Sub WriteProblemSaving()
    Sub Write()

End Interface