﻿Public Class SystemCheckFileSystemFactory
    Inherits BaseFactory(Of ISystemCheckFileSystem)

    'Private Shared m_FactoryMember As ISystemCheckFileSystem

    'Public Shared Function FactoryGet() As ISystemCheckFileSystem

    '    'new implementation
    '    If m_FactoryMember Is Nothing Then
    '        Return New SystemCheckFileSystem            'using live implementation
    '    Else
    '        Return m_FactoryMember                      'using stub implementation
    '    End If

    'End Function

    'Public Shared Sub FactorySet(ByVal obj As ISystemCheckFileSystem)

    '    m_FactoryMember = obj

    'End Sub

    Public Overrides Function Implementation() As ISystemCheckFileSystem

        Return New SystemCheckFileSystem            'using live implementation
    End Function
End Class