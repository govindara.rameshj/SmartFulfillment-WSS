﻿Public Class LogFactory

    Private Shared m_RequirementEnabled As System.Nullable(Of Boolean)
    Private Shared m_FactoryMember As ILog

    Public Shared Function FactoryGet(ByVal Type As WebServiceType) As ILog

        'new implementation
        If m_FactoryMember Is Nothing Then
            Return New Log(WebServiceName(Type))       'using live implementation
        Else
            Return m_FactoryMember                     'using stub implementation
        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As ILog)

        m_FactoryMember = obj

    End Sub

#Region "Private Procedures And Functions"

    Private Shared Function WebServiceName(ByVal Type As WebServiceType) As String

        Select Case Type
            Case WebServiceType.CtsFulfilmentRequest
                Return "CtsFulfilmentRequest"

            Case WebServiceType.CtsStatusNotification
                Return "CtsStatusNotification"

            Case WebServiceType.CtsUpdateRefund
                Return "CtsUpdateRefund"

            Case WebServiceType.CtsQoqCreate
                Return "CtsQoqCreate"

            Case WebServiceType.CtsEnableRefundCreate
                Return "CtsEnableRefundCreate"

            Case WebServiceType.CtsEnableStatusNotification
                Return "CtsEnableStatusNotification"

        End Select
        Return String.Empty

    End Function

#End Region

End Class