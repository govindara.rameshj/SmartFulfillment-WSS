﻿Public Class XMLToQodTextsConvertorFactory

    Inherits RequirementSwitchFactory(Of IXMLToQodTextsConvertor)

    Public Overrides Function ImplementationA() As IXMLToQodTextsConvertor
        Return New XMLToQodTextsConvertor
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = CBool(SwitchRepository.RequirementEnabled(-118))
    End Function

    Public Overrides Function ImplementationB() As IXMLToQodTextsConvertor
        Return New XMLToQodTextsConvertorOldWay
    End Function

End Class
