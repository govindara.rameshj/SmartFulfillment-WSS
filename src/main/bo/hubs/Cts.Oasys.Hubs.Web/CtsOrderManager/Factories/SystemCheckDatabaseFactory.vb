﻿Public Class SystemCheckDatabaseFactory
    Inherits BaseFactory(Of ISystemCheckDatabase)

    Private Shared m_FactoryMember As ISystemCheckDatabase

    'Public Shared Function FactoryGet() As ISystemCheckDatabase

    '    'new implementation
    '    If m_FactoryMember Is Nothing Then
    '        Return New SystemCheckDatabase           'using live implementation
    '    Else
    '        Return m_FactoryMember                   'using stub implementation
    '    End If

    'End Function

    Public Shared Sub FactorySet(ByVal obj As ISystemCheckDatabase)

        m_FactoryMember = obj

    End Sub

    Public Overrides Function Implementation() As ISystemCheckDatabase

        Return New SystemCheckDatabase           'using live implementation
    End Function
End Class