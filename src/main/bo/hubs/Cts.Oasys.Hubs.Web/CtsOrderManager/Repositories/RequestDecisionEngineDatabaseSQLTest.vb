﻿Imports Cts.Oasys.Hubs.Core.System.EndOfDayLock

Namespace TpWickes

    Public Class RequestDecisionEngineDatabaseSQLTest
        Implements IRequestDecisionEngineDatabase

        Public Function NitmasCompleteTaskID() As Integer Implements IRequestDecisionEngineDatabase.NitmasCompleteTaskID

            NitmasCompleteTaskID = CType(Cts.Oasys.Hubs.Core.System.Parameter.GetString(6001), Integer)

        End Function

        Public Function NitmasPendingTime() As Date Implements IRequestDecisionEngineDatabase.NitmasPendingTime

            Dim strSplit() As String

            strSplit = Cts.Oasys.Hubs.Core.System.Parameter.GetString(6000).Split(CChar(":"))

            NitmasPendingTime = New System.DateTime(Now.Year, Now.Month, Now.Day, CType(strSplit(0), Integer), CType(strSplit(1), Integer), 0)

        End Function

        Public Function NitmasStarted(ByVal NitmasPending As Date) As Boolean Implements IRequestDecisionEngineDatabase.NitmasStarted

            NitmasStarted = True
        End Function

        Public Function NitmasStopped(ByVal NitmasPending As Date, ByVal TaskID As Integer) As Boolean Implements IRequestDecisionEngineDatabase.NitmasStopped
            NitmasStopped = True
        End Function

        'Public Function NitmasStoppedDate(ByVal TaskID As Integer) As Date Implements IRequestDecisionEngineDatabase.NitmasStoppedDate
        '    Dim paramStoppedDate As SqlParameter

        '    Using con As New Connection
        '        Using com As New Command(con)
        '            com.StoredProcedureName = "NitmasStoppedDate"
        '            com.AddParameter("@TaskID", TaskID)
        '            paramStoppedDate = com.AddOutputParameter("@StoppedDate", SqlDbType.DateTime)

        '            com.ExecuteNonQuery()
        '        End Using
        '    End Using

        '    NitmasStoppedDate = CType(paramStoppedDate.Value, DateTime)
        'End Function

    End Class

End Namespace

