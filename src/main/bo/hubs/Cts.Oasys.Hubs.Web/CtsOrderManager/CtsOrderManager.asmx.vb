Option Explicit On

Imports Cts.Oasys.Hubs.Webservices.Configuration
Imports Ninject

<Web.Services.WebService(Namespace:="http://orderManagerWebServices.tpplc.co.uk")> _
<Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class CtsOrderManager
    Inherits Web.Services.WebService

    Shared ReadOnly Kernel As IKernel

    Shared Sub New()
        Dim config As New DependencyInjectionConfig()
        Kernel = config.GetKernel()
    End Sub

    Private Function CreateRequestHandler(Of T As {BaseRequestHandler})() As T
        Return Kernel.Get(Of T)()
    End Function

#Region "Web Services"

    <WebMethod()> Public Function CTS_Fulfilment_Request(ByVal CTS_Fulfilment_RequestXMLInput As String) As <System.Xml.Serialization.XmlElementAttribute("CTS_Fulfilment_RequestXMLOutput")> String

        Dim handler = CreateRequestHandler(Of CtsFulfilmentRequestHandler)()
        Dim response = handler.Handle(CTS_Fulfilment_RequestXMLInput)
        Return response

    End Function

    <WebMethod()> Public Function CTS_Status_Notification(ByVal CTS_Status_NotificationXMLInput As String) As <System.Xml.Serialization.XmlElementAttribute("CTS_Status_NotificationXMLOutput")> String

        Dim handler = CreateRequestHandler(Of CtsStatusNotificationHandler)()
        Dim response = handler.Handle(CTS_Status_NotificationXMLInput)
        Return response

    End Function

    <WebMethod()> Public Function CTS_Update_Refund(ByVal CTS_Update_RefundXMLInput As String) As <System.Xml.Serialization.XmlElementAttribute("CTS_Update_RefundXMLOutput")> String

        Dim handler = CreateRequestHandler(Of CtsUpdateRefundHandler)()
        Dim result = handler.Handle(CTS_Update_RefundXMLInput)
        Return result

    End Function

    <WebMethod()> Public Function CTS_QOD_Create(ByVal CTS_QOD_CreateXMLInput As String) As <System.Xml.Serialization.XmlElementAttribute("CTS_QOD_CreateXMLOutput")> String

        Dim handler = CreateRequestHandler(Of CtsQodCreateHandler)()
        Dim result = handler.Handle(CTS_QOD_CreateXMLInput)
        Return result

    End Function

    <WebMethod()> Public Function CTS_Enable_Refund_Create(ByVal CTS_Enable_Refund_CreateXMLInput As String) As <System.Xml.Serialization.XmlElementAttribute("CTS_Enable_Refund_CreateXMLOutput")> String

        Dim handler = CreateRequestHandler(Of CtsEnableRefundCreateHandler)()
        Dim result = handler.Handle(CTS_Enable_Refund_CreateXMLInput)
        Return result

    End Function

    <WebMethod()> Public Function CTS_Enable_Status_Notification(ByVal CTS_Enable_Status_NotificationXMLInput As String) As <System.Xml.Serialization.XmlElementAttribute("CTS_Enable_Status_NotificationXMLOutput")> String

        Dim handler = CreateRequestHandler(Of CtsEnableStatusNotificationHandler)()
        Dim response = handler.Handle(CTS_Enable_Status_NotificationXMLInput)
        Return response

    End Function

#End Region

End Class