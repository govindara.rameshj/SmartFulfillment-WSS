﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SystemCheck.aspx.vb" Inherits="Cts.Oasys.Hubs.Webservices.SystemCheck" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
           <asp:Button ID="btnDatabasePermissionCheck" runat="server" Text="Database Permission Check" />
        </div>
        <br />
        <div>
            <asp:Button ID="btnLogFileCheck" runat="server" Text="Log File Check" />
        </div>
        <br />
        <div>
            <asp:Label ID="lblError" runat="server" Text="" Font-Bold="True" ForeColor="Red" ></asp:Label>
        </div>
        <span class="Apple-style-span" 
            style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 16px; orphans: 2; text-align: -webkit-auto; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-decorations-in-effect: none; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(237, 248, 221); ">
        <br />
        <div>
            <asp:Label ID="lblError0" runat="server" Text="" Font-Bold="True" 
                ForeColor="Red" ></asp:Label>
        </div>
        </span>
        <div>
            <asp:GridView ID="gridLog" runat="server"></asp:GridView>
        </div>
        <br />
        <div>
            <asp:GridView ID="gridOutput" runat="server"></asp:GridView>
        </div>
    </form>
</body>
</html>
