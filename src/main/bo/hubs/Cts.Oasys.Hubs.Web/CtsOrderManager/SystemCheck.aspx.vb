﻿Partial Public Class SystemCheck
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnDatabasePermissionCheck_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDatabasePermissionCheck.Click
        Dim CheckDatabase As ISystemCheckDatabase

        Try
            'reset
            ResetUI()

            lblError0.Text = "Logged In As: " & Page.User.Identity.Name.ToString & " : " & Page.User.Identity.ToString

            CheckDatabase = (New SystemCheckDatabaseFactory).GetImplementation

            With gridOutput
                .DataSource = CheckDatabase.Check
                .DataBind()
            End With

        Catch ex As Exception

            lblError.Text = ex.Message

        End Try

    End Sub

    Protected Sub btnLogFileCheck_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLogFileCheck.Click
        Dim CheckFileSystem As ISystemCheckFileSystem

        Dim Log As New List(Of String)

        Try
            'reset
            ResetUI()

            CheckFileSystem = (New SystemCheckFileSystemFactory).GetImplementation

            'check folder exists
            Log.Add("Check Folder Exists")
            If CheckFileSystem.FolderExist = False Then
                Log.Add("Check Folder Exists: Failure - " & System.AppDomain.CurrentDomain.BaseDirectory & "Logs")
                Exit Sub
            Else
                Log.Add("Check Folder Exists: Success - " & System.AppDomain.CurrentDomain.BaseDirectory & "Logs")
            End If

            'create new file
            Log.Add("Create And Write To New Test File")
            Try
                CheckFileSystem.TestFileCreate()
                CheckFileSystem.TestFileWrite()
                Log.Add("Create And Write To New Test File: Success")

            Catch ex As Exception
                Log.Add("Create And Write To New Test File: Failure")
            Finally
                CheckFileSystem.TestFileClose()
            End Try

            'update existing file
            Log.Add("Open And Write To Existing Test File")
            Try
                CheckFileSystem.TestFileUpdate()
                CheckFileSystem.TestFileWrite()
                Log.Add("Open And Write To Existing Test File: Success")

            Catch ex As Exception
                Log.Add("Open And Write To Existing Test File: Failure")
            Finally
                CheckFileSystem.TestFileClose()
            End Try

            'delete file
            Log.Add("Delete Test File")
            Try
                CheckFileSystem.TestFileDelete()
                Log.Add("Delete Test File: Success")

            Catch ex As Exception
                Log.Add("Delete Test File: Failure")
            End Try

        Catch ex As Exception

            Log.Add(ex.Message)

        Finally

            'output to grid
            LogGrid(Log)

        End Try

    End Sub

#Region "Private procedures And Functions"

    Private Sub ResetUI()

        lblError.Text = String.Empty

        With gridOutput
            .DataSource = Nothing
            .DataBind()
        End With

        With gridLog
            .DataSource = Nothing
            .DataBind()
        End With

    End Sub

    Private Sub LogGrid(ByRef LocalLog As List(Of String))
        Dim DT As New DataTable

        DT.Columns.Add("Text")

        For Each strText As String In LocalLog
            DT.Rows.Add(strText)
        Next

        With gridLog
            .DataSource = DT
            .DataBind()
        End With

    End Sub

#End Region

End Class