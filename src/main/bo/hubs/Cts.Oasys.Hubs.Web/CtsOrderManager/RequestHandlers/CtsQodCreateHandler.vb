﻿Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.SystemEnvironment
Imports Cts.Oasys.Hubs.Core.System.EndOfDayLock
Imports WSS.BO.DataLayer.Model

Public Class CtsQodCreateHandler
    Inherits BaseRequestHandler

    Private Log As ILog
    Private Response As New CTSQODCreateResponse

    Public Sub New(ByVal systemEnvironment As ISystemEnvironment, ByVal dataLayerFactory As IDataLayerFactory)
        MyBase.New(systemEnvironment, dataLayerFactory)
    End Sub

    Public Overrides Function Handle(ByVal CTS_QOD_CreateXMLInput As String) As String
        Log = LogFactory.FactoryGet(WebServiceType.CtsQoqCreate)
        Using Log
            Dim XML As XDocument
            Dim XSDPath As String
            Dim XSDName As String

            Dim QodHeader As Qod.QodHeader = Nothing
            Dim SaleHeader As Qod.SaleHeader = Nothing
            Dim LinesUpdated As Integer

            Dim StockExist As Stock.Stock

            Dim blnErrorOccurred As Boolean
            Dim blnValidationPassed As Boolean

            Dim TempId As String
            Dim blnHomeDeliveryOrder As Boolean

            Dim allQodSources = QODSources.LoadAll()

            LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsQoqCreate: Start")
            LocalWrite(Log, LoggingInformation.StandardLogic, "Configuration XML File: " & AppConfiguration.GetConfigurationFilePath())
            LocalWrite(Log, LoggingInformation.StandardLogic, "Input XML Request: " & CTS_QOD_CreateXMLInput)

            LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: Start")
            If IsMonitorPing(CTS_QOD_CreateXMLInput) Then

                LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: Failed")
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsQoqCreate: End")
                Log.Dispose()
                Return String.Empty

            End If
            LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: End")

            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: Start")
            XML = XDocument.Parse(CTS_QOD_CreateXMLInput)
            XSDPath = AppConfiguration.GetXsdFilesDirectoryPath()
            XSDName = AppConfiguration.GetInputXdsFileName("CTSQODCreate")

            If ValidateXML(XSDPath & XSDName, XML) Then

                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: Failed")
                LocalWrite(Log, LoggingInformation.StandardLogic, _XMLError)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsQoqCreate: End")
                Log.Dispose()
                Return String.Empty

            End If
            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: End")

            'properly formed xml which has been validated against the xsd - validate actual values in xml
            blnErrorOccurred = False
            Try
                LocalWrite(Log, LoggingInformation.StandardLogic, "Deserialise Request: Start")
                Dim VendaSaleRequest As New CTSQODCreateRequest
                VendaSaleRequest = CType(VendaSaleRequest.Deserialise(CTS_QOD_CreateXMLInput), CTSQODCreateRequest)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Deserialise Request: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Create Response Object: Start")
                Response.SetFieldsFromRequest(VendaSaleRequest, SystemEnvironment.GetDateTimeNow())
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create Response Object: End")

                'change request - CR0026
                'check that overnight routines are not running - NITMAS
                LocalWrite(Log, LoggingInformation.StandardLogic, "Accept Request: Start")
                If AcceptRequest Is Nothing Then AcceptRequest = New RequestDecisionEngine(New RequestDecisionEngineDatabaseSQL(DataLayerFactory), SystemEnvironment)
                If AcceptRequest.AcceptRequest() = False Then

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Accept Request: Failed; Overnight Run In Progress")
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsQoqCreate: End")
                    Log.Dispose()
                    Return String.Empty

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Accept Request: End")

                blnValidationPassed = True

                'validate Order source
                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Order Source: Start")
                If Not allQodSources.AllowRemoteCreate(Response.OrderHeader.Source.Value) Then
                    blnValidationPassed = False
                    Response.OrderHeader.SourceTypeNotAccepted()
                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Order Source: End")

                'validate product sku
                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Product: Start")
                For Each Line As CTSQODCreateResponseOrderLine In Response.OrderLines

                    LocalWrite(Log, LoggingInformation.DetailedLogic, "Validate Product: Validate Product - " & Line.ProductCode.Value)
                    StockExist = Nothing
                    StockExist = Stock.Stock.GetStock(Line.ProductCode.Value)

                    If StockExist Is Nothing Then

                        Line.SetProductCodeNotFound()
                        blnValidationPassed = False
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Product: Product Missing - " & Line.ProductCode.Value)

                    End If
                Next
                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Product: End")

                'home delivery order - all lines have recordsaleonly = true
                LocalWrite(Log, LoggingInformation.StandardLogic, "Home Delivery Component In Order: Start")
                blnHomeDeliveryOrder = True
                For Each Line As CTSQODCreateResponseOrderLine In Response.OrderLines
                    If Line.RecordSaleOnly.Value = False And Line.DeliveryChargeItem.Value = False Then

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Home Delivery Component In Order: No")
                        blnHomeDeliveryOrder = False
                        Exit For

                    End If
                Next

                If blnHomeDeliveryOrder = True Then LocalWrite(Log, LoggingInformation.StandardLogic, "Home Delivery Component In Order: Yes")
                LocalWrite(Log, LoggingInformation.StandardLogic, "Home Delivery Component In Order: End")

                'home delivery validation
                If blnHomeDeliveryOrder = True Then

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Home Delivery Component In Order: Start")
                    'validate that (delivery charge = 0 and recordsaleonly = true) or (delivery charge > 0 and recordsaleonly = false)
                    If (Response.OrderHeader.DeliveryCharge.Value = 0 And Response.OrderHeader.RecordSaleOnly.Value = False) Or _
                       (Response.OrderHeader.DeliveryCharge.Value > 0 And Response.OrderHeader.RecordSaleOnly.Value = True) Then

                        Response.OrderHeader.HomeDeliveryDeliveryCharge()
                        Response.OrderHeader.HomeDeliveryRecordSaleOnly()
                        blnValidationPassed = False

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Home Delivery Component In Order: Failure - Inconsistent RecordSaleOnly And DeliveryCharge")

                    End If
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Home Delivery Component In Order: End")

                End If

                ValidateSourceAndSourceOrderNumberCombinationIsUnique(blnValidationPassed)

            Catch exc As DeserialiseException
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: Deserialize Exception - " & exc.ToString)
                blnErrorOccurred = True

            Catch exc As InvalidFieldSettingException
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: Invalid Field Setting Exception - " & exc.ToString)
                blnErrorOccurred = True

            Catch exc As Exception
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: General Exception - " & exc.ToString)
                blnErrorOccurred = True

            End Try

            If blnErrorOccurred = False Then
                If blnValidationPassed = False Then

                    Response.SuccessFlag.Value = False

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Validation Failure")
                    LocalWrite(Log, LoggingInformation.StandardLogic, Response.Serialise)
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsQoqCreate: End")
                    Log.Dispose()
                    Return Response.Serialise

                End If
            Else

                LocalWrite(Log, LoggingInformation.StandardLogic, "Exception Failure")
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsQoqCreate: End")
                Log.Dispose()
                Return String.Empty

            End If

            'create venda sale
            blnErrorOccurred = False
            Try
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create New Transaction Number: Start")
                If allQodSources.IsClickAndCollect(Response.OrderHeader.Source.Value) Then
                    TempId = GetNewEnableId()
                Else
                    TempId = GetNewId()
                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create New Transaction Number: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Create New Sale Business Object: Start")
                SaleHeader = New Qod.SaleHeader(Response, Left(TempId, 2), Right(TempId, 4))
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create New Sale Business Object: End")

                If blnHomeDeliveryOrder = False Then

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Create New QOD Business Object: Start")
                    QodHeader = New Qod.QodHeader(Response, Left(TempId, 2), Right(TempId, 4))
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Create New QOD Business Object: End")

                End If

                If allQodSources.IsClickAndCollect(Response.OrderHeader.Source.Value) Then
                    CtsFulfilmentRequestHandler.UpdateQodHeaderForSelfFulfillment(Response.OrderHeader.DeliveryInstructions.InstructionLine, QodHeader)
                    ' Fill Cashier
                    SaleHeader.CashierID = GlobalConstants.WebOrderCashierID.ToString()
                    ' Fill tender type
                    For Each salePaid As Qod.SalePaid In SaleHeader.SaleTenders
                        salePaid.TenderType = GlobalConstants.WebOrderTenderTypeID
                    Next
                End If

                Response.SuccessFlag.Value = True

                'perist to database
                Using con As New Connection
                    Try
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Start")
                        con.StartTransaction()
                        If blnHomeDeliveryOrder = False Then
                            LinesUpdated += QodHeader.PersistTree(con, False, allQodSources.IsClickAndCollect(Response.OrderHeader.Source.Value))
                            SaleHeader.OrderNumber = QodHeader.Number
                            If allQodSources.IsClickAndCollect(Response.OrderHeader.Source.Value) Then
                                CreateNewAlert(con, QodHeader, False, Log)
                            End If
                        End If

                        SaleHeader.IsClickAndCollect = allQodSources.IsClickAndCollect(Response.OrderHeader.Source.Value)

                        LinesUpdated += SaleHeader.PersistTree(con, SystemEnvironment)

                        LinesUpdated += SaleHeader.UpdateCashBalancingTables(con)

                        con.CommitTransaction()
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Success")
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: End")

                        If blnHomeDeliveryOrder = False Then Response.OrderHeader.StoreOrderNumber.Value = QodHeader.Number
                        LocalWrite(Log, LoggingInformation.StandardLogic, Response.Serialise)

                    Catch ex As Exception

                        con.RollbackTransaction()
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Failure - " & ex.ToString)
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: End")

                        Response.SuccessFlag.Value = False
                        blnErrorOccurred = True

                    End Try
                End Using

            Catch ex As Exception
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: General Exception - " & ex.ToString)
                blnErrorOccurred = True

            Finally
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsQoqCreate: End")
                Log.Dispose()
            End Try

            If blnErrorOccurred = False Then
                Return Response.Serialise
            Else
                Return String.Empty
            End If
        End Using
    End Function

    Private Sub ValidateSourceAndSourceOrderNumberCombinationIsUnique(ByRef blnValidationPassed As Boolean)
        ' Referral 814
        ' Moved this 'If' outside of HomeDelivery condition as check is not specific to Home Deliveries (just the debug/log messages that correspond to the type of order)
        'validate that no sale exists with the same venda order no
        LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Source and SourceOrderNumber combination is unique: Start")
        Dim existingHeader = Qod.GetCncHeaderForSourceAndSourceOrderNumber(Response.OrderHeader.Source.Value, Response.OrderHeader.SourceOrderNumber.Value)
        If Not existingHeader Is Nothing Then

            Response.OrderHeader.WebOrderInUse()
            blnValidationPassed = False
            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Source and SourceOrderNumber combination is unique: Failure - Source (" & Response.OrderHeader.Source.Value & ") and Source Order Number (" & Response.OrderHeader.SourceOrderNumber.Value & ") In Use")

        End If
        LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Source and SourceOrderNumber combination is unique: End")
    End Sub

End Class
