﻿Imports Cts.Oasys.Core.SystemEnvironment
Imports WSS.BO.DataLayer.Model

Public MustInherit Class BaseRequestHandler

    Private m_Error As Boolean = False
    Protected _XMLError As String = String.Empty
    Private ReadOnly _systemEnvironment As ISystemEnvironment
    Private ReadOnly _dataLayerFactory As IDataLayerFactory

    Protected ReadOnly Property SystemEnvironment As ISystemEnvironment
        Get
            Return _systemEnvironment
        End Get
    End Property

    Protected ReadOnly Property DataLayerFactory As IDataLayerFactory
        Get
            Return _dataLayerFactory
        End Get
    End Property

    Protected Sub New(ByVal systemEnvironment As ISystemEnvironment, ByVal dataLayerFactory As IDataLayerFactory)
        _systemEnvironment = systemEnvironment
        _dataLayerFactory = dataLayerFactory
    End Sub

#Region "Private Procedures And Functions"

    Protected Function IsMonitorPing(ByVal input As String) As Boolean
        Return (input = "This is a monitor ping from Order Manager")
    End Function

    Protected Function GetIbtOutNumber(ByVal Qod As Qod.QodHeader) As String

        Dim StoreId As String = ThisStore.Id4.ToString
        For Each line As Qod.QodLine In Qod.Lines
            If line.DeliverySource = StoreId Then
                Return line.DeliverySourceIbtOut
            End If
        Next
        Return String.Empty

    End Function

    Protected Sub XSDErrors(ByVal o As Object, ByVal e As ValidationEventArgs)
        Using log As New Output("XMLLogs")
            log.Write("Invalid XML: " & e.Message)
            _XMLError = e.Message
            log.Dispose()
            m_Error = True
        End Using
    End Sub

    Protected Function ValidateXML(ByVal XSDPath As String, ByVal XML As XDocument) As Boolean

        Dim sr As StreamReader = New StreamReader(XSDPath)
        Dim xsdMarkup As XElement = XElement.Parse(sr.ReadToEnd)

        Dim schemas As XmlSchemaSet = New XmlSchemaSet()
        schemas.Add("", xsdMarkup.CreateReader)

        m_Error = False
        XML.Validate(schemas, AddressOf XSDErrors)

        sr.Close()
        xsdMarkup = Nothing

        Return m_Error
    End Function

    Protected Function GetNewId() As String
        Dim strOrderNumber As String
        Dim intTillNo As Integer
        Dim intTranNo As Integer

        strOrderNumber = String.Empty
        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        'get order no
                        com.CommandText = "Select NEXT16 from SYSNUM where FKEY='01'"
                        strOrderNumber = com.ExecuteValue.ToString

                        'update order number for next time
                        intTillNo = CType(strOrderNumber.Substring(0, 2), Integer)
                        intTranNo = CType(strOrderNumber.Substring(2, 4), Integer)

                        'till  - 2 digit number between 1 and 10 formatted with leading zero
                        'trans - 4 digit number between 1 and 9999 formatted with leading zeros

                        '010001 to 019999, 020001 to 029999, ........, 090001 to 099999, 100001 to 109999, 010001

                        If intTranNo < 9999 Then
                            intTranNo += 1
                        Else
                            intTranNo = 1

                            If intTillNo < 10 Then
                                intTillNo += 1
                            Else
                                intTillNo = 1
                            End If
                        End If
                        com.CommandText = "Update SYSNUM set NEXT16=? where FKEY='01'"
                        com.AddParameter("Number", intTillNo.ToString("00") & intTranNo.ToString("0000"))
                        com.ExecuteNonQuery()

                    Case DataProvider.Sql
                        com.StoredProcedureName = My.Resources.GetNextTillTranNumber
                        com.AddParameter("@TillTranNumber", intTillNo.ToString("00") & intTranNo.ToString("0000"), SqlDbType.NChar, 6, ParameterDirection.InputOutput)
                        com.ExecuteNonQuery()
                        strOrderNumber = com.GetParameterValue("@TillTranNumber").ToString
                End Select
            End Using
        End Using
        Return strOrderNumber
    End Function

    Protected Function GetNewEnableId() As String
        Dim strOrderNumber As String
        strOrderNumber = String.Empty
        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = My.Resources.GetEnableTillTranNumber
                com.AddParameter("@TillTranNumber", "000000", SqlDbType.NChar, 6, ParameterDirection.InputOutput)
                com.ExecuteNonQuery()
                strOrderNumber = com.GetParameterValue("@TillTranNumber").ToString
            End Using
        End Using
        Return strOrderNumber
    End Function

    Protected Sub LocalWrite(ByRef Log As ILog, ByVal LogLevel As LoggingInformation, ByVal strMessage As String)

        Select Case LogLevel
            Case LoggingInformation.StandardLogic
                Log.Write(strMessage)

                'existing log system; sync debug output with log file
                If Log.GetType.FullName = "Cts.Oasys.Hubs.Webservices.Output" Then Trace.WriteLine(strMessage)

            Case LoggingInformation.DetailedLogic, LoggingInformation.BusinessObjectState, LoggingInformation.DatabaseState
                'log non-standard information only with the new log system
                If Log.GetType.FullName = "Cts.Oasys.Hubs.Webservices.Log" Then Log.Write(strMessage)

        End Select

    End Sub

    Protected Function CreateNewAlert(ByVal con As Connection, QodHeader As Qod.QodHeader, ByVal isRefund As Boolean, Log As ILog) As Integer
        Dim LinesUpdated As Integer

        Try
            LocalWrite(Log, LoggingInformation.StandardLogic, "Persist Alert To Database: Start")
            LinesUpdated = QodHeader.ClickAndCollectAlertInsert(con, isRefund)
        Catch ex As Exception
            LocalWrite(Log, LoggingInformation.StandardLogic, "Persist Alert To Database: Failure - " & ex.ToString)
        Finally
            LocalWrite(Log, LoggingInformation.StandardLogic, "Persist Alert To Database: End")
        End Try

        Return LinesUpdated

    End Function

#End Region

    Public MustOverride Function Handle(ByVal requestXml As String) As String

End Class
