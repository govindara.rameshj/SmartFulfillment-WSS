﻿Imports Cts.Oasys.Core.SystemEnvironment
Imports WSS.BO.DataLayer.Model

Public Class CtsStatusNotificationHandler
    Inherits BaseRequestHandler

    Private Log As ILog
    Private response As CTSStatusNotificationResponse
    Private QodHeader As Qod.QodHeader
    Private IBTHeaderCollection As Ibt.IBTHeaderCollection

    Public Sub New(ByVal systemEnvironment As ISystemEnvironment, ByVal dataLayerFactory As IDataLayerFactory)
        MyBase.New(systemEnvironment, dataLayerFactory)
    End Sub

    Public Overrides Function Handle(ByVal CTS_Status_NotificationXMLInput As String) As String
        Log = LogFactory.FactoryGet(WebServiceType.CtsStatusNotification)
        Using Log
            LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsStatusNotification: Start")
            LocalWrite(Log, LoggingInformation.StandardLogic, "Configuration XML File: " & AppConfiguration.GetConfigurationFilePath())
            LocalWrite(Log, LoggingInformation.StandardLogic, "Input XML Request: " & CTS_Status_NotificationXMLInput)


            LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: Start")
            If IsMonitorPing(CTS_Status_NotificationXMLInput) Then

                LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: Failed")
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsStatusNotification: End")
                Log.Dispose()
                Return String.Empty

            End If
            LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: End")

            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: Start")

            Dim XML As XDocument = XDocument.Parse(CTS_Status_NotificationXMLInput)
            Dim XSDPath As String = AppConfiguration.GetXsdFilesDirectoryPath()
            Dim XSDName As String = AppConfiguration.GetInputXdsFileName("CTSStatusNotification")

            If ValidateXML(XSDPath & XSDName, XML) Then

                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: Failed")
                LocalWrite(Log, LoggingInformation.StandardLogic, _XMLError)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsStatusNotification: End")
                Log.Dispose()
                Return String.Empty

            End If
            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: End")

            Dim request As New CTSStatusNotificationRequest
            response = New CTSStatusNotificationResponse

            Dim DoneIBT As Boolean = False

            Try
                LocalWrite(Log, LoggingInformation.StandardLogic, "Deserialise Request: Start")
                request = CType(request.Deserialise(CTS_Status_NotificationXMLInput), CTSStatusNotificationRequest)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Deserialise Request: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Create Response Object: Start")
                If CInt(request.OrderStatus.Value) = Qod.State.Delivery.IbtInAllStock Then
                    DoneIBT = True
                End If
                response.SetFieldsFromRequest(request)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create Response Object: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Load Existing QOD Order: Start")
                QodHeader = Qod.GetForOmOrderNumber(CInt(response.OMOrderNumber.Value))
                If QodHeader Is Nothing Then

                    response.SetQodNotFound()
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Load Existing QOD Order: Failure - " & response.OMOrderNumber.Value.ToString)

                    Exit Try

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Load Existing QOD Order: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Load QOD Order Lines: Start")
                If Not response.AreOrderLinesInQod(QodHeader.Lines) Then

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Load QOD Order Lines: Failure - " & response.OMOrderNumber.Value.ToString)
                    Exit Try

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Load QOD Order Lines: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Create IBT Business Object: Start")
                If response.OrderHeader.SellingStoreCode.Value = ThisStore.Id4.ToString Then

                    If CInt(response.OrderStatus.Value) = Qod.State.Delivery.IbtInAllStock AndAlso Not DoneIBT Then

                        'Do the IBTs if needed
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Create IBT Business Object: New Object Created")
                        IBTHeaderCollection = New Ibt.IBTHeaderCollection(QodHeader, request)

                    End If

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create IBT Business Object: End")

                ' try and save the data away to the database
                Using con As New Connection
                    Dim LinesUpdated As Integer = 0
                    Try
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Start")

                        con.StartTransaction()
                        If IBTHeaderCollection IsNot Nothing Then LinesUpdated += IBTHeaderCollection.Persist(con)
                        UpdateQodHeaderAndResponseIbtNumbers()
                        LinesUpdated += QodHeader.PersistTree(con)

                        con.CommitTransaction()
                        response.SuccessFlag.Value = True

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Success")
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: End")
                        LocalWrite(Log, LoggingInformation.StandardLogic, response.Serialise)

                    Catch ex As Exception
                        con.RollbackTransaction()
                        response.SuccessFlag.Value = False

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Failure - " & ex.ToString)
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: End")

                    End Try
                End Using

            Catch ex As DeserialiseException
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: Deserialize Exception - " & ex.ToString)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsStatusNotification: End")
                Log.Dispose()
                Return String.Empty

            Catch ex As InvalidFieldSettingException
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: Invalid Field Setting Exception - " & ex.ToString)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsStatusNotification: End")
                Log.Dispose()
                Return String.Empty

            Catch ex As Exception
                response.SuccessFlag.Value = False
                response.SuccessFlag.ValidationStatus = ex.ToString

                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: General Exception - " & ex.ToString)
                LocalWrite(Log, LoggingInformation.StandardLogic, response.Serialise)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsStatusNotification: End")
                Log.Dispose()
                Return response.Serialise

            End Try

            LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsStatusNotification: End")
            Log.Dispose()
            Return response.Serialise

        End Using

    End Function

    Private Sub UpdateQodHeaderAndResponseIbtNumbers()

        LocalWrite(Log, LoggingInformation.StandardLogic, "Update IBT Number In Response: Start")
        If IBTHeaderCollection IsNot Nothing AndAlso IBTHeaderCollection.Count > 0 Then
            For Each FulfilmentSite As CTSStatusNotificationResponseFulfilmentSite In response.FulfilmentSites

                If FulfilmentSite.FulfilmentSite.Value <> response.OrderHeader.SellingStoreCode.Value Then
                    FulfilmentSite.SellingStoreIBTInNumber.Value = IBTHeaderCollection.GetIBTNumber(FulfilmentSite.FulfilmentSite.Value)
                End If

            Next
        End If
        LocalWrite(Log, LoggingInformation.StandardLogic, "Update IBT Number In Response: End")

        ' #
        ' OM sends a Status Notification immediately following a response from an Update Refund where a line
        ' has been fully refunded.  However, the Update Refund has just changed the statuses so the OM ststus
        ' values are out of date, so do not overwrite the status.  This situation is identifiable by the
        ' Suspended flag being on, on the header

        LocalWrite(Log, LoggingInformation.StandardLogic, "Update Refund And Immediate OM Status Notification Situtaion: Start")
        If Not QodHeader.IsSuspended Then
            QodHeader.SetDeliveryStatus(CInt(response.OrderStatus.Value))
        End If

        For Each FulfilmentSite As CTSStatusNotificationResponseFulfilmentSite In response.FulfilmentSites

            For Each OrderLine As CTSStatusNotificationResponseFulfilmentSiteOrderLine In FulfilmentSite.OrderLines

                Dim QodLine As Qod.QodLine = QodHeader.Lines.Find(OrderLine.SellingStoreLineNo.Value)

                If FulfilmentSite.SellingStoreIBTInNumber.Value <> String.Empty Then
                    If CInt(FulfilmentSite.SellingStoreIBTInNumber.Value) > 0 Then
                        QodLine.SellingStoreIbtIn = FulfilmentSite.SellingStoreIBTInNumber.Value
                    End If
                End If

                If FulfilmentSite.FulfilmentSiteIBTOutNumber.Value <> String.Empty Then
                    If CInt(FulfilmentSite.FulfilmentSiteIBTOutNumber.Value) > 0 Then
                        QodLine.DeliverySourceIbtOut = FulfilmentSite.FulfilmentSiteIBTOutNumber.Value
                    End If
                End If

                QodLine.DeliverySource = FulfilmentSite.FulfilmentSite.Value
                ' See  # comment above
                If Not QodHeader.IsSuspended Then
                    QodLine.setDeliveryStatus(CInt(OrderLine.LineStatus.Value))
                End If

            Next
        Next
        LocalWrite(Log, LoggingInformation.StandardLogic, "Update Refund And Immediate OM Status Notification Situtaion: End")
    End Sub

End Class
