﻿Imports Cts.Oasys.Core.SystemEnvironment
Imports WSS.BO.DataLayer.Model

Public Class CtsEnableStatusNotificationHandler
    Inherits BaseRequestHandler

    Private log As ILog

    Public Sub New(ByVal systemEnvironment As ISystemEnvironment, ByVal dataLayerFactory As IDataLayerFactory)
        MyBase.New(systemEnvironment, dataLayerFactory)
        log = LogFactory.FactoryGet(WebServiceType.CtsEnableStatusNotification)
    End Sub

    Public Overrides Function Handle(ByVal requestXml As String) As String

        LocalWrite(log, LoggingInformation.StandardLogic, "Web Service - CtsEnableStatusNotification: Start")
        LocalWrite(log, LoggingInformation.StandardLogic, "Configuration XML File: " & AppConfiguration.GetConfigurationFilePath())
        LocalWrite(log, LoggingInformation.StandardLogic, "Input XML Request: " & requestXml)

        LocalWrite(log, LoggingInformation.StandardLogic, "Ping Order Manager: Start")
        If IsMonitorPing(requestXml) Then

            LocalWrite(log, LoggingInformation.StandardLogic, "Ping Order Manager: Failed")
            LocalWrite(log, LoggingInformation.StandardLogic, "Web Service - CtsEnableStatusNotification: End")
            log.Dispose()
            Return String.Empty

        End If
        LocalWrite(log, LoggingInformation.StandardLogic, "Ping Order Manager: End")

        LocalWrite(log, LoggingInformation.StandardLogic, "Validate XML: Start")

        Dim XML As XDocument = XDocument.Parse(requestXml)
        Dim XSDPath As String = AppConfiguration.GetXsdFilesDirectoryPath()
        Dim XSDName As String = AppConfiguration.GetInputXdsFileName("CTSEnableStatusNotification")

        If ValidateXML(XSDPath & XSDName, XML) Then

            LocalWrite(log, LoggingInformation.StandardLogic, "Validate XML: Failed")
            LocalWrite(log, LoggingInformation.StandardLogic, _XMLError)
            LocalWrite(log, LoggingInformation.StandardLogic, "Web Service - CtsEnableStatusNotification: End")
            log.Dispose()
            Return String.Empty

        End If
        LocalWrite(log, LoggingInformation.StandardLogic, "Validate XML: End")

        Dim request As New CTSEnableStatusNotificationRequest
        Dim response As New CTSEnableStatusNotificationResponse

        Try
            LocalWrite(log, LoggingInformation.StandardLogic, "Deserialise Request: Start")
            request = CType(request.Deserialise(requestXml), CTSEnableStatusNotificationRequest)
            Dim validRequestStatus As Boolean = FillDeliveryStatus(request)
            LocalWrite(log, LoggingInformation.StandardLogic, "Deserialise Request: End")

            LocalWrite(log, LoggingInformation.StandardLogic, "Create Response Object: Start")
            response.SetFieldsFromRequest(request)
            LocalWrite(log, LoggingInformation.StandardLogic, "Create Response Object: End")

            LocalWrite(log, LoggingInformation.StandardLogic, "Validate Order Status: Start")
            If Not validRequestStatus Then
                response.SetStatusNotAllowed()
                Exit Try
            End If
            LocalWrite(log, LoggingInformation.StandardLogic, "Validate Order Status: End")

            Dim allQodSources = QODSources.LoadAll()

            LocalWrite(log, LoggingInformation.StandardLogic, "Validate Order Source: Start")
            If Not allQodSources.IsClickAndCollect(response.Source.Value) Then
                response.SetSourceNotAccepted()
                Exit Try
            End If
            LocalWrite(log, LoggingInformation.StandardLogic, "Validate Order Source: End")

            LocalWrite(log, LoggingInformation.StandardLogic, "Load Existing QOD Order: Start")
            Dim QodHeader = Qod.GetCncHeaderForSourceAndSourceOrderNumber(response.Source.Value, response.SourceOrderNumber.Value)

            If QodHeader Is Nothing Then

                response.SetQodNotFound()
                LocalWrite(log, LoggingInformation.StandardLogic, "Load Existing QOD Order: Failure - Source " & response.Source.Value.ToString & ", SourceOrderNumber " & response.SourceOrderNumber.Value.ToString)

                Exit Try

            End If
            LocalWrite(log, LoggingInformation.StandardLogic, "Load Existing QOD Order: End")

            LocalWrite(log, LoggingInformation.StandardLogic, "Update Refund And Immediate OM Status Notification Situtaion: Start")
            QodHeader.SetDeliveryStatus(CInt(response.OrderStatus.Value))

            For Each QodLine As Qod.QodLine In QodHeader.Lines
                QodLine.setDeliveryStatus(CInt(response.OrderStatus.Value))
            Next

            LocalWrite(log, LoggingInformation.StandardLogic, "Update Refund And Immediate OM Status Notification Situtaion: End")

            response.SuccessFlag.Value = True

            ' try and save the data away to the database
            Using con As New Connection
                Dim LinesUpdated As Integer = 0
                Try
                    LocalWrite(log, LoggingInformation.StandardLogic, "Persist To Database: Start")

                    con.StartTransaction()
                    LinesUpdated += QodHeader.PersistTree(con)
                    con.CommitTransaction()

                    LocalWrite(log, LoggingInformation.StandardLogic, "Persist To Database: Success")
                    LocalWrite(log, LoggingInformation.StandardLogic, "Persist To Database: End")

                Catch ex As Exception
                    con.RollbackTransaction()
                    response.SuccessFlag.Value = False

                    LocalWrite(log, LoggingInformation.StandardLogic, "Persist To Database: Failure - " & ex.ToString)
                    LocalWrite(log, LoggingInformation.StandardLogic, "Persist To Database: End")

                    Throw ex

                End Try
            End Using

        Catch ex As DeserialiseException
            LocalWrite(log, LoggingInformation.StandardLogic, "Error State: Deserialize Exception - " & ex.ToString)
            LocalWrite(log, LoggingInformation.StandardLogic, "Web Service - CtsEnableStatusNotification: End")
            log.Dispose()
            Return String.Empty

        Catch ex As InvalidFieldSettingException
            LocalWrite(log, LoggingInformation.StandardLogic, "Error State: Invalid Field Setting Exception - " & ex.ToString)
            LocalWrite(log, LoggingInformation.StandardLogic, "Web Service - CtsEnableStatusNotification: End")
            log.Dispose()
            Return String.Empty

        Catch ex As Exception
            response.SuccessFlag.Value = False
            response.SuccessFlag.ValidationStatus = ex.ToString

            LocalWrite(log, LoggingInformation.StandardLogic, "Error State: General Exception - " & ex.ToString)
            LocalWrite(log, LoggingInformation.StandardLogic, response.Serialise)
            LocalWrite(log, LoggingInformation.StandardLogic, "Web Service - CtsEnableStatusNotification: End")
            LocalWrite(log, LoggingInformation.StandardLogic, response.Serialise)
            log.Dispose()
            Return response.Serialise

        End Try

        LocalWrite(log, LoggingInformation.StandardLogic, response.Serialise)
        LocalWrite(log, LoggingInformation.StandardLogic, "Web Service - CtsEnableStatusNotification: End")
        log.Dispose()
        Return response.Serialise

    End Function

    Private Function FillDeliveryStatus(ByRef request As CTSEnableStatusNotificationRequest) As Boolean
        Dim status = EnableStates.GetDeliveryStatus(request.OrderStatus.Value)
        If status = Nothing Then
            FillDeliveryStatus = False
            Exit Function
        End If
        request.OrderStatus.Value = status
        FillDeliveryStatus = True
    End Function

End Class
