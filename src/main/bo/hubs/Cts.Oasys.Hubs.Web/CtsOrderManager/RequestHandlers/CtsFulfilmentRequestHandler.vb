﻿Imports System.Linq
Imports Cts.Oasys.Core.SystemEnvironment
Imports Cts.Oasys.Hubs.Core.System
Imports WSS.BO.DataLayer.Model
Imports WSS.BO.DataLayer.Model.Entities.NonPersistent
Imports WSS.BO.DataLayer.Model.Repositories

Public Class CtsFulfilmentRequestHandler
    Inherits BaseRequestHandler

    Private response As CTSFulfilmentResponse
    Private qodHeader As Qod.QodHeader = Nothing
    Private ibtHeader As Ibt.IBTHeader = Nothing
    Private request As CTSFulfilmentRequest
    Private log As ILog

    Public Sub New(ByVal systemEnvironment As ISystemEnvironment, ByVal dataLayerFactory As IDataLayerFactory)
        MyBase.New(systemEnvironment, dataLayerFactory)
        log = LogFactory.FactoryGet(WebServiceType.CtsFulfilmentRequest)
    End Sub

    Private Function CheckRequestBeforeProcessing(requestXml As String) As Boolean

        LocalWrite(log, LoggingInformation.StandardLogic, "Web Service - CtsFulfilmentRequest: Start")
        LocalWrite(log, LoggingInformation.StandardLogic, "Configuration XML File: " & AppConfiguration.GetConfigurationFilePath())
        LocalWrite(log, LoggingInformation.StandardLogic, "Input XML Request: " & requestXml)

        LocalWrite(log, LoggingInformation.StandardLogic, "Ping Order Manager: Start")
        If IsMonitorPing(requestXml) Then

            LocalWrite(log, LoggingInformation.StandardLogic, "Ping Order Manager: Failed")
            LocalWrite(log, LoggingInformation.StandardLogic, "Web Service - CtsFulfilmentRequest: End")
            log.Dispose()
            Return False

        End If
        LocalWrite(log, LoggingInformation.StandardLogic, "Ping Order Manager: End")

        LocalWrite(log, LoggingInformation.StandardLogic, "Validate XML: Start")

        Dim XML As XDocument = XDocument.Parse(requestXml)
        Dim XSDPath As String = AppConfiguration.GetXsdFilesDirectoryPath()
        Dim XSDName As String = AppConfiguration.GetInputXdsFileName("CTSFulfilmentRequest")

        If ValidateXML(XSDPath & XSDName, XML) Then

            LocalWrite(log, LoggingInformation.StandardLogic, "Validate XML: Failed")
            LocalWrite(log, LoggingInformation.StandardLogic, _XMLError)
            LocalWrite(log, LoggingInformation.StandardLogic, "Web Service - CtsFulfilmentRequest: End")
            log.Dispose()
            Return False

        End If
        LocalWrite(log, LoggingInformation.StandardLogic, "Validate XML: End")

        Return True
    End Function

    Private Shared Sub UpdateQodHeaderStatuses(qodHeader As Qod.QodHeader)
        qodHeader.Lines.SetDeliveryStatus(Qod.State.Delivery.IbtOutAllStock, ThisStore.Id4)
        qodHeader.SetDeliveryStatus(Qod.State.Delivery.IbtOutAllStock)
    End Sub

    Private Sub UpdateQodHeaderAndResponseStatuses()
        LocalWrite(log, LoggingInformation.StandardLogic, "Update QOD Business Object: Start")

        UpdateQodHeaderStatuses(qodHeader)

        'Make sure the fulfilment sites and status have been updated
        response.OrderHeader.OrderStatus.Value = qodHeader.DeliveryStatus.ToString
        If ibtHeader IsNot Nothing Then response.IBTOutNumber.Value = ibtHeader.Number

        Dim Index As Integer = 0
        For Each ResponseOrderLine As CTSFulfilmentResponseOrderLine In response.OrderLines

            Dim line As Qod.QodLine = qodHeader.Lines(Index)
            line.DeliverySource = ResponseOrderLine.FulfilmentSite.Value

            If Not line.IsDeliveryChargeItem AndAlso CInt(line.DeliverySource) = ThisStore.Id4 AndAlso ibtHeader IsNot Nothing Then
                line.DeliverySourceIbtOut = ibtHeader.Number
            End If
            ResponseOrderLine.LineStatus.Value = qodHeader.Lines(Index).DeliveryStatus.ToString
            Index += 1
        Next

        LocalWrite(log, LoggingInformation.StandardLogic, "Update QOD Business Object: End")
    End Sub

    Private Sub TryAndSaveDataAwayToDatabase(ByVal FailedSave As Boolean)
        Try
            Using con As New Connection
                Dim LinesUpdated As Integer = 0

                Try
                    LocalWrite(log, LoggingInformation.StandardLogic, "Persist To Database: Start")

                    con.StartTransaction()

                    If ibtHeader IsNot Nothing Then
                        LinesUpdated += ibtHeader.Persist(con)
                    End If
                    UpdateQodHeaderAndResponseStatuses()
                    LinesUpdated += qodHeader.PersistTree(con)

                    con.CommitTransaction()
                    response.FulfillingSiteOrderNumber.Value = qodHeader.Number
                    response.SuccessFlag.Value = True

                    LocalWrite(log, LoggingInformation.StandardLogic, "Persist To Database: Success")
                    LocalWrite(log, LoggingInformation.StandardLogic, "Persist To Database: End")

                Catch ex As Exception
                    con.RollbackTransaction()
                    response.SuccessFlag.Value = False

                    LocalWrite(log, LoggingInformation.StandardLogic, "Persist To Database: Failure - " & ex.ToString)
                    LocalWrite(log, LoggingInformation.StandardLogic, "Persist To Database: End")

                    Throw ex

                End Try

            End Using

        Catch ex As Exception
            response.SuccessFlag.Value = False
            FailedSave = True

        Finally
            LocalWrite(log, LoggingInformation.StandardLogic, response.Serialise)

        End Try
    End Sub

    Private Shared Sub UpdateQodTextsFromDeliveryLines(deliveryLines As IEnumerable(Of IDeliveryLine), qodHeader As Qod.QodHeader)
        Dim factory As IXMLToQodTextsConvertor = (New XMLToQodTextsConvertorFactory).GetImplementation
        Dim Texts As Qod.QodTextCollection = factory.ConvertToQodTexts(deliveryLines, qodHeader)
        For Each txt As Qod.QodText In Texts
            Dim newtxt As Qod.QodText = qodHeader.Texts.FindOrCreate(txt.Number, txt.TextType)
            newtxt.SellingStoreOrderId = txt.SellingStoreOrderId
            newtxt.SellingStoreId = txt.SellingStoreId
            newtxt.Text = txt.Text
        Next
    End Sub

    Public Overrides Function Handle(requestXml As String) As String

        Using log

            Dim continueProcessing = CheckRequestBeforeProcessing(requestXml)
            If Not continueProcessing Then
                Return String.Empty
            End If

            Dim FailedSave As Boolean = False

            Try
                LocalWrite(log, LoggingInformation.StandardLogic, "Deserialise Request: Start")
                request = New CTSFulfilmentRequest()
                request = CType(request.Deserialise(requestXml), CTSFulfilmentRequest)
                LocalWrite(log, LoggingInformation.StandardLogic, "Deserialise Request: End")

                LocalWrite(log, LoggingInformation.StandardLogic, "Create Response Object: Start")
                response = New CTSFulfilmentResponse()
                response.SetFieldsFromRequest(request)
                LocalWrite(log, LoggingInformation.StandardLogic, "Create Response Object: End")

                LocalWrite(log, LoggingInformation.StandardLogic, "Store No: Start")
                Dim thisStoreId As Integer = ThisStore.Id4
                LocalWrite(log, LoggingInformation.StandardLogic, "Store No: End")

                LocalWrite(log, LoggingInformation.StandardLogic, "Valid delivery slot for OVC order: Start")
                If Not response.IsDeliverySlotFilledForOVCOrder() Then
                    LocalWrite(log, LoggingInformation.StandardLogic, "Valid delivery slot for OVC order: No")
                    Exit Try
                End If
                LocalWrite(log, LoggingInformation.StandardLogic, "Valid delivery slot for OVC order: End")

                LocalWrite(log, LoggingInformation.StandardLogic, "Valid Fulfilment Site: Start")
                If Not response.IsTargetFulfilmentSiteValid(thisStoreId) Then
                    LocalWrite(log, LoggingInformation.StandardLogic, "Valid Fulfilment Site: No")
                    Exit Try
                End If
                LocalWrite(log, LoggingInformation.StandardLogic, "Valid Fulfilment Site: End")

                LocalWrite(log, LoggingInformation.StandardLogic, "Load Existing QOD Order: Start")
                Dim sellingStoreId As String = response.OrderHeader.SellingStoreCode.Value
                qodHeader = Qod.GetForOmOrderNumber(CInt(response.OrderHeader.OMOrderNumber.Value))
                If qodHeader Is Nothing AndAlso CInt(sellingStoreId) = thisStoreId Then

                    response.SetQodNotFound()
                    LocalWrite(log, LoggingInformation.StandardLogic, "Load Existing QOD Order: Failure - " & response.OrderHeader.OMOrderNumber.Value.ToString)
                    Exit Try

                End If
                LocalWrite(log, LoggingInformation.StandardLogic, "Load Existing QOD Order: End")

                LocalWrite(log, LoggingInformation.StandardLogic, "Fulfilling Store Processing: Start")
                If CInt(sellingStoreId) <> thisStoreId Then

                    LocalWrite(log, LoggingInformation.StandardLogic, "Fulfilling Store Processing: Non Selling Store")

                    If qodHeader IsNot Nothing Then

                        response.SetFieldsFromQod(qodHeader)
                        response.SuccessFlag.Value = True
                        response.IBTOutNumber.Value = GetIbtOutNumber(qodHeader) '*****
                        response.FulfillingSiteOrderNumber.Value = qodHeader.Number

                        LocalWrite(log, LoggingInformation.StandardLogic, "Fulfilling Store Processing: QOD exists")
                        LocalWrite(log, LoggingInformation.StandardLogic, response.Serialise)
                        Exit Try

                    End If

                    LocalWrite(log, LoggingInformation.StandardLogic, "Create New QOD Order Business Object: Start")
                    qodHeader = Qod.CreateFromCtsFulfillmentResponse(response)
                    LocalWrite(log, LoggingInformation.StandardLogic, "Create New QOD Order Business Object: Finish")

                    'Check and fill delivery slot
                    If qodHeader.WasCreatedInStore() AndAlso String.IsNullOrEmpty(qodHeader.RequiredDeliverySlotID) Then
                        Dim slotId As String = CInt(qodHeader.DetermineDeliverySlot(qodHeader.DateDelivery.Value)).ToString()

                        Dim repo = DataLayerFactory.Create(Of IDictionariesRepository)()
                        Dim slot = repo.GetDeliverySlots().First(Function(value As DeliverySlot) value.Id = slotId)
                        qodHeader.SetDelierySlot(slot)
                    End If

                    'Check that all SKUs exist in this store and create all of the order lines
                    LocalWrite(log, LoggingInformation.StandardLogic, "Process Lines: Start")
                    For Each responseOrderLine As CTSFulfilmentResponseOrderLine In response.OrderLines

                        If responseOrderLine.FulfilmentSite.Value = ThisStore.Id4.ToString Then

                            LocalWrite(log, LoggingInformation.StandardLogic, "Validate Product: Start")
                            'Check this SKU
                            Dim sku As Stock.Stock = Stock.Stock.GetStock(responseOrderLine.ProductCode.Value)

                            If sku Is Nothing Then
                                responseOrderLine.SetProductCodeNotFound()
                                response.SuccessFlag.Value = False

                                LocalWrite(log, LoggingInformation.StandardLogic, "Validate Product: Missing Product - " & responseOrderLine.ProductCode.Value)
                                LocalWrite(log, LoggingInformation.StandardLogic, response.Serialise)
                                Exit Try
                            End If
                            LocalWrite(log, LoggingInformation.StandardLogic, "Validate Product: End")

                        End If

                        LocalWrite(log, LoggingInformation.StandardLogic, "Create New QOD Line Business Object: Start")
                        Dim QodLine As Qod.QodLine = qodHeader.Lines.AddNew(responseOrderLine.ProductCode.Value, responseOrderLine.SellingPrice.Value)

                        QodLine.QtyOrdered = CInt(responseOrderLine.TotalOrderQuantity.Value)
                        QodLine.QtyTaken = CInt(responseOrderLine.QuantityTaken.Value)
                        QodLine.QtyToDeliver = QodLine.QtyOrdered - QodLine.QtyTaken
                        QodLine.DeliverySource = responseOrderLine.FulfilmentSite.Value
                        QodLine.IsDeliveryChargeItem = responseOrderLine.DeliveryChargeItem.Value
                        QodLine.SellingStoreId = qodHeader.SellingStoreId
                        QodLine.SellingStoreOrderId = qodHeader.SellingStoreOrderId

                        If CInt(responseOrderLine.LineStatus.Value) = Qod.State.Delivery.DeliveredNotifyOk Then
                            QodLine.DeliveryStatus = Qod.State.Delivery.DeliveredStatusOk
                        Else
                            QodLine.DeliveryStatus = CInt(responseOrderLine.LineStatus.Value)
                        End If

                        If QodLine.IsDeliveryChargeItem Then
                            QodLine.QtyToDeliver = 0
                            QodLine.SetDeliveredStatusOk()
                        Else
                            qodHeader.QtyOrdered += QodLine.QtyOrdered
                            qodHeader.QtyTaken += QodLine.QtyTaken
                        End If
                        LocalWrite(log, LoggingInformation.StandardLogic, "Create New QOD Line Business Object: End")

                    Next
                    LocalWrite(log, LoggingInformation.StandardLogic, "Process Lines: End")

                    'Add any instruction texts
                    LocalWrite(log, LoggingInformation.StandardLogic, "Process Delivery Instruction: Start")
                    UpdateQodTextsFromDeliveryLines(response.OrderHeader.DeliveryInstructions.InstructionLine, qodHeader)
                    LocalWrite(log, LoggingInformation.StandardLogic, "Process Delivery Instruction: End")

                    'Create the IBT out
                    LocalWrite(log, LoggingInformation.StandardLogic, "Create IBT Business Object: Start")
                    ibtHeader = New Ibt.IBTHeader(qodHeader)
                    LocalWrite(log, LoggingInformation.StandardLogic, "Create IBT Business Object: End")
                Else
                    UpdateQodTextsFromDeliveryLines(response.OrderHeader.DeliveryInstructions.InstructionLine, qodHeader)
                End If

                LocalWrite(log, LoggingInformation.StandardLogic, "Fulfilling Store Processing: End")

                TryAndSaveDataAwayToDatabase(FailedSave)

                If response.IsSuccessful() Then
                    Try
                        If qodHeader.IsForDelivery Then

                            Dim repo = DataLayerFactory.Create(Of IExternalRequestRepository)()

                            If qodHeader.WasCreatedInStore() Then
                                LocalWrite(log, LoggingInformation.StandardLogic, "Generate Allocate capacity ExternalRequest: Start")
                                qodHeader.GenerateAllocateUpdateCapacityRequest(repo)
                                LocalWrite(log, LoggingInformation.StandardLogic, "Generate Allocate capacity ExternalRequest: End")
                            End If

                            If Parameter.ShouldNotifyDeliveryService Then
                                LocalWrite(log, LoggingInformation.StandardLogic, "Generate New consignment ExternalRequest: Start")
                                qodHeader.GenerateNewConsignmentRequest(repo)
                                LocalWrite(log, LoggingInformation.StandardLogic, "Generate New consignment ExternalRequest: End")
                            End If
                        End If
                    Catch ex As Exception
                        LocalWrite(log, LoggingInformation.StandardLogic, String.Format("Generate ExternalRequest: Exception - {0}", ex))
                    End Try
                End If


            Catch ex As DeserialiseException
                LocalWrite(log, LoggingInformation.StandardLogic, "Error State: Deserialize Exception - " & ex.ToString)
                LocalWrite(log, LoggingInformation.StandardLogic, "Web Service - CtsFulfilmentRequest: End")
                log.Dispose()
                Return String.Empty

            Catch ex As InvalidFieldSettingException
                LocalWrite(log, LoggingInformation.StandardLogic, "Error State: Invalid Field Setting Exception - " & ex.ToString)
                LocalWrite(log, LoggingInformation.StandardLogic, "Web Service - CtsFulfilmentRequest: End")
                log.Dispose()
                Return String.Empty

            Catch ex As Exception
                response.SuccessFlag.Value = False
                response.SuccessFlag.ValidationStatus = ex.ToString

                LocalWrite(log, LoggingInformation.StandardLogic, "Error State: General Exception - " & ex.ToString)
                LocalWrite(log, LoggingInformation.StandardLogic, response.Serialise)

            End Try

            LocalWrite(log, LoggingInformation.StandardLogic, "Web Service - CtsFulfilmentRequest: End")
            log.Dispose()
            Return response.Serialise

        End Using

    End Function

    ' Do essential parts of fulfillment as it is perfomerd in the selling store for all order line. The case for C&C
    Public Shared Sub UpdateQodHeaderForSelfFulfillment(deliveryLines As IEnumerable(Of IDeliveryLine), qodHeader As Qod.QodHeader)
        UpdateQodTextsFromDeliveryLines(deliveryLines, qodHeader)

        ' Extract from UpdateQodHeaderAndResponseStatuses for self-fulfillment
        UpdateQodHeaderStatuses(qodHeader)

        For Each qodLine In qodHeader.Lines
            qodLine.DeliverySource = ThisStore.Id4.ToString
        Next
    End Sub

End Class
