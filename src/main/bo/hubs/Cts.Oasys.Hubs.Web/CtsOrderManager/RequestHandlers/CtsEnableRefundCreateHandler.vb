﻿Imports System.Linq
Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.SystemEnvironment
Imports Cts.Oasys.Hubs.Core
Imports Cts.Oasys.Hubs.Core.System.EndOfDayLock
Imports WSS.BO.DataLayer.Model

Public Class CtsEnableRefundCreateHandler
    Inherits BaseRequestHandler

    Public Sub New(ByVal systemEnvironment As ISystemEnvironment, ByVal dataLayerFactory As IDataLayerFactory)
        MyBase.New(systemEnvironment, dataLayerFactory)
    End Sub

    Public Overrides Function Handle(ByVal CTS_Refund_CreateXMLInput As String) As String
        Dim Log As ILog

        Log = LogFactory.FactoryGet(WebServiceType.CtsEnableRefundCreate)
        Using Log
            Dim XML As XDocument
            Dim XSDPath As String
            Dim XSDName As String

            Dim VendaRefundRequest As New CTSEnableRefundCreateRequest
            Dim Response As New CTSEnableRefundCreateResponse

            Dim ExistingQODOrderCollection As New Qod.QodHeaderCollection
            Dim ExistingQODOrder As Qod.QodHeader
            Dim ExistingSaleCollection As New Qod.SaleHeaderCollection
            Dim ExistingSale As Qod.SaleHeader

            Dim allQodSources = QODSources.LoadAll()

            LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsEnableRefundCreate: Start")
            LocalWrite(Log, LoggingInformation.StandardLogic, "Configuration XML File: " & AppConfiguration.GetConfigurationFilePath())
            LocalWrite(Log, LoggingInformation.StandardLogic, "Input XML Request: " & CTS_Refund_CreateXMLInput)

            LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: Start")
            If IsMonitorPing(CTS_Refund_CreateXMLInput) Then

                LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: Failed")
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsEnableRefundCreate: End")
                Log.Dispose()
                Return String.Empty

            End If
            LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: End")

            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: Start")
            XML = XDocument.Parse(CTS_Refund_CreateXMLInput)
            XSDPath = AppConfiguration.GetXsdFilesDirectoryPath()
            XSDName = AppConfiguration.GetInputXdsFileName("CTSEnableRefundCreate")

            If ValidateXML(XSDPath & XSDName, XML) = True Then

                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: Failed")
                LocalWrite(Log, LoggingInformation.StandardLogic, _XMLError)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsEnableRefundCreate: End")
                Log.Dispose()
                Return String.Empty

            End If
            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: End")

            Try
                LocalWrite(Log, LoggingInformation.StandardLogic, "Deserialise Request: Start")
                VendaRefundRequest = CType(VendaRefundRequest.Deserialise(CTS_Refund_CreateXMLInput), CTSEnableRefundCreateRequest)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Deserialise Request: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Create Response Object: Start")
                Response.SetFieldsFromRequest(VendaRefundRequest)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create Response Object: End")

                'change request - CR0026
                'check that overnight routines are not running - NITMAS
                LocalWrite(Log, LoggingInformation.StandardLogic, "Accept Request: Start")
                If AcceptRequest Is Nothing Then AcceptRequest = New RequestDecisionEngine(New RequestDecisionEngineDatabaseSQL(DataLayerFactory), SystemEnvironment)
                If AcceptRequest.AcceptRequest() = False Then

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Accept Request: Failed; Overnight Run In Progress")
                    Return String.Empty

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Accept Request: End")

                'validate Order source
                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Order Source: Start")
                If Not allQodSources.IsClickAndCollect(Response.RefundHeader.Source.Value) Then
                    Response.SuccessFlag.Value = False
                    Response.RefundHeader.SourceTypeNotAccepted()
                    Return Response.Serialise

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Order Source: End")

                'validate that refund contains lines - either products or delivery charge
                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Refund Lines: Start")
                If Not (Response.RefundLines.Any() OrElse Response.RefundHeader.DeliveryChargeRefundValue.Value > 0) Then
                    Response.SuccessFlag.Value = False
                    Response.RefundHeader.NoRefundLines()
                    Return Response.Serialise

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Refund Lines: End")


                Dim blnAllExistingSKUsFound As Boolean
                Dim blnExistingSaleDeliveryLineFound As Boolean

                'validate product sku
                blnAllExistingSKUsFound = True
                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Product: Start")
                For Each VendaRefundLine As CTSEnableRefundCreateResponseRefundLine In Response.RefundLines

                    LocalWrite(Log, LoggingInformation.DetailedLogic, "Validate Product: Validate Product - " & VendaRefundLine.ProductCode.Value)
                    Dim StockExist = Stock.Stock.GetStock(VendaRefundLine.ProductCode.Value)

                    If StockExist Is Nothing Then

                        blnAllExistingSKUsFound = False
                        VendaRefundLine.SetProductCodeNotFound()
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Product: Product Missing - " & VendaRefundLine.ProductCode.Value)

                    End If
                Next
                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Product: End")

                If blnAllExistingSKUsFound = False Then

                    Response.SuccessFlag.Value = False

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Product: Failure")
                    LocalWrite(Log, LoggingInformation.StandardLogic, Response.Serialise)
                    Return Response.Serialise

                End If


                'QOD
                LocalWrite(Log, LoggingInformation.StandardLogic, "Load Existing QOD Order: Start")
                ExistingQODOrder = Qod.GetCncHeaderForSourceAndSourceOrderNumber(Response.RefundHeader.Source.Value, Response.RefundHeader.SourceOrderNumber.Value)

                If ExistingQODOrder Is Nothing Then
                    Response.RefundHeader.SourceOrderNumberNotFound()
                    Response.SuccessFlag.Value = False

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Load Existing QOD Order: Failure - Source " & Response.RefundHeader.Source.Value.ToString & ", SourceOrderNumber " & Response.RefundHeader.SourceOrderNumber.Value.ToString)
                    LocalWrite(Log, LoggingInformation.StandardLogic, Response.Serialise)
                    Return Response.Serialise
                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Load Existing QOD Order: End")


                LocalWrite(Log, LoggingInformation.StandardLogic, "Standard Order - Load Existing Sale Order: Start")
                ExistingSaleCollection.LoadExistingSale(ExistingQODOrder.TranDate.Value, ExistingQODOrder.TranTill, ExistingQODOrder.TranNumber)

                'validate sale exist
                If ExistingSaleCollection.Count = 0 Then
                    Response.RefundHeader.SaleNotFound()
                    Response.SuccessFlag.Value = False

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Standard Order - Load Existing Sale Order: Failure")
                    LocalWrite(Log, LoggingInformation.StandardLogic, Response.Serialise)
                    Return Response.Serialise

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Standard Order - Load Existing Sale Order: End")


                ExistingSale = ExistingSaleCollection.Item(0)

                'validate line exist, will either go to corlin or dlline depending on whether store line no exist
                '
                'line(s) with recordsaleonly set to true are held in dlline only
                '
                'primary key for dlline : DATE1, TILL, TRAN, NUMB
                'since we are not going to be provided with line number for line(s) with recordsaleonly set to true
                'we are going to assume that sku will be unique

                LocalWrite(Log, LoggingInformation.StandardLogic, "Standard Order - Validate Line(s) Exist: Start")

                Dim blnAllExistingLinesFound As Boolean = True

                For Each requestLine As CTSEnableRefundCreateResponseRefundLine In Response.RefundLines
                    LocalWrite(Log, LoggingInformation.DetailedLogic, "Standard Order - Validate Line(s) Exist: Check QOD Table CORLIN. Source Line Number (" & requestLine.SourceLineNo.Value & ")")

                    'find line in corlin
                    Dim tempRequestLine = requestLine
                    Dim qodLine = ExistingQODOrder.Lines.FirstOrDefault(Function(line) line.SourceOrderLineNo = CInt(tempRequestLine.SourceLineNo.Value) AndAlso line.SkuNumber = tempRequestLine.ProductCode.Value)
                    If Not qodLine Is Nothing Then
                        LocalWrite(Log, LoggingInformation.DetailedLogic, "Standard Order - Validate Line(s) Exist: CORLIN Line Found")
                    Else
                        LocalWrite(Log, LoggingInformation.DetailedLogic, "Standard Order - Validate Line(s) Exist: CORLIN Line Not Found")
                        requestLine.SourceLineNotFound()
                        blnAllExistingLinesFound = False
                    End If
                Next

                If blnAllExistingLinesFound = False Then

                    Response.SuccessFlag.Value = False

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Standard Order - Validate Line(s) Exist: Failure")
                    LocalWrite(Log, LoggingInformation.StandardLogic, Response.Serialise)
                    Return Response.Serialise

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Standard Order - Validate Line(s) Exist: End")

                'validate that delivery line exist if applicable
                If Response.RefundHeader.DeliveryChargeRefundValue.Value > 0 Then

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Delivery Line: Start")
                    blnExistingSaleDeliveryLineFound = False
                    For Each ExistingSaleLine As Qod.SaleLine In ExistingSale.SaleLines

                        If ExistingSaleLine.SKUNumber = "805111" Then
                            blnExistingSaleDeliveryLineFound = True
                            Exit For
                        End If

                    Next
                    If blnExistingSaleDeliveryLineFound = False Then

                        Response.RefundHeader.ExistingDeliveryNotFound()
                        Response.SuccessFlag.Value = False

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Delivery Line: Failure")
                        LocalWrite(Log, LoggingInformation.StandardLogic, Response.Serialise)
                        Return Response.Serialise

                    End If
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Delivery Line: End")

                End If

                LocalWrite(Log, LoggingInformation.StandardLogic, "Create New Transaction Number: Start")
                Dim tempId = GetNewEnableId()
                Dim tillTran = Left(tempId, 2)
                Dim tranNumber = Right(tempId, 4)

                LocalWrite(Log, LoggingInformation.StandardLogic, "Create New Transaction Number: End")
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create New Sale Business Object: Start")

                Dim SaleHeader = New Qod.SaleHeader(Response, tillTran, tranNumber, ExistingQODOrder, ExistingSale)

                If allQodSources.IsClickAndCollect(Response.RefundHeader.Source.Value) Then
                    ' Fill tender type
                    For Each salePaid As Qod.SalePaid In SaleHeader.SaleTenders
                        salePaid.TenderType = GlobalConstants.WebOrderTenderTypeID
                    Next
                End If

                LocalWrite(Log, LoggingInformation.StandardLogic, "Create New Sale Business Object: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Update Response Object: Start")
                ExistingQODOrder.MakeRefundFromEnable(Response, tillTran, tranNumber)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Update Response Object: End")

                If ExistingQODOrder.Status = "1" Then
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Update Refund Status: Start")
                    ExistingQODOrder.SetDeliveryStatus(Qod.State.Delivery.UndeliveredStatusOk)

                    For Each QodLine As Qod.QodLine In ExistingQODOrder.Lines
                        QodLine.setDeliveryStatus(CInt(Qod.State.Delivery.UndeliveredStatusOk))
                    Next
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Update Refund Status: End")
                End If

                Response.SuccessFlag.Value = True

                'write to database
                Using con As New Connection
                    Try

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Start")

                        con.StartTransaction()

                        ExistingQODOrder.PersistTree(con)

                        If allQodSources.IsClickAndCollect(Response.RefundHeader.Source.Value) AndAlso ExistingQODOrder.DeliveryStatus < CInt(Qod.State.Delivery.DeliveredStatusOk) AndAlso Response.RefundLines.Any() Then
                            CreateNewAlert(con, ExistingQODOrder, True, Log)
                        End If

                        SaleHeader.IsClickAndCollect = allQodSources.IsClickAndCollect(Response.RefundHeader.Source.Value)
                        SaleHeader.PersistTree(con, SystemEnvironment)
                        SaleHeader.UpdateCashBalancingTables(con)

                        con.CommitTransaction()

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Success")
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: End")
                        LocalWrite(Log, LoggingInformation.StandardLogic, Response.Serialise)

                    Catch ex As Exception

                        con.RollbackTransaction()
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Failure - " & ex.ToString)
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: End")

                        Response.SuccessFlag.Value = False
                        Throw ex

                    End Try
                End Using

                Return Response.Serialise

            Catch ex As DeserialiseException
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: Deserialize Exception - " & ex.ToString)
                Return String.Empty

            Catch ex As InvalidFieldSettingException
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: Invalid Field Setting Exception - " & ex.ToString)
                Return String.Empty

            Catch ex As Exception
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: General Exception - " & ex.ToString)
                Return String.Empty

            Finally
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsRefundCreate: End")
                Log.Dispose()
            End Try
        End Using
    End Function

End Class
