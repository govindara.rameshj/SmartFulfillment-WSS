﻿Imports Cts.Oasys.Core.SystemEnvironment
Imports Cts.Oasys.Hubs.Core.System
Imports WSS.BO.DataLayer.Model
Imports WSS.BO.DataLayer.Model.Repositories

Public Class CtsUpdateRefundHandler
    Inherits BaseRequestHandler

    Private response As CTSUpdateRefundResponse
    Dim qodHeader As Qod.QodHeader

    Public Sub New(ByVal systemEnvironment As ISystemEnvironment, ByVal dataLayerFactory As IDataLayerFactory)
        MyBase.New(systemEnvironment, dataLayerFactory)
    End Sub

    Public Overrides Function Handle(ByVal CTS_Update_RefundXMLInput As String) As String
        Dim Log As ILog

        Log = LogFactory.FactoryGet(WebServiceType.CtsUpdateRefund)
        Using Log
            LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsUpdateRefund: Start")
            LocalWrite(Log, LoggingInformation.StandardLogic, "Configuration XML File: " & AppConfiguration.GetConfigurationFilePath())
            LocalWrite(Log, LoggingInformation.StandardLogic, "Input XML Request: " & CTS_Update_RefundXMLInput)

            LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: Start")
            If IsMonitorPing(CTS_Update_RefundXMLInput) Then

                LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: Failed")
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsUpdateRefund: End")
                Log.Dispose()
                Return String.Empty

            End If
            LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: End")

            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: Start")

            Dim XML As XDocument = XDocument.Parse(CTS_Update_RefundXMLInput)
            Dim XSDPath As String = AppConfiguration.GetXsdFilesDirectoryPath()
            Dim XSDName As String = AppConfiguration.GetInputXdsFileName("CTSUpdateRefund")

            If ValidateXML(XSDPath & XSDName, XML) Then

                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: Failed")
                LocalWrite(Log, LoggingInformation.StandardLogic, _XMLError)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsUpdateRefund: End")
                Log.Dispose()
                Return String.Empty

            End If
            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: End")

            Dim request As New CTSUpdateRefundRequest
            response = New CTSUpdateRefundResponse

            Dim IBTHeaderCollection As New Ibt.IBTHeaderCollection
            Dim IBTHeader As Ibt.IBTHeader = Nothing
            Dim ibtNumber As Integer = 0

            Try
                LocalWrite(Log, LoggingInformation.StandardLogic, "Deserialise Request: Start")
                request = CType(request.Deserialise(CTS_Update_RefundXMLInput), CTSUpdateRefundRequest)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Deserialise Request: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Create Response Object: Start")
                response.SetFieldsFromRequest(request)
                response.DateTimeStamp.Value = request.DateTimeStamp
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create Response Object: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Store No: Start")
                Dim StoreId As Integer = ThisStore.Id4
                LocalWrite(Log, LoggingInformation.StandardLogic, "Store No: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Load Existing QOD Order: Start")
                qodHeader = Qod.GetForOmOrderNumber(CInt(response.OMOrderNumber.Value))
                If qodHeader Is Nothing Then

                    response.SetQodNotFound()
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Load Existing QOD Order: Failure - " & response.OMOrderNumber.Value)
                    Exit Try

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Load Existing QOD Order: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Load QOD Order Lines: Start")
                If Not response.AreRefundLinesInQod(qodHeader.Lines) Then

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Load QOD Order Lines: Failure - " & response.OMOrderNumber.Value.ToString)
                    Exit Try

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Load QOD Order Lines: End")

                'CR0031 - We need to allow a venda refund if the eStore warehouse store number 076 parameter, is the 
                'current(store) and the selling store is equal to the eStore SellingStore store 120 parameter
                'and the deliver status is greater or equal to 900 the allow the refund to progress.
                LocalWrite(Log, LoggingInformation.StandardLogic, "EStore Warehouse Refund: Start")

                Dim blnEstoreRefundReject As Boolean = True


                LocalWrite(Log, LoggingInformation.StandardLogic, "EStore Warehouse Refund: Store 076 Test")
                If Core.System.Parameter.EstoreWarehouseNumber = ThisStore.Id4 Then
                    If qodHeader.DeliveryStatus >= Qod.State.Delivery.DeliveredCreated And Core.System.Parameter.EstoreSellingStoreNumber = qodHeader.SellingStoreId Then

                        LocalWrite(Log, LoggingInformation.StandardLogic, "EStore Warehouse Refund: Store 076 Test Negative")
                        blnEstoreRefundReject = False

                    End If
                End If

                If blnEstoreRefundReject = True Then 'CR0031 - This will only be false if estore (076) is the fulfiller and the delivery status for the order is >= 900
                    If Not qodHeader.IsRefundable Then

                        response.SetQodNotRefundable()
                        LocalWrite(Log, LoggingInformation.StandardLogic, "EStore Warehouse Refund: Order Not Refundable")
                        Exit Try

                    End If
                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "EStore Warehouse Refund: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Process Refunds: Start")
                For Each OrderRefund As CTSUpdateRefundResponseOrderRefund In response.OrderRefunds

                    LocalWrite(Log, LoggingInformation.StandardLogic, "IBT Processing: Start")
                    If Not OrderRefund.RefundStoreCode.Value = StoreId.ToString Then
                        LocalWrite(Log, LoggingInformation.StandardLogic, "This Is Not Refund Store: Create IBT Business Object")
                        'This is not the refund store, so do an IBT in!
                        IBTHeader = New Ibt.IBTHeader(request) 'Ref 807/792 MO'C 24/05/2011 - Don't process IBT when fullfilmentSite is nothing
                        If IBTHeader.Lines.Count > 0 Then  'We have a cancelled qantity to IBT
                            ibtNumber -= 1
                            IBTHeader.Number = CStr(ibtNumber)
                            For Each FulfilmentSite As CTSUpdateRefundResponseOrderRefundFulfilmentSite In OrderRefund.FulfilmentSites
                                If FulfilmentSite.FulfilmentSiteCode.Value = StoreId.ToString Then
                                    FulfilmentSite.FulfilmentSiteIBTInNumber = IBTHeader.Number
                                End If
                            Next
                            IBTHeaderCollection.Add(IBTHeader)
                        Else 'All refund lines for this store were returned, not cancelled, so no IBT is needed
                            IBTHeader = Nothing
                        End If
                    End If
                    LocalWrite(Log, LoggingInformation.StandardLogic, "IBT Processing: End")

                    LocalWrite(Log, LoggingInformation.StandardLogic, "QOD Processing: Start")
                    For Each ResponseRefundLine As CTSUpdateRefundResponseOrderRefundRefundLine In OrderRefund.RefundLines

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Find QOD Refund Order: Start")
                        Dim QodRefund As Qod.QodRefund = qodHeader.Refunds.Find(CInt(ResponseRefundLine.SellingStoreLineNo.Value), CInt(OrderRefund.RefundStoreCode.Value), OrderRefund.RefundDate.Value, OrderRefund.RefundTill.Value, OrderRefund.RefundTransaction.Value)
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Find QOD Refund Order: End")

                        If QodRefund Is Nothing Then

                            LocalWrite(Log, LoggingInformation.StandardLogic, "QOD Refund Order Not Found")
                            QodRefund = qodHeader.Refunds.AddNew
                            QodRefund.OrderNumber = qodHeader.Number
                            QodRefund.Number = ResponseRefundLine.SellingStoreLineNo.Value
                            QodRefund.RefundStoreId = CInt(OrderRefund.RefundStoreCode.Value)
                            QodRefund.RefundDate = OrderRefund.RefundDate.Value
                            QodRefund.RefundTill = OrderRefund.RefundTill.Value
                            QodRefund.RefundTransaction = OrderRefund.RefundTransaction.Value
                            QodRefund.SkuNumber = ResponseRefundLine.ProductCode.Value
                            QodRefund.Price = ResponseRefundLine.RefundLineValue.Value
                            QodRefund.QtyCancelled = CInt(ResponseRefundLine.QuantityCancelled.Value)
                            QodRefund.QtyReturned = CInt(ResponseRefundLine.QuantityReturned.Value)
                            QodRefund.RefundStatus = CInt(IIf(CInt(ResponseRefundLine.RefundLineStatus.Value) = Qod.State.Refund.NotifyOk, Qod.State.Refund.StatusUpdateOk, ResponseRefundLine.RefundLineStatus.Value))

                            Dim qodLine As Qod.QodLine = qodHeader.Lines.Find(QodRefund.Number)
                            If CInt(qodLine.DeliverySource) = StoreId AndAlso IBTHeader IsNot Nothing Then
                                QodRefund.FulfillingStoreIbtIn = IBTHeader.Number
                                QodRefund.SellingStoreIbtOut = IBTHeader.IBTNumber
                            End If

                            'MO'C - Update Refunded lines (basically do what the till does on the selling store data for remote sites)
                            If qodHeader.SellingStoreId <> StoreId Then 'The till will already have done this at the selling store
                                qodLine.QtyRefunded -= (QodRefund.QtyCancelled + QodRefund.QtyReturned)
                                qodLine.QtyToDeliver -= QodRefund.QtyCancelled
                                'Have to check if QtyTaken is greater than zero as delivery charge lines are never taken!
                                If qodLine.QtyTaken > 0 Then
                                    qodLine.QtyTaken -= QodRefund.QtyReturned
                                End If
                                If Not qodLine.IsDeliveryChargeItem Then
                                    qodHeader.QtyRefunded -= (QodRefund.QtyCancelled + QodRefund.QtyReturned)
                                    qodHeader.QtyTaken -= QodRefund.QtyReturned
                                End If
                            End If
                            qodHeader.SetRefundStatus(QodRefund.RefundStatus)

                        Else

                            LocalWrite(Log, LoggingInformation.StandardLogic, "QOD Refund Order Found")
                            QodRefund.RefundStatus = CInt(IIf(CInt(ResponseRefundLine.RefundLineStatus.Value) = Qod.State.Refund.NotifyOk, Qod.State.Refund.StatusUpdateOk, ResponseRefundLine.RefundLineStatus.Value))

                        End If

                    Next
                    LocalWrite(Log, LoggingInformation.StandardLogic, "QOD Processing: End")

                Next
                LocalWrite(Log, LoggingInformation.StandardLogic, "Process Refunds: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Additional Processing: Start")
                If qodHeader.SellingStoreId <> StoreId Then
                    If qodHeader.QtyOrdered + qodHeader.QtyRefunded = 0 Then
                        qodHeader.Status = "1"
                    ElseIf qodHeader.QtyRefunded < 0 Then
                        qodHeader.Status = "2"
                    End If
                    qodHeader.RevisionNumber += 1
                End If
                ' Referrals 631, 711, 737, 738, 739 & 740
                ' Reworked this area for above referrals
                'Check to see if we need to suspend the order at this store
                Dim suspensionRequired As Boolean = False

                For Each QodRefund As Qod.QodRefund In qodHeader.Refunds
                    Dim QodLine As Qod.QodLine = qodHeader.Lines.Find(QodRefund.Number)  ' Get existing line for the refund

                    ' Flag it up if the line is being fulfilled by this store and a cancellation has been made by the refund against the line
                    If QodLine.DeliverySource = StoreId.ToString AndAlso QodRefund.QtyCancelled > 0 Then
                        ' If cancellation means that there is nothing left to deliver on that line, set its status to complete (999)
                        If QodLine.QtyToDeliver = 0 Then
                            QodLine.DeliveryStatus = Qod.State.Delivery.DeliveredStatusOk
                        End If
                        ' Referral 759 - move this condition to here (from outside the whole loop and subsequent suspendedRequired check) so
                        ' that above code (set line status to 999 when fully refunded) is run, no matter what the line's original delivery
                        ' status.  Flag up a Suspension only if original statuses warrant it, i.e.
                        'If the Order is ready for dispatch when the refund happened - somewhere between 500-599 and 800-899 is allowed - Referral 737
                        If (qodHeader.Lines.DeliveryStatus(StoreId) >= Qod.State.Delivery.PickingListCreated _
                        AndAlso qodHeader.Lines.DeliveryStatus(StoreId) <= Qod.State.Delivery.PickingStatusOk) _
                        OrElse (qodHeader.Lines.DeliveryStatus(StoreId) >= Qod.State.Delivery.UndeliveredCreated _
                        AndAlso qodHeader.Lines.DeliveryStatus(StoreId) <= Qod.State.Delivery.UndeliveredStatusOk) Then
                            ' Remember a line (in picking or failed delivery) has had a cancellation against it (part or full)
                            suspensionRequired = True
                        End If
                    End If
                Next
                ' If have cancelled part/all of a line, suspend the order and reset the delivery statuses for other lines being fulfilled by this store,
                ' but NOT if now all the lines for this store are set to DeliveredStatusOk
                If suspensionRequired AndAlso qodHeader.Lines.DeliveryStatus(StoreId) < Qod.State.Delivery.DeliveredStatusOk Then
                    ' If one line that this store is delivering has changed, then all lines that this store is delivering should change
                    ' Set all the delivery source lines for this store back to IbtInAllStock/ReceiptStatusOk (depending on whether delivery/collection) - Referral 740
                    qodHeader.Lines.DeliveryStatus(StoreId) = CType(IIf(qodHeader.IsForDelivery, Qod.State.Delivery.IbtInAllStock, Qod.State.Delivery.ReceiptStatusOk), Qod.State.Delivery)
                    ' Suspend the order at this store
                    qodHeader.IsSuspended = True
                End If
                ' Store status before adjustment
                Dim oldDeliveryStatus As Integer = qodHeader.DeliveryStatus
                ' Adjust the header status according to the changed in the lines status's
                qodHeader.SetDeliveryStatusAsLines(StoreId)

                ' End of Referrals 631, 711, 737, 738, 739 & 740
                LocalWrite(Log, LoggingInformation.StandardLogic, "Additional Processing: End")

                'Try writing the data away to the database
                Using con As New Connection
                    Dim LinesUpdated As Integer = 0
                    Try
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Start")

                        con.StartTransaction()

                        Dim ibtNumberMap As New Dictionary(Of Integer, String)
                        For Each ibt As Ibt.IBTHeader In IBTHeaderCollection
                            ibtNumber = CInt(ibt.Number)
                            LinesUpdated += ibt.Persist(con)

                            ibtNumberMap.Add(ibtNumber, ibt.Number)
                        Next

                        UpdateQodHeaderAndResponseIbtNumbers(ibtNumberMap)
                        LinesUpdated += qodHeader.PersistTree(con)
                        con.CommitTransaction()

                        For Each OrderRefund As CTSUpdateRefundResponseOrderRefund In response.OrderRefunds
                            OrderRefund.SuccessFlag = True
                        Next

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Success")
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: End")
                        LocalWrite(Log, LoggingInformation.StandardLogic, response.Serialise)

                    Catch ex As Exception
                        con.RollbackTransaction()

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Failure - " & ex.ToString)
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: End")

                    End Try
                End Using

                If response.IsSuccessful() Then
                    Try
                        ' Additional actions for 1) delivery AND 2)not despatched AND 3)fully refunded order
                        If qodHeader.IsForDelivery AndAlso
                            oldDeliveryStatus < Qod.State.Delivery.DespatchCreated AndAlso
                            qodHeader.DeliveryStatus = Qod.State.Delivery.DeliveredStatusOk Then

                            Dim repo = DataLayerFactory.Create(Of IExternalRequestRepository)()

                            If qodHeader.WasCreatedInStore() Then
                                LocalWrite(Log, LoggingInformation.StandardLogic, "Generate ExternalRequest: Start")
                                qodHeader.GenerateDeallocateUpdateCapacityRequest(repo)
                                LocalWrite(Log, LoggingInformation.StandardLogic, "Generate ExternalRequest: End")
                            End If

                            If Parameter.ShouldNotifyDeliveryService Then
                                LocalWrite(Log, LoggingInformation.StandardLogic, "Generate ExternalRequest: Start")
                                qodHeader.GenerateCancelConsignmentRequest(repo)
                                LocalWrite(Log, LoggingInformation.StandardLogic, "Generate ExternalRequest: End")
                            End If
                        End If
                    Catch ex As Exception
                        LocalWrite(Log, LoggingInformation.StandardLogic, String.Format("Generate ExternalRequest: Exception - {0}", ex))
                    End Try
                End If

            Catch ex As DeserialiseException
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: Deserialize Exception - " & ex.ToString)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsUpdateRefund: End")
                Log.Dispose()
                Return String.Empty

            Catch ex As InvalidFieldSettingException
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: Invalid Field Setting Exception - " & ex.ToString)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsUpdateRefund: End")
                Log.Dispose()
                Return String.Empty

            Catch ex As Exception
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: General Exception - " & ex.ToString)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsUpdateRefund: End")
                Log.Dispose()
                Return String.Empty

            End Try

            LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsUpdateRefund: End")
            Log.Dispose()
            Return response.Serialise

        End Using

    End Function

    Private Sub UpdateQodHeaderAndResponseIbtNumbers(ByVal ibtNumberMap As Dictionary(Of Integer, String))

        ' Update response
        For Each OrderRefund As CTSUpdateRefundResponseOrderRefund In response.OrderRefunds
            For Each FulfilmentSite As CTSUpdateRefundResponseOrderRefundFulfilmentSite In OrderRefund.FulfilmentSites
                Dim number As Integer
                If Integer.TryParse(FulfilmentSite.FulfilmentSiteIBTInNumber, number) AndAlso number < 0 Then
                    FulfilmentSite.FulfilmentSiteIBTInNumber = ibtNumberMap(number)
                End If
            Next
        Next

        ' Update QOD refunds
        For Each refund As Qod.QodRefund In qodHeader.Refunds
            If refund.ExistsInDB Then Continue For

            Dim number As Integer
            If Integer.TryParse(refund.FulfillingStoreIbtIn, number) AndAlso number < 0 Then
                refund.FulfillingStoreIbtIn = ibtNumberMap(number)
            End If
        Next

    End Sub

End Class
