﻿Imports Cts.Oasys.Core.SystemEnvironment
Imports Ninject
Imports WSS.BO.Common.Hosting

Namespace Configuration

    Public Class DependencyInjectionConfig
        Inherits BaseDependencyInjectionConfig

        Public Overrides Function GetKernel() As IKernel
            Dim kernel = MyBase.GetKernel()
            kernel.Bind(Of ISystemEnvironment)().To(Of LiveSystemEnvironment)().InSingletonScope()
            Return kernel
        End Function
    End Class
End Namespace