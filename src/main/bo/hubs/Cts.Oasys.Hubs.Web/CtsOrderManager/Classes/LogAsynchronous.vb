﻿Public Class LogAsynchronous
    Implements ILog, IDisposable

    Shared WithEvents oBackground As New System.ComponentModel.BackgroundWorker
    'Shared WithEvents obGWrite As New System.ComponentModel.BackgroundWorker

    Private _EnableTrace As Boolean
    Private _Header As String = String.Empty
    Private _Log As System.IO.StreamWriter = Nothing

    Shared Q As New System.Collections.Queue

    Shared _Output As New Log

    Private Enum Caller
        Create
        CreateWithPath
        Dispose
        SetLogPath
        SetLogPathString
        Write
        WriteMessage
        WriteCannotCreateRequest
        WriteCannotCreateResponse
        WriteCreatingIbtOut
        WriteInput
        WriteProblemSaving
        WriteQodAlreadyExists
        WriteQodNotFound
        WriteReturnEmptyString
        WriteServiceCalled
        WriteServiceCompleted
    End Enum

#Region "Contructor"

    Public Sub New()

    End Sub

#End Region

#Region "Properties"

    Public Property Header() As String Implements ILog.Header
        Get
            Return _Header
        End Get
        Set(ByVal value As String)
            _Header = value
        End Set
    End Property

    Public Property Log() As System.IO.StreamWriter Implements ILog.Log
        Get
            Return _Log
        End Get
        Set(ByVal value As System.IO.StreamWriter)
            _Log = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub SetLogPath() Implements ILog.SetLogPath
        Try
            Dim arg As Object() = {Caller.SetLogPath}
            Q.Enqueue(arg)
            'Dim arg(Caller.CreateWithTrace.ToString, header) As Object
            oBackground.RunWorkerAsync()

        Catch ex As Exception

        End Try
        'oBackground.RunWorkerAsync(Caller.SetLogPath)
    End Sub

    Public Sub Write(ByVal message As String) Implements ILog.Write
        Try
            Dim arg As Object() = {Caller.WriteMessage, message}
            Q.Enqueue(arg)
            'Dim arg(Caller.CreateWithTrace.ToString, header) As Object
            oBackground.RunWorkerAsync()

        Catch ex As Exception

        End Try
        'oBackground.RunWorkerAsync(Caller.WriteMessage)
    End Sub

#End Region

#Region "Existing functionality; following not required to be implemented"

    Public Sub Write() Implements ILog.Write

    End Sub

    Public Sub WriteCannotCreateRequest() Implements ILog.WriteCannotCreateRequest

    End Sub

    Public Sub WriteCannotCreateResponse() Implements ILog.WriteCannotCreateResponse

    End Sub

    Public Sub WriteCreatingIbtIn() Implements ILog.WriteCreatingIbtIn

    End Sub

    Public Sub WriteCreatingIbtOut() Implements ILog.WriteCreatingIbtOut

    End Sub

    Public Sub WriteCreatingQod() Implements ILog.WriteCreatingQod

    End Sub

    Public Sub WriteGettingQod() Implements ILog.WriteGettingQod

    End Sub

    Public Sub WriteInput(ByVal Input As String) Implements ILog.WriteInput

    End Sub

    Public Sub WriteProblemSaving() Implements ILog.WriteProblemSaving

    End Sub

    Public Sub WriteQodAlreadyExists() Implements ILog.WriteQodAlreadyExists

    End Sub

    Public Sub WriteQodLineNotFound(ByVal LineNumber As String) Implements ILog.WriteQodLineNotFound

    End Sub

    Public Sub WriteQodNotFound() Implements ILog.WriteQodNotFound

    End Sub

    Public Sub WriteReturnEmptyString() Implements ILog.WriteReturnEmptyString

    End Sub

    Public Sub WriteSaveSuccessful() Implements ILog.WriteSaveSuccessful

    End Sub

    Public Sub WriteServiceCalled() Implements ILog.WriteServiceCalled

    End Sub

    Public Sub WriteServiceCompleted() Implements ILog.WriteServiceCompleted

    End Sub

#End Region

#Region "IDisposable Support"

    Private disposedValue As Boolean = False        ' To detect redundant calls

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                'If Not _output Is Nothing Then
                '    _output.Dispose()
                'End If

                'If Not _log Is Nothing Then
                '    With _log
                '        .Flush()
                '        .Close()
                '        .Dispose()
                '    End With
                'End If

                ' TODO: free other state (managed objects).
            End If

            ' TODO: free your own state (unmanaged objects).
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
        GC.SuppressFinalize(Me)
    End Sub

    Public Sub Dispose() Implements System.IDisposable.Dispose
        Try
            Dim arg As Object() = {Caller.Dispose}
            Q.Enqueue(arg)
            'Dim arg(Caller.CreateWithTrace.ToString, header) As Object
            oBackground.RunWorkerAsync()

            Dispose(True)
            GC.SuppressFinalize(Me)

        Catch ex As Exception

        End Try
    End Sub

#Region "Asynchronous Operations"

    Shared Sub BGWrite_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles oBackground.DoWork
        'Friend Sub BackgroundWorker_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles oBackground.DoWork

        'Dim args As Object() = DirectCast(e.Argument, Object())
        'Dim operation As Caller = CType(args(0), Caller)
        Dim args As Object
        'Dim message As String = String.Empty
        'Dim path As String = String.Empty
        Dim operation As Caller

        'operation = CType(args(0), Caller)

        While Q.Count > 0
            'get the next ietm from the queue
            args = Q.Dequeue()

            operation = CType(args, Caller)

            'If args.Length > 1 Then
            '    message = args(1).ToString
            'Else : message = String.Empty
            'End If

            'If args.Length > 2 Then
            '    path = args(2).ToString
            'Else : path = String.Empty
            'End If

            Try
                Select Case operation.ToString
                    'Case Caller.Create.ToString : _Output.Create(message)
                    'Case Caller.CreateWithPath.ToString : _Output.Create(message, path)
                    'Case Caller.Write.ToString : _output.Write()
                    'Case Caller.WriteMessage.ToString : _Output.Write(message)
                    'Case Caller.WriteCannotCreateRequest.ToString : _output.WriteCannotCreateRequest()
                    'Case Caller.WriteCannotCreateResponse.ToString : _output.WriteCannotCreateResponse()
                    'Case Caller.WriteCreatingIbtOut.ToString : _output.WriteCreatingIbtOut()
                    'Case Caller.WriteInput.ToString : _output.WriteInput(message)
                    'Case Caller.WriteProblemSaving.ToString : _output.WriteProblemSaving()
                    'Case Caller.WriteQodAlreadyExists.ToString : _output.WriteQodAlreadyExists()
                    'Case Caller.WriteQodNotFound.ToString : _output.WriteQodNotFound()
                    'Case Caller.WriteReturnEmptyString.ToString : _output.WriteReturnEmptyString()
                    'Case Caller.WriteServiceCalled.ToString : _output.WriteServiceCalled()
                    'Case Caller.WriteServiceCompleted.ToString : _output.WriteServiceCompleted()
                    Case Caller.Dispose.ToString : DisposeLog()
                End Select

            Catch ex As Exception

            End Try
        End While

    End Sub

    Shared Sub DisposeLog()

        If Not _Output Is Nothing Then
            _Output.Dispose()
        End If

    End Sub

#End Region

    '#Region " IDisposable Support "
    '    ' This code added by Visual Basic to correctly implement the disposable pattern.
    '    Public Sub Dispose() Implements IDisposable.Dispose
    '        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '        Try
    '            Dim arg As Object() = {Caller.Dispose}
    '            q.Enqueue(arg)
    '            'Dim arg(Caller.CreateWithTrace.ToString, header) As Object
    '            oBackground.RunWorkerAsync()

    '        Catch ex As Exception

    '        End Try

    '        'Dispose(True)
    '        'GC.SuppressFinalize(Me)
    '    End Sub
    '#End Region

#End Region

End Class