﻿Public Class XMLToQodTextsConvertorOldWay

    Implements IXMLToQodTextsConvertor

    Public Function ConvertToQodTexts(ByVal deliveryLines As IEnumerable(Of IDeliveryLine), ByVal QodHeader As Core.Order.Qod.QodHeader) As Core.Order.Qod.QodTextCollection Implements IXMLToQodTextsConvertor.ConvertToQodTexts

        If deliveryLines IsNot Nothing Then
            For Each deliveryLine As IDeliveryLine In deliveryLines
                Dim txt As Qod.QodText = QodHeader.Texts.FindOrCreate(CStr(deliveryLine.LineNumber), Qod.QodTextType.Type.DeliveryInstruction)
                txt.SellingStoreId = QodHeader.SellingStoreId
                txt.SellingStoreOrderId = QodHeader.SellingStoreOrderId
                If deliveryLine.LineText IsNot Nothing Then
                    txt.Text = deliveryLine.LineText
                End If
            Next
        End If
        Return QodHeader.Texts
    End Function
End Class
