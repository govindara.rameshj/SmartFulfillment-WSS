﻿Friend Class Output
    Implements ILog, IDisposable

    Private _header As String = String.Empty
    Private _log As StreamWriter = Nothing

    Public Sub New(ByVal header As String)
        _header = header
        SetLogPath()
    End Sub

    Private Sub SetLogPath() Implements ILog.SetLogPath

        Dim sb As New StringBuilder(AppDomain.CurrentDomain.BaseDirectory.ToString)
        sb.Append("\\logs\\")

        Directory.CreateDirectory(sb.ToString())

        sb.Append(_header & ".")
        'log name - year, month, day, hour, minute, second, millisecond (padded left to 3 digits)
        sb.Append(Now.ToString("yyyyMMdd HHmmss") & Now.ToString("FFF").PadLeft(3, "0"c) & ".txt")


        _log = New StreamWriter(sb.ToString, True)
    End Sub

    Public Sub WriteServiceCalled() Implements ILog.WriteServiceCalled
        Write("Web Service called: " & Now.ToLongTimeString)
    End Sub

    Public Sub WriteInput(ByVal input As String) Implements ILog.WriteInput
        Write("Input " & input)
    End Sub

    Public Sub WriteReturnEmptyString() Implements ILog.WriteReturnEmptyString
        Write("Returning empty string")
    End Sub

    Public Sub WriteCannotCreateRequest() Implements ILog.WriteCannotCreateRequest
        Write("Cannot create request object")
    End Sub

    Public Sub WriteCannotCreateResponse() Implements ILog.WriteCannotCreateResponse
        Write("Cannot create response object")
    End Sub

    Public Sub WriteGettingQod() Implements ILog.WriteGettingQod
        Write("Retrieving qod order")
    End Sub

    Public Sub WriteQodNotFound() Implements ILog.WriteQodNotFound
        Write("Qod order not found")
    End Sub

    Public Sub WriteQodAlreadyExists() Implements ILog.WriteQodAlreadyExists
        Write("Qod already exists")
    End Sub

    Public Sub WriteQodLineNotFound(ByVal lineNumber As String) Implements ILog.WriteQodLineNotFound
        Write("Qod line not found for line " & lineNumber)
    End Sub

    Public Sub WriteCreatingQod() Implements ILog.WriteCreatingQod
        Write("Creating qod order")
    End Sub

    Public Sub WriteCreatingIbtOut() Implements ILog.WriteCreatingIbtOut
        Write("Creating ibt out")
    End Sub

    Public Sub WriteCreatingIbtIn() Implements ILog.WriteCreatingIbtIn
        Write("Creating ibt in")
    End Sub

    Public Sub WriteSaveSuccessful() Implements ILog.WriteSaveSuccessful
        Write("Saved successfully")
    End Sub

    Public Sub WriteProblemSaving() Implements ILog.WriteProblemSaving
        Write("Problem saving to database")
    End Sub

    Public Sub Write() Implements ILog.Write
        If _log IsNot Nothing Then _log.WriteLine(Environment.NewLine)
    End Sub

    Public Sub Write(ByVal message As String) Implements ILog.Write
        Dim sb As New StringBuilder()
        If _header IsNot Nothing Then sb.Append(_header & ": ")
        If message IsNot Nothing Then sb.Append(message.Trim)
        If _log IsNot Nothing Then _log.WriteLine(sb.ToString)
    End Sub

#Region "New functionality; following not required to be implemented"

    Public Property Header() As String Implements ILog.Header
        Get
            Return _header
        End Get
        Set(ByVal value As String)
            _header = value
        End Set
    End Property

    Public Property Log() As System.IO.StreamWriter Implements ILog.Log
        Get
            Return _log
        End Get
        Set(ByVal value As System.IO.StreamWriter)
            _log = value
        End Set
    End Property

    Public Sub WriteServiceCompleted() Implements ILog.WriteServiceCompleted

    End Sub

#End Region

#Region " IDisposable Support "

    Private disposedValue As Boolean = False        ' To detect redundant calls

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                If _log IsNot Nothing Then
                    _log.Flush()
                    _log.Close()
                    _log.Dispose()
                End If
            End If
        End If
        Me.disposedValue = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

End Class