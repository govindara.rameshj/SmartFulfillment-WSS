﻿Public Class Log
    Implements ILog, IDisposable

    Private _EnableTrace As Boolean = False
    Private _Header As String = String.Empty
    Private _Log As System.IO.StreamWriter = Nothing

#Region "Contructor"

    Public Sub New()

    End Sub

    Public Sub New(ByVal Header As String)
        _Header = Header

        SetLogPath()



        'extended login not implemented; time constraints
        'for now write to file system & debug window; longer term will be parameter driven
        _EnableTrace = True 'temp 


    End Sub

#End Region

#Region "Properties"

    Public Property Header() As String Implements ILog.Header
        Get
            Return _Header
        End Get
        Set(ByVal value As String)
            _Header = value
        End Set
    End Property

    Public Property Log() As System.IO.StreamWriter Implements ILog.Log
        Get
            Return _Log
        End Get
        Set(ByVal value As System.IO.StreamWriter)
            _Log = value
        End Set
    End Property

#End Region

#Region "Methods"

    Private Sub SetLogPath() Implements ILog.SetLogPath
        Dim directoryPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString, "logs\\")

        If Not Directory.Exists(directoryPath) Then
            Directory.CreateDirectory(directoryPath)
        End If

        Dim sb As New StringBuilder(directoryPath)

        sb.Append(_Header & ".")
        'log name - year, month, day, hour, minute, second, millisecond (padded left to 3 digits)
        sb.Append(Now.ToString("yyyyMMdd HHmmss") & Now.ToString("FFF").PadLeft(3, CType("0", Char)) & ".txt")

        _Log = New System.IO.StreamWriter(sb.ToString, True)
        _Log.AutoFlush = True
    End Sub

    Private Sub SetLoggingSource()

    End Sub

    Public Sub Write(ByVal Message As String) Implements ILog.Write

        Dim sb As New StringBuilder()

        If _Header IsNot Nothing Then sb.Append(_Header & ": ")
        If Message IsNot Nothing Then sb.Append(Message.Trim)

        If _Log IsNot Nothing Then _Log.WriteLine(Now.ToString("yyyyMMdd HH:mm:ss.") & Now.ToString("FFF").PadLeft(3, CType("0", Char)) & ": " & sb.ToString)
        If _EnableTrace = True Then Trace.WriteLine(Now.ToString("yyyyMMdd HH:mm:ss.") & Now.ToString("FFF").PadLeft(3, CType("0", Char)) & ": " & sb.ToString)

    End Sub

#End Region

#Region "Existing functionality; following not required to be implemented"

    Public Sub Write() Implements ILog.Write

    End Sub

    Public Sub WriteCannotCreateRequest() Implements ILog.WriteCannotCreateRequest

    End Sub

    Public Sub WriteCannotCreateResponse() Implements ILog.WriteCannotCreateResponse

    End Sub

    Public Sub WriteCreatingIbtIn() Implements ILog.WriteCreatingIbtIn

    End Sub

    Public Sub WriteCreatingIbtOut() Implements ILog.WriteCreatingIbtOut

    End Sub

    Public Sub WriteCreatingQod() Implements ILog.WriteCreatingQod

    End Sub

    Public Sub WriteGettingQod() Implements ILog.WriteGettingQod

    End Sub

    Public Sub WriteInput(ByVal Input As String) Implements ILog.WriteInput

    End Sub

    Public Sub WriteProblemSaving() Implements ILog.WriteProblemSaving

    End Sub

    Public Sub WriteQodAlreadyExists() Implements ILog.WriteQodAlreadyExists

    End Sub

    Public Sub WriteQodLineNotFound(ByVal LineNumber As String) Implements ILog.WriteQodLineNotFound

    End Sub

    Public Sub WriteQodNotFound() Implements ILog.WriteQodNotFound

    End Sub

    Public Sub WriteReturnEmptyString() Implements ILog.WriteReturnEmptyString

    End Sub

    Public Sub WriteSaveSuccessful() Implements ILog.WriteSaveSuccessful

    End Sub

    Public Sub WriteServiceCalled() Implements ILog.WriteServiceCalled

    End Sub

    Public Sub WriteServiceCompleted() Implements ILog.WriteServiceCompleted

    End Sub

#End Region

#Region "IDisposable Support"

    Private disposedValue As Boolean = False        ' To detect redundant calls

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                If _Log IsNot Nothing Then
                    _Log.Flush()
                    _Log.Close()
                    _Log.Dispose()
                End If
            End If
        End If
        Me.disposedValue = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

End Class