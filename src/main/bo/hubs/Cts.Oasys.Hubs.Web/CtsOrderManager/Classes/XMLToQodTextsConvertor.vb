﻿Public Class XMLToQodTextsConvertor

    Implements IXMLToQodTextsConvertor

    Friend Function AddFullStop(ByVal Line As String, ByVal IsPickInstruction As Boolean) As String
        If Not Line.EndsWith(".") And IsPickInstruction Then
            Line += "."
        End If
        Return Line
    End Function

    Public Function ConvertToQodTexts(ByVal deliveryLines As IEnumerable(Of IDeliveryLine), ByVal QodHeader As Core.Order.Qod.QodHeader) As Core.Order.Qod.QodTextCollection Implements IXMLToQodTextsConvertor.ConvertToQodTexts
        Dim IsPickInstruction As Boolean = False
        If deliveryLines IsNot Nothing Then
            For Each outputText As IDeliveryLine In deliveryLines
                Dim txt As Qod.QodText = QodHeader.Texts.FindOrCreate(CStr(outputText.LineNumber), CType(IIf(IsPickInstruction, Qod.QodTextType.Type.PickInstruction, Qod.QodTextType.Type.DeliveryInstruction), Qod.QodTextType.Type))
                txt.SellingStoreId = QodHeader.SellingStoreId
                txt.SellingStoreOrderId = QodHeader.SellingStoreOrderId
                If outputText.LineText IsNot Nothing Then
                    If outputText.LineText.Contains(Core.Order.Qod.QodTextType.Description.PickInstructionToken) Then
                        IsPickInstruction = True
                        txt.Text = ""
                    Else
                        txt.Text = AddFullStop(outputText.LineText, IsPickInstruction)
                    End If

                End If
            Next
        End If
        Return QodHeader.Texts
    End Function
End Class
