﻿Public Class Command
    Implements IDisposable
    Private _dataProvider As DataProvider
    Private _command As SqlCommand
    Private _commandOdbc As OdbcCommand

    Public Sub New(ByRef con As Connection)
        _dataProvider = con.DataProvider

        Select Case con.DataProvider
            Case DataProvider.Odbc
                _commandOdbc = New OdbcCommand
                _commandOdbc.Connection = con.OdbcConnection
                _commandOdbc.CommandType = CommandType.Text
                _commandOdbc.CommandTimeout = 0
                If con.OdbcTransaction IsNot Nothing Then
                    _commandOdbc.Transaction = con.OdbcTransaction
                End If

            Case DataProvider.Sql
                _command = New SqlCommand
                _command.Connection = con.SqlConnection
                _command.CommandType = CommandType.Text
                _command.CommandTimeout = 0
                If con.SqlTransaction IsNot Nothing Then
                    _command.Transaction = con.SqlTransaction
                End If
        End Select

    End Sub

    Public Sub New(ByRef con As Connection, ByVal storedProcedure As String)
        _dataProvider = con.DataProvider

        Select Case con.DataProvider
            Case DataProvider.Odbc
                _commandOdbc = New OdbcCommand
                _commandOdbc.Connection = con.OdbcConnection
                _commandOdbc.CommandType = CommandType.StoredProcedure
                _commandOdbc.CommandTimeout = 0
                _commandOdbc.CommandText = storedProcedure

                If con.OdbcTransaction IsNot Nothing Then
                    _commandOdbc.Transaction = con.OdbcTransaction
                End If

            Case DataProvider.Sql
                _command = New SqlCommand
                _command.Connection = con.SqlConnection
                _command.CommandType = CommandType.StoredProcedure
                _command.CommandTimeout = 0
                _command.CommandText = storedProcedure

                If con.SqlTransaction IsNot Nothing Then
                    _command.Transaction = con.SqlTransaction
                End If
        End Select

    End Sub

    Public Property StoredProcedureName() As String
        Get
            Select Case _dataProvider
                Case DataProvider.Odbc : Return _commandOdbc.CommandText
                Case DataProvider.Sql : Return _command.CommandText
                Case Else : Return String.Empty
            End Select
        End Get
        Set(ByVal value As String)
            Select Case _dataProvider
                Case DataProvider.Odbc
                    _commandOdbc.CommandText = value
                    _commandOdbc.CommandType = CommandType.StoredProcedure
                Case DataProvider.Sql
                    _command.CommandText = value
                    _command.CommandType = CommandType.StoredProcedure
            End Select
        End Set
    End Property

    Public Property CommandText() As String
        Get
            Select Case _dataProvider
                Case DataProvider.Odbc : Return _commandOdbc.CommandText
                Case DataProvider.Sql : Return _command.CommandText
                Case Else : Return String.Empty
            End Select
        End Get
        Set(ByVal value As String)
            Select Case _dataProvider
                Case DataProvider.Odbc
                    _commandOdbc.CommandText = value
                    _commandOdbc.CommandType = CommandType.Text
                Case DataProvider.Sql
                    _command.CommandText = value
                    _command.CommandType = CommandType.Text
            End Select
        End Set
    End Property


    Public Sub ClearParamters()
        If _commandOdbc IsNot Nothing Then _commandOdbc.Parameters.Clear()
        If _command IsNot Nothing Then _command.Parameters.Clear()
    End Sub

    Public Sub AddParameter(ByVal name As String, ByVal value As Object)
        Select Case _dataProvider
            Case DataProvider.Odbc : _commandOdbc.Parameters.Add(New OdbcParameter(name, value))
            Case DataProvider.Sql : _command.Parameters.Add(New SqlParameter(name, value))
        End Select
    End Sub

    Public Sub AddParameter(ByVal name As String, ByVal value As Object, ByVal type As SqlDbType)
        _command.Parameters.Add(New SqlParameter(name, type)).Value = value
    End Sub

    Public Sub AddParameter(ByVal name As String, ByVal value As Object, ByVal type As SqlDbType, ByVal size As Integer, ByVal direction As ParameterDirection)

        Dim param As New SqlParameter(name, type, size)
        param.Direction = direction
        param.Value = value

        _command.Parameters.Add(param)

    End Sub

    Public Sub AddParameter(ByVal name As String, ByVal value As Object, ByVal type As SqlDbType, ByVal size As Integer)
        _command.Parameters.Add(New SqlParameter(name, type, size)).Value = value
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Partha Dutta
    ' Date        : 07/10/2010
    ' Referral No : 388
    ' Notes       : Function allowing the use of an output parameter
    '               Certain data types do not require "size", an overloaded function is provided where this does not have to be set
    '               The above ref has drove this requirement but can be used generally by the system
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Public Function AddOutputParameter(ByVal name As String, ByVal type As SqlDbType, ByVal size As Integer) As SqlParameter

        Dim param As New SqlParameter(name, type, size)

        param.Direction = ParameterDirection.Output
        _command.Parameters.Add(param)

        Return param
    End Function

    Public Function AddOutputParameter(ByVal name As String, ByVal type As SqlDbType) As SqlParameter

        Dim param As New SqlParameter(name, type)

        param.Direction = ParameterDirection.Output
        _command.Parameters.Add(param)

        Return param
    End Function




    Public Sub AddParameterOutput(ByVal name As String, ByVal type As System.Type, ByVal size As Integer)

        Select Case _dataProvider
            Case DataProvider.Odbc
                Dim dbtype As OdbcType = OdbcType.Char
                Select Case type.ToString
                    Case GetType(String).ToString
                    Case GetType(Boolean).ToString : dbtype = OdbcType.Bit
                    Case GetType(Date).ToString : dbtype = OdbcType.Date
                    Case GetType(Integer).ToString : dbtype = OdbcType.Int
                    Case GetType(Decimal).ToString : dbtype = OdbcType.Decimal
                End Select

                _commandOdbc.Parameters.Add(New OdbcParameter(name, dbtype, size)).Direction = ParameterDirection.Output

            Case DataProvider.Sql
                Dim dbtype As SqlDbType = SqlDbType.Char
                Select Case type.ToString
                    Case GetType(String).ToString
                    Case GetType(Boolean).ToString : dbtype = SqlDbType.Bit
                    Case GetType(Date).ToString : dbtype = SqlDbType.Date
                    Case GetType(Integer).ToString : dbtype = SqlDbType.Int
                    Case GetType(Decimal).ToString : dbtype = SqlDbType.Decimal
                End Select

                _command.Parameters.Add(New SqlParameter(name, dbtype, size)).Direction = ParameterDirection.Output
        End Select

    End Sub

    Public Function GetParameterValue(ByVal name As String) As Object
        Select Case _dataProvider
            Case DataProvider.Odbc : Return _commandOdbc.Parameters(name).Value
            Case DataProvider.Sql : Return _command.Parameters(name).Value
            Case Else : Return Nothing
        End Select
    End Function



    Public Function ExecuteValue() As Object

        WriteTrace()
        Select Case _dataProvider
            Case DataProvider.Odbc : Return _commandOdbc.ExecuteScalar
            Case DataProvider.Sql : Return _command.ExecuteScalar
            Case Else : Return Nothing
        End Select

    End Function

    Public Function ExecuteDataTable() As DataTable

        WriteTrace()
        Dim dt As New DataTable

        Select Case _dataProvider
            Case DataProvider.Odbc
                If _commandOdbc.Connection.State <> ConnectionState.Open Then _commandOdbc.Connection.Open()
                Dim reader As OdbcDataReader = _commandOdbc.ExecuteReader()
                dt.Load(reader)
                reader.Close()
            Case DataProvider.Sql
                Dim reader As SqlDataReader = _command.ExecuteReader
                dt.Load(reader)
                reader.Close()
        End Select
        Return dt

    End Function

    Public Function ExecuteDataSet() As DataSet

        WriteTrace()
        Select Case _dataProvider
            Case DataProvider.Sql
                Dim ds As New DataSet

                Using da As New SqlDataAdapter(_command)
                    da.Fill(ds)
                End Using

                Return ds
            Case Else
                Return Nothing
        End Select

    End Function

    Public Function ExecuteNonQuery() As Integer

        WriteTrace()
        Dim linesAffected As Integer = 0

        Select Case _dataProvider
            Case DataProvider.Odbc : linesAffected = _commandOdbc.ExecuteNonQuery()
            Case DataProvider.Sql
                Select Case _command.CommandType
                    Case CommandType.StoredProcedure
                        Dim retValue As New SqlParameter("RetValue", SqlDbType.Int)
                        retValue.Direction = ParameterDirection.ReturnValue
                        _command.Parameters.Add(retValue)
                        _command.ExecuteNonQuery()
                        linesAffected = CInt(retValue.Value)
                    Case Else
                        linesAffected = _command.ExecuteNonQuery
                End Select
            Case Else : linesAffected = -1
        End Select

        Return linesAffected

    End Function

    Private Sub WriteTrace()

        'output trace
        Dim sb As New StringBuilder()
        Select Case _dataProvider
            Case DataProvider.Odbc
                sb.Append(_commandOdbc.CommandText & Space(1))
                For Each param As OdbcParameter In _commandOdbc.Parameters
                    If param.Direction <> ParameterDirection.Input Then Continue For
                    If param.Value Is Nothing Then Continue For
                    sb.Append(param.ParameterName & "=" & param.Value.ToString & ",")
                Next

            Case DataProvider.Sql
                sb.Append(_command.CommandText & Space(1))
                For Each param As SqlParameter In _command.Parameters
                    If param.Direction <> ParameterDirection.Input Then Continue For
                    If param.Value Is Nothing Then Continue For
                    sb.Append(param.ParameterName & "=" & param.Value.ToString & ",")
                Next
        End Select

        sb.Remove(sb.Length - 1, 1)
        Trace.WriteLine(sb.ToString, Me.GetType.ToString)

    End Sub

#Region "Unit Test"

    Friend Function UnitTestExposeSqlCommandObject() As SqlCommand

        Return _command

    End Function

#End Region

#Region " IDisposable Support "
    Private disposedValue As Boolean = False        ' To detect redundant calls
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                If _command IsNot Nothing Then _command.Dispose()
                If _commandOdbc IsNot Nothing Then _commandOdbc.Dispose()
            End If
        End If
        Me.disposedValue = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class