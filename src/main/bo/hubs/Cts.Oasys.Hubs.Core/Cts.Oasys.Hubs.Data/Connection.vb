﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.Odbc

<Assembly: Runtime.CompilerServices.InternalsVisibleTo("Cts.Oasys.Hubs.Core.Stock_StoredProcedures_Tests, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                                                                              "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                                                                              "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                                                                              "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                                                                              "54e0a4a4")> 
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("Cts.Oasys.Hubs.Core.Order.UnitTest, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                                                                "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                                                                "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                                                                "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                                                                "54e0a4a4")> 
Public Class Connection
    Implements IDisposable
    Private _dataprovider As DataProvider = DataProvider.None
    Private _sqlConnection As SqlConnection = Nothing
    Private _sqlTransaction As SqlTransaction = Nothing
    Private _odbcConnection As OdbcConnection = Nothing
    Private _odbcTransaction As OdbcTransaction = Nothing

    Public Sub New()
        _dataprovider = GetDataProvider()
        Select Case _dataprovider
            Case DataProvider.Odbc
                _odbcConnection = New OdbcConnection(GetConnectionString())
                _odbcConnection.Open()
            Case DataProvider.Sql
                _sqlConnection = New SqlConnection(GetConnectionString())
                _sqlConnection.Open()
        End Select
    End Sub


    Public Function NewCommand() As Command
        Dim com As New Command(Me)
        Return com
    End Function

    Public Function NewCommand(ByVal storedProcedure As String) As Command
        Dim com As New Command(Me, storedProcedure)
        Return com
    End Function

    Public ReadOnly Property DataProvider() As DataProvider
        Get
            Return _dataprovider
        End Get
    End Property


    Friend ReadOnly Property SqlConnection() As SqlConnection
        Get
            Return _sqlConnection
        End Get
    End Property

    Friend ReadOnly Property SqlTransaction() As SqlTransaction
        Get
            Return _sqlTransaction
        End Get
    End Property

    Friend ReadOnly Property OdbcConnection() As OdbcConnection
        Get
            Return _odbcConnection
        End Get
    End Property

    Friend ReadOnly Property OdbcTransaction() As OdbcTransaction
        Get
            Return _odbcTransaction
        End Get
    End Property


    Public Sub StartTransaction()
        Select Case _dataprovider
            Case DataProvider.Odbc : _odbcTransaction = _odbcConnection.BeginTransaction
            Case DataProvider.Sql : _sqlTransaction = _sqlConnection.BeginTransaction
        End Select
    End Sub

    Public Sub CommitTransaction()
        Select Case _dataprovider
            Case DataProvider.Odbc
                If _odbcTransaction IsNot Nothing Then _odbcTransaction.Commit()
                _odbcTransaction.Dispose()
            Case DataProvider.Sql
                If _sqlTransaction IsNot Nothing Then _sqlTransaction.Commit()
                _sqlTransaction.Dispose()
        End Select
    End Sub

    Public Sub RollbackTransaction()
        Select Case _dataprovider
            Case DataProvider.Odbc
                If _odbcTransaction IsNot Nothing Then _odbcTransaction.Rollback()
                _odbcTransaction.Dispose()
            Case DataProvider.Sql
                If _sqlTransaction IsNot Nothing Then _sqlTransaction.Rollback()
                _sqlTransaction.Dispose()
        End Select
    End Sub

#Region " IDisposable Support "
    Private disposedValue As Boolean = False        ' To detect redundant calls

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                If _sqlTransaction IsNot Nothing Then
                    _sqlTransaction.Dispose()
                End If

                If _sqlConnection IsNot Nothing Then
                    If _sqlConnection.State <> ConnectionState.Closed Then _sqlConnection.Close()
                    _sqlConnection.Dispose()
                End If

                If _odbcTransaction IsNot Nothing Then
                    _odbcTransaction.Dispose()
                End If

                If _odbcConnection IsNot Nothing Then
                    If _odbcConnection.State <> ConnectionState.Closed Then _odbcConnection.Close()
                    _odbcConnection.Dispose()
                End If
            End If
        End If
        Me.disposedValue = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class