﻿<TestClass()> _
    Public MustInherit Class StoredProcedure_Test

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    <ClassInitialize()> _
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)

        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Sql
                    Case Else
                        Assert.Fail("Cannot perform Stored Procedures tests as not connected to a SQL Server database")
                End Select
            End Using
        End Using
    End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    MustOverride ReadOnly Property StoredProcedureName() As String
    MustOverride Function GetExistingSQL() As String
    MustOverride Function GetNonExistingSQL() As String
    MustOverride Function GetExistingDataAndKeyWithDynamicSQL(ByRef ExistingData As DataTable, ByRef Keys As Collection) As Boolean
    MustOverride Function GetNonExistingKeyWithDynamicSQL(ByRef Keys As Collection) As Boolean
    MustOverride Function GetKeyFieldsNoValueRequired(ByRef KeyFields As Collection) As Boolean

    Protected Const TC_KEY_FOUNDSTOREDPROCEDURE As String = "FoundStoredProcedure"
    Protected Const TC_KEY_INITIALISED As String = "Initialised"
    Protected Const TC_KEY_EXISTING As String = "Existing"
    Protected Const TC_KEY_NONEXISTINGKEY As String = "NonExistingKey"
    Protected Const TC_KEY_EXISTINGKEY As String = "ExistingKey"
    Protected Const TC_KEY_STOREDPROCEDUREEXISTING As String = "StoredProcedureExisting"
    Protected Const TC_KEY_STOREDPROCEDURENONEXISTING As String = "StoredProcedureNonExisting"

    <TestMethod()> _
    Public Overridable Sub StoredProcedureExists()

        If InitialiseTests() Then
            Assert.IsTrue(CBool(SafeGetTestContextProperty(TC_KEY_FOUNDSTOREDPROCEDURE, GetType(Boolean))), "Stored procedure '" & StoredProcedureName & "' does not exist.")
        End If
    End Sub

    <TestMethod()> _
    Public Overridable Sub WhenDataExists_StoredProcedureGetsDataTable()

        If InitialiseTests() Then
            Assert.IsNotNull(SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable)), StoredProcedureName & " did not retrieve a datatable")
        End If
    End Sub

    <TestMethod()> _
    Public Overridable Sub WhenDataExists_StoredProcedureGetsAtLeastOneRecord()

        If InitialiseTests() Then
            Dim TrySPExisting As Object = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable))

            If TrySPExisting IsNot Nothing AndAlso TypeOf TrySPExisting Is DataTable Then
                Dim SPExisting = CType(TrySPExisting, DataTable)

                If SPExisting.Rows.Count = 0 Then

                End If
            Else
                Assert.Inconclusive("Intialisation failed as did not get a datatable using " & StoredProcedureName & " so cannot perform this test")
            End If
        End If
    End Sub

    ' '' Example field test
    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedFieldData()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("FieldData", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

#Region "Common Tests"

    Protected Overridable Function HaveDataToCompare(Optional ByVal MultipleRowsAllowed As Boolean = False) As Boolean

        If InitialiseTests() Then
            Dim TryExisting = CType(SafeGetTestContextProperty(TC_KEY_EXISTING, GetType(DataTable)), DataTable)

            If TryExisting IsNot Nothing AndAlso TypeOf TryExisting Is DataTable Then
                Dim Existing As DataTable = TryExisting

                If Existing.Rows.Count = 1 Or (MultipleRowsAllowed And Existing.Rows.Count > 0) Then
                    Dim TrySPExisting = CType(SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable)), DataTable)

                    If TrySPExisting IsNot Nothing Then
                        Dim SPExisting As DataTable = TrySPExisting

                        If SPExisting.Rows.Count = 1 Or (MultipleRowsAllowed And SPExisting.Rows.Count > 0) Then
                            HaveDataToCompare = True
                        Else
                            Assert.Inconclusive(StoredProcedureName & " failed to return an existing record so this test has nothing to compare")
                        End If
                    Else
                        Assert.Inconclusive(StoredProcedureName & " failed to return a datatable so this test has nothing to compare")
                    End If
                Else
                    Assert.Inconclusive("Dynamic sql failed to return an existing record so this test has nothing to compare against")
                End If
            Else
                Assert.Inconclusive("Dynamic sql failed to return a datatable so this test has nothing to compare against")
            End If
        End If
    End Function
#End Region

#Region "Initialise Test Context methods"

    Protected Overridable Function CheckForStoredProcedure() As Boolean

        Try
            Using con As New Connection
                Using com As New Command(con)
                    Dim spCheck As DataTable

                    com.CommandText = "Select * From sys.objects Where type = 'P' And name = '" & StoredProcedureName & "'"
                    spCheck = com.ExecuteDataTable
                    If spCheck IsNot Nothing AndAlso spCheck.Rows.Count = 1 Then
                        SafeAddTestContextProperty(TC_KEY_FOUNDSTOREDPROCEDURE, True)
                    Else
                        SafeAddTestContextProperty(TC_KEY_FOUNDSTOREDPROCEDURE, False)
                    End If
                    CheckForStoredProcedure = True
                End Using
            End Using
        Catch ex As Exception

            'Assert.Inconclusive("Initialisation for tests failed.  Failed in the attempt to check for existence of the stored procedure (" & StoredProcedureName & ".")
            Assert.Fail("Failure message in CheckForStoredProcedure: " & ex.Message)

        End Try
    End Function

    Protected Overridable Function GetExisting() As Boolean

        Try
            Dim ExistingData As New DataTable
            Dim Keys As Collection = New Collection

            If GetExistingDataAndKeyWithDynamicSQL(ExistingData, Keys) Then
                ' Keep the key for use in retrieval by stored procedure
                SafeAddTestContextProperty(TC_KEY_EXISTINGKEY, New StoredProcedureKey(Keys))
                SafeAddTestContextProperty(TC_KEY_EXISTING, ExistingData)
                GetExisting = True
            End If
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record.")
        End Try
    End Function

    Protected Overridable Function GetNonExistingKey() As Boolean

        Try
            Dim Keys As Collection = New Collection

            If GetNonExistingKeyWithDynamicSQL(Keys) Then
                SafeAddTestContextProperty(TC_KEY_NONEXISTINGKEY, New StoredProcedureKey(Keys))
                GetNonExistingKey = True
            End If
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a non existing record key.")
        End Try
    End Function

    Protected Overridable Function GetExistingUsingStoredProcedure() As Boolean

        Using con As New Connection
            Using com As New Command(con)
                Dim TrySPKey As Object

                com.StoredProcedureName = StoredProcedureName
                TrySPKey = SafeGetTestContextProperty(TC_KEY_EXISTINGKEY, GetType(StoredProcedureKey))
                If TrySPKey IsNot Nothing AndAlso TypeOf TrySPKey Is StoredProcedureKey Then
                    Dim SPKey = CType(TrySPKey, StoredProcedureKey)

                    If SPKey.AddParameters(com) Then
                        Dim SPExisting As DataTable

                        SPExisting = com.ExecuteDataTable
                        SafeAddTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, SPExisting)
                        GetExistingUsingStoredProcedure = True
                    End If
                End If
            End Using
        End Using
    End Function

    Protected Overridable Function GetNonExistingUsingStoredProcedure() As Boolean

        Using con As New Connection
            Using com As New Command(con)
                Dim TrySPKey As Object

                com.StoredProcedureName = StoredProcedureName
                TrySPKey = SafeGetTestContextProperty(TC_KEY_NONEXISTINGKEY, GetType(StoredProcedureKey))
                If TrySPKey IsNot Nothing AndAlso TypeOf TrySPKey Is StoredProcedureKey Then
                    Dim SPKey = CType(TrySPKey, StoredProcedureKey)

                    If SPKey.AddParameters(com) Then
                        Dim SPNonExisting As DataTable

                        SPNonExisting = com.ExecuteDataTable
                        SafeAddTestContextProperty(TC_KEY_STOREDPROCEDURENONEXISTING, SPNonExisting)
                        GetNonExistingUsingStoredProcedure = True
                    End If
                End If
            End Using
        End Using
    End Function

    Protected Overridable Function InitialiseTests() As Boolean

        If TestContext.Properties(TC_KEY_INITIALISED) Is Nothing Then
            If CheckForStoredProcedure() Then
                If GetExisting() Then
                    If GetNonExistingKey() Then
                        If GetExistingUsingStoredProcedure() Then
                            If GetNonExistingUsingStoredProcedure() Then
                                SafeAddTestContextProperty(TC_KEY_INITIALISED, True)
                            Else
                                Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an non existing record using stored procedure")
                            End If
                        Else
                            Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using stored procedure")
                        End If
                    Else
                        Assert.Inconclusive("Test initialisation failed whilst trying to retrieve a key for a non existing record using dynamic sql")
                    End If
                Else
                    Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using dynamic sql")
                End If
            Else
                Assert.Inconclusive("Test initialisation failed whilst trying to check for the stored procedure (" & StoredProcedureName & ")")
            End If
        End If
        InitialiseTests = CBool(SafeGetTestContextProperty(TC_KEY_INITIALISED, GetType(Boolean)))
    End Function

    Public Sub SafeAddTestContextProperty(ByVal PropertyKey As String, ByVal PropertyValue As Object)

        If TestContext.Properties(PropertyKey) IsNot Nothing Then
            TestContext.Properties.Remove(PropertyKey)
        End If
        TestContext.Properties.Add(PropertyKey, PropertyValue)
    End Sub

    Public Function SafeGetTestContextProperty(ByVal PropertyKey As String, ByVal PropertyType As Type) As Object

        SafeGetTestContextProperty = Nothing
        If TestContext IsNot Nothing AndAlso TestContext.Properties(PropertyKey) IsNot Nothing Then
            If PropertyType IsNot Nothing Then
                Select Case PropertyType.Name
                    Case GetType(String).Name
                        SafeGetTestContextProperty = TestContext.Properties(PropertyKey).ToString()
                    Case GetType(Integer).Name
                        Dim Prop As Integer

                        If Integer.TryParse(TestContext.Properties(PropertyKey).ToString(), Prop) Then
                            SafeGetTestContextProperty = Prop
                        End If
                    Case GetType(Boolean).Name
                        Dim Prop As Boolean

                        If Boolean.TryParse(TestContext.Properties(PropertyKey).ToString(), Prop) Then
                            SafeGetTestContextProperty = Prop
                        End If
                    Case GetType(Date).Name
                        Dim Prop As Date

                        If Date.TryParse(TestContext.Properties(PropertyKey).ToString(), Prop) Then
                            SafeGetTestContextProperty = Prop
                        End If
                    Case GetType(DataTable).Name
                        Dim TryDataTable As Object = TestContext.Properties(PropertyKey)
                        Dim Prop As DataTable

                        If TypeOf TryDataTable Is DataTable Then
                            Prop = CType(TryDataTable, DataTable)
                            SafeGetTestContextProperty = Prop
                        End If
                    Case Else
                        SafeGetTestContextProperty = TestContext.Properties(PropertyKey)
                End Select
            Else
                SafeGetTestContextProperty = TestContext.Properties(PropertyKey)
            End If
        End If
    End Function
#End Region

#Region "Field Comparisons"

    Protected Enum FieldType
        ftString = 0
        ftInteger
        ftDecimal
        ftBoolean
        ftDate
    End Enum

    Protected Function FieldsMatch(ByVal FieldName As String, ByRef Message As String, ByVal Type As FieldType) As Boolean
        Dim Marker As String = "Start"

        Try
            Dim TryExisting As Object = SafeGetTestContextProperty(TC_KEY_EXISTING, GetType(DataTable))

            Marker = "Existing"
            If TryExisting IsNot Nothing AndAlso TypeOf TryExisting Is DataTable Then
                Dim Existing = CType(TryExisting, DataTable)

                If Existing.Rows.Count = 1 Then
                    If Existing.Rows(0).Item(FieldName) IsNot Nothing Then
                        Dim TrySPExisting As Object = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable))

                        Marker = "Retreived"
                        If TrySPExisting IsNot Nothing AndAlso TypeOf TrySPExisting Is DataTable Then
                            Dim SPExisting = CType(TrySPExisting, DataTable)

                            If SPExisting.Rows.Count = 1 Then
                                If SPExisting.Rows(0).Item(FieldName) IsNot Nothing Then
                                    Marker = "DBNull"
                                    If TypeOf Existing.Rows(0).Item(FieldName) Is DBNull Then
                                        If TypeOf SPExisting.Rows(0).Item(FieldName) Is DBNull Then
                                            FieldsMatch = True
                                        Else
                                            Message = FieldName & " values do not match as 'existing' is Null and retreived by stored procedure is not Null"
                                        End If
                                    Else
                                        If TypeOf SPExisting.Rows(0).Item(FieldName) Is DBNull Then
                                            Message = FieldName & " values do not match as 'existing' is not Null and retreived by stored procedure is Null"
                                        Else
                                            Marker = "Select"
                                            Select Case Type
                                                Case FieldType.ftString
                                                    Marker = "String"
                                                    FieldsMatch = StringsMatch(Existing.Rows(0).Item(FieldName), SPExisting.Rows(0).Item(FieldName), Message)
                                                Case FieldType.ftInteger
                                                    Marker = "Integer"
                                                    FieldsMatch = IntegersMatch(Existing.Rows(0).Item(FieldName), SPExisting.Rows(0).Item(FieldName), Message)
                                                Case FieldType.ftBoolean
                                                    Marker = "Boolean"
                                                    FieldsMatch = BooleansMatch(Existing.Rows(0).Item(FieldName), SPExisting.Rows(0).Item(FieldName), Message)
                                                Case FieldType.ftDecimal
                                                    Marker = "Decimal"
                                                    FieldsMatch = DecimalsMatch(Existing.Rows(0).Item(FieldName), SPExisting.Rows(0).Item(FieldName), Message)
                                                Case FieldType.ftDate
                                                    Marker = "Date"
                                                    FieldsMatch = DatesMatch(Existing.Rows(0).Item(FieldName), SPExisting.Rows(0).Item(FieldName), Message)
                                                Case Else
                                                    Marker = "Not recognised"
                                                    Assert.Inconclusive("Cannot check field " & FieldName & " as field type specified is no recognised")
                                            End Select
                                        End If
                                    End If
                                Else
                                    Marker = "Error"
                                    Message = "Stored Procedure " & StoredProcedureName & " field name " & FieldName & " is not a field name in the original dynamic sql"
                                End If
                            Else
                                Marker = "Error"
                                Message = "Stored Procedure " & StoredProcedureName & " failed to bring back the record so cannot compare this field"
                            End If
                        Else
                            Marker = "Error"
                            Message = "Stored Procedure " & StoredProcedureName & " failed to bring back any data so cannot compare this field"
                        End If
                    Else
                        Marker = "Error"
                        Message = "Cannot check field " & FieldName & ", it is not a field in the original dynamic sql row."
                    End If
                Else
                    Marker = "Error"
                    Message = "Dynamic sql failed to bring back the record so cannot compare this field"
                End If
            Else
                Marker = "Error"
                Message = "Dynamic sql failed to bring back any data so cannot compare this field"
            End If
        Catch ex As Exception
            Select Case Marker
                Case "Start"
                    Message = "Cannot check field " & FieldName & " problem at start of FieldsMatch function. " & ex.Message
                Case "Existing"
                    Message = "Cannot check field " & FieldName & ", it is not a field in the original dynamic sql row."
                Case "Retreived"
                    Message = "Stored Procedure " & StoredProcedureName & " uses a different field name to the one (" & FieldName & ") used in the original dynamic sql."
                Case "DBNull"
                    Message = "Cannot check field " & FieldName & " problem whilst determining if field values are Null. " & ex.Message
                Case "Select"
                    Message = "Cannot check field " & FieldName & " problem processing field type. " & ex.Message
                Case "String"
                    Message = "Cannot check field " & FieldName & " problem processing string field type. " & ex.Message
                Case "Integer"
                    Message = "Cannot check field " & FieldName & " problem processing integer field type. " & ex.Message
                Case "Boolean"
                    Message = "Cannot check field " & FieldName & " problem processing boolean field type. " & ex.Message
                Case "Decimal"
                    Message = "Cannot check field " & FieldName & " problem processing decimal field type. " & ex.Message
                Case "Date"
                    Message = "Cannot check field " & FieldName & " problem processing date field type. " & ex.Message
                Case "Not recognised"
                    Message = "Cannot check field " & FieldName & " problem processing unrecognised field type. " & ex.Message
                Case "Error"
                    ' Already got a message, so nothing to do here
                Case Else
                    Message = "Cannot check field " & FieldName & " problem with FieldsMatch function. " & ex.Message
            End Select
        End Try
    End Function

    Friend Function BooleansMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Booleans Match fail (" & FailReason & ")"
        Else
            If TypeOf Against Is Boolean Then
                Dim AgainstAsBoolean = CBool(Against)

                If TypeOf Compare Is Boolean Then
                    Dim CompareAsBoolean = CBool(Compare)

                    If AgainstAsBoolean = CompareAsBoolean Then
                        BooleansMatch = True
                    Else
                        FailReason = "Booleans Match fail as 'Against' (" & AgainstAsBoolean.ToString & ") and 'Compare' (" & CompareAsBoolean.ToString & ") are not the same"
                    End If
                Else
                    FailReason = "Booleans Match fail as 'Compare' is not a Boolean"
                End If
            Else
                FailReason = "Booleans Match fail as 'Against' is not a Boolean"
            End If
        End If
        Message = FailReason
    End Function

    Friend Function DatesMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String, Optional ByVal DateInterval As DateInterval = DateInterval.Second, Optional ByVal AreNullable As Boolean = False) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Dates Match fail (" & FailReason & ")"
        Else
            Dim AgainstDate As Date
            Dim CompareDate As Date

            If AreNullable Then
                If TypeOf Against Is Date? Then
                    Dim AgainstAsNullableDate As Date?

                    If TypeOf Compare Is Date? Then
                        Dim CompareAsNullableDate As Date?

                        If AgainstAsNullableDate.HasValue Then
                            AgainstDate = AgainstAsNullableDate.Value
                            If CompareAsNullableDate.HasValue Then
                                CompareDate = CompareAsNullableDate.Value
                            Else
                                FailReason = "Dates Match fail as 'Against' is not Null but 'Compare' is Null"
                            End If
                        Else
                            If CompareAsNullableDate.HasValue Then
                                FailReason = "Dates Match fail as 'Against' is Null but 'Compare' is not Null"
                            Else
                                Message = FailReason
                                DatesMatch = True
                                Exit Function
                            End If
                        End If
                    Else
                        FailReason = "Dates Match fail as 'Compare' is not a Nullable Date"
                    End If
                Else
                    FailReason = "Dates Match fail as 'Compare' is not a Nullable Date"
                End If
            Else
                If TypeOf Against Is Date Then
                    AgainstDate = CDate(Against)
                    If TypeOf Compare Is Date Then
                        CompareDate = CDate(Compare)
                    Else
                        FailReason = "Dates Match fail as 'Compare' is not a Date"
                    End If
                Else
                    FailReason = "Dates Match fail as 'Compare' is not a Date"
                End If
            End If
            Dim Diff As Boolean = DateDiff(DateInterval, AgainstDate, CompareDate) <> 0

            If Diff Then
                Dim Interval As String

                Select Case DateInterval
                    Case Microsoft.VisualBasic.DateInterval.Day
                        Interval = "days"
                    Case Microsoft.VisualBasic.DateInterval.DayOfYear
                        Interval = "days (of year)"
                    Case Microsoft.VisualBasic.DateInterval.Hour
                        Interval = "hours"
                    Case Microsoft.VisualBasic.DateInterval.Month
                        Interval = "months"
                    Case Microsoft.VisualBasic.DateInterval.Quarter
                        Interval = "quarters"
                    Case Microsoft.VisualBasic.DateInterval.Second
                        Interval = "seconds"
                    Case Microsoft.VisualBasic.DateInterval.Weekday
                        Interval = "week days"
                    Case Microsoft.VisualBasic.DateInterval.WeekOfYear
                        Interval = "weeks (of year)"
                    Case Microsoft.VisualBasic.DateInterval.Year
                        Interval = "years"
                    Case Else
                        Interval = "unknown units"
                End Select
                FailReason = "Dates Match fail as 'Against' (" & AgainstDate.ToString("F") & ") is " & CInt(Diff).ToString & " " & Interval & " different to 'Compare' (" & AgainstDate.ToString("F") & ")"
            Else
                DatesMatch = True
            End If
        End If
        Message = FailReason
    End Function

    Friend Function DecimalsMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Integers Match fail (" & FailReason & ")"
        Else
            Dim AgainstAsDecimal As Decimal

            If Decimal.TryParse(Against.ToString, AgainstAsDecimal) Then
                Dim CompareAsDecimal As Decimal

                If Decimal.TryParse(Compare.ToString, CompareAsDecimal) Then
                    If AgainstAsDecimal <> CompareAsDecimal Then
                        FailReason = "Decimals Match fail as 'Against' (= " & AgainstAsDecimal & ") and 'Compare' (= " & CompareAsDecimal & ") are not the same"
                    Else
                        DecimalsMatch = True
                    End If
                Else
                    FailReason = "Decimals Match fail as 'Compare' is not a Decimal"
                End If
            Else
                FailReason = "Decimals Match fail as 'Against' is not a Decimal"
            End If
        End If
        Message = FailReason
    End Function

    Friend Function IntegersMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Integers Match fail (" & FailReason & ")"
        Else
            Dim AgainstAsInteger As Integer

            If Integer.TryParse(Against.ToString, AgainstAsInteger) Then
                Dim CompareAsInteger As Integer

                If Integer.TryParse(Compare.ToString, CompareAsInteger) Then
                    If AgainstAsInteger <> CompareAsInteger Then
                        FailReason = "Integers Match fail as 'Against' (= " & AgainstAsInteger & ") and 'Compare' (= " & CompareAsInteger & ") are not the same"
                    Else
                        IntegersMatch = True
                    End If
                Else
                    FailReason = "Integers Match fail as 'Compare' is not an Integer"
                End If
            Else
                FailReason = "Integers Match fail as 'Against' is not an Integer"
            End If
        End If
        Message = FailReason
    End Function

    Friend Function StringsMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String, Optional ByVal StringComparisonType As System.StringComparison = StringComparison.CurrentCulture) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Strings Match fail (" & FailReason & ")"
        Else
            If TypeOf Against Is String Then
                Dim AgainstAsString As String = Against.ToString

                If TypeOf Compare Is String Then
                    Dim CompareAsString As String = Compare.ToString

                    If AgainstAsString = String.Empty Then
                        If CompareAsString = String.Empty Then
                            StringsMatch = True
                        Else
                            FailReason = "Strings Match fail as 'Against' is Empty but 'Compare' is not Empty"
                        End If
                    Else
                        If CompareAsString = String.Empty Then
                            FailReason = "Strings Match fail as 'Against' is not Empty but 'Compare' is Empty"
                        Else
                            If String.Equals(AgainstAsString, CompareAsString, StringComparisonType) Then
                                StringsMatch = True
                            Else
                                FailReason = "Strings Match fail as 'Against' (= " & AgainstAsString & ") and 'Compare' (= " & CompareAsString & ") are not the same"
                            End If
                        End If
                    End If
                Else
                    FailReason = "Strings Match fail as 'Compare' is not a string"
                End If
            Else
                FailReason = "Strings Match fail as 'Against' is not a string"
            End If
        End If
        Message = FailReason
    End Function

    Private Function ObjectsBothNotNothingOrBothSomething(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
        Dim FailReason As String

        If Against Is Nothing Then
            If Compare IsNot Nothing Then
                FailReason = "Against is 'Nothing', Compare is not 'Nothing'"
            Else
                ObjectsBothNotNothingOrBothSomething = True
            End If
        Else
            If Compare Is Nothing Then
                FailReason = "Against is not 'Nothing', Compare is 'Nothing'"
            Else
                ObjectsBothNotNothingOrBothSomething = True
            End If
        End If
    End Function
#End Region

    Protected Structure Key

        Private storedProcedureName As String
        Private storedProcedureParamName As String
        Private dynSQLName As String
        Private friendlyName As String
        Private keyValue As Object
        Private keyfieldType As FieldType

        Public Property SPName() As String
            Get
                SPName = storedProcedureName
            End Get
            Set(ByVal value As String)
                storedProcedureName = value
            End Set
        End Property

        Public Property SPParamName() As String
            Get
                SPParamName = storedProcedureParamName
            End Get
            Set(ByVal value As String)
                storedProcedureParamName = value
            End Set
        End Property

        Public Property DynamicSQLName() As String
            Get
                DynamicSQLName = dynSQLName
            End Get
            Set(ByVal value As String)
                dynSQLName = value
            End Set
        End Property

        Public Property Caption() As String
            Get
                Caption = friendlyName
            End Get
            Set(ByVal value As String)
                friendlyName = value
            End Set
        End Property

        Public Property Value() As Object
            Get
                Value = keyValue
            End Get
            Set(ByVal value As Object)
                keyValue = value
            End Set
        End Property

        Public Property Type() As FieldType
            Get
                Type = keyfieldType
            End Get
            Set(ByVal value As FieldType)
                keyfieldType = value
            End Set
        End Property

        Public Sub New(ByVal KeySPParamName As String, ByVal KeyDynamicSQLName As String, ByVal KeyCaption As String, ByRef KeyValue As Object, ByVal KeyType As FieldType)

            SPParamName = KeySPParamName
            DynamicSQLName = KeyDynamicSQLName
            Caption = KeyCaption
            Value = KeyValue
            Type = KeyType
        End Sub

        Public Sub New(ByVal KeySPParamName As String, ByVal KeyDynamicSQLName As String, ByVal KeyCaption As String, ByRef KeyValue As Object, ByVal SPName As String, ByVal KeyType As FieldType)

            SPParamName = KeySPParamName
            DynamicSQLName = KeyDynamicSQLName
            Caption = KeyCaption
            Value = KeyValue
            SPName = SPName
            Type = KeyType
        End Sub
    End Structure

    Protected Class StoredProcedureKey
        Private Keys As Collection

        Public Sub New(ByVal SPKeys As Collection)

            Keys = New Collection
            If SPKeys IsNot Nothing Then
                For Each SPKey As Key In SPKeys
                    Keys.Add(SPKey, SPKey.DynamicSQLName)
                Next
            End If
        End Sub

        Public Overrides Function ToString() As String

            ToString = ""
            If Keys IsNot Nothing Then
                For Each SPKey As Key In Keys
                    If ToString.Length > 0 Then
                        ToString &= ";"
                    End If
                    ToString &= SPKey.Caption & " = '" & SPKey.Value.ToString & "'"
                Next
            Else
                ToString = "Empty StoredProcedureKey"
            End If
        End Function

        Public Function GetKeyString(ByRef From As DataRow) As String

            GetKeyString = ""
            If Keys IsNot Nothing Then
                For Each SPKey As Key In Keys
                    If ToString.Length > 0 Then
                        GetKeyString &= ";"
                    End If
                    GetKeyString &= SPKey.Caption & " = '" & From.Item(SPKey.DynamicSQLName).ToString & "'"
                Next
            Else
                GetKeyString = "Empty StoredProcedureKey"
            End If
        End Function

        Public Function AddParameters(ByRef ComToAddTo As Command) As Boolean

            If Keys IsNot Nothing And ComToAddTo IsNot Nothing Then
                For Each SPKey As Key In Keys
                    ComToAddTo.AddParameter("@" & SPKey.SPParamName, SPKey.Value)
                Next
                AddParameters = True
            End If
        End Function

        Public Function AddParameters(ByRef ComToAddTo As SqlCommand) As Boolean

            If Keys IsNot Nothing And ComToAddTo IsNot Nothing Then
                For Each SPKey As Key In Keys
                    ComToAddTo.Parameters.Add(New SqlParameter("@" & SPKey.SPParamName, SPKey.Value))
                Next
                AddParameters = True
            End If
        End Function
    End Class

End Class
