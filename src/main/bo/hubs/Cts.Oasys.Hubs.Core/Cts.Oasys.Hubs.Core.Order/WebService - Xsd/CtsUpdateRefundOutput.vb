﻿Imports System.Xml.Serialization

Namespace WebService

    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True), _
      Xml.Serialization.XmlRootAttribute([Namespace]:="", IsNullable:=False)> _
    Partial Public Class CTSUpdateRefundResponse

        Private dateTimeStampField As CTSUpdateRefundResponseDateTimeStamp

        Private oMOrderNumberField As CTSUpdateRefundResponseOMOrderNumber

        Private orderRefundsField() As CTSUpdateRefundResponseOrderRefund

        '''<remarks/>
        Public Property DateTimeStamp() As CTSUpdateRefundResponseDateTimeStamp
            Get
                Return Me.dateTimeStampField
            End Get
            Set(ByVal value As CTSUpdateRefundResponseDateTimeStamp)
                Me.dateTimeStampField = value
            End Set
        End Property

        '''<remarks/>
        Public Property OMOrderNumber() As CTSUpdateRefundResponseOMOrderNumber
            Get
                Return Me.oMOrderNumberField
            End Get
            Set(ByVal value As CTSUpdateRefundResponseOMOrderNumber)
                Me.oMOrderNumberField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlArrayItemAttribute("OrderRefund", IsNullable:=False)> _
        Public Property OrderRefunds() As CTSUpdateRefundResponseOrderRefund()
            Get
                Return Me.orderRefundsField
            End Get
            Set(ByVal value As CTSUpdateRefundResponseOrderRefund())
                Me.orderRefundsField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundResponseDateTimeStamp

        Private validationStatusField As String

        Private valueField As Date

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundResponseOMOrderNumber

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundResponseOrderRefund

        Private successFlagField As Boolean

        Private notRefundableFlagField As Boolean

        Private refundDateField As CTSUpdateRefundResponseOrderRefundRefundDate

        Private refundStoreCodeField As CTSUpdateRefundResponseOrderRefundRefundStoreCode

        Private refundTransactionField As CTSUpdateRefundResponseOrderRefundRefundTransaction

        Private refundTillField As CTSUpdateRefundResponseOrderRefundRefundTill

        Private fulfilmentSitesField() As CTSUpdateRefundResponseOrderRefundFulfilmentSite

        Private refundLinesField() As CTSUpdateRefundResponseOrderRefundRefundLine

        '''<remarks/>
        Public Property SuccessFlag() As Boolean
            Get
                Return Me.successFlagField
            End Get
            Set(ByVal value As Boolean)
                Me.successFlagField = value
            End Set
        End Property

        '''<remarks/>
        Public Property NotRefundableFlag() As Boolean
            Get
                Return Me.notRefundableFlagField
            End Get
            Set(ByVal value As Boolean)
                Me.notRefundableFlagField = value
            End Set
        End Property

        '''<remarks/>
        Public Property RefundDate() As CTSUpdateRefundResponseOrderRefundRefundDate
            Get
                Return Me.refundDateField
            End Get
            Set(ByVal value As CTSUpdateRefundResponseOrderRefundRefundDate)
                Me.refundDateField = value
            End Set
        End Property

        '''<remarks/>
        Public Property RefundStoreCode() As CTSUpdateRefundResponseOrderRefundRefundStoreCode
            Get
                Return Me.refundStoreCodeField
            End Get
            Set(ByVal value As CTSUpdateRefundResponseOrderRefundRefundStoreCode)
                Me.refundStoreCodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property RefundTransaction() As CTSUpdateRefundResponseOrderRefundRefundTransaction
            Get
                Return Me.refundTransactionField
            End Get
            Set(ByVal value As CTSUpdateRefundResponseOrderRefundRefundTransaction)
                Me.refundTransactionField = value
            End Set
        End Property

        '''<remarks/>
        Public Property RefundTill() As CTSUpdateRefundResponseOrderRefundRefundTill
            Get
                Return Me.refundTillField
            End Get
            Set(ByVal value As CTSUpdateRefundResponseOrderRefundRefundTill)
                Me.refundTillField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlArrayItemAttribute("FulfilmentSite", IsNullable:=False)> _
        Public Property FulfilmentSites() As CTSUpdateRefundResponseOrderRefundFulfilmentSite()
            Get
                Return Me.fulfilmentSitesField
            End Get
            Set(ByVal value As CTSUpdateRefundResponseOrderRefundFulfilmentSite())
                Me.fulfilmentSitesField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlArrayItemAttribute("RefundLine", IsNullable:=False)> _
        Public Property RefundLines() As CTSUpdateRefundResponseOrderRefundRefundLine()
            Get
                Return Me.refundLinesField
            End Get
            Set(ByVal value As CTSUpdateRefundResponseOrderRefundRefundLine())
                Me.refundLinesField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundResponseOrderRefundRefundDate

        Private validationStatusField As String

        Private valueField As Date

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundResponseOrderRefundRefundStoreCode

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundResponseOrderRefundRefundTransaction

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundResponseOrderRefundRefundTill

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundResponseOrderRefundFulfilmentSite

        Private fulfilmentSiteCodeField As CTSUpdateRefundResponseOrderRefundFulfilmentSiteFulfilmentSiteCode

        Private sellingStoreIBTOutNumberField As CTSUpdateRefundResponseOrderRefundFulfilmentSiteSellingStoreIBTOutNumber

        Private fulfilmentSiteIBTInNumberField As String

        '''<remarks/>
        Public Property FulfilmentSiteCode() As CTSUpdateRefundResponseOrderRefundFulfilmentSiteFulfilmentSiteCode
            Get
                Return Me.fulfilmentSiteCodeField
            End Get
            Set(ByVal value As CTSUpdateRefundResponseOrderRefundFulfilmentSiteFulfilmentSiteCode)
                Me.fulfilmentSiteCodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingStoreIBTOutNumber() As CTSUpdateRefundResponseOrderRefundFulfilmentSiteSellingStoreIBTOutNumber
            Get
                Return Me.sellingStoreIBTOutNumberField
            End Get
            Set(ByVal value As CTSUpdateRefundResponseOrderRefundFulfilmentSiteSellingStoreIBTOutNumber)
                Me.sellingStoreIBTOutNumberField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(DataType:="integer")> _
        Public Property FulfilmentSiteIBTInNumber() As String
            Get
                Return Me.fulfilmentSiteIBTInNumberField
            End Get
            Set(ByVal value As String)
                Me.fulfilmentSiteIBTInNumberField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundResponseOrderRefundFulfilmentSiteFulfilmentSiteCode

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundResponseOrderRefundFulfilmentSiteSellingStoreIBTOutNumber

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundResponseOrderRefundRefundLine

        Private sellingStoreLineNoField As CTSUpdateRefundResponseOrderRefundRefundLineSellingStoreLineNo

        Private oMOrderLineNoField As CTSUpdateRefundResponseOrderRefundRefundLineOMOrderLineNo

        Private productCodeField As CTSUpdateRefundResponseOrderRefundRefundLineProductCode

        Private fulfilmentSiteCodeField As CTSUpdateRefundResponseOrderRefundRefundLineFulfilmentSiteCode

        Private quantityReturnedField As CTSUpdateRefundResponseOrderRefundRefundLineQuantityReturned

        Private quantityCancelledField As CTSUpdateRefundResponseOrderRefundRefundLineQuantityCancelled

        Private refundLineValueField As CTSUpdateRefundResponseOrderRefundRefundLineRefundLineValue

        Private refundLineStatusField As CTSUpdateRefundResponseOrderRefundRefundLineRefundLineStatus

        '''<remarks/>
        Public Property SellingStoreLineNo() As CTSUpdateRefundResponseOrderRefundRefundLineSellingStoreLineNo
            Get
                Return Me.sellingStoreLineNoField
            End Get
            Set(ByVal value As CTSUpdateRefundResponseOrderRefundRefundLineSellingStoreLineNo)
                Me.sellingStoreLineNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property OMOrderLineNo() As CTSUpdateRefundResponseOrderRefundRefundLineOMOrderLineNo
            Get
                Return Me.oMOrderLineNoField
            End Get
            Set(ByVal value As CTSUpdateRefundResponseOrderRefundRefundLineOMOrderLineNo)
                Me.oMOrderLineNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property ProductCode() As CTSUpdateRefundResponseOrderRefundRefundLineProductCode
            Get
                Return Me.productCodeField
            End Get
            Set(ByVal value As CTSUpdateRefundResponseOrderRefundRefundLineProductCode)
                Me.productCodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property FulfilmentSiteCode() As CTSUpdateRefundResponseOrderRefundRefundLineFulfilmentSiteCode
            Get
                Return Me.fulfilmentSiteCodeField
            End Get
            Set(ByVal value As CTSUpdateRefundResponseOrderRefundRefundLineFulfilmentSiteCode)
                Me.fulfilmentSiteCodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property QuantityReturned() As CTSUpdateRefundResponseOrderRefundRefundLineQuantityReturned
            Get
                Return Me.quantityReturnedField
            End Get
            Set(ByVal value As CTSUpdateRefundResponseOrderRefundRefundLineQuantityReturned)
                Me.quantityReturnedField = value
            End Set
        End Property

        '''<remarks/>
        Public Property QuantityCancelled() As CTSUpdateRefundResponseOrderRefundRefundLineQuantityCancelled
            Get
                Return Me.quantityCancelledField
            End Get
            Set(ByVal value As CTSUpdateRefundResponseOrderRefundRefundLineQuantityCancelled)
                Me.quantityCancelledField = value
            End Set
        End Property

        '''<remarks/>
        Public Property RefundLineValue() As CTSUpdateRefundResponseOrderRefundRefundLineRefundLineValue
            Get
                Return Me.refundLineValueField
            End Get
            Set(ByVal value As CTSUpdateRefundResponseOrderRefundRefundLineRefundLineValue)
                Me.refundLineValueField = value
            End Set
        End Property

        '''<remarks/>
        Public Property RefundLineStatus() As CTSUpdateRefundResponseOrderRefundRefundLineRefundLineStatus
            Get
                Return Me.refundLineStatusField
            End Get
            Set(ByVal value As CTSUpdateRefundResponseOrderRefundRefundLineRefundLineStatus)
                Me.refundLineStatusField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundResponseOrderRefundRefundLineSellingStoreLineNo

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundResponseOrderRefundRefundLineOMOrderLineNo

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundResponseOrderRefundRefundLineProductCode

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundResponseOrderRefundRefundLineFulfilmentSiteCode

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundResponseOrderRefundRefundLineQuantityReturned

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundResponseOrderRefundRefundLineQuantityCancelled

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundResponseOrderRefundRefundLineRefundLineValue

        Private validationStatusField As String

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundResponseOrderRefundRefundLineRefundLineStatus

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

End Namespace