﻿Imports System.Xml.Serialization
Imports System.Xml

Namespace WebService

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True), _
     Xml.Serialization.XmlRootAttribute([Namespace]:="", IsNullable:=False)> _
    Partial Public Class OMOrderCreateRequest

        Private dateTimeStampField As OMOrderCreateRequestDateTimeStamp

        Private orderHeaderField As OMOrderCreateRequestOrderHeader

        Private orderLinesField() As OMOrderCreateRequestOrderLine

        '''<remarks/>
        Public Property DateTimeStamp() As OMOrderCreateRequestDateTimeStamp
            Get
                Return Me.dateTimeStampField
            End Get
            Set(ByVal value As OMOrderCreateRequestDateTimeStamp)
                Me.dateTimeStampField = value
            End Set
        End Property

        '''<remarks/>
        Public Property OrderHeader() As OMOrderCreateRequestOrderHeader
            Get
                Return Me.orderHeaderField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeader)
                Me.orderHeaderField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlArrayItemAttribute("OrderLine", IsNullable:=False)> _
        Public Property OrderLines() As OMOrderCreateRequestOrderLine()
            Get
                Return Me.orderLinesField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderLine())
                Me.orderLinesField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestDateTimeStamp

        Private valueField As Date

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeader

        Private sourceField As String

        Private sourceOrderNumberField As String

        Private sellingStoreCodeField As OMOrderCreateRequestOrderHeaderSellingStoreCode

        Private sellingStoreOrderNumberField As OMOrderCreateRequestOrderHeaderSellingStoreOrderNumber

        Private sellingStoreTillField As String

        Private sellingStoreTransactionField As String

        Private requiredDeliveryDateField As OMOrderCreateRequestOrderHeaderRequiredDeliveryDate

        Private deliveryChargeField As OMOrderCreateRequestOrderHeaderDeliveryCharge

        Private totalOrderValueField As OMOrderCreateRequestOrderHeaderTotalOrderValue

        Private saleDateField As OMOrderCreateRequestOrderHeaderSaleDate

        Private customerAccountNoField As OMOrderCreateRequestOrderHeaderCustomerAccountNo

        Private customerNameField As OMOrderCreateRequestOrderHeaderCustomerName

        Private customerAddressLine1Field As OMOrderCreateRequestOrderHeaderCustomerAddressLine1

        Private customerAddressLine2Field As OMOrderCreateRequestOrderHeaderCustomerAddressLine2

        Private customerAddressTownField As OMOrderCreateRequestOrderHeaderCustomerAddressTown

        Private customerAddressLine4Field As OMOrderCreateRequestOrderHeaderCustomerAddressLine4

        Private customerPostcodeField As OMOrderCreateRequestOrderHeaderCustomerPostcode

        Private deliveryAddressLine1Field As OMOrderCreateRequestOrderHeaderDeliveryAddressLine1

        Private deliveryAddressLine2Field As OMOrderCreateRequestOrderHeaderDeliveryAddressLine2

        Private deliveryAddressTownField As OMOrderCreateRequestOrderHeaderDeliveryAddressTown

        Private deliveryAddressLine4Field As OMOrderCreateRequestOrderHeaderDeliveryAddressLine4

        Private deliveryPostcodeField As OMOrderCreateRequestOrderHeaderDeliveryPostcode

        Private contactPhoneHomeField As OMOrderCreateRequestOrderHeaderContactPhoneHome

        Private contactPhoneMobileField As OMOrderCreateRequestOrderHeaderContactPhoneMobile

        Private contactPhoneWorkField As OMOrderCreateRequestOrderHeaderContactPhoneWork

        Private contactEmailField As OMOrderCreateRequestOrderHeaderContactEmail

        Private deliveryInstructionsField As OMOrderCreateRequestOrderHeaderDeliveryInstructions

        Private toBeDeliveredField As OMOrderCreateRequestOrderHeaderToBeDelivered

        Private extendedLeadTimeField As Boolean

        Private extendedLeadTimeFieldSpecified As Boolean

        Private deliveryContactNameField As String

        Private deliveryContactPhoneField As String

        Private tillOrderReferenceField As String

        Private requiredFulfilmentSlotField As OMOrderCreateRequestOrderHeaderRequiredFulfilmentSlot

        '''<remarks/>
        Public Property Source() As String
            Get
                Return Me.sourceField
            End Get
            Set(ByVal value As String)
                Me.sourceField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SourceOrderNumber() As String
            Get
                Return Me.sourceOrderNumberField
            End Get
            Set(ByVal value As String)
                Me.sourceOrderNumberField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingStoreCode() As OMOrderCreateRequestOrderHeaderSellingStoreCode
            Get
                Return Me.sellingStoreCodeField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderSellingStoreCode)
                Me.sellingStoreCodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingStoreOrderNumber() As OMOrderCreateRequestOrderHeaderSellingStoreOrderNumber
            Get
                Return Me.sellingStoreOrderNumberField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderSellingStoreOrderNumber)
                Me.sellingStoreOrderNumberField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingStoreTill() As String
            Get
                Return Me.sellingStoreTillField
            End Get
            Set(ByVal value As String)
                Me.sellingStoreTillField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingStoreTransaction() As String
            Get
                Return Me.sellingStoreTransactionField
            End Get
            Set(ByVal value As String)
                Me.sellingStoreTransactionField = value
            End Set
        End Property

        '''<remarks/>
        Public Property RequiredDeliveryDate() As OMOrderCreateRequestOrderHeaderRequiredDeliveryDate
            Get
                Return Me.requiredDeliveryDateField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderRequiredDeliveryDate)
                Me.requiredDeliveryDateField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryCharge() As OMOrderCreateRequestOrderHeaderDeliveryCharge
            Get
                Return Me.deliveryChargeField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderDeliveryCharge)
                Me.deliveryChargeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property TotalOrderValue() As OMOrderCreateRequestOrderHeaderTotalOrderValue
            Get
                Return Me.totalOrderValueField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderTotalOrderValue)
                Me.totalOrderValueField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SaleDate() As OMOrderCreateRequestOrderHeaderSaleDate
            Get
                Return Me.saleDateField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderSaleDate)
                Me.saleDateField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property CustomerAccountNo() As OMOrderCreateRequestOrderHeaderCustomerAccountNo
            Get
                Return Me.customerAccountNoField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderCustomerAccountNo)
                Me.customerAccountNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property CustomerName() As OMOrderCreateRequestOrderHeaderCustomerName
            Get
                Return Me.customerNameField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderCustomerName)
                Me.customerNameField = value
            End Set
        End Property

        '''<remarks/>
        Public Property CustomerAddressLine1() As OMOrderCreateRequestOrderHeaderCustomerAddressLine1
            Get
                Return Me.customerAddressLine1Field
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderCustomerAddressLine1)
                Me.customerAddressLine1Field = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property CustomerAddressLine2() As OMOrderCreateRequestOrderHeaderCustomerAddressLine2
            Get
                Return Me.customerAddressLine2Field
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderCustomerAddressLine2)
                Me.customerAddressLine2Field = value
            End Set
        End Property

        '''<remarks/>
        Public Property CustomerAddressTown() As OMOrderCreateRequestOrderHeaderCustomerAddressTown
            Get
                Return Me.customerAddressTownField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderCustomerAddressTown)
                Me.customerAddressTownField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property CustomerAddressLine4() As OMOrderCreateRequestOrderHeaderCustomerAddressLine4
            Get
                Return Me.customerAddressLine4Field
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderCustomerAddressLine4)
                Me.customerAddressLine4Field = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property CustomerPostcode() As OMOrderCreateRequestOrderHeaderCustomerPostcode
            Get
                Return Me.customerPostcodeField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderCustomerPostcode)
                Me.customerPostcodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryAddressLine1() As OMOrderCreateRequestOrderHeaderDeliveryAddressLine1
            Get
                Return Me.deliveryAddressLine1Field
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderDeliveryAddressLine1)
                Me.deliveryAddressLine1Field = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property DeliveryAddressLine2() As OMOrderCreateRequestOrderHeaderDeliveryAddressLine2
            Get
                Return Me.deliveryAddressLine2Field
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderDeliveryAddressLine2)
                Me.deliveryAddressLine2Field = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryAddressTown() As OMOrderCreateRequestOrderHeaderDeliveryAddressTown
            Get
                Return Me.deliveryAddressTownField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderDeliveryAddressTown)
                Me.deliveryAddressTownField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property DeliveryAddressLine4() As OMOrderCreateRequestOrderHeaderDeliveryAddressLine4
            Get
                Return Me.deliveryAddressLine4Field
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderDeliveryAddressLine4)
                Me.deliveryAddressLine4Field = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property DeliveryPostcode() As OMOrderCreateRequestOrderHeaderDeliveryPostcode
            Get
                Return Me.deliveryPostcodeField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderDeliveryPostcode)
                Me.deliveryPostcodeField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property ContactPhoneHome() As OMOrderCreateRequestOrderHeaderContactPhoneHome
            Get
                Return Me.contactPhoneHomeField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderContactPhoneHome)
                Me.contactPhoneHomeField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property ContactPhoneMobile() As OMOrderCreateRequestOrderHeaderContactPhoneMobile
            Get
                Return Me.contactPhoneMobileField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderContactPhoneMobile)
                Me.contactPhoneMobileField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property ContactPhoneWork() As OMOrderCreateRequestOrderHeaderContactPhoneWork
            Get
                Return Me.contactPhoneWorkField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderContactPhoneWork)
                Me.contactPhoneWorkField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property ContactEmail() As OMOrderCreateRequestOrderHeaderContactEmail
            Get
                Return Me.contactEmailField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderContactEmail)
                Me.contactEmailField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryInstructions() As OMOrderCreateRequestOrderHeaderDeliveryInstructions
            Get
                Return Me.deliveryInstructionsField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderDeliveryInstructions)
                Me.deliveryInstructionsField = value
            End Set
        End Property

        '''<remarks/>
        Public Property ToBeDelivered() As OMOrderCreateRequestOrderHeaderToBeDelivered
            Get
                Return Me.toBeDeliveredField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderToBeDelivered)
                Me.toBeDeliveredField = value
            End Set
        End Property

        '''<remarks/>
        Public Property ExtendedLeadTime() As Boolean
            Get
                Return Me.extendedLeadTimeField
            End Get
            Set(ByVal value As Boolean)
                Me.extendedLeadTimeField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlIgnoreAttribute()> _
        Public Property ExtendedLeadTimeSpecified() As Boolean
            Get
                Return Me.extendedLeadTimeFieldSpecified
            End Get
            Set(ByVal value As Boolean)
                Me.extendedLeadTimeFieldSpecified = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryContactName() As String
            Get
                Return Me.deliveryContactNameField
            End Get
            Set(ByVal value As String)
                Me.deliveryContactNameField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryContactPhone() As String
            Get
                Return Me.deliveryContactPhoneField
            End Get
            Set(ByVal value As String)
                Me.deliveryContactPhoneField = value
            End Set
        End Property

        '''<remarks/>
        Public Property TillOrderReference() As String
            Get
                Return Me.tillOrderReferenceField
            End Get
            Set(ByVal value As String)
                Me.tillOrderReferenceField = value
            End Set
        End Property

        '''<remarks/>
        Public Property RequiredFulfilmentSlot() As OMOrderCreateRequestOrderHeaderRequiredFulfilmentSlot
            Get
                Return Me.requiredFulfilmentSlotField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderRequiredFulfilmentSlot)
                Me.requiredFulfilmentSlotField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderSellingStoreCode

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderSellingStoreOrderNumber

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderRequiredDeliveryDate

        Private valueField As Date

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="date")> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderDeliveryCharge

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderTotalOrderValue

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderSaleDate

        Private valueField As Date

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="date")> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderCustomerAccountNo

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderCustomerName

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderCustomerAddressLine1

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderCustomerAddressLine2

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderCustomerAddressTown

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderCustomerAddressLine4

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderCustomerPostcode

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderDeliveryAddressLine1

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderDeliveryAddressLine2

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderDeliveryAddressTown

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderDeliveryAddressLine4

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderDeliveryPostcode

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderContactPhoneHome

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderContactPhoneMobile

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderContactPhoneWork

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderContactEmail

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderDeliveryInstructions

        Private instructionLineField() As OMOrderCreateRequestOrderHeaderDeliveryInstructionsInstructionLine

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute("InstructionLine")> _
        Public Property InstructionLine() As OMOrderCreateRequestOrderHeaderDeliveryInstructionsInstructionLine()
            Get
                Return Me.instructionLineField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderDeliveryInstructionsInstructionLine())
                Me.instructionLineField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderDeliveryInstructionsInstructionLine

        Private lineNoField As OMOrderCreateRequestOrderHeaderDeliveryInstructionsInstructionLineLineNo

        Private lineTextField As OMOrderCreateRequestOrderHeaderDeliveryInstructionsInstructionLineLineText

        '''<remarks/>
        Public Property LineNo() As OMOrderCreateRequestOrderHeaderDeliveryInstructionsInstructionLineLineNo
            Get
                Return Me.lineNoField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderDeliveryInstructionsInstructionLineLineNo)
                Me.lineNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property LineText() As OMOrderCreateRequestOrderHeaderDeliveryInstructionsInstructionLineLineText
            Get
                Return Me.lineTextField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderHeaderDeliveryInstructionsInstructionLineLineText)
                Me.lineTextField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderDeliveryInstructionsInstructionLineLineNo

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderDeliveryInstructionsInstructionLineLineText

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderHeaderToBeDelivered

        Private valueField As Boolean

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Boolean
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Boolean)
                Me.valueField = value
            End Set
        End Property

    End Class

     '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
   Partial Public Class OMOrderCreateRequestOrderHeaderRequiredFulfilmentSlot

        Private idField As String

        Private descriptionField As String

        Private startTimeField As Date

        Private endTimeField As Date

        '''<remarks/>
        Public Property Id() As String
            Get
                Return Me.idField
            End Get
            Set(ByVal value As String)
                Me.idField = value
            End Set
        End Property

        '''<remarks/>
        Public Property Description() As String
            Get
                Return Me.descriptionField
            End Get
            Set(ByVal value As String)
                Me.descriptionField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(DataType:="time")>  _
        Public Property StartTime() As Date
            Get
                Return Me.startTimeField
            End Get
            Set(ByVal value As Date)
                Me.startTimeField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(DataType:="time")>  _
        Public Property EndTime() As Date
            Get
                Return Me.endTimeField
            End Get
            Set(ByVal value As Date)
                Me.endTimeField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderLine

        Private sourceOrderLineNoField As String

        Private sellingStoreLineNoField As OMOrderCreateRequestOrderLineSellingStoreLineNo

        Private productCodeField As OMOrderCreateRequestOrderLineProductCode

        Private productDescriptionField As OMOrderCreateRequestOrderLineProductDescription

        Private totalOrderQuantityField As OMOrderCreateRequestOrderLineTotalOrderQuantity

        Private quantityTakenField As OMOrderCreateRequestOrderLineQuantityTaken

        Private uOMField As OMOrderCreateRequestOrderLineUOM

        Private lineValueField As OMOrderCreateRequestOrderLineLineValue

        Private deliveryChargeItemField As OMOrderCreateRequestOrderLineDeliveryChargeItem

        Private sellingPriceField As OMOrderCreateRequestOrderLineSellingPrice

         Private requiredFulfillerField As String

       '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(DataType:="integer")> _
        Public Property SourceOrderLineNo() As String
            Get
                Return Me.sourceOrderLineNoField
            End Get
            Set(ByVal value As String)
                Me.sourceOrderLineNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingStoreLineNo() As OMOrderCreateRequestOrderLineSellingStoreLineNo
            Get
                Return Me.sellingStoreLineNoField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderLineSellingStoreLineNo)
                Me.sellingStoreLineNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property ProductCode() As OMOrderCreateRequestOrderLineProductCode
            Get
                Return Me.productCodeField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderLineProductCode)
                Me.productCodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property ProductDescription() As OMOrderCreateRequestOrderLineProductDescription
            Get
                Return Me.productDescriptionField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderLineProductDescription)
                Me.productDescriptionField = value
            End Set
        End Property

        '''<remarks/>
        Public Property TotalOrderQuantity() As OMOrderCreateRequestOrderLineTotalOrderQuantity
            Get
                Return Me.totalOrderQuantityField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderLineTotalOrderQuantity)
                Me.totalOrderQuantityField = value
            End Set
        End Property

        '''<remarks/>
        Public Property QuantityTaken() As OMOrderCreateRequestOrderLineQuantityTaken
            Get
                Return Me.quantityTakenField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderLineQuantityTaken)
                Me.quantityTakenField = value
            End Set
        End Property

        '''<remarks/>
        Public Property UOM() As OMOrderCreateRequestOrderLineUOM
            Get
                Return Me.uOMField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderLineUOM)
                Me.uOMField = value
            End Set
        End Property

        '''<remarks/>
        Public Property LineValue() As OMOrderCreateRequestOrderLineLineValue
            Get
                Return Me.lineValueField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderLineLineValue)
                Me.lineValueField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryChargeItem() As OMOrderCreateRequestOrderLineDeliveryChargeItem
            Get
                Return Me.deliveryChargeItemField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderLineDeliveryChargeItem)
                Me.deliveryChargeItemField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingPrice() As OMOrderCreateRequestOrderLineSellingPrice
            Get
                Return Me.sellingPriceField
            End Get
            Set(ByVal value As OMOrderCreateRequestOrderLineSellingPrice)
                Me.sellingPriceField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(DataType:="integer")>  _
        Public Property RequiredFulfiller() As String
            Get
                Return Me.requiredFulfillerField
            End Get
            Set(ByVal value As String)
                Me.requiredFulfillerField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderLineSellingStoreLineNo

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderLineProductCode

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderLineProductDescription

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderLineTotalOrderQuantity

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderLineQuantityTaken

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderLineUOM

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderLineLineValue

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderLineDeliveryChargeItem

        Private valueField As Boolean

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Boolean
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Boolean)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateRequestOrderLineSellingPrice

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property

    End Class

End Namespace
