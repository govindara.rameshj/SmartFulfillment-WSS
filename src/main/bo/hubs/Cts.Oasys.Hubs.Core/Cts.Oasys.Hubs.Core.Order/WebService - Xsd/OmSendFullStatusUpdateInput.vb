﻿Imports System.Xml
Imports System.Xml.Serialization

Namespace WebService

    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True), _
      Serialization.XmlRootAttribute([Namespace]:="", IsNullable:=False)> _
    Partial Public Class OMSendFullStatusUpdateRequest
        Private dateTimeStampField As Date
        Private oMOrderNumberField As String
        Private sellingStoreCodeField As String
        Private sellingStoreOrderNumberField As String
        Private tillOrderReferenceField As String

        Public Property DateTimeStamp() As Date
            Get
                Return Me.dateTimeStampField
            End Get
            Set(ByVal value As Date)
                Me.dateTimeStampField = value
            End Set
        End Property
        Public Property OMOrderNumber() As String
            Get
                Return Me.oMOrderNumberField
            End Get
            Set(ByVal value As String)
                Me.oMOrderNumberField = value
            End Set
        End Property
        <Serialization.XmlElementAttribute(DataType:="integer")> _
        Public Property SellingStoreCode() As String
            Get
                Return Me.sellingStoreCodeField
            End Get
            Set(ByVal value As String)
                Me.sellingStoreCodeField = value
            End Set
        End Property
        <Serialization.XmlElementAttribute(DataType:="integer")> _
        Public Property SellingStoreOrderNumber() As String
            Get
                Return Me.sellingStoreOrderNumberField
            End Get
            Set(ByVal value As String)
                Me.sellingStoreOrderNumberField = value
            End Set
        End Property
        Public Property TillOrderReference() As String
            Get
                Return Me.tillOrderReferenceField
            End Get
                Set(ByVal value As String)
                Me.tillOrderReferenceField = value
            End Set
        End Property
    End Class

End Namespace