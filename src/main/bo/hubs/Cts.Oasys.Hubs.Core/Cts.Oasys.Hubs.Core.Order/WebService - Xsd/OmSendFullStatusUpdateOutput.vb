﻿Imports System.Xml
Imports System.Xml.Serialization

Namespace WebService

    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True), _
       Serialization.XmlRootAttribute([Namespace]:="", IsNullable:=False)> _
    Partial Public Class OMSendFullStatusUpdateResponse

        Private dateTimeStampField As OMSendFullStatusUpdateResponseDateTimeStamp

        Private successFlagField As Boolean

        Private orderStatusField As OMSendFullStatusUpdateResponseOrderStatus

        '''<remarks/>
        Public Property DateTimeStamp() As OMSendFullStatusUpdateResponseDateTimeStamp
            Get
                Return Me.dateTimeStampField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseDateTimeStamp)
                Me.dateTimeStampField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SuccessFlag() As Boolean
            Get
                Return Me.successFlagField
            End Get
            Set(ByVal value As Boolean)
                Me.successFlagField = value
            End Set
        End Property

        '''<remarks/>
        Public Property OrderStatus() As OMSendFullStatusUpdateResponseOrderStatus
            Get
                Return Me.orderStatusField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatus)
                Me.orderStatusField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseDateTimeStamp

        Private validationStatusField As String

        Private valueField As Date

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatus

        Private itemField As Object

        '''<remarks/>
        <Serialization.XmlElementAttribute("ErrorDetail", GetType(OMSendFullStatusUpdateResponseOrderStatusErrorDetail)), _
           Serialization.XmlElementAttribute("OrderDetail", GetType(OMSendFullStatusUpdateResponseOrderStatusOrderDetail))> _
        Public Property Item() As Object
            Get
                Return Me.itemField
            End Get
            Set(ByVal value As Object)
                Me.itemField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusErrorDetail

        Private oMOrderNumberField As OMSendFullStatusUpdateResponseOrderStatusErrorDetailOMOrderNumber

        Private sellingStoreCodeField As OMSendFullStatusUpdateResponseOrderStatusErrorDetailSellingStoreCode

        Private sellingStoreOrderNumberField As OMSendFullStatusUpdateResponseOrderStatusErrorDetailSellingStoreOrderNumber

        Private tillOrderReferenceField As OMSendFullStatusUpdateResponseOrderStatusErrorDetailTillOrderReference

        '''<remarks/>
        Public Property OMOrderNumber() As OMSendFullStatusUpdateResponseOrderStatusErrorDetailOMOrderNumber
            Get
                Return Me.oMOrderNumberField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusErrorDetailOMOrderNumber)
                Me.oMOrderNumberField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingStoreCode() As OMSendFullStatusUpdateResponseOrderStatusErrorDetailSellingStoreCode
            Get
                Return Me.sellingStoreCodeField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusErrorDetailSellingStoreCode)
                Me.sellingStoreCodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingStoreOrderNumber() As OMSendFullStatusUpdateResponseOrderStatusErrorDetailSellingStoreOrderNumber
            Get
                Return Me.sellingStoreOrderNumberField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusErrorDetailSellingStoreOrderNumber)
                Me.sellingStoreOrderNumberField = value
            End Set
        End Property

        '''<remarks/>
        Public Property TillOrderReference() As OMSendFullStatusUpdateResponseOrderStatusErrorDetailTillOrderReference
            Get
                Return Me.tillOrderReferenceField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusErrorDetailTillOrderReference)
                Me.tillOrderReferenceField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusErrorDetailOMOrderNumber

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusErrorDetailSellingStoreCode

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusErrorDetailSellingStoreOrderNumber

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
     Partial Public Class OMSendFullStatusUpdateResponseOrderStatusErrorDetailTillOrderReference

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetail

        Private orderHeaderField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeader

        Private orderLinesField() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLine

        '''<remarks/>
        Public Property OrderHeader() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeader
            Get
                Return Me.orderHeaderField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeader)
                Me.orderHeaderField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlArrayItemAttribute("OrderLine", IsNullable:=False)> _
        Public Property OrderLines() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLine()
            Get
                Return Me.orderLinesField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLine())
                Me.orderLinesField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeader

        Private sellingStoreCodeField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderSellingStoreCode

        Private sellingStoreOrderNumberField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderSellingStoreOrderNumber

        Private requiredDeliveryDateField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderRequiredDeliveryDate

        Private deliveryChargeField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryCharge

        Private totalOrderValueField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderTotalOrderValue

        Private saleDateField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderSaleDate

        Private oMOrderNumberField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderOMOrderNumber

        Private orderStatusField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderOrderStatus

        Private customerAccountNoField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAccountNo

        Private customerNameField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerName

        Private customerAddressLine1Field As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAddressLine1

        Private customerAddressLine2Field As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAddressLine2

        Private customerAddressTownField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAddressTown

        Private customerAddressLine4Field As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAddressLine4

        Private customerPostcodeField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerPostcode

        Private deliveryAddressLine1Field As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryAddressLine1

        Private deliveryAddressLine2Field As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryAddressLine2

        Private deliveryAddressTownField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryAddressTown

        Private deliveryAddressLine4Field As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryAddressLine4

        Private deliveryPostcodeField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryPostcode

        Private contactPhoneHomeField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderContactPhoneHome

        Private contactPhoneWorkField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderContactPhoneWork

        Private contactPhoneMobileField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderContactPhoneMobile

        Private contactEmailField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderContactEmail

        Private deliveryInstructionsField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryInstructions

        Private toBeDeliveredField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderToBeDelivered

        Private tillOrderReferenceField As String

        Private requiredFulfilmentSlotField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderRequiredFulfilmentSlot

        '''<remarks/>
        Public Property SellingStoreCode() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderSellingStoreCode
            Get
                Return Me.sellingStoreCodeField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderSellingStoreCode)
                Me.sellingStoreCodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingStoreOrderNumber() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderSellingStoreOrderNumber
            Get
                Return Me.sellingStoreOrderNumberField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderSellingStoreOrderNumber)
                Me.sellingStoreOrderNumberField = value
            End Set
        End Property

        '''<remarks/>
        Public Property RequiredDeliveryDate() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderRequiredDeliveryDate
            Get
                Return Me.requiredDeliveryDateField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderRequiredDeliveryDate)
                Me.requiredDeliveryDateField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryCharge() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryCharge
            Get
                Return Me.deliveryChargeField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryCharge)
                Me.deliveryChargeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property TotalOrderValue() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderTotalOrderValue
            Get
                Return Me.totalOrderValueField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderTotalOrderValue)
                Me.totalOrderValueField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SaleDate() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderSaleDate
            Get
                Return Me.saleDateField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderSaleDate)
                Me.saleDateField = value
            End Set
        End Property

        '''<remarks/>
        Public Property OMOrderNumber() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderOMOrderNumber
            Get
                Return Me.oMOrderNumberField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderOMOrderNumber)
                Me.oMOrderNumberField = value
            End Set
        End Property

        '''<remarks/>
        Public Property OrderStatus() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderOrderStatus
            Get
                Return Me.orderStatusField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderOrderStatus)
                Me.orderStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property CustomerAccountNo() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAccountNo
            Get
                Return Me.customerAccountNoField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAccountNo)
                Me.customerAccountNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property CustomerName() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerName
            Get
                Return Me.customerNameField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerName)
                Me.customerNameField = value
            End Set
        End Property

        '''<remarks/>
        Public Property CustomerAddressLine1() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAddressLine1
            Get
                Return Me.customerAddressLine1Field
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAddressLine1)
                Me.customerAddressLine1Field = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property CustomerAddressLine2() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAddressLine2
            Get
                Return Me.customerAddressLine2Field
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAddressLine2)
                Me.customerAddressLine2Field = value
            End Set
        End Property

        '''<remarks/>
        Public Property CustomerAddressTown() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAddressTown
            Get
                Return Me.customerAddressTownField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAddressTown)
                Me.customerAddressTownField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property CustomerAddressLine4() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAddressLine4
            Get
                Return Me.customerAddressLine4Field
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAddressLine4)
                Me.customerAddressLine4Field = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property CustomerPostcode() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerPostcode
            Get
                Return Me.customerPostcodeField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerPostcode)
                Me.customerPostcodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryAddressLine1() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryAddressLine1
            Get
                Return Me.deliveryAddressLine1Field
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryAddressLine1)
                Me.deliveryAddressLine1Field = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property DeliveryAddressLine2() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryAddressLine2
            Get
                Return Me.deliveryAddressLine2Field
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryAddressLine2)
                Me.deliveryAddressLine2Field = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryAddressTown() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryAddressTown
            Get
                Return Me.deliveryAddressTownField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryAddressTown)
                Me.deliveryAddressTownField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property DeliveryAddressLine4() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryAddressLine4
            Get
                Return Me.deliveryAddressLine4Field
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryAddressLine4)
                Me.deliveryAddressLine4Field = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property DeliveryPostcode() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryPostcode
            Get
                Return Me.deliveryPostcodeField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryPostcode)
                Me.deliveryPostcodeField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property ContactPhoneHome() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderContactPhoneHome
            Get
                Return Me.contactPhoneHomeField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderContactPhoneHome)
                Me.contactPhoneHomeField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property ContactPhoneWork() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderContactPhoneWork
            Get
                Return Me.contactPhoneWorkField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderContactPhoneWork)
                Me.contactPhoneWorkField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property ContactPhoneMobile() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderContactPhoneMobile
            Get
                Return Me.contactPhoneMobileField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderContactPhoneMobile)
                Me.contactPhoneMobileField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property ContactEmail() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderContactEmail
            Get
                Return Me.contactEmailField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderContactEmail)
                Me.contactEmailField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryInstructions() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryInstructions
            Get
                Return Me.deliveryInstructionsField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryInstructions)
                Me.deliveryInstructionsField = value
            End Set
        End Property

        '''<remarks/>
        Public Property ToBeDelivered() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderToBeDelivered
            Get
                Return Me.toBeDeliveredField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderToBeDelivered)
                Me.toBeDeliveredField = value
            End Set
        End Property

        '''<remarks/>
        Public Property TillOrderReference() As String
            Get
                Return Me.tillOrderReferenceField
            End Get
            Set(ByVal value As String)
                Me.tillOrderReferenceField = value
            End Set
        End Property

        '''<remarks/>
        Public Property RequiredFulfilmentSlot() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderRequiredFulfilmentSlot
            Get
                Return Me.requiredFulfilmentSlotField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderRequiredFulfilmentSlot)
                Me.requiredFulfilmentSlotField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderSellingStoreCode

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderSellingStoreOrderNumber

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderRequiredDeliveryDate

        Private valueField As Date

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="date")> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryCharge

        Private valueField As Decimal

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderTotalOrderValue

        Private valueField As Decimal

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderSaleDate

        Private valueField As Date

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="date")> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderOMOrderNumber

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderOrderStatus

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAccountNo

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerName

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAddressLine1

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAddressLine2

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAddressTown

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerAddressLine4

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderCustomerPostcode

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryAddressLine1

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryAddressLine2

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryAddressTown

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryAddressLine4

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryPostcode

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderContactPhoneHome

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderContactPhoneWork

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderContactPhoneMobile

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderContactEmail

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryInstructions

        Private instructionLineField() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryInstructionsInstructionLine

        '''<remarks/>
        <Serialization.XmlElementAttribute("InstructionLine")> _
        Public Property InstructionLine() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryInstructionsInstructionLine()
            Get
                Return Me.instructionLineField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryInstructionsInstructionLine())
                Me.instructionLineField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryInstructionsInstructionLine

        Private lineNoField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryInstructionsInstructionLineLineNo

        Private lineTextField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryInstructionsInstructionLineLineText

        '''<remarks/>
        Public Property LineNo() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryInstructionsInstructionLineLineNo
            Get
                Return Me.lineNoField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryInstructionsInstructionLineLineNo)
                Me.lineNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property LineText() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryInstructionsInstructionLineLineText
            Get
                Return Me.lineTextField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryInstructionsInstructionLineLineText)
                Me.lineTextField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryInstructionsInstructionLineLineNo

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryInstructionsInstructionLineLineText

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderToBeDelivered

        Private valueField As Boolean

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As Boolean
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Boolean)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderRequiredFulfilmentSlot

        Private idField As String

        Private descriptionField As String

        Private startTimeField As Date

        Private endTimeField As Date

        '''<remarks/>
        Public Property Id() As String
            Get
                Return Me.idField
            End Get
            Set(ByVal value As String)
                Me.idField = value
            End Set
        End Property

        '''<remarks/>
        Public Property Description() As String
            Get
                Return Me.descriptionField
            End Get
            Set(ByVal value As String)
                Me.descriptionField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlElementAttribute(DataType:="time")>  _
        Public Property StartTime() As Date
            Get
                Return Me.startTimeField
            End Get
            Set(ByVal value As Date)
                Me.startTimeField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlElementAttribute(DataType:="time")>  _
        Public Property EndTime() As Date
            Get
                Return Me.endTimeField
            End Get
            Set(ByVal value As Date)
                Me.endTimeField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLine

        Private sellingStoreLineNoField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineSellingStoreLineNo

        Private oMOrderLineNoField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineOMOrderLineNo

        Private lineStatusField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineLineStatus

        Private productCodeField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineProductCode

        Private productDescriptionField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineProductDescription

        Private totalOrderQuantityField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineTotalOrderQuantity

        Private quantityTakenField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineQuantityTaken

        Private uOMField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineUOM

        Private lineValueField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineLineValue

        Private deliveryChargeItemField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineDeliveryChargeItem

        Private sellingPriceField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineSellingPrice

        Private fulfilmentSiteField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineFulfilmentSite

        Private fulfilmentSiteOrderLineNumberField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineFulfilmentSiteOrderLineNumber

        Private fulfilmentSiteIBTOutNumberField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineFulfilmentSiteIBTOutNumber

        Private sellingStoreIBTInNumberField As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineSellingStoreIBTInNumber

        Private cancellableFlagField As Boolean

        Private cancellationInstructionsField As String

        '''<remarks/>
        Public Property SellingStoreLineNo() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineSellingStoreLineNo
            Get
                Return Me.sellingStoreLineNoField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineSellingStoreLineNo)
                Me.sellingStoreLineNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property OMOrderLineNo() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineOMOrderLineNo
            Get
                Return Me.oMOrderLineNoField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineOMOrderLineNo)
                Me.oMOrderLineNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property LineStatus() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineLineStatus
            Get
                Return Me.lineStatusField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineLineStatus)
                Me.lineStatusField = value
            End Set
        End Property

        '''<remarks/>
        Public Property ProductCode() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineProductCode
            Get
                Return Me.productCodeField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineProductCode)
                Me.productCodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property ProductDescription() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineProductDescription
            Get
                Return Me.productDescriptionField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineProductDescription)
                Me.productDescriptionField = value
            End Set
        End Property

        '''<remarks/>
        Public Property TotalOrderQuantity() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineTotalOrderQuantity
            Get
                Return Me.totalOrderQuantityField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineTotalOrderQuantity)
                Me.totalOrderQuantityField = value
            End Set
        End Property

        '''<remarks/>
        Public Property QuantityTaken() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineQuantityTaken
            Get
                Return Me.quantityTakenField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineQuantityTaken)
                Me.quantityTakenField = value
            End Set
        End Property

        '''<remarks/>
        Public Property UOM() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineUOM
            Get
                Return Me.uOMField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineUOM)
                Me.uOMField = value
            End Set
        End Property

        '''<remarks/>
        Public Property LineValue() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineLineValue
            Get
                Return Me.lineValueField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineLineValue)
                Me.lineValueField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryChargeItem() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineDeliveryChargeItem
            Get
                Return Me.deliveryChargeItemField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineDeliveryChargeItem)
                Me.deliveryChargeItemField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingPrice() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineSellingPrice
            Get
                Return Me.sellingPriceField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineSellingPrice)
                Me.sellingPriceField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property FulfilmentSite() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineFulfilmentSite
            Get
                Return Me.fulfilmentSiteField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineFulfilmentSite)
                Me.fulfilmentSiteField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property FulfilmentSiteOrderLineNumber() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineFulfilmentSiteOrderLineNumber
            Get
                Return Me.fulfilmentSiteOrderLineNumberField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineFulfilmentSiteOrderLineNumber)
                Me.fulfilmentSiteOrderLineNumberField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property FulfilmentSiteIBTOutNumber() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineFulfilmentSiteIBTOutNumber
            Get
                Return Me.fulfilmentSiteIBTOutNumberField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineFulfilmentSiteIBTOutNumber)
                Me.fulfilmentSiteIBTOutNumberField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property SellingStoreIBTInNumber() As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineSellingStoreIBTInNumber
            Get
                Return Me.sellingStoreIBTInNumberField
            End Get
            Set(ByVal value As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineSellingStoreIBTInNumber)
                Me.sellingStoreIBTInNumberField = value
            End Set
        End Property

        '''<remarks/>
        Public Property CancellableFlag() As Boolean
            Get
                Return Me.cancellableFlagField
            End Get
            Set(ByVal value As Boolean)
                Me.cancellableFlagField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property CancellationInstructions() As String
            Get
                Return Me.cancellationInstructionsField
            End Get
            Set(ByVal value As String)
                Me.cancellationInstructionsField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineSellingStoreLineNo

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineOMOrderLineNo

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineLineStatus

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineProductCode

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineProductDescription

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineTotalOrderQuantity

        Private valueField As Decimal

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineQuantityTaken

        Private valueField As Decimal

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineUOM

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineLineValue

        Private valueField As Decimal

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineDeliveryChargeItem

        Private valueField As Boolean

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As Boolean
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Boolean)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineSellingPrice

        Private valueField As Decimal

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineFulfilmentSite

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineFulfilmentSiteOrderLineNumber

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineFulfilmentSiteIBTOutNumber

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
       Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLineSellingStoreIBTInNumber

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

End Namespace