﻿Imports Cts.Oasys.Hubs.Core.Order.Qod

<Assembly: Runtime.CompilerServices.InternalsVisibleTo("Cts.Oasys.Hubs.Core.Order.UnitTest, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                                                                "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                                                                "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                                                                "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                                                                "54e0a4a4")> 
Namespace WebService

    Public MustInherit Class BaseRequest
        Inherits BaseServiceArgument

        Public Overridable Sub SetFields()

        End Sub

    End Class

#Region "   Cts Fulfilment"

    Partial Public Class CTSFulfilmentRequest
        Inherits WebService.BaseRequest

        Public Sub New()
            Me.DateTimeStamp = New CTSFulfilmentRequestDateTimeStamp
            Me.TargetFulfilmentSite = New CTSFulfilmentRequestTargetFulfilmentSite

            Me.OrderHeader = New CTSFulfilmentRequestOrderHeader
            Me.OrderHeader.SellingStoreCode = New CTSFulfilmentRequestOrderHeaderSellingStoreCode
            Me.OrderHeader.SellingStoreOrderNumber = New CTSFulfilmentRequestOrderHeaderSellingStoreOrderNumber
            Me.OrderHeader.RequiredDeliveryDate = New CTSFulfilmentRequestOrderHeaderRequiredDeliveryDate
            Me.OrderHeader.DeliveryCharge = New CTSFulfilmentRequestOrderHeaderDeliveryCharge
            Me.OrderHeader.TotalOrderValue = New CTSFulfilmentRequestOrderHeaderTotalOrderValue
            Me.OrderHeader.SaleDate = New CTSFulfilmentRequestOrderHeaderSaleDate
            Me.OrderHeader.OMOrderNumber = New CTSFulfilmentRequestOrderHeaderOMOrderNumber
            Me.OrderHeader.OrderStatus = New CTSFulfilmentRequestOrderHeaderOrderStatus
            Me.OrderHeader.CustomerAccountNo = New CTSFulfilmentRequestOrderHeaderCustomerAccountNo
            Me.OrderHeader.CustomerName = New CTSFulfilmentRequestOrderHeaderCustomerName
            Me.OrderHeader.CustomerAddressLine1 = New CTSFulfilmentRequestOrderHeaderCustomerAddressLine1
            Me.OrderHeader.CustomerAddressLine2 = New CTSFulfilmentRequestOrderHeaderCustomerAddressLine2
            Me.OrderHeader.CustomerAddressTown = New CTSFulfilmentRequestOrderHeaderCustomerAddressTown
            Me.OrderHeader.CustomerAddressLine4 = New CTSFulfilmentRequestOrderHeaderCustomerAddressLine4
            Me.OrderHeader.CustomerPostcode = New CTSFulfilmentRequestOrderHeaderCustomerPostcode
            Me.OrderHeader.DeliveryAddressLine1 = New CTSFulfilmentRequestOrderHeaderDeliveryAddressLine1
            Me.OrderHeader.DeliveryAddressLine2 = New CTSFulfilmentRequestOrderHeaderDeliveryAddressLine2
            Me.OrderHeader.DeliveryAddressTown = New CTSFulfilmentRequestOrderHeaderDeliveryAddressTown
            Me.OrderHeader.DeliveryAddressLine4 = New CTSFulfilmentRequestOrderHeaderDeliveryAddressLine4
            Me.OrderHeader.DeliveryPostcode = New CTSFulfilmentRequestOrderHeaderDeliveryPostcode
            Me.OrderHeader.ContactPhoneHome = New CTSFulfilmentRequestOrderHeaderContactPhoneHome
            Me.OrderHeader.ContactPhoneMobile = New CTSFulfilmentRequestOrderHeaderContactPhoneMobile
            Me.OrderHeader.ContactPhoneWork = New CTSFulfilmentRequestOrderHeaderContactPhoneWork
            Me.OrderHeader.ContactEmail = New CTSFulfilmentRequestOrderHeaderContactEmail
            Me.OrderHeader.ToBeDelivered = New CTSFulfilmentRequestOrderHeaderToBeDelivered
            Me.OrderHeader.DeliveryInstructions = New CTSFulfilmentRequestOrderHeaderDeliveryInstructions
        End Sub

    End Class

    Partial Public Class CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLine

        Public Sub New()
            Me.LineNo = New CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLineLineNo
            Me.LineText = New CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLineLineText
        End Sub

    End Class

    Partial Public Class CTSFulfilmentRequestOrderLine

        Public Sub New()
            Me.DeliveryChargeItem = New CTSFulfilmentRequestOrderLineDeliveryChargeItem
            Me.FulfilmentSite = New CTSFulfilmentRequestOrderLineFulfilmentSite
            Me.LineStatus = New CTSFulfilmentRequestOrderLineLineStatus
            Me.LineValue = New CTSFulfilmentRequestOrderLineLineValue
            Me.OMOrderLineNo = New CTSFulfilmentRequestOrderLineOMOrderLineNo
            Me.ProductCode = New CTSFulfilmentRequestOrderLineProductCode
            Me.ProductDescription = New CTSFulfilmentRequestOrderLineProductDescription
            Me.QuantityTaken = New CTSFulfilmentRequestOrderLineQuantityTaken
            Me.SellingStoreLineNo = New CTSFulfilmentRequestOrderLineSellingStoreLineNo
            Me.TotalOrderQuantity = New CTSFulfilmentRequestOrderLineTotalOrderQuantity
            Me.UOM = New CTSFulfilmentRequestOrderLineUOM
            Me.SellingPrice = New CTSFulfilmentRequestOrderLineSellingPrice
        End Sub

    End Class

#End Region

#Region "   Cts"

    Partial Public Class CTSStatusNotificationRequest
        Inherits BaseRequest
    End Class

    Partial Public Class CTSUpdateRefundRequest
        Inherits BaseRequest
    End Class

    Partial Class CTSQODCreateRequest
        Inherits BaseRequest
    End Class

    Partial Class CTSEnableRefundCreateRequest
        Inherits BaseRequest
    End Class

    Partial Public Class CTSEnableStatusNotificationRequest
        Inherits BaseRequest
    End Class

#End Region

#Region "   Om Order Create"

    Partial Public Class OMOrderCreateRequest
        Inherits BaseRequest

        Public Sub New()
            Me.DateTimeStamp = New OMOrderCreateRequestDateTimeStamp
            Me.OrderHeader = New OMOrderCreateRequestOrderHeader
            Me.OrderHeader.SellingStoreCode = New OMOrderCreateRequestOrderHeaderSellingStoreCode
            Me.OrderHeader.SellingStoreOrderNumber = New OMOrderCreateRequestOrderHeaderSellingStoreOrderNumber
            Me.OrderHeader.RequiredDeliveryDate = New OMOrderCreateRequestOrderHeaderRequiredDeliveryDate
            Me.OrderHeader.DeliveryCharge = New OMOrderCreateRequestOrderHeaderDeliveryCharge
            Me.OrderHeader.TotalOrderValue = New OMOrderCreateRequestOrderHeaderTotalOrderValue
            Me.OrderHeader.SaleDate = New OMOrderCreateRequestOrderHeaderSaleDate
            Me.OrderHeader.CustomerAccountNo = New OMOrderCreateRequestOrderHeaderCustomerAccountNo
            Me.OrderHeader.CustomerName = New OMOrderCreateRequestOrderHeaderCustomerName
            Me.OrderHeader.CustomerAddressLine1 = New OMOrderCreateRequestOrderHeaderCustomerAddressLine1
            Me.OrderHeader.CustomerAddressLine2 = New OMOrderCreateRequestOrderHeaderCustomerAddressLine2
            Me.OrderHeader.CustomerAddressTown = New OMOrderCreateRequestOrderHeaderCustomerAddressTown
            Me.OrderHeader.CustomerAddressLine4 = New OMOrderCreateRequestOrderHeaderCustomerAddressLine4
            Me.OrderHeader.CustomerPostcode = New OMOrderCreateRequestOrderHeaderCustomerPostcode
            Me.OrderHeader.DeliveryAddressLine1 = New OMOrderCreateRequestOrderHeaderDeliveryAddressLine1
            Me.OrderHeader.DeliveryAddressLine2 = New OMOrderCreateRequestOrderHeaderDeliveryAddressLine2
            Me.OrderHeader.DeliveryAddressTown = New OMOrderCreateRequestOrderHeaderDeliveryAddressTown
            Me.OrderHeader.DeliveryAddressLine4 = New OMOrderCreateRequestOrderHeaderDeliveryAddressLine4
            Me.OrderHeader.DeliveryPostcode = New OMOrderCreateRequestOrderHeaderDeliveryPostcode
            Me.OrderHeader.ContactPhoneHome = New OMOrderCreateRequestOrderHeaderContactPhoneHome
            Me.OrderHeader.ContactPhoneMobile = New OMOrderCreateRequestOrderHeaderContactPhoneMobile
            Me.OrderHeader.ContactPhoneWork = New OMOrderCreateRequestOrderHeaderContactPhoneWork
            Me.OrderHeader.ContactEmail = New OMOrderCreateRequestOrderHeaderContactEmail
            Me.OrderHeader.ToBeDelivered = New OMOrderCreateRequestOrderHeaderToBeDelivered
            Me.OrderHeader.DeliveryInstructions = New OMOrderCreateRequestOrderHeaderDeliveryInstructions
        End Sub

        Overrides Sub SetFieldsFromQod(ByVal QodHeader As QodHeader)

            Me.DateTimeStamp.Value = Now

            If QodHeader.IsOVCOrder() Then
                Me.OrderHeader.TillOrderReference = QodHeader.SourceOrderNumber
            Else
                Me.OrderHeader.Source = QodHeader.Source                              'if NOT venda sale then will be nothing 
                Me.OrderHeader.SourceOrderNumber = QodHeader.SourceOrderNumber        'if NOT venda sale then will be nothing 
            End If

            Me.OrderHeader.SellingStoreCode.Value = QodHeader.SellingStoreId.ToString
            Me.OrderHeader.SellingStoreOrderNumber.Value = QodHeader.SellingStoreOrderId.ToString

            If QodHeader.Source IsNot Nothing AndAlso QodHeader.Source = "WO" Then
                'venda sale
                Me.OrderHeader.SellingStoreTill = QodHeader.TranTill
                Me.OrderHeader.SellingStoreTransaction = QodHeader.TranNumber
            End If

            If QodHeader.DateDelivery.HasValue Then Me.OrderHeader.RequiredDeliveryDate.Value = QodHeader.DateDelivery.Value
            Me.OrderHeader.DeliveryCharge.Value = QodHeader.DeliveryCharge
            Me.OrderHeader.TotalOrderValue.Value = QodHeader.Lines.OrderValue
            Me.OrderHeader.SaleDate.Value = QodHeader.DateOrder

            If QodHeader.CustomerAccountNo Is Nothing Then QodHeader.CustomerAccountNo = String.Empty
            Me.OrderHeader.CustomerAccountNo.Value = QodHeader.CustomerAccountNo    'if NOT venda sale then will be a blank string

            Me.OrderHeader.CustomerName.Value = QodHeader.CustomerName
            Me.OrderHeader.CustomerAddressLine1.Value = QodHeader.CustomerAddress1
            Me.OrderHeader.CustomerAddressLine2.Value = QodHeader.CustomerAddress2
            Me.OrderHeader.CustomerAddressTown.Value = QodHeader.CustomerAddress3
            Me.OrderHeader.CustomerAddressLine4.Value = QodHeader.CustomerAddress4
            Me.OrderHeader.CustomerPostcode.Value = QodHeader.CustomerPostcode
            Me.OrderHeader.DeliveryAddressLine1.Value = QodHeader.DeliveryAddress1
            Me.OrderHeader.DeliveryAddressLine2.Value = QodHeader.DeliveryAddress2
            If QodHeader.DeliveryAddress3.Length > 0 Then Me.OrderHeader.DeliveryAddressTown.Value = QodHeader.DeliveryAddress3.Split(","c)(0)
            Me.OrderHeader.DeliveryAddressLine4.Value = QodHeader.DeliveryAddress4
            Me.OrderHeader.DeliveryPostcode.Value = QodHeader.DeliveryPostCode
            Me.OrderHeader.ContactPhoneHome.Value = QodHeader.PhoneNumber
            Me.OrderHeader.ContactPhoneMobile.Value = QodHeader.PhoneNumberMobile
            Me.OrderHeader.ContactPhoneWork.Value = QodHeader.PhoneNumberWork
            Me.OrderHeader.ContactEmail.Value = QodHeader.CustomerEmail

            Dim instructions As QodTextCollection = QodHeader.Texts.GetTexts(QodTextType.Type.DeliveryInstruction)
            Dim limit As Integer = instructions.Count
            If limit > 0 Then
                ReDim Me.OrderHeader.DeliveryInstructions.InstructionLine(limit - 1)
                For index As Integer = 0 To limit - 1
                    Me.OrderHeader.DeliveryInstructions.InstructionLine(index) = New OMOrderCreateRequestOrderHeaderDeliveryInstructionsInstructionLine
                    Me.OrderHeader.DeliveryInstructions.InstructionLine(index).LineNo.Value = instructions(index).Number
                    Me.OrderHeader.DeliveryInstructions.InstructionLine(index).LineText.Value = instructions(index).Text
                Next
            End If

            Me.OrderHeader.ToBeDelivered.Value = QodHeader.IsForDelivery
            Me.OrderHeader.ExtendedLeadTime = QodHeader.ExtendedLeadTime
            If QodHeader.Source IsNot Nothing AndAlso QodHeader.Source = "WO" Then
                'venda sale
                Me.OrderHeader.ExtendedLeadTimeSpecified = True       'results in the XML tag <ExtendedLeadTime> being created
            End If
            Me.OrderHeader.DeliveryContactName = QodHeader.DeliveryContactName          'if NOT venda sale then will be nothing 
            Me.OrderHeader.DeliveryContactPhone = QodHeader.DeliveryContactPhone        'if NOT venda sale then will be nothing 

            If QodHeader.RequiredDeliverySlotID IsNot Nothing Then
                Me.OrderHeader.RequiredFulfilmentSlot = New OMOrderCreateRequestOrderHeaderRequiredFulfilmentSlot

                Me.OrderHeader.RequiredFulfilmentSlot.Id = QodHeader.RequiredDeliverySlotID

                If QodHeader.RequiredDeliverySlotDescription IsNot Nothing Then
                    Me.OrderHeader.RequiredFulfilmentSlot.Description = QodHeader.RequiredDeliverySlotDescription
                End If

                If QodHeader.RequiredDeliverySlotStartTime IsNot Nothing Then
                    Me.OrderHeader.RequiredFulfilmentSlot.StartTime = New Date(QodHeader.RequiredDeliverySlotStartTime.Value.Ticks)
                End If

                If QodHeader.RequiredDeliverySlotEndTime IsNot Nothing Then
                    Me.OrderHeader.RequiredFulfilmentSlot.EndTime = New Date(QodHeader.RequiredDeliverySlotEndTime.Value.Ticks)
                End If
            End If

            If QodHeader.Lines.Count > 0 Then
                Dim Index As Integer = 0
                For Each QodLine As QodLine In QodHeader.Lines
                    ReDim Preserve OrderLines(Index)

                    OrderLines(Index) = New WebService.OMOrderCreateRequestOrderLine

                    With OrderLines(Index)

                        If QodHeader.Source IsNot Nothing AndAlso QodHeader.Source = "WO" Then
                            'venda sale

                            'for now setting the venda sales order line no to the selling store order line no
                            'this is because we are not storing "venda sales order line no" in the database!
                            .SourceOrderLineNo = QodLine.SourceOrderLineNo.ToString
                        End If

                        .SellingStoreLineNo.Value = QodLine.Number
                        .ProductCode.Value = QodLine.SkuNumber
                        .ProductDescription.Value = QodLine.SkuDescription
                        .TotalOrderQuantity.Value = QodLine.QtyOrdered

                        If QodLine.QtyRefunded < 0 Then
                            Dim QodRefundCollection As QodRefundCollection = QodHeader.Refunds.FindAll(CInt(QodLine.Number))

                            .QuantityTaken.Value = (QodLine.QtyTaken + QodRefundCollection.TotalReturned).ToString
                        Else
                            .QuantityTaken.Value = QodLine.QtyTaken.ToString
                        End If

                        .UOM.Value = QodLine.SkuUnitMeasure
                        .LineValue.Value = QodLine.OrderValue
                        .DeliveryChargeItem.Value = QodLine.IsDeliveryChargeItem
                        .SellingPrice.Value = QodLine.Price

                        If QodLine.RequiredFulfiller.HasValue AndAlso QodLine.RequiredFulfiller.Value > 0 Then
                            .RequiredFulfiller = QodLine.RequiredFulfiller.Value.ToString
                        End If

                    End With
                    Index += 1
                Next
            End If

        End Sub

    End Class

    Partial Public Class OMOrderCreateRequestOrderHeaderDeliveryInstructionsInstructionLine

        Public Sub New()
            Me.LineNo = New OMOrderCreateRequestOrderHeaderDeliveryInstructionsInstructionLineLineNo
            Me.LineText = New OMOrderCreateRequestOrderHeaderDeliveryInstructionsInstructionLineLineText
        End Sub

    End Class

    Partial Public Class OMOrderCreateRequestOrderLine

        Public Sub New()
            Me.SellingStoreLineNo = New OMOrderCreateRequestOrderLineSellingStoreLineNo
            Me.ProductCode = New OMOrderCreateRequestOrderLineProductCode
            Me.ProductDescription = New OMOrderCreateRequestOrderLineProductDescription
            Me.TotalOrderQuantity = New OMOrderCreateRequestOrderLineTotalOrderQuantity
            Me.QuantityTaken = New OMOrderCreateRequestOrderLineQuantityTaken
            Me.UOM = New OMOrderCreateRequestOrderLineUOM
            Me.LineValue = New OMOrderCreateRequestOrderLineLineValue
            Me.DeliveryChargeItem = New OMOrderCreateRequestOrderLineDeliveryChargeItem
            Me.SellingPrice = New OMOrderCreateRequestOrderLineSellingPrice
        End Sub

    End Class

#End Region

#Region "   Om Order Refund"

    Partial Public Class OMOrderRefundRequest
        Inherits BaseRequest

        Public Shared Function Create(ByVal qodHeader As QodHeader) As OMOrderRefundRequest
            Dim Request As New OMOrderRefundRequest
            Const NotifyOk As Integer = 150
            Request.DateTimeStamp = Now
            Request.OMOrderNumber = qodHeader.OmOrderNumber.ToString

            Dim Index As Integer = 0
            For Each QodRefund As QodRefund In qodHeader.Refunds
                If QodRefund.RefundStatus < NotifyOk Then     'To Do Mo'c add refund States to Project
                    Dim FulfilSite As String = qodHeader.Lines.Find(QodRefund.Number).DeliverySource
                    Dim RefundXml As OMOrderRefundRequestOrderRefund = Request.FindOrderRefund(QodRefund)
                    RefundXml.AddRefundLine(QodRefund, FulfilSite, Index)
                    RefundXml.AddFulfilmentSite(FulfilSite, QodRefund.SellingStoreIbtOut)
                End If
                Index += 1
            Next

            Return request
        End Function

        Public Function FindOrderRefund(ByVal refund As QodRefund) As OMOrderRefundRequestOrderRefund
            'check for existing record first
            For Each ref As OMOrderRefundRequestOrderRefund In Me.OrderRefunds
                If (ref.RefundDate = refund.RefundDate) _
                AndAlso (CInt(ref.RefundStoreCode) = refund.RefundStoreId) _
                AndAlso (ref.RefundTransaction = refund.RefundTransaction) _
                AndAlso (ref.RefundTill = refund.RefundTill) Then
                    Return ref
                End If
            Next

            'need to add new refund so get length of array and add one
            Dim length As Integer = Me.OrderRefunds.Length
            ReDim Me.OrderRefunds(length)

            Me.OrderRefunds(length) = New OMOrderRefundRequestOrderRefund
            Me.OrderRefunds(length).RefundDate = refund.RefundDate
            Me.OrderRefunds(length).RefundStoreCode = refund.RefundStoreId.ToString
            Me.OrderRefunds(length).RefundTransaction = refund.RefundTransaction
            Me.OrderRefunds(length).RefundTill = refund.RefundTill
            Return Me.OrderRefunds(length)

        End Function

        Overrides Sub SetFieldsFromQod(ByVal qodHeader As QodHeader)

            Dim OrderRefundsIndex As Integer = -1
            Dim FulfilmentSitesIndex As Integer = 0
            Dim RefundLinesIndex As Integer = 0
            Dim NeedNewSite As Boolean = True
            Dim CurrentStore As Integer = 0
            Dim CurrentTill As String = String.Empty
            Dim CurrentTransaction As String = String.Empty
            Dim RefundSeqNo As Nullable(Of Integer)

            DateTimeStamp = CDate(Format(Now.ToLocalTime, "General Date"))
            OMOrderNumber = qodHeader.OmOrderNumber.ToString

            For Each QodRefund As QodRefund In qodHeader.Refunds

                'If QodRefund.RefundStatus = 199 OrElse QodRefund.RefundStatus = 105 Then Continue For
                Dim F As New RefundHoldingFactory
                Dim RefundHolding As IRefundHolding = F.FactoryGet()  'Referral 963: Refund Holding Issue
                If RefundHolding.IsThisRefundToBeSkipped(QodRefund) Then Continue For

                If CurrentStore <> QodRefund.RefundStoreId OrElse CurrentTill <> QodRefund.RefundTill OrElse CurrentTransaction <> QodRefund.RefundTransaction Then

                    OrderRefundsIndex += 1
                    FulfilmentSitesIndex = 0
                    RefundLinesIndex = 0
                    CurrentStore = QodRefund.RefundStoreId
                    CurrentTill = QodRefund.RefundTill
                    CurrentTransaction = QodRefund.RefundTransaction

                    ReDim Preserve OrderRefunds(OrderRefundsIndex)

                    OrderRefunds(OrderRefundsIndex) = New WebService.OMOrderRefundRequestOrderRefund
                    OrderRefunds(OrderRefundsIndex).RefundDate = QodRefund.RefundDate
                    OrderRefunds(OrderRefundsIndex).RefundStoreCode = CurrentStore.ToString
                    OrderRefunds(OrderRefundsIndex).RefundTill = CurrentTill
                    OrderRefunds(OrderRefundsIndex).RefundTransaction = CurrentTransaction

                    RefundSeqNo = PopulateRefundSequenceNo(QodRefund.RefundSequenceNo)
                    If RefundSeqNo.HasValue = True Then OrderRefunds(OrderRefundsIndex).PreProcessorRefundSeq = RefundSeqNo.Value.ToString

                End If

                Dim QodLine As QodLine = qodHeader.Lines.Find(QodRefund.Number)

                If Not (QodLine.DeliverySource = String.Empty OrElse QodLine.DeliverySource = "0") Then

                    NeedNewSite = True
                    If OrderRefunds(OrderRefundsIndex).FulfilmentSites IsNot Nothing AndAlso OrderRefunds(OrderRefundsIndex).FulfilmentSites.Count > 0 Then

                        For Each FulfilmentSite As WebService.OMOrderRefundRequestOrderRefundFulfilmentSite In OrderRefunds(OrderRefundsIndex).FulfilmentSites
                            If FulfilmentSite.FulfilmentSiteCode = QodLine.DeliverySource Then
                                NeedNewSite = False
                                Exit For
                            End If
                        Next

                    End If

                    If NeedNewSite Then

                        ReDim Preserve OrderRefunds(OrderRefundsIndex).FulfilmentSites(FulfilmentSitesIndex)
                        OrderRefunds(OrderRefundsIndex).FulfilmentSites(FulfilmentSitesIndex) = New WebService.OMOrderRefundRequestOrderRefundFulfilmentSite
                        With OrderRefunds(OrderRefundsIndex).FulfilmentSites(FulfilmentSitesIndex)
                            .FulfilmentSiteCode = QodLine.DeliverySource
                            If QodRefund.SellingStoreIbtOut = String.Empty Then
                                .SellingStoreIBTOutNumber = "0"
                            Else
                                .SellingStoreIBTOutNumber = QodRefund.SellingStoreIbtOut
                            End If
                        End With
                        FulfilmentSitesIndex += 1

                    End If

                End If

                ReDim Preserve OrderRefunds(OrderRefundsIndex).RefundLines(RefundLinesIndex)

                OrderRefunds(OrderRefundsIndex).RefundLines(RefundLinesIndex) = New WebService.OMOrderRefundRequestOrderRefundRefundLine
                With OrderRefunds(OrderRefundsIndex).RefundLines(RefundLinesIndex)
                    .SellingStoreLineNo = QodRefund.Number
                    .OMOrderLineNo = QodRefund.Number
                    .ProductCode = QodRefund.SkuNumber
                    .FulfilmentSiteCode = QodLine.DeliverySource
                    .QuantityReturned = QodRefund.QtyReturned.ToString
                    .QuantityCancelled = QodRefund.QtyCancelled.ToString
                    .RefundLineValue = QodRefund.Price * (QodRefund.QtyReturned + QodRefund.QtyCancelled)
                    .RefundLineStatus = QodRefund.RefundStatus.ToString
                End With

                RefundLinesIndex += 1
            Next

        End Sub

#Region "Private, Protected And Friend Procedures And Functions"

        Friend Function PopulateRefundSequenceNo(ByRef RefundSeqNo As Nullable(Of Integer)) As Nullable(Of Integer)

            Dim V As TpWickes.IQodAndSaleLightWeight

            V = TpWickes.QodAndSaleLightWeightFactory.FactoryGet
            V.RefundSequenceNo = RefundSeqNo

            Return V.RefundSequenceNo

        End Function

#End Region

    End Class

    Partial Public Class OMOrderRefundRequestOrderRefund

        Public Sub AddEmptyRecord()
            Try

                Dim length As Integer = 0
                ReDim Me.RefundLines(length)

                Me.RefundLines(length) = New OMOrderRefundRequestOrderRefundRefundLine
                Me.RefundLines(length).SellingStoreLineNo = "0"
                Me.RefundLines(length).OMOrderLineNo = "0"
                Me.RefundLines(length).ProductCode = String.Empty
                Me.RefundLines(length).FulfilmentSiteCode = String.Empty
                Me.RefundLines(length).QuantityReturned = "0"
                Me.RefundLines(length).QuantityCancelled = "0"
                Me.RefundLines(length).RefundLineValue = 0
                Me.RefundLines(length).RefundLineStatus = "0"
            Catch ex As Exception
                Throw New Exception(ex.ToString)
            End Try

        End Sub

        Public Sub AddRefundLine(ByVal refund As QodRefund, ByVal fulfilmentSiteId As String, ByVal index As Integer)
            'Need to add new refund so get length of array and add one
            'Dim length As Integer = refund.'Me.RefundLines.Length
            Try
                ReDim Preserve Me.RefundLines(index)

                'Create and add refund line to collection
                Me.RefundLines(index) = New OMOrderRefundRequestOrderRefundRefundLine
                Me.RefundLines(index).SellingStoreLineNo = refund.Number
                Me.RefundLines(index).OMOrderLineNo = refund.Number
                Me.RefundLines(index).ProductCode = refund.SkuNumber
                Me.RefundLines(index).FulfilmentSiteCode = fulfilmentSiteId
                Me.RefundLines(index).QuantityReturned = refund.QtyReturned.ToString
                Me.RefundLines(index).QuantityCancelled = refund.QtyCancelled.ToString
                Me.RefundLines(index).RefundLineValue = refund.Price
                Me.RefundLines(index).RefundLineStatus = refund.RefundStatus.ToString
            Catch ex As Exception
                Throw New Exception(ex.ToString)
            End Try

        End Sub

        Public Sub AddFulfilmentSite(ByVal fulfilmentSiteId As String, ByVal ibtOut As String)
            'Check if already there
            If Not Me.FulfilmentSites Is Nothing Then
                For Each site As OMOrderRefundRequestOrderRefundFulfilmentSite In Me.FulfilmentSites
                    If site.FulfilmentSiteCode = fulfilmentSiteId AndAlso site.SellingStoreIBTOutNumber = ibtOut Then
                        Exit Sub
                    End If
                Next
            End If

            'Need to add new refund so get length of array and add one
            Dim length As Integer = Me.FulfilmentSites.Length
            ReDim Me.FulfilmentSites(length)

            'Add site to xml
            Me.FulfilmentSites(length) = New OMOrderRefundRequestOrderRefundFulfilmentSite
            Me.FulfilmentSites(length).FulfilmentSiteCode = fulfilmentSiteId
            Me.FulfilmentSites(length).SellingStoreIBTOutNumber = ibtOut
        End Sub

    End Class

#End Region

#Region "   Om"

    Partial Public Class OMOrderReceiveStatusUpdateRequest
        Inherits BaseRequest

        Public Sub New()
            Me.DateTimeStamp = New OMOrderReceiveStatusUpdateRequestDateTimeStamp
            Me.OMOrderNumber = New OMOrderReceiveStatusUpdateRequestOMOrderNumber
            Me.FulfilmentSite = New OMOrderReceiveStatusUpdateRequestFulfilmentSite
            Me.OrderStatus = New OMOrderReceiveStatusUpdateRequestOrderStatus
        End Sub

        Overrides Sub SetFieldsFromQod(ByVal qod As QodHeader)

            Me.DateTimeStamp.Value = Now
            Me.OMOrderNumber.Value = qod.OmOrderNumber.ToString
            Me.FulfilmentSite.Value = qod.StoreId.ToString
            If CInt(Me.FulfilmentSite.Value) = 0 Then
                Me.FulfilmentSite.Value = qod.SellingStoreId.ToString
                qod.StoreId = qod.SellingStoreId
            End If

            Dim lines As New QodLineCollection
            For Each line As QodLine In qod.Lines
                Dim delSource As Integer = 0
                If Not Integer.TryParse(line.DeliverySource, delSource) Then
                    line.DeliverySource = "0"
                End If
                If delSource = CInt(line.DeliverySource) And line.DeliveryStatus = qod.DeliveryStatus Then
                    lines.Add(line)
                End If
            Next

            Dim limit As Integer = lines.Count
            If limit > 0 Then
                Dim requestlines(limit - 1) As OMOrderReceiveStatusUpdateRequestOrderStatusOrderLine
                For index As Integer = 0 To limit - 1
                    requestlines(index) = New OMOrderReceiveStatusUpdateRequestOrderStatusOrderLine
                    requestlines(index).OMOrderLineNo.Value = lines(index).Number
                    requestlines(index).LineStatus.Value = lines(index).DeliveryStatus.ToString
                Next

                Me.OrderStatus.OrderLines = requestlines
            End If

        End Sub

    End Class

    Partial Public Class OMSendFullStatusUpdateRequest
        Inherits BaseRequest

        Public Overloads Sub SetFields(ByVal omOrderNumber As Nullable(Of Integer), ByVal sellingStoreId As Nullable(Of Integer), ByVal sellingStoreOrderId As Nullable(Of Integer), ByVal tillOrderReference As String)
            Me.DateTimeStamp = Now
            If omOrderNumber.HasValue Then Me.OMOrderNumber = omOrderNumber.Value.ToString
            If sellingStoreId.HasValue Then Me.SellingStoreCode = sellingStoreId.Value.ToString
            If sellingStoreOrderId.HasValue Then Me.SellingStoreOrderNumber = sellingStoreOrderId.Value.ToString
            If Not String.IsNullOrEmpty(tillOrderReference) Then Me.TillOrderReference = tillOrderReference
        End Sub

    End Class


    Partial Public Class OMSourceTransactionLookupRequest
        Inherits BaseRequest

#Region "Set Fields overloads"

        Public Overloads Sub SetFields(ByVal WebOrderNumber As String)

            If Me.DateTimeStamp Is Nothing Then
                Me.DateTimeStamp = New OMSourceTransactionLookupRequestDateTimeStamp
            End If
            Me.DateTimeStamp.Value = Now
            If Me.Source Is Nothing Then
                Me.Source = New OMSourceTransactionLookupRequestSource
            End If
            Me.Source.Value = SourceFromOrderNumberOrDefault(WebOrderNumber)
            If Me.SourceOrderNumber Is Nothing Then
                Me.SourceOrderNumber = New OMSourceTransactionLookupRequestSourceOrderNumber
            End If
            Me.SourceOrderNumber.Value = WebOrderNumber
        End Sub

        Public Overloads Sub SetFields(ByVal WebOrderNumber As String, ByVal Source As String)

            If Me.DateTimeStamp Is Nothing Then
                Me.DateTimeStamp = New OMSourceTransactionLookupRequestDateTimeStamp
            End If
            Me.DateTimeStamp.Value = Now
            If Me.Source Is Nothing Then
                Me.Source = New OMSourceTransactionLookupRequestSource
            End If
            Me.Source.Value = Source
            If Me.SourceOrderNumber Is Nothing Then
                Me.SourceOrderNumber = New OMSourceTransactionLookupRequestSourceOrderNumber
            End If
            Me.SourceOrderNumber.Value = WebOrderNumber
        End Sub

        Public Overloads Sub SetFields(ByVal WebOrderNumber As String, ByVal DateTimeStamp As Date)

            If Me.DateTimeStamp Is Nothing Then
                Me.DateTimeStamp = New OMSourceTransactionLookupRequestDateTimeStamp
            End If
            Me.DateTimeStamp.Value = DateTimeStamp
            If Me.Source Is Nothing Then
                Me.Source = New OMSourceTransactionLookupRequestSource
            End If
            Me.Source.Value = SourceFromOrderNumberOrDefault(WebOrderNumber)
            If Me.SourceOrderNumber Is Nothing Then
                Me.SourceOrderNumber = New OMSourceTransactionLookupRequestSourceOrderNumber
            End If
            Me.SourceOrderNumber.Value = WebOrderNumber
        End Sub

        Public Overloads Sub SetFields(ByVal WebOrderNumber As String, ByVal Source As String, ByVal DateTimeStamp As Date)

            If Me.DateTimeStamp Is Nothing Then
                Me.DateTimeStamp = New OMSourceTransactionLookupRequestDateTimeStamp
            End If
            Me.DateTimeStamp.Value = DateTimeStamp
            If Me.Source Is Nothing Then
                Me.Source = New OMSourceTransactionLookupRequestSource
            End If
            Me.Source.Value = Source
            If Me.SourceOrderNumber Is Nothing Then
                Me.SourceOrderNumber = New OMSourceTransactionLookupRequestSourceOrderNumber
            End If
            Me.SourceOrderNumber.Value = WebOrderNumber
        End Sub

#End Region

#Region "Properties"

        Private _WebOrderSourcesOverride As String()

        Public Property WebOrderSourcesOverride() As String()
            Get
                Return _WebOrderSourcesOverride
            End Get
            Set(ByVal value As String())
                If _WebOrderSourcesOverride.Length = 0 Then
                    ReDim _WebOrderSourcesOverride(value.GetUpperBound(0))
                    For SourceCount As Integer = 0 To value.GetUpperBound(0)
                        _WebOrderSourcesOverride(SourceCount) = value(SourceCount)
                    Next
                End If
            End Set
        End Property

#End Region

        ''' <summary>
        ''' Looks for standard prefixes in weborder number that might be the Source.
        ''' Return Source or Default Source from Source Transaction Lookup Service.
        ''' Removes Source if found from Web Order number, hence ByRef parameter
        ''' </summary>
        ''' <param name="WebOrderNo">Order Number to check for source preifx</param>
        ''' <returns>Source, either from WebOrderNo or from Default Source identified by Source Transaction Lookup Service</returns>
        ''' <history></history>
        ''' <remarks>Removes source from start of WebOrderNo if found.  This is a ByRef param deliberately.</remarks>
        Private Function SourceFromOrderNumberOrDefault(ByRef WebOrderNo As String) As String
            Dim stlService As SourceTransactionLookupService

            ' Allow a different list of source types to be provided - or use default ones
            If WebOrderSourcesOverride IsNot Nothing Then
                stlService = New SourceTransactionLookupService(WebOrderSourcesOverride)
            Else
                stlService = New SourceTransactionLookupService
            End If
            ' Start with the default Source type - use the 0 position as default
            SourceFromOrderNumberOrDefault = stlService.Sources(0)
            ' Check for prefix match in order number and remove the source from order number and use it as returned source
            For Each SourceType As String In stlService.Sources
                If WebOrderNo.StartsWith(SourceType, StringComparison.CurrentCultureIgnoreCase) Then
                    ' Return the found Source
                    SourceFromOrderNumberOrDefault = SourceType
                    ' Remove source from WebOrderNo start
                    WebOrderNo = WebOrderNo.Substring(SourceType.Length)
                    ' Also allow for (and remove) possible delimiter (non alphanumeric) between Prefix and actual Order number
                    Do While (WebOrderNo.Length > 0 AndAlso Not Char.IsLetterOrDigit(WebOrderNo.First))
                        WebOrderNo = WebOrderNo.Substring(1)
                    Loop
                    ' No need to look any furhter
                    Exit For
                End If
            Next
        End Function

    End Class

#End Region

End Namespace