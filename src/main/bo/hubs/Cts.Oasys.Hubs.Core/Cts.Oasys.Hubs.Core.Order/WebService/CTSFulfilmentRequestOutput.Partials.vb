﻿Imports System.Collections
Imports System.Collections.Generic

Namespace WebService

    Partial Public Class CTSFulfilmentResponseOrderHeaderDeliveryInstructionsInstructionLine
        Implements IDeliveryLine

        Public ReadOnly Property LineNumber As Integer Implements IDeliveryLine.LineNumber
            Get
                Return CInt(LineNo.Value)
            End Get
        End Property

        Public ReadOnly Property LineTextForIDeliveryLine As String Implements IDeliveryLine.LineText
            Get
                Return LineText.Value
            End Get
        End Property
    End Class

End Namespace

