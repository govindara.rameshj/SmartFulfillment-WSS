﻿Public Class DeleteTextLineOldWay
    Implements IDeleteTextLine

    Public Function IsItOkToDeleteText(ByVal QodText As Qod.QodText, ByVal QodHeader As Qod.QodHeader) As Boolean Implements IDeleteTextLine.IsItOkToDeleteText
        Dim result As Boolean = False
        If QodHeader.Texts.Find(QodText.Number, QodTextType.Type.DeliveryInstruction) Is Nothing Then
            result = True
        End If
        Return result
    End Function
End Class
