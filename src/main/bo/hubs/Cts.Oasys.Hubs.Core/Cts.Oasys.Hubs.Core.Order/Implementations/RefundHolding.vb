﻿Public Class RefundHolding

    Implements IRefundHolding

    Public Function IsThisRefundToBeSkipped(ByVal QodRefund As Qod.QodRefund) As Boolean Implements IRefundHolding.IsThisRefundToBeSkipped
        Return (QodRefund.RefundStatus = 199)
    End Function
End Class
