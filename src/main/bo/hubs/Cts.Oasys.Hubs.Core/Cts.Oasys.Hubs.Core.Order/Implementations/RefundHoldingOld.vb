﻿Public Class RefundHoldingOld
    Implements IRefundHolding

    Public Function IsThisRefundToBeSkipped(ByVal QodRefund As Qod.QodRefund) As Boolean Implements IRefundHolding.IsThisRefundToBeSkipped
        Return (QodRefund.RefundStatus = 199 OrElse QodRefund.RefundStatus = 105)
    End Function
End Class
