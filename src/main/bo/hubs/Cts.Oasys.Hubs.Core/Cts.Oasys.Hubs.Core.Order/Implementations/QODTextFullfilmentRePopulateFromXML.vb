﻿Imports Cts.Oasys.Hubs.Core.Order.WebService

Public Class QODTextFullfilmentRePopulateFromXML
    Implements IQODTextFullfilmentRePopulateFromXML

    Public Function PopulateQODTextFromXML(ByVal response As WebService.CTSFulfilmentResponse, ByRef QODHeader As Qod.QodHeader) As Boolean Implements IQODTextFullfilmentRePopulateFromXML.PopulateQODTextFromXML
        If response.OrderHeader.DeliveryInstructions.InstructionLine IsNot Nothing Then
            Dim LastXMLLine As Integer = 0
            For Each outputText As CTSFulfilmentResponseOrderHeaderDeliveryInstructionsInstructionLine In response.OrderHeader.DeliveryInstructions.InstructionLine

                Dim txt As Qod.QodText = QODHeader.Texts.FindOrCreate(outputText.LineNo.Value, Qod.QodTextType.Type.DeliveryInstruction)
                txt.SellingStoreId = QODHeader.SellingStoreId
                txt.SellingStoreOrderId = QODHeader.SellingStoreOrderId
                If outputText.LineText.Value IsNot Nothing Then
                    txt.Text = outputText.LineText.Value
                End If
                LastXMLLine = CInt(outputText.LineNo.Value)
            Next

            If QODHeader.Texts.Count > LastXMLLine Then
                For i As Integer = LastXMLLine To QODHeader.Texts.Count - 1
                    QODHeader.Texts(i).Deleted = True
                Next
            End If
        End If
        Return True
    End Function

    Public Function SaveQODText(ByVal con As Data.Connection, ByRef QODHeader As Qod.QodHeader) As Integer Implements IQODTextFullfilmentRePopulateFromXML.SaveQODText
        Dim LinesUpdated As Integer = 0

        For Each QodText As Qod.QodText In QODHeader.Texts
            QodText.OrderNumber = QODHeader.Number

            If QodText.Deleted Then
                LinesUpdated += QODHeader.OrderTextDelete(con, QodText)
            Else
                LinesUpdated += QodText.Persist(con)
            End If
        Next
        Return LinesUpdated
    End Function
End Class
