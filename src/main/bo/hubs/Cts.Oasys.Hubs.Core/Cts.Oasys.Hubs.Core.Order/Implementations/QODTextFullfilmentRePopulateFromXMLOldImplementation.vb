﻿Public Class QODTextFullfilmentRePopulateFromXMLOldImplementation
    Implements IQODTextFullfilmentRePopulateFromXML

    Public Function SaveQODText(ByVal con As Data.Connection, ByRef QODHeader As Qod.QodHeader) As Integer Implements IQODTextFullfilmentRePopulateFromXML.SaveQODText
        Dim LinesUpdated As Integer = 0

        For Each QodText As Qod.QodText In QODHeader.Texts
            QodText.OrderNumber = QODHeader.Number
            LinesUpdated += QodText.Persist(con)
        Next

        Return LinesUpdated
    End Function

    Public Function PopulateQODTextFromXML(ByVal response As WebService.CTSFulfilmentResponse, ByRef QODHeader As Qod.QodHeader) As Boolean Implements IQODTextFullfilmentRePopulateFromXML.PopulateQODTextFromXML
        Return False
    End Function
End Class
