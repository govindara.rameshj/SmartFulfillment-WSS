﻿Imports Cts.Oasys.Hubs.Data
Imports System.Text

Namespace DataAccess

    <HideModuleName()> Friend Module DataSupplier

        Friend Function SupplierGetAll() As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.SupplierGetAll)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

        Friend Function SupplierGetAllForOrder(ByVal type As String) As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.SupplierGetAllForOrder)
                    com.AddParameter(My.Resources.Parameters.Type, type)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

        Friend Function SupplierGetOrdering(ByVal supplierNumber As String) As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.SupplierGetByNumber)
                    com.AddParameter(My.Resources.Parameters.Type, "O")
                    com.AddParameter(My.Resources.Parameters.Number, supplierNumber)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

        Friend Function SupplierGetHeadOffice(ByVal supplierNumber As String) As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.SupplierGetByNumber)
                    com.AddParameter(My.Resources.Parameters.Type, "S")
                    com.AddParameter(My.Resources.Parameters.Number, supplierNumber)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

        Friend Function SupplierGetByNumbers(ByVal supplierNumbers As String) As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.SupplierGetByNumbers)
                    com.AddParameter(My.Resources.Parameters.Numbers, supplierNumbers)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

        Friend Sub SupplierUpdateSoqInit(ByVal sup As Supplier)

            Using con As New Connection
                con.StartTransaction()

                Try
                    'update all stock items for supplier
                    For Each stockItem As Stock.Stock In sup.Stocks
                        Using com As New Command(con, My.Resources.Procedures.StockUpdateSoqInit)
                            com.AddParameter(My.Resources.Parameters.SkuNumber, stockItem.SkuNumber)
                            com.AddParameter(My.Resources.Parameters.DemandPattern, stockItem.DemandPattern)
                            com.AddParameter(My.Resources.Parameters.PeriodDemand, stockItem.PeriodDemand)
                            com.AddParameter(My.Resources.Parameters.PeriodTrend, stockItem.PeriodTrend)
                            com.AddParameter(My.Resources.Parameters.ErrorForecast, stockItem.ErrorForecast)
                            com.AddParameter(My.Resources.Parameters.ErrorSmoothed, stockItem.ErrorSmoothed)
                            com.AddParameter(My.Resources.Parameters.BufferConversion, stockItem.BufferConversion)
                            com.AddParameter(My.Resources.Parameters.BufferStock, stockItem.BufferStock)
                            com.AddParameter(My.Resources.Parameters.OrderLevel, stockItem.OrderLevel)
                            com.AddParameter(My.Resources.Parameters.ValueAnnualUsage, stockItem.ValueAnnualUsage)
                            com.AddParameter(My.Resources.Parameters.FlierPeriod1, stockItem.FlierPeriod1)
                            com.AddParameter(My.Resources.Parameters.FlierPeriod2, stockItem.FlierPeriod2)
                            com.AddParameter(My.Resources.Parameters.FlierPeriod3, stockItem.FlierPeriod3)
                            com.AddParameter(My.Resources.Parameters.FlierPeriod4, stockItem.FlierPeriod4)
                            com.AddParameter(My.Resources.Parameters.FlierPeriod5, stockItem.FlierPeriod5)
                            com.AddParameter(My.Resources.Parameters.FlierPeriod6, stockItem.FlierPeriod6)
                            com.AddParameter(My.Resources.Parameters.FlierPeriod7, stockItem.FlierPeriod7)
                            com.AddParameter(My.Resources.Parameters.FlierPeriod8, stockItem.FlierPeriod8)
                            com.AddParameter(My.Resources.Parameters.FlierPeriod9, stockItem.FlierPeriod9)
                            com.AddParameter(My.Resources.Parameters.FlierPeriod10, stockItem.FlierPeriod10)
                            com.AddParameter(My.Resources.Parameters.FlierPeriod11, stockItem.FlierPeriod11)
                            com.AddParameter(My.Resources.Parameters.FlierPeriod12, stockItem.FlierPeriod12)
                            com.ExecuteNonQuery()
                        End Using
                    Next

                    con.CommitTransaction()

                Catch ex As Exception
                    con.RollbackTransaction()
                    Throw
                End Try
            End Using

        End Sub

        Friend Sub SupplierUpdateSoqInventory(ByRef sup As Supplier)

            Using con As New Connection
                con.StartTransaction()

                Try
                    Dim suggested As Boolean = False

                    'update all stock items for supplier
                    For Each stockItem As Stock.Stock In sup.Stocks
                        If stockItem.OrderLevel > 0 Then suggested = True

                        Using com As New Command(con, My.Resources.Procedures.StockUpdateSoqInventory)
                            com.AddParameter(My.Resources.Parameters.SkuNumber, stockItem.SkuNumber)
                            com.AddParameter(My.Resources.Parameters.BufferStock, stockItem.BufferStock)
                            com.AddParameter(My.Resources.Parameters.OrderLevel, stockItem.OrderLevel)
                            com.ExecuteNonQuery()
                        End Using
                    Next

                    'update supplier
                    Using com As New Command(con, My.Resources.Procedures.SupplierUpdateSoqInventory)
                        com.AddParameter(My.Resources.Parameters.Number, sup.Number)
                        com.AddParameter(My.Resources.Parameters.Suggested, suggested)
                        com.AddParameter(My.Resources.Parameters.SoqDate, sup.SoqDate)
                        com.AddParameter(My.Resources.Parameters.SoqOrdered, sup.SoqOrdered)
                        com.AddParameter(My.Resources.Parameters.SoqNumber, sup.SoqNumber, SqlDbType.Int, 4, ParameterDirection.Output)
                        com.ExecuteNonQuery()

                        sup.SoqNumber = CInt(com.GetParameterValue(My.Resources.Parameters.SoqNumber))
                    End Using

                    con.CommitTransaction()

                Catch ex As Exception
                    con.RollbackTransaction()
                    Throw
                End Try
            End Using

        End Sub

        Friend Sub SupplierUpdateSoqReset(ByRef sup As Supplier)

            Using con As New Connection
                con.StartTransaction()

                Try
                    'update all stock items for supplier
                    For Each stockItem As Stock.Stock In sup.Stocks
                        stockItem.BufferStock = 0
                        stockItem.OrderLevel = 0

                        Using com As New Command(con, My.Resources.Procedures.StockUpdateSoqInventory)
                            com.AddParameter(My.Resources.Parameters.SkuNumber, stockItem.SkuNumber)
                            com.AddParameter(My.Resources.Parameters.BufferStock, stockItem.BufferStock)
                            com.AddParameter(My.Resources.Parameters.OrderLevel, stockItem.OrderLevel)
                            com.ExecuteNonQuery()
                        End Using
                    Next

                    'update supplier
                    Using com As New Command(con, My.Resources.Procedures.SupplierUpdateSoqInventory)
                        sup.SoqDate = Now.Date
                        sup.SoqOrdered = False
                        sup.SoqNumber = Nothing

                        com.AddParameter(My.Resources.Parameters.Number, sup.Number)
                        com.AddParameter(My.Resources.Parameters.Suggested, False)
                        com.AddParameter(My.Resources.Parameters.SoqDate, sup.SoqDate)
                        com.AddParameter(My.Resources.Parameters.SoqOrdered, sup.SoqOrdered)
                        com.AddParameter(My.Resources.Parameters.SoqNumber, sup.SoqNumber, SqlDbType.Int, 4, ParameterDirection.Output)
                        com.ExecuteNonQuery()
                    End Using

                    con.CommitTransaction()

                Catch ex As Exception
                    con.RollbackTransaction()
                    Throw
                End Try
            End Using

        End Sub

        Friend Function EnableStatesGetAll() As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.GetEnableStates)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

        Friend Function QodSourceGetAll() As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.QodSourceGetAll)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

    End Module

    <HideModuleName()> Friend Module DataOperations

        Friend Function GetConsignment(ByVal number As String) As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.ConsignmentGetByNumber)
                    com.AddParameter(My.Resources.Parameters.Number, number)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

        Friend Function ContainerGetByDateRange(ByVal startDate As Date, ByVal endDate As Date) As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.ContainerGetByDateRange)
                    com.AddParameter(My.Resources.Parameters.StartDate, startDate)
                    com.AddParameter(My.Resources.Parameters.EndDate, endDate)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

        Friend Function ContainerGetLines(ByVal assemblyDepotNumber As String, ByVal containerNumber As String) As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.ContainerGetLines)
                    com.AddParameter(My.Resources.Parameters.AssemblyDepotNumber, assemblyDepotNumber)
                    com.AddParameter(My.Resources.Parameters.ContainerNumber, containerNumber)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

        Friend Function ReturnGetNonReleased() As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.ReturnGetNonReleased)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

        Friend Function ReturnGetLines(ByVal returnId As String) As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.ReturnGetLines)
                    com.AddParameter(My.Resources.Parameters.ReturnId, returnId)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

    End Module

End Namespace




