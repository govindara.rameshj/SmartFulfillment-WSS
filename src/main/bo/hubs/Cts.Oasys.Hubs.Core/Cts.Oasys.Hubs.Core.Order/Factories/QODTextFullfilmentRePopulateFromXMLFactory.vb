﻿Public Class QODTextFullfilmentRePopulateFromXMLFactory

    Private Const Parameter As Integer = -109

    Public Function FactoryGet() As IQODTextFullfilmentRePopulateFromXML
        If IsEnabled() = True Then
            Return New QODTextFullfilmentRePopulateFromXML
        Else
            Return New QODTextFullfilmentRePopulateFromXMLOldImplementation
        End If
    End Function

    Public Overridable Function IsEnabled() As Boolean
        Return System.Parameter.GetBoolean(Parameter)
    End Function
End Class


