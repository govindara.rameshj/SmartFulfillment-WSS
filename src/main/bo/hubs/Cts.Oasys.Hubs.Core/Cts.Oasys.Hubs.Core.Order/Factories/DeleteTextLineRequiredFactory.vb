﻿Public Class DeleteTextLineRequiredFactory
    Inherits RequirementSwitchFactory(Of IDeleteTextLine)

    Public Overrides Function ImplementationA() As IDeleteTextLine
        Return New DeleteTextLine
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = CBool(SwitchRepository.RequirementEnabled(-118))
    End Function

    Public Overrides Function ImplementationB() As IDeleteTextLine
        Return New DeleteTextLineOldWay
    End Function

End Class
