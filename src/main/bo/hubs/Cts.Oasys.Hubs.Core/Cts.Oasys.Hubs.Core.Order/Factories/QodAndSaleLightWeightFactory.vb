﻿Namespace TpWickes

    Public Class QodAndSaleLightWeightFactory

        Private Shared m_FactoryMember As IQodAndSaleLightWeight

        Public Shared Function FactoryGet() As IQodAndSaleLightWeight

            If m_FactoryMember Is Nothing Then

                If RefundSequenceNoRequirementEnabled() = True Then

                    Return New QodAndSaleLightWeightNew()

                Else

                    Return New QodAndSaleLightWeight()

                End If

            Else

                Return m_FactoryMember   'stub implementation

            End If

        End Function

        Public Shared Sub FactorySet(ByVal obj As IQodAndSaleLightWeight)

            m_FactoryMember = obj

        End Sub

#Region "Private or Friend Functions And Procedures"

        Friend Shared Function RefundSequenceNoRequirementEnabled() As Boolean

            Dim Repository As IRequirementRepository
            Dim RequirementEnabled As Nullable(Of Boolean)

            Repository = RequirementRepositoryFactory.FactoryGet
            RequirementEnabled = Repository.RequirementEnabled(-2111)

            If RequirementEnabled.HasValue = False Then Return False

            Return RequirementEnabled.Value

        End Function

#End Region

    End Class

End Namespace