﻿Public Class DeliveryStatusChangeFactory
    Inherits RequirementSwitchFactory(Of IDeliveryStatusChange)

    Public Overrides Function ImplementationA() As IDeliveryStatusChange
        Return New DeliveryStatusChange
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = CBool(SwitchRepository.RequirementEnabled(-22021))
    End Function

    Public Overrides Function ImplementationB() As IDeliveryStatusChange
        Return New DeliveryStatusChangeOldWay
    End Function
End Class
