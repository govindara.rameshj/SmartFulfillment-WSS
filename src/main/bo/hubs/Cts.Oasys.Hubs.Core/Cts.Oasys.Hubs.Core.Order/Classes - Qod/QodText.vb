﻿Imports Cts.Oasys.Hubs.Core.Order.WebService
Imports Cts.Oasys.Hubs.Data
Imports System.Xml
Imports System.Reflection
Imports System.Text

Namespace Qod

    Public Class QodText

        Inherits Oasys.Hubs.Core.Base

        Private _orderNumber As String
        Private _number As String
        Private _type As String
        Private _text As String
        Private _sellingStoreId As Integer
        Private _sellingStoreOrderId As Integer
        Private _ExistsInDB As Boolean
        Private _Deleted As Boolean

        <ColumnMapping("OrderNumber")> Public Property OrderNumber() As String
            Get
                Return _orderNumber
            End Get
            Set(ByVal value As String)
                _orderNumber = value
            End Set
        End Property

        <ColumnMapping("Number")> Public Property Number() As String
            Get
                Return _number
            End Get
            Set(ByVal value As String)
                _number = value
            End Set
        End Property

        <ColumnMapping("Type")> Public Property Type() As String
            Get
                Return _type
            End Get
            Set(ByVal value As String)
                _type = value
            End Set
        End Property

        <ColumnMapping("Text")> Public Property Text() As String
            Get
                Return _text
            End Get
            Set(ByVal value As String)
                _text = value
            End Set
        End Property

        <ColumnMapping("SellingStoreId")> Public Property SellingStoreId() As Integer
            Get
                Return _sellingStoreId
            End Get
            Set(ByVal value As Integer)
                _sellingStoreId = value
            End Set
        End Property

        <ColumnMapping("SellingStoreOrderId")> Public Property SellingStoreOrderId() As Integer
            Get
                Return _sellingStoreOrderId
            End Get
            Set(ByVal value As Integer)
                _sellingStoreOrderId = value
            End Set
        End Property

        Public ReadOnly Property TextType() As QodTextType.Type
            Get
                Select Case Me.Type
                    Case QodTextType.Description.DeliveryInstruction : Return QodTextType.Type.DeliveryInstruction
                    Case QodTextType.Description.PickInstruction : Return QodTextType.Type.PickInstruction
                    Case QodTextType.Description.ValidationError : Return QodTextType.Type.ValidationError
                    Case Else : Return QodTextType.Type.None
                End Select
            End Get
        End Property

        Public Property ExistsInDB() As Boolean
            Get
                Return _ExistsInDB
            End Get
            Set(ByVal value As Boolean)
                _ExistsInDB = value
            End Set
        End Property

        Public Property Deleted() As Boolean
            Get
                Return _Deleted
            End Get
            Set(ByVal value As Boolean)
                _Deleted = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub

        Friend Sub New(ByVal qod As QodHeader, ByVal txt As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryInstructionsInstructionLine)
            MyBase.New()
            Me.OrderNumber = qod.Number
            Me.Number = txt.LineNo.Value
            Me.Type = QodTextType.Description.DeliveryInstruction
            Me.Text = txt.LineText.Value
            Me.SellingStoreId = qod.SellingStoreId
            Me.SellingStoreOrderId = qod.SellingStoreOrderId
        End Sub

        Friend Sub New(ByVal qod As QodHeader, ByVal txt As OMOrderCreateResponseOrderHeaderDeliveryInstructionsInstructionLine)
            MyBase.New()
            Me.OrderNumber = qod.Number
            Me.Number = txt.LineNo.Value
            Me.Type = QodTextType.Description.DeliveryInstruction
            Me.Text = txt.LineText.Value
            Me.SellingStoreId = qod.SellingStoreId
            Me.SellingStoreOrderId = qod.SellingStoreOrderId
        End Sub

        Friend Sub New(ByVal qod As QodHeader, ByVal txt As CTSQODCreateResponseOrderHeaderDeliveryInstructionsInstructionLine)
            MyBase.New()
            Me.OrderNumber = qod.Number
            Me.Number = txt.LineNo.Value
            Me.Type = QodTextType.Description.DeliveryInstruction
            Me.Text = txt.LineText.Value
            Me.SellingStoreId = qod.SellingStoreId
            Me.SellingStoreOrderId = qod.SellingStoreOrderId
        End Sub

        Public Sub New(ByVal qodCreateText As CTSQODCreateResponseOrderHeaderDeliveryInstructionsInstructionLine)
            MyBase.New()
            Me.Type = QodTextType.Description.DeliveryInstruction
            Me.Text = qodCreateText.LineText.Value
            Me.Number = qodCreateText.LineNo.Value
        End Sub

        Public Function HasChanged(ByVal configxml As XmlNode, ByVal TempText As QodText) As Boolean
            Try
                Dim myType As Global.System.Type = Me.GetType
                Dim myProperties() As PropertyInfo = Me.GetType.GetProperties
                Dim PropertyNames As XmlNodeList = configxml.SelectNodes("QodText/Properties/Name")
                Dim PropertyItem As PropertyInfo

                For Each PropertyName As XmlNode In PropertyNames
                    PropertyItem = Me.GetType.GetProperty(PropertyName.InnerText)
                    Try
                        If PropertyItem.GetValue(Me, Nothing) IsNot Nothing AndAlso PropertyItem.GetValue(TempText, Nothing) IsNot Nothing Then
                            If PropertyItem.GetValue(Me, Nothing).ToString <> PropertyItem.GetValue(TempText, Nothing).ToString Then
                                Return True
                            End If
                        ElseIf PropertyItem.GetValue(Me, Nothing) Is Nothing And PropertyItem.GetValue(TempText, Nothing) IsNot Nothing Then
                            Return True
                        ElseIf PropertyItem.GetValue(Me, Nothing) IsNot Nothing And PropertyItem.GetValue(TempText, Nothing) Is Nothing Then
                            Return True
                        End If
                    Catch
                    End Try
                Next
            Catch
                Return True
            End Try
            Return False
        End Function

        Private Function PersistNew(ByVal con As Connection) As Integer

            Dim LinesInserted As Integer = 0

            If (Me.Text IsNot Nothing) AndAlso (Me.Text.Trim.Length > 0) Then
                Using com As New Command(con)
                    Select Case con.DataProvider
                        Case DataProvider.Odbc
                            Dim sb As New StringBuilder
                            sb.Append("Insert into CORTXT (")
                            sb.Append("NUMB,LINE,TYPE,SellingStoreId,SellingStoreOrderId,TEXT")
                            sb.Append(") values (")
                            sb.Append("?,?,?,?,?,? )")

                            com.CommandText = sb.ToString
                            com.AddParameter("OrderNumber", Me.OrderNumber)
                            com.AddParameter("Number", Me.Number)
                            com.AddParameter("Type", Me.Type)
                            com.AddParameter("SellingStoreId", Me.SellingStoreId)
                            com.AddParameter("SellingStoreOrderId", Me.SellingStoreOrderId)
                            com.AddParameter("Text", Me.Text)
                            LinesInserted += com.ExecuteNonQuery()
                            If LinesInserted > 0 Then ExistsInDB = True

                        Case DataProvider.Sql
                            com.StoredProcedureName = My.Resources.Procedures.SaleOrderTextInsert
                            com.AddParameter(My.Resources.Parameters.OrderNumber, Me.OrderNumber)
                            com.AddParameter(My.Resources.Parameters.Number, Me.Number)
                            com.AddParameter(My.Resources.Parameters.Type, Me.Type)
                            com.AddParameter(My.Resources.Parameters.SellingStoreId, Me.SellingStoreId)
                            com.AddParameter(My.Resources.Parameters.SellingStoreOrderId, Me.SellingStoreOrderId)
                            com.AddParameter(My.Resources.Parameters.Text, Me.Text)
                            LinesInserted += com.ExecuteNonQuery()
                            If LinesInserted > 0 Then ExistsInDB = True

                    End Select
                End Using
            End If

            Trace.WriteLine("Lines Affected: = " & LinesInserted.ToString)
            Return LinesInserted

        End Function

        Public Function Persist(ByVal con As Connection) As Integer

            If Not _ExistsInDB Then
                Return PersistNew(con)
            Else
                Dim LinesUpdated As Integer = 0
                If (Me.Text IsNot Nothing) AndAlso (Me.Text.Trim.Length > 0) Then
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                com.CommandText = "Update CORTXT set TEXT=? where NUMB=? and LINE=? and TYPE=?"
                                com.AddParameter("TEXT", Me.Text)
                                com.AddParameter("NUMB", Me.OrderNumber)
                                com.AddParameter("LINE", Me.Number)
                                com.AddParameter("TYPE", Me.Type)

                                LinesUpdated += com.ExecuteNonQuery()

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleOrderTextUpdate
                                com.AddParameter(My.Resources.Parameters.OrderNumber, Me.OrderNumber)
                                com.AddParameter(My.Resources.Parameters.Number, Me.Number)
                                com.AddParameter(My.Resources.Parameters.Type, Me.Type)
                                com.AddParameter(My.Resources.Parameters.Text, Me.Text)
                                LinesUpdated += com.ExecuteNonQuery()

                        End Select
                    End Using
                End If

                Trace.WriteLine("Lines Affected: = " & LinesUpdated.ToString)
                Return LinesUpdated
            End If
        End Function
    End Class

    Public Class QodTextCollection
        Inherits BaseCollection(Of QodText)

        Public Sub New()
            MyBase.New()
        End Sub

        Public Overloads Sub Load(ByVal orderNumber As String)
            Dim dt As DataTable = DataAccess.GetTexts(orderNumber)
            MyBase.Load(dt)
            For Each txt As QodText In Me.Items
                txt.ExistsInDB = True
            Next
        End Sub

        Public Overloads Sub Add(ByVal type As QodTextType.Type)
            Dim txt As New QodText
            txt.Type = QodTextType.Description.GetDescription(type)
            txt.Number = "000"
            txt.Number = Me.NextNumber(type)
            Me.Add(txt)
        End Sub

        Public Overloads Function AddNew(ByVal type As QodTextType.Type) As QodText
            Dim txt As QodText = Me.AddNew()
            txt.Type = QodTextType.Description.GetDescription(type)
            txt.Number = "000"
            txt.Number = Me.NextNumber(type)
            Return txt
        End Function

        Public Sub AddValidationErrors(ByVal errors As List(Of String))

            'check that less than 99 texts
            If Me.Items.Count < 99 Then
                For Each es As String In errors
                    Dim alreadyPresent As Boolean = False

                    'check if error not already present and less than 99 texts
                    For Each txt As QodText In Me.Items
                        If txt.Text = es Then
                            alreadyPresent = True
                            Exit For
                        End If
                    Next

                    If Not alreadyPresent AndAlso Me.Items.Count < 99 Then
                        Dim text As QodText = Me.AddNew(QodTextType.Type.ValidationError)
                        text.Text = es
                    End If
                Next
            End If

        End Sub

        Public Function Find(ByVal number As String, ByVal type As QodTextType.Type) As QodText
            For Each txt As QodText In Me.Items
                If txt.Number = number AndAlso txt.TextType = type Then Return txt
            Next
            Return Nothing
        End Function

        Public Function FindOrCreate(ByVal number As String, ByVal type As QodTextType.Type) As QodText
            For Each txt As QodText In Me.Items
                If txt.Number = number AndAlso txt.TextType = type Then Return txt
            Next

            Dim newText As QodText = Me.AddNew
            newText.Number = number
            newText.Type = QodTextType.Description.GetDescription(type)
            Return newText
        End Function

        Public Function GetTexts(ByVal type As QodTextType.Type) As QodTextCollection
            Dim col As New QodTextCollection
            For Each txt As QodText In Me.Items
                If txt.TextType = type Then col.Items.Add(txt)
            Next
            Return col
        End Function

        Public Function NextNumber(ByVal type As QodTextType.Type) As String
            Dim num As Integer = 0
            For Each txt As QodText In Me.Items
                If txt.Type = QodTextType.Description.GetDescription(type) Then
                    If txt.Number Is Nothing Then Continue For
                    Dim txtNumber As Integer = 0
                    If Integer.TryParse(txt.Number, txtNumber) Then
                        If txtNumber > num Then num = txtNumber
                    End If
                End If
            Next
            Return (num + 1).ToString("00")
        End Function

    End Class

    Namespace QodTextType

        Public Enum Type
            None
            DeliveryInstruction
            PickInstruction
            ValidationError
        End Enum

        Public Structure Description
            Private _value As String

            Public Const PickInstructionToken As String = "S#OM.PI#F"
            Public Shared ReadOnly DeliveryInstruction As String = "DI"
            Public Shared ReadOnly PickInstruction As String = "PI"
            Public Shared ReadOnly ValidationError As String = "VE"

            Public Shared Function GetDescription(ByVal type As QodTextType.Type) As String
                Select Case type
                    Case QodTextType.Type.DeliveryInstruction : Return DeliveryInstruction
                    Case QodTextType.Type.PickInstruction : Return PickInstruction
                    Case QodTextType.Type.ValidationError : Return ValidationError
                    Case Else : Return String.Empty
                End Select
            End Function

        End Structure

    End Namespace

End Namespace

