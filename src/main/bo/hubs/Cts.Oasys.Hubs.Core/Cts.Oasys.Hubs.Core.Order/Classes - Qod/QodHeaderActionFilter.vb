﻿Namespace Qod

    ''' <summary>
    ''' Used to hold three filtered lists of QodHeaders for use in the Action Confirmation screen
    ''' </summary>
    ''' <history>
    ''' <created author="Charles McMahon" date="07/10/2010"></created>
    ''' </history>
    ''' <remarks></remarks>
    Public Class QodHeaderActionFilter
        Private _PositiveActionList As New QodHeaderList
        Private _NegativeActionList As New QodHeaderList
        Private _PositiveActionNegateList As New QodHeaderList

        ''' <summary>
        ''' Filtered list of Qods that appear in the initial positive action grid
        ''' </summary>
        ''' <value></value>
        ''' <history>
        ''' <created author="Charles McMahon" date="07/10/2010"></created>
        ''' </history>
        ''' <remarks></remarks>
        Public ReadOnly Property PositiveActionList() As QodHeaderList
            Get
                Return _PositiveActionList
            End Get
        End Property

        ''' <summary>
        ''' Filtered list of Qods that appear in the negative action grid
        ''' </summary>
        ''' <value></value>
        ''' <history>
        ''' <created author="Charles McMahon" date="07/10/2010"></created>
        ''' </history>
        ''' <remarks></remarks>
        Public ReadOnly Property NegativeActionList() As QodHeaderList
            Get
                Return _NegativeActionList
            End Get
        End Property


        ''' <summary>
        ''' Filtered list of Qods that the user de-selects from the initial positive action grid
        ''' </summary>
        ''' <value></value>
        ''' <history>
        ''' <created author="Charles McMahon" date="07/10/2010"></created>
        ''' </history>
        ''' <remarks></remarks>
        Public ReadOnly Property PositiveActionNegateList() As QodHeaderList
            Get
                Return _PositiveActionNegateList
            End Get
        End Property

        ''' <summary>
        ''' Filters a collection of Qods based on minimum and maximum delivery statuses
        ''' </summary>
        ''' <param name="qodHeaderCollection">The collection to be filtered</param>
        ''' <param name="minStatus">The minumum status that a Qod should be at to appear in the positive action list</param>
        ''' <param name="maxStatus">The maximum status that a Qod should be at to appear in the positive action list</param>
        ''' <param name="storeId">The local store ID to filter on</param>
        ''' <history>
        ''' <created author="Charles McMahon" date="07/10/2010"></created>
        ''' </history>
        ''' <remarks>
        ''' Filters based on the LINE statuses of the local store using the store id
        ''' </remarks>
        Public Sub New(ByVal qodHeaderCollection As QodHeaderCollection, ByVal minStatus As State.Delivery, _
                       ByVal maxStatus As State.Delivery, ByVal storeId As String)
            Dim QodHeaderIsPositive As Boolean
            'Check each header in the collection
            For Each QodHeader As QodHeader In qodHeaderCollection
                'Assume that it should not be in the positive action list
                QodHeaderIsPositive = False
                'Make sure that the header has the local store as a despatch site and that it is not suspended
                If QodHeader.DespatchSite.Contains(storeId) AndAlso Not QodHeader.IsSuspended Then
                    'Check the delivery status for lines for this order that are for the local store
                    For Each QodLine As QodLine In QodHeader.Lines
                        If QodLine.DeliverySource = storeId AndAlso QodLine.DeliveryStatus >= minStatus AndAlso _
                        QodLine.DeliveryStatus <= maxStatus Then
                            'This LINE is a match, so the header should be included in the positive action list
                            QodHeaderIsPositive = True
                            Exit For
                        End If
                    Next
                End If
                If QodHeaderIsPositive Then
                    'Add the Qod to the positive action list
                    _PositiveActionList.Add(QodHeader)
                Else
                    'Add the Qod to the negative action list
                    _NegativeActionList.Add(QodHeader)
                End If
            Next
        End Sub

        Public Sub Union(ByVal outerActionFilter As QodHeaderActionFilter)
            Dim UnionPositiveList As New QodHeaderList
            Dim IntersectNegativeList As New QodHeaderList
            'Union the positive action list
            For Each QodHeader As QodHeader In PositiveActionList
                UnionPositiveList.Add(QodHeader)
            Next
            For Each QodHeader As QodHeader In outerActionFilter.PositiveActionList
                If Not UnionPositiveList.Contains(QodHeader, New QodHeaderEqualityComparer()) Then
                    UnionPositiveList.Add(QodHeader)
                End If
            Next
            'Intersect the negative action list
            For Each QodHeader As QodHeader In outerActionFilter.NegativeActionList
                If NegativeActionList.Contains(QodHeader, New QodHeaderEqualityComparer()) Then
                    IntersectNegativeList.Add(QodHeader)
                End If
            Next
            _PositiveActionList = UnionPositiveList
            _NegativeActionList = IntersectNegativeList
        End Sub

        Public Sub UnionPositive(ByVal outerActionFilter As QodHeaderActionFilter)
            'Union the positive list, intersect the negative list and
            'union the complement of the positive list against the outer positive negate list AND
            'the complement of the positive negate list against the outer positive list
            Dim UnionPositiveList = CType(PositiveActionList.Union(outerActionFilter.PositiveActionList, New QodHeaderEqualityComparer()), QodHeaderList)
            Dim IntersectNegativeList = CType(NegativeActionList.Intersect(outerActionFilter.NegativeActionList), QodHeaderList)
            Dim ComplimentNegateList = CType(PositiveActionNegateList.Except(outerActionFilter.PositiveActionList), QodHeaderList)
            Dim ComplimentNegateOuterList = outerActionFilter.PositiveActionNegateList.Except(PositiveActionList)
            Dim UnionNegateList = CType(ComplimentNegateList.Union(ComplimentNegateOuterList), QodHeaderList)
            _PositiveActionList = UnionPositiveList
            _NegativeActionList = IntersectNegativeList
            _PositiveActionNegateList = UnionNegateList
        End Sub

        Public Sub UnionNegative(ByVal outerActionFilter As QodHeaderActionFilter)
            'Union the negative list, intersect the positive list and
            'union the intersection of the positive action negate list and the outer positive action list with
            'the intersection of the outer positive action negate list and the positive action list with
            'the intersection of the outer positive action negate list and the positive action negate list
            _PositiveActionList = CType(PositiveActionList.Intersect(outerActionFilter.PositiveActionList), QodHeaderList)
            _NegativeActionList = CType(NegativeActionList.Union(outerActionFilter.NegativeActionList), QodHeaderList)
            Dim IntersectInnerPositiveActionNegateList = PositiveActionNegateList.Intersect(outerActionFilter.PositiveActionList)
            Dim IntersectOuterPositiveActionNegateList = outerActionFilter.PositiveActionNegateList.Intersect(PositiveActionList)
            Dim IntersectPositiveActionNegateList = PositiveActionNegateList.Intersect(outerActionFilter.PositiveActionNegateList)
            _PositiveActionNegateList = CType(IntersectInnerPositiveActionNegateList.Union(IntersectOuterPositiveActionNegateList).Union(IntersectPositiveActionNegateList), QodHeaderList)
        End Sub

    End Class

    Public Class QodHeaderEqualityComparer
        Implements IEqualityComparer(Of QodHeader)

        Public Function Equals1(ByVal x As QodHeader, ByVal y As QodHeader) As Boolean Implements IEqualityComparer(Of QodHeader).Equals
            Return Integer.Equals(x.SellingStoreOrderId, y.SellingStoreOrderId) AndAlso Integer.Equals(x.SellingStoreId, y.SellingStoreId)
        End Function

        Public Function GetHashCode1(ByVal obj As QodHeader) As Integer Implements IEqualityComparer(Of QodHeader).GetHashCode
            Return obj.ToString.ToLower.GetHashCode()
        End Function
    End Class

    ''' <summary>
    ''' List of QodHeaders for use in the QodHeaderActionFilter class
    ''' </summary>
    ''' <history>
    ''' <created author="Charles McMahon" date="07/10/2010"></created>
    ''' </history>
    ''' <remarks></remarks>
    Public Class QodHeaderList
        Inherits List(Of QodHeader)
    End Class

End Namespace

