﻿Namespace TpWickes

    Public Class QodAndSaleLightWeightNew
        Implements IQodAndSaleLightWeight

        Private _RefundSequenceNo As Nullable(Of Integer)

        Public Property RefundSequenceNo() As Integer? Implements IQodAndSaleLightWeight.RefundSequenceNo
            Get

                Return _RefundSequenceNo

            End Get
            Set(ByVal value As Integer?)

                _RefundSequenceNo = value

            End Set
        End Property

        Public Sub AddRefundSequenceNoSqlParameterObject(ByRef Cmd As Data.Command) Implements IQodAndSaleLightWeight.AddRefundSequenceNoSqlParameterObject

            If Me._RefundSequenceNo.HasValue = True Then Cmd.AddParameter("RefundSequenceNo", RefundSequenceNo.Value)

        End Sub

    End Class

End Namespace