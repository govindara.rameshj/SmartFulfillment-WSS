Imports System.Xml
Imports System.IO
Imports System.Text
Imports System.Xml.Serialization
Imports System.Reflection
Imports Cts.Oasys.Hubs.Data
Imports Cts.Oasys.Hubs.Core.Order.WebService
Imports Cts.Oasys.Hubs.Core.Order.Qod.State

Namespace Qod
    Public Class QodLine
        Inherits Oasys.Hubs.Core.Base

#Region "       Table Fields"
        Private _orderNumber As String
        Private _number As String
        Private _skuNumber As String
        Private _skuDescription As String
        Private _skuUnitMeasure As String
        Private _qtyOrdered As Integer
        Private _qtyTaken As Integer
        Private _qtyRefunded As Integer
        Private _qtyToDeliver As Integer
        Private _qtyOnHand As Integer
        Private _qtyOnOrder As Integer
        Private _price As Decimal
        Private _isDeliveryChargeItem As Boolean
        Private _weight As Decimal
        Private _volume As Decimal
        Private _priceOverrideCode As Integer
        Private _deliveryStatus As Integer
        Private _sellingStoreId As Integer
        Private _sellingStoreOrderId As Integer
        Private _deliverySource As String
        Private _deliverySourceIbtOut As String
        Private _sellingStoreIbtIn As String

        'Added for Hubs 2.0

        Private _QuantityScanned As Integer
        Private _SourceOrderLineNo As Integer

        'Changes for Hubs 2.0 ends

        Private _requiredFulfiller As Integer?

        <ColumnMapping("OrderNumber")> Public Property OrderNumber() As String
            Get
                Return _orderNumber
            End Get
            Set(ByVal value As String)
                _orderNumber = value
            End Set
        End Property
        <ColumnMapping("Number")> Public Property Number() As String
            Get
                Return _number
            End Get
            Set(ByVal value As String)
                _number = value
            End Set
        End Property
        <ColumnMapping("SkuNumber")> Public Property SkuNumber() As String
            Get
                Return _skuNumber
            End Get
            Set(ByVal value As String)
                _skuNumber = value
            End Set
        End Property
        <ColumnMapping("SkuDescription")> Public Property SkuDescription() As String
            Get
                Return _skuDescription
            End Get
            Set(ByVal value As String)
                _skuDescription = value
            End Set
        End Property
        <ColumnMapping("SkuUnitMeasure")> Public Property SkuUnitMeasure() As String
            Get
                Return _skuUnitMeasure
            End Get
            Set(ByVal value As String)
                _skuUnitMeasure = value
            End Set
        End Property
        <ColumnMapping("QtyOrdered")> Public Property QtyOrdered() As Integer
            Get
                Return _qtyOrdered
            End Get
            Set(ByVal value As Integer)
                _qtyOrdered = value
            End Set
        End Property
        <ColumnMapping("QtyTaken")> Public Property QtyTaken() As Integer
            Get
                Return _qtyTaken
            End Get
            Set(ByVal value As Integer)
                _qtyTaken = value
            End Set
        End Property
        <ColumnMapping("QtyRefunded")> Public Property QtyRefunded() As Integer
            Get
                Return _qtyRefunded
            End Get
            Set(ByVal value As Integer)
                _qtyRefunded = value
            End Set
        End Property
        <ColumnMapping("QtyToDeliver")> Public Property QtyToDeliver() As Integer
            Get
                Return _qtyToDeliver
            End Get
            Set(ByVal value As Integer)
                _qtyToDeliver = value
            End Set
        End Property
        <ColumnMapping("QtyOnHand")> Public Property QtyOnHand() As Integer
            Get
                Return _qtyOnHand
            End Get
            Set(ByVal value As Integer)
                _qtyOnHand = value
            End Set
        End Property
        <ColumnMapping("QtyOnOrder")> Public Property QtyOnOrder() As Integer
            Get
                Return _qtyOnOrder
            End Get
            Set(ByVal value As Integer)
                _qtyOnOrder = value
            End Set
        End Property
        <ColumnMapping("Price")> Public Property Price() As Decimal
            Get
                Return _price
            End Get
            Set(ByVal value As Decimal)
                _price = value
            End Set
        End Property
        <ColumnMapping("IsDeliveryChargeItem")> Public Property IsDeliveryChargeItem() As Boolean
            Get
                Return _isDeliveryChargeItem
            End Get
            Set(ByVal value As Boolean)
                _isDeliveryChargeItem = value
            End Set
        End Property
        <ColumnMapping("Weight")> Public Property Weight() As Decimal
            Get
                Return _weight
            End Get
            Set(ByVal value As Decimal)
                _weight = value
            End Set
        End Property
        <ColumnMapping("Volume")> Public Property Volume() As Decimal
            Get
                Return _volume
            End Get
            Set(ByVal value As Decimal)
                _volume = value
            End Set
        End Property
        <ColumnMapping("PriceOverrideCode")> Public Property PriceOverrideCode() As Integer
            Get
                Return _priceOverrideCode
            End Get
            Set(ByVal value As Integer)
                _priceOverrideCode = value
            End Set
        End Property
        <ColumnMapping("DeliveryStatus")> Public Property DeliveryStatus() As Integer
            Get
                Return _deliveryStatus
            End Get
            Set(ByVal value As Integer)
                _deliveryStatus = value
            End Set
        End Property
        <ColumnMapping("SellingStoreId")> Public Property SellingStoreId() As Integer
            Get
                Return _sellingStoreId
            End Get
            Set(ByVal value As Integer)
                _sellingStoreId = value
            End Set
        End Property
        <ColumnMapping("SellingStoreOrderId")> Public Property SellingStoreOrderId() As Integer
            Get
                Return _sellingStoreOrderId
            End Get
            Set(ByVal value As Integer)
                _sellingStoreOrderId = value
            End Set
        End Property
        <ColumnMapping("DeliverySource")> Public Property DeliverySource() As String
            Get
                Return _deliverySource
            End Get
            Set(ByVal value As String)
                _deliverySource = value
            End Set
        End Property
        <ColumnMapping("DeliverySourceIbtOut")> Public Property DeliverySourceIbtOut() As String
            Get
                Return _deliverySourceIbtOut
            End Get
            Set(ByVal value As String)
                _deliverySourceIbtOut = value
            End Set
        End Property
        <ColumnMapping("SellingStoreIbtIn")> Public Property SellingStoreIbtIn() As String
            Get
                Return _sellingStoreIbtIn
            End Get
            Set(ByVal value As String)
                _sellingStoreIbtIn = value
            End Set
        End Property
        <ColumnMapping("NextPoDateDelivery")> Public Property NextPoDateDelivery() As Date?
            Get
                Return _nextPoDateDelivery
            End Get
            Set(ByVal value As Date?)
                _nextPoDateDelivery = value
            End Set
        End Property

        'Added for hubs 2.0

        <ColumnMapping("QuantityScanned")> Public Property QuantityScanned() As Integer
            Get
                Return _QuantityScanned
            End Get
            Set(ByVal value As Integer)
                _QuantityScanned = value
            End Set
        End Property

        <ColumnMapping("SourceOrderLineNo")> Public Property SourceOrderLineNo() As Integer
            Get
                Return _SourceOrderLineNo
            End Get
            Set(ByVal value As Integer)
                _SourceOrderLineNo = value
            End Set
        End Property

        'Changes ends for hubs 2.0

        <ColumnMapping("RequiredFulfiller")> Public Property RequiredFulfiller() As Integer?
            Get
                Return _requiredFulfiller
            End Get
            Set(ByVal value As Integer?)
                _requiredFulfiller = value
            End Set
        End Property

#End Region

#Region "       Properties"
        Private _nextPoDateDelivery As Date?
        Private _isCancellable As Boolean
        Private _ExistsInDB As Boolean = False
        Private _FulfillingStore As System.Store.Store
        Private _SearchedFulfillingStore As Boolean = False
        Private _FulfillingStoreName As String = String.Empty
        Private _FulfillingStorePhone As String = String.Empty
        Private _DeliveryChangedStatus As IDeliveryStatusChange

        Public Property IsCancellable() As Boolean
            Get
                Return _isCancellable
            End Get
            Set(ByVal value As Boolean)
                _isCancellable = value
            End Set
        End Property

        Public ReadOnly Property IsDependentOnOrders() As Boolean
            Get
                Return (Me.DeliveryStatus < Delivery.DespatchCreated _
                        AndAlso (Me.QtyToDeliver > 0) _
                        AndAlso Me.QtyToDeliver <= (Me.QtyOnHand + Me.QtyOnOrder) _
                        AndAlso Me.QtyToDeliver > Me.QtyOnHand)
            End Get
        End Property
        Public ReadOnly Property IsNotFulfillable() As Boolean
            Get
                Return (Me.DeliveryStatus < Delivery.DespatchCreated _
                        AndAlso (Me.QtyToDeliver > 0) _
                        AndAlso (Me.QtyToDeliver > (Me.QtyOnHand + Me.QtyOnOrder)))
            End Get
        End Property
        Public ReadOnly Property OrderValue() As Decimal
            Get
                Return Me.QtyOrdered * Me.Price
            End Get
        End Property

        Public Property ExistsInDB() As Boolean
            Get
                Return _ExistsInDB
            End Get
            Set(ByVal value As Boolean)
                _ExistsInDB = value
            End Set
        End Property

        Public ReadOnly Property DeliveryPhase() As String
            Get
                Select Case _deliveryStatus
                    Case Is < State.Delivery.DeliveryRequestAccepted
                        Return "Awaiting OM Assignment"
                    Case Is < State.Delivery.IbtOutAllStock
                        Return "Awaiting OM Confirmation"
                    Case Is < State.Delivery.PickingListCreated
                        Return "Awaiting Print of Pick Note"
                    Case Is < State.Delivery.DespatchCreated
                        Return "In Picking"
                    Case Is < State.Delivery.UndeliveredCreated
                        Return "Despatched"
                    Case Is < State.Delivery.DeliveredCreated
                        Return "Failed Delivery"
                    Case Is < State.Delivery.DeliveredStatusOk
                        Return "Delivery Confirmed"
                    Case Else
                        Return "Completed"
                End Select
            End Get
        End Property

        Public ReadOnly Property FulfillingStoreName() As String
            Get
                If _FulfillingStoreName = String.Empty Then
                    If FulfillingStore Is Nothing Then
                        If _deliverySource = String.Empty OrElse _deliverySource = "0" Then
                            _FulfillingStoreName = "N/A"
                        Else
                            _FulfillingStoreName = "Unavailable"
                        End If
                    Else
                        _FulfillingStoreName = FulfillingStore.Name
                    End If
                End If
                Return _FulfillingStoreName
            End Get
        End Property

        Public ReadOnly Property FulfillingStorePhone() As String
            Get
                If _FulfillingStorePhone = String.Empty Then
                    If FulfillingStore Is Nothing Then
                        If _deliverySource = String.Empty OrElse _deliverySource = "0" Then
                            _FulfillingStorePhone = "N/A"
                        Else
                            _FulfillingStorePhone = "Unavailable"
                        End If
                    Else
                        _FulfillingStorePhone = FulfillingStore.PhoneNumber
                    End If
                End If
                Return _FulfillingStorePhone
            End Get
        End Property

        Public ReadOnly Property FulfillingStore() As System.Store.Store
            Get
                If _FulfillingStore Is Nothing AndAlso Not _SearchedFulfillingStore Then
                    If Not _deliverySource = String.Empty Then
                        _FulfillingStore = AllStores.GetStore(CInt(_deliverySource))
                    End If
                    _SearchedFulfillingStore = True
                End If
                Return _FulfillingStore
            End Get
        End Property

#End Region

        Public Function Clone() As QodLine

            Try
                'Create a string writer and serialise the object to be cloned (Me) to XML
                Dim objStringWriter As New StringWriter
                Dim xmlQodLine As New XmlSerializer(Me.GetType)
                xmlQodLine.Serialize(objStringWriter, Me)
                'Create a string reader and deserialise the XML into a new object (_tempQodLine)
                Dim objStringReader As New StringReader(objStringWriter.ToString)
                Dim _tempQodLine As QodLine = CType(xmlQodLine.Deserialize(objStringReader), QodLine)
                Return _tempQodLine
            Catch ex As Exception
                Return Nothing
            End Try

        End Function

        Public Sub New()
            MyBase.New()
            _DeliveryChangedStatus = (New DeliveryStatusChangeFactory).GetImplementation
        End Sub

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub

        Public Sub New(ByVal qod As QodHeader, ByVal line As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLine)
            MyBase.New()
            Me.OrderNumber = qod.Number
            Me.Number = line.SellingStoreLineNo.Value
            Me.SkuNumber = line.ProductCode.Value
            Me.SkuDescription = line.ProductDescription.Value
            Me.SkuUnitMeasure = line.UOM.Value
            Me.QtyOrdered = CInt(line.TotalOrderQuantity.Value)
            Me.QtyTaken = CInt(line.QuantityTaken.Value)
            Me.QtyToDeliver = Me.QtyOrdered - Me.QtyTaken
            Me.Price = line.SellingPrice.Value
            Me.IsDeliveryChargeItem = line.DeliveryChargeItem.Value
            Me.DeliveryStatus = CInt(line.LineStatus.Value)
            Me.SellingStoreId = qod.SellingStoreId
            Me.SellingStoreOrderId = qod.SellingStoreOrderId
            Me.DeliverySource = line.FulfilmentSite.Value
            Me.DeliverySourceIbtOut = line.FulfilmentSiteIBTOutNumber.Value
            Me.SellingStoreIbtIn = line.SellingStoreIBTInNumber.Value
            Me.IsCancellable = line.CancellableFlag
            If Me.IsDeliveryChargeItem Then Me.SetDeliveredStatusOk()
        End Sub

        Public Sub New(ByVal qod As QodHeader, ByVal line As OMOrderCreateResponseOrderLine)
            MyBase.New()
            Me.OrderNumber = qod.Number
            Me.SellingStoreId = qod.SellingStoreId
            Me.SellingStoreOrderId = qod.SellingStoreOrderId
            Me.Number = line.SellingStoreLineNo.Value
            Me.SkuNumber = line.ProductCode.Value
            Me.SkuDescription = line.ProductDescription.Value
            Me.SkuUnitMeasure = line.UOM.Value
            Me.QtyOrdered = CInt(line.TotalOrderQuantity.Value)
            Me.QtyTaken = CInt(line.QuantityTaken.Value)
            Me.QtyToDeliver = Me.QtyOrdered - Me.QtyTaken
            Me.Price = line.SellingPrice.Value
            Me.IsDeliveryChargeItem = line.DeliveryChargeItem.Value
            If Me.IsDeliveryChargeItem Then Me.SetDeliveredStatusOk()
        End Sub

        Public Sub New(ByVal qod As QodHeader, ByVal line As CTSQODCreateResponseOrderLine)
            MyBase.New()
            Me.OrderNumber = qod.Number
            Me.SellingStoreId = qod.SellingStoreId
            Me.SellingStoreOrderId = qod.SellingStoreOrderId
            Me.Number = line.SourceOrderLineNo.Value
            Me.SkuNumber = line.ProductCode.Value
            Me.SkuDescription = line.ProductDescription.Value
            Me.QtyOrdered = CInt(line.TotalOrderQuantity.Value)
            Me.QtyToDeliver = Me.QtyOrdered - Me.QtyTaken
            Me.Price = line.SellingPrice.Value
            Me.IsDeliveryChargeItem = line.DeliveryChargeItem.Value
            If Me.IsDeliveryChargeItem Then Me.SetDeliveredStatusOk()
        End Sub

        Public Sub New(ByVal qodCreateLine As WebService.CTSQODCreateResponseOrderLine)
            MyBase.New()
            Me.SkuNumber = qodCreateLine.ProductCode.Value
            Me.SkuDescription = qodCreateLine.ProductDescription.Value
            Me.Price = qodCreateLine.SellingPrice.Value
            Me.IsDeliveryChargeItem = qodCreateLine.DeliveryChargeItem.Value
            Me.QtyOrdered = CInt(qodCreateLine.TotalOrderQuantity.Value)
            Me.QtyRefunded = 0
            Me.QuantityScanned = 0
            Me.QtyTaken = 0
            If Me.IsDeliveryChargeItem Then
                Me.QtyToDeliver = 0
            Else
                Me.QtyToDeliver = Me.QtyOrdered
            End If
        End Sub

        Public Function HasChanged(ByVal configxml As XmlNode, ByVal TempLine As QodLine) As Boolean
            Try
                Dim myType As Global.System.Type = Me.GetType
                Dim myProperties() As PropertyInfo = Me.GetType.GetProperties
                Dim PropertyNames As XmlNodeList = configxml.SelectNodes("QodLine/Properties/Name")
                Dim PropertyItem As PropertyInfo

                For Each PropertyName As XmlNode In PropertyNames
                    PropertyItem = Me.GetType.GetProperty(PropertyName.InnerText)
                    Try
                        If PropertyItem.GetValue(Me, Nothing) IsNot Nothing AndAlso PropertyItem.GetValue(TempLine, Nothing) IsNot Nothing Then
                            If PropertyItem.GetValue(Me, Nothing).ToString <> PropertyItem.GetValue(TempLine, Nothing).ToString Then
                                Return True
                            End If
                        ElseIf PropertyItem.GetValue(Me, Nothing) Is Nothing And PropertyItem.GetValue(TempLine, Nothing) IsNot Nothing Then
                            Return True
                        ElseIf PropertyItem.GetValue(Me, Nothing) IsNot Nothing And PropertyItem.GetValue(TempLine, Nothing) Is Nothing Then
                            Return True
                        End If
                    Catch
                    End Try
                Next
            Catch
                Return True
            End Try
            Return False
        End Function

        Public Function IsPickedAndNotDespatched() As Boolean
            Return (Me.DeliveryStatus >= Delivery.PickingStatusOk AndAlso Me.DeliveryStatus < Delivery.DespatchCreated)
        End Function

        Public Function IsRefundable() As Boolean
            Return (Me.DeliveryStatus < Delivery.DespatchCreated)
        End Function

        Public Function HasHadOrderRequestAccepted() As Boolean
            Return (Me.DeliveryStatus >= Delivery.DeliveryRequestAccepted)
        End Function

        Public Function HasDoneIbtOut() As Boolean
            Return (Me.DeliveryStatus >= Delivery.IbtOutAllStock)
        End Function

        Public Function HasDoneIbtIn() As Boolean
            Return (Me.DeliveryStatus >= Delivery.IbtInAllStock)
        End Function

        Public Function HasDonePicking() As Boolean
            Return (Me.DeliveryStatus >= Delivery.PickingListCreated)
        End Function

        Public Function HasDoneDespatch() As Boolean
            Return (Me.DeliveryStatus >= Delivery.DespatchCreated)
        End Function

        Public Function HasDoneUndelivered() As Boolean
            Return (Me.DeliveryStatus >= Delivery.UndeliveredCreated)
        End Function

        Public Function HasDoneDelivered() As Boolean
            Return (Me.DeliveryStatus >= Delivery.DeliveredCreated)
        End Function

        Public Function HasDeliveredOk() As Boolean
            Return (Me.DeliveryStatus >= Delivery.DeliveredStatusOk)
        End Function

        Public Function RequiresPickingStatusOk() As Boolean
            Return ((Me.DeliveryStatus >= Delivery.PickingNotifyOk) AndAlso (Me.DeliveryStatus <= Delivery.PickingStatusFailedData))
        End Function

        Public Function RequiresPickConfirmationStatusOk() As Boolean
            Return ((Me.DeliveryStatus >= Delivery.PickingConfirmNotifyOk) AndAlso (Me.DeliveryStatus <= Delivery.PickingConfirmStatusFailedData))
        End Function

        Public Function RequiresDespatchStatusOk() As Boolean
            Return ((Me.DeliveryStatus >= Delivery.DespatchNotifyOk) AndAlso (Me.DeliveryStatus <= Delivery.DespatchStatusFailedData))
        End Function

        Public Function RequiresUndeliveredStatusOk() As Boolean
            Return ((Me.DeliveryStatus >= Delivery.UndeliveredNotifyOk) AndAlso (Me.DeliveryStatus <= Delivery.UndeliveredStatusFailedData))
        End Function

        Public Function RequiresDeliveredStatusOk() As Boolean
            Return ((Me.DeliveryStatus >= Delivery.DeliveredNotifyOk) AndAlso (Me.DeliveryStatus <= Delivery.DeliveredStatusFailedData))
        End Function

        Public Function RequiresPickingNotifyOk() As Boolean
            Return ((Me.DeliveryStatus >= Delivery.PickingListCreated) AndAlso (Me.DeliveryStatus <= Delivery.PickingNotifyFailedData))
        End Function

        Public Function RequiresPickConfirmNotifyOk() As Boolean
            Return ((Me.DeliveryStatus >= Delivery.PickingConfirmCreated) AndAlso (Me.DeliveryStatus <= Delivery.PickingConfirmNotifyFailedData))
        End Function

        Public Function RequiresDespatchNotifyOk() As Boolean
            Return ((Me.DeliveryStatus >= Delivery.DespatchCreated) AndAlso (Me.DeliveryStatus <= Delivery.DespatchNotifyFailedData))
        End Function

        Public Function RequiresUndeliveredNotifyOk() As Boolean
            Return ((Me.DeliveryStatus >= Delivery.UndeliveredCreated) AndAlso (Me.DeliveryStatus <= Delivery.UndeliveredNotifyFailedData))
        End Function

        Public Function RequiresDeliveredNotifyOk() As Boolean
            Return ((Me.DeliveryStatus >= Delivery.DeliveredCreated) AndAlso (Me.DeliveryStatus <= Delivery.DeliveredNotifyFailedData))
        End Function

        Public Sub SetDoneIbtOut()
            If Not HasDoneIbtOut() Then Me.DeliveryStatus = Delivery.IbtOutAllStock
        End Sub

        Public Sub SetDoneIbtIn()
            If Not HasDoneIbtIn() Then Me.DeliveryStatus = Delivery.IbtInAllStock
        End Sub

        Public Sub SetDeliveryNextNotifyOk()
            If RequiresPickingNotifyOk() Then
                SetPickingNotifyOk()
                Exit Sub
            End If

            If _DeliveryChangedStatus.PickingConfirmStatusAvailable Then
                If RequiresPickConfirmNotifyOk() Then
                    SetPickingConfirmationNotifyOk()
                    Exit Sub
                End If
            End If

            If RequiresDespatchNotifyOk() Then
                SetDespatchNotifyOk()
                Exit Sub
            End If

            If RequiresUndeliveredNotifyOk() Then
                SetUndeliveredNotifyOk()
                Exit Sub
            End If

            If RequiresDeliveredNotifyOk() Then
                SetDeliveredNotifyOk()
                Exit Sub
            End If

        End Sub

        Public Sub SetPickingNotifyOk()
            If Me.DeliveryStatus < Delivery.PickingNotifyOk Then Me.DeliveryStatus = Delivery.PickingNotifyOk
        End Sub

        Public Sub SetPickingConfirmationNotifyOk()
            If Me.DeliveryStatus < Delivery.PickingConfirmNotifyOk Then Me.DeliveryStatus = Delivery.PickingConfirmNotifyOk
        End Sub

        Public Sub SetDespatchNotifyOk()
            If Me.DeliveryStatus < Delivery.DespatchNotifyOk Then Me.DeliveryStatus = Delivery.DespatchNotifyOk
        End Sub

        Public Sub SetUndeliveredNotifyOk()
            If Me.DeliveryStatus < Delivery.UndeliveredNotifyOk Then Me.DeliveryStatus = Delivery.UndeliveredNotifyOk
        End Sub

        Public Sub SetDeliveredNotifyOk()
            If Me.DeliveryStatus < Delivery.DeliveredNotifyOk Then Me.DeliveryStatus = Delivery.DeliveredNotifyOk
        End Sub

        Public Sub SetDeliveryNextStatusOk()

            If RequiresPickingStatusOk() Then
                SetPickingStatusOk()
                Exit Sub
            End If

            If _DeliveryChangedStatus.PickingConfirmStatusAvailable Then
                If RequiresPickConfirmationStatusOk() Then
                    SetPickingConfirmationStatusOk()
                    Exit Sub
                End If
            End If

            If RequiresDespatchStatusOk() Then
                SetDespatchStatusOk()
                Exit Sub
            End If

            If RequiresUndeliveredStatusOk() Then
                SetUndeliveredStatusOk()
                Exit Sub
            End If

            If RequiresDeliveredStatusOk() Then
                SetDeliveredStatusOk()
                Exit Sub
            End If

        End Sub

        Public Sub SetPickingStatusOk()
            If Me.DeliveryStatus < Delivery.PickingStatusOk Then Me.DeliveryStatus = Delivery.PickingStatusOk
        End Sub

        Public Sub SetPickingConfirmationStatusOk()
            If Me.DeliveryStatus < Delivery.PickingConfirmStatusOk Then Me.DeliveryStatus = Delivery.PickingConfirmStatusOk
        End Sub

        Public Sub SetDespatchStatusOk()
            If Me.DeliveryStatus < Delivery.DespatchStatusOk Then Me.DeliveryStatus = Delivery.DespatchStatusOk
        End Sub

        Public Sub SetUndeliveredStatusOk()
            If Me.DeliveryStatus < Delivery.UndeliveredStatusOk Then Me.DeliveryStatus = Delivery.UndeliveredStatusOk
        End Sub

        Public Sub SetDeliveredStatusOk()
            If Me.DeliveryStatus < Delivery.DeliveredStatusOk Then Me.DeliveryStatus = Delivery.DeliveredStatusOk
        End Sub

        ''' <summary>
        ''' Set Delivery status to the value passed in or if an error status or the current status to reflect the failure
        ''' </summary>
        ''' <param name="newStatus"></param>
        ''' <returns>Me.DeliveryStaus</returns>
        ''' <history>
        ''' <created author="Michael O'Cain" date="17/11/2010"></created>
        ''' </history>
        ''' <remarks></remarks>
        Public Function setDeliveryStatus(ByVal newStatus As Integer, Optional ByVal StoreId As Integer = 0) As Integer

            If Me.DeliveryStatus = Delivery.DeliveredStatusOk Then
                Return Me.DeliveryStatus
            End If

            If StoreId <> 0 AndAlso Not String.IsNullOrEmpty(Me.DeliverySource) AndAlso _
                StoreId <> CInt(Me.DeliverySource.ToString) Then
                Return Me.DeliveryStatus
            End If

            If newStatus >= State.Delivery.OrderFailedConnection And newStatus <= Delivery.OrderFailedData Then
                Me.DeliveryStatus = CInt((Me.DeliveryStatus \ 100) & newStatus.ToString)
                Return Me.DeliveryStatus
            End If

            If newStatus > Me.DeliveryStatus Then
                Me.DeliveryStatus = newStatus
            ElseIf Me.DeliveryStatus > State.Delivery.DespatchStatusOk AndAlso Me.DeliveryStatus < State.Delivery.DeliveredCreated Then
                Me.DeliveryStatus = newStatus
            End If

            Return Me.DeliveryStatus

        End Function

        ''' <summary>
        ''' Copied from QodHeader to allow lines to report on their own Delivery State
        ''' </summary>
        ''' <value></value>
        ''' <history>
        ''' <created author="Charles McMahon" date="07/10/2010"></created>
        ''' </history>
        ''' <remarks></remarks>
        Public Property DeliveryState() As State.Delivery
            Get
                Return CType(Me.DeliveryStatus, State.Delivery)
            End Get
            Set(ByVal value As State.Delivery)
                Me.DeliveryStatus = CInt(value)
            End Set
        End Property

        ''' <summary>
        ''' Copied from QodHeader to allow lines to report on their own Delivery State Description
        ''' </summary>
        ''' <value></value>
        ''' <history>
        ''' <created author="Charles McMahon" date="07/10/2010"></created>
        ''' </history>
        ''' <remarks></remarks>
        Public ReadOnly Property DeliveryStateDescription() As String
            Get
                Return State.DeliveryDescription.GetDescription(Me.DeliveryState)
            End Get
        End Property

        ''' <summary>
        ''' Copied from QodHeader to allow lines to report on their own Delivery State Phase
        ''' </summary>
        ''' <value></value>
        ''' <history>
        ''' <created author="Charles McMahon" date="07/10/2010"></created>
        ''' </history>
        ''' <remarks></remarks>
        Public ReadOnly Property DeliveryStatePhase() As String
            Get
                Return State.DeliveryDescription.GetPhase(Me.DeliveryState)
            End Get
        End Property

        Public Function SelfPersist() As Integer
            Dim LinesUpdated As Integer = 0
            Using con As New Connection
                Try
                    con.StartTransaction()
                    LinesUpdated = Persist(con)
                    con.CommitTransaction()
                Catch ex As Exception
                    con.RollbackTransaction()
                End Try
            End Using
            Return LinesUpdated
        End Function

        Public Function Persist(ByVal con As Connection) As Integer
            If Not ExistsInDB Then
                Return PersistNew(con)
            Else
                Dim LinesUpdated As Integer = 0
                Dim sb As StringBuilder
                Using com As New Command(con)
                    Select Case con.DataProvider
                        Case DataProvider.Odbc
                            sb = New StringBuilder
                            sb.Append("Update CORLIN set ")
                            sb.Append("QTYO=?,QTYT=?,QTYR=?,DeliveryStatus=?,QtyToBeDelivered=?,")
                            sb.Append("DeliverySource=?,DeliverySourceIbtOut=?,SellingStoreIbtIn=?,QuantityScanned=? ")
                            sb.Append("where NUMB=? and LINE=?")

                            com.CommandText = sb.ToString
                            com.AddParameter("QTYO", Me.QtyOrdered)
                            com.AddParameter("QTYT", Me.QtyTaken)
                            com.AddParameter("QTYR", Me.QtyRefunded)
                            com.AddParameter("DeliveryStatus", Me.DeliveryStatus)
                            com.AddParameter("QtyDelivered", Me.QtyToDeliver)
                            com.AddParameter("DelSource", IIf(Me.DeliverySource IsNot Nothing, Me.DeliverySource, ""))
                            com.AddParameter("DeliverySourceIbtOut", IIf(Me.DeliverySourceIbtOut IsNot Nothing, Me.DeliverySourceIbtOut, ""))
                            com.AddParameter("SellingStoreIbtIn", IIf(Me.SellingStoreIbtIn IsNot Nothing, Me.SellingStoreIbtIn, ""))
                            'added for hubs2
                            com.AddParameter("QuantityScanned", QuantityScanned)

                            com.AddParameter("NUMB", Me.OrderNumber)
                            com.AddParameter("LINE", Me.Number)
                            LinesUpdated += com.ExecuteNonQuery()

                        Case DataProvider.Sql
                            com.StoredProcedureName = My.Resources.Procedures.SaleOrderLineUpdate
                            com.AddParameter(My.Resources.Parameters.OrderNumber, Me.OrderNumber)
                            com.AddParameter(My.Resources.Parameters.Number, Me.Number)
                            com.AddParameter(My.Resources.Parameters.QtyOrdered, Me.QtyOrdered)
                            com.AddParameter(My.Resources.Parameters.QtyTaken, Me.QtyTaken)
                            com.AddParameter(My.Resources.Parameters.QtyRefunded, Me.QtyRefunded)
                            com.AddParameter(My.Resources.Parameters.DeliveryStatus, Me.DeliveryStatus)
                            com.AddParameter(My.Resources.Parameters.QtyToDeliver, Me.QtyToDeliver)
                            'added for hubs2
                            com.AddParameter(My.Resources.Parameters.QtyScanned, Me.QuantityScanned)
                            If Me.DeliverySource IsNot Nothing Then com.AddParameter(My.Resources.Parameters.DeliverySource, Me.DeliverySource)
                            If Me.DeliverySourceIbtOut IsNot Nothing Then com.AddParameter(My.Resources.Parameters.DeliverySourceIbtOut, Me.DeliverySourceIbtOut)
                            If Me.SellingStoreIbtIn IsNot Nothing Then com.AddParameter(My.Resources.Parameters.SellingStoreIbtIn, Me.SellingStoreIbtIn)
                            LinesUpdated += com.ExecuteNonQuery()
                    End Select
                End Using
                Return LinesUpdated
            End If

        End Function

        Private Function PersistNew(ByVal con As Connection) As Integer
            Dim LinesUpdated As Integer = 0
            Dim sb As StringBuilder
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        sb = New StringBuilder
                        sb.Append("Insert into CORLIN (")
                        sb.Append("NUMB,LINE,SKUN,QTYO,QTYT,QTYR,Price,IsDeliveryChargeItem,WGHT,VOLU,PORC,")
                        sb.Append("DeliveryStatus,SellingStoreId,SellingStoreOrderId,QtyToBeDelivered,DeliverySource,")
                        sb.Append("DeliverySourceIbtOut, SellingStoreIbtIn, QuantityScanned, RequiredFulfiller")
                        sb.Append(") values	(")
                        sb.Append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )")

                        com.CommandText = sb.ToString
                        com.AddParameter("OrderNumber", Me.OrderNumber)
                        com.AddParameter("Number", Me.Number)
                        com.AddParameter("SkuNumber", Me.SkuNumber)
                        com.AddParameter("QtyOrdered", Me.QtyOrdered)
                        com.AddParameter("QtyTaken", Me.QtyTaken)
                        com.AddParameter("QtyRefunded", Me.QtyRefunded)
                        com.AddParameter("Price", Me.Price)
                        com.AddParameter("IsDeliveryChargeItem", Me.IsDeliveryChargeItem)
                        com.AddParameter("Weight", Me.Weight)
                        com.AddParameter("Volume", Me.Volume)
                        com.AddParameter("PriceOverrideCode", Me.PriceOverrideCode)
                        com.AddParameter("DeliveryStatus", Me.DeliveryStatus)
                        com.AddParameter("SellingStoreId", Me.SellingStoreId)
                        com.AddParameter("SellingStoreOrderId", Me.SellingStoreOrderId)
                        com.AddParameter("QtyToBeDelivered", Me.QtyToDeliver)
                        com.AddParameter("DeliverySource", IIf(Me.DeliverySource IsNot Nothing, Me.DeliverySource, ""))
                        com.AddParameter("DeliverySourceIbtOut", IIf(Me.DeliverySourceIbtOut IsNot Nothing, Me.DeliverySourceIbtOut, ""))
                        com.AddParameter("SellingStoreIbtIn", IIf(Me.SellingStoreIbtIn IsNot Nothing, Me.SellingStoreIbtIn, ""))
                        'added for hubs2
                        com.AddParameter("QuantityScanned", Me.QuantityScanned)
                        com.AddParameter("RequiredFulfiller", Me.RequiredFulfiller)

                        LinesUpdated += com.ExecuteNonQuery()

                        'insert corlin2
                        sb = New StringBuilder
                        sb.Append("insert into CORLIN2 (NUMB, LINE, SourceOrderLineNo) values ( ?, ?, ? )")
                        com.ClearParamters()
                        com.CommandText = sb.ToString

                        com.AddParameter("NUMB", Me.OrderNumber)
                        com.AddParameter("Number", Me.Number)
                        com.AddParameter("SourceOrderLineNo", Me.SourceOrderLineNo)

                        LinesUpdated += com.ExecuteNonQuery

                    Case DataProvider.Sql
                        com.StoredProcedureName = My.Resources.Procedures.SaleOrderLineInsert
                        com.AddParameter(My.Resources.Parameters.OrderNumber, Me.OrderNumber)
                        com.AddParameter(My.Resources.Parameters.Number, Me.Number)
                        com.AddParameter(My.Resources.Parameters.SkuNumber, Me.SkuNumber)
                        com.AddParameter(My.Resources.Parameters.QtyOrdered, Me.QtyOrdered)
                        com.AddParameter(My.Resources.Parameters.QtyTaken, Me.QtyTaken)
                        com.AddParameter(My.Resources.Parameters.QtyRefunded, Me.QtyRefunded)
                        com.AddParameter(My.Resources.Parameters.Price, Me.Price)
                        com.AddParameter(My.Resources.Parameters.IsDeliveryChargeItem, Me.IsDeliveryChargeItem)
                        com.AddParameter(My.Resources.Parameters.Weight, Me.Weight)
                        com.AddParameter(My.Resources.Parameters.Volume, Me.Volume)
                        com.AddParameter(My.Resources.Parameters.PriceOverrideCode, Me.PriceOverrideCode)
                        com.AddParameter(My.Resources.Parameters.DeliveryStatus, Me.DeliveryStatus)
                        com.AddParameter(My.Resources.Parameters.SellingStoreId, Me.SellingStoreId)
                        com.AddParameter(My.Resources.Parameters.SellingStoreOrderId, Me.SellingStoreOrderId)
                        com.AddParameter(My.Resources.Parameters.QtyToDeliver, Me.QtyToDeliver)

                        'Added for hubs 2.0
                        com.AddParameter(My.Resources.Parameters.QtyScanned, Me.QuantityScanned)
                        com.AddParameter(My.Resources.Parameters.SourceOrderLineNo, Me.SourceOrderLineNo)

                        If Me.DeliverySource IsNot Nothing Then com.AddParameter(My.Resources.Parameters.DeliverySource, Me.DeliverySource)
                        If Me.DeliverySourceIbtOut IsNot Nothing Then com.AddParameter(My.Resources.Parameters.DeliverySourceIbtOut, Me.DeliverySourceIbtOut)
                        If Me.SellingStoreIbtIn IsNot Nothing Then com.AddParameter(My.Resources.Parameters.SellingStoreIbtIn, Me.SellingStoreIbtIn)

                        com.AddParameter("@RequiredFulfiller", Me.RequiredFulfiller)

                        LinesUpdated += com.ExecuteNonQuery()
                End Select
            End Using
            Return LinesUpdated
        End Function

    End Class

End Namespace