﻿Imports System.Collections.Generic

Namespace Qod

    ''' <summary>
    ''' List of print lines for the picking note
    ''' </summary>
    ''' <history>
    ''' <created author="Charles McMahon" date="01/09/2010">
    ''' Added to allow different print lines to contain different types of information, while sticking to the page overrun rules.
    ''' In this case price override lines are shown with only a modified description and a modified quantity display.
    ''' </created>
    ''' </history>
    ''' <remarks>This class allows the building of printlines for the pick list.  It allows the default values to be overridden in the
    ''' event of a price override so that thay lines print differently</remarks>
    Public Class QodPrintLineList
        Inherits List(Of QodPrintLine)
        ''' <summary>
        ''' Turns QodLines into PrintLines to be added to the list
        ''' </summary>
        ''' <param name="qodLine">The line that equivilent printline(s) have to be created from</param>
        ''' <history>
        ''' <created author="Charles McMahon" date="01/09/2010"></created>
        ''' </history>
        ''' <remarks>
        ''' This Add method actually takes a QodLine as a parameter and creates a list of PrintLines
        ''' By examining the SKU, Price and price override codes the mathod creates/add to a printline with the correct details
        ''' </remarks>
        Public Overloads Sub Add(ByVal qodLine As QodLine)
            'Take a Qod line and turn it into a print line
            Dim NewPrintLine As New QodPrintLine(qodLine, 0)
            Dim CurrentPrintLine As QodPrintLine = Nothing
            'See if we already have a printline for this SKU and Price
            If Me.Contains(NewPrintLine, New QodPrintLineEqualityComparer()) Then
                'Add the current quantity to the existing line
                Dim Finder As New QodPrintLineFinder(qodLine.SkuNumber, qodLine.Price)
                CurrentPrintLine = Me.Find(AddressOf Finder.CompareSkuLine)
                If CurrentPrintLine IsNot Nothing Then
                    CurrentPrintLine.Quantity += NewPrintLine.Quantity
                    CurrentPrintLine.QtyPicked += NewPrintLine.QtyPicked
                    CurrentPrintLine.DisplayQtyPicked = CurrentPrintLine.QtyPicked.ToString 'MO'C Ref: 791 - Incorrect Pick Quantity displayed
                    If qodLine.PriceOverrideCode > 0 Then
                        CurrentPrintLine.HasOverride = True
                    End If
                Else
                    'Something went wrong finding the correct line *this should never happen, but play it safe and a as a new line
                    MyBase.Add(NewPrintLine)
                    If qodLine.PriceOverrideCode > 0 Then
                        NewPrintLine.HasOverride = True
                    End If
                End If
            Else
                'Add the new print line to the list
                MyBase.Add(NewPrintLine)
                If qodLine.PriceOverrideCode > 0 Then
                    NewPrintLine.HasOverride = True
                End If
            End If
            If qodLine.PriceOverrideCode > 0 Then
                'Create another line for the price override
                NewPrintLine = New QodPrintLine(qodLine, qodLine.PriceOverrideCode)
                MyBase.Add(NewPrintLine)
            End If


            Dim FactoryGetSkuWeight As New FactoryGetSkuWeight
            Dim Weight As IGetSkuWeight = FactoryGetSkuWeight.FactoryGet()  'Referral 922: Get weight for the Sku ?
            NewPrintLine.Weight = Weight.GetSkuWeight(qodLine.SkuNumber)
            qodLine.Weight = NewPrintLine.Weight

            'Now make sure it is all sorted by SKU, price and price override code to keep it grouped nicely together
            MyBase.Sort(New QodPrintLineComparer)
        End Sub
    End Class

    ''' <summary>
    ''' Comparer for sorting print line lists
    ''' </summary>
    ''' <history>
    ''' <created author="Charles McMahon" date="01/09/2010"></created>
    ''' </history>
    ''' <remarks></remarks>
    Public Class QodPrintLineComparer
        Implements IComparer(Of QodPrintLine)
        ''' <summary>
        ''' Compare used by List.Sort
        ''' </summary>
        ''' <param name="x">QodPrintLine - the first line to compare with for sorting</param>
        ''' <param name="y">QodPrintLine - the second line to compare with for sorting</param>
        ''' <returns>-1 if x is less than y, 0 if they are the same and 1 if x is greater than y</returns>
        ''' <history>
        ''' <created author="Charles McMahon" date="01/09/2010"></created>
        ''' </history>
        ''' <remarks>
        ''' This compares the print lines based on three critera, firstly the SKU number, then the Price and finally the price override code
        ''' </remarks>
        Public Function Compare(ByVal x As QodPrintLine, ByVal y As QodPrintLine) As Integer Implements IComparer(Of QodPrintLine).Compare
            If x.SkuNumber < y.SkuNumber Then
                Return -1
            ElseIf x.SkuNumber > y.SkuNumber Then
                Return 1
            Else
                If x.Price < y.Price Then
                    Return -1
                ElseIf x.Price > y.Price Then
                    Return 1
                Else
                    If x.PriceOverride < y.PriceOverride Then
                        Return -1
                    ElseIf x.PriceOverride > y.PriceOverride Then
                        Return 1
                    Else
                        Return 0
                    End If
                End If
            End If
        End Function
    End Class

    ''' <summary>
    ''' Comparer for finding equality among print lines
    ''' </summary>
    ''' <history>
    ''' <created author="Charles McMahon" date="01/09/2010"></created>
    ''' </history>
    ''' <remarks>
    ''' For finding equal print lines based on SKU and price so that the quantities can be added to make one print line for the total
    ''' </remarks>
    Public Class QodPrintLineEqualityComparer
        Implements IEqualityComparer(Of QodPrintLine)

        Public Function Equals1(ByVal x As QodPrintLine, ByVal y As QodPrintLine) As Boolean Implements IEqualityComparer(Of QodPrintLine).Equals
            Return String.Equals(x.SkuNumber, y.SkuNumber) AndAlso Decimal.Equals(x.Price, y.Price)
        End Function

        Public Function GetHashCode1(ByVal obj As QodPrintLine) As Integer Implements IEqualityComparer(Of QodPrintLine).GetHashCode
            Return obj.ToString.ToLower.GetHashCode()
        End Function
    End Class

    ''' <summary>
    ''' Finder for locating an equal print line within a list
    ''' </summary>
    ''' <history>
    ''' <created author="Charles McMahon" date="01/09/2010"></created>
    ''' </history>
    ''' <remarks>
    ''' Compares based on SKU and price to allow the List class to return the correct print line
    ''' </remarks>
    Public Class QodPrintLineFinder
        Private _SkuNumber As String
        Private _Price As Decimal
        Public Sub New(ByVal skuNumber As String, ByVal price As Decimal)
            _SkuNumber = skuNumber
            _Price = price
        End Sub
        Public Function CompareSkuLine(ByVal qodPrintLine As QodPrintLine) As Boolean
            Return (_SkuNumber = qodPrintLine.SkuNumber) AndAlso (_Price = qodPrintLine.Price)
        End Function
    End Class

    ''' <summary>
    ''' Print line to be printed on the pick list
    ''' </summary>
    ''' <history>
    ''' <created author="Charles McMahon" date="01/09/2010"></created>
    ''' </history>
    ''' <remarks>
    ''' This stores the qodline details relevant for printing.  Has different outputs based on the HasOverride and IsOverride flags
    ''' </remarks>
    Public Class QodPrintLine
        Private _SkuNumber As String
        Private _PriceOverride As Integer
        Private _Description As String
        Private _Quantity As Integer
        Private _QtyPicked As Integer
        Private _DisplayQtyPicked As String
        Private _Price As Decimal
        Private _Weight As Decimal
        Private _Volume As Decimal
        Private _HasOverride As Boolean = False
        Private _IsOverride As Boolean = False

        Public Property SkuNumber() As String
            Get
                Return _SkuNumber
            End Get
            Set(ByVal value As String)
                _SkuNumber = value
            End Set
        End Property

        Public ReadOnly Property SkuNumberDisplay() As String
            Get
                If Me.IsOverride Then
                    Return " "
                Else
                    Return _SkuNumber
                End If
            End Get
        End Property

        Public Property PriceOverride() As Integer
            Get
                Return _PriceOverride
            End Get
            Set(ByVal value As Integer)
                _PriceOverride = value
            End Set
        End Property

        Public Property Description() As String
            Get
                Return _Description
            End Get
            Set(ByVal value As String)
                _Description = value
            End Set
        End Property

        Public Property Quantity() As Integer
            Get
                Return _Quantity
            End Get
            Set(ByVal value As Integer)
                _Quantity = value
            End Set
        End Property

        Public Property QtyPicked() As Integer
            Get
                Return _QtyPicked
            End Get
            Set(ByVal value As Integer)
                _QtyPicked = value
            End Set
        End Property

        Public Property DisplayQtyPicked() As String
            Get
                Return _DisplayQtyPicked
            End Get
            Set(ByVal value As String)
                _DisplayQtyPicked = value
            End Set
        End Property

        Public ReadOnly Property QuantityDisplay() As String
            Get
                If Me.IsOverride Then
                    Return "(" & Quantity.ToString & ")"
                ElseIf Me.HasOverride Then
                    Return Quantity.ToString & "*"
                Else
                    Return Quantity.ToString
                End If
            End Get
        End Property

        Public Property Price() As Decimal
            Get
                Return _Price
            End Get
            Set(ByVal value As Decimal)
                _Price = value
            End Set
        End Property

        Public ReadOnly Property PriceDisplay() As String
            Get
                If Me.IsOverride Then
                    Return " "
                Else
                    Return Price.ToString("0.00")
                End If
            End Get
        End Property

        Public Property Weight() As Decimal
            Get
                Return _Weight
            End Get
            Set(ByVal value As Decimal)
                _Weight = value
            End Set
        End Property

        Public ReadOnly Property WeightDisplay() As String
            Get
                If Me.IsOverride Then
                    Return " "
                Else
                    Return _Weight.ToString("0.00")
                End If
            End Get
        End Property

        Public Property Volume() As Decimal
            Get
                Return _Volume
            End Get
            Set(ByVal value As Decimal)
                _Volume = value
            End Set
        End Property

        Public ReadOnly Property VolumeDisplay() As String
            Get
                If Me.IsOverride Then
                    Return " "
                Else
                    Return _Volume.ToString("0.00")
                End If
            End Get
        End Property

        Public Property IsOverride() As Boolean
            Get
                Return _IsOverride
            End Get
            Set(ByVal value As Boolean)
                _IsOverride = value
            End Set
        End Property

        Public Property HasOverride() As Boolean
            Get
                Return _HasOverride
            End Get
            Set(ByVal value As Boolean)
                _HasOverride = value
            End Set
        End Property

        Public Sub New(ByVal qodLine As QodLine, ByVal priceOverride As Integer)

            Me.SkuNumber = qodLine.SkuNumber
            Me.Description = qodLine.SkuDescription
            Me.Quantity = qodLine.QtyToDeliver
            Me.QtyPicked = qodLine.QuantityScanned
            Me.DisplayQtyPicked = IIf(qodLine.QuantityScanned = 0, " ", qodLine.QuantityScanned).ToString
            Me.Price = qodLine.Price
            Me.Weight = qodLine.Weight
            Me.Volume = qodLine.Volume
            Me.PriceOverride = priceOverride

            If priceOverride > 0 Then
                'We need to get the override description from the database
                Me.IsOverride = True
                Dim qodPriceOverrideList As New QodPriceOverrideList
                Me.Description = "Price Override - " & qodPriceOverrideList.GetDescription(priceOverride)
            End If

        End Sub
    End Class

    ''' <summary>
    ''' Price override code and associated reason
    ''' </summary>
    ''' <history>
    ''' <created author="Charles McMahon" date="01/09/2010"></created>
    ''' </history>
    ''' <remarks></remarks>
    Public Class QodPriceOverride
        Inherits Oasys.Hubs.Core.Base
        Private _Code As Integer
        Private _Description As String
        <ColumnMapping("Code")> Public Property Code() As Integer
            Get
                Code = _Code
            End Get
            Set(ByVal value As Integer)
                _Code = value
            End Set
        End Property
        <ColumnMapping("Description")> Public Property Description() As String
            Get
                Description = _Description
            End Get
            Set(ByVal value As String)
                _Description = value
            End Set
        End Property
    End Class

    ''' <summary>
    ''' List of price override codes and associated descriptions
    ''' </summary>
    ''' <history>
    ''' <created author="Charles McMahon" date="01/09/2010"></created>
    ''' </history>
    ''' <remarks></remarks>
    Public Class QodPriceOverrideList
        Inherits BaseCollection(Of QodPriceOverride)
        Public Sub New()
            Dim dt As DataTable = DataAccess.GetSystemCodes(My.Resources.Strings.PriceOverrideCodeType)
            MyBase.Load(dt)
        End Sub
        Public Function GetDescription(ByVal code As Integer) As String
            Dim Description As String = ""
            For Each qodPriceOverride As QodPriceOverride In Me
                If qodPriceOverride.Code = code Then
                    Description = qodPriceOverride.Description
                    Exit For
                End If
            Next
            Return Description
        End Function
    End Class

End Namespace
