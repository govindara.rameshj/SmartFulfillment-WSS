﻿Imports Cts.Oasys.Core.Net4Replacements
Imports System.Data


Public Class EnableStates

    Private Class EnableState
        Inherits Base
        <ColumnMapping("State")> Public Property StateCode As String
        <ColumnMapping("Description")> Public Property Description As String
        <ColumnMapping("DeliveryStatus")> Public Property DeliveryStatus As Integer

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub
    End Class

    Private Shared states As Lazy(Of Dictionary(Of String, EnableState)) = New Lazy(Of Dictionary(Of String, EnableState))(Function() LoadAll())

    Private Shared Function LoadAll() As Dictionary(Of String, EnableState)
        Dim table = DataAccess.EnableStatesGetAll().AsEnumerable()
        LoadAll = table.Select(Function(row) New EnableState(row)).ToDictionary(Function(s) s.StateCode)
    End Function

    Public Shared Function GetDeliveryStatus(stateCode As String) As String
        If (stateCode Is Nothing OrElse Not states.Value.ContainsKey(stateCode)) Then
            GetDeliveryStatus = Nothing
            Exit Function
        End If
        GetDeliveryStatus = states.Value(stateCode).DeliveryStatus.ToString()
    End Function
End Class
