﻿Imports System.ComponentModel

Namespace Container

    <HideModuleName()> Public Module SharedFunctions

        Public Function GetByDateRange(ByVal startDate As Date, ByVal endDate As Date) As HeaderCollection
            Dim col As New HeaderCollection
            col.LoadByDateRange(startDate, endDate)
            Return col
        End Function

    End Module


    Public Class Header
        Inherits Base

#Region "Private Variables"
        Private _assemblyDepotNumber As String
        Private _number As String
        Private _dateDespatch As Date
        Private _description As String
        Private _despatchDepotNumber As String
        Private _vehicleLoadReference As String
        Private _numberLines As Integer
        Private _parentNumber As String
        Private _isReceived As Boolean
        Private _printRequired As Boolean
        Private _dateDelivery As Date
        Private _value As Decimal
        Private _automaticallyPrinted As Boolean
        Private _rejectedReason As String

        Private _lines As LineCollection = Nothing
#End Region

#Region "Properties"
        <ColumnMapping("AssemblyDepotNumber")> Public Property AssemblyDepotNumber() As String
            Get
                Return _assemblyDepotNumber
            End Get
            Private Set(ByVal value As String)
                _assemblyDepotNumber = value
            End Set
        End Property
        <ColumnMapping("Number")> Public Property Number() As String
            Get
                Return _number
            End Get
            Private Set(ByVal value As String)
                _number = value
            End Set
        End Property
        <ColumnMapping("DateDespatch")> Public Property DateDespatch() As Date
            Get
                Return _dateDespatch
            End Get
            Private Set(ByVal value As Date)
                _dateDespatch = value
            End Set
        End Property
        <ColumnMapping("Description")> Public Property Description() As String
            Get
                Return _description
            End Get
            Set(ByVal value As String)
                _description = value
            End Set
        End Property
        <ColumnMapping("DespatchDepotNumber")> Public Property DespatchDepotNumber() As String
            Get
                Return _despatchDepotNumber
            End Get
            Private Set(ByVal value As String)
                _despatchDepotNumber = value
            End Set
        End Property
        <ColumnMapping("VehicleLoadReference")> Public Property VehicleLoadReference() As String
            Get
                Return _vehicleLoadReference
            End Get
            Private Set(ByVal value As String)
                _vehicleLoadReference = value
            End Set
        End Property
        <ColumnMapping("NumberLines")> Public Property NumberLines() As Integer
            Get
                Return _numberLines
            End Get
            Private Set(ByVal value As Integer)
                _numberLines = value
            End Set
        End Property
        <ColumnMapping("ParentNumber")> Public Property ParentNumber() As String
            Get
                Return _parentNumber
            End Get
            Private Set(ByVal value As String)
                _parentNumber = value
            End Set
        End Property
        <ColumnMapping("IsReceived")> Public Property IsReceived() As Boolean
            Get
                Return _isReceived
            End Get
            Private Set(ByVal value As Boolean)
                _isReceived = value
            End Set
        End Property
        <ColumnMapping("PrintRequired")> Public Property PrintRequired() As Boolean
            Get
                Return _printRequired
            End Get
            Private Set(ByVal value As Boolean)
                _printRequired = value
            End Set
        End Property
        <ColumnMapping("DateDelivery")> Public Property DateDelivery() As Date
            Get
                Return _dateDelivery
            End Get
            Private Set(ByVal value As Date)
                _dateDelivery = value
            End Set
        End Property
        <ColumnMapping("Value")> Public Property Value() As Decimal
            Get
                Return _value
            End Get
            Private Set(ByVal value As Decimal)
                _value = value
            End Set
        End Property
        <ColumnMapping("AutomaticallyPrinted")> Public Property AutomaticallyPrinted() As Boolean
            Get
                Return _automaticallyPrinted
            End Get
            Private Set(ByVal value As Boolean)
                _automaticallyPrinted = value
            End Set
        End Property
        <ColumnMapping("RejectedReason")> Public Property RejectedReason() As String
            Get
                Return _rejectedReason
            End Get
            Private Set(ByVal value As String)
                _rejectedReason = value
            End Set
        End Property

        Public ReadOnly Property Lines() As LineCollection
            Get
                If _lines Is Nothing Then _lines = New LineCollection(Me)
                Return _lines
            End Get
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub

    End Class

    Public Class HeaderCollection
        Inherits BaseCollection(Of Header)

        Public Sub LoadByDateRange(ByVal startDate As Date, ByVal endDate As Date)
            Dim dt As DataTable = DataAccess.ContainerGetByDateRange(startDate, endDate)
            Me.Load(dt)
        End Sub

    End Class

    Public Class Line
        Inherits Oasys.Hubs.Core.Base

#Region "Private Variables"
        Private _assemblyDepotNumber As String
        Private _containerNumber As String
        Private _storePoNumber As String
        Private _storePoLineNumber As String
        Private _skuNumber As String
        Private _skuDescription As String
        Private _skuProductCode As String
        Private _skuPackSize As Integer
        Private _qty As Integer
        Private _price As Decimal
        Private _rejectedReason As String
#End Region

#Region "Properties"
        <ColumnMapping("AssemblyDepotNumber")> Public Property AssemblyDepotNumber() As String
            Get
                Return _assemblyDepotNumber
            End Get
            Private Set(ByVal value As String)
                _assemblyDepotNumber = value
            End Set
        End Property
        <ColumnMapping("Number")> Public Property Number() As String
            Get
                Return _containerNumber
            End Get
            Private Set(ByVal value As String)
                _containerNumber = value
            End Set
        End Property
        <ColumnMapping("StorePoNumber")> Public Property StorePoNumber() As String
            Get
                Return _storePoNumber
            End Get
            Set(ByVal value As String)
                _storePoNumber = value
            End Set
        End Property
        <ColumnMapping("StorePoLineNumber")> Public Property StorePoLineNumber() As String
            Get
                Return _storePoLineNumber
            End Get
            Private Set(ByVal value As String)
                _storePoLineNumber = value
            End Set
        End Property
        <ColumnMapping("SkuNumber")> Public Property SkuNumber() As String
            Get
                Return _skuNumber
            End Get
            Private Set(ByVal value As String)
                _skuNumber = value
            End Set
        End Property
        <ColumnMapping("SkuDescription")> Public Property SkuDescription() As String
            Get
                Return _skuDescription
            End Get
            Private Set(ByVal value As String)
                _skuDescription = value
            End Set
        End Property
        <ColumnMapping("SkuProductCode")> Public Property SkuProductCode() As String
            Get
                Return _skuProductCode
            End Get
            Private Set(ByVal value As String)
                _skuProductCode = value.Trim
            End Set
        End Property
        <ColumnMapping("SkuPackSize")> Public Property SkuPackSize() As Integer
            Get
                Return _skuPackSize
            End Get
            Private Set(ByVal value As Integer)
                _skuPackSize = value
            End Set
        End Property
        <ColumnMapping("Qty")> Public Property Qty() As Integer
            Get
                Return _qty
            End Get
            Private Set(ByVal value As Integer)
                _qty = value
            End Set
        End Property
        <ColumnMapping("Price")> Public Property Price() As Decimal
            Get
                Return _price
            End Get
            Private Set(ByVal value As Decimal)
                _price = value
            End Set
        End Property
        <ColumnMapping("RejectedReason")> Public Property RejectedReason() As String
            Get
                Return _rejectedReason
            End Get
            Private Set(ByVal value As String)
                _rejectedReason = value
            End Set
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub

    End Class

    Public Class LineCollection
        Inherits BindingList(Of Line)
        Private _containerHeader As Header

        Friend Sub New(ByRef containerHeader As Header)
            MyBase.New()
            _containerHeader = containerHeader
            LoadLines()
        End Sub

        Private Sub LoadLines()

            Dim dt As DataTable = DataAccess.ContainerGetLines(_containerHeader.AssemblyDepotNumber, _containerHeader.Number)
            For Each dr As DataRow In dt.Rows
                Me.Items.Add(New Line(dr))
            Next

        End Sub


        Public Function StockExists(ByVal skuNumber As String) As Boolean

            For Each line As Line In Me.Items
                If line.SkuNumber = skuNumber Then
                    Return True
                End If
            Next
            Return False

        End Function

        Public Function Qty() As Integer

            Dim q As Integer = 0
            For Each line As Line In Me.Items
                q += line.Qty
            Next

            Return q

        End Function

        Public Function Value() As Decimal

            Dim val As Decimal = 0
            For Each line As Line In Me.Items
                val += (line.Qty * line.Price)
            Next

            Return val

        End Function

    End Class

End Namespace