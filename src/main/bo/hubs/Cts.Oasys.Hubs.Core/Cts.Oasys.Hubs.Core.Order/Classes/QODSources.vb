﻿Public Class QODSources

    Private Class QodSource
        Inherits Base
        <ColumnMapping("SOURCE_CODE")> Public Property SourceCode As String
        <ColumnMapping("SHOW_IN_UI")> Public Property ShowInUI As String
        <ColumnMapping("SEND_UPDATES_TO_OM")> Public Property SendUpdatesToOM As String
        <ColumnMapping("ALLOW_REMOTE_CREATE")> Public Property AllowRemoteCreate As Boolean
        <ColumnMapping("IS_CLICK_AND_COLLECT")> Public Property IsClickAndCollect As Boolean

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub
    End Class

    Private sources As Dictionary(Of String, QodSource) = New Dictionary(Of String, QodSource)

    Public Shared Function LoadAll() As QODSources
        Dim result = New QODSources
        Dim table = DataAccess.QodSourceGetAll()

        For Each row As DataRow In table.Rows
            Dim source = New QodSource(row)
            result.sources(source.SourceCode) = source
        Next

        Return result
    End Function

    Public Function AllowRemoteCreate(sourceCode As String) As Boolean
        If (sourceCode Is Nothing) Then
            Return False
        Else
            Return sources(sourceCode).AllowRemoteCreate
        End If
    End Function

    Public Function IsClickAndCollect(sourceCode As String) As Boolean
        If (sourceCode Is Nothing) Then
            Return False
        Else
            Return sources(sourceCode).IsClickAndCollect
        End If
    End Function

End Class
