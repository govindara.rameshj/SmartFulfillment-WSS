﻿Namespace TpWickes

    Public Interface IQodAndSaleLightWeight

        Property RefundSequenceNo() As Nullable(Of Integer)
        Sub AddRefundSequenceNoSqlParameterObject(ByRef Cmd As Cts.Oasys.Hubs.Data.Command)

    End Interface

End Namespace