﻿Imports Cts.Oasys.Hubs.Core.Order.Qod

Public Interface IRefundHolding
    Function IsThisRefundToBeSkipped(ByVal QodRefund As QodRefund) As Boolean
End Interface
