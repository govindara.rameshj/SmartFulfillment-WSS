﻿Imports Cts.Oasys.Hubs.Core.Order.WebService
Imports Cts.Oasys.Hubs.Data

Public Interface IQODTextFullfilmentRePopulateFromXML
    Function SaveQODText(ByVal con As Connection, ByRef QODHeader As Qod.QodHeader) As Integer
    Function PopulateQODTextFromXML(ByVal response As CTSFulfilmentResponse, ByRef QODHeader As Qod.QodHeader) As Boolean
End Interface
