Imports System.Collections
Imports System.Collections.Generic


Public Interface IDeliveryLine
    ReadOnly Property LineNumber As Integer
    ReadOnly Property LineText As String
End Interface
