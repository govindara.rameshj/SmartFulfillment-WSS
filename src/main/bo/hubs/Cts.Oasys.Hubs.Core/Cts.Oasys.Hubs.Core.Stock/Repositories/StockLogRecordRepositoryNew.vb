﻿' TODO: Probably, this file is not used anywhere and should be removed
Imports System.Text
Imports Cts.Oasys.Hubs.Data

Public Class StockLogRecordRepositoryNew
    Implements IStockLogRecordRepository



    Public Function InsertStockLogRecord(ByVal stocklogrecord As StockLog, Optional ByVal connection As Data.Connection = Nothing) As Integer Implements IStockLogRecordRepository.InsertStockLogRecord
        Dim LinesUpdated As Integer = 0

        Using com As New Command(connection)
            Select Case connection.DataProvider
                Case DataProvider.Odbc
                    Dim sb As New StringBuilder
                    sb.Append("INSERT INTO STKLOG (DATE1, TIME, SKUN, TYPE, KEYS, ICOM, EEID, ")
                    sb.Append("SSTK, SRET, SMDN, SWTF, SPRI, ESTK, ERET, EMDN, EWTF, EPRI, DAYN, RTI)")
                    sb.Append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")

                    com.CommandText = sb.ToString
                    com.ClearParamters()
                    com.AddParameter("DATE1", stocklogrecord.StockLogDate)
                    com.AddParameter("TIME", stocklogrecord.StockLogTime)
                    com.AddParameter("SKUN", stocklogrecord.PartCode)
                    com.AddParameter("TYPE", stocklogrecord.StockLogType)
                    com.AddParameter("KEYS", stocklogrecord.StockLogKeys)
                    com.AddParameter("ICOM", 0)
                    com.AddParameter("EEID", stocklogrecord.StockLogCashier)
                    com.AddParameter("SSTK", stocklogrecord.StockLogStartingStock)
                    com.AddParameter("SRET", stocklogrecord.StockLogStartingOpenReturns)
                    com.AddParameter("SMDN", stocklogrecord.StockLogStartingMarkDown)
                    com.AddParameter("SWTF", stocklogrecord.StockLogStartingWriteOff)
                    com.AddParameter("SPRI", stocklogrecord.StockLogStartingPrice)
                    com.AddParameter("ESTK", stocklogrecord.StockLogEndingStock)
                    com.AddParameter("ERET", stocklogrecord.StockLogEndingOpenReturns)
                    com.AddParameter("EMDN", stocklogrecord.StockLogEndingMarkdown)
                    com.AddParameter("EWTF", stocklogrecord.StockLogEndingWriteOff)
                    com.AddParameter("EPRI", stocklogrecord.StockLogEndingPrice)
                    com.AddParameter("DAYN", stocklogrecord.StockLogDayNumber)
                    com.AddParameter("RTI", "S")
                    LinesUpdated += com.ExecuteNonQuery

                Case DataProvider.Sql
                    com.StoredProcedureName = My.Resources.Procedures.InsertStockLogForSku
                    com.AddParameter(My.Resources.Parameters.AdjustmentDate, stocklogrecord.StockLogDate)
                    com.AddParameter(My.Resources.Parameters.AdjustmentTime, stocklogrecord.StockLogTime)
                    com.AddParameter(My.Resources.Parameters.SkuNumber, stocklogrecord.PartCode)
                    com.AddParameter(My.Resources.Parameters.StockLogType, stocklogrecord.StockLogType)
                    com.AddParameter(My.Resources.Parameters.StockLogKeys, stocklogrecord.StockLogKeys)
                    com.AddParameter(My.Resources.Parameters.UserId, stocklogrecord.StockLogCashier)
                    com.AddParameter(My.Resources.Parameters.StartingStock, stocklogrecord.StockLogStartingStock)
                    com.AddParameter(My.Resources.Parameters.StartingReturns, stocklogrecord.StockLogStartingOpenReturns)
                    com.AddParameter(My.Resources.Parameters.StartingMarkdown, stocklogrecord.StockLogStartingMarkDown)
                    com.AddParameter(My.Resources.Parameters.StartingWriteOff, stocklogrecord.StockLogStartingWriteOff)
                    com.AddParameter(My.Resources.Parameters.StartingPrice, stocklogrecord.StockLogStartingPrice)
                    com.AddParameter(My.Resources.Parameters.EndingStock, stocklogrecord.StockLogEndingStock)
                    com.AddParameter(My.Resources.Parameters.EndingReturns, stocklogrecord.StockLogEndingOpenReturns)
                    com.AddParameter(My.Resources.Parameters.EndingMarkdown, stocklogrecord.StockLogEndingMarkdown)
                    com.AddParameter(My.Resources.Parameters.EndingWriteOff, stocklogrecord.StockLogEndingWriteOff)
                    com.AddParameter(My.Resources.Parameters.EndingPrice, stocklogrecord.StockLogEndingPrice)
                    com.AddParameter(My.Resources.Parameters.DayNumber, stocklogrecord.StockLogDayNumber)
                    LinesUpdated += com.ExecuteNonQuery

            End Select
        End Using

        Return Nothing
    End Function
    Public Function UpdateStmasRecord(ByVal stocklogrecord As StockLog, Optional ByVal connection As Connection = Nothing) As Integer Implements IStockLogRecordRepository.UpdateStmasRecord
        Dim LinesUpdated As Integer = 0


        Using com As New Command(connection)
            Select Case connection.DataProvider
                Case DataProvider.Odbc
                    Dim sb As New StringBuilder

                    'Update stkmas set " & lngQuantityAtHand & ", " & lngMarkDownQuantity & ", " & 
                    'lngUnitsSoldYesterday & ", " & lngUnitsSoldThisWeek & ", " & lngUnitsSoldThisPeriod & "," & 
                    'lngUnitsSoldThisYear & ", " & curValueSoldYesterday & ", " & curValueSoldThisWeek & ", SALV4 = " & curValueSoldThisPeriod & ", SALV6 = " 
                    '& curValueSoldThisYear & " where SKUN = '" & mPartCode & "'")
                    sb.Append("UPDATE STKMAS SET ")
                    sb.Append("ONHA = ?, ")
                    sb.Append("MDNQ = ?, ")
                    sb.Append("SALU1 = ?, ")
                    sb.Append("SALU2 = ?, ")
                    sb.Append("SALU4 = ?, ")
                    sb.Append("SALU6 = ?, ")
                    sb.Append("SALV1 = ?, ")
                    sb.Append("SALV2 = ?, ")
                    sb.Append("SALV4 = ?, ")
                    sb.Append("SALV6 = ? ")
                    sb.Append("WHERE SKUN = ? ")

                    com.CommandText = sb.ToString
                    com.ClearParamters()
                    com.AddParameter("ONHA", stocklogrecord.QuantityOnHand)
                    com.AddParameter("MDNQ", stocklogrecord.MarkDownQuantity)
                    com.AddParameter("SALU1", stocklogrecord.UnitsSoldYesterday)
                    com.AddParameter("SALU2", stocklogrecord.UnitsSoldThisWeek)
                    com.AddParameter("SALU4", stocklogrecord.UnitsSoldThisPeriod)
                    com.AddParameter("SALU6", stocklogrecord.UnitsSoldThisYear)
                    com.AddParameter("SALV1", stocklogrecord.ValueSoldYesterday)
                    com.AddParameter("SALV2", stocklogrecord.ValueSoldThisWeek)
                    com.AddParameter("SALV4", stocklogrecord.ValueSoldThisPeriod)
                    com.AddParameter("SALV6", stocklogrecord.ValueSoldThisYear)
                    com.AddParameter("SKUN", stocklogrecord.PartCode)
                    LinesUpdated += com.ExecuteNonQuery


                Case DataProvider.Sql
                    com.StoredProcedureName = My.Resources.Procedures.StockUpdateSTKMASStkLogValues
                    com.AddParameter(My.Resources.Parameters.ONHA, stocklogrecord.QuantityOnHand)
                    com.AddParameter(My.Resources.Parameters.MDNQ, stocklogrecord.MarkDownQuantity)
                    com.AddParameter(My.Resources.Parameters.SALU1, stocklogrecord.UnitsSoldYesterday)
                    com.AddParameter(My.Resources.Parameters.SALU2, stocklogrecord.UnitsSoldThisWeek)
                    com.AddParameter(My.Resources.Parameters.SALU4, stocklogrecord.UnitsSoldThisPeriod)
                    com.AddParameter(My.Resources.Parameters.SALU6, stocklogrecord.UnitsSoldThisYear)
                    com.AddParameter(My.Resources.Parameters.SALV1, stocklogrecord.ValueSoldYesterday)
                    com.AddParameter(My.Resources.Parameters.SALV2, stocklogrecord.ValueSoldThisWeek)
                    com.AddParameter(My.Resources.Parameters.SALV4, stocklogrecord.ValueSoldThisPeriod)
                    com.AddParameter(My.Resources.Parameters.SALV6, stocklogrecord.ValueSoldThisYear)
                    com.AddParameter(My.Resources.Parameters.PartCode, stocklogrecord.PartCode)
                    LinesUpdated += com.ExecuteNonQuery

            End Select
        End Using
        Return LinesUpdated
    End Function
End Class
