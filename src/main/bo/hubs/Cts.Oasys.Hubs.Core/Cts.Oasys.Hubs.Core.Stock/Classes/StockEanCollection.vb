﻿Imports System.ComponentModel

<CLSCompliant(True)> Public Class StockEanCollection
    Inherits BindingList(Of StockEan)

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal skuNumber As String)

        Dim dt As DataTable = DataAccess.GetStockEans(skuNumber)
        For Each dr As DataRow In dt.Rows
            Me.Items.Add(New StockEan(dr))
        Next

    End Sub

End Class