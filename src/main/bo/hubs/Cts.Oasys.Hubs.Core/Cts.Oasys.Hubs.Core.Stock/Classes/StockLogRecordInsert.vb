﻿Public Class StockLogRecordInsert
    Implements IStockLogRecord


    Public Function InsertStockLogRecord(ByVal stocklogrecord As StockLog, Optional ByVal connection As Connection = Nothing) As Integer Implements IStockLogRecord.InsertStockLogRecord

        Return (New StockLogRecordRepositoryFactory).GetImplementation.InsertStockLogRecord(stocklogrecord, connection)

    End Function

    Public Function UpdateStmasRecord(ByVal stocklogrecord As StockLog, Optional ByVal connection As Connection = Nothing) As Integer Implements IStockLogRecord.UpdateStmasRecord

        Return (New StockLogRecordRepositoryFactory).GetImplementation.UpdateStmasRecord(stocklogrecord, connection)

    End Function

End Class