﻿Public Class StockLogRecordFactory

    Private Shared _mFactoryMember As IStockLogRecord
    Private Const requirementID As Integer = -2960

    Public Shared Function RequirementEnabled() As Boolean

        Return Cts.Oasys.Hubs.Core.System.Parameter.GetBoolean(requirementID)

    End Function

    Public Shared Function FactoryGet() As IStockLogRecord

        If _mFactoryMember Is Nothing Then

            If RequirementEnabled() Then

                Return New StockLogRecordInsertNew()
            Else
                Return New StockLogRecordInsert()
            End If

        Else

            Return _mFactoryMember   'stub implementation

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IStockLogRecord)

        _mFactoryMember = obj

    End Sub
End Class
