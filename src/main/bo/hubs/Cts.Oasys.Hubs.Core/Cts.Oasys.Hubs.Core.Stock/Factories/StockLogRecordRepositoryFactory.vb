﻿Public Class StockLogRecordRepositoryFactory
    Inherits RequirementSwitchFactory(Of IStockLogRecordRepository)

    Public Overrides Function ImplementationA() As IStockLogRecordRepository

        Return New StockLogRecordRepositoryNew()

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()

        ImplementationA_IsActive = Enabled()

    End Function

    Public Overrides Function ImplementationB() As IStockLogRecordRepository

        Return New StockLogRecordRepository()

    End Function

#Region "Private Functions & Procedures"

    Friend Overridable Function Enabled() As Boolean

        Return CouponsRequirementEnabled() Or StockLogTransactionSafeRequirementEnabled()

    End Function

    Friend Overridable Function CouponsRequirementEnabled() As Boolean

        Return System.Parameter.GetBoolean(-2960)

    End Function

    Friend Overridable Function StockLogTransactionSafeRequirementEnabled() As Boolean

        Return System.Parameter.GetBoolean(-885)

    End Function

#End Region

End Class