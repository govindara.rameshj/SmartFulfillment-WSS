﻿Imports Cts.Oasys.Hubs.Data

Public Interface IStockLogRecordRepository

    Function InsertStockLogRecord(ByVal stocklogrecord As StockLog, Optional ByVal connection As Connection = Nothing) As Integer
    Function UpdateStmasRecord(ByVal stocklogrecord As StockLog, Optional ByVal connection As Connection = Nothing) As Integer

End Interface
