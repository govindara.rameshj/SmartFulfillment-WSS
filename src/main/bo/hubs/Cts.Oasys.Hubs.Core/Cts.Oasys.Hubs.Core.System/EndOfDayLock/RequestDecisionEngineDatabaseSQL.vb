﻿Imports System.Data.SqlClient
Imports Cts.Oasys.Hubs.Data
Imports WSS.BO.DataLayer.Model
Imports WSS.BO.DataLayer.Repositories

Namespace EndOfDayLock

    Public Class RequestDecisionEngineDatabaseSQL
        Implements IRequestDecisionEngineDatabase

        Private Shared _dataLayerFactory As IDataLayerFactory = Nothing

        Public Sub New(factory As IDataLayerFactory)

            _dataLayerFactory = factory

        End Sub

        Public Overridable Function NitmasCompleteTaskID() As Integer Implements IRequestDecisionEngineDatabase.NitmasCompleteTaskID

            Dim repository As DictionariesRepository
            repository = _dataLayerFactory.Create(Of DictionariesRepository)()

            NitmasCompleteTaskID = CType(repository.GetSettingStringValue(6001), Integer)

        End Function

        Public Overridable Function NitmasPendingTime() As Date Implements IRequestDecisionEngineDatabase.NitmasPendingTime

            Dim strSplit() As String

            Dim repository As DictionariesRepository
            repository = _dataLayerFactory.Create(Of DictionariesRepository)()

            strSplit = repository.GetSettingStringValue(6000).Split(CChar(":"))

            NitmasPendingTime = New DateTime(Now.Year, Now.Month, Now.Day, CType(strSplit(0), Integer), CType(strSplit(1), Integer), 0)

        End Function

        Public Function NitmasStarted(ByVal NitmasPending As Date) As Boolean Implements IRequestDecisionEngineDatabase.NitmasStarted
            Dim repository As NitmasRepository
            repository = _dataLayerFactory.Create(Of NitmasRepository)()
            NitmasStarted = repository.NitmasStarted(NitmasPending)
        End Function

        Public Function NitmasStopped(ByVal NitmasPending As Date, ByVal TaskID As Integer) As Boolean Implements IRequestDecisionEngineDatabase.NitmasStopped
            Dim repository As NitmasRepository
            repository = _dataLayerFactory.Create(Of NitmasRepository)()
            NitmasStopped = repository.NitmasStopped(NitmasPending, TaskID)
        End Function

        'Public Function NitmasStoppedDate(ByVal TaskID As Integer) As Date Implements IRequestDecisionEngineDatabase.NitmasStoppedDate
        '    Dim paramStoppedDate As SqlParameter

        '    Using con As New Connection
        '        Using com As New Command(con)
        '            com.StoredProcedureName = "NitmasStoppedDate"
        '            com.AddParameter("@TaskID", TaskID)
        '            paramStoppedDate = com.AddOutputParameter("@StoppedDate", SqlDbType.DateTime)

        '            com.ExecuteNonQuery()
        '        End Using
        '    End Using

        '    NitmasStoppedDate = CType(paramStoppedDate.Value, DateTime)
        'End Function

    End Class

End Namespace