﻿Imports System.Text
Imports Cts.Oasys.Hubs.Data

Namespace User

    <HideModuleName()> Public Module SharedFunctions

        Public Function GetUsers() As UserCollection
            Dim uc As New UserCollection
            uc.LoadAll()
            Return uc
        End Function

        Public Function GetUser(ByVal id As Integer) As User
            Dim dt As DataTable = DataAccess.UserGet(id)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                Return New User(dt.Rows(0))
            End If
            Return Nothing
        End Function

        Public Function GetUserIdName(ByVal userId As Integer) As String
            Dim dt As DataTable = DataAccess.UserGet(userId)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                Dim u As New User(dt.Rows(0))
                Return u.IdName
            End If
            Return String.Empty
        End Function

        Public Function IsManager(ByVal userId As Integer) As Boolean
            Dim dt As DataTable = DataAccess.UserGet(userId)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                Dim u As New User(dt.Rows(0))
                Return u.IsManager
            End If
            Return False
        End Function

    End Module

    Public Class User
        Inherits Base

#Region "Private Variables"
        Private _id As Integer
        Private _code As String
        Private _profileId As Integer
        Private _name As String
        Private _initials As String
        Private _position As String
        Private _payrollId As String
        Private _password As String
        Private _passwordExpires As Date
        Private _superPassword As String
        Private _superPasswordExpires As Nullable(Of Date)
        Private _outlet As String
        Private _isManager As Boolean
        Private _isSupervisor As Boolean
        Private _isDeleted As Boolean
        Private _deletedDate As Nullable(Of Date)
        Private _deletedBy As String
        Private _deletedWhere As String
        Private _tillReceiptName As String
        Private _languageCode As String

        Private _profile As Profile = Nothing
#End Region

#Region "Properties"
        <ColumnMapping("Id")> Public Property Id() As Integer
            Get
                Return _id
            End Get
            Private Set(ByVal value As Integer)
                _id = value
            End Set
        End Property
        <ColumnMapping("Code")> Public Property Code() As String
            Get
                Return _code
            End Get
            Private Set(ByVal value As String)
                _code = value
            End Set
        End Property
        <ColumnMapping("ProfileID")> Public Property ProfileId() As Integer
            Get
                Return _profileId
            End Get
            Set(ByVal value As Integer)
                _profileId = value
            End Set
        End Property
        <ColumnMapping("Name")> Public Property Name() As String
            Get
                Return _name
            End Get
            Set(ByVal value As String)
                _name = value
            End Set
        End Property
        <ColumnMapping("Initials")> Public Property Initials() As String
            Get
                Return _initials
            End Get
            Set(ByVal value As String)
                _initials = value
            End Set
        End Property
        <ColumnMapping("Position")> Public Property Position() As String
            Get
                Return _position
            End Get
            Set(ByVal value As String)
                _position = value
            End Set
        End Property
        <ColumnMapping("PayrollId")> Public Property PayrollId() As String
            Get
                Return _payrollId
            End Get
            Set(ByVal value As String)
                _payrollId = value
            End Set
        End Property
        <ColumnMapping("Password")> Public Property Password() As String
            Get
                Return _password
            End Get
            Set(ByVal value As String)
                _password = value
            End Set
        End Property
        <ColumnMapping("PasswordExpires")> Public Property PasswordExpires() As Date
            Get
                Return _passwordExpires
            End Get
            Set(ByVal value As Date)
                _passwordExpires = value
            End Set
        End Property
        <ColumnMapping("SuperPassword")> Public Property SuperPassword() As String
            Get
                Return _superPassword
            End Get
            Set(ByVal value As String)
                _superPassword = value
            End Set
        End Property
        <ColumnMapping("SuperPasswordExpires")> Public Property SuperPasswordExpires() As Nullable(Of Date)
            Get
                Return _superPasswordExpires
            End Get
            Set(ByVal value As Nullable(Of Date))
                _superPasswordExpires = value
            End Set
        End Property
        <ColumnMapping("Outlet")> Public Property Outlet() As String
            Get
                Return _outlet
            End Get
            Set(ByVal value As String)
                _outlet = value
            End Set
        End Property
        <ColumnMapping("IsManager")> Public Property IsManager() As Boolean
            Get
                Return _isManager
            End Get
            Set(ByVal value As Boolean)
                _isManager = value
            End Set
        End Property
        <ColumnMapping("IsSupervisor")> Public Property IsSupervisor() As Boolean
            Get
                Return _isSupervisor
            End Get
            Set(ByVal value As Boolean)
                _isSupervisor = value
            End Set
        End Property
        <ColumnMapping("IsDeleted")> Public Property IsDeleted() As Boolean
            Get
                Return _isDeleted
            End Get
            Private Set(ByVal value As Boolean)
                _isDeleted = value
            End Set
        End Property
        <ColumnMapping("DeletedDate")> Public Property DeletedDate() As Nullable(Of Date)
            Get
                Return _deletedDate
            End Get
            Private Set(ByVal value As Nullable(Of Date))
                _deletedDate = value
            End Set
        End Property
        <ColumnMapping("DeletedBy")> Public Property DeletedBy() As String
            Get
                Return _deletedBy
            End Get
            Private Set(ByVal value As String)
                _deletedBy = value
            End Set
        End Property
        <ColumnMapping("DeletedWhere")> Public Property DeletedWhere() As String
            Get
                Return _deletedWhere
            End Get
            Private Set(ByVal value As String)
                _deletedWhere = value
            End Set
        End Property
        <ColumnMapping("TillReceiptName")> Public Property TillReceiptName() As String
            Get
                Return _tillReceiptName
            End Get
            Set(ByVal value As String)
                _tillReceiptName = value
            End Set
        End Property
        <ColumnMapping("LanguageCode")> Public Property LanguageCode() As String
            Get
                Return _languageCode
            End Get
            Set(ByVal value As String)
                _languageCode = value
            End Set
        End Property

        Public ReadOnly Property Profile() As Profile
            Get
                If _profile Is Nothing Then LoadProfile()
                Return _profile
            End Get
        End Property
        Public ReadOnly Property IdName() As String
            Get
                Return Me.Id.ToString("000") & Space(1) & Me.Name
            End Get
        End Property
#End Region

        Public Sub New()
            MyBase.New()
            _id = DataAccess.UserGetNextId()
            _code = _id.ToString("000")
        End Sub

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub

        Public Sub LoadProfile()
            _profile = Profile.GetProfile(_profileId)
        End Sub


        Public Overrides Function ToString() As String
            Dim sb As New StringBuilder(_code & Space(1))
            If _initials.Trim.Length > 0 Then sb.Append(_initials.Trim & Space(1))
            sb.Append(_name.Trim)
            Return sb.ToString
        End Function

        Public Function Insert() As Integer
            Return DataAccess.UserInsert(Me)
        End Function

        Public Function Update() As Integer
            Return DataAccess.UserUpdate(Me)
        End Function

        Public Function Delete(ByVal byUserId As Integer, ByVal whereOutlet As Integer) As Integer
            _isDeleted = True
            _deletedBy = byUserId.ToString("000")
            _deletedWhere = whereOutlet.ToString("00")
            _deletedDate = Now.Date
            Return DataAccess.UserUpdate(Me)
        End Function

    End Class

    Public Class UserCollection
        Inherits BaseCollection(Of User)

        Public Sub LoadAll()
            Dim dt As DataTable = DataAccess.UserGet()
            Me.Load(dt)
        End Sub

    End Class

    Public Class Profile
        Inherits Base

#Region "       Table Fields"
        Private _id As Integer
        Private _description As String
        Private _daysPasswordValid As Integer
        Private _isDeleted As Boolean

        <ColumnMapping("Id")> Public Property Id() As Integer
            Get
                Return _id
            End Get
            Private Set(ByVal value As Integer)
                _id = value
            End Set
        End Property
        <ColumnMapping("Description")> Public Property Description() As String
            Get
                Return _description
            End Get
            Private Set(ByVal value As String)
                _description = value
            End Set
        End Property
        <ColumnMapping("DaysPasswordValid")> Public Property DaysPasswordValid() As Integer
            Get
                Return _daysPasswordValid
            End Get
            Private Set(ByVal value As Integer)
                _daysPasswordValid = value
            End Set
        End Property
        <ColumnMapping("IsDeleted")> Public Property IsDeleted() As Boolean
            Get
                Return _isDeleted
            End Get
            Private Set(ByVal value As Boolean)
                _isDeleted = value
            End Set
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub

        Public Shared Function GetProfiles() As ProfileCollection
            Dim uc As New ProfileCollection
            uc.LoadAll()
            Return uc
        End Function

        Public Shared Function GetProfile(ByVal profileId As Integer) As Profile
            Dim dt As DataTable = DataAccess.UserProfileGet(profileId)
            If dt.Rows.Count > 0 Then
                Return New Profile(dt.Rows(0))
            End If
            Return Nothing
        End Function

    End Class

    Public Class ProfileCollection
        Inherits BaseCollection(Of Profile)

        Public Overloads Sub LoadAll()
            Dim dt As DataTable = DataAccess.UserProfileGet()
            Me.Load(dt)
        End Sub

        Public Function Find(ByVal id As Integer) As Profile
            For Each up As Profile In Me.Items
                If up.Id = id Then Return up
            Next
            Return Nothing
        End Function

    End Class

    <HideModuleName()> Friend Module DataAccess

        Friend Function UserGet() As DataTable

            Using con As New Connection
                Using com As New Command(con)
                    Select Case con.DataProvider
                        Case DataProvider.Odbc
                            Dim sb As New StringBuilder
                            sb.Append("Select EEID as Id, ")
                            sb.Append("EEID as Code, ")
                            sb.Append("NAME as Name, ")
                            sb.Append("INIT as Initials ")
                            sb.Append("From SYSPAS")
                            com.CommandText = sb.ToString
                            Return com.ExecuteDataTable

                        Case DataProvider.Sql
                            com.StoredProcedureName = My.Resources.Procedures.UserGet
                            Return com.ExecuteDataTable

                        Case Else
                            Return Nothing
                    End Select
                End Using
            End Using

        End Function

        Friend Function UserGet(ByVal id As Integer) As DataTable

            Using con As New Connection
                Using com As New Command(con)
                    Select Case con.DataProvider
                        Case DataProvider.Odbc
                            Dim sb As New StringBuilder
                            sb.Append("Select EEID as Id, ")
                            sb.Append("EEID as Code, ")
                            sb.Append("NAME as Name, ")
                            sb.Append("INIT as Initials ")
                            sb.Append("From SYSPAS where EEID=?")
                            com.CommandText = sb.ToString
                            com.AddParameter("EEID", id.ToString("000"))
                            Return com.ExecuteDataTable

                        Case DataProvider.Sql
                            com.StoredProcedureName = My.Resources.Procedures.UserGet
                            com.AddParameter(My.Resources.Parameters.Id, id)
                            Return com.ExecuteDataTable

                        Case Else
                            Return Nothing
                    End Select
                End Using
            End Using

        End Function

        Friend Function UserProfileGet() As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.UserProfileGet)
                    Return com.ExecuteDataTable
                End Using
            End Using
            Return Nothing

        End Function

        Friend Function UserProfileGet(ByVal id As Integer) As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.UserProfileGet)
                    com.AddParameter(My.Resources.Parameters.Id, id)
                    Return com.ExecuteDataTable
                End Using
            End Using
            Return Nothing

        End Function


        Friend Function UserGetNextId() As Integer

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.UserGetNextId)
                    Return CInt(com.ExecuteValue)
                End Using
            End Using

        End Function

        Friend Function UserInsert(ByRef updateUser As User) As Integer

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.UserInsert)
                    com.AddParameter(My.Resources.Parameters.Id, updateUser.Id)
                    com.AddParameter(My.Resources.Parameters.EmployeeCode, updateUser.Code)
                    com.AddParameter(My.Resources.Parameters.Name, updateUser.Name)
                    com.AddParameter(My.Resources.Parameters.Initials, updateUser.Initials)
                    com.AddParameter(My.Resources.Parameters.Position, updateUser.Position)
                    com.AddParameter(My.Resources.Parameters.PayrollId, updateUser.PayrollId)
                    com.AddParameter(My.Resources.Parameters.Outlet, updateUser.Outlet)
                    com.AddParameter(My.Resources.Parameters.TillReceiptName, updateUser.TillReceiptName)
                    com.AddParameter(My.Resources.Parameters.LanguageCode, updateUser.LanguageCode)
                    com.AddParameter(My.Resources.Parameters.ProfileId, updateUser.ProfileId)
                    com.AddParameter(My.Resources.Parameters.Password, updateUser.Password)
                    com.AddParameter(My.Resources.Parameters.PasswordExpires, updateUser.PasswordExpires)
                    com.AddParameter(My.Resources.Parameters.IsManager, updateUser.IsManager)
                    com.AddParameter(My.Resources.Parameters.IsSupervisor, updateUser.IsSupervisor)

                    If (updateUser.SuperPassword IsNot Nothing) AndAlso (updateUser.SuperPassword.Trim.Length > 0) Then
                        com.AddParameter(My.Resources.Parameters.SuperPassword, updateUser.SuperPassword)
                        com.AddParameter(My.Resources.Parameters.SuperPasswordExpires, updateUser.SuperPasswordExpires)
                    End If

                    Return com.ExecuteNonQuery()
                End Using
            End Using

        End Function

        Friend Function UserUpdate(ByRef updateUser As User) As Integer

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.UserUpdate)
                    com.AddParameter(My.Resources.Parameters.Id, updateUser.Id)
                    com.AddParameter(My.Resources.Parameters.EmployeeCode, updateUser.Code)
                    com.AddParameter(My.Resources.Parameters.Name, updateUser.Name)
                    com.AddParameter(My.Resources.Parameters.Initials, updateUser.Initials)
                    com.AddParameter(My.Resources.Parameters.Position, updateUser.Position)
                    com.AddParameter(My.Resources.Parameters.PayrollId, updateUser.PayrollId)
                    com.AddParameter(My.Resources.Parameters.Outlet, updateUser.Outlet)
                    com.AddParameter(My.Resources.Parameters.TillReceiptName, updateUser.TillReceiptName)
                    com.AddParameter(My.Resources.Parameters.LanguageCode, updateUser.LanguageCode)
                    com.AddParameter(My.Resources.Parameters.ProfileId, updateUser.ProfileId)
                    com.AddParameter(My.Resources.Parameters.IsManager, updateUser.IsManager)
                    com.AddParameter(My.Resources.Parameters.IsSupervisor, updateUser.IsSupervisor)

                    If updateUser.IsDeleted Then
                        com.AddParameter(My.Resources.Parameters.IsDeleted, updateUser.IsDeleted)
                        com.AddParameter(My.Resources.Parameters.DeletedDate, updateUser.DeletedDate)
                        com.AddParameter(My.Resources.Parameters.DeletedBy, updateUser.DeletedBy)
                        com.AddParameter(My.Resources.Parameters.DeletedWhere, updateUser.DeletedWhere)
                    End If

                    If updateUser.Password IsNot Nothing Then
                        com.AddParameter(My.Resources.Parameters.Password, updateUser.Password)
                        com.AddParameter(My.Resources.Parameters.PasswordExpires, updateUser.PasswordExpires)
                    End If

                    If (updateUser.SuperPassword IsNot Nothing) AndAlso (updateUser.SuperPassword.Trim.Length > 0) Then
                        com.AddParameter(My.Resources.Parameters.SuperPassword, updateUser.SuperPassword)
                        com.AddParameter(My.Resources.Parameters.SuperPasswordExpires, updateUser.SuperPasswordExpires)
                    End If

                    Return com.ExecuteNonQuery()
                End Using
            End Using

        End Function

    End Module

End Namespace