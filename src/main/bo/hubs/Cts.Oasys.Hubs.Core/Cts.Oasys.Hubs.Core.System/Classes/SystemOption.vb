﻿Public Class SystemOption
    Inherits Oasys.Hubs.Core.Base

#Region "       Table Fields"
    Private _storeName As String
    Private _storeNumber As String
    Private _masterWorkstationId As Integer
    Private _accountabilityType As String
    Private _dateLastReformat As Date

    <ColumnMapping("StoreName")> Public Property StoreName() As String
        Get
            Return _storeName
        End Get
        Set(ByVal value As String)
            _storeName = value
        End Set
    End Property
    <ColumnMapping("StoreNumber")> Public Property StoreNumber() As String
        Get
            Return _storeNumber
        End Get
        Set(ByVal value As String)
            _storeNumber = value
        End Set
    End Property
    <ColumnMapping("MasterWorkstationId")> Public Property MasterWorkstationId() As Integer
        Get
            Return _masterWorkstationId
        End Get
        Set(ByVal value As Integer)
            _masterWorkstationId = value
        End Set
    End Property
    <ColumnMapping("AccountabilityType")> Public Property AccountabilityType() As String
        Get
            Return _accountabilityType
        End Get
        Set(ByVal value As String)
            _accountabilityType = value
        End Set
    End Property
    <ColumnMapping("DateLastReformat")> Public Property DateLastReformat() As Date
        Get
            Return _dateLastReformat
        End Get
        Set(ByVal value As Date)
            _dateLastReformat = value
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

    Private Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

    Public Shared Function GetSystemOption() As SystemOption
        Dim dt As DataTable = DataAccess.SystemOptionGet
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Return New SystemOption(dt.Rows(0))
        End If
        Return Nothing
    End Function

    Public Shared Function GetStoreId() As Integer
        Dim dt As DataTable = DataAccess.SystemOptionGet
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Return CInt(dt.Rows(0).Item(GetPropertyName(Function(f As SystemOption) f.StoreNumber)))
        End If
        Return 0
    End Function

    Public Shared Function GetIdName() As String
        Dim dt As DataTable = DataAccess.SystemOptionGet
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim id As Integer = CInt(dt.Rows(0).Item(GetPropertyName(Function(f As SystemOption) f.StoreNumber)))
            If id < 8000 Then id += 8000
            Dim name As String = dt.Rows(0).Item(GetPropertyName(Function(f As SystemOption) f.StoreName)).ToString
            Return id.ToString & Space(1) & name.Trim
        End If
        Return String.Empty
    End Function

    Public Shared Function GetMasterWorkstationId() As Integer
        Dim dt As DataTable = DataAccess.SystemOptionGet
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Return CInt(dt.Rows(0).Item(GetPropertyName(Function(f As SystemOption) f.MasterWorkstationId)))
        End If
        Return Nothing
    End Function

    Public Shared Function SetMasterOpen(ByVal masterOpen As Boolean) As Integer
        Return DataAccess.SystemNetworkUpdate(masterOpen)
    End Function

    Public Shared Function GetMasterOpen() As Boolean
        Return DataAccess.SystemNetworkGet
    End Function

    Public Shared Function GetAccountability() As String
        Dim dt As DataTable = DataAccess.SystemOptionGet
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Return CStr(dt.Rows(0).Item(GetPropertyName(Function(f As SystemOption) f.AccountabilityType)))
        End If
        Return Nothing
    End Function

    Public Shared Function UpdateLastReportDate(ByVal dateLastReformat As Date) As Boolean
        Return (DataAccess.SystemOptionUpdate(dateLastReformat) > 0)
    End Function

End Class

Public Structure Accountabilities
    Private value As String
    Public Shared Till As String = "T"
    Public Shared Cashier As String = "C"
End Structure