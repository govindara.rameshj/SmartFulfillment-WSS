﻿Imports Cts.Oasys.Core
Imports System.IO

Public Module AppConfiguration

    Public Function GetConfigurationFilePath() As String
        If GlobalVars.IsTestEnvironment Then
            ' always return sqlconnection
            Return GlobalVars.SystemEnvironment.GetConfigXmlDocumentPath()
        Else
            Return ConfigXMLDoc.Path
        End If
    End Function

    Public Function GetXsdFilesDirectoryPath() As String

        If GlobalVars.IsTestEnvironment Then
            Return GlobalVars.SystemEnvironment.GetXsdFilesDirectoryPath()
        Else
            Dim configValue As String = ConfigXMLDoc.SelectSingleNode("Configuration/XSDFiles/Path").InnerText

            Try
                If (IO.Path.IsPathRooted(configValue)) Then
                    Return configValue
                Else
                    ' Path is relative, try to resolve it relative to Config.xml file
                    Dim pathRelativeToConfig = Path.Combine(Path.GetDirectoryName(GetConfigurationFilePath()), configValue)
                    If (IO.Directory.Exists(pathRelativeToConfig)) Then
                        Return pathRelativeToConfig
                    Else
                        ' If file does not exist on relative path, dont return it. Let the caller decide what to do
                        Return configValue
                    End If
                End If
            Catch ex As Exception
                ' Just to be sure that this fix (relative path) wont crush the Live system in case or errors
                Return configValue
            End Try
        End If

    End Function

    Public Function GetInputXdsFileName(ByVal xsdFileType As String) As String
        If GlobalVars.IsTestEnvironment Then
            Return GlobalVars.SystemEnvironment.GetInputXsdFileName(xsdFileType)
        Else
            Dim xmlNodePath = String.Format("Configuration/XSDFiles/{0}/Input", xsdFileType)
            Return ConfigXMLDoc.SelectSingleNode(xmlNodePath).InnerText
        End If
    End Function

    Public Function GetOutputXdsFileName(ByVal xsdFileType As String) As String
        If GlobalVars.IsTestEnvironment Then
            Return GlobalVars.SystemEnvironment.GetOutputXsdFileName(xsdFileType)
        Else
            Dim xmlNodePath = String.Format("Configuration/XSDFiles/{0}/Output", xsdFileType)
            Return ConfigXMLDoc.SelectSingleNode(xmlNodePath).InnerText
        End If
    End Function

End Module
