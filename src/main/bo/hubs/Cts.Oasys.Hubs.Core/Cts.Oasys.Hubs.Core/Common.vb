﻿Imports System.Linq.Expressions
Imports System.Text
Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.Net4Replacements

<HideModuleName()> Public Module Common

    Private _ThisStore As System.Store.Store
    Public Property ThisStore() As System.Store.Store
        Get
            If _ThisStore Is Nothing Then
                _ThisStore = New System.Store.Store(0) 'Passing 0 gets the local store!
            End If
            Return _ThisStore
        End Get
        Set(value As System.Store.Store)
            _ThisStore = value
        End Set
    End Property

    Private _AllStores As System.Store.StoreCollection
    Public Property AllStores() As System.Store.StoreCollection
        Get
            If _AllStores Is Nothing Then
                _AllStores = New System.Store.StoreCollection
            End If
            Return _AllStores
        End Get
        Set(value As System.Store.StoreCollection)
            _AllStores = value
        End Set
    End Property

    Private _configXMLDoc As Lazy(Of ConfigXMLDocument) = New Lazy(Of ConfigXMLDocument)(Function() New ConfigXMLDocument())

    Public ReadOnly Property ConfigXMLDoc As ConfigXMLDocument
        Get
            Return _configXMLDoc.Value
        End Get
    End Property

    Public Class ConfigXMLDocument
        Inherits Xml.XmlDocument

        Public Path As String = ""

        Public Sub New()
            MyBase.New()
            Dim resolver As ConfigPathResolver = New ConfigPathResolver()
            Path = resolver.ResolveConfigXml()
            Me.Load(Path)
        End Sub

    End Class

    Public Enum BaseClassState
        Unchanged = 0
        Modified
        Added
        Deleted
    End Enum

    ''' <summary>
    ''' Gets name of property, parameter syntax as (function (f as 'classname') f.'propertyname')
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <typeparam name="R"></typeparam>
    ''' <param name="expression"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPropertyName(Of T, R)(ByVal expression As Expression(Of Func(Of T, R))) As String
        Dim memberExpression As MemberExpression = DirectCast(expression.Body, MemberExpression)
        Return (memberExpression.Member.Name)
    End Function

    ''' <summary>
    ''' Concetenates given array of strings into comma separated string
    ''' </summary>
    ''' <param name="strings"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Concetenate(ByVal strings As String()) As String

        Dim sb As New StringBuilder
        Dim first As Boolean = True

        For Each s As String In strings
            If s IsNot Nothing AndAlso s.Trim.Length > 0 Then
                If Not first Then sb.Append(", ")
                sb.Append(s.Trim)
                first = False
            End If
        Next

        Return sb.ToString

    End Function

    ''' <summary>
    ''' Concetenates given array of strings using new lines
    ''' </summary>
    ''' <param name="strings"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ConcetenateNewLines(ByVal strings As String()) As String

        Dim sb As New StringBuilder
        Dim first As Boolean = True

        For Each s As String In strings
            If s IsNot Nothing AndAlso s.Trim.Length > 0 Then
                If Not first Then sb.Append(Environment.NewLine)
                sb.Append(s.Trim)
                first = False
            End If
        Next

        Return sb.ToString

    End Function

    ''' <summary>
    ''' Returns string representation of input string with spaces inserted before any uppercase characters
    ''' </summary>
    ''' <param name="input"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CapitalSpaceInsert(ByVal input As String) As String

        Dim sb As New StringBuilder

        If input IsNot Nothing AndAlso input.Length > 0 Then
            For index As Integer = 0 To input.Length - 1
                If index > 1 Then
                    If Char.IsUpper(input.Chars(index)) Then sb.Append(Space(1))
                End If
                sb.Append(input.Chars(index))
            Next
        End If

        Return sb.ToString

    End Function


    Public Function StringIsSomething(ByVal testString As String) As Boolean
        Return (testString IsNot Nothing AndAlso testString.Length > 0)
    End Function

End Module

Namespace Rti

    Public Enum State
        None = 0
        Pending
        Sending
        Completed
    End Enum

    Public Structure Description
        Private _value As String
        Public Shared None As String = ""
        Public Shared Pending As String = "N"
        Public Shared Sending As String = "S"
        Public Shared Completed As String = "C"

        Public Shared Function GetDescription(ByVal rtiState As State) As String
            Select Case rtiState
                Case State.None : Return Description.None
                Case State.Pending : Return Description.Pending
                Case State.Sending : Return Description.Sending
                Case State.Completed : Return Description.Completed
                Case Else : Return String.Empty
            End Select
        End Function

    End Structure

End Namespace