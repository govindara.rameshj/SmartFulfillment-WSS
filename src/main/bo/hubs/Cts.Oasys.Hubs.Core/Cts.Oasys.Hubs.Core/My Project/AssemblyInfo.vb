﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic.Strings


' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Cts.Oasys.Hubs.Core")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("TP-Wickes")> 
<Assembly: AssemblyProduct("Cts.Oasys.Hubs.Core")> 
<Assembly: AssemblyCopyright("Copyright " & Chrw(169) & " 2011")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("f2cbb23b-db95-41d6-8998-d7d7812abd86")> 

<Assembly: AssemblyVersion("3.1.0.0")> 
<Assembly: AssemblyFileVersion("3.1.0.0")> 
