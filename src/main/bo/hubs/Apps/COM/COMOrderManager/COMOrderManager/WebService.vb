﻿Imports Cts.Oasys.Hubs.Core.Order.WebService
Imports System.Runtime.InteropServices
Imports System.Text

'<ComClass(WebService.ClassId, WebService.InterfaceId, WebService.EventsId), _
<ComVisible(True), _
 Guid("7DA3428A-5FA7-413e-ACDB-28B8D3ADC4F7"), _
 ClassInterface(ClassInterfaceType.None), _
 ProgId("COMOrderManager.WebService.1"), _
 ComDefaultInterface(GetType(IWebService))> _
Public Class WebService
    Implements COMOrderManager.IWebService

    Public Sub New()

        MyBase.New()
    End Sub

    ' UNCOMMENT THIS FUNCTION WHEN ACTUALLY USING ORDER MANAGER WEB SERVICE
    ''' <summary>
    ''' Sends a dummy request to Order Manager's 'Source Transaction Lookup' web service.  If get a response then the web service is available for use
    ''' </summary>
    ''' <returns>True if get a response, error or otherwise, from Order Manager.  Otherwise returns false</returns>
    ''' <history></history>
    ''' <remarks>Dummy request contains an Web Order Number string with just a single 0 character in it.</remarks>
    Public Function WebServiceIsAvailable() As Boolean Implements IWebService.WebServiceIsAvailable

        Try
            ' Use a dummy, and therefore hopefully non existant WebOrderNumber
            Dim responseXML As String = GetSourceTransaction("0")

            If responseXML IsNot Nothing And responseXML.Length > 0 Then 'Got a response
                ' If had a response from Order Manager then that means the service is running
                ' no matter whether the response was an error (most likely, given the request data)
                ' or not
                Return True
            End If
        Catch ex As Exception
            If ex.Message.Contains("Connection.xml") Then
                Throw New Exception("Connection.xml file not found." & vbNewLine & "Please ensure that this file exists in the Resources folder.")
            End If
            Return False
        End Try
    End Function

    ' UNCOMMENT THIS FUNCTION WHEN ACTUALLY USING ORDER MANAGER WEB SERVICE
    ''' <summary>
    ''' Wrapper for call to Order Manager's 'Source Transaction Lookup' web service
    ''' </summary>
    ''' <param name="WebOrderNumber">Order number to look up transaction details for.  Usually a Venda order number.</param>
    ''' <param name="Store">Returned value of Selling Store ID (usually the Venda store)</param>
    ''' <param name="Till">Returned value of Selling Store Till ID (usually ??)</param>
    ''' <param name="TranDate">Returned value of the Transaction Date as a string (to make COM interop easier)</param>
    ''' <param name="Tran">Returned value of the Transaction Number</param>
    ''' <param name="ErrorMessages">
    ''' Returned value of concatenated ('StringDelimiter' delimited
    ''' for COM interoperability) error messages if request failed in any way.
    ''' </param>
    ''' <param name="Source">Source Type to be used if Source is not part of (prefix for) the WebOrderNumber</param>
    ''' <param name="DateTimeStamp">DateTimeStamp value to used instead of Now()</param>
    ''' <param name="SourceTypeOverrides">
    ''' String of concatenated ('StringDelimiter' delimited COM interoperability) Source
    ''' types to use to overide the default ones provided by SourceTransactionLookupService
    ''' </param>
    ''' <returns>True if request was valid and yielded a valid meaningful response.  False if invalid request or produces an Error Detail type response.</returns>
    ''' <history></history>
    ''' <remarks>If the request produces an error, any messages are returned in the ErrorMessages param, otherwise thetransaction details returned in the remaining byref parameters</remarks>
    Public Function OMSourceTransactionLookup(ByVal WebOrderNumber As String, _
                                              ByRef Store As String, ByRef Till As String, _
                                              ByRef TranDate As String, ByRef Tran As String, _
                                              ByRef ErrorMessages As String, _
                                              Optional ByVal Source As String = "", _
                                              Optional ByVal DateTimeStamp As Date = #1/1/1111 11:11:11 AM#, _
                                              Optional ByVal SourceTypeOverrides As String = "") As Boolean Implements IWebService.OMSourceTransactionLookup

        Try
            Dim orderManagerService As BaseService

            If SourceTypeOverrides = "" Then
                orderManagerService = New SourceTransactionLookupService
            Else
                Dim SourceOverrides() As String = SourceTypeOverrides.Split(CChar(StringDelimiter))
                orderManagerService = New SourceTransactionLookupService(SourceOverrides)
            End If

            Dim stlResponse As OMSourceTransactionLookupResponse = CType(orderManagerService.GetResponse, OMSourceTransactionLookupResponse)

            Try
                Dim responseXML As String = GetSourceTransaction(WebOrderNumber, Source, DateTimeStamp)

                ' Check for a response
                If responseXML IsNot Nothing And responseXML.Length > 0 Then
                    stlResponse = CType(stlResponse.Deserialise(responseXML), OMSourceTransactionLookupResponse)
                    With stlResponse
                        If .IsSuccessful Then
                            Dim stlTransactionDetails As OMSourceTransactionLookupResponseSourceTransactionDetailOrderDetail = .GetOrderDetail

                            If stlTransactionDetails IsNot Nothing Then
                                With stlTransactionDetails
                                    Store = .SellingStoreCode.Value
                                    Till = .SellingStoreTill.Value
                                    ' Duplicate the date format used by serialisation
                                    TranDate = .SaleDate.Value.ToString("s")
                                    Tran = .SellingStoreTransaction.Value
                                    OMSourceTransactionLookup = True
                                End With
                            End If
                        Else
                            ' Concatenate error messages into a single string, for COM interoperability
                            ErrorMessages = ConcatenateErrors(.ValidationErrors)
                        End If
                    End With
                End If
            Catch ex As TimeoutException
                OMSourceTransactionLookup = False
                ErrorMessages = "Timed out trying to connect to Order Manager."
            Catch ex As Exception
                OMSourceTransactionLookup = False
                If stlResponse IsNot Nothing Then
                    ' Concatenate error messages into a single string, for COM interoperability
                    ErrorMessages = ConcatenateErrors(stlResponse.ValidationErrors)
                Else
                    ErrorMessages = ex.Message
                    If ex.InnerException IsNot Nothing Then
                        ErrorMessages &= "; " & ex.InnerException.Message
                    End If
                End If
            End Try
        Catch ex As Exception
            OMSourceTransactionLookup = False
            ErrorMessages = "Failed setting up to receive the response from Order Manager."
        End Try
    End Function

    ''' <summary>
    ''' Used to delimit each string when concatenating any collection (array, list etc) of strings into a single string, for COM interoperabilty)
    ''' </summary>
    ''' <value>Value to be used between each string to mark the beginning and end of each</value>
    ''' <history></history>
    ''' <remarks>Cannot pass string arrays etc through COM interop, so concatenate any such into a single string using this delimiter</remarks>
    Public ReadOnly Property StringDelimiter() As String Implements IWebService.StringDelimiter
        Get
            StringDelimiter = "|"
        End Get
    End Property

    Private Function ConcatenateErrors(ByVal ValidationErrors As List(Of String)) As String
        ' Concatenate error messages into a single string, for COM interoperability
        Dim mess As New StringBuilder

        For Each _error As String In ValidationErrors
            mess.Append(_error & StringDelimiter)
        Next
        With mess.ToString
            ' Chop off last delimiter
            ConcatenateErrors = .Remove(.Length - StringDelimiter.Length, StringDelimiter.Length)
        End With
    End Function

    Private Function GetSourceTransaction(ByVal WebOrderNumber As String, _
                                          Optional ByVal Source As String = "", _
                                          Optional ByVal DateTimeStamp As Date = #1/1/1111 11:11:11 AM#) As String
        Dim orderManagerService As BaseService = New SourceTransactionLookupService
        Dim stlRequest As OMSourceTransactionLookupRequest

        stlRequest = CType(orderManagerService.GetRequest, OMSourceTransactionLookupRequest)
        With stlRequest
            ' No date supplied, so none to pass on
            If DateDiff("s", DateTimeStamp, #1/1/1111 11:11:11 AM#) = 0 Then
                ' No source either
                If Source = "" Then
                    .SetFields(WebOrderNumber)
                Else
                    .SetFields(WebOrderNumber, Source)
                End If
            Else
                ' No source to pass on
                If Source = "" Then
                    .SetFields(WebOrderNumber, DateTimeStamp)
                Else
                    .SetFields(WebOrderNumber, Source, DateTimeStamp)
                End If
            End If
            GetSourceTransaction = orderManagerService.GetResponseXml(.Serialise)
        End With
    End Function

    '' REMOVE THIS FUNCTION WHEN ACTUALLY USING ORDER MANAGER WEB SERVICE
    '' Used to test COM visibility and VB6 code without having to wait for Order Manager to supply
    '' the required web service
    'Public Function WebServiceIsAvailable() As Boolean Implements IWebService.WebServiceIsAvailable

    '    Return True
    'End Function

    '' REMOVE THIS FUNCTION WHEN ACTUALLY USING ORDER MANAGER WEB SERVICE
    '' Used to test COM visibility and VB6 code without having to wait for Order Manager to supply
    '' the required web service
    'Public Function OMSourceTransactionLookup(ByVal WebOrderNumber As String, _
    '                                          ByRef Store As String, ByRef Till As String, _
    '                                          ByRef TranDate As String, ByRef Tran As String, _
    '                                          ByRef ErrorMessages As String) As Boolean Implements IWebService.OMSourceTransactionLookup
    '    Dim NumericOrderNo As Integer

    '    If Integer.TryParse(WebOrderNumber, NumericOrderNo) Then
    '        Select Case NumericOrderNo
    '            Case 1
    '                Store = "8099"
    '                Till = "55"
    '                TranDate = DateAdd("d", -1, Now).ToString("s")
    '                Tran = "1234"
    '                OMSourceTransactionLookup = True
    '            Case 2
    '                Store = "BADSTORE"
    '                Till = "55"
    '                TranDate = DateAdd("d", -1, Now).ToString("s")
    '                Tran = "1234"
    '                OMSourceTransactionLookup = True
    '            Case 3
    '                Store = "8099"
    '                Till = "BADTILL"
    '                TranDate = "BADDATE"
    '                Tran = "1234"
    '                OMSourceTransactionLookup = True
    '            Case 4
    '                Store = "8099"
    '                Till = "55"
    '                TranDate = DateAdd("d", -1, Now).ToString("s")
    '                Tran = "BADTRAN"
    '                OMSourceTransactionLookup = True
    '            Case 5
    '                ErrorMessages = "Unknown source order"
    '            Case 6
    '                ErrorMessages = "OM order not yet created for source transaction"
    '            Case 7
    '                ErrorMessages = "Invalid OM order for source transaction"
    '            Case 8
    '                ErrorMessages = "Timed out trying to connect to Order Manager."
    '            Case 9
    '                ErrorMessages = "OM Request XML badly formed"
    '            Case 10
    '                ErrorMessages = "OM Request XML invalid data"
    '                ErrorMessages &= OMSourceTransactionLookupResponse.StringDelimiter & "Unknown source order"
    '                ErrorMessages &= OMSourceTransactionLookupResponse.StringDelimiter & "OM order not yet created for source transaction"
    '                ErrorMessages &= OMSourceTransactionLookupResponse.StringDelimiter & "Invalid OM order for source transaction"
    '                ErrorMessages &= OMSourceTransactionLookupResponse.StringDelimiter & "Timed out trying to connect to Order Manager."
    '                ErrorMessages &= OMSourceTransactionLookupResponse.StringDelimiter & "OM Request XML badly formed"
    '                ErrorMessages &= OMSourceTransactionLookupResponse.StringDelimiter & "OM Request XML invalid data"
    '            Case Else
    '                ErrorMessages = "Failed setting up to receive the response from Order Manager."
    '        End Select
    '    Else
    '        ErrorMessages = "Failed setting up to receive the response from Order Manager."
    '    End If
    'End Function
End Class
