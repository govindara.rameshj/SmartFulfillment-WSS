﻿Imports System.Runtime.InteropServices

<InterfaceType(ComInterfaceType.InterfaceIsDual), _
 Guid("6F7A00D2-41CD-477c-828A-162FF93F4B61"), _
 ComVisible(True)> _
Public Interface IWebService

    ''' <summary>
    ''' Sends a dummy request to Order Manager's 'Source Transaction Lookup' web service.  If get a response then the web service is available for use
    ''' </summary>
    ''' <returns>True if get a response, error or otherwise, from Order Manager.  Otherwise returns false</returns>
    ''' <history></history>
    ''' <remarks></remarks>
    Function WebServiceIsAvailable() As Boolean

    ''' <summary>
    ''' Wrapper for call to Order Manager's 'Source Transaction Lookup' web service
    ''' </summary>
    ''' <param name="WebOrderNumber">Order number to look up transaction details for.  Usually a Venda order number.</param>
    ''' <param name="Store">Returned value of Selling Store ID (usually the Venda store)</param>
    ''' <param name="Till">Returned value of Selling Store Till ID (usually ??)</param>
    ''' <param name="TranDate">Returned value of the Transaction Date as a string (to make COM interop easier)</param>
    ''' <param name="Tran">Returned value of the Transaction Number</param>
    ''' <param name="ErrorMessages">
    ''' Returned value of concatenated ('StringDelimiter' delimited
    ''' for COM interoperability) error messages if request failed in any way.
    ''' </param>
    ''' <param name="Source">Source Type to be used if Source is not part of (prefix for) the WebOrderNumber</param>
    ''' <param name="DateTimeStamp">DateTimeStamp value to used instead of Now()</param>
    ''' <param name="SourceTypeOverrides">
    ''' String of concatenated ('StringDelimiter' delimited COM interoperability) Source
    ''' types to use to overide the default ones provided by SourceTransactionLookupService
    ''' </param>
    ''' <returns>True if request was valid and yielded a valid meaningful response.  False if invalid request or produces an Error Detail type response.</returns>
    ''' <history></history>
    ''' <remarks>If the request produces an error, any messages are returned in the ErrorMessages param, otherwise thetransaction details returned in the remaining byref parameters</remarks>
    Function OMSourceTransactionLookup(ByVal WebOrderNumber As String, _
                                       ByRef Store As String, ByRef Till As String, _
                                       ByRef TranDate As String, ByRef Tran As String, _
                                       ByRef ErrorMessages As String, _
                                       Optional ByVal Source As String = "", _
                                       Optional ByVal DateTimeStamp As Date = #1/1/1111 11:11:11 AM#, _
                                       Optional ByVal SourceTypeOverrides As String = "") As Boolean

    ''' <summary>
    ''' Used to delimit each string when concatenating any collection (array, list etc) of strings into a single string, for COM interoperabilty)
    ''' </summary>
    ''' <value>Value to be used between each string to mark the beginning and end of each</value>
    ''' <history></history>
    ''' <remarks>Cannot pass string arrays etc through COM interop, so concatenate any such into a single string using this delimiter</remarks>
    ReadOnly Property StringDelimiter() As String
End Interface
