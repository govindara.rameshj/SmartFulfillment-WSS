﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTestCOMOrderManager
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.butGetDetails = New System.Windows.Forms.Button
        Me.lblOrderNumber = New System.Windows.Forms.Label
        Me.txtOrderNumber = New System.Windows.Forms.TextBox
        Me.lblStoreNo = New System.Windows.Forms.Label
        Me.txtStoreNo = New System.Windows.Forms.TextBox
        Me.txtTranDate = New System.Windows.Forms.TextBox
        Me.lblTranDate = New System.Windows.Forms.Label
        Me.txtTill = New System.Windows.Forms.TextBox
        Me.lblTill = New System.Windows.Forms.Label
        Me.txtTranNo = New System.Windows.Forms.TextBox
        Me.lblTranNo = New System.Windows.Forms.Label
        Me.lblErrors = New System.Windows.Forms.Label
        Me.txtErrors = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'butGetDetails
        '
        Me.butGetDetails.Location = New System.Drawing.Point(274, 12)
        Me.butGetDetails.Name = "butGetDetails"
        Me.butGetDetails.Size = New System.Drawing.Size(75, 23)
        Me.butGetDetails.TabIndex = 0
        Me.butGetDetails.Text = "Go"
        Me.butGetDetails.UseVisualStyleBackColor = True
        '
        'lblOrderNumber
        '
        Me.lblOrderNumber.AutoSize = True
        Me.lblOrderNumber.Location = New System.Drawing.Point(6, 17)
        Me.lblOrderNumber.Name = "lblOrderNumber"
        Me.lblOrderNumber.Size = New System.Drawing.Size(50, 13)
        Me.lblOrderNumber.TabIndex = 1
        Me.lblOrderNumber.Text = "Order No"
        '
        'txtOrderNumber
        '
        Me.txtOrderNumber.Location = New System.Drawing.Point(62, 14)
        Me.txtOrderNumber.Name = "txtOrderNumber"
        Me.txtOrderNumber.Size = New System.Drawing.Size(142, 20)
        Me.txtOrderNumber.TabIndex = 2
        Me.txtOrderNumber.Text = "WO*494107"
        '
        'lblStoreNo
        '
        Me.lblStoreNo.AutoSize = True
        Me.lblStoreNo.Location = New System.Drawing.Point(7, 43)
        Me.lblStoreNo.Name = "lblStoreNo"
        Me.lblStoreNo.Size = New System.Drawing.Size(49, 13)
        Me.lblStoreNo.TabIndex = 3
        Me.lblStoreNo.Text = "Store No"
        '
        'txtStoreNo
        '
        Me.txtStoreNo.Location = New System.Drawing.Point(62, 40)
        Me.txtStoreNo.Name = "txtStoreNo"
        Me.txtStoreNo.Size = New System.Drawing.Size(100, 20)
        Me.txtStoreNo.TabIndex = 4
        '
        'txtTranDate
        '
        Me.txtTranDate.Location = New System.Drawing.Point(62, 66)
        Me.txtTranDate.Name = "txtTranDate"
        Me.txtTranDate.Size = New System.Drawing.Size(100, 20)
        Me.txtTranDate.TabIndex = 6
        '
        'lblTranDate
        '
        Me.lblTranDate.AutoSize = True
        Me.lblTranDate.Location = New System.Drawing.Point(26, 69)
        Me.lblTranDate.Name = "lblTranDate"
        Me.lblTranDate.Size = New System.Drawing.Size(30, 13)
        Me.lblTranDate.TabIndex = 5
        Me.lblTranDate.Text = "Date"
        '
        'txtTill
        '
        Me.txtTill.Location = New System.Drawing.Point(62, 92)
        Me.txtTill.Name = "txtTill"
        Me.txtTill.Size = New System.Drawing.Size(100, 20)
        Me.txtTill.TabIndex = 8
        '
        'lblTill
        '
        Me.lblTill.AutoSize = True
        Me.lblTill.Location = New System.Drawing.Point(36, 95)
        Me.lblTill.Name = "lblTill"
        Me.lblTill.Size = New System.Drawing.Size(20, 13)
        Me.lblTill.TabIndex = 7
        Me.lblTill.Text = "Till"
        '
        'txtTranNo
        '
        Me.txtTranNo.Location = New System.Drawing.Point(62, 118)
        Me.txtTranNo.Name = "txtTranNo"
        Me.txtTranNo.Size = New System.Drawing.Size(100, 20)
        Me.txtTranNo.TabIndex = 10
        '
        'lblTranNo
        '
        Me.lblTranNo.AutoSize = True
        Me.lblTranNo.Location = New System.Drawing.Point(10, 121)
        Me.lblTranNo.Name = "lblTranNo"
        Me.lblTranNo.Size = New System.Drawing.Size(46, 13)
        Me.lblTranNo.TabIndex = 9
        Me.lblTranNo.Text = "Tran No"
        '
        'lblErrors
        '
        Me.lblErrors.AutoSize = True
        Me.lblErrors.Location = New System.Drawing.Point(168, 43)
        Me.lblErrors.Name = "lblErrors"
        Me.lblErrors.Size = New System.Drawing.Size(34, 13)
        Me.lblErrors.TabIndex = 11
        Me.lblErrors.Text = "Errors"
        '
        'txtErrors
        '
        Me.txtErrors.Location = New System.Drawing.Point(171, 62)
        Me.txtErrors.Multiline = True
        Me.txtErrors.Name = "txtErrors"
        Me.txtErrors.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtErrors.Size = New System.Drawing.Size(178, 76)
        Me.txtErrors.TabIndex = 12
        '
        'frmTestCOMOrderManager
        '
        Me.AcceptButton = Me.butGetDetails
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(361, 150)
        Me.Controls.Add(Me.txtErrors)
        Me.Controls.Add(Me.lblErrors)
        Me.Controls.Add(Me.txtTranNo)
        Me.Controls.Add(Me.lblTranNo)
        Me.Controls.Add(Me.txtTill)
        Me.Controls.Add(Me.lblTill)
        Me.Controls.Add(Me.txtTranDate)
        Me.Controls.Add(Me.lblTranDate)
        Me.Controls.Add(Me.txtStoreNo)
        Me.Controls.Add(Me.lblStoreNo)
        Me.Controls.Add(Me.txtOrderNumber)
        Me.Controls.Add(Me.lblOrderNumber)
        Me.Controls.Add(Me.butGetDetails)
        Me.Name = "frmTestCOMOrderManager"
        Me.Text = "Test COM Order Manager"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents butGetDetails As System.Windows.Forms.Button
    Friend WithEvents lblOrderNumber As System.Windows.Forms.Label
    Friend WithEvents txtOrderNumber As System.Windows.Forms.TextBox
    Friend WithEvents lblStoreNo As System.Windows.Forms.Label
    Friend WithEvents txtStoreNo As System.Windows.Forms.TextBox
    Friend WithEvents txtTranDate As System.Windows.Forms.TextBox
    Friend WithEvents lblTranDate As System.Windows.Forms.Label
    Friend WithEvents txtTill As System.Windows.Forms.TextBox
    Friend WithEvents lblTill As System.Windows.Forms.Label
    Friend WithEvents txtTranNo As System.Windows.Forms.TextBox
    Friend WithEvents lblTranNo As System.Windows.Forms.Label
    Friend WithEvents lblErrors As System.Windows.Forms.Label
    Friend WithEvents txtErrors As System.Windows.Forms.TextBox

End Class
