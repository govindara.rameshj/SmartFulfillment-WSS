﻿Public Class frmTestCOMOrderManager

    Private Sub butGetDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butGetDetails.Click
        Dim OrderManagerWebService As COMOrderManager.WebService
        Dim Store, Till, TranDate, TranNo, ErrorMess, StoreNo, Mess As String
        Dim DateTran As Date
        Dim Errors() As String

        Try
            If txtOrderNumber.Text.Length > 0 Then
                ' Clear down any existing store etc data
                txtStoreNo.Text = ""
                txtTranDate.Text = ""
                txtTill.Text = ""
                txtTranNo.Text = ""
                txtErrors.Text = ""
                ' Initialise byref params
                Store = ""
                Till = ""
                TranDate = ""
                TranNo = ""
                ErrorMess = ""
                StoreNo = ""
                Mess = ""
                ' Find Venda transaction here
                OrderManagerWebService = New COMOrderManager.WebService
                If OrderManagerWebService IsNot Nothing Then
                    With OrderManagerWebService
                        If .WebServiceIsAvailable Then
                            If .OMSourceTransactionLookup(txtOrderNumber.Text, Store, Till, TranDate, TranNo, ErrorMess) Then
                                If IsOMStore(Store, StoreNo) Then
                                    txtStoreNo.Text = StoreNo
                                    If IsOMDate(TranDate, DateTran) Then
                                        txtTranDate.Text = Format$(DateTran, "dd/mm/yy")
                                        If IsNumeric(Till) Then
                                            txtTill.Text = Format$(Val(Till), "00")
                                            If IsNumeric(TranNo) Then
                                                txtTranNo.Text = Format$(Val(TranNo), "0000")
                                                Exit Sub
                                            Else
                                                Call MsgBox("'Order Manager' returned an invalid Transaction Number - " & TranNo, vbExclamation, "Invalid Transaction Number")
                                            End If
                                        Else
                                            Call MsgBox("'Order Manager' returned an invalid Till ID - " & Till, vbExclamation, "Invalid Till ID")
                                        End If
                                    Else
                                        Call MsgBox("'Order Manager' returned an invalid Transaction Date - " & TranDate, vbExclamation, "Invalid Transaction Date")
                                    End If
                                Else
                                    Call MsgBox("'Order Manager' returned an invalid Store Number - " & Store, vbExclamation, "Invalid Transaction Store Number")
                                End If
                            Else
                                Errors = ErrorMess.Split(CChar(OrderManagerWebService.StringDelimiter))
                                Mess = "Failed to retrieve transaction details from 'Order Manager'."
                                For Each ErrMess As String In Errors
                                    Mess &= vbNewLine & ErrMess
                                    With txtErrors
                                        If .Text.Length > 0 Then
                                            .Text &= vbNewLine
                                        End If
                                        .Text &= ErrMess
                                    End With
                                Next
                                Call MsgBox(Mess, vbExclamation, "Failed getting transaction details")
                            End If
                        Else
                            Call MsgBox("'Order Manager' web service is not currently available." & vbNewLine & "Cannot retrieve the transaction details.", vbExclamation, "'Order Manager' web service unavailable")
                        End If
                    End With
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Failed testing COMOrderManager.dll")
        End Try
    End Sub

    Private Function IsOMDate(ByVal OMDate As String, ByRef RealDate As Date) As Boolean
        Dim TPos As Integer
        Dim OMDatePart() As String
        Dim OMDay As String
        Dim OMMonth As String
        Dim OMYear As String

        TPos = InStr(1, OMDate, "T", vbTextCompare)
        If TPos > 0 Then
            ReDim OMDatePart(0)
            OMDatePart = OMDate.Substring(0, TPos - 1).Split("-"c)
            If UBound(OMDatePart) = 2 Then
                If IsNumeric(OMDatePart(0)) Then
                    OMYear = Format$(CInt(OMDatePart(0)), "00")
                    If IsNumeric(OMDatePart(1)) Then
                        OMMonth = Format$(CInt(OMDatePart(1)), "00")
                        If IsNumeric(OMDatePart(2)) Then
                            OMDay = Format$(CInt(OMDatePart(2)), "00")
                            If IsDate(OMDay & "/" & OMMonth & "/" & OMYear) Then
                                RealDate = Date.Parse(OMDay & "/" & OMMonth & "/" & OMYear)
                                IsOMDate = True
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Function
    '
    ' Order manager Store has an 8 preceding the Store number.
    ' Just want the store number part, if its valid
    Private Function IsOMStore(ByVal Store As String, ByRef StoreNo As String) As Boolean
        Dim StoreNum As Integer

        If Store.Trim.Length = 4 Then
            If Store.First = "8"c Then
                Store = Store.Substring(1)
                If Integer.TryParse(Store, StoreNum) Then
                    StoreNo = Format$(StoreNum, "000")
                    IsOMStore = True
                End If
            End If
        End If
    End Function
End Class
