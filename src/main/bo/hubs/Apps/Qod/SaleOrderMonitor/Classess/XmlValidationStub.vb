﻿Namespace TpWickes

    Public Class XmlValidationStub
        Implements IXmlValidation

        Private _XmlFailureReason As New List(Of String)

#Region "Stub Code"

        Private _ValidateXML As Boolean

        Friend Sub ConfigureStub(ByVal ValidateXML As Boolean, ByVal XmlFailureReason As List(Of String))

            _ValidateXML = ValidateXML
            _XmlFailureReason = XmlFailureReason

        End Sub

#End Region

#Region "Interface"

        Public ReadOnly Property XmlFailureReason() As System.Collections.Generic.List(Of String) Implements IXmlValidation.XmlFailureReason
            Get
                Return _XmlFailureReason
            End Get
        End Property

        Public Function ValidateXML(ByVal RequestXml As String, ByVal RequestWebServiceType As WebServiceType) As Boolean Implements IXmlValidation.ValidateXML

            'parameters ignored

            Return _ValidateXML

        End Function

#End Region

    End Class

End Namespace