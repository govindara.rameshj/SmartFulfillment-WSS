﻿Namespace TpWickes

    Public Class LogSaleOrderMonitorInvalidXml
        Implements ILogSaleOrderMonitorInvalidXml

        Public Function Insert(ByVal StoreOrderNo As String, ByVal OrderManagerOrderNo As Integer?, ByVal DateLogged As Date, ByVal RequestWebServiceType As WebServiceType, ByVal FailureReason As System.Collections.Generic.List(Of String), ByVal VendaOrderNo As String, ByVal FailedXml As System.Xml.Linq.XDocument) As Integer? Implements ILogSaleOrderMonitorInvalidXml.Insert

            Dim Repository As ILogSaleOrderMonitorInvalidXmlRepository

            Repository = LogSaleOrderMonitorInvalidXmlRepositoryFactory.FactoryGet

            If SaveXmlData() = True Then

                Return Repository.Insert(StoreOrderNo, OrderManagerOrderNo, DateLogged, RequestWebServiceType, FailureReason, VendaOrderNo, FailedXml)

            Else

                Return Repository.Insert(StoreOrderNo, OrderManagerOrderNo, DateLogged, RequestWebServiceType, FailureReason, VendaOrderNo, Nothing)

            End If

        End Function

#Region "Private, Protected, Friend Functions And Procedures"

        Friend Function SaveXmlData() As Boolean

            Dim Repository As ILogSaleOrderMonitorInvalidXmlRepository

            Repository = LogSaleOrderMonitorInvalidXmlRepositoryFactory.FactoryGet

            If Repository.SaveXmlData.HasValue = False Then Return False

            Return Repository.SaveXmlData.Value

        End Function

#End Region

    End Class

End Namespace