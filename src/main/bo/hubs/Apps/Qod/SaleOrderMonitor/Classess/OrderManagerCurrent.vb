﻿Namespace TpWickes

    Public Class OrderManagerCurrent
        Implements IOrderManager

        Public Function OrderManagerRequest(ByRef OM As Cts.Oasys.Hubs.Core.Order.WebService.BaseService, ByVal RequestXml As String) As String Implements IOrderManager.OrderManagerRequest

            Return OM.GetResponseXml(RequestXml)

        End Function

        Public Function OrderManagerRequest(ByRef OM As Cts.Oasys.Hubs.Core.Order.WebService.BaseService, ByVal RequestXml As String, ByVal DictionaryValue As System.Collections.Generic.Dictionary(Of String, String)) As String Implements IOrderManager.OrderManagerRequest

            Return Me.OrderManagerRequest(OM, RequestXml)

        End Function

    End Class

End Namespace