﻿Namespace TpWickes

    Public Class XmlConfig
        Implements IXmlConfig

        Public Function XSDNameOrderManagerCreateOrder() As String Implements IXmlConfig.OrderManagerCreateOrderXSDName

            Return ConfigXMLDoc.SelectSingleNode("Configuration/XSDFiles/OrderManagerCreateOrder/Input").InnerText

        End Function

        Public Function XSDNameOrderManagerCreateRefund() As String Implements IXmlConfig.OrderManagerCreateRefundXSDName

            Return ConfigXMLDoc.SelectSingleNode("Configuration/XSDFiles/OrderManagerCreateRefund/Input").InnerText

        End Function

        Public Function XSDNameOrderManagerStatusUpdate() As String Implements IXmlConfig.OrderManagerStatusUpdateXSDName

            Return ConfigXMLDoc.SelectSingleNode("Configuration/XSDFiles/OrderManagerStatusUpdate/Input").InnerText

        End Function

        Public Function XSDPathLocation() As String Implements IXmlConfig.XSDPathLocation

            Return ConfigXMLDoc.SelectSingleNode("Configuration/XSDFiles/Path").InnerText

        End Function

        Public Function GetXSD(ByVal PathAndFileName As String) As System.Xml.Linq.XElement Implements IXmlConfig.GetXSD

            Dim SR As StreamReader

            SR = New StreamReader(PathAndFileName)

            Return XElement.Parse(SR.ReadToEnd)

        End Function

    End Class

End Namespace