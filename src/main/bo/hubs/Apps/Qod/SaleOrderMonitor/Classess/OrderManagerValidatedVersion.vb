﻿Namespace TpWickes

    Public Class OrderManagerValidatedVersion
        Implements IOrderManager

        Public Function OrderManagerRequest(ByRef OM As Cts.Oasys.Hubs.Core.Order.WebService.BaseService, ByVal RequestXml As String) As String Implements IOrderManager.OrderManagerRequest

            Dim Validate As IXmlValidation
            Dim WebType As WebServiceType

            WebType = OrderManagerServiceType(OM)

            Validate = XmlValidationFactory.FactoryGet
            If Validate.ValidateXML(RequestXml, WebType) = True Then

                Return OM.GetResponseXml(RequestXml)

            Else

                Return OrderManagerRequestReturnErrorValue(OM)

            End If

        End Function

        Public Overridable Function OrderManagerRequest(ByRef OM As Cts.Oasys.Hubs.Core.Order.WebService.BaseService, ByVal RequestXml As String, ByVal DictionaryValue As System.Collections.Generic.Dictionary(Of String, String)) As String Implements IOrderManager.OrderManagerRequest

            Return Me.OrderManagerRequest(OM, RequestXml)

        End Function

#Region "Private, Protected, Friend Functions And Procedures"

        Friend Function OrderManagerServiceClassName(ByRef OM As Cts.Oasys.Hubs.Core.Order.WebService.BaseService) As String

            Return OM.GetType.FullName

        End Function

        Friend Function OrderManagerRequestReturnErrorValue(ByRef OM As Cts.Oasys.Hubs.Core.Order.WebService.BaseService) As String

            OrderManagerRequestReturnErrorValue = String.Empty
            Select Case OrderManagerServiceClassName(OM)

                Case "Cts.Oasys.Hubs.Core.Order.WebService.CreateService"
                    Return "Failed Validation - Order"

                Case "Cts.Oasys.Hubs.Core.Order.WebService.RefundService"
                    Return "Failed Validation - Refund"

                Case "Cts.Oasys.Hubs.Core.Order.WebService.UpdateService"
                    Return "Failed Validation - Update"

            End Select

        End Function

        Friend Function OrderManagerServiceType(ByRef OM As Cts.Oasys.Hubs.Core.Order.WebService.BaseService) As WebServiceType

            Select Case OrderManagerServiceClassName(OM)
                Case "Cts.Oasys.Hubs.Core.Order.WebService.CreateService"
                    Return WebServiceType.CreateOrder

                Case "Cts.Oasys.Hubs.Core.Order.WebService.RefundService"
                    Return WebServiceType.RefundOrder

                Case "Cts.Oasys.Hubs.Core.Order.WebService.UpdateService"
                    Return WebServiceType.StatusUpdate

            End Select

        End Function

#End Region

    End Class

End Namespace