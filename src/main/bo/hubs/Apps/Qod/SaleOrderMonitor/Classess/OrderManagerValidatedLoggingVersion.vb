﻿Namespace TpWickes

    Public Class OrderManagerValidatedLoggingVersion
        Inherits OrderManagerValidatedVersion
        Implements IOrderManager

        Public Overrides Function OrderManagerRequest(ByRef OM As Cts.Oasys.Hubs.Core.Order.WebService.BaseService, _
                                                      ByVal RequestXml As String, ByVal DictionaryValue As System.Collections.Generic.Dictionary(Of String, String)) As String

            Dim Validate As IXmlValidation
            Dim WebType As WebServiceType

            WebType = OrderManagerServiceType(OM)

            Validate = XmlValidationFactory.FactoryGet
            If Validate.ValidateXML(RequestXml, WebType) = True Then

                Return OM.GetResponseXml(RequestXml)

            Else

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Author         : Partha Dutta
                'Date           : 13/09/2011
                'Change Request : CR0054-02
                'TFS User Story : 2290
                'TFS Task ID    : 2374
                'Description    : Log Sales Order Monitor invalid requests to Order Manager
                '
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Dim Log As ILogSaleOrderMonitorInvalidXml

                Dim VendaOrderNo As String

                Log = LogSaleOrderMonitorInvalidXmlFactory.FactoryGet

                VendaOrderNo = ExtractVendaOrderNo(DictionaryValue)

                If VendaOrderNo = String.Empty Then

                    Log.Insert(ExtractStoreOrderNo(DictionaryValue), ExtractOrderManagerOrderNo(DictionaryValue), _
                               Now, OrderManagerServiceType(OM), Validate.XmlFailureReason, Nothing, XDocument.Parse(RequestXml))

                Else

                    Log.Insert(ExtractStoreOrderNo(DictionaryValue), ExtractOrderManagerOrderNo(DictionaryValue), _
                               Now, OrderManagerServiceType(OM), Validate.XmlFailureReason, VendaOrderNo, XDocument.Parse(RequestXml))

                End If

                Return OrderManagerRequestReturnErrorValue(OM)

            End If

        End Function

#Region "Private, Protected, Friend Functions And Procedures"

        Friend Function ExtractStoreOrderNo(ByVal DictionaryValue As System.Collections.Generic.Dictionary(Of String, String)) As String

            Dim Value As String

            If DictionaryValue Is Nothing OrElse DictionaryValue.ContainsKey("Store Order No") = False Then Return String.Empty

            Value = DictionaryValue.Item("Store Order No")

            If Value Is Nothing OrElse Value = String.Empty Then Return String.Empty

            Return Value

        End Function

        Friend Function ExtractOrderManagerOrderNo(ByVal DictionaryValue As System.Collections.Generic.Dictionary(Of String, String)) As System.Nullable(Of Integer)

            Dim Value As String
            Dim IntegerValue As Integer

            If DictionaryValue Is Nothing OrElse DictionaryValue.ContainsKey("Order Manager Order No") = False Then Return Nothing

            Value = DictionaryValue.Item("Order Manager Order No")

            If Value Is Nothing OrElse Value = String.Empty Then Return Nothing

            If IsNumeric(Value) = False Then Return Nothing

            IntegerValue = CType(Value, Integer)

            If IntegerValue = 0 Then Return Nothing

            Return IntegerValue

        End Function

        Friend Function ExtractVendaOrderNo(ByVal DictionaryValue As System.Collections.Generic.Dictionary(Of String, String)) As String

            Dim Value As String

            If DictionaryValue Is Nothing OrElse DictionaryValue.ContainsKey("Venda Order No") = False Then Return String.Empty

            Value = DictionaryValue.Item("Venda Order No")

            If Value Is Nothing OrElse Value = String.Empty Then Return String.Empty

            Return Value

        End Function

#End Region

    End Class

End Namespace