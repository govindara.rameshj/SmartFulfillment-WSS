﻿Namespace TpWickes

    Public Class LogSaleOrderMonitorInvalidXmlStub
        Implements ILogSaleOrderMonitorInvalidXml

#Region "Stub Code"

        Private _ID As System.Nullable(Of Integer)

        Friend Sub ConfigureStub(ByVal ID As System.Nullable(Of Integer))

            _ID = ID

        End Sub

#End Region

#Region "Interface"

        Public Function Insert(ByVal StoreOrderNo As String, ByVal OrderManagerOrderNo As Integer?, ByVal DateLogged As Date, ByVal RequestWebServiceType As WebServiceType, ByVal FailureReason As System.Collections.Generic.List(Of String), ByVal VendaOrderNo As String, ByVal FailedXml As System.Xml.Linq.XDocument) As Integer? Implements ILogSaleOrderMonitorInvalidXml.Insert

            'ignore input parameters

            Return _ID

        End Function

#End Region

    End Class

End Namespace