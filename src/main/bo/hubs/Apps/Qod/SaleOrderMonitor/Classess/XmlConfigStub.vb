﻿Namespace TpWickes

    Public Class XmlConfigStub
        Implements IXmlConfig

#Region "Stub Code"

        Private _OrderManagerCreateOrderXSDName As String
        Private _OrderManagerCreateRefundXSDName As String
        Private _OrderManagerStatusUpdateXSDName As String
        Private _XSDPath As String
        Private _XSD As XElement

        Friend Sub ConfigureStub(ByVal CreateOrder As String, ByVal CreateRefund As String, _
                                 ByVal StatusUpdate As String, ByVal Path As String, ByVal XSD As XElement)

            _OrderManagerCreateOrderXSDName = CreateOrder
            _OrderManagerCreateRefundXSDName = CreateRefund
            _OrderManagerStatusUpdateXSDName = StatusUpdate
            _XSDPath = Path
            _XSD = XSD

        End Sub

#End Region

#Region "Interface"

        Public Function XSDPathLocation() As String Implements IXmlConfig.XSDPathLocation

            Return _XSDPath

        End Function

        Public Function XSDNameOrderManagerCreateOrder() As String Implements IXmlConfig.OrderManagerCreateOrderXSDName

            Return _OrderManagerCreateOrderXSDName

        End Function

        Public Function XSDNameOrderManagerCreateRefund() As String Implements IXmlConfig.OrderManagerCreateRefundXSDName

            Return _OrderManagerCreateRefundXSDName

        End Function

        Public Function XSDNameOrderManagerStatusUpdate() As String Implements IXmlConfig.OrderManagerStatusUpdateXSDName

            Return _OrderManagerStatusUpdateXSDName

        End Function

        Public Function GetXSD(ByVal PathAndFileName As String) As System.Xml.Linq.XElement Implements IXmlConfig.GetXSD

            'ignore input paramters

            Return _XSD

        End Function

#End Region

    End Class

End Namespace