﻿Namespace TpWickes

    Public Class XmlValidationMultipleErrorVersion
        Implements IXmlValidation

        Private _XmlError As Boolean
        Private _XmlFailureReason As New List(Of String)

        Public ReadOnly Property XmlFailureReason() As System.Collections.Generic.List(Of String) Implements IXmlValidation.XmlFailureReason
            Get
                Return _XmlFailureReason
            End Get
        End Property

        Public Function ValidateXML(ByVal RequestXml As String, ByVal RequestWebServiceType As WebServiceType) As Boolean Implements IXmlValidation.ValidateXML

            Dim strXsdPathLocation As String
            Dim strXsdFileName As String
            Dim XSD As System.Xml.Linq.XElement

            strXsdPathLocation = XsdPathLocation()
            strXsdFileName = XSDFileName(RequestWebServiceType)
            XSD = GetXSD(strXsdPathLocation, strXsdFileName)

            Return Validate(XSD, XDocument.Parse(RequestXml))

        End Function

#Region "Private, Protected, Friend Functions And Procedures"

        Private Sub XsdErrorHandler(ByVal o As Object, ByVal e As ValidationEventArgs)

            _XmlFailureReason.Add(e.Message)
            _XmlError = True

        End Sub

        Friend Function Validate(ByVal XSD As XElement, ByVal XML As XDocument) As Boolean

            Dim ReaderSettings As New XmlReaderSettings
            Dim Reader As XmlReader

            With ReaderSettings
                .ConformanceLevel = ConformanceLevel.Fragment
                .IgnoreComments = True
                .IgnoreWhitespace = True
                .ValidationType = ValidationType.Schema
                .ValidationFlags = .ValidationFlags Or XmlSchemaValidationFlags.ReportValidationWarnings

                .Schemas.Add(Nothing, XSD.CreateReader)

                AddHandler ReaderSettings.ValidationEventHandler, AddressOf XsdErrorHandler
            End With

            Reader = XmlReader.Create(New System.IO.StringReader(XML.ToString), ReaderSettings)
            While (Reader.Read())
            End While

            Return Not _XmlError

        End Function

        Friend Function XsdPathLocation() As String

            Dim Config As IXmlConfig

            Config = XmlConfigFactory.FactoryGet

            Return Config.XSDPathLocation

        End Function

        Friend Function XSDFileName(ByVal RequestWebServiceType As WebServiceType) As String

            Dim Config As IXmlConfig

            Config = XmlConfigFactory.FactoryGet

            XSDFileName = String.Empty
            Select Case RequestWebServiceType

                Case WebServiceType.CreateOrder
                    Return Config.OrderManagerCreateOrderXSDName

                Case WebServiceType.RefundOrder
                    Return Config.OrderManagerCreateRefundXSDName

                Case WebServiceType.StatusUpdate
                    Return Config.OrderManagerStatusUpdateXSDName

            End Select

        End Function

        Friend Function GetXSD(ByVal XSDPathLocation As String, ByVal XSDFileName As String) As System.Xml.Linq.XElement

            Dim Config As IXmlConfig

            Config = XmlConfigFactory.FactoryGet

            Return Config.GetXSD(XSDPathLocation & XSDFileName)

        End Function

#End Region

    End Class

End Namespace