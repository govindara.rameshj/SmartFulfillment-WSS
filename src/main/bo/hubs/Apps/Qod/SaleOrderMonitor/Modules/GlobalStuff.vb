﻿<Assembly: InternalsVisibleTo("SaleOrderMonitor.UnitTest, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                                   "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                                   "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                                   "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                                   "54e0a4a4")> 
Namespace TpWickes

    Module GlobalStuff

        Friend Function OrderNumberCollection(ByVal SourceOrderNumber As String, _
                                              ByVal StoreOrderNo As String, _
                                              ByVal OrderManagerOrderNo As Integer) As Dictionary(Of String, String)

            Dim Value As New Dictionary(Of String, String)

            Value.Add("Venda Order No", SourceOrderNumber)
            Value.Add("Store Order No", StoreOrderNo)
            Value.Add("Order Manager Order No", OrderManagerOrderNo.ToString)

            Return Value

        End Function

    End Module

End Namespace