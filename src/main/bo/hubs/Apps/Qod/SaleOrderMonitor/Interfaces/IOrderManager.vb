﻿Namespace TpWickes

    Public Interface IOrderManager

        Function OrderManagerRequest(ByRef OM As BaseService, ByVal RequestXml As String) As String
        Function OrderManagerRequest(ByRef OM As BaseService, ByVal RequestXml As String, ByVal DictionaryValue As Dictionary(Of String, String)) As String

    End Interface

End Namespace