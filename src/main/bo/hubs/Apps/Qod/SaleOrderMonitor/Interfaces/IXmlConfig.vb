﻿Namespace TpWickes

    Public Interface IXmlConfig

        Function XSDPathLocation() As String

        Function OrderManagerCreateOrderXSDName() As String

        Function OrderManagerCreateRefundXSDName() As String

        Function OrderManagerStatusUpdateXSDName() As String

        Function GetXSD(ByVal PathAndFileName As String) As XElement

    End Interface

End Namespace