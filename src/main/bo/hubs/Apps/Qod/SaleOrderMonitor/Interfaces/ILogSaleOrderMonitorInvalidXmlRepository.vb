﻿Namespace TpWickes

    Public Interface ILogSaleOrderMonitorInvalidXmlRepository

        Function XmlErrorLoggingRequirementEnabled() As System.Nullable(Of Boolean)

        Function SaveXmlData() As System.Nullable(Of Boolean)

        Function Insert(ByVal StoreOrderNo As String, ByVal OrderManagerOrderNo As System.Nullable(Of Integer), _
                        ByVal DateLogged As Date, ByVal RequestWebServiceType As WebServiceType, _
                        ByVal FailureReason As List(Of String), _
                        ByVal VendaOrderNo As String, _
                        ByVal FailedXml As XDocument) As System.Nullable(Of Integer)

    End Interface

End Namespace