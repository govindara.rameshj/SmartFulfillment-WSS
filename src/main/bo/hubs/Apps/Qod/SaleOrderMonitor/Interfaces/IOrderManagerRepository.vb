﻿Namespace TpWickes

    Public Interface IOrderManagerRepository

        Function XmlValidationRequirementEnabled() As System.Nullable(Of Boolean)

    End Interface

End Namespace