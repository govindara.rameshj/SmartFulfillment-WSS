﻿Namespace TpWickes

    Public Enum WebServiceType
        CreateOrder = 1
        RefundOrder = 2
        StatusUpdate = 3
    End Enum

    Public Interface IXmlValidation

        ReadOnly Property XmlFailureReason() As List(Of String)

        Function ValidateXML(ByVal RequestXml As String, ByVal RequestWebServiceType As WebServiceType) As Boolean

    End Interface

End Namespace