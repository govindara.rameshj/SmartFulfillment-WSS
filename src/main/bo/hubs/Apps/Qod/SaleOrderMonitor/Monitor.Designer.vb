﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Monitor
    Inherits System.Windows.Forms.Form

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Monitor))
        Me.uxOrderTypeGroup = New DevExpress.XtraEditors.GroupControl
        Me.uxRefundRetryRadio = New System.Windows.Forms.RadioButton
        Me.uxRefundNewRadio = New System.Windows.Forms.RadioButton
        Me.uxOrderRetryRadio = New System.Windows.Forms.RadioButton
        Me.uxOrderNewRadio = New System.Windows.Forms.RadioButton
        Me.uxMemoEdit = New DevExpress.XtraEditors.MemoEdit
        Me.uxSendButton = New DevExpress.XtraEditors.SimpleButton
        Me.uxResetButton = New DevExpress.XtraEditors.SimpleButton
        Me.uxDebugRefundButton = New DevExpress.XtraEditors.SimpleButton
        CType(Me.uxOrderTypeGroup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxOrderTypeGroup.SuspendLayout()
        CType(Me.uxMemoEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'uxOrderTypeGroup
        '
        Me.uxOrderTypeGroup.Controls.Add(Me.uxRefundRetryRadio)
        Me.uxOrderTypeGroup.Controls.Add(Me.uxRefundNewRadio)
        Me.uxOrderTypeGroup.Controls.Add(Me.uxOrderRetryRadio)
        Me.uxOrderTypeGroup.Controls.Add(Me.uxOrderNewRadio)
        Me.uxOrderTypeGroup.Location = New System.Drawing.Point(12, 12)
        Me.uxOrderTypeGroup.Name = "uxOrderTypeGroup"
        Me.uxOrderTypeGroup.Size = New System.Drawing.Size(202, 120)
        Me.uxOrderTypeGroup.TabIndex = 3
        Me.uxOrderTypeGroup.Text = "Select Order Type"
        '
        'uxRefundRetryRadio
        '
        Me.uxRefundRetryRadio.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxRefundRetryRadio.Location = New System.Drawing.Point(5, 92)
        Me.uxRefundRetryRadio.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.uxRefundRetryRadio.Name = "uxRefundRetryRadio"
        Me.uxRefundRetryRadio.Size = New System.Drawing.Size(192, 20)
        Me.uxRefundRetryRadio.TabIndex = 3
        Me.uxRefundRetryRadio.Text = "Retry Refunds"
        Me.uxRefundRetryRadio.UseVisualStyleBackColor = True
        '
        'uxRefundNewRadio
        '
        Me.uxRefundNewRadio.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxRefundNewRadio.Location = New System.Drawing.Point(5, 69)
        Me.uxRefundNewRadio.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.uxRefundNewRadio.Name = "uxRefundNewRadio"
        Me.uxRefundNewRadio.Size = New System.Drawing.Size(192, 20)
        Me.uxRefundNewRadio.TabIndex = 2
        Me.uxRefundNewRadio.Text = "New Refunds"
        Me.uxRefundNewRadio.UseVisualStyleBackColor = True
        '
        'uxOrderRetryRadio
        '
        Me.uxOrderRetryRadio.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxOrderRetryRadio.Location = New System.Drawing.Point(5, 46)
        Me.uxOrderRetryRadio.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.uxOrderRetryRadio.Name = "uxOrderRetryRadio"
        Me.uxOrderRetryRadio.Size = New System.Drawing.Size(192, 20)
        Me.uxOrderRetryRadio.TabIndex = 1
        Me.uxOrderRetryRadio.Text = "Retry Orders"
        Me.uxOrderRetryRadio.UseVisualStyleBackColor = True
        '
        'uxOrderNewRadio
        '
        Me.uxOrderNewRadio.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxOrderNewRadio.Checked = True
        Me.uxOrderNewRadio.Location = New System.Drawing.Point(5, 23)
        Me.uxOrderNewRadio.Name = "uxOrderNewRadio"
        Me.uxOrderNewRadio.Size = New System.Drawing.Size(192, 20)
        Me.uxOrderNewRadio.TabIndex = 0
        Me.uxOrderNewRadio.TabStop = True
        Me.uxOrderNewRadio.Text = "New Orders"
        Me.uxOrderNewRadio.UseVisualStyleBackColor = True
        '
        'uxMemoEdit
        '
        Me.uxMemoEdit.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxMemoEdit.Location = New System.Drawing.Point(12, 138)
        Me.uxMemoEdit.Name = "uxMemoEdit"
        Me.uxMemoEdit.Size = New System.Drawing.Size(664, 322)
        Me.uxMemoEdit.TabIndex = 4
        '
        'uxSendButton
        '
        Me.uxSendButton.Location = New System.Drawing.Point(236, 12)
        Me.uxSendButton.Name = "uxSendButton"
        Me.uxSendButton.Size = New System.Drawing.Size(75, 23)
        Me.uxSendButton.TabIndex = 5
        Me.uxSendButton.Text = "Send"
        '
        'uxResetButton
        '
        Me.uxResetButton.Location = New System.Drawing.Point(236, 41)
        Me.uxResetButton.Name = "uxResetButton"
        Me.uxResetButton.Size = New System.Drawing.Size(75, 23)
        Me.uxResetButton.TabIndex = 6
        Me.uxResetButton.Text = "Reset"
        '
        'uxDebugRefundButton
        '
        Me.uxDebugRefundButton.Enabled = False
        Me.uxDebugRefundButton.Location = New System.Drawing.Point(351, 12)
        Me.uxDebugRefundButton.Name = "uxDebugRefundButton"
        Me.uxDebugRefundButton.Size = New System.Drawing.Size(112, 23)
        Me.uxDebugRefundButton.TabIndex = 7
        Me.uxDebugRefundButton.Text = "Debug Response"
        Me.uxDebugRefundButton.Visible = False
        '
        'Monitor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(688, 472)
        Me.Controls.Add(Me.uxDebugRefundButton)
        Me.Controls.Add(Me.uxResetButton)
        Me.Controls.Add(Me.uxSendButton)
        Me.Controls.Add(Me.uxMemoEdit)
        Me.Controls.Add(Me.uxOrderTypeGroup)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "Monitor"
        Me.Text = "Customer Order Monitor"
        CType(Me.uxOrderTypeGroup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxOrderTypeGroup.ResumeLayout(False)
        CType(Me.uxMemoEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents uxOrderTypeGroup As DevExpress.XtraEditors.GroupControl
    Friend WithEvents uxMemoEdit As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents uxSendButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxResetButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxRefundRetryRadio As System.Windows.Forms.RadioButton
    Friend WithEvents uxRefundNewRadio As System.Windows.Forms.RadioButton
    Friend WithEvents uxOrderRetryRadio As System.Windows.Forms.RadioButton
    Friend WithEvents uxOrderNewRadio As System.Windows.Forms.RadioButton
    Friend WithEvents uxDebugRefundButton As DevExpress.XtraEditors.SimpleButton

End Class
