﻿<Assembly: InternalsVisibleTo("SaleOrderMonitor.Database, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                                   "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                                   "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                                   "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                                   "54e0a4a4")> 
Namespace TpWickes

    Public Class LogSaleOrderMonitorInvalidXmlRepositorySql
        Implements ILogSaleOrderMonitorInvalidXmlRepository

        Public Function XmlErrorLoggingRequirementEnabled() As Boolean? Implements ILogSaleOrderMonitorInvalidXmlRepository.XmlErrorLoggingRequirementEnabled

            Return Parameter.GetBoolean(-2290)

        End Function

        Public Function SaveXmlData() As Boolean? Implements ILogSaleOrderMonitorInvalidXmlRepository.SaveXmlData

            Return Parameter.GetBoolean(3001)

        End Function

        Public Function Insert(ByVal StoreOrderNo As String, ByVal OrderManagerOrderNo As Integer?, ByVal DateLogged As Date, ByVal RequestWebServiceType As WebServiceType, ByVal FailureReason As System.Collections.Generic.List(Of String), ByVal VendaOrderNo As String, ByVal FailedXml As System.Xml.Linq.XDocument) As Integer? Implements ILogSaleOrderMonitorInvalidXmlRepository.Insert

            Dim Identity As Integer
            Dim IdentityParameter As SqlParameter

            Using con As New Connection

                Try
                    con.StartTransaction()

                    'header
                    Using com As New Command(con)

                        com.StoredProcedureName = "LogSOM_InvalidXmlInsert"

                        If VendaOrderNo IsNot Nothing Then com.AddParameter("@VendaOrderNo", VendaOrderNo, SqlDbType.NVarChar, 20)
                        com.AddParameter("@StoreOrderNo", StoreOrderNo, SqlDbType.NVarChar, 6)
                        If OrderManagerOrderNo.HasValue = True Then com.AddParameter("@OrderManagerOrderNo", OrderManagerOrderNo, SqlDbType.Int)
                        com.AddParameter("@DateLogged", DateLogged, SqlDbType.DateTime)
                        com.AddParameter("@RequestTypeID", RequestWebServiceType, SqlDbType.Int)
                        If FailedXml IsNot Nothing Then com.AddParameter("@FailedXML", FailedXml.ToString, SqlDbType.Xml)

                        IdentityParameter = com.AddOutputParameter("@IdentityValue", SqlDbType.Int, ParameterDirection.Output)

                        com.ExecuteNonQuery()

                        Identity = CType(IdentityParameter.Value, Integer)

                    End Using

                    'detail
                    For Each Failure As String In FailureReason

                        Using com As New Command(con)

                            com.StoredProcedureName = "LogSOM_InvalidXmlDetailInsert"

                            com.AddParameter("@LogID", Identity, SqlDbType.Int)
                            com.AddParameter("@FailureReason", Failure, SqlDbType.NVarChar, 255)

                            com.ExecuteNonQuery()

                        End Using

                    Next

                    con.CommitTransaction()

                    Return Identity

                Catch ex As Exception

                    con.RollbackTransaction()

                    Return New System.Nullable(Of Integer)

                End Try

            End Using

        End Function

    End Class

End Namespace