﻿Namespace TpWickes

    Public Class OrderManagerRepositoryStub
        Implements IOrderManagerRepository

#Region "Stub Code"

        Private _RequirementEnabled As System.Nullable(Of Boolean)

        Friend Sub ConfigureStub(ByVal RequirementEnabled As System.Nullable(Of Boolean))

            _RequirementEnabled = RequirementEnabled

        End Sub

#End Region

#Region "Interface"

        Public Function XmlValidationRequirementEnabled() As Boolean? Implements IOrderManagerRepository.XmlValidationRequirementEnabled

            Return _RequirementEnabled

        End Function

#End Region

    End Class

End Namespace