﻿<Assembly: InternalsVisibleTo("SaleOrderMonitor.Database, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                                   "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                                   "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                                   "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                                   "54e0a4a4")> 
Namespace TpWickes

    Public Class LogSaleOrderMonitorInvalidXmlRepositoryPervasive
        Implements ILogSaleOrderMonitorInvalidXmlRepository

        Public Function XmlErrorLoggingRequirementEnabled() As Boolean? Implements ILogSaleOrderMonitorInvalidXmlRepository.XmlErrorLoggingRequirementEnabled

            Return Parameter.GetBoolean(-2290)

        End Function

        Public Function SaveXmlData() As Boolean? Implements ILogSaleOrderMonitorInvalidXmlRepository.SaveXmlData

            Return Parameter.GetBoolean(3001)

        End Function

        Public Function Insert(ByVal StoreOrderNo As String, ByVal OrderManagerOrderNo As Integer?, ByVal DateLogged As Date, ByVal RequestWebServiceType As WebServiceType, ByVal FailureReason As System.Collections.Generic.List(Of String), ByVal VendaOrderNo As String, ByVal FailedXml As System.Xml.Linq.XDocument) As Integer? Implements ILogSaleOrderMonitorInvalidXmlRepository.Insert

            Dim SB As New StringBuilder
            Dim ID As Integer
            Dim LineNo As Integer

            Using con As New Connection

                Try

                    ID = NextID(con)        'get next id
                    If ID = 0 Then Return 0 'failed to retrieve id value

                    con.StartTransaction()

                    'header
                    Using com As New Command(con)

                        SB.AppendLine("insert into LogSOMBadXML                    (                         ")
                        SB.AppendLine("                                                 ID,                  ")
                        If VendaOrderNo IsNot Nothing Then SB.AppendLine("              VendaOrderNo,        ")
                        SB.AppendLine("                                                 StoreOrderNo,        ")
                        If OrderManagerOrderNo.HasValue = True Then SB.AppendLine("     OrderManagerOrderNo, ")
                        SB.AppendLine("                                                 DateLoggedDate,      ")
                        SB.AppendLine("                                                 DateLoggedTime,      ")
                        SB.AppendLine("                                                 RequestType          ")
                        SB.AppendLine("                                            )                         ")
                        SB.AppendLine("                                     values (                         ")
                        SB.AppendLine("                                                 ?,                   ")
                        If VendaOrderNo IsNot Nothing Then SB.AppendLine("              ?,                   ")
                        SB.AppendLine("                                                 ?,                   ")
                        If OrderManagerOrderNo.HasValue = True Then SB.AppendLine("     ?,                   ")
                        SB.AppendLine("                                                 ?,                   ")
                        SB.AppendLine("                                                 ?,                   ")
                        SB.AppendLine("                                                 ?                    ")
                        SB.AppendLine("                                            )                         ")

                        com.CommandText = SB.ToString

                        com.AddParameter("ID", ID)
                        If VendaOrderNo IsNot Nothing Then com.AddParameter("VendaOrderNo", VendaOrderNo)
                        com.AddParameter("StoreOrderNo", StoreOrderNo)
                        If OrderManagerOrderNo.HasValue = True Then com.AddParameter("OrderManagerOrderNo", OrderManagerOrderNo)
                        com.AddParameter("DateLoggedDate", DateLogged)
                        com.AddParameter("DateLoggedTime", DateLogged)
                        com.AddParameter("RequestType", RequestWebServiceTypeConvertToString(RequestWebServiceType))

                        com.ExecuteNonQuery()

                    End Using

                    'detail
                    LineNo = 0
                    For Each Failure As String In FailureReason

                        LineNo += 1

                        Using com As New Command(con)

                            SB.Remove(0, SB.Length)
                            SB.Append("insert into LogSOMBadXmlDetail (LogID, LineNo, FailureReason) values (?, ?, ?) ")

                            com.CommandText = SB.ToString

                            com.AddParameter("LogID", ID)
                            com.AddParameter("LineNo", LineNo)
                            com.AddParameter("FailureReason", Failure)

                            com.ExecuteNonQuery()

                        End Using

                    Next

                    con.CommitTransaction()

                    Return ID

                Catch ex As Exception

                    con.RollbackTransaction()

                    Return New System.Nullable(Of Integer)

                End Try

            End Using

        End Function

#Region "Private, Protected, Friend Functions And Procedures"

        Friend Function NextID(ByRef Con As Connection) As Integer

            Dim ID As Integer

            Try

                Con.StartTransaction()

                Using com As New Command(Con)

                    com.CommandText = "select  NextID from LogSOMBadXmlNextID"
                    ID = CType(com.ExecuteValue, Integer)

                    com.CommandText = "Update LogSOMBadXmlNextID set  NextID=?"
                    com.AddParameter(" NextID", ID + 1)
                    com.ExecuteNonQuery()

                End Using

                Con.CommitTransaction()

            Catch ex As Exception

                Con.RollbackTransaction()

                ID = 0

            End Try

            Return ID

        End Function

        Friend Function RequestWebServiceTypeConvertToString(ByVal RequestWebServiceType As WebServiceType) As String

            Dim StringValue As String = String.Empty

            Select Case RequestWebServiceType

                Case WebServiceType.CreateOrder

                    StringValue = "Create Order"

                Case WebServiceType.RefundOrder

                    StringValue = "Refund Order"

                Case WebServiceType.StatusUpdate

                    StringValue = "Status Update"

            End Select

            Return StringValue

        End Function

#End Region

    End Class

End Namespace