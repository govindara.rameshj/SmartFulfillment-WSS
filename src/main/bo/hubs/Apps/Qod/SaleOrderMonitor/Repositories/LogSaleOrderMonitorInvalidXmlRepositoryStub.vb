﻿Namespace TpWickes

    Public Class LogSaleOrderMonitorInvalidXmlRepositoryStub
        Implements ILogSaleOrderMonitorInvalidXmlRepository

#Region "Stub Code"

        Private _XmlErrorLoggingRequirementEnabled As System.Nullable(Of Boolean)
        Private _SaveXmlData As System.Nullable(Of Boolean)
        Private _ID As System.Nullable(Of Integer)

        Friend Sub ConfigureStub(ByVal XmlErrorLoggingRequirementEnabled As System.Nullable(Of Boolean), _
                                 ByVal SaveXmlData As System.Nullable(Of Boolean), ByVal ID As System.Nullable(Of Integer))

            _XmlErrorLoggingRequirementEnabled = XmlErrorLoggingRequirementEnabled
            _SaveXmlData = SaveXmlData
            _ID = ID

        End Sub

#End Region

#Region "Interface"

        Public Function XmlErrorLoggingRequirementEnabled() As Boolean? Implements ILogSaleOrderMonitorInvalidXmlRepository.XmlErrorLoggingRequirementEnabled

            Return _XmlErrorLoggingRequirementEnabled

        End Function

        Public Function SaveXmlData() As Boolean? Implements ILogSaleOrderMonitorInvalidXmlRepository.SaveXmlData

            Return _SaveXmlData

        End Function

        Public Function Insert(ByVal StoreOrderNo As String, ByVal OrderManagerOrderNo As Integer?, ByVal DateLogged As Date, ByVal RequestWebServiceType As WebServiceType, ByVal FailureReason As System.Collections.Generic.List(Of String), ByVal VendaOrderNo As String, ByVal FailedXml As System.Xml.Linq.XDocument) As Integer? Implements ILogSaleOrderMonitorInvalidXmlRepository.Insert

            'ignore input parameters

            Return _ID

        End Function

#End Region

    End Class

End Namespace