﻿Namespace TpWickes

    Public Class OrderManagerRepository
        Implements IOrderManagerRepository

        Public Function XmlValidationRequirementEnabled() As Boolean? Implements IOrderManagerRepository.XmlValidationRequirementEnabled

            Return Parameter.GetBoolean(-2289)

        End Function

    End Class

End Namespace