﻿Namespace TpWickes

    Public Class XmlConfigFactory

        Private Shared m_FactoryMember As IXmlConfig

        Public Shared Function FactoryGet() As IXmlConfig

            If m_FactoryMember Is Nothing Then

                Return New XmlConfig              'live implementation

            Else

                Return m_FactoryMember            'stub implementation

            End If

        End Function

        Public Shared Sub FactorySet(ByVal obj As IXmlConfig)

            m_FactoryMember = obj

        End Sub

    End Class

End Namespace