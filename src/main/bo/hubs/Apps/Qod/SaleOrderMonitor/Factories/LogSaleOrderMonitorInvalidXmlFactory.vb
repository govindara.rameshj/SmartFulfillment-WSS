﻿Namespace TpWickes

    Public Class LogSaleOrderMonitorInvalidXmlFactory

        Private Shared m_FactoryMember As ILogSaleOrderMonitorInvalidXml

        Public Shared Function FactoryGet() As ILogSaleOrderMonitorInvalidXml

            If m_FactoryMember Is Nothing Then

                If XmlValidationRequirementEnabled() = True Then

                    Return New LogSaleOrderMonitorInvalidXml            'live implementation: new

                Else

                    Return New LogSaleOrderMonitorInvalidXmlEmpty       'live implementation: nothing

                End If

            Else

                Return m_FactoryMember                                 'stub implementation

            End If

        End Function

        Public Shared Sub FactorySet(ByVal obj As ILogSaleOrderMonitorInvalidXml)

            m_FactoryMember = obj

        End Sub

#Region "Private Functions And Procedures"

        Private Shared Function XmlValidationRequirementEnabled() As Boolean

            Dim Repository As ILogSaleOrderMonitorInvalidXmlRepository

            Repository = LogSaleOrderMonitorInvalidXmlRepositoryFactory.FactoryGet

            If Repository.XmlErrorLoggingRequirementEnabled.HasValue = False Then Return False

            Return Repository.XmlErrorLoggingRequirementEnabled.Value

        End Function

#End Region

    End Class

End Namespace