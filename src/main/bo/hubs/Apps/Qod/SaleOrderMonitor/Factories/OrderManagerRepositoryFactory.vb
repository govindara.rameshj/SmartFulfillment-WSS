﻿Namespace TpWickes

    Public Class OrderManagerRepositoryFactory

        Private Shared m_FactoryMember As IOrderManagerRepository

        Public Shared Function FactoryGet() As IOrderManagerRepository

            If m_FactoryMember Is Nothing Then

                Return New OrderManagerRepository             'live implementation

            Else

                Return m_FactoryMember                        'stub implementation

            End If

        End Function

        Public Shared Sub FactorySet(ByVal obj As IOrderManagerRepository)

            m_FactoryMember = obj

        End Sub

    End Class

End Namespace