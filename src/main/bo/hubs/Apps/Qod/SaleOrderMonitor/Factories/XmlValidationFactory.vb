﻿Namespace TpWickes

    Public Class XmlValidationFactory

        Private Shared m_FactoryMember As IXmlValidation

        Public Shared Function FactoryGet() As IXmlValidation

            If m_FactoryMember Is Nothing Then

                If XmlErrorLoggingRequirementEnabled() = False Then

                    Return New XmlValidation                         'live implementation: existing validated version

                Else

                    Return New XmlValidationMultipleErrorVersion     'live implementation: new validated & logging version

                End If

            Else

                Return m_FactoryMember              'stub implementation

            End If

        End Function

        Public Shared Sub FactorySet(ByVal obj As IXmlValidation)

            m_FactoryMember = obj

        End Sub

#Region "Private Functions And Procedures"

        Private Shared Function XmlErrorLoggingRequirementEnabled() As Boolean

            Dim Repository As ILogSaleOrderMonitorInvalidXmlRepository

            Repository = LogSaleOrderMonitorInvalidXmlRepositoryFactory.FactoryGet

            If Repository.XmlErrorLoggingRequirementEnabled.HasValue = False Then Return False

            Return Repository.XmlErrorLoggingRequirementEnabled.Value

        End Function

#End Region

    End Class

End Namespace