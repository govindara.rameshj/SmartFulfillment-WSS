﻿Namespace TpWickes

    Public Class OrderManagerFactory

        Private Shared m_FactoryMember As IOrderManager

        Public Shared Function FactoryGet() As IOrderManager

            If m_FactoryMember Is Nothing Then

                If XmlValidationRequirementEnabled() = True Then

                    If XmlErrorLoggingRequirementEnabled() = False Then

                        Return New OrderManagerValidatedVersion            'live implementation: new validated version

                    Else

                        Return New OrderManagerValidatedLoggingVersion     'live implementation: new validated & logging version

                    End If

                Else

                    Return New OrderManagerCurrent                         'live implementation: existing

                End If

            Else

                Return m_FactoryMember                                     'stub implementation

            End If

        End Function

        Public Shared Sub FactorySet(ByVal obj As IOrderManager)

            m_FactoryMember = obj

        End Sub

#Region "Private Functions And Procedures"

        Private Shared Function XmlValidationRequirementEnabled() As Boolean

            Dim Repository As IOrderManagerRepository

            Repository = OrderManagerRepositoryFactory.FactoryGet

            If Repository.XmlValidationRequirementEnabled.HasValue = False Then Return False

            Return Repository.XmlValidationRequirementEnabled.Value

        End Function

        Private Shared Function XmlErrorLoggingRequirementEnabled() As Boolean

            Dim Repository As ILogSaleOrderMonitorInvalidXmlRepository

            Repository = LogSaleOrderMonitorInvalidXmlRepositoryFactory.FactoryGet

            If Repository.XmlErrorLoggingRequirementEnabled.HasValue = False Then Return False

            Return Repository.XmlErrorLoggingRequirementEnabled.Value

        End Function

#End Region

    End Class

End Namespace