﻿Namespace TpWickes

    Public Class LogSaleOrderMonitorInvalidXmlRepositoryFactory

        Private Shared m_FactoryMember As ILogSaleOrderMonitorInvalidXmlRepository

        Public Shared Function FactoryGet() As ILogSaleOrderMonitorInvalidXmlRepository

            If m_FactoryMember Is Nothing Then

                'select database engine
                Select Case Cts.Oasys.Hubs.Data.GetDataProvider

                    Case Cts.Oasys.Hubs.Data.DataProvider.Odbc

                        Return New LogSaleOrderMonitorInvalidXmlRepositoryPervasive

                    Case Else

                        Return New LogSaleOrderMonitorInvalidXmlRepositorySQL

                End Select

            Else

                Return m_FactoryMember                        'stub implementation

            End If

        End Function

        Public Shared Sub FactorySet(ByVal obj As ILogSaleOrderMonitorInvalidXmlRepository)

            m_FactoryMember = obj

        End Sub

    End Class

End Namespace