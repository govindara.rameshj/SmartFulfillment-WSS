﻿Public Class Monitor

    Private _output As Output
    Private m_Error As Boolean = False

    Public Sub New()

        InitializeComponent()
        AddHandler Application.ThreadException, New ThreadExceptionEventHandler(AddressOf ApplicationThreadException)
        Trace.WriteLine("Creating New Monitor", Application.ProductName)
    End Sub

    Private Sub ApplicationThreadException(ByVal sender As Object, ByVal e As System.Threading.ThreadExceptionEventArgs)

        'trace log and write to event log
        Trace.WriteLine(e.Exception.ToString, Application.ProductName)
        EventLog.WriteEntry("Application", e.Exception.ToString, EventLogEntryType.Error)
        If _output IsNot Nothing Then
            _output.Write("ERROR: " & e.Exception.ToString)
            _output.Dispose()
        End If
        Me.Close()
    End Sub

    Private Sub Monitor_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        Trace.WriteLine("Monitor Form closing", Application.ProductName)
        If _output IsNot Nothing Then
            _output.Dispose()
        End If
    End Sub

    Private Sub Monitor_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' Referral 802
        ' Further changes to get the check for different existing instance of Sale Order Monitor process running.
        ' Added to some of the logging/debugging messages at the same time
        Try
            If MonitorAlreadyRunning() Then
                Trace.WriteLine("Different 'Monitor' process already running, so closing this one down.", Application.ProductName & ".Monitor_Load")
                Me.Close()
            Else
                Trace.WriteLine("Setting up log file", Application.ProductName & ".Monitor_Load")
                _output = New Output
                _output.Write()
                _output.Write("Accessed " & Now.ToLocalTime)

                If Not Hubs.Core.System.Parameter.IsQodMonitorSwitchedOn Then
                    _output.WriteMonitorNotSwitchedOn()
                    Trace.WriteLine("Monitor not switched on.", Application.ProductName & ".Monitor_Load")
                    Me.Close()
                Else
                    If My.Application.CommandLineArgs.Count > 0 Then
                        Trace.WriteLine("Command line arguments=" & My.Application.CommandLineArgs(0), Application.ProductName & ".Monitor_Load")
                        Select Case My.Application.CommandLineArgs(0).ToLower
                            Case "all"
                                'MO'C - 01/04/2011
                                'Added this for store 120 to try to stop 4 copies of SaleOrderMonitor from running.
                                'Step 1. Process any New Sales Orders
                                Trace.WriteLine("Processing 'all' option", Application.ProductName & ".Monitor_Load")
                                uxOrderNewRadio.Checked = True
                                Trace.WriteLine("uxOrderNewRadio.Checked=" & uxOrderNewRadio.Checked.ToString, Application.ProductName & ".Monitor_Load")
                                Application.DoEvents()
                                Trace.WriteLine("Calling ProcessSaleOrders - new", Application.ProductName & ".Monitor_Load")
                                ProcessSalesOrders()
                                Trace.WriteLine("Finished ProcessSaleOrders - new", Application.ProductName & ".Monitor_Load")

                                'Step 2. Process any New Refund Orders
                                uxRefundNewRadio.Checked = True
                                Trace.WriteLine("uxRefundNewRadio.Checked=" & uxRefundNewRadio.Checked.ToString, Application.ProductName & ".Monitor_Load")
                                Application.DoEvents()
                                Trace.WriteLine("Calling ProcessRefunds - new", Application.ProductName & ".Monitor_Load")
                                ProcessRefunds()
                                Trace.WriteLine("Finished ProcessRefunds - new", Application.ProductName & ".Monitor_Load")

                                'Step 3. Process any Sales retry Orders
                                uxOrderRetryRadio.Checked = True
                                Trace.WriteLine("uxOrderRetryRadio.Checked=" & uxOrderRetryRadio.Checked.ToString, Application.ProductName & ".Monitor_Load")
                                Application.DoEvents()
                                Trace.WriteLine("Calling ProcessSaleOrders - retry", Application.ProductName & ".Monitor_Load")
                                ProcessSalesOrders()
                                Trace.WriteLine("Finished ProcessSaleOrders - retry", Application.ProductName & ".Monitor_Load")

                                'Step 4. Process any New Refund retry Orders
                                uxRefundRetryRadio.Checked = True
                                Trace.WriteLine("uxRefundRetryRadio.Checked=" & uxRefundRetryRadio.Checked.ToString, Application.ProductName & ".Monitor_Load")
                                Application.DoEvents()
                                Trace.WriteLine("Calling ProcessRefunds - retry", Application.ProductName & ".Monitor_Load")
                                ProcessRefunds()
                                Trace.WriteLine("Finished ProcessRefunds - retry", Application.ProductName & ".Monitor_Load")
                                Trace.WriteLine("Finished processing 'all' option", Application.ProductName & ".Monitor_Load")
                            Case "new"
                                Trace.WriteLine("Processing 'new' option", Application.ProductName & ".Monitor_Load")
                                uxOrderNewRadio.Checked = True
                                Trace.WriteLine("uxOrderNewRadio.Checked=" & uxOrderNewRadio.Checked.ToString, Application.ProductName & ".Monitor_Load")
                                Trace.WriteLine("Calling ProcessSaleOrders - new", Application.ProductName & ".Monitor_Load")
                                ProcessSalesOrders()
                                Trace.WriteLine("Finished ProcessSaleOrders - new", Application.ProductName & ".Monitor_Load")
                                Trace.WriteLine("Finished processing 'new' option", Application.ProductName & ".Monitor_Load")
                            Case "retry"
                                Trace.WriteLine("Processing 'retry' option", Application.ProductName & ".Monitor_Load")
                                uxOrderRetryRadio.Checked = True
                                Trace.WriteLine("uxOrderRetryRadio.Checked=" & uxOrderRetryRadio.Checked.ToString, Application.ProductName & ".Monitor_Load")
                                Trace.WriteLine("Calling ProcessSaleOrders - retry", Application.ProductName & ".Monitor_Load")
                                ProcessSalesOrders()
                                Trace.WriteLine("Finished ProcessSaleOrders - retry", Application.ProductName & ".Monitor_Load")
                                Trace.WriteLine("Finished processing 'retry' option", Application.ProductName & ".Monitor_Load")
                            Case "refundnew"
                                Trace.WriteLine("Processing 'refundnew' option", Application.ProductName & ".Monitor_Load")
                                uxRefundNewRadio.Checked = True
                                Trace.WriteLine("uxRefundNewRadio.Checked=" & uxRefundNewRadio.Checked.ToString, Application.ProductName & ".Monitor_Load")
                                Trace.WriteLine("Calling ProcessRefunds - new", Application.ProductName & ".Monitor_Load")
                                ProcessRefunds()
                                Trace.WriteLine("Finished ProcessRefunds - new", Application.ProductName & ".Monitor_Load")
                                Trace.WriteLine("Finished processing 'refundnew' option", Application.ProductName & ".Monitor_Load")
                            Case "refundretry"
                                Trace.WriteLine("Processing 'refundretry' option", Application.ProductName & ".Monitor_Load")
                                uxRefundRetryRadio.Checked = True
                                Trace.WriteLine("uxRefundRetryRadio.Checked=" & uxRefundRetryRadio.Checked.ToString, Application.ProductName & ".Monitor_Load")
                                Trace.WriteLine("Calling ProcessRefunds - retry", Application.ProductName & ".Monitor_Load")
                                ProcessRefunds()
                                Trace.WriteLine("Finished ProcessRefunds - retry", Application.ProductName & ".Monitor_Load")
                                Trace.WriteLine("Finished processing 'refundretry' option", Application.ProductName & ".Monitor_Load")
                        End Select

                        Me.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            Trace.WriteLine("Failed in Monitor_Load.  " & ex.Message, Application.ProductName & ".Monitor_Load")
            If _output IsNot Nothing Then
                _output.Write("Failed in Monitor_Load.  " & ex.Message)
                _output.Dispose()
            End If
            Me.Close()
        End Try
        ' End of Referral 802
    End Sub

    Private Sub ProcessSalesOrders()
        Dim qods As Qod.QodHeaderCollection
        Dim forWhat As String = IIf(uxOrderNewRadio.Checked, "new", "retry").ToString
        Dim DuplicateOrder As Boolean

        Try
            Cursor = Cursors.WaitCursor
            ResetControls()
            'Get Sales Orders
            Trace.WriteLine("Getting " & forWhat & " sale orders.", Application.ProductName & ".ProcessSaleOrders")
            qods = GetSaleOrders()
            'Are there any orders to process ?
            If qods.IsEmpty Then ' No so finish
                _output.WriteNoQodsReturned()
                Trace.WriteLine("No orders returned", Application.ProductName & ".ProcessSaleOrders")
                Exit Sub
            Else
                Dim toOut As New StringBuilder

                With toOut
                    .AppendLine("Got " & forWhat & " orders to process...")
                    For Each qh As Qod.QodHeader In qods
                        .AppendLine(vbTab & "Order: No=" & qh.Number & ", OM No=" & qh.OmOrderNumber & ", WO No=" & qh.SourceOrderNumber & ", Delivery Status=" & qh.DeliveryStatus & ", Refund Status=" & qh.RefundStatus & ".")
                    Next
                    .AppendLine("End of " & forWhat & " orders to be processed list.")
                    _output.Write(.ToString)
                    Trace.WriteLine(.ToString, Application.ProductName & ".ProcessSaleOrders")
                End With
            End If

            'There are Orders to process Create Request XML
            For Each qodHeader As Qod.QodHeader In qods
                Trace.WriteLine("Sale Order Monitor: Progressing to Web Service for order (Order No=" & qodHeader.Number & ").", Application.ProductName & ".ProcessSaleOrders")

                Dim orderManagerService As BaseService = GetOrderManagerService(qodHeader)
                Dim request As BaseServiceArgument = Nothing
                Dim response As BaseServiceArgument = Nothing

                Try
                    DuplicateOrder = False
                    'Create Request XML
                    Trace.WriteLine("Setting up 'create request'.", Application.ProductName & ".ProcessSaleOrders")
                    request = orderManagerService.GetRequest
                    request.SetFieldsFromQod(qodHeader)

                    Dim requestXml As String = request.Serialise

                    Trace.WriteLine("Set up 'create request'." & vbNewLine & requestXml, Application.ProductName & ".ProcessSaleOrders")

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    'Author         : Partha Dutta
                    'Date           : 09/09/2011
                    'Change Request : CR0054-01
                    'TFS User Story : 2289
                    'TFS Task ID    : 2371
                    'Notes          : Validate outgoing requests to OM
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    'existing code
                    'Dim responseXml As String = orderManagerService.GetResponseXml(requestXml)

                    Dim responseXml As String
                    Dim OM As TpWickes.IOrderManager
                    Dim ErrorMessage As String

                    OM = TpWickes.OrderManagerFactory.FactoryGet


                    'Send Request to Order Manager
                    _output.Write("Request (sent at " & Now.ToLocalTime & ") - " & requestXml)
                    Trace.WriteLine("Sending request at " & Now.ToLocalTime, Application.ProductName & ".ProcessSaleOrders")


                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    'Author         : Partha Dutta
                    'Date           : 15/09/2011
                    'Change Request : CR0054-02
                    'TFS User Story : 2290
                    'TFS Task ID    : 2374
                    'Notes          : Log Sales Order Monitor invalid requests to Order Manager
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    'existing code
                    'responseXml = OM.OrderManagerRequest(orderManagerService, requestXml)

                    Dim DictionaryValue As New Dictionary(Of String, String)

                    DictionaryValue = TpWickes.OrderNumberCollection(qodHeader.SourceOrderNumber, qodHeader.Number, qodHeader.OmOrderNumber)

                    responseXml = OM.OrderManagerRequest(orderManagerService, requestXml, DictionaryValue)


                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


                    _output.Write("Response (receieved at " & Now.ToLocalTime & ") - " & responseXml)
                    Trace.WriteLine("Response from OM to request (receieved at " & Now.ToLocalTime & ")..." & vbNewLine & responseXml, Application.ProductName & ".ProcessSaleOrders")



                    If responseXml = "Failed Validation - Order" Then

                        _output.Write("Sale Order Monitor: OM Order failed XSD Validation")
                        Trace.WriteLine("Sale Order Monitor: OM Order failed XSD Validation")

                        'existing code replicated from - "Catch ex As Exception"

                        ErrorMessage = "Order failed XSD validation"

                        Trace.WriteLine("Failed processing " & forWhat & " sale orders.  " & ErrorMessage, Application.ProductName & ".ProcessSaleOrders")
                        _output.Write(ErrorMessage)
                        'header
                        Trace.WriteLine("Setting order header (Order No=" & qodHeader.Number & ") delivery status to " & Qod.State.Delivery.OrderFailedConnection & ".", Application.ProductName & ".ProcessSaleOrders")
                        qodHeader.SetDeliveryStatus(Qod.State.Delivery.OrderFailedConnection)
                        Trace.WriteLine("Set order header (Order No=" & qodHeader.Number & ") delivery status to " & qodHeader.DeliveryStatus & ".", Application.ProductName & ".ProcessSaleOrders")
                        'lines
                        Trace.WriteLine("Setting order header (Order No=" & qodHeader.Number & ") lines delivery status to " & Qod.State.Delivery.OrderFailedConnection & ".", Application.ProductName & ".ProcessSaleOrders")
                        qodHeader.Lines.SetDeliveryStatus(Qod.State.Delivery.OrderFailedConnection)
                        Trace.WriteLine("Set order header (Order No=" & qodHeader.Number & ") lines delivery status to " & qodHeader.Lines.DeliveryStatus & ".", Application.ProductName & ".ProcessSaleOrders")

                        Exit Try

                    End If

                    If responseXml = "Failed Validation - Update" Then

                        _output.Write("Sale Order Monitor: OM Update failed XSD Validation")
                        Trace.WriteLine("Sale Order Monitor: OM Update failed XSD Validation")

                        'existing code replicated from below

                        _output.WriteNothingReturnedFromOm()
                        Trace.WriteLine("Sale Order Monitor: Sale failed.  Setting order header (Order No=" & qodHeader.Number & ") delivery status to " & Qod.State.Delivery.OrderFailedData & ".", Application.ProductName & ".ProcessSaleOrders")
                        qodHeader.SetDeliveryStatus(Qod.State.Delivery.OrderFailedData)
                        Trace.WriteLine("Set order header (Order No=" & qodHeader.Number & ") delivery status to " & qodHeader.DeliveryStatus & ".", Application.ProductName & ".ProcessSaleOrders")
                        Trace.WriteLine("Setting order header (Order No=" & qodHeader.Number & ") lines delivery status to " & Qod.State.Delivery.OrderFailedData & ".", Application.ProductName & ".ProcessSaleOrders")
                        qodHeader.Lines.SetDeliveryStatus(Qod.State.Delivery.OrderFailedData)
                        Trace.WriteLine("Set order header (Order No=" & qodHeader.Number & ") lines delivery status to " & qodHeader.Lines.DeliveryStatus & ".", Application.ProductName & ".ProcessSaleOrders")

                        Exit Try

                    End If


                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


                    If responseXml Is Nothing OrElse responseXml.Length = 0 Then 'If failed response

                        'this code has been replicated above in the "failed validation - update" process
                        'not sure if other error conditions exist that could return an empty or blank string

                        _output.WriteNothingReturnedFromOm()
                        Trace.WriteLine("Sale Order Monitor: Sale failed.  Setting order header (Order No=" & qodHeader.Number & ") delivery status to " & Qod.State.Delivery.OrderFailedData & ".", Application.ProductName & ".ProcessSaleOrders")
                        qodHeader.SetDeliveryStatus(Qod.State.Delivery.OrderFailedData)
                        Trace.WriteLine("Set order header (Order No=" & qodHeader.Number & ") delivery status to " & qodHeader.DeliveryStatus & ".", Application.ProductName & ".ProcessSaleOrders")
                        Trace.WriteLine("Setting order header (Order No=" & qodHeader.Number & ") lines delivery status to " & Qod.State.Delivery.OrderFailedData & ".", Application.ProductName & ".ProcessSaleOrders")
                        qodHeader.Lines.SetDeliveryStatus(Qod.State.Delivery.OrderFailedData)
                        Trace.WriteLine("Set order header (Order No=" & qodHeader.Number & ") lines delivery status to " & qodHeader.Lines.DeliveryStatus & ".", Application.ProductName & ".ProcessSaleOrders")

                    Else

                        'We had a response from Order Manager
                        Trace.WriteLine("Sale Order Monitor: Processing response from OM to order (Order No=" & qodHeader.Number & ") .", Application.ProductName & ".ProcessSaleOrders")
                        response = orderManagerService.GetResponse
                        response = response.Deserialise(responseXml)
                        response.SetFieldsInQod(qodHeader)
                        ' Referral 802
                        ' Remove the Duplicate order check now that only 1 instance of SOMonitor will run at any one time
                        ' ...call was here
                        ' End of referral 802
                        Trace.WriteLine("Sale Order Monitor: Processed response from OM to order (Order No=" & qodHeader.Number & ").", Application.ProductName & ".ProcessSaleOrders")

                    End If

                Catch ex As TimeoutException

                    Trace.WriteLine("Timed out processing " & forWhat & " sale orders.  " & ex.Message, Application.ProductName & ".ProcessSaleOrders")
                    _output.Write(ex.ToString)
                    Trace.WriteLine("Setting order header (Order No=" & qodHeader.Number & ") delivery status to " & Qod.State.Delivery.OrderFailedConnection & ".", Application.ProductName & ".ProcessSaleOrders")
                    qodHeader.SetDeliveryStatus(Qod.State.Delivery.OrderFailedConnection)
                    Trace.WriteLine("Set order header (Order No=" & qodHeader.Number & ") delivery status to " & qodHeader.DeliveryStatus & ".", Application.ProductName & ".ProcessSaleOrders")
                    Trace.WriteLine("Setting order header (Order No=" & qodHeader.Number & ") lines delivery status to " & Qod.State.Delivery.OrderFailedConnection & ".", Application.ProductName & ".ProcessSaleOrders")
                    qodHeader.Lines.SetDeliveryStatus(Qod.State.Delivery.OrderFailedConnection)
                    Trace.WriteLine("Set order header (Order No=" & qodHeader.Number & ") lines delivery status to " & qodHeader.Lines.DeliveryStatus & ".", Application.ProductName & ".ProcessSaleOrders")

                Catch ex As Exception

                    'this code has been replicated above in the "failed validation - order" process
                    'not sure if other error conditions exist that could call this

                    Trace.WriteLine("Failed processing " & forWhat & " sale orders.  " & ex.Message, Application.ProductName & ".ProcessSaleOrders")
                    _output.Write(ex.ToString)
                    Trace.WriteLine("Setting order header (Order No=" & qodHeader.Number & ") delivery status to " & Qod.State.Delivery.OrderFailedConnection & ".", Application.ProductName & ".ProcessSaleOrders")
                    qodHeader.SetDeliveryStatus(Qod.State.Delivery.OrderFailedConnection)
                    Trace.WriteLine("Set order header (Order No=" & qodHeader.Number & ") delivery status to " & qodHeader.DeliveryStatus & ".", Application.ProductName & ".ProcessSaleOrders")
                    Trace.WriteLine("Setting order header (Order No=" & qodHeader.Number & ") lines delivery status to " & Qod.State.Delivery.OrderFailedConnection & ".", Application.ProductName & ".ProcessSaleOrders")
                    qodHeader.Lines.SetDeliveryStatus(Qod.State.Delivery.OrderFailedConnection)
                    Trace.WriteLine("Set order header (Order No=" & qodHeader.Number & ") lines delivery status to " & qodHeader.Lines.DeliveryStatus & ".", Application.ProductName & ".ProcessSaleOrders")

                Finally

                    If Not DuplicateOrder Then
                        Using con As New Connection
                            Try
                                Trace.WriteLine("Committing " & forWhat & " order (Order No=" & qodHeader.Number & ") at " & Now.ToLocalTime & ".", Application.ProductName & ".ProcessSaleOrders")
                                Trace.WriteLine("Starting transaction.", Application.ProductName & ".ProcessSaleOrders")
                                con.StartTransaction()
                                Trace.WriteLine("Saving order.", Application.ProductName & ".ProcessSaleOrders")
                                qodHeader.PersistTree(con)
                                Trace.WriteLine("Saved order.", Application.ProductName & ".ProcessSaleOrders")
                                Trace.WriteLine("Committing transaction.", Application.ProductName & ".ProcessSaleOrders")
                                con.CommitTransaction()
                                Trace.WriteLine("Committed transaction at " & Now.ToLocalTime & ".", Application.ProductName & ".ProcessSaleOrders")
                            Catch ex As Exception
                                Trace.WriteLine("Failed committing " & forWhat & " order.  " & ex.Message, Application.ProductName & ".ProcessSaleOrders")
                                Trace.WriteLine("Rolling back order.")
                                con.RollbackTransaction()
                                Trace.WriteLine("Rolled back order.")
                                _output.Write(ex.ToString)
                                Throw ex
                            End Try
                        End Using
                    End If

                End Try
                Trace.WriteLine("Sale Order Monitor: Finished processing order (Order No=." & qodHeader.Number & ").", Application.ProductName & ".ProcessSaleOrders")

            Next qodHeader
        Catch ex As Exception

            _output.Write("Failed processing " & forWhat & " sale orders.  " & ex.Message)
            Trace.WriteLine("Failed processing " & forWhat & " sale orders.  " & ex.Message, Application.ProductName & ".ProcessSaleOrders")

        Finally

            uxMemoEdit.EditValue = _output.GetLogString
            Cursor = Cursors.Default
            Trace.WriteLine("Sale Order Monitor: ProcessSaleOrders finished.", Application.ProductName & ".ProcessSaleOrders")

        End Try

    End Sub

    Private Sub ProcessRefunds()
        Dim qods As Qod.QodHeaderCollection
        Dim forWhat As String = IIf(uxRefundNewRadio.Checked, "new", "retry").ToString

        Try
            Cursor = Cursors.WaitCursor
            ResetControls()

            Trace.WriteLine("Getting " & forWhat & " refunds.", Application.ProductName & ".ProcessRefunds")
            qods = GetRefunds()

            If qods.IsEmpty Then
                _output.WriteNoQodsReturned()
                Trace.WriteLine("No orders returned", Application.ProductName & ".ProcessRefunds")
                Exit Sub
            Else
                Dim toOut As New StringBuilder

                With toOut
                    .AppendLine("Got " & forWhat & "orders to process...")
                    For Each qh As Qod.QodHeader In qods
                        .AppendLine(vbTab & "Order: No=" & qh.Number & ", OM No=" & qh.OmOrderNumber & ", WO No=" & qh.SourceOrderNumber & ", Delivery Status=" & qh.DeliveryStatus & ", Refund Status=" & qh.RefundStatus & ".")
                    Next
                    .AppendLine("End of " & forWhat & "orders to be processed list.")
                    _output.Write(.ToString)
                    Trace.WriteLine(.ToString, Application.ProductName & ".ProcessRefunds")
                End With
            End If

            For Each qodHeader As Qod.QodHeader In qods
                Trace.WriteLine("Sale Order Monitor: Progressing to Web Service for order (Order No=" & qodHeader.Number & ").", Application.ProductName & ".ProcessRefunds")

                Dim LinesUpdated As Integer = 0

                'Does the Refund Require Holding ?
                If qodHeader.RequiresRefundHolding Then
                    _output.WriteQodNeedsHolding()
                    qodHeader.Refunds.SetHolding()
                    qodHeader.RefundStatus = Qod.State.Refund.CreatedOnHold '105
                    qodHeader.SelfPersistTree()
                    Continue For
                End If

                'Are there any remote fulfil lines, The IBT Class should deliver this
                Dim IbtHeaderCollection As New Ibt.IBTHeaderCollection(qodHeader)

                Trace.WriteLine("Sale Order Monitor: Progressing to Web Service.", Application.ProductName & ".ProcessSaleOrders")

                Dim orderManagerService As BaseService = New RefundService
                Dim request As BaseServiceArgument = Nothing
                Dim response As BaseServiceArgument = Nothing

                Dim SaleLines As Qod.SaleLineCollection
                Dim DeliveryLine As Qod.SaleLine

                Try
                    Trace.WriteLine("Setting up 'create request'.", Application.ProductName & ".ProcessSaleOrders")
                    request = orderManagerService.GetRequest

                    'retrieve actual delivery refund value, held in dlline
                    Trace.WriteLine("Retrieve actual delivery refund value, held in dlline", Application.ProductName & ".ProcessRefunds")
                    For Each Refund As Qod.QodRefund In qodHeader.Refunds
                        If Refund.SkuNumber = "805111" Then
                            Trace.WriteLine("Found delivery refund", Application.ProductName & ".ProcessRefunds")
                            SaleLines = New Qod.SaleLineCollection
                            SaleLines.Load(Refund.RefundDate, Refund.RefundTransaction, Refund.RefundTill)
                            DeliveryLine = SaleLines.SaleLine(Refund.SkuNumber)
                            Refund.Price = DeliveryLine.SystemLookupPrice
                            Trace.WriteLine("Delivery refund value =" & Refund.Price.ToString(ChrW(163) & "999990.00"), Application.ProductName & ".ProcessRefunds")
                        End If
                    Next

                    request.SetFieldsFromQod(qodHeader)

                    Dim requestXml As String = request.Serialise

                    Trace.WriteLine("Set up 'create request'." & vbNewLine & requestXml, Application.ProductName & ".ProcessRefunds")


                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    'Author         : Partha Dutta
                    'Date           : 09/09/2011
                    'Change Request : CR0054-01
                    'TFS User Story : 2289
                    'TFS Task ID    : 2371
                    'Notes          : Validate outgoing requests to OM
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    'existing code
                    'Dim responseXml As String = orderManagerService.GetResponseXml(requestXml)

                    Dim responseXml As String
                    Dim OM As TpWickes.IOrderManager

                    OM = TpWickes.OrderManagerFactory.FactoryGet


                    'Send Request to Order Manager
                    _output.Write("Request (sent at " & Now.ToLocalTime & ") - " & requestXml)
                    Trace.WriteLine("Sending request at " & Now.ToLocalTime & ".", Application.ProductName & ".ProcessRefunds")


                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    'Author         : Partha Dutta
                    'Date           : 15/09/2011
                    'Change Request : CR0054-02
                    'TFS User Story : 2290
                    'TFS Task ID    : 2374
                    'Notes          : Log Sales Order Monitor invalid requests to Order Manager
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    'existing code
                    'responseXml = OM.OrderManagerRequest(orderManagerService, requestXml)

                    Dim DictionaryValue As New Dictionary(Of String, String)

                    DictionaryValue = TpWickes.OrderNumberCollection(qodHeader.SourceOrderNumber, qodHeader.Number, qodHeader.OmOrderNumber)

                    responseXml = OM.OrderManagerRequest(orderManagerService, requestXml, DictionaryValue)


                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


                    _output.Write("Response (receieved at " & Now.ToLocalTime & ") - " & responseXml)
                    Trace.WriteLine("Response from OM to request (receieved at " & Now.ToLocalTime & ")..." & vbNewLine & responseXml, Application.ProductName & ".ProcessRefunds")



                    If responseXml = "Failed Validation - Refund" Then

                        _output.Write("Sale Order Monitor: Refund failed XSD Validation")
                        Trace.WriteLine("Sale Order Monitor: Refund failed XSD Validation")

                        'existing code replicated from below

                        _output.WriteNothingReturnedFromOm()
                        Trace.WriteLine("Sale Order Monitor: Refund failed.  Setting order header (Order No=" & qodHeader.Number & ") refund status to " & Qod.State.Refund.RefundFailedConnection & ".", Application.ProductName & ".ProcessRefunds")
                        qodHeader.SetRefundStatus(Qod.State.Refund.RefundFailedConnection)

                        Trace.WriteLine("Set order header (Order No=" & qodHeader.Number & ") refund status to " & qodHeader.RefundStatus & ".", Application.ProductName & ".ProcessRefunds")
                        Trace.WriteLine("Setting order header (Order No=" & qodHeader.Number & ") refunds refund status to " & Qod.State.Refund.RefundFailedConnection & ".", Application.ProductName & ".ProcessRefunds")
                        qodHeader.Refunds.SetRefundStatus(Qod.State.Refund.RefundFailedConnection)



                        Trace.WriteLine("Set order header (Order No=" & qodHeader.Number & ") refunds refund minimum status is now " & qodHeader.Refunds.MinimumRefundStatus & ".", Application.ProductName & ".ProcessRefunds")
                        'Ref: 797 We can't save a Refund IBT Out if we have had a error from Order Manager
                        Trace.WriteLine("Removing Ibt collection.", Application.ProductName & ".ProcessRefunds")
                        If Not IbtHeaderCollection Is Nothing Then
                            IbtHeaderCollection = Nothing
                        End If
                        Trace.WriteLine("Removed Ibt collection.", Application.ProductName & ".ProcessRefunds")

                        Exit Try

                    End If

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''



                    If responseXml Is Nothing OrElse responseXml.Length = 0 Then 'If failed response

                        'this code has been replicated above in the "failed validation - refund" process
                        'not sure if other error conditions exist that could return an empty or blank string

                        _output.WriteNothingReturnedFromOm()
                        Trace.WriteLine("Sale Order Monitor: Refund failed.  Setting order header (Order No=" & qodHeader.Number & ") refund status to " & Qod.State.Refund.RefundFailedConnection & ".", Application.ProductName & ".ProcessRefunds")
                        qodHeader.SetRefundStatus(Qod.State.Refund.RefundFailedConnection)
                        Trace.WriteLine("Set order header (Order No=" & qodHeader.Number & ") refund status to " & qodHeader.RefundStatus & ".", Application.ProductName & ".ProcessRefunds")
                        Trace.WriteLine("Setting order header (Order No=" & qodHeader.Number & ") refunds refund status to " & Qod.State.Refund.RefundFailedConnection & ".", Application.ProductName & ".ProcessRefunds")
                        'qodHeader.Lines.SetDeliveryStatus(Qod.State.Refund.RefundFailedConnection)
                        qodHeader.Refunds.SetRefundStatus(Qod.State.Refund.RefundFailedConnection)
                        Trace.WriteLine("Set order header (Order No=" & qodHeader.Number & ") refunds refund minimum status is now " & qodHeader.Refunds.MinimumRefundStatus & ".", Application.ProductName & ".ProcessRefunds")
                        'Ref: 797 We can't save a Refund IBT Out if we have had a error from Order Manager
                        Trace.WriteLine("Removing Ibt collection.", Application.ProductName & ".ProcessRefunds")
                        If Not IbtHeaderCollection Is Nothing Then
                            IbtHeaderCollection = Nothing
                        End If
                        Trace.WriteLine("Removed Ibt collection.", Application.ProductName & ".ProcessRefunds")

                    Else

                        'We had a response from Order Manager
                        Trace.WriteLine("Sale Order Monitor: Processing response from OM to refund (Order No=" & qodHeader.Number & ") .", Application.ProductName & ".ProcessRefunds")
                        response = orderManagerService.GetResponse
                        response = response.Deserialise(responseXml)
                        response.SetFieldsInQod(qodHeader)

                        'Ref 835 Removed the need to do this as we now check to see if we had a previous IBT record So its Ok to update the IBT first time round when at refund status 100
                        'Ref: 809 MO'C 11/04/2011 - If Order manager sent back a failed status for bad data we need to blank out any IBT Outs.
                        'If qodHeader.RefundStatus = Qod.State.Refund.NotifyFailedData Then
                        '    If Not IbtHeaderCollection Is Nothing Then
                        '        IbtHeaderCollection = Nothing
                        '    End If
                        'End If
                        Trace.WriteLine("Sale Order Monitor: Processed response from OM to refund (Order No=" & qodHeader.Number & ") .", Application.ProductName & ".ProcessRefunds")

                    End If

                Catch ex As TimeoutException

                    Trace.WriteLine("Timed out processing " & forWhat & " refunds.  " & ex.Message, Application.ProductName & ".ProcessRefunds")
                    _output.Write(ex.ToString)
                    Trace.WriteLine("Setting order header (Order No=" & qodHeader.Number & ") refund status to " & Qod.State.Refund.RefundFailedConnection & ".", Application.ProductName & ".ProcessRefunds")
                    qodHeader.SetRefundStatus(Qod.State.Refund.RefundFailedConnection)
                    Trace.WriteLine("Set order header (Order No=" & qodHeader.Number & ") refund status to " & qodHeader.RefundStatus & ".", Application.ProductName & ".ProcessRefunds")
                    Trace.WriteLine("Setting order header (Order No=" & qodHeader.Number & ") refunds refund status to " & Qod.State.Refund.RefundFailedConnection & ".", Application.ProductName & ".ProcessRefunds")
                    qodHeader.Refunds.SetRefundStatus(Qod.State.Refund.RefundFailedConnection)
                    Trace.WriteLine("Set order header (Order No=" & qodHeader.Number & ") refunds refund minimum status is now " & qodHeader.Refunds.MinimumRefundStatus & ".", Application.ProductName & ".ProcessRefunds")
                    'Ref: 797 We can't save a Refund IBT Out if we have had a error from Order Manager
                    Trace.WriteLine("Removing Ibt collection.", Application.ProductName & ".ProcessRefunds")
                    If Not IbtHeaderCollection Is Nothing Then
                        IbtHeaderCollection = Nothing
                    End If
                    Trace.WriteLine("Removed Ibt collection.", Application.ProductName & ".ProcessRefunds")

                Catch ex As Exception

                    Trace.WriteLine("Failed processing " & forWhat & " refunds.  " & ex.Message, Application.ProductName & ".ProcessRefunds")
                    _output.Write(ex.ToString)
                    Trace.WriteLine("Setting order header (Order No=" & qodHeader.Number & ") refund status to " & Qod.State.Refund.RefundFailedState & ".", Application.ProductName & ".ProcessRefunds")
                    qodHeader.SetRefundStatus(Qod.State.Refund.RefundFailedState)
                    Trace.WriteLine("Set order header (Order No=" & qodHeader.Number & ") refund status to " & qodHeader.RefundStatus & ".", Application.ProductName & ".ProcessRefunds")
                    Trace.WriteLine("Setting order header (Order No=" & qodHeader.Number & ") refunds refund status to " & Qod.State.Refund.RefundFailedState & ".", Application.ProductName & ".ProcessRefunds")
                    qodHeader.Refunds.SetRefundStatus(Qod.State.Refund.RefundFailedState)
                    Trace.WriteLine("Set order header (Order No=" & qodHeader.Number & ") refunds refund minimum status is now " & qodHeader.Refunds.MinimumRefundStatus & ".", Application.ProductName & ".ProcessRefunds")
                    'Ref: 797 We can't save a Refund IBT Out if we have had a error from Order Manager
                    Trace.WriteLine("Removing Ibt collection.", Application.ProductName & ".ProcessRefunds")
                    If Not IbtHeaderCollection Is Nothing Then
                        IbtHeaderCollection = Nothing
                    End If
                    Trace.WriteLine("Removed Ibt collection.", Application.ProductName & ".ProcessRefunds")

                Finally

                    Using con As New Connection
                        Try
                            Trace.WriteLine("Committing " & forWhat & " refund (Order No=" & qodHeader.Number & ") at " & Now.ToLocalTime & ".", Application.ProductName & ".ProcessRefunds")
                            Trace.WriteLine("Starting transaction.", Application.ProductName & ".ProcessRefunds")
                            con.StartTransaction()
                            Dim LinesIbt As Integer = 0
                            If IbtHeaderCollection IsNot Nothing Then
                                Trace.WriteLine("Saving Ibt collection.", Application.ProductName & ".ProcessRefunds")
                                For Each IbtHeader As Ibt.IBTHeader In IbtHeaderCollection
                                    LinesIbt += IbtHeader.Persist(con)
                                    If IbtHeader.Number <> "" Then
                                        For Each refund As Qod.QodRefund In qodHeader.Refunds
                                            For Each line As Ibt.IBTLine In IbtHeader.Lines
                                                If line.SKUNumber = refund.SkuNumber Then
                                                    refund.SellingStoreIbtOut = IbtHeader.Number
                                                End If
                                            Next
                                        Next
                                    End If
                                Next
                                Trace.WriteLine("Saved Ibt collection (" & LinesUpdated & " total lines updated).", Application.ProductName & ".ProcessRefunds")
                            End If
                            Trace.WriteLine("Saving refund.", Application.ProductName & ".ProcessRefunds")
                            LinesUpdated = qodHeader.PersistTree(con)
                            Trace.WriteLine("Saved refund (" & LinesUpdated & " total lines updated).", Application.ProductName & ".ProcessRefunds")
                            LinesUpdated += LinesIbt
                            Trace.WriteLine("Committing transaction.", Application.ProductName & ".ProcessRefunds")
                            con.CommitTransaction()
                            Trace.WriteLine("Committed transaction at " & Now.ToLocalTime & ".", Application.ProductName & ".ProcessRefunds")
                        Catch ex As Exception
                            Trace.WriteLine("Failed committing " & forWhat & " refund.  " & ex.Message, Application.ProductName & ".ProcessRefunds")
                            Trace.WriteLine("Rolling back refund.")
                            con.RollbackTransaction()
                            Trace.WriteLine("Rolled back refund.")
                            _output.Write(ex.ToString)
                            Throw ex
                        End Try
                    End Using

                End Try
                Trace.WriteLine("Sale Order Monitor: Finished processing refund (Order No=." & qodHeader.Number & ").", Application.ProductName & ".ProcessRefunds")
                Trace.WriteLine("Updated " & LinesUpdated.ToString & " lines.", Application.ProductName)

            Next qodHeader
        Catch ex As Exception

            _output.Write("Failed processing " & forWhat & " refunds.  " & ex.Message)
            Trace.WriteLine("Failed processing " & forWhat & " refunds.  " & ex.Message, Application.ProductName & ".ProcessRefunds")

        Finally

            uxMemoEdit.EditValue = _output.GetLogString
            Cursor = Cursors.Default
            Trace.WriteLine("Sale Order Monitor: ProcessRefunds finished.", Application.ProductName & ".ProcessRefunds")

        End Try

    End Sub

    Private Function GetOrders() As Qod.QodHeaderCollection
        _output.Write("Retrieving orders")
        Select Case True
            Case uxOrderNewRadio.Checked
                Trace.WriteLine("Retrieving new orders")
                Return Qod.GetAllNewOrders
            Case uxOrderRetryRadio.Checked
                Trace.WriteLine("Retrieving retry orders")
                Return Qod.GetAllRetryOrders
            Case uxRefundNewRadio.Checked
                Trace.WriteLine("Retrieving new refunds")
                Return Qod.GetAllNewRefundOrders
            Case uxRefundRetryRadio.Checked
                Trace.WriteLine("Retrieving retry refunds")
                Return Qod.GetAllRetryRefundOrders
            Case Else : Return New Qod.QodHeaderCollection
        End Select
    End Function

    ''' <summary>
    ''' This routine will retreive all orders in the COR??? tables at status 100/500/700/800 and 900
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetSaleOrders() As Qod.QodHeaderCollection
        _output.Write("Retrieving orders")
        Select Case True
            Case uxOrderNewRadio.Checked
                Trace.WriteLine("Retrieving new orders")
                Return Qod.GetAllNewOrders
            Case uxOrderRetryRadio.Checked
                Trace.WriteLine("Retrieving retry orders")
                Return Qod.GetAllRetryOrders
            Case Else
                Trace.WriteLine("Retrieving NO orders")
                Return New Qod.QodHeaderCollection
        End Select
    End Function

    Private Function GetRefunds() As Qod.QodHeaderCollection
        _output.Write("Retrieving refunds")
        Select Case True
            Case uxRefundNewRadio.Checked
                Trace.WriteLine("Retrieving new refunds")
                Return Qod.GetAllNewRefundOrders
            Case uxRefundRetryRadio.Checked
                Trace.WriteLine("Retrieving retry refunds")
                Return Qod.GetAllRetryRefundOrders
            Case Else
                Trace.WriteLine("Retrieving NO orders")
                Return New Qod.QodHeaderCollection
        End Select
    End Function

    Private Function GetOrderManagerService(ByVal qod As Qod.QodHeader) As BaseService
        If InRefundMode() Then
            Return New RefundService
        Else
            If qod.RequiresOrderManagerCreation Then
                Return New CreateService
            Else
                Return New UpdateService
            End If
        End If
    End Function

    Private Function InRefundMode() As Boolean
        Return (uxRefundNewRadio.Checked OrElse uxRefundRetryRadio.Checked)
    End Function

    Private Sub ResetControls()
        uxMemoEdit.EditValue = Nothing
    End Sub

    Private Sub uxSendButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxSendButton.Click

        If uxOrderNewRadio.Checked OrElse uxOrderRetryRadio.Checked Then
            ProcessSalesOrders()
        Else
            ProcessRefunds()
        End If

    End Sub

    Private Sub uxResetButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxResetButton.Click
        ResetControls()
    End Sub

    ' Referral 802
    ' Remove the Duplicate order check now that only 1 instance of SOMonitor will run at any one time
    ' ...function was here
    ' End of referral 802

    ' Referral 802
    ' Further changes to get the check for different existing instance of Sale Order Monitor process running.
    ' Added to some of the logging messages at the same time
    Private Function MonitorAlreadyRunning() As Boolean
        Dim debugProcName As String
        Dim IntialProcessCount As Integer
        Dim GotIntialProcessCount As Boolean

        debugProcName = Diagnostics.Process.GetCurrentProcess.ProcessName
        Trace.WriteLine("Looking for process " & debugProcName & " that is already running (that does not have my Id of " & Diagnostics.Process.GetCurrentProcess.Id & ".", Application.ProductName & ".MonitorAlreadyRunning")
        If Diagnostics.Process.GetProcessesByName(debugProcName).Length > 0 Then
            If Diagnostics.Process.GetProcessesByName(debugProcName).Length = 1 Then
                If Diagnostics.Process.GetProcessesByName(debugProcName)(0).Id = Diagnostics.Process.GetCurrentProcess.Id Then
                    Trace.WriteLine("Found myself as " & debugProcName & " process already runnning.", Application.ProductName & ".MonitorAlreadyRunning")
                Else
                    Trace.WriteLine("Found " & debugProcName & " process already runnning.  Id=" & Diagnostics.Process.GetProcessesByName(debugProcName)(0).Id & ".", Application.ProductName & ".MonitorAlreadyRunning")
                    MonitorAlreadyRunning = True
                End If
            Else
                If Not GotIntialProcessCount Then
                    IntialProcessCount = Diagnostics.Process.GetProcessesByName(debugProcName).Length
                    GotIntialProcessCount = True
                End If
                Trace.WriteLine("Found " & debugProcName & " processes already runnning.", Application.ProductName & ".MonitorAlreadyRunning")
                For Each nextProc As Process In Diagnostics.Process.GetProcessesByName(debugProcName)
                    Trace.WriteLine(debugProcName & " process found, Id=" & nextProc.Id & ".", Application.ProductName & ".MonitorAlreadyRunning")
                Next
                MonitorAlreadyRunning = True
            End If
        Else
            Trace.WriteLine("Not found process " & debugProcName & " that is already running.", Application.ProductName & ".MonitorAlreadyRunning")
        End If
    End Function
    ' End of Referral 802

End Class

Enum OrderStates
    [New] = 0
    Retry = 1
    RefundNew = 2
    RefundRetry = 3
End Enum

Friend Class Output
    Implements IDisposable

    Private _log As StreamWriter = Nothing
    Private _logString As New StringBuilder

    Public header As String = "Monitor"

    Public Sub New()
        SetLogPath()
    End Sub

    Private Sub SetLogPath()
        Dim logpath As String
        Dim strFileName As String
        Dim strTime As String
        Const intMaxFileLoops As Integer = 5

        logpath = My.Settings.logPath
        If Not Directory.Exists(logpath) Then Directory.CreateDirectory(logpath)
        If Not logpath.EndsWith("\") Then logpath = logpath + "\"

        For intIndex As Integer = 1 To intMaxFileLoops Step 1
            Try
                'log name - year, month, day, hour, minute, second, millisecond (padded left to 3 digits)
                strTime = Now.ToString("yyyyMMddHHmmss") & Now.ToString("FFF").PadLeft(3, CType("0", Char))
                strFileName = logpath & "SaleOrderMonitor-" & strTime & ".txt"

                If Not System.IO.File.Exists(strFileName) Then
                    _log = New StreamWriter(strFileName, True)

                    Trace.WriteLine("Log file : " & strFileName, Application.ProductName)

                    Exit For
                Else
                    Trace.WriteLine("Log file : " & strFileName & " already exists", Application.ProductName)
                    System.Threading.Thread.Sleep(CType(Math.Floor(New Random().NextDouble * 500), Integer))
                End If

            Catch FileInUse As IOException
                System.Threading.Thread.Sleep(CType(Math.Floor(New Random().NextDouble * 500), Integer))
                Continue For
            Catch ex As Exception
                Throw
            End Try
        Next
    End Sub

    Public Sub WriteMonitorNotSwitchedOn()
        Write("Order monitor not switched on via parameter 3000")
    End Sub

    Public Sub WriteNoQodsReturned()
        Write("No orders returned")
    End Sub

    Public Sub WriteQodSuspended()
        Write("Order is suspended")
    End Sub

    Public Sub WriteQodHeaderStatusNotSynched()
        Write("Header status is not equal to minimum line status")
    End Sub

    Public Sub WriteQodNeedsHolding()
        Write("Order requires holding")
    End Sub

    Public Sub WriteIbtOutCreated(ByVal ibtNumber As String)
        Write("IBT out created number " & ibtNumber)
    End Sub

    Public Sub WriteNothingReturnedFromOm()
        Write("Nothing returned from order manager")
    End Sub

    Public Sub Write()
        If _log IsNot Nothing Then
            _logString.Append(Environment.NewLine)
            _log.WriteLine(Environment.NewLine)
        End If
    End Sub

    Public Sub Write(ByVal message As String)
        Dim sb As New StringBuilder()
        If header IsNot Nothing Then sb.Append(header & ": ")
        If message IsNot Nothing Then sb.Append(message.Trim)

        If _log IsNot Nothing Then
            _logString.Append(sb.ToString)
            _log.WriteLine(sb.ToString)
            Write()
        End If
    End Sub

    Public Function GetLogString() As String
        Return _logString.ToString
    End Function

#Region " IDisposable Support "

    Private disposedValue As Boolean = False        ' To detect redundant calls

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                If _log IsNot Nothing Then
                    _log.Flush()
                    _log.Close()
                    _log.Dispose()
                End If
            End If
        End If
        Me.disposedValue = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

End Class