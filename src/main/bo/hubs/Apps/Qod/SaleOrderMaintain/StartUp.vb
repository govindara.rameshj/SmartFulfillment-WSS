﻿Imports Cts.Oasys.Hubs.WinForm.Order.SaleOrders
Imports Cts.Oasys.WinForm
Imports WSS.BO.Common.Hosting
Imports WSS.BO.DataLayer.Model

<HideModuleName()> Module StartUp

    Sub Main()
        Const userId As Integer = 0
        Dim workstationId As Integer = CInt(My.Computer.Registry.GetValue(My.Settings.registry, "WorkstationID", 20))
        Const securityLevel As Integer = 9
        Dim runParameters As String = String.Empty

        If My.Application.CommandLineArgs IsNot Nothing Then
            For Each s As String In My.Application.CommandLineArgs
                runParameters &= Space(1) & s
            Next
        End If

        Dim resolver = New SimpleResolver()
        Dim dlFactory = resolver.Resolve(Of IDataLayerFactory)()

        Using app As New MainForm(userId, workstationId, securityLevel, runParameters, dlFactory)
            Using host As New HostForm(app)
                host.WindowState = FormWindowState.Maximized
                host.ShowDialog()
            End Using
        End Using

    End Sub

End Module
