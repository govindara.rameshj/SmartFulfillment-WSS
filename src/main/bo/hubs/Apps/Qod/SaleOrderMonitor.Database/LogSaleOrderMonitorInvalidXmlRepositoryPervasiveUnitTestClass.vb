﻿<TestClass()> Public Class LogSaleOrderMonitorInvalidXmlRepositoryPervasiveUnitTestClass

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test: Class"

#Region "Function: RequestWebServiceTypeConvertToString"

    <TestMethod()> Public Sub RepositoryPervasive_Function_RequestWebServiceTypeConvertToString_Parameter_CreateOrder_ReturnCorrectString()

        Dim Repository As New LogSaleOrderMonitorInvalidXmlRepositoryPervasive

        Assert.AreEqual("Create Order", Repository.RequestWebServiceTypeConvertToString(WebServiceType.CreateOrder))

    End Sub

    <TestMethod()> Public Sub RepositoryPervasive_Function_RequestWebServiceTypeConvertToString_Parameter_RefundOrder_ReturnCorrectString()

        Dim Repository As New LogSaleOrderMonitorInvalidXmlRepositoryPervasive

        Assert.AreEqual("Refund Order", Repository.RequestWebServiceTypeConvertToString(WebServiceType.RefundOrder))

    End Sub

    <TestMethod()> Public Sub RepositoryPervasive_Function_RequestWebServiceTypeConvertToString_Parameter_StatusUpdate_ReturnCorrectString()

        Dim Repository As New LogSaleOrderMonitorInvalidXmlRepositoryPervasive

        Assert.AreEqual("Status Update", Repository.RequestWebServiceTypeConvertToString(WebServiceType.StatusUpdate))

    End Sub

#End Region

#End Region

End Class