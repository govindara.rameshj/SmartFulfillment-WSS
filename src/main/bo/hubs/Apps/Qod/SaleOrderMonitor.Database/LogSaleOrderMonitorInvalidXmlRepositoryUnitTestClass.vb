﻿<TestClass()> Public Class LogSaleOrderMonitorInvalidXmlRepositoryUnitTestClass

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'PLEASE NOTE
    '        RUN TESTS INDIVIDUALLY 
    '        RUN TESTS IN ONE PASS RESULTS IN INTERMITENT FAILURES
    '        SEQUEL LAYER ONLY!!!
    '
    '        UNDER TIME PRESSURE : ISSSUE NOT RESOLVED TO DATE : 15/09/2011
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Private Shared _StoreOrderNumber As New List(Of Integer)

    Private TestXml As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                   <OMOrderCreateRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                                       <DateTimeStamp>2011-09-13T15:48:07</DateTimeStamp>
                                       <OrderHeader>
                                           <Source>WO</Source>
                                           <SourceOrderNumber>111114</SourceOrderNumber>
                                           <SellingStoreCode>8080</SellingStoreCode>
                                           <SellingStoreOrderNumber>17847</SellingStoreOrderNumber>
                                           <SellingStoreTill>01</SellingStoreTill>
                                           <SellingStoreTransaction>0021</SellingStoreTransaction>
                                           <RequiredDeliveryDate>2011-08-09</RequiredDeliveryDate>
                                           <DeliveryCharge>0.00</DeliveryCharge>
                                           <TotalOrderValue>192.97</TotalOrderValue>
                                           <SaleDate>2011-09-13</SaleDate>
                                           <CustomerAccountNo>69481</CustomerAccountNo>
                                           <CustomerName>The Doomed</CustomerName>
                                           <CustomerAddressLine1>102 Inkerman Road</CustomerAddressLine1>
                                           <CustomerAddressLine2>Knaphill</CustomerAddressLine2>
                                           <CustomerAddressTown>Woking</CustomerAddressTown>
                                           <CustomerAddressLine4>Surrey</CustomerAddressLine4>
                                           <CustomerPostcode>GU21 2WB</CustomerPostcode>
                                           <DeliveryAddressLine1>102 Inkerman Road</DeliveryAddressLine1>
                                           <DeliveryAddressLine2>Knaphill</DeliveryAddressLine2>
                                           <DeliveryAddressTown>Woking</DeliveryAddressTown>
                                           <DeliveryAddressLine4>Surrey</DeliveryAddressLine4>
                                           <DeliveryPostcode>GU21 2WB</DeliveryPostcode>
                                           <ContactPhoneHome>01483 489801</ContactPhoneHome>
                                           <ContactPhoneMobile>07584 491292</ContactPhoneMobile>
                                           <ContactPhoneWork>01483 489801</ContactPhoneWork>
                                           <ContactEmail>kevan.madelin@wickes.co.uk</ContactEmail>
                                           <DeliveryInstructions>
                                               <InstructionLine>
                                                   <LineNo>1</LineNo>
                                                   <LineText>Deliver before riots</LineText>
                                               </InstructionLine>
                                           </DeliveryInstructions>
                                           <ToBeDelivered>true</ToBeDelivered>
                                           <ExtendedLeadTime>true</ExtendedLeadTime>
                                           <DeliveryContactName>The Forky Driver</DeliveryContactName>
                                           <DeliveryContactPhone>01483 489801</DeliveryContactPhone>
                                       </OrderHeader>
                                       <OrderLines>
                                           <OrderLine>
                                               <SourceOrderLineNo>1</SourceOrderLineNo>
                                               <SellingStoreLineNo>0001</SellingStoreLineNo>
                                               <ProductCode>190234</ProductCode>
                                               <ProductDescription>Nitrile Safety Wellington 10 Black</ProductDescription>
                                               <TotalOrderQuantity>2</TotalOrderQuantity>
                                               <QuantityTaken>0</QuantityTaken>
                                               <UOM>EACH</UOM>
                                               <LineValue>29.98</LineValue>
                                               <DeliveryChargeItem>false</DeliveryChargeItem>
                                               <SellingPrice>14.99</SellingPrice>
                                           </OrderLine>
                                           <OrderLine>
                                               <SourceOrderLineNo>2</SourceOrderLineNo>
                                               <SellingStoreLineNo>0002</SellingStoreLineNo>
                                               <ProductCode>189571</ProductCode>
                                               <ProductDescription>Chopin Arch</ProductDescription>
                                               <TotalOrderQuantity>1</TotalOrderQuantity>
                                               <QuantityTaken>0</QuantityTaken>
                                               <UOM>EACH</UOM>
                                               <LineValue>162.99</LineValue>
                                               <DeliveryChargeItem>false</DeliveryChargeItem>
                                               <SellingPrice>162.99</SellingPrice>
                                           </OrderLine>
                                       </OrderLines>
                                   </OMOrderCreateRequest>

#Region "Stored Procedure Unit Test: LogSOM_InvalidXmlInsert"

#Region "Create Order"

    <TestMethod()> Public Sub InvalidXmlInsert_Parameters_CreateOrder_NullOrderManagerNo_NullVendaOrderNo_NullFailedXml_ReturnOneRecord()

        Dim CurrentDate As Date
        Dim MaxStoreOrderNumber As Integer
        Dim SelectCount As Integer
        Dim Identity As Integer

        'arrange
        CurrentDate = Now
        'MaxStoreOrderNumber = NextStoreOrderNumber() + 1
        MaxStoreOrderNumber = GetNextStoreOrderNumber()

        'act
        Identity = InvalidXmlInsert(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.CreateOrder, Nothing, Nothing)

        'assert
        SelectCount = InvalidXmlSelect(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.CreateOrder, Nothing, Nothing)
        Assert.IsTrue(SelectCount = 1)

        'clean
        InvalidXmlDelete(Identity)

    End Sub

    <TestMethod()> Public Sub InvalidXmlInsert_Parameters_CreateOrder_NotNullOrderManagerNo_NullVendaOrderNo_NullFailedXml_ReturnOneRecord()

        Dim CurrentDate As Date
        Dim MaxStoreOrderNumber As Integer
        Dim SelectCount As Integer
        Dim Identity As Integer

        'arrange
        CurrentDate = Now
        'MaxStoreOrderNumber = NextStoreOrderNumber() + 1
        MaxStoreOrderNumber = GetNextStoreOrderNumber()

        'act
        Identity = InvalidXmlInsert(MaxStoreOrderNumber.ToString, 123456, CurrentDate, WebServiceType.CreateOrder, Nothing, Nothing)

        'assert
        SelectCount = InvalidXmlSelect(MaxStoreOrderNumber.ToString, 123456, CurrentDate, WebServiceType.CreateOrder, Nothing, Nothing)
        Assert.IsTrue(SelectCount = 1)

        'clean
        InvalidXmlDelete(Identity)

    End Sub

    <TestMethod()> Public Sub InvalidXmlInsert_Parameters_CreateOrder_NullOrderManagerNo_NotNullVendaOrderNo_NullFailedXml_ReturnOneRecord()

        Dim CurrentDate As Date
        Dim MaxStoreOrderNumber As Integer
        Dim SelectCount As Integer
        Dim Identity As Integer

        'arrange
        CurrentDate = Now
        'MaxStoreOrderNumber = NextStoreOrderNumber() + 1
        MaxStoreOrderNumber = GetNextStoreOrderNumber()

        'act
        Identity = InvalidXmlInsert(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.CreateOrder, "123456", Nothing)

        'assert
        SelectCount = InvalidXmlSelect(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.CreateOrder, "123456", Nothing)
        Assert.IsTrue(SelectCount = 1)

        'clean
        InvalidXmlDelete(Identity)

    End Sub

    <TestMethod()> Public Sub InvalidXmlInsert_Parameters_CreateOrder_NullOrderManagerNo_NullVendaOrderNo_NotNullFailedXml_ReturnOneRecord()

        Dim CurrentDate As Date
        Dim MaxStoreOrderNumber As Integer
        Dim SelectCount As Integer
        Dim Identity As Integer

        'arrange
        CurrentDate = Now
        'MaxStoreOrderNumber = NextStoreOrderNumber() + 1
        MaxStoreOrderNumber = GetNextStoreOrderNumber()

        'act
        Identity = InvalidXmlInsert(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.CreateOrder, "123456", TestXml)

        'assert
        SelectCount = InvalidXmlSelect(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.CreateOrder, "123456", TestXml)
        Assert.IsTrue(SelectCount = 1)

        'clean
        InvalidXmlDelete(Identity)

    End Sub

#End Region

#Region "Refund Order"

    <TestMethod()> Public Sub InvalidXmlInsert_Parameters_RefundOrder_NullOrderManagerNo_NullVendaOrderNo_NullFailedXml_ReturnOneRecord()

        Dim CurrentDate As Date
        Dim MaxStoreOrderNumber As Integer
        Dim SelectCount As Integer
        Dim Identity As Integer

        'arrange
        CurrentDate = Now
        'MaxStoreOrderNumber = NextStoreOrderNumber() + 1
        MaxStoreOrderNumber = GetNextStoreOrderNumber()

        'act
        Identity = InvalidXmlInsert(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.RefundOrder, Nothing, Nothing)

        'assert
        SelectCount = InvalidXmlSelect(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.RefundOrder, Nothing, Nothing)
        Assert.IsTrue(SelectCount = 1)

        'clean
        InvalidXmlDelete(Identity)

    End Sub

    <TestMethod()> Public Sub InvalidXmlInsert_Parameters_RefundOrder_NotNullOrderManagerNo_NullVendaOrderNo_NullFailedXml_ReturnOneRecord()

        Dim CurrentDate As Date
        Dim MaxStoreOrderNumber As Integer
        Dim SelectCount As Integer
        Dim Identity As Integer

        'arrange
        CurrentDate = Now
        'MaxStoreOrderNumber = NextStoreOrderNumber() + 1
        MaxStoreOrderNumber = GetNextStoreOrderNumber()

        'act
        Identity = InvalidXmlInsert(MaxStoreOrderNumber.ToString, 123456, CurrentDate, WebServiceType.RefundOrder, Nothing, Nothing)

        'assert
        SelectCount = InvalidXmlSelect(MaxStoreOrderNumber.ToString, 123456, CurrentDate, WebServiceType.RefundOrder, Nothing, Nothing)
        Assert.IsTrue(SelectCount = 1)

        'clean
        InvalidXmlDelete(Identity)

    End Sub

    <TestMethod()> Public Sub InvalidXmlInsert_Parameters_RefundOrder_NullOrderManagerNo_NotNullVendaOrderNo_NullFailedXml_ReturnOneRecord()

        Dim CurrentDate As Date
        Dim MaxStoreOrderNumber As Integer
        Dim SelectCount As Integer
        Dim Identity As Integer

        'arrange
        CurrentDate = Now
        'MaxStoreOrderNumber = NextStoreOrderNumber() + 1
        MaxStoreOrderNumber = GetNextStoreOrderNumber()

        'act
        Identity = InvalidXmlInsert(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.RefundOrder, "123456", Nothing)

        'assert
        SelectCount = InvalidXmlSelect(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.RefundOrder, "123456", Nothing)
        Assert.IsTrue(SelectCount = 1)

        'clean
        InvalidXmlDelete(Identity)

    End Sub

    <TestMethod()> Public Sub InvalidXmlInsert_Parameters_RefundOrder_NullOrderManagerNo_NullVendaOrderNo_NotNullFailedXml_ReturnOneRecord()

        Dim CurrentDate As Date
        Dim MaxStoreOrderNumber As Integer
        Dim SelectCount As Integer
        Dim Identity As Integer

        'arrange
        CurrentDate = Now
        'MaxStoreOrderNumber = NextStoreOrderNumber() + 1
        MaxStoreOrderNumber = GetNextStoreOrderNumber()

        'act
        Identity = InvalidXmlInsert(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.RefundOrder, "123456", TestXml)

        'assert
        SelectCount = InvalidXmlSelect(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.RefundOrder, "123456", TestXml)
        Assert.IsTrue(SelectCount = 1)

        'clean
        InvalidXmlDelete(Identity)


    End Sub

#End Region

#Region "Status Update"

    <TestMethod()> Public Sub InvalidXmlInsert_Parameters_StatusUpdate_NullOrderManagerNo_NullVendaOrderNo_NullFailedXml_ReturnOneRecord()

        Dim CurrentDate As Date
        Dim MaxStoreOrderNumber As Integer
        Dim SelectCount As Integer
        Dim Identity As Integer

        'arrange
        CurrentDate = Now
        'MaxStoreOrderNumber = NextStoreOrderNumber() + 1
        MaxStoreOrderNumber = GetNextStoreOrderNumber()

        'act
        Identity = InvalidXmlInsert(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.StatusUpdate, Nothing, Nothing)

        'assert
        SelectCount = InvalidXmlSelect(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.StatusUpdate, Nothing, Nothing)
        Assert.IsTrue(SelectCount = 1)

        'clean
        InvalidXmlDelete(Identity)

    End Sub

    <TestMethod()> Public Sub InvalidXmlInsert_Parameters_StatusUpdate_NotNullOrderManagerNo_NullVendaOrderNo_NullFailedXml_ReturnOneRecord()

        Dim CurrentDate As Date
        Dim MaxStoreOrderNumber As Integer
        Dim SelectCount As Integer
        Dim Identity As Integer

        'arrange
        CurrentDate = Now
        'MaxStoreOrderNumber = NextStoreOrderNumber() + 1
        MaxStoreOrderNumber = GetNextStoreOrderNumber()

        'act
        Identity = InvalidXmlInsert(MaxStoreOrderNumber.ToString, 123456, CurrentDate, WebServiceType.StatusUpdate, Nothing, Nothing)

        'assert
        SelectCount = InvalidXmlSelect(MaxStoreOrderNumber.ToString, 123456, CurrentDate, WebServiceType.StatusUpdate, Nothing, Nothing)
        Assert.IsTrue(SelectCount = 1)

        'clean
        InvalidXmlDelete(Identity)

    End Sub

    <TestMethod()> Public Sub InvalidXmlInsert_Parameters_StatusUpdate_NullOrderManagerNo_NotNullVendaOrderNo_NullFailedXml_ReturnOneRecord()

        Dim CurrentDate As Date
        Dim MaxStoreOrderNumber As Integer
        Dim SelectCount As Integer
        Dim Identity As Integer

        'arrange
        CurrentDate = Now
        'MaxStoreOrderNumber = NextStoreOrderNumber() + 1
        MaxStoreOrderNumber = GetNextStoreOrderNumber()

        'act
        Identity = InvalidXmlInsert(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.StatusUpdate, "123456", Nothing)

        'assert
        SelectCount = InvalidXmlSelect(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.StatusUpdate, "123456", Nothing)
        Assert.IsTrue(SelectCount = 1)

        'clean
        InvalidXmlDelete(Identity)

    End Sub

    <TestMethod()> Public Sub InvalidXmlInsert_Parameters_StatusUpdate_NullOrderManagerNo_NullVendaOrderNo_NotNullFailedXml_ReturnOneRecord()

        Dim CurrentDate As Date
        Dim MaxStoreOrderNumber As Integer
        Dim SelectCount As Integer
        Dim Identity As Integer

        'arrange
        CurrentDate = Now
        'MaxStoreOrderNumber = NextStoreOrderNumber() + 1
        MaxStoreOrderNumber = GetNextStoreOrderNumber()

        'act
        Identity = InvalidXmlInsert(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.StatusUpdate, "123456", TestXml)

        'assert
        SelectCount = InvalidXmlSelect(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.StatusUpdate, "123456", TestXml)
        Assert.IsTrue(SelectCount = 1)

        'clean
        InvalidXmlDelete(Identity)

    End Sub

#End Region

#End Region

#Region "Stored Procedure Unit Test: LogSOM_InvalidXmlDetailInsert"

#Region "Create Order"

    <TestMethod()> Public Sub InvalidXmlDetailInsert_Parameters_CreateOrder_NullOrderManagerNo_NullVendaOrderNo_NullFailedXml_Single_FailureReason_ReturnOneRecord()

        Dim CurrentDate As Date
        Dim MaxStoreOrderNumber As Integer
        Dim FailureReason As New List(Of String)
        Dim Identity As Integer
        Dim SelectCount As Integer

        'arrange
        CurrentDate = Now
        'MaxStoreOrderNumber = NextStoreOrderNumber() + 1
        MaxStoreOrderNumber = GetNextStoreOrderNumber()
        FailureReason.Add("Failure reason 1")

        'act
        Identity = InvalidXmlInsert(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.CreateOrder, Nothing, Nothing)
        InvalidXmlDetailInsert(Identity, FailureReason)

        'assert
        SelectCount = InvalidXmlDetailSelect(Identity)
        Assert.IsTrue(SelectCount = 1)

        'clean
        InvalidXmlDetailDelete(Identity)
        InvalidXmlDelete(Identity)

    End Sub

    '<TestMethod()> Public Sub SP_InvalidXmlDetailInsert_Parameters_CreateOrder_NotNullOrderManagerNo_NullVendaOrderNo_NullFailedXml_Single_FailureReason_ReturnOneRecord()

    '    Assert.Inconclusive()

    'End Sub

    '<TestMethod()> Public Sub SP_InvalidXmlDetailInsert_Parameters_CreateOrder_NullOrderManagerNo_NotNullVendaOrderNo_NullFailedXml_Single_FailureReason_ReturnOneRecord()

    '    Assert.Inconclusive()

    'End Sub

    '<TestMethod()> Public Sub SP_InvalidXmlDetailInsert_Parameters_CreateOrder_NullOrderManagerNo_NullVendaOrderNo_NotNullFailedXml_Single_FailureReason_ReturnOneRecord()

    '    Assert.Inconclusive()

    'End Sub

    <TestMethod()> Public Sub InvalidXmlDetailInsert_Parameters_CreateOrder_NullOrderManagerNo_NullVendaOrderNo_NullFailedXml_Multiple_FailureReason_ReturnThreeRecord()

        Dim CurrentDate As Date
        Dim MaxStoreOrderNumber As Integer
        Dim FailureReason As New List(Of String)
        Dim Identity As Integer
        Dim SelectCount As Integer

        'arrange
        CurrentDate = Now
        'MaxStoreOrderNumber = NextStoreOrderNumber() + 1
        MaxStoreOrderNumber = GetNextStoreOrderNumber()
        FailureReason.Add("Failure reason 1")
        FailureReason.Add("Failure reason 2")
        FailureReason.Add("Failure reason 3")

        'act
        Identity = InvalidXmlInsert(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.CreateOrder, Nothing, Nothing)
        InvalidXmlDetailInsert(Identity, FailureReason)

        'assert
        SelectCount = InvalidXmlDetailSelect(Identity)
        Assert.IsTrue(SelectCount = 3)

        'clean
        InvalidXmlDetailDelete(Identity)
        InvalidXmlDelete(Identity)

    End Sub

    '<TestMethod()> Public Sub SP_InvalidXmlDetailInsert_Parameters_CreateOrder_NotNullOrderManagerNo_NullVendaOrderNo_NullFailedXml_Multiple_FailureReason_ReturnThreeRecord()

    '    Assert.Inconclusive()

    'End Sub

    '<TestMethod()> Public Sub SP_InvalidXmlDetailInsert_Parameters_CreateOrder_NullOrderManagerNo_NotNullVendaOrderNo_NullFailedXml_Multiple_FailureReason_ReturnThreeRecord()

    '    Assert.Inconclusive()

    'End Sub

    '<TestMethod()> Public Sub SP_InvalidXmlDetailInsert_Parameters_CreateOrder_NullOrderManagerNo_NullVendaOrderNo_NotNullFailedXml_Multiple_FailureReason_ReturnThreeRecord()

    '    Assert.Inconclusive()

    'End Sub

#End Region

#Region "Refund Order"

    'NOT CREATED: VARIATION OF ABOVE

#End Region

#Region "Status Update"

    'NOT CREATED: VARIATION OF ABOVE

#End Region

#End Region

#Region "Unit Test: Class"

#Region "Create Order"

    '<TestMethod()> Public Sub Repository_Function_Insert_Parameters_CreateOrder_NotNullOrderManagerNo_NullVendaOrderNo_NullFailedXml_Single_FailureReason_ReturnOneRecord()

    '    Assert.Inconclusive()

    'End Sub

    '<TestMethod()> Public Sub Repository_Function_Insert_Parameters_CreateOrder_NullOrderManagerNo_NotNullVendaOrderNo_NullFailedXml_Single_FailureReason_ReturnOneRecord()

    '    Assert.Inconclusive()

    'End Sub

    '<TestMethod()> Public Sub Repository_Function_Insert_Parameters_CreateOrder_NullOrderManagerNo_NullVendaOrderNo_NotNullFailedXml_Single_FailureReason_ReturnOneRecord()

    '    Assert.Inconclusive()

    'End Sub


    '<TestMethod()> Public Sub Repository_Function_Insert_Parameters_CreateOrder_NullOrderManagerNo_NullVendaOrderNo_NullFailedXml_Multiple_FailureReason_ReturnThreeRecord()

    '    Assert.Inconclusive()

    'End Sub

    '<TestMethod()> Public Sub Repository_Function_Insert_Parameters_CreateOrder_NotNullOrderManagerNo_NullVendaOrderNo_NullFailedXml_Multiple_FailureReason_ReturnThreeRecord()

    '    Assert.Inconclusive()

    'End Sub

    '<TestMethod()> Public Sub Repository_Function_Insert_Parameters_CreateOrder_NullOrderManagerNo_NotNullVendaOrderNo_NullFailedXml_Multiple_FailureReason_ReturnThreeRecord()

    '    Assert.Inconclusive()

    'End Sub

    '<TestMethod()> Public Sub Repository_Function_Insert_Parameters_CreateOrder_NullOrderManagerNo_NullVendaOrderNo_NotNullFailedXml_Multiple_FailureReason_ReturnThreeRecord()

    '    Assert.Inconclusive()

    'End Sub

#End Region

#Region "Refund Order"

    'NOT CREATED: VARIATION OF ABOVE

#End Region

#Region "Status Update"

    'NOT CREATED: VARIATION OF ABOVE

#End Region

#Region "Test Harness"

    <TestMethod()> Public Sub TestHarness_Parameters_CreateOrder_NullOrderManagerNo_NullVendaOrderNo_NullFailedXml_Single_FailureReason_ReturnOneRecord()

        Dim Repository As ILogSaleOrderMonitorInvalidXmlRepository
        Dim CurrentDate As Date
        Dim MaxStoreOrderNumber As Integer
        Dim FailureReason As New List(Of String)
        Dim Identity As System.Nullable(Of Integer)

        Dim Valid As Boolean

        'arrange 
        CurrentDate = Now
        MaxStoreOrderNumber = GetNextStoreOrderNumber()
        FailureReason.Add("Failure reason 1")
        Valid = True
        Repository = LogSaleOrderMonitorInvalidXmlRepositoryFactory.FactoryGet

        'act
        Identity = Repository.Insert(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.CreateOrder, FailureReason, Nothing, Nothing)

        'assert
        If Identity.HasValue = False Then Valid = False
        'check header
        If InvalidXmlSelect(MaxStoreOrderNumber.ToString, New System.Nullable(Of Integer), CurrentDate, WebServiceType.CreateOrder, Nothing, Nothing) <> 1 Then Valid = False
        'check detail
        If Identity.HasValue = True Then If InvalidXmlDetailSelect(Identity.Value) <> 1 Then Valid = False
        Assert.IsTrue(Valid)

        'clean
        If Identity.HasValue = True Then

            InvalidXmlDetailDelete(Identity.Value)
            InvalidXmlDelete(Identity.Value)

        End If

    End Sub

#End Region

#End Region

#Region "Private Procedures And Functions"

#Region "Database: Sequel / Pervasive Layer"

    'PLEASE NOTE: Pervasive Test
    '             Ideally would use the actual class LogSaleOrderMonitorInvalidXmlRepositoryPervasive
    '             but the Insert() function writes to both header and line
    '             Have taken a fragment of the code
    '
    '             Code has been duplicated

    Private Function InvalidXmlInsert(ByVal StoreOrderNo As String, ByVal OrderManagerOrderNo As System.Nullable(Of Integer), _
                                      ByVal DateLogged As Date, ByVal RequestWebServiceType As WebServiceType, _
                                      ByVal VendaOrderNo As String, ByVal FailedXml As XDocument) As Integer

        Using con As New Connection

            Using com As New Command(con)

                Select Case Cts.Oasys.Hubs.Data.GetDataProvider

                    Case DataProvider.Odbc

                        Dim SB As New StringBuilder
                        Dim ID As Integer

                        ID = NextIDPervasiveOnly(con)

                        SB.AppendLine("insert into LogSOMBadXML                    (                         ")
                        SB.AppendLine("                                                 ID,                  ")
                        If VendaOrderNo IsNot Nothing Then SB.AppendLine("              VendaOrderNo,        ")
                        SB.AppendLine("                                                 StoreOrderNo,        ")
                        If OrderManagerOrderNo.HasValue = True Then SB.AppendLine("     OrderManagerOrderNo, ")
                        SB.AppendLine("                                                 DateLoggedDate,      ")
                        SB.AppendLine("                                                 DateLoggedTime,      ")
                        SB.AppendLine("                                                 RequestType          ")
                        SB.AppendLine("                                            )                         ")
                        SB.AppendLine("                                     values (                         ")
                        SB.AppendLine("                                                 ?,                   ")
                        If VendaOrderNo IsNot Nothing Then SB.AppendLine("              ?,                   ")
                        SB.AppendLine("                                                 ?,                   ")
                        If OrderManagerOrderNo.HasValue = True Then SB.AppendLine("     ?,                   ")
                        SB.AppendLine("                                                 ?,                   ")
                        SB.AppendLine("                                                 ?,                   ")
                        SB.AppendLine("                                                 ?                    ")
                        SB.AppendLine("                                            )                         ")

                        com.CommandText = SB.ToString

                        com.AddParameter("ID", ID)
                        If VendaOrderNo IsNot Nothing Then com.AddParameter("VendaOrderNo", VendaOrderNo)
                        com.AddParameter("StoreOrderNo", StoreOrderNo)
                        If OrderManagerOrderNo.HasValue = True Then com.AddParameter("OrderManagerOrderNo", OrderManagerOrderNo)
                        com.AddParameter("DateLoggedDate", DateLogged)
                        com.AddParameter("DateLoggedTime", DateLogged)
                        com.AddParameter("RequestType", RequestWebServiceTypeConvertToString(RequestWebServiceType))

                        com.ExecuteNonQuery()

                        Return ID

                    Case DataProvider.Sql

                        Dim IdentityParameter As SqlParameter

                        com.StoredProcedureName = "LogSOM_InvalidXmlInsert"

                        If VendaOrderNo IsNot Nothing Then com.AddParameter("@VendaOrderNo", VendaOrderNo, SqlDbType.NVarChar, 20)
                        com.AddParameter("@StoreOrderNo", StoreOrderNo, SqlDbType.NVarChar, 6)
                        If OrderManagerOrderNo.HasValue = True Then com.AddParameter("@OrderManagerOrderNo", OrderManagerOrderNo, SqlDbType.Int)
                        com.AddParameter("@DateLogged", DateLogged, SqlDbType.DateTime)
                        com.AddParameter("@RequestTypeID", RequestWebServiceType, SqlDbType.Int)
                        If FailedXml IsNot Nothing Then com.AddParameter("@FailedXML", FailedXml.ToString, SqlDbType.Xml)

                        IdentityParameter = com.AddOutputParameter("@IdentityValue", SqlDbType.Int, ParameterDirection.Output)

                        com.ExecuteNonQuery()

                        Return CType(IdentityParameter.Value, Integer)

                End Select

            End Using

        End Using

    End Function

    'PLEASE NOTE: Pervasive Test
    '             Ideally would use the actual class LogSaleOrderMonitorInvalidXmlRepositoryPervasive
    '             but the Insert() function writes to both header and line
    '             Have taken a fragment of the code
    '
    '             Code has been duplicated

    Private Sub InvalidXmlDetailInsert(ByVal LogID As Integer, ByVal FailureReason As List(Of String))

        Dim PervasiveLineNo As Integer

        Using con As New Connection

            PervasiveLineNo = 0

            For Each Failure As String In FailureReason

                Using com As New Command(con)

                    Select Case Cts.Oasys.Hubs.Data.GetDataProvider

                        Case DataProvider.Odbc

                            Dim SB As New StringBuilder

                            PervasiveLineNo += 1


                            SB.Append("insert into LogSOMBadXmlDetail (LogID, LineNo, FailureReason) values (?, ?, ?) ")

                            com.CommandText = SB.ToString

                            com.AddParameter("LogID", LogID)
                            com.AddParameter("LineNo", PervasiveLineNo)
                            com.AddParameter("FailureReason", Failure)

                            com.ExecuteNonQuery()

                        Case DataProvider.Sql

                            com.StoredProcedureName = "LogSOM_InvalidXmlDetailInsert"

                            com.AddParameter("@LogID", LogID, SqlDbType.Int)
                            com.AddParameter("@FailureReason", Failure, SqlDbType.NVarChar, 100)

                            com.ExecuteNonQuery()

                    End Select

                End Using

            Next

        End Using

    End Sub

    Private Function InvalidXmlSelect(ByVal StoreOrderNo As String, ByVal OrderManagerOrderNo As System.Nullable(Of Integer), _
                                      ByVal DateLogged As Date, ByVal RequestWebServiceType As WebServiceType, _
                                      ByVal VendaOrderNo As String, ByVal FailedXml As XDocument) As Integer

        'could use identity value but it would not check if correct values have been stored

        Using con As New Connection

            Using com As New Command(con)

                Dim SB As New StringBuilder

                With SB
                    .AppendLine("select *               ")
                    .AppendLine("from LogSOM_InvalidXml ")
                    .AppendLine("where                  ")
                    If VendaOrderNo Is Nothing Then
                        .AppendLine("       VendaOrderNo is null ")
                    Else
                        .AppendLine("       VendaOrderNo        = '" & VendaOrderNo & "' ")
                    End If
                    .AppendLine("and   ")
                    .AppendLine("           StoreOrderNo        = '" & StoreOrderNo & "' ")
                    .AppendLine("and   ")
                    If OrderManagerOrderNo.HasValue = False Then
                        .AppendLine("       OrderManagerOrderNo is null ")
                    Else
                        .AppendLine("       OrderManagerOrderNo = " & OrderManagerOrderNo.Value & " ")
                    End If
                    .AppendLine("and   ")
                    '.AppendLine("           DateLogged          = '" & DateLogged.ToString("yyyy-MM-dd HH:mm:ss:fff") & "' ")
                    .AppendLine("            datepart(year,   DateLogged) = " & DateLogged.Year.ToString & " ")
                    .AppendLine("and   ")
                    .AppendLine("            datepart(month,  DateLogged) = " & DateLogged.Month.ToString & " ")
                    .AppendLine("and   ")
                    .AppendLine("            datepart(day,    DateLogged) = " & DateLogged.Day.ToString & " ")
                    .AppendLine("and   ")
                    .AppendLine("            datepart(hour,   DateLogged) = " & DateLogged.Hour.ToString & " ")
                    .AppendLine("and   ")
                    .AppendLine("            datepart(minute, DateLogged) = " & DateLogged.Minute.ToString & " ")
                    .AppendLine("and   ")
                    .AppendLine("            datepart(second, DateLogged) = " & DateLogged.Second.ToString & " ")
                    .AppendLine("and   ")
                    .AppendLine("           RequestTypeID       = " & RequestWebServiceType & " ")


                    .AppendLine("and   ")
                    .AppendLine("           RequestTypeID       = " & RequestWebServiceType & " ")
                    .AppendLine("and   ")
                    If FailedXml Is Nothing Then
                        .AppendLine("       FailedXml is null ")
                    Else
                        .AppendLine("       FailedXml.exist('" & FailedXml.ToString & "') = 1 ")
                    End If
                End With

                com.CommandText = SB.ToString

                Dim dt As DataTable = com.ExecuteDataTable
                Return dt.Rows.Count

            End Using

        End Using

    End Function

    Private Function InvalidXmlDetailSelect(ByVal LogID As Integer) As Integer
        Using con As New Connection
            Using com As New Command(con)
                com.CommandText = "select * from LogSOM_InvalidXmlDetail where LogID = " & LogID.ToString
                Dim DT As DataTable = com.ExecuteDataTable
                Return DT.Rows.Count
            End Using
        End Using
    End Function

    Private Sub InvalidXmlDelete(ByVal ID As Integer)

        Using con As New Connection

            Using com As New Command(con)

                Select Case Cts.Oasys.Hubs.Data.GetDataProvider

                    Case DataProvider.Odbc

                        com.CommandText = "delete from LogSOMBadXML where ID = " & ID.ToString
                        com.ExecuteNonQuery()

                    Case DataProvider.Sql

                        com.CommandText = "delete LogSOM_InvalidXml where ID = " & ID.ToString
                        com.ExecuteNonQuery()

                End Select

            End Using

        End Using

    End Sub

    Private Sub InvalidXmlDetailDelete(ByVal LogID As Integer)

        Using con As New Connection

            Using com As New Command(con)

                Select Case Cts.Oasys.Hubs.Data.GetDataProvider

                    Case DataProvider.Odbc

                        com.CommandText = "delete from LogSOMBadXmlDetail where LogID = " & LogID.ToString
                        com.ExecuteNonQuery()

                    Case DataProvider.Sql

                        com.CommandText = "delete LogSOM_InvalidXmlDetail where LogID = " & LogID.ToString
                        com.ExecuteNonQuery()

                End Select

            End Using

        End Using

    End Sub

    Friend Function NextIDPervasiveOnly(ByRef Con As Connection) As Integer

        Dim Repository As New LogSaleOrderMonitorInvalidXmlRepositoryPervasive

        Return Repository.NextID(Con)

    End Function

    'Private Function NextStoreOrderNumber() As Integer

    '    Dim DT As New DataTable

    '    Using con As New Connection

    '        Using com As New Command(con)

    '            Select Case Cts.Oasys.Hubs.Data.GetDataProvider

    '                Case DataProvider.Odbc

    '                    com.CommandText = "select max(StoreOrderNo) as MaxStoreOrderNumber from LogSOMBadXML"
    '                    DT = com.ExecuteDataTable

    '                Case DataProvider.Sql

    '                    com.CommandText = "select MaxStoreOrderNumber = isnull(max(StoreOrderNo), 0) from LogSOM_InvalidXml"
    '                    DT = com.ExecuteDataTable

    '            End Select

    '        End Using

    '    End Using

    '    If DT.Rows(0).IsNull("MaxStoreOrderNumber") = True Then Return 0

    '    Return CType(DT.Rows(0).Item("MaxStoreOrderNumber"), Integer)

    'End Function

#End Region

    Private Function GetNextStoreOrderNumber() As Integer

        _StoreOrderNumber.Add(_StoreOrderNumber.Count)

        Return _StoreOrderNumber.Count

    End Function

    Private Function RequestWebServiceTypeConvertToString(ByVal RequestWebServiceType As WebServiceType) As String

        'PLEASE NOTE THIS FUNCTION IS A COPY OF LogSaleOrderMonitorInvalidXmlRepositoryPervasive->RequestWebServiceTypeConvertToString

        Dim StringValue As String = String.Empty

        Select Case RequestWebServiceType

            Case WebServiceType.CreateOrder

                StringValue = "Create Order"

            Case WebServiceType.RefundOrder

                StringValue = "Refund Order"

            Case WebServiceType.StatusUpdate

                StringValue = "Status Update"

        End Select

        Return StringValue

    End Function

#End Region

End Class