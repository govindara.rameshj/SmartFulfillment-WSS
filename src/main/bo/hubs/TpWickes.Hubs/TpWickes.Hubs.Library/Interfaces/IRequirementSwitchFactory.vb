﻿Public Interface IRequirementSwitchFactory(Of T)

    Function GetImplementation() As T
    Function ImplementationA_IsActive() As Boolean
    Function ImplementationA() As T
    Function ImplementationB() As T
End Interface
