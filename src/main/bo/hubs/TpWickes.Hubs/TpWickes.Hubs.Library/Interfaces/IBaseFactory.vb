﻿Public Interface IBaseFactory(Of T)

    Function GetImplementation() As T
    Function Implementation() As T
End Interface
