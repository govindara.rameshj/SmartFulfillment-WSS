﻿Public Class RequirementRepositoryFactory

    Private Shared m_FactoryMember As IRequirementRepository

    Public Shared Function FactoryGet() As IRequirementRepository

        If m_FactoryMember Is Nothing Then

            Return New RequirementRepository              'live implementation

        Else

            Return m_FactoryMember                        'stub implementation

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As IRequirementRepository)

        m_FactoryMember = obj

    End Sub

End Class