﻿Public Class RequirementRepository
    Implements IRequirementRepository

    Public Function RequirementEnabled(ByVal RequirementID As Integer) As Boolean? Implements IRequirementRepository.RequirementEnabled

        Return Cts.Oasys.Hubs.Core.System.Parameter.GetBoolean(RequirementID)

    End Function

End Class