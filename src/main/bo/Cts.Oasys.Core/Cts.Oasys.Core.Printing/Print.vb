﻿Imports DevExpress.XtraPrintingLinks
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports System.Drawing
Imports DevExpress.XtraGrid.Views.Grid
Imports Cts.Oasys.Core
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo

Public Class Print
    Implements IDisposable
    Private _grid As GridControl
    Private _storeIdName As String = String.Empty
    Private _userIdName As String = String.Empty
    Private _workstationId As Integer
    Private _printHeader As String = String.Empty
    Private _printHeaderSub As String = String.Empty
    Private _printLandscape As Boolean

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 21/06/2011
    ' Referral No : 677
    ' Notes       : Modifed to Allow support users to use the support logins provided.
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Public Sub New(ByVal grid As GridControl, ByVal userId As Integer, ByVal workstationId As Integer)
        _grid = grid
        _storeIdName = Core.System.Store.GetIdName
        _userIdName = Core.System.User.GetUserIdName(userId, IncludeExternalIds:=True)
        _workstationId = workstationId
        _printHeader = _grid.MainView.Name
    End Sub

    Public Property PrintHeader() As String
        Get
            Return _printHeader
        End Get
        Set(ByVal value As String)
            _printHeader = value
        End Set
    End Property
    Public Property PrintHeaderSub() As String
        Get
            Return _printHeaderSub
        End Get
        Set(ByVal value As String)
            _printHeaderSub = value
        End Set
    End Property
    Public Property PrintLandscape() As Boolean
        Get
            Return _printLandscape
        End Get
        Set(ByVal value As Boolean)
            _printLandscape = value
        End Set
    End Property


    Private Function GetReport() As CompositeLink

        'set up grid
        Dim view As GridView = CType(_grid.MainView, GridView)
        view.Appearance.HeaderPanel.Font = New Font("Tahoma", 8, FontStyle.Bold)
        view.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        view.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        view.Appearance.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        view.Appearance.HeaderPanel.Options.UseFont = True
        view.Appearance.HeaderPanel.Options.UseTextOptions = True
        view.Appearance.GroupFooter.Font = New Font("Tahoma", 7.5, FontStyle.Bold)
        view.Appearance.GroupFooter.Options.UseFont = True
        view.Appearance.FooterPanel.Font = New Font("Tahoma", 7.5, FontStyle.Bold)
        view.Appearance.FooterPanel.Options.UseFont = True

        view.AppearancePrint.Assign(view.Appearance)
        view.AppearancePrint.HeaderPanel.Font = New Font("Tahoma", 7.5, FontStyle.Bold)
        view.AppearancePrint.Row.Font = New Font("Tahoma", 7, FontStyle.Regular)
        view.OptionsPrint.UsePrintStyles = True
        view.OptionsPrint.PrintFilterInfo = False
        view.OptionsBehavior.Editable = False

        'set up report
        Dim complink As New CompositeLink(New PrintingSystem)
        complink.Margins.Bottom = 70
        complink.Margins.Top = 70
        complink.Margins.Left = 50
        complink.Margins.Right = 50
        complink.Landscape = _printLandscape
        complink.PaperKind = Global.System.Drawing.Printing.PaperKind.A4
        complink.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Watermark, CommandVisibility.None)
        complink.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.FillBackground, CommandVisibility.None)
        complink.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.EditPageHF, CommandVisibility.None)

        AddHandler complink.CreateMarginalHeaderArea, AddressOf Report_CreateMarginalHeaderArea
        AddHandler complink.CreateMarginalFooterArea, AddressOf Report_CreateMarginalFooterArea

        Dim reportGrid As New PrintableComponentLink
        reportGrid.Component = _grid
        complink.Links.Add(reportGrid)

        Return complink

    End Function

    Private Sub Report_CreateMarginalHeaderArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        e.Graph.Modifier = BrickModifier.MarginalHeader
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near, StringAlignment.Far)
        e.Graph.Font = New Font("Tahoma", 9, FontStyle.Bold)
        Dim tb As TextBrick = e.Graph.DrawString(_printHeader, Color.Black, New RectangleF(0, 10, e.Graph.ClientPageSize.Width / 2, 15), DevExpress.XtraPrinting.BorderSide.None)

        e.Graph.Font = New Font("Tahoma", 9, FontStyle.Regular)
        tb = e.Graph.DrawString(_printHeaderSub, Color.Black, New RectangleF(0, 25, e.Graph.ClientPageSize.Width / 2, 15), DevExpress.XtraPrinting.BorderSide.None)

        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Far, StringAlignment.Far)
        tb = e.Graph.DrawString("Date: " & Now.Date.ToShortDateString, Color.Black, New RectangleF(e.Graph.ClientPageSize.Width / 2, 25, e.Graph.ClientPageSize.Width / 2, 15), DevExpress.XtraPrinting.BorderSide.None)

    End Sub

    Private Sub Report_CreateMarginalFooterArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        e.Graph.Modifier = BrickModifier.MarginalFooter

        Dim r As RectangleF = RectangleF.Empty
        r.Height = 30

        Dim sb As New StringBuilder
        sb.Append("Printed: " & _userIdName & Space(2) & "WS: " & _workstationId & Space(2) & "Time: {0:HH:mm}")
        sb.Append(Environment.NewLine)
        sb.Append("Wickes " & _storeIdName)

        Dim pibPrinted As New PageInfoBrick
        pibPrinted = e.Graph.DrawPageInfo(PageInfo.DateTime, sb.ToString, Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pibPrinted.Alignment = BrickAlignment.Near
        pibPrinted.HorzAlignment = DevExpress.Utils.HorzAlignment.Near

        Dim pibPage As New PageInfoBrick
        pibPage = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, "Page: {0} of {1}", Color.Black, r, DevExpress.XtraPrinting.BorderSide.None)
        pibPage.Alignment = BrickAlignment.Far

    End Sub

    Private Sub ReportBreak_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim tb As TextBrick = New TextBrick()
        tb.Rect = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 15)
        tb.BackColor = Color.Transparent
        tb.BorderStyle = BrickBorderStyle.Inset
        tb.BorderColor = Color.White
        e.Graph.DrawBrick(tb)
    End Sub



    Public Sub ShowPreviewDialog()
        Using complink As CompositeLink = GetReport()
            complink.ShowPreviewDialog()
        End Using
    End Sub

    Public Sub Print()
        Using complink As CompositeLink = GetReport()
            Dim ps As New Global.System.Drawing.Printing.PrinterSettings
            complink.Print(ps.PrinterName)
        End Using
    End Sub

    Public Sub Print(ByVal printerName As String)
        Using complink As CompositeLink = GetReport()
            complink.Print(printerName)
        End Using
    End Sub

#Region " IDisposable Support "
    Private disposedValue As Boolean = False

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then

        End If
        Me.disposedValue = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
