﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic.Strings


' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Cts.Oasys.Core.Purchasing")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("TP-Wickes")> 
<Assembly: AssemblyProduct("Cts.Oasys.Core.Purchasing")> 
<Assembly: AssemblyCopyright("Copyright " & Chrw(169) & " 2010")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("edc8594f-c328-4398-8ce2-eb7f3d4be24f")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("3.0.0.1")> 
<Assembly: AssemblyFileVersion("3.0.0.1")> 
