﻿Imports System.Text
Imports Cts.Oasys.Core
Imports System.ComponentModel

Public Class [Return]
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _id As Integer
    Private _number As Integer
    Private _supplierNumber As String
    Private _supplierName As String
    Private _dateCreated As Date
    Private _dateCollected As Date
    Private _receiptNumber As String
    Private _value As Decimal
    Private _isDeleted As Boolean
    Private _notPrintedEntry As Boolean
    Private _notPrintedRelease As Boolean
    Private _lines As ReturnLineCollection = Nothing
#End Region

#Region "Properties"
    <ColumnMapping("Id")> Public Property Id() As Integer
        Get
            Return _id
        End Get
        Private Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    <ColumnMapping("Number")> Public Property Number() As Integer
        Get
            Return _number
        End Get
        Private Set(ByVal value As Integer)
            _number = value
        End Set
    End Property
    <ColumnMapping("SupplierNumber")> Public Property SupplierNumber() As String
        Get
            Return _supplierNumber
        End Get
        Private Set(ByVal value As String)
            _supplierNumber = value
        End Set
    End Property
    <ColumnMapping("SupplierName")> Public Property SupplierName() As String
        Get
            Return _supplierName
        End Get
        Private Set(ByVal value As String)
            _supplierName = value
        End Set
    End Property
    <ColumnMapping("DateCreated")> Public Property DateCreated() As Date
        Get
            Return _dateCreated
        End Get
        Private Set(ByVal value As Date)
            _dateCreated = value
        End Set
    End Property
    <ColumnMapping("DateCollected")> Public Property DateCollected() As Date
        Get
            Return _dateCollected
        End Get
        Private Set(ByVal value As Date)
            _dateCollected = value
        End Set
    End Property
    <ColumnMapping("ReceiptNumber")> Public Property ReceiptNumber() As String
        Get
            Return _receiptNumber
        End Get
        Private Set(ByVal value As String)
            _receiptNumber = value
        End Set
    End Property
    <ColumnMapping("Value")> Public Property Value() As Decimal
        Get
            Return _value
        End Get
        Private Set(ByVal value As Decimal)
            _value = value
        End Set
    End Property
    <ColumnMapping("IsDeleted")> Public Property IsDeleted() As Boolean
        Get
            Return _isDeleted
        End Get
        Private Set(ByVal value As Boolean)
            _isDeleted = value
        End Set
    End Property
    <ColumnMapping("NotPrintedEntry")> Public Property NotPrintedEntry() As Boolean
        Get
            Return _notPrintedEntry
        End Get
        Set(ByVal value As Boolean)
            _notPrintedEntry = value
        End Set
    End Property
    <ColumnMapping("NotPrintedRelease")> Public Property NotPrintedRelease() As Boolean
        Get
            Return _notPrintedRelease
        End Get
        Set(ByVal value As Boolean)
            _notPrintedRelease = value
        End Set
    End Property
    Public ReadOnly Property Lines() As ReturnLineCollection
        Get
            If _lines Is Nothing Then _lines = New ReturnLineCollection(_id)
            Return _lines
        End Get
    End Property
#End Region

#Region "Constructors"

    Public Sub New()
        MyBase.New()
    End Sub

    Private Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region

    Public Shared Function GetNonReleased() As BindingList(Of [Return])

        Dim returns As New BindingList(Of [Return])

        Dim dt As DataTable = DataOperations.ReturnGetNonReleased
        For Each dr As DataRow In dt.Rows
            returns.Add(New [Return](dr))
        Next

        Return returns

    End Function

End Class
