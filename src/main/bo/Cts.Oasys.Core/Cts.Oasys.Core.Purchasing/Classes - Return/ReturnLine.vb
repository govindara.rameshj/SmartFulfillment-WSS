﻿Imports Cts.Oasys.Core

<CLSCompliant(True)> Public Class ReturnLine
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _id As Integer
    Private _returnId As Integer
    Private _skuNumber As String
    Private _skuDescription As String
    Private _qty As Integer
    Private _price As Decimal
    Private _cost As Decimal
    Private _reasonCode As String
    Private _reasonDescription As String
#End Region

#Region "Properties"
    <ColumnMapping("Id")> Public Property Id() As Integer
        Get
            Return _id
        End Get
        Friend Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    <ColumnMapping("ReturnId")> Public Property ReturnId() As Integer
        Get
            Return _returnId
        End Get
        Friend Set(ByVal value As Integer)
            _returnId = value
        End Set
    End Property
    <ColumnMapping("SkuNumber")> Public Property SkuNumber() As String
        Get
            Return _skuNumber
        End Get
        Set(ByVal value As String)
            _skuNumber = value
        End Set
    End Property
    <ColumnMapping("SkuDescription")> Public Property SkuDescription() As String
        Get
            Return _skuDescription
        End Get
        Private Set(ByVal value As String)
            _skuDescription = value
        End Set
    End Property
    <ColumnMapping("Qty")> Public Property Qty() As Integer
        Get
            Return _qty
        End Get
        Set(ByVal value As Integer)
            _qty = value
        End Set
    End Property
    <ColumnMapping("Price")> Public Property Price() As Decimal
        Get
            Return _price
        End Get
        Set(ByVal value As Decimal)
            _price = value
        End Set
    End Property
    <ColumnMapping("Cost")> Public Property Cost() As Decimal
        Get
            Return _cost
        End Get
        Private Set(ByVal value As Decimal)
            _cost = value
        End Set
    End Property
    <ColumnMapping("ReasonCode")> Public Property ReasonCode() As String
        Get
            Return _reasonCode
        End Get
        Set(ByVal value As String)
            _reasonCode = value
        End Set
    End Property
    <ColumnMapping("ReasonDescription")> Public Property ReasonDescription() As String
        Get
            Return _reasonDescription
        End Get
        Set(ByVal value As String)
            _reasonDescription = value
        End Set
    End Property
#End Region

#Region "Constructors"
    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region



End Class