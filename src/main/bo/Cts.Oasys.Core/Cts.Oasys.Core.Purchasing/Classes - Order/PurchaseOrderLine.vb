﻿Imports Cts.Oasys.Core

Public Class PurchaseOrderLine
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _id As Integer
    Private _orderId As Integer
    Private _skuNumber As String
    Private _skuDescription As String
    Private _skuProductCode As String
    Private _skuPackSize As Integer
    Private _orderQty As Integer
    Private _price As Decimal
    Private _cost As Decimal
#End Region

#Region "Properties"
    <ColumnMapping("Id")> Public Property Id() As Integer
        Get
            Return _id
        End Get
        Private Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    <ColumnMapping("OrderId")> Public Property OrderId() As Integer
        Get
            Return _orderId
        End Get
        Friend Set(ByVal value As Integer)
            _orderId = value
        End Set
    End Property
    <ColumnMapping("SkuNumber")> Public Property SkuNumber() As String
        Get
            Return _skuNumber
        End Get
        Private Set(ByVal value As String)
            _skuNumber = value
        End Set
    End Property
    <ColumnMapping("SkuDescription")> Public Property SkuDescription() As String
        Get
            Return _skuDescription
        End Get
        Private Set(ByVal value As String)
            _skuDescription = value.Trim
        End Set
    End Property
    <ColumnMapping("SkuProductCode")> Public Property SkuProductCode() As String
        Get
            Return _skuProductCode
        End Get
        Private Set(ByVal value As String)
            _skuProductCode = value.Trim
        End Set
    End Property
    <ColumnMapping("SkuPackSize")> Public Property SkuPackSize() As Integer
        Get
            Return _skuPackSize
        End Get
        Private Set(ByVal value As Integer)
            _skuPackSize = value
        End Set
    End Property
    <ColumnMapping("OrderQty")> Public Property OrderQty() As Integer
        Get
            Return _orderQty
        End Get
        Set(ByVal value As Integer)
            _orderQty = value
        End Set
    End Property
    <ColumnMapping("Price")> Public Property Price() As Decimal
        Get
            Return _price
        End Get
        Private Set(ByVal value As Decimal)
            _price = value
        End Set
    End Property
    <ColumnMapping("Cost")> Public Property Cost() As Decimal
        Get
            Return _cost
        End Get
        Private Set(ByVal value As Decimal)
            _cost = value
        End Set
    End Property
#End Region

#Region "Constructors"
    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

    Public Sub New(ByVal stockItem As Stock.Stock)
        MyBase.New()
        _skuNumber = stockItem.SkuNumber
        _skuDescription = stockItem.Description
        _skuProductCode = stockItem.ProductCode
        _skuPackSize = stockItem.PackSize
        _price = stockItem.Price
        _cost = stockItem.Cost
    End Sub
#End Region

End Class