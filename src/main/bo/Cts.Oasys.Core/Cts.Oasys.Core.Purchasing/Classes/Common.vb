﻿Public Enum RtiState
    ToBeSent
    Completed
End Enum

Friend Structure RtiStates
    Private _value As String
    Public Shared ToBeSent As String = "S"
    Public Shared Completed As String = "C"
End Structure