﻿Imports System.ComponentModel

<CLSCompliant(True)> Public Class SupplierMinimumOrderInfo
    Private _supplier As Supplier
    Private _minType As Type = Type.None

    Friend Sub New(ByRef sup As Supplier)
        _supplier = sup
        Select Case _supplier.MinOrderType
            Case "M" : _minType = Type.Money
            Case "C", "U" : _minType = Type.Unit
            Case "W", "T" : _minType = Type.Weight
            Case "E" : _minType = Type.MoneyOrUnit
        End Select
    End Sub

    Public Enum Result
        Successful
        Unsuccessful
        AllStocksZeroDemand
    End Enum

    Private Enum Type
        None
        Money
        Unit
        Weight
        MoneyOrUnit
    End Enum

    ''' <summary>
    ''' String representation of supplier minimum order constraints
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ConstraintString() As String
        Get
            Select Case _minType
                Case Type.Money : Return String.Format(My.Resources.Strings.MinOrderMoney, _supplier.MinOrderValue)
                Case Type.Unit : Return String.Format(My.Resources.Strings.MinOrderCartons, _supplier.MinOrderCartons)
                Case Type.Weight : Return String.Format(My.Resources.Strings.MinOrderWeight, _supplier.MinOrderWeight)
                Case Type.MoneyOrUnit : Return String.Format(My.Resources.Strings.MinOrderEither, _supplier.MinOrderValue, _supplier.MinOrderCartons)
                Case Else : Return My.Resources.Strings.MinOrderNone
            End Select
        End Get
    End Property

    ''' <summary>
    ''' Returns whether order has reached mcp
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Reached() As Boolean

        Dim value As Decimal
        Dim units As Integer
        Dim weight As Decimal
        _supplier.Stocks.TotalsOrder(value, units, weight)

        Select Case _minType
            Case Type.Money : If value > _supplier.MinOrderValue Then Return True
            Case Type.Unit : If units > _supplier.MinOrderCartons Then Return True
            Case Type.Weight : If weight > _supplier.MinOrderWeight Then Return True
            Case Type.MoneyOrUnit : If (value > _supplier.MinOrderValue) OrElse (units > _supplier.MinOrderCartons) Then Return True
            Case Else : Return True
        End Select
        Return False

    End Function

    ''' <summary>
    ''' Performs automatic increase of stock order quantities until min order value is reached
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IncreaseOrderToLevel() As Result

        'check that not all zero periods first
        If _supplier.Stocks.AllZeroPeriodDemand Then
            Return Result.AllStocksZeroDemand
        End If

        Dim week As Integer = CInt(Math.Floor(Now.DayOfYear / 53))
        Dim weights As New Core.System.Store.SaleWeightCollection


        Do While (Not Me.Reached())
            Dim stockToIncrease As Stock.Stock = Nothing
            Dim LowestCover As Decimal = Nothing
            Dim LowestPrice As Decimal = Nothing

            'loop over all order stocks (forcing load)
            'check whether demand period is greater then zero in order to include in mcp
            For Each stockItem As Stock.Stock In _supplier.Stocks
                If stockItem.IsNonStock Then Continue For

                If stockItem.PeriodDemand > 0 Then
                    weights.AddActiveWeights(stockItem.SaleWeightBank)
                    weights.AddActiveWeights(stockItem.SaleWeightPromo)
                    weights.AddActiveWeights(stockItem.SaleWeightSeason)
                    Dim adjuster As Double = weights.Adjuster(week)
                    Dim cover As Decimal = CDec(Math.Abs(stockItem.OrderQty - stockItem.OrderLevel) / (stockItem.PeriodDemand * adjuster))

                    If (stockToIncrease Is Nothing) Then
                        stockToIncrease = stockItem
                        LowestPrice = stockItem.Price
                        LowestCover = cover

                    ElseIf (cover = LowestCover) And (stockItem.Price < LowestPrice) Then
                        stockToIncrease = stockItem
                        LowestPrice = stockItem.Price
                        LowestCover = cover

                    ElseIf (cover < LowestCover) Then
                        stockToIncrease = stockItem
                        LowestPrice = stockItem.Price
                        LowestCover = cover

                    End If
                End If
            Next

            If stockToIncrease IsNot Nothing Then
                stockToIncrease.OrderQty += stockToIncrease.PackSize
            End If
        Loop

        Return Result.Successful

    End Function

End Class