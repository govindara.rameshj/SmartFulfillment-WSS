﻿Imports Cts.Oasys.Core
Imports System.ComponentModel
Imports Cts.Oasys.Core.Stock

<CLSCompliant(True)> Public Class Supplier
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _number As String
    Private _name As String
    Private _alpha As String
    Private _orderDepot As Integer
    Private _dateLastOrdered As Nullable(Of Date)
    Private _dateOrderCloseStart As Nullable(Of Date)
    Private _dateOrderCloseEnd As Nullable(Of Date)
    Private _soqNumber As Nullable(Of Integer)
    Private _soqOrdered As Boolean
    Private _soqDate As Nullable(Of Date)
    Private _palletCheck As Boolean
    Private _bbcNumber As String
    Private _isDeleted As Boolean
    Private _isTradanet As Boolean
    Private _depotNumber As Integer
    Private _depotNotes As String
    Private _phoneNumber1 As String
    Private _reviewDay0 As Boolean
    Private _reviewDay1 As Boolean
    Private _reviewDay2 As Boolean
    Private _reviewDay3 As Boolean
    Private _reviewDay4 As Boolean
    Private _reviewDay5 As Boolean
    Private _reviewDay6 As Boolean
    Private _leadTime0 As Integer
    Private _leadTime1 As Integer
    Private _leadTime2 As Integer
    Private _leadTime3 As Integer
    Private _leadTime4 As Integer
    Private _leadTime5 As Integer
    Private _leadTime6 As Integer
    Private _leadTimeFixed As Integer
    Private _minOrderType As String
    Private _minOrderValue As Integer
    Private _minOrderCartons As Integer
    Private _minOrderWeight As Integer

    'Private _soqStatus As Soq.State
    Private _progress As Integer
    Private _minOrder As SupplierMinimumOrderInfo = Nothing
    Private _stocks As StockCollection = Nothing
#End Region

#Region "Properties"
    <ColumnMapping("Number")> Public Property Number() As String
        Get
            Return _number
        End Get
        Private Set(ByVal value As String)
            _number = value
        End Set
    End Property
    <ColumnMapping("Name")> Public Property Name() As String
        Get
            Return _name
        End Get
        Private Set(ByVal value As String)
            _name = value
        End Set
    End Property
    <ColumnMapping("Alpha")> Public Property Alpha() As String
        Get
            Return _alpha
        End Get
        Private Set(ByVal value As String)
            _alpha = value
        End Set
    End Property
    <ColumnMapping("OrderDepot")> Public Property OrderDepot() As Integer
        Get
            Return _orderDepot
        End Get
        Private Set(ByVal value As Integer)
            _orderDepot = value
        End Set
    End Property
    <ColumnMapping("DateLastOrdered")> Public Property DateLastOrdered() As Nullable(Of Date)
        Get
            Return _dateLastOrdered
        End Get
        Private Set(ByVal value As Nullable(Of Date))
            _dateLastOrdered = value
        End Set
    End Property
    <ColumnMapping("DateOrderCloseStart")> Public Property DateOrderCloseStart() As Nullable(Of Date)
        Get
            Return _dateOrderCloseStart
        End Get
        Private Set(ByVal value As Nullable(Of Date))
            _dateOrderCloseStart = value
        End Set
    End Property
    <ColumnMapping("DateOrderCloseEnd")> Public Property DateOrderCloseEnd() As Nullable(Of Date)
        Get
            Return _dateOrderCloseEnd
        End Get
        Private Set(ByVal value As Nullable(Of Date))
            _dateOrderCloseEnd = value
        End Set
    End Property
    <ColumnMapping("SoqNumber")> Public Property SoqNumber() As Nullable(Of Integer)
        Get
            Return _soqNumber
        End Get
        Friend Set(ByVal value As Nullable(Of Integer))
            _soqNumber = value
        End Set
    End Property
    <ColumnMapping("SoqOrdered")> Public Property SoqOrdered() As Boolean
        Get
            Return _soqOrdered
        End Get
        Friend Set(ByVal value As Boolean)
            _soqOrdered = value
        End Set
    End Property
    <ColumnMapping("SoqDate")> Public Property SoqDate() As Nullable(Of Date)
        Get
            Return _soqDate
        End Get
        Friend Set(ByVal value As Nullable(Of Date))
            _soqDate = value
        End Set
    End Property
    <ColumnMapping("PalletCheck")> Public Property PalletCheck() As Boolean
        Get
            Return _palletCheck
        End Get
        Private Set(ByVal value As Boolean)
            _palletCheck = value
        End Set
    End Property
    <ColumnMapping("BbcNumber")> Public Property BbcNumber() As String
        Get
            If _bbcNumber Is Nothing Then _bbcNumber = String.Empty
            Return _bbcNumber
        End Get
        Private Set(ByVal value As String)
            _bbcNumber = value
        End Set
    End Property
    <ColumnMapping("IsTradanet")> Public Property IsTradanet() As Boolean
        Get
            Return _isTradanet
        End Get
        Private Set(ByVal value As Boolean)
            _isTradanet = value
        End Set
    End Property
    <ColumnMapping("IsDeleted")> Public Property IsDeleted() As Boolean
        Get
            Return _isDeleted
        End Get
        Private Set(ByVal value As Boolean)
            _isDeleted = value
        End Set
    End Property
    <ColumnMapping("DepotNumber")> Public Property DepotNumber() As Integer
        Get
            Return _depotNumber
        End Get
        Private Set(ByVal value As Integer)
            _depotNumber = value
        End Set
    End Property
    <ColumnMapping("DepotNotes")> Public Property DepotNotes() As String
        Get
            Return _depotNotes
        End Get
        Private Set(ByVal value As String)
            _depotNotes = value
        End Set
    End Property
    <ColumnMapping("PhoneNumber1")> Public Property PhoneNumber1() As String
        Get
            Return _phoneNumber1
        End Get
        Private Set(ByVal value As String)
            _phoneNumber1 = value
        End Set
    End Property
    <ColumnMapping("ReviewDay0")> Public Property ReviewDay0() As Boolean
        Get
            Return _reviewDay0
        End Get
        Private Set(ByVal value As Boolean)
            _reviewDay0 = value
        End Set
    End Property
    <ColumnMapping("ReviewDay1")> Public Property ReviewDay1() As Boolean
        Get
            Return _reviewDay1
        End Get
        Private Set(ByVal value As Boolean)
            _reviewDay1 = value
        End Set
    End Property
    <ColumnMapping("ReviewDay2")> Public Property ReviewDay2() As Boolean
        Get
            Return _reviewDay2
        End Get
        Private Set(ByVal value As Boolean)
            _reviewDay2 = value
        End Set
    End Property
    <ColumnMapping("ReviewDay3")> Public Property ReviewDay3() As Boolean
        Get
            Return _reviewDay3
        End Get
        Private Set(ByVal value As Boolean)
            _reviewDay3 = value
        End Set
    End Property
    <ColumnMapping("ReviewDay4")> Public Property ReviewDay4() As Boolean
        Get
            Return _reviewDay4
        End Get
        Private Set(ByVal value As Boolean)
            _reviewDay4 = value
        End Set
    End Property
    <ColumnMapping("ReviewDay5")> Public Property ReviewDay5() As Boolean
        Get
            Return _reviewDay5
        End Get
        Private Set(ByVal value As Boolean)
            _reviewDay5 = value
        End Set
    End Property
    <ColumnMapping("ReviewDay6")> Public Property ReviewDay6() As Boolean
        Get
            Return _reviewDay6
        End Get
        Private Set(ByVal value As Boolean)
            _reviewDay6 = value
        End Set
    End Property
    Public ReadOnly Property ReviewDay(ByVal dayWeek As DayOfWeek) As Boolean
        Get
            Select Case dayWeek
                Case DayOfWeek.Sunday : Return _reviewDay0
                Case DayOfWeek.Monday : Return _reviewDay1
                Case DayOfWeek.Tuesday : Return _reviewDay2
                Case DayOfWeek.Wednesday : Return _reviewDay3
                Case DayOfWeek.Thursday : Return _reviewDay4
                Case DayOfWeek.Friday : Return _reviewDay5
                Case DayOfWeek.Saturday : Return _reviewDay6
                Case Else : Return False
            End Select
        End Get
    End Property
    <ColumnMapping("LeadTime0")> Public Property LeadTime0() As Integer
        Get
            Return _leadTime0
        End Get
        Private Set(ByVal value As Integer)
            _leadTime0 = value
        End Set
    End Property
    <ColumnMapping("LeadTime1")> Public Property LeadTime1() As Integer
        Get
            Return _leadTime1
        End Get
        Private Set(ByVal value As Integer)
            _leadTime1 = value
        End Set
    End Property
    <ColumnMapping("LeadTime2")> Public Property LeadTime2() As Integer
        Get
            Return _leadTime2
        End Get
        Private Set(ByVal value As Integer)
            _leadTime2 = value
        End Set
    End Property
    <ColumnMapping("LeadTime3")> Public Property LeadTime3() As Integer
        Get
            Return _leadTime3
        End Get
        Private Set(ByVal value As Integer)
            _leadTime3 = value
        End Set
    End Property
    <ColumnMapping("LeadTime4")> Public Property LeadTime4() As Integer
        Get
            Return _leadTime4
        End Get
        Private Set(ByVal value As Integer)
            _leadTime4 = value
        End Set
    End Property
    <ColumnMapping("LeadTime5")> Public Property LeadTime5() As Integer
        Get
            Return _leadTime5
        End Get
        Private Set(ByVal value As Integer)
            _leadTime5 = value
        End Set
    End Property
    <ColumnMapping("LeadTime6")> Public Property LeadTime6() As Integer
        Get
            Return _leadTime6
        End Get
        Private Set(ByVal value As Integer)
            _leadTime6 = value
        End Set
    End Property
    Public ReadOnly Property LeadTime(ByVal dayWeek As DayOfWeek) As Integer
        Get
            Select Case dayWeek
                Case DayOfWeek.Sunday : Return _leadTime0
                Case DayOfWeek.Monday : Return _leadTime1
                Case DayOfWeek.Tuesday : Return _leadTime2
                Case DayOfWeek.Wednesday : Return _leadTime3
                Case DayOfWeek.Thursday : Return _leadTime4
                Case DayOfWeek.Friday : Return _leadTime5
                Case DayOfWeek.Saturday : Return _leadTime6
                Case Else : Return 0
            End Select
        End Get
    End Property
    <ColumnMapping("LeadTimeFixed")> Public Property LeadTimeFixed() As Integer
        Get
            Return _leadTimeFixed
        End Get
        Private Set(ByVal value As Integer)
            _leadTimeFixed = value
        End Set
    End Property
    <ColumnMapping("MinOrderType")> Friend Property MinOrderType() As String
        Get
            Return _minOrderType
        End Get
        Private Set(ByVal value As String)
            _minOrderType = value
        End Set
    End Property
    <ColumnMapping("MinOrderValue")> Friend Property MinOrderValue() As Integer
        Get
            Return _minOrderValue
        End Get
        Private Set(ByVal value As Integer)
            _minOrderValue = value
        End Set
    End Property
    <ColumnMapping("MinOrderCartons")> Friend Property MinOrderCartons() As Integer
        Get
            Return _minOrderCartons
        End Get
        Private Set(ByVal value As Integer)
            _minOrderCartons = value
        End Set
    End Property
    <ColumnMapping("MinOrderWeight")> Friend Property MinOrderWeight() As Integer
        Get
            Return _minOrderWeight
        End Get
        Private Set(ByVal value As Integer)
            _minOrderWeight = value
        End Set
    End Property

    Public Property Progress() As Integer
        Get
            Return _progress
        End Get
        Set(ByVal value As Integer)
            _progress = value
        End Set
    End Property

    ''' <summary>
    ''' String Representation of supplier type (Direct/Warehouse)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property SupplierTypeString() As String
        Get
            Select Case True
                Case BbcNumber.Trim.Length = 0 : Return My.Resources.Strings.TypeDirect
                Case Else : Return My.Resources.Strings.TypeWarehouse
            End Select
        End Get
    End Property

    ''' <summary>
    ''' String representation of supplier order type (eg. IW Discrete Tradanet)
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property OrderTypeString() As String
        Get
            Dim sb As New Text.StringBuilder
            Select Case _bbcNumber
                Case "D" : sb.Append(My.Resources.Strings.TypeIWDiscrete)
                Case "C" : sb.Append(My.Resources.Strings.TypeIWConsolidated)
                Case "W" : sb.Append(My.Resources.Strings.TypeIW)
                Case "A" : sb.Append(My.Resources.Strings.TypeAlternativeSupplier)
                Case Else : sb.Append(My.Resources.Strings.TypeDirect)
            End Select
            If IsTradanet Then sb.Append(Space(1) & My.Resources.Strings.TypeTradanet)
            Return sb.ToString
        End Get
    End Property

    '''' <summary>
    '''' Returns soq status of supplier as SoqStates enumeration
    '''' </summary>
    '''' <value></value>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Public ReadOnly Property SoqStatus() As Soq.State
    '    Get
    '        CalculateSoqStatus()
    '        Return _soqStatus
    '    End Get
    'End Property

    '''' <summary>
    '''' String representation of supplier soq status
    '''' </summary>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Public ReadOnly Property SoqStatusString() As String
    '    Get
    '        CalculateSoqStatus()
    '        Select Case _soqStatus
    '            Case Soq.State.Expired : Return My.Resources.Strings.SoqExpired
    '            Case Soq.State.No : Return My.Resources.Strings.SoqNo
    '            Case Soq.State.Ordered : Return My.Resources.Strings.SoqOrdered
    '            Case Soq.State.Yes : Return My.Resources.Strings.SoqSuggested
    '            Case Else : Return String.Empty
    '        End Select
    '    End Get
    'End Property

    ''' <summary>
    ''' Collection of stocks for this supplier
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Stocks() As StockCollection
        Get
            If _stocks Is Nothing Then _stocks = Stock.Stock.GetStocksBySupplier(_number)
            Return _stocks
        End Get
    End Property

    ''' <summary>
    ''' Supplier minimum order properties
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property MinimumOrder() As SupplierMinimumOrderInfo
        Get
            If _minOrder Is Nothing Then _minOrder = New SupplierMinimumOrderInfo(Me)
            Return _minOrder
        End Get
    End Property
#End Region

#Region "Constructors"
    Public Sub New()
        MyBase.New()
    End Sub

    Private Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub
#End Region

    ''' <summary>
    ''' Returns order due date for supplier taking days store open into account. 
    ''' </summary>
    ''' <param name="daysOpen"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DateOrderDue(ByVal daysOpen As Integer, Optional ByVal fromDate As Date = Nothing) As Date

        If fromDate = Nothing Then
            fromDate = Now.Date
        End If

        Dim lead As Integer = 0
        Dim dueDate As Date

        Select Case _bbcNumber
            Case "D", "C", "W", "A" : dueDate = fromDate.AddDays(LeadTime(fromDate.AddDays(1).DayOfWeek) - 1)
            Case Else : dueDate = fromDate.AddDays(LeadTime(fromDate.DayOfWeek))
        End Select

        'check that duedate is on an open day
        Select Case dueDate.DayOfWeek
            Case DayOfWeek.Sunday : If daysOpen < 7 Then dueDate = dueDate.AddDays(1)
            Case DayOfWeek.Saturday : If daysOpen < 6 Then dueDate = dueDate.AddDays(2)
            Case DayOfWeek.Friday : If daysOpen < 5 Then dueDate = dueDate.AddDays(3)
            Case DayOfWeek.Thursday : If daysOpen < 4 Then dueDate = dueDate.AddDays(4)
            Case DayOfWeek.Wednesday : If daysOpen < 3 Then dueDate = dueDate.AddDays(5)
            Case DayOfWeek.Tuesday : If daysOpen < 2 Then dueDate = dueDate.AddDays(6)
        End Select

        Return dueDate

    End Function

    ''' <summary>
    ''' Returns next but one order due date taking days store open into account.
    ''' </summary>
    ''' <param name="daysOpen"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DateNextOrderDue(ByVal daysOpen As Integer) As Date

        Return DateOrderDue(daysOpen, DateOrderDue(daysOpen))

    End Function


    'Private Sub CalculateSoqStatus()

    '    Select Case True
    '        Case (Not _soqDate.HasValue)
    '            _soqStatus = Soq.State.No

    '        Case (_soqDate.Value <> Now.Date)
    '            _soqStatus = Soq.State.Expired

    '        Case _soqOrdered
    '            _soqStatus = Soq.State.Ordered

    '        Case _soqNumber.HasValue AndAlso _soqNumber.Value <> 0
    '            _soqStatus = Soq.State.Yes

    '        Case Else
    '            _soqStatus = Soq.State.No
    '    End Select

    'End Sub


    ''' <summary>
    ''' Resets all supplier stock items soq order level to zero
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub SoqReset()

        SupplierUpdateSoqReset(Me)

    End Sub



    Private Function DaysReviewVariable() As Integer

        Dim reviewVariable As Integer = 0

        Dim lead As Integer = 0
        If BbcNumber.Trim.Length = 0 Then lead = 1

        'get number of days till next review day (limited to one week)
        Do Until ReviewDay(Now.AddDays(reviewVariable + lead).DayOfWeek) Or reviewVariable = 7
            reviewVariable += 1
        Loop

        ' Check whether supplier has closedown dates and get new review date if true and determine if
        ' value falls within this period and calculate next review day after closeEnd
        If (_dateOrderCloseStart IsNot Nothing) And (_dateOrderCloseEnd IsNot Nothing) Then

            If (Now.AddDays(reviewVariable) > _dateOrderCloseStart) AndAlso (Now.AddDays(reviewVariable) < _dateOrderCloseEnd) Then

                Dim check As Integer = 0
                reviewVariable = CInt(DateDiff(DateInterval.Day, Now.AddDays(lead), _dateOrderCloseEnd.Value.Date))
                Do Until ReviewDay(Now.AddDays(reviewVariable + lead).DayOfWeek) Or check = 7
                    reviewVariable += 1
                    check += 1
                Loop

            End If
        End If

        Return reviewVariable

    End Function

    Public Function DaysLeadVariable() As Integer

        Dim lead As Integer = 0
        If BbcNumber.Trim.Length = 0 Then lead = 1

        DaysLeadVariable = LeadTime(Now.AddDays(lead).DayOfWeek)
        If DaysLeadVariable = 0 Then DaysLeadVariable = _leadTimeFixed

    End Function

    Public Function DaysLeadReviewVariable() As Integer

        Return DaysLeadVariable() + DaysReviewVariable()

    End Function

    Public Function DaysLeadReviewFixed(ByVal daysOpen As Integer) As Decimal

        Return DaysLeadFixed(daysOpen) + DaysReviewFixed()

    End Function

    Public Function DaysLeadFixed(ByVal daysOpen As Integer) As Decimal

        If daysOpen = 0 Then Return 0
        Return CDec(_leadTimeFixed / daysOpen)

    End Function

    Public Function DaysReviewFixed() As Decimal

        Dim reviewFixed As Decimal = 0
        If _reviewDay0 Then reviewFixed += 1
        If _reviewDay1 Then reviewFixed += 1
        If _reviewDay2 Then reviewFixed += 1
        If _reviewDay3 Then reviewFixed += 1
        If _reviewDay4 Then reviewFixed += 1
        If _reviewDay5 Then reviewFixed += 1
        If _reviewDay6 Then reviewFixed += 1

        If reviewFixed = 0 Then reviewFixed = 1 Else reviewFixed = 1 / reviewFixed
        Return reviewFixed

    End Function



    Public Shared Function GetAllForOrder(Optional ByVal type As String = "A") As BindingList(Of Supplier)

        Dim suppliers As New BindingList(Of Supplier)

        Dim dt As DataTable = DataOperations.SupplierGetAllForOrder(type)
        For Each dr As DataRow In dt.Rows
            suppliers.Add(New Supplier(dr))
        Next

        Return suppliers

    End Function

    Public Shared Function GetByNumber(ByVal supplierNumber As String) As Supplier

        Dim dt As DataTable = DataOperations.SupplierGetOrdering(supplierNumber)
        If dt.Rows.Count = 0 Then
            dt = DataOperations.SupplierGetHeadOffice(supplierNumber)
        End If

        For Each dr As DataRow In dt.Rows
            Return New Supplier(dr)
        Next

        Return Nothing

    End Function

    Public Shared Function GetByNumbers(ByVal supplierNumbers As String) As BindingList(Of Supplier)

        Dim suppliers As New BindingList(Of Supplier)

        Dim dt As DataTable = DataOperations.SupplierGetByNumbers(supplierNumbers)
        For Each dr As DataRow In dt.Rows
            suppliers.Add(New Supplier(dr))
        Next

        Return suppliers

    End Function

    Public Shared Function GetAll() As BindingList(Of Supplier)

        Dim suppliers As New BindingList(Of Supplier)

        Dim dt As DataTable = DataOperations.SupplierGetAll
        For Each dr As DataRow In dt.Rows
            suppliers.Add(New Supplier(dr))
        Next

        Return suppliers

    End Function

End Class