﻿Imports Cts.Oasys.Core
Imports System.ComponentModel
Imports System.Text

Public Class Receipt
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _number As String
    Private _type As String
    Private _dateReceipt As Date
    Private _employeeId As Integer
    Private _value As Decimal
    Private _initials As String
    Private _comments As String
    Private _isProjectSalesOrder As Boolean
    Private _rtiStatus As String

    Private _poId As Integer
    Private _poNumber As Nullable(Of Integer)
    Private _poConsignNumber As Nullable(Of Integer)
    Private _poSupplierNumber As String
    Private _poSupplierName As String
    Private _poSupplierBbc As Boolean
    Private _poSoqNumber As Nullable(Of Integer)
    Private _poReleaseNumber As Integer
    Private _poOrderDate As Nullable(Of Date)
    Private _poDeliveryNote1 As String
    Private _poDeliveryNote2 As String
    Private _poDeliveryNote3 As String
    Private _poDeliveryNote4 As String
    Private _poDeliveryNote5 As String
    Private _poDeliveryNote6 As String
    Private _poDeliveryNote7 As String
    Private _poDeliveryNote8 As String
    Private _poDeliveryNote9 As String

    Private _ibtStoreId As String
    Private _ibtKeyedIn As String
    Private _ibtNeedsPrinting As Boolean
    Private _ibtConsignmentNumber As String
    Private _returnSupplierNumber As String
    Private _returnDate As Nullable(Of Date)
    Private _returnNumber As String

    Private _lines As ReceiptLineCollection = Nothing
#End Region

#Region "Properties"
    <ColumnMapping("Number")> Public Property Number() As String
        Get
            Return _number
        End Get
        Friend Set(ByVal value As String)
            _number = value
        End Set
    End Property
    <ColumnMapping("Type")> Public Property Type() As String
        Get
            Return _type
        End Get
        Set(ByVal value As String)
            _type = value
        End Set
    End Property
    <ColumnMapping("DateReceipt")> Public Property DateReceipt() As Date
        Get
            Return _dateReceipt
        End Get
        Private Set(ByVal value As Date)
            _dateReceipt = value
        End Set
    End Property
    <ColumnMapping("EmployeeId")> Public Property EmployeeId() As Integer
        Get
            Return _employeeId
        End Get
        Private Set(ByVal value As Integer)
            _employeeId = value
        End Set
    End Property
    <ColumnMapping("Value")> Public Property Value() As Decimal
        Get
            Return _value
        End Get
        Set(ByVal value As Decimal)
            _value = value
        End Set
    End Property
    <ColumnMapping("Initials")> Public Property Initials() As String
        Get
            Return _initials
        End Get
        Set(ByVal value As String)
            _initials = value
        End Set
    End Property
    <ColumnMapping("Comments")> Public Property Comments() As String
        Get
            Return _comments
        End Get
        Set(ByVal value As String)
            _comments = value
        End Set
    End Property
    <ColumnMapping("IsProjectSalesOrder")> Public Property IsProjectSalesOrder() As Boolean
        Get
            Return _isProjectSalesOrder
        End Get
        Set(ByVal value As Boolean)
            _isProjectSalesOrder = value
        End Set
    End Property
    <ColumnMapping("RtiStatus")> Public Property RtiStatus() As String
        Get
            Return _rtiStatus
        End Get
        Set(ByVal value As String)
            _rtiStatus = value
        End Set
    End Property

    <ColumnMapping("PoId")> Public Property PoId() As Integer
        Get
            Return _poId
        End Get
        Private Set(ByVal value As Integer)
            _poId = value
        End Set
    End Property
    <ColumnMapping("PoNumber")> Public Property PoNumber() As Nullable(Of Integer)
        Get
            Return _poNumber
        End Get
        Private Set(ByVal value As Nullable(Of Integer))
            _poNumber = value
        End Set
    End Property
    <ColumnMapping("PoConsignNumber")> Public Property PoConsignNumber() As Nullable(Of Integer)
        Get
            Return _poConsignNumber
        End Get
        Private Set(ByVal value As Nullable(Of Integer))
            _poConsignNumber = value
        End Set
    End Property
    <ColumnMapping("PoSupplierNumber")> Public Property PoSupplierNumber() As String
        Get
            Return _poSupplierNumber
        End Get
        Private Set(ByVal value As String)
            _poSupplierNumber = value
        End Set
    End Property
    <ColumnMapping("PoSupplierName")> Public Property PoSupplierName() As String
        Get
            Return _poSupplierName
        End Get
        Private Set(ByVal value As String)
            _poSupplierName = value
        End Set
    End Property
    <ColumnMapping("PoSupplierBbc")> Public Property PoSupplierBbc() As Boolean
        Get
            Return _poSupplierBbc
        End Get
        Private Set(ByVal value As Boolean)
            _poSupplierBbc = value
        End Set
    End Property
    <ColumnMapping("PoSoqNumber")> Public Property PoSoqNumber() As Nullable(Of Integer)
        Get
            Return _poSoqNumber
        End Get
        Private Set(ByVal value As Nullable(Of Integer))
            _poSoqNumber = value
        End Set
    End Property
    <ColumnMapping("PoReleaseNumber")> Public Property PoReleaseNumber() As Integer
        Get
            Return _poReleaseNumber
        End Get
        Private Set(ByVal value As Integer)
            _poReleaseNumber = value
        End Set
    End Property
    <ColumnMapping("PoOrderDate")> Public Property PoOrderDate() As Nullable(Of Date)
        Get
            Return _poOrderDate
        End Get
        Private Set(ByVal value As Nullable(Of Date))
            _poOrderDate = value
        End Set
    End Property

    <ColumnMapping("PoDeliveryNote1")> Public Property PoDeliveryNote1() As String
        Get
            If _poDeliveryNote1 Is Nothing Then _poDeliveryNote1 = String.Empty
            Return _poDeliveryNote1
        End Get
        Set(ByVal value As String)
            _poDeliveryNote1 = value
        End Set
    End Property
    <ColumnMapping("PoDeliveryNote2")> Public Property PoDeliveryNote2() As String
        Get
            If _poDeliveryNote2 Is Nothing Then _poDeliveryNote2 = String.Empty
            Return _poDeliveryNote2
        End Get
        Set(ByVal value As String)
            _poDeliveryNote2 = value
        End Set
    End Property
    <ColumnMapping("PoDeliveryNote3")> Public Property PoDeliveryNote3() As String
        Get
            If _poDeliveryNote3 Is Nothing Then _poDeliveryNote3 = String.Empty
            Return _poDeliveryNote3
        End Get
        Set(ByVal value As String)
            _poDeliveryNote3 = value
        End Set
    End Property
    <ColumnMapping("PoDeliveryNote4")> Public Property PoDeliveryNote4() As String
        Get
            If _poDeliveryNote4 Is Nothing Then _poDeliveryNote4 = String.Empty
            Return _poDeliveryNote4
        End Get
        Set(ByVal value As String)
            _poDeliveryNote4 = value
        End Set
    End Property
    <ColumnMapping("PoDeliveryNote5")> Public Property PoDeliveryNote5() As String
        Get
            If _poDeliveryNote5 Is Nothing Then _poDeliveryNote5 = String.Empty
            Return _poDeliveryNote5
        End Get
        Set(ByVal value As String)
            _poDeliveryNote5 = value
        End Set
    End Property
    <ColumnMapping("PoDeliveryNote6")> Public Property PoDeliveryNote6() As String
        Get
            If _poDeliveryNote6 Is Nothing Then _poDeliveryNote6 = String.Empty
            Return _poDeliveryNote6
        End Get
        Set(ByVal value As String)
            _poDeliveryNote6 = value
        End Set
    End Property
    <ColumnMapping("PoDeliveryNote7")> Public Property PoDeliveryNote7() As String
        Get
            If _poDeliveryNote7 Is Nothing Then _poDeliveryNote7 = String.Empty
            Return _poDeliveryNote7
        End Get
        Set(ByVal value As String)
            _poDeliveryNote7 = value
        End Set
    End Property
    <ColumnMapping("PoDeliveryNote8")> Public Property PoDeliveryNote8() As String
        Get
            If _poDeliveryNote8 Is Nothing Then _poDeliveryNote8 = String.Empty
            Return _poDeliveryNote8
        End Get
        Set(ByVal value As String)
            _poDeliveryNote8 = value
        End Set
    End Property
    <ColumnMapping("PoDeliveryNote9")> Public Property PoDeliveryNote9() As String
        Get
            If _poDeliveryNote9 Is Nothing Then _poDeliveryNote9 = String.Empty
            Return _poDeliveryNote9
        End Get
        Set(ByVal value As String)
            _poDeliveryNote9 = value
        End Set
    End Property
    Public ReadOnly Property PoDeliveryNoteString() As String
        Get
            If _poDeliveryNote1.Trim.Length = 0 Then
                Return String.Empty
            Else
                Dim sb As New StringBuilder(_poDeliveryNote1.Trim)
                If _poDeliveryNote2.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote2.Trim)
                If _poDeliveryNote3.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote3.Trim)
                If _poDeliveryNote4.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote4.Trim)
                If _poDeliveryNote5.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote5.Trim)
                If _poDeliveryNote6.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote6.Trim)
                If _poDeliveryNote7.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote7.Trim)
                If _poDeliveryNote8.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote8.Trim)
                If _poDeliveryNote9.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote9.Trim)
                Return sb.ToString
            End If
        End Get
    End Property

    <ColumnMapping("IbtStoreId")> Public Property IbtStoreId() As String
        Get
            Return _ibtStoreId
        End Get
        Set(ByVal value As String)
            _ibtStoreId = value
        End Set
    End Property
    <ColumnMapping("IbtKeyedIn")> Public Property IbtKeyedIn() As String
        Get
            Return _ibtKeyedIn
        End Get
        Set(ByVal value As String)
            _ibtKeyedIn = value
        End Set
    End Property
    <ColumnMapping("IbtNeedsPrinting")> Public Property IbtNeedsPrinting() As Boolean
        Get
            Return _ibtNeedsPrinting
        End Get
        Set(ByVal value As Boolean)
            _ibtNeedsPrinting = value
        End Set
    End Property
    <ColumnMapping("IbtConsignmentNumber")> Public Property IbtConsignmentNumber() As String
        Get
            Return _ibtConsignmentNumber
        End Get
        Set(ByVal value As String)
            _ibtConsignmentNumber = value
        End Set
    End Property
    <ColumnMapping("ReturnSupplierNumber")> Public Property ReturnSupplierNumber() As String
        Get
            Return _returnSupplierNumber
        End Get
        Set(ByVal value As String)
            _returnSupplierNumber = value
        End Set
    End Property
    <ColumnMapping("ReturnDate")> Public Property ReturnDate() As Nullable(Of Date)
        Get
            Return _returnDate
        End Get
        Set(ByVal value As Nullable(Of Date))
            _returnDate = value
        End Set
    End Property
    <ColumnMapping("ReturnNumber")> Public Property ReturnNumber() As String
        Get
            Return _returnNumber
        End Get
        Set(ByVal value As String)
            _returnNumber = value
        End Set
    End Property

    Public Property Lines() As ReceiptLineCollection
        Get
            If _lines Is Nothing Then LoadLines()
            Return _lines
        End Get
        Private Set(ByVal value As ReceiptLineCollection)
            _lines = value
        End Set
    End Property

#End Region

#Region "Constructors"

    Public Sub New()
        MyBase.New()
    End Sub

    Private Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

    Public Sub New(ByVal order As PurchaseOrder, ByVal userId As Integer)
        MyBase.New()
        _poNumber = order.PoNumber
        _poId = order.Id
        _poConsignNumber = order.ConsignNumber
        _poSupplierNumber = order.SupplierNumber
        _poSupplierName = order.SupplierName
        _poSupplierBbc = order.IsSupplierBbc
        _poOrderDate = order.DateCreated
        _poSoqNumber = order.SoqNumber
        _dateReceipt = Now.Date
        _employeeId = userId
    End Sub

#End Region

    ''' <summary>
    ''' Resets all properties to default values
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ResetValues()
        _number = Nothing
        _type = Nothing
        _dateReceipt = Now.Date
        _employeeId = 0
        _value = 0
        _comments = Nothing
        _isProjectSalesOrder = False
        _rtiStatus = Nothing

        _poId = 0
        _poNumber = Nothing
        _poConsignNumber = Nothing
        _poSupplierNumber = Nothing
        _poSupplierName = Nothing
        _poSupplierBbc = False
        _poSoqNumber = Nothing
        _poReleaseNumber = 0
        _poOrderDate = Nothing
        _poDeliveryNote1 = Nothing
        _poDeliveryNote2 = Nothing
        _poDeliveryNote3 = Nothing
        _poDeliveryNote4 = Nothing
        _poDeliveryNote5 = Nothing
        _poDeliveryNote6 = Nothing
        _poDeliveryNote7 = Nothing
        _poDeliveryNote8 = Nothing
        _poDeliveryNote9 = Nothing

        _ibtStoreId = Nothing
        _ibtKeyedIn = Nothing
        _ibtNeedsPrinting = False
        _ibtConsignmentNumber = Nothing
        _returnSupplierNumber = Nothing
        _returnDate = Nothing
        _returnNumber = Nothing

        _lines = New ReceiptLineCollection
    End Sub

    ''' <summary>
    ''' Loads receipt lines into lines collection of this instance
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadLines()
        _lines = New ReceiptLineCollection
        _lines.LoadLines(_number)
    End Sub

    ''' <summary>
    ''' Persists instance to database returning number of lines updated
    ''' </summary>
    ''' <param name="userId"></param>
    ''' <remarks></remarks>
    Public Function Update(ByVal userId As Integer) As Integer
        _value = Lines.ReceivedValue()
        Return DataOperations.ReceiptMaintain(Me, userId)
    End Function

    ''' <summary>
    ''' Persists instance to update returning number of lines inserted
    ''' </summary>
    ''' <remarks></remarks>
    Public Function Insert() As Integer
        _value = Lines.ReceivedValue()
        Return DataOperations.ReceiptInsert(Me)
    End Function

    ''' <summary>
    ''' Persists instance to database for IBT returning number of rows inserted
    ''' </summary>
    ''' <remarks></remarks>
    Public Function InsertIbt(ByVal includeInSales As Boolean) As Integer
        Return DataOperations.ReceiptIbtInsert(Me, includeInSales)
    End Function



    Public Shared Function GetMaintablePurchaseReceipts() As BindingList(Of Receipt)

        Dim receipts As New BindingList(Of Receipt)

        Dim dt As DataTable = DataOperations.ReceiptGetMaintainableOrders
        For Each dr As DataRow In dt.Rows
            receipts.Add(New Receipt(dr))
        Next

        Return receipts

    End Function

    ''' <summary>
    ''' Returns a new IBT in receipt for a fulfilment order
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CreateIbtInFulfilment(ByVal IbtOutNumber As String) As Receipt

        Dim rec As New Receipt
        rec.Type = "1"
        rec.Value = 0
        rec.DateReceipt = Now.Date
        rec.Initials = "Auto"
        rec.Comments = "Fulfilment Order"
        rec.RtiStatus = RtiStates.ToBeSent
        rec.IbtStoreId = System.Store.GetId.ToString("000")
        If rec.IbtStoreId.Length = 3 Then rec.IbtStoreId = rec.IbtStoreId.Insert(0, "8")
        rec.IbtConsignmentNumber = IbtOutNumber
        rec.Lines = New ReceiptLineCollection
        rec.Lines.Clear()
        Return rec

    End Function

    ''' <summary>
    ''' Returns a new IBT out receipt for a fulfilment order
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CreateIbtOutFulfilment(ByVal storeId As Integer) As Receipt
        If storeId > 8000 Then storeId -= 8000

        Dim rec As New Receipt
        rec.Type = "2"
        rec.Value = 0
        rec.DateReceipt = Now.Date
        rec.Initials = "Auto"
        rec.Comments = "Fulfilment Order"
        rec.RtiStatus = RtiStates.ToBeSent
        rec.IbtStoreId = storeId.ToString("000")
        rec.Lines = New ReceiptLineCollection
        rec.Lines.Clear()
        Return rec

    End Function

End Class
