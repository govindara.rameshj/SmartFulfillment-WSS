﻿Imports System.ComponentModel

Public Class ReceiptLineCollection
    Inherits BindingList(Of ReceiptLine)

    Public Sub New()
        MyBase.New()
    End Sub


    ''' <summary>
    ''' Clears collection and loads receipt lines for given receipt number
    ''' </summary>
    ''' <param name="receiptNumber"></param>
    ''' <remarks></remarks>
    Public Sub LoadLines(ByVal receiptNumber As String)

        Dim dt As DataTable = DataOperations.ReceiptGetLines(receiptNumber)
        Me.Items.Clear()
        For Each dr As DataRow In dt.Rows
            Me.Items.Add(New ReceiptLine(dr))
        Next

    End Sub


    ''' <summary>
    ''' Adds new line to collection, setting receipt number and sequence to next number
    ''' </summary>
    ''' <param name="receiptNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Create(ByVal skuNumber As String, ByVal receiptNumber As String) As ReceiptLine
        Dim line As ReceiptLine = MyBase.AddNew
        line.ReceiptNumber = receiptNumber
        line.Sequence = Me.Items.Count.ToString("0000")
        line.SkuNumber = skuNumber
        line.RtiStatus = RtiStates.ToBeSent
        Return line
    End Function

    ''' <summary>
    ''' Creates new receipt line with given vales and adds to collection
    ''' </summary>
    ''' <param name="skuNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Create(ByVal skuNumber As String) As ReceiptLine
        Dim line As ReceiptLine = MyBase.AddNew
        line.Sequence = Me.Items.Count.ToString("0000")
        line.SkuNumber = skuNumber
        line.RtiStatus = RtiStates.ToBeSent
        Return line
    End Function



    ''' <summary>
    ''' Sets rti status for all lines
    ''' </summary>
    ''' <param name="state"></param>
    ''' <remarks></remarks>
    Public Sub SetRtiState(ByVal state As RtiState)

        Dim rti As String = String.Empty
        Select Case state
            Case Purchasing.RtiState.ToBeSent : rti = RtiStates.ToBeSent
            Case Purchasing.RtiState.Completed : rti = RtiStates.Completed
        End Select

        For Each line As ReceiptLine In Me.Items
            line.RtiStatus = rti
        Next

    End Sub

    ''' <summary>
    ''' Returns whether stock item exits in collection
    ''' </summary>
    ''' <param name="skuNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function StockExists(ByVal skuNumber As String) As Boolean

        For Each line As ReceiptLine In Me.Items
            If line.SkuNumber = skuNumber Then
                Return True
            End If
        Next
        Return False

    End Function

    ''' <summary>
    ''' Returns next sequence number
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function NextSequenceNumber() As String

        Dim sequence As Integer = CInt(Me.Items.Max(Function(f As ReceiptLine) f.Sequence))
        Return (sequence + 1).ToString("0000")

    End Function



    Public Function OrderedQty() As Integer

        Dim qty As Integer = 0
        For Each line As ReceiptLine In Me.Items
            qty += line.OrderQty
        Next

        Return qty

    End Function

    Public Function OrderedValue() As Decimal

        Dim value As Decimal = 0
        For Each line As ReceiptLine In Me.Items
            value += (line.OrderQty * line.OrderPrice)
        Next

        Return value

    End Function

    Public Function ReceivedQty() As Integer

        Dim qty As Integer = 0
        For Each line As ReceiptLine In Me.Items
            qty += line.ReceivedQty
        Next

        Return qty

    End Function

    Public Function ReceivedValue() As Decimal

        Dim value As Decimal = 0
        For Each line As ReceiptLine In Me.Items
            value += (line.ReceivedQty * line.ReceivedPrice)
        Next

        Return value

    End Function

    Public Function IbtQty() As Integer

        Dim qty As Integer = 0
        For Each line As ReceiptLine In Me.Items
            qty += line.IbtQty
        Next
        Return qty

    End Function

    Public Function IbtValue() As Decimal

        Dim value As Decimal = 0
        For Each line As ReceiptLine In Me.Items
            value += (line.IbtQty * line.ReceivedPrice)
        Next
        Return value

    End Function


    ''' <summary>
    ''' Calculates receipt order qtys and values
    ''' </summary>
    ''' <param name="orderqty"></param>
    ''' <param name="ordervalue"></param>
    ''' <remarks></remarks>
    Public Sub Totals(ByRef orderQty As Integer, ByRef orderValue As Decimal)

        orderQty = 0
        orderValue = 0

        For Each line As ReceiptLine In Me.Items
            orderQty += line.OrderQty
            orderValue += (line.OrderQty * line.OrderPrice)
        Next

    End Sub

    ''' <summary>
    ''' Calculates receipt order/received qtys and values
    ''' </summary>
    ''' <param name="orderQty"></param>
    ''' <param name="ordervalue"></param>
    ''' <remarks></remarks>
    Public Sub Totals(ByRef orderQty As Integer, ByRef orderValue As Decimal, ByRef receivedQty As Integer, ByRef receivedValue As Decimal)

        orderQty = 0
        orderValue = 0
        receivedQty = 0
        receivedValue = 0

        For Each line As ReceiptLine In Me.Items
            orderQty += line.OrderQty
            orderValue += (line.OrderQty * line.OrderPrice)
            receivedQty += line.ReceivedQty
            receivedValue += (line.ReceivedQty * line.ReceivedPrice)
        Next

    End Sub

End Class