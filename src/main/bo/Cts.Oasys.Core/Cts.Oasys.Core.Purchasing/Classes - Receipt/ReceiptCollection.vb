﻿Imports System.ComponentModel

Public Class ReceiptCollection
    Inherits BindingList(Of Receipt)

    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' Persists all receipts in collection to dtabase returning number of lines inserted
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function InsertIbt(ByVal includeInSales As Boolean) As Integer
        Return DataOperations.ReceiptIbtInsert(Me, includeInSales)
    End Function

End Class