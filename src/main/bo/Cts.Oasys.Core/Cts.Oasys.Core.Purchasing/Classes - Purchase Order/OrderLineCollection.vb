﻿Imports System.ComponentModel

<CLSCompliant(True)> Public Class OrderLineCollection
    Inherits BindingList(Of OrderLine)
    Private _order As Order

    Friend Sub New(ByRef order As Order)
        MyBase.New()
        _order = order
        LoadLines()
    End Sub

    Private Sub LoadLines()

        Dim dt As DataTable = DataOperations.OrderGetLines(_order.Id)
        For Each dr As DataRow In dt.Rows
            Me.Items.Add(New OrderLine(dr))
        Next

    End Sub


    ''' <summary>
    ''' Returns whether stock item exits in collection
    ''' </summary>
    ''' <param name="skuNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function StockExists(ByVal skuNumber As String) As Boolean

        For Each line As OrderLine In Me.Items
            If line.SkuNumber = skuNumber Then
                Return True
            End If
        Next
        Return False

    End Function

    ''' <summary>
    ''' Calculates receipt order qtys and values
    ''' </summary>
    ''' <param name="orderqty"></param>
    ''' <param name="ordervalue"></param>
    ''' <remarks></remarks>
    Public Sub Totals(ByRef orderQty As Integer, ByRef orderValue As Decimal)

        orderQty = 0
        orderValue = 0

        For Each line As OrderLine In Me.Items
            orderQty += line.OrderQty
            orderValue += (line.OrderQty * line.Price)
        Next

    End Sub

    ''' <summary>
    ''' Returns arraylist of any stock item sku numbers that do not exist in database.  Returns nothing if all exist
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function StockItemsNotExisting() As ArrayList

        Dim skuNumbers As New ArrayList
        For Each line As OrderLine In Me.Items
            skuNumbers.Add(line.SkuNumber)
        Next

        If skuNumbers.Count > 0 Then
            Dim skusNotOnFile As ArrayList = Stock.Stock.GetStockItemsNotExist(skuNumbers)
            If skusNotOnFile.Count > 0 Then Return skusNotOnFile
        End If
        Return Nothing

    End Function

    Public Function OrderedQty() As Integer

        Dim qty As Integer = 0
        For Each line As OrderLine In Me.Items
            qty += line.OrderQty
        Next

        Return qty

    End Function

    Public Function OrderedValue() As Decimal

        Dim value As Decimal = 0
        For Each line As OrderLine In Me.Items
            value += (line.OrderQty * line.Price)
        Next

        Return value

    End Function

End Class