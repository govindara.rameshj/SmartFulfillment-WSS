﻿Imports System.Data.SqlClient
Imports System.Text
Imports Cts.Oasys.Core

<CLSCompliant(True)> Public Class ContainerLine
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _assemblyDepotNumber As String
    Private _containerNumber As String
    Private _storePoNumber As String
    Private _storePoLineNumber As String
    Private _skuNumber As String
    Private _skuDescription As String
    Private _skuProductCode As String
    Private _skuPackSize As Integer
    Private _qty As Integer
    Private _price As Decimal
    Private _rejectedReason As String
#End Region

#Region "Properties"
    <ColumnMapping("AssemblyDepotNumber")> Public Property AssemblyDepotNumber() As String
        Get
            Return _assemblyDepotNumber
        End Get
        Private Set(ByVal value As String)
            _assemblyDepotNumber = value
        End Set
    End Property
    <ColumnMapping("Number")> Public Property Number() As String
        Get
            Return _containerNumber
        End Get
        Private Set(ByVal value As String)
            _containerNumber = value
        End Set
    End Property
    <ColumnMapping("StorePoNumber")> Public Property StorePoNumber() As String
        Get
            Return _storePoNumber
        End Get
        Set(ByVal value As String)
            _storePoNumber = value
        End Set
    End Property
    <ColumnMapping("StorePoLineNumber")> Public Property StorePoLineNumber() As String
        Get
            Return _storePoLineNumber
        End Get
        Private Set(ByVal value As String)
            _storePoLineNumber = value
        End Set
    End Property
    <ColumnMapping("SkuNumber")> Public Property SkuNumber() As String
        Get
            Return _skuNumber
        End Get
        Private Set(ByVal value As String)
            _skuNumber = value
        End Set
    End Property
    <ColumnMapping("SkuDescription")> Public Property SkuDescription() As String
        Get
            Return _skuDescription
        End Get
        Private Set(ByVal value As String)
            _skuDescription = value
        End Set
    End Property
    <ColumnMapping("SkuProductCode")> Public Property SkuProductCode() As String
        Get
            Return _skuProductCode
        End Get
        Private Set(ByVal value As String)
            _skuProductCode = value.Trim
        End Set
    End Property
    <ColumnMapping("SkuPackSize")> Public Property SkuPackSize() As Integer
        Get
            Return _skuPackSize
        End Get
        Private Set(ByVal value As Integer)
            _skuPackSize = value
        End Set
    End Property
    <ColumnMapping("Qty")> Public Property Qty() As Integer
        Get
            Return _qty
        End Get
        Private Set(ByVal value As Integer)
            _qty = value
        End Set
    End Property
    <ColumnMapping("Price")> Public Property Price() As Decimal
        Get
            Return _price
        End Get
        Private Set(ByVal value As Decimal)
            _price = value
        End Set
    End Property
    <ColumnMapping("RejectedReason")> Public Property RejectedReason() As String
        Get
            Return _rejectedReason
        End Get
        Private Set(ByVal value As String)
            _rejectedReason = value
        End Set
    End Property
#End Region

#Region "Constructors"
    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub
#End Region


End Class
