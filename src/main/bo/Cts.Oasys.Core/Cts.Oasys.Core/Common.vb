﻿Imports System.Linq.Expressions
Imports System.Text
Imports Cts.Oasys.Core.Net4Replacements

<HideModuleName()> Public Module Common

    Private _configXMLDoc As Lazy(Of ConfigXMLDocument) = New Lazy(Of ConfigXMLDocument)(Function() New ConfigXMLDocument())

    Public ReadOnly Property ConfigXMLDoc As ConfigXMLDocument
        Get
            Return _configXMLDoc.Value
        End Get
    End Property

    Public Class ConfigXMLDocument
        Inherits Xml.XmlDocument

        Public Sub New()
            MyBase.New()
            Dim resolver As New ConfigPathResolver
            Dim path = resolver.ResolveConfigXml()
            Me.Load(path)
        End Sub

    End Class

    Public Enum BaseClassState
        Unchanged = 0
        Modified
        Added
        Deleted
    End Enum

    ''' <summary>
    ''' Gets name of property, parameter syntax as (function (f as 'classname') f.'propertyname')
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <typeparam name="R"></typeparam>
    ''' <param name="expression"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPropertyName(Of T, R)(ByVal expression As Expression(Of Func(Of T, R))) As String
        Dim memberExpression As MemberExpression = DirectCast(expression.Body, MemberExpression)
        Return (memberExpression.Member.Name)
    End Function

    ''' <summary>
    ''' Concetenates given array of strings into comma separated string
    ''' </summary>
    ''' <param name="strings"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Concetenate(ByVal strings As String()) As String

        Dim sb As New StringBuilder
        Dim first As Boolean = True

        For Each s As String In strings
            If s IsNot Nothing AndAlso s.Trim.Length > 0 Then
                If Not first Then sb.Append(", ")
                sb.Append(s.Trim)
                first = False
            End If
        Next

        Return sb.ToString

    End Function

    ''' <summary>
    ''' Concetenates given array of strings using new lines
    ''' </summary>
    ''' <param name="strings"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ConcetenateNewLines(ByVal strings As String()) As String

        Dim sb As New StringBuilder
        Dim first As Boolean = True

        For Each s As String In strings
            If s IsNot Nothing AndAlso s.Trim.Length > 0 Then
                If Not first Then sb.Append(Environment.NewLine)
                sb.Append(s.Trim)
                first = False
            End If
        Next

        Return sb.ToString

    End Function

    ''' <summary>
    ''' Returns string representation of input string with spaces inserted before any uppercase characters
    ''' </summary>
    ''' <param name="input"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CapitalSpaceInsert(ByVal input As String) As String

        Dim sb As New StringBuilder

        If input IsNot Nothing AndAlso input.Length > 0 Then
            For index As Integer = 0 To input.Length - 1
                If index > 1 Then
                    If Char.IsUpper(input.Chars(index)) Then sb.Append(Space(1))
                End If
                sb.Append(input.Chars(index))
            Next
        End If

        Return sb.ToString

    End Function

    Public Function StringIsSomething(ByVal testString As String) As Boolean
        Return (testString IsNot Nothing AndAlso testString.Length > 0)
    End Function

    Public Function NullableToString(Of T As Structure)(ByVal value As Nullable(Of T), ByVal defaultValue As String) As String
        If (value Is Nothing) Then
            Return defaultValue
        Else
            Return value.Value.ToString
        End If

    End Function

End Module

Namespace Rti

    Public Enum State
        None = 0
        Pending
        Sending
        Completed
    End Enum

    Public Structure Description
        Private _value As String
        Public Shared None As String = ""
        Public Shared Pending As String = "N"
        Public Shared Sending As String = "S"
        Public Shared Completed As String = "C"

        Public Shared Function GetDescription(ByVal rtiState As State) As String
            Select Case rtiState
                Case State.None : Return Description.None
                Case State.Pending : Return Description.Pending
                Case State.Sending : Return Description.Sending
                Case State.Completed : Return Description.Completed
                Case Else : Return String.Empty
            End Select
        End Function

    End Structure

End Namespace