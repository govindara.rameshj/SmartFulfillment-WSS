﻿Namespace Rti

    Public Enum State
        None = 0
        Pending
        Sending
        Completed
    End Enum

    Public Structure Description
        Private _value As String
        Public Shared None As String = ""
        Public Shared Pending As String = "N"
        Public Shared Sending As String = "S"
        Public Shared Completed As String = "C"

        Public Shared Function Retrieve(ByVal rtiState As State) As String
            Select Case rtiState
                Case State.None : Return Description.None
                Case State.Pending : Return Description.Pending
                Case State.Sending : Return Description.Sending
                Case State.Completed : Return Description.Completed
                Case Else : Return String.Empty
            End Select
        End Function

    End Structure

End Namespace