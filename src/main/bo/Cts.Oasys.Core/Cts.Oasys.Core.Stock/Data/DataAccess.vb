﻿Imports Cts.Oasys.Data
Imports System.Text

Namespace DataAccess

    <HideModuleName()> Friend Module DataStock

        Friend Function GetStocksBySupplier(ByVal supplierNumber As String) As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.StockGetStocksBySupplier)
                    com.AddParameter(My.Resources.Parameters.SupplierNumber, supplierNumber)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

        Friend Function GetStocksByNumberEan(ByVal skuNumber As String, ByVal eanNumber As String) As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.StockGetStocksByNumberEan)
                    com.AddParameter(My.Resources.Parameters.SkuNumber, skuNumber)
                    com.AddParameter(My.Resources.Parameters.EanNumber, eanNumber)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

        Friend Function GetStock(ByVal skuNumber As String) As DataTable

            Using con As New Connection
                Using com As New Command(con)
                    Select Case con.DataProvider
                        Case DataProvider.Odbc
                            Dim sb As New StringBuilder
                            sb.Append("Select ")
                            sb.Append("st.skun	as SkuNumber,")
                            sb.Append("st.descr	as Description,")
                            sb.Append("st.prod	as ProductCode,")
                            sb.Append("st.pack	as PackSize,")
                            sb.Append("st.pric	as Price,")
                            sb.Append("st.COST	as Cost,")
                            sb.Append("st.WGHT	as Weight,")
                            sb.Append("st.onha	as OnHandQty,")
                            sb.Append("st.onor  as OnOrderQty,")
                            sb.Append("st.mini  as MinimumQty,")
                            sb.Append("st.maxi  as MaximumQty,")
                            sb.Append("st.retq  as OpenReturnsQty,")
                            sb.Append("st.mdnq  as MarkdownQty,")
                            sb.Append("st.wtfq  as WriteOffQty,")
                            sb.Append("st.INON  as IsNonStock,")
                            sb.Append("st.IOBS  as IsObsolete,")
                            sb.Append("st.IDEL  as IsDeleted,")
                            sb.Append("st.IRIS  as IsItemSingle,")
                            sb.Append("st.NOOR  as IsNonOrder,")
                            sb.Append("st.treq  as ReceivedTodayQty,")
                            sb.Append("st.trev  as ReceivedTodayValue,")
                            sb.Append("st.dats  as DateFirstStocked,")
                            sb.Append("st.IODT  as DateFirstOrder,")
                            sb.Append("st.FODT  as DateFinalOrder,")
                            sb.Append("st.drec  as DateLastReceived,")
                            sb.Append("st.CTGY  as HieCategory,")
                            sb.Append("st.GRUP  as HieGroup,")
                            sb.Append("st.SGRP  as HieSubgroup,")
                            sb.Append("st.STYL  as HieStyle,")
                            sb.Append("st.tact  as ActivityToday,")
                            sb.Append("st.us001 as UnitSales1,")
                            sb.Append("st.us002 as UnitSales2,")
                            sb.Append("st.us003 as UnitSales3,")
                            sb.Append("st.us004 as UnitSales4,")
                            sb.Append("st.us005 as UnitSales5,")
                            sb.Append("st.us006 as UnitSales6,")
                            sb.Append("st.us007 as UnitSales7,")
                            sb.Append("st.us008 as UnitSales8,")
                            sb.Append("st.us009 as UnitSales9,")
                            sb.Append("st.us0010    as UnitSales10,")
                            sb.Append("st.us0011    as UnitSales11,")
                            sb.Append("st.us0012    as UnitSales12,")
                            sb.Append("st.us0013    as UnitSales13,")
                            sb.Append("st.us0014    as UnitSales14,")
                            sb.Append("st.do001 as DaysOutStock1,")
                            sb.Append("st.do002 as DaysOutStock2,")
                            sb.Append("st.do003 as DaysOutStock3,")
                            sb.Append("st.do004 as DaysOutStock4,")
                            sb.Append("st.do005 as DaysOutStock5,")
                            sb.Append("st.do006 as DaysOutStock6,")
                            sb.Append("st.do007 as DaysOutStock7,")
                            sb.Append("st.do008 as DaysOutStock8,")
                            sb.Append("st.do009 as DaysOutStock9,")
                            sb.Append("st.do0010    as DaysOutStock10,")
                            sb.Append("st.do0011    as DaysOutStock11,")
                            sb.Append("st.do0012    as DaysOutStock12,")
                            sb.Append("st.do0013    as DaysOutStock13,")
                            sb.Append("st.do0014    as DaysOutStock14 ")
                            sb.Append("from stkmas st where st.skun = ?")

                            com.CommandText = sb.ToString
                            com.AddParameter("SKUN", skuNumber)
                            Return com.ExecuteDataTable

                        Case DataProvider.Sql
                            com.StoredProcedureName = My.Resources.Procedures.StockGetStock
                            com.AddParameter(My.Resources.Parameters.SkuNumber, skuNumber)
                            Return com.ExecuteDataTable
                    End Select
                End Using
            End Using
            Return Nothing

        End Function

        Friend Function GetStocks(ByVal skuNumbers As ArrayList) As DataTable

            Dim sb As New StringBuilder
            For Each skuNumber As String In skuNumbers
                If sb.Length > 0 Then sb.Append(",")
                sb.Append(skuNumber)
            Next

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.StockGetStocks)
                    com.AddParameter(My.Resources.Parameters.SkuNumbers, sb.ToString)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

        Friend Function GetStockEans(ByVal skuNumber As String) As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.StockGetStockEans)
                    com.AddParameter(My.Resources.Parameters.SkuNumber, skuNumber)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

    End Module

End Namespace

