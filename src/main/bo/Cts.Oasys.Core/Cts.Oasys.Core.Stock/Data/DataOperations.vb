﻿Imports Cts.Oasys.Data
Imports System.Text

Friend Module DataOperations

    Friend Function GetStocksBySupplier(ByVal supplierNumber As String) As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.StockGetStocksBySupplier)
                com.AddParameter(My.Resources.Parameters.SupplierNumber, supplierNumber)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Function GetStocksByNumberEan(ByVal skuNumber As String, ByVal eanNumber As String) As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.StockGetStocksByNumberEan)
                com.AddParameter(My.Resources.Parameters.SkuNumber, skuNumber)
                com.AddParameter(My.Resources.Parameters.EanNumber, eanNumber)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Function GetStock(ByVal skuNumber As String) As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.StockGetStock)
                com.AddParameter(My.Resources.Parameters.SkuNumber, skuNumber)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Function GetStocks(ByVal skuNumbers As ArrayList) As DataTable

        Dim sb As New StringBuilder
        For Each skuNumber As String In skuNumbers
            If sb.Length > 0 Then sb.Append(",")
            sb.Append(skuNumber)
        Next

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.StockGetStocks)
                com.AddParameter(My.Resources.Parameters.SkuNumbers, sb.ToString)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

    Friend Function GetStockEans(ByVal skuNumber As String) As DataTable

        Using con As New Connection
            Using com As New Command(con, My.Resources.Procedures.StockGetStockEans)
                com.AddParameter(My.Resources.Parameters.SkuNumber, skuNumber)
                Return com.ExecuteDataTable
            End Using
        End Using

    End Function

End Module
