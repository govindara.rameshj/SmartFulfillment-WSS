﻿Imports System.ComponentModel

<CLSCompliant(True)> Public Class StockCollection
    Inherits BindingList(Of Stock)

    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal supplierNumber As String)

        Dim dt As DataTable = DataOperations.GetStocksBySupplier(supplierNumber)
        For Each dr As DataRow In dt.Rows
            Me.Items.Add(New Stock(dr))
        Next

    End Sub

    Friend Sub New(ByVal skuNumber As String, ByVal eanNumber As String)

        Dim dt As DataTable = DataOperations.GetStocksByNumberEan(skuNumber, eanNumber)
        For Each dr As DataRow In dt.Rows
            Me.Items.Add(New Stock(dr))
        Next

    End Sub



    ''' <summary>
    ''' Calculates order value, units and weight currently on order
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="units"></param>
    ''' <param name="weight"></param>
    ''' <remarks></remarks>
    Public Sub TotalsOrder(ByRef value As Decimal, ByRef units As Integer, ByRef weight As Decimal)

        'reset values
        value = 0
        units = 0
        weight = 0

        'get initial values
        For Each stockItem As Stock In Me.Items
            value += (stockItem.OrderQty * stockItem.Price)
            units += (stockItem.OrderQty)
            weight += (stockItem.OrderQty * stockItem.Weight)
        Next

    End Sub

    ''' <summary>
    ''' Calculates order value, units, weight and cartons currently on order
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="units"></param>
    ''' <param name="weight"></param>
    ''' <param name="cartons"></param>
    ''' <remarks></remarks>
    Public Sub TotalsOrder(ByRef value As Decimal, ByRef units As Integer, ByRef weight As Decimal, ByRef cartons As Integer)

        'reset values
        value = 0
        units = 0
        weight = 0
        cartons = 0

        'get initial values
        For Each stockItem As Stock In Me.Items
            value += (stockItem.OrderQty * stockItem.Price)
            units += (stockItem.OrderQty)
            weight += (stockItem.OrderQty * stockItem.Weight)
            cartons += (stockItem.OrderQty * stockItem.PackSize)
        Next

    End Sub

    ''' <summary>
    ''' Calculates units and value of all stock items where stock on hand is greater than capacity
    ''' </summary>
    ''' <param name="units"></param>
    ''' <param name="value"></param>
    ''' <remarks></remarks>
    Public Sub TotalsOverShelfCapacity(ByRef units As Integer, ByRef value As Decimal)

        'reset values
        units = 0
        value = 0

        For Each stockItem As Stock In Me.Items
            Dim over As Integer = stockItem.OnHandQty - stockItem.Capacity
            If over > 0 Then
                units += over
                value += (over * stockItem.Price)
            End If
        Next
    End Sub

    ''' <summary>
    ''' Calculates count and value of all stock items where stock on hand is greater than capacity
    '''  taking on order qty into account
    ''' </summary>
    ''' <param name="count"></param>
    ''' <param name="value"></param>
    ''' <param name="soqUnits"></param>
    ''' <param name="soqValue"></param>
    ''' <remarks></remarks>
    Public Sub TotalsOverShelfCapacity(ByRef count As Integer, ByRef value As Decimal, ByRef soqUnits As Integer, ByRef soqValue As Decimal)

        For Each stockItem As Stock In Me.Items
            Dim over As Integer = stockItem.OnOrderQty + stockItem.OnHandQty - stockItem.Capacity
            If over > 0 Then
                count += 1
                value += (over * stockItem.Price)
            End If

            If stockItem.OrderLevel > stockItem.Capacity Then
                soqUnits += (stockItem.OrderLevel - stockItem.Capacity)
                soqValue += ((stockItem.OrderLevel - stockItem.Capacity) * stockItem.Price)
            End If
        Next

    End Sub

    ''' <summary>
    ''' Sets order qty to soq qty for all stock items
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub OrderQtySetSoq()

        For Each stockItem As Stock In Me.Items
            stockItem.OrderQty = stockItem.OrderLevel
        Next

    End Sub

    ''' <summary>
    ''' Sets order qty to zero for all stock items
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub OrderQtySetZero()

        For Each stockItem As Stock In Me.Items
            stockItem.OrderQty = 0
        Next

    End Sub

    ''' <summary>
    ''' Returns whether all order qtys are zero
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function AllZeroOrderQty() As Boolean

        For Each stockItem As Stock In Me.Items
            If stockItem.OrderQty <> 0 Then
                Return False
            End If
        Next
        Return True

    End Function

    ''' <summary>
    ''' Returns whether all have zero period demand
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function AllZeroPeriodDemand() As Boolean

        For Each stockItem As Stock In Me.Items
            If stockItem.PeriodDemand <> 0 Then
                Return False
            End If
        Next
        Return True

    End Function

    ''' <summary>
    ''' Returns whether stock item exists in collection given stock sku number
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function StockExists(ByVal skuNumber) As Boolean

        For Each stockItem As Stock In Me.Items
            If stockItem.SkuNumber = skuNumber Then
                Return True
            End If
        Next
        Return False

    End Function

    ''' <summary>
    ''' Returns stock item from collection. Returns nothing if not in collection
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Stock(ByVal skuNumber) As Stock

        For Each stockItem As Stock In Me.Items
            If stockItem.SkuNumber = skuNumber Then
                Return stockItem
            End If
        Next
        Return Nothing

    End Function

End Class