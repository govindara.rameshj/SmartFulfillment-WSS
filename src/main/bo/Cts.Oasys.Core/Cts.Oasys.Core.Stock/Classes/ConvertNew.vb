﻿Namespace TpWickes

    Public Class ConvertNew
        Implements IConvert

        Public Function NullToEmptyString(ByVal Value As String) As String Implements IConvert.NullToEmptyString

            If Value Is Nothing Then

                Return String.Empty

            Else

                Return Value

            End If

        End Function

    End Class

End Namespace