﻿Imports Cts.Oasys.Core

<CLSCompliant(True)> Public Class StockEan
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _number As String
    Private _skuNumber As String
#End Region

#Region "Properties"
    <ColumnMapping("Number")> Public Property Number() As String
        Get
            Return _number
        End Get
        Private Set(ByVal value As String)
            _number = value
        End Set
    End Property
    <ColumnMapping("SkuNumber")> Public Property SkuNumber() As String
        Get
            Return _skuNumber
        End Get
        Private Set(ByVal value As String)
            _skuNumber = value
        End Set
    End Property
#End Region

#Region "Constructors"
    Public Sub New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub
#End Region

End Class
