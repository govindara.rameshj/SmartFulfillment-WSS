﻿<HideModuleName()> Public Module Common

    Public Structure SaleTypes
        Private _value As String
        Public Shared ReadOnly Basic As String = "B"
        Public Shared ReadOnly Performance As String = "P"
        Public Shared ReadOnly Aesthetic As String = "A"
        Public Shared ReadOnly Showroom As String = "S"
        Public Shared ReadOnly GoodPallet As String = "G"
        Public Shared ReadOnly ReducedPallet As String = "R"
        Public Shared ReadOnly Delivery As String = "D"
        Public Shared ReadOnly Installation As String = "I"
        Public Shared ReadOnly Other As String = "O"
    End Structure

    Public Structure DemandPatterns
        Private _value As String
        Public Shared ReadOnly None As String = ""
        Public Shared ReadOnly Obsolete As String = "Obsolete"
        Public Shared ReadOnly Superceded As String = "Superceded"
        Public Shared ReadOnly [New] As String = "New"
        Public Shared ReadOnly Normal As String = "Normal"
        Public Shared ReadOnly Erratic As String = "Erratic"
        Public Shared ReadOnly Fast As String = "Fast"
        Public Shared ReadOnly Slow As String = "Slow"
        Public Shared ReadOnly Lumpy As String = "Lumpy"
        Public Shared ReadOnly TrendUp As String = "TrendUp"
        Public Shared ReadOnly TrendDown As String = "TrendDown"
    End Structure

End Module
