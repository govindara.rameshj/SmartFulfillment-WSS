﻿Namespace TpWickes

    Public Interface IRequirementRepository

        Function RequirementEnabled(ByVal RequirementID As Integer) As Nullable(Of Boolean)

    End Interface

End Namespace