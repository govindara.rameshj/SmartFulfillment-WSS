﻿Public Interface IReportColumn
    Inherits IReportColumnLive

    Property Fontsize() As Single
    Property IsHighlight() As Boolean
    Property HighlightColour() As Integer
End Interface
