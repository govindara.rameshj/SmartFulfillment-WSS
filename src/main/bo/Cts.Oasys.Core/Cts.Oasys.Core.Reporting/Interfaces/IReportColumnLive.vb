﻿Public Interface IReportColumnLive

    Property Id() As Integer
    Property Name() As String
    Property Caption() As String
    Property FormatType() As Integer
    Property Format() As String
    Property IsVisible() As Boolean
    Property IsBold() As Boolean
    Property IsImagePath() As Boolean
    Property MinWidth() As Integer
    Property MaxWidth() As Integer
    Property Alignment() As Integer
End Interface
