﻿Public Class ReportColumnFactory
    Inherits RequirementSwitchFactory(Of IReportColumnLive)

    Friend Const REQUIREMENTSWITCHP022_002 As Integer = -22002

    Public Overrides Function ImplementationA() As IReportColumnLive

        Return New ReportColumn
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(REQUIREMENTSWITCHP022_002)
    End Function

    Public Overrides Function ImplementationB() As IReportColumnLive

        Return New ReportColumnLive
    End Function
End Class
