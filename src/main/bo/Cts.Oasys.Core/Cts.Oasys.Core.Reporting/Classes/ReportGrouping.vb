﻿Public Class ReportGrouping
    Inherits Base

#Region "Private Variables"
    Private _reportId As Integer
    Private _tableId As Integer
    Private _id As Integer
    Private _name As String
    Private _isDescending As Boolean
    Private _summaryType As Integer
    Private _showExpanded As Boolean
#End Region

#Region "Properties"
    <ColumnMapping("ReportId")> Public Property ReportId() As Integer
        Get
            Return _reportId
        End Get
        Private Set(ByVal value As Integer)
            _reportId = value
        End Set
    End Property
    <ColumnMapping("TableId")> Public Property TableId() As Integer
        Get
            Return _tableId
        End Get
        Private Set(ByVal value As Integer)
            _tableId = value
        End Set
    End Property
    <ColumnMapping("Id")> Public Property Id() As Integer
        Get
            Return _id
        End Get
        Private Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    <ColumnMapping("Name")> Public Property Name() As String
        Get
            Return _name
        End Get
        Private Set(ByVal value As String)
            _name = value
        End Set
    End Property
    <ColumnMapping("IsDescending")> Public Property IsDescending() As Boolean
        Get
            Return _isDescending
        End Get
        Private Set(ByVal value As Boolean)
            _isDescending = value
        End Set
    End Property
    <ColumnMapping("SummaryType")> Public Property SummaryType() As Integer
        Get
            Return _summaryType
        End Get
        Private Set(ByVal value As Integer)
            _summaryType = value
        End Set
    End Property
    <ColumnMapping("ShowExpanded")> Public Property ShowExpanded() As Boolean
        Get
            Return _showExpanded
        End Get
        Private Set(ByVal value As Boolean)
            _showExpanded = value
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

End Class

Public Class ReportGroupingCollection
    Inherits BaseCollection(Of ReportGrouping)

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub LoadGrouping(ByVal reportId As Integer, ByVal tableId As Integer)
        Dim dt As DataTable = DataAccess.GetReportGrouping(reportId, tableId)
        Me.Load(dt)
    End Sub

End Class