﻿Public Class ReportColumn
    Inherits ReportColumnLive
    Implements IReportColumn

#Region "Private Variables"
    Private _fontsize As Integer
    Private _isHighlight As Boolean
    Private _highlightColour As Integer
#End Region

#Region "Properties"

    <ColumnMapping("Fontsize")> _
    Public Property Fontsize() As Single Implements IReportColumn.Fontsize
        Get
            Return _fontsize
        End Get
        Friend Set(ByVal value As Single)
            _fontsize = CInt(value)
        End Set
    End Property

    <ColumnMapping("IsHighlight")> _
    Public Property IsHighlight() As Boolean Implements IReportColumn.IsHighlight
        Get
            Return _isHighlight
        End Get
        Friend Set(ByVal value As Boolean)
            _isHighlight = value
        End Set
    End Property

    <ColumnMapping("HighlightColour")> _
    Public Property HighlightColour() As Integer Implements IReportColumn.HighlightColour
        Get
            Return _highlightColour
        End Get
        Friend Set(ByVal value As Integer)
            _highlightColour = value
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub
End Class
