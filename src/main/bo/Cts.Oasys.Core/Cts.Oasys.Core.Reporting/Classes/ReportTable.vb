﻿Public Class ReportTable
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _reportId As Integer
    Private _id As Integer
    Private _name As String
    Private _allowGrouping As Boolean
    Private _allowSelection As Boolean
    Private _showHeaders As Boolean
    Private _showIndicator As Boolean
    Private _showLinesVertical As Boolean
    Private _showLinesHorizontal As Boolean
    Private _showBorder As Boolean
    Private _showInPrintOnly As Boolean
    Private _autoFitColumns As Boolean
    Private _headerHeight As Integer
    Private _rowHeight As Integer
    Private _anchorTop As Boolean
    Private _anchorBottom As Boolean

    Private _columns As ReportColumnCollection = Nothing
    Private _groupings As ReportGroupingCollection = Nothing
    Private _summaries As ReportSummaryCollection = Nothing
    Private _hyperlinks As ReportHyperlinkCollection = Nothing
#End Region

#Region "Properties"
    <ColumnMapping("ReportId")> Public Property ReportId() As Integer
        Get
            Return _reportId
        End Get
        Private Set(ByVal value As Integer)
            _reportId = value
        End Set
    End Property
    <ColumnMapping("Id")> Public Property Id() As Integer
        Get
            Return _id
        End Get
        Private Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    <ColumnMapping("Name")> Public Property Name() As String
        Get
            Return _name
        End Get
        Private Set(ByVal value As String)
            _name = value
        End Set
    End Property
    <ColumnMapping("AllowGrouping")> Public Property AllowGrouping() As Boolean
        Get
            Return _allowGrouping
        End Get
        Private Set(ByVal value As Boolean)
            _allowGrouping = value
        End Set
    End Property
    <ColumnMapping("AllowSelection")> Public Property AllowSelection() As Boolean
        Get
            Return _allowSelection
        End Get
        Private Set(ByVal value As Boolean)
            _allowSelection = value
        End Set
    End Property
    <ColumnMapping("ShowHeaders")> Public Property ShowHeaders() As Boolean
        Get
            Return _showHeaders
        End Get
        Private Set(ByVal value As Boolean)
            _showHeaders = value
        End Set
    End Property
    <ColumnMapping("ShowIndicator")> Public Property ShowIndicator() As Boolean
        Get
            Return _showIndicator
        End Get
        Private Set(ByVal value As Boolean)
            _showIndicator = value
        End Set
    End Property
    <ColumnMapping("ShowLinesVertical")> Public Property ShowLinesVertical() As Boolean
        Get
            Return _showLinesVertical
        End Get
        Private Set(ByVal value As Boolean)
            _showLinesVertical = value
        End Set
    End Property
    <ColumnMapping("ShowLinesHorizontal")> Public Property ShowLinesHorizontal() As Boolean
        Get
            Return _showLinesHorizontal
        End Get
        Private Set(ByVal value As Boolean)
            _showLinesHorizontal = value
        End Set
    End Property
    <ColumnMapping("ShowBorder")> Public Property ShowBorder() As Boolean
        Get
            Return _showBorder
        End Get
        Private Set(ByVal value As Boolean)
            _showBorder = value
        End Set
    End Property
    <ColumnMapping("ShowInPrintOnly")> Public Property ShowInPrintOnly() As Boolean
        Get
            Return _showInPrintOnly
        End Get
        Private Set(ByVal value As Boolean)
            _showInPrintOnly = value
        End Set
    End Property
    <ColumnMapping("AutoFitColumns")> Public Property AutoFitColumns() As Boolean
        Get
            Return _autoFitColumns
        End Get
        Private Set(ByVal value As Boolean)
            _autoFitColumns = value
        End Set
    End Property
    <ColumnMapping("HeaderHeight")> Public Property HeaderHeight() As Integer
        Get
            Return _headerHeight
        End Get
        Private Set(ByVal value As Integer)
            _headerHeight = value
        End Set
    End Property
    <ColumnMapping("AnchorTop")> Public Property AnchorTop() As Boolean
        Get
            Return _anchorTop
        End Get
        Private Set(ByVal value As Boolean)
            _anchorTop = value
        End Set
    End Property
    <ColumnMapping("AnchorBottom")> Public Property AnchorBottom() As Boolean
        Get
            Return _anchorBottom
        End Get
        Private Set(ByVal value As Boolean)
            _anchorBottom = value
        End Set
    End Property

    ''' <summary>
    ''' If value is -1 then set row height to grid height
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <ColumnMapping("RowHeight")> Public Property RowHeight() As Integer
        Get
            Return _rowHeight
        End Get
        Private Set(ByVal value As Integer)
            _rowHeight = value
        End Set
    End Property

    Public ReadOnly Property Columns() As ReportColumnCollection
        Get
            If _columns Is Nothing Then LoadColumns()
            Return _columns
        End Get
    End Property
    Public ReadOnly Property Groupings() As ReportGroupingCollection
        Get
            If _groupings Is Nothing Then LoadGroupings()
            Return _groupings
        End Get
    End Property
    Public ReadOnly Property Summaries() As ReportSummaryCollection
        Get
            If _summaries Is Nothing Then LoadSummaries()
            Return _summaries
        End Get
    End Property
    Public ReadOnly Property Hyperlinks() As ReportHyperlinkCollection
        Get
            If _hyperlinks Is Nothing Then LoadHyperlinks()
            Return _hyperlinks
        End Get
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub LoadColumns()
        _columns = New ReportColumnCollection
        _columns.LoadColumns(_reportId, _id)
    End Sub

    Public Sub LoadGroupings()
        _groupings = New ReportGroupingCollection
        _groupings.LoadGrouping(_reportId, _id)
    End Sub

    Public Sub LoadSummaries()
        _summaries = New ReportSummaryCollection()
        _summaries.LoadSummaries(_reportId, _id)
    End Sub

    Public Sub LoadHyperlinks()
        _hyperlinks = New ReportHyperlinkCollection()
        _hyperlinks.LoadHyperlinks(_reportId, _id)
    End Sub

End Class

Public Class ReportTableCollection
    Inherits BaseCollection(Of ReportTable)

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub LoadTables(ByVal reportId As Integer)
        Dim dt As DataTable = DataAccess.GetReportTables(reportId)
        Me.Load(dt)
    End Sub

End Class