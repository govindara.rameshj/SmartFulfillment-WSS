﻿Public Class ReportColumnLive
    Inherits Base
    Implements IReportColumnLive

#Region "Private Variables"
    Private _id As Integer
    Private _name As String
    Private _caption As String
    Private _formatType As Integer
    Private _format As String
    Private _isVisible As Boolean
    Private _isBold As Boolean
    Private _isImagePath As Boolean
    Private _minWidth As Integer
    Private _maxWidth As Integer
    Private _alignment As Integer
#End Region

#Region "Properties"
    <ColumnMapping("Id")> _
    Public Property Id() As Integer Implements IReportColumnLive.Id
        Get
            Return _id
        End Get
        Friend Set(ByVal value As Integer)
            _id = value
        End Set
    End Property

    <ColumnMapping("Name")> _
    Public Property Name() As String Implements IReportColumnLive.Name
        Get
            Return _name
        End Get
        Friend Set(ByVal value As String)
            _name = value
        End Set
    End Property

    <ColumnMapping("Caption")> _
    Public Property Caption() As String Implements IReportColumnLive.Caption
        Get
            Return _caption
        End Get
        Friend Set(ByVal value As String)
            _caption = value
        End Set
    End Property

    <ColumnMapping("FormatType")> _
    Public Property FormatType() As Integer Implements IReportColumnLive.FormatType
        Get
            Return _formatType
        End Get
        Friend Set(ByVal value As Integer)
            _formatType = value
        End Set
    End Property

    <ColumnMapping("Format")> _
    Public Property Format() As String Implements IReportColumnLive.Format
        Get
            If _format Is Nothing Then _format = String.Empty
            Return _format
        End Get
        Friend Set(ByVal value As String)
            _format = value
        End Set
    End Property

    <ColumnMapping("IsVisible")> _
    Public Property IsVisible() As Boolean Implements IReportColumnLive.IsVisible
        Get
            Return _isVisible
        End Get
        Friend Set(ByVal value As Boolean)
            _isVisible = value
        End Set
    End Property

    <ColumnMapping("IsBold")> _
    Public Property IsBold() As Boolean Implements IReportColumnLive.IsBold
        Get
            Return _isBold
        End Get
        Friend Set(ByVal value As Boolean)
            _isBold = value
        End Set
    End Property

    <ColumnMapping("IsImagePath")> _
    Public Property IsImagePath() As Boolean Implements IReportColumnLive.IsImagePath
        Get
            Return _isImagePath
        End Get
        Friend Set(ByVal value As Boolean)
            _isImagePath = value
        End Set
    End Property

    <ColumnMapping("MinWidth")> _
    Public Property MinWidth() As Integer Implements IReportColumnLive.MinWidth
        Get
            Return _minWidth
        End Get
        Friend Set(ByVal value As Integer)
            _minWidth = value
        End Set
    End Property

    <ColumnMapping("MaxWidth")> _
    Public Property MaxWidth() As Integer Implements IReportColumnLive.MaxWidth
        Get
            Return _maxWidth
        End Get
        Friend Set(ByVal value As Integer)
            _maxWidth = value
        End Set
    End Property

    <ColumnMapping("Alignment")> _
    Public Property Alignment() As Integer Implements IReportColumnLive.Alignment
        Get
            Return _alignment
        End Get
        Friend Set(ByVal value As Integer)
            _alignment = value
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

End Class

Public Class ReportColumnCollection
    Inherits BaseCollection(Of ReportColumn)

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub LoadColumns(ByVal reportId As Integer, ByVal tableId As Integer)
        Dim dt As DataTable = DataAccess.GetReportColumns(reportId, tableId)
        Me.Load(dt)
    End Sub

    Public Function Column(ByVal columnId As Integer) As ReportColumn
        For Each col As ReportColumn In Me.Items
            If col.Id = columnId Then Return col
        Next
        Return Nothing
    End Function

    Public Function Column(ByVal columnName As String) As ReportColumn
        For Each col As ReportColumn In Me.Items
            If col.Name = columnName Then Return col
        Next
        Return Nothing
    End Function

    Public Function ColumnName(ByVal columnId As Integer) As String
        For Each col As ReportColumn In Me.Items
            If col.Id = columnId Then Return col.Name
        Next
        Return Nothing
    End Function

End Class