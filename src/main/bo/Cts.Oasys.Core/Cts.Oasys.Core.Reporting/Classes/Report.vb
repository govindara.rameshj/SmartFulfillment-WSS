﻿Imports System.ComponentModel

Public Class Report
    Inherits Base
    Private _id As Integer
    Private _header As String
    Private _title As String
    Private _procedureName As String
    Private _hideWhenNoData As Boolean
    Private _printLandscape As Boolean
    Private _minWidth As Integer
    Private _minHeight As Integer
    Private _showResetButton As Boolean

    Private _dataset As DataSet = Nothing
    Private _parameters As ReportParameterCollection = Nothing
    Private _relations As ReportRelationCollection = Nothing
    Private _tables As ReportTableCollection = Nothing

    <ColumnMapping("Id")> Public Property Id() As Integer
        Get
            Return _id
        End Get
        Private Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    <ColumnMapping("Header")> Public Property Header() As String
        Get
            Return _header
        End Get
        Private Set(ByVal value As String)
            _header = value
        End Set
    End Property
    <ColumnMapping("Title")> Public Property Title() As String
        Get
            Return _title
        End Get
        Private Set(ByVal value As String)
            _title = value
        End Set
    End Property
    <ColumnMapping("ProcedureName")> Public Property ProcedureName() As String
        Get
            Return _procedureName
        End Get
        Private Set(ByVal value As String)
            _procedureName = value
        End Set
    End Property
    <ColumnMapping("HideWhenNoData")> Public Property HideWhenNoData() As Boolean
        Get
            Return _hideWhenNoData
        End Get
        Private Set(ByVal value As Boolean)
            _hideWhenNoData = value
        End Set
    End Property
    <ColumnMapping("PrintLandscape")> Public Property PrintLandscape() As Boolean
        Get
            Return _printLandscape
        End Get
        Private Set(ByVal value As Boolean)
            _printLandscape = value
        End Set
    End Property
    <ColumnMapping("MinWidth")> Public Property MinWidth() As Integer
        Get
            Return _minWidth
        End Get
        Private Set(ByVal value As Integer)
            _minWidth = value
        End Set
    End Property
    <ColumnMapping("MinHeight")> Public Property MinHeight() As Integer
        Get
            Return _minHeight
        End Get
        Private Set(ByVal value As Integer)
            _minHeight = value
        End Set
    End Property
    <ColumnMapping("ShowResetButton")> Public Property ShowResetButton() As Boolean
        Get
            Return _showResetButton
        End Get
        Private Set(ByVal value As Boolean)
            _showResetButton = value
        End Set
    End Property

    Public ReadOnly Property Dataset() As DataSet
        Get
            If _dataset Is Nothing Then LoadData()
            Return _dataset
        End Get
    End Property
    Public ReadOnly Property Parameters() As ReportParameterCollection
        Get
            If _parameters Is Nothing Then LoadParameters()
            Return _parameters
        End Get
    End Property
    Public ReadOnly Property Relations() As ReportRelationCollection
        Get
            If _relations Is Nothing Then LoadRelations()
            Return _relations
        End Get
    End Property
    Public ReadOnly Property Tables() As ReportTableCollection
        Get
            If _tables Is Nothing Then LoadTables()
            Return _tables
        End Get
    End Property


    Public Sub New()
        MyBase.New()
    End Sub

    Private Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub


    Public Sub LoadParameters()
        _parameters = New ReportParameterCollection()
        _parameters.LoadParameters(_id)
    End Sub

    Public Sub LoadRelations()
        _relations = New ReportRelationCollection
        _relations.LoadRelations(_id)
    End Sub

    Public Sub LoadTables()
        _tables = New ReportTableCollection
        _tables.LoadTables(_id)
    End Sub

    Public Sub LoadData()

        _dataset = DataAccess.GetProcedureDataset(Me)

        'set up table names (forcing load)
        For index As Integer = 0 To Tables.Count - 1
            _dataset.Tables(index).TableName = _tables(index).Name
        Next

        'set up relations (forcing load)
        For Each repRelation As ReportRelation In Relations
            Dim parentCols As New ArrayList
            Dim childCols As New ArrayList

            For Each parentCol As String In repRelation.ParentColumns.Split(","c)
                parentCols.Add(_dataset.Tables(repRelation.ParentTable).Columns(parentCol.Trim))
            Next

            For Each childCol As String In repRelation.ChildColumns.Split(","c)
                childCols.Add(_dataset.Tables(repRelation.ChildTable).Columns(childCol.Trim))
            Next

            _dataset.Relations.Add(repRelation.Name, CType(parentCols.ToArray(GetType(DataColumn)), DataColumn()), CType(childCols.ToArray(GetType(DataColumn)), DataColumn()))
        Next

    End Sub


    Public Shared Function GetReport(ByVal id As Integer) As Report

        Dim dr As DataRow = DataAccess.GetReport(id).Rows(0)
        Dim rep As New Report(dr)
        rep.LoadTables()
        rep.LoadParameters()
        rep.LoadRelations()
        Return rep

    End Function

    Public Shared Function GetReports(ByVal ids As String) As ReportCollection

        Dim reports As New ReportCollection

        Dim dt As DataTable = DataAccess.GetReports(ids)
        For Each dr As DataRow In dt.Rows
            reports.Add(New Report(dr))
        Next

        Return reports

    End Function

End Class

Public Class ReportCollection
    Inherits BaseCollection(Of Report)

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub LoadReports()
        Dim dt As DataTable = DataAccess.GetReports()
        Me.Load(dt)
    End Sub

End Class