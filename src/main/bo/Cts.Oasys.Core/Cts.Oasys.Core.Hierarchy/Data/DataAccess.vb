﻿Imports Cts.Oasys.Data

Namespace DataAccess

    <HideModuleName()> Friend Module DataOperations

        Friend Function MasterGetAll() As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.HierarchyMasterGetAll)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

        Friend Function HierarchyGetAll() As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.HierarchyGetAll)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

    End Module

End Namespace


