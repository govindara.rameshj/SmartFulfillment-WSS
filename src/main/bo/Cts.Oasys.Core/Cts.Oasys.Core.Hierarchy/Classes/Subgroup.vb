﻿Imports Cts.Oasys.Core
Imports System.ComponentModel

Public Class Subgroup
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _number As String
    Private _categoryNumber As String
    Private _groupNumber As String
    Private _description As String
    Private _alpha As String
    Private _styles As StyleCollection = Nothing
#End Region

#Region "Properties"
    <ColumnMapping("Number")> Public Property Number() As String
        Get
            Return _number
        End Get
        Private Set(ByVal value As String)
            _number = value
        End Set
    End Property
    <ColumnMapping("CategoryNumber")> Public Property CategoryNumber() As String
        Get
            Return _categoryNumber
        End Get
        Private Set(ByVal value As String)
            _categoryNumber = value
        End Set
    End Property
    <ColumnMapping("GroupNumber")> Public Property GroupNumber() As String
        Get
            Return _groupNumber
        End Get
        Private Set(ByVal value As String)
            _groupNumber = value
        End Set
    End Property
    <ColumnMapping("Description")> Public Property Description() As String
        Get
            Return _description
        End Get
        Private Set(ByVal value As String)
            _description = value
        End Set
    End Property
    <ColumnMapping("Alpha")> Public Property Alpha() As String
        Get
            Return _alpha
        End Get
        Private Set(ByVal value As String)
            _alpha = value
        End Set
    End Property
    Public ReadOnly Property Styles() As StyleCollection
        Get
            If _styles Is Nothing Then _styles = New StyleCollection(Me)
            Return _styles
        End Get
    End Property
#End Region

#Region "Constructors"

    Public Sub New()
        MyBase.New()
    End Sub

    Private Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

    Friend Sub New(ByVal hie As Hierarchy)
        MyBase.New()
        _categoryNumber = hie.CategoryNumber
        _groupNumber = hie.GroupNumber
        _number = hie.SubgroupNumber
        _description = hie.SubgroupName
        _alpha = hie.SubgroupAlpha
    End Sub

#End Region


End Class