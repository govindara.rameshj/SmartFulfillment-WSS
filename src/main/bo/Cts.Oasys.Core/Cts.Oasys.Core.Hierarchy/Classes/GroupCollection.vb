﻿Imports System.ComponentModel

Public Class GroupCollection
    Inherits BindingList(Of Group)
    Private _category As Category = Nothing

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByRef category As Category)
        MyBase.New()
        _category = category
    End Sub

    Public Overloads Function Add(ByVal hie As Hierarchy) As Group

        'check if category been set and return nothing if not
        If _category Is Nothing Then
            Return Nothing
        End If

        'check if group exists in collection
        Dim hieGroup As Group = Nothing
        For Each g As Group In Me.Items
            If g.Number = hie.GroupNumber AndAlso hie.CategoryNumber = _category.Number Then
                hieGroup = g
                Exit For
            End If
        Next

        'if not then create
        If hieGroup Is Nothing Then
            hieGroup = New Group(hie)
            Me.Items.Add(hieGroup)
        End If

        'add subgroup
        hieGroup.Subgroups.Add(hie)

        Return hieGroup

    End Function

End Class