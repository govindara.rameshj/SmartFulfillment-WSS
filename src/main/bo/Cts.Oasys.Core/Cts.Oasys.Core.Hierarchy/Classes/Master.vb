﻿Imports Cts.Oasys.Core
Imports System.ComponentModel

Public Class Master
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _level As Integer
    Private _number As String
    Private _description As String
    Private _isDeleted As Boolean
    Private _maxOverride As Integer
    Private _reductionWeek1 As Decimal
    Private _reductionWeek2 As Decimal
    Private _reductionWeek3 As Decimal
    Private _reductionWeek4 As Decimal
    Private _reductionWeek5 As Decimal
    Private _reductionWeek6 As Decimal
    Private _reductionWeek7 As Decimal
    Private _reductionWeek8 As Decimal
    Private _reductionWeek9 As Decimal
    Private _reductionWeek10 As Decimal
#End Region

#Region "Properties"
    <ColumnMapping("Level")> Public Property Level() As Integer
        Get
            Return _level
        End Get
        Private Set(ByVal value As Integer)
            _level = value
        End Set
    End Property
    <ColumnMapping("Number")> Public Property Number() As String
        Get
            Return _number
        End Get
        Private Set(ByVal value As String)
            _number = value
        End Set
    End Property
    <ColumnMapping("Description")> Public Property Description() As String
        Get
            Return _description
        End Get
        Private Set(ByVal value As String)
            _description = value
        End Set
    End Property
    <ColumnMapping("IsDeleted")> Public Property IsDeleted() As Boolean
        Get
            Return _isDeleted
        End Get
        Private Set(ByVal value As Boolean)
            _isDeleted = value
        End Set
    End Property
    <ColumnMapping("MaxOverride")> Public Property MaxOverride() As Integer
        Get
            Return _maxOverride
        End Get
        Private Set(ByVal value As Integer)
            _maxOverride = value
        End Set
    End Property
    <ColumnMapping("ReductionWeek1")> Public Property ReductionWeek1() As Decimal
        Get
            Return _reductionWeek1
        End Get
        Private Set(ByVal value As Decimal)
            _reductionWeek1 = value
        End Set
    End Property
    <ColumnMapping("ReductionWeek2")> Public Property ReductionWeek2() As Decimal
        Get
            Return _reductionWeek2
        End Get
        Private Set(ByVal value As Decimal)
            _reductionWeek2 = value
        End Set
    End Property
    <ColumnMapping("ReductionWeek3")> Public Property ReductionWeek3() As Decimal
        Get
            Return _reductionWeek3
        End Get
        Private Set(ByVal value As Decimal)
            _reductionWeek3 = value
        End Set
    End Property
    <ColumnMapping("ReductionWeek4")> Public Property ReductionWeek4() As Decimal
        Get
            Return _reductionWeek4
        End Get
        Private Set(ByVal value As Decimal)
            _reductionWeek4 = value
        End Set
    End Property
    <ColumnMapping("ReductionWeek5")> Public Property ReductionWeek5() As Decimal
        Get
            Return _reductionWeek5
        End Get
        Private Set(ByVal value As Decimal)
            _reductionWeek5 = value
        End Set
    End Property
    <ColumnMapping("ReductionWeek6")> Public Property ReductionWeek6() As Decimal
        Get
            Return _reductionWeek6
        End Get
        Private Set(ByVal value As Decimal)
            _reductionWeek6 = value
        End Set
    End Property
    <ColumnMapping("ReductionWeek7")> Public Property ReductionWeek7() As Decimal
        Get
            Return _reductionWeek7
        End Get
        Private Set(ByVal value As Decimal)
            _reductionWeek7 = value
        End Set
    End Property
    <ColumnMapping("ReductionWeek8")> Public Property ReductionWeek8() As Decimal
        Get
            Return _reductionWeek8
        End Get
        Private Set(ByVal value As Decimal)
            _reductionWeek8 = value
        End Set
    End Property
    <ColumnMapping("ReductionWeek9")> Public Property ReductionWeek9() As Decimal
        Get
            Return _reductionWeek9
        End Get
        Private Set(ByVal value As Decimal)
            _reductionWeek9 = value
        End Set
    End Property
    <ColumnMapping("ReductionWeek10")> Public Property ReductionWeek10() As Decimal
        Get
            Return _reductionWeek10
        End Get
        Private Set(ByVal value As Decimal)
            _reductionWeek10 = value
        End Set
    End Property
#End Region

#Region "Constructors"

    Public Sub New()
        MyBase.New()
    End Sub

    Private Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region


End Class