﻿Imports System.ComponentModel

Public Class CategoryCollection
    Inherits BindingList(Of Category)

    Public Sub New()
        MyBase.New()
    End Sub

    Public Overloads Function Add(ByVal hie As Hierarchy) As Category

        Dim hieCategory As Category = Nothing

        'find category if already in collection
        For Each cat As Category In Me.Items
            If cat.Number = hie.CategoryNumber Then
                hieCategory = cat
                Exit For
            End If
        Next

        'else if not in collection then create new instance
        If hieCategory Is Nothing Then
            hieCategory = New Category(hie)
            Me.Items.Add(hieCategory)
        End If

        'add group to this category
        hieCategory.Groups.Add(hie)

        Return hieCategory

    End Function

End Class