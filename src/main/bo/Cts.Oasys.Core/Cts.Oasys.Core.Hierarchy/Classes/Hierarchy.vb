﻿Imports Cts.Oasys.Core
Imports System.ComponentModel

Public Class Hierarchy
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _categoryNumber As String
    Private _categoryName As String
    Private _categoryAlpha As String
    Private _groupNumber As String
    Private _groupName As String
    Private _groupAlpha As String
    Private _subgroupNumber As String
    Private _subgroupName As String
    Private _subgroupAlpha As String
    Private _styleNumber As String
    Private _styleName As String
    Private _styleAlpha As String
#End Region

#Region "Properties"
    <ColumnMapping("CategoryNumber")> Public Property CategoryNumber() As String
        Get
            Return _categoryNumber
        End Get
        Private Set(ByVal value As String)
            _categoryNumber = value
        End Set
    End Property
    <ColumnMapping("CategoryName")> Public Property CategoryName() As String
        Get
            Return _categoryName
        End Get
        Private Set(ByVal value As String)
            _categoryName = value
        End Set
    End Property
    <ColumnMapping("CategoryAlpha")> Public Property CategoryAlpha() As String
        Get
            Return _categoryAlpha
        End Get
        Private Set(ByVal value As String)
            _categoryAlpha = value
        End Set
    End Property
    <ColumnMapping("GroupNumber")> Public Property GroupNumber() As String
        Get
            Return _groupNumber
        End Get
        Private Set(ByVal value As String)
            _groupNumber = value
        End Set
    End Property
    <ColumnMapping("GroupName")> Public Property GroupName() As String
        Get
            Return _groupName
        End Get
        Private Set(ByVal value As String)
            _groupName = value
        End Set
    End Property
    <ColumnMapping("GroupAlpha")> Public Property GroupAlpha() As String
        Get
            Return _groupAlpha
        End Get
        Private Set(ByVal value As String)
            _groupAlpha = value
        End Set
    End Property
    <ColumnMapping("SubgroupNumber")> Public Property SubgroupNumber() As String
        Get
            Return _subgroupNumber
        End Get
        Private Set(ByVal value As String)
            _subgroupNumber = value
        End Set
    End Property
    <ColumnMapping("SubgroupName")> Public Property SubgroupName() As String
        Get
            Return _subgroupName
        End Get
        Private Set(ByVal value As String)
            _subgroupName = value
        End Set
    End Property
    <ColumnMapping("SubgroupAlpha")> Public Property SubgroupAlpha() As String
        Get
            Return _subgroupAlpha
        End Get
        Private Set(ByVal value As String)
            _subgroupAlpha = value
        End Set
    End Property
    <ColumnMapping("StyleNumber")> Public Property StyleNumber() As String
        Get
            Return _styleNumber
        End Get
        Private Set(ByVal value As String)
            _styleNumber = value
        End Set
    End Property
    <ColumnMapping("StyleName")> Public Property StyleName() As String
        Get
            Return _styleName
        End Get
        Private Set(ByVal value As String)
            _styleName = value
        End Set
    End Property
    <ColumnMapping("StyleAlpha")> Public Property StyleAlpha() As String
        Get
            Return _styleAlpha
        End Get
        Private Set(ByVal value As String)
            _styleAlpha = value
        End Set
    End Property
#End Region

#Region "Constructors"

    Public Sub New()
        MyBase.New()
    End Sub

    Private Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

#End Region

    ''' <summary>
    ''' Returns all records from hierarchy view
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetAll() As HierarchyCollection

        Dim hc As New HierarchyCollection
        Dim dt As DataTable = DataAccess.HierarchyGetAll
        For Each dr As DataRow In dt.Rows
            hc.Add(New Hierarchy(dr))
        Next

        Return hc

    End Function

End Class