﻿Imports System.ComponentModel

Public Class StyleCollection
    Inherits BindingList(Of Style)
    Private _subgroup As Subgroup = Nothing

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByRef subgroup As Subgroup)
        MyBase.New()
        _subgroup = subgroup
    End Sub

    Public Overloads Function Add(ByVal hie As Hierarchy) As Style

        'check if group been set and return nothing if not
        If _subgroup Is Nothing Then
            Return Nothing
        End If

        'check if group exists in collection
        Dim hieStyle As Style = Nothing
        For Each g As Style In Me.Items
            If g.Number = hie.SubgroupNumber AndAlso hie.SubgroupNumber = _subgroup.Number AndAlso hie.GroupNumber = _subgroup.GroupNumber AndAlso hie.CategoryNumber = _subgroup.CategoryNumber Then
                hieStyle = g
                Exit For
            End If
        Next

        'if not then create
        If hieStyle Is Nothing Then
            hieStyle = New Style(hie)
            Me.Items.Add(hieStyle)
        End If

        Return hieStyle

    End Function

End Class