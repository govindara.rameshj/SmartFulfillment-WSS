﻿Imports Cts.Oasys.Core
Imports System.ComponentModel

Public Class Category
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _number As String
    Private _description As String
    Private _alpha As String
    Private _groups As GroupCollection = Nothing
#End Region

#Region "Properties"
    <ColumnMapping("Number")> Public Property Number() As String
        Get
            Return _number
        End Get
        Private Set(ByVal value As String)
            _number = value
        End Set
    End Property
    <ColumnMapping("Description")> Public Property Description() As String
        Get
            Return _description
        End Get
        Private Set(ByVal value As String)
            _description = value
        End Set
    End Property
    <ColumnMapping("Alpha")> Public Property Alpha() As String
        Get
            Return _alpha
        End Get
        Private Set(ByVal value As String)
            _alpha = value
        End Set
    End Property
    Public ReadOnly Property Groups() As GroupCollection
        Get
            If _groups Is Nothing Then _groups = New GroupCollection(Me)
            Return _groups
        End Get
    End Property
#End Region

#Region "Constructors"

    Public Sub New()
        MyBase.New()
    End Sub

    Private Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

    Friend Sub New(ByVal hie As Hierarchy)
        MyBase.New()
        _number = hie.CategoryNumber
        _description = hie.CategoryName
        _alpha = hie.CategoryAlpha
    End Sub

#End Region

    ''' <summary>
    ''' Returns category collection instance with children
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetAllCategorised() As CategoryCollection

        Dim cc As New CategoryCollection
        Dim hc As HierarchyCollection = Hierarchy.GetAll

        For Each hie As Hierarchy In hc
            cc.Add(hie)
        Next
        Return cc

    End Function

End Class