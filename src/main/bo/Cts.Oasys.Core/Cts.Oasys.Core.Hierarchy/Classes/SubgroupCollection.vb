﻿Imports System.ComponentModel

Public Class SubgroupCollection
    Inherits BindingList(Of Subgroup)
    Private _group As Group

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByRef group As Group)
        MyBase.New()
        _group = group
    End Sub

    Public Overloads Function Add(ByVal hie As Hierarchy) As Subgroup

        'check if group been set and return nothing if not
        If _group Is Nothing Then
            Return Nothing
        End If

        'check if group exists in collection
        Dim hieSub As Subgroup = Nothing
        For Each g As Subgroup In Me.Items
            If g.Number = hie.SubgroupNumber AndAlso hie.GroupNumber = _group.Number AndAlso hie.CategoryNumber = _group.CategoryNumber Then
                hieSub = g
                Exit For
            End If
        Next

        'if not then create
        If hieSub Is Nothing Then
            hieSub = New Subgroup(hie)
            Me.Items.Add(hieSub)
        End If

        'add subgroup
        hieSub.Styles.Add(hie)

        Return hieSub

    End Function

End Class