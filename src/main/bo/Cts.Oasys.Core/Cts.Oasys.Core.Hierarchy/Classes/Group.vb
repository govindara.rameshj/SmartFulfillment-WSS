﻿Imports Cts.Oasys.Core
Imports System.ComponentModel

Public Class Group
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _number As String
    Private _categoryNumber As String
    Private _description As String
    Private _alpha As String
    Private _subgroups As SubgroupCollection = Nothing
#End Region

#Region "Properties"
    <ColumnMapping("Number")> Public Property Number() As String
        Get
            Return _number
        End Get
        Private Set(ByVal value As String)
            _number = value
        End Set
    End Property
    <ColumnMapping("CategoryNumber")> Public Property CategoryNumber() As String
        Get
            Return _categoryNumber
        End Get
        Private Set(ByVal value As String)
            _categoryNumber = value
        End Set
    End Property
    <ColumnMapping("Description")> Public Property Description() As String
        Get
            Return _description
        End Get
        Private Set(ByVal value As String)
            _description = value
        End Set
    End Property
    <ColumnMapping("Alpha")> Public Property Alpha() As String
        Get
            Return _alpha
        End Get
        Private Set(ByVal value As String)
            _alpha = value
        End Set
    End Property
    Public ReadOnly Property Subgroups() As SubgroupCollection
        Get
            If _subgroups Is Nothing Then _subgroups = New SubgroupCollection(Me)
            Return _subgroups
        End Get
    End Property
#End Region

#Region "Constructors"

    Public Sub New()
        MyBase.New()
    End Sub

    Private Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

    Friend Sub New(ByVal hie As Hierarchy)
        MyBase.New()
        _categoryNumber = hie.CategoryNumber
        _number = hie.GroupNumber
        _description = hie.GroupName
        _alpha = hie.GroupAlpha
    End Sub

#End Region


End Class