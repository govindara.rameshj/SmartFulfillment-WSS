﻿Imports Cts.Oasys.Core
Imports System.ComponentModel

Public Class Style
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _number As String
    Private _categoryNumber As String
    Private _groupNumber As String
    Private _subgroupNumber As String
    Private _description As String
    Private _alpha As String
#End Region

#Region "Properties"
    <ColumnMapping("Number")> Public Property Number() As String
        Get
            Return _number
        End Get
        Private Set(ByVal value As String)
            _number = value
        End Set
    End Property
    <ColumnMapping("CategoryNumber")> Public Property CategoryNumber() As String
        Get
            Return _categoryNumber
        End Get
        Private Set(ByVal value As String)
            _categoryNumber = value
        End Set
    End Property
    <ColumnMapping("GroupNumber")> Public Property GroupNumber() As String
        Get
            Return _groupNumber
        End Get
        Private Set(ByVal value As String)
            _groupNumber = value
        End Set
    End Property
    <ColumnMapping("SubgroupNumber")> Public Property SubgroupNumber() As String
        Get
            Return _subgroupNumber
        End Get
        Private Set(ByVal value As String)
            _subgroupNumber = value
        End Set
    End Property
    <ColumnMapping("Description")> Public Property Description() As String
        Get
            Return _description
        End Get
        Private Set(ByVal value As String)
            _description = value
        End Set
    End Property
    <ColumnMapping("Alpha")> Public Property Alpha() As String
        Get
            Return _alpha
        End Get
        Private Set(ByVal value As String)
            _alpha = value
        End Set
    End Property
#End Region

#Region "Constructors"

    Public Sub New()
        MyBase.New()
    End Sub

    Private Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

    Friend Sub New(ByVal hie As Hierarchy)
        MyBase.New()
        _categoryNumber = hie.CategoryNumber
        _groupNumber = hie.GroupNumber
        _subgroupNumber = hie.SubgroupNumber
        _number = hie.StyleNumber
        _description = hie.StyleName
        _alpha = hie.StyleAlpha
    End Sub

#End Region


End Class