﻿Imports Cts.Oasys.Core
Imports System.ComponentModel

Public Class SystemDate
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _id As String
    Private _daysOpen As Decimal
    Private _today As Date
    Private _todayDayNumber As Decimal
    Private _tomorrow As Date
    Private _tomorrowDayNumber As Decimal
    Private _cycleWeeksTotal As Decimal
    Private _cycleWeeksCurrent As Decimal
    Private _nightlyTask As String
#End Region

#Region "Properties"
    <ColumnMapping("Id")> Public Property Id() As String
        Get
            Return _id
        End Get
        Private Set(ByVal value As String)
            _id = value
        End Set
    End Property
    <ColumnMapping("DaysOpen")> Public Property DaysOpen() As Decimal
        Get
            Return _daysOpen
        End Get
        Private Set(ByVal value As Decimal)
            _daysOpen = value
        End Set
    End Property
    <ColumnMapping("Today")> Public Property Today() As Date
        Get
            Return _today
        End Get
        Set(ByVal value As Date)
            _today = value
        End Set
    End Property
    <ColumnMapping("TodayDayNumber")> Public Property TodayDayNumber() As Decimal
        Get
            Return _todayDayNumber
        End Get
        Private Set(ByVal value As Decimal)
            _todayDayNumber = value
        End Set
    End Property
    <ColumnMapping("Tomorrow")> Public Property Tomorrow() As Date
        Get
            Return _tomorrow
        End Get
        Private Set(ByVal value As Date)
            _tomorrow = value
        End Set
    End Property
    <ColumnMapping("TomorrowDayNumber")> Public Property TomorrowDayNumber() As Decimal
        Get
            Return _tomorrowDayNumber
        End Get
        Private Set(ByVal value As Decimal)
            _tomorrowDayNumber = value
        End Set
    End Property
    <ColumnMapping("CycleWeeksTotal")> Public Property CycleWeeksTotal() As Decimal
        Get
            Return _cycleWeeksTotal
        End Get
        Private Set(ByVal value As Decimal)
            _cycleWeeksTotal = value
        End Set
    End Property
    <ColumnMapping("CycleWeeksCurrent")> Public Property CycleWeeksCurrent() As Decimal
        Get
            Return _cycleWeeksCurrent
        End Get
        Private Set(ByVal value As Decimal)
            _cycleWeeksCurrent = value
        End Set
    End Property
    <ColumnMapping("NightlyTask")> Public Property NightlyTask() As String
        Get
            Return _nightlyTask
        End Get
        Private Set(ByVal value As String)
            _nightlyTask = value
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

    Public Shared Function GetToday() As Date
        Dim dt As DataTable = DataAccess.SystemDateGet
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Return CDate(dt.Rows(0).Item(GetPropertyName(Function(f As SystemDate) f.Today)))
        End If
        Return Nothing
    End Function

    Public Shared Function GetTomorrow() As Date
        Dim dt As DataTable = DataAccess.SystemDateGet
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Return CDate(dt.Rows(0).Item(GetPropertyName(Function(f As SystemDate) f.Tomorrow)))
        End If
        Return Nothing
    End Function

    Public Shared Function GetNightlyTask() As String
        Dim dt As DataTable = DataAccess.SystemDateGet
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(GetPropertyName(Function(f As SystemDate) f.NightlyTask)).ToString
        End If
        Return Nothing
    End Function

End Class