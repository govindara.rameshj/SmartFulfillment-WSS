﻿Public Class SystemPeriod
    Inherits Base

#Region "       Table Fields"
    Private _id As Integer
    Private _startDate As Date
    Private _endDate As Date
    Private _isClosed As Boolean

    <ColumnMapping("ID")> Public Property Id() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    <ColumnMapping("StartDate")> Public Property StartDate() As Date
        Get
            Return _startDate
        End Get
        Set(ByVal value As Date)
            _startDate = value
        End Set
    End Property
    <ColumnMapping("EndDate")> Public Property EndDate() As Date
        Get
            Return _endDate
        End Get
        Set(ByVal value As Date)
            _endDate = value
        End Set
    End Property
    <ColumnMapping("IsClosed")> Public Property IsClosed() As Boolean
        Get
            Return _isClosed
        End Get
        Set(ByVal value As Boolean)
            _isClosed = value
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

    Private Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

    Public Shared Function GetCurrent() As SystemPeriod
        Dim dt As DataTable = DataAccess.SystemPeriodGet(Now.Date, Now.Date)
        If dt.Rows.Count > 0 Then Return New SystemPeriod(dt.Rows(0))
        Return Nothing
    End Function

    Public Shared Function GetAllToDate() As SystemPeriodCollection
        Dim spc As New SystemPeriodCollection
        spc.LoadAllToDate()
        Return spc
    End Function

End Class

Public Class SystemPeriodCollection
    Inherits BaseCollection(Of SystemPeriod)

    Public Sub LoadAllToDate()
        Dim dt As DataTable = DataAccess.SystemPeriodGet(Nothing, Now.Date)
        Me.Load(dt)
    End Sub

End Class