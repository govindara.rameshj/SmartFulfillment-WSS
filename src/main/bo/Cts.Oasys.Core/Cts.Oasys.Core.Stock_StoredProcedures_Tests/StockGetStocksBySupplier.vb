﻿<TestClass()> Public Class StockGetStocksBySupplier
    Inherits StoredProcedure_MultipleRows_Test

    Private Const NewSupplierID As String = "00001"
    Private Const NewStockID As String = "000001"

#Region "StoredProcedure_Test Overridable Tests"

    ' Seems it is necessary to override these tests (and just call the base test with the override),
    ' just to get base test to run.

    <TestMethod()> _
    Public Overrides Sub StoredProcedureExists()

        'Try

        '    'CreateNewSupplier(NewSupplierID)
        '    'CreateNewStock(NewSupplierID, NewStockID)

        '    MyBase.StoredProcedureExists()



        'Catch ex As Exception

        '    System.Diagnostics.Trace.WriteLine(ex.Message)

        'Finally

        '    'need to guarantee delete code is called
        '    'DeleteStock(NewStockID)
        '    'DeleteSupplier(NewSupplierID)

        'End Try


        'CreateNewSupplier(NewSupplierID)
        'CreateNewStock(NewSupplierID, NewStockID)

        MyBase.StoredProcedureExists()


        'DeleteStock(NewStockID)
        'DeleteSupplier(NewSupplierID)




    End Sub

    <TestMethod()> _
    Public Overrides Sub WhenDataExists_StoredProcedureGetsDataTable()

        MyBase.WhenDataExists_StoredProcedureGetsDataTable()
    End Sub

    <TestMethod()> _
    Public Overrides Sub WhenDataExists_StoredProcedureGetsAtLeastOneRecord()

        MyBase.WhenDataExists_StoredProcedureGetsAtLeastOneRecord()
    End Sub

    <TestMethod()> _
    Public Overrides Sub WhenDataExists_StoredProcedureCanGetMultipleRecords()

        MyBase.WhenDataExists_StoredProcedureCanGetMultipleRecords()
    End Sub

    <TestMethod()> _
    Public Overrides Sub WhenDataExists_StoredProcedureGetsRecordsByKey()

        MyBase.WhenDataExists_StoredProcedureGetsRecordsByKey()
    End Sub

#End Region

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    <ClassInitialize()> _
    Public Shared Shadows Sub MyClassInitialize(ByVal testContext As TestContext)

        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Sql
                    Case Else
                        Assert.Fail("Cannot perform Stored Procedures tests as not connected to a SQL Server database")
                End Select
            End Using
        End Using
    End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Field Tests"

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedSkuNumber()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("SkuNumber", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDescription()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("Description", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedProductCode()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("ProductCode", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedPackSize()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("PackSize", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedPrice()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("Price", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedWeight()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("Weight", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedCost()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("Cost", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedOnHandQty()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("OnHandQty", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedOnOrderQty()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("OnOrderQty", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedMinimumQty()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("MinimumQty", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedMaximumQty()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("MaximumQty", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedOpenReturnsQty()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("OpenReturnsQty", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedMarkdownQty()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("MarkdownQty", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedWriteOffQty()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("WriteOffQty", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedIsNonStock()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("IsNonStock", Message, FieldType.ftBoolean), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedIsObsolete()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("IsObsolete", Message, FieldType.ftBoolean), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedIsDeleted()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("IsDeleted", Message, FieldType.ftBoolean), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedIsItemSingle()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("IsItemSingle", Message, FieldType.ftBoolean), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedIsNonOrder()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("IsNonOrder", Message, FieldType.ftBoolean), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedReceivedTodayQty()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("ReceivedTodayQty", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedReceivedTodayValue()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("ReceivedTodayValue", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDateFirstStocked()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DateFirstStocked", Message, FieldType.ftDate), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDateFirstOrder()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DateFirstOrder", Message, FieldType.ftDate), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDateFinalOrder()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DateFinalOrder", Message, FieldType.ftDate), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDateLastReceived()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DateLastReceived", Message, FieldType.ftDate), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedSupplierNumber()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("SupplierNumber", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedSupplierName()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("SupplierName", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedHieCategory()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("HieCategory", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedHieCategoryName()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("HieCategoryName", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedHieGroup()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("HieGroup", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedHieSubgroup()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("HieSubgroup", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedHieStyle()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("HieStyle", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedActivityToday()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("ActivityToday", Message, FieldType.ftBoolean), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedUnitSales1()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("UnitSales1", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedUnitSales2()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("UnitSales2", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedUnitSales3()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("UnitSales3", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedUnitSales4()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("UnitSales4", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedUnitSales5()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("UnitSales5", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedUnitSales6()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("UnitSales6", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedUnitSales7()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("UnitSales7", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedUnitSales8()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("UnitSales8", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedUnitSales9()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("UnitSales9", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedUnitSales10()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("UnitSales10", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedUnitSales11()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("UnitSales11", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedUnitSales12()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("UnitSales12", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedUnitSales13()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("UnitSales13", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedUnitSales14()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("UnitSales14", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDaysOutStock1()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DaysOutStock1", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDaysOutStock2()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DaysOutStock2", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDaysOutStock3()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DaysOutStock3", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDaysOutStock4()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DaysOutStock4", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDaysOutStock5()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DaysOutStock5", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDaysOutStock6()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DaysOutStock6", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDaysOutStock7()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DaysOutStock7", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDaysOutStock8()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DaysOutStock8", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDaysOutStock9()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DaysOutStock9", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDaysOutStock10()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DaysOutStock10", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDaysOutStock11()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DaysOutStock11", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDaysOutStock12()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DaysOutStock12", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDaysOutStock13()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DaysOutStock13", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDaysOutStock14()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DaysOutStock14", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDateLastSoqInit()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DateLastSoqInit", Message, FieldType.ftDate), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedIsInitialised()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("IsInitialised", Message, FieldType.ftBoolean), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedToForecast()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("ToForecast", Message, FieldType.ftBoolean), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedFlierPeriod1()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("FlierPeriod1", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedFlierPeriod2()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("FlierPeriod2", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedFlierPeriod3()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("FlierPeriod3", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedFlierPeriod4()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("FlierPeriod4", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedFlierPeriod5()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("FlierPeriod5", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedFlierPeriod6()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("FlierPeriod6", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedFlierPeriod7()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("FlierPeriod7", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedFlierPeriod8()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("FlierPeriod8", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedFlierPeriod9()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("FlierPeriod9", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedFlierPeriod10()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("FlierPeriod10", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedFlierPeriod11()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("FlierPeriod11", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedFlierPeriod12()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("FlierPeriod12", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDemandPattern()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("DemandPattern", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedPeriodDemand()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("PeriodDemand", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedPeriodTrend()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("PeriodTrend", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedErrorForecast()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("ErrorForecast", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedErrorSmoothed()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("ErrorSmoothed", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedBufferConversion()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("BufferConversion", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedBufferStock()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("BufferStock", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedServiceLevelOverride()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("ServiceLevelOverride", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedValueAnnualUsage()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("ValueAnnualUsage", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedOrderLevel()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("OrderLevel", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedSaleWeightSeason()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("SaleWeightSeason", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedSaleWeightBank()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("SaleWeightBank", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedSaleWeightPromo()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("SaleWeightPromo", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedCapacity()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Assert.IsTrue(FieldsMatch("Capacity", Message, FieldType.ftInteger), Message)
        End If
    End Sub

#End Region

#Region "StoredProcedureSingleRow_TestMust Overrides"

    Public Overrides Function GetExistingSQL() As String
        Dim Sql As New StringBuilder

        ' Get Stock for a given supplier using field list as expected to be returned by stored procedure.
        ' Use sql to find a supplier that has not been deleted and has at least 1 stock item that has a 
        ' Null IODT field value as well as the Obsolete, Deleted and Related Item Single flags set to 0.
        ' Return al the rows for that sup[lier that would be expecetd to be returned by the stored procedure
        Sql.Append("Select ")
        Sql.Append("	STKMAS.skun As 'SkuNumber', ")
        Sql.Append("	STKMAS.descr As 'Description', ")
        Sql.Append("	STKMAS.prod As 'ProductCode', ")
        Sql.Append("	STKMAS.pack As 'PackSize', ")
        Sql.Append("	STKMAS.pric As 'Price', ")
        Sql.Append("	STKMAS.WGHT As 'Weight', ")
        Sql.Append("	STKMAS.COST As 'Cost', ")
        Sql.Append("	STKMAS.onha As 'OnHandQty', ")
        Sql.Append("	STKMAS.onor As 'OnOrderQty', ")
        Sql.Append("	STKMAS.mini As 'MinimumQty', ")
        Sql.Append("	STKMAS.maxi As 'MaximumQty', ")
        Sql.Append("	STKMAS.retq As 'OpenReturnsQty', ")
        Sql.Append("	STKMAS.mdnq As 'MarkdownQty', ")
        Sql.Append("	STKMAS.wtfq As 'WriteOffQty', ")
        Sql.Append("	STKMAS.INON As 'IsNonStock', ")
        Sql.Append("	STKMAS.IOBS As 'IsObsolete', ")
        Sql.Append("	STKMAS.IDEL As 'IsDeleted', ")
        Sql.Append("	STKMAS.IRIS As 'IsItemSingle', ")
        Sql.Append("	STKMAS.NOOR As 'IsNonOrder', ")
        Sql.Append("	STKMAS.treq As 'ReceivedTodayQty', ")
        Sql.Append("	STKMAS.trev As 'ReceivedTodayValue', ")
        Sql.Append("	STKMAS.dats As 'DateFirstStocked', ")
        Sql.Append("	STKMAS.IODT As 'DateFirstOrder', ")
        Sql.Append("	STKMAS.FODT As 'DateFinalOrder', ")
        Sql.Append("	STKMAS.drec As 'DateLastReceived', ")
        Sql.Append("	STKMAS.SUPP As 'SupplierNumber', ")
        Sql.Append("	RTrim(SUPMAS.NAME) As 'SupplierName', ")
        Sql.Append("	STKMAS.CTGY As 'HieCategory', ")
        Sql.Append("	RTrim(HIECAT.DESCR) As 'HieCategoryName', ")
        Sql.Append("	STKMAS.GRUP As 'HieGroup', ")
        Sql.Append("	STKMAS.SGRP As 'HieSubgroup', ")
        Sql.Append("	STKMAS.STYL As 'HieStyle',	 ")
        Sql.Append("	STKMAS.tact As 'ActivityToday', ")
        Sql.Append("	STKMAS.us001 As 'UnitSales1', ")
        Sql.Append("	STKMAS.us002 As 'UnitSales2', ")
        Sql.Append("	STKMAS.us003 As 'UnitSales3', ")
        Sql.Append("	STKMAS.us004 As 'UnitSales4', ")
        Sql.Append("	STKMAS.us005 As 'UnitSales5', ")
        Sql.Append("	STKMAS.us006 As 'UnitSales6', ")
        Sql.Append("	STKMAS.us007 As 'UnitSales7', ")
        Sql.Append("	STKMAS.us008 As 'UnitSales8', ")
        Sql.Append("	STKMAS.us009 As 'UnitSales9', ")
        Sql.Append("	STKMAS.us0010 As 'UnitSales10', ")
        Sql.Append("	STKMAS.us0011 As 'UnitSales11', ")
        Sql.Append("	STKMAS.us0012 As 'UnitSales12', ")
        Sql.Append("	STKMAS.us0013 As 'UnitSales13', ")
        Sql.Append("	STKMAS.us0014 As 'UnitSales14', ")
        Sql.Append("	STKMAS.do001 As 'DaysOutStock1',	 ")
        Sql.Append("	STKMAS.do002 As 'DaysOutStock2',	 ")
        Sql.Append("	STKMAS.do003 As 'DaysOutStock3',	 ")
        Sql.Append("	STKMAS.do004 As 'DaysOutStock4',	 ")
        Sql.Append("	STKMAS.do005 As 'DaysOutStock5',	 ")
        Sql.Append("	STKMAS.do006 As 'DaysOutStock6',	 ")
        Sql.Append("	STKMAS.do007 As 'DaysOutStock7',	 ")
        Sql.Append("	STKMAS.do008 As 'DaysOutStock8',	 ")
        Sql.Append("	STKMAS.do009 As 'DaysOutStock9',	 ")
        Sql.Append("	STKMAS.do0010 As 'DaysOutStock10',	 ")
        Sql.Append("	STKMAS.do0011 As 'DaysOutStock11',	 ")
        Sql.Append("	STKMAS.do0012 As 'DaysOutStock12',	 ")
        Sql.Append("	STKMAS.do0013 As 'DaysOutStock13', ")
        Sql.Append("	STKMAS.do0014 As 'DaysOutStock14', ")
        Sql.Append("	STKMAS.DateLastSoqInit, ")
        Sql.Append("	STKMAS.IsInitialised, ")
        Sql.Append("	STKMAS.ToForecast,	 ")
        Sql.Append("	STKMAS.FlierPeriod1, ")
        Sql.Append("	STKMAS.FlierPeriod2, ")
        Sql.Append("	STKMAS.FlierPeriod3, ")
        Sql.Append("	STKMAS.FlierPeriod4, ")
        Sql.Append("	STKMAS.FlierPeriod5, ")
        Sql.Append("	STKMAS.FlierPeriod6, ")
        Sql.Append("	STKMAS.FlierPeriod7, ")
        Sql.Append("	STKMAS.FlierPeriod8, ")
        Sql.Append("	STKMAS.FlierPeriod9, ")
        Sql.Append("	STKMAS.FlierPeriod10, ")
        Sql.Append("	STKMAS.FlierPeriod11, ")
        Sql.Append("	STKMAS.FlierPeriod12, ")
        Sql.Append("	STKMAS.DemandPattern, ")
        Sql.Append("	STKMAS.PeriodDemand, ")
        Sql.Append("	STKMAS.PeriodTrend, ")
        Sql.Append("	STKMAS.ErrorForecast, ")
        Sql.Append("	STKMAS.ErrorSmoothed, ")
        Sql.Append("	STKMAS.BufferConversion, ")
        Sql.Append("	STKMAS.BufferStock, ")
        Sql.Append("	STKMAS.ServiceLevelOverride, ")
        Sql.Append("	STKMAS.ValueAnnualUsage, ")
        Sql.Append("	STKMAS.OrderLevel, ")
        Sql.Append("	STKMAS.SaleWeightSeason, ")
        Sql.Append("	STKMAS.SaleWeightBank, ")
        Sql.Append("	STKMAS.SaleWeightPromo, ")
        Sql.Append("	(SELECT SUM(capacity) FROM PLANGRAM WHERE SKUN = STKMAS.SKUN) As 'Capacity' ")
        Sql.Append("From ")
        Sql.Append("	STKMAS ")
        Sql.Append("		Inner Join ")
        Sql.Append("			SUPMAS ")
        Sql.Append("		On ")
        Sql.Append("			STKMAS.SUPP=SUPMAS.SUPN ")
        Sql.Append("		Inner Join ")
        Sql.Append("			HIECAT ")
        Sql.Append("		On ")
        Sql.Append("			STKMAS.CTGY = HIECAT.NUMB ")
        Sql.Append("Where ")
        Sql.Append("	STKMAS.SUPP = ")
        Sql.Append("		( ")
        Sql.Append("			Select ")
        Sql.Append("				Top 1 (SUPN) ")
        Sql.Append("			From ")
        Sql.Append("				SUPMAS ")
        Sql.Append("			Where ")
        Sql.Append("				SUPMAS.DELC = 0 ")
        Sql.Append("            And ")
        Sql.Append("                SUPN In ")
        Sql.Append("                    ( ")
        Sql.Append("                        Select ")
        Sql.Append("                            Distinct(SUPP) ")
        Sql.Append("                        From ")
        Sql.Append("                            STKMAS ")
        Sql.Append("                        Where ")
        Sql.Append("                            IODT Is Null ")
        Sql.Append("                        And ")
        Sql.Append("                        	STKMAS.IDEL = 0 ")
        Sql.Append("                        And ")
        Sql.Append("                        	STKMAS.IOBS = 0 ")
        Sql.Append("                        And ")
        Sql.Append("                        	STKMAS.IRIS = 0 ")
        Sql.Append("                    ) ")
        Sql.Append("		) ")
        Sql.Append("And ")
        Sql.Append("	STKMAS.IDEL = 0 ")
        Sql.Append("And ")
        Sql.Append("	STKMAS.IOBS = 0 ")
        Sql.Append("And ")
        Sql.Append("	STKMAS.IRIS = 0 ")
        Sql.Append("And ")
        Sql.Append("	( ")
        Sql.Append("		STKMAS.IODT <= GetDate() ")
        Sql.Append("	Or ")
        Sql.Append("		STKMAS.IODT Is NULL ")
        Sql.Append("	) ")
        Sql.Append("ORDER BY ")
        Sql.Append("	STKMAS.CTGY, ")
        Sql.Append("	STKMAS.GRUP, ")
        Sql.Append("	STKMAS.SGRP, ")
        Sql.Append("	STKMAS.STYL, ")
        Sql.Append("	STKMAS.SKUN ")

        GetExistingSQL = Sql.ToString
    End Function

    Public Overrides Function GetNonExistingSQL() As String
        Dim Sql As New StringBuilder

        ' Return as supplier number for a supplier that does not have any stock items
        ' that fit the stock item selection criteria expected to be applied by the stored
        ' procedure, i.e. Null or 'today and earlier' IODT field value as well as the
        ' Obsolete, Deleted and Related Item Single flags set to 0.
        Sql.Append("Select ")
        Sql.Append("	Top 1 (SUPN) ")
        Sql.Append("From ")
        Sql.Append("	SUPMAS ")
        Sql.Append("Where ")
        Sql.Append("	SUPMAS.SUPN Not In ")
        Sql.Append("		( ")
        Sql.Append("			Select ")
        Sql.Append("				Distinct(SUPP) ")
        Sql.Append("			From ")
        Sql.Append("				STKMAS ")
        Sql.Append("		    Where  ")
        Sql.Append("	            STKMAS.IDEL = 0 ")
        Sql.Append("            And ")
        Sql.Append("	            STKMAS.IOBS = 0 ")
        Sql.Append("            And ")
        Sql.Append("	            STKMAS.IRIS = 0 ")
        Sql.Append("            And ")
        Sql.Append("	            ( ")
        Sql.Append("		            STKMAS.IODT <= GetDate() ")
        Sql.Append("	            Or ")
        Sql.Append("		            STKMAS.IODT Is NULL ")
        Sql.Append("	            ) ")
        Sql.Append("		) ")

        GetNonExistingSQL = Sql.ToString
    End Function

    Public Overrides Function GetExistingDataAndKey(ByRef ExistingData As System.Data.DataTable, ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL(), con)
                    Dim reader As SqlDataReader = com.ExecuteReader

                    ExistingData = New DataTable
                    ExistingData.Load(reader)
                    reader.Close()

                    If ExistingData.Rows.Count > 0 Then
                        Dim SNKey As New Key

                        With SNKey
                            .SPParamName = "SupplierNumber"
                            .DynamicSQLName = "SupplierNumber"
                            .Caption = "Supplier Number"
                            ' Supplier Number Should be same for all rows, so just get it from 1st one
                            .Value = ExistingData.Rows(0).Item(.DynamicSQLName)
                            .Type = FieldType.ftString
                        End With
                        Keys.Add(SNKey, SNKey.DynamicSQLName)
                        GetExistingDataAndKey = True
                    End If
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a existing Stock Item record(s) for any one Supplier.")
        End Try
    End Function

    Public Overrides Function GetKeyFieldsNoValueRequired(ByRef KeyFields As Microsoft.VisualBasic.Collection) As Boolean
        Dim SkunKey As New Key("", "SkuNumber", "Sku Number", Nothing, "SkuNumber", FieldType.ftString)

        KeyFields.Add(SkunKey, SkunKey.DynamicSQLName)
        GetKeyFieldsNoValueRequired = True
    End Function

    Public Overrides Function GetNonExistingKey(ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean
        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetNonExistingSQL(), con)
                    Dim ReturnedData As DataTable
                    Dim reader As SqlDataReader = com.ExecuteReader

                    ReturnedData = New DataTable
                    ReturnedData.Load(reader)
                    reader.Close()

                    ' SQL is returning top 1 row (supplier with no stock as per stock selection criteria)
                    If ReturnedData.Rows.Count = 1 Then
                        Dim SNKey As New Key("SupplierNumber", "SupplierNumber", "Supplier Number", ReturnedData.Rows(0).Item("SUPN"), FieldType.ftString)

                        Keys.Add(SNKey, SNKey.DynamicSQLName)
                        GetNonExistingKey = True
                    End If
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a supplier key for supplier with no stock.")
        End Try
    End Function

    Public Overrides Function GetExistingMultipleDataAndKey(ByRef ExistingMultipleData As System.Data.DataTable, ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL(), con)
                    Dim reader As SqlDataReader = com.ExecuteReader

                    ExistingMultipleData = New DataTable
                    ExistingMultipleData.Load(reader)
                    reader.Close()

                    If ExistingMultipleData.Rows.Count > 1 Then
                        Dim SNKey As New Key

                        With SNKey
                            .SPParamName = "SupplierNumber"
                            .DynamicSQLName = "SupplierNumber"
                            .Caption = "Supplier Number"
                            ' Supplier Number Should be same for all rows, so just get it from 1st one
                            .Value = ExistingMultipleData.Rows(0).Item(.DynamicSQLName)
                            .Type = FieldType.ftString
                        End With
                        Keys.Add(SNKey, SNKey.DynamicSQLName)
                        GetExistingMultipleDataAndKey = True
                    End If
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a multiple existing Stock Item records for any one Supplier.")
        End Try
    End Function

    Public Overrides ReadOnly Property StoredProcedureName() As String
        Get
            StoredProcedureName = "StockGetStocksBySupplier"
        End Get
    End Property

#End Region

#Region "Private Procedures And Functions"

    Private Sub CreateNewSupplier(ByVal SupplierID As String)

        Dim SB As New StringBuilder

        SB.Append("insert SUPMAS(SUPN, NAME, DELC, ODNO, QFLG, PalletCheck) ")
        SB.Append("       values('" & SupplierID & "', 'Test Supplier', 0, 0, 0, 0)")

        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Sql
                        com.CommandText = SB.ToString
                        com.ExecuteNonQuery()
                End Select
            End Using
        End Using

    End Sub

    Private Sub CreateNewStock(ByVal SupplierID As String, ByVal StockID As String)

        Dim SB As New StringBuilder

        SB.Append("insert STKMAS(SKUN, DESCR, SUPP, LABN, LABM, LABL, WGHT, VOLU, CTGY, GRUP, SGRP, STYL, ")
        SB.Append("              FlierPeriod1, FlierPeriod2, FlierPeriod3, FlierPeriod4, FlierPeriod5, FlierPeriod6, FlierPeriod7, ")
        SB.Append("              FlierPeriod8, FlierPeriod9, FlierPeriod10, FlierPeriod11, FlierPeriod12) ")
        SB.Append("       values('" & StockID & "', 'Test Stock', '" & SupplierID & "', 0, 0, 0, 0.0, 0.0, ")
        SB.Append("              '000003', '000000', '000000', '000000', '', '', '', '', '', '', '', '', '', '', '', '')")
        SB.Append("")

        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Sql
                        com.CommandText = SB.ToString
                        com.ExecuteNonQuery()
                End Select
            End Using
        End Using

    End Sub

    Private Sub DeleteStock(ByVal StockID As String)

        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Sql
                        com.CommandText = "delete STKMAS where SKUN = '" & StockID & "'"
                        com.ExecuteNonQuery()
                End Select
            End Using
        End Using

    End Sub

    Private Sub DeleteSupplier(ByVal SupplierID As String)

        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Sql
                        com.CommandText = "delete SUPMAS where SUPN = '" & SupplierID & "'"
                        com.ExecuteNonQuery()
                End Select
            End Using
        End Using

    End Sub

#End Region

End Class