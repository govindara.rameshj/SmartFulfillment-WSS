﻿Imports Microsoft.Practices.Unity
Imports Cts.Oasys.Core.SystemEnvironment
Imports Cts.Oasys.Core.Tests
Imports Cts.Oasys.Core

<TestClass()>
Public Class DISetup

    <AssemblyInitializeAttribute()>
    Public Shared Sub Setup(context As TestContext)
        DIContainer.Initialize(TestsDISetup.SetupConnectionToAatDb)
    End Sub

End Class
