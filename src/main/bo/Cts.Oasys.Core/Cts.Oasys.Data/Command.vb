﻿'<Assembly: Runtime.CompilerServices.InternalsVisibleTo("TpWickes.Repository.Implementation.UnitTest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
'                                                                                                              "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
'                                                                                                              "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
'                                                                                                              "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
'                                                                                                              "6da046ba")> 
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("SaveIBTOut_UnitTests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100874eb030e7d4f4" & _
                                                                                                              "a176a7f8bd0689e3d8c57fdc843ab2fd5ec17245298a81f62277dea9e602e14187fc2b39313059" & _
                                                                                                              "334c2a185c9f79113cc49ad446b5955090d35ed718b9ac99b03c9d6584a8134f7e349abc7478dd" & _
                                                                                                              "8f7c7f65be55c8173a3a919028641f634453434e3f26c90b936a97f316eb99891e810dd5b36082" & _
                                                                                                              "6da046ba")> 


Public Class Command
    Implements IDisposable
    Implements ICommand

    Private _dataProvider As DataProvider
    Friend _command As SqlCommand
    Private _commandOdbc As OdbcCommand

#Region "Constructors"

    Public Sub New(ByRef con As IConnection)

        _dataProvider = con.DataProvider

        Select Case con.DataProvider
            Case DataProvider.Odbc
                _commandOdbc = New OdbcCommand
                _commandOdbc.Connection = con.OdbcConnection
                _commandOdbc.CommandType = CommandType.Text
                _commandOdbc.CommandTimeout = 0
                If con.OdbcTransaction IsNot Nothing Then
                    _commandOdbc.Transaction = con.OdbcTransaction
                End If

            Case DataProvider.Sql
                _command = New SqlCommand
                _command.Connection = con.SqlConnection
                _command.CommandType = CommandType.Text
                _command.CommandTimeout = 0
                If con.SqlTransaction IsNot Nothing Then
                    _command.Transaction = con.SqlTransaction
                End If
        End Select

    End Sub

    Public Sub New(ByRef con As IConnection, ByVal storedProcedure As String)

        _dataProvider = con.DataProvider

        Select Case con.DataProvider
            Case DataProvider.Odbc
                _commandOdbc = New OdbcCommand
                _commandOdbc.Connection = con.OdbcConnection
                _commandOdbc.CommandType = CommandType.StoredProcedure
                _commandOdbc.CommandTimeout = 0
                _commandOdbc.CommandText = storedProcedure

                If con.OdbcTransaction IsNot Nothing Then
                    _commandOdbc.Transaction = con.OdbcTransaction
                End If

            Case DataProvider.Sql
                _command = New SqlCommand
                _command.Connection = con.SqlConnection
                _command.CommandType = CommandType.StoredProcedure
                _command.CommandTimeout = 0
                _command.CommandText = storedProcedure

                If con.SqlTransaction IsNot Nothing Then
                    _command.Transaction = con.SqlTransaction
                End If
        End Select

    End Sub

#End Region

#Region "Properties"

    Public Property StoredProcedureName() As String Implements ICommand.StoredProcedureName
        Get
            Select Case _dataProvider
                Case DataProvider.Odbc : Return _commandOdbc.CommandText
                Case DataProvider.Sql : Return _command.CommandText
                Case Else : Return String.Empty
            End Select
        End Get
        Set(ByVal value As String)
            Select Case _dataProvider
                Case DataProvider.Odbc
                    _commandOdbc.CommandText = value
                    _commandOdbc.CommandType = CommandType.StoredProcedure
                Case DataProvider.Sql
                    _command.CommandText = value
                    _command.CommandType = CommandType.StoredProcedure
            End Select
        End Set
    End Property

    Public Property CommandText() As String
        Get
            Select Case _dataProvider
                Case DataProvider.Odbc : Return _commandOdbc.CommandText
                Case DataProvider.Sql : Return _command.CommandText
                Case Else : Return String.Empty
            End Select
        End Get
        Set(ByVal value As String)
            Select Case _dataProvider
                Case DataProvider.Odbc
                    _commandOdbc.CommandText = value
                    _commandOdbc.CommandType = CommandType.Text
                Case DataProvider.Sql
                    _command.CommandText = value
                    _command.CommandType = CommandType.Text
            End Select
        End Set
    End Property

    Public ReadOnly Property ParameterCollectionCount() As Integer Implements ICommand.ParameterCollectionCount
        Get

            Return _command.Parameters.Count

        End Get
    End Property

    Public ReadOnly Property ParameterName(ByVal Index As Integer) As String Implements ICommand.ParameterName
        Get

            Return _command.Parameters.Item(Index).ParameterName

        End Get
    End Property

    Public ReadOnly Property ParameterTypeProperty(ByVal Index As Integer) As SqlDbType Implements ICommand.ParameterTypeProperty
        Get

            Return _command.Parameters.Item(Index).SqlDbType

        End Get
    End Property

    Public ReadOnly Property ParameterDirectionProperty(ByVal Index As Integer) As ParameterDirection Implements ICommand.ParameterDirectionProperty
        Get

            Return _command.Parameters.Item(Index).Direction

        End Get
    End Property

#End Region

#Region "Methods"

    Public Sub ClearParamters()
        If _commandOdbc IsNot Nothing Then _commandOdbc.Parameters.Clear()
        If _command IsNot Nothing Then _command.Parameters.Clear()
    End Sub

    Public Sub AddParameter(ByVal name As String, ByVal value As Object) Implements ICommand.AddParameter
        Select Case _dataProvider
            Case DataProvider.Odbc : _commandOdbc.Parameters.Add(New OdbcParameter(name, value))
            Case DataProvider.Sql : _command.Parameters.Add(New SqlParameter(name, value))
        End Select
    End Sub

    Public Sub AddParameter(ByVal name As String, ByVal value As Object, ByVal type As SqlDbType) Implements ICommand.AddParameter
        _command.Parameters.Add(New SqlParameter(name, type)).Value = value
    End Sub

    Public Sub AddParameter(ByVal name As String, ByVal value As Object, ByVal type As SqlDbType, ByVal size As Integer, ByVal direction As ParameterDirection)

        Dim param As New SqlParameter(name, type, size)
        param.Direction = direction
        param.Value = value

        _command.Parameters.Add(param)

    End Sub

    Public Sub AddParameter(ByVal name As String, ByVal value As Object, ByVal type As SqlDbType, ByVal size As Integer)
        _command.Parameters.Add(New SqlParameter(name, type, size)).Value = value
    End Sub

    Public Sub AddParameterOutput(ByVal name As String, ByVal type As System.Type, ByVal size As Integer)

        Select Case _dataProvider
            Case DataProvider.Odbc
                Dim dbtype As OdbcType = OdbcType.Char
                Select Case type.ToString
                    Case GetType(String).ToString
                    Case GetType(Boolean).ToString : dbtype = OdbcType.Bit
                    Case GetType(Date).ToString : dbtype = OdbcType.Date
                    Case GetType(Integer).ToString : dbtype = OdbcType.Int
                    Case GetType(Decimal).ToString : dbtype = OdbcType.Decimal
                End Select

                _commandOdbc.Parameters.Add(New OdbcParameter(name, dbtype, size)).Direction = ParameterDirection.Output

            Case DataProvider.Sql
                Dim dbtype As SqlDbType = SqlDbType.Char
                Select Case type.ToString
                    Case GetType(String).ToString
                    Case GetType(Boolean).ToString : dbtype = SqlDbType.Bit
                    Case GetType(Date).ToString : dbtype = SqlDbType.Date
                    Case GetType(Integer).ToString : dbtype = SqlDbType.Int
                    Case GetType(Decimal).ToString : dbtype = SqlDbType.Decimal
                End Select

                _command.Parameters.Add(New SqlParameter(name, dbtype, size)).Direction = ParameterDirection.Output
        End Select

    End Sub

    Public Function AddOutputParameter(ByVal name As String, ByVal type As SqlDbType, ByVal size As Integer) As SqlParameter Implements ICommand.AddOutputParameter

        Dim param As New SqlParameter(name, type, size)

        param.Direction = ParameterDirection.Output
        _command.Parameters.Add(param)

        Return param
    End Function

    Public Function AddOutputParameter(ByVal name As String, ByVal type As SqlDbType) As SqlParameter Implements ICommand.AddOutputParameter

        Dim param As New SqlParameter(name, type)

        param.Direction = ParameterDirection.Output
        _command.Parameters.Add(param)

        Return param
    End Function

    Public Function GetParameterValue(ByVal name As String) As Object Implements ICommand.GetParameterValue
        Select Case _dataProvider
            Case DataProvider.Odbc : Return _commandOdbc.Parameters(name).Value
            Case DataProvider.Sql : Return _command.Parameters(name).Value
            Case Else : Return Nothing
        End Select
    End Function

#End Region

#Region "Methods - Data Operations"

    Public Function ExecuteValue() As Object

        WriteTrace()
        Select Case _dataProvider
            Case DataProvider.Odbc : Return _commandOdbc.ExecuteScalar
            Case DataProvider.Sql : Return _command.ExecuteScalar
            Case Else : Return Nothing
        End Select

    End Function

    Public Function ExecuteDataTable() As System.Data.DataTable Implements ICommand.ExecuteDataTable

        WriteTrace()
        Dim dt As New DataTable

        Select Case _dataProvider
            Case DataProvider.Odbc
                If _commandOdbc.Connection.State <> ConnectionState.Open Then _commandOdbc.Connection.Open()
                Dim reader As OdbcDataReader = _commandOdbc.ExecuteReader()
                dt.Load(reader)
                reader.Close()
            Case DataProvider.Sql
                Dim reader As SqlDataReader = _command.ExecuteReader
                dt.Load(reader)
                reader.Close()
        End Select
        Return dt

    End Function

    Public Function ExecuteDataSet() As DataSet

        WriteTrace()
        Select Case _dataProvider
            Case DataProvider.Sql
                Dim ds As New DataSet

                Using da As New SqlDataAdapter(_command)
                    da.Fill(ds)
                End Using

                Return ds
            Case Else
                Return Nothing
        End Select

    End Function

    Public Function ExecuteNonQuery() As Integer Implements ICommand.ExecuteNonQuery

        WriteTrace()
        Dim linesAffected As Integer = 0

        Select Case _dataProvider
            Case DataProvider.Odbc : linesAffected = _commandOdbc.ExecuteNonQuery()
            Case DataProvider.Sql
                Select Case _command.CommandType
                    Case CommandType.StoredProcedure
                        Dim retValue As New SqlParameter("RetValue", SqlDbType.Int)
                        retValue.Direction = ParameterDirection.ReturnValue
                        _command.Parameters.Add(retValue)
                        _command.ExecuteNonQuery()
                        linesAffected = CInt(retValue.Value)
                    Case Else
                        linesAffected = _command.ExecuteNonQuery
                End Select
            Case Else : linesAffected = -1
        End Select

        Return linesAffected

    End Function

#End Region

#Region "Private Procedures & Functions"

    Private Sub WriteTrace()

        'output trace
        Dim sb As New StringBuilder()
        Select Case _dataProvider
            Case DataProvider.Odbc
                sb.Append(_commandOdbc.CommandText & Space(1))
                For Each param As OdbcParameter In _commandOdbc.Parameters
                    If param.Direction <> ParameterDirection.Input Then Continue For
                    If param.Value Is Nothing Then Continue For
                    sb.Append(param.ParameterName & "=" & param.Value.ToString & ",")
                Next

            Case DataProvider.Sql
                sb.Append(_command.CommandText & Space(1))
                For Each param As SqlParameter In _command.Parameters
                    If param.Direction <> ParameterDirection.Input Then Continue For
                    If param.Value Is Nothing Then Continue For
                    sb.Append(param.ParameterName & "=" & param.Value.ToString & ",")
                Next
        End Select

        sb.Remove(sb.Length - 1, 1)
        Trace.WriteLine(sb.ToString, Me.GetType.ToString)

    End Sub

#End Region

#Region " IDisposable Support "

    Private disposedValue As Boolean = False        ' To detect redundant calls

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                If _command IsNot Nothing Then _command.Dispose()
                If _commandOdbc IsNot Nothing Then _commandOdbc.Dispose()
            End If
        End If
        Me.disposedValue = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

End Class