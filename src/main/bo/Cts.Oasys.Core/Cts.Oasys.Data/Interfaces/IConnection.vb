﻿Public Interface IConnection
    Inherits IDisposable

    Sub InitialiseConnection()
    Function NewCommand() As Command
    Function NewCommand(ByVal storedProcedure As String) As Command
    ReadOnly Property DataProvider() As DataProvider
    Property SqlConnection() As SqlConnection
    ReadOnly Property SqlTransaction() As SqlTransaction
    Property OdbcConnection() As OdbcConnection
    ReadOnly Property OdbcTransaction() As OdbcTransaction
    Sub StartTransaction()
    Sub CommitTransaction()
    Sub RollbackTransaction()

End Interface
