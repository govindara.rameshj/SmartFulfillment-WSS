﻿Public Interface ICommand
    Inherits IDisposable

    Property StoredProcedureName() As String

    ReadOnly Property ParameterCollectionCount() As Integer
    ReadOnly Property ParameterName(ByVal Index As Integer) As String
    ReadOnly Property ParameterTypeProperty(ByVal Index As Integer) As SqlDbType
    ReadOnly Property ParameterDirectionProperty(ByVal Index As Integer) As ParameterDirection

    Sub AddParameter(ByVal Name As String, ByVal Value As Object)
    Sub AddParameter(ByVal name As String, ByVal value As Object, ByVal type As SqlDbType)

    Function AddOutputParameter(ByVal Name As String, ByVal Type As SqlDbType, ByVal Size As Integer) As SqlParameter
    Function AddOutputParameter(ByVal Name As String, ByVal Type As SqlDbType) As SqlParameter

    Function GetParameterValue(ByVal name As String) As Object

    Function ExecuteDataTable() As DataTable
    Function ExecuteNonQuery() As Integer

End Interface