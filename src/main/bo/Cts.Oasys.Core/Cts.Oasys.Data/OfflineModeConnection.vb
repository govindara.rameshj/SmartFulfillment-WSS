﻿Public Class OfflineModeConnection
    Inherits Connection

    Protected Overrides Sub InitialiseConnection()

        Try
            Select Case DataProvider
                Case DataProvider.Odbc
                    OdbcConnection = New OdbcConnection(GetOfflineConnectionString("odbc"))
                    OdbcConnection.Open()
                Case DataProvider.Sql
                    ' Hard coding so that Operations do not have to deploy another xml file, or deploy a changed existing xml file
                    SqlConnection = New SqlConnection(GetHardCodedOfflineConnectionString)
                    'SqlConnection = New SqlConnection(GetOfflineConnectionString("sqlconnection"))
                    SqlConnection.Open()
            End Select
        Catch ex As Exception
            Throw New Exception(My.Resources.Messages.CannotInitialiseOfflineConnection)
        End Try
    End Sub
End Class
