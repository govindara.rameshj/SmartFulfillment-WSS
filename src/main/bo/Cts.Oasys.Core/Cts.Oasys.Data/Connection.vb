﻿Public Class Connection
    Implements IDisposable
    Implements IConnection

    Protected DataProviderField As DataProvider = DataProvider.None
    Protected SqlConnectionField As SqlConnection = Nothing
    Private _sqlTransaction As SqlTransaction = Nothing
    Private _odbcConnection As OdbcConnection = Nothing
    Private _odbcTransaction As OdbcTransaction = Nothing

    Public Sub New()

        SetDataProvider()
        InitialiseConnection()
    End Sub

    Protected Overridable Sub SetDataProvider()

        DataProviderField = GetDataProvider()

    End Sub

    Protected Overridable Sub InitialiseConnection() Implements IConnection.InitialiseConnection

        Select Case DataProviderField
            Case DataProvider.Odbc
                _odbcConnection = New OdbcConnection(GetConnectionString("odbc"))
                _odbcConnection.Open()
            Case DataProvider.Sql
                SqlConnectionField = New SqlConnection(GetConnectionString("sqlconnection"))
                SqlConnectionField.Open()
        End Select
    End Sub

    Public Function NewCommand() As Command Implements IConnection.NewCommand
        Dim com As New Command(Me)
        Return com
    End Function

    Public Function NewCommand(ByVal storedProcedure As String) As Command Implements IConnection.NewCommand
        Dim com As New Command(Me, storedProcedure)
        Return com
    End Function

    Public ReadOnly Property DataProvider() As DataProvider Implements IConnection.DataProvider
        Get
            Return DataProviderField
        End Get
    End Property

    Protected Friend Property SqlConnection() As SqlConnection Implements IConnection.SqlConnection
        Get
            Return SqlConnectionField
        End Get
        Set(ByVal value As SqlConnection)
            SqlConnectionField = value
        End Set
    End Property

    Friend ReadOnly Property SqlTransaction() As SqlTransaction Implements IConnection.SqlTransaction
        Get
            Return _sqlTransaction
        End Get
    End Property

    Protected Friend Property OdbcConnection() As OdbcConnection Implements IConnection.OdbcConnection
        Get
            Return _odbcConnection
        End Get
        Set(ByVal value As OdbcConnection)
            _odbcConnection = value
        End Set
    End Property

    Friend ReadOnly Property OdbcTransaction() As OdbcTransaction Implements IConnection.OdbcTransaction
        Get
            Return _odbcTransaction
        End Get
    End Property

    Public Sub StartTransaction() Implements IConnection.StartTransaction
        Select Case DataProviderField
            Case DataProvider.Odbc : _odbcTransaction = _odbcConnection.BeginTransaction
            Case DataProvider.Sql : _sqlTransaction = SqlConnectionField.BeginTransaction
        End Select
    End Sub

    Public Sub CommitTransaction() Implements IConnection.CommitTransaction
        Select Case DataProviderField
            Case DataProvider.Odbc
                If _odbcTransaction IsNot Nothing Then _odbcTransaction.Commit()
                _odbcTransaction.Dispose()
            Case DataProvider.Sql
                If _sqlTransaction IsNot Nothing Then _sqlTransaction.Commit()
                _sqlTransaction.Dispose()
        End Select
    End Sub

    Public Sub RollbackTransaction() Implements IConnection.RollbackTransaction
        Select Case DataProviderField
            Case DataProvider.Odbc
                If _odbcTransaction IsNot Nothing Then _odbcTransaction.Rollback()
                _odbcTransaction.Dispose()
            Case DataProvider.Sql
                If _sqlTransaction IsNot Nothing Then _sqlTransaction.Rollback()
                _sqlTransaction.Dispose()
        End Select
    End Sub

#Region " IDisposable Support "
    Private disposedValue As Boolean = False        ' To detect redundant calls

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                If _sqlTransaction IsNot Nothing Then
                    _sqlTransaction.Dispose()
                End If

                If SqlConnectionField IsNot Nothing Then
                    If SqlConnectionField.State <> ConnectionState.Closed Then SqlConnectionField.Close()
                    SqlConnectionField.Dispose()
                End If

                If _odbcTransaction IsNot Nothing Then
                    _odbcTransaction.Dispose()
                End If

                If _odbcConnection IsNot Nothing Then
                    If _odbcConnection.State <> ConnectionState.Closed Then _odbcConnection.Close()
                    _odbcConnection.Dispose()
                End If
            End If
        End If
        Me.disposedValue = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class