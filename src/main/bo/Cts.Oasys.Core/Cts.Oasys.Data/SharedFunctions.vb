﻿Imports System.Xml
Imports System.Reflection
Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.SystemEnvironment

<HideModuleName()> Public Module SharedFunctions

    Private _strConnectionStrings As String = "configuration/connectionStrings/string"
    Private _offlineConnectionStrings As String = "configuration/offlineConnectionStrings/string"
    Private _strURLs As String = "configuration/urls/string"

    Private Function GetConfigPath() As String

        Dim resolver = New ConfigPathResolver()
        Dim result = resolver.ResolveConnectionXml

        If (String.IsNullOrEmpty(result)) Then
            Throw New Exception(My.Resources.Messages.NoConnectionFile)
        End If

        Return result

    End Function

    Private Function GetOfflineConfigPath() As String
        'get current location of this assembly
        Dim configFilename As String
        Dim codeBase As String = Assembly.GetExecutingAssembly().CodeBase
        Dim uriBuilder As New UriBuilder(codeBase)
        Dim assemblyPath As String = Uri.UnescapeDataString(uriBuilder.Path)

        configFilename = System.AppDomain.CurrentDomain.BaseDirectory & "Resources\OfflineConnection.xml"
        If IO.File.Exists(configFilename) Then
            Return configFilename
        Else
            configFilename = "C:\Program Files\CTS Retail\Oasys3\Resources\OfflineConnection.xml"
            If IO.File.Exists(configFilename) Then
                Return configFilename
            Else
                configFilename = "C:\Program Files\CTS\Oasys3\Resources\OfflineConnection.xml"
                If IO.File.Exists(configFilename) Then
                    Return configFilename
                Else
                    ''for development only
                    configFilename = "C:\Projects\OasysHW\VB.NET2008\Staging Area\Resources\OfflineConnection.xml"
                    If IO.File.Exists(configFilename) Then
                        Return configFilename
                    Else
                        ''Partha Dutta - 02/08/2010
                        ''Development location has moved
                        'Start change
                        configFilename = "C:\Projects\Dev\Back Office\Staging Area\Resources\OfflineConnection.xml"
                        If IO.File.Exists(configFilename) Then
                            Return configFilename
                        Else
                            Throw New Exception(My.Resources.Messages.NoConnectionFile)
                        End If
                        ''End change
                    End If
                End If
            End If
        End If
    End Function

    Public Function GetConnectionString() As String

        GetConnectionString = GetConnectionString("sqlconnection", False)
        If GetConnectionString.Length = 0 Then
            GetConnectionString = GetConnectionString("odbc", False)
        End If
    End Function

    Public Function GetConnectionString(ByVal configKey As String, Optional ByVal IfFailTryWithNoConfigKey As Boolean = True) As String

        If GlobalVars.IsTestEnvironment Then
            Dim env = GlobalVars.SystemEnvironment
            If "odbc".Equals(configKey, StringComparison.CurrentCultureIgnoreCase) Then
                Return env.GetOasysOdbcConnectionString()
            Else
                ' always return sqlconnection
                Return env.GetOasysSqlServerConnectionString()
            End If

        Else
            Try
                Trace.WriteLine("Get Connection String", "Cts.Oasys.Data.GetConnectionString")

                'loading config file
                Dim doc As XmlDocument = New XmlDocument()
                Dim ConfigPath As String = GetConfigPath()

                If ConfigPath.Length > 0 Then
                    doc.Load(ConfigPath)
                    Trace.WriteLine("Get Connection String: retrieving from " & ConfigPath, "Cts.Oasys.Data.GetConnectionString")

                    'search for odbc connection string
                    For Each node As XmlNode In doc.SelectNodes(_strConnectionStrings)
                        If String.Compare(node.Attributes.GetNamedItem("name").Value, configKey, True) = 0 Then
                            Trace.WriteLine("Get Connection String (" & configKey & ") " & node.Attributes.GetNamedItem("connectionString").Value, "Cts.Oasys.Data.GetConnectionString")
                            Return node.Attributes.GetNamedItem("connectionString").Value
                        End If
                    Next
                    If IfFailTryWithNoConfigKey Then
                        Return GetConnectionString()
                    Else
                        Throw New Exception(My.Resources.Messages.NoConnectionString)
                    End If
                Else
                    Throw New Exception(My.Resources.Messages.NoConnectionFile)
                End If
            Catch ex As System.IO.FileNotFoundException
                Throw New Exception(My.Resources.Messages.NoConnectionString)
            End Try
        End If
    End Function

    Public Function GetHardCodedOfflineConnectionString() As String

        GetHardCodedOfflineConnectionString = "Data Source=(local)\SQLExpress;Initial Catalog=Oasys;Integrated Security=True"
    End Function

    Public Function GetOfflineConnectionString() As String

        GetOfflineConnectionString = GetOfflineConnectionString("sqlconnection", False)
        If GetOfflineConnectionString.Length = 0 Then
            GetOfflineConnectionString = GetOfflineConnectionString("odbc", False)
        End If
    End Function

    Friend Function GetOfflineConnectionString(ByVal configKey As String, Optional ByVal IfFailTryWithNoConfigKey As Boolean = True) As String

        Try
            Trace.WriteLine("Get Connection String", "Cts.Oasys.Data.GetOfflineConnectionString")
            'loading config file
            Dim doc As XmlDocument = New XmlDocument()
            Dim ConfigPath As String = GetOfflineConfigPath()

            If ConfigPath.Length > 0 Then
                doc.Load(ConfigPath)
                Trace.WriteLine("Get Connection String: retrieving from " & ConfigPath, "Cts.Oasys.Data.GetOfflineConnectionString")
                'search for odbc connection string
                For Each node As XmlNode In doc.SelectNodes(_offlineConnectionStrings)
                    If node.Attributes.GetNamedItem("name").Value = configKey.ToLower Then
                        Trace.WriteLine("Get Connection String (" & configKey & ") " & node.Attributes.GetNamedItem("connectionString").Value, "Cts.Oasys.Data.GetOfflineConnectionString")
                        Return node.Attributes.GetNamedItem("connectionString").Value
                    End If
                Next
                'Prevent infinite loop of 1 sub calling another sub calling the 1st sub...
                If IfFailTryWithNoConfigKey Then
                    Return GetOfflineConnectionString()
                Else
                    Throw New Exception(My.Resources.Messages.NoConnectionString)
                End If
            Else
                Throw New Exception(My.Resources.Messages.NoConnectionFile)
            End If
        Catch ex As System.IO.FileNotFoundException
            Throw New Exception(My.Resources.Messages.NoConnectionString)
        End Try
    End Function

    Public Function GetDataProvider() As DataProvider

        If GlobalVars.IsTestEnvironment Then
            Dim env = GlobalVars.SystemEnvironment
            Return CType(IIf(env.IsOasysDataProviderSqlSever(), DataProvider.Sql, DataProvider.Odbc), DataProvider)
        Else
            Dim configFilename As String = GetConfigPath()
            Dim doc As XmlDocument = New XmlDocument()
            doc.Load(configFilename)

            'search for sql connection string
            For Each node As XmlNode In doc.SelectNodes(_strConnectionStrings)
                If node.Attributes.GetNamedItem("name").Value.ToLower = "sqlconnection" Then
                    Return DataProvider.Sql
                End If
            Next

            'search for odbc dsn name
            For Each node As XmlNode In doc.SelectNodes(_strConnectionStrings)
                If node.Attributes.GetNamedItem("name").Value.ToLower = "odbc" Then
                    Return DataProvider.Odbc
                End If
            Next

            Return DataProvider.None
        End If


    End Function

    Public Function GetUrl(ByVal name As String) As String

        Dim configFilename As String = GetConfigPath()
        Dim doc As XmlDocument = New XmlDocument()
        doc.Load(configFilename)

        'search for order manager url
        For Each node As XmlNode In doc.SelectNodes(_strURLs)
            If node.Attributes.GetNamedItem("name").Value.ToLower = name.ToLower Then
                Return node.Attributes.GetNamedItem("url").Value
            End If
        Next

        Throw New Exception(String.Format(My.Resources.Messages.NoUrl, name))

    End Function

    Public Function GetUrlOrderManager() As String

        Dim configFilename As String = GetConfigPath()
        Dim doc As XmlDocument = New XmlDocument()
        doc.Load(configFilename)

        'search for order manager url
        For Each node As XmlNode In doc.SelectNodes(_strURLs)
            If node.Attributes.GetNamedItem("name").Value.ToLower = "ordermanager" Then
                Return node.Attributes.GetNamedItem("url").Value
            End If
        Next

        Throw New Exception(My.Resources.Messages.NoUrlOrderManager)

    End Function

    Public Function GetCommunicationPath() As String

        'loadig config file
        Dim doc As XmlDocument = New XmlDocument()
        doc.Load(".\Resources\Connection.xml")

        'search for comms path
        For Each node As XmlNode In doc.SelectNodes("configuration/path")
            If node.Attributes.GetNamedItem("name").Value = "CommunicationsPath" Then
                Return node.Attributes.GetNamedItem("value").Value
            End If
        Next
        Throw New Exception("Communications path not found")
    End Function
End Module

Public Enum DataProvider
    None
    Sql
    Odbc
End Enum