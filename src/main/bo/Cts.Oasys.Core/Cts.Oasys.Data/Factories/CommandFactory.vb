﻿Public Class CommandFactory

    Private Shared m_FactoryMember As ICommand = Nothing

    Public Shared Function FactoryGet(ByRef Con As IConnection) As ICommand

        If m_FactoryMember Is Nothing Then
            Return New Command(Con)
        Else
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As ICommand)

        m_FactoryMember = obj
    End Sub
End Class
