﻿Public Class ConnectionFactory

    Private Shared m_FactoryMember As IConnection = Nothing

    Public Shared Function FactoryGet(ByVal IsInOfflineMode As Boolean) As IConnection

        If m_FactoryMember Is Nothing Then
            If IsInOfflineMode Then
                Return New OfflineModeConnection
            Else
                Return New Connection
            End If
        Else
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IConnection)

        m_FactoryMember = obj
    End Sub
End Class
