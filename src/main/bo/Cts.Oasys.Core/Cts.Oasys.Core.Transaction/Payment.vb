﻿Public Class Payment
    Inherits Base
    <ColumnMapping("TranDate")> Public Property TranDate As Date
    <ColumnMapping("TillId")> Public Property TillId As Integer
    <ColumnMapping("TranNumber")> Public Property TranNumber As String
    <ColumnMapping("Number")> Public Property Number As Integer

    Public Sub New()
        MyBase.New()
    End Sub

End Class

Public Class PaymentCollection
    Inherits BaseCollection(Of Payment)

    Public Sub LoadPayments(ByVal tranDate As Date, ByVal tillId As Integer, ByVal tranNumber As String)
        Dim dt As DataTable = DataAccess.GetPayments(tranDate, tillId, tranNumber)
        Me.Load(dt)
    End Sub

End Class