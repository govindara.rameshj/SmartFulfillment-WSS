﻿<HideModuleName()> Public Module SharedFunctions

    Public Function GetTransaction(ByVal tranDate As Date, ByVal tillId As Integer, ByVal tranNumber As String) As Transaction
        Dim dt As DataTable = DataAccess.GetTransaction(tranDate, tillId, tranNumber)
        If (dt IsNot Nothing) AndAlso (dt.Rows.Count > 0) Then
            Dim tran As New Transaction(dt.Rows(0))
            Return tran
        End If
        Return Nothing
    End Function

End Module

Public Class Transaction
    Inherits Base
    <ColumnMapping("TranDate")> Public Property TranDate As Date
    <ColumnMapping("TillId")> Public Property TillId As Integer
    <ColumnMapping("TranNumber")> Public Property TranNumber As String
    <ColumnMapping("OrderNumber")> Public Property OrderNumber As String
    <ColumnMapping("IsVoided")> Public Property IsVoided As Boolean
    <ColumnMapping("IsTraining")> Public Property IsTrainging As Boolean
    <ColumnMapping("IsComplete")> Public Property IsComplete As Boolean

    Private _lines As LineCollection = Nothing
    Private _payments As PaymentCollection = Nothing

    Public ReadOnly Property Lines As LineCollection
        Get
            If _lines Is Nothing Then LoadLines()
            Return _lines
        End Get
    End Property
    Public ReadOnly Property Payments As PaymentCollection
        Get
            If _payments Is Nothing Then LoadPayments()
            Return _payments
        End Get
    End Property


    Public Sub New()
        MyBase.New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

    Public Sub LoadLines()
        _lines = New LineCollection
        _lines.LoadLines(Me.TranDate, Me.TillId, Me.TranNumber)
    End Sub

    Public Sub LoadPayments()
        _payments = New PaymentCollection
        _payments.LoadPayments(Me.TranDate, Me.TillId, Me.TranNumber)
    End Sub

End Class
