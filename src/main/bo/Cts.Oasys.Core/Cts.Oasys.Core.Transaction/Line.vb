﻿Public Class Line
    Inherits Base
    <ColumnMapping("TranDate")> Public Property TranDate As Date
    <ColumnMapping("TillId")> Public Property TillId As Integer
    <ColumnMapping("TranNumber")> Public Property TranNumber As String
    <ColumnMapping("Number")> Public Property Number As Integer
    <ColumnMapping("SkuNumber")> Public Property SkuNumber As String
    <ColumnMapping("Quantity")> Public Property Quantity As Integer
    <ColumnMapping("Price")> Public Property Price As Decimal

    Public Sub New()
        MyBase.New()
    End Sub

End Class

Public Class LineCollection
    Inherits BaseCollection(Of Line)

    Public Sub LoadLines(ByVal tranDate As Date, ByVal tillId As Integer, ByVal tranNumber As String)
        Dim dt As DataTable = DataAccess.GetLines(tranDate, tillId, tranNumber)
        Me.Load(dt)
    End Sub

End Class