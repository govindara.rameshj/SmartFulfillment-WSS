﻿Imports Cts.Oasys.Data
Imports System.Text

Friend Module DataAccess

    Friend Function GetTransaction(ByVal tranDate As Date, ByVal tillId As Integer, ByVal tranNumber As String) As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand()
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        Dim sb As New StringBuilder
                        sb.Append("SELECT ")
                        sb.Append("DATE1 as TranDate, ")
                        sb.Append("TILL as TillId, ")
                        sb.Append("[TRAN] as TranNumber, ")
                        sb.Append("ORDN as OrderNumber, ")
                        sb.Append("VOID as IsVoided, ")
                        sb.Append("TMOD as IsTraining, ")
                        sb.Append("ICOM as IsComplete ")
                        sb.Append("FROM DLTOTS ")
                        sb.Append("WHERE DATE1=? AND TILL=? AND TRAN=? ")
                        com.CommandText = sb.ToString
                        com.AddParameter("Date", tranDate)
                        com.AddParameter("Till", tillId)
                        com.AddParameter("Tran", tranNumber)

                    Case DataProvider.Sql
                        com.StoredProcedureName = My.Resources.Procedures.TransactionGet
                        com.AddParameter(My.Resources.Parameters.TranDate, tranDate)
                        com.AddParameter(My.Resources.Parameters.TillId, tillId)
                        com.AddParameter(My.Resources.Parameters.TranNumber, tranNumber)
                End Select
                Return com.ExecuteDataTable

            End Using
        End Using
        Return Nothing

    End Function

    Friend Function GetLines(ByVal tranDate As Date, ByVal tillId As Integer, ByVal tranNumber As String) As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand()
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        Dim sb As New StringBuilder
                        sb.Append("SELECT ")
                        sb.Append("DATE1 as TranDate, ")
                        sb.Append("TILL as TillId, ")
                        sb.Append("[TRAN] as TranNumber, ")
                        sb.Append("NUMB as Number, ")
                        sb.Append("SKUN as SkuNumber, ")
                        sb.Append("QUAN as Quantity, ")
                        sb.Append("PRIC as Price ")
                        sb.Append("FROM DLLINE ")
                        sb.Append("WHERE DATE1=? AND TILL=? AND TRAN=? ")
                        sb.Append("ORDER BY NUMB")
                        com.CommandText = sb.ToString
                        com.AddParameter("Date", tranDate)
                        com.AddParameter("Till", tillId)
                        com.AddParameter("Tran", tranNumber)

                    Case DataProvider.Sql
                        com.StoredProcedureName = My.Resources.Procedures.TransactionLinesGet
                        com.AddParameter(My.Resources.Parameters.TranDate, tranDate)
                        com.AddParameter(My.Resources.Parameters.TillId, tillId)
                        com.AddParameter(My.Resources.Parameters.TranNumber, tranNumber)
                End Select
                Return com.ExecuteDataTable

            End Using
        End Using
        Return Nothing

    End Function

    Friend Function GetPayments(ByVal tranDate As Date, ByVal tillId As Integer, ByVal tranNumber As String) As DataTable

        Using con As New Connection
            Using com As Command = con.NewCommand()
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        Dim sb As New StringBuilder
                        sb.Append("SELECT ")
                        sb.Append("DATE1 as TranDate, ")
                        sb.Append("TILL as TillId, ")
                        sb.Append("[TRAN] as TranNumber, ")
                        sb.Append("NUMB as Number ")
                        sb.Append("FROM DLPAID ")
                        sb.Append("WHERE DATE1=? AND TILL=? AND TRAN=? ")
                        sb.Append("ORDER BY NUMB")
                        com.CommandText = sb.ToString
                        com.AddParameter("Date", tranDate)
                        com.AddParameter("Till", tillId)
                        com.AddParameter("Tran", tranNumber)

                    Case DataProvider.Sql
                        com.StoredProcedureName = My.Resources.Procedures.TransactionPaymentsGet
                        com.AddParameter(My.Resources.Parameters.TranDate, tranDate)
                        com.AddParameter(My.Resources.Parameters.TillId, tillId)
                        com.AddParameter(My.Resources.Parameters.TranNumber, tranNumber)
                End Select
                Return com.ExecuteDataTable

            End Using
        End Using
        Return Nothing

    End Function

End Module

