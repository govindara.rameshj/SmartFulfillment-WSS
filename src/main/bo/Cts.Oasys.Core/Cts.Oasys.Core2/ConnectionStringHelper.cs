using System;
using System.IO;
using System.Xml;

namespace Cts.Oasys.Core
{
    public class ConnectionStringHelper
    {
        public static string GetSqlConnectionString()
        {
            if (GlobalVars.IsTestEnvironment)
            {
                var env = GlobalVars.SystemEnvironment;
                return env.GetOasysSqlServerConnectionString();
            }
            else
            {
                var resolver = new ConfigPathResolver();
                var configPath = resolver.ResolveConnectionXml();

                if (string.IsNullOrEmpty(configPath) || !File.Exists(configPath))
                    throw new ApplicationException("Configuration file doesn't exist.");

                var doc = new XmlDocument();
                doc.Load(configPath);

                foreach (XmlNode node in doc.SelectNodes("configuration/connectionStrings/string"))
                {
                    if (string.Compare(node.Attributes.GetNamedItem("name").Value, "sqlconnection", true) == 0)
                        return node.Attributes.GetNamedItem("connectionString").Value;
                }

                throw new ApplicationException("Sql connection string isn't specified.");
            }
        }
    }
}
