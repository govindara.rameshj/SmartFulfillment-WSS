﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Cts.Oasys.Core.HierarchyNavigation
{
    class ChildCollectionNavigatorCreator<TParent, TChild> : IHierarchyNavigatorCreator<TChild>
    {
        private readonly HierarchyNavigator<TParent> parentContext;
        private readonly Expression<Func<TParent, IList<TChild>>> memberAccessExpression;
        private readonly int index;

        public ChildCollectionNavigatorCreator(
            HierarchyNavigator<TParent> parentContext,
            Expression<Func<TParent, IList<TChild>>> memberAccessExpression, int index)
        {
            this.parentContext = parentContext;
            this.memberAccessExpression = memberAccessExpression;
            this.index = index;
        }

        public HierarchyNavigator<T> Create<T>()
            where T : TChild
        {
            return parentContext.CreateChildNavigator<T, TChild>(memberAccessExpression, index);
        }
    }
}
