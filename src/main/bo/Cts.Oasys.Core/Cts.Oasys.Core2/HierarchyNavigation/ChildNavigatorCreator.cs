﻿using System;
using System.Linq.Expressions;

namespace Cts.Oasys.Core.HierarchyNavigation
{
    class ChildNavigatorCreator<TParent, TChild> : IHierarchyNavigatorCreator<TChild>
    {
        private readonly HierarchyNavigator<TParent> parentNavigator;
        private readonly Expression<Func<TParent, TChild>> memberAccessExpression;

        public ChildNavigatorCreator(
            HierarchyNavigator<TParent> parentNavigator,
            Expression<Func<TParent, TChild>> memberAccessExpression)
        {
            this.parentNavigator = parentNavigator;
            this.memberAccessExpression = memberAccessExpression;
        }

        public HierarchyNavigator<T> Create<T>()
            where T : TChild
        {
            return parentNavigator.CreateChildNavigator<T, TChild>(memberAccessExpression);
        }
    }
}
