namespace Cts.Oasys.Core.HierarchyNavigation
{
    public interface IHierarchyNavigatorCreator<in TBaseEntity>
    {
        HierarchyNavigator<T> Create<T>() where T : TBaseEntity;
    }
}