﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Cts.Oasys.Core.HierarchyNavigation.Context
{
    public abstract class HierarchyNavigationContext<TEntity>
    {
        private readonly HierarchyNavigator<TEntity> navigator;

        protected HierarchyNavigationContext(TEntity entity)
            : this(new HierarchyNavigator<TEntity>(entity))
        {
        }

        protected HierarchyNavigationContext(HierarchyNavigator<TEntity> navigator)
        {
            this.navigator = navigator;
        }

        public TEntity Entity
        {
            get { return navigator.Entity; }
        }

        protected HierarchyNavigator<TEntity> Navigator
        {
            get { return navigator; }
        }

        protected IHierarchyNavigationContextCreator<TChildEntity> GetChildContextCreator<TChildEntity>(
            Expression<Func<TEntity, IList<TChildEntity>>> memberAccessExpression, int index)
        {
            return new ChildCollectionItemContextCreator<TEntity, TChildEntity>(this, memberAccessExpression, index);
        }

        protected HierarchyNavigationContext<TChildEntity> CreateChildContext<TChildEntity>(
            Expression<Func<TEntity, IList<TChildEntity>>> memberAccessExpression, int index)
        {
            return CreateChildContext<TChildEntity, TChildEntity>(memberAccessExpression, index);
        }

        protected internal HierarchyNavigationContext<TResultEntity> CreateChildContext<TResultEntity, TChildEntity>(
            Expression<Func<TEntity, IList<TChildEntity>>> memberAccessExpression, int index)
            where TResultEntity : TChildEntity
        {
            var childNavigator = navigator.CreateChildNavigator<TResultEntity, TChildEntity>(memberAccessExpression, index);
            return InnerCreateChildContext(childNavigator);
        }

        protected HierarchyNavigationContext<TChildEntity> CreateChildContext<TChildEntity>(
            Expression<Func<TEntity, TChildEntity>> memberAccessExpression)
        {
            return CreateChildContext<TChildEntity, TChildEntity>(memberAccessExpression);
        }

        protected internal HierarchyNavigationContext<TResultEntity> CreateChildContext<TResultEntity, TChildEntity>(
            Expression<Func<TEntity, TChildEntity>> memberAccessExpression)
            where TResultEntity : TChildEntity
        {
            var childNavigator = navigator.CreateChildNavigator<TResultEntity, TChildEntity>(memberAccessExpression);
            return InnerCreateChildContext(childNavigator);
        }

        protected IHierarchyNavigationContextCreator<TChildEntity> GetChildContextCreator<TChildEntity>(
            Expression<Func<TEntity, TChildEntity>> memberAccessExpression)
        {
            return new ChildContextCreator<TEntity, TChildEntity>(this, memberAccessExpression);
        }

        protected abstract HierarchyNavigationContext<TChildEntity> InnerCreateChildContext<TChildEntity>(HierarchyNavigator<TChildEntity> childNavigator);
    }
}
