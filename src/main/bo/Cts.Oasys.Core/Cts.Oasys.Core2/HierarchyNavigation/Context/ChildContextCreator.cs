﻿using System;
using System.Linq.Expressions;

namespace Cts.Oasys.Core.HierarchyNavigation.Context
{
    class ChildContextCreator<TParent, TChild> : IHierarchyNavigationContextCreator<TChild>
    {
        private readonly HierarchyNavigationContext<TParent> parentContext;
        private readonly Expression<Func<TParent, TChild>> memberAccessExpression;

        public ChildContextCreator(
            HierarchyNavigationContext<TParent> parentContext,
            Expression<Func<TParent, TChild>> memberAccessExpression)
        {
            this.parentContext = parentContext;
            this.memberAccessExpression = memberAccessExpression;
        }

        public HierarchyNavigationContext<T> Create<T>()
            where T : TChild
        {
            return parentContext.CreateChildContext<T, TChild>(memberAccessExpression);
        }
    }
}
