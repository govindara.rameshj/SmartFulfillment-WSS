namespace Cts.Oasys.Core.HierarchyNavigation.Context
{
    public interface IHierarchyNavigationContextCreator<in TBaseEntity>
    {
        HierarchyNavigationContext<T> Create<T>() where T : TBaseEntity;
    }
}