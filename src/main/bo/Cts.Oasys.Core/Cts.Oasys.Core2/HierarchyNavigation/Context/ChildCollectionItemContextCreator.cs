﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Cts.Oasys.Core.HierarchyNavigation.Context
{
    class ChildCollectionItemContextCreator<TParent, TChild> : IHierarchyNavigationContextCreator<TChild>
    {
        private readonly HierarchyNavigationContext<TParent> parentContext;
        private readonly Expression<Func<TParent, IList<TChild>>> memberAccessExpression;
        private readonly int index;

        public ChildCollectionItemContextCreator(
            HierarchyNavigationContext<TParent> parentContext,
            Expression<Func<TParent, IList<TChild>>> memberAccessExpression, int index)
        {
            this.parentContext = parentContext;
            this.memberAccessExpression = memberAccessExpression;
            this.index = index;
        }

        public HierarchyNavigationContext<T> Create<T>()
            where T : TChild
        {
            return parentContext.CreateChildContext<T, TChild>(memberAccessExpression, index);
        }
    }
}
