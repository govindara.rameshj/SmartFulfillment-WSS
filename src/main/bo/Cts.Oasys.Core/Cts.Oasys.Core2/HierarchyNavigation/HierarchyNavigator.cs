﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Cts.Oasys.Core.HierarchyNavigation.Propertites;

namespace Cts.Oasys.Core.HierarchyNavigation
{
    public class HierarchyNavigator<TEntity>
    {
        private readonly TEntity entity;
        private readonly IPropertyAccessor<TEntity> parentProperty;

        public HierarchyNavigator(TEntity entity)
        {
            this.entity = entity;
        }

        public TEntity Entity
        {
            get { return entity; }
        }

        public string EntityPath
        {
            get
            {
                return parentProperty != null ? parentProperty.HierarchyFullName : typeof(TEntity).Name;
            }
        }

        public IPropertyAccessor<TEntity> ParentProperty
        {
            get { return parentProperty; }
        }

        private HierarchyNavigator(IPropertyAccessor<TEntity> parentProperty)
        {
            entity = parentProperty.GetValue();
            this.parentProperty = parentProperty;
        }

        public IHierarchyNavigatorCreator<TChildEntity> GetChildNavigatorCreator<TChildEntity>(
            Expression<Func<TEntity, IList<TChildEntity>>> memberAccessExpression, int index)
        {
            return new ChildCollectionNavigatorCreator<TEntity, TChildEntity>(this, memberAccessExpression, index);
        }

        public HierarchyNavigator<TChildEntity> CreateChildNavigator<TChildEntity>(
            Expression<Func<TEntity, IList<TChildEntity>>> memberAccessExpression, int index)
        {
            return CreateChildNavigator<TChildEntity, TChildEntity>(memberAccessExpression, index);
        }

        public HierarchyNavigator<TResultEntity> CreateChildNavigator<TResultEntity, TChildEntity>(
            Expression<Func<TEntity, IList<TChildEntity>>> memberAccessExpression, int index)
            where TResultEntity : TChildEntity
        {
            var property = GetPropertyAccessor<TChildEntity, TResultEntity>(memberAccessExpression, index);
            return new HierarchyNavigator<TResultEntity>(property);
        }

        public HierarchyNavigator<TChildEntity> CreateChildNavigator<TChildEntity>(
            Expression<Func<TEntity, TChildEntity>> memberAccessExpression)
        {
            return CreateChildNavigator<TChildEntity, TChildEntity>(memberAccessExpression);
        }

        public HierarchyNavigator<TResultEntity> CreateChildNavigator<TResultEntity, TChildEntity>(
            Expression<Func<TEntity, TChildEntity>> memberAccessExpression)
            where TResultEntity : TChildEntity
        {
            var property = GetPropertyAccessor<TChildEntity, TResultEntity>(memberAccessExpression);
            return new HierarchyNavigator<TResultEntity>(property);
        }

        public IHierarchyNavigatorCreator<TChildEntity> GetChildNavigatorCreator<TChildEntity>(
            Expression<Func<TEntity, TChildEntity>> memberAccessExpression)
        {
            return new ChildNavigatorCreator<TEntity, TChildEntity>(this, memberAccessExpression);
        }

        public IPropertyAccessor<TValue> GetPropertyAccessor<TValue>(Expression<Func<TEntity, TValue>> memberAccessExpression)
        {
            return GetPropertyAccessor<TValue, TValue>(memberAccessExpression);
        }

        public IPropertyAccessor<TResultValue> GetPropertyAccessor<TValue, TResultValue>(Expression<Func<TEntity, TValue>> memberAccessExpression)
            where TResultValue : TValue
        {
            return new PropertyAccessor<TEntity, TValue, TResultValue>(this, memberAccessExpression);
        }

        public IPropertyAccessor<TResultValue> GetPropertyAccessor<TValue, TResultValue>(Expression<Func<TEntity, IList<TValue>>> memberAccessExpression, int index)
            where TResultValue : TValue
        {
            return new PropertyElementAccessor<TEntity, TValue, TResultValue>(this, memberAccessExpression, index);
        }
    }
}
