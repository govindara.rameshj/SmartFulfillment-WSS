﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Cts.Oasys.Core.HierarchyNavigation.Propertites
{
    internal class PropertyElementAccessor<TEntity, TValue, TResultValue> : IPropertyAccessor<TResultValue>
        where TResultValue : TValue
    {
        private readonly HierarchyNavigator<TEntity> navigator;
        private readonly int index;
        private readonly PropertyInfo propertyInfo;
        private readonly IList<TValue> collection;

        internal PropertyElementAccessor(HierarchyNavigator<TEntity> navigator, Expression<Func<TEntity, IList<TValue>>> memberAccessExpression, int index)
        {
            this.navigator = navigator;
            this.index = index;
            propertyInfo = (PropertyInfo)((MemberExpression)memberAccessExpression.Body).Member;
            collection = (IList<TValue>)propertyInfo.GetValue(navigator.Entity, null);
        }

        public TResultValue GetValue()
        {
            return (TResultValue)collection[index];
        }

        public void SetValue(TResultValue value)
        {
            collection[index] = value;
        }

        public string Name
        {
            get
            {
                return propertyInfo.Name;
            }
        }

        public string HierarchyFullName
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("{0}.{1}", navigator.EntityPath, Name);
                sb.AppendFormat("[{0}]", index);
                return sb.ToString();
            }
        }
    }
}