namespace Cts.Oasys.Core.HierarchyNavigation.Propertites
{
    public interface IPropertyAccessor<TValue> : IPropertyAccessor
    {
        TValue GetValue();
        void SetValue(TValue value);
    }

    public interface IPropertyAccessor
    {
        string Name { get; }
        string HierarchyFullName { get; }
    }
}