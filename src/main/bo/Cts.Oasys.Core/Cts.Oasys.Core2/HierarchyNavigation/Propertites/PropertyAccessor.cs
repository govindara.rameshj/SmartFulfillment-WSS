﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Cts.Oasys.Core.HierarchyNavigation.Propertites
{
    internal class PropertyAccessor<TEntity, TValue, TResultValue> : IPropertyAccessor<TResultValue>
        where TResultValue : TValue
    {
        private readonly HierarchyNavigator<TEntity> navigator;
        private readonly PropertyInfo propertyInfo;

        internal PropertyAccessor(HierarchyNavigator<TEntity> navigator, Expression<Func<TEntity, TValue>> memberAccessExpression)
        {
            this.navigator = navigator;
            propertyInfo = (PropertyInfo)((MemberExpression)memberAccessExpression.Body).Member;
        }

        public TResultValue GetValue()
        {
            return (TResultValue)propertyInfo.GetValue(navigator.Entity, null);
        }

        public void SetValue(TResultValue value)
        {
            propertyInfo.SetValue(navigator.Entity, value, null);
        }

        public string Name
        {
            get
            {
                return propertyInfo.Name;
            }
        }

        public string HierarchyFullName
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("{0}.{1}", navigator.EntityPath, Name);
                return sb.ToString();
            }
        }
    }
}
