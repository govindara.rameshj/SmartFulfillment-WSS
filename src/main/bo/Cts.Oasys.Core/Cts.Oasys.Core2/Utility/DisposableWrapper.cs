﻿using System;

namespace Cts.Oasys.Core.Utility
{
    /// <summary>
    /// The sole purpose of this calss to conform with Sonar rule "MITRE, CWE-459 - Incomplete Cleanup".
    /// Some .NET classes like ManualResetEvent implements IDisposable privately, so it is not possible to call Dispose for them explicitly which violates the rule.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DisposableWrapper<T> : IDisposable
        where T: IDisposable
    {
        public DisposableWrapper(T value)
        {
            Value = value;
        }

        public T Value { get; private set; }

        public void Dispose()
        {
            Value.Dispose();
        }
    }
}
