﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Cts.Oasys.Core.Helpers
{
    public static class PropertyUtils
    {
        public static string GetName<TClassType, TMemberType>(Expression<Func<TClassType, TMemberType>> expression)
        {
            MemberExpression memberExpression = (MemberExpression)expression.Body;
            return (memberExpression.Member.Name);
        }

        public static PropertyInfo GetPropertyInfo<TClassType, TMemberType>(Expression<Func<TClassType, TMemberType>> expression)
        {
            MemberExpression memberExpression = (MemberExpression)expression.Body;
            return (PropertyInfo)memberExpression.Member;
        }

        private static Expression GetMemberInitExpression<T>(T obj, Func<PropertyInfo, bool> propertiesFilter = null)
        {
            Type objectType = typeof(T);

            IEnumerable<PropertyInfo> properties = objectType.GetProperties();
            if (propertiesFilter != null)
            {
                properties = properties.Where(propertiesFilter);
            }

            var bindings = properties
                .Select(p => Expression.Bind(p, Expression.Constant(p.GetValue(obj, null), p.PropertyType)))
                .Cast<MemberBinding>();

            return Expression.MemberInit(Expression.New(objectType), bindings);
        }

        public static Expression<Func<TEntityType, TMemberType>> GetAddToMemberExpression<TEntityType, TMemberType>(
            Expression<Func<TEntityType, TMemberType>> memberAccessExp,
            TMemberType addedValue)
        {
            // memberAccessExp is in form of x => x.Property
            // we need to return x => x.Property + addedValue
            var parameter = Expression.Parameter(typeof(TEntityType), "x");                     // x =>
            MemberInfo memberInfo = GetPropertyInfo(memberAccessExp);                           // Property
            var leftPart = Expression.MakeMemberAccess(parameter, memberInfo);                  // x.Property
            var rightPart = Expression.Constant(addedValue);                                    // addedValue
            var addition = Expression.AddChecked(leftPart, rightPart);                          // x.Property + addedValue
            var exp = Expression.Lambda<Func<TEntityType, TMemberType>>(addition, parameter);   // x => x.Property + addedValue
            return exp;
        }


    }
}
