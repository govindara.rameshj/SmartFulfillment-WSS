﻿using System;
using System.Globalization;

namespace Cts.Oasys.Core.Helpers
{
    public static class ConversionUtils
    {
        public static string TimeToOasysString(DateTime date)
        {
            return date.Hour.ToString("D2") + date.Minute.ToString("D2") + date.Second.ToString("D2");
        }

        public static TimeSpan OasysStringToTime(string timeString)
        {
            return DateTime.ParseExact(timeString, "HHmmss", CultureInfo.InvariantCulture).TimeOfDay;
        }

        public static string IncrementTransactionNumber(string transactionNumber)
        {
            return (Convert.ToInt32(transactionNumber) + 1).ToString("d4");
        }

        public static decimal CalculateDaysDifference(DateTime since, DateTime till)
        {
            return Convert.ToDecimal((till - since).TotalDays);
        }

        public static decimal GetDaysSinceStartOf1900(DateTime till)
        {
            return CalculateDaysDifference(new DateTime(1900, 1, 1), till);
        }

        public static string BooleanToYorN(bool value)
        {
            return value ? "Y" : "N";
        }

        public static string ToCharField(int value, int fieldSize)
        {
            return value.ToString("d" + fieldSize);
        }

        public static string IntValueToCharField(string value, int fieldSize)
        {
            return ToCharField(Int32.Parse(value), fieldSize);
        }

        public static string ToEventCode(string code)
        {
            return code.PadLeft(6, '0');
        }

        public static DateTime OasysStringToMonth(string value)
        {
            return DateTime.ParseExact(value, "MMyy", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal);
        }

        public static string MonthToOasysString(DateTime value)
        {
            return value.ToString("MMyy");
        }

        public static string GetCanonicalTillNumber(string tillNumber)
        {
            return tillNumber.PadLeft(2, '0');
        }
    }
}
