﻿using System;

namespace Cts.Oasys.Core.Helpers
{
    public static class AmountCalculationEngine
    {
        public static decimal CalculateVatValue(decimal totalAmount, decimal vatAmount)
        {
            return totalAmount - CalculateExVatValue(totalAmount, vatAmount);
        }

        public static decimal CalculateExVatValue(decimal totalAmount, decimal vatAmount)
        {
            return totalAmount - vatAmount;
        }

        public static decimal ApplyDiscountToValue(decimal value, decimal discountRate, decimal eventDiscountAmount = 0)
        {
            return Math.Round((value - eventDiscountAmount) * (1 - discountRate) + eventDiscountAmount, 2);
        }

        public static decimal CalculateDiscountAmount(decimal totalAmount, decimal discountRate)
        {
            return Math.Round(totalAmount * discountRate, 2);
        }
    }
}
