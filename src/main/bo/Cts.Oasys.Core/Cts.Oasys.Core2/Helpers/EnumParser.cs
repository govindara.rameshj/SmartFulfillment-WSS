﻿
using System;

namespace Cts.Oasys.Core.Helpers
{
    public static class EnumParser
    {
        // Parse string input as enum name, allows enum value parts to be delimeted by spaces
        public static T Parse<T>(string toParse, string wordsDelimeter ="")
        {
            return (T)Enum.Parse(typeof(T), toParse.Replace(" ", wordsDelimeter), true);
        }
    }
}
