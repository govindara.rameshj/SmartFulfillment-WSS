﻿
using System.Configuration;

namespace Cts.Oasys.Core.Helpers
{
    public static class ConfigurationHelper
    {
        public static bool GetAppSettingValue(string keyName, bool defaultValue)
        {
            var keyValue = ConfigurationManager.AppSettings[keyName];
            return keyValue != null ? bool.Parse(keyValue) : defaultValue;
        }

        public static int GetAppSettingValue(string keyName, int defaultValue)
        {
            var keyValue = ConfigurationManager.AppSettings[keyName];
            return keyValue != null ? int.Parse(keyValue) : defaultValue;
        }
    }
}
