﻿using System.Data;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Helpers
{
    public static class Mapper
    {
        public static void ObjectToDataRow(object obj, DataRow dataRow)
        {
            Map.ObjectToDataRow(obj, dataRow);
        }

        public static T ObjectToObject<T>(T source)
        {
            return Map.ObjectToObject<T>(source);
        }
    }
}
