﻿using System.Collections.Generic;
using System.Linq;

namespace Cts.Oasys.Core.Helpers
{
    public static class LinqExtensions
    {
        public static IEnumerable<TBase> ConcatCasted<TBase, TDerived>(this IEnumerable<TBase> source,
            IEnumerable<TDerived> y)
            where TDerived : TBase
        {
            return source.Concat(y.Cast<TBase>());
        }

        public static IEnumerable<T> ToEnumerable<T>(this T element)
            where T : class
        {
            return element == null ? Enumerable.Empty<T>() : new[] {element};
        }

        public static IEnumerable<int> EnumerateIndexes<T>(this IList<T> list)
        {
            return Enumerable.Range(0, list.Count());
        }
    }
}
