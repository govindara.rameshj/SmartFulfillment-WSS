﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public class ChangesHolder<T>
{
    public T CurrentState { get; private set;}
    public T PreviousState { get; private set; }
    public List<PropertyInfo> TrackingProperties { get; private set; }

    public ChangesHolder(T previousState, T currentState, List<PropertyInfo> trackingProperties)
    {
        CurrentState = currentState;
        PreviousState = previousState;
        TrackingProperties = trackingProperties;
    }

    public bool HasChanges()
    {
        var _hasChanges = false;
        foreach (PropertyInfo trackingProperty in TrackingProperties)
        {
            _hasChanges = HasChange(trackingProperty);
            if (_hasChanges)
            {
                break;
            }
        }
        return _hasChanges;
    }

    public bool HasChange(PropertyInfo property)
    {
        return Comparer.Default.Compare(GetValue(PreviousState, property), GetValue(CurrentState, property)) != 0;
    }

    private Type GetPropertyType(PropertyInfo property)
    {
        Type propertyType = property.PropertyType;
        Type underlyingType = Nullable.GetUnderlyingType(propertyType);
        if (underlyingType != null)
        {
            propertyType = underlyingType;
        }
        return propertyType;
    }

    private PT GetTypedValue<PT>(T instance, PropertyInfo property)
    {
        return (PT)Convert.ChangeType(GetValue(instance, property), typeof(PT));
    }

    private object GetValue(T instance, PropertyInfo property)
    {
        return property.GetValue(instance, null);
    }
}
