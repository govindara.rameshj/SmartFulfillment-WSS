﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using NLog.Config;
using NLog.Targets;
using System.IO;
using Cts.Oasys.Core.Helpers;
using OasysCBaseComRCW;

namespace Cts.Oasys.Core.Logger
{
    public class LogManager : IDisposable 
    {
        private static LogManager _instance;
        private static LogManager Instance
        {
            get { return _instance ?? (_instance = new LogManager()); }
        }

        private bool isEnabled = false;
        private DateTime startDate;
        private DateTime endDate;
        private int archiveSize;

        private NLog.Logger _logger;
        private ArchievedLogsZippingRoutine archievedLogsZippingRoutine;
        
        private LogManager()
        {
            try
            {
                string regValue = "";
                int regResult = 0;
                var reg = new RegistryClass();
                reg.GetCTSStringValue("Oasys\\2.0\\Wickes\\Enterprise", "WorkstationID", ref regValue, ref regResult);
                if (regResult != 0)
                    return;

                int tillNumber = Convert.ToInt32(regValue);
                var fileName = string.Format("Till-{0}", ConversionUtils.GetCanonicalTillNumber(tillNumber.ToString()));

                // Read parameters from DB
                var prm = new DbParameters();
                prm.RetrieveParameters(tillNumber);
                if (!prm.LoggingEnabled)
                    return;
                
                isEnabled = true;
                startDate = prm.LogStartDate;
                endDate = prm.LogEndDate;
                archiveSize = prm.LogArchiveSize;

                var config = new LoggingConfiguration();
                var target = new FileTarget();

                target.Encoding = Encoding.UTF8;
                target.FileName = Path.Combine(prm.LogFilePlace, string.Format("{0}.${{date:yyyy-MM-dd}}.log", fileName));
                target.Layout = "${time}    ${message}";

                // archiving
                target.ArchiveAboveSize = prm.LogFileSize * 1024 * 1024;
                target.ArchiveNumbering = ArchiveNumberingMode.Sequence;
                target.ArchiveFileName = Path.Combine(prm.LogFilePlace, string.Format("{0}.${{date:yyyy-MM-dd}}.${{date:HH-mm-ss}}.log", fileName));
                target.MaxArchiveFiles = prm.LogArchiveSize / prm.LogFileSize - 1; // If zipping does not work, it will limit overall archived log files

                config.AddTarget("file", target);
                config.LoggingRules.Add(new LoggingRule("*", LogLevel.Trace, target));

                NLog.LogManager.Configuration = config;

                _instance = this; // So archievedLogsZippingRoutine may use logging facilities

                archievedLogsZippingRoutine = new ArchievedLogsZippingRoutine(
                    new ArchievedLogsZipFile.Parameters 
                    {
                        LogsDirectory = prm.LogFilePlace,
                        ZipFileName = fileName + "-archive.zip",
                        ZipFileSizeLimit = (prm.LogArchiveSize - prm.LogFileSize * 3) * 1024 * 1024, // 1 active file and 2 files in case the speed of files generatoin by application is more then zipping speed.
                        LogFileNameFormat = string.Format("{0}.{{0:yyyy-MM-dd}}.log", fileName),
                        LogFileNameMask = string.Format("{0}.????-??-??.log", fileName),
                        ArchivedLogFileMask = string.Format("{0}.*.*.log", fileName),
                    },
                    prm.LogZippingPeriod);
                archievedLogsZippingRoutine.Start();
 
                // This class is used bu VB6, but it does not call Dispose, so dispose it during domain upload
                AppDomain.CurrentDomain.ProcessExit += (sender, eventArgs) => DisposeInstance(); 
            }
            catch { }
        }

        private void InnerWriteLog(string format, params object[] args)
        {
            try
            {
                if (!isEnabled || startDate > DateTime.Now)
                    return;

                if (endDate < DateTime.Now)
                {
                    isEnabled = false;
                    return;
                }

                if (_logger == null)
                    _logger = NLog.LogManager.GetCurrentClassLogger();

                _logger.Info(format, args);
            }
            catch { }
        }

        public static void WriteLog(string format, params object[] args)
        {
            Instance.InnerWriteLog(format, args);
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (archievedLogsZippingRoutine != null)
            {
                archievedLogsZippingRoutine.Stop();
            }
        }

        #endregion

        private static void DisposeInstance()
        {
            if (_instance != null)
            {
                _instance.Dispose();
                _instance = null;
            }
        }

    }
}
