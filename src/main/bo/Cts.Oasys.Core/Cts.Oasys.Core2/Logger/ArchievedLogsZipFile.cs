﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Ionic.Zip;

namespace Cts.Oasys.Core.Logger
{
    class ArchievedLogsZipFile
    {
        private const string LogContextName = "[ArchievedLogsZipFile] ";

        public class Parameters
        {
            public string LogsDirectory;
            public string ZipFileName;
            public string LogFileNameMask;
            public string LogFileNameFormat;
            public string ArchivedLogFileMask;
            public long ZipFileSizeLimit;
        }

        private readonly Parameters parameters;

        public ArchievedLogsZipFile(Parameters parameters)
        {
            this.parameters = parameters;
        }

        public void ProcessArchivedFiles()
        {
            LogManager.WriteLog(LogContextName + "Start processing archived log files in directory [{0}]", parameters.LogsDirectory);
            LogManager.WriteLog(LogContextName + "Archive logs file mask [{0}]", parameters.ArchivedLogFileMask);

            var archivedLogFiles = FindArchivedLogFiles();

            if (archivedLogFiles.Any())
            {
                var zipFilePath = Path.Combine(parameters.LogsDirectory, parameters.ZipFileName);
                ZipFile zipFile = new ZipFile(zipFilePath);

                var zipFileInfo = new FileInfo(zipFilePath);
                if (zipFileInfo.Exists)
                {
                    var zipFileSize = zipFileInfo.Length;
                    LogManager.WriteLog(LogContextName + "Current zip file size {0}", zipFileSize);

                    LogManager.WriteLog(LogContextName + "Start of average ratio calculation");
                    var averageRatio = zipFile.Entries.Average(entry => (double)entry.CompressedSize / entry.UncompressedSize); // ratio is in form 98%, which means file has been shrinked by 98%. So math ratio CompressedSize/OriginalSize is 2% here
                    LogManager.WriteLog(LogContextName + "End of average ratio calculation. Ration = {0}", averageRatio);

                    var newFilesSize = archivedLogFiles.Sum(fileInfo => fileInfo.Length);
                    var approximatedNewFileCompressedSize = (long)Math.Round(averageRatio * newFilesSize * 1.2); // Behave pessimistically - add additional 20% in case if the new files would be harder to compress.

                    var freeSpace = parameters.ZipFileSizeLimit - zipFileSize;
                    if (approximatedNewFileCompressedSize > freeSpace)
                    {
                        CleanUpSpace(zipFile, approximatedNewFileCompressedSize - freeSpace);
                        zipFileInfo.Refresh();
                        LogManager.WriteLog(LogContextName + "New zip file size after clean up {0}, cleaned up {1}", zipFileInfo.Length, zipFileSize - zipFileInfo.Length);
                    }
                }
                else
                {
                    LogManager.WriteLog(LogContextName + "No zip file found, it will be created");
                }

                MoveFilesToZip(zipFile, archivedLogFiles);
                LogManager.WriteLog(LogContextName + "New zip file size {0}", zipFileInfo.Length);
            }
            else
            {
                LogManager.WriteLog(LogContextName + "No archived log files found");
            }

            LogManager.WriteLog(LogContextName + "End processing archived log files");
        }

        private void MoveFilesToZip(ZipFile zipFile, IList<FileInfo> archivedLogFiles)
        {
            foreach (var fileInfo in archivedLogFiles)
            {
                LogManager.WriteLog(LogContextName + "Start archiving [{0}]", fileInfo.FullName);

                if (zipFile.Entries.Any(entry => entry.FileName == fileInfo.Name))
                {
                    zipFile.RemoveEntry(fileInfo.Name);
                    LogManager.WriteLog(LogContextName + "The file [{0}] is already in the zip. It will be overwritten.", fileInfo.Name);
                }

                zipFile.AddFile(fileInfo.FullName, String.Empty);
                zipFile.Save();
                fileInfo.Delete();
                LogManager.WriteLog(LogContextName + "End archiving [{0}]", fileInfo.FullName);
            }
        }

        private void CleanUpSpace(ZipFile zipFile, long spaceToClean)
        {
            var sortedEntries = zipFile.Entries.OrderBy(entry => entry.LastModified);
            
            long cleanedSpace = 0;
            var entriesToRemove = sortedEntries.TakeWhile(entry => {
                if (cleanedSpace >= spaceToClean)
                {
                    return false;
                }
                else
                {
                    LogManager.WriteLog(LogContextName + "Set [{0}] file to remove from zip", entry.FileName);
                    cleanedSpace += entry.CompressedSize;
                    return true;
                }
            }).Select(entry => entry.FileName).ToList();

            zipFile.RemoveEntries(entriesToRemove);
            zipFile.Save();
            LogManager.WriteLog(LogContextName + "Files have been removed from zip");
        }

        private IList<FileInfo> FindArchivedLogFiles()
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(parameters.LogsDirectory);

            var rotated = directoryInfo.GetFiles(parameters.ArchivedLogFileMask).ToList();

            var activeLogFileName = String.Format(parameters.LogFileNameFormat, DateTime.Now);

            var obsoleteNotRotated = directoryInfo.GetFiles(parameters.LogFileNameMask).Where(fileInfo => fileInfo.Name != activeLogFileName).ToList();

            return rotated.Union(obsoleteNotRotated).ToList();
        }
    }
}
