﻿using System;
using System.Threading;
using Cts.Oasys.Core.Utility;

namespace Cts.Oasys.Core.Logger
{
    class ArchievedLogsZippingRoutine : IDisposable
    {
        private const string LogContextName = "[ArchievedLogsZippingRoutine] ";

        private readonly ArchievedLogsZipFile zipFile;
        private Thread workThread;
        private readonly DisposableWrapper<ManualResetEvent> stopEvent = new DisposableWrapper<ManualResetEvent>(new ManualResetEvent(false));
        private readonly TimeSpan waitPeriod;

        public ArchievedLogsZippingRoutine(ArchievedLogsZipFile.Parameters parameters, TimeSpan waitPeriod)
        {
            this.waitPeriod = waitPeriod;
            zipFile = new ArchievedLogsZipFile(parameters); 
        }

        public void WorkRoutine()
        {
            bool stop = false;

            while (!stop)
            {
                try
                {
                    LogManager.WriteLog(LogContextName + "Start processing archived log files work routine");

                    while (!stop)
                    {
                        bool stopped = stopEvent.Value.WaitOne(waitPeriod);

                        if (stopped)
                        {
                            stop = true;
                        }
                        else
                        {
                            zipFile.ProcessArchivedFiles();
                        }
                    }

                    LogManager.WriteLog(LogContextName + "End processing archived log files work routine");
                }
                catch (Exception ex)
                {
                    LogManager.WriteLog(LogContextName + "Unhandled error occured - {0}", ex);
                }
            }
        }

        public void Start()
        {
            LogManager.WriteLog(LogContextName + "Begin starting work thread");

            stopEvent.Value.Reset();
            workThread = new Thread(WorkRoutine);
            workThread.Start();

            LogManager.WriteLog(LogContextName + "Work thread started");
        }

        public void Stop()
        {
            LogManager.WriteLog(LogContextName + "Stop event received");

            stopEvent.Value.Set();
            workThread.Join(5000);
            workThread = null;

            LogManager.WriteLog(LogContextName + "Work thread Stoped");
        }

        public void Dispose()
        {
            stopEvent.Dispose();
        }
    }
}
