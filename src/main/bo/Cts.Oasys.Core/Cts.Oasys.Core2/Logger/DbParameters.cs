﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using BLToolkit.Data;
using System.Data;

namespace Cts.Oasys.Core.Logger
{
    public class DbParameters
    {
        const int LogFilePlacePrmID = 5500;
        const int LogFileSizePrmID = 5501;
        const int LogArchiveSizePrmID = 5502;
        const int LogStartDatePrmID = 5503;
        const int LogEndDatePrmID = 5504;
        const int LogTillsPrmID = 5505;
        const int LogZippingPeriodPrmID = 5506;

        public bool LoggingEnabled = false;
        public string LogFilePlace = string.Empty;
        public int LogFileSize = 0;
        public int LogArchiveSize = 0;
        public DateTime LogStartDate = DateTime.MinValue;
        public DateTime LogEndDate = DateTime.MaxValue;
        private string LogTills = string.Empty;
        public TimeSpan LogZippingPeriod = new TimeSpan(0, 1, 0);

        public void RetrieveParameters(int tillNumber)
        {
            using (var db = new DbManager(new SqlConnection(ConnectionStringHelper.GetSqlConnectionString())))
            {
                var prmList = db.SetCommand("SELECT [ParameterID], [StringValue] FROM [dbo].[Parameters] WHERE [ParameterID] BETWEEN @start AND @end",
                    db.Parameter("@start", LogFilePlacePrmID, DbType.Int32),
                    db.Parameter("@end", LogTillsPrmID, DbType.Int32)).ExecuteList<DbParameter>();

                foreach (var prm in prmList)
                {
                    switch (prm.ParameterID)
                    {
                        case LogFilePlacePrmID:
                            LogFilePlace = prm.StringValue;
                            break;
                        case LogFileSizePrmID:
                            LogFileSize = Convert.ToInt32(prm.StringValue);
                            break;
                        case LogArchiveSizePrmID:
                            LogArchiveSize = Convert.ToInt32(prm.StringValue);
                            break;
                        case LogStartDatePrmID:
                            LogStartDate = Convert.ToDateTime(prm.StringValue);
                            break;
                        case LogEndDatePrmID:
                            LogEndDate = Convert.ToDateTime(prm.StringValue);
                            break;
                        case LogTillsPrmID:
                            LogTills = prm.StringValue;
                            break;
                        case LogZippingPeriodPrmID:
                            try
                            {
                                LogZippingPeriod = TimeSpan.Parse(prm.StringValue);
                            }
                            catch
                            {
                                // irnore format exception, use default value in such cases
                            }
                            break;
                    }
                }

                // Logging disabled
                if (LogEndDate < DateTime.Now)
                    return;

                if (string.IsNullOrEmpty(LogFilePlace))
                    LogFilePlace = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Wickes\\Logs");
                if (LogFileSize == 0)
                    LogFileSize = 2;
                if (LogArchiveSize == 0)
                    LogArchiveSize = 500;
                if (string.IsNullOrEmpty(LogTills))
                    LoggingEnabled = true;

                var tillList = new List<String>(LogTills.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries));
                if (!string.IsNullOrEmpty(tillList.Find(s => Convert.ToInt32(s) == tillNumber)))
                    LoggingEnabled = true;
            }
        }
    }
}
