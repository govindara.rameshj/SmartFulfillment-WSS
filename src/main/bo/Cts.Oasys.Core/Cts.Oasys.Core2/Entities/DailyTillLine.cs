﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("DLLINE")]
    public class DailyTillLine
    {
        [MapField("NUMB")]
        public int SequenceNumber { get; set; }

        [MapField("SKUN")]
        public string SKU { get; set; }
         
        [MapField("QUAN")]
        public decimal Quantity { get; set; }        
  
        [MapField("PRIC")]
        public decimal Price { get; set; } 
 
        [MapField("DATE1")]
        public DateTime Date { get; set; }

        [MapField("TILL")]
        public string TillId { get; set; }

        [MapField("TRAN")]
        public string TransactionNumber { get; set; }
    }
}
