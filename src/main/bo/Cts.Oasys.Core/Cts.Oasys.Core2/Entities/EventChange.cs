﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("EVTCHG")]
    public class EventChange
    {
        [MapField("SKUN")]
        public string SKU { get; set; }
        [MapField("SDAT")]
        public DateTime StartDate { get; set; }
        [MapField("PRIO")]
        public string Priority { get; set; }
        [MapField("NUMB")]
        public string EventNumber { get; set; }
        [MapField("EDAT")]
        public DateTime? EndingDate { get; set; }
        [MapField("PRIC")]
        public decimal Price { get; set; }
        [MapField("IDEL")]
        public bool IsDeleted { get; set; }
    }
}
