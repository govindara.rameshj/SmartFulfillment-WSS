﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("CARD_LIST")]
    public class CardList
    {
        [MapField("DELETED")]
        public bool IsDeleted;

        [MapField("Card_NO")]
        public string CardNumber;

        [MapField("CUST_NAME")]
        public string CustomerName;
    }
}
