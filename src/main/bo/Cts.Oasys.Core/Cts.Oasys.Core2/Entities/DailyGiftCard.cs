﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System.Data;

namespace Cts.Oasys.Core.Entities
{
    [TableName("DLGIFTCARD")]
    public class DailyGiftCard
    {
        [MapField("DATE1")]
        public DateTime Date { get; set; }  

        [MapField("AMNT")]
        public decimal TenderAmount { get; set; }  

        [MapField("TYPE")]
        public string TransactionType { get; set; }   
       
        [MapField("TRAN")]
        public string TransactionNumber  { get; set; } 
    }
}
