﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("SYSNID")]
    public class NetworkIDSystem
    {
        [MapField("DATE1")]
        public DateTime sysDate { get; set; }
    }
}
