﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("SUPMAS")]
    public class SupplierMaster
    {
        [MapField("SUPN")]
        public string SupplierNumber { get; set; }
        [MapField("NAME")]
        public string SupplierName { get; set; }
    }
}
