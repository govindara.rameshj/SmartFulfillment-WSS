﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("DLCOMM")]
    public class Dlcomm
    {
        [MapField("DATE1")]
        public DateTime Date { get; set; }

        [MapField("TOKENID")]
        public string CommideaAccountId { get; set; }

        [MapField("TILL")]
        public string TillId { get; set; }

        [MapField("TRAN")]
        public string TransactionNumber { get; set; }
    }
}
