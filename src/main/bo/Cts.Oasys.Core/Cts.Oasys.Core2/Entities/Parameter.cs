﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("Parameters")]
    public class Parameter
    {
        // not all fields are here

        [PrimaryKey, Identity]
        [MapField("ParameterID")]
        public int Id;

        [MapField("StringValue")]
        public string StringValue;

        [MapField("LongValue")]
        public int LongValue;

        [MapField("Description")]
        public string Description;

        [MapField("BooleanValue")]
        public bool BooleanValue;

        [MapField("DecimalValue")]
        public Decimal DecimalValue;
    }
}
