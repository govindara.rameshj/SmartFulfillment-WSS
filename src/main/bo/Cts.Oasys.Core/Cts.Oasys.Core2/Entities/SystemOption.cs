﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("SYSOPT")]
    public class SystemOption
    {
        [MapField("STOR")]
        public string StoreNumber { get; set; }
    }
}
