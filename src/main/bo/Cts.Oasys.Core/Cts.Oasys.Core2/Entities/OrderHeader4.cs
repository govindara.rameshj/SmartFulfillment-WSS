﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("CORHDR4")]
    public class OrderHeader4
    {
        [MapField("DeliveryStatus")]
        public int DeliveryStatus { get; set; }

        [MapField("RefundStatus")]
        public int RefundStatus { get; set; }

        [MapField("OMOrderNumber")]
        public int OMOrderNumber { get; set; }

        [MapField("NUMB")]
        public string OrderNumber { get; set; }   
    }
}
