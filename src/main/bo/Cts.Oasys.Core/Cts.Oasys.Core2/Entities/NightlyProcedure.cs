﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("NITMAS")]
    public class NightlyProcedure
    {
        [MapField("NSET")]
        public string SetNumber { get; set; }

        [MapField("TASK")]
        public string TaskNumber { get; set; }

        [MapField("DESCR")]
        public string Description { get; set; }

        [MapField("PROG")]
        public string ProgramToRun { get; set; }

        [MapField("NITE")]
        public bool RunInNight { get; set; }

        [MapField("RETY")]
        public bool RunInRetry { get; set; }

        [MapField("WEEK")]
        public bool RunInWeekOnly { get; set; }

        [MapField("PEND")]
        public bool RunInPeriodEndOnly { get; set; }

        [MapField("LIVE")]
        public bool RunBeforeStoreIsLive { get; set; }

        [MapField("OPTN")]
        public int OptionNumber { get; set; }

        [MapField("ABOR")]
        public bool Abort { get; set; }

        [MapField("JRUN")]
        public string RunStatement { get; set; }

        [MapField("DAYS1")]
        public bool RunOnDays1 { get; set; }

        [MapField("DAYS2")]
        public bool RunOnDays2 { get; set; }

        [MapField("DAYS3")]
        public bool RunOnDays3 { get; set; }

        [MapField("DAYS4")]
        public string RunOnDays4  { get; set; }

        [MapField("DAYS5")]
        public bool RunOnDays5 { get; set; }

        [MapField("DAYS6")]
        public bool RunOnDays6 { get; set; }

        [MapField("DAYS7")]
        public bool RunOnDays7 { get; set; }

        [MapField("BEGD")]
        public DateTime BeginningDateValidToRun  { get; set; }

        [MapField("ENDD")]
        public DateTime EndingDateValidToRun { get; set; }

        [MapField("SDAT")]
        public DateTime LastDateRunStarted { get; set; }

        [MapField("STIM")]
        public string StartedTimeAsHhmmss  { get; set; }

        [MapField("EDAT")]
        public DateTime LastDateRunEndedOk { get; set; }

        [MapField("ETIM")]
        public string EndingTimeAsHhmmss { get; set; }

        [MapField("ADAT")]
        public DateTime LastAbortDate { get; set; }

        [MapField("ATIM")]
        public string LastAbortTimeAsHhmmss { get; set; }

        [MapField("TTYPE")]
        public int TypeOfApplication { get; set; }
    }
}
