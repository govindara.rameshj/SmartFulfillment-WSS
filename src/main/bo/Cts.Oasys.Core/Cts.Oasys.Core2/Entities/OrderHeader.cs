﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("CORHDR")]
    public class OrderHeader
    {
        [MapField("NUMB")]
        public string OrderNumber { get; set; }  
    }
}
