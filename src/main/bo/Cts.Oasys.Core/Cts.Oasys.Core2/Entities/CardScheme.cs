﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("CARD_SCHEME")]
    public class CardScheme
    {
        [MapField("DISCOUNT")]
        public decimal Discount { get; set; }

        [MapField("SCHEME_NAME")]
        public string Name { get; set; }
    }
}
