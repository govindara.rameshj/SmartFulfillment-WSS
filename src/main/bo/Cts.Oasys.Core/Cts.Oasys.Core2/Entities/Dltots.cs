﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("DLTOTS")]
    public class Dltots
    {
        // not all fields are here
        public DateTime Date1 { get; set; }
        public string Tran { get; set; }
        public string Till { get; set; }
        public DateTime ReceivedDate { get; set; }

        [MapField("TOTL")]
        public decimal TotalSaleAmount { get; set; }

        [MapField("TCOD")]
        public string TransactionCode { get; set;}
    }
}
