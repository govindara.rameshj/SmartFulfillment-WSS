﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("skuword")]
    public class SkuWord
    {
        // not all fields are here

        [MapField("Id")]
        public string Id { get; set; }
    }
}
