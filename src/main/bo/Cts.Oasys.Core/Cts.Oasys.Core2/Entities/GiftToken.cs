﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("GIFHOT")]
    public class GiftToken
    {
        [MapField("SERI")]
        public string SerialNumber { get; set; }
        
        [MapField("TEXT")]
        public string RedemptionInfo { get; set; }
        
        [MapField("HOTT")]
        public string HotText { get; set; }

        [MapField("COMM")]
        public bool CommedFlag { get; set; }

        [MapField("SPARE")]
        public string Filter { get; set; }
    }
}
