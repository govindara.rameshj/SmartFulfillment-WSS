﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("SYSDAT")]
    public class SystemDate
    {
        [MapField("TODT")]
        public DateTime sysDate { get; set; }
    }
}
