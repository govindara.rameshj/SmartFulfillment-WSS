﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("EVTMAS")]
    public class EventMaster
    {
        [MapField("NUMB")]
        public string EventNumber { get; set; }

        [MapField("TYPE")]
        public string EventType { get; set; }
        
        [MapField("KEY1")]
        public string EventKey1 { get; set; }

        [MapField("KEY2")]
        public string EventKey2 { get; set; }
        
        [MapField("PRIO")]
        public string Priority { get; set; }

        [MapField("IDEL")]
        public bool IsDeleted { get; set; }         

        [MapField("IDOW")]
        public bool IsDayOfWeekEvent { get; set; }
        
        [MapField("SDAT")]
        public DateTime StartDate { get; set; }

        [MapField("EDAT")]
        public DateTime EndDate { get; set; }
        
        [MapField("PRIC")]
        public decimal Price { get; set; }
                      
        [MapField("BQTY")]
        public int BuyQuantity { get; set; }
                  
        [MapField("GQTY")]
        public int GetQuantity { get; set; }
                      
        [MapField("VDIS")]
        public decimal ValueDiscount { get; set; } 

        [MapField("PDIS")]
        public decimal PercentageDiscount  { get; set; }  

        [MapField("BUYCPN")]
        public string BuyCoupon  { get; set; }

        [MapField("GETCPN")]
        public string GetCoupon  { get; set; }
    }
}
