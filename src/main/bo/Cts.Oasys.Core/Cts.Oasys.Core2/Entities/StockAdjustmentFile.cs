﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("STKADJ")]
    public class StockAdjustmentFile
    {
        [MapField("DATE1")]
        public DateTime Date { get; set; }
    }
}
