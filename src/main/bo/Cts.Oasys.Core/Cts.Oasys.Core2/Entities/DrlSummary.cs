﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("DRLSUM")]
    public class DrlSummary
    {
        // not all fields are here

        [MapField("NUMB")]
        public string DrlNumber { get; set; }
    }
}
