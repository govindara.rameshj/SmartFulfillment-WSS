﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.Mapping;
using BLToolkit.DataAccess;

namespace Cts.Oasys.Core.Entities
{
    [TableName("RETOPT")]
    public class RetailOptions
    {
        // not all fields are here

        [MapField("FKEY")]
        public string KeyToFile { get; set; }
    }
}
