﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("Alert")]
    public class Alert
    {
        // not all fields are here

        [PrimaryKey, Identity]
        public int Id { get; set; }

        [NotNull]
        public int OrderNumb { get; set; }

        [NotNull]
        public bool Refund { get; set; }

        [NotNull]
        public DateTime ReceivedDate { get; set; }

        [NotNull]
        public bool Authorized { get; set; }

        public int AuthUser { get; set; }

        public DateTime AuthDate { get; set; }
    }
}
