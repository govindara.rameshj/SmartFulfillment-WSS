﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("RSCASH")]
    public class CashierFlashTotal
    {
        // not all fields are here

        [MapField("TILL")]
        public string TillNumber { get; set; }

        [MapField("CASH")]
        public string CashierNumber { get; set; }
    }
}
