﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("QODSource")]
    public class OrderSourceCode
    {
        [MapField("SOURCE_CODE")]
        public string Code { get; set; }  
 
        [MapField("SHOW_IN_UI")]
        public bool IsShowInUi { get; set; }  

        [MapField("SEND_UPDATES_TO_OM")]
        public bool IsNotifyOrderManager { get; set; }  

        [MapField("ALLOW_REMOTE_CREATE")]
        public bool IsAllowCreateRemote { get; set; }  

        [MapField("IS_CLICK_AND_COLLECT")]
        public bool IsClickAndCollect { get; set; } 
    }
}
