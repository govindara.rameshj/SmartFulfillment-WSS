﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("HHTDET")]
    public class PicCountDetailFile
    {
        // not all fields are here

        [MapField("DATE1")]
        public DateTime Date { get; set; }
    }
}
