﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("SystemUsers")]
    public class SystemUser
    {
        // not all fields are here
               
        [MapField("ID")]
        public int Id { get; set; }

        [MapField("EmployeeCode")]
        public string EmployeeCode { get; set; }

        [MapField("Password")]
        public string Password { get; set; }

        [MapField("PasswordExpires")]
        public DateTime PasswordExpirationDate { get; set; }

        [MapField("SupervisorPassword")]
        public string SupervisorPassword { get; set; }

        [MapField("SupervisorPwdExpires")]
        public DateTime SupervisorPasswordExpirationDate { get; set; }      
        
        [MapField("IsDeleted")]
        public bool IsDeleted { get; set; }    

        [MapField("IsManager")]
        public bool IsManager { get; set; } 

        [MapField("IsSupervisor")]
        public bool IsSupervisor { get; set; } 
    }
}
