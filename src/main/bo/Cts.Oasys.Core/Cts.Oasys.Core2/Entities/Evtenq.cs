﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("EVTENQ")]
    public class Evtenq
    {
        // not all fields are here

        [MapField("PRIO")]
        public string Prio { get; set; }
    }
}
