﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("DLGIFT")]
    public class DailyGiftToken
    {
        // not all fields are here

        [MapField("SERI")]
        public string SerialNumber { get; set; }
    }
}
