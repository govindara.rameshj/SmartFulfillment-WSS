﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Cts.Oasys.Core.Entities
{
    [TableName("STKMAS")]
    public class StockMaster
    {
        [MapField("SKUN")]
        public string SKU { get; set; }
        [MapField("PRIC")]
        public decimal Price { get; set; }
        [MapField("REVT")]
        public string EventNumber { get; set; }
        [MapField("RPRI")]
        public string Priority { get; set; }
        [MapField("IOBS")]
        public string Obsolete { get; set; }
        [MapField("ONHA")]
        public string StockOnHand { get; set; }
        [MapField("INON")]
        public string NonStocked { get; set; }
        [MapField("MDNQ")]
        public string MarkdownStock { get; set; }
        [MapField("AAPC")]
        public string AutoApply { get; set; }
        [MapField("LABN")]
        public int LabelSmall { get; set; }
        [MapField("LABM")]
        public int LabelMedium { get; set; }
        [MapField("LABL")]
        public int LabelLarge { get; set; }
        [MapField("SUPP")]
        public string SupplierId { get; set; }
        [MapField("DOBS")]
        public DateTime? DOBS { get; set; }
        [MapField("DDEL")]
        public DateTime? DDEL { get; set; }
        [MapField("DESCR")]
        public string Description { get; set; }
    }
}
