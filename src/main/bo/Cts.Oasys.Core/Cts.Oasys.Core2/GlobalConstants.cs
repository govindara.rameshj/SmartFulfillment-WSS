﻿namespace Cts.Oasys.Core
{
    /// <summary>
    /// Solution wide constants
    /// </summary>
    public static class GlobalConstants
    {
        /// <summary>
        /// Exact length of SKU number text
        /// </summary>
        public const int SkuNumberTextLength = 6;

        /// <summary>
        /// ID of the tender type for WebOrders(C&C orders)
        /// </summary>
        public const int WebOrderTenderTypeID = 12;

        /// <summary>
        /// ID of the virtual cashier for WebOrders(C&C orders)
        /// </summary>
        public const int WebOrderCashierID = 499;

        /// <summary>
        /// ID of the start till for WebOrders(C&C orders)
        /// </summary>
        public const int WebOrderStartTillID = 90;

        /// <summary>
        /// ID of the end till for WebOrders(C&C orders)
        /// </summary>
        public const int WebOrderEndTillID = 99;
    }
}