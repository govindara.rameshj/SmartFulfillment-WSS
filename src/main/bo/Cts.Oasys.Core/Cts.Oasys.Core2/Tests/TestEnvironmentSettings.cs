using System;

namespace Cts.Oasys.Core.Tests
{
    public class TestEnvironmentSettings
    {
        public string OasysSqlServerConnectionString { get; set; }
        public string OasysOdbcConnectionString { get; set; }
        public string XsdFilesDirectoryPath { get; set; }
        public string CommsDirectoryPath { get; set; }

        private const string WssOasysSqlServerConnectionStringEnvVarName = "WSS_OASYS_SQL_SERVER_CONNECTION_STRING";
        private const string WssOasysOdbcConnectionStringEnvVarName = "WSS_OASYS_ODBC_CONNECTION_STRING";
        private const string WssXsdFilesDirectoryPathEnvVarName = "WSS_XSD_FILES_DIRECTORY_PATH";
        private const string WssCommsDirectoryPathEnvVarName = "WSS_COMM_DIRECTORY_PATH";

        public void RestoreFromEnvironmentVariables()
        {
            TryToOverrideFromEnvironmentVariable(WssOasysSqlServerConnectionStringEnvVarName, value => OasysSqlServerConnectionString = value);
            TryToOverrideFromEnvironmentVariable(WssOasysOdbcConnectionStringEnvVarName, value => OasysOdbcConnectionString = value);
            TryToOverrideFromEnvironmentVariable(WssXsdFilesDirectoryPathEnvVarName, value => XsdFilesDirectoryPath = value);
            TryToOverrideFromEnvironmentVariable(WssCommsDirectoryPathEnvVarName, value => CommsDirectoryPath = value);
        }

        public void SaveToEnvironmentVariables(System.Diagnostics.ProcessStartInfo procesStartInfo)
        {
            var vars = procesStartInfo.EnvironmentVariables;
            vars.Add(WssOasysSqlServerConnectionStringEnvVarName,OasysSqlServerConnectionString);
            vars.Add(WssOasysOdbcConnectionStringEnvVarName, OasysOdbcConnectionString);
            vars.Add(WssXsdFilesDirectoryPathEnvVarName, XsdFilesDirectoryPath);
            vars.Add(WssCommsDirectoryPathEnvVarName, CommsDirectoryPath);
        }

        private static void TryToOverrideFromEnvironmentVariable(string environmentVariableName, Action<string> overrideAction)
        {
            var envVarValue = System.Environment.GetEnvironmentVariable(environmentVariableName);
            if (!String.IsNullOrEmpty(envVarValue))
            {
                overrideAction(envVarValue);
            }
        }

    }
}
