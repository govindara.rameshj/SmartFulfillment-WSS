using System;
using Cts.Oasys.Core.SystemEnvironment;

namespace Cts.Oasys.Core.Tests
{
    public class TestSystemEnvironment : ISystemEnvironment  
    {
        private readonly LiveSystemEnvironment live = new LiveSystemEnvironment();

        public TestEnvironmentSettings Settings {get; private set;}

        public TestSystemEnvironment(TestEnvironmentSettings settings)
        {
            Settings = settings;
            GetDateTimeNowMethod = () => live.GetDateTimeNow();
        }

        public Func<DateTime> GetDateTimeNowMethod { get; set; }

        public DateTime GetDateTimeNow()
        {
            return GetDateTimeNowMethod();
        }

        public string GetOasysSqlServerConnectionString()
        {
            return Settings.OasysSqlServerConnectionString;
        }

        public string GetOasysOdbcConnectionString()
        {
            return Settings.OasysOdbcConnectionString;
        }

        public string GetOasysOdbcDatabaseType()
        {
            return "MicrosoftSQL";
        }

        public bool IsOasysDataProviderSqlSever()
        {
            return true;
        }

        public string GetProxyAssemblyName()
        {
            return "Oasys.Data.SqlClient";
        }

        public string GetProxyTypeName()
        {
            return "Oasys.SqlClientDataProxy";
        }

        public string GetConfigXmlDocumentPath()
        {
            return "<test run. Confix.xml is not used>";
        }

        public string GetCommsDirectoryPath()
        {
            return Settings.CommsDirectoryPath;
        }

        public string GetXsdFilesDirectoryPath()
        {
            return Settings.XsdFilesDirectoryPath;
        }

        private static class XsdTypes
        {
            public const string OrderManagerCheckFulfilment = "ORDERMANAGERCHECKFULFILMENT";
            public const string CTSFulfilmentRequest = "CTSFULFILMENTREQUEST";
            public const string CTSStatusNotification = "CTSSTATUSNOTIFICATION"; 
            public const string CTSEnableStatusNotification = "CTSENABLESTATUSNOTIFICATION"; 
            public const string CTSUpdateRefund = "CTSUPDATEREFUND"; 
            public const string CTSQODCreate = "CTSQODCREATE"; 
            public const string CTSEnableRefundCreate = "CTSENABLEREFUNDCREATE"; 
            public const string OrderManagerCreateOrder = "ORDERMANAGERCREATEORDER"; 
            public const string OrderManagerCreateRefund = "ORDERMANAGERCREATEREFUND"; 
            public const string OrderManagerStatusUpdate = "ORDERMANAGERSTATUSUPDATE"; 
        }

        public string GetInputXsdFileName(string xsdFileType)
        {
            switch (xsdFileType.ToUpper())
            {
                case XsdTypes.CTSFulfilmentRequest:
                    return "CtsFulfilmentRequestInput.xsd";
                case XsdTypes.CTSStatusNotification: 
                    return  "CtsStatusNotificationInput.xsd";
                case XsdTypes.CTSEnableStatusNotification: 
                    return  "CtsEnableStatusNotificationInput.xsd";
                case XsdTypes.CTSUpdateRefund: 
                    return  "CtsUpdateRefundInput.xsd";
                case XsdTypes.CTSQODCreate: 
                    return  "CtsQodCreateInput.xsd";
                case XsdTypes.CTSEnableRefundCreate: 
                    return  "CtsEnableRefundCreateInput.xsd";
                case XsdTypes.OrderManagerCreateOrder: 
                    return  "OmOrderCreateInput.xsd";
                case XsdTypes.OrderManagerCreateRefund: 
                    return  "OmOrderRefundRequestInput.xsd";
                case XsdTypes.OrderManagerStatusUpdate: 
                    return  "OmOrderStatusInput.xsd";
                case XsdTypes.OrderManagerCheckFulfilment: 
                    return  "OM_Check_Fulfilment_Input.xsd";
                default:
                    throw new ArgumentException();
            }
        }

        public string GetOutputXsdFileName(string xsdFileType)
        {
            switch (xsdFileType.ToUpper())
            {
                case XsdTypes.CTSFulfilmentRequest:
                    return "CtsFulfilmentRequestOutput.xsd";
                case XsdTypes.CTSStatusNotification: 
                    return  "CtsStatusNotificationOutput.xsd";
                case XsdTypes.CTSEnableStatusNotification: 
                    return  "CtsEnableStatusNotificationOutput.xsd";
                case XsdTypes.CTSUpdateRefund: 
                    return  "CTSUpdateRefundOutput.xsd";
                case XsdTypes.CTSQODCreate: 
                    return  "CtsQodCreateOutput.xsd";
                case XsdTypes.CTSEnableRefundCreate: 
                    return  "CtsEnableRefundCreateOutput.xsd";
                case XsdTypes.OrderManagerCreateOrder: 
                    return  "OmOrderCreateOutput.xsd";
                case XsdTypes.OrderManagerCreateRefund: 
                    return  "OmOrderRefundRequestOutput.xsd";
                case XsdTypes.OrderManagerStatusUpdate: 
                    return  "OmOrderStatusOutput.xsd";
                case XsdTypes.OrderManagerCheckFulfilment: 
                    return  "OM_Check_Fulfilment_Output.xsd";
                default:
                    throw new ArgumentException();
            }

        }
    }
}
