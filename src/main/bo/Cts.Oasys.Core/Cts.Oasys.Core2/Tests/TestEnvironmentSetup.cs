using System;
using System.Diagnostics;
using Cts.Oasys.Core.SystemEnvironment;

namespace Cts.Oasys.Core.Tests
{
    public static class TestEnvironmentSetup
    {
        private const string WssTestRunEnvVarName = "WSS_TEST_RUN";

        public static void SetupConnectionToAatDb()
        {
            Setup(settings => settings.ConnectToAatDb());
        }

        public static void ConnectToDevDb(this TestEnvironmentSettings settings)
        {
            settings.OasysSqlServerConnectionString = "Data Source=oasys-dev-dbserver;Initial Catalog=Oasys;Integrated Security=True";
            settings.OasysOdbcConnectionString = "dsn=Oasys";
        }

        public static void ConnectToAatDb(this TestEnvironmentSettings settings)
        {
            settings.OasysSqlServerConnectionString = "Data Source=oasys-fitnesse-dbserver;Initial Catalog=Oasys;Integrated Security=True";
            settings.OasysOdbcConnectionString = "dsn=OasysFitnesse";
        }

        #region Own public methods

        public static ISystemEnvironment Setup(Action<TestEnvironmentSettings> setupSettings)
        {
            TestEnvironmentSettings settings = new TestEnvironmentSettings();
            setupSettings(settings);
            settings.RestoreFromEnvironmentVariables();

            var env = new TestSystemEnvironment(settings);
            GlobalVars.SystemEnvironment = env;
            return env;
        }

        public static void SetupSpawnedProcess(ProcessStartInfo procesStartInfo)
        {
            procesStartInfo.UseShellExecute = false; // To allow usage of env vars
            
            var vars = procesStartInfo.EnvironmentVariables;
            vars.Add(WssTestRunEnvVarName, "true");

            ((TestSystemEnvironment)GlobalVars.SystemEnvironment).Settings.SaveToEnvironmentVariables(procesStartInfo);
        }

        public static void SetupIfTestRun()
        {
            try
            {
                if ("true".Equals(Environment.GetEnvironmentVariable(WssTestRunEnvVarName), StringComparison.CurrentCultureIgnoreCase))
                {
                    Setup(settings => {});
                }
            }
            catch
            {
                // ignored
            }
        }

        #endregion
    }
}
