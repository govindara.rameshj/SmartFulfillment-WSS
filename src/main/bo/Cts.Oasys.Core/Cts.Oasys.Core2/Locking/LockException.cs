﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cts.Oasys.Core.Locking
{
    public class LockException: Exception
    {
        private string entityId;
        public string EntityId
        {
            get { return entityId; }
        }

        private string ownerId;
        public string OwnerId
        {
            get { return ownerId; }
        }

        private DateTime lockTime;
        public DateTime LockTime
        {
            get { return lockTime; }
        }

        public LockException(string entityId, string ownerId, DateTime lockTime)
        {
            this.entityId = entityId;
            this.ownerId = ownerId;
            this.lockTime = lockTime;
        }
    }
}
