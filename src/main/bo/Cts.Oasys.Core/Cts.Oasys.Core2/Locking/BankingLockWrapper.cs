using System;

namespace Cts.Oasys.Core.Locking
{
    public class BankingLockWrapper : IDisposable
    {
        private readonly EntityLock _bankingLock;
        public bool IsLockAcquired { get; set; }

        public BankingLockWrapper(string entityId, string ownerId)
        {
            try
            {
                _bankingLock = new EntityLock(entityId, ownerId);
                IsLockAcquired = true;
            }
            catch (LockException)
            {
                IsLockAcquired = false;
            }         
        }

        bool _disposed;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
            }

            if (_bankingLock != null)
            {
                _bankingLock.Dispose();
            }
            _disposed = true;
        }

        ~BankingLockWrapper()
        {
            Dispose(false);
        }
    }
}
