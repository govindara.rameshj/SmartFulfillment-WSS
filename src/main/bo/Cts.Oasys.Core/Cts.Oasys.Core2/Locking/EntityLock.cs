using System;
using System.Data.SqlClient;

namespace Cts.Oasys.Core.Locking
{
    public class EntityLock: IDisposable
    {
        private string entityId;
        private string ownerId;

        public EntityLock(string entityId, string ownerId)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetSqlConnectionString()))
            {
                conn.Open();
                SqlTransaction tran = conn.BeginTransaction();

                SqlCommand cmd = new SqlCommand(@"SELECT EntityId, OwnerId, LockTime FROM Locks WITH (ROWLOCK) WHERE EntityId = @entityId;", conn, tran);
                cmd.Parameters.AddWithValue("@entityId", entityId);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                    throw new LockException((string)reader["EntityId"], (string)reader["OwnerId"], (DateTime)reader["LockTime"]);
                reader.Close();

                cmd = new SqlCommand(@"INSERT INTO Locks(EntityId, OwnerId, LockTime) VALUES(@entityId, @ownerId, GETDATE());", conn, tran);
                cmd.Parameters.AddWithValue("@entityId", entityId);
                cmd.Parameters.AddWithValue("@ownerId", ownerId);
                cmd.ExecuteNonQuery();

                tran.Commit();
            }

            this.entityId = entityId;
            this.ownerId = ownerId;
        }

        public static void RemoveLock(string entityId)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetSqlConnectionString()))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(@"DELETE FROM Locks WHERE EntityId = @entityId;", conn);
                cmd.Parameters.AddWithValue("@entityId", entityId);
                cmd.ExecuteNonQuery();
            }
        }

        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                //
            }

            // Free any unmanaged objects here.
            if (!string.IsNullOrEmpty(entityId))
                RemoveLock(entityId);

            disposed = true;
        }

        ~EntityLock()
        {
            Dispose(false);
        }
    }
}
