﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace Cts.Oasys.Core
{
    public static class Parameters
    {
        public const int PARAMETER_ESTORE_STORE_NUMBER = 3020;

        public static T GetParameterValue<T>(int id)
        {
            string colName = null;

            if (typeof(T) == typeof(int))
                colName = "LongValue";
            else if (typeof(T) == typeof(String))
                colName = "StringValue";
            else if (typeof(T) == typeof(Boolean))
                colName = "BooleanValue";
            else if (typeof(T) == typeof(Decimal))
                colName = "DecimalValue";

            if (colName == null)
                throw new ArgumentException("Undefined value type given for parameter");

            using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetSqlConnectionString()))
            {
                SqlCommand cmd = new SqlCommand()
                {
                    Connection = conn,
                    CommandType = System.Data.CommandType.Text,
                    CommandText = "SELECT " + colName + @" AS Val FROM [Parameters] WHERE ParameterID = @id",
                };
                cmd.Parameters.AddWithValue("@id", id);

                conn.Open();
                return (T)cmd.ExecuteScalar();
            }
        }

        private static string storeId = null;
        /// <summary>
        /// Data from RETOPT:STOR column
        /// </summary>
        public static string GetStoreId()
        {
            if (storeId == null)
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetSqlConnectionString()))
                {
                    SqlCommand cmd = new SqlCommand()
                    {
                        Connection = conn,
                        CommandType = System.Data.CommandType.Text,
                        CommandText = "SELECT STOR FROM RETOPT",
                    };

                    conn.Open();
                    storeId = (string)cmd.ExecuteScalar();
                }
            }

            return storeId;
        }
    }
}
