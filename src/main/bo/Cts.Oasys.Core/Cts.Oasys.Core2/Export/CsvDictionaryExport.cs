﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Cts.Oasys.Core.Export
{
    public class CsvDictionaryExport<T> where T : class
    {
        private readonly List<T> _objects;
        private readonly Func<T, string> _sortExpression;

        public CsvDictionaryExport(List<T> objects, Func<T, string> sortExpression)
        {
            _objects = objects;
            _sortExpression = sortExpression;
        }

        public string DoubleFormat { get; set; }

        public string Export()
        {
            return Export(true);
        }

        public string Export(bool includeHeaderLine)
        {
            StringBuilder sb = new StringBuilder();
            IList<PropertyInfo> propertyInfos = typeof (T).GetProperties();

            if (includeHeaderLine)
            {
                foreach (PropertyInfo propertyInfo in propertyInfos)
                {
                    sb.Append(propertyInfo.Name).Append(",");
                }
                sb.Remove(sb.Length - 1, 1).AppendLine();
            }

            var sortedStats = _objects.OrderBy(_sortExpression);
            foreach (var obj in sortedStats)
            {
                foreach (PropertyInfo propertyInfo in propertyInfos)
                {
                    sb.Append(MakeValueCsvFriendly(propertyInfo.GetValue(obj, null))).Append(",");
                }
                sb.Remove(sb.Length - 1, 1).AppendLine();
            }

            return sb.ToString();
        }

        public void ExportToFile(string path)
        {
            File.WriteAllText(path, Export());
        }

        public byte[] ExportToBytes()
        {
            return Encoding.UTF8.GetBytes(Export());
        }

        private string MakeValueCsvFriendly(object value)
        {
            if (value == null) return "";
            var nullable = value as INullable;
            if (nullable != null && nullable.IsNull) return "";

            if (DoubleFormat != null && value is double)
            {
                var doubleValue = (double) value;
                return doubleValue.ToString(DoubleFormat);
            }

            if (value is DateTime)
            {
                var dateTime = (DateTime) value;

                if (dateTime.TimeOfDay == TimeSpan.Zero)
                    return dateTime.ToString("yyyy-MM-dd");
                return dateTime.ToString("yyyy-MM-dd HH:mm:ss");
            }
            string output = value.ToString();

            if (output.Contains(",") || output.Contains("\""))
                output = '"' + output.Replace("\"", "\"\"") + '"';

            return output;
        }
    }
}