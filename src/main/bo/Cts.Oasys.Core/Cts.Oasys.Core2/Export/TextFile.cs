﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Cts.Oasys.Core.Export
{
    public class TextFile
    {
        private static void CreateDirectoryIfNotExist(string pathToFile)
        {
            try
            {
                // If the directory already exists, Create method does nothing.
                new FileInfo(pathToFile).Directory.Create();
            }
            catch { /* ignore it */ }
        }

        public static void Save(string pathToFile, IEnumerable<string> lines)
        {
            CreateDirectoryIfNotExist(pathToFile);

            using (StreamWriter file = new StreamWriter(pathToFile))
            {
                foreach (var line in lines)
                {
                    file.WriteLine(line);
                }
            }
        }

        public static void Move(string source, string target)
        {
            // Ensure that the target does not exist. 
            if (File.Exists(target))
            {
                File.Delete(target);
            }

            CreateDirectoryIfNotExist(target);

            // Move the file.
            File.Move(source, target);
        }
    }
}
