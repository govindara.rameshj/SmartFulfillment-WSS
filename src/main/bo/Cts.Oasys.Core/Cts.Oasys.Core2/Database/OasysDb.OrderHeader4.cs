﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.Data.Linq;
using Cts.Oasys.Core.Entities;

namespace Cts.Oasys.Core.Database
{
    public partial class OasysDb
    {
        public Table<OrderHeader4> OrderHeaders4Table { get { return GetTable<OrderHeader4>(); } }
    }
}
