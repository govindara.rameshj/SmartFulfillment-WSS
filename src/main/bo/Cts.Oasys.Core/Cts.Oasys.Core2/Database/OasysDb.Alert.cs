﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cts.Oasys.Core.Entities;
using BLToolkit.Data.Linq;

namespace Cts.Oasys.Core.Database
{
    public partial class OasysDb
    {
        public Table<Alert> AlertTable { get { return GetTable<Alert>(); } }

        public static List<Alert> GetUnauthorizedAlerts()
        {
            using (var db = new OasysDb())
            {
                // a.Authorized == false - to force good sql generating
                return db.AlertTable
                    .Where(a => a.Authorized == false)
                    .Select(a => a).ToList<Alert>();
            }
        }

        public static int AuthorizeAlerts(List<Alert> alerts, int userId)
        {
            using (var db = new OasysDb())
            {
                // a.Authorized == false - to force good sql generating
                return db.AlertTable
                    .Where(a => alerts.Contains(a) && a.Authorized == false)
                    .Update(a => new Alert
                    {
                        Authorized = true,
                        AuthUser = userId,
                        AuthDate = DateTime.Now
                    }
                );
            }
        }
    }
}
