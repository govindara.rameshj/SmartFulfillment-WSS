﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cts.Oasys.Core.Entities;
using BLToolkit.Data.Linq;

namespace Cts.Oasys.Core.Database
{
    public partial class OasysDb
    {
        public Table<PicCountHeaderFile> PicCountHeaderFileTable { get { return GetTable<PicCountHeaderFile>(); } }
    }
}
