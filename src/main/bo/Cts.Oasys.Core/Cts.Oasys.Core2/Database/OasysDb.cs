﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.Data;
using System.Data.SqlClient;
using Cts.Oasys.Core.Net4Replacements;
using System.Linq.Expressions;
using System.Reflection;

namespace Cts.Oasys.Core.Database
{
    public partial class OasysDb : DbManager
    {
        private static Lazy<String> ConnStr = new Lazy<String>(ConnectionStringHelper.GetSqlConnectionString);

        public OasysDb() : base(new SqlConnection(ConnStr.Value)) { }

        public OasysDb(SqlConnection connection) : base(connection) { }
    }
}
