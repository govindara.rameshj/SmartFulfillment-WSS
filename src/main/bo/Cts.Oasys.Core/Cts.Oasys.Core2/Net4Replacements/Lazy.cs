﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cts.Oasys.Core.Net4Replacements
{
    /// <summary>
    /// Provides support for lazy initialization.
    /// http://stackoverflow.com/questions/3207580/implementation-of-lazyt-for-net-3-5
    /// </summary>
    /// <typeparam name="T">Specifies the type of object that is being lazily initialized.</typeparam>
    public sealed class Lazy<T>
    {
        private readonly ILazyImplementation<T> implementation;

        /// <summary>
        /// Gets the lazily initialized value of the current Lazy{T} instance.
        /// </summary>
        public T Value
        {
            get
            {
                return implementation.Value;
            }
        }

        /// <summary>
        /// Gets a value that indicates whether a value has been created for this Lazy{T} instance.
        /// </summary>
        public bool IsValueCreated
        {
            get
            {
                return implementation.IsValueCreated;
            }
        }

        /// <summary>
        /// Initializes a new instance of the Lazy{T} class.
        /// </summary>
        /// <param name="createValue">The delegate that produces the value when it is needed.</param>
        public Lazy(Func<T> createValue, bool isThreadSafe = true)
        {
            if (createValue == null) throw new ArgumentNullException("createValue");

            this.implementation = isThreadSafe ? new ThreadSafeLazyImplementation<T>(createValue) : new ThreadUnsafeLazyImplementation<T>(createValue);
        }

        /// <summary>
        /// Creates and returns a string representation of the Lazy{T}.Value.
        /// </summary>
        /// <returns>The string representation of the Lazy{T}.Value property.</returns>
        public override string ToString()
        {
            return Value.ToString();
        }
    }

    interface ILazyImplementation<T>
    {
        bool IsValueCreated { get; }
        T Value { get;  }
    }

    class ThreadUnsafeLazyImplementation<T> : ILazyImplementation<T>
    {
        private readonly Func<T> createValue = null;
        private bool isValueCreated;
        private T value;

        public ThreadUnsafeLazyImplementation()
        {
        }

        public ThreadUnsafeLazyImplementation(Func<T> createValue)
        {
            this.createValue = createValue;
        }

        /// <summary>
        /// Gets the lazily initialized value of the current Lazy{T} instance.
        /// </summary>
        public virtual T Value
        {
            get
            {
                if (!isValueCreated)
                {
                    if (!isValueCreated)
                    {
                        value = createValue();
                        isValueCreated = true;
                    }
                }
                return value;
            }
        }

        /// <summary>
        /// Gets a value that indicates whether a value has been created for this Lazy{T} instance.
        /// </summary>
        public virtual bool IsValueCreated
        {
            get
            {
                return isValueCreated;
            }
        }
    }

    class ThreadSafeLazyImplementation<T> : ThreadUnsafeLazyImplementation<T>
    {
        private readonly object padlock = new object();

        public ThreadSafeLazyImplementation()
        {
        }
        
        public ThreadSafeLazyImplementation(Func<T> createValue)
            : base(createValue)
        {
        }

        /// <summary>
        /// Gets the lazily initialized value of the current Lazy{T} instance.
        /// </summary>
        public override T Value
        {
            get
            {
                lock (padlock)
                {
                    return base.Value;
                }
            }
        }

        /// <summary>
        /// Gets a value that indicates whether a value has been created for this Lazy{T} instance.
        /// </summary>
        public override bool IsValueCreated
        {
            get
            {
                lock (padlock)
                {
                    return base.IsValueCreated;
                }
            }
        }
    }
}
