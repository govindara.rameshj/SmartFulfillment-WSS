﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace Cts.Oasys.Core
{
    public static class XmlSerializationHelper
    {
        public static string Serialize(Type type, Object obj)
        {
            var serializer = new XmlSerializer(type);
            var sb = new StringBuilder();

            using (var writer = new StringWriter(sb))
            {
                serializer.Serialize(writer, obj);
            }
            return sb.ToString();
        }

        public static string Serialize<T>(T obj)
        {
            var serializer = new XmlSerializer(typeof(T));
            var sb = new StringBuilder();

            using(var writer = new StringWriter(sb))
            {
                serializer.Serialize(writer, obj);
            }
            return sb.ToString();
        }

        public static T Deserialize<T>(string xml)
        {
            var serializer = new XmlSerializer(typeof(T));

            using(var reader = new StringReader(xml))
            {
                return (T)serializer.Deserialize(reader);
            }
        }

        public static object Deserialize(Type type, string xml)
        {
            var serializer = new XmlSerializer(type);

            using (var reader = new StringReader(xml))
            {
                return serializer.Deserialize(reader);
            }
        }
    }
}
