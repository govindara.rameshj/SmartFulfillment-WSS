using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.IO;

namespace Cts.Oasys.Core
{
    /// <summary>
    /// Resolves a path to Oasys configuration files.
    /// </summary>
    public class ConfigPathResolver
    {
        private IEnumerable<string> GetPossibleParentsForResourcesDirectory()
        {
            string baseDirectory = System.AppDomain.CurrentDomain.BaseDirectory;
            System.Diagnostics.Debug.WriteLine("ConfigPathResolver - BaseDirectory=[" + baseDirectory + "]");

            yield return baseDirectory;

            // from Oasys3/SOM/ to Oasys3/Resources/
            if (baseDirectory.EndsWith("\\SOM\\", StringComparison.InvariantCultureIgnoreCase))
            {
                yield return System.IO.Path.GetFullPath(System.IO.Path.Combine(baseDirectory, @"..\"));
            }

            // Running Hubs Standard Store CtsOrderManager from sources.
            if (baseDirectory.EndsWith("\\src\\main\\bo\\hubs\\Cts.Oasys.Hubs.Web\\CtsOrderManager\\"))
            {
                yield return System.IO.Path.GetFullPath(System.IO.Path.Combine(baseDirectory, @"..\..\..\..\..\..\out\build\Oasys3\"));
            }

            // !!! Below are hardcoded directories.
            // But please dont just add you directory to this list, try to do something more generic, like code above.
            yield return @"C:\Program Files\CTS Retail\Oasys3\";
            yield return @"C:\Program Files\CTS\Oasys3\";

            // for development only
            yield return @"C:\Projects\OasysHW\VB.NET2008\Staging Area\";
            yield return @"C:\Projects\HUBS 2.0\Back Office\Staging Area";
            yield return @"C:\Projects\Wickes SS\HUBS 2.0\Back Office\Staging Area";

            // Partha Dutta - 02/08/2010
            // Development location has moved
            // Start change
            yield return @"C:\Projects\Dev\Back Office\Staging Area\";
            yield return @"C:\Projects\Wickes SS\Dev\Back Office\Staging Area\";
            // End change

            string vb6Result = null;
            try
            {
                // Dll is loaded by Till, which is run from VB6 debugger
                // Use Till \Resources directory based on assembly location.
                string processExeName = System.Diagnostics.Process.GetCurrentProcess().MainModule.ModuleName;
                if (String.Compare(processExeName, "VB6.EXE", true) == 0)
                {
                    string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                    var uriBuilder = new UriBuilder(codeBase);
                    string assemblyPath = Uri.UnescapeDataString(uriBuilder.Path);
                    string assemblyLocation = System.IO.Path.GetDirectoryName(assemblyPath);

                    vb6Result = assemblyLocation;
                }
            }
            catch
            {
            }

            if (!String.IsNullOrEmpty(vb6Result))
            {
                yield return vb6Result;
            }
        }

        /// <summary>
        /// It is not static because there is an idea to resolve ConfigPathResolver via DI container
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string Resolve(string fileName)
        {
            string relativeFilePath = @"Resources\" + fileName;
            return GetPossibleParentsForResourcesDirectory()
                .Select(d => Path.GetFullPath(System.IO.Path.Combine(d, relativeFilePath)))
                .FirstOrDefault(fp => File.Exists(fp));
        }

        public string ResolveConfigXml()
        {
            return Resolve("Config.xml");
        }

        public string ResolveConnectionXml()
        {
            return Resolve("Connection.xml");
        }

        public static string ResolveBusinessObjectsDllPath(string dllFileName)
        {
            string result = dllFileName;

            try
            {
                // Dll is loaded by Runner.exe, which is run by Fitnesse 
                string processExeName = System.Diagnostics.Process.GetCurrentProcess().MainModule.ModuleName;
                if (String.Compare(processExeName, "RUNNER.EXE", true) == 0)
                {
                    // check if dll is in the fitsharp runner folder
                    result = "./fitsharp/" + dllFileName;
                    if (!File.Exists(result))
                    {
                        result = "../../../out/build/Oasys3/" + dllFileName;
                    }
                }
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}
