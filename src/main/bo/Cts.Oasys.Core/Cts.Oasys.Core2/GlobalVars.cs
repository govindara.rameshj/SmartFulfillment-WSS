using System;
using Cts.Oasys.Core.SystemEnvironment;
using Cts.Oasys.Core.Tests;

namespace Cts.Oasys.Core
{
    public static class GlobalVars
    {
        private static ISystemEnvironment systemEnvironment;
        private static readonly object SystemEnvironmentLock = new object();

        public static bool IsTestEnvironment
        {
            get
            {
                // also works if SystemEnvironment is null, returns false is such case;
                return SystemEnvironment is TestSystemEnvironment;
            }
        }

        public static ISystemEnvironment SystemEnvironment
        {
            get
            {
                return systemEnvironment;
            }
            internal set
            {
                lock (SystemEnvironmentLock)
                {
                    if (systemEnvironment != null)
                    {
                        throw new ApplicationException("SystemEnvironment is already initialized.");
                    }
                    systemEnvironment = value;
                }
            }
        }    }
}
