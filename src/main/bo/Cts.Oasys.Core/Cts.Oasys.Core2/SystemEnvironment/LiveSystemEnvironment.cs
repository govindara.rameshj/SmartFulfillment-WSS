﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cts.Oasys.Core.SystemEnvironment
{
    public class LiveSystemEnvironment : ISystemEnvironment 
    {
        #region ISystemEnvironment Members

        public DateTime GetDateTimeNow()
        {
            return DateTime.Now;
        }

        public string GetOasysSqlServerConnectionString()
        {
            throw new ApplicationException("ISystemEnvironment.GetOasysSqlServerConnectionString should not be used in Live.");
        }

        public string GetOasysOdbcConnectionString()
        {
            throw new ApplicationException("ISystemEnvironment.GetOasysOdbcConnectionString should not be used in Live.");
        }

        public string GetOasysOdbcDatabaseType()
        {
            throw new ApplicationException("ISystemEnvironment.GetOasysOdbcDatabaseType should not be used in Live.");
        }

        public bool IsOasysDataProviderSqlSever()
        {
            throw new ApplicationException("ISystemEnvironment.IsOasysDataProviderSqlSever should not be used in Live.");
        }

        public string GetProxyAssemblyName()
        {
            throw new ApplicationException("ISystemEnvironment.getProxyAssemblyName should not be used in Live.");
        }

        public string GetProxyTypeName()
        {
            throw new ApplicationException("ISystemEnvironment.getProxyTypeName should not be used in Live.");
        }

        public string GetConfigXmlDocumentPath()
        {
            throw new ApplicationException("ISystemEnvironment.GetConfigXmlDocumentPath should not be used in Live.");
        }

        public string GetXsdFilesDirectoryPath()
        {
            throw new ApplicationException("ISystemEnvironment.GetXsdFilesDirectoryPath should not be used in Live.");
        }

        public string GetInputXsdFileName(string xsdFileType)
        {
            throw new ApplicationException("ISystemEnvironment.GetInputXsdFileName should not be used in Live.");
        }

        public string GetOutputXsdFileName(string xsdFileType)
        {
            throw new ApplicationException("ISystemEnvironment.GetOutputXsdFileName should not be used in Live.");
        }

        public string GetCommsDirectoryPath()
        {
            throw new ApplicationException("ISystemEnvironment.GetCommsDirectoryPath should not be used in Live.");
        }        

        #endregion
    }
}
