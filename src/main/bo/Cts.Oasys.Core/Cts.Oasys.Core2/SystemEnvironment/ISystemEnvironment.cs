using System;

namespace Cts.Oasys.Core.SystemEnvironment
{
    public interface ISystemEnvironment
    {
        DateTime GetDateTimeNow();
        string GetOasysSqlServerConnectionString();
        string GetOasysOdbcConnectionString();
        string GetOasysOdbcDatabaseType();
        bool IsOasysDataProviderSqlSever();
        string GetProxyAssemblyName();
        string GetProxyTypeName();
        string GetConfigXmlDocumentPath();
        string GetXsdFilesDirectoryPath();
        string GetInputXsdFileName(string xsdFileType);
        string GetOutputXsdFileName(string xsdFileType);
        string GetCommsDirectoryPath();
    }
}
