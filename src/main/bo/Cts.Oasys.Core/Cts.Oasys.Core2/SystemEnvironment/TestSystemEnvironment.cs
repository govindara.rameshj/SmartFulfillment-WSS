﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cts.Oasys.Core.SystemEnvironment
{
    public class TestSystemEnvironment : LiveSystemEnvironment  
    {
        public TestSystemEnvironment()
        {
            GetDateTimeNowMethod = () => base.GetDateTimeNow();
            GetResourcesDirectoryPathMethod = base.GetResourcesDirectoryPath;
        }

        public Func<DateTime> GetDateTimeNowMethod { get; set; }

        public override DateTime GetDateTimeNow()
        {
            return GetDateTimeNowMethod();
        }

        public Func<string> GetResourcesDirectoryPathMethod { get; set; }

        public override string GetResourcesDirectoryPath()
        {
            return GetResourcesDirectoryPathMethod();
        }
    }
}
