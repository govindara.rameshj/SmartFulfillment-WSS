Public Class StockLookUp

    Public Enum ItemFilters
        All
        Transfer
    End Enum

    Private _oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB
    Private _stock As BOStock.cStock
    Private _ItemFilter As ItemFilters = ItemFilters.All
    Private _SupplierNumber As String = String.Empty
    Private _allowMulti As Boolean = False
    Private _SkuNumber As String = String.Empty
    Private _SkuNumbers As ArrayList
    Private _StocksSelected As New List(Of BOStock.cStock)
    Private _lvSupplierMouseX As Integer = 0
    Private _lvSupplierMouseY As Integer = 0
    Private _blnReset As Boolean = False

    Public ReadOnly Property SkuNumber() As String
        Get
            Return _SkuNumber
        End Get
    End Property

    Public ReadOnly Property SkuNumbers() As ArrayList
        Get
            Return _SkuNumbers
        End Get
    End Property

    Public ReadOnly Property Stocks() As List(Of BOStock.cStock)
        Get
            Return _StocksSelected
        End Get
    End Property

    Public Property AllowMulti() As Boolean
        Get
            Return _allowMulti
        End Get
        Set(ByVal value As Boolean)
            _allowMulti = value

            If _allowMulti Then
                'lstItem.SelectionMode = SelectionMode.MultiExtended
                lvItem.MultiSelect = CBool(SelectionMode.MultiExtended)
                btnSelectAll.Visible = True
                btnAccept.Left += btnSelectAll.Width + btnSelectAll.Margin.Right + btnAccept.Margin.Left
            End If
        End Set
    End Property

    Public Sub New()
        InitializeComponent()
        _oasys3DB = New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
        Initialise()
    End Sub

    Public Sub New(ByVal supplierNumber As String)
        InitializeComponent()
        _oasys3DB = New OasysDBBO.Oasys3.DB.clsOasys3DB("Default", 0)
        _SupplierNumber = supplierNumber
        Initialise()
    End Sub

    Public Sub New(ByRef oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB)
        InitializeComponent()
        _oasys3DB = oasys3DB
        Initialise()
    End Sub

    Public Sub New(ByRef Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal ItemFilter As ItemFilters)
        InitializeComponent()
        _oasys3DB = Oasys3DB
        _ItemFilter = ItemFilter
        Initialise()
    End Sub

    Public Sub New(ByRef Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal SupplierNumber As String)
        InitializeComponent()
        _oasys3DB = Oasys3DB
        _SupplierNumber = SupplierNumber
        Initialise()
    End Sub

    Private Sub Initialise()

        'get hierarchies to set up treeview from view
        _stock = New BOStock.cStock(_oasys3DB)
        Dim hierarchy As New BOHierarchy.vwHierarchies(_oasys3DB)
        hierarchy.LoadView(True)
        Dim dt As DataTable = hierarchy.Table

        Dim numberCat As String = String.Empty
        Dim numberGroup As String = String.Empty
        Dim numberSub As String = String.Empty
        Dim nodeCat As TreeNode = Nothing
        Dim nodeGroup As TreeNode = Nothing
        Dim nodeSub As TreeNode = Nothing

        For Each dr As DataRow In dt.Rows
            If CStr(dr(BOHierarchy.vwHierarchies.col.CategoryNumber)) <> numberCat Then
                numberCat = CStr(dr(BOHierarchy.vwHierarchies.col.CategoryNumber))
                numberGroup = String.Empty
                numberSub = String.Empty

                nodeCat = New TreeNode
                nodeCat.Tag = numberCat
                nodeCat.Text = CStr(dr(BOHierarchy.vwHierarchies.col.CategoryName))
                trvHierarchy.Nodes.Add(nodeCat)
            End If

            If CStr(dr(BOHierarchy.vwHierarchies.col.GroupNumber)) <> numberGroup Then
                numberGroup = CStr(dr(BOHierarchy.vwHierarchies.col.GroupNumber))
                numberSub = String.Empty

                nodeGroup = New TreeNode
                nodeGroup.Tag = numberGroup
                nodeGroup.Text = CStr(dr(BOHierarchy.vwHierarchies.col.GroupName))
                nodeCat.Nodes.Add(nodeGroup)
            End If

            If CStr(dr(BOHierarchy.vwHierarchies.col.SubgroupNumber)) <> numberSub Then
                numberSub = CStr(dr(BOHierarchy.vwHierarchies.col.SubgroupNumber))

                nodeSub = New TreeNode
                nodeSub.Tag = numberSub
                nodeSub.Text = CStr(dr(BOHierarchy.vwHierarchies.col.SubgroupName))
                nodeGroup.Nodes.Add(nodeSub)
            End If

            Dim nodeStyle As New TreeNode
            nodeStyle.Tag = dr(BOHierarchy.vwHierarchies.col.StyleNumber)
            nodeStyle.Text = CStr(dr(BOHierarchy.vwHierarchies.col.StyleName))
            nodeSub.Nodes.Add(nodeStyle)
        Next

        'cmbSupplier_Initialise()

    End Sub

    Private Sub StockLookUp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        e.Handled = True
        Select Case e.KeyData
            Case Keys.F2 : btnSearch.PerformClick()
            Case Keys.F4 : btnSelectAll.PerformClick()
            Case Keys.F5 : btnAccept.PerformClick()
            Case Keys.F11 : btnReset.PerformClick()
            Case Keys.F12 : btnExit.PerformClick()
            Case Else : e.Handled = False
        End Select

    End Sub

    Private Sub txtNumber_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNumber.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then btnSearch.PerformClick()
        Select Case e.KeyChar
            Case "0"c To "9"c
                e.Handled = False
            Case ChrW(Keys.Back)
                e.Handled = False
            Case ChrW(Keys.Delete)
                e.Handled = False
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txtDescription_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If e.KeyChar = ChrW(Keys.Enter) Then btnSearch.PerformClick()
    End Sub

    Private Sub chkSupplier_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSupplier.CheckedChanged

        If chkSupplier.Checked Then
            txtSupplier.Visible = True
            txtSupplier.Focus()
            lblSupplierInfo.Visible = True
            btnSearch.Enabled = False
        Else
            txtSupplier.Visible = False
            txtSupplier.Clear()
            lblSupplierInfo.Visible = False
        End If

    End Sub

    Private Sub chkHierarchy_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkHierarchy.CheckedChanged
        trvHierarchy.Visible = chkHierarchy.Checked
    End Sub



    Private Sub btnSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectAll.Click

        Select Case btnSelectAll.Text
            Case My.Resources.F4SelectAll
                btnSelectAll.Text = My.Resources.F4UnselectAll
                For index As Integer = 0 To lvItem.Items.Count - 1
                    lvItem.Items(index).Selected = True
                Next

            Case My.Resources.F4UnselectAll
                btnSelectAll.Text = My.Resources.F4SelectAll
                For index As Integer = 0 To lvItem.Items.Count - 1
                    lvItem.Items(index).Selected = False
                Next

        End Select

    End Sub

    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click

        If _SkuNumber <> String.Empty Then
            _SkuNumber = lvItem.SelectedItems(0).SubItems(1).Text
            DialogResult = Windows.Forms.DialogResult.OK
            Close()
        Else
            _SkuNumbers = New ArrayList
            _StocksSelected = New List(Of BOStock.cStock)

            For Each item As ListViewItem In lvItem.SelectedItems
                'Dim stock As New BOStock.cStock(_oasys3DB)
                'stock.LoadFromRow(CType(item, ListViewItem).Tag) ' changed dh
                '_StocksSelected.Add(stock)
                _SkuNumbers.Add(item.SubItems(1).Text)
            Next

            DialogResult = Windows.Forms.DialogResult.OK
            Close()
        End If

    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Try
            Dim strWords() As String = Nothing
            Dim strSQL As String = String.Empty
            Dim blnSkuOrder As Boolean = False
            Dim strData As String = String.Empty

            Cursor = Cursors.WaitCursor
            lvItem.Items.Clear()
            strSQL = "SELECT SKUN, DESCR, PRIC, ONHA, ONOR, SUPP, PROD, IDEL, IOBS, INON, IRIS FROM STKMAS WHERE "
            Dim needsJoin As Boolean = False

            If txtNumber.Text.Trim = "" AndAlso txtDescription.Text.Trim = "" AndAlso txtAlternativeDesc.Text.Trim = "" AndAlso _
                (chkSupplier.Checked = False Or txtSupplier.Text = "") AndAlso chkHierarchy.Checked = False And (chkDeleted.Checked = False Or chkDeleted.Checked = True) _
                AndAlso (chkNonStock.Checked = False Or chkNonStock.Checked = True) Then
                If MessageBox.Show("Are you sure you want to display all items?", "Full Search Requested", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If
            End If

            'check for sku number
            If txtNumber.Text.Length > 0 Then
                If needsJoin Then strSQL += " AND "
                If txtNumber.Text.Length > 6 Then
                    strSQL += " SKUN IN (SELECT SKUN FROM EANMAS WHERE EANMAS.NUMB = '" & Format(CLng(txtNumber.Text.Trim), "0000000000000000") & "') " ' "
                Else
                    strSQL += " SKUN = '" & txtNumber.Text.Trim & "' "
                End If
                needsJoin = True
            End If

            'Check for description
            If chkDescription.Checked AndAlso chkDescription.Enabled Then
                If txtDescription.Text.Length > 0 Then
                    strWords = txtDescription.Text.Trim.Split(" "c)
                    strSQL += "("
                    For Each strWord As String In strWords

                        If chkExactMatch.Checked Then
                            If needsJoin Then strSQL += " OR "
                            strSQL += " ([DESCR] LIKE '% " & txtDescription.Text.Trim & " %' OR "
                            strSQL += " [DESCR] LIKE '" & txtDescription.Text.Trim & " %') "
                            needsJoin = True
                            Exit For
                        Else
                            If strWord.Trim <> String.Empty Then
                                If needsJoin Then strSQL += " OR "
                                strSQL += " [DESCR] LIKE'%" & strWord.Trim & "%' "
                                needsJoin = True
                            End If
                        End If
                    Next strWord
                    strSQL += ")"
                End If
            End If

            strWords = Nothing 'Clear
            'Check for Alternative Description
            If chkAltDescription.Checked AndAlso chkAltDescription.Enabled Then
                If txtAlternativeDesc.Text.Length > 0 Then
                    If needsJoin Then
                        strSQL += " OR "
                        needsJoin = False
                    End If
                    strWords = txtAlternativeDesc.Text.Trim.Split(" "c)
                    strSQL += "("
                    For Each strWord As String In strWords
                        If chkExactMatch.Checked Then
                            If needsJoin Then strSQL += " OR "
                            strSQL += " [EQUV] Like '% " & txtAlternativeDesc.Text.Trim & " %' OR "
                            strSQL += " [EQUV] LIKE '" & txtAlternativeDesc.Text.Trim & " %' "
                            needsJoin = True
                            Exit For
                        Else
                            If strWord.Trim <> String.Empty Then
                                If needsJoin Then strSQL += " OR "
                                strSQL += " [EQUV] Like '%" & strWord.Trim & "%' "
                                needsJoin = True
                            End If
                        End If

                    Next strWord
                    strSQL += ")"
                End If
            End If

            'Check for item filter on sale type
            Select Case _ItemFilter
                Case ItemFilters.Transfer
                    If needsJoin Then strSQL += " AND "
                    strSQL += " SALT = 'G' "
                    needsJoin = True
            End Select

            'Check for supplier
            If txtSupplier.Text.Contains("-") Then
                _SupplierNumber = txtSupplier.Text.Substring(0, 5)
            End If

            If _SupplierNumber <> "" Then
                blnSkuOrder = True
                If needsJoin Then strSQL += " AND "
                strSQL += " SUPP = '" & _SupplierNumber & "' "
                _SupplierNumber = String.Empty
                needsJoin = True
            End If

            'Check if hierarchy node selected
            If chkHierarchy.Checked AndAlso chkHierarchy.Enabled Then
                If trvHierarchy.SelectedNode Is Nothing Then
                    MessageBox.Show("No Hierachy Node has been selected. Make a selection and try again.", "Search Criteria not Met", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If

                blnSkuOrder = True
                Dim node As TreeNode = trvHierarchy.SelectedNode

                Select Case node.Level
                    Case 0
                        If needsJoin Then strSQL += " AND "
                        strSQL += String.Format(" CTGY = '{0}' ", node.Tag)
                        needsJoin = True
                    Case 1
                        If needsJoin Then strSQL += " AND "
                        strSQL += String.Format(" GRUP = '{0}' ", node.Tag)
                        needsJoin = True
                    Case 2
                        If needsJoin Then strSQL += " AND "
                        strSQL += String.Format(" SGRP = '{0}' ", node.Tag)
                        needsJoin = True
                    Case 3
                        If needsJoin Then strSQL += " AND "
                        strSQL += String.Format(" STYL = '{0}' ", node.Tag)
                        needsJoin = True
                    Case Else
                        Debug.Print("Shouldn't get here.")
                End Select
            End If

            If chkNonStock.Checked Then
                If needsJoin Then strSQL += " AND "
                strSQL += " INON = 0"
                needsJoin = True
            End If

            If chkDeleted.Checked Then
                If needsJoin Then strSQL += " AND "
                strSQL += " ((IDEL = 1 AND IOBS = 1) OR (IDEL = 0 AND IOBS = 0) OR (IDEL = 0 AND IOBS = 1) OR (IDEL = 1 AND IOBS = 0)) "
                needsJoin = True
            Else
                If needsJoin Then strSQL += " AND "
                strSQL += " (IDEL = 0 AND IOBS = 0) "
                needsJoin = True
            End If

            If blnSkuOrder Then
                strSQL += " ORDER BY SKUN ASC"
            End If

            Dim ds As DataSet = _oasys3DB.ExecuteSql(strSQL)
            Dim dt As DataTable = ds.Tables(0)

            If dt.Rows.Count = 0 Then
                MessageBox.Show("No Matches Found.", "Search", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

            dt.Columns.Add("display", GetType(String), _stock.SkuNumber.ColumnName & " + '   ' + " & _stock.Description.ColumnName)

            For Each row As DataRow In dt.Rows
                If CStr(row.Item("IDEL")) = "True" Then
                    strData = "Deleted"
                Else
                    If CStr(row.Item("IOBS")) = "True" Then
                        strData = "Obsolete"
                    Else
                        If CStr(row.Item("INON")) = "True" Then
                            strData = "Non-Stock"
                        Else
                            If CStr(row.Item("IRIS")) = "True" Then
                                strData = "Single"
                            Else
                                strData = " "
                            End If
                        End If
                    End If
                End If

                Dim myItem As ListViewItem = lvItem.Items.Add(strData)
                myItem.SubItems.Add(CStr(row.Item("SKUN")))
                myItem.SubItems.Add(CStr(row.Item("DESCR")))
                myItem.SubItems.Add(CStr(row.Item("PRIC")))
                myItem.SubItems.Add(CStr(row.Item("ONHA")))
                myItem.SubItems.Add(CStr(row.Item("ONOR")))
                myItem.SubItems.Add(CStr(row.Item("SUPP")))
                myItem.SubItems.Add(CStr(row.Item("PROD")))

            Next row

            lvItem.Columns(3).TextAlign = HorizontalAlignment.Right
            lvItem.Columns(4).TextAlign = HorizontalAlignment.Right
            lvItem.Columns(5).TextAlign = HorizontalAlignment.Right
            lvItem.Columns(6).TextAlign = HorizontalAlignment.Right
            lvItem.Columns(7).TextAlign = HorizontalAlignment.Left

            lvItem.Columns(0).Width = 60
            lvItem.Columns(1).Width = 50
            lvItem.Columns(2).Width = 250
            lvItem.Columns(3).Width = 60
            lvItem.Columns(4).Width = 60
            lvItem.Columns(5).Width = 60
            lvItem.Columns(6).Width = 60

            ToolStripStatusLabel1.Text = "Item Count = " & lvItem.Items.Count.ToString

        Catch ex As Exception
            MessageBox.Show(My.Resources.ProblemGetItems, Me.Text)

        Finally
            Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click

        txtNumber.Clear()
        txtDescription.Clear()
        txtAlternativeDesc.Clear()
        txtSupplier.Clear()
        txtNumber.Focus()
        chkDescription.Checked = False
        chkAltDescription.Checked = False
        chkDeleted.Checked = False
        chkHierarchy.Checked = False
        chkSupplier.Checked = False
        chkNonStock.Checked = False
        lvItem.Items.Clear()
        ToolStripStatusLabel1.Text = "Item Count = 0"

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub



    Private Sub lvItem_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvItem.DoubleClick
        If _allowMulti Then
            If lvItem.SelectedItems.Count <> 1 Then Exit Sub
        End If
        If lvItem.SelectedItems.Count = 1 Then
            _SkuNumber = lvItem.SelectedItems(0).SubItems(1).Text
            lvItem.SelectedItems.Clear()
        End If

        btnAccept.PerformClick()
    End Sub

    Private Sub lvItem_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvItem.GotFocus
        If lvItem.Items.Count > 0 Then
            If lvItem.Items.Count = 0 Then
                SendKeys.Send("{END}")
                SendKeys.Send("{HOME}")
            End If
            btnAccept.Enabled = True
        End If
    End Sub

    Private Sub lvItem_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles lvItem.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then btnAccept.PerformClick()
    End Sub

    Private Sub lvItem_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvItem.LostFocus
        If lvItem.SelectedItems.Count = 1 Then
            _SkuNumber = lvItem.SelectedItems(0).SubItems(1).Text
            lvItem.SelectedItems.Clear()
        End If
        btnAccept.Enabled = False
    End Sub

    Private Sub lvItem_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lvItem.MouseDown
        Dim info As ListViewHitTestInfo = lvItem.HitTest(e.X, e.Y)

        If info.Location.ToString() = "None" Then
            btnAccept.Enabled = False
        Else
            btnAccept.Enabled = True
        End If

    End Sub

    Private Sub lvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvItem.SelectedIndexChanged
        btnAccept.Enabled = True
        btnSelectAll.Enabled = True
    End Sub



    Private Sub txtNumber_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNumber.TextChanged
        If txtNumber.Text.Length > 0 Then
            SetAllOtherSearchMethods(False)
            chkAltDescription.Checked = False
            chkDescription.Checked = False
            chkHierarchy.Checked = False
            chkNonStock.Checked = False
            chkSupplier.Checked = False
        Else
            SetAllOtherSearchMethods(True)
        End If
    End Sub

    Private Sub SetAllOtherSearchMethods(ByVal blnStyle As Boolean)
        chkAltDescription.Enabled = blnStyle
        chkDeleted.Enabled = blnStyle
        chkDescription.Enabled = blnStyle
        chkHierarchy.Enabled = blnStyle
        chkNonStock.Enabled = blnStyle
        chkSupplier.Enabled = blnStyle
        chkSupplier.Enabled = blnStyle

    End Sub

    Private Sub StockLookUp_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ToolStripStatusLabel1.Text = "Item Count = 0"
        txtDescription.Visible = False
        txtAlternativeDesc.Visible = False
        txtSupplier.Visible = False
    End Sub

    Private Sub chkAltDescription_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAltDescription.CheckedChanged
        If chkAltDescription.Checked Then
            txtAlternativeDesc.Visible = True
            txtAlternativeDesc.Focus()
            chkDescription.Enabled = False
        Else
            txtAlternativeDesc.Visible = False
            txtAlternativeDesc.Clear()
            chkDescription.Enabled = True
        End If
    End Sub

    Private Sub chkDescription_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDescription.CheckedChanged
        If chkDescription.Checked Then
            txtDescription.Visible = True
            txtDescription.Focus()
            chkAltDescription.Enabled = False
        Else
            txtDescription.Visible = False
            txtDescription.Clear()
            chkAltDescription.Enabled = True
        End If
    End Sub

    Private Sub txtSupplier_ControlAdded(ByVal sender As Object, ByVal e As System.Windows.Forms.ControlEventArgs) Handles txtSupplier.ControlAdded

    End Sub

    Private Sub txtSupplier_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSupplier.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            If txtSupplier.Text.Contains("-") Then
                txtSupplier.Text = txtSupplier.Text.Substring(0, 5)
            End If
            If IsNumeric(txtSupplier.Text.Trim) Then
                If LoadSuppliers(True) = False Then
                    Exit Sub
                End If
            Else
                If LoadSuppliers(False) = False Then
                    Exit Sub
                End If
            End If

            grpSupplier.Visible = True
            lvSupplier.Focus()
            Me.Refresh()
            Application.DoEvents()

        End If

    End Sub

    Private Sub txtSupplier_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSupplier.LostFocus
        'grpSupplier.Visible = False
    End Sub

    Private Function LoadSuppliers(ByVal blnLoadByNumber As Boolean) As Boolean

        Dim strSql As String = String.Empty
        Dim strNumber As String = String.Empty

        lvSupplier.Items.Clear()

        strSql = "SELECT DISTINCT [SUPMAS].[SUPN], [SUPMAS].[NAME], [SUPMAS].[ALPH] " & _
                 "FROM [SUPMAS] "
        If blnLoadByNumber = True Then
            strNumber = Format(CInt(txtSupplier.Text.Trim), "00000")
            strSql += "WHERE [SUPMAS].[SUPN] = '" & strNumber & "'"
        Else
            strSql += "WHERE [SUPMAS].[NAME] Like '%" & txtSupplier.Text.Trim & "%' "
        End If
        strSql += "ORDER BY SUPN ASC"

        Dim ds As DataSet = _oasys3DB.ExecuteSql(strSql)
        Dim dt As DataTable = ds.Tables(0)

        If dt.Rows.Count = 0 Then

            MessageBox.Show("No Matches Found.", "Supplier Search", MessageBoxButtons.OK, MessageBoxIcon.Information)
            btnCancel.PerformClick()
            Return False

        Else

            For Each row As DataRow In dt.Rows

                Dim myItem As ListViewItem = lvSupplier.Items.Add(CStr(row.Item("SUPN")))
                myItem.SubItems.Add(CStr(row.Item("NAME")))
                myItem.SubItems.Add(CStr(row.Item("ALPH")))

            Next row
        End If

        Return True

    End Function

    Private Sub lvSupplier_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvSupplier.Click
        If _blnReset Then
            lvSupplier.Items(lvSupplier.Items.Count - 1).Selected = True
            lvSupplier.Focus()
            lvSupplier.Select()
        End If
    End Sub

    Private Sub lvSupplier_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvSupplier.DoubleClick
        btnOk.PerformClick()
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click

        If lvSupplier.SelectedItems.Count = 1 Then
            txtSupplier.Text = lvSupplier.SelectedItems(0).SubItems(0).Text & " - " & lvSupplier.SelectedItems(0).SubItems(1).Text
            _SupplierNumber = lvSupplier.SelectedItems(0).SubItems(0).Text
        Else
            txtSupplier.Text = ""
            _SupplierNumber = ""
        End If

        grpSupplier.Visible = False
        lblSupplierInfo.Visible = False
        btnSearch.Enabled = True
        txtSupplier.Focus()

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        lblSupplierInfo.Visible = False
        grpSupplier.Visible = False
        btnSearch.Enabled = True
        txtSupplier.Focus()
    End Sub

    Private Sub lvSupplier_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvSupplier.GotFocus
        If lvSupplier.Items.Count > 0 Then
            SendKeys.Send("{END}")
            SendKeys.Send("{HOME}")
        End If
    End Sub

    Private Sub lvSupplier_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles lvSupplier.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            btnOk.PerformClick()
        End If

        If e.KeyChar = ChrW(Keys.Escape) Then
            btnCancel.PerformClick()
        End If

        If e.KeyChar = ChrW(Keys.Space) Then
            e.Handled = True
        End If

    End Sub

    Private Sub lvSupplier_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lvSupplier.MouseDown

        Dim info As ListViewHitTestInfo = lvSupplier.HitTest(e.X, e.Y)

        If info.Location.ToString() = "None" Then
            _blnReset = True
        Else
            _blnReset = False
        End If

    End Sub

    Private Sub txtAlternativeDesc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAlternativeDesc.TextChanged
        If txtAlternativeDesc.Text.Length > 0 And btnSearch.Enabled = False Then
            btnSearch.Enabled = True
        End If
    End Sub

    Private Sub txtDescription_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDescription.TextChanged
        If txtDescription.Text.Length > 0 And btnSearch.Enabled = False Then
            btnSearch.Enabled = True
        End If
    End Sub

    Private Sub chkNonStock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkNonStock.Click
        If btnSearch.Enabled = False Then
            btnSearch.Enabled = True
        End If
    End Sub

    Private Sub chkDeleted_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkDeleted.Click
        If btnSearch.Enabled = False Then
            btnSearch.Enabled = True
        End If
    End Sub

    Private Sub chkExactMatch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkExactMatch.Click
        If btnSearch.Enabled = False Then
            btnSearch.Enabled = True
        End If
    End Sub
End Class