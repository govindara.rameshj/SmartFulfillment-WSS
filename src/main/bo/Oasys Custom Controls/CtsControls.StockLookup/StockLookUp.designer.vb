<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StockLookUp
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lblSupplierInfo = New System.Windows.Forms.Label
        Me.txtDescription = New System.Windows.Forms.TextBox
        Me.txtSupplier = New System.Windows.Forms.TextBox
        Me.chkExactMatch = New System.Windows.Forms.CheckBox
        Me.chkAltDescription = New System.Windows.Forms.CheckBox
        Me.chkDescription = New System.Windows.Forms.CheckBox
        Me.chkDeleted = New System.Windows.Forms.CheckBox
        Me.chkNonStock = New System.Windows.Forms.CheckBox
        Me.txtAlternativeDesc = New System.Windows.Forms.TextBox
        Me.txtNumber = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.chkHierarchy = New System.Windows.Forms.CheckBox
        Me.chkSupplier = New System.Windows.Forms.CheckBox
        Me.trvHierarchy = New System.Windows.Forms.TreeView
        Me.btnAccept = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnReset = New System.Windows.Forms.Button
        Me.btnSearch = New System.Windows.Forms.Button
        Me.grpResults = New System.Windows.Forms.GroupBox
        Me.lvItem = New System.Windows.Forms.ListView
        Me.ColStatus = New System.Windows.Forms.ColumnHeader
        Me.ColSKU = New System.Windows.Forms.ColumnHeader
        Me.ColDescription = New System.Windows.Forms.ColumnHeader
        Me.ColPrice = New System.Windows.Forms.ColumnHeader
        Me.ColOnHand = New System.Windows.Forms.ColumnHeader
        Me.ColOnOrder = New System.Windows.Forms.ColumnHeader
        Me.ColSupplier = New System.Windows.Forms.ColumnHeader
        Me.ColRefNo = New System.Windows.Forms.ColumnHeader
        Me.btnSelectAll = New System.Windows.Forms.Button
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
        Me.grpSupplier = New System.Windows.Forms.GroupBox
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnOk = New System.Windows.Forms.Button
        Me.lvSupplier = New System.Windows.Forms.ListView
        Me.colNumb = New System.Windows.Forms.ColumnHeader
        Me.colName = New System.Windows.Forms.ColumnHeader
        Me.colAlphaKey = New System.Windows.Forms.ColumnHeader
        Me.SupplierName = New System.Windows.Forms.ColumnHeader
        Me.Address = New System.Windows.Forms.ColumnHeader
        Me.AlphaKey = New System.Windows.Forms.ColumnHeader
        Me.GroupBox1.SuspendLayout()
        Me.grpResults.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.grpSupplier.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.lblSupplierInfo)
        Me.GroupBox1.Controls.Add(Me.txtDescription)
        Me.GroupBox1.Controls.Add(Me.txtSupplier)
        Me.GroupBox1.Controls.Add(Me.chkExactMatch)
        Me.GroupBox1.Controls.Add(Me.chkAltDescription)
        Me.GroupBox1.Controls.Add(Me.chkDescription)
        Me.GroupBox1.Controls.Add(Me.chkDeleted)
        Me.GroupBox1.Controls.Add(Me.chkNonStock)
        Me.GroupBox1.Controls.Add(Me.txtAlternativeDesc)
        Me.GroupBox1.Controls.Add(Me.txtNumber)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.chkHierarchy)
        Me.GroupBox1.Controls.Add(Me.chkSupplier)
        Me.GroupBox1.Controls.Add(Me.trvHierarchy)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(358, 503)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Enter Selection Criteria"
        '
        'lblSupplierInfo
        '
        Me.lblSupplierInfo.AutoSize = True
        Me.lblSupplierInfo.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblSupplierInfo.Location = New System.Drawing.Point(43, 115)
        Me.lblSupplierInfo.Name = "lblSupplierInfo"
        Me.lblSupplierInfo.Size = New System.Drawing.Size(271, 13)
        Me.lblSupplierInfo.TabIndex = 20
        Me.lblSupplierInfo.Text = "Please Type Supplier Number or Name and Press Enter."
        Me.lblSupplierInfo.Visible = False
        '
        'txtDescription
        '
        Me.txtDescription.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDescription.Location = New System.Drawing.Point(132, 40)
        Me.txtDescription.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(223, 20)
        Me.txtDescription.TabIndex = 1
        '
        'txtSupplier
        '
        Me.txtSupplier.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSupplier.Location = New System.Drawing.Point(132, 87)
        Me.txtSupplier.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.txtSupplier.Name = "txtSupplier"
        Me.txtSupplier.Size = New System.Drawing.Size(223, 20)
        Me.txtSupplier.TabIndex = 19
        '
        'chkExactMatch
        '
        Me.chkExactMatch.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkExactMatch.Location = New System.Drawing.Point(194, 155)
        Me.chkExactMatch.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.chkExactMatch.Name = "chkExactMatch"
        Me.chkExactMatch.Size = New System.Drawing.Size(90, 21)
        Me.chkExactMatch.TabIndex = 18
        Me.chkExactMatch.Text = "Exact Match"
        Me.chkExactMatch.UseVisualStyleBackColor = True
        '
        'chkAltDescription
        '
        Me.chkAltDescription.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkAltDescription.Location = New System.Drawing.Point(6, 63)
        Me.chkAltDescription.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.chkAltDescription.Name = "chkAltDescription"
        Me.chkAltDescription.Size = New System.Drawing.Size(121, 21)
        Me.chkAltDescription.TabIndex = 3
        Me.chkAltDescription.Text = "Alt Description"
        Me.chkAltDescription.UseVisualStyleBackColor = True
        '
        'chkDescription
        '
        Me.chkDescription.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkDescription.Location = New System.Drawing.Point(6, 40)
        Me.chkDescription.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.chkDescription.Name = "chkDescription"
        Me.chkDescription.Size = New System.Drawing.Size(121, 21)
        Me.chkDescription.TabIndex = 1
        Me.chkDescription.Text = "Description"
        Me.chkDescription.UseVisualStyleBackColor = True
        '
        'chkDeleted
        '
        Me.chkDeleted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkDeleted.Location = New System.Drawing.Point(132, 134)
        Me.chkDeleted.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.chkDeleted.Name = "chkDeleted"
        Me.chkDeleted.Size = New System.Drawing.Size(152, 21)
        Me.chkDeleted.TabIndex = 8
        Me.chkDeleted.Text = "Include Obsolete/Deleted"
        Me.chkDeleted.UseVisualStyleBackColor = True
        '
        'chkNonStock
        '
        Me.chkNonStock.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkNonStock.Location = New System.Drawing.Point(6, 132)
        Me.chkNonStock.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.chkNonStock.Name = "chkNonStock"
        Me.chkNonStock.Size = New System.Drawing.Size(121, 21)
        Me.chkNonStock.TabIndex = 7
        Me.chkNonStock.Text = "Exclude Non-Stock"
        Me.chkNonStock.UseVisualStyleBackColor = True
        '
        'txtAlternativeDesc
        '
        Me.txtAlternativeDesc.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAlternativeDesc.Location = New System.Drawing.Point(132, 64)
        Me.txtAlternativeDesc.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.txtAlternativeDesc.Name = "txtAlternativeDesc"
        Me.txtAlternativeDesc.Size = New System.Drawing.Size(223, 20)
        Me.txtAlternativeDesc.TabIndex = 4
        '
        'txtNumber
        '
        Me.txtNumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNumber.Location = New System.Drawing.Point(132, 17)
        Me.txtNumber.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.txtNumber.MaxLength = 16
        Me.txtNumber.Name = "txtNumber"
        Me.txtNumber.Size = New System.Drawing.Size(223, 20)
        Me.txtNumber.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(6, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(121, 20)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Sku Number or EAN"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkHierarchy
        '
        Me.chkHierarchy.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkHierarchy.Location = New System.Drawing.Point(6, 156)
        Me.chkHierarchy.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.chkHierarchy.Name = "chkHierarchy"
        Me.chkHierarchy.Size = New System.Drawing.Size(121, 20)
        Me.chkHierarchy.TabIndex = 9
        Me.chkHierarchy.Text = "Hierarchy"
        Me.chkHierarchy.UseVisualStyleBackColor = True
        '
        'chkSupplier
        '
        Me.chkSupplier.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkSupplier.Location = New System.Drawing.Point(6, 87)
        Me.chkSupplier.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.chkSupplier.Name = "chkSupplier"
        Me.chkSupplier.Size = New System.Drawing.Size(121, 21)
        Me.chkSupplier.TabIndex = 5
        Me.chkSupplier.Text = "Supplier"
        Me.chkSupplier.UseVisualStyleBackColor = True
        '
        'trvHierarchy
        '
        Me.trvHierarchy.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.trvHierarchy.HideSelection = False
        Me.trvHierarchy.Location = New System.Drawing.Point(6, 179)
        Me.trvHierarchy.Name = "trvHierarchy"
        Me.trvHierarchy.Size = New System.Drawing.Size(346, 321)
        Me.trvHierarchy.TabIndex = 10
        Me.trvHierarchy.Visible = False
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Enabled = False
        Me.btnAccept.Location = New System.Drawing.Point(367, 515)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(76, 39)
        Me.btnAccept.TabIndex = 4
        Me.btnAccept.TabStop = False
        Me.btnAccept.Text = "F5 Accept"
        Me.btnAccept.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(839, 515)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 14
        Me.btnExit.TabStop = False
        Me.btnExit.Text = "F12 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.Location = New System.Drawing.Point(760, 515)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(76, 39)
        Me.btnReset.TabIndex = 13
        Me.btnReset.Text = "F11 Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.Location = New System.Drawing.Point(6, 515)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(76, 39)
        Me.btnSearch.TabIndex = 11
        Me.btnSearch.Text = "F2 Search"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'grpResults
        '
        Me.grpResults.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpResults.Controls.Add(Me.lvItem)
        Me.grpResults.Location = New System.Drawing.Point(378, 6)
        Me.grpResults.Name = "grpResults"
        Me.grpResults.Size = New System.Drawing.Size(551, 503)
        Me.grpResults.TabIndex = 5
        Me.grpResults.TabStop = False
        Me.grpResults.Text = "Search Results"
        '
        'lvItem
        '
        Me.lvItem.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColStatus, Me.ColSKU, Me.ColDescription, Me.ColPrice, Me.ColOnHand, Me.ColOnOrder, Me.ColSupplier, Me.ColRefNo})
        Me.lvItem.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvItem.FullRowSelect = True
        Me.lvItem.GridLines = True
        Me.lvItem.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvItem.Location = New System.Drawing.Point(3, 16)
        Me.lvItem.MultiSelect = False
        Me.lvItem.Name = "lvItem"
        Me.lvItem.Size = New System.Drawing.Size(545, 484)
        Me.lvItem.TabIndex = 14
        Me.lvItem.UseCompatibleStateImageBehavior = False
        Me.lvItem.View = System.Windows.Forms.View.Details
        '
        'ColStatus
        '
        Me.ColStatus.Text = "Status"
        Me.ColStatus.Width = 46
        '
        'ColSKU
        '
        Me.ColSKU.Text = "SKU"
        Me.ColSKU.Width = 52
        '
        'ColDescription
        '
        Me.ColDescription.Text = "Description"
        Me.ColDescription.Width = 169
        '
        'ColPrice
        '
        Me.ColPrice.Text = "Price"
        Me.ColPrice.Width = 44
        '
        'ColOnHand
        '
        Me.ColOnHand.Text = "On Hand"
        '
        'ColOnOrder
        '
        Me.ColOnOrder.Text = "On Order"
        '
        'ColSupplier
        '
        Me.ColSupplier.Text = "Supplier"
        Me.ColSupplier.Width = 69
        '
        'ColRefNo
        '
        Me.ColRefNo.Text = "Sup Ref No:"
        Me.ColRefNo.Width = 78
        '
        'btnSelectAll
        '
        Me.btnSelectAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSelectAll.Enabled = False
        Me.btnSelectAll.Location = New System.Drawing.Point(367, 515)
        Me.btnSelectAll.Name = "btnSelectAll"
        Me.btnSelectAll.Size = New System.Drawing.Size(76, 39)
        Me.btnSelectAll.TabIndex = 12
        Me.btnSelectAll.TabStop = False
        Me.btnSelectAll.Text = "F4 Select All"
        Me.btnSelectAll.UseVisualStyleBackColor = True
        Me.btnSelectAll.Visible = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(3, 557)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(918, 22)
        Me.StatusStrip1.TabIndex = 14
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(74, 17)
        Me.ToolStripStatusLabel1.Text = "Item Count: 0"
        '
        'grpSupplier
        '
        Me.grpSupplier.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.grpSupplier.Controls.Add(Me.btnCancel)
        Me.grpSupplier.Controls.Add(Me.btnOk)
        Me.grpSupplier.Controls.Add(Me.lvSupplier)
        Me.grpSupplier.Location = New System.Drawing.Point(367, 93)
        Me.grpSupplier.Name = "grpSupplier"
        Me.grpSupplier.Size = New System.Drawing.Size(536, 289)
        Me.grpSupplier.TabIndex = 22
        Me.grpSupplier.TabStop = False
        Me.grpSupplier.Text = "Choose Supplier"
        Me.grpSupplier.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(271, 261)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(82, 21)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnOk
        '
        Me.btnOk.Location = New System.Drawing.Point(183, 261)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(82, 21)
        Me.btnOk.TabIndex = 4
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'lvSupplier
        '
        Me.lvSupplier.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lvSupplier.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colNumb, Me.colName, Me.colAlphaKey})
        Me.lvSupplier.FullRowSelect = True
        Me.lvSupplier.GridLines = True
        Me.lvSupplier.HideSelection = False
        Me.lvSupplier.Location = New System.Drawing.Point(6, 19)
        Me.lvSupplier.MultiSelect = False
        Me.lvSupplier.Name = "lvSupplier"
        Me.lvSupplier.Size = New System.Drawing.Size(525, 236)
        Me.lvSupplier.TabIndex = 3
        Me.lvSupplier.UseCompatibleStateImageBehavior = False
        Me.lvSupplier.View = System.Windows.Forms.View.Details
        '
        'colNumb
        '
        Me.colNumb.Text = "Number"
        '
        'colName
        '
        Me.colName.Text = "Name"
        Me.colName.Width = 262
        '
        'colAlphaKey
        '
        Me.colAlphaKey.Text = "Alpha Key"
        Me.colAlphaKey.Width = 184
        '
        'SupplierName
        '
        Me.SupplierName.Text = "Name"
        Me.SupplierName.Width = 145
        '
        'Address
        '
        Me.Address.Text = "Address"
        Me.Address.Width = 145
        '
        'AlphaKey
        '
        Me.AlphaKey.Text = "Alpha Key"
        Me.AlphaKey.Width = 155
        '
        'StockLookUp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnExit
        Me.ClientSize = New System.Drawing.Size(924, 582)
        Me.ControlBox = False
        Me.Controls.Add(Me.grpSupplier)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnSelectAll)
        Me.Controls.Add(Me.grpResults)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnAccept)
        Me.Controls.Add(Me.GroupBox1)
        Me.KeyPreview = True
        Me.Name = "StockLookUp"
        Me.Padding = New System.Windows.Forms.Padding(3)
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Stock Item Enquiry"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.grpResults.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.grpSupplier.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnAccept As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents trvHierarchy As System.Windows.Forms.TreeView
    Friend WithEvents grpResults As System.Windows.Forms.GroupBox
    Friend WithEvents chkHierarchy As System.Windows.Forms.CheckBox
    Friend WithEvents chkSupplier As System.Windows.Forms.CheckBox
    Friend WithEvents btnSelectAll As System.Windows.Forms.Button
    Friend WithEvents txtNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtAlternativeDesc As System.Windows.Forms.TextBox
    Friend WithEvents chkDeleted As System.Windows.Forms.CheckBox
    Friend WithEvents chkNonStock As System.Windows.Forms.CheckBox
    Friend WithEvents lvItem As System.Windows.Forms.ListView
    Friend WithEvents ColStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColSKU As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColDescription As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColPrice As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColOnHand As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColOnOrder As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColSupplier As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColRefNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkDescription As System.Windows.Forms.CheckBox
    Friend WithEvents chkAltDescription As System.Windows.Forms.CheckBox
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents chkExactMatch As System.Windows.Forms.CheckBox
    Friend WithEvents txtSupplier As System.Windows.Forms.TextBox
    Friend WithEvents grpSupplier As System.Windows.Forms.GroupBox
    Friend WithEvents lvSupplier As System.Windows.Forms.ListView
    'Friend WithEvents SupplierNumber As System.Windows.Forms.ColumnHeader
    Friend WithEvents SupplierName As System.Windows.Forms.ColumnHeader
    Friend WithEvents Address As System.Windows.Forms.ColumnHeader
    Friend WithEvents AlphaKey As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents colNumb As System.Windows.Forms.ColumnHeader
    Friend WithEvents colName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colAlphaKey As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents lblSupplierInfo As System.Windows.Forms.Label
End Class
