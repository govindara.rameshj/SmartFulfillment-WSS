﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CollapsibleGroupBox
    Inherits System.Windows.Forms.GroupBox

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CollapsibleGroupBox))
        Me.imgCollapse = New System.Windows.Forms.PictureBox
        Me.imgExpand = New System.Windows.Forms.PictureBox
        CType(Me.imgCollapse, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgExpand, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'imgCollapse
        '
        Me.imgCollapse.Image = CType(resources.GetObject("imgCollapse.Image"), System.Drawing.Image)
        Me.imgCollapse.Location = New System.Drawing.Point(5, 2)
        Me.imgCollapse.Name = "imgCollapse"
        Me.imgCollapse.Size = New System.Drawing.Size(9, 9)
        Me.imgCollapse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.imgCollapse.TabIndex = 0
        Me.imgCollapse.TabStop = False
        '
        'imgExpand
        '
        Me.imgExpand.Image = CType(resources.GetObject("imgExpand.Image"), System.Drawing.Image)
        Me.imgExpand.Location = New System.Drawing.Point(5, 2)
        Me.imgExpand.Name = "imgExpand"
        Me.imgExpand.Size = New System.Drawing.Size(9, 9)
        Me.imgExpand.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.imgExpand.TabIndex = 0
        Me.imgExpand.TabStop = False
        Me.imgExpand.Visible = False
        '
        'CollapsibleGroupBox
        '
        Me.Controls.Add(Me.imgCollapse)
        Me.Controls.Add(Me.imgExpand)
        CType(Me.imgCollapse, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgExpand, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents imgCollapse As System.Windows.Forms.PictureBox
    Friend WithEvents imgExpand As System.Windows.Forms.PictureBox

End Class
