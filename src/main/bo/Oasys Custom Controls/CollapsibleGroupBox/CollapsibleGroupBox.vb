﻿Public Class CollapsibleGroupBox
    Public Enum Styles
        Height
        Width
    End Enum
    Public Enum Layouts
        Horizontal
        Vertical
    End Enum
    Private Enum States
        None = 0
        Collapsed
        Expanded
    End Enum
    Private _FullSize As Size = Me.Size
    Private _MinHeight As Integer = 18
    Private _MinWidth As Integer = 18
    Private _State As States = States.Expanded
    Private _Style As Styles = Styles.Width
    Private _ControlStates As Dictionary(Of Control, Boolean) = Nothing
    Public Event Collapsed(ByVal sender As Object, ByVal e As CollapsibleEventArgs)

    <Description("Height when collapsed")> _
    Public Property MinHeight() As Integer
        Get
            Return _MinHeight
        End Get
        Set(ByVal value As Integer)
            If value >= 18 Then _MinHeight = value
        End Set
    End Property
    <Description("Width when collapsed")> _
    Public Property MinWidth() As Integer
        Get
            Return _MinWidth
        End Get
        Set(ByVal value As Integer)
            If value >= 18 Then _MinWidth = value
        End Set
    End Property
    Public Property Style() As Styles
        Get
            Return _Style
        End Get
        Set(ByVal value As Styles)
            _Style = value
        End Set
    End Property

    Sub New()
        InitializeComponent()
    End Sub


    Private Sub CollapsableGroupBox_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize

        Select Case _State
            Case States.Collapsed
                Select Case _Style
                    Case Styles.Height : _FullSize.Width = Size.Width
                    Case Styles.Width : _FullSize.Height = Size.Height
                End Select

            Case States.Expanded
                _FullSize = Size
        End Select

    End Sub

    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        Dim textSize As Size = TextRenderer.MeasureText(Me.Text & Space(2), Me.Font)
        Dim borderRect As Rectangle = Me.ClientRectangle

        borderRect.Y = CInt(borderRect.Y + (textSize.Height / 2))
        borderRect.Height = CInt(borderRect.Height - (textSize.Height / 2))
        ControlPaint.DrawBorder(e.Graphics, borderRect, Color.LightGray, ButtonBorderStyle.Solid)

        If _State = States.Collapsed And _Style = Styles.Width Then
            Dim textRect As New Rectangle(0, 20, textSize.Height, textSize.Width)
            Dim format As New StringFormat
            format.FormatFlags = StringFormatFlags.DirectionVertical
            format.LineAlignment = StringAlignment.Far
            format.Alignment = StringAlignment.Near
            e.Graphics.FillRectangle(New SolidBrush(Me.BackColor), textRect)
            e.Graphics.DrawString(Me.Text, Me.Font, New SolidBrush(Color.Blue), textRect, format)
        Else
            Dim textRect As New Rectangle(20, 0, textSize.Width, textSize.Height)
            e.Graphics.FillRectangle(New SolidBrush(Me.BackColor), textRect)
            e.Graphics.DrawString(Me.Text, Me.Font, New SolidBrush(Color.Blue), textRect)
        End If

    End Sub

    Private Sub imgCollapse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCollapse.Click

        _State = States.None
        imgExpand.Visible = True
        imgCollapse.Visible = False

        Dim heightChange As Integer = _FullSize.Height - _MinHeight
        Dim widthChange As Integer = _FullSize.Width - _MinWidth

        'get all controls and visibility state into dictionary and make all controls non visible
        _ControlStates = New Dictionary(Of Control, Boolean)
        For Each ctl As Control In Controls
            Select Case ctl.Name
                Case imgCollapse.Name
                Case imgExpand.Name
                Case Else
                    _ControlStates.Add(ctl, ctl.Visible)
                    ctl.Visible = False
            End Select
        Next

        Select Case _Style
            Case Styles.Height
                Me.SuspendLayout()
                'For Each ctl As Control In Controls
                '    If ctl.Name = "imgExpand" Then Continue For
                '    If ctl.Name = "imgCollapse" Then Continue For
                '    ctl.Top += _MinHeight
                'Next
                Height = _MinHeight
                Me.ResumeLayout()

                Me.Parent.SuspendLayout()
                For Each ctl As Control In Me.Parent.Controls
                    'check if ctl is not groupbox and within groupbox limits
                    If ctl.Name = Me.Name Then Continue For
                    If ctl.Bottom < Me.Top Then Continue For
                    If ctl.Left > Me.Right Then Continue For
                    If ctl.Right < Me.Left Then Continue For

                    'ctl is in correct region to resize so move and/or resize if top anchored
                    If CBool(ctl.Anchor And AnchorStyles.Top) Then
                        ctl.Top -= heightChange
                        If CBool(ctl.Anchor And AnchorStyles.Bottom) Then ctl.Height += heightChange
                    End If
                Next
                Me.Parent.ResumeLayout()


            Case Styles.Width
                'Me.SuspendLayout()
                'For Each ctl As Control In Controls
                '    If ctl.Name = "imgExpand" Then Continue For
                '    If ctl.Name = "imgCollapse" Then Continue For
                '    ctl.Left += _MinWidth
                'Next
                'Me.ResumeLayout()

                Width = _MinWidth
                Me.Parent.SuspendLayout()
                For Each ctl As Control In Me.Parent.Controls
                    'skip if is this groupbox control
                    If ctl.Name = Me.Name Then Continue For

                    'check if ctl is within groupbox limits
                    If ctl.Bottom < Me.Top Then Continue For
                    If ctl.Top > Me.Bottom Then Continue For
                    If ctl.Right < Me.Left Then Continue For

                    'ctl is in correct region to resize so move and/or resize
                    If CBool(ctl.Anchor And AnchorStyles.Left) Then
                        ctl.Left -= widthChange
                        If CBool(ctl.Anchor And AnchorStyles.Right) Then ctl.Width += widthChange
                    End If
                Next
                Me.Parent.ResumeLayout()

        End Select

        _State = States.Collapsed
        RaiseEvent Collapsed(sender, New CollapsibleEventArgs(True, heightChange, widthChange))

    End Sub

    Private Sub imgExpand_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgExpand.Click

        _State = States.None
        imgExpand.Visible = False
        imgCollapse.Visible = True

        Dim heightChange As Integer = _FullSize.Height - _MinHeight
        Dim widthChange As Integer = _FullSize.Width - _MinWidth

        'make controls visible
        For Each ctl As Control In Controls
            Select Case ctl.Name
                Case imgCollapse.Name
                Case imgExpand.Name
                Case Else
                    ctl.Visible = _ControlStates.Item(ctl)
            End Select
        Next

        Select Case _Style
            Case Styles.Height
                'Me.SuspendLayout()
                'For Each ctl As Control In Controls
                '    If ctl.Name = "imgExpand" Then Continue For
                '    If ctl.Name = "imgCollapse" Then Continue For
                '    ctl.Top -= _MinHeight
                'Next
                Height = _FullSize.Height
                'Me.ResumeLayout()

                Me.Parent.SuspendLayout()
                For Each ctl As Control In Me.Parent.Controls
                    'check if ctl is not groupbox and within groupbox limits
                    If ctl.Name = Me.Name Then Continue For
                    If ctl.Bottom < Me.Top Then Continue For
                    If ctl.Left > Me.Right Then Continue For
                    If ctl.Right < Me.Left Then Continue For

                    'ctl is in correct region to resize so move and/or resize if top anchored
                    If CBool(ctl.Anchor And AnchorStyles.Top) Then
                        ctl.Top += heightChange
                        If CBool(ctl.Anchor And AnchorStyles.Bottom) Then ctl.Height -= heightChange
                    End If
                Next
                Me.Parent.ResumeLayout()

            Case Styles.Width
                'Me.SuspendLayout()
                'For Each ctl As Control In Controls
                '    If ctl.Name = "imgExpand" Then Continue For
                '    If ctl.Name = "imgCollapse" Then Continue For
                '    ctl.Left -= _MinWidth
                'Next
                Width = _FullSize.Width
                'Me.ResumeLayout()

                Me.Parent.SuspendLayout()
                For Each ctl As Control In Me.Parent.Controls
                    'skip if is this groupbox control
                    If ctl.Name = Me.Name Then Continue For

                    'check if ctl is within groupbox limits
                    If ctl.Bottom < Me.Top Then Continue For
                    If ctl.Top > Me.Bottom Then Continue For
                    If ctl.Right < Me.Left Then Continue For

                    'ctl is in correct region to resize so move and/or resize
                    If CBool(ctl.Anchor And AnchorStyles.Left) Then
                        ctl.Left += widthChange
                        If CBool(ctl.Anchor And AnchorStyles.Right) Then ctl.Width -= widthChange
                    End If
                Next
                Me.Parent.ResumeLayout()

        End Select

        _State = States.Expanded
        RaiseEvent Collapsed(sender, New CollapsibleEventArgs(False, heightChange, widthChange))

    End Sub

    ''' <summary>
    ''' Force the group box to collapse
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Collapse()

        If imgCollapse.Visible Then imgCollapse_Click(Me, New EventArgs)

    End Sub

    ''' <summary>
    ''' Force the group box to expand
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Expand()

        If imgExpand.Visible Then imgExpand_Click(Me, New EventArgs)

    End Sub

    ''' <summary>
    ''' Will layout and distribute all controls according to given style.
    '''  Controls will be docked left, top and right for horizontal and left, top, bottom for vertical.
    ''' </summary>
    ''' <param name="Layout"></param>
    ''' <param name="Resize">set to to true to automatically resize group box after layout</param>
    ''' <remarks></remarks>
    Public Sub LayoutControls(ByVal Layout As Layouts, Optional ByVal Resize As Boolean = False)

        Dim t As Integer = Me.Padding.Top + 9
        Dim l As Integer = Me.Padding.Left
        Dim r As Integer = Me.Padding.Right
        Dim b As Integer = Me.Padding.Bottom

        Select Case Layout
            Case Layouts.Horizontal
                For Each ctl As Control In Me.Controls
                    Select Case ctl.Name
                        Case imgCollapse.Name
                        Case imgExpand.Name
                        Case Else
                            ctl.Left = l + ctl.Margin.Left
                            ctl.Width = Me.Width - l - r - ctl.Margin.Left - ctl.Margin.Right
                            ctl.Top = t + ctl.Margin.Top
                            ctl.Anchor = AnchorStyles.Left Or AnchorStyles.Top Or AnchorStyles.Right
                            t = ctl.Bottom + ctl.Margin.Bottom

                    End Select
                Next

                If Resize Then Me.Height = t + b

            Case Layouts.Vertical
                For Each ctl As Control In Me.Controls
                    Select Case ctl.Name
                        Case imgCollapse.Name
                        Case imgExpand.Name
                        Case Else
                            ctl.Top = t
                            ctl.Height = Me.Height - t - b - ctl.Margin.Top - ctl.Margin.Bottom
                            ctl.Left = l + ctl.Margin.Left
                            ctl.Anchor = AnchorStyles.Left Or AnchorStyles.Top Or AnchorStyles.Bottom
                            l = ctl.Right + ctl.Margin.Right
                    End Select
                Next
                If Resize Then Me.Width = l + r

        End Select

    End Sub

End Class

Public Class CollapsibleEventArgs
    Inherits System.EventArgs
    Public Collapsed As Boolean
    Public Width As Integer
    Public Height As Integer

    Public Sub New(ByVal NewCollapsed As Boolean, ByVal CollapseHeight As Integer, ByVal CollapseWidth As Integer)
        Collapsed = NewCollapsed
        Width = CollapseWidth
        Height = CollapseHeight
    End Sub
End Class