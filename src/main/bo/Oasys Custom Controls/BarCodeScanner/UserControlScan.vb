﻿Public Class UserControlScan

    Private WithEvents Scanner As IScannerInput
    Private Delegate Sub SetTextDelegate(ByVal text As String)

    Public Event ReceivedScannerInput(ByVal strInput As String)


    Public Function CloseScanner() As Boolean
        Return Scanner.CloseScanner()
    End Function

    Public Function InitialiseScanner() As Boolean
        Scanner = (New ScannerInputFactory).FactoryGet()
        Scanner.InitialiseScanner()

    End Function

    Public Sub OnScan(ByVal strInput As String) Handles Scanner.InputReceived
        Try
            If txtInput.InvokeRequired Then
                txtInput.Invoke(New SetTextDelegate(AddressOf Me.OnScan), strInput)
            Else
                txtInput.Text = strInput
                RaiseEvent ReceivedScannerInput(txtInput.Text)
            End If
        Catch ex As Exception
            Trace.WriteLine(ex.Message)
        End Try
    End Sub


    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        InitialiseScanner()

    End Sub
End Class
