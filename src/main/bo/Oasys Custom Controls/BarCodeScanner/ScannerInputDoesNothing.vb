﻿Public Class ScannerInputDoesNothing

    Implements IScannerInput

    Private _ScannerInput As String = String.Empty

    Public Function CloseScanner() As Boolean Implements IScannerInput.CloseScanner
        Return False
    End Function

    Public Function InitialiseScanner() As Boolean Implements IScannerInput.InitialiseScanner
        Return False
    End Function

    Public Event InputReceived(ByVal strInput As String) Implements IScannerInput.InputReceived

    Public Function IsScannerEventDriven() As Boolean Implements IScannerInput.IsScannerEventDriven
        Return False
    End Function

    Public Property ScannerInput() As String Implements IScannerInput.ScannerInput
        Get
            Return _ScannerInput
        End Get
        Set(ByVal value As String)
            _ScannerInput = value
        End Set
    End Property

End Class
