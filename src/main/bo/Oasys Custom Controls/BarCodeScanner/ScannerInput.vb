﻿Imports System
Imports CoreScanner
Imports System.Xml
Imports System.IO
Imports System.ComponentModel

Public Class ScannerInput

    Implements IScannerInput

    Private _ScannerInput As String = String.Empty
    Private _ScannerValue As String = String.Empty
    Private m_TotalScanners As Integer = 8
    Private WithEvents Scanner As New CCoreScannerClass
    Private ScannerTypes() As Short = {1, 0, 0, 0, 0, 0, 0, 0}
    Private STATUS_FALSE As Integer = 1
    Private STATUS_SUCCESS As Integer = 0
    Private _ProcessingEvent As Boolean = False
    Private Const DISABLE_SCAN As Integer = 2013
    Private Const ENABLE_SCAN As Integer = 2014

    Public Event InputReceived(ByVal strInput As String) Implements IScannerInput.InputReceived

    Private Function registerForEvents() As Integer
        Dim nEvents As Integer = 6
        Dim status As Integer = 1
        Dim strEventIds As String = "1,2,4,8,16,32"
        Dim outXml As String = String.Empty
        Dim inXml As String = "<inArgs><cmdArgs><arg-int>" + nEvents.ToString + "</arg-int><arg-int>" + strEventIds + "</arg-int></cmdArgs></inArgs>"
        Dim opCode As Integer = 1001
        Scanner.ExecCommand(opCode, inXml, outXml, status)
        Return status
    End Function

    Public Function CloseScanner() As Boolean Implements IScannerInput.CloseScanner
        Dim appHandle As Integer = 0
        Dim status As Integer = 1

        Scanner.Close(appHandle, status)
        Return status = STATUS_SUCCESS
    End Function

    Public Function InitialiseScanner() As Boolean Implements IScannerInput.InitialiseScanner
        Dim appHandle As Integer = 0
        Dim status As Integer = 1

        Scanner.Open(appHandle, ScannerTypes, 1, status)
        If status = STATUS_SUCCESS Then
            status = registerForEvents()
            If IsScannerEventDriven() = False Then
                MessageBox.Show("Barcode Scanner is not configured to work in this mode, please re-program your device.", "Barcode Scanner Configuration Issue")
            End If
        End If
        Return status = STATUS_SUCCESS

    End Function

    Private Sub SendCommand(ByVal OpCode As Integer)
        Dim inXml As String = "<inArgs><scannerID>1</scannerID></inArgs>"
        Dim outXml As String = String.Empty
        Dim status As Integer = STATUS_FALSE

        Scanner.ExecCommand(opCode, inXml, outXml, status)

    End Sub

    Private Function GetScannedBarcodeFromXML(ByVal strXML As String) As String
        Dim xmlDoc As New XmlDocument
        xmlDoc.LoadXml(strXML)
        Dim barcode As String = xmlDoc.DocumentElement.GetElementsByTagName("datalabel").Item(0).InnerText
        Dim numbers() As String = barcode.Split(" "c)
        Dim result As String = String.Empty

        For Each number As String In numbers
            If number Is Nothing Or number = String.Empty Then Exit For
            result += Chr(Convert.ToInt32(number, 16)).ToString
        Next

        Return result
    End Function

    Public Sub OnScan(ByVal eventType As Short, ByRef strInput As String) Handles Scanner.BarcodeEvent
        CloseScanner()
        Dim strBarcode As String = GetScannedBarcodeFromXML(strInput)
        Try
            RaiseEvent InputReceived(strBarcode)
            InitialiseScanner()
        Catch ex As Exception
            Trace.WriteLine(ex.Message)
        End Try
    End Sub

    Private Function GetScannerTypeFromXML(ByVal strXML As String) As String
        Dim xmlRead As XmlTextReader = New XmlTextReader(New StringReader(strXML))
        Dim ScannerType As String = String.Empty

        While (xmlRead.Read())
            Select Case xmlRead.NodeType
                Case XmlNodeType.Element
                    If xmlRead.Name = "scanner" Then
                        ScannerType = xmlRead.GetAttribute("type")
                        Exit While
                    End If
            End Select
        End While

        Return ScannerType
    End Function

    Public Function IsScannerEventDriven() As Boolean Implements IScannerInput.IsScannerEventDriven
        Dim result As Boolean = False
        Dim numOfScanners As Short = 0
        Dim nScannerCount As Integer = 0
        Dim outXML As String = String.Empty
        Dim status As Integer = STATUS_FALSE
        Dim scannerIdList(255) As Integer

        Scanner.GetScanners(numOfScanners, scannerIdList, outXML, status)
        If status = STATUS_SUCCESS Then
            Dim scannerType = GetScannerTypeFromXML(outXML)
            If scannerType = "USBIBMHID" Or scannerType = String.Empty Then
                result = True
            End If
        End If
        Return result
    End Function

    Public Property ScannerInput() As String Implements IScannerInput.ScannerInput
        Get
            Return _ScannerInput
        End Get
        Set(ByVal value As String)
            _ScannerInput = value
        End Set
    End Property
End Class

