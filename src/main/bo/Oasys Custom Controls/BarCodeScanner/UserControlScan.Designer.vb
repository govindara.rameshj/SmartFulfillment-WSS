﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserControlScan
    Inherits System.Windows.Forms.UserControl

    'UserControl1 overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally

            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UserControlScan))
        Me.Icon = New System.Windows.Forms.PictureBox
        Me.txtInput = New System.Windows.Forms.TextBox
        CType(Me.Icon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Icon
        '
        Me.Icon.Image = CType(resources.GetObject("Icon.Image"), System.Drawing.Image)
        Me.Icon.Location = New System.Drawing.Point(2, 3)
        Me.Icon.Name = "Icon"
        Me.Icon.Size = New System.Drawing.Size(35, 41)
        Me.Icon.TabIndex = 0
        Me.Icon.TabStop = False
        '
        'txtInput
        '
        Me.txtInput.Location = New System.Drawing.Point(46, 13)
        Me.txtInput.Name = "txtInput"
        Me.txtInput.Size = New System.Drawing.Size(80, 20)
        Me.txtInput.TabIndex = 1
        Me.txtInput.Visible = False
        '
        'UserControlScan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.txtInput)
        Me.Controls.Add(Me.Icon)
        Me.Name = "UserControlScan"
        Me.Size = New System.Drawing.Size(136, 44)
        CType(Me.Icon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Icon As System.Windows.Forms.PictureBox
    Friend WithEvents txtInput As System.Windows.Forms.TextBox

End Class
