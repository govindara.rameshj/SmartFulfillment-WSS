Public Class UserLogin

    Public Enum Levels
        User
        Supervisor
        Manager
    End Enum

    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB
    Private _User As BOSecurityProfile.cSystemUsers = Nothing
    Private _LogUser As BOSecurityProfile.cSystemUsers = Nothing
    Private _Level As Levels = Levels.User
    Private _AlreadyShown As Boolean = False

    Public ReadOnly Property User() As BOSecurityProfile.cSystemUsers
        Get
            Return _User
        End Get
    End Property

    Public ReadOnly Property UserID() As Integer
        Get
            Return _User.ID.Value
        End Get
    End Property

    Public Sub New(ByRef Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal Level As Levels)
        InitializeComponent()
        _Oasys3DB = Oasys3DB
        _Level = Level
        AddHandler cgbPassword.Collapsed, AddressOf cgb_Collapsed
        'AddHandler cgbSupervisor.Collapsed, AddressOf cgb_Collapsed
        _AlreadyShown = False
        Select Case Level
            Case Levels.User : Me.Text = My.Resources.UserAuthorisation
            Case Levels.Supervisor : Me.Text = My.Resources.SupervisorAuthorisation
            Case Levels.Manager : Me.Text = My.Resources.ManagerAuthorisation
        End Select

    End Sub

    Public Sub New(ByRef Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal Level As Levels, ByVal Header As String)
        InitializeComponent()
        _Oasys3DB = Oasys3DB
        _Level = Level
        AddHandler cgbPassword.Collapsed, AddressOf cgb_Collapsed
        'AddHandler cgbSupervisor.Collapsed, AddressOf cgb_Collapsed

        _AlreadyShown = False
        Select Case Level
            Case Levels.User : Me.Text = My.Resources.UserAuthorisation & " - " & Header
            Case Levels.Supervisor : Me.Text = My.Resources.SupervisorAuthorisation & " - " & Header
            Case Levels.Manager : Me.Text = My.Resources.ManagerAuthorisation & " - " & Header
        End Select

    End Sub

    Private Sub frmLogon_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cgbPassword.Collapse()
        cgbSupervisor.Collapse()
        txtUserID.Focus()
    End Sub

    Private Sub frmLogin_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        Select Case e.KeyData
            Case Keys.F5 : btnProceed.PerformClick()
            Case Keys.F10 : btnExit.PerformClick()
        End Select

    End Sub

    Private Sub UserLogin_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If _User Is Nothing Then
            DialogResult = Windows.Forms.DialogResult.Cancel
        Else
            DialogResult = Windows.Forms.DialogResult.OK
        End If

    End Sub


    Private Sub txtUserID_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUserID.KeyPress

        _User = Nothing
        btnProceed.Enabled = False

        'if enter button then go to next control
        If e.KeyChar = ChrW(Keys.Enter) Then
            txtPassword.Enabled = True
            Me.SelectNextControl(Me.ActiveControl, True, True, True, True)
        End If


        'allow tab, delete and digits
        e.Handled = True
        If e.KeyChar = ChrW(Keys.Tab) Then e.Handled = False
        If e.KeyChar = ChrW(Keys.Delete) Then e.Handled = False
        If e.KeyChar = ChrW(Keys.Back) Then e.Handled = False
        If Char.IsDigit(e.KeyChar) Then e.Handled = False

    End Sub

    Private Sub txtUserID_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUserID.Leave
        Dim Msg As New frmNewPassword

        Try
            'check that cancel button has not been clicked
            If Me.ActiveControl Is btnExit Then Exit Sub

            'if txtvalue empty then set to zero
            If txtUserID.Text = String.Empty Then Exit Sub

            'check that ID is an integer
            Dim value As Integer = 0
            If Integer.TryParse(txtUserID.Text, value) Then
                txtUserID.Text = FormatNumber(value, 0)
            Else
                lblInitials.Text = String.Empty
                lblFullName.Text = My.Resources.ErrUserIdNotNumber
                txtPassword.Enabled = False
                txtUserID.SelectAll()
                txtUserID.Focus()
                Exit Sub
            End If

            'check user exists
            _User = Nothing
            _LogUser = New BOSecurityProfile.cSystemUsers(_Oasys3DB)

            If _LogUser.LoadUser(value) Then
                'check against level
                Dim userOk As Boolean = False
                Select Case _Level
                    Case Levels.Manager : If _LogUser.IsManager.Value Then userOk = True
                    Case Levels.Supervisor : If _LogUser.IsSupervisor.Value Then userOk = True
                    Case Else : userOk = True
                End Select

                If userOk Then
                    If Now >= _LogUser.PasswordExpires.Value Then
                        lblInitials.Text = _LogUser.Initials.Value.ToString
                        lblFullName.Text = _LogUser.Name.Value.ToString
                        If _AlreadyShown = False Then
                            Msg.ShowDialog()
                            _AlreadyShown = True
                            cgbPassword.Expand()
                            If _LogUser.IsSupervisor.Value Then
                                cgbSupervisor.Expand()
                            Else
                                cgbSupervisor.Visible = False
                            End If
                        End If
                        txtNewPass.Focus()
                    Else
                        lblInitials.Text = _LogUser.Initials.Value.ToString
                        lblFullName.Text = _LogUser.Name.Value.ToString
                        txtPassword.Enabled = True
                        txtPassword.Focus()
                    End If

                Else
                    lblInitials.Text = String.Empty
                    lblFullName.Text = My.Resources.ErrUserNotLevel
                    txtPassword.Enabled = False
                    txtUserID.SelectAll()
                    txtUserID.Focus()
                End If

            Else
                lblInitials.Text = String.Empty
                lblFullName.Text = My.Resources.ErrUserNotRecognised
                txtPassword.Enabled = False
                txtUserID.SelectAll()
                txtUserID.Focus()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub txtPassword_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPassword.KeyPress

        Try
            _User = Nothing
            btnProceed.Enabled = False
            txtPassword.BackColor = Color.White

            If e.KeyChar = ChrW(Keys.Enter) Then
                If _LogUser.CheckPassword(txtPassword.Text.ToString) Then
                    _User = _LogUser
                    txtPassword.BackColor = Color.Green

                    btnProceed.Enabled = True
                    DialogResult = Windows.Forms.DialogResult.OK
                    Me.SelectNextControl(Me.ActiveControl, True, True, True, True)
                Else
                    txtPassword.BackColor = Color.Red
                    txtPassword.Clear()
                    txtPassword.Focus()
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub cgb_Collapsed(ByVal sender As Object, ByVal e As CtsControls.CollapsibleEventArgs)

        If e.Collapsed Then
            Height -= e.Height
            cgbSupervisor.Collapse()
        Else
            Height += e.Height
            cgbSupervisor.Expand()
        End If

    End Sub

    Private Sub btnProceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        _LogUser.Password.Value = txtNewPass.Text
        _LogUser.PasswordExpires.Value = DateAdd(DateInterval.Day, 30, Now)
        If _LogUser.IsSupervisor.Value Then
            _LogUser.SupervisorPassword.Value = txtSupConfirm.Text
            _LogUser.SupervisorPwdExpires.Value = DateAdd(DateInterval.Day, 30, Now)
        End If
        _LogUser.ChangePassword(_LogUser.ID.Value)

        _User = _LogUser
        txtPassword.BackColor = Color.Green
        btnProceed.Enabled = True
        DialogResult = Windows.Forms.DialogResult.OK
        Me.SelectNextControl(Me.ActiveControl, True, True, True, True)

        'Close()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub txtNewPass_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNewPass.KeyDown
        If e.KeyCode = Keys.Return Then
            If ValidatePassword(txtNewPass.Text) = True Then
                txtConfirmPass.Focus()
            Else
                txtNewPass.Text = ""
                txtNewPass.Focus()
            End If
        End If
    End Sub

    Private Function ValidatePassword(ByVal strPassword As String) As Boolean

        If strPassword.Length <> 5 Then
            MessageBox.Show("Password must be five digits long.", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
        End If

        For intIndex As Integer = 0 To 4
            For intPos As Integer = 0 To 4
                If intPos = intIndex Then
                Else
                    If strPassword.Substring(intIndex, 1) = strPassword.Substring(intPos, 1) Then
                        MessageBox.Show("Password must not repeat a digit.", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Return False
                    End If
                    If strPassword = _LogUser.Password.Value Then
                        MessageBox.Show("Password must be different from your current one.", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Return False
                    End If
                End If
            Next intPos
        Next intIndex

        Return True

    End Function

    Private Sub txtConfirmPass_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtConfirmPass.KeyDown
        If e.KeyCode = Keys.Return Then
            If txtConfirmPass.Text = txtNewPass.Text Then
                If cgbSupervisor.Visible Then
                    btnProceed.Enabled = True
                    txtSupPassword.Focus()
                Else
                    btnProceed.Enabled = True
                End If
            Else
                btnProceed.Enabled = False
                MessageBox.Show("Password did not match the confirm password.", "Password Not Confirmed", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                txtConfirmPass.Focus()
            End If
        End If

    End Sub

    Private Sub txtSupPassword_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSupPassword.KeyDown
        If e.KeyCode = Keys.Return Then
            If ValidatePassword(txtSupPassword.Text) = True Then
                txtSupConfirm.Focus()
            Else
                txtSupPassword.Text = ""
                txtSupPassword.Focus()
            End If
        End If

    End Sub

    Private Sub txtSupConfirm_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSupConfirm.KeyDown
        If e.KeyCode = Keys.Return Then
            If txtSupConfirm.Text = txtSupPassword.Text Then
                btnProceed.Enabled = True
            Else
                btnProceed.Enabled = False
                MessageBox.Show("Password did not match the confirm password.", "Password Not Confirmed", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                txtSupConfirm.Focus()
            End If
        End If
    End Sub

    Private Sub txtSupConfirm_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSupConfirm.KeyPress
        Select Case e.KeyChar
            Case ChrW(8)
                e.Handled = False
            Case ChrW(48) To ChrW(57)
                e.Handled = False
            Case Else
                e.KeyChar = ChrW(0)
                e.Handled = True
        End Select
    End Sub

    Private Sub txtNewPass_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNewPass.KeyPress
        Select Case e.KeyChar
            Case ChrW(8)
                e.Handled = False
            Case ChrW(48) To ChrW(57)
                e.Handled = False
            Case Else
                e.KeyChar = ChrW(0)
                e.Handled = True
        End Select
    End Sub

    Private Sub txtConfirmPass_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtConfirmPass.KeyPress
        Select Case e.KeyChar
            Case ChrW(8)
                e.Handled = False
            Case ChrW(48) To ChrW(57)
                e.Handled = False
            Case Else
                e.KeyChar = ChrW(0)
                e.Handled = True
        End Select
    End Sub

    Private Sub txtSupPassword_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSupPassword.KeyPress
        Select Case e.KeyChar
            Case ChrW(8)
                e.Handled = False
            Case ChrW(48) To ChrW(57)
                e.Handled = False
            Case Else
                e.KeyChar = ChrW(0)
                e.Handled = True
        End Select
    End Sub

    Private Sub txtSupPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSupPassword.TextChanged

    End Sub
End Class