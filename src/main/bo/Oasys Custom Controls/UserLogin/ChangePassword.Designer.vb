﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ChangePassword
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtSupConfirm = New System.Windows.Forms.TextBox
        Me.txtSupPassword = New System.Windows.Forms.TextBox
        Me.label6 = New System.Windows.Forms.Label
        Me.txtConfirmPass = New System.Windows.Forms.TextBox
        Me.txtNewPass = New System.Windows.Forms.TextBox
        Me.label7 = New System.Windows.Forms.Label
        Me.gbxPassword = New System.Windows.Forms.GroupBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.gbxSupervisor = New System.Windows.Forms.GroupBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtSupOld = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.btnExit = New System.Windows.Forms.Button
        Me.txtOldPass = New DevExpress.XtraEditors.TextEdit
        Me.gbxPassword.SuspendLayout()
        Me.gbxSupervisor.SuspendLayout()
        CType(Me.txtOldPass.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtSupConfirm
        '
        Me.txtSupConfirm.AcceptsReturn = True
        Me.txtSupConfirm.BackColor = System.Drawing.SystemColors.Window
        Me.txtSupConfirm.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSupConfirm.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSupConfirm.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.txtSupConfirm.Location = New System.Drawing.Point(116, 67)
        Me.txtSupConfirm.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.txtSupConfirm.MaxLength = 5
        Me.txtSupConfirm.Name = "txtSupConfirm"
        Me.txtSupConfirm.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtSupConfirm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSupConfirm.Size = New System.Drawing.Size(74, 20)
        Me.txtSupConfirm.TabIndex = 3
        '
        'txtSupPassword
        '
        Me.txtSupPassword.AcceptsReturn = True
        Me.txtSupPassword.BackColor = System.Drawing.SystemColors.Window
        Me.txtSupPassword.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSupPassword.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSupPassword.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.txtSupPassword.Location = New System.Drawing.Point(116, 44)
        Me.txtSupPassword.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.txtSupPassword.MaxLength = 5
        Me.txtSupPassword.Name = "txtSupPassword"
        Me.txtSupPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtSupPassword.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSupPassword.Size = New System.Drawing.Size(74, 20)
        Me.txtSupPassword.TabIndex = 2
        '
        'label6
        '
        Me.label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label6.BackColor = System.Drawing.Color.Transparent
        Me.label6.Cursor = System.Windows.Forms.Cursors.Default
        Me.label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.label6.Location = New System.Drawing.Point(6, 44)
        Me.label6.Name = "label6"
        Me.label6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.label6.Size = New System.Drawing.Size(104, 20)
        Me.label6.TabIndex = 18
        Me.label6.Text = "New Password"
        Me.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtConfirmPass
        '
        Me.txtConfirmPass.AcceptsReturn = True
        Me.txtConfirmPass.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtConfirmPass.BackColor = System.Drawing.SystemColors.Window
        Me.txtConfirmPass.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtConfirmPass.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtConfirmPass.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.txtConfirmPass.Location = New System.Drawing.Point(116, 67)
        Me.txtConfirmPass.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.txtConfirmPass.MaxLength = 5
        Me.txtConfirmPass.Name = "txtConfirmPass"
        Me.txtConfirmPass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtConfirmPass.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtConfirmPass.Size = New System.Drawing.Size(74, 20)
        Me.txtConfirmPass.TabIndex = 3
        '
        'txtNewPass
        '
        Me.txtNewPass.AcceptsReturn = True
        Me.txtNewPass.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNewPass.BackColor = System.Drawing.SystemColors.Window
        Me.txtNewPass.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNewPass.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNewPass.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.txtNewPass.Location = New System.Drawing.Point(116, 44)
        Me.txtNewPass.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.txtNewPass.MaxLength = 5
        Me.txtNewPass.Name = "txtNewPass"
        Me.txtNewPass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtNewPass.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtNewPass.Size = New System.Drawing.Size(74, 20)
        Me.txtNewPass.TabIndex = 2
        '
        'label7
        '
        Me.label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label7.BackColor = System.Drawing.Color.Transparent
        Me.label7.Cursor = System.Windows.Forms.Cursors.Default
        Me.label7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.label7.Location = New System.Drawing.Point(6, 67)
        Me.label7.Name = "label7"
        Me.label7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.label7.Size = New System.Drawing.Size(104, 20)
        Me.label7.TabIndex = 20
        Me.label7.Text = "Confirm Password"
        Me.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbxPassword
        '
        Me.gbxPassword.Controls.Add(Me.txtOldPass)
        Me.gbxPassword.Controls.Add(Me.Label1)
        Me.gbxPassword.Controls.Add(Me.label6)
        Me.gbxPassword.Controls.Add(Me.label7)
        Me.gbxPassword.Controls.Add(Me.txtNewPass)
        Me.gbxPassword.Controls.Add(Me.txtConfirmPass)
        Me.gbxPassword.Location = New System.Drawing.Point(12, 12)
        Me.gbxPassword.Name = "gbxPassword"
        Me.gbxPassword.Size = New System.Drawing.Size(196, 93)
        Me.gbxPassword.TabIndex = 7
        Me.gbxPassword.TabStop = False
        Me.gbxPassword.Text = "Change Password"
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(6, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(104, 20)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Old Password"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbxSupervisor
        '
        Me.gbxSupervisor.Controls.Add(Me.Label2)
        Me.gbxSupervisor.Controls.Add(Me.txtSupOld)
        Me.gbxSupervisor.Controls.Add(Me.Label3)
        Me.gbxSupervisor.Controls.Add(Me.txtSupConfirm)
        Me.gbxSupervisor.Controls.Add(Me.Label8)
        Me.gbxSupervisor.Controls.Add(Me.txtSupPassword)
        Me.gbxSupervisor.Location = New System.Drawing.Point(214, 12)
        Me.gbxSupervisor.Name = "gbxSupervisor"
        Me.gbxSupervisor.Size = New System.Drawing.Size(196, 93)
        Me.gbxSupervisor.TabIndex = 8
        Me.gbxSupervisor.TabStop = False
        Me.gbxSupervisor.Text = "Change Supervisor Password"
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(6, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(104, 20)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "Old Password"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSupOld
        '
        Me.txtSupOld.AcceptsReturn = True
        Me.txtSupOld.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSupOld.BackColor = System.Drawing.SystemColors.Window
        Me.txtSupOld.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSupOld.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSupOld.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.txtSupOld.Location = New System.Drawing.Point(116, 21)
        Me.txtSupOld.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.txtSupOld.MaxLength = 5
        Me.txtSupOld.Name = "txtSupOld"
        Me.txtSupOld.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtSupOld.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSupOld.Size = New System.Drawing.Size(74, 20)
        Me.txtSupOld.TabIndex = 21
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(6, 44)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(104, 20)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "New Password"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label8.Location = New System.Drawing.Point(6, 67)
        Me.Label8.Name = "Label8"
        Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label8.Size = New System.Drawing.Size(104, 20)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Confirm Password"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(334, 114)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 9
        Me.btnExit.TabStop = False
        Me.btnExit.Text = "F10 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'txtOldPass
        '
        Me.txtOldPass.Location = New System.Drawing.Point(116, 22)
        Me.txtOldPass.Name = "txtOldPass"
        Me.txtOldPass.Properties.Mask.EditMask = "\d{0,5}"
        Me.txtOldPass.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtOldPass.Size = New System.Drawing.Size(74, 20)
        Me.txtOldPass.TabIndex = 10
        '
        'ChangePassword
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(422, 165)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.gbxSupervisor)
        Me.Controls.Add(Me.gbxPassword)
        Me.Name = "ChangePassword"
        Me.ShowIcon = False
        Me.Text = "Change Password"
        Me.gbxPassword.ResumeLayout(False)
        Me.gbxPassword.PerformLayout()
        Me.gbxSupervisor.ResumeLayout(False)
        Me.gbxSupervisor.PerformLayout()
        CType(Me.txtOldPass.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Public WithEvents txtSupConfirm As System.Windows.Forms.TextBox
    Public WithEvents txtSupPassword As System.Windows.Forms.TextBox
    Public WithEvents label6 As System.Windows.Forms.Label
    Public WithEvents txtConfirmPass As System.Windows.Forms.TextBox
    Public WithEvents txtNewPass As System.Windows.Forms.TextBox
    Public WithEvents label7 As System.Windows.Forms.Label
    Friend WithEvents gbxPassword As System.Windows.Forms.GroupBox
    Public WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents gbxSupervisor As System.Windows.Forms.GroupBox
    Public WithEvents Label2 As System.Windows.Forms.Label
    Public WithEvents txtSupOld As System.Windows.Forms.TextBox
    Public WithEvents Label3 As System.Windows.Forms.Label
    Public WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents txtOldPass As DevExpress.XtraEditors.TextEdit
End Class
