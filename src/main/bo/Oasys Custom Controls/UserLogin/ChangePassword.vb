﻿Public Class ChangePassword
    Private _User As BOSecurityProfile.cSystemUsers

    Sub New(ByVal user As BOSecurityProfile.cSystemUsers)
        InitializeComponent()
        _User = user
    End Sub



    Private Sub txtNewPass_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNewPass.KeyDown
        If e.KeyCode = Keys.Return Then
            If ValidatePassword(txtNewPass.Text) = True Then
                txtConfirmPass.Focus()
            Else
                txtNewPass.Text = ""
                txtNewPass.Focus()
            End If
        End If
    End Sub

    Private Function ValidatePassword(ByVal strPassword As String) As Boolean

        If strPassword.Length <> 5 Then
            MessageBox.Show("Password must be five digits long.", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
        End If

        For intIndex As Integer = 0 To 4
            For intPos As Integer = 0 To 4
                If intPos = intIndex Then
                Else
                    If strPassword.Substring(intIndex, 1) = strPassword.Substring(intPos, 1) Then
                        MessageBox.Show("Password must not repeat a digit.", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Return False
                    End If
                    If strPassword = _User.Password.Value Then
                        MessageBox.Show("Password must be different from your current one.", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Return False
                    End If
                End If
            Next intPos
        Next intIndex

        Return True

    End Function


    Private Sub txtConfirmPass_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtConfirmPass.KeyDown

        If e.KeyCode = Keys.Return Then
            If txtConfirmPass.Text = txtNewPass.Text Then
                MessageBox.Show("Password changed")
                If gbxSupervisor.Visible Then txtSupPassword.Focus()
            Else
                MessageBox.Show("Password did not match the confirm password.", "Password Not Confirmed", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                txtConfirmPass.Focus()
            End If
        End If

    End Sub

    Private Sub txtSupPassword_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSupPassword.KeyDown
        If e.KeyCode = Keys.Return Then
            If ValidatePassword(txtSupPassword.Text) = True Then
                txtSupConfirm.Focus()
            Else
                txtSupPassword.Text = ""
                txtSupPassword.Focus()
            End If
        End If

    End Sub

    Private Sub txtSupConfirm_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSupConfirm.KeyDown
        If e.KeyCode = Keys.Return Then
            If txtSupConfirm.Text = txtSupPassword.Text Then
                MessageBox.Show("Password changed")
            Else
                MessageBox.Show("Password did not match the confirm password.", "Password Not Confirmed", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                txtSupConfirm.Focus()
            End If
        End If
    End Sub

    Private Sub txtSupConfirm_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSupConfirm.KeyPress
        Select Case e.KeyChar
            Case ChrW(8)
                e.Handled = False
            Case ChrW(48) To ChrW(57)
                e.Handled = False
            Case Else
                e.KeyChar = ChrW(0)
                e.Handled = True
        End Select
    End Sub

    Private Sub txtNewPass_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNewPass.KeyPress
        Select Case e.KeyChar
            Case ChrW(8)
                e.Handled = False
            Case ChrW(48) To ChrW(57)
                e.Handled = False
            Case Else
                e.KeyChar = ChrW(0)
                e.Handled = True
        End Select
    End Sub

    Private Sub txtConfirmPass_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtConfirmPass.KeyPress
        Select Case e.KeyChar
            Case ChrW(8)
                e.Handled = False
            Case ChrW(48) To ChrW(57)
                e.Handled = False
            Case Else
                e.KeyChar = ChrW(0)
                e.Handled = True
        End Select
    End Sub

    Private Sub txtSupPassword_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSupPassword.KeyPress
        Select Case e.KeyChar
            Case ChrW(8)
                e.Handled = False
            Case ChrW(48) To ChrW(57)
                e.Handled = False
            Case Else
                e.KeyChar = ChrW(0)
                e.Handled = True
        End Select
    End Sub

    Private Sub txtSupPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSupPassword.TextChanged

    End Sub


    Private Sub ConfirmPassword()

        'check old password first


    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub


End Class