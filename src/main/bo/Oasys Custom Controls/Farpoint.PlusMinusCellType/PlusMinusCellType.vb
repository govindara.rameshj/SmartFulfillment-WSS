﻿Public Class PlusMinusCellType
    Inherits Farpoint.Win.Spread.CellType.GeneralCellType
    Public Enum States
        Plus
        Minus
    End Enum
    Delegate Sub PopUpDelegate(ByVal sender As Object, ByVal state As States)
    Private _popUpDelegate As PopUpDelegate
    Private _buttonPressed As Boolean
    Private _tag As Object
    Private _state As States
    Private _imagePlus As Image = My.Resources.buttonPlus
    Private _imageMinus As Image = My.Resources.buttonMinus

    Public ReadOnly Property State() As States
        Get
            Return _state
        End Get
    End Property

    Public Property Tag() As Object
        Get
            Return _tag
        End Get
        Set(ByVal value As Object)
            _tag = value
        End Set
    End Property



    Public Sub New(ByVal callback As PopUpDelegate, ByVal state As States)
        _popUpDelegate = CType([Delegate].Combine(_popUpDelegate, callback), PopUpDelegate)
        _state = state
        Start()
    End Sub

    Public Sub New(ByVal callback As PopUpDelegate, ByVal state As States, ByVal tag As Object)
        _popUpDelegate = CType([Delegate].Combine(_popUpDelegate, callback), PopUpDelegate)
        _state = state
        _tag = tag
        Start()
    End Sub

    Public Sub New(ByVal callback As PopUpDelegate, ByVal imagePlus As Image, ByVal imageMinus As Image, ByVal state As States)
        _popUpDelegate = CType([Delegate].Combine(_popUpDelegate, callback), PopUpDelegate)
        _imagePlus = imagePlus
        _imageMinus = imageMinus
        _state = state
        Start()
    End Sub

    Private Sub Start()

        Select Case _state
            Case States.Plus : Me.BackgroundImage = New Farpoint.Win.Picture(_imagePlus, Farpoint.Win.RenderStyle.Normal, Color.Transparent, Farpoint.Win.HorizontalAlignment.Center, Farpoint.Win.VerticalAlignment.Center)
            Case States.Minus : Me.BackgroundImage = New Farpoint.Win.Picture(_imageMinus, Farpoint.Win.RenderStyle.Normal, Color.Transparent, Farpoint.Win.HorizontalAlignment.Center, Farpoint.Win.VerticalAlignment.Center)
        End Select

    End Sub



    Public Overrides Function IsReservedLocation(ByVal g As Graphics, ByVal x As Integer, ByVal y As Integer, ByVal rc As Rectangle, ByVal appearance As FarPoint.Win.Spread.Appearance, ByVal value As Object, ByVal zoomFactor As Single) As Object
        Dim w As Integer = Me.BackgroundImage.Image.Width
        Dim h As Integer = Me.BackgroundImage.Image.Height

        Dim rectButton As Rectangle = New Rectangle(CInt(rc.X + (rc.Width - w) / 2), CInt(rc.Y + (rc.Height - h) / 2), w, h)
        If (rectButton.Contains(New Point(x, y))) Then
            _buttonPressed = True
            Return Me
        Else
            _buttonPressed = False
            Return Nothing
        End If

    End Function

    Public Overrides Sub StartEditing(ByVal e As System.EventArgs, ByVal selectAll As Boolean, ByVal autoClipboard As Boolean)

        If _buttonPressed Then
            _buttonPressed = False

            Dim ePop As New Farpoint.Win.PopUpEventArgs
            Dim app As New Farpoint.Win.Spread.Appearance
            _popUpDelegate(Me, _state)

            Select Case _state
                Case States.Minus
                    _state = States.Plus
                    Me.BackgroundImage = New Farpoint.Win.Picture(_imagePlus, Farpoint.Win.RenderStyle.Normal, Color.Transparent, Farpoint.Win.HorizontalAlignment.Center, Farpoint.Win.VerticalAlignment.Center)
                Case States.Plus
                    _state = States.Minus
                    Me.BackgroundImage = New Farpoint.Win.Picture(_imageMinus, Farpoint.Win.RenderStyle.Normal, Color.Transparent, Farpoint.Win.HorizontalAlignment.Center, Farpoint.Win.VerticalAlignment.Center)
            End Select
        End If

        Me.StopEditing()

    End Sub

    Public Sub PerformClick()

        _buttonPressed = True
        StartEditing(New EventArgs, False, False)

    End Sub

End Class
