﻿Public Class Panel
    Private _DrawTop As Boolean = True
    Private _DrawLeft As Boolean = True
    Private _DrawRight As Boolean = True
    Private _DrawBottom As Boolean = True
    Private _LineColour As Color = Color.Black
    Private _LineWidth As Integer = 1

    Private _DrawGrid As Boolean = False
    Private _GridWidth As Integer = 10
    Private _GridHeight As Integer = 10
    Private _GridColour As Color = Color.Blue

    Public Property DrawTop() As Boolean
        Get
            Return _DrawTop
        End Get
        Set(ByVal value As Boolean)
            _DrawTop = value
        End Set
    End Property
    Public Property DrawLeft() As Boolean
        Get
            Return _DrawLeft
        End Get
        Set(ByVal value As Boolean)
            _DrawLeft = value
        End Set
    End Property
    Public Property DrawRight() As Boolean
        Get
            Return _DrawRight
        End Get
        Set(ByVal value As Boolean)
            _DrawRight = value
        End Set
    End Property
    Public Property DrawBottom() As Boolean
        Get
            Return _DrawBottom
        End Get
        Set(ByVal value As Boolean)
            _DrawBottom = value
        End Set
    End Property
    Public Property LineColour() As Color
        Get
            Return _LineColour
        End Get
        Set(ByVal value As Color)
            _LineColour = value
        End Set
    End Property
    Public Property LineWidth() As Integer
        Get
            Return _LineWidth
        End Get
        Set(ByVal value As Integer)
            _LineWidth = value
        End Set
    End Property

    Public Property DrawGrid() As Boolean
        Get
            Return _DrawGrid
        End Get
        Set(ByVal value As Boolean)
            _DrawGrid = value
        End Set
    End Property
    Public Property GridWidth() As Integer
        Get
            Return _GridWidth
        End Get
        Set(ByVal value As Integer)
            _GridWidth = value
        End Set
    End Property
    Public Property GridHeight() As Integer
        Get
            Return _GridHeight
        End Get
        Set(ByVal value As Integer)
            _GridHeight = value
        End Set
    End Property
    Public Property GridColour() As Color
        Get
            Return _GridColour
        End Get
        Set(ByVal value As Color)
            _GridColour = value
        End Set
    End Property

    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)
        Dim p As New Pen(_LineColour, _LineWidth)

        If _DrawTop Then e.Graphics.DrawLine(p, New Point(0, 0), New Point(Width, 0))
        If _DrawLeft Then e.Graphics.DrawLine(p, New Point(0, 0), New Point(0, Height))
        If _DrawRight Then e.Graphics.DrawLine(p, New Point(Width - 1, 0), New Point(Width - 1, Height))
        If _DrawBottom Then e.Graphics.DrawLine(p, New Point(0, Height - 1), New Point(Width, Height - 1))

        If _DrawGrid Then
            System.Windows.Forms.ControlPaint.DrawGrid(e.Graphics, e.ClipRectangle, New Size(_GridWidth, _GridHeight), _GridColour)
        End If

        MyBase.OnPaint(e)
    End Sub

    Public Sub SnapToGrid(ByVal ctl As Control)

        Dim x As Integer = Math.Max(0, ctl.Location.X)
        Dim y As Integer = Math.Max(0, ctl.Location.Y)

        'calculate nearest gridlines to ctl location
        Dim xRemainder As Integer = x Mod _GridWidth
        Dim yRemainder As Integer = y Mod _GridHeight

        x -= xRemainder
        If xRemainder > _GridWidth / 2 Then x += _GridWidth

        y -= yRemainder
        If yRemainder > _GridHeight / 2 Then y += _GridHeight

        ctl.Location = New Point(x, y)

    End Sub

End Class
