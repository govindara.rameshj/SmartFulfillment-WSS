﻿Public Class PrintEnquiry
    Inherits System.Drawing.Printing.PrintDocument
    Private _StoreIdName As String = String.Empty
    Private _Spd() As FpSpread
    Private _PrintSpreads As New List(Of spdEnquiry)
    Private _CurrentPage As Integer = 0
    Private _TotalPages As Integer = 1
    Private _PrintDateTime As Date = Nothing
    Private _PrintRectangle As Rectangle = Nothing

    Public Declare Function GetDeviceCaps Lib "gdi32.dll" (ByVal hdc As IntPtr, ByVal nIndex As Int32) As Int32
    Private Const PHYSICALOFFSETX As Int32 = 112
    Private Const PHYSICALOFFSETY As Int32 = 113
    Private Const HORZRES As Int32 = 8
    Private Const VERTRES As Int32 = 10
    Private Const HORZSIZE As Int32 = 4
    Private Const VERTSIZE As Int32 = 6

    Public Sub New(ByVal spd() As FpSpread, ByVal StoreIdName As String)

        _StoreIdName = StoreIdName
        _Spd = spd

    End Sub

    Protected Overrides Sub OnBeginPrint(ByVal e As System.Drawing.Printing.PrintEventArgs)
        MyBase.OnBeginPrint(e)

        _CurrentPage = 0
        _TotalPages = 1
        _PrintDateTime = Now

    End Sub

    Protected Overrides Sub OnPrintPage(ByVal e As System.Drawing.Printing.PrintPageEventArgs)
        MyBase.OnPrintPage(e)

        Try
            'set up rectangles for printing
            If _PrintRectangle = Nothing Then
                _PrintRectangle = GetHardMargins(e.Graphics)
            End If

            'draw header, get footer top and main print rectangle 
            Dim botHeader As Integer = DrawHeader(e.Graphics)
            Dim topFooter As Integer = DrawFooter(e.Graphics, False)
            Dim rMain As New Rectangle(_PrintRectangle.X, botHeader, _PrintRectangle.Width, topFooter - botHeader)

            Dim x As Integer = rMain.X
            Dim y As Integer = rMain.Y

            If _CurrentPage = 0 Then
                'set up print spread items
                For Each s As FpSpread In _Spd
                    _PrintSpreads.Add(New spdEnquiry(s, rMain.Width))
                Next

                For Each s As spdEnquiry In _PrintSpreads
                    If s.Height + y > rMain.Height Then
                        'start new page
                        _TotalPages += 1
                        x = rMain.X
                        y = rMain.Y
                    Else
                        y += s.Height
                    End If
                Next
            End If

            'print spreads for this page
            x = rMain.X
            y = rMain.Y
            Dim pageFull As Boolean = False
            For Each s As spdEnquiry In _PrintSpreads.Where(Function(p As spdEnquiry) p.Printed = False)

                'check if spread to print will fall of bottom print area
                If s.Height + y > _PrintRectangle.Height Then
                    If x = rMain.X Then
                        'wrap around to form two columns and print
                        x += s.Width
                        y = rMain.Y
                    Else
                        pageFull = True
                    End If
                End If

                'check whether page is full else print spread
                If pageFull Then Exit For
                s.Draw(e.Graphics, x, y)
                y += s.Height
            Next

            'check whether need to print another page
            _CurrentPage += 1
            If _CurrentPage = _TotalPages Then
                e.HasMorePages = False
            Else
                e.HasMorePages = True
            End If

            'draw footer
            DrawFooter(e.Graphics, True)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Protected Overrides Sub OnEndPrint(ByVal e As System.Drawing.Printing.PrintEventArgs)
        _CurrentPage = 0
        _TotalPages = 1
        _PrintDateTime = Nothing
    End Sub

    Private Function GetHardMargins(ByVal g As Graphics) As Rectangle

        Dim hdc As IntPtr = g.GetHdc
        Dim offx As Integer = GetDeviceCaps(hdc, PHYSICALOFFSETX)
        Dim offy As Integer = GetDeviceCaps(hdc, PHYSICALOFFSETY)
        Dim resx As Single = Convert.ToSingle(GetDeviceCaps(hdc, HORZRES))
        Dim resy As Single = Convert.ToSingle(GetDeviceCaps(hdc, VERTRES))
        Dim hsz As Single = Convert.ToSingle(GetDeviceCaps(hdc, HORZSIZE)) / 25.4F ' Screen width in inches.
        Dim vsz As Single = Convert.ToSingle(GetDeviceCaps(hdc, VERTSIZE)) / 25.4F ' Screen height in inches.
        Dim ppix As Single = resx / hsz
        Dim ppiy As Single = resy / vsz

        g.ReleaseHdc()

        Dim Left As Integer = CInt(offx / ppix * 100.0F)
        Dim Top As Integer = CInt(offy / ppiy * 100.0F)
        Dim width As Integer = CInt(hsz * 100.0F - 30.0F)
        Dim height As Integer = CInt(vsz * 100.0F - 10.0F)

        Return New Rectangle(Left, Top, width, height)

    End Function

    Private Function DrawHeader(ByVal g As Graphics) As Integer

        Try
            Dim fmt As New StringFormat
            fmt.FormatFlags = StringFormatFlags.NoClip
            fmt.Alignment = StringAlignment.Near
            fmt.LineAlignment = StringAlignment.Near

            'draw store id and name
            Dim f As New Font(FontFamily.GenericSansSerif, 7)
            Dim r As Rectangle = New Rectangle(_PrintRectangle.X, _PrintRectangle.Y, _PrintRectangle.Width, f.Height)
            g.DrawString("Store: " & _StoreIdName, f, Brushes.Black, r, fmt)

            Return r.Bottom

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Function DrawFooter(ByVal g As Graphics, ByVal drawText As Boolean) As Integer

        Try
            Dim fmt As New StringFormat
            Dim f As New Font(FontFamily.GenericSansSerif, 7)
            Dim r As Rectangle = New Rectangle(_PrintRectangle.X, _PrintRectangle.Height - f.Height, _PrintRectangle.Width, f.Height)

            If drawText Then
                fmt.Alignment = StringAlignment.Near
                fmt.LineAlignment = StringAlignment.Near
                g.DrawString("Printed: " & _PrintDateTime, f, Brushes.Black, r, fmt)

                fmt.Alignment = StringAlignment.Center
                fmt.LineAlignment = StringAlignment.Center
                g.DrawString("Page " & _CurrentPage & " of " & _TotalPages, f, Brushes.Black, r, fmt)

                fmt.Alignment = StringAlignment.Far
                fmt.LineAlignment = StringAlignment.Far
                g.DrawString("Version " & Application.ProductVersion, f, Brushes.Black, r, fmt)
            End If

            Return r.Top

        Catch ex As Exception
            Throw ex
        End Try

    End Function


End Class

Friend Class spdEnquiry
    Private _Spd As FpSpread
    Private _Height As Integer = 0
    Private _Width As Integer = 0
    Private _X As Integer = 0
    Private _Y As Integer = 0
    Private _TitleFont As Font
    Private _TitleFormat As New StringFormat
    Private _Printed As Boolean = False

    Public Property Height() As Integer
        Get
            Return _Height
        End Get
        Set(ByVal value As Integer)
            _Height = value
        End Set
    End Property
    Public Property Width() As Integer
        Get
            Return _Width
        End Get
        Set(ByVal value As Integer)
            _Width = value
        End Set
    End Property
    Public Property X() As Integer
        Get
            Return _X
        End Get
        Set(ByVal value As Integer)
            _X = value
        End Set
    End Property
    Public Property Y() As Integer
        Get
            Return _Y
        End Get
        Set(ByVal value As Integer)
            _Y = value
        End Set
    End Property
    Public Property Printed() As Boolean
        Get
            Return _Printed
        End Get
        Set(ByVal value As Boolean)
            _Printed = value
        End Set
    End Property


    Public Sub New(ByVal spd As FpSpread, ByVal width As Integer)

        Try
            _Spd = spd
            _TitleFormat.FormatFlags = StringFormatFlags.NoClip
            _TitleFormat.Alignment = StringAlignment.Center
            _TitleFormat.LineAlignment = StringAlignment.Center
            _TitleFont = New Font(FontFamily.GenericSansSerif, 14, FontStyle.Bold)
            _Width = width

            For Each sheet As SheetView In spd.Sheets
                _Height += _TitleFont.Height
                _Height += CInt(sheet.ColumnHeader.Rows(0).Height)
                _Height += CInt(sheet.RowCount * sheet.Rows.Default.Height)
                _Height += 10 ' for good measure
            Next

        Catch ex As Exception
            Trace.WriteLine(ex.Message, spd.Sheets(0).SheetName)
            Throw ex
        End Try

    End Sub

    Public Sub Draw(ByVal g As Graphics, ByVal x As Integer, ByVal y As Integer)

        Try
            Dim r As Rectangle

            'draw sheet name
            For index As Integer = 0 To _Spd.Sheets.Count - 1
                r = New Rectangle(x, y, _Width, _TitleFont.Height)
                g.DrawString(_Spd.Sheets(index).SheetName, _TitleFont, Brushes.Black, r, _TitleFormat)

                'update y coord
                y += _TitleFont.Height

                'get height of sheet to print
                Dim height As Integer = 0
                height += CInt(_Spd.Sheets(index).ColumnHeader.Rows(0).Height)
                height += CInt(_Spd.Sheets(index).RowCount * _Spd.Sheets(index).Rows.Default.Height)
                height += 10 ' for good measure

                'draw spread
                r = New Rectangle(x, y, _Width, height)
                _Spd.OwnerPrintDraw(g, r, index, 1)

                'update y coord
                y += height
            Next

            _Printed = True

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

End Class