Public Class UserLogin
    Public Enum Levels
        User
        Supervisor
        Manager
    End Enum
    Private _Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB
    Private _User As BOSecurityProfile.cSystemUsers = Nothing
    Private _LogUser As BOSecurityProfile.cSystemUsers = Nothing
    Private _Level As Levels = Levels.User

    Public ReadOnly Property User() As BOSecurityProfile.cSystemUsers
        Get
            Return _User
        End Get
    End Property

    Public ReadOnly Property UserID() As Integer
        Get
            Return _User.UserID.Value
        End Get
    End Property



    Public Sub New(ByRef Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal Level As Levels)
        InitializeComponent()
        _Oasys3DB = Oasys3DB
        _Level = Level
        AddHandler cgbPassword.Collapsed, AddressOf cgb_Collapsed

        Select Case Level
            Case Levels.User : Me.Text = My.Resources.UserAuthorisation
            Case Levels.Supervisor : Me.Text = My.Resources.SupervisorAuthorisation
            Case Levels.Manager : Me.Text = My.Resources.ManagerAuthorisation
        End Select

    End Sub

    Public Sub New(ByRef Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal Level As Levels, ByVal Header As String)
        InitializeComponent()
        _Oasys3DB = Oasys3DB
        _Level = Level
        AddHandler cgbPassword.Collapsed, AddressOf cgb_Collapsed

        Select Case Level
            Case Levels.User : Me.Text = My.Resources.UserAuthorisation & " - " & Header
            Case Levels.Supervisor : Me.Text = My.Resources.SupervisorAuthorisation & " - " & Header
            Case Levels.Manager : Me.Text = My.Resources.ManagerAuthorisation & " - " & Header
        End Select

    End Sub

    Private Sub frmLogon_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cgbPassword.Collapse()
        txtUserID.Focus()
    End Sub

    Private Sub frmLogin_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        Select Case e.KeyData
            Case Keys.F5 : btnProceed.PerformClick()
            Case Keys.F10 : btnExit.PerformClick()
        End Select

    End Sub

    Private Sub UserLogin_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If _User Is Nothing Then
            DialogResult = Windows.Forms.DialogResult.Cancel
        Else
            DialogResult = Windows.Forms.DialogResult.OK
        End If

    End Sub


    Private Sub txtUserID_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUserID.KeyPress

        _User = Nothing
        btnProceed.Enabled = False

        'if enter button then go to next control
        If e.KeyChar = ChrW(Keys.Enter) Then Me.SelectNextControl(Me.ActiveControl, True, True, True, True)

        'allow tab, delete and digits
        e.Handled = True
        If e.KeyChar = ChrW(Keys.Tab) Then e.Handled = False
        If e.KeyChar = ChrW(Keys.Delete) Then e.Handled = False
        If e.KeyChar = ChrW(Keys.Back) Then e.Handled = False
        If Char.IsDigit(e.KeyChar) Then e.Handled = False

    End Sub

    Private Sub txtUserID_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUserID.Leave

        Try
            'check that cancel button has not been clicked
            If Me.ActiveControl Is btnExit Then Exit Sub

            'if txtvalue empty then set to zero
            If txtUserID.Text = String.Empty Then Exit Sub

            'check that ID is an integer
            Dim value As Integer = 0
            If Integer.TryParse(txtUserID.Text, value) Then
                txtUserID.Text = FormatNumber(value, 0)
            Else
                lblInitials.Text = String.Empty
                lblFullName.Text = My.Resources.ErrUserIdNotNumber
                txtPassword.Enabled = False
                txtUserID.SelectAll()
                txtUserID.Focus()
                Exit Sub
            End If

            'check user exists
            _User = Nothing
            _LogUser = New BOSecurityProfile.cSystemUsers(_Oasys3DB)

            If _LogUser.LoadUser(value) Then
                'check against level
                Dim userOk As Boolean = False
                Select Case _Level
                    Case Levels.Manager : If _LogUser.IsManager.Value Then userOk = True
                    Case Levels.Supervisor : If _LogUser.IsSupervisor.Value Then userOk = True
                    Case Else : userOk = True
                End Select

                If userOk Then
                    lblInitials.Text = _LogUser.Initials.Value.ToString
                    lblFullName.Text = _LogUser.Name.Value.ToString
                    txtPassword.Enabled = True
                    txtPassword.Focus()
                Else
                    lblInitials.Text = String.Empty
                    lblFullName.Text = My.Resources.ErrUserNotLevel
                    txtPassword.Enabled = False
                    txtUserID.SelectAll()
                    txtUserID.Focus()
                End If

            Else
                lblInitials.Text = String.Empty
                lblFullName.Text = My.Resources.ErrUserNotRecognised
                txtPassword.Enabled = False
                txtUserID.SelectAll()
                txtUserID.Focus()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub txtPassword_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPassword.KeyPress

        Try
            _User = Nothing
            btnProceed.Enabled = False
            txtPassword.BackColor = Color.White

            If e.KeyChar = ChrW(Keys.Enter) Then
                If _LogUser.CheckPassword(txtPassword.Text.ToString) Then
                    _User = _LogUser
                    txtPassword.BackColor = Color.Green

                    btnProceed.Enabled = True
                    DialogResult = Windows.Forms.DialogResult.OK
                    Me.SelectNextControl(Me.ActiveControl, True, True, True, True)
                Else
                    txtPassword.BackColor = Color.Red
                    txtPassword.Clear()
                    txtPassword.Focus()
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub



    Private Sub cgb_Collapsed(ByVal sender As Object, ByVal e As CollapsibleGroupBox.CollapsibleEventArgs)

        If e.Collapsed Then
            Height -= e.Height
        Else
            Height += e.Height
        End If

    End Sub



    Private Sub btnProceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        Close()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

End Class