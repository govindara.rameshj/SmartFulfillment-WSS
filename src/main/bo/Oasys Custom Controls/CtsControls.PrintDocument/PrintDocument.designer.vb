<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserLogin
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button
        Me.txtConfirmPass = New System.Windows.Forms.TextBox
        Me.txtNewPass = New System.Windows.Forms.TextBox
        Me.txtAnswer = New System.Windows.Forms.TextBox
        Me.txtQuestion = New System.Windows.Forms.TextBox
        Me.lblInfo = New System.Windows.Forms.Label
        Me.label7 = New System.Windows.Forms.Label
        Me.label6 = New System.Windows.Forms.Label
        Me.label5 = New System.Windows.Forms.Label
        Me.label4 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.lblFullName = New System.Windows.Forms.Label
        Me.lblInitials = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.txtUserID = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.grpLogin = New System.Windows.Forms.GroupBox
        Me.cgbPassword = New CollapsibleGroupBox.CollapsibleGroupBox
        Me.btnProceed = New System.Windows.Forms.Button
        Me.grpLogin.SuspendLayout()
        Me.cgbPassword.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(6, 207)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 39)
        Me.btnExit.TabIndex = 2
        Me.btnExit.TabStop = False
        Me.btnExit.Text = "F10 Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'txtConfirmPass
        '
        Me.txtConfirmPass.AcceptsReturn = True
        Me.txtConfirmPass.BackColor = System.Drawing.SystemColors.Window
        Me.txtConfirmPass.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtConfirmPass.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtConfirmPass.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.txtConfirmPass.Location = New System.Drawing.Point(116, 85)
        Me.txtConfirmPass.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.txtConfirmPass.MaxLength = 5
        Me.txtConfirmPass.Name = "txtConfirmPass"
        Me.txtConfirmPass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtConfirmPass.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtConfirmPass.Size = New System.Drawing.Size(65, 20)
        Me.txtConfirmPass.TabIndex = 3
        '
        'txtNewPass
        '
        Me.txtNewPass.AcceptsReturn = True
        Me.txtNewPass.BackColor = System.Drawing.SystemColors.Window
        Me.txtNewPass.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNewPass.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNewPass.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.txtNewPass.Location = New System.Drawing.Point(116, 62)
        Me.txtNewPass.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.txtNewPass.MaxLength = 5
        Me.txtNewPass.Name = "txtNewPass"
        Me.txtNewPass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtNewPass.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtNewPass.Size = New System.Drawing.Size(65, 20)
        Me.txtNewPass.TabIndex = 2
        '
        'txtAnswer
        '
        Me.txtAnswer.AcceptsReturn = True
        Me.txtAnswer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAnswer.BackColor = System.Drawing.SystemColors.Window
        Me.txtAnswer.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAnswer.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAnswer.Location = New System.Drawing.Point(116, 39)
        Me.txtAnswer.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.txtAnswer.MaxLength = 20
        Me.txtAnswer.Name = "txtAnswer"
        Me.txtAnswer.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAnswer.Size = New System.Drawing.Size(383, 20)
        Me.txtAnswer.TabIndex = 1
        '
        'txtQuestion
        '
        Me.txtQuestion.AcceptsReturn = True
        Me.txtQuestion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtQuestion.BackColor = System.Drawing.SystemColors.Window
        Me.txtQuestion.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtQuestion.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtQuestion.Location = New System.Drawing.Point(116, 16)
        Me.txtQuestion.MaxLength = 50
        Me.txtQuestion.Name = "txtQuestion"
        Me.txtQuestion.ReadOnly = True
        Me.txtQuestion.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtQuestion.Size = New System.Drawing.Size(383, 20)
        Me.txtQuestion.TabIndex = 0
        '
        'lblInfo
        '
        Me.lblInfo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblInfo.BackColor = System.Drawing.Color.Transparent
        Me.lblInfo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInfo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblInfo.Location = New System.Drawing.Point(187, 62)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblInfo.Size = New System.Drawing.Size(312, 18)
        Me.lblInfo.TabIndex = 17
        Me.lblInfo.Text = "Changing your question/answer combination is permitted"
        Me.lblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblInfo.Visible = False
        '
        'label7
        '
        Me.label7.BackColor = System.Drawing.Color.Transparent
        Me.label7.Cursor = System.Windows.Forms.Cursors.Default
        Me.label7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.label7.Location = New System.Drawing.Point(6, 85)
        Me.label7.Name = "label7"
        Me.label7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.label7.Size = New System.Drawing.Size(104, 20)
        Me.label7.TabIndex = 20
        Me.label7.Text = "Confirm Password"
        Me.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'label6
        '
        Me.label6.BackColor = System.Drawing.Color.Transparent
        Me.label6.Cursor = System.Windows.Forms.Cursors.Default
        Me.label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.label6.Location = New System.Drawing.Point(6, 62)
        Me.label6.Name = "label6"
        Me.label6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.label6.Size = New System.Drawing.Size(104, 20)
        Me.label6.TabIndex = 18
        Me.label6.Text = "New Password"
        Me.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'label5
        '
        Me.label5.BackColor = System.Drawing.Color.Transparent
        Me.label5.Cursor = System.Windows.Forms.Cursors.Default
        Me.label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.label5.Location = New System.Drawing.Point(6, 39)
        Me.label5.Name = "label5"
        Me.label5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.label5.Size = New System.Drawing.Size(104, 20)
        Me.label5.TabIndex = 15
        Me.label5.Text = "Answer"
        Me.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'label4
        '
        Me.label4.BackColor = System.Drawing.Color.Transparent
        Me.label4.Cursor = System.Windows.Forms.Cursors.Default
        Me.label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.label4.Location = New System.Drawing.Point(6, 16)
        Me.label4.Name = "label4"
        Me.label4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.label4.Size = New System.Drawing.Size(104, 20)
        Me.label4.TabIndex = 13
        Me.label4.Text = "Question"
        Me.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(6, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(104, 20)
        Me.Label1.TabIndex = 54
        Me.Label1.Tag = "110"
        Me.Label1.Text = "Payroll Number"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label8.Location = New System.Drawing.Point(187, 60)
        Me.Label8.Name = "Label8"
        Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label8.Size = New System.Drawing.Size(273, 20)
        Me.Label8.TabIndex = 61
        Me.Label8.Tag = "115"
        Me.Label8.Text = "Up to 5 characters"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFullName
        '
        Me.lblFullName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblFullName.BackColor = System.Drawing.SystemColors.Window
        Me.lblFullName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFullName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFullName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblFullName.Location = New System.Drawing.Point(187, 37)
        Me.lblFullName.Name = "lblFullName"
        Me.lblFullName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFullName.Size = New System.Drawing.Size(312, 20)
        Me.lblFullName.TabIndex = 2
        Me.lblFullName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInitials
        '
        Me.lblInitials.BackColor = System.Drawing.SystemColors.Window
        Me.lblInitials.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInitials.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblInitials.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblInitials.Location = New System.Drawing.Point(116, 37)
        Me.lblInitials.Name = "lblInitials"
        Me.lblInitials.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblInitials.Size = New System.Drawing.Size(65, 20)
        Me.lblInitials.TabIndex = 1
        Me.lblInitials.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(6, 59)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(104, 20)
        Me.Label3.TabIndex = 59
        Me.Label3.Tag = "112"
        Me.Label3.Text = "Password"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPassword
        '
        Me.txtPassword.AcceptsReturn = True
        Me.txtPassword.BackColor = System.Drawing.SystemColors.Window
        Me.txtPassword.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPassword.Enabled = False
        Me.txtPassword.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPassword.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.txtPassword.Location = New System.Drawing.Point(116, 60)
        Me.txtPassword.MaxLength = 5
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPassword.Size = New System.Drawing.Size(65, 20)
        Me.txtPassword.TabIndex = 3
        '
        'txtUserID
        '
        Me.txtUserID.AcceptsReturn = True
        Me.txtUserID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtUserID.BackColor = System.Drawing.SystemColors.Window
        Me.txtUserID.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtUserID.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtUserID.Location = New System.Drawing.Point(116, 13)
        Me.txtUserID.MaxLength = 20
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtUserID.Size = New System.Drawing.Size(383, 20)
        Me.txtUserID.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(6, 37)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(104, 20)
        Me.Label2.TabIndex = 56
        Me.Label2.Tag = "111"
        Me.Label2.Text = "Name"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'grpLogin
        '
        Me.grpLogin.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpLogin.Controls.Add(Me.Label1)
        Me.grpLogin.Controls.Add(Me.Label2)
        Me.grpLogin.Controls.Add(Me.txtUserID)
        Me.grpLogin.Controls.Add(Me.Label8)
        Me.grpLogin.Controls.Add(Me.txtPassword)
        Me.grpLogin.Controls.Add(Me.lblFullName)
        Me.grpLogin.Controls.Add(Me.Label3)
        Me.grpLogin.Controls.Add(Me.lblInitials)
        Me.grpLogin.Location = New System.Drawing.Point(6, 1)
        Me.grpLogin.Name = "grpLogin"
        Me.grpLogin.Size = New System.Drawing.Size(505, 86)
        Me.grpLogin.TabIndex = 0
        Me.grpLogin.TabStop = False
        '
        'cgbPassword
        '
        Me.cgbPassword.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgbPassword.Controls.Add(Me.label4)
        Me.cgbPassword.Controls.Add(Me.txtQuestion)
        Me.cgbPassword.Controls.Add(Me.label5)
        Me.cgbPassword.Controls.Add(Me.txtAnswer)
        Me.cgbPassword.Controls.Add(Me.label6)
        Me.cgbPassword.Controls.Add(Me.lblInfo)
        Me.cgbPassword.Controls.Add(Me.txtConfirmPass)
        Me.cgbPassword.Controls.Add(Me.txtNewPass)
        Me.cgbPassword.Controls.Add(Me.label7)
        Me.cgbPassword.Location = New System.Drawing.Point(6, 93)
        Me.cgbPassword.MinHeight = 18
        Me.cgbPassword.MinWidth = 18
        Me.cgbPassword.Name = "cgbPassword"
        Me.cgbPassword.Size = New System.Drawing.Size(505, 111)
        Me.cgbPassword.Style = CollapsibleGroupBox.CollapsibleGroupBox.Styles.Height
        Me.cgbPassword.TabIndex = 3
        Me.cgbPassword.TabStop = False
        Me.cgbPassword.Text = "Change Password "
        '
        'btnProceed
        '
        Me.btnProceed.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnProceed.Enabled = False
        Me.btnProceed.Location = New System.Drawing.Point(435, 207)
        Me.btnProceed.Name = "btnProceed"
        Me.btnProceed.Size = New System.Drawing.Size(76, 39)
        Me.btnProceed.TabIndex = 1
        Me.btnProceed.TabStop = False
        Me.btnProceed.Text = "F5 Proceed"
        Me.btnProceed.UseVisualStyleBackColor = True
        '
        'UserLogin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnExit
        Me.ClientSize = New System.Drawing.Size(517, 252)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnProceed)
        Me.Controls.Add(Me.cgbPassword)
        Me.Controls.Add(Me.grpLogin)
        Me.Controls.Add(Me.btnExit)
        Me.KeyPreview = True
        Me.Name = "UserLogin"
        Me.Padding = New System.Windows.Forms.Padding(3)
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "User Authentication"
        Me.grpLogin.ResumeLayout(False)
        Me.grpLogin.PerformLayout()
        Me.cgbPassword.ResumeLayout(False)
        Me.cgbPassword.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Public WithEvents txtConfirmPass As System.Windows.Forms.TextBox
    Public WithEvents txtNewPass As System.Windows.Forms.TextBox
    Public WithEvents txtAnswer As System.Windows.Forms.TextBox
    Public WithEvents txtQuestion As System.Windows.Forms.TextBox
    Public WithEvents lblInfo As System.Windows.Forms.Label
    Public WithEvents label7 As System.Windows.Forms.Label
    Public WithEvents label6 As System.Windows.Forms.Label
    Public WithEvents label5 As System.Windows.Forms.Label
    Public WithEvents label4 As System.Windows.Forms.Label
    Public WithEvents Label1 As System.Windows.Forms.Label
    Public WithEvents Label8 As System.Windows.Forms.Label
    Public WithEvents lblFullName As System.Windows.Forms.Label
    Public WithEvents lblInitials As System.Windows.Forms.Label
    Public WithEvents Label3 As System.Windows.Forms.Label
    Public WithEvents txtPassword As System.Windows.Forms.TextBox
    Public WithEvents txtUserID As System.Windows.Forms.TextBox
    Public WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grpLogin As System.Windows.Forms.GroupBox
    Friend WithEvents cgbPassword As CollapsibleGroupBox.CollapsibleGroupBox
    Friend WithEvents btnProceed As System.Windows.Forms.Button
End Class
