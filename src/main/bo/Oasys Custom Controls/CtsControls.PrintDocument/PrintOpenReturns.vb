﻿Public Class PrintOpenReturns
    Private _component As Object
    Private _retHeader As BOPurchases.cReturnHeader
    Private _retOptions As BOSystem.cRetailOptions
    Private _sysOptions As BOSystem.cSystemOptions

    Public Sub New(ByVal oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal retHeader As BOPurchases.cReturnHeader, ByVal component As Object)
        _component = component
        _retHeader = retHeader
        _retOptions = New BOSystem.cRetailOptions(oasys3DB)
        _retOptions.LoadRetailOptions()
        _sysOptions = New BOSystem.cSystemOptions(oasys3DB)
        _sysOptions.Load()
    End Sub

    Public Sub PrintCreationReports(ByVal PrintPreview As Boolean)

        'setup report with margins including header and footers
        Dim report As New CompositeLink(New PrintingSystem())
        report.Margins.Bottom = 50
        report.Margins.Top = 50
        report.Margins.Left = 50
        report.Margins.Right = 50
        report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Watermark, CommandVisibility.None)
        report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.FillBackground, CommandVisibility.None)
        report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.EditPageHF, CommandVisibility.None)

        AddHandler report.CreateMarginalHeaderArea, AddressOf report_CreateMarginalHeaderArea
        AddHandler report.CreateMarginalFooterArea, AddressOf report_CreateMarginalFooterArea

        'set up return header component
        Dim retHeader As New Link
        AddHandler retHeader.CreateDetailArea, AddressOf retHeader_CreateDetailArea

        'set up grid component
        Dim retGrid As New PrintableComponentLink
        retGrid.Component = CType(_component, IPrintable)

        'set up page break
        Dim reportBreak As New Link
        AddHandler reportBreak.CreateDetailArea, AddressOf pageBreak_CreateDetailArea

        'supplier header and footers
        Dim supHeader As New Link
        AddHandler supHeader.CreateDetailArea, AddressOf supHeader_CreateDetailArea
        Dim supFooter As New Link
        AddHandler supFooter.CreateDetailArea, AddressOf retFooter_CreateDetailArea

        'populate the collection of links in the composite link in order
        report.Links.Add(supHeader)
        report.Links.Add(retHeader)
        report.Links.Add(retGrid)
        report.Links.Add(supFooter)
        If PrintPreview Then
            report.ShowPreviewDialog()
        Else
            report.Print("default")
        End If

    End Sub

    Private Sub report_CreateMarginalHeaderArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        e.Graph.Modifier = BrickModifier.MarginalHeader
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near, StringAlignment.Far)

        'draw store number and name
        Dim tb As TextBrick = e.Graph.DrawString("Store: " & _retOptions.StoreIdName, Color.Black, New RectangleF(0, 10, e.Graph.ClientPageSize.Width, 20), BorderSide.None)

    End Sub

    Private Sub report_CreateMarginalFooterArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        Dim pib As PageInfoBrick
        e.Graph.Modifier = BrickModifier.MarginalFooter

        Dim r As RectangleF = RectangleF.Empty
        r.Height = 20

        'draw printed footer
        pib = e.Graph.DrawPageInfo(PageInfo.DateTime, "Printed: {0:dd/MM/yyyy HH:mm}", Color.Black, r, BorderSide.None)
        pib.Alignment = BrickAlignment.Near

        'draw page number
        pib = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, "Page {0} of {1}", Color.Black, r, BorderSide.None)
        pib.Alignment = BrickAlignment.Center

        'draw version number
        pib = e.Graph.DrawPageInfo(PageInfo.None, "Version " & Application.ProductVersion, Color.Black, r, BorderSide.None)
        pib.Alignment = BrickAlignment.Far

    End Sub

    Private Sub pageBreak_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        e.Graph.DrawEmptyBrick(New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 1))
        e.Graph.PrintingSystem.InsertPageBreak(0)

    End Sub

    Private Sub supHeader_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        'draw title
        e.Graph.Font = New Font(FontFamily.GenericSansSerif, 14, FontStyle.Bold)
        Dim tb As TextBrick = e.Graph.DrawString("Open Return", Color.Black, New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 20), BorderSide.None)
        tb.StringFormat = tb.StringFormat.ChangeAlignment(StringAlignment.Center)

    End Sub

    Private Sub retHeader_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        'work out column coords
        Dim report As Link = CType(sender, Link)
        Dim width1 As Integer = CInt(e.Graph.ClientPageSize.Width / 10 * 1.5)
        Dim width2 As Integer = CInt(e.Graph.ClientPageSize.Width / 10 * 3.5)

        e.Graph.BeginUnionRect()
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near, StringAlignment.Near)

        'draw first column
        e.Graph.Font = New Font(FontFamily.GenericSansSerif, 8, FontStyle.Regular)
        Dim tb As TextBrick = e.Graph.DrawString("Returns Order:", Color.Black, New RectangleF(0, 10, width1, 20), BorderSide.None)
        tb = e.Graph.DrawString("Creation Date:", Color.Black, New RectangleF(0, 30, width1, 20), BorderSide.None)
        tb = e.Graph.DrawString("To:", Color.Black, New RectangleF(0, 50, width1, 100), BorderSide.None)

        'draw second column
        tb = e.Graph.DrawString(_retHeader.Number.Value.PadLeft(6, "0"c), Color.Black, New RectangleF(width1, 10, width2, 20), BorderSide.None)
        tb = e.Graph.DrawString(_retHeader.DateCreated.Value.ToShortDateString, Color.Black, New RectangleF(width1, 30, width2, 20), BorderSide.None)
        tb = e.Graph.DrawString(_retHeader.Supplier.ReturnNumberNameAddress, Color.Black, New RectangleF(width1, 50, width2, 15), BorderSide.None)

        ' Finish the creation of a non-separable group of bricks.
        e.Graph.EndUnionRect()

    End Sub

    Private Sub retFooter_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        Dim width1 As Integer = CInt(e.Graph.ClientPageSize.Width / 2)
        Dim width2 As Integer = CInt(e.Graph.ClientPageSize.Width / 10 * 1.8)

        e.Graph.Modifier = BrickModifier.InnerPageFooter
        e.Graph.BackColor = Color.Transparent
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near, StringAlignment.Near)
        e.Graph.BeginUnionRect()

        ' Finish the creation of a non-separable group of bricks.
        e.Graph.EndUnionRect()

    End Sub


End Class