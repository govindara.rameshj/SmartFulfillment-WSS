﻿Public Class PrintReturns
    Private _component As Object
    Private _retHeader As BOPurchases.cReturnHeader
    Private _retOptions As BOSystem.cRetailOptions
    Private _sysOptions As BOSystem.cSystemOptions

    Public Sub New(ByVal oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal retHeader As BOPurchases.cReturnHeader, ByVal component As Object)
        _component = component
        _retHeader = retHeader
        _retOptions = New BOSystem.cRetailOptions(oasys3DB)
        _retOptions.LoadRetailOptions()
        _sysOptions = New BOSystem.cSystemOptions(oasys3DB)
        _sysOptions.Load()
    End Sub

    Public Sub PrintCreationReports()

        'setup report with margins including header and footers
        Dim report As New CompositeLink(New PrintingSystem())
        report.Margins.Bottom = 50
        report.Margins.Top = 50
        report.Margins.Left = 50
        report.Margins.Right = 50
        report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Watermark, CommandVisibility.None)
        report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.FillBackground, CommandVisibility.None)
        report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.EditPageHF, CommandVisibility.None)

        AddHandler report.CreateMarginalHeaderArea, AddressOf report_CreateMarginalHeaderArea
        AddHandler report.CreateMarginalFooterArea, AddressOf report_CreateMarginalFooterArea

        'set up return header component
        Dim retHeader As New Link
        AddHandler retHeader.CreateDetailArea, AddressOf retHeader_CreateDetailArea

        'set up grid component
        Dim retGrid As New PrintableComponentLink
        retGrid.Component = CType(_component, IPrintable)

        'set up page break
        Dim reportBreak As New Link
        AddHandler reportBreak.CreateDetailArea, AddressOf pageBreak_CreateDetailArea

        'supplier header and footers
        Dim supHeader As New Link
        AddHandler supHeader.CreateDetailArea, AddressOf supHeader_CreateDetailArea
        Dim supFooter As New Link
        AddHandler supFooter.CreateDetailArea, AddressOf retFooter_CreateDetailArea

        'merchandise header and footers
        Dim merHeader As New Link
        AddHandler merHeader.CreateDetailArea, AddressOf merHeader_CreateDetailArea
        Dim merFooter As New Link
        AddHandler merFooter.CreateDetailArea, AddressOf retFooter_CreateDetailArea

        'populate the collection of links in the composite link in order
        report.Links.Add(supHeader)
        report.Links.Add(retHeader)
        report.Links.Add(retGrid)
        report.Links.Add(supFooter)
        report.Links.Add(reportBreak)
        report.Links.Add(merHeader)
        report.Links.Add(retHeader)
        report.Links.Add(retGrid)
        report.Links.Add(merFooter)
        report.ShowPreviewDialog()

    End Sub

    Public Sub PrintReleaseReports()

        'setup report with margins including header and footers
        Dim report As New CompositeLink(New PrintingSystem())
        report.Margins.Bottom = 50
        report.Margins.Top = 50
        report.Margins.Left = 50
        report.Margins.Right = 50
        report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Watermark, CommandVisibility.None)
        report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.FillBackground, CommandVisibility.None)
        report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.EditPageHF, CommandVisibility.None)
        AddHandler report.CreateMarginalHeaderArea, AddressOf report_CreateMarginalHeaderArea
        AddHandler report.CreateMarginalFooterArea, AddressOf report_CreateMarginalFooterArea

        'set up grid component
        Dim reportGrid As New PrintableComponentLink
        reportGrid.Component = CType(_component, IPrintable)

        'set up page break
        Dim reportBreak As New Link
        AddHandler reportBreak.CreateDetailArea, AddressOf pageBreak_CreateDetailArea

        'supplier header and footers
        Dim supHeader As New Link
        AddHandler supHeader.CreateDetailArea, AddressOf storeHeader_CreateDetailArea
        Dim supFooter As New Link
        AddHandler supFooter.CreateDetailArea, AddressOf retFooter_CreateDetailArea

        'merchandise header and footers
        Dim merHeader As New Link
        AddHandler merHeader.CreateDetailArea, AddressOf hoHeader_CreateDetailArea
        Dim merFooter As New Link
        AddHandler merFooter.CreateDetailArea, AddressOf retFooter_CreateDetailArea

        'populate the collection of links in the composite link in order
        report.Links.Add(supHeader)
        report.Links.Add(reportGrid)
        report.Links.Add(supFooter)
        report.Links.Add(reportBreak)
        report.Links.Add(merHeader)
        report.Links.Add(reportGrid)
        report.Links.Add(merFooter)
        report.ShowPreviewDialog()

    End Sub


    Private Sub report_CreateMarginalHeaderArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        e.Graph.Modifier = BrickModifier.MarginalHeader
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near, StringAlignment.Far)

        'draw store number and name
        Dim tb As TextBrick = e.Graph.DrawString("Store: " & _retOptions.StoreIdName, Color.Black, New RectangleF(0, 10, e.Graph.ClientPageSize.Width, 20), BorderSide.None)

    End Sub

    Private Sub report_CreateMarginalFooterArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        Dim pib As PageInfoBrick
        e.Graph.Modifier = BrickModifier.MarginalFooter

        Dim r As RectangleF = RectangleF.Empty
        r.Height = 20

        'draw printed footer
        pib = e.Graph.DrawPageInfo(PageInfo.DateTime, "Printed: {0:dd/MM/yyyy HH:mm}", Color.Black, r, BorderSide.None)
        pib.Alignment = BrickAlignment.Near

        'draw page number
        pib = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, "Page {0} of {1}", Color.Black, r, BorderSide.None)
        pib.Alignment = BrickAlignment.Center

        'draw version number
        pib = e.Graph.DrawPageInfo(PageInfo.None, "Version " & Application.ProductVersion, Color.Black, r, BorderSide.None)
        pib.Alignment = BrickAlignment.Far

    End Sub



    Private Sub pageBreak_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        e.Graph.DrawEmptyBrick(New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 1))
        e.Graph.PrintingSystem.InsertPageBreak(0)

    End Sub

    Private Sub supHeader_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        'draw title
        e.Graph.Font = New Font(FontFamily.GenericSansSerif, 14, FontStyle.Bold)
        Dim tb As TextBrick = e.Graph.DrawString("Supplier Copy", Color.Black, New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 30), BorderSide.None)
        tb.StringFormat = tb.StringFormat.ChangeAlignment(StringAlignment.Center)

    End Sub

    Private Sub merHeader_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        'draw title
        e.Graph.Font = New Font(FontFamily.GenericSansSerif, 14, FontStyle.Bold)
        Dim tb As TextBrick = e.Graph.DrawString("Packed with Merchandise", Color.Black, New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 30), BorderSide.None)
        tb.StringFormat = tb.StringFormat.ChangeAlignment(StringAlignment.Center)

    End Sub

    Private Sub hoHeader_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        'draw title
        e.Graph.Font = New Font(FontFamily.GenericSansSerif, 14, FontStyle.Bold)
        Dim tb As TextBrick = e.Graph.DrawString("Head Office Copy", Color.Black, New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 30), BorderSide.None)
        tb.StringFormat = tb.StringFormat.ChangeAlignment(StringAlignment.Center)

    End Sub

    Private Sub storeHeader_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        'draw title
        e.Graph.Font = New Font(FontFamily.GenericSansSerif, 14, FontStyle.Bold)
        Dim tb As TextBrick = e.Graph.DrawString("Store Copy", Color.Black, New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 30), BorderSide.None)
        tb.StringFormat = tb.StringFormat.ChangeAlignment(StringAlignment.Center)

    End Sub


    Private Sub retHeader_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        'work out column coords
        Dim report As Link = CType(sender, Link)
        Dim width1 As Integer = CInt(e.Graph.ClientPageSize.Width / 10 * 1.5)
        Dim width2 As Integer = CInt(e.Graph.ClientPageSize.Width / 10 * 3.5)

        e.Graph.BeginUnionRect()
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near, StringAlignment.Near)

        'draw first column
        e.Graph.Font = New Font(FontFamily.GenericSansSerif, 8, FontStyle.Regular)
        Dim tb As TextBrick = e.Graph.DrawString("Returns Order:", Color.Black, New RectangleF(0, 40, width1, 20), BorderSide.None)
        tb = e.Graph.DrawString("Creation Date:", Color.Black, New RectangleF(0, 60, width1, 20), BorderSide.None)
        tb = e.Graph.DrawString("To:", Color.Black, New RectangleF(0, 80, width1, 100), BorderSide.None)

        'draw second column
        tb = e.Graph.DrawString(_retHeader.Number.Value.PadLeft(6, "0"c), Color.Black, New RectangleF(width1, 40, width2, 20), BorderSide.None)
        tb = e.Graph.DrawString(_retHeader.DateCreated.Value.ToShortDateString, Color.Black, New RectangleF(width1, 60, width2, 20), BorderSide.None)
        tb = e.Graph.DrawString(_retHeader.Supplier.ReturnNumberNameAddress, Color.Black, New RectangleF(width1, 80, width2, 100), BorderSide.None)

        'draw third column
        tb = e.Graph.DrawString("Your Ref:", Color.Black, New RectangleF(width1 + width2, 40, width1, 20), BorderSide.None)
        tb = e.Graph.DrawString("Collection Date:", Color.Black, New RectangleF(width1 + width2, 60, width1, 20), BorderSide.None)
        tb = e.Graph.DrawString("From:", Color.Black, New RectangleF(width1 + width2, 80, width1, 100), BorderSide.None)

        'draw last column
        Dim lb As New LineBrick
        lb.LineStyle = Drawing2D.DashStyle.Dot
        lb = e.Graph.DrawLine(New PointF(2 * width1 + width2, 55), New PointF(e.Graph.ClientPageSize.Width, 55), Color.Gray, 0.5)
        tb = e.Graph.DrawString(_retHeader.DateExpectCollect.Value.ToShortDateString, Color.Black, New RectangleF(width1 + width2 + width1, 60, width2, 20), BorderSide.None)
        tb = e.Graph.DrawString(_retOptions.StoreNameAddress, Color.Black, New RectangleF(width1 + width2 + width1, 80, width2, 100), BorderSide.None)

        ' Finish the creation of a non-separable group of bricks.
        e.Graph.EndUnionRect()

    End Sub

    Private Sub retFooter_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        Dim width1 As Integer = CInt(e.Graph.ClientPageSize.Width / 2)
        Dim width2 As Integer = CInt(e.Graph.ClientPageSize.Width / 10 * 1.8)

        e.Graph.Modifier = BrickModifier.InnerPageFooter
        e.Graph.BackColor = Color.Transparent
        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Near, StringAlignment.Near)
        e.Graph.BeginUnionRect()

        'draw first line
        Dim sb As New StringBuilder("The above goods will be despatched to you on:")
        sb.Append("....................................................Date; Unless already collected")
        Dim tb As TextBrick = e.Graph.DrawString(sb.ToString, Color.Black, New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 20), BorderSide.None)

        'insert line
        Dim lb As LineBrick = e.Graph.DrawLine(New PointF(0, 20), New PointF(e.Graph.ClientPageSize.Width, 20), Color.Black, 1)

        'draw first column
        tb = e.Graph.DrawString("Send ALL Credit Notes to:", Color.Black, New RectangleF(0, 30, width1, 20), BorderSide.None)
        tb = e.Graph.DrawString(_sysOptions.PurchaseLedgerDepartment, Color.Black, New RectangleF(0, 50, width1, 100), BorderSide.None)

        'draw second column
        tb = e.Graph.DrawString("Returned VIA:", Color.Black, New RectangleF(width1, 30, width2, 20), BorderSide.None)
        tb = e.Graph.DrawString("Drivers Signature:", Color.Black, New RectangleF(width1, 100, width2, 20), BorderSide.None)
        tb = e.Graph.DrawString("Drivers Name Printed:", Color.Black, New RectangleF(width1, 130, width2, 20), BorderSide.None)

        'draw third column (all line boxes)
        lb.LineStyle = Drawing2D.DashStyle.Dot
        e.Graph.ForeColor = Color.Black
        lb = e.Graph.DrawLine(New PointF(width1 + width2, 45), New PointF(e.Graph.ClientPageSize.Width, 45), Color.Black, 0.5)
        lb = e.Graph.DrawLine(New PointF(width1 + width2, 115), New PointF(e.Graph.ClientPageSize.Width, 115), Color.Black, 0.5)
        lb = e.Graph.DrawLine(New PointF(width1 + width2, 145), New PointF(e.Graph.ClientPageSize.Width, 145), Color.Black, 0.5)


        ' Finish the creation of a non-separable group of bricks.
        e.Graph.EndUnionRect()

    End Sub


End Class