﻿''' <summary>
''' Wrapper for enquiry selection criteria
''' </summary>
''' <remarks></remarks>
Public Class EnquirySelection
    Private _Oasys3DB As clsOasys3DB
    Private _ViewCol As BOViewer.cViewerColumns

    Sub New(ByRef Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByRef ViewConfig As BOViewer.cViewerConfig, ByRef ViewCol As BOViewer.cViewerColumns)
        InitializeComponent()
        _Oasys3DB = Oasys3DB
        _ViewCol = ViewCol

        'set up text label and operator dropdown
        lblName.Text = ViewCol.BOColumn.HeaderText
        cmbOperator_Initialise()

        Select Case ViewCol.SelectionTypeValue
            Case cViewerColumns.SelectionTypes.ComboBox, cViewerColumns.SelectionTypes.ComboBoxDescriptionOnly
                Dim config As cViewerConfig = ViewConfig
                Dim valueCol As cViewerColumns = Nothing
                Dim displayCol As cViewerColumns = Nothing

                Select Case ViewCol.LookupConfigID.Value
                    Case 0
                        Throw New Exception("No lookup Config ID specified")
                        Exit Sub
                    Case ViewConfig.ID.Value
                        'lookup is from this config
                        valueCol = ViewCol
                        displayCol = ViewConfig.Column(ViewCol.LookupColumnID.Value)

                    Case Else
                        'lookup is from another config so need to load it up
                        config = New BOViewer.cViewerConfig(Oasys3DB)
                        config.AddLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pEquals, config.ID, ViewCol.LookupConfigID.Value)
                        config.LoadMatches()

                        valueCol = config.Column(ViewCol.LookupColumnID.Value)
                        displayCol = config.Column(valueCol.LookupColumnID.Value)
                End Select

                'populate datatable and insert all record
                config.BO.AddLoadField(valueCol.BOColumn)
                config.BO.AddLoadField(displayCol.BOColumn)




                Dim Column As IEnquirySelection

                Column = (New EnquirySelectionFactory).GetImplementation

                config.BO.SortBy(Column.SelectionColumn(valueCol.BOColumn.ColumnName, displayCol.BOColumn.ColumnName), _
                                 clsOasys3DB.eOrderByType.Ascending)
                'config.BO.SortBy(valueCol.BOColumn.ColumnName, clsOasys3DB.eOrderByType.Ascending)





                Dim dt As DataTable = config.BO.GetSQLSelectDataSet.Tables(0)

                'trim display values and if normal combobox then add value to string
                For Each row As DataRow In dt.Rows
                    row(1) = row(1).ToString.Trim
                    If ViewCol.SelectionTypeValue = cViewerColumns.SelectionTypes.ComboBox Then
                        row(1) = CStr(row(0)) & Space(1) & CStr(row(1))
                    End If
                Next

                'get copy for second combo box and add ---All--- to first one
                Dim dt2 As DataTable = dt.Copy
                Dim dr As DataRow = dt.NewRow
                dr(0) = ""
                dr(1) = "---All---"
                dt.Rows.InsertAt(dr, 0)

                'from combo box
                cmbFrom.Visible = True
                cmbFrom.ValueMember = dt.Columns(0).ColumnName
                cmbFrom.DisplayMember = dt.Columns(1).ColumnName
                cmbFrom.DataSource = dt

                'to combo box
                cmbTo.ValueMember = dt2.Columns(0).ColumnName
                cmbTo.DisplayMember = dt2.Columns(1).ColumnName
                cmbTo.DataSource = dt2

            Case cViewerColumns.SelectionTypes.DateTimePicker
                dtpFrom.Visible = True
                dtpFrom.Value = Now.Date.AddDays(-Now.Day + 1)
                dtpTo.Value = Now.Date

            Case cViewerColumns.SelectionTypes.TextBox
                txtFrom.Visible = True

            Case cViewerColumns.SelectionTypes.TrueFalse
                Dim dt As New DataTable
                dt.Columns.Add("id", GetType(Integer))
                dt.Columns.Add("display", GetType(String))
                dt.Rows.Add(-1, "Neither")
                dt.Rows.Add(1, "True")
                dt.Rows.Add(0, "False")

                'from combo box
                cmbFrom.Visible = True
                cmbFrom.ValueMember = dt.Columns(0).ColumnName
                cmbFrom.DisplayMember = dt.Columns(1).ColumnName
                cmbFrom.DataSource = dt
        End Select

    End Sub


    Private Sub cmbOperator_Initialise()

        'get list of operators required
        Dim ops As New List(Of clsOasys3DB.eOperator)

        Select Case _ViewCol.SelectionTypeValue
            Case cViewerColumns.SelectionTypes.TextBox
                ops.Add(clsOasys3DB.eOperator.pIn)
                ops.Add(clsOasys3DB.eOperator.pNotIn)
                ops.Add(clsOasys3DB.eOperator.pLike)
                ops.Add(clsOasys3DB.eOperator.pGreaterThan)
                ops.Add(clsOasys3DB.eOperator.pGreaterThanOrEquals)
                ops.Add(clsOasys3DB.eOperator.pLessThan)
                ops.Add(clsOasys3DB.eOperator.pLessThanOrEquals)

            Case cViewerColumns.SelectionTypes.TrueFalse
                ops.Add(clsOasys3DB.eOperator.pEquals)

            Case Else
                ops.Add(clsOasys3DB.eOperator.pEquals)
                ops.Add(clsOasys3DB.eOperator.pNotEquals)
                ops.Add(clsOasys3DB.eOperator.pGreaterThan)
                ops.Add(clsOasys3DB.eOperator.pGreaterThanOrEquals)
                ops.Add(clsOasys3DB.eOperator.pLessThan)
                ops.Add(clsOasys3DB.eOperator.pLessThanOrEquals)
        End Select


        'create datatable of operators
        Dim dt As New DataTable
        dt.Columns.Add("op", GetType(clsOasys3DB.eOperator))
        dt.Columns.Add("display", GetType(String))
        For Each op As clsOasys3DB.eOperator In ops
            dt.Rows.Add(op, _Oasys3DB.OperatorString(op))
        Next

        'bind table to cmb control
        cmbOperator.ValueMember = "op"
        cmbOperator.DisplayMember = "display"
        cmbOperator.DataSource = dt

    End Sub

    Private Sub cmbOperator_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbOperator.SelectedIndexChanged

        Dim op As clsOasys3DB.eOperator = CType(cmbOperator.SelectedValue, clsOasys3DB.eOperator)
        Select Case op
            Case clsOasys3DB.eOperator.pGreaterThan
                chkLessThan.Text = "&& " & _Oasys3DB.OperatorString(clsOasys3DB.eOperator.pLessThan)
                If cmbFrom.Visible And cmbFrom.SelectedIndex = 0 Then Exit Sub
                chkLessThan.Visible = True
            Case clsOasys3DB.eOperator.pGreaterThanOrEquals
                chkLessThan.Text = "&& " & _Oasys3DB.OperatorString(clsOasys3DB.eOperator.pLessThanOrEquals)
                If cmbFrom.Visible And cmbFrom.SelectedIndex = 0 Then Exit Sub
                chkLessThan.Visible = True
            Case Else
                chkLessThan.Visible = False
                chkLessThan.Checked = False
        End Select

    End Sub

    Private Sub cmbFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbFrom.SelectedIndexChanged

        If cmbFrom.SelectedIndex = 0 Then
            Dim op As clsOasys3DB.eOperator = CType(cmbOperator.SelectedValue, clsOasys3DB.eOperator)
            Select Case op
                Case clsOasys3DB.eOperator.pGreaterThan
                    chkLessThan.Text = "&& " & _Oasys3DB.OperatorString(clsOasys3DB.eOperator.pLessThan)
                    chkLessThan.Visible = True
                Case clsOasys3DB.eOperator.pGreaterThanOrEquals
                    chkLessThan.Text = "&& " & _Oasys3DB.OperatorString(clsOasys3DB.eOperator.pLessThanOrEquals)
                    chkLessThan.Visible = True
                Case Else
                    chkLessThan.Visible = False
                    chkLessThan.Checked = False
            End Select
        Else
            'ensure that cmbTo is greater than this one
            If CDec(cmbTo.SelectedValue) < CDec(cmbFrom.SelectedValue) Then cmbTo.SelectedValue = cmbFrom.SelectedValue
        End If

    End Sub

    Private Sub cmbTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTo.SelectedIndexChanged

        'ensure that cmbFrom is less than this one
        If CDec(cmbFrom.SelectedValue) > CDec(cmbTo.SelectedValue) Then cmbFrom.SelectedValue = cmbTo.SelectedValue

    End Sub

    Public Sub AddLoadFilters(ByRef ViewConfig As cViewerConfig, ByRef First As Boolean)

        'get operator 
        Dim op As clsOasys3DB.eOperator = CType(cmbOperator.SelectedValue, clsOasys3DB.eOperator)

        Select Case True
            Case dtpFrom.Visible
                'add join if not the first filter
                CheckFirstLoad(ViewConfig, First)
                ViewConfig.BO.AddUntypedLoadFilter(op, _ViewCol.BOColumn, dtpFrom.Value)

                'check if dtpTo is visible and add extra filter
                If dtpTo.Visible Then
                    Select Case op
                        Case clsOasys3DB.eOperator.pGreaterThan
                            ViewConfig.BO.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                            ViewConfig.BO.AddUntypedLoadFilter(clsOasys3DB.eOperator.pLessThan, _ViewCol.BOColumn, dtpTo.Value)

                        Case clsOasys3DB.eOperator.pGreaterThanOrEquals
                            ViewConfig.BO.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                            ViewConfig.BO.AddUntypedLoadFilter(clsOasys3DB.eOperator.pLessThanOrEquals, _ViewCol.BOColumn, dtpTo.Value)
                    End Select
                End If


            Case cmbFrom.Visible
                If cmbFrom.SelectedIndex = 0 Then Exit Sub
                CheckFirstLoad(ViewConfig, First)
                ViewConfig.BO.AddUntypedLoadFilter(op, _ViewCol.BOColumn, cmbFrom.SelectedValue)

                'check if cmbTo is visible and add extra filter
                If cmbTo.Visible Then
                    Select Case op
                        Case clsOasys3DB.eOperator.pGreaterThan
                            ViewConfig.BO.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                            ViewConfig.BO.AddUntypedLoadFilter(clsOasys3DB.eOperator.pLessThan, _ViewCol.BOColumn, cmbTo.SelectedValue)

                        Case clsOasys3DB.eOperator.pGreaterThanOrEquals
                            ViewConfig.BO.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                            ViewConfig.BO.AddUntypedLoadFilter(clsOasys3DB.eOperator.pLessThanOrEquals, _ViewCol.BOColumn, cmbTo.SelectedValue)
                    End Select
                End If


            Case txtFrom.Visible
                'check that there is a value for from value
                If txtFrom.Text <> String.Empty Then
                    CheckFirstLoad(ViewConfig, First)
                    ViewConfig.BO.AddUntypedLoadFilter(op, _ViewCol.BOColumn, txtFrom.Text.Trim)
                End If

                'check if dtpTo is visible and add extra filter
                If txtTo.Visible AndAlso (txtTo.Text.Trim <> String.Empty) Then
                    Select Case op
                        Case clsOasys3DB.eOperator.pGreaterThan
                            CheckFirstLoad(ViewConfig, First)
                            ViewConfig.BO.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                            ViewConfig.BO.AddUntypedLoadFilter(clsOasys3DB.eOperator.pLessThan, _ViewCol.BOColumn, txtTo.Text.Trim)

                        Case clsOasys3DB.eOperator.pGreaterThanOrEquals
                            CheckFirstLoad(ViewConfig, First)
                            ViewConfig.BO.JoinLoadFilter(clsOasys3DB.eOperator.pAnd)
                            ViewConfig.BO.AddUntypedLoadFilter(clsOasys3DB.eOperator.pLessThanOrEquals, _ViewCol.BOColumn, txtTo.Text.Trim)
                    End Select
                End If
        End Select

    End Sub

    Private Sub CheckFirstLoad(ByRef viewConfig As BOViewer.cViewerConfig, ByRef first As Boolean)
        If first Then
            first = False
        Else
            viewConfig.BO.JoinLoadFilter(OasysDBBO.Oasys3.DB.clsOasys3DB.eOperator.pAnd)
        End If
    End Sub

    Private Sub chkLessThan_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLessThan.CheckedChanged

        If chkLessThan.Checked Then
            Select Case True
                Case dtpFrom.Visible : dtpTo.Visible = True
                Case cmbFrom.Visible : cmbTo.Visible = True
                Case txtFrom.Visible : txtTo.Visible = True
            End Select
        Else
            dtpTo.Visible = False
            cmbTo.Visible = False
            txtTo.Visible = False
        End If
    End Sub



End Class
