﻿Imports System.IO

Public Class Enquiry
    Private Enum MousePositions
        None
        Header
        Top
        Right
        Left
        Bottom
        CornerNW
        CornerNE
        CornerSW
        CornerSE
    End Enum
    Private _MousePosition As MousePositions = MousePositions.None
    Private _MouseRelative As Point = Nothing
    Private _DisplayStatus As DisplayStatus
    Private _DisplayError As DisplayError
    Private _DisplayOasys As DisplayOasys

    Private _Oasys3DB As New clsOasys3DB("Default", 0)
    Private _Selections As New List(Of EnquirySelection)
    Private _UserID As Integer
    Private _WorkstationID As Integer
    Private _RowForeColour As BOSystem.cParameter.RowForeColour
    Private _RowBackColour As BOSystem.cParameter.RowBackColour
    Public Delegate Sub DisplayStatus(ByVal message As String)
    Public Delegate Sub DisplayError(ByVal ex As Exception, ByVal Title As String)
    Public Delegate Sub DisplayOasys(ByVal ex As OasysDbException, ByVal Title As String)

    Public Property Selections() As List(Of EnquirySelection)
        Get
            Return _Selections
        End Get
        Set(ByVal value As List(Of EnquirySelection))
            _Selections = value
        End Set
    End Property
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 12/05/2011
    ' Referral No : 442
    ' Notes       : Added to get the sheet index
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''' <summary>
    ''' Added to get the sheet index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>

    Public ReadOnly Property SheetActive() As Integer
        Get
            If (spd.ActiveSheet.ActiveRow Is Nothing) Then
                Return -1
            Else
                If (spd.ActiveSheet.ActiveRow.Index >= 0 And spd.ActiveSheet.ActiveRow.ForeColor.Name.ToLower.Equals("blue")) Then
                    Return spd.ActiveSheet.ActiveRow.Index
                Else
                    Return -1
                End If
            End If
        End Get
    End Property

    Public ReadOnly Property SheetActiveColumn() As Integer
        Get
            If (spd.ActiveSheet.ActiveRow Is Nothing) Then
                Return -1
            Else
                If (spd.ActiveSheet.ActiveRow.Index >= 0) Then
                    Return spd.ActiveSheet.ActiveColumn.Index
                Else
                    Return -1
                End If
            End If
        End Get
    End Property
    Public Property SheetColumnTab() As Integer
        Get
            If (spd.ActiveSheet.ActiveRow Is Nothing) Then
                Return -1
            Else
                If (spd.ActiveSheet.ActiveRow.Index >= 0) Then

                    Return spd.ActiveSheet.ActiveColumn.Index
                Else
                    Return -1
                End If
            End If
        End Get
        Set(ByVal value As Integer)
            spd.ActiveSheet.SetActiveCell(0, value + 1)
        End Set
    End Property

    Sub New(ByRef displayStatus As DisplayStatus, ByRef displayError As DisplayError, ByRef displayOasys As DisplayOasys, ByVal RunParameters As String, ByVal UserID As Integer, ByVal WorkStationID As Integer, ByVal fromEnquiry As Boolean, Optional ByVal Title As String = "")
        InitializeComponent()

        Try
            _DisplayStatus = displayStatus
            _DisplayError = displayError
            _DisplayOasys = displayOasys

            'get row fore and back colours
            Dim param As New BOSystem.cParameter(_Oasys3DB)
            _RowBackColour = param.GetRowBackColours
            _RowForeColour = param.GetRowForeColours

            'initialise spread sheet
            spd_Initialise()

            'get viewer configs in run parameters
            Dim parametersString As String = String.Empty
            Dim configs As String() = RunParameters.Split(";"c)

            For configIndex As Integer = 0 To configs.Count - 1
                Dim configID As Integer = 0
                Dim configParams As New ArrayList

                For Each s As String In configs(configIndex).Split("|"c)
                    If s.StartsWith("CONFIG=") Then
                        If Not Integer.TryParse(s.Substring(7).Trim, configID) Then
                            ' DisplayStatus(My.Resources.ErrViewConfigIdNotInteger, DisplayTypes.IsError)
                            Exit Sub
                        End If
                    Else
                        configParams.Add(s)
                    End If
                Next

                'load viewer config from id
                Dim viewConfig As New cViewerConfig(_Oasys3DB)
                viewConfig.Load(configID)

                'if any parameters given then add these to config BO and load records
                If configParams.Count > 0 Then
                    viewConfig.Parameters = configParams
                    viewConfig.LoadRecords(True)
                    parametersString = viewConfig.ParametersString
                End If

                'add this config to the spread and add any sorting and tabs
                spd_AddSheet(viewConfig)
                ddbSorting_Initialise(viewConfig)
                ddbTabs_Initialise(viewConfig)

                'reset active sheet back to first sheet
                spd.ActiveSheet = spd.Sheets(0)

                'if first viewconfig then add selections
                If configIndex = 0 Then
                    If Not fromEnquiry Then Selections_Initialise(viewConfig)
                End If

                'if any parameters given then add records to sheet else set tooltips
                If configParams.Count > 0 Then
                    spd_AddSheetRecords(configIndex)
                Else
                    ToolTip1.SetToolTip(btnAdd, My.Resources.ToolTips.btnAdd)
                    ToolTip1.SetToolTip(btnRefresh, My.Resources.ToolTips.btnRefreshGet)
                End If
            Next


            'if only one sheet then use sheetname of sheet as title else use title if exists
            If spd.Sheets.Count = 1 Then
                lblTitle.Text = spd.ActiveSheet.SheetName & Space(2) & parametersString
            Else
                If Title.Length = 0 Then Title = spd.ActiveSheet.SheetName
                lblTitle.Text = Title & Space(2) & parametersString
            End If

            spd_ActiveSheetChanged(spd, New EventArgs)

        Catch ex As OasysDbException
            _DisplayOasys(ex, lblTitle.Text)
        Catch ex As Exception
            _DisplayError(ex, lblTitle.Text)
        End Try

    End Sub



    Private Sub spd_Initialise()

        'key mappings
        Dim im As InputMap = spd.GetInputMap(InputMapMode.WhenAncestorOfFocused)
        im.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)
        im.Put(New Keystroke(Keys.F2, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F3, Keys.None), SpreadActions.None)
        im.Put(New Keystroke(Keys.F4, Keys.None), SpreadActions.None)
        Dim imFocused As InputMap = spd.GetInputMap(InputMapMode.WhenFocused)
        imFocused.Put(New Keystroke(Keys.Enter, Keys.None), SpreadActions.MoveToNextRow)

        'AddHandler spd.Resize, AddressOf spd_Resize
        AddHandler spd.EditModeOff, AddressOf spd_EditModeOff
        AddHandler spd.SelectionChanged, AddressOf spd_SelectionChanged
        AddHandler spd.CellClick, AddressOf spd_CellClick
        AddHandler spd.TextTipFetch, AddressOf spd_TextTipFetch
        AddHandler spd.ActiveSheetChanged, AddressOf spd_ActiveSheetChanged

        spd.TextTipPolicy = TextTipPolicy.Floating
        spd.TextTipDelay = 1000

    End Sub

    Private Sub spd_AddSheet(ByVal viewConfig As cViewerConfig)

        Dim sheet As New SheetView
        sheet.RowCount = 0
        sheet.ColumnCount = 0
        sheet.DefaultStyle.VerticalAlignment = CellVerticalAlignment.Bottom
        sheet.DefaultStyle.CellType = New CellType.TextCellType
        sheet.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Left
        sheet.SheetName = viewConfig.Title.Value
        sheet.Tag = viewConfig

        'column/row headers
        sheet.ColumnHeaderHorizontalGridLine = New GridLine(GridLineType.None)
        sheet.ColumnHeaderVerticalGridLine = New GridLine(GridLineType.None)
        sheet.ColumnHeader.DefaultStyle.BackColor = Drawing.Color.Transparent
        sheet.ColumnHeader.DefaultStyle.Border = New LineBorder(Drawing.Color.Black, 1, False, False, False, True)
        sheet.ColumnHeader.DefaultStyle.Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.5, Drawing.FontStyle.Bold)
        sheet.ColumnHeader.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Center
        sheet.ColumnHeader.Rows(-1).Height = 30
        sheet.ColumnHeaderVisible = False

        sheet.RowHeaderVisible = False
        sheet.RowHeader.DefaultStyle.Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.5, Drawing.FontStyle.Bold)
        sheet.RowHeader.DefaultStyle.Border = New LineBorder(Drawing.Color.Black, 1, False, False, True, False)
        sheet.RowHeader.DefaultStyle.BackColor = Drawing.Color.Transparent
        sheet.RowHeader.DefaultStyle.HorizontalAlignment = CellHorizontalAlignment.Left

        'selection modes
        sheet.SelectionBackColor = _RowBackColour.Selection
        sheet.SelectionStyle = SelectionStyles.SelectionColors
        Select Case viewConfig.IsHorizontal.Value
            Case True : sheet.SelectionUnit = Model.SelectionUnit.Row
            Case False : sheet.SelectionUnit = Model.SelectionUnit.Column
        End Select

        'print info stuff
        sheet.PrintInfo.Header = "/c/fz""14""" & sheet.SheetName
        sheet.PrintInfo.Header &= "/n "
        sheet.PrintInfo.Margin.Left = 50
        sheet.PrintInfo.Margin.Right = 50
        sheet.PrintInfo.ShowBorder = False
        sheet.PrintInfo.UseSmartPrint = True

        'loop through viewer configs
        Dim index As Integer = 0
        Dim fontUnderline As Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8.5, Drawing.FontStyle.Underline)
        Dim typeDate As New CellType.DateTimeCellType
        typeDate.DateTimeFormat = CellType.DateTimeFormat.ShortDate

        For Each viewCol As BOViewer.cViewerColumns In viewConfig.Columns
            'set up index of BOColumn and index (required for totalling in sorts)
            viewConfig.BOIndex(index) = viewConfig.BO.BOFields.IndexOf(viewCol.BOColumn)
            viewCol.Index = index

            If Not viewCol.DisplayColumn.Value Then Continue For

            'set up column/row headers
            Select Case viewConfig.IsHorizontal.Value
                Case True
                    sheet.Columns.Add(index, 1)
                    sheet.Columns(index).Tag = viewCol
                    sheet.Columns(index).Label = viewCol.HeaderText
                    If viewCol.BOColumn.IdentityKey Then
                        sheet.Columns(index).Locked = True
                    Else
                        sheet.Columns(index).Locked = Not (viewConfig.AllowUpdate.Value And viewCol.IsEditable.Value)
                    End If

                Case False
                    sheet.Rows.Add(index, 1)
                    sheet.Rows(index).Tag = viewCol
                    sheet.Rows(index).Label = viewCol.HeaderText
                    If viewCol.BOColumn.IdentityKey Then
                        sheet.Rows(index).Locked = True
                    Else
                        sheet.Rows(index).Locked = Not (viewConfig.AllowUpdate.Value And viewCol.IsEditable.Value)
                    End If
            End Select


            'check whether lookup column, load config and get datatable
            Select Case viewCol.LookupConfigID.Value
                Case 0, viewConfig.ID.Value

                    'Set celltype of column
                    Select Case viewCol.ColumnType
                        Case ColumnTypes.Date
                            Select Case viewConfig.IsHorizontal.Value
                                Case True : sheet.Columns(index).CellType = typeDate
                                Case False : sheet.Rows(index).CellType = typeDate
                            End Select

                        Case ColumnTypes.Number
                            Dim typeNumber As New CellType.NumberCellType
                            typeNumber.DecimalPlaces = viewCol.DecimalPlaces
                            Select Case viewConfig.IsHorizontal.Value
                                Case True
                                    sheet.Columns(index).CellType = typeNumber
                                    sheet.Columns(index).HorizontalAlignment = CellHorizontalAlignment.Right
                                Case False
                                    sheet.Rows(index).CellType = typeNumber
                                    sheet.Rows(index).HorizontalAlignment = CellHorizontalAlignment.Right
                            End Select

                        Case ColumnTypes.Boolean
                            Select Case viewConfig.IsHorizontal.Value
                                Case True
                                    sheet.Columns(index).CellType = New CellType.CheckBoxCellType
                                    sheet.Columns(index).HorizontalAlignment = CellHorizontalAlignment.Center
                                    sheet.Columns(index).VerticalAlignment = CellVerticalAlignment.Center
                                Case False
                                    sheet.Rows(index).CellType = New CellType.CheckBoxCellType
                                    sheet.Rows(index).HorizontalAlignment = CellHorizontalAlignment.Center
                                    sheet.Rows(index).VerticalAlignment = CellVerticalAlignment.Center
                            End Select
                    End Select

                Case Else
                    'Get lookup for column
                    Dim config As New BOViewer.cViewerConfig(_Oasys3DB)
                    config.Load(viewCol.LookupConfigID.Value)
                    config.BO.AddLoadField(config.Column(viewCol.LookupColumnID.Value).BOColumn)
                    config.BO.AddLoadField(config.Column(config.Column(viewCol.LookupColumnID.Value).LookupColumnID.Value).BOColumn)

                    Dim dt As DataTable = config.BO.GetSQLSelectDataSet.Tables(0)
                    Dim items(dt.Rows.Count - 1) As String
                    Dim itemData(dt.Rows.Count - 1) As String
                    For Each row As DataRow In dt.Rows
                        items(dt.Rows.IndexOf(row)) = CStr(row(0)) & Space(1) & row(1).ToString.Trim
                        itemData(dt.Rows.IndexOf(row)) = CStr(row(0))
                    Next

                    Dim typeCombo As New myComboCellType
                    typeCombo.AcceptsArrowKeys = SuperEdit.AcceptsArrowKeys.AllArrows
                    typeCombo.AutoCompleteMode = AutoCompleteMode.Append
                    typeCombo.ButtonAlign = ButtonAlign.Right
                    typeCombo.Editable = False
                    typeCombo.Items = items
                    typeCombo.ItemData = itemData
                    typeCombo.EditorValue = CellType.EditorValue.ItemData
                    typeCombo.MaxDrop = Math.Min(6, dt.Rows.Count)
                    typeCombo.Locked = sheet.Columns(sheet.ColumnCount - 1).Locked

                    If viewConfig.IsHorizontal.Value Then
                        sheet.Columns(index).CellType = typeCombo
                    Else
                        sheet.Rows(index).CellType = typeCombo
                    End If
            End Select

            'check whether hyperlink cell and if so then underline entries
            If viewCol.HyperLink.Value <> String.Empty Then
                Select Case viewConfig.IsHorizontal.Value
                    Case True : sheet.Columns(index).Font = fontUnderline
                    Case False : sheet.Rows(index).Font = fontUnderline
                End Select
            End If

            'increment index
            index += 1
        Next

        spd.Sheets.Add(sheet)

    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 12/05/2011
    ' Referral No : 442
    ' Notes       : Modified to bring the focus to the first row in the sheet
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''' <summary>
    ''' Modified to bring the focus to the first row in the sheet
    ''' </summary>
    ''' <param name="sheetIndex"></param>
    ''' <remarks></remarks>
    Private Sub spd_AddSheetRecords(ByVal sheetIndex As Integer)

        Try
            'get sheet and viewer config
            Dim sheet As SheetView = spd.Sheets(sheetIndex)
            Dim viewConfig As cViewerConfig = CType(sheet.Tag, cViewerConfig)

            'set cursor and display message
            Cursor = Cursors.WaitCursor
            _DisplayStatus(My.Resources.Messages.GetRecords & sheet.SheetName)

            'reset sheet rows/columns first removing any sorting
            Select Case viewConfig.IsHorizontal.Value
                Case True
                    sheet.AutoSortColumn(0)
                    sheet.RowCount = 0
                Case False : sheet.ColumnCount = 0
            End Select
            sheet.RowHeaderVisible = False
            sheet.ColumnHeaderVisible = False
            spd.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
            spd.VerticalScrollBarPolicy = ScrollBarPolicy.Never
            Refresh()

            'clear bo first
            viewConfig.BO.ClearLists()

            ''add load fields
            'For Each viewCol As cViewerColumns In viewConfig.Columns
            '    viewConfig.BO.AddLoadField(viewCol.BOColumn)
            'Next

            'Get load filters from parameter user controls if first sheet
            Dim firstFilter As Boolean = True
            If sheetIndex = 0 Then
                For Each selection As EnquirySelection In _Selections
                    selection.AddLoadFilters(viewConfig, firstFilter)
                Next
            End If

            'add sheet and populate with records
            viewConfig.LoadRecords(firstFilter)

            'check how many records
            Dim count As Integer = viewConfig.BO.BORecords.Count
            Dim message As String = My.Resources.Messages.RecordsReturned & count
            _DisplayStatus(message)

            'add all records
            Select Case viewConfig.IsHorizontal.Value
                Case True
                    For Each record As OasysDBBO.cBaseClass In viewConfig.BO.BORecords
                        sheet.Rows.Add(sheet.RowCount, 1)
                        sheet.Rows(sheet.RowCount - 1).Tag = record

                        'loop through rows/columns in the sheet
                        For index As Integer = 0 To sheet.ColumnCount - 1
                            sheet.Cells(sheet.RowCount - 1, index).Value = record.BOFields(viewConfig.BOIndex(index)).UntypedValue
                        Next
                    Next

                Case False
                    For Each record As OasysDBBO.cBaseClass In viewConfig.BO.BORecords
                        sheet.Columns.Add(sheet.ColumnCount, 1)
                        sheet.Columns(sheet.ColumnCount - 1).Tag = record

                        'loop through rows/columns in the sheet
                        For index As Integer = 0 To sheet.RowCount - 1
                            Dim viewCol As cViewerColumns = CType(sheet.Rows(index).Tag, cViewerColumns)
                            sheet.Cells(index, sheet.ColumnCount - 1).Value = record.BOFields(viewConfig.BOIndex(index)).UntypedValue
                        Next
                    Next
            End Select

            'if any records added then perform resize
            Dim added As Boolean = False
            Select Case viewConfig.IsHorizontal.Value
                Case True : If sheet.RowCount > 0 Then added = True
                Case False : If sheet.ColumnCount > 0 Then added = True
            End Select

            If added Then
                'lock any key fields
                For Each viewCol As BOViewer.cViewerColumns In viewConfig.Columns
                    If viewCol.BOColumn.ColumnKey Then
                        Select Case viewConfig.IsHorizontal.Value
                            Case True : sheet.Cells(0, viewCol.Index, sheet.RowCount - 1, viewCol.Index).Locked = True
                            Case False : sheet.Cells(viewCol.Index, 0, viewCol.Index, sheet.ColumnCount - 1).Locked = True
                        End Select
                    End If
                Next

                'select first row/column


                _Selections.Item(0).txtFrom.Enabled = False
                _Selections.Item(0).cmbOperator.Enabled = False
                _Selections.Item(0).cmbOperator.Enabled = True
                _Selections.Item(0).txtFrom.Enabled = True
                spd.Focus()
                sheet.SetActiveCell(0, 1)


                Select Case viewConfig.IsHorizontal.Value
                    Case True
                        sheet.AddSelection(0, 0, 1, sheet.ColumnCount)
                        sheet.ColumnHeaderVisible = True

                        'apply any sorting
                        For Each ctl As Control In pnlControlBox.Controls
                            If TypeOf ctl Is DevExpress.XtraEditors.DropDownButton Then
                                If ctl.Text = "sorting" And CInt(ctl.Tag) = spd.ActiveSheetIndex Then
                                    'get selected menu item
                                    Dim menu As DXPopupMenu = CType(CType(ctl, DevExpress.XtraEditors.DropDownButton).DropDownControl, DXPopupMenu)
                                    For Each menuItem As DXMenuItem In menu.Items
                                        If menuItem.Image IsNot Nothing Then ddbSorting_Click(menuItem, New EventArgs)
                                    Next
                                End If
                            End If
                        Next

                    Case False
                        sheet.AddSelection(0, 0, sheet.RowCount, 1)
                        sheet.RowHeaderVisible = True

                        'set width of row headers
                        For Each col As Column In sheet.RowHeader.Columns
                            col.Width = col.GetPreferredWidth
                        Next
                End Select

                btnDelete.Enabled = True
                btnSave.Enabled = True
                btnPrint.Enabled = True
                spd_Resize(spd, New EventArgs, True)
            End If

        Finally
            Cursor = Cursors.Default
            _DisplayStatus(String.Empty)
        End Try

    End Sub



    Private Sub spd_Resize(ByVal sender As Object, ByVal e As EventArgs, Optional ByVal resize As Boolean = False) Handles spd.Resize

        Try
            Dim spread As FpSpread = CType(sender, FpSpread)
            spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
            spread.VerticalScrollBarPolicy = ScrollBarPolicy.Never

            'check that are sheets available
            If spread.Sheets.Count = 0 Then Exit Sub

            'get tab strip and border style sizes
            Dim borderWidth As Integer = 2 * SystemInformation.Border3DSize.Width
            Dim borderHeight As Integer = 2 * SystemInformation.Border3DSize.Height
            Dim scrollWidth As Integer = SystemInformation.VerticalScrollBarWidth
            Dim scrollHeight As Integer = SystemInformation.HorizontalScrollBarHeight
            Dim spreadWidth As Single = spread.Width
            Dim spreadHeight As Single = spread.Height

            'make allowance for tab strip and border
            If spread.TabStripPolicy <> TabStripPolicy.Never AndAlso spread.TabStrip.Count > 1 Then spreadHeight -= 20
            If spread.BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then
                spreadHeight -= (borderHeight * 2)
                spreadWidth -= (borderWidth * 2)
            End If


            For Each sheet As SheetView In spread.Sheets
                'check sheet has any rows first
                If sheet.RowCount = 0 Then Continue For

                Dim sheetHeight As Single = spreadHeight
                Dim sheetWidth As Single = spreadWidth

                'Get row header widths
                If sheet.Visible = False Then Continue For
                If sheet.RowHeaderVisible Then
                    For Each header As Column In sheet.RowHeader.Columns
                        If header.Visible Then sheetWidth -= header.Width - 3 ' 3 for border
                    Next
                End If

                'Get column header heights
                If sheet.ColumnHeaderVisible Then
                    For Each header As Row In sheet.ColumnHeader.Rows
                        If header.Visible Then sheetHeight -= header.Height - 3 ' 3 for border
                    Next
                End If

                'get rowsHeight and colsWidth
                Dim rowsHeight As Integer = CInt(sheet.RowCount * sheet.Rows.Default.Height)
                Dim colsWidth As Integer = 0
                For Each col As Column In sheet.Columns
                    If col.Visible Then colsWidth += CInt(col.Width)
                Next

                'check whether need scrollbars
                If colsWidth > sheetWidth Then spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
                If spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always Then sheetHeight -= scrollHeight

                If rowsHeight > sheetHeight Then spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always
                If spread.VerticalScrollBarPolicy = ScrollBarPolicy.Always Then sheetWidth -= scrollWidth


                'get columns to resize and new widths
                Dim colsTotal As Single = 0
                Dim colsResizable As Integer = 0
                For Each c As Column In sheet.Columns
                    If c.Visible And c.Resizable Then
                        If resize Then c.Width = c.GetPreferredWidth
                        colsResizable += 1
                    End If
                    If c.Visible Then colsTotal += c.Width
                Next

                'check whether need horizontal scrollbar
                If colsTotal > sheetWidth Then
                    spread.HorizontalScrollBarPolicy = ScrollBarPolicy.Always
                Else
                    'if resizing then fill up empty space
                    If resize Then
                        If colsResizable = 0 Then Continue For
                        Dim increase As Integer = CInt((sheetWidth - colsTotal) / colsResizable)
                        For Each c As Column In sheet.Columns
                            If c.Visible And c.Resizable Then c.Width += increase
                        Next
                    End If
                End If

            Next

        Catch ex As Exception
            Trace.WriteLine(ex.Message, lblTitle.Text)
        End Try

    End Sub

    Private Sub spd_EditModeOff(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            'get record and set cell current value
            Dim sheet As SheetView = CType(sender, FpSpread).ActiveSheet
            Dim viewConfig As cViewerConfig = CType(sheet.Tag, cViewerConfig)
            Dim value As Object = sheet.ActiveCell.Value

            Dim record As cBaseClass = Nothing
            Dim viewCol As cViewerColumns = Nothing
            Dim colour As Drawing.Color = Color.Empty
            Select Case viewConfig.IsHorizontal.Value
                Case True
                    record = CType(sheet.ActiveRow.Tag, cBaseClass)
                    colour = sheet.ActiveRow.ForeColor
                    viewCol = CType(sheet.ActiveColumn.Tag, cViewerColumns)
                Case False
                    record = CType(sheet.ActiveColumn.Tag, cBaseClass)
                    colour = sheet.ActiveColumn.ForeColor
                    viewCol = CType(sheet.ActiveRow.Tag, cViewerColumns)
            End Select

            'set new value in record and set forecolour
            record.GetProperty(viewCol.ColumnName.Value).UntypedValue = value
            Select Case colour
                Case _RowForeColour.Delete : colour = _RowForeColour.ChangedDelete
                Case _RowForeColour.Add
                Case Else : colour = _RowForeColour.Changed
            End Select

            Select Case viewConfig.IsHorizontal.Value
                Case True : sheet.ActiveRow.ForeColor = colour
                Case False : sheet.ActiveColumn.ForeColor = colour
            End Select

            'check for duplicates
            CheckForDuplicates()

        Catch ex As Exception
            _DisplayError(ex, lblTitle.Text)
        End Try

    End Sub

    Private Sub spd_ActiveSheetChanged(ByVal sender As Object, ByVal e As EventArgs)

        Try
            'set up control box controls visibility
            Dim sheet As SheetView = spd.ActiveSheet
            Dim viewConfig As cViewerConfig = CType(sheet.Tag, cViewerConfig)
            If Not viewConfig.AllowAddition.Value Then btnAdd.Visible = False
            If Not viewConfig.AllowDeletion.Value Then btnDelete.Visible = False
            If Not (viewConfig.AllowAddition.Value OrElse viewConfig.AllowDeletion.Value OrElse viewConfig.AllowUpdate.Value) Then
                btnSave.Visible = False
            End If

            'make all ddb visible in control box for this sheet index
            For Each ctl As Control In pnlControlBox.Controls
                If TypeOf ctl Is DevExpress.XtraEditors.DropDownButton Then
                    Dim menu As DXPopupMenu = CType(CType(ctl, DevExpress.XtraEditors.DropDownButton).DropDownControl, DXPopupMenu)
                    ctl.Visible = (CInt(ctl.Tag) = spd.ActiveSheetIndex AndAlso menu.Items.Count > 1)
                End If
            Next

        Catch ex As Exception
            _DisplayError(ex, lblTitle.Text)
        End Try

    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author      : Dhanesh Ramachandran
    ' Date        : 12/05/2011
    ' Referral No : 442
    ' Notes       : Modified to for grid selection
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    ''' <summary>
    ''' Modified to for grid selection
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>



    Private Sub spd_SelectionChanged(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.SelectionChangedEventArgs)

        Try
            'set up title label and control box controls visibility
            Dim sheet As SheetView = spd.ActiveSheet
            Dim myButton As Button
            'SheetActive = spd.ActiveSheet.ActiveRow.Index

            Dim viewConfig As cViewerConfig = CType(sheet.Tag, cViewerConfig)

            If sheet.RowCount = 0 Then
                btnDelete.Enabled = False
                btnSave.Enabled = False
                btnPrint.Enabled = False
                spd.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
                spd.VerticalScrollBarPolicy = ScrollBarPolicy.Never
                sheet.ColumnHeaderVisible = False
                sheet.RowHeaderVisible = False

                ToolTip1.RemoveAll()
                ToolTip1.SetToolTip(btnAdd, My.Resources.ToolTips.btnAdd)
                ToolTip1.SetToolTip(btnRefresh, My.Resources.ToolTips.btnRefreshGet)

            ElseIf sender.GetType.ToString.Equals("System.Windows.Forms.Button") Then
                myButton = CType(sender, Button)
                If (sheet.RowCount = 1 And sheet.ActiveColumn.Index = 3 And myButton.Name.Equals("btnSave")) Then

                    sheet.Rows.Item(0).Remove()
                    btnDelete.Enabled = False
                    btnSave.Enabled = False
                    btnPrint.Enabled = False
                    spd.HorizontalScrollBarPolicy = ScrollBarPolicy.Never
                    spd.VerticalScrollBarPolicy = ScrollBarPolicy.Never
                    sheet.ColumnHeaderVisible = False
                    sheet.RowHeaderVisible = False

                    ToolTip1.RemoveAll()
                    ToolTip1.SetToolTip(btnAdd, My.Resources.ToolTips.btnAdd)
                    ToolTip1.SetToolTip(btnRefresh, My.Resources.ToolTips.btnRefreshGet)
                    _Selections.Item(0).txtFrom.Clear()
                    _Selections.Item(0).txtFrom.Focus()
                End If
            Else
                btnDelete.Enabled = True
                btnSave.Enabled = True
                btnPrint.Enabled = True

                'set tooltips
                ToolTip1.SetToolTip(btnExit, My.Resources.ToolTips.btnExit)
                ToolTip1.SetToolTip(btnPrint, My.Resources.ToolTips.btnPrint)
                ToolTip1.SetToolTip(btnRefresh, My.Resources.ToolTips.btnRefresh)
                ToolTip1.SetToolTip(btnAdd, My.Resources.ToolTips.btnAdd)
                ToolTip1.SetToolTip(btnSave, My.Resources.ToolTips.btnSave)

                'get state of current record
                Dim colour As Drawing.Color = Color.White
                Select Case viewConfig.IsHorizontal.Value
                    Case True : colour = sheet.ActiveRow.ForeColor
                    Case False : colour = sheet.ActiveColumn.ForeColor
                End Select

                Select Case colour
                    Case _RowForeColour.Delete, _RowForeColour.ChangedDelete : ToolTip1.SetToolTip(btnDelete, My.Resources.ToolTips.btnDeleteUnset)
                    Case _RowForeColour.Add : ToolTip1.SetToolTip(btnDelete, My.Resources.ToolTips.btnDeleteRemove)
                    Case Else : ToolTip1.SetToolTip(btnDelete, My.Resources.ToolTips.btnDeleteSet)
                End Select
            End If

        Catch ex As Exception
            _DisplayError(ex, lblTitle.Text)
        End Try

    End Sub

    Private Sub spd_CellClick(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.CellClickEventArgs)

        'if header click or total row/column then exit 
        If e.RowHeader Or e.ColumnHeader Then Exit Sub
        Dim sheet As SheetView = CType(sender, FpSpread).ActiveSheet
        If sheet.Cells(e.Row, e.Column).Value Is Nothing Then Exit Sub

        'get view col and record
        Dim viewConfig As cViewerConfig = CType(sheet.Tag, cViewerConfig)
        Dim viewCol As cViewerColumns = Nothing
        Dim record As cBaseClass = Nothing
        Select Case viewConfig.IsHorizontal.Value
            Case True
                viewCol = CType(sheet.Columns(e.Column).Tag, cViewerColumns)
                record = CType(sheet.Rows(e.Row).Tag, cBaseClass)
            Case False
                record = CType(sheet.Columns(e.Column).Tag, cBaseClass)
                viewCol = CType(sheet.Rows(e.Row).Tag, cViewerColumns)
        End Select


        'more checking
        If viewCol.HyperLink.Value = String.Empty Then Exit Sub
        If sheet.Cells(e.Row, e.Column).Locked Then
        Else
            If viewConfig.AllowUpdate.Value Then
                If e.Button <> Windows.Forms.MouseButtons.Right Then Exit Sub
            End If
        End If


        'get hyperlink string
        Dim hyperlink As New StringBuilder
        For Each link As String In viewCol.HyperLink.Value.Split(";"c)
            'get value for any question marks
            For Each param As String In link.Split("|"c)
                hyperlink.Append(param & "|")

                If param.Contains("?") Then
                    For Each boField As IColField In record.BOFields
                        If param.StartsWith(boField.ColumnName) Then
                            hyperlink.Replace("?", CStr(boField.UntypedValue))
                            Exit For
                        End If
                    Next
                End If
            Next

            hyperlink.Remove(hyperlink.Length - 1, 1)
            hyperlink.Append(";")
        Next
        hyperlink.Remove(hyperlink.Length - 1, 1)


        'detect type of hyperlink
        Select Case viewCol.HyperLinkTypeValue
            Case cViewerColumns.HyperLinkTypes.New
                For Each link As String In hyperlink.ToString.Split(";"c)
                    Dim configID As Integer = 0
                    Dim configParams As New ArrayList

                    For Each s As String In link.Split("|"c)
                        If s.StartsWith("CONFIG=") Then
                            If Not Integer.TryParse(s.Substring(7).Trim, configID) Then

                            End If
                            Exit For
                        End If
                    Next

                    'load viewer config from id
                    Dim viewConfigToLoad As New cViewerConfig(_Oasys3DB)
                    viewConfigToLoad.Load(configID)

                    'get menuConfig to load - hardcoded to 101
                    Dim menuConfig As New BOSecurityProfile.cMenuConfig(_Oasys3DB)
                    menuConfig.Load(101)
                    menuConfig.Parameters.Value = link

                    Dim parameterDocument As StandardClasses.ParameterDocument
                    parameterDocument = StandardClasses.StandardApp.StandardParametersDocument(CShort(_UserID), CShort(_WorkstationID), menuConfig.SecurityLevel)
                    parameterDocument.AddItem("AppName", viewConfigToLoad.Title.Value.Trim)
                    parameterDocument.AddItem("MenuID", menuConfig.ID.Value.ToString)
                    parameterDocument.AddItem("ConnectionString", _Oasys3DB.ConnectionString)
                    parameterDocument.AddItem("RunParameters", menuConfig.Parameters.Value & Space(1))

                    Dim oasysApp As New OasysCTS.App(menuConfig.ID.Value, _UserID, _WorkstationID, menuConfig.SecurityLevel)
                    oasysApp.Show(parameterDocument, Me.ParentForm.FindForm.MdiParent)
                Next

            Case cViewerColumns.HyperLinkTypes.Split
                For Each link As String In hyperlink.ToString.Split(";"c)
                    Dim enq As New CtsControls.Enquiry(_DisplayStatus, _DisplayError, _DisplayOasys, link, _UserID, _WorkstationID, True, viewConfig.Title.Value)
                    Me.Parent.Controls.Add(enq)
                Next

            Case cViewerColumns.HyperLinkTypes.Tab
                Dim enq As New CtsControls.Enquiry(_DisplayStatus, _DisplayError, _DisplayOasys, hyperlink.ToString, _UserID, _WorkstationID, True, viewConfig.Title.Value)
                Me.Parent.Controls.Add(enq)
        End Select

    End Sub

    Private Sub spd_TextTipFetch(ByVal sender As Object, ByVal e As FarPoint.Win.Spread.TextTipFetchEventArgs)

        'check whether hyperlink cell
        Dim spread As FpSpread = CType(sender, FpSpread)
        Dim sheet As SheetView = spread.ActiveSheet
        Dim viewConfig As cViewerConfig = CType(sheet.Tag, cViewerConfig)

        'check if row/column is underlined (as set at this level)
        If sheet.Cells(e.Row, e.Column).Locked Then
        Else
            If viewConfig.AllowUpdate.Value Then
                Dim underlined As Boolean = False
                Select Case viewConfig.IsHorizontal.Value
                    Case True
                        If sheet.Columns(e.Column).Font Is Nothing Then Exit Select
                        If sheet.Columns(e.Column).Font.Underline Then underlined = True
                    Case False
                        If sheet.Rows(e.Row).Font Is Nothing Then Exit Select
                        If sheet.Rows(e.Row).Font.Underline Then underlined = True
                End Select

                If underlined Then
                    e.TipText = My.Resources.ToolTips.spdRightClick
                    e.ShowTip = True
                End If
            End If
        End If

    End Sub


    Private Function CheckForDuplicates() As Boolean

        Try
            _DisplayStatus(String.Empty)

            Dim sheet As SheetView = spd.ActiveSheet
            Dim viewConfig As cViewerConfig = CType(sheet.Tag, cViewerConfig)


            'check for any constraints for each viewcol
            For Each viewCol As cViewerColumns In viewConfig.Columns.Where(Function(v As cViewerColumns) v.BOColumnConstraints.Count > 0)
                For Each bocolumns As Object() In viewCol.BOColumnConstraints

                    'get row/column indexes of these fields
                    Dim indexes As New List(Of Integer)
                    For Each bocolumn As IColField In bocolumns
                        indexes.Add(viewConfig.Column(bocolumn.ColumnName.ToString).Index)
                    Next

                    'get values from sheet and insert into list to check for duplicates
                    Dim values As New List(Of Object())
                    Select Case viewConfig.IsHorizontal.Value
                        Case True
                            For rowIndex As Integer = 0 To sheet.RowCount - 1
                                Dim rowValues(indexes.Count - 1) As Object
                                For index As Integer = 0 To indexes.Count - 1
                                    rowValues(index) = sheet.Cells(rowIndex, indexes(index)).Value
                                Next

                                'check if already exists
                                For valuesIndex As Integer = 0 To values.Count - 1
                                    Dim ok As Boolean = False
                                    For index As Integer = 0 To indexes.Count - 1
                                        If Not Equals(values(valuesIndex)(index), rowValues(index)) Then ok = True
                                    Next

                                    'if duplicate found then highlight cells
                                    If Not ok Then
                                        For index As Integer = 0 To indexes.Count - 1
                                            sheet.Cells(valuesIndex, indexes(index)).BackColor = _RowBackColour.Error
                                            sheet.Cells(rowIndex, indexes(index)).BackColor = _RowBackColour.Error
                                        Next

                                        _DisplayStatus(My.Resources.Messages.DuplicateFound)
                                        Return False
                                    Else
                                        For index As Integer = 0 To indexes.Count - 1
                                            sheet.Cells(valuesIndex, indexes(index)).BackColor = _RowBackColour.Normal
                                            sheet.Cells(rowIndex, indexes(index)).BackColor = _RowBackColour.Normal
                                        Next

                                    End If
                                Next

                                values.Add(rowValues)
                            Next

                        Case False
                            For colIndex As Integer = 0 To sheet.ColumnCount - 1
                                Dim rowValues(indexes.Count - 1) As Object
                                For index As Integer = 0 To indexes.Count - 1
                                    rowValues(index) = sheet.Cells(indexes(index), colIndex).Value
                                Next

                                'check if already exists
                                For valuesIndex As Integer = 0 To values.Count - 1
                                    Dim ok As Boolean = False
                                    For index As Integer = 0 To indexes.Count - 1
                                        If Not Equals(values(valuesIndex)(index), rowValues(index)) Then ok = True
                                    Next

                                    'if duplicate found then highlight cells
                                    If Not ok Then
                                        For index As Integer = 0 To indexes.Count - 1
                                            sheet.Cells(indexes(index), valuesIndex).BackColor = _RowBackColour.Error
                                            sheet.Cells(indexes(index), colIndex).BackColor = _RowBackColour.Error
                                        Next

                                        _DisplayStatus(My.Resources.Messages.DuplicateFound)
                                        Return False
                                    Else
                                        For index As Integer = 0 To indexes.Count - 1
                                            sheet.Cells(indexes(index), valuesIndex).BackColor = _RowBackColour.Error
                                            sheet.Cells(indexes(index), colIndex).BackColor = _RowBackColour.Error
                                        Next

                                    End If
                                Next

                                values.Add(rowValues)
                            Next
                    End Select
                Next
            Next

            Return True

        Catch ex As Exception
            _DisplayError(ex, lblTitle.Text)
        End Try

    End Function


    Private Sub ApplyTotals(ByRef viewSort As cViewerSort)

        Try
            'loop over all sort columns in reverse order
            For index As Integer = viewSort.SortCols.Count - 1 To 0 Step -1
                ApplyTotalsSortCol(viewSort.SortCols(index))
            Next

            'insert grand totals if exist
            If viewSort.SortColGrand.ID.Value <> 0 Then
                ApplyTotalsInsertTotalsGrand(viewSort.SortColGrand)
            End If

        Catch ex As Exception
            _DisplayError(ex, lblTitle.Text)
        End Try

    End Sub

    Private Sub ApplyTotalsSortCol(ByVal sortCol As cViewerSortColumns)

        Try
            'get initial value for sort column
            Dim sheet As SheetView = spd.ActiveSheet
            Dim viewConfig As cViewerConfig = CType(sheet.Tag, cViewerConfig)
            Dim sortValue As Object = Nothing

            Select Case viewConfig.IsHorizontal.Value
                Case True : sortValue = sheet.Cells(0, viewConfig.Column(sortCol.ID.Value).Index).Value
                Case False : sortValue = sheet.Cells(viewConfig.Column(sortCol.ID.Value).Index, 0).Value
            End Select


            'initialise and loop over row/columns in sheet
            Dim sortTypes() As String = {"SUM", "COUNT", "MINIMUM", "MAXIMUM", "AVERAGE"}
            Dim indexSort As Integer = viewConfig.Column(sortCol.ID.Value).Index
            Dim index As Integer = 0

            Do
                'ignore any total row/columns
                Dim totalRow As Boolean = False
                Select Case viewConfig.IsHorizontal.Value
                    Case True : If sheet.Rows(index).BackColor = _RowBackColour.Total Then totalRow = True
                    Case False : If sheet.Columns(index).BackColor = _RowBackColour.Total Then totalRow = True
                End Select

                If totalRow Then
                    index += 1
                    Select Case viewConfig.IsHorizontal.Value
                        Case True : If index = sheet.RowCount Then Exit Do
                        Case False : If index = sheet.ColumnCount Then Exit Do
                    End Select
                    Continue Do
                End If

                'get value from current row/column for this sort column
                Dim currentValue As Object = Nothing
                Select Case viewConfig.IsHorizontal.Value
                    Case True : currentValue = sheet.Cells(index, indexSort).Value
                    Case False : currentValue = sheet.Cells(indexSort, index).Value
                End Select

                'check if this value is different to its sortValue and insert total rows if true
                If Not Equals(sortValue, currentValue) Then
                    sortValue = currentValue
                    ApplyTotalsInsertTotals(sortCol, index, indexSort, sortTypes)
                End If

                'loop over sort columns
                For sortTypeIndex As Integer = 0 To sortTypes.Length - 1
                    Dim colIDs As String = String.Empty
                    Select Case sortTypeIndex
                        Case 0 : colIDs = sortCol.SumIDs.Value
                        Case 1 : colIDs = sortCol.CountIDs.Value
                        Case 2 : colIDs = sortCol.MinIDs.Value
                        Case 3 : colIDs = sortCol.MaxIDs.Value
                        Case 4 : colIDs = sortCol.AveIDs.Value
                    End Select

                    'increment total counters (for sum/count/min/max/ave)
                    For Each colID As String In colIDs.Split(","c)
                        Dim ID As Integer = 0
                        If Not Integer.TryParse(colID, ID) Then Continue For

                        Dim totalViewCol As cViewerColumns = viewConfig.Column(ID)
                        Dim totalValue As Object = Nothing
                        Select Case viewConfig.IsHorizontal.Value
                            Case True : totalValue = sheet.Cells(index, totalViewCol.Index).Value
                            Case False : totalValue = sheet.Cells(totalViewCol.Index, index).Value
                        End Select

                        Select Case sortTypeIndex
                            Case 0 : totalViewCol.Sum += CDec(totalValue)
                            Case 1 : totalViewCol.Count += 1
                            Case 2 : totalViewCol.Min = CDec(totalValue)
                            Case 3 : totalViewCol.Max = CDec(totalValue)
                            Case 4 : totalViewCol.AveValues.Add(CDec(totalValue))
                        End Select
                    Next

                Next

                'increment row/col index and perform loop exit check
                index += 1
                Select Case viewConfig.IsHorizontal.Value
                    Case True : If index = sheet.RowCount Then Exit Do
                    Case False : If index = sheet.ColumnCount Then Exit Do
                End Select
            Loop

            'insert last totals
            Select Case viewConfig.IsHorizontal.Value
                Case True : index = sheet.RowCount
                Case False : index = sheet.ColumnCount
            End Select
            ApplyTotalsInsertTotals(sortCol, index, indexSort, sortTypes)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub ApplyTotalsInsertTotals(ByRef sortCol As cViewerSortColumns, ByRef index As Integer, ByVal indexSort As Integer, ByVal sortTypes() As String)

        Try
            Dim sheet As SheetView = spd.ActiveSheet
            Dim viewConfig As cViewerConfig = CType(sheet.Tag, cViewerConfig)

            Dim addedBorder As Boolean = False

            'loop over all total types (ie sum/count/max/min/ave)
            For sortTypeIndex As Integer = 0 To sortTypes.Length - 1
                Dim added As Boolean = False

                'get indexes of columns to total as comma-separated string
                Dim colIDs As String = String.Empty
                Select Case sortTypeIndex
                    Case 0 : colIDs = sortCol.SumIDs.Value
                    Case 1 : colIDs = sortCol.CountIDs.Value
                    Case 2 : colIDs = sortCol.MinIDs.Value
                    Case 3 : colIDs = sortCol.MaxIDs.Value
                    Case 4 : colIDs = sortCol.AveIDs.Value
                End Select

                'loop over each column index in string
                For Each colID As String In colIDs.Split(","c)
                    Dim ID As Integer = 0
                    If Not Integer.TryParse(colID, ID) Then Continue For
                    Dim totalViewCol As cViewerColumns = viewConfig.Column(ID)

                    'insert row if not already there and format row and insert total header
                    If Not added Then
                        Select Case viewConfig.IsHorizontal.Value
                            Case True
                                sheet.Rows.Add(index, 1)
                                sheet.Rows(index).BackColor = _RowBackColour.Total
                                sheet.Rows(index).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8, Drawing.FontStyle.Bold)
                                sheet.Rows(index).CellType = New CellType.TextCellType
                                sheet.Rows(index).Locked = True
                                sheet.Cells(index, indexSort).Value = sortTypes(sortTypeIndex)

                                If Not addedBorder Then
                                    sheet.Rows(index).Border = New LineBorder(Drawing.Color.Black, 1, False, True, False, False)
                                    addedBorder = True
                                End If

                            Case False
                                sheet.Columns.Add(index, 1)
                                sheet.Columns(index).BackColor = _RowBackColour.Total
                                sheet.Columns(index).Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 8, Drawing.FontStyle.Bold)
                                sheet.Columns(index).CellType = New CellType.TextCellType
                                sheet.Columns(index).Locked = True
                                sheet.Cells(indexSort, index).Value = sortTypes(sortTypeIndex)

                                If Not addedBorder Then
                                    sheet.Columns(index).Border = New LineBorder(Drawing.Color.Black, 1, False, True, False, False)
                                    addedBorder = True
                                End If
                        End Select

                        added = True
                    End If

                    'insert total value 
                    Dim totalValue As Object = Nothing
                    Select Case sortTypeIndex
                        Case 0 : totalValue = totalViewCol.Sum
                        Case 1 : totalValue = totalViewCol.Count
                        Case 2 : totalValue = totalViewCol.Min
                        Case 3 : totalValue = totalViewCol.Max
                        Case 4 : totalValue = totalViewCol.Average
                    End Select

                    'set cell type of count to integer
                    If sortTypeIndex = 1 Then
                        Dim typeInt As New CellType.NumberCellType
                        typeInt.DecimalPlaces = 0
                        Select Case viewConfig.IsHorizontal.Value
                            Case True : sheet.Cells(index, totalViewCol.Index).CellType = typeInt
                            Case False : sheet.Cells(totalViewCol.Index, index).CellType = typeInt
                        End Select
                    End If

                    Select Case viewConfig.IsHorizontal.Value
                        Case True : sheet.Cells(index, totalViewCol.Index).Value = totalValue
                        Case False : sheet.Cells(totalViewCol.Index, index).Value = totalValue
                    End Select

                    Select Case sortTypeIndex
                        Case 0 : totalViewCol.Sum = 0
                        Case 1 : totalViewCol.Count = 0
                        Case 2 : totalViewCol.Min = Nothing
                        Case 3 : totalViewCol.Max = Nothing
                        Case 4 : totalViewCol.AveValues.Clear()
                    End Select
                Next

                If added Then index += 1
            Next

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub ApplyTotalsInsertTotalsGrand(ByRef sortCol As cViewerSortColumns)

        Try
            Dim sheet As SheetView = spd.ActiveSheet
            Dim viewConfig As cViewerConfig = CType(sheet.Tag, cViewerConfig)

            Dim sortTypes() As String = {"GRAND SUM", "GRAND COUNT", "MINIMUM", "MAXIMUM", "AVERAGE"}
            Dim addedBorder As Boolean = False
            Dim countMax As Integer = 0
            Select Case viewConfig.IsHorizontal.Value
                Case True : countMax = sheet.RowCount
                Case False : countMax = sheet.ColumnCount
            End Select

            'check that there are any rows/columns to sort
            If countMax = 0 Then Exit Sub
            For index As Integer = 0 To countMax - 1

                'ignore any total row/columns
                Select Case viewConfig.IsHorizontal.Value
                    Case True : If sheet.Rows(index).BackColor = Drawing.Color.LightGray Then Continue For
                    Case False : If sheet.Columns(index).BackColor = Drawing.Color.LightGray Then Continue For
                End Select

                For sortTypeIndex As Integer = 0 To sortTypes.Length - 1
                    Dim colIDs As String = String.Empty
                    Select Case sortTypeIndex
                        Case 0 : colIDs = sortCol.SumIDs.Value
                        Case 1 : colIDs = sortCol.CountIDs.Value
                        Case 2 : colIDs = sortCol.MinIDs.Value
                        Case 3 : colIDs = sortCol.MaxIDs.Value
                        Case 4 : colIDs = sortCol.AveIDs.Value
                    End Select

                    'increment total counters (for sum/count/min/max/ave)
                    For Each colID As String In colIDs.Split(","c)
                        Dim ID As Integer = 0
                        If Not Integer.TryParse(colID, ID) Then Continue For

                        Dim totalViewCol As cViewerColumns = viewConfig.Column(ID)
                        Dim totalValue As Object = Nothing
                        Select Case viewConfig.IsHorizontal.Value
                            Case True : totalValue = sheet.Cells(index, totalViewCol.Index).Value
                            Case False : totalValue = sheet.Cells(totalViewCol.Index, index).Value
                        End Select

                        Select Case sortTypeIndex
                            Case 0 : totalViewCol.Sum += CDec(totalValue)
                            Case 1 : totalViewCol.Count += 1
                            Case 2 : totalViewCol.Min = CDec(totalValue)
                            Case 3 : totalViewCol.Max = CDec(totalValue)
                            Case 4 : totalViewCol.AveValues.Add(CDec(totalValue))
                        End Select
                    Next
                Next
            Next

            'insert grand totals into sheet
            ApplyTotalsInsertTotals(sortCol, countMax, 0, sortTypes)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub




    'Private Sub lblTitle_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lblTitle.MouseMove

    '    Try
    '        If e.Button = Windows.Forms.MouseButtons.Left Then
    '            'move control
    '            BringToFront()
    '            If _MouseRelative = Nothing Then _MouseRelative = New Point(e.X, e.Y)
    '            Me.Left += e.X - _MouseRelative.X
    '            Me.Top += e.Y - _MouseRelative.Y
    '        Else
    '            'set mouse position and cursor
    '            _MouseRelative = Nothing
    '            Cursor = Cursors.SizeAll
    '        End If

    '    Catch ex As Exception
    '        Trace.WriteLine(ex.Message, Name)
    '    End Try

    'End Sub

    'Private Sub pnlControlBox_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pnlControlBox.MouseMove

    '    Try
    '        If e.Button = Windows.Forms.MouseButtons.Left Then
    '            'resize control
    '            BringToFront()
    '            Select Case Cursor
    '                Case Cursors.SizeNS
    '                    Me.Top += e.Y
    '                    Me.Height -= e.Y

    '                Case Cursors.SizeWE
    '                    Select Case _MousePosition
    '                        Case MousePositions.Right : Me.Width = e.X
    '                        Case MousePositions.Left
    '                            Me.Left += e.X
    '                            Me.Width -= e.X
    '                    End Select

    '                Case Cursors.SizeNESW
    '                    Me.Top += e.Y
    '                    Me.Height -= e.Y
    '                    Me.Width = e.X

    '                Case Cursors.SizeNWSE
    '                    Me.Top += e.Y
    '                    Me.Height -= e.Y
    '                    Me.Left += e.X
    '                    Me.Width -= e.X
    '            End Select
    '            Refresh()

    '        Else
    '            'set mouse position and cursor
    '            Select Case e.Y
    '                Case Is < 4
    '                    Select Case e.X
    '                        Case Is < 4 : _MousePosition = MousePositions.CornerNW
    '                        Case Is > pnlControlBox.Width - 4 : _MousePosition = MousePositions.CornerNE
    '                        Case Else : _MousePosition = MousePositions.Bottom
    '                    End Select

    '                Case Else
    '                    Select Case e.X
    '                        Case Is < 4 : _MousePosition = MousePositions.Left
    '                        Case Is > pnlControlBox.Width - 4 : _MousePosition = MousePositions.Right
    '                        Case Else : _MousePosition = MousePositions.None
    '                    End Select
    '            End Select

    '            'set cursor type
    '            Select Case _MousePosition
    '                Case MousePositions.Bottom, MousePositions.Top : Cursor = Cursors.SizeNS
    '                Case MousePositions.Left, MousePositions.Right : Cursor = Cursors.SizeWE
    '                Case MousePositions.CornerNW : Cursor = Cursors.SizeNWSE
    '                Case MousePositions.CornerNE : Cursor = Cursors.SizeNESW
    '                Case Else : Cursor = Cursors.Default
    '            End Select
    '        End If

    '    Catch ex As Exception
    '        Trace.WriteLine(ex.Message, Name)
    '    End Try

    'End Sub

    'Private Sub pnlSpread_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pnlSpread.MouseMove

    '    Try
    '        If e.Button = Windows.Forms.MouseButtons.Left Then
    '            'resize control
    '            BringToFront()
    '            Select Case Cursor
    '                Case Cursors.SizeNS : Me.Height = e.Y + (Me.Height - pnlSpread.Height)
    '                Case Cursors.SizeWE
    '                    Select Case _MousePosition
    '                        Case MousePositions.Right : Me.Width = e.X
    '                        Case MousePositions.Left
    '                            Me.Left += e.X
    '                            Me.Width -= e.X
    '                    End Select

    '                Case Cursors.SizeNWSE
    '                    Me.Height = e.Y + (Me.Height - pnlSpread.Height)
    '                    Me.Width = e.X

    '                Case Cursors.SizeNESW
    '                    Me.Height = e.Y + (Me.Height - pnlSpread.Height)
    '                    Me.Left += e.X
    '                    Me.Width -= e.X
    '            End Select
    '            Refresh()

    '        Else
    '            'set mouse position and cursor
    '            Select Case e.Y
    '                Case Is > pnlSpread.Height - 4
    '                    Select Case e.X
    '                        Case Is < 4 : _MousePosition = MousePositions.CornerSW
    '                        Case Is > pnlSpread.Width - 4 : _MousePosition = MousePositions.CornerSE
    '                        Case Else : _MousePosition = MousePositions.Bottom
    '                    End Select

    '                Case Else
    '                    Select Case e.X
    '                        Case Is < 4 : _MousePosition = MousePositions.Left
    '                        Case Is > pnlSpread.Width - 4 : _MousePosition = MousePositions.Right
    '                        Case Else : _MousePosition = MousePositions.None
    '                    End Select
    '            End Select

    '            'set cursor type
    '            Select Case _MousePosition
    '                Case MousePositions.Bottom, MousePositions.Top : Cursor = Cursors.SizeNS
    '                Case MousePositions.Left, MousePositions.Right : Cursor = Cursors.SizeWE
    '                Case MousePositions.CornerSW : Cursor = Cursors.SizeNESW
    '                Case MousePositions.CornerSE : Cursor = Cursors.SizeNWSE
    '                Case Else : Cursor = Cursors.Default
    '            End Select
    '        End If

    '    Catch ex As Exception
    '        Trace.WriteLine(ex.Message, Name)
    '    End Try

    'End Sub

    'Private Sub btn_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnRefresh.MouseMove, btnPrint.MouseMove, btnExit.MouseMove, btnAdd.MouseMove, btnDelete.MouseMove, btnSave.MouseMove
    '    Cursor = Cursors.Default
    'End Sub



    Private Sub Selections_Initialise(ByVal viewConfig As cViewerConfig)

        Try
            'setup selection criteria
            For Each viewCol As cViewerColumns In viewConfig.Columns
                If viewCol.SelectionTypeValue = cViewerColumns.SelectionTypes.None Then Continue For
                Dim selection As New EnquirySelection(_Oasys3DB, viewConfig, viewCol)
                _Selections.Add(selection)
            Next

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub ddbTabs_Initialise(ByVal viewConfig As cViewerConfig)

        Try
            If viewConfig.Tabs.Count = 0 Then
                Exit Sub
            End If

            Dim menu As New DXPopupMenu
            For Each viewTab As cViewerTabs In viewConfig.Tabs
                Dim menuItem As New DXMenuItem
                menuItem.Caption = viewTab.Description.Value.Trim
                menuItem.Tag = viewTab.ID.Value
                menuItem.Image = Nothing
                AddHandler menuItem.Click, AddressOf ddbTabs_Click
                menu.Items.Add(menuItem)
            Next

            Select Case menu.Items.Count
                Case Is > 1
                    ddbTabs_Click(menu.Items(0), New EventArgs)

                    Dim ddb As New DevExpress.XtraEditors.DropDownButton
                    pnlControlBox.Controls.Add(ddb)
                    ddb.DropDownControl = menu
                    ddb.Text = "views"
                    ddb.Appearance.Font = New System.Drawing.Font("Tahoma", 7.5!)
                    ddb.Location = New Point(346, 3)
                    ddb.Size = New Size(76, 20)
                    ddb.Anchor = AnchorStyles.Top Or AnchorStyles.Right
                    ddb.Visible = (spd.Sheets.Count = 1)
                    ddb.Tag = spd.Sheets.Count - 1
                    ddb.BringToFront()

                Case Is = 1
                    spd.ActiveSheet = spd.Sheets(spd.Sheets.Count - 1)
                    ddbTabs_Click(menu.Items(0), New EventArgs)
            End Select

        Catch ex As OasysDbException
            _DisplayOasys(ex, lblTitle.Text)
        Catch ex As Exception
            _DisplayError(ex, lblTitle.Text)
        End Try

    End Sub

    Private Sub ddbTabs_Click(ByVal sender As Object, ByVal e As EventArgs)

        Try
            Dim menuItem As DXMenuItem = CType(sender, DXMenuItem)
            If menuItem.Image Is Nothing Then
                'reset all other images in popup menu items and set this item to ticked
                For Each item As DXMenuItem In menuItem.Collection
                    item.Image = Nothing
                Next
                menuItem.Image = My.Resources.Ok.ToBitmap

                'apply tab selection
                Dim tabID As String = menuItem.Tag.ToString
                Dim sheet As SheetView = spd.ActiveSheet
                Dim viewConfig As cViewerConfig = CType(sheet.Tag, cViewerConfig)

                'get sheet row/col count
                Dim indexMax As Integer = 0
                Select Case viewConfig.IsHorizontal.Value
                    Case True : indexMax = sheet.ColumnCount
                    Case False : indexMax = sheet.RowCount
                End Select

                'loop through sheet row/col and get viewer config
                Dim column As BOViewer.cViewerColumns = Nothing
                For index As Integer = 0 To indexMax - 1

                    'get viewer column for config for this row/column
                    Select Case viewConfig.IsHorizontal.Value
                        Case True : column = CType(sheet.Columns(index).Tag, cViewerColumns)
                        Case False : column = CType(sheet.Rows(index).Tag, cViewerColumns)
                    End Select

                    If column Is Nothing Then
                        MessageBox.Show(My.Resources.Errors.NoViewCol, lblTitle.Text)
                        Exit Sub
                    End If

                    'check if tab ID is in viewer column and set visibilty accordingly
                    Select Case viewConfig.IsHorizontal.Value
                        Case True : sheet.Columns(index).Visible = column.TabIDs.Value.Contains(tabID)
                        Case False : sheet.Rows(index).Visible = column.TabIDs.Value.Contains(tabID)
                    End Select
                Next
                Exit Sub
            End If

        Catch ex As OasysDbException
            _DisplayOasys(ex, lblTitle.Text)
        Catch ex As Exception
            _DisplayError(ex, lblTitle.Text)
        End Try

    End Sub

    Private Sub ddbSorting_Initialise(ByVal viewConfig As cViewerConfig)

        Try
            If viewConfig.Sorts.Count = 0 Then
                Exit Sub
            End If

            If viewConfig.IsHorizontal.Value = False Then
                Exit Sub
            End If

            Dim menu As New DXPopupMenu
            For index As Integer = 0 To viewConfig.Sorts.Count - 1
                Dim menuItem As New DXMenuItem
                menuItem.Caption = viewConfig.Sorts(index).Description.Value.Trim
                menuItem.Tag = viewConfig.Sorts(index).ID.Value
                menuItem.Image = Nothing
                If index = 0 Then menuItem.Image = My.Resources.Ok.ToBitmap
                AddHandler menuItem.Click, AddressOf ddbSorting_Click
                menu.Items.Add(menuItem)
            Next

            Dim ddb As New DevExpress.XtraEditors.DropDownButton
            pnlControlBox.Controls.Add(ddb)
            ddb.DropDownControl = menu
            ddb.Text = "sorting"
            ddb.Appearance.Font = New System.Drawing.Font("Tahoma", 7.5!)
            ddb.Location = New Point(424, 3)
            ddb.Size = New Size(76, 20)
            ddb.Anchor = AnchorStyles.Top Or AnchorStyles.Right
            ddb.Visible = (spd.Sheets.Count = 1 AndAlso menu.Items.Count > 1)
            ddb.Tag = spd.Sheets.Count - 1
            ddb.BringToFront()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ddbSorting_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim menuItem As DXMenuItem = CType(sender, DXMenuItem)

            'reset all other images in popup menu items and set this item to ticked
            For Each item As DXMenuItem In menuItem.Collection
                item.Image = Nothing
            Next
            menuItem.Image = My.Resources.Ok.ToBitmap

            Dim sheet As SheetView = spd.ActiveSheet
            Dim viewConfig As cViewerConfig = CType(sheet.Tag, cViewerConfig)

            'perform sort on spread sheet
            Dim sortID As Integer = CInt(menuItem.Tag)
            Dim viewSort As BOViewer.cViewerSort = viewConfig.Sort(sortID)

            If viewSort.SortCols.Count = 0 Then
                MessageBox.Show(My.Resources.Errors.NoSorting & sortID, lblTitle.Text)
                Exit Sub
            End If

            'get row and coluumn counts for loops
            Dim indexMax As Integer = 0
            Dim countMax As Integer = 0
            Select Case viewConfig.IsHorizontal.Value
                Case True
                    indexMax = sheet.ColumnCount
                    countMax = sheet.RowCount
                Case False
                    indexMax = sheet.RowCount
                    countMax = sheet.ColumnCount
            End Select

            'check that there are any rows/columns to sort
            If countMax = 0 Then Exit Sub

            'get rid of any existing total row/columns
            For index As Integer = countMax - 1 To 0 Step -1
                Select Case viewConfig.IsHorizontal.Value
                    Case True
                        If sheet.Rows(index).BackColor = _RowBackColour.Total Then
                            sheet.Rows.Remove(index, 1)
                            countMax -= 1
                        End If

                    Case False
                        If sheet.Columns(index).BackColor = _RowBackColour.Total Then
                            sheet.Columns.Remove(index, 1)
                            countMax -= 1
                        End If
                End Select
            Next

            'get row/col index to sort given col id
            Dim sort(viewSort.SortCols.Count - 1) As SortInfo
            For Each sortCol As BOViewer.cViewerSortColumns In viewSort.SortCols
                Dim viewCol As BOViewer.cViewerColumns = viewConfig.Column(sortCol.ID.Value)
                sort(viewSort.SortCols.IndexOf(sortCol)) = New SortInfo(viewCol.Index, sortCol.Ascending.Value)
            Next

            'apply sort
            Select Case viewConfig.IsHorizontal.Value
                Case True : sheet.SortRows(0, countMax, sort)
                Case False : sheet.SortColumns(0, countMax, sort)
            End Select

            'apply totals
            ApplyTotals(viewSort)

        Catch ex As OasysDbException
            _DisplayOasys(ex, lblTitle.Text)
        Catch ex As Exception
            _DisplayError(ex, lblTitle.Text)
        End Try

    End Sub



    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        Try
            Dim sheet As SheetView = spd.ActiveSheet
            Dim viewConfig As cViewerConfig = CType(sheet.Tag, cViewerConfig)

            Dim strClass As String = viewConfig.BOName.Value.Trim
            Dim strFile As String = strClass.Substring(0, strClass.IndexOf(".") + 1) & "dll"

            'do checks
            spd_SelectionChanged(spd, Nothing)

            Dim record As OasysDBBO.cBaseClass = CType(Activator.CreateInstanceFrom(strFile, strClass, True, Reflection.BindingFlags.Default, Nothing, New Object() {_Oasys3DB}, System.Globalization.CultureInfo.CurrentCulture, Nothing, Nothing).Unwrap, OasysDBBO.cBaseClass)
            Select Case viewConfig.IsHorizontal.Value
                Case True
                    sheet.Rows.Add(sheet.RowCount, 1)
                    sheet.Rows(sheet.RowCount - 1).Tag = record
                    sheet.Rows(sheet.RowCount - 1).ForeColor = _RowForeColour.Add

                    'loop through rows/columns in the sheet
                    For index As Integer = 0 To sheet.ColumnCount - 1
                        Dim viewCol As cViewerColumns = CType(sheet.Columns(index).Tag, cViewerColumns)
                        sheet.Cells(sheet.RowCount - 1, index).Value = record.GetProperty(viewCol.ColumnName.Value).UntypedValue
                    Next

                    'check if first row
                    If sheet.RowCount = 1 Then
                        sheet.ColumnHeaderVisible = True
                        spd_Resize(spd, New EventArgs, True)
                    End If

                    'set selection and scroll to row
                    sheet.SetActiveCell(sheet.RowCount - 1, 0)
                    sheet.ClearSelection()
                    sheet.AddSelection(sheet.RowCount - 1, 0, 1, sheet.ColumnCount)
                    spd.SetViewportTopRow(spd.ActiveSheetIndex, 0, sheet.RowCount - 1)
                    spd.Focus()

                Case False
                    sheet.Columns.Add(sheet.ColumnCount, 1)
                    sheet.Columns(sheet.ColumnCount - 1).Tag = record
                    sheet.Columns(sheet.ColumnCount - 1).ForeColor = _RowForeColour.Add

                    'loop through rows/columns in the sheet
                    For index As Integer = 0 To sheet.RowCount - 1
                        Dim viewCol As cViewerColumns = CType(sheet.Columns(index).Tag, cViewerColumns)
                        sheet.Cells(index, sheet.ColumnCount - 1).Value = record.GetProperty(viewCol.ColumnName.Value).UntypedValue
                    Next

                    'check if first column
                    If sheet.ColumnCount = 1 Then
                        sheet.RowHeaderVisible = True
                        spd_Resize(spd, New EventArgs, True)
                    End If

                    'set selection and scroll to column
                    sheet.SetActiveCell(0, sheet.ColumnCount - 1)
                    sheet.ClearSelection()
                    sheet.AddSelection(0, sheet.ColumnCount - 1, sheet.RowCount, 1)
                    spd.SetViewportTopRow(spd.ActiveSheetIndex, 0, sheet.ColumnCount - 1)
                    spd.Focus()
            End Select

            'set tooltips etc
            spd_SelectionChanged(sender, Nothing)

        Catch ex As Exception
            _DisplayError(ex, lblTitle.Text)
        End Try

    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Try
            Dim sheet As SheetView = spd.ActiveSheet
            Dim viewConfig As cViewerConfig = CType(sheet.Tag, cViewerConfig)
            Dim record As cBaseClass = Nothing

            'get record from row/column tag
            Select Case viewConfig.IsHorizontal.Value
                Case True : record = CType(sheet.Rows(sheet.ActiveRowIndex).Tag, cBaseClass)
                Case False : record = CType(sheet.Columns(sheet.ActiveColumnIndex).Tag, cBaseClass)
            End Select

            'check fore colour of row and hence the state of the record
            Select Case viewConfig.IsHorizontal.Value
                Case True
                    Select Case sheet.ActiveRow.ForeColor
                        Case _RowForeColour.Normal, Color.Empty : sheet.ActiveRow.ForeColor = _RowForeColour.Delete
                        Case _RowForeColour.Add : sheet.Rows.Remove(sheet.ActiveRowIndex, 1)
                        Case _RowForeColour.Delete : sheet.ActiveRow.ForeColor = _RowForeColour.Normal
                        Case _RowForeColour.ChangedDelete : sheet.ActiveRow.ForeColor = _RowForeColour.Changed
                        Case _RowForeColour.Changed : sheet.ActiveRow.ForeColor = _RowForeColour.ChangedDelete
                    End Select
                Case False
                    Select Case sheet.ActiveColumn.ForeColor
                        Case _RowForeColour.Normal, Color.Empty : sheet.ActiveColumn.ForeColor = _RowForeColour.Delete
                        Case _RowForeColour.Add : sheet.Columns.Remove(sheet.ActiveColumnIndex, 1)
                        Case _RowForeColour.Changed : sheet.ActiveColumn.ForeColor = _RowForeColour.ChangedDelete
                        Case _RowForeColour.ChangedDelete : sheet.ActiveColumn.ForeColor = _RowForeColour.Changed
                        Case _RowForeColour.Changed : sheet.ActiveColumn.ForeColor = _RowForeColour.ChangedDelete
                    End Select
            End Select

            'set tooltips etc
            spd_SelectionChanged(sender, Nothing)

        Catch ex As Exception
            _DisplayError(ex, lblTitle.Text)
        End Try

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Try
            Dim sheet As SheetView = spd.ActiveSheet
            Dim viewConfig As cViewerConfig = CType(sheet.Tag, cViewerConfig)
            Cursor = Cursors.WaitCursor
            Dim recordValidation As cBaseClass = Nothing
            Dim boolStore As Boolean



            'check for duplicates
            If CheckForDuplicates() = False Then
                Exit Sub
            End If

            'get column/row count into countMax
            Dim errorsFound As Boolean = False
            Dim countMax As Integer = 0
            Select Case viewConfig.IsHorizontal.Value
                Case True : countMax = sheet.RowCount
                Case False : countMax = sheet.ColumnCount
            End Select

            'Validate the values entered

            If DirectCast(DirectCast(sender, System.Windows.Forms.Button).TopLevelControl, System.Windows.Forms.Control).Text.ToLower.Equals("store maintenance") Then
                For index As Integer = countMax - 1 To 0 Step -1
                    recordValidation = CType(sheet.Rows(index).Tag(), cBaseClass)

                    For indexval As Integer = 0 To recordValidation.BOFields.Count - 1
                        Select Case recordValidation.BOFields(indexval).columnname

                            Case "NUMB" : If Not recordValidation.BOFields(0).UntypedValue Is Nothing Then
                                    If CStr(recordValidation.BOFields(0).UntypedValue).Length > 3 Then
                                        MessageBox.Show(My.Resources.Errors.StoreIdError, lblTitle.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                        boolStore = True
                                    End If
                                End If

                            Case "ADD1" : If Not recordValidation.BOFields(1).UntypedValue Is Nothing Then
                                    If CStr(recordValidation.BOFields(1).UntypedValue).Length > 30 Then
                                        MessageBox.Show(My.Resources.Errors.AddressLine1Error, lblTitle.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                        boolStore = True
                                    End If
                                End If
                            Case "ADD2" : If Not recordValidation.BOFields(2).UntypedValue Is Nothing Then
                                    If CStr(recordValidation.BOFields(2).UntypedValue).Length > 30 Then
                                        MessageBox.Show(My.Resources.Errors.AddressLine2Error, lblTitle.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                        boolStore = True
                                    End If
                                End If
                            Case "ADD3" : If Not recordValidation.BOFields(3).UntypedValue Is Nothing Then
                                    If CStr(recordValidation.BOFields(3).UntypedValue).Length > 30 Then
                                        MessageBox.Show(My.Resources.Errors.AddressLine3Error, lblTitle.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                        boolStore = True
                                    End If
                                End If
                            Case "ADD4" : If Not recordValidation.BOFields(4).UntypedValue Is Nothing Then
                                    If CStr(recordValidation.BOFields(4).UntypedValue).Length > 30 Then
                                        MessageBox.Show(My.Resources.Errors.AddressLine4Error, lblTitle.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                        boolStore = True
                                    End If
                                End If
                            Case "TILD" : If Not recordValidation.BOFields(5).UntypedValue Is Nothing Then
                                    If CStr(recordValidation.BOFields(5).UntypedValue).Length > 12 Then
                                        MessageBox.Show(My.Resources.Errors.StoreNameError, lblTitle.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                        boolStore = True
                                    End If
                                End If

                            Case "PHON" : If Not recordValidation.BOFields(6).UntypedValue Is Nothing Then
                                    If CStr(recordValidation.BOFields(6).UntypedValue).Length > 12 Then
                                        MessageBox.Show(My.Resources.Errors.PhoneNumberError, lblTitle.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                        boolStore = True
                                    End If
                                End If
                            Case "SFAX" : If Not recordValidation.BOFields(7).UntypedValue Is Nothing Then
                                    If CStr(recordValidation.BOFields(7).UntypedValue).Length > 12 Then
                                        MessageBox.Show(My.Resources.Errors.FaxNumberError, lblTitle.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                        boolStore = True
                                    End If
                                End If
                            Case "MANG" : If Not recordValidation.BOFields(8).UntypedValue Is Nothing Then
                                    If CStr(recordValidation.BOFields(8).UntypedValue).Length > 20 Then
                                        MessageBox.Show(My.Resources.Errors.ManagNameError, lblTitle.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                        boolStore = True
                                    End If
                                End If
                            Case "REGC" : If Not recordValidation.BOFields(9).UntypedValue Is Nothing Then
                                    If CStr(recordValidation.BOFields(9).UntypedValue).Length > 2 Then
                                        MessageBox.Show(My.Resources.Errors.RegionCodeError, lblTitle.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                        boolStore = True
                                    End If
                                End If
                            Case "CountryCode" : If Not recordValidation.BOFields(11).UntypedValue Is Nothing Then
                                    If CStr(recordValidation.BOFields(11).UntypedValue).Length > 3 Then
                                        MessageBox.Show(My.Resources.Errors.CountryCodeError, lblTitle.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                        boolStore = True
                                    End If
                                End If
                        End Select

                    Next

                Next

            End If

            If Not boolStore Then
                'do deletions first as will require to remove rows/column from sheet
                _DisplayStatus(My.Resources.Messages.DoingDeletions)
                For index As Integer = countMax - 1 To 0 Step -1
                    'get forecolour of row/column
                    Dim colour As Drawing.Color = Color.Empty
                    Select Case viewConfig.IsHorizontal.Value
                        Case True : colour = sheet.Rows(index).ForeColor
                        Case False : colour = sheet.Columns(index).ForeColor
                    End Select

                    Select Case colour
                        Case _RowForeColour.Delete, _RowForeColour.ChangedDelete
                            'get record for deletion
                            Dim record As cBaseClass = Nothing
                            Select Case viewConfig.IsHorizontal.Value
                                Case True : record = CType(sheet.Rows(index).Tag, cBaseClass)
                                Case False : record = CType(sheet.Columns(index).Tag, cBaseClass)
                            End Select

                            'if deleted then remove from collection and sheet else highlight as error
                            If record.Delete Then
                                viewConfig.BORecords.Remove(record)
                                Select Case viewConfig.IsHorizontal.Value
                                    Case True : sheet.Rows.Remove(index, 1)
                                    Case False : sheet.Columns.Remove(index, 1)
                                End Select
                            Else
                                errorsFound = True
                                Select Case viewConfig.IsHorizontal.Value
                                    Case True : sheet.Rows(index).BackColor = _RowBackColour.Error
                                    Case False : sheet.Columns(index).BackColor = _RowBackColour.Error
                                End Select

                            End If
                    End Select
                Next

                'reset row/column count
                Select Case viewConfig.IsHorizontal.Value
                    Case True : countMax = sheet.RowCount
                    Case False : countMax = sheet.ColumnCount
                End Select

                'check that any rows/cols left 
                If countMax = 0 Then
                    spd_SelectionChanged(spd, Nothing)
                    Exit Sub
                End If


                'now do updates/inserts
                _DisplayStatus(My.Resources.Messages.DoingUpdates)

                For index As Integer = 0 To countMax - 1
                    'get forecolour of row/column
                    Dim colour As Drawing.Color = Color.Empty
                    Select Case viewConfig.IsHorizontal.Value
                        Case True : colour = sheet.Rows(index).ForeColor
                        Case False : colour = sheet.Columns(index).ForeColor
                    End Select

                    Select Case colour
                        Case _RowForeColour.Add
                            'get record for addition
                            Dim record As cBaseClass = Nothing
                            Select Case viewConfig.IsHorizontal.Value
                                Case True : record = CType(sheet.Rows(index).Tag, cBaseClass)
                                Case False : record = CType(sheet.Columns(index).Tag, cBaseClass)
                            End Select

                            'if inserted then add to collection and update values else highlight as error
                            'If record.BOFields(1).VALUE.length > 12 Then
                            '    'MessageBox.Show(My.Resources.Messages.SaveWithErrors, lblTitle.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                            '    MessageBox.Show("ADD1 SHOULD BE LESS THAN 12 CHARACTER LENGTH", lblTitle.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            'End If

                            If record.SaveIfNew Then
                                viewConfig.BORecords.Add(record)
                                Select Case viewConfig.IsHorizontal.Value
                                    Case True
                                        sheet.Rows(index).ForeColor = _RowForeColour.Normal
                                        For colIndex As Integer = 0 To sheet.ColumnCount - 1
                                            Dim viewCol As cViewerColumns = CType(sheet.Columns(colIndex).Tag, cViewerColumns)
                                            sheet.Cells(index, colIndex).Value = record.GetProperty(viewCol.ColumnName.Value).UntypedValue
                                            If viewCol.BOColumn.ColumnKey Then sheet.Cells(index, colIndex).Locked = True
                                        Next

                                    Case False
                                        sheet.Columns(index).ForeColor = _RowForeColour.Normal
                                        For rowIndex As Integer = 0 To sheet.RowCount - 1
                                            Dim viewCol As cViewerColumns = CType(sheet.Columns(rowIndex).Tag, cViewerColumns)
                                            sheet.Cells(rowIndex, index).Value = record.GetProperty(viewCol.ColumnName.Value).UntypedValue
                                            If viewCol.BOColumn.ColumnKey Then sheet.Cells(rowIndex, index).Locked = True
                                        Next
                                End Select

                            Else
                                errorsFound = True
                                Select Case viewConfig.IsHorizontal.Value
                                    Case True : sheet.Rows(index).BackColor = _RowBackColour.Error
                                    Case False : sheet.Columns(index).BackColor = _RowBackColour.Error
                                End Select
                            End If


                        Case _RowForeColour.Changed
                            'get record for updates
                            Dim record As cBaseClass = Nothing
                            Select Case viewConfig.IsHorizontal.Value
                                Case True : record = CType(sheet.Rows(index).Tag, cBaseClass)
                                Case False : record = CType(sheet.Columns(index).Tag, cBaseClass)
                            End Select

                            'set where fields with original values

                            'if updated then ok else highlight as error
                            If record.SaveIfExists Then
                                Select Case viewConfig.IsHorizontal.Value
                                    Case True : sheet.Rows(index).ForeColor = _RowForeColour.Normal
                                    Case False : sheet.Columns(index).ForeColor = _RowForeColour.Normal
                                End Select

                            Else
                                errorsFound = True
                                Select Case viewConfig.IsHorizontal.Value
                                    Case True : sheet.Rows(index).BackColor = _RowBackColour.Error
                                    Case False : sheet.Columns(index).BackColor = _RowBackColour.Error
                                End Select
                            End If
                    End Select
                Next

                'set tooltips etc
                spd_SelectionChanged(sender, Nothing)

                'check whether any errors found
                If errorsFound Then
                    MessageBox.Show(My.Resources.Messages.SaveWithErrors, lblTitle.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    MessageBox.Show(My.Resources.Messages.SaveSuccessful, lblTitle.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If

        Catch ex As OasysDbException
            _DisplayOasys(ex, lblTitle.Text)
        Catch ex As Exception
            _DisplayError(ex, lblTitle.Text)
        Finally
            Cursor = Cursors.Default
            _DisplayStatus(String.Empty)
        End Try

    End Sub

    Private Sub btnExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcel.Click

        Using dialog As New SaveFileDialog
            dialog.RestoreDirectory = True
            dialog.Filter = "Excel Files (*.xls)|*.xls"
            dialog.CheckPathExists = True

            If dialog.ShowDialog(Me) = DialogResult.OK Then
                spd.SaveExcel(dialog.FileName, FarPoint.Win.Spread.Model.IncludeHeaders.BothCustomOnly)
            End If
        End Using

    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click

        Try
            If Not (spd.ActiveSheet Is Nothing) Then
                spd.ActiveSheet.Rows.Remove(0, spd.ActiveSheet.Rows.Count)
            End If

            spd_AddSheetRecords(spd.ActiveSheetIndex)
        Catch ex As OasysDbException
            _DisplayOasys(ex, lblTitle.Text)
        Catch ex As Exception
            _DisplayError(ex, lblTitle.Text)
        End Try

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Dim dialog As New PrintDialog

        Try
            Dim result As DialogResult = dialog.ShowDialog(Me)
            If result = DialogResult.OK Then
                Dim timeNow As DateTime = Now

                For Each sheet As SheetView In spd.Sheets
                    sheet.PrintInfo.Printer = dialog.PrinterSettings.PrinterName
                    sheet.PrintInfo.Footer = "/lPrinted: " & timeNow
                    sheet.PrintInfo.Footer &= "/cPage /p of /pc"
                    sheet.PrintInfo.Footer &= "/rVersion " & Application.ProductVersion
                    Select Case dialog.PrinterSettings.DefaultPageSettings.Landscape
                        Case True : sheet.PrintInfo.Orientation = PrintOrientation.Landscape
                        Case False : sheet.PrintInfo.Orientation = PrintOrientation.Portrait
                    End Select
                Next

                For index As Integer = 1 To dialog.PrinterSettings.Copies
                    Me.spd.PrintSheet(-1)
                Next
            End If

        Catch ex As Exception
            Trace.WriteLine(ex.Message, Me.FindForm.Text)
        Finally
            dialog.Dispose()
        End Try

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Dispose()
    End Sub
End Class