﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EnquirySelection
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblName = New System.Windows.Forms.Label
        Me.dtpFrom = New System.Windows.Forms.DateTimePicker
        Me.dtpTo = New System.Windows.Forms.DateTimePicker
        Me.txtFrom = New System.Windows.Forms.TextBox
        Me.txtTo = New System.Windows.Forms.TextBox
        Me.cmbFrom = New System.Windows.Forms.ComboBox
        Me.cmbTo = New System.Windows.Forms.ComboBox
        Me.cmbOperator = New System.Windows.Forms.ComboBox
        Me.chkLessThan = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(0, 0)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(165, 22)
        Me.lblName.TabIndex = 0
        Me.lblName.Text = "Label"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpFrom
        '
        Me.dtpFrom.Location = New System.Drawing.Point(317, 0)
        Me.dtpFrom.Name = "dtpFrom"
        Me.dtpFrom.Size = New System.Drawing.Size(200, 20)
        Me.dtpFrom.TabIndex = 8
        Me.dtpFrom.Visible = False
        '
        'dtpTo
        '
        Me.dtpTo.Location = New System.Drawing.Point(659, 0)
        Me.dtpTo.Name = "dtpTo"
        Me.dtpTo.Size = New System.Drawing.Size(200, 20)
        Me.dtpTo.TabIndex = 9
        Me.dtpTo.Visible = False
        '
        'txtFrom
        '
        Me.txtFrom.Location = New System.Drawing.Point(317, 0)
        Me.txtFrom.Name = "txtFrom"
        Me.txtFrom.Size = New System.Drawing.Size(200, 20)
        Me.txtFrom.TabIndex = 10
        Me.txtFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtFrom.Visible = False
        '
        'txtTo
        '
        Me.txtTo.Location = New System.Drawing.Point(659, 0)
        Me.txtTo.Name = "txtTo"
        Me.txtTo.Size = New System.Drawing.Size(200, 20)
        Me.txtTo.TabIndex = 11
        Me.txtTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTo.Visible = False
        '
        'cmbFrom
        '
        Me.cmbFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFrom.FormattingEnabled = True
        Me.cmbFrom.Location = New System.Drawing.Point(317, 0)
        Me.cmbFrom.Name = "cmbFrom"
        Me.cmbFrom.Size = New System.Drawing.Size(200, 21)
        Me.cmbFrom.TabIndex = 12
        Me.cmbFrom.Visible = False
        '
        'cmbTo
        '
        Me.cmbTo.FormattingEnabled = True
        Me.cmbTo.Location = New System.Drawing.Point(659, 0)
        Me.cmbTo.Name = "cmbTo"
        Me.cmbTo.Size = New System.Drawing.Size(200, 21)
        Me.cmbTo.TabIndex = 17
        Me.cmbTo.Visible = False
        '
        'cmbOperator
        '
        Me.cmbOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOperator.FormattingEnabled = True
        Me.cmbOperator.Location = New System.Drawing.Point(171, 0)
        Me.cmbOperator.Name = "cmbOperator"
        Me.cmbOperator.Size = New System.Drawing.Size(140, 21)
        Me.cmbOperator.TabIndex = 21
        '
        'chkLessThan
        '
        Me.chkLessThan.Location = New System.Drawing.Point(525, 0)
        Me.chkLessThan.Name = "chkLessThan"
        Me.chkLessThan.Size = New System.Drawing.Size(128, 20)
        Me.chkLessThan.TabIndex = 22
        Me.chkLessThan.UseVisualStyleBackColor = True
        '
        'EnquirySelection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.chkLessThan)
        Me.Controls.Add(Me.dtpTo)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.txtTo)
        Me.Controls.Add(Me.cmbTo)
        Me.Controls.Add(Me.cmbOperator)
        Me.Controls.Add(Me.txtFrom)
        Me.Controls.Add(Me.cmbFrom)
        Me.Controls.Add(Me.dtpFrom)
        Me.Name = "EnquirySelection"
        Me.Size = New System.Drawing.Size(862, 22)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents dtpFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtFrom As System.Windows.Forms.TextBox
    Friend WithEvents txtTo As System.Windows.Forms.TextBox
    Friend WithEvents cmbFrom As System.Windows.Forms.ComboBox
    Friend WithEvents cmbTo As System.Windows.Forms.ComboBox
    Friend WithEvents cmbOperator As System.Windows.Forms.ComboBox
    Friend WithEvents chkLessThan As System.Windows.Forms.CheckBox

End Class
