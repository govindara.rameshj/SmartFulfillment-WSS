﻿Public Module Extensions

    ''' <summary>
    ''' Returns "R?:C" value for given activity ID in sheet row tags 
    ''' </summary>
    ''' <param name="sheet"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Public Function RowColumnCoord(ByRef sheet As SheetView, ByVal ID As Integer) As String

        Try
            For Each r As Row In sheet.Rows
                Dim act As BODashboard.cActivity = CType(r.Tag, cActivity)
                If act.ID.Value = ID Then Return "R" & r.Index + 1 & "C"
            Next
            Return String.Empty

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    ''' <summary>
    ''' Returns count of visible rows
    ''' </summary>
    ''' <param name="sheet"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> Public Function RowCountVisible(ByVal sheet As SheetView) As Integer

        Dim total As Integer = 0
        For Each r As Row In sheet.Rows
            If r.Visible Then total += 1
        Next
        Return total

    End Function

End Module
