﻿Friend Class myComboCellType
    Inherits FarPoint.Win.Spread.CellType.ComboBoxCellType
    Private cCombo As myCombo = Nothing
    Private _Locked As Boolean = False

    Public Property Locked() As Boolean
        Get
            Return _Locked
        End Get
        Set(ByVal value As Boolean)
            _Locked = value
        End Set
    End Property

    Public Overrides Function GetEditorControl(ByVal appearance As FarPoint.Win.Spread.Appearance, ByVal zoomFactor As Single) As Control
        Dim cOrig As FpCombo = CType(MyBase.GetEditorControl(appearance, zoomFactor), FpCombo)
        If cCombo Is Nothing Then
            cCombo = New myCombo
            cCombo.Formatter = cOrig.Formatter
            cCombo.ButtonStyle = cOrig.ButtonStyle
            cCombo.AutoHeight = cOrig.AutoHeight
            cCombo.BorderStyle = cOrig.BorderStyle
            cCombo.Editable = cOrig.Editable
            cCombo.Multiline = cOrig.Multiline
            cCombo.WordWrap = cOrig.WordWrap
            cCombo.AcceptsReturn = True
            cCombo.ListOffset = cOrig.ListOffset
            cCombo.List = cOrig.List
            cCombo.MaxDrop = cOrig.MaxDrop
            cCombo.ListWidth = cOrig.ListWidth
            cCombo.MarginLeft = cOrig.MarginLeft
            cCombo.AutoSearch = cOrig.AutoSearch
            cCombo.ListAlignment = cOrig.ListAlignment
            cCombo.MaximumLength = cOrig.MaximumLength
            cCombo.ButtonWidth = cOrig.ButtonWidth
            cCombo.AcceptsArrowKeys = cOrig.AcceptsArrowKeys
            cCombo.ListControl = cOrig.ListControl
            cCombo.NullColor = cOrig.NullColor
            cCombo.ForeColor = cOrig.ForeColor
            cCombo.Font = cOrig.Font
        End If
        Return cCombo
    End Function 'GetEditorControl

    Public Overrides Function GetEditorValue() As Object
        Select Case CType(EditorValue, CellType.EditorValue)
            Case CellType.EditorValue.String : Return Items(cCombo.SelectedIndex)
            Case CellType.EditorValue.ItemData : Return ItemData(cCombo.SelectedIndex)
            Case CellType.EditorValue.Index : Return cCombo.SelectedIndex
        End Select
        Return Nothing
    End Function 'GetEditorValue

    Public Overrides Sub SetEditorValue(ByVal value As Object)
        If cCombo Is Nothing Then
            Return
        End If

        If value Is Nothing Or value Is DBNull.Value Then
            cCombo.SelectedIndex = -1
            If Editable And Not (cCombo Is Nothing) Then
                cCombo.Text = Nothing
            End If
        Else
            'Get index of value in items list
            Dim itemIndex As Integer = 0

            For index As Integer = 0 To Items.GetUpperBound(0)
                If Items(index) = value.ToString Then itemIndex = index
            Next

            cCombo.Text = value.ToString
            cCombo.SelectedIndex = itemIndex

            'Select Case CType(EditorValue, CellType.EditorValue)
            '    Case CellType.EditorValue.String : cCombo.SelectedItem = value.ToString
            '    Case CellType.EditorValue.ItemData : cCombo.SelectedItem = ItemData(itemIndex)
            'End Select
        End If
    End Sub 'SetEditorValue

    Public Overrides Function StopEditing() As Boolean
        If cCombo IsNot Nothing Then
            cCombo.ShowList(False)
        End If
        FireEditingStopped()
        If cCombo IsNot Nothing Then
            cCombo.Dispose()
            cCombo = Nothing
        End If
        Return True
    End Function 'StopEditing

    Public Overrides Sub StartEditing(ByVal e As EventArgs, ByVal selectAll As Boolean, ByVal autoClipboard As Boolean)
        If Not ((e Is Nothing) Or (cCombo Is Nothing)) Then
            Dim rc As Drawing.Rectangle = cCombo.Bounds
            Dim rectButton As New Drawing.Rectangle(rc.X + rc.Width - 14, rc.Y, 14, rc.Height)
            If TypeOf e Is MouseEventArgs Then
                If Not Editable And Not rectButton.Contains(New Drawing.Point(CType(e, MouseEventArgs).X, CType(e, MouseEventArgs).Y)) Then
                    cCombo.Focus()
                    cCombo.ShowList(True)
                Else
                    If selectAll And Editable Then
                        cCombo.EditModeCursorPosition = FarPoint.Win.EditModeCursorPosition.SelectAll
                        cCombo.SelectAll()
                    Else
                        cCombo.EditModeCursorPosition = FarPoint.Win.EditModeCursorPosition.End
                    End If
                End If
            End If

            If TypeOf e Is KeyEventArgs Then
                cCombo.DroppedDown = True
            End If

        End If
    End Sub 'StartEditing

    Public Overrides Sub CancelEditing()
        If Not (cCombo Is Nothing) Then
            cCombo.ShowList(False)
        End If
        FireEditingCanceled()
        If Not (cCombo Is Nothing) Then
            cCombo.Dispose()
            cCombo = Nothing
        End If
    End Sub 'CancelEditing

    Public Overrides Sub PaintCell(ByVal g As System.Drawing.Graphics, ByVal r As System.Drawing.Rectangle, ByVal appearance As FarPoint.Win.Spread.Appearance, ByVal value As Object, ByVal isSelected As Boolean, ByVal isLocked As Boolean, ByVal zoomFactor As Single)

        If _Locked Then
            appearance.DrawPrimaryButton = False
            appearance.DrawSecondaryButton = False
        End If
        MyBase.PaintCell(g, r, appearance, value, isSelected, isLocked, zoomFactor)
    End Sub

End Class

Friend Class myCombo
    Inherits FarPoint.Win.FpCombo

    Protected Overrides Function IsInputKey(ByVal keyData As Keys) As Boolean
        If keyData = Keys.Return Then
            Return False
        End If
        Return MyBase.IsInputKey(keyData)
    End Function 'IsInputKey

End Class