﻿Public Class EnquirySelectionFactory
    Inherits RequirementSwitchFactory(Of IEnquirySelection)

    Public Overrides Function ImplementationA() As IEnquirySelection

        Return New SelectionColumnDisplay

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim RequirementSwitch As IRequirementRepository

        RequirementSwitch = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = RequirementSwitch.IsSwitchPresentAndEnabled(-22001)

    End Function

    Public Overrides Function ImplementationB() As IEnquirySelection

        Return New SelectionColumnID

    End Function

End Class