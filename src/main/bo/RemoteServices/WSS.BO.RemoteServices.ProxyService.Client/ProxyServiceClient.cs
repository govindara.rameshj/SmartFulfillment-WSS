using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using RestSharp;
using WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup;
using WSS.BO.RemoteServices.ProxyService.Contract.Capacity;
using WSS.BO.RemoteServices.ProxyService.Contract.DeliveryNotification;
using WSS.BO.RemoteServices.Common.Client;

namespace WSS.BO.RemoteServices.ProxyService.Client
{
    /// <summary>
    /// Methods are made virtual for unit testing purposes.
    /// </summary>
    public class ProxyServiceClient : BaseServicesClient, IAddressLookupService, ICapacityService, IDeliveryNotificationService
    {
        public ProxyServiceClient(string apiAddress) : base(apiAddress) {}

        public ProxyServiceClient(string apiAddress, RestClient client) : base(apiAddress, client) {}

        #region Capacity

        public virtual void AllocateCapacity(AllocationDetails allocationDetails)
        {
            var request = CreateRequest("/shipment/{fulfillerId}_{date}_{slot_id}/allocatetostore", Method.POST);
            AddAllocationRequestParameters(allocationDetails, request);

            ExecuteCapacityRequest(request);
        }

        public virtual void DeallocateCapacity(AllocationDetails allocationDetails)
        {
            var request = CreateRequest("/shipment/{fulfillerId}_{date}_{slot_id}/deallocatefromstore", Method.POST);
            AddAllocationRequestParameters(allocationDetails, request);

            ExecuteCapacityRequest(request);
        }

        private void AddAllocationRequestParameters(AllocationDetails allocationDetails, RestRequest request)
        {
            request.AddParameter("fulfillerId", allocationDetails.FulfillerId, ParameterType.UrlSegment);
            request.AddParameter("date", allocationDetails.Date.ToString("yyyy-MM-dd"), ParameterType.UrlSegment);
            request.AddParameter("slot_id", allocationDetails.SlotId, ParameterType.UrlSegment);
        }

        private static readonly HttpStatusCode[] CapacitySuccessCodes = { HttpStatusCode.OK };
        private static readonly HttpStatusCode[] CapacityFatalCodes = {};

        private void ExecuteCapacityRequest(RestRequest request)
        {
            ExecuteRequest(request, CapacitySuccessCodes, CapacityFatalCodes);
        }

        #endregion

        #region Consignment

        public virtual void NewConsignment(ConsignmentDetails consignmentDetails)
        {
            var request = CreateRequest("/contactengine/fulfillers/{fulfiller-number}/consignments", Method.POST);
            AddConsignmentUrlParameters(consignmentDetails, request, false);
            AddConsignmentBodyParameters(consignmentDetails, request);

            ExecuteConsignmentRequest(request);
        }

        public virtual void UpdateConsignment(ConsignmentDetails consignmentDetails)
        {
            var request = CreateRequest("/contactengine/fulfillers/{fulfiller-number}/consignments/{consignment-number}", Method.PUT);
            AddConsignmentUrlParameters(consignmentDetails, request, true);
            AddConsignmentBodyParameters(consignmentDetails, request);

            ExecuteConsignmentRequest(request);
        }

        public virtual void CancelConsignment(ConsignmentDetails consignmentDetails)
        {
            var request = CreateRequest("/contactengine/fulfillers/{fulfiller-number}/consignments/{consignment-number}", Method.DELETE);
            AddConsignmentUrlParameters(consignmentDetails, request, true);

            ExecuteConsignmentRequest(request);
        }

        private void AddConsignmentUrlParameters(ConsignmentDetails consignmentDetails, RestRequest request, bool includeConsignmentNumber)
        {
            request.AddParameter("fulfiller-number", consignmentDetails.FulfillerNumber, ParameterType.UrlSegment);
            if (includeConsignmentNumber)
            {
                request.AddParameter("consignment-number", consignmentDetails.ConsignmentNumber, ParameterType.UrlSegment);
            }
        }

        private void AddConsignmentBodyParameters(ConsignmentDetails consignmentDetails, RestRequest request)
        {
            var jsonObject = new
            {
                consignmentNumber = consignmentDetails.ConsignmentNumber,

                fulfillerNumber = consignmentDetails.FulfillerNumber,
                fulfillerName = consignmentDetails.FulfillerName,

                deliveryDate = consignmentDetails.DeliveryDate,

                // Phones are optional
                customerMobilePhone = ConvertEmptyToNull(consignmentDetails.CustomerMobilePhone),
                customerContactPhone = ConvertEmptyToNull(consignmentDetails.CustomerContactPhone),
                customerHomePhone = ConvertEmptyToNull(consignmentDetails.CustomerHomePhone),
                customerWorkPhone = ConvertEmptyToNull(consignmentDetails.CustomerWorkPhone),

                customerName = consignmentDetails.CustomerName,

                postCode = consignmentDetails.PostCode,
                addressLine1 = consignmentDetails.AddressLine1,
                // Address line 2 is optional
                addressLine2 = ConvertEmptyToNull(consignmentDetails.AddressLine2),
                addressLine3 = consignmentDetails.AddressLine3,
                // Address line 4 is optional
                addressLine4 = ConvertEmptyToNull(consignmentDetails.AddressLine4),

                customerOrderNumber = consignmentDetails.CustomerOrderNumber,

                originatingStoreNumber = consignmentDetails.OriginatingStoreNumber,
                originatingStoreName = consignmentDetails.OriginatingStoreName
           };

            AddJsonBody(request, jsonObject);
        }

        private string ConvertEmptyToNull(string value)
        {
            return String.IsNullOrEmpty(value) ? null : value;
        }

        private static readonly HttpStatusCode[] ConsignmentSuccessCodes = { HttpStatusCode.OK, HttpStatusCode.Created };
        private static readonly HttpStatusCode[] ConsignmentFatalCodes = { HttpStatusCode.BadRequest, HttpStatusCode.NotFound, HttpStatusCode.Conflict };

        private void ExecuteConsignmentRequest(RestRequest request)
        {
            ExecuteRequest(request, ConsignmentSuccessCodes, ConsignmentFatalCodes);
        }

        #endregion

        #region QAS

        private static readonly HttpStatusCode[] AddressLookupSuccessCodes = { HttpStatusCode.OK };
        private static readonly IEnumerable<HttpStatusCode> AddressLookupFatalCodes = Enumerable.Empty<HttpStatusCode>();

        public AddressLookupResponse<TAddress> ResolvePostCode<TAddress>(string postCode)
        {
            var request = CreateRequest("/AddressLookup/addresses?postCode={postcode}&format={format}", Method.GET);
            request.AddParameter("postcode", postCode, ParameterType.UrlSegment);
            request.AddParameter("format", typeof(TAddress).Name, ParameterType.UrlSegment);

            return GetAddressLookupResponse<TAddress>(request);
        }

        public AddressLookupResponse<TAddress> RefinePickListItem<TAddress>(string pickupItemId)
        {
            var request = CreateRequest("/AddressLookup/addresses?pickupItemId={pickupItemId}&format={format}", Method.GET);
            request.AddParameter("pickupItemId", pickupItemId, ParameterType.UrlSegment);
            request.AddParameter("format", typeof(TAddress).Name, ParameterType.UrlSegment);

            return GetAddressLookupResponse<TAddress>(request);
        }

        public AddressLookupResponse<TAddress> ClearUpAddress<TAddress>(TAddress address)
        {
            var request = CreateRequest("/AddressLookup/clearUpAddress", Method.POST);
            AddJsonBody(request, new { addressFormat = typeof(TAddress).Name, address });

            return GetAddressLookupResponse<TAddress>(request);
        }

        private AddressLookupResponse<TAddress> GetAddressLookupResponse<TAddress>(RestRequest request)
        {
            return ExecuteRequestWithResult<AddressLookupResponse<TAddress>>(request, AddressLookupSuccessCodes, AddressLookupFatalCodes);
        }

        #endregion

    }
}
