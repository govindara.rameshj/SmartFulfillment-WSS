﻿namespace WSS.BO.RemoteServices.ProxyService.Contract.DeliveryNotification
{
    public interface IDeliveryNotificationService
    {
        void NewConsignment(ConsignmentDetails consignmentDetails);
        void UpdateConsignment(ConsignmentDetails consignmentDetails);
        void CancelConsignment(ConsignmentDetails consignmentDetails);
    }
}
