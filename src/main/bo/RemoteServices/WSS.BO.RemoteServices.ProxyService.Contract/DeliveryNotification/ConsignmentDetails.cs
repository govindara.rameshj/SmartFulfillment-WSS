using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.RemoteServices.ProxyService.Contract.DeliveryNotification
{
    public class ConsignmentDetails
    {
        public string ConsignmentNumber { get; set; }
        public int FulfillerNumber { get; set; }
        public string FulfillerName { get; set; }

        public DateTime DeliveryDate { get; set; }

        public string CustomerMobilePhone { get; set; }
        public string CustomerContactPhone { get; set; }
        public string CustomerHomePhone { get; set; }
        public string CustomerWorkPhone { get; set; }

        public string CustomerName { get; set; }

        public string PostCode { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }

        public int CustomerOrderNumber { get; set; }

        public int OriginatingStoreNumber { get; set; }
        public string OriginatingStoreName { get; set; }

    }
}
