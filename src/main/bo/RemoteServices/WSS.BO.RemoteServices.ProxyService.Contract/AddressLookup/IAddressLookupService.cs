﻿namespace WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup
{
    public interface IAddressLookupService
    {
        AddressLookupResponse<TAddress> ResolvePostCode<TAddress>(string postcode);
        AddressLookupResponse<TAddress> RefinePickListItem<TAddress>(string pickupItemId);
        AddressLookupResponse<TAddress> ClearUpAddress<TAddress>(TAddress address);
    }
}