﻿using System.Collections.Generic;

namespace WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup
{
    public class AddressLookupResponse<TAddress>
    {
        public AddressLookupStatus Status { get; set; }
        public TAddress Address { get; set; }
        public List<PickupItem> PickupItems { get; set; }
    }
}