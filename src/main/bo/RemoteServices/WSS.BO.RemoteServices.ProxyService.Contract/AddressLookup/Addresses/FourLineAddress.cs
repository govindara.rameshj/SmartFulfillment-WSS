﻿using System.Text;

namespace WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.Addresses
{
    public class FourLineAddress
    {
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Town { get; set; }
        public string PostCode { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("AddressLine1=[{0}], ", AddressLine1);
            sb.AppendFormat("AddressLine2=[{0}], ", AddressLine2);
            sb.AppendFormat("Town=[{0}], ", Town);
            sb.AppendFormat("PostCode=[{0}]", PostCode);
            return sb.ToString();
        }
    }
}
