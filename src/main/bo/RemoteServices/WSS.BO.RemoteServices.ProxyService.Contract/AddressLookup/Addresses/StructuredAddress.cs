﻿using System.Text;

namespace WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.Addresses
{
    public class StructuredAddress
    {
        public string BuildingNumber { get; set; }
        public string BuildingName { get; set; }
        public string Organization { get; set; }
        public string SubBuildingName { get; set; }
        public string Thoroughfare { get; set; }
        public string Town { get; set; }
        public string District { get; set; }
        public string County { get; set; }
        public string PostCode { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("BuildingNumber=[{0}], ", BuildingNumber);
            sb.AppendFormat("BuildingName=[{0}], ", BuildingName);
            sb.AppendFormat("Organization=[{0}], ", Organization);
            sb.AppendFormat("SubBuildingName=[{0}], ", SubBuildingName);
            sb.AppendFormat("Thoroughfare=[{0}], ", Thoroughfare);
            sb.AppendFormat("Town=[{0}], ", Town);
            sb.AppendFormat("District=[{0}], ", District);
            sb.AppendFormat("County=[{0}], ", County);
            sb.AppendFormat("PostCode=[{0}]", PostCode);
            return sb.ToString();
        }
    }
}
