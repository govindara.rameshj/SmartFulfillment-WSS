﻿using System.Text;

namespace WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.Addresses
{
    public class NonStructuredAddress
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string PostCode { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Address1=[{0}], ", Address1);
            sb.AppendFormat("Address2=[{0}], ", Address2);
            sb.AppendFormat("Address3=[{0}], ", Address3);
            sb.AppendFormat("Town=[{0}], ", Town);
            sb.AppendFormat("County=[{0}], ", County);
            sb.AppendFormat("PostCode=[{0}]", PostCode);
            return sb.ToString();
        }
    }
}
