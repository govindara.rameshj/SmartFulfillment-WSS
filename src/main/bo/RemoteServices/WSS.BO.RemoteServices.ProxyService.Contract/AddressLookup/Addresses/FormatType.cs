﻿namespace WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.Addresses
{
    public enum FormatType
    {
        FourLineAddress,
        NonStructuredAddress,
        StructuredAddress
    }
}
