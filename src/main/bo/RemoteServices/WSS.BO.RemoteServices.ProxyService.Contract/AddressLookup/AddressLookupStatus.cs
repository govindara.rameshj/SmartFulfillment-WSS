﻿namespace WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup
{
    public enum AddressLookupStatus
    {
        NotFound, //show error
        TooManyMatches, //show error
        ExactMatchFound, //use Address field
        SeveralMatchesFound, //show pickup field, use PickupItems. 
    }
}