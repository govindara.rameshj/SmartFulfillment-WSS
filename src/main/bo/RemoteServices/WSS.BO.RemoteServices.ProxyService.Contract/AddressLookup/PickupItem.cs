﻿namespace WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup
{
    public class PickupItem
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }
}