using System;

namespace WSS.BO.RemoteServices.ProxyService.Contract.Capacity
{
    public class AllocationDetails
    {
        public string FulfillerId { get; set; }
        public DateTime Date { get; set; }
        public int SlotId { get; set; }
    }
}
