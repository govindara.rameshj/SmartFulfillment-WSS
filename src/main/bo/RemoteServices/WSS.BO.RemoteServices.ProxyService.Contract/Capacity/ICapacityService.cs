﻿namespace WSS.BO.RemoteServices.ProxyService.Contract.Capacity
{
    public interface ICapacityService
    {
        void AllocateCapacity(AllocationDetails allocationDetails);
        void DeallocateCapacity(AllocationDetails allocationDetails);
    }
}
