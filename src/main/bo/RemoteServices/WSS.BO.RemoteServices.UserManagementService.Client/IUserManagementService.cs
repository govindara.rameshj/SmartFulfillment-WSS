namespace WSS.BO.RemoteServices.UserManagementService.Client
{
    public interface IUserManagementService
    {
        void NewUser(UserManagmentDetails userManagmentDetails);
        void UpdateUser(UserManagmentDetails userManagmentDetails);
    }
}