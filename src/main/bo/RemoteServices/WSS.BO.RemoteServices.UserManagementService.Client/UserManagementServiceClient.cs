using System.Net;
using RestSharp;
using WSS.BO.RemoteServices.Common.Client;

namespace WSS.BO.RemoteServices.UserManagementService.Client
{
    public class UserManagementServiceClient : BaseServicesClient, IUserManagementService
    {
        public UserManagementServiceClient(string apiAddress) : base(apiAddress) {}

        public UserManagementServiceClient(string apiAddress, RestClient client) : base(apiAddress, client) { }

        public void NewUser(UserManagmentDetails userManagmentDetails)
        {
            var request = CreateRequest("/users", Method.POST);
            AddUserManagmentBodyParameters(userManagmentDetails, request);

            ExecuteUserManagementRequest(request);
        }

        public void UpdateUser(UserManagmentDetails userManagmentDetails)
        {
            var request = CreateRequest("/users", Method.PUT);
            AddUserManagmentBodyParameters(userManagmentDetails, request);

            ExecuteUserManagementRequest(request);
        }

        private void AddUserManagmentBodyParameters(UserManagmentDetails userManagmentDetails, RestRequest request)
        {
            var jsonObject = new
            {
                id = userManagmentDetails.Id,
                code = userManagmentDetails.Code,
                name = userManagmentDetails.Name,
                isDeleted = userManagmentDetails.IsDeleted,
                securityProfile = userManagmentDetails.SecurityProfileId,
                store = userManagmentDetails.StoreId,
                passwordHash = userManagmentDetails.PasswordHash

            };

            AddJsonBody(request, jsonObject);
        }

        private static readonly HttpStatusCode[] UserManagmentSuccessCodes = { HttpStatusCode.OK };
        private static readonly HttpStatusCode[] UserManagmentFatalCodes = { HttpStatusCode.BadRequest, HttpStatusCode.Unauthorized };

        private void ExecuteUserManagementRequest(RestRequest request)
        {
            ExecuteRequest(request, UserManagmentSuccessCodes, UserManagmentFatalCodes);
        }

    }
}
