using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.RemoteServices.UserManagementService.Client
{
    public class UserManagmentDetails
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public string SecurityProfileId { get; set; }
        public string StoreId { get; set; }
        public string PasswordHash { get; set; }
    }
}
