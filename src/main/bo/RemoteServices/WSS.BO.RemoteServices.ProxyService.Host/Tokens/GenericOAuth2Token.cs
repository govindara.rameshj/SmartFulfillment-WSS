using System;

namespace WSS.BO.RemoteServices.ProxyService.Host.Tokens
{
    public class GenericOAuth2Token
    {
        private readonly DateTime? _expireTime; 

        public bool IsExpired(DateTime now)
        {
            return _expireTime <= now;
        }

        public string AccessToken { get; set; }

        public GenericOAuth2Token(string accessToken, long expiresInSeconds, DateTime issued) 
        {
            AccessToken = accessToken;
            _expireTime = issued.AddSeconds(expiresInSeconds);
        }
    }
}