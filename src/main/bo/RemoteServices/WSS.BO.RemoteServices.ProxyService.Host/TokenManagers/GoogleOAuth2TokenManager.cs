using System.Security.Cryptography.X509Certificates;
using System.Threading;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Util;

namespace WSS.BO.RemoteServices.ProxyService.Host.TokenManagers
{
    public class GoogleOAuth2TokenManager : BaseTokenManager
    {
        private TokenResponse _token;
        private readonly X509Certificate2 _certificate;
        private readonly string _serviceEmail;

        public GoogleOAuth2TokenManager(X509Certificate2 certificate, string serviceEmail)
        {
            _certificate = certificate;
            _serviceEmail = serviceEmail;
            _token = null;
        }

        public override string Token
        {
            get
            {
                if (_token == null)
                {
                    return string.Empty;
                }
                return string.Format("{0} {1}", _token.TokenType, _token.AccessToken);
            }
        }

        protected override bool IsValidToken()
        {
            return _token == null || _token.IsExpired(SystemClock.Default);
        }

        protected override void AquireToken()
        {
            ServiceAccountCredential credential = new ServiceAccountCredential(
                        new ServiceAccountCredential.Initializer(_serviceEmail)
                        {
                            Scopes = new[] { "email" }
                        }.FromCertificate(_certificate)
                     );

            var task = credential.RequestAccessTokenAsync(new CancellationToken());
            task.Wait();

            _token = credential.Token; 
        }
    }
}