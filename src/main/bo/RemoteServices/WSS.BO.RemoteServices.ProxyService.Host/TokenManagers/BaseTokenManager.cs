using System.Net;
using WSS.BO.RemoteServices.ProxyService.Host.Interfaces;

namespace WSS.BO.RemoteServices.ProxyService.Host.TokenManagers
{
    public abstract class  BaseTokenManager : ITokenManager
    {
        private static readonly slf4net.ILogger Logger = slf4net.LoggerFactory.GetLogger(typeof(BaseTokenManager));

        private static readonly object LockHandler = new object();

        public abstract string Token { get; }

        public virtual void TryToReAquireToken(string token)
        {
            lock (LockHandler)
            {
                if (token == Token)
                {
                    Logger.Info("We have unauthorized exception. Trying to re-aquire token.");
                    TryToReAquireToken();
                }
                else
                {
                    Logger.Info("We have unauthorized exception. Token re-acquiring already has made.");
                }
            }
        }

        public virtual bool IsTokenExpired(HttpWebResponse response)
        {
            return response.StatusCode == HttpStatusCode.Unauthorized;
        }

        public virtual void CheckToken()
        {
            lock (LockHandler)
            {
                if (IsValidToken())
                {
                    TryToReAquireToken();
                }
            }
        }

        protected abstract bool IsValidToken();

        protected abstract void AquireToken();

        private void TryToReAquireToken()
        {
            Logger.Info("Token is exipred. Trying to re-aquire");
            AquireToken();
            Logger.Info("Token succesfully re-aquired");
        }
    }
}