using System;
using System.Collections.Generic;
using System.Net;
using RestSharp;
using RestSharp.Deserializers;
using WSS.BO.RemoteServices.ProxyService.Host.Tokens;

namespace WSS.BO.RemoteServices.ProxyService.Host.TokenManagers
{
    public class GenericOAuth2TokenManager : BaseTokenManager
    {
        private GenericOAuth2Token _token;
        private readonly string _clientId;
        private readonly string _clientSecret;
        private readonly string _baseUrl;

        public GenericOAuth2TokenManager(string clientId, string clientSecret, string baseUrl)
        {
            _clientId = clientId;
            _clientSecret = clientSecret;
            _baseUrl = baseUrl;
            _token = null;
        }

        public override string Token
        {
            get
            {
                if (_token == null)
                {
                    return string.Empty;
                }
                return _token.AccessToken;
            }
        }

        protected override bool IsValidToken()
        {
            return _token == null || _token.IsExpired(DateTime.Now);
        }

        protected override void AquireToken()
        {
            var client = new RestClient(_baseUrl);
            var request = new RestRequest("oauth2/request_token", Method.POST);
            request.AddParameter("clientid", _clientId);
            request.AddParameter("clientsecret", _clientSecret);
            var response = client.Execute(request);

            if (response.ErrorException != null)
            {
                throw response.ErrorException;
            }

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new ApplicationException(
                    String.Format("Failed to aquire token, OAuth2 server returned status code {0} and body [{1}]",
                    response.StatusCode, response.Content));
            }

            try
            {
                JsonDeserializer deserial = new JsonDeserializer();
                var json = deserial.Deserialize<Dictionary<string, string>>(response);
                _token = new GenericOAuth2Token(json["Access_Token"], long.Parse(json["Expires_in"]), DateTime.Now);
            }
            catch (Exception ex)
            {
                throw new ApplicationException(
                    String.Format("Failed to aquire token - unable to parse OAuth2 server response [{0}].\n Inner exception: {1}",
                    response.Content, ex));
            }
        }
    }
}