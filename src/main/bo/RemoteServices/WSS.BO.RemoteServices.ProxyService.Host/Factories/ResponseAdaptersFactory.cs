using System.Net;
using RestSharp;
using WSS.BO.RemoteServices.ProxyService.Host.Adapters;

namespace WSS.BO.RemoteServices.ProxyService.Host.Factories
{
    public class ResponseAdaptersFactory
    {
        public virtual WebResponseAdapter CreateWebResponseAdapter(HttpWebResponse response)
        {
            return new WebResponseAdapter(response);
        }

        public virtual RestResponseAdapter CreateRestResponseAdapter(IRestResponse response)
        {
            return new RestResponseAdapter(response);
        }
    }
}