using System;
using System.Security.Cryptography.X509Certificates;
using WSS.BO.RemoteServices.ProxyService.Host.Settings;
using WSS.BO.RemoteServices.ProxyService.Host.TokenManagers;

namespace WSS.BO.RemoteServices.ProxyService.Host.Factories
{
    public class TokenManagerFactory
    {
        public static GenericOAuth2TokenManager CreateGenericOAuth2TokenManager(string baseUrl)
        {
            var appSettings = SecretAppSettings.Current;
            return new GenericOAuth2TokenManager(appSettings.ContactEngineClientId, appSettings.ContactEngineClientSecret, baseUrl);
        }

        public static GoogleOAuth2TokenManager CreateGoogleOAuth2TokenManager(string serviceEmail) 
        {
            var appSettings = SecretAppSettings.Current;
            var p12Data = Convert.FromBase64String(appSettings.AppEngineCertificateContents);
            X509Certificate2 certificate = new X509Certificate2(p12Data, appSettings.AppEngineCertificatePassword, X509KeyStorageFlags.Exportable | X509KeyStorageFlags.MachineKeySet);
            return new GoogleOAuth2TokenManager(certificate, serviceEmail);
        }
    }
}