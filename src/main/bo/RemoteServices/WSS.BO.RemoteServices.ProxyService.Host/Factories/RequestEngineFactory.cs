using System.Net;
using RestSharp;

namespace WSS.BO.RemoteServices.ProxyService.Host.Factories
{
    public class RequestEngineFactory
    {
        public virtual HttpWebRequest CreateWebRequestEngine(string requestUriString)
        {
            return (HttpWebRequest)WebRequest.Create(requestUriString);
        }

        public virtual IRestClient CreateRestRequestEngine(string url)
        {
           return new RestClient(url);
        }
    }
}