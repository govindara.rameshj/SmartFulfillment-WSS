using WSS.BO.RemoteServices.ProxyService.Host.Executers;
using WSS.BO.RemoteServices.ProxyService.Host.Helpers;

namespace WSS.BO.RemoteServices.ProxyService.Host.Factories
{
    public class ExecutersFactory
    {
        public static ShipmentExecuter CreateShipmentExecuter(string baseUrl, string serviceEmail)
        {
            return new ShipmentExecuter(baseUrl,
                new RequestEngineFactory(),
                new ResponseConverter(),
                TokenManagerFactory.CreateGoogleOAuth2TokenManager(serviceEmail),
                new ResponseAdaptersFactory());
        }

        public static ContactEngineExecuter CreateContactEngineExecuter(string baseUrl)
        {
            return new ContactEngineExecuter(baseUrl,
                new RequestEngineFactory(),
                new ResponseConverter(),
                TokenManagerFactory.CreateGenericOAuth2TokenManager(baseUrl),
                new ResponseAdaptersFactory());
        }
    }
}