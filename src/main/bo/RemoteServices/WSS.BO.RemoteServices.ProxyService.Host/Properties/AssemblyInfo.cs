﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("WSS.BO.RemoteServices.ProxyService.Host")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Travis Perkins")]
[assembly: AssemblyProduct("WSS.BO.RemoteServices.ProxyService.Host")]
[assembly: AssemblyCopyright("Copyright \u00a9 Travis Perkins 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("f41d9c63-d6b4-40ae-ba9e-80d79179a6a8")]

[assembly: AssemblyVersion("1.2.1.0")]
[assembly: AssemblyFileVersion("1.2.1.0")]