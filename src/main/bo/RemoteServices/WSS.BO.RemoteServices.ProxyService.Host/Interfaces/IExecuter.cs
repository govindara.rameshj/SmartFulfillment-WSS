using System.Net.Http;

namespace WSS.BO.RemoteServices.ProxyService.Host.Interfaces
{
    public interface IExecuter
    {
        HttpResponseMessage ExecuteRequest(string parameters, string method, byte[] body = null, string contentType = "");
    }
}
