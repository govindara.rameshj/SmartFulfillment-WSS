using System.Collections.Specialized;
using System.Net;
using System.Net.Http;

namespace WSS.BO.RemoteServices.ProxyService.Host.Interfaces
{
    public interface IResponseAdapter
    {
        HttpStatusCode StatusCode { get; }
        NameValueCollection Headers { get; }
        HttpContent Content { get; }
    }
}
