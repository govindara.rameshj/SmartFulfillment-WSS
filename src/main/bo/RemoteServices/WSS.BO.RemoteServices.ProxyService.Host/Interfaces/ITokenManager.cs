using System.Net;

namespace WSS.BO.RemoteServices.ProxyService.Host.Interfaces
{
    public interface ITokenManager
    {
        string Token { get; }
        void TryToReAquireToken(string token);
        bool IsTokenExpired(HttpWebResponse response);
        void CheckToken();
    }
}