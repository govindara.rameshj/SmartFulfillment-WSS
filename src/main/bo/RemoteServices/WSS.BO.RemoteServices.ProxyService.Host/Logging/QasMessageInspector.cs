﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text.RegularExpressions;

namespace WSS.BO.RemoteServices.ProxyService.Host.Logging
{
    public class QasMessageInspector : IClientMessageInspector
    {
        private static readonly slf4net.ILogger Logger = slf4net.LoggerFactory.GetLogger(typeof(QasMessageInspector));
        
        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            if (Logger.IsDebugEnabled)
            {
                Logger.Debug("QasMessageInspector.AfterReceiveReply called.");
                Logger.Debug("Message: {0}", reply);
            }
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            if (Logger.IsDebugEnabled)
            {
                Logger.Debug("QasMessageInspector.BeforeSendRequest called.");
                var unsecureMessage = request.ToString();
                var secureMessage = RemoveUnsecureInfo(unsecureMessage);
                Logger.Debug("Message: {0}", secureMessage);
            }
            return null;
        }

        private string RemoveUnsecureInfo(string unsecureMessage)
        {
            var secureMessage = unsecureMessage;
            secureMessage = Regex.Replace(secureMessage, "<Username>.*</Username>", "<Username>XXXXXXXX</Username>");
            secureMessage = Regex.Replace(secureMessage, "<Password>.*</Password>", "<Password>XXXXXXXX</Password>");
            return secureMessage;
        }
    }
}