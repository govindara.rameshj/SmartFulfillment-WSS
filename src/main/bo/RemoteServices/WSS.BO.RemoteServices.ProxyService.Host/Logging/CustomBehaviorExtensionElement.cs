﻿using System;
using System.ServiceModel.Configuration;

namespace WSS.BO.RemoteServices.ProxyService.Host.Logging
{
    public class CustomBehaviorExtensionElement : BehaviorExtensionElement
    {
        protected override object CreateBehavior()
        {
            return new QasEndpointBehavior();
        }

        public override Type BehaviorType
        {
            get
            {
                return typeof(QasEndpointBehavior);
            }
        }
    }
}