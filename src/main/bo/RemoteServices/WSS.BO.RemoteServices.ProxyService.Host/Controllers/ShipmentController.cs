using System;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Text.RegularExpressions;
using System.Web.Http;
using WSS.BO.RemoteServices.ProxyService.Host.Executers;
using WSS.BO.RemoteServices.ProxyService.Host.Factories;
using WSS.BO.RemoteServices.ProxyService.Host.Helpers;
using WSS.BO.RemoteServices.ProxyService.Host.QA;
using WSS.BO.RemoteServices.ProxyService.Host.Settings;

namespace WSS.BO.RemoteServices.ProxyService.Host.Controllers
{
    public class ShipmentController : ApiController
    {
        private static readonly slf4net.ILogger Logger = slf4net.LoggerFactory.GetLogger(typeof(ShipmentController));

        private static readonly ServiceEnvironmentSettings EnvironmentSettings;
        private static readonly FulfillerValidator FulfillerValidator;
        private static readonly ShipmentExecuter Executer;

        private static readonly Regex StubRegex;

        static ShipmentController()
        {
            EnvironmentSettings = ServiceEnvironmentSettings.Current;
            FulfillerValidator = new FulfillerValidator(EnvironmentSettings, @"^(?<fulfillerId>\d+)_", EnvironmentSettings.RequireFulfillerCheck);
            Executer = ExecutersFactory.CreateShipmentExecuter(EnvironmentSettings.CloudApiUrl, EnvironmentSettings.ServiceEmail);
            if (EnvironmentSettings.EnableQa)
            {
                StubRegex = new Regex(@"^(\d+)_(.+)_(\d+)", RegexOptions.IgnoreCase);
            }
        }

        public HttpResponseMessage Get(string parameters)
        {
            Logger.Info("Invoked shipment API GET request with parameters: {0}", parameters);
            try
            {
                FulfillerValidator.Validate(User, parameters);

                if (EnvironmentSettings.StubMode)
                {
                    var m = StubRegex.Match(parameters);
                    if (m.Success)
                    {
                        var fulfillerId = int.Parse(m.Groups[1].Value);
                        var dt = DateTime.ParseExact(m.Groups[2].Value, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                        var slot = int.Parse(m.Groups[3].Value);

                        return new HttpResponseMessage(HttpStatusCode.OK)
                        {
                            Content = new StringContent(string.Format("OK, fulfiller id: {0}, date: {1:yyyy-MM-dd}, slot: {2}, store counter for this date: {3}, capacity: {4}",
                                fulfillerId, dt, slot, Counters.Store[fulfillerId][dt], Counters.Capacity[dt]))
                        };
                    }
                    return new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(string.Format("Couldn't parse parameters: {0}", parameters)) };
                }
                return Executer.ExecuteRequest(parameters, HttpMethod.Get.ToString());
            }
            catch (AuthenticationException ex)
            {
                Logger.Error("Authorization error: {0}", ex);
                return new HttpResponseMessage(HttpStatusCode.Unauthorized) { Content = new StringContent(ex.Message) };
            }
        }

        public HttpResponseMessage Post(string parameters, string storeaction)
        {
            Logger.Info("Invoked shipment API POST request with parameters: {0} and store action: {1}", parameters, storeaction);
            try
            {
                FulfillerValidator.Validate(User, parameters);

                if (EnvironmentSettings.StubMode)
                {
                    var m = StubRegex.Match(parameters);
                    if (m.Success)
                    {
                        var fulfillerId = int.Parse(m.Groups[1].Value);
                        var dt = DateTime.ParseExact(m.Groups[2].Value, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                        var slot = int.Parse(m.Groups[3].Value);

                        if (storeaction == "allocatetostore")
                        {
                            Counters.Capacity[dt]--;
                            Counters.Store[fulfillerId][dt]++;
                        }
                        else if (storeaction == "deallocatefromstore")
                        {
                            Counters.Capacity[dt]++;
                            Counters.Store[fulfillerId][dt]--;
                        }

                        return new HttpResponseMessage(HttpStatusCode.OK)
                        {
                            Content = new StringContent(string.Format("OK, fulfiller id: {0}, date: {1:yyyy-MM-dd}, slot: {2}, store counter for this date: {3}, capacity: {4}, action: {5}",
                                fulfillerId, dt, slot, Counters.Store[fulfillerId][dt], Counters.Capacity[dt], storeaction))
                        };
                    }
                    return new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(string.Format("Couldn't parse parameters: {0}, storeaction: {1}", parameters, storeaction)) };
                }
                return Executer.ExecuteRequest(parameters + "/" + storeaction, HttpMethod.Post.ToString());
            }
            catch (AuthenticationException ex)
            {
                Logger.Error("Authorization error: {0}", ex);
                return new HttpResponseMessage(HttpStatusCode.Unauthorized) { Content = new StringContent(ex.Message) };
            }
        }
    }
}
