using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Text.RegularExpressions;
using System.Web.Http;
using WSS.BO.RemoteServices.ProxyService.Host.Executers;
using WSS.BO.RemoteServices.ProxyService.Host.Factories;
using WSS.BO.RemoteServices.ProxyService.Host.Helpers;
using WSS.BO.RemoteServices.ProxyService.Host.QA;
using WSS.BO.RemoteServices.ProxyService.Host.Settings;

namespace WSS.BO.RemoteServices.ProxyService.Host.Controllers
{
    public class ContactEngineController : ApiController
    {
        private static readonly slf4net.ILogger Logger = slf4net.LoggerFactory.GetLogger(typeof(ContactEngineController));

        private static readonly ServiceEnvironmentSettings Settings;
        private static readonly FulfillerValidator FulfillerValidator;
        private static readonly ContactEngineExecuter Executer;

        private static readonly Regex StubRegex;

        static ContactEngineController()
        {
            Settings = ServiceEnvironmentSettings.Current;
            var baseUrl = Settings.ContactEngineApiUrl;

            FulfillerValidator = new FulfillerValidator(Settings, @"fulfillers/(?<fulfillerId>\d+)/", Settings.RequireFulfillerCheckForContactEngine);
            Executer = ExecutersFactory.CreateContactEngineExecuter(baseUrl);

            if (Settings.EnableQa)
            {
                StubRegex = new Regex(@"^consignmentNumber=(\d+)(.+)customerWorkPhone=(.+)&customerName=(.+)&postCode=(.+)&addressLine1=(.+)&addressLine2=(.+)&addressLine3=(.+)&customer(.+)", RegexOptions.IgnoreCase);
            }
        }

        [HttpDelete]
        [HttpGet]
        [HttpHead]
        [HttpOptions]
        [HttpPatch]
        [HttpPost]
        [HttpPut]
        public HttpResponseMessage Push(HttpRequestMessage message, string path)
        {
            Logger.Info("Invoked ContactEngine API {1} request with path: {0}", path, message.Method);

            try
            {
                FulfillerValidator.Validate(User, path);
                if (Settings.StubMode) 
                {

                    var m = StubRegex.Match(GetContent(message));
                    if (m.Success)
                    {
                        ConsignmentsContainer.Set(m.Groups[1].Value, new ConsignmentsContainer.ConsignmentDetails()
                        {
                            CustomerName = m.Groups[4].Value,
                            PostCode = m.Groups[5].Value,
                            AddressLine1 = m.Groups[6].Value,
                            AddressLine2 = m.Groups[7].Value,
                            AddressLine3 = m.Groups[8].Value,
                            CustomerWorkPhone = m.Groups[3].Value
                        });

                        return new HttpResponseMessage(HttpStatusCode.OK);
                    }
                    return new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(string.Format("Couldn't parse parameters: {0}", GetContent(message))) };
                }
                return Executer.ExecuteRequest(path, GetMethod(message), GetBody(message), GetContentType(message));
            }
            catch (AuthenticationException exc)
            {
                Logger.Error("Authorization error: {0}", exc);
                return new HttpResponseMessage(HttpStatusCode.Unauthorized) { Content = new StringContent(exc.Message) };
            }
        }

        private string GetMethod(HttpRequestMessage message)
        {
            return message.Method.Method;
        }

        private byte[] GetBody(HttpRequestMessage message)
        {
            return message.Content.ReadAsByteArrayAsync().Result;
        }

        private string GetContentType(HttpRequestMessage message)
        {
            string contentType = string.Empty;
            if (message.Content.Headers.ContentType != null)
            {
                contentType = message.Content.Headers.ContentType.MediaType;
            }
            return contentType;
        }

        private string GetContent(HttpRequestMessage message)
        {
            return message.Content.ReadAsStringAsync().Result;
        }

    }
}
