﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using WSS.BO.RemoteServices.ProxyService.Host.Settings;
using WSS.BO.RemoteServices.QasIntegration;
using WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup;
using WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.Addresses;

namespace WSS.BO.RemoteServices.ProxyService.Host.Controllers
{
    public class AddressLookupController : ApiController
    {
        private static readonly slf4net.ILogger Logger = slf4net.LoggerFactory.GetLogger(typeof(AddressLookupController));
        private static readonly IAddressLookupService Processor;
        
        static AddressLookupController()
        {
            var secretAppSettings = SecretAppSettings.Current;
            var globalSettings = ServiceGlobalSettings.Current;

            var settings = new QasSettings
            {
                EndpointConfigurationName = "QASOnDemand",
                RemoteAddress = ServiceEnvironmentSettings.Current.QasUrl,

                UserName = secretAppSettings.QasUsername,
                Password = secretAppSettings.QasPassword,

                Country = globalSettings.QasCountry,
                FourLineLayout = globalSettings.QasFourLineLayout,
                SixLineLayout = globalSettings.QasSixLineLayout,
                NineLineLayout = globalSettings.QasNineLineLayout,
            };

            var factory = new AddressLookupFactory();
            Processor = factory.CreateProcessor(settings);
        }

        [HttpGet]
        public HttpResponseMessage ResolvePostCode(string postCode, FormatType format = FormatType.FourLineAddress)
        {
            try
            {
                Logger.Info("ResolvedPostCode method in controller is called with postCode: '{0}' and format = '{1}'", postCode, format);
                var factory = new AddressFormatAdapterFactory();
                var adapter = factory.GetAdapter(format, Processor);
                var response = adapter.ResolvePostCodeWithLayout(postCode);
                var jsonResult = ConvertToJson(response);

                Logger.Info("ResolvePostCode method in controller for postCode = '{0}' has successfully executed. Result : {1}", postCode, jsonResult);
                return new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(jsonResult) };
            }
            catch (Exception ex)
            {
                Logger.Error("Internal server error: {0}", ex);
                return new HttpResponseMessage(HttpStatusCode.InternalServerError) { Content = new StringContent(ex.Message) };
            }
        }

        [HttpGet]
        public HttpResponseMessage RefinePickListItem(string pickupItemId, FormatType format = FormatType.FourLineAddress)
        {
            try
            {
                Logger.Info("RefinePickListItem method in controller is called with pickupItemId: '{0}' and format = '{1}'", pickupItemId, format);

                var factory = new AddressFormatAdapterFactory();
                var adapter = factory.GetAdapter(format, Processor);
                var response = adapter.RefinePickListItemWithLayout(pickupItemId);
                var jsonResult = ConvertToJson(response);

                Logger.Info("RefinePickListItem method in controller for pickupItemId = '{0}' has successfully executed. Result : {1}", pickupItemId, jsonResult);
                return new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(jsonResult) };
            }
            catch (Exception ex)
            {
                Logger.Error("Internal server error: {0}", ex);
                return new HttpResponseMessage(HttpStatusCode.InternalServerError) { Content = new StringContent(ex.Message) };
            }
        }

        [HttpPost]
        public HttpResponseMessage ClearUpAddress(JObject jsonData)
        {
            try
            {
                Logger.Info("ClearUpAddress method in controller is called with json request: \n {0}", jsonData);

                var addressFormat = (FormatType)Enum.Parse(typeof(FormatType), jsonData.GetValue("addressFormat").Value<string>());

                var factory = new AddressFormatAdapterFactory();
                var adapter = factory.GetAdapter(addressFormat, Processor);
                var addressToken = jsonData.GetValue("address");
                object address = adapter.DeserializeFromJson(addressToken);

                var response = adapter.ClearUpAddressWithLayout(address);
                var jsonResult = ConvertToJson(response);

                Logger.Info("ClearUpAddress method in controller successfully executed. Result : {0}", jsonResult);
                return new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(jsonResult) };
            }
            catch (Exception ex)
            {
                Logger.Error("Internal server error: {0}", ex);
                return new HttpResponseMessage(HttpStatusCode.InternalServerError) { Content = new StringContent(ex.Message) };
            }
        }

        private string ConvertToJson(object response)
        {
            var serializer = new JsonSerializer()
            {
                Formatting = Formatting.Indented,
                Converters = { new StringEnumConverter() }
            };

            var stringBuilder = new StringBuilder();
            using (var sw = new StringWriter(stringBuilder))
            {
                serializer.Serialize(sw, response);
            }
            return stringBuilder.ToString();
        }

        interface IAddressFormatAdapter
        {
            object DeserializeFromJson(JToken jsonToken);
            object ResolvePostCodeWithLayout(string postcode);
            object RefinePickListItemWithLayout(string pickupItemId);
            object ClearUpAddressWithLayout(object address);
        }

        private class AddressFormatAdapter<TAddress> : IAddressFormatAdapter
        {
            private readonly IAddressLookupService _processor;

            public AddressFormatAdapter(IAddressLookupService processor)
            {
                _processor = processor;
            }

            public object DeserializeFromJson(JToken jsonToken)
            {
                return jsonToken.ToObject<TAddress>();
            }

            public object ResolvePostCodeWithLayout(string postcode)
            {
                return _processor.ResolvePostCode<TAddress>(postcode);
            }

            public object RefinePickListItemWithLayout(string pickupItemId)
            {
                return _processor.RefinePickListItem<TAddress>(pickupItemId);
            }

            public object ClearUpAddressWithLayout(object address)
            {
                return _processor.ClearUpAddress((TAddress)address);
            }
        }

        private class AddressFormatAdapterFactory
        {
            public IAddressFormatAdapter GetAdapter(FormatType formatType, IAddressLookupService processor)
            {
                switch (formatType)
                {
                    case FormatType.FourLineAddress:
                        return new AddressFormatAdapter<FourLineAddress>(processor);
                    case FormatType.NonStructuredAddress:
                        return new AddressFormatAdapter<NonStructuredAddress>(processor);
                    case FormatType.StructuredAddress:
                        return new AddressFormatAdapter<StructuredAddress>(processor);
                    default:
                        throw new ArgumentException(String.Format("Unknown format type [{0}]", formatType));
                }
            }
        }
    }
}