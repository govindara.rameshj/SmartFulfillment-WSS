using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSS.BO.RemoteServices.ProxyService.Host.QA;
using WSS.BO.RemoteServices.ProxyService.Host.Settings;

namespace WSS.BO.RemoteServices.ProxyService.Host.Controllers
{
    public class QAController : ApiController
    {
        private static ServiceEnvironmentSettings _settings;

        static QAController()
        {
            _settings = ServiceEnvironmentSettings.Current;
        }

        [HttpGet, HttpPost]
        public HttpResponseMessage Capacity(DateTime date)
        {
            if (Request.Method == HttpMethod.Get)
            {
                var res = new HttpResponseMessage(HttpStatusCode.OK);
                res.Content = new StringContent(Counters.Capacity[date].ToString());
                return res;
            }
            if (Request.Method == HttpMethod.Post)
            {
                var body = Request.Content.ReadAsStringAsync().Result;
                var newValue = int.Parse(body);
                Counters.Capacity[date] = newValue;
                return new HttpResponseMessage(HttpStatusCode.OK);
            }

            return new HttpResponseMessage(HttpStatusCode.NotImplemented);
        }

        [HttpGet, HttpPost]
        public HttpResponseMessage Store(int storeId, DateTime date)
        {
            if (Request.Method == HttpMethod.Get)
            {
                var res = new HttpResponseMessage(HttpStatusCode.OK);
                var i = Counters.Store[storeId][date];
                res.Content = new StringContent(i.ToString());
                return res;
            }
            if (Request.Method == HttpMethod.Post)
            {
                var body = Request.Content.ReadAsStringAsync().Result;
                var newValue = int.Parse(body);
                Counters.Store[storeId][date] = newValue;
                return new HttpResponseMessage(HttpStatusCode.OK);
            }

            return new HttpResponseMessage(HttpStatusCode.NotImplemented);
        }

        [HttpGet]
        public HttpResponseMessage Consignment(string key)
        {
            var consignment = ConsignmentsContainer.Get(key);
            if (consignment == null)
                return new HttpResponseMessage(HttpStatusCode.NoContent);

            var res = new HttpResponseMessage(HttpStatusCode.OK);
            res.Content = new StringContent(consignment.ToString());
            return res;
        }

        [HttpGet]
        public HttpResponseMessage ClearContainers()
        {
            ConsignmentsContainer.ClearData();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}
