﻿using System.Web.Mvc;

namespace WSS.BO.RemoteServices.ProxyService.Host.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
