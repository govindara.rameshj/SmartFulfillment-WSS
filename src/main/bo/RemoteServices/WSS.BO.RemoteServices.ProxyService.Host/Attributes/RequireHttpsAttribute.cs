using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace WSS.BO.RemoteServices.ProxyService.Host.Attributes
{
    public class RequireHttpsAttribute : AuthorizationFilterAttribute
    {
        public static readonly string HttpsRequired = "HTTPS Required";

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext.Request.RequestUri.Scheme != Uri.UriSchemeHttps)
            {
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden)
                {
                    ReasonPhrase = HttpsRequired
                };
            }
            else
            {
                base.OnAuthorization(actionContext);
            }
        }
    }
}