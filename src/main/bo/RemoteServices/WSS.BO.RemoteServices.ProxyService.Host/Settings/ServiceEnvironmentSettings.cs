using System.Configuration;
using System.Xml;

namespace WSS.BO.RemoteServices.ProxyService.Host.Settings
{
    public class ServiceEnvironmentSettings: ConfigurationSection
    {
        public string EnvironmentName;
        public bool StubMode;
        public bool RequireFulfillerCheck;
        public bool RequireFulfillerCheckForContactEngine;
        public string FulfillerIdPattern;
        public string CloudApiUrl;
        public string ContactEngineApiUrl;
        public string ServiceEmail;
        public bool EnableQa;
        public string QasUrl;

        protected override void DeserializeSection(XmlReader reader)
        {
            while (reader.Read())
            {
                if (reader.NodeType != XmlNodeType.Element)
                    continue;

                switch (reader.Name)
                {
                    case "EnvironmentName":
                        EnvironmentName = reader.ReadElementContentAsString();
                        break;
                    case "StubMode":
                        StubMode = reader.ReadElementContentAsBoolean();
                        break;
                    case "FulfillerAuthorization":
                        RequireFulfillerCheck = bool.Parse(reader.GetAttribute("enabled"));
                        RequireFulfillerCheckForContactEngine = bool.Parse(reader.GetAttribute("enabledForContactEngine"));
                        FulfillerIdPattern = reader.GetAttribute("fulfillerIdPattern");
                        break;
                    case "CloudApiUrl":
                        CloudApiUrl = reader.ReadElementContentAsString();
                        break;
                    case "ContactEngineApiUrl":
                        ContactEngineApiUrl = reader.ReadElementContentAsString();
                        break;
                    case "ServiceEmail":
                        ServiceEmail = reader.ReadElementContentAsString();
                        break;
                    case "EnableQA":
                        EnableQa = reader.ReadElementContentAsBoolean();
                        break;
                    case "QasUrl":
                        QasUrl = reader.ReadElementContentAsString();
                        break;
                }
            }

            if (EnableQa && !StubMode)
                throw new ConfigurationErrorsException("If EnableQA is set to true, then StubMode must be enabled");
        }

        private static ServiceEnvironmentSettings _current;
        public static ServiceEnvironmentSettings Current
        {
            get {
                return _current ?? (_current = (ServiceEnvironmentSettings)ConfigurationManager.GetSection("serviceEnvironmentSettings"));
            }
        }
    }
}