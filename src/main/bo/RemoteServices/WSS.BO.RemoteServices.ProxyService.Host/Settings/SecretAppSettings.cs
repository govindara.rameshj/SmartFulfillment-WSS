﻿using System;
using System.Configuration;

namespace WSS.BO.RemoteServices.ProxyService.Host.Settings
{
    public class SecretAppSettings
    {
        private readonly ConnectionStringSettingsCollection connectionStrings;

        private static readonly Lazy<SecretAppSettings> InnerCurrent = new Lazy<SecretAppSettings>();
        public static SecretAppSettings Current
        {
            get
            {
                return InnerCurrent.Value;
            }
        }

        public SecretAppSettings()
        {
            connectionStrings = ConfigurationManager.ConnectionStrings;
        }

        public string AppEngineCertificateContents
        {
            get { return GetValueByName("AppEngine.CertificateContents"); }
        }

        public string AppEngineCertificatePassword
        {
            get { return GetValueByName("AppEngine.CertificatePassword"); }
        }


        public string ContactEngineClientId
        {
            get { return GetValueByName("ContactEngine.ClientId"); }
        }

        public string ContactEngineClientSecret
        {
            get { return GetValueByName("ContactEngine.ClientSecret"); }
        }


        public string QasUsername
        {
            get { return GetValueByName("Qas.Username"); }
        }

        public string QasPassword
        {
            get { return GetValueByName("Qas.Password"); }
        }

        private string GetValueByName(string name)
        {
            return connectionStrings[name].ConnectionString;
        }
    }
}