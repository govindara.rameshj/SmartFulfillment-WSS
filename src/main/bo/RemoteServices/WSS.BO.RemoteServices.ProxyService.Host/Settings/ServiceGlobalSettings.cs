using System.Configuration;
using System.Xml;

namespace WSS.BO.RemoteServices.ProxyService.Host.Settings
{
    public class ServiceGlobalSettings: ConfigurationSection
    {
        public string QasCountry;

        public string QasFourLineLayout;
        public string QasSixLineLayout;
        public string QasNineLineLayout;

        protected override void DeserializeSection(XmlReader reader)
        {
            while (reader.Read())
            {
                if (reader.NodeType != XmlNodeType.Element)
                    continue;

                switch (reader.Name)
                {
                    case "QasCountry":
                        QasCountry = reader.ReadElementContentAsString();
                        break;
                    case "QasFourLineLayout":
                        QasFourLineLayout = reader.ReadElementContentAsString();
                        break;
                    case "QasSixLineLayout":
                        QasSixLineLayout = reader.ReadElementContentAsString();
                        break;
                    case "QasNineLineLayout":
                        QasNineLineLayout = reader.ReadElementContentAsString();
                        break;
                }
            }
        }

        private static ServiceGlobalSettings _current;
        public static ServiceGlobalSettings Current
        {
            get {
                return _current ?? (_current = (ServiceGlobalSettings)ConfigurationManager.GetSection("serviceGlobalSettings"));
            }
        }
    }
}