using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using WSS.BO.RemoteServices.ProxyService.Host.Interfaces;

namespace WSS.BO.RemoteServices.ProxyService.Host.Adapters
{
    public class WebResponseAdapter : IResponseAdapter
    {
        private readonly HttpWebResponse _response;

        public WebResponseAdapter(HttpWebResponse response)
        {
            _response = response;
        }

        public HttpStatusCode StatusCode
        {
            get { return _response.StatusCode; }
        }

        public NameValueCollection Headers
        {
            get { return _response.Headers; }
        }

        public HttpContent Content
        {
            get { return new StreamContent(_response.GetResponseStream()); }
        }
    }
}