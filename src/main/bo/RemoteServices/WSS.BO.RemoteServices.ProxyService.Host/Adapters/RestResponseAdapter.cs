using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using RestSharp;
using WSS.BO.RemoteServices.ProxyService.Host.Interfaces;

namespace WSS.BO.RemoteServices.ProxyService.Host.Adapters
{
    public class RestResponseAdapter : IResponseAdapter
    {
        private readonly IRestResponse _response;
        private NameValueCollection _headers;

        public RestResponseAdapter(IRestResponse response)
        {
            _response = response;
        }

        public HttpStatusCode StatusCode
        {
            get { return _response.StatusCode; }
        }

        public NameValueCollection Headers
        {
            get 
            {
                if (_headers == null)
                {
                    _headers = new NameValueCollection();
                    foreach (var header in _response.Headers)
                    {
                        _headers.Add(header.Name, header.Value.ToString());
                    }

                }
                return _headers;
            }
        }

        public HttpContent Content
        {
            get { return new ByteArrayContent(_response.RawBytes); }
        }
    }
}