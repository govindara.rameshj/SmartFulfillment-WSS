﻿using System.Web.Mvc;

namespace WSS.BO.RemoteServices.ProxyService.Host
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}