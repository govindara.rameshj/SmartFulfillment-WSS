﻿using System.Web.Http;
using WSS.BO.RemoteServices.ProxyService.Host.Helpers;

namespace WSS.BO.RemoteServices.ProxyService.Host
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            RegisterContactEngine(config);
            RegisterShipment(config);
            RegisterQa(config);
            RegisterAddressLookup(config);

            config.Filters.Add(new TraceExceptionLogger());
        }

        private static void RegisterContactEngine(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute("ContactEngine",
                "contactengine/{*path}",
                new {controller = "ContactEngine", action = "Push"}
                );
        }

        private static void RegisterShipment(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                "ShipmentApi get",
                "shipment/{parameters}",
                new {controller = "Shipment"}
                );

            config.Routes.MapHttpRoute(
                "ShipmentApi action",
                "shipment/{parameters}/{storeaction}",
                new {controller = "Shipment"}
                );
        }

        private static void RegisterQa(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                "QA capacity",
                "qa/capacity/{date}",
                new {controller = "QA", action = "Capacity"}
                );

            config.Routes.MapHttpRoute(
                "QA store",
                "qa/store/{storeId}/{date}",
                new {controller = "QA", action = "Store"}
                );

            config.Routes.MapHttpRoute(
                "QA consignment",
                "qa/consignment/{key}",
                new {controller = "QA", action = "Consignment"}
                );

            config.Routes.MapHttpRoute(
                "QA consignment clear",
                "qa/clear",
                new {controller = "QA", action = "ClearContainers"}
                );
        }

        private static void RegisterAddressLookup(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                "Resolve post code",
                "AddressLookup/addresses",
                new {controller = "AddressLookup"});

            config.Routes.MapHttpRoute(
                "Clear up address",
                "AddressLookup/clearUpAddress",
                new {controller = "AddressLookup", action = "clearUpAddress"});
        }
    }
}
