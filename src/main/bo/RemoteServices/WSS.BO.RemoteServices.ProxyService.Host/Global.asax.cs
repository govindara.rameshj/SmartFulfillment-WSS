using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;


namespace WSS.BO.RemoteServices.ProxyService.Host
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : HttpApplication
    {
        private static readonly slf4net.ILogger Logger = slf4net.LoggerFactory.GetLogger(typeof(WebApiApplication));

        protected void Application_Start()
        {
            Logger.Info("Application version {0} started", GetApplicationVersion());

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            GlobalConfiguration.Configuration.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
        }

        private static readonly Lazy<Version> Version = new Lazy<Version>(() => typeof(WebApiApplication).Assembly.GetName().Version);

        public static Version GetApplicationVersion()
        {
            return Version.Value;
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            if (ex != null)
            {
                Logger.Error("Application has encountered fatal error: {0}", ex);
            }
            else
            {
                Logger.Error("Application has encountered unspecified fatal error");
            }
        }

        protected void Application_End(object sender, EventArgs e)
        {
            Logger.Info("Application ended");
        }
    }
}
