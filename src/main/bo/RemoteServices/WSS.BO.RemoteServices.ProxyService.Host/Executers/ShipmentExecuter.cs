using System.Net;
using System.Net.Http;
using WSS.BO.RemoteServices.ProxyService.Host.Factories;
using WSS.BO.RemoteServices.ProxyService.Host.Helpers;
using WSS.BO.RemoteServices.ProxyService.Host.TokenManagers;

namespace WSS.BO.RemoteServices.ProxyService.Host.Executers
{
    public class ShipmentExecuter : BaseExecuter
    {
        public ShipmentExecuter(string baseUrl, RequestEngineFactory requestFactory, ResponseConverter responseConverter, GoogleOAuth2TokenManager tokenManager, ResponseAdaptersFactory adapterFactory)
            : base(baseUrl, requestFactory, responseConverter, tokenManager, adapterFactory) { }

        protected override string CreateStartLogMessage(string parameters, string method, byte[] body, string contentType)
        {
            return string.Format("Executing request to Cloudshop, parameters = {0}, method = {1}", parameters, method);
        }

        protected override HttpResponseMessage GetResponse(string parameters, string method, bool isFirstRun, string accessToken, byte[] body, string contentType)
        {
            HttpWebRequest request = RequestFactory.CreateWebRequestEngine(BaseUrl + "/shipment/" + parameters);
            request.Method = method;
            request.Headers.Add("Authorization", accessToken);

            if (method == HttpMethod.Post.ToString())
            {
                var stream = request.GetRequestStream();
                stream.Close();
            }
            return ResponseConverter.GetHttpResponse(AdapterFactory.CreateWebResponseAdapter((HttpWebResponse)request.GetResponse()));
        }
    }
}