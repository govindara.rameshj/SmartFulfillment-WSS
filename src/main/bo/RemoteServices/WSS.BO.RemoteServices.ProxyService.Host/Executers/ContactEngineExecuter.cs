using System;
using System.Net.Http;
using System.Text;
using RestSharp;
using WSS.BO.RemoteServices.ProxyService.Host.Factories;
using WSS.BO.RemoteServices.ProxyService.Host.Helpers;
using WSS.BO.RemoteServices.ProxyService.Host.TokenManagers;

namespace WSS.BO.RemoteServices.ProxyService.Host.Executers
{
    public class ContactEngineExecuter : BaseExecuter
    {
        public ContactEngineExecuter(string baseUrl, RequestEngineFactory requestFactory, ResponseConverter responseConverter, GenericOAuth2TokenManager tokenManager, ResponseAdaptersFactory adapterFactory)
        : base(baseUrl, requestFactory, responseConverter, tokenManager, adapterFactory) {}

        protected override string CreateStartLogMessage(string parameters, string method, byte[] body, string contentType)
        {
            return string.Format("Executing request to ContactEngine, path = {0}, method = {1}, body = {2}, contentType = {3}", parameters, method, Encoding.UTF8.GetString(body), contentType);
        }

        protected override HttpResponseMessage GetResponse(string parameters, string method, bool isFirstRun, string accessToken, byte[] body, string contentType)
        {
            IRestRequest request = CreateRequest(method, body, contentType);
            IRestClient client = RequestFactory.CreateRestRequestEngine(string.Format("{0}/{1}", BaseUrl, parameters));
            request.AddQueryParameter("access_token", accessToken);
            var response = client.Execute(request);
            if (response.ErrorException != null)
            {
                throw response.ErrorException;
            }
            return ResponseConverter.GetHttpResponse(AdapterFactory.CreateRestResponseAdapter(response));
        }

        private IRestRequest CreateRequest(string method, byte[] body, string contentType)
        {
            var request = new RestRequest((Method)Enum.Parse(typeof(Method), method));
            if (body != null)
            {
                request.AddParameter(contentType, body, ParameterType.RequestBody);
            }
            return request;
        }
    }
}