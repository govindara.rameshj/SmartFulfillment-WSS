using System.Net;
using System.Net.Http;
using WSS.BO.RemoteServices.ProxyService.Host.Adapters;
using WSS.BO.RemoteServices.ProxyService.Host.Factories;
using WSS.BO.RemoteServices.ProxyService.Host.Helpers;
using WSS.BO.RemoteServices.ProxyService.Host.Interfaces;

namespace WSS.BO.RemoteServices.ProxyService.Host.Executers
{
    public abstract class BaseExecuter : IExecuter
    {
        private static readonly slf4net.ILogger Logger = slf4net.LoggerFactory.GetLogger(typeof(BaseExecuter));

        protected ITokenManager TokenManager;
        protected ResponseConverter ResponseConverter;
        protected RequestEngineFactory RequestFactory;
        protected string BaseUrl;
        protected ResponseAdaptersFactory AdapterFactory;

        protected BaseExecuter(string baseUrl, RequestEngineFactory requestFactory, ResponseConverter responseConverter, ITokenManager tokenManager, ResponseAdaptersFactory adapterFactory)
        {
            BaseUrl = baseUrl;
            RequestFactory = requestFactory;
            ResponseConverter = responseConverter;
            TokenManager = tokenManager;
            AdapterFactory = adapterFactory;
        }

        public HttpResponseMessage ExecuteRequest(string parameters, string method, byte[] body = null, string contentType = "")
        {

            Logger.Debug(CreateStartLogMessage(parameters, method, body, contentType));
            TokenManager.CheckToken();
            return DoRequest(parameters, method, true, TokenManager.Token, body, contentType);
        }

        protected abstract string CreateStartLogMessage(string parameters, string method, byte[] body, string contentType);

        protected abstract HttpResponseMessage GetResponse(string parameters, string method, bool isFirstRun, string accessToken, byte[] body, string contentType);

        private HttpResponseMessage DoRequest(string parameters,string method, bool isFirstRun, string accessToken, byte[] body, string contentType)
        {
            try
            {
                HttpResponseMessage res = GetResponse(parameters, method, isFirstRun, accessToken, body, contentType);
                Logger.Debug("Request has been succesfull, HTTP status code is {0}", res.StatusCode);
                return res;
            }
            catch (WebException exc)
            {
                HttpWebResponse httpWebResponse = exc.Response as HttpWebResponse;
                if (httpWebResponse != null)
                {
                    if (isFirstRun && TokenManager.IsTokenExpired(httpWebResponse))
                    {
                        TokenManager.TryToReAquireToken(accessToken);
                        return DoRequest(parameters, method, false, TokenManager.Token, body, contentType);
                    }
                    Logger.Error("Exception occured while making the request: {0}", exc);
                    return ResponseConverter.GetHttpResponse(new WebResponseAdapter(httpWebResponse));
                }

                Logger.Error("Exception occured while making the request: {0}", exc);
                throw;
            }
        }
    }
}