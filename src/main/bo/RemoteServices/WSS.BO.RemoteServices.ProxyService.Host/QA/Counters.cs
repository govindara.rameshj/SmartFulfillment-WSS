﻿namespace WSS.BO.RemoteServices.ProxyService.Host.QA
{
    public static class Counters
    {
        private static CountersContainer capacity;
        public static CountersContainer Capacity
        {
            get { return capacity; }
        }

        private static StoreCountersContainer store;
        public static StoreCountersContainer Store
        {
            get { return store; }
        }

        static Counters()
        {
            capacity = new CountersContainer();
            store = new StoreCountersContainer();
        }
    }
}
