﻿using System.Collections.Generic;

namespace WSS.BO.RemoteServices.ProxyService.Host.QA
{
    public class StoreCountersContainer
    {
        private Dictionary<int, CountersContainer> counters = new Dictionary<int, CountersContainer>();

        public CountersContainer this[int storeId]
        {
            get
            {
                if (!counters.ContainsKey(storeId))
                    counters[storeId] = new CountersContainer();
                return counters[storeId];
            }
        }
    }
}