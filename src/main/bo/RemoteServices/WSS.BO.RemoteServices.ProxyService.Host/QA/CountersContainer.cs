﻿using System;
using System.Collections.Generic;

namespace WSS.BO.RemoteServices.ProxyService.Host.QA
{
    public class CountersContainer
    {
        private Dictionary<DateTime, int> counters = new Dictionary<DateTime, int>();

        public int this[DateTime date]
        {
            get
            {
                if (counters.ContainsKey(date))
                    return counters[date];
                return 0;
            }

            set
            {
                counters[date] = value;
            }
        }
    }
}