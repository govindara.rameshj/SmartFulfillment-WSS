﻿using System;
using System.Collections.Generic;

namespace WSS.BO.RemoteServices.ProxyService.Host.QA
{
    public static class ConsignmentsContainer
    {
        public class ConsignmentDetails
        {
            public string CustomerName;
            public string PostCode;
            public string AddressLine1;
            public string AddressLine2;
            public string AddressLine3;
            public string CustomerWorkPhone;

            public override string ToString()
            {
                return string.Format("{0};{1};{2};{3};{4};{5}", CustomerName, PostCode, AddressLine1, AddressLine2, AddressLine3, CustomerWorkPhone);
            }
        }

        private static readonly Dictionary<string, ConsignmentDetails> Data = new Dictionary<String, ConsignmentDetails>();

        public static ConsignmentDetails Get(string key)
        {
            if (Data.ContainsKey(key))
                return Data[key];
            return null;
        }

        public static void Set(string key, ConsignmentDetails val)
        {
            Data[key] = val;
        }

        public static void ClearData()
        {
            Data.Clear();
        }
    }
}