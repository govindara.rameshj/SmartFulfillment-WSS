using System.Web.Http.Filters;

namespace WSS.BO.RemoteServices.ProxyService.Host.Helpers
{
    public class TraceExceptionLogger : ExceptionFilterAttribute
    {
        private static readonly slf4net.ILogger Logger = slf4net.LoggerFactory.GetLogger(typeof(TraceExceptionLogger));

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            Logger.Error("Exception occurred while running action: {0}", actionExecutedContext.Exception);
        }
    }
}