using System.Linq;
using System.Net.Http;
using WSS.BO.RemoteServices.ProxyService.Host.Interfaces;

namespace WSS.BO.RemoteServices.ProxyService.Host.Helpers
{
    public class ResponseConverter
    {
        // See HttpResponseHeader.Content* for reference
        private static readonly string[] ContentHeaders = {
            "content-length",
            "content-type",
            "content-encoding",
            "content-langauge",
            "content-location",
            "content-md5",
            "content-range"
        };

        // Content-Length response header MUST NOT be present when Transfer-Encoding is used (RFC2616 Section 4.4)
        private static readonly string[] IgnoredHeaders = {
            "transfer-encoding"
        };

        public virtual HttpResponseMessage GetHttpResponse(IResponseAdapter response)
        {
            HttpResponseMessage res = new HttpResponseMessage(response.StatusCode);
            res.Content = response.Content;
            foreach (string header in response.Headers)
            {
                if (ContentHeaders.Contains(header.ToLower()))
                    res.Content.Headers.Add(header, response.Headers[header]);
                else
                {
                    if (!IgnoredHeaders.Contains(header.ToLower()))
                    {
                        res.Headers.Add(header, response.Headers[header]);
                    }
                }
            }
            return res;
        }
    }
}