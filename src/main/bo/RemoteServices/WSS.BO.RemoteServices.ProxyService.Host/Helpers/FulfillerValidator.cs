using System.Security.Authentication;
using System.Security.Principal;
using System.Text.RegularExpressions;
using WSS.BO.RemoteServices.ProxyService.Host.Settings;

namespace WSS.BO.RemoteServices.ProxyService.Host.Helpers
{
    public class FulfillerValidator
    {
        private static readonly slf4net.ILogger Logger = slf4net.LoggerFactory.GetLogger(typeof(FulfillerValidator));

        private readonly Regex _userNamefulfillerIdRegex;
        private readonly Regex _requestFulfillerIdRegex;
        private readonly bool _isRequireFulfillerCheck;

        public FulfillerValidator(ServiceEnvironmentSettings settings, string requestFulfillerIdPattern, bool isRequireFulfillerCheck)
        {
            _userNamefulfillerIdRegex = new Regex(settings.FulfillerIdPattern, RegexOptions.IgnoreCase);
            _requestFulfillerIdRegex = new Regex(requestFulfillerIdPattern, RegexOptions.IgnoreCase);
            _isRequireFulfillerCheck = isRequireFulfillerCheck;
        }

        public void Validate(IPrincipal user, string parameters)
        {
            if (_isRequireFulfillerCheck)
            {
                Logger.Info("Checking authentication");

                if (!user.Identity.IsAuthenticated)
                    throw new AuthenticationException("User is not authenticated");

                Logger.Info("User name: {0}", user.Identity.Name);

                // Authorize
                Match m = _userNamefulfillerIdRegex.Match(user.Identity.Name);
                if (m.Success)
                {
                    string clientFulfillerId = m.Groups["fulfillerId"].Value;

                    Match requestMatch = _requestFulfillerIdRegex.Match(parameters);
                    if (requestMatch.Success)
                    {
                        string requestFulfillerId = requestMatch.Groups["fulfillerId"].Value;

                        if (clientFulfillerId == requestFulfillerId)
                            return;
                        else
                            throw new AuthenticationException(string.Format("User {0} is not authorized to make this request as fulfillerId {1}", user.Identity.Name, requestFulfillerId));
                    }
                }

                throw new AuthenticationException(string.Format("User {0} is not authorized to make this request", user.Identity.Name));
            }
        }
    }
}