using System;
using System.Collections.Generic;
using System.Linq;
using RestSharp;
using System.Net;
using Newtonsoft.Json;
using slf4net;

namespace WSS.BO.RemoteServices.Common.Client
{
    public abstract class BaseServicesClient
    {
        private static readonly ILogger Logger = LoggerFactory.GetLogger(typeof(BaseServicesClient));
        private readonly string _apiAddress;
        private readonly RestClient _restClient;

        protected BaseServicesClient(string apiAddress)
        {
            _apiAddress = apiAddress;
            _restClient = CreateRestClient();
        }

        protected BaseServicesClient(string apiAddress, RestClient client)
        {
            _apiAddress = apiAddress;
            _restClient = client;
        }

        protected IRestResponse ExecuteRequest(RestRequest request, IEnumerable<HttpStatusCode> successStatusCodes, IEnumerable<HttpStatusCode> fatalStatusCodes)
        {
            var uri = _restClient.BuildUri(request);

            Logger.Info(string.Format("Send request: {0}", uri));
            IRestResponse response = _restClient.Execute(request);
            Logger.Info(string.Format("Response status code: {0}", response.StatusCode));

            HandleResponseStatus(response, successStatusCodes, fatalStatusCodes);

            return response;
        }

        protected TResult ExecuteRequestWithResult<TResult>(RestRequest request, IEnumerable<HttpStatusCode> successStatusCodes, IEnumerable<HttpStatusCode> fatalStatusCodes)
        {
            var response = ExecuteRequest(request, successStatusCodes, fatalStatusCodes);

            Logger.Info(string.Format("Response content: {0}", response.Content));
            return JsonConvert.DeserializeObject<TResult>(response.Content);
        }

        private void HandleResponseStatus(IRestResponse response, IEnumerable<HttpStatusCode> successStatusCodes, IEnumerable<HttpStatusCode> fatalStatusCodes)
        {
            var statusCode = response.StatusCode;
            if (successStatusCodes.Contains(statusCode))
            {
                return;
            }
            var severity = fatalStatusCodes.Contains(statusCode) ? BaseServiceClientException.ErrorSeverity.Fatal : BaseServiceClientException.ErrorSeverity.Normal;
            string message = String.Format("Proxy returned HTTP status {0}, message \"{1}\", body \"{2}\"",
                response.StatusCode, response.ErrorMessage ?? response.StatusDescription, response.Content);
            throw new BaseServiceClientException(severity, message);
        }

        protected RestRequest CreateRequest(String resourse, Method method)
        {
            RestRequest request = new RestRequest(resourse, method)
            {
                Credentials = CredentialCache.DefaultNetworkCredentials
            };

            request.AddHeader("Accept", "text/plain");

            return request;
        }

        protected RestClient CreateRestClient()
        {
            return new RestClient(_apiAddress);
        }

        protected static void AddJsonBody(RestRequest request, object obj)
        {
            request.JsonSerializer =
                new RestSharpJsonNetSerializer(new JsonSerializer { NullValueHandling = NullValueHandling.Ignore });
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(obj);
        }
    }
}
