using System;

namespace WSS.BO.RemoteServices.Common.Client
{
    public class BaseServiceClientException : Exception
    {
        public ErrorSeverity Severity
        {
            get { return _severity; }
        }

        private readonly ErrorSeverity _severity;

        public BaseServiceClientException(ErrorSeverity severity, string message)
            : base(message)
        {
            _severity = severity;
        }

        public enum ErrorSeverity : byte
        { 
            Normal = 0,
            Fatal
        }
    }
}
