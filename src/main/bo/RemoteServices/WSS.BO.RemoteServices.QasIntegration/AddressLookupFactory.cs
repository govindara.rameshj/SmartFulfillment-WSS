﻿using WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup;

namespace WSS.BO.RemoteServices.QasIntegration
{
    public class AddressLookupFactory
    {
        public IAddressLookupService CreateProcessor(QasSettings qasSettings)
        {
            return new AddressLookupProcessor(qasSettings);
        }
    }
}
