﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup;
using WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.Addresses;
using WSS.BO.RemoteServices.QasIntegration.QasServiceReference;

namespace WSS.BO.RemoteServices.QasIntegration
{
    public class AddressLookupProcessor : IAddressLookupService
    {
        private static readonly slf4net.ILogger Logger = slf4net.LoggerFactory.GetLogger(typeof(AddressLookupProcessor));
        private const string PostcodeSearch = " @x";
        private readonly QasSettings _qasSettings;

        internal AddressLookupProcessor(QasSettings qasSettings)
        {
            _qasSettings = qasSettings;
        }

        public AddressLookupResponse<TAddress> ResolvePostCode<TAddress>(string postcode)
        {
            Logger.Info("AddressLookupProcessor.ResolvePostCode method started work with postCode: '{0}'", postcode);
            using (var client = new QASOnDemandClient(_qasSettings.EndpointConfigurationName, _qasSettings.RemoteAddress))
            {
                var header = GetQueryHeader();
                var engine = GetEngine();
                var query = GetSearchQuery(postcode + PostcodeSearch, _qasSettings.Country, engine);

                QASearchResult searchResult;
                client.DoSearch(header, query, out searchResult);
                return ProcessSearchResult<TAddress>(searchResult.QAPicklist);
            }
        }

        public AddressLookupResponse<TAddress> RefinePickListItem<TAddress>(string moniker)
        {
            Logger.Info("AddressLookupProcessor.RefinePickListItem method started work with moniker: '{0}'", moniker);
            using (var client = new QASOnDemandClient(_qasSettings.EndpointConfigurationName, _qasSettings.RemoteAddress))
            {
                var header = GetQueryHeader();
                var query = GetRefineQuery(moniker);
                Picklist picklist;
                client.DoRefine(header, query, out picklist);
                return ProcessSearchResult<TAddress>(picklist.QAPicklist);
            }
        }

        public AddressLookupResponse<TAddress> ClearUpAddress<TAddress>(TAddress address)
        {
            Logger.Info("AddressLookupProcessor.ClearUpAddress method started work with address: '{0}'", address);

            var factory = new AddressFormatAdapterFactory();
            var adapter = factory.GetAdapter<TAddress>(_qasSettings);
            var parts = adapter.GetAddressParts(address);
            var combinedAddress = String.Join(",", parts);

            Logger.Debug("AddressLookupProcessor.ClearUpAddress method will clean up address string : '{0}'", combinedAddress);

            using (var client = new QASOnDemandClient(_qasSettings.EndpointConfigurationName, _qasSettings.RemoteAddress))
            {
                var header = GetQueryHeader();
                var engine = GetEngine();
                var query = GetSearchQuery(combinedAddress, _qasSettings.Country, engine);

                QASearchResult searchResult;
                client.DoSearch(header, query, out searchResult);
                return ProcessSearchResult<TAddress>(searchResult.QAPicklist);
            }
        }

        private AddressLookupResponse<TAddress> ProcessSearchResult<TAddress>(QAPicklistType resultPicklist)
        {
            var parsedResult = new AddressLookupResponse<TAddress>();

            SetupStatus(parsedResult, resultPicklist);

            if (parsedResult.Status == AddressLookupStatus.SeveralMatchesFound)
            {
                CreatePicklist(parsedResult, resultPicklist);
            }

            if (resultPicklist.AutoFormatSafe || (parsedResult.Status == AddressLookupStatus.ExactMatchFound && resultPicklist.PicklistEntry.First().FullAddress)) 
            {
                CreateAddress(parsedResult, resultPicklist.PicklistEntry.First().Moniker);
            }

            if (resultPicklist.AutoStepinSafe)
            {
                parsedResult = RefinePickListItem<TAddress>(resultPicklist.PicklistEntry.First().Moniker);
            }

            return parsedResult;
        }

        private void SetupStatus<TAddress>(AddressLookupResponse<TAddress> parsedResult, QAPicklistType resultPicklist)
        {
            if (resultPicklist.MaxMatches || resultPicklist.LargePotential)
            {
                parsedResult.Status = AddressLookupStatus.TooManyMatches;
                return;
            }
            if (resultPicklist.PicklistEntry[0].Picklist == "No matches")
            {
                parsedResult.Status = AddressLookupStatus.NotFound;
                return;
            }
            if (resultPicklist.PicklistEntry.Count() == 1)
            {
                parsedResult.Status = AddressLookupStatus.ExactMatchFound;
                return;
            }
            parsedResult.Status = AddressLookupStatus.SeveralMatchesFound;
        }

        private void CreatePicklist<TAddress>(AddressLookupResponse<TAddress> parsedResult, QAPicklistType resultPicklist)
        {
            parsedResult.PickupItems = new List<PickupItem>();
            foreach (var foundAddress in resultPicklist.PicklistEntry)
            {
                var newPickupItem = new PickupItem()
                {
                    Description = foundAddress.Picklist,
                    Id = foundAddress.Moniker
                };
                parsedResult.PickupItems.Add(newPickupItem);
            }
        }

        private void CreateAddress<TAddress>(AddressLookupResponse<TAddress> parsedResult, string moniker)
        {
            var factory = new AddressFormatAdapterFactory();
            var adapter = factory.GetAdapter<TAddress>(_qasSettings);
            
            var layout = adapter.GetLayout();
            var qasAddress = GetAddress(moniker, layout);

            parsedResult.Address = (TAddress)adapter.GenerateFormattedAddress(qasAddress);
        }

        private Address GetAddress(string moniker, string layout)
        {
            using (var client = new QASOnDemandClient(_qasSettings.EndpointConfigurationName, _qasSettings.RemoteAddress))
            {
                QAGetAddress qaGetAddress = new QAGetAddress() { Moniker = moniker, Layout = layout};
                var header = GetQueryHeader();
                Address result;

                client.DoGetAddress(header, qaGetAddress, out result);
                return result;
            }
        }

        private QAQueryHeader GetQueryHeader()
        {
            var queryHeader = new QAQueryHeader
            {
                QAAuthentication = new QAAuthentication
                {
                    Username = _qasSettings.UserName,
                    Password = _qasSettings.Password
                }
            };
            return queryHeader;
        }

        private EngineType GetEngine()
        {
            var engine = new EngineType
            {
                Value = EngineEnumType.Singleline,
                Intensity = EngineIntensityType.Exact,
                Flatten = false
            };
            return engine;
        }

        private QASearch GetSearchQuery(string valueToSearch, string country, EngineType engine)
        {
            return new QASearch() { Search = valueToSearch, Country = country, Engine = engine };
        }

        private QARefine GetRefineQuery(string moniker)
        {
            return new QARefine() { Moniker = moniker };
        }

        interface IAddressFormatAdapter
        {
            object GenerateFormattedAddress(Address qasAddress);
            string GetLayout();
            string[] GetAddressParts(object address);
        }

        private class FourLineAddressFormatAdapter : IAddressFormatAdapter
        {
            private readonly QasSettings _qasSettings;

            public FourLineAddressFormatAdapter(QasSettings qasSettings)
            {
                _qasSettings = qasSettings;
            }
            
            public object GenerateFormattedAddress(Address qasAddress)
            {
                return new FourLineAddress()
                {
                    AddressLine1 = qasAddress.QAAddress.AddressLine[0].Line,
                    AddressLine2 = qasAddress.QAAddress.AddressLine[1].Line,
                    Town = qasAddress.QAAddress.AddressLine[2].Line,
                    PostCode = qasAddress.QAAddress.AddressLine[3].Line
                };
            }

            public string GetLayout()
            {
                return _qasSettings.FourLineLayout;
            }

            public string[] GetAddressParts(object address)
            {
                var typedAddress = address as FourLineAddress;
                Debug.Assert(typedAddress != null, "typedAddress != null");
                return new[]
                {
                    typedAddress.AddressLine1,
                    typedAddress.AddressLine2,
                    typedAddress.Town,
                    typedAddress.PostCode
                };
            }
        }

        private class NonStructuredAddressFormatAdapter : IAddressFormatAdapter
        {
            private readonly QasSettings _qasSettings;

            public NonStructuredAddressFormatAdapter(QasSettings qasSettings)
            {
                _qasSettings = qasSettings;
            }
            
            public object GenerateFormattedAddress(Address qasAddress)
            {
                return new NonStructuredAddress()
                {
                    Address1 = qasAddress.QAAddress.AddressLine[0].Line,
                    Address2 = qasAddress.QAAddress.AddressLine[1].Line,
                    Address3 = qasAddress.QAAddress.AddressLine[2].Line,
                    Town = qasAddress.QAAddress.AddressLine[3].Line,
                    County = qasAddress.QAAddress.AddressLine[4].Line,
                    PostCode = qasAddress.QAAddress.AddressLine[5].Line
                };
            }

            public string GetLayout()
            {
                return _qasSettings.SixLineLayout;
            }

            public string[] GetAddressParts(object address)
            {
                var typedAddress = address as NonStructuredAddress;
                Debug.Assert(typedAddress != null, "typedAddress != null");
                return new[]
                {
                    typedAddress.Address1,
                    typedAddress.Address2,
                    typedAddress.Address3,
                    typedAddress.Town,
                    typedAddress.County,
                    typedAddress.PostCode
                };
            }
        }

        private class StructuredAddressFormatAdapter : IAddressFormatAdapter
        {
            private readonly QasSettings _qasSettings;

            public StructuredAddressFormatAdapter(QasSettings qasSettings)
            {
                _qasSettings = qasSettings;
            }
            
            public object GenerateFormattedAddress(Address qasAddress)
            {
                return new StructuredAddress()
                {
                    BuildingNumber = qasAddress.QAAddress.AddressLine[0].Line,
                    BuildingName = qasAddress.QAAddress.AddressLine[1].Line,
                    Organization = qasAddress.QAAddress.AddressLine[2].Line,
                    SubBuildingName = qasAddress.QAAddress.AddressLine[3].Line,
                    Thoroughfare = qasAddress.QAAddress.AddressLine[4].Line,
                    Town = qasAddress.QAAddress.AddressLine[5].Line,
                    District = qasAddress.QAAddress.AddressLine[6].Line,
                    County = qasAddress.QAAddress.AddressLine[7].Line,
                    PostCode = qasAddress.QAAddress.AddressLine[8].Line
                };
            }

            public string GetLayout()
            {
                return _qasSettings.NineLineLayout;
            }

            public string[] GetAddressParts(object address)
            {
                var typedAddress = address as StructuredAddress;
                Debug.Assert(typedAddress != null, "typedAddress != null");
                return new[]
                {
                    typedAddress.BuildingNumber,
                    typedAddress.BuildingName, 
                    typedAddress.Organization, 
                    typedAddress.SubBuildingName,
                    typedAddress.Thoroughfare,
                    typedAddress.Town,
                    typedAddress.District,
                    typedAddress.County,
                    typedAddress.PostCode
                };
            }
        }

        private class AddressFormatAdapterFactory
        {
            public IAddressFormatAdapter GetAdapter<TAddress>(QasSettings settings)
            {
                if (typeof(TAddress) == typeof(FourLineAddress))
                    return new FourLineAddressFormatAdapter(settings);
                if (typeof (TAddress) == typeof (NonStructuredAddress))
                    return new NonStructuredAddressFormatAdapter(settings);
                if (typeof (TAddress) == typeof (StructuredAddress))
                    return new StructuredAddressFormatAdapter(settings);
                throw new ArgumentException(String.Format("Unknown address type = {0}", typeof(TAddress).Name));
            }
        }
    }
}
