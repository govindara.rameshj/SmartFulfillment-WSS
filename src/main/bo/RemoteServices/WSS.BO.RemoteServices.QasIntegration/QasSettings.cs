﻿
namespace WSS.BO.RemoteServices.QasIntegration
{
    public class QasSettings
    {
        public string EndpointConfigurationName { get; set; }
        public string RemoteAddress { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }

        public string Country { get; set; }

        public string FourLineLayout { get; set; }
        public string SixLineLayout { get; set; }
        public string NineLineLayout { get; set; }
    }
}
