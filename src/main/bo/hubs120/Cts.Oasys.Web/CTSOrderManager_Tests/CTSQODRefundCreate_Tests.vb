﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports System.Diagnostics
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Cts.Oasys.Webservices.CtsOrderManager
Imports Cts.Oasys.Data
Imports System.Xml
Imports System.Xml.Serialization

<TestClass()> Public Class CTSQODRefundCreate_Tests

    Private testContextInstance As TestContext
    Private Const NON_EXISTENT_ORDER_NUMBER As String = "1"
    'Private Const EXISTING_ORDER_NUMBER As String = "726381"
    Private Const CTSQODRefundCreateRequest_XML As String = "<CTSRefundCreateRequest><DateTimeStamp>2011-08-09T00:02:50.590</DateTimeStamp><RefundHeader><Source>WO</Source><SourceOrderNumber>726982</SourceOrderNumber><StoreOrderNumber>012711</StoreOrderNumber><RefundDate>2011-08-08T00:00:49.778</RefundDate><RefundSeq>0001</RefundSeq><LoyaltyCardNumber></LoyaltyCardNumber><DeliveryChargeRefundValue>0</DeliveryChargeRefundValue><RefundTotal>1.96</RefundTotal></RefundHeader><RefundLines><RefundLine><StoreLineNo>4</StoreLineNo><ProductCode>153089</ProductCode><QuantityCancelled>4</QuantityCancelled><RefundLineValue>1.96</RefundLineValue></RefundLine></RefundLines></CTSRefundCreateRequest>"
    Private Const CTSRefundCreateOneLineRequest_XML As String = "<CTSRefundCreateRequest><DateTimeStamp>2011-08-11T07:03:28.469</DateTimeStamp><RefundHeader><Source>WO</Source><SourceOrderNumber>726982</SourceOrderNumber><StoreOrderNumber>012711</StoreOrderNumber><RefundDate>2011-08-11T00:00:25.387</RefundDate><RefundSeq>0001</RefundSeq><LoyaltyCardNumber></LoyaltyCardNumber><DeliveryChargeRefundValue>0.00</DeliveryChargeRefundValue><RefundTotal>2.21</RefundTotal></RefundHeader><RefundLines><RefundLine><StoreLineNo>3</StoreLineNo><ProductCode>107177</ProductCode><QuantityCancelled>1</QuantityCancelled><RefundLineValue>2.21</RefundLineValue></RefundLine></RefundLines></CTSRefundCreateRequest>"
    Private Const CTSQODRefundCreateRequestComplexOrder_XML As String = ""
    Private Const CTSQODRefundCreateRequestFiveLineOrder_XML As String = ""
    Private Const MaximumRequests As Integer = 1
    Private Const EXISTING_ORDER_NUMBER As String = "123333"
    Private Const EXISTING_STOREORDER_NUMBER As String = "014014"

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub CTSQODRefundCreate_Perfomance_CheckOK()

        ' Arrange

        Dim ramCounter As PerformanceCounter
        Dim requestXML As String
        ramCounter = New PerformanceCounter("Memory", "Available MBytes")

        Dim initialMemoryFree As Single = 0, finalMemoryFree As Single = 0
        initialMemoryFree = ramCounter.NextValue

        ' configure log for test
        Dim log As New Cts.Oasys.Webservices.Log
        With log
            .Header = "C:\temp\"
        End With

        Dim dbase As New Cts.Oasys.Webservices.TpWickes.RequestDecisionEngineDatabaseSQLTest
        Cts.Oasys.Webservices.TpWickes.RequestDecisionEngineDatabaseFactory.SetRequestDecisionEngineDatabase(dbase)
        Cts.Oasys.Webservices.LogFactory.FactorySet(log)

        Dim WebOrderNumber As Long = CLng(EXISTING_ORDER_NUMBER)
        Dim StoreOrderNumber As Long = CLng(EXISTING_STOREORDER_NUMBER)
        Dim ws As New Cts.Oasys.Webservices.CtsOrderManager

        ' Act
        For counter As Integer = 1 To 100
            WebOrderNumber += 1
            StoreOrderNumber += 1
            Dim TmpXML As String = _
                        CTSRefundCreateOneLineRequest_XML.Replace("<SourceOrderNumber>726982</SourceOrderNumber>", _
                                                         "<SourceOrderNumber>" & WebOrderNumber.ToString("000000") & "</SourceOrderNumber>")

            requestXML = TmpXML.Replace("<StoreOrderNumber>012711</StoreOrderNumber>", _
                                        "<StoreOrderNumber>" & StoreOrderNumber.ToString("000000") & "</StoreOrderNumber>")

            ws.CTS_Refund_Create(requestXML)
        Next


        finalMemoryFree = ramCounter.NextValue

        Assert.IsTrue(initialMemoryFree * 0.95 < finalMemoryFree)

    End Sub

    <TestMethod()> Public Sub CTSQODRefundCreate_Perfomance_CheckComplexOrderOK()

        ' Arrange

        'Dim ramCounter As PerformanceCounter
        'Dim requestXML As String
        'ramCounter = New PerformanceCounter("Memory", "Available MBytes")

        'Dim initialMemoryFree As Single = 0, finalMemoryFree As Single = 0
        'initialMemoryFree = ramCounter.NextValue

        '' configure log for test
        'Dim log As New Cts.Oasys.Webservices.Log
        'With log
        '    .Header = "C:\temp\"
        'End With

        'Dim dbase As New Cts.Oasys.Webservices.TpWickes.RequestDecisionEngineDatabaseSQLTest
        'Cts.Oasys.Webservices.TpWickes.RequestDecisionEngineDatabaseFactory.SetRequestDecisionEngineDatabase(dbase)
        'Cts.Oasys.Webservices.LogFactory.FactorySet(log)

        'Dim NewWebOrderNumber As Long = 0
        'Dim ws As New Cts.Oasys.Webservices.CtsOrderManager

        '' Act
        'For counter As Integer = 1 To 100
        '    NewWebOrderNumber += 1
        '    requestXML = _
        '                CTSQODRefundCreateRequestComplexOrder_XML.Replace("<SourceOrderNumber>729551</SourceOrderNumber>", _
        '                                                 "<SourceOrderNumber>" & NewWebOrderNumber.ToString & "</SourceOrderNumber>")
        '    Dim str As String = ws.CTS_Refund_Create(requestXML)
        'Next

        '' Assert
        'finalMemoryFree = ramCounter.NextValue

        'Assert.IsTrue(initialMemoryFree * 0.95 < finalMemoryFree)

    End Sub
    '<TestMethod()> Public Sub CTSQODRefundCreate_SourceOrder_DoesExist()

    '    ' Arrange

    '    Dim xmlDoc As New XmlDocument

    '    Dim chk As String

    '    Dim ws As New Cts.Oasys.Webservices.CtsOrderManager

    '    Dim log As New Cts.Oasys.Webservices.Log
    '    With log
    '        .Header = "C:\temp\"
    '    End With

    '    Dim dtbase As New Cts.Oasys.Webservices.TpWickes.RequestDecisionEngineDatabaseSQLTest
    '    Cts.Oasys.Webservices.TpWickes.RequestDecisionEngineDatabaseFactory.SetRequestDecisionEngineDatabase(dtbase)

    '    Cts.Oasys.Webservices.LogFactory.FactorySet(log)

    '    '  Act

    '    Dim str As String = ws.CTS_Refund_Create(CTSQODRefundCreateRequest_XML)
    '    xmlDoc.LoadXml(str)
    '    chk = xmlDoc.DocumentElement.ChildNodes(1).ChildNodes(0).InnerText


    '    ' Assert
    '    Assert.IsTrue(chk.Equals("false"))

    'End Sub
    '<TestMethod()> Public Sub CTSQODRefundCreate_SourceOrder_DoesNotExist()

    '    ' Arrange

    '    Dim NewWebOrderNumber As Long = 0
    '    Dim xmlDoc As New XmlDocument

    '    Dim chk As String

    '    Dim log As New Cts.Oasys.Webservices.Log
    '    With log
    '        .Header = "C:\temp\"
    '    End With

    '    Dim requestXML As String = _
    '       CTSQODRefundCreateRequest_XML.Replace("<SourceOrderNumber>726378</SourceOrderNumber>", _
    '                                                     "<SourceOrderNumber>" & NewWebOrderNumber.ToString & "</SourceOrderNumber>")

    '    Dim dtbase As New Cts.Oasys.Webservices.TpWickes.RequestDecisionEngineDatabaseSQLTest
    '    Cts.Oasys.Webservices.TpWickes.RequestDecisionEngineDatabaseFactory.SetRequestDecisionEngineDatabase(dtbase)

    '    Cts.Oasys.Webservices.LogFactory.FactorySet(log)

    '    '  Act

    '    Dim str As String = ws.CTS_Refund_Create(requestXML)
    '    xmlDoc.LoadXml(str)
    '    chk = xmlDoc.DocumentElement.ChildNodes(1).ChildNodes(0).InnerText


    '    ' Assert
    '    Assert.IsTrue(chk.Equals("true"))

    'End Sub

    '<TestMethod()> Public Sub CTSQODRefundCreate_ProductCode_DoesNotExist()

    '    ' Arrange
    '    Dim requestXML As String = _
    '        CTSQODRefundCreateRequest_XML.Replace("<ProductCode>240693</ProductCode>", _
    '                                                     "<ProductCode>" & NON_EXISTENT_ORDER_NUMBER & "</ProductCode>")
    '    Dim xmlDoc As New XmlDocument

    '    Dim chk As String

    '    Dim ws As New Cts.Oasys.Webservices.CtsOrderManager

    '    Dim log As New Cts.Oasys.Webservices.Log
    '    With log
    '        .Header = "C:\temp\"
    '    End With

    '    Dim dtbase As New Cts.Oasys.Webservices.TpWickes.RequestDecisionEngineDatabaseSQLTest
    '    Cts.Oasys.Webservices.TpWickes.RequestDecisionEngineDatabaseFactory.SetRequestDecisionEngineDatabase(dtbase)

    '    Cts.Oasys.Webservices.LogFactory.FactorySet(log)

    '    '  Act

    '    Dim str As String = ws.CTS_Refund_Create(requestXML)
    '    xmlDoc.LoadXml(str)
    '    chk = xmlDoc.DocumentElement.ChildNodes(1).ChildNodes(0).InnerText


    '    ' Assert
    '    Assert.IsTrue(chk.Equals("false"))

    'End Sub
    '<TestMethod()> Public Sub CTSQODRefundCreate_ProductCode_DoesExist()

    '    ' Arrange

    '    Dim requestXML As String = _
    '       CTSQODRefundCreateRequest_XML.Replace("<SourceOrderNumber>726378</SourceOrderNumber>", _
    '                                                     "<SourceOrderNumber>" & EXISTING_ORDER_NUMBER & "</SourceOrderNumber>")

    '    Dim xmlDoc As New XmlDocument

    '    Dim chk As String

    '    Dim ws As New Cts.Oasys.Webservices.CtsOrderManager

    '    Dim log As New Cts.Oasys.Webservices.Log
    '    With log
    '        .Header = "C:\temp\"
    '    End With

    '    Dim dtbase As New Cts.Oasys.Webservices.TpWickes.RequestDecisionEngineDatabaseSQLTest
    '    Cts.Oasys.Webservices.TpWickes.RequestDecisionEngineDatabaseFactory.SetRequestDecisionEngineDatabase(dtbase)

    '    Cts.Oasys.Webservices.LogFactory.FactorySet(log)

    '    '  Act

    '    Dim str As String = ws.CTS_Refund_Create(CTSQODRefundCreateRequest_XML)
    '    xmlDoc.LoadXml(str)
    '    chk = xmlDoc.DocumentElement.ChildNodes(1).ChildNodes(0).InnerText


    '    ' Assert
    '    Assert.IsTrue(chk.Equals("false"))

    'End Sub
End Class
