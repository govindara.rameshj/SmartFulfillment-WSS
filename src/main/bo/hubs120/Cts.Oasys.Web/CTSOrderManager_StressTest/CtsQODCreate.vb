﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Diagnostics
Imports Cts.Oasys.Webservices.CtsOrderManager
Imports Cts.Oasys.Data

<TestClass()> Public Class CtsQODCreate

    Private Const NON_EXISTENT_ORDER_NUMBER As String = "012082"
    Private Const EXISTING_ORDER_NUMBER As String = ""
    Private Const CTSQODCreateRequest_XML As String = "<CTSQODCreateRequest> <DateTimeStamp>2011-07-20T12:18:07.52</DateTimeStamp> <OrderHeader> <Source>WO</Source> <SourceOrderNumber>700161</SourceOrderNumber> <RequiredDeliveryDate>2011-07-21</RequiredDeliveryDate> <DeliveryCharge>0</DeliveryCharge> <TotalOrderValue>192.97</TotalOrderValue> <SaleDate>2011-03-18</SaleDate> <CustomerAccountNo>164</CustomerAccountNo> <CustomerName>Mr Tom Jericho</CustomerName> <CustomerAddressLine1>11 Downing Street</CustomerAddressLine1> <CustomerAddressLine2></CustomerAddressLine2> <CustomerAddressTown>LONDON</CustomerAddressTown> <CustomerAddressLine4></CustomerAddressLine4> <CustomerPostcode>SW1A 2AA</CustomerPostcode> <DeliveryAddressLine1>19 Clangadanger Drove</DeliveryAddressLine1> <DeliveryAddressLine2></DeliveryAddressLine2> <DeliveryAddressTown>ABERDEEN</DeliveryAddressTown> <DeliveryAddressLine4></DeliveryAddressLine4> <DeliveryPostcode>SN5 7ET</DeliveryPostcode> <ContactPhoneHome>01604 744178</ContactPhoneHome> <ContactPhoneMobile></ContactPhoneMobile> <ContactPhoneWork></ContactPhoneWork> <ContactEmail>pnich@travisperkins.co.uk</ContactEmail> <DeliveryInstructions></DeliveryInstructions> <ToBeDelivered>true</ToBeDelivered> <CreateDeliveryChargeItem>false</CreateDeliveryChargeItem> <LoyaltyCardNumber></LoyaltyCardNumber> <ExtendedLeadTime>false</ExtendedLeadTime> <DeliveryContactName>Mr Spackmo Chestnut</DeliveryContactName> <DeliveryContactPhone>0121 6384256</DeliveryContactPhone> <RecordSaleOnly>false</RecordSaleOnly> </OrderHeader> <OrderLines> <OrderLine> <SourceOrderLineNo>1</SourceOrderLineNo> <ProductCode>190234</ProductCode> <ProductDescription>Nitrile Safety Wellington 10 Black</ProductDescription> <TotalOrderQuantity>2</TotalOrderQuantity> <LineValue>29.98</LineValue> <DeliveryChargeItem>false</DeliveryChargeItem> <SellingPrice>14.99</SellingPrice> <RecordSaleOnly>false</RecordSaleOnly> </OrderLine> <OrderLine> <SourceOrderLineNo>2</SourceOrderLineNo> <ProductCode>189571</ProductCode> <ProductDescription>Chopin Arch</ProductDescription> <TotalOrderQuantity>1</TotalOrderQuantity> <LineValue>162.99</LineValue> <DeliveryChargeItem>false</DeliveryChargeItem> <SellingPrice>162.99</SellingPrice> <RecordSaleOnly>true</RecordSaleOnly> </OrderLine> </OrderLines> </CTSQODCreateRequest>"

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub CTSQODCreate_SubmitMultipleRequestsCreateOrder_MemoryCheckedOK()
        Dim ramCounter As PerformanceCounter
        ramCounter = New PerformanceCounter("Memory", "Available MBytes")

        Dim initialMemoryFree As Single = 0, finalMemoryFree As Single = 0
        initialMemoryFree = ramCounter.NextValue

        ' Arrange
        Dim NewWebOrderNumber As Long = 0
        Dim ws As New Cts.Oasys.Webservices.CtsOrderManager
        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        com.CommandText = "Select MAX(SourceOrderNumber) from corhdr5"
                        NewWebOrderNumber = CInt(com.ExecuteValue)
                    Case DataProvider.Sql
                        com.CommandText = "Select MAX(SourceOrderNumber) from corhdr5"
                        NewWebOrderNumber = CInt(com.ExecuteValue)
                End Select
            End Using
        End Using

        ' Act
        For counter As Integer = 1 To 100
            NewWebOrderNumber += 1
            Dim requestXML As String = _
                        CTSQODCreateRequest_XML.Replace("<SourceOrderNumber>4494491</SourceOrderNumber>", _
                                                         "<SourceOrderNumber>" & NewWebOrderNumber.ToString & "</SourceOrderNumber>")
            ws.CTS_QOD_Create(requestXML)
        Next

        ' Assert
        finalMemoryFree = ramCounter.NextValue

        Assert.IsTrue(initialMemoryFree * 0.95 < finalMemoryFree)

    End Sub

End Class
