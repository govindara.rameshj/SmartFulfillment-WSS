﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Diagnostics
Imports Cts.Oasys.Webservices.CtsOrderManager

<TestClass()> Public Class CTSStatusNotification_Tests

    Private Const NON_EXISTENT_ORDER_NUMBER As String = "000502433"
    Private Const EXISTING_ORDER_NUMBER As String = "000577526"
    Private Const CTSStatusNotificationRequest_XML As String = "<CTSStatusNotificationRequest><DateTimeStamp>2011-07-19T16:27:50.635</DateTimeStamp><OMOrderNumber>000502433</OMOrderNumber><OrderStatus>200</OrderStatus><OrderHeader><SellingStoreCode>8100</SellingStoreCode>    <SellingStoreOrderNumber>1282</SellingStoreOrderNumber></OrderHeader><FulfilmentSites><FulfilmentSite><FulfilmentSite>8666</FulfilmentSite><FulfilmentSiteOrderNumber>11910</FulfilmentSiteOrderNumber><FulfilmentSiteIBTOutNumber>226833</FulfilmentSiteIBTOutNumber><OrderLines><OrderLine><SellingStoreLineNo>2</SellingStoreLineNo><OMOrderLineNo>2</OMOrderLineNo><ProductCode>530027</ProductCode><LineStatus>300</LineStatus></OrderLine></OrderLines></FulfilmentSite></FulfilmentSites></CTSStatusNotificationRequest>"
    Private Const MaximumRequests As Integer = 1

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub CTSStatusNotification_SubmitMultipleRequestsNonExistentOrder_MemoryCheckedOK()
        Dim ramCounter As PerformanceCounter
        ramCounter = New PerformanceCounter("Memory", "Available MBytes")

        Dim initialMemoryFree As Single = 0, finalMemoryFree As Single = 0
        initialMemoryFree = ramCounter.NextValue

        ' Arrange
        Dim requestXML As String = _
            CTSStatusNotificationRequest_XML.Replace("<OMOrderNumber>000502433</OMOrderNumber>", _
                                                     "<OMOrderNumber>" & NON_EXISTENT_ORDER_NUMBER & "</OMOrderNumber>")

        Dim ws As New Cts.Oasys.Webservices.CtsOrderManager

        ' Act
        For counter As Integer = 1 To MaximumRequests
            Dim str As String = ws.CTS_Status_Notification(requestXML)
        Next

        ' Assert
        finalMemoryFree = ramCounter.NextValue

        Assert.IsTrue(initialMemoryFree * 0.95 < finalMemoryFree)

    End Sub

    <TestMethod()> Public Sub CTSStatusNotification_SubmitMultipleRequestsExistingOrder_MemoryCheckedOK()
        Dim ramCounter As PerformanceCounter
        ramCounter = New PerformanceCounter("Memory", "Available MBytes")

        Dim initialMemoryFree As Single = 0, finalMemoryFree As Single = 0
        initialMemoryFree = ramCounter.NextValue

        ' Arrange
        Dim requestXML As String = _
            CTSStatusNotificationRequest_XML.Replace("<OMOrderNumber>000502433</OMOrderNumber>", _
                                                     "<OMOrderNumber>" & EXISTING_ORDER_NUMBER & "</OMOrderNumber>")

        Dim ws As New Cts.Oasys.Webservices.CtsOrderManager

        ' Act
        For counter As Integer = 1 To MaximumRequests
            ws.CTS_Status_Notification(requestXML)
        Next

        ' Assert
        finalMemoryFree = ramCounter.NextValue

        Assert.IsTrue(initialMemoryFree * 0.95 < finalMemoryFree)

    End Sub


End Class
