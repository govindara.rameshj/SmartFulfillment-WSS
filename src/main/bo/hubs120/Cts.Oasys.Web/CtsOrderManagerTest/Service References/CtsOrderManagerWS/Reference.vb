﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.1008
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Namespace CtsOrderManagerWS
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ServiceModel.ServiceContractAttribute([Namespace]:="http://orderManagerWebServices.tpplc.co.uk", ConfigurationName:="CtsOrderManagerWS.CtsOrderManagerSoap")>  _
    Public Interface CtsOrderManagerSoap
        
        'CODEGEN: Generating message contract since element name CTS_Fulfilment_RequestXMLInput from namespace http://orderManagerWebServices.tpplc.co.uk is not marked nillable
        <System.ServiceModel.OperationContractAttribute(Action:="http://orderManagerWebServices.tpplc.co.uk/CTS_Fulfilment_Request", ReplyAction:="*")>  _
        Function CTS_Fulfilment_Request(ByVal request As CtsOrderManagerWS.CTS_Fulfilment_RequestRequest) As CtsOrderManagerWS.CTS_Fulfilment_RequestResponse
        
        'CODEGEN: Generating message contract since element name CTS_Status_NotificationXMLInput from namespace http://orderManagerWebServices.tpplc.co.uk is not marked nillable
        <System.ServiceModel.OperationContractAttribute(Action:="http://orderManagerWebServices.tpplc.co.uk/CTS_Status_Notification", ReplyAction:="*")>  _
        Function CTS_Status_Notification(ByVal request As CtsOrderManagerWS.CTS_Status_NotificationRequest) As CtsOrderManagerWS.CTS_Status_NotificationResponse
        
        'CODEGEN: Generating message contract since element name CTS_Update_RefundXMLInput from namespace http://orderManagerWebServices.tpplc.co.uk is not marked nillable
        <System.ServiceModel.OperationContractAttribute(Action:="http://orderManagerWebServices.tpplc.co.uk/CTS_Update_Refund", ReplyAction:="*")>  _
        Function CTS_Update_Refund(ByVal request As CtsOrderManagerWS.CTS_Update_RefundRequest) As CtsOrderManagerWS.CTS_Update_RefundResponse
        
        'CODEGEN: Generating message contract since element name CTS_QOD_CreateXMLInput from namespace http://orderManagerWebServices.tpplc.co.uk is not marked nillable
        <System.ServiceModel.OperationContractAttribute(Action:="http://orderManagerWebServices.tpplc.co.uk/Process_Venda_Orders", ReplyAction:="*")>  _
        Function Process_Venda_Orders(ByVal request As CtsOrderManagerWS.Process_Venda_OrdersRequest) As CtsOrderManagerWS.Process_Venda_OrdersResponse
    End Interface
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class CTS_Fulfilment_RequestRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="CTS_Fulfilment_Request", [Namespace]:="http://orderManagerWebServices.tpplc.co.uk", Order:=0)>  _
        Public Body As CtsOrderManagerWS.CTS_Fulfilment_RequestRequestBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As CtsOrderManagerWS.CTS_Fulfilment_RequestRequestBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://orderManagerWebServices.tpplc.co.uk")>  _
    Partial Public Class CTS_Fulfilment_RequestRequestBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public CTS_Fulfilment_RequestXMLInput As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal CTS_Fulfilment_RequestXMLInput As String)
            MyBase.New
            Me.CTS_Fulfilment_RequestXMLInput = CTS_Fulfilment_RequestXMLInput
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class CTS_Fulfilment_RequestResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="CTS_Fulfilment_RequestResponse", [Namespace]:="http://orderManagerWebServices.tpplc.co.uk", Order:=0)>  _
        Public Body As CtsOrderManagerWS.CTS_Fulfilment_RequestResponseBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As CtsOrderManagerWS.CTS_Fulfilment_RequestResponseBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://orderManagerWebServices.tpplc.co.uk")>  _
    Partial Public Class CTS_Fulfilment_RequestResponseBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public CTS_Fulfilment_RequestXMLOutput As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal CTS_Fulfilment_RequestXMLOutput As String)
            MyBase.New
            Me.CTS_Fulfilment_RequestXMLOutput = CTS_Fulfilment_RequestXMLOutput
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class CTS_Status_NotificationRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="CTS_Status_Notification", [Namespace]:="http://orderManagerWebServices.tpplc.co.uk", Order:=0)>  _
        Public Body As CtsOrderManagerWS.CTS_Status_NotificationRequestBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As CtsOrderManagerWS.CTS_Status_NotificationRequestBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://orderManagerWebServices.tpplc.co.uk")>  _
    Partial Public Class CTS_Status_NotificationRequestBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public CTS_Status_NotificationXMLInput As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal CTS_Status_NotificationXMLInput As String)
            MyBase.New
            Me.CTS_Status_NotificationXMLInput = CTS_Status_NotificationXMLInput
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class CTS_Status_NotificationResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="CTS_Status_NotificationResponse", [Namespace]:="http://orderManagerWebServices.tpplc.co.uk", Order:=0)>  _
        Public Body As CtsOrderManagerWS.CTS_Status_NotificationResponseBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As CtsOrderManagerWS.CTS_Status_NotificationResponseBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://orderManagerWebServices.tpplc.co.uk")>  _
    Partial Public Class CTS_Status_NotificationResponseBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public CTS_Status_NotificationXMLOutput As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal CTS_Status_NotificationXMLOutput As String)
            MyBase.New
            Me.CTS_Status_NotificationXMLOutput = CTS_Status_NotificationXMLOutput
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class CTS_Update_RefundRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="CTS_Update_Refund", [Namespace]:="http://orderManagerWebServices.tpplc.co.uk", Order:=0)>  _
        Public Body As CtsOrderManagerWS.CTS_Update_RefundRequestBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As CtsOrderManagerWS.CTS_Update_RefundRequestBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://orderManagerWebServices.tpplc.co.uk")>  _
    Partial Public Class CTS_Update_RefundRequestBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public CTS_Update_RefundXMLInput As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal CTS_Update_RefundXMLInput As String)
            MyBase.New
            Me.CTS_Update_RefundXMLInput = CTS_Update_RefundXMLInput
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class CTS_Update_RefundResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="CTS_Update_RefundResponse", [Namespace]:="http://orderManagerWebServices.tpplc.co.uk", Order:=0)>  _
        Public Body As CtsOrderManagerWS.CTS_Update_RefundResponseBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As CtsOrderManagerWS.CTS_Update_RefundResponseBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://orderManagerWebServices.tpplc.co.uk")>  _
    Partial Public Class CTS_Update_RefundResponseBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public CTS_Update_Refund_XMLOutput As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal CTS_Update_Refund_XMLOutput As String)
            MyBase.New
            Me.CTS_Update_Refund_XMLOutput = CTS_Update_Refund_XMLOutput
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class Process_Venda_OrdersRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="Process_Venda_Orders", [Namespace]:="http://orderManagerWebServices.tpplc.co.uk", Order:=0)>  _
        Public Body As CtsOrderManagerWS.Process_Venda_OrdersRequestBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As CtsOrderManagerWS.Process_Venda_OrdersRequestBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://orderManagerWebServices.tpplc.co.uk")>  _
    Partial Public Class Process_Venda_OrdersRequestBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public CTS_QOD_CreateXMLInput As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal CTS_QOD_CreateXMLInput As String)
            MyBase.New
            Me.CTS_QOD_CreateXMLInput = CTS_QOD_CreateXMLInput
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class Process_Venda_OrdersResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="Process_Venda_OrdersResponse", [Namespace]:="http://orderManagerWebServices.tpplc.co.uk", Order:=0)>  _
        Public Body As CtsOrderManagerWS.Process_Venda_OrdersResponseBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As CtsOrderManagerWS.Process_Venda_OrdersResponseBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://orderManagerWebServices.tpplc.co.uk")>  _
    Partial Public Class Process_Venda_OrdersResponseBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public CTS_QOD_CreateXMLOutput As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal CTS_QOD_CreateXMLOutput As String)
            MyBase.New
            Me.CTS_QOD_CreateXMLOutput = CTS_QOD_CreateXMLOutput
        End Sub
    End Class
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")>  _
    Public Interface CtsOrderManagerSoapChannel
        Inherits CtsOrderManagerWS.CtsOrderManagerSoap, System.ServiceModel.IClientChannel
    End Interface
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")>  _
    Partial Public Class CtsOrderManagerSoapClient
        Inherits System.ServiceModel.ClientBase(Of CtsOrderManagerWS.CtsOrderManagerSoap)
        Implements CtsOrderManagerWS.CtsOrderManagerSoap
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String)
            MyBase.New(endpointConfigurationName)
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As String)
            MyBase.New(endpointConfigurationName, remoteAddress)
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
            MyBase.New(endpointConfigurationName, remoteAddress)
        End Sub
        
        Public Sub New(ByVal binding As System.ServiceModel.Channels.Binding, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
            MyBase.New(binding, remoteAddress)
        End Sub
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function CtsOrderManagerWS_CtsOrderManagerSoap_CTS_Fulfilment_Request(ByVal request As CtsOrderManagerWS.CTS_Fulfilment_RequestRequest) As CtsOrderManagerWS.CTS_Fulfilment_RequestResponse Implements CtsOrderManagerWS.CtsOrderManagerSoap.CTS_Fulfilment_Request
            Return MyBase.Channel.CTS_Fulfilment_Request(request)
        End Function
        
        Public Function CTS_Fulfilment_Request(ByVal CTS_Fulfilment_RequestXMLInput As String) As String
            Dim inValue As CtsOrderManagerWS.CTS_Fulfilment_RequestRequest = New CtsOrderManagerWS.CTS_Fulfilment_RequestRequest()
            inValue.Body = New CtsOrderManagerWS.CTS_Fulfilment_RequestRequestBody()
            inValue.Body.CTS_Fulfilment_RequestXMLInput = CTS_Fulfilment_RequestXMLInput
            Dim retVal As CtsOrderManagerWS.CTS_Fulfilment_RequestResponse = CType(Me,CtsOrderManagerWS.CtsOrderManagerSoap).CTS_Fulfilment_Request(inValue)
            Return retVal.Body.CTS_Fulfilment_RequestXMLOutput
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function CtsOrderManagerWS_CtsOrderManagerSoap_CTS_Status_Notification(ByVal request As CtsOrderManagerWS.CTS_Status_NotificationRequest) As CtsOrderManagerWS.CTS_Status_NotificationResponse Implements CtsOrderManagerWS.CtsOrderManagerSoap.CTS_Status_Notification
            Return MyBase.Channel.CTS_Status_Notification(request)
        End Function
        
        Public Function CTS_Status_Notification(ByVal CTS_Status_NotificationXMLInput As String) As String
            Dim inValue As CtsOrderManagerWS.CTS_Status_NotificationRequest = New CtsOrderManagerWS.CTS_Status_NotificationRequest()
            inValue.Body = New CtsOrderManagerWS.CTS_Status_NotificationRequestBody()
            inValue.Body.CTS_Status_NotificationXMLInput = CTS_Status_NotificationXMLInput
            Dim retVal As CtsOrderManagerWS.CTS_Status_NotificationResponse = CType(Me,CtsOrderManagerWS.CtsOrderManagerSoap).CTS_Status_Notification(inValue)
            Return retVal.Body.CTS_Status_NotificationXMLOutput
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function CtsOrderManagerWS_CtsOrderManagerSoap_CTS_Update_Refund(ByVal request As CtsOrderManagerWS.CTS_Update_RefundRequest) As CtsOrderManagerWS.CTS_Update_RefundResponse Implements CtsOrderManagerWS.CtsOrderManagerSoap.CTS_Update_Refund
            Return MyBase.Channel.CTS_Update_Refund(request)
        End Function
        
        Public Function CTS_Update_Refund(ByVal CTS_Update_RefundXMLInput As String) As String
            Dim inValue As CtsOrderManagerWS.CTS_Update_RefundRequest = New CtsOrderManagerWS.CTS_Update_RefundRequest()
            inValue.Body = New CtsOrderManagerWS.CTS_Update_RefundRequestBody()
            inValue.Body.CTS_Update_RefundXMLInput = CTS_Update_RefundXMLInput
            Dim retVal As CtsOrderManagerWS.CTS_Update_RefundResponse = CType(Me,CtsOrderManagerWS.CtsOrderManagerSoap).CTS_Update_Refund(inValue)
            Return retVal.Body.CTS_Update_Refund_XMLOutput
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function CtsOrderManagerWS_CtsOrderManagerSoap_Process_Venda_Orders(ByVal request As CtsOrderManagerWS.Process_Venda_OrdersRequest) As CtsOrderManagerWS.Process_Venda_OrdersResponse Implements CtsOrderManagerWS.CtsOrderManagerSoap.Process_Venda_Orders
            Return MyBase.Channel.Process_Venda_Orders(request)
        End Function
        
        Public Function Process_Venda_Orders(ByVal CTS_QOD_CreateXMLInput As String) As String
            Dim inValue As CtsOrderManagerWS.Process_Venda_OrdersRequest = New CtsOrderManagerWS.Process_Venda_OrdersRequest()
            inValue.Body = New CtsOrderManagerWS.Process_Venda_OrdersRequestBody()
            inValue.Body.CTS_QOD_CreateXMLInput = CTS_QOD_CreateXMLInput
            Dim retVal As CtsOrderManagerWS.Process_Venda_OrdersResponse = CType(Me,CtsOrderManagerWS.CtsOrderManagerSoap).Process_Venda_Orders(inValue)
            Return retVal.Body.CTS_QOD_CreateXMLOutput
        End Function
    End Class
End Namespace
