﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.uxInputText = New System.Windows.Forms.TextBox
        Me.uxOutputText = New System.Windows.Forms.TextBox
        Me.uxVendaButton = New System.Windows.Forms.Button
        Me.uxTab = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.uxTab.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'uxInputText
        '
        Me.uxInputText.Dock = System.Windows.Forms.DockStyle.Fill
        Me.uxInputText.Location = New System.Drawing.Point(3, 3)
        Me.uxInputText.Multiline = True
        Me.uxInputText.Name = "uxInputText"
        Me.uxInputText.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.uxInputText.Size = New System.Drawing.Size(797, 498)
        Me.uxInputText.TabIndex = 0
        Me.uxInputText.Text = resources.GetString("uxInputText.Text")
        Me.uxInputText.WordWrap = False
        '
        'uxOutputText
        '
        Me.uxOutputText.Dock = System.Windows.Forms.DockStyle.Fill
        Me.uxOutputText.Location = New System.Drawing.Point(3, 3)
        Me.uxOutputText.Multiline = True
        Me.uxOutputText.Name = "uxOutputText"
        Me.uxOutputText.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.uxOutputText.Size = New System.Drawing.Size(797, 498)
        Me.uxOutputText.TabIndex = 1
        Me.uxOutputText.WordWrap = False
        '
        'uxVendaButton
        '
        Me.uxVendaButton.Location = New System.Drawing.Point(829, 12)
        Me.uxVendaButton.Name = "uxVendaButton"
        Me.uxVendaButton.Size = New System.Drawing.Size(75, 23)
        Me.uxVendaButton.TabIndex = 2
        Me.uxVendaButton.Text = "Venda"
        Me.uxVendaButton.UseVisualStyleBackColor = True
        '
        'uxTab
        '
        Me.uxTab.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxTab.Controls.Add(Me.TabPage1)
        Me.uxTab.Controls.Add(Me.TabPage2)
        Me.uxTab.Location = New System.Drawing.Point(12, 12)
        Me.uxTab.Name = "uxTab"
        Me.uxTab.SelectedIndex = 0
        Me.uxTab.Size = New System.Drawing.Size(811, 530)
        Me.uxTab.TabIndex = 3
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.uxInputText)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(803, 504)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Input"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.uxOutputText)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(803, 504)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Output"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(916, 554)
        Me.Controls.Add(Me.uxTab)
        Me.Controls.Add(Me.uxVendaButton)
        Me.Name = "Form1"
        Me.Text = "Cts Order Manager Test"
        Me.uxTab.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents uxInputText As System.Windows.Forms.TextBox
    Friend WithEvents uxOutputText As System.Windows.Forms.TextBox
    Friend WithEvents uxVendaButton As System.Windows.Forms.Button
    Friend WithEvents uxTab As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage

End Class
