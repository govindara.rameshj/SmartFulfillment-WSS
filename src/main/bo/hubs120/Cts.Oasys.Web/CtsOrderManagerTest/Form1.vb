﻿Imports System.Xml
Imports System.Text
Imports System.IO

Public Class Form1

    Private Sub uxVendaButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxVendaButton.Click

        Using ws As New CtsOrderManagerWS.CtsOrderManagerSoapClient
            Dim output As String = ws.Process_Venda_Orders(uxInputText.Text)
            uxOutputText.Text = FormatXml(output)
            uxTab.SelectedIndex = 1
        End Using

    End Sub

    Private Function FormatXml(ByVal input As String) As String
        Dim xd As New XmlDocument
        xd.LoadXml(input)

        Dim sb As New StringBuilder
        Using sw As New StringWriter(sb)
            Using xtw As New XmlTextWriter(sw)
                xtw.Formatting = Formatting.Indented
                xd.WriteTo(xtw)
            End Using
        End Using

        Return sb.ToString
    End Function


End Class
