<System.Web.Services.WebService(Namespace:="http://orderManagerWebServices.tpplc.co.uk")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class CtsOrderManager
    Inherits System.Web.Services.WebService

    Private m_Error As Boolean = False
    Private _XMLError As String = String.Empty

#Region "Web Services"

    <WebMethod()> Public Function CTS_Fulfilment_Request(ByVal CTS_Fulfilment_RequestXMLInput As String) As <System.Xml.Serialization.XmlElementAttribute("CTS_Fulfilment_RequestXMLOutput")> String
        Dim Log As ILog

        Log = LogFactory.FactoryGet(WebServiceType.CtsFulfilmentRequest)
        Using Log
            LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsFulfilmentRequest: Start")
            LocalWrite(Log, LoggingInformation.StandardLogic, "Configuration XML File: " & ConfigXMLDoc.Path.ToString)
            LocalWrite(Log, LoggingInformation.StandardLogic, "Input XML Request: " & CTS_Fulfilment_RequestXMLInput)

            LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: Start")
            If IsMonitorPing(CTS_Fulfilment_RequestXMLInput) Then

                LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: Failed")
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsFulfilmentRequest: End")
                Log.Dispose()
                Return String.Empty

            End If
            LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: End")

            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: Start")

            Dim XML As XDocument = XDocument.Parse(CTS_Fulfilment_RequestXMLInput)
            Dim XSDPath As String = ConfigXMLDoc.SelectSingleNode("Configuration/XSDFiles/Path").InnerText
            Dim XSDName As String = ConfigXMLDoc.SelectSingleNode("Configuration/XSDFiles/CTSFulfilmentRequest/Input").InnerText

            If ValidateXML(XSDPath & XSDName, XML) Then

                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: Failed")
                LocalWrite(Log, LoggingInformation.StandardLogic, _XMLError)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsFulfilmentRequest: End")
                Log.Dispose()
                Return String.Empty

            End If
            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: End")

            Dim request As New CTSFulfilmentRequest
            Dim response As New CTSFulfilmentResponse
            Dim IBTHeader As Ibt.IBTHeader = Nothing
            Dim QodHeader As Qod.QodHeader = Nothing
            Dim FailedSave As Boolean = False

            Try
                LocalWrite(Log, LoggingInformation.StandardLogic, "Deserialise Request: Start")
                request = CType(request.Deserialise(CTS_Fulfilment_RequestXMLInput), CTSFulfilmentRequest)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Deserialise Request: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Create Response Object: Start")
                response.SetFieldsFromRequest(request)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create Response Object: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Store No: Start")
                Dim storeId As Integer = ThisStore.Id4
                LocalWrite(Log, LoggingInformation.StandardLogic, "Store No: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Valid Fulfilment Site: Start")
                If Not response.IsTargetFulfilmentSiteValid(storeId) Then

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Valid Fulfilment Site: No")
                    Exit Try

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Valid Fulfilment Site: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Load Existing QOD Order: Start")
                QodHeader = Qod.GetForOmOrderNumber(CInt(response.OrderHeader.OMOrderNumber.Value))
                If QodHeader Is Nothing AndAlso CInt(response.OrderHeader.SellingStoreCode.Value) = storeId Then

                    response.SetQodNotFound()
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Load Existing QOD Order: Failure - " & response.OrderHeader.OMOrderNumber.Value.ToString)
                    Exit Try

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Load Existing QOD Order: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Fulfilling Store Processing: Start")
                If CInt(response.OrderHeader.SellingStoreCode.Value) <> storeId Then

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Fulfilling Store Processing: Non Selling Store")

                    If QodHeader IsNot Nothing Then

                        response.SetFieldsFromQod(QodHeader)
                        response.SuccessFlag.Value = True
                        response.IBTOutNumber.Value = GetIbtOutNumber(QodHeader) '*****
                        response.FulfillingSiteOrderNumber.Value = QodHeader.Number

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Fulfilling Store Processing: QOD exists")
                        LocalWrite(Log, LoggingInformation.StandardLogic, response.Serialise)
                        Exit Try

                    End If

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Create New QOD Order Business Object: Start")
                    QodHeader = Qod.CreateFromCtsFulfillmentResponse(response)
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Create New QOD Order Business Object: Finish")

                    'Check that all SKUs exist in this store and create all of the order lines
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Process Lines: Start")
                    For Each responseOrderLine As CTSFulfilmentResponseOrderLine In response.OrderLines

                        If responseOrderLine.FulfilmentSite.Value = ThisStore.Id4.ToString Then

                            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Product: Start")
                            'Check this SKU
                            Dim sku As Stock.Stock = Stock.Stock.GetStock(responseOrderLine.ProductCode.Value)

                            If sku Is Nothing Then
                                responseOrderLine.SetProductCodeNotFound()
                                response.SuccessFlag.Value = False

                                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Product: Missing Product - " & responseOrderLine.ProductCode.Value)
                                LocalWrite(Log, LoggingInformation.StandardLogic, response.Serialise)
                                Exit Try
                            End If
                            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Product: End")

                        End If

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Create New QOD Line Business Object: Start")
                        Dim QodLine As Qod.QodLine = QodHeader.Lines.AddNew(responseOrderLine.ProductCode.Value, responseOrderLine.SellingPrice.Value)

                        QodLine.QtyOrdered = CInt(responseOrderLine.TotalOrderQuantity.Value)
                        QodLine.QtyTaken = CInt(responseOrderLine.QuantityTaken.Value)
                        QodLine.QtyToDeliver = QodLine.QtyOrdered - QodLine.QtyTaken
                        QodLine.DeliverySource = responseOrderLine.FulfilmentSite.Value
                        QodLine.IsDeliveryChargeItem = responseOrderLine.DeliveryChargeItem.Value
                        QodLine.SellingStoreId = QodHeader.SellingStoreId
                        QodLine.SellingStoreOrderId = QodHeader.SellingStoreOrderId

                        If CInt(responseOrderLine.LineStatus.Value) = Qod.State.Delivery.DeliveredNotifyOk Then
                            QodLine.DeliveryStatus = Qod.State.Delivery.DeliveredStatusOk
                        Else
                            QodLine.DeliveryStatus = CInt(responseOrderLine.LineStatus.Value)
                        End If

                        If QodLine.IsDeliveryChargeItem Then
                            QodLine.QtyToDeliver = 0
                            QodLine.SetDeliveredStatusOk()
                        Else
                            QodHeader.QtyOrdered += QodLine.QtyOrdered
                            QodHeader.QtyTaken += QodLine.QtyTaken
                        End If
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Create New QOD Line Business Object: End")

                    Next
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Process Lines: End")

                    'Add any instruction texts
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Process Delivery Instruction: Start")
                    If response.OrderHeader.DeliveryInstructions.InstructionLine IsNot Nothing Then
                        For Each outputText As CTSFulfilmentResponseOrderHeaderDeliveryInstructionsInstructionLine In response.OrderHeader.DeliveryInstructions.InstructionLine

                            Dim txt As Qod.QodText = QodHeader.Texts.FindOrCreate(outputText.LineNo.Value, Qod.QodTextType.Type.DeliveryInstruction)
                            txt.SellingStoreId = QodHeader.SellingStoreId
                            txt.SellingStoreOrderId = QodHeader.SellingStoreOrderId
                            If outputText.LineText.Value IsNot Nothing Then
                                txt.Text = outputText.LineText.Value
                            End If

                        Next
                    End If
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Process Delivery Instruction: End")

                    'Create the IBT out
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Create IBT Business Object: Start")
                    IBTHeader = New Ibt.IBTHeader(QodHeader)
                    response.IBTOutNumber.Value = IBTHeader.Number
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Create IBT Business Object: End")

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Fulfilling Store Processing: End")

                'Update the status
                LocalWrite(Log, LoggingInformation.StandardLogic, "Update QOD Business Object: Start")

                QodHeader.Lines.SetDeliveryStatus(Qod.State.Delivery.IbtOutAllStock, ThisStore.Id4)
                QodHeader.SetDeliveryStatus(Qod.State.Delivery.IbtOutAllStock)

                'Make sure the fulfilment sites and status have been updated
                response.OrderHeader.OrderStatus.Value = QodHeader.DeliveryStatus.ToString
                Dim Index As Integer = 0

                For Each ResponseOrderLine As CTSFulfilmentResponseOrderLine In response.OrderLines

                    QodHeader.Lines(Index).DeliverySource = ResponseOrderLine.FulfilmentSite.Value

                    If Not QodHeader.Lines(Index).IsDeliveryChargeItem AndAlso CInt(QodHeader.Lines(Index).DeliverySource) = storeId AndAlso IBTHeader IsNot Nothing Then
                        QodHeader.Lines(Index).DeliverySourceIbtOut = IBTHeader.Number
                    End If
                    ResponseOrderLine.LineStatus.Value = QodHeader.Lines(Index).DeliveryStatus.ToString
                    Index += 1
                Next
                LocalWrite(Log, LoggingInformation.StandardLogic, "Update QOD Business Object: End")

                response.SuccessFlag.Value = True
                'Try and save the data away to the database
                Try
                    Using con As New Connection
                        Dim LinesUpdated As Integer = 0

                        Try
                            LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Start")

                            con.StartTransaction()
                            LinesUpdated += QodHeader.PersistTree(con)
                            If IBTHeader IsNot Nothing Then
                                LinesUpdated += IBTHeader.Persist(con)
                            End If
                            con.CommitTransaction()
                            response.FulfillingSiteOrderNumber.Value = QodHeader.Number

                            LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Success")
                            LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: End")

                        Catch ex As Exception
                            con.RollbackTransaction()
                            response.SuccessFlag.Value = False

                            LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Failure - " & ex.ToString)
                            LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: End")

                            Throw ex

                        End Try

                    End Using

                Catch ex As Exception
                    response.SuccessFlag.Value = False
                    FailedSave = True

                Finally
                    LocalWrite(Log, LoggingInformation.StandardLogic, response.Serialise)

                End Try

            Catch ex As DeserialiseException
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: Deserialize Exception - " & ex.ToString)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsFulfilmentRequest: End")
                Log.Dispose()
                Return String.Empty

            Catch ex As InvalidFieldSettingException
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: Invalid Field Setting Exception - " & ex.ToString)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsFulfilmentRequest: End")
                Log.Dispose()
                Return String.Empty

            Catch ex As Exception
                response.SuccessFlag.Value = False
                response.SuccessFlag.ValidationStatus = ex.ToString

                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: General Exception - " & ex.ToString)
                LocalWrite(Log, LoggingInformation.StandardLogic, response.Serialise)

            End Try

            LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsFulfilmentRequest: End")
            Log.Dispose()
            Return response.Serialise

        End Using

    End Function

    <WebMethod()> Public Function CTS_Status_Notification(ByVal CTS_Status_NotificationXMLInput As String) As <System.Xml.Serialization.XmlElementAttribute("CTS_Status_NotificationXMLOutput")> String
        Dim Log As ILog

        Log = LogFactory.FactoryGet(WebServiceType.CtsStatusNotification)
        Using Log
            LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsStatusNotification: Start")
            LocalWrite(Log, LoggingInformation.StandardLogic, "Configuration XML File: " & ConfigXMLDoc.Path.ToString)
            LocalWrite(Log, LoggingInformation.StandardLogic, "Input XML Request: " & CTS_Status_NotificationXMLInput)


            LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: Start")
            If IsMonitorPing(CTS_Status_NotificationXMLInput) Then

                LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: Failed")
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsStatusNotification: End")
                Log.Dispose()
                Return String.Empty

            End If
            LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: End")

            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: Start")

            Dim XML As XDocument = XDocument.Parse(CTS_Status_NotificationXMLInput)
            Dim XSDPath As String = ConfigXMLDoc.SelectSingleNode("Configuration/XSDFiles/Path").InnerText
            Dim XSDName As String = ConfigXMLDoc.SelectSingleNode("Configuration/XSDFiles/CTSStatusNotification/Input").InnerText

            If ValidateXML(XSDPath & XSDName, XML) Then

                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: Failed")
                LocalWrite(Log, LoggingInformation.StandardLogic, _XMLError)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsStatusNotification: End")
                Log.Dispose()
                Return String.Empty

            End If
            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: End")

            Dim request As New CTSStatusNotificationRequest
            Dim response As New CTSStatusNotificationResponse
            Dim QodHeader As Qod.QodHeader = Nothing
            Dim IBTHeaderCollection As Ibt.IBTHeaderCollection = Nothing

            Dim DoneIBT As Boolean = False

            Try
                LocalWrite(Log, LoggingInformation.StandardLogic, "Deserialise Request: Start")
                request = CType(request.Deserialise(CTS_Status_NotificationXMLInput), CTSStatusNotificationRequest)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Deserialise Request: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Create Response Object: Start")
                If CInt(request.OrderStatus.Value) = Qod.State.Delivery.IbtInAllStock Then
                    DoneIBT = True
                End If
                response.SetFieldsFromRequest(request)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create Response Object: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Load Existing QOD Order: Start")
                QodHeader = Qod.GetForOmOrderNumber(CInt(response.OMOrderNumber.Value))
                If QodHeader Is Nothing Then

                    response.SetQodNotFound()
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Load Existing QOD Order: Failure - " & response.OMOrderNumber.Value.ToString)

                    Exit Try

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Load Existing QOD Order: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Create IBT Business Object: Start")
                If response.OrderHeader.SellingStoreCode.Value = ThisStore.Id4.ToString Then

                    If CInt(response.OrderStatus.Value) = Qod.State.Delivery.IbtInAllStock AndAlso Not DoneIBT Then

                        'Do the IBTs if needed
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Create IBT Business Object: New Object Created")
                        IBTHeaderCollection = New Ibt.IBTHeaderCollection(QodHeader, request)
                        If IBTHeaderCollection.Count > 0 Then

                            For Each FulfilmentSite As CTSStatusNotificationResponseFulfilmentSite In response.FulfilmentSites

                                If FulfilmentSite.FulfilmentSite.Value <> response.OrderHeader.SellingStoreCode.Value Then
                                    FulfilmentSite.SellingStoreIBTInNumber.Value = IBTHeaderCollection.GetIBTNumber(FulfilmentSite.FulfilmentSite.Value)
                                End If

                            Next

                        End If

                    End If

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create IBT Business Object: End")

                ' #
                ' OM sends a Status Notification immediately following a response from an Update Refund where a line
                ' has been fully refunded.  However, the Update Refund has just changed the statuses so the OM ststus
                ' values are out of date, so do not overwrite the status.  This situation is identifiable by the
                ' Suspended flag being on, on the header

                LocalWrite(Log, LoggingInformation.StandardLogic, "Update Refund And Immediate OM Status Notification Situtaion: Start")
                If Not QodHeader.IsSuspended Then
                    QodHeader.SetDeliveryStatus(CInt(response.OrderStatus.Value))
                End If

                For Each FulfilmentSite As CTSStatusNotificationResponseFulfilmentSite In response.FulfilmentSites

                    For Each OrderLine As CTSStatusNotificationResponseFulfilmentSiteOrderLine In FulfilmentSite.OrderLines

                        Dim QodLine As Qod.QodLine = QodHeader.Lines(CInt(OrderLine.SellingStoreLineNo.Value) - 1)

                        If FulfilmentSite.SellingStoreIBTInNumber.Value <> String.Empty Then
                            If CInt(FulfilmentSite.SellingStoreIBTInNumber.Value) > 0 Then
                                QodLine.SellingStoreIbtIn = FulfilmentSite.SellingStoreIBTInNumber.Value
                            End If
                        End If

                        If FulfilmentSite.FulfilmentSiteIBTOutNumber.Value <> String.Empty Then
                            If CInt(FulfilmentSite.FulfilmentSiteIBTOutNumber.Value) > 0 Then
                                QodLine.DeliverySourceIbtOut = FulfilmentSite.FulfilmentSiteIBTOutNumber.Value
                            End If
                        End If

                        QodLine.DeliverySource = FulfilmentSite.FulfilmentSite.Value
                        ' See  # comment above
                        If Not QodHeader.IsSuspended Then
                            QodLine.setDeliveryStatus(CInt(OrderLine.LineStatus.Value))
                        End If

                    Next
                Next
                LocalWrite(Log, LoggingInformation.StandardLogic, "Update Refund And Immediate OM Status Notification Situtaion: End")

                response.SuccessFlag.Value = True
                ' try and save the data away to the database
                Try
                    Using con As New Connection
                        Dim LinesUpdated As Integer = 0
                        Try
                            LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Start")

                            con.StartTransaction()
                            LinesUpdated += QodHeader.PersistTree(con)
                            If IBTHeaderCollection IsNot Nothing Then LinesUpdated += IBTHeaderCollection.Persist(con)
                            con.CommitTransaction()

                            LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Success")
                            LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: End")
                            LocalWrite(Log, LoggingInformation.StandardLogic, response.Serialise)

                        Catch ex As Exception
                            con.RollbackTransaction()
                            response.SuccessFlag.Value = False

                            LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Failure - " & ex.ToString)
                            LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: End")

                            Throw ex

                        End Try
                    End Using

                Catch ex As Exception

                End Try

            Catch ex As DeserialiseException
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: Deserialize Exception - " & ex.ToString)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsStatusNotification: End")
                Log.Dispose()
                Return String.Empty

            Catch ex As InvalidFieldSettingException
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: Invalid Field Setting Exception - " & ex.ToString)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsStatusNotification: End")
                Log.Dispose()
                Return String.Empty

            Catch ex As Exception
                response.SuccessFlag.Value = False
                response.SuccessFlag.ValidationStatus = ex.ToString

                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: General Exception - " & ex.ToString)
                LocalWrite(Log, LoggingInformation.StandardLogic, response.Serialise)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsStatusNotification: End")
                Log.Dispose()
                Return response.Serialise

            End Try

            LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsStatusNotification: End")
            Log.Dispose()
            Return response.Serialise

        End Using

    End Function

    <WebMethod()> Public Function CTS_Update_Refund(ByVal CTS_Update_RefundXMLInput As String) As <System.Xml.Serialization.XmlElementAttribute("CTS_Update_RefundXMLOutput")> String
        Dim Log As ILog

        Log = LogFactory.FactoryGet(WebServiceType.CtsUpdateRefund)
        Using Log
            LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsUpdateRefund: Start")
            LocalWrite(Log, LoggingInformation.StandardLogic, "Configuration XML File: " & ConfigXMLDoc.Path.ToString)
            LocalWrite(Log, LoggingInformation.StandardLogic, "Input XML Request: " & CTS_Update_RefundXMLInput)

            LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: Start")
            If IsMonitorPing(CTS_Update_RefundXMLInput) Then

                LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: Failed")
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsUpdateRefund: End")
                Log.Dispose()
                Return String.Empty

            End If
            LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: End")

            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: Start")

            Dim XML As XDocument = XDocument.Parse(CTS_Update_RefundXMLInput)
            Dim XSDPath As String = ConfigXMLDoc.SelectSingleNode("Configuration/XSDFiles/Path").InnerText
            Dim XSDName As String = ConfigXMLDoc.SelectSingleNode("Configuration/XSDFiles/CTSUpdateRefund/Input").InnerText

            If ValidateXML(XSDPath & XSDName, XML) Then

                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: Failed")
                LocalWrite(Log, LoggingInformation.StandardLogic, _XMLError)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsUpdateRefund: End")
                Log.Dispose()
                Return String.Empty

            End If
            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: End")

            Dim request As New CTSUpdateRefundRequest
            Dim response As New CTSUpdateRefundResponse
            Dim IBTHeaderCollection As New Ibt.IBTHeaderCollection
            Dim IBTHeader As Ibt.IBTHeader = Nothing
            Dim qodHeader As Qod.QodHeader = Nothing
            Dim FailedSave As Boolean = False

            Try
                LocalWrite(Log, LoggingInformation.StandardLogic, "Deserialise Request: Start")
                request = CType(request.Deserialise(CTS_Update_RefundXMLInput), CTSUpdateRefundRequest)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Deserialise Request: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Create Response Object: Start")
                response.SetFieldsFromRequest(request)
                response.DateTimeStamp.Value = request.DateTimeStamp
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create Response Object: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Store No: Start")
                Dim StoreId As Integer = ThisStore.Id4
                LocalWrite(Log, LoggingInformation.StandardLogic, "Store No: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Load Existing QOD Order: Start")
                qodHeader = Qod.GetForOmOrderNumber(CInt(response.OMOrderNumber.Value))
                If qodHeader Is Nothing Then

                    response.SetQodNotFound()
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Load Existing QOD Order: Failure - " & response.OMOrderNumber.Value)
                    Exit Try

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Load Existing QOD Order: End")

                'CR0031 - We need to allow a venda refund if the eStore warehouse store number 076 parameter, is the 
                'current(store) and the selling store is equal to the eStore SellingStore store 120 parameter
                'and the deliver status is greater or equal to 900 the allow the refund to progress.
                LocalWrite(Log, LoggingInformation.StandardLogic, "EStore Warehouse Refund: Start")

                Dim blnEstoreRefundReject As Boolean = True


                LocalWrite(Log, LoggingInformation.StandardLogic, "EStore Warehouse Refund: Store 076 Test")
                If Core.System.Parameter.EstoreWarehouseNumber = ThisStore.Id4 Then
                    If qodHeader.DeliveryStatus >= Qod.State.Delivery.DeliveredCreated And Core.System.Parameter.EstoreSellingStoreNumber = qodHeader.SellingStoreId Then

                        LocalWrite(Log, LoggingInformation.StandardLogic, "EStore Warehouse Refund: Store 076 Test Negative")
                        blnEstoreRefundReject = False

                    End If
                End If

                If blnEstoreRefundReject = True Then 'CR0031 - This will only be false if estore (076) is the fulfiller and the delivery status for the order is >= 900
                    If Not qodHeader.IsRefundable Then

                        response.SetQodNotRefundable()
                        LocalWrite(Log, LoggingInformation.StandardLogic, "EStore Warehouse Refund: Order Not Refundable")
                        Exit Try

                    End If
                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "EStore Warehouse Refund: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Process Refunds: Start")
                For Each OrderRefund As CTSUpdateRefundResponseOrderRefund In response.OrderRefunds

                    LocalWrite(Log, LoggingInformation.StandardLogic, "IBT Processing: Start")
                    If Not OrderRefund.RefundStoreCode.Value = StoreId.ToString Then
                        LocalWrite(Log, LoggingInformation.StandardLogic, "This Is Not Refund Store: Create IBT Business Object")
                        'This is not the refund store, so do an IBT in!
                        IBTHeader = New Ibt.IBTHeader(request) 'Ref 807/792 MO'C 24/05/2011 - Don't process IBT when fullfilmentSite is nothing
                        If IBTHeader.Lines.Count > 0 Then  'We have a cancelled qantity to IBT
                            For Each FulfilmentSite As CTSUpdateRefundResponseOrderRefundFulfilmentSite In OrderRefund.FulfilmentSites
                                If FulfilmentSite.FulfilmentSiteCode.Value = StoreId.ToString Then
                                    FulfilmentSite.FulfilmentSiteIBTInNumber = IBTHeader.Number
                                End If
                            Next
                            IBTHeaderCollection.Add(IBTHeader)
                        Else 'All refund lines for this store were returned, not cancelled, so no IBT is needed
                            IBTHeader = Nothing
                        End If
                    End If
                    LocalWrite(Log, LoggingInformation.StandardLogic, "IBT Processing: End")

                    LocalWrite(Log, LoggingInformation.StandardLogic, "QOD Processing: Start")
                    For Each ResponseRefundLine As CTSUpdateRefundResponseOrderRefundRefundLine In OrderRefund.RefundLines

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Find QOD Refund Order: Start")
                        Dim QodRefund As Qod.QodRefund = qodHeader.Refunds.Find(CInt(ResponseRefundLine.SellingStoreLineNo.Value), CInt(OrderRefund.RefundStoreCode.Value), OrderRefund.RefundDate.Value, OrderRefund.RefundTill.Value, OrderRefund.RefundTransaction.Value)
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Find QOD Refund Order: End")

                        If QodRefund Is Nothing Then

                            LocalWrite(Log, LoggingInformation.StandardLogic, "QOD Refund Order Not Found")
                            QodRefund = qodHeader.Refunds.AddNew
                            QodRefund.OrderNumber = qodHeader.Number
                            QodRefund.Number = ResponseRefundLine.SellingStoreLineNo.Value
                            QodRefund.RefundStoreId = CInt(OrderRefund.RefundStoreCode.Value)
                            QodRefund.RefundDate = OrderRefund.RefundDate.Value
                            QodRefund.RefundTill = OrderRefund.RefundTill.Value
                            QodRefund.RefundTransaction = OrderRefund.RefundTransaction.Value
                            QodRefund.SkuNumber = ResponseRefundLine.ProductCode.Value
                            QodRefund.Price = ResponseRefundLine.RefundLineValue.Value
                            QodRefund.QtyCancelled = CInt(ResponseRefundLine.QuantityCancelled.Value)
                            QodRefund.QtyReturned = CInt(ResponseRefundLine.QuantityReturned.Value)
                            QodRefund.RefundStatus = CInt(IIf(CInt(ResponseRefundLine.RefundLineStatus.Value) = Qod.State.Refund.NotifyOk, Qod.State.Refund.StatusUpdateOk, ResponseRefundLine.RefundLineStatus.Value))

                            If CInt(qodHeader.Lines((CInt(QodRefund.Number) - 1)).DeliverySource) = StoreId AndAlso IBTHeader IsNot Nothing Then
                                QodRefund.FulfillingStoreIbtIn = IBTHeader.Number
                                QodRefund.SellingStoreIbtOut = IBTHeader.IBTNumber
                            End If

                            'MO'C - Update Refunded lines (basically do what the till does on the selling store data for remote sites)
                            Dim qodLine As Qod.QodLine = qodHeader.Lines.Find(QodRefund.Number)
                            If qodHeader.SellingStoreId <> StoreId Then 'The till will already have done this at the selling store
                                qodLine.QtyRefunded -= (QodRefund.QtyCancelled + QodRefund.QtyReturned)
                                qodLine.QtyToDeliver -= QodRefund.QtyCancelled
                                'Have to check if QtyTaken is greater than zero as delivery charge lines are never taken!
                                If qodLine.QtyTaken > 0 Then
                                    qodLine.QtyTaken -= QodRefund.QtyReturned
                                End If
                                If Not qodLine.IsDeliveryChargeItem Then
                                    qodHeader.QtyRefunded -= (QodRefund.QtyCancelled + QodRefund.QtyReturned)
                                    qodHeader.QtyTaken -= QodRefund.QtyReturned
                                End If
                            End If
                            qodHeader.SetRefundStatus(QodRefund.RefundStatus)

                        Else

                            LocalWrite(Log, LoggingInformation.StandardLogic, "QOD Refund Order Found")
                            QodRefund.RefundStatus = CInt(IIf(CInt(ResponseRefundLine.RefundLineStatus.Value) = Qod.State.Refund.NotifyOk, Qod.State.Refund.StatusUpdateOk, ResponseRefundLine.RefundLineStatus.Value))

                        End If

                    Next
                    LocalWrite(Log, LoggingInformation.StandardLogic, "QOD Processing: End")

                Next
                LocalWrite(Log, LoggingInformation.StandardLogic, "Process Refunds: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Additional Processing: Start")
                If qodHeader.SellingStoreId <> StoreId Then
                    If qodHeader.QtyOrdered + qodHeader.QtyRefunded = 0 Then
                        qodHeader.Status = "1"
                    ElseIf qodHeader.QtyRefunded < 0 Then
                        qodHeader.Status = "2"
                    End If
                    qodHeader.RevisionNumber += 1
                End If
                ' Referrals 631, 711, 737, 738, 739 & 740
                ' Reworked this area for above referrals
                'Check to see if we need to suspend the order at this store
                Dim suspensionRequired As Boolean = False

                For Each QodRefund As Qod.QodRefund In qodHeader.Refunds
                    Dim QodLine As Qod.QodLine = qodHeader.Lines.Find(QodRefund.Number)  ' Get existing line for the refund

                    ' Flag it up if the line is being fulfilled by this store and a cancellation has been made by the refund against the line
                    If QodLine.DeliverySource = StoreId.ToString AndAlso QodRefund.QtyCancelled > 0 Then
                        ' If cancellation means that there is nothing left to deliver on that line, set its status to complete (999)
                        If QodLine.QtyToDeliver = 0 Then
                            QodLine.DeliveryStatus = Qod.State.Delivery.DeliveredStatusOk
                        End If
                        ' Referral 759 - move this condition to here (from outside the whole loop and subsequent suspendedRequired check) so
                        ' that above code (set line status to 999 when fully refunded) is run, no matter what the line's original delivery
                        ' status.  Flag up a Suspension only if original statuses warrant it, i.e.
                        'If the Order is ready for dispatch when the refund happened - somewhere between 500-599 and 800-899 is allowed - Referral 737
                        If (qodHeader.Lines.DeliveryStatus(StoreId) >= Qod.State.Delivery.PickingListCreated _
                        AndAlso qodHeader.Lines.DeliveryStatus(StoreId) <= Qod.State.Delivery.PickingStatusOk) _
                        OrElse (qodHeader.Lines.DeliveryStatus(StoreId) >= Qod.State.Delivery.UndeliveredCreated _
                        AndAlso qodHeader.Lines.DeliveryStatus(StoreId) <= Qod.State.Delivery.UndeliveredStatusOk) Then
                            ' Remember a line (in picking or failed delivery) has had a cancellation against it (part or full)
                            suspensionRequired = True
                        End If
                    End If
                Next
                ' If have cancelled part/all of a line, suspend the order and reset the delivery statuses for other lines being fulfilled by this store,
                ' but NOT if now all the lines for this store are set to DeliveredStatusOk
                If suspensionRequired AndAlso qodHeader.Lines.DeliveryStatus(StoreId) < Qod.State.Delivery.DeliveredStatusOk Then
                    ' If one line that this store is delivering has changed, then all lines that this store is delivering should change
                    ' Set all the delivery source lines for this store back to IbtInAllStock/ReceiptStatusOk (depending on whether delivery/collection) - Referral 740
                    qodHeader.Lines.DeliveryStatus(StoreId) = CType(IIf(qodHeader.IsForDelivery, Qod.State.Delivery.IbtInAllStock, Qod.State.Delivery.ReceiptStatusOk), Qod.State.Delivery)
                    ' Suspend the order at this store
                    qodHeader.IsSuspended = True
                End If
                ' Adjust the header status according to the changed in the lines status's
                qodHeader.SetDeliveryStatusAsLines(StoreId)
                ' End of Referrals 631, 711, 737, 738, 739 & 740
                For Each OrderRefund As CTSUpdateRefundResponseOrderRefund In response.OrderRefunds
                    OrderRefund.SuccessFlag = True
                Next
                LocalWrite(Log, LoggingInformation.StandardLogic, "Additional Processing: End")

                'Try writing the data away to the database
                Try
                    Using con As New Connection
                        Dim LinesUpdated As Integer = 0
                        Try
                            LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Start")

                            con.StartTransaction()
                            LinesUpdated += qodHeader.PersistTree(con)
                            If IBTHeaderCollection.Count > 0 Then
                                LinesUpdated += IBTHeaderCollection.Persist(con)
                            End If
                            con.CommitTransaction()

                            LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Success")
                            LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: End")
                            LocalWrite(Log, LoggingInformation.StandardLogic, response.Serialise)

                        Catch ex As Exception
                            con.RollbackTransaction()

                            LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Failure - " & ex.ToString)
                            LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: End")

                            Throw ex

                        End Try
                    End Using

                Catch
                    FailedSave = True

                End Try

            Catch ex As DeserialiseException
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: Deserialize Exception - " & ex.ToString)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsUpdateRefund: End")
                Log.Dispose()
                Return String.Empty

            Catch ex As InvalidFieldSettingException
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: Invalid Field Setting Exception - " & ex.ToString)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsUpdateRefund: End")
                Log.Dispose()
                Return String.Empty

            Catch ex As Exception
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: General Exception - " & ex.ToString)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsUpdateRefund: End")
                Log.Dispose()
                Return String.Empty

            End Try

            LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsUpdateRefund: End")
            Log.Dispose()
            Return response.Serialise

        End Using

    End Function

    <WebMethod()> Public Function CTS_QOD_Create(ByVal CTS_QOD_CreateXMLInput As String) As <System.Xml.Serialization.XmlElementAttribute("CTS_QOD_CreateXMLOutput")> String
        Dim Log As ILog

        Log = LogFactory.FactoryGet(WebServiceType.CtsQoqCreate)
        Using Log
            Dim XML As XDocument
            Dim XSDPath As String
            Dim XSDName As String

            Dim VendaSaleRequest As New CTSQODCreateRequest
            Dim Response As New CTSQODCreateResponse
            Dim QodHeader As Qod.QodHeader = Nothing
            Dim SaleHeader As Qod.SaleHeader = Nothing
            Dim LinesUpdated As Integer

            Dim StockExist As Stock.Stock

            Dim blnErrorOccurred As Boolean
            Dim blnValidationPassed As Boolean

            Dim TempId As String
            Dim blnHomeDeliveryOrder As Boolean
            Dim dtmSystemDate As Date

            LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsQoqCreate: Start")
            LocalWrite(Log, LoggingInformation.StandardLogic, "Configuration XML File: " & ConfigXMLDoc.Path.ToString)
            LocalWrite(Log, LoggingInformation.StandardLogic, "Input XML Request: " & CTS_QOD_CreateXMLInput)

            LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: Start")
            If IsMonitorPing(CTS_QOD_CreateXMLInput) Then

                LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: Failed")
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsQoqCreate: End")
                Log.Dispose()
                Return String.Empty

            End If
            LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: End")

            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: Start")
            XML = XDocument.Parse(CTS_QOD_CreateXMLInput)
            XSDPath = ConfigXMLDoc.SelectSingleNode("Configuration/XSDFiles/Path").InnerText
            XSDName = ConfigXMLDoc.SelectSingleNode("Configuration/XSDFiles/CTSQODCreate/Input").InnerText

            If ValidateXML(XSDPath & XSDName, XML) Then

                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: Failed")
                LocalWrite(Log, LoggingInformation.StandardLogic, _XMLError)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsQoqCreate: End")
                Log.Dispose()
                Return String.Empty

            End If
            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: End")

            'properly formed xml which has been validated against the xsd - validate actual values in xml
            blnErrorOccurred = False
            Try
                LocalWrite(Log, LoggingInformation.StandardLogic, "Deserialise Request: Start")
                VendaSaleRequest = CType(VendaSaleRequest.Deserialise(CTS_QOD_CreateXMLInput), CTSQODCreateRequest)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Deserialise Request: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Create Response Object: Start")
                dtmSystemDate = Now.Date
                Response.SetFieldsFromRequest(VendaSaleRequest, dtmSystemDate)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create Response Object: End")

                'change request - CR0026
                'check that overnight routines are not running - NITMAS
                LocalWrite(Log, LoggingInformation.StandardLogic, "Accept Request: Start")
                If AcceptRequest Is Nothing Then AcceptRequest = New TpWickes.RequestDecisionEngine
                If AcceptRequest.AcceptRequest() = False Then

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Accept Request: Failed; Overnight Run In Progress")
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsQoqCreate: End")
                    Log.Dispose()
                    Return String.Empty

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Accept Request: End")

                'validate product sku
                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Product: Start")
                blnValidationPassed = True
                For Each Line As CTSQODCreateResponseOrderLine In Response.OrderLines

                    LocalWrite(Log, LoggingInformation.DetailedLogic, "Validate Product: Validate Product - " & Line.ProductCode.Value)
                    StockExist = Nothing
                    StockExist = Stock.Stock.GetStock(Line.ProductCode.Value)

                    If StockExist Is Nothing Then

                        Line.SetProductCodeNotFound()
                        blnValidationPassed = False
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Product: Product Missing - " & Line.ProductCode.Value)

                    End If
                Next
                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Product: End")

                'home delivery order - all lines have recordsaleonly = true
                LocalWrite(Log, LoggingInformation.StandardLogic, "Home Delivery Component In Order: Start")
                blnHomeDeliveryOrder = True
                For Each Line As CTSQODCreateResponseOrderLine In Response.OrderLines
                    If Line.RecordSaleOnly.Value = False And Line.DeliveryChargeItem.Value = False Then

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Home Delivery Component In Order: No")
                        blnHomeDeliveryOrder = False
                        Exit For

                    End If
                Next

                If blnHomeDeliveryOrder = True Then LocalWrite(Log, LoggingInformation.StandardLogic, "Home Delivery Component In Order: Yes")
                LocalWrite(Log, LoggingInformation.StandardLogic, "Home Delivery Component In Order: End")

                'home delivery validation
                If blnHomeDeliveryOrder = True Then

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Home Delivery Component In Order: Start")
                    'validate that (delivery charge = 0 and recordsaleonly = true) or (delivery charge > 0 and recordsaleonly = false)
                    If (Response.OrderHeader.DeliveryCharge.Value = 0 And Response.OrderHeader.RecordSaleOnly.Value = False) Or _
                       (Response.OrderHeader.DeliveryCharge.Value > 0 And Response.OrderHeader.RecordSaleOnly.Value = True) Then

                        Response.OrderHeader.HomeDeliveryDeliveryCharge()
                        Response.OrderHeader.HomeDeliveryRecordSaleOnly()
                        blnValidationPassed = False

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Home Delivery Component In Order: Failure - Inconsistent RecordSaleOnly And DeliveryCharge")

                    End If
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Home Delivery Component In Order: End")

                End If

                ' Referral 814
                ' Moved this 'If' outside of HomeDelivery condition as check is not specific to Home Deliveries (just the debug/log messages that correspond to the type of order)
                'validate that no sale exists with the same venda order no
                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Venda Order No Is Unique: Start")
                If Qod.VendaOrderNoInUse(Response.OrderHeader.SourceOrderNumber.Value) = True Then

                    Response.OrderHeader.WebOrderInUse()
                    blnValidationPassed = False
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Venda Order No Is Unique: Failure - Venda Order Number (" & Response.OrderHeader.SourceOrderNumber.Value & ") In Use")

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Venda Order No Is Unique: End")

            Catch exc As DeserialiseException
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: Deserialize Exception - " & exc.ToString)
                blnErrorOccurred = True

            Catch exc As InvalidFieldSettingException
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: Invalid Field Setting Exception - " & exc.ToString)
                blnErrorOccurred = True

            Catch exc As Exception
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: General Exception - " & exc.ToString)
                blnErrorOccurred = True

            End Try

            If blnErrorOccurred = False Then
                If blnValidationPassed = False Then

                    Response.SuccessFlag.Value = False

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Validation Failure")
                    LocalWrite(Log, LoggingInformation.StandardLogic, Response.Serialise)
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsQoqCreate: End")
                    Log.Dispose()
                    Return Response.Serialise

                End If
            Else

                LocalWrite(Log, LoggingInformation.StandardLogic, "Exception Failure")
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsQoqCreate: End")
                Log.Dispose()
                Return String.Empty

            End If

            'create venda sale
            blnErrorOccurred = False
            Try
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create New Transaction Number: Start")
                TempId = GetNewId()
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create New Transaction Number: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Create New Sale Business Object: Start")
                SaleHeader = New Qod.SaleHeader(Response, Left(TempId, 2), Right(TempId, 4))
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create New Sale Business Object: End")

                If blnHomeDeliveryOrder = False Then

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Create New QOD Business Object: Start")
                    QodHeader = New Qod.QodHeader(Response, Left(TempId, 2), Right(TempId, 4))
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Create New QOD Business Object: End")

                End If

                Response.SuccessFlag.Value = True

                'perist to database
                Using con As New Connection
                    Try
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Start")
                        con.StartTransaction()
                        If blnHomeDeliveryOrder = False Then
                            LinesUpdated += QodHeader.PersistTree(con)
                            SaleHeader.OrderNumber = QodHeader.Number
                        End If

                        LinesUpdated += SaleHeader.PersistTree(con)

                        LinesUpdated += SaleHeader.UpdatecashierBalancingTables(con)

                        con.CommitTransaction()
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Success")
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: End")

                        If blnHomeDeliveryOrder = False Then Response.OrderHeader.StoreOrderNumber.Value = QodHeader.Number
                        LocalWrite(Log, LoggingInformation.StandardLogic, Response.Serialise)

                    Catch ex As Exception

                        con.RollbackTransaction()
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Failure - " & ex.ToString)
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: End")

                        Response.SuccessFlag.Value = False
                        blnErrorOccurred = True

                    End Try
                End Using

            Catch ex As Exception
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: General Exception - " & ex.ToString)
                blnErrorOccurred = True

            Finally
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsQoqCreate: End")
                Log.Dispose()
            End Try

            If blnErrorOccurred = False Then
                Return Response.Serialise
            Else
                Return String.Empty
            End If
        End Using
    End Function

    <WebMethod()> Public Function CTS_Refund_Create(ByVal CTS_Refund_CreateXMLInput As String) As <System.Xml.Serialization.XmlElementAttribute("CTS_Refund_CreateXMLOutput")> String
        Dim Log As ILog

        Log = LogFactory.FactoryGet(WebServiceType.CtsRefundCreate)
        Using Log
            Dim XML As XDocument
            Dim XSDPath As String
            Dim XSDName As String

            Dim VendaRefundRequest As New CTSRefundCreateRequest
            Dim Response As New CTSRefundCreateResponse

            Dim ExistingQODOrderCollection As New Qod.QodHeaderCollection
            Dim ExistingQODOrder As Qod.QodHeader
            Dim ExistingSaleCollection As New Qod.SaleHeaderCollection
            Dim ExistingSale As Qod.SaleHeader

            Dim SaleHeader As Qod.SaleHeader
            Dim TempId As String
            Dim blnHomeDeliveryOrder As Boolean
            Dim dtmSystemDate As Date

            LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsRefundCreate: Start")
            LocalWrite(Log, LoggingInformation.StandardLogic, "Configuration XML File: " & ConfigXMLDoc.Path.ToString)
            LocalWrite(Log, LoggingInformation.StandardLogic, "Input XML Request: " & CTS_Refund_CreateXMLInput)

            LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: Start")
            If IsMonitorPing(CTS_Refund_CreateXMLInput) Then

                LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: Failed")
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsRefundCreate: End")
                Log.Dispose()
                Return String.Empty

            End If
            LocalWrite(Log, LoggingInformation.StandardLogic, "Ping Order Manager: End")

            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: Start")
            XML = XDocument.Parse(CTS_Refund_CreateXMLInput)
            XSDPath = ConfigXMLDoc.SelectSingleNode("Configuration/XSDFiles/Path").InnerText
            XSDName = ConfigXMLDoc.SelectSingleNode("Configuration/XSDFiles/CTSRefundCreate/Input").InnerText

            If ValidateXML(XSDPath & XSDName, XML) = True Then

                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: Failed")
                LocalWrite(Log, LoggingInformation.StandardLogic, _XMLError)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsRefundCreate: End")
                Log.Dispose()
                Return String.Empty

            End If
            LocalWrite(Log, LoggingInformation.StandardLogic, "Validate XML: End")

            Try
                LocalWrite(Log, LoggingInformation.StandardLogic, "Deserialise Request: Start")
                VendaRefundRequest = CType(VendaRefundRequest.Deserialise(CTS_Refund_CreateXMLInput), CTSRefundCreateRequest)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Deserialise Request: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Create Response Object: Start")
                dtmSystemDate = Now.Date
                Response.SetFieldsFromRequest(VendaRefundRequest, dtmSystemDate)
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create Response Object: End")

                'change request - CR0026
                'check that overnight routines are not running - NITMAS
                LocalWrite(Log, LoggingInformation.StandardLogic, "Accept Request: Start")
                If AcceptRequest Is Nothing Then AcceptRequest = New TpWickes.RequestDecisionEngine
                If AcceptRequest.AcceptRequest() = False Then

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Accept Request: Failed; Overnight Run In Progress")
                    Return String.Empty

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Accept Request: End")

                'xml validated against xsd scheme
                Dim StockExist As Stock.Stock

                Dim blnAllExistingSKUsFound As Boolean
                Dim blnExistingSaleDeliveryLineFound As Boolean

                Dim blnExistingLineFound As Boolean
                Dim blnAllExistingLinesFound As Boolean

                'validate product sku
                blnAllExistingSKUsFound = True
                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Product: Start")
                For Each VendaRefundLine As CTSRefundCreateResponseRefundLine In Response.RefundLinesCollection

                    LocalWrite(Log, LoggingInformation.DetailedLogic, "Validate Product: Validate Product - " & VendaRefundLine.ProductCode.Value)
                    StockExist = Nothing
                    StockExist = Stock.Stock.GetStock(VendaRefundLine.ProductCode.Value)

                    If StockExist Is Nothing Then

                        blnAllExistingSKUsFound = False
                        VendaRefundLine.SetProductCodeNotFound()
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Product: Product Missing - " & VendaRefundLine.ProductCode.Value)

                    End If
                Next
                LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Product: End")

                If blnAllExistingSKUsFound = False Then

                    Response.SuccessFlag.Value = False
                    Response.RemoveRefundLineCollection()

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Product: Failure")
                    LocalWrite(Log, LoggingInformation.StandardLogic, Response.Serialise)
                    Return Response.Serialise

                End If

                'home delivery refund - store order number not present
                LocalWrite(Log, LoggingInformation.StandardLogic, "Standard Or Home Delivery Order: Start")
                blnHomeDeliveryOrder = False
                If Response.RefundHeader.StoreOrderNumber Is Nothing OrElse _
                   Response.RefundHeader.StoreOrderNumber.Value Is Nothing OrElse Response.RefundHeader.StoreOrderNumber.Value = String.Empty Then blnHomeDeliveryOrder = True
                LocalWrite(Log, LoggingInformation.StandardLogic, "Standard Or Home Delivery Order: End")

                Select Case blnHomeDeliveryOrder
                    Case False
                        'QOD
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Standard Order - Load Existing QOD Order: Start")
                        ExistingQODOrderCollection.LoadExistingOrder(Response.RefundHeader.StoreOrderNumber.Value)

                        'validate store order number exist
                        If ExistingQODOrderCollection.Count = 0 Then

                            Response.RefundHeader.StoreOrderNumberNotFound()
                            Response.SuccessFlag.Value = False
                            Response.RemoveRefundLineCollection()


                            LocalWrite(Log, LoggingInformation.StandardLogic, "Standard Order - Load Existing QOD Order: Failure - QOD Order No - " & Response.RefundHeader.StoreOrderNumber.Value)
                            LocalWrite(Log, LoggingInformation.StandardLogic, Response.Serialise)
                            Return Response.Serialise

                        End If
                        ExistingQODOrder = ExistingQODOrderCollection.Item(0)
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Standard Order - Load Existing QOD Order: End")

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Standard Order - Load Existing Sale Order: Start")
                        ExistingSaleCollection.LoadExistingSale(CDate(ExistingQODOrder.TranDate), ExistingQODOrder.TranTill, ExistingQODOrder.TranNumber)

                    Case True
                        'HD
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Home Delivery Order - Load Existing Sale Order: Start")
                        ExistingSaleCollection.LoadExistingSale(Response.RefundHeader.SourceOrderNumber.Value)

                        'Referral : 804
                        'Author   : Partha Dutta
                        'Date     : 11/04/2011

                        If ExistingSaleCollection.Count = 0 Then
                            'existing sale not found
                            'pad out existing web order no to 8 digits with leading zeros, try again
                            LocalWrite(Log, LoggingInformation.StandardLogic, "Home Delivery Order - Load Existing Sale Order 2nd Attempt With Leading Zeros: Start")

                            ExistingSaleCollection.LoadExistingSale(Response.RefundHeader.SourceOrderNumber.Value.PadLeft(8).Replace(" ", "0"))
                        End If

                End Select
                'validate sale exist
                If ExistingSaleCollection.Count = 0 Then
                    Select Case blnHomeDeliveryOrder
                        Case False
                            'QOD
                            Response.RefundHeader.SaleNotFound()
                        Case True
                            'HD
                            Response.RefundHeader.HomeDeliverySaleNotFound()
                    End Select
                    Response.SuccessFlag.Value = False
                    Response.RemoveRefundLineCollection()

                    LocalWrite(Log, LoggingInformation.StandardLogic, CStr(IIf(blnHomeDeliveryOrder = True, "Home Delivery Order", "Standard Order")) & " - Load Existing Sale Order: Failure")
                    LocalWrite(Log, LoggingInformation.StandardLogic, Response.Serialise)
                    Return Response.Serialise

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, CStr(IIf(blnHomeDeliveryOrder = True, "Home Delivery Order", "Standard Order")) & " - Load Existing Sale Order: End")


                ExistingSale = ExistingSaleCollection.Item(0)
                Select Case blnHomeDeliveryOrder
                    Case False
                        'QOD

                        'validate line exist, will either go to corlin or dlline depending on whether store line no exist
                        '
                        'line(s) with recordsaleonly set to true are held in dlline only
                        '
                        'primary key for dlline : DATE1, TILL, TRAN, NUMB
                        'since we are not going to be provided with line number for line(s) with recordsaleonly set to true
                        'we are going to assume that sku will be unique

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Standard Order - Validate Line(s) Exist: Start")
                        blnAllExistingLinesFound = True
                        For Each VendaLine As CTSRefundCreateResponseRefundLine In Response.RefundLinesCollection
                            blnExistingLineFound = False

                            If VendaLine.StoreLineNo IsNot Nothing AndAlso VendaLine.StoreLineNo.Value IsNot Nothing Then

                                LocalWrite(Log, LoggingInformation.DetailedLogic, "Standard Order - Validate Line(s) Exist: Check QOD Table CORLIN")
                                'find line in corlin
                                For Each QodLine As Qod.QodLine In ExistingQODOrder.Lines

                                    LocalWrite(Log, LoggingInformation.DetailedLogic, "Standard Order - Validate Line(s) Exist: Check QOD Table CORLIN" & _
                                                                                      " Order Number (" & Response.RefundHeader.StoreOrderNumber.Value.ToString.PadLeft(6, "0"c) & ")" & _
                                                                                      " Line Number (" & VendaLine.StoreLineNo.Value.ToString.PadLeft(4, "0"c) & ")" & _
                                                                                      " Product (" & VendaLine.ProductCode.Value & ")")

                                    If QodLine.OrderNumber = Response.RefundHeader.StoreOrderNumber.Value.ToString.PadLeft(6, "0"c) And _
                                       QodLine.Number = VendaLine.StoreLineNo.Value.ToString.PadLeft(4, "0"c) And _
                                       QodLine.SkuNumber = VendaLine.ProductCode.Value Then

                                        LocalWrite(Log, LoggingInformation.DetailedLogic, "Standard Order - Validate Line(s) Exist: CORLIN Line Found")
                                        blnExistingLineFound = True
                                        Exit For

                                    End If
                                Next
                                If blnExistingLineFound = False Then

                                    LocalWrite(Log, LoggingInformation.DetailedLogic, "Standard Order - Validate Line(s) Exist: CORLIN Line Not Found")
                                    VendaLine.StoreLineNotFound()
                                    blnAllExistingLinesFound = False

                                End If

                            Else

                                LocalWrite(Log, LoggingInformation.DetailedLogic, "Standard Order - Validate Line(s) Exist: Check Sale Table DLLINE")
                                'find line in dlline
                                For Each SaleLine As Qod.SaleLine In ExistingSale.SaleLines

                                    LocalWrite(Log, LoggingInformation.DetailedLogic, "Standard Order - Validate Line(s) Exist: Check Sale Table DLLINE" & _
                                                                                      " Product (" & VendaLine.ProductCode.Value & ")")

                                    If SaleLine.SKUNumber = VendaLine.ProductCode.Value Then

                                        LocalWrite(Log, LoggingInformation.DetailedLogic, "Standard Order - Validate Line(s) Exist: DLLINE Line Found")
                                        blnExistingLineFound = True
                                        Exit For

                                    End If
                                Next
                                If blnExistingLineFound = False Then

                                    LocalWrite(Log, LoggingInformation.DetailedLogic, "Standard Order - Validate Line(s) Exist: DLLINE Line Not Found")
                                    VendaLine.SetProductCodeNotFound()
                                    blnAllExistingLinesFound = False

                                End If

                            End If
                        Next

                    Case True
                        'HD

                        'primary key for dlline : DATE1, TILL, TRAN, NUMB
                        'since we are not going to be provided with line number we are going to assume that sku will be unique

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Home Delivery Order - Validate Line(s) Exist: Start")
                        blnAllExistingLinesFound = True
                        For Each VendaLine As CTSRefundCreateResponseRefundLine In Response.RefundLinesCollection
                            blnExistingLineFound = False

                            LocalWrite(Log, LoggingInformation.DetailedLogic, "Home Delivery Order - Validate Line(s) Exist: Check Sale Table DLLINE" & _
                                                                              " Product (" & VendaLine.ProductCode.Value & ")")


                            For Each SaleLine As Qod.SaleLine In ExistingSale.SaleLines
                                If SaleLine.SKUNumber = VendaLine.ProductCode.Value Then

                                    LocalWrite(Log, LoggingInformation.DetailedLogic, "Home Delivery Order - Validate Line(s) Exist: DLLINE Line Found")
                                    blnExistingLineFound = True
                                    Exit For

                                End If
                            Next
                            If blnExistingLineFound = False Then

                                LocalWrite(Log, LoggingInformation.DetailedLogic, "Home Delivery Order - Validate Line(s) Exist: DLLINE Line Not Found")
                                VendaLine.SetProductCodeNotFound()
                                blnAllExistingLinesFound = False

                            End If
                        Next

                End Select
                If blnAllExistingLinesFound = False Then

                    Response.SuccessFlag.Value = False
                    Response.RemoveRefundLineCollection()

                    LocalWrite(Log, LoggingInformation.StandardLogic, CStr(IIf(blnHomeDeliveryOrder = True, "Home Delivery Order", "Standard Order")) & " - Validate Line(s) Exist: Failure")
                    LocalWrite(Log, LoggingInformation.StandardLogic, Response.Serialise)
                    Return Response.Serialise

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, CStr(IIf(blnHomeDeliveryOrder = True, "Home Delivery Order", "Standard Order")) & " - Validate Line(s) Exist: End")

                'validate that delivery line exist if applicable
                If Response.RefundHeader.DeliveryChargeRefundValue.Value > 0 Then

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Delivery Line: Start")
                    blnExistingSaleDeliveryLineFound = False
                    For Each ExistingSaleLine As Qod.SaleLine In ExistingSale.SaleLines

                        If ExistingSaleLine.SKUNumber = "805111" Then
                            blnExistingSaleDeliveryLineFound = True
                            Exit For
                        End If

                    Next
                    If blnExistingSaleDeliveryLineFound = False Then

                        Response.RefundHeader.ExistingDeliveryNotFound()
                        Response.SuccessFlag.Value = False
                        Response.RemoveRefundLineCollection()

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Delivery Line: Failure")
                        LocalWrite(Log, LoggingInformation.StandardLogic, Response.Serialise)
                        Return Response.Serialise

                    End If
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Validate Delivery Line: End")

                End If

                LocalWrite(Log, LoggingInformation.StandardLogic, "Create New Transaction Number: Start")
                TempId = GetNewId()
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create New Transaction Number: End")

                LocalWrite(Log, LoggingInformation.StandardLogic, "Create New Sale Business Object: Start")
                SaleHeader = New Qod.SaleHeader(Response, Left(TempId, 2), Right(TempId, 4), blnHomeDeliveryOrder, ExistingQODOrder, ExistingSale)

                If blnHomeDeliveryOrder = False Then

                    LocalWrite(Log, LoggingInformation.StandardLogic, "Update Response Object: Start")
                    ExistingQODOrder.VendaRefund(Response, Left(TempId, 2), Right(TempId, 4))
                    LocalWrite(Log, LoggingInformation.StandardLogic, "Update Response Object: End")

                End If
                LocalWrite(Log, LoggingInformation.StandardLogic, "Create New Sale Business Object: End")

                Response.SuccessFlag.Value = True

                'write to database
                Using con As New Connection
                    Try

                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Start")
                        con.StartTransaction()
                        If blnHomeDeliveryOrder = False Then ExistingQODOrder.PersistTree(con)
                        SaleHeader.PersistTree(con)

                        SaleHeader.UpdatecashierBalancingTables(con)

                        con.CommitTransaction()
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Success")
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: End")

                        Response.RemoveRefundLineCollection()
                        LocalWrite(Log, LoggingInformation.StandardLogic, Response.Serialise)

                    Catch ex As Exception

                        con.RollbackTransaction()
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: Failure - " & ex.ToString)
                        LocalWrite(Log, LoggingInformation.StandardLogic, "Persist To Database: End")

                        Response.SuccessFlag.Value = False
                        Throw ex

                    End Try
                End Using

                Response.RemoveRefundLineCollection()
                Return Response.Serialise

            Catch ex As DeserialiseException
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: Deserialize Exception - " & ex.ToString)
                Return String.Empty

            Catch ex As InvalidFieldSettingException
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: Invalid Field Setting Exception - " & ex.ToString)
                Return String.Empty

            Catch ex As Exception
                LocalWrite(Log, LoggingInformation.StandardLogic, "Error State: General Exception - " & ex.ToString)
                Return String.Empty

            Finally
                LocalWrite(Log, LoggingInformation.StandardLogic, "Web Service - CtsRefundCreate: End")
                Log.Dispose()
            End Try
        End Using
    End Function

#End Region

#Region "Private Procedures And Functions"

    Private Function IsRequestXmlPopulated(ByVal requestXml As String) As Boolean
        If Not StringIsSomething(requestXml) Then Return False
        If IsMonitorPing(requestXml) Then Return False
        Return True
    End Function

    Private Function IsMonitorPing(ByVal input As String) As Boolean
        Return (input = "This is a monitor ping from Order Manager")
    End Function

    Private Function VendaEstoreImportOutput(ByVal input As String) As String

        Dim output As String = String.Empty

        Using proc As Diagnostics.Process = New System.Diagnostics.Process()
            proc.StartInfo.FileName = My.Settings.VendaEstoreImportPath
            proc.StartInfo.Arguments = input
            proc.StartInfo.RedirectStandardOutput = True
            proc.StartInfo.UseShellExecute = False
            proc.Start()
            output = proc.StandardOutput.ReadToEnd
            proc.Close()
        End Using

        Return output

    End Function

    Private Function GetPriceFromLine(ByVal Qod As Qod.QodHeader, ByVal LineNo As String) As Decimal

        For Each line As Qod.QodLine In Qod.Lines
            If line.Number = LineNo.ToString.PadLeft(4, "0"c) Then
                Return line.Price
            End If
        Next
        Return 0
    End Function

    Private Function GetDeliverySourceId(ByVal Qod As Qod.QodHeader, ByVal LineNo As String) As String

        For Each line As Qod.QodLine In Qod.Lines
            If line.Number = LineNo.ToString.PadLeft(4, "0"c) Then
                Return line.DeliverySource
            End If
        Next
        Return String.Empty
    End Function

    Private Function GetSellingStoreId(ByVal Qod As Qod.QodHeader, ByVal LineNo As String) As String

        For Each line As Qod.QodLine In Qod.Lines
            If line.Number = LineNo.ToString.PadLeft(4, "0"c) Then
                Return line.SellingStoreId.ToString
            End If
        Next
        Return String.Empty
    End Function

    Private Function GetIbtOutNumber(ByVal Qod As Qod.QodHeader) As String

        Dim StoreId As String = ThisStore.Id4.ToString
        For Each line As Qod.QodLine In Qod.Lines
            If line.DeliverySource = StoreId Then
                Return line.DeliverySourceIbtOut
            End If
        Next
        Return String.Empty

    End Function

    Private Sub XSDErrors(ByVal o As Object, ByVal e As ValidationEventArgs)
        Using log As New Output("XMLLogs")
            log.Write("Invalid XML: " & e.Message)
            _XMLError = e.Message
            log.Dispose()
            m_Error = True
        End Using
    End Sub

    'Private Sub XSDErrors(ByVal o As Object, ByVal e As ValidationEventArgs)
    '    Using log As New Log("XMLLogs")
    '        log.Write("Invalid XML: " & e.Message)
    '        _XMLError = e.Message
    '        log.Dispose()
    '        m_Error = True
    '    End Using
    'End Sub

    Private Function ValidateXML(ByVal XSDPath As String, ByVal XML As XDocument) As Boolean

        Dim sr As StreamReader = New StreamReader(XSDPath)
        Dim xsdMarkup As XElement = XElement.Parse(sr.ReadToEnd)

        Dim schemas As XmlSchemaSet = New XmlSchemaSet()
        schemas.Add("", xsdMarkup.CreateReader)

        m_Error = False
        XML.Validate(schemas, AddressOf XSDErrors)

        sr.Close()
        xsdMarkup = Nothing

        Return m_Error
    End Function

    Private Function GetNewId() As String
        Dim strOrderNumber As String
        Dim intTillNo As Integer
        Dim intTranNo As Integer

        strOrderNumber = String.Empty
        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        'get order no
                        com.CommandText = "Select NEXT16 from SYSNUM where FKEY='01'"
                        strOrderNumber = CStr(com.ExecuteValue)

                        'update order number for next time
                        intTillNo = CType(strOrderNumber.Substring(0, 2), Integer)
                        intTranNo = CType(strOrderNumber.Substring(2, 4), Integer)

                        'till  - 2 digit number between 1 and 10 formatted with leading zero
                        'trans - 4 digit number between 1 and 9999 formatted with leading zeros

                        '010001 to 019999, 020001 to 029999, ........, 090001 to 099999, 100001 to 109999, 010001

                        If intTranNo < 9999 Then
                            intTranNo += 1
                        Else
                            intTranNo = 1

                            If intTillNo < 10 Then
                                intTillNo += 1
                            Else
                                intTillNo = 1
                            End If
                        End If
                        com.CommandText = "Update SYSNUM set NEXT16=? where FKEY='01'"
                        com.AddParameter("Number", intTillNo.ToString("00") & intTranNo.ToString("0000"))
                        com.ExecuteNonQuery()

                    Case DataProvider.Sql
                        com.StoredProcedureName = My.Resources.GetNextTillTranNumber
                        com.AddParameter("@TillTranNumber", intTillNo.ToString("00") & intTranNo.ToString("0000"), SqlDbType.NChar, 6, ParameterDirection.InputOutput)
                        com.ExecuteNonQuery()
                        strOrderNumber = CStr(com.GetParameterValue("@TillTranNumber"))
                End Select
            End Using
        End Using
        Return strOrderNumber
    End Function

    Private Sub LocalWrite(ByRef Log As ILog, ByVal LogLevel As LoggingInformation, ByVal strMessage As String)

        Select Case LogLevel
            Case LoggingInformation.StandardLogic
                Log.Write(strMessage)

                'existing log system; sync debug output with log file
                If Log.GetType.FullName = "Cts.Oasys.Webservices.Output" Then Trace.WriteLine(strMessage)

            Case LoggingInformation.DetailedLogic, LoggingInformation.BusinessObjectState, LoggingInformation.DatabaseState
                'log non-standard information only with the new log system
                If Log.GetType.FullName = "Cts.Oasys.Webservices.Log" Then Log.Write(strMessage)

        End Select

    End Sub

#End Region

End Class