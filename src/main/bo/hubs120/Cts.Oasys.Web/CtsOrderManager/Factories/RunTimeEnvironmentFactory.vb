﻿Namespace TpWickes

    Public Class RunTimeEnvironmentFactory

        Private Shared m_RunTimeEnvironment As IRunTimeEnvironment = Nothing

        Public Shared Function Create() As IRunTimeEnvironment
            If m_RunTimeEnvironment Is Nothing Then                 'using real environment
                Return New TpWickes.RunTimeEnvironment
            Else
                Return m_RunTimeEnvironment                         'using test stub environment
            End If
        End Function

        Public Shared Sub SetRunTimeEnvironment(ByVal val As IRunTimeEnvironment)
            m_RunTimeEnvironment = val
        End Sub

    End Class

End Namespace