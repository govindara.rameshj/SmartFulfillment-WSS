﻿Namespace TpWickes

    Public Class RequestDecisionEngineDatabaseFactory

        Private Shared m_RequestDecisionEngineDatabase As IRequestDecisionEngineDatabase = Nothing

        Public Shared Function Create() As IRequestDecisionEngineDatabase
            If m_RequestDecisionEngineDatabase Is Nothing Then                 'using real database
                'select database engine
                Select Case Cts.Oasys.Data.GetDataProvider
                    Case Data.DataProvider.Odbc
                        Return New TpWickes.RequestDecisionEngineDatabasePervasive
                    Case Else
                        Return New TpWickes.RequestDecisionEngineDatabaseSQL
                End Select
            Else
                Return m_RequestDecisionEngineDatabase                         'using test stub database
            End If
        End Function

        Public Shared Sub SetRequestDecisionEngineDatabase(ByVal db As IRequestDecisionEngineDatabase)
            m_RequestDecisionEngineDatabase = db
        End Sub

    End Class

End Namespace