﻿Public Class LogFactory

    Private Shared m_RequirementEnabled As System.Nullable(Of Boolean)
    Private Shared m_FactoryMember As ILog

    Public Shared Function FactoryGet(ByVal Type As WebServiceType) As ILog

        If RequirementEnabled() = True Then

            'new implementation
            If m_FactoryMember Is Nothing Then
                Return New Log(WebServiceName(Type))       'using live implementation
            Else
                Return m_FactoryMember                     'using stub implementation
            End If

        Else

            'existing implementation
            If m_FactoryMember Is Nothing Then
                Return New Output(WebServiceName(Type))    'using live implementation
            Else
                Return m_FactoryMember                     'using stub implementation
            End If

        End If

    End Function

    Public Shared Sub FactorySet(ByVal obj As ILog)

        m_FactoryMember = obj

    End Sub

#Region "Private Procedures And Functions"

    Private Shared Function RequirementEnabled() As Boolean

        If m_RequirementEnabled.HasValue = False Then m_RequirementEnabled = Cts.Oasys.Core.System.Parameter.GetBoolean(-1)
        Return m_RequirementEnabled.Value

    End Function

    Private Shared Function WebServiceName(ByVal Type As WebServiceType) As String

        Select Case Type
            Case WebServiceType.CtsFulfilmentRequest
                Return "CtsFulfilmentRequest"

            Case WebServiceType.CtsStatusNotification
                Return "CtsStatusNotification"

            Case WebServiceType.CtsUpdateRefund
                Return "CtsUpdateRefund"

            Case WebServiceType.CtsQoqCreate
                Return "CtsQoqCreate"

            Case WebServiceType.CtsRefundCreate
                Return "CtsRefundCreate"

        End Select
        Return String.Empty

    End Function

#End Region

End Class