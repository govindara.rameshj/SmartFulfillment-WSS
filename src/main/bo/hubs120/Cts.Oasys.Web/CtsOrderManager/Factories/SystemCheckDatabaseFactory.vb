﻿Public Class SystemCheckDatabaseFactory

    Private Shared m_RequirementEnabled As System.Nullable(Of Boolean)
    Private Shared m_FactoryMember As ISystemCheckDatabase

    Private Shared Function RequirementEnabled() As Boolean

        If m_RequirementEnabled.HasValue = False Then m_RequirementEnabled = Cts.Oasys.Core.System.Parameter.GetBoolean(-2)

        Return m_RequirementEnabled.Value

    End Function

    Public Shared Function FactoryGet() As ISystemCheckDatabase

        If RequirementEnabled() = True Then

            'new implementation
            If m_FactoryMember Is Nothing Then
                Return New SystemCheckDatabase           'using live implementation
            Else
                Return m_FactoryMember                   'using stub implementation
            End If

        End If
        Return Nothing

    End Function

    Public Shared Sub FactorySet(ByVal obj As ISystemCheckDatabase)

        m_FactoryMember = obj

    End Sub

End Class