﻿Imports System.Xml

Public Class EstoreResponse
    Private _date As Date
    Private _tillId As Integer
    Private _tranNumber As String
    Private _errorDescription As String = String.Empty

    Public Property [Date]() As Date
        Get
            Return _date
        End Get
        Private Set(ByVal value As Date)
            _date = value
        End Set
    End Property

    Public Property TillId() As Integer
        Get
            Return _tillId
        End Get
        Private Set(ByVal value As Integer)
            _tillId = value
        End Set
    End Property

    Public Property TranNumber() As String
        Get
            Return _tranNumber
        End Get
        Private Set(ByVal value As String)
            _tranNumber = value
        End Set
    End Property

    Public Property ErrorDescription() As String
        Get
            Return _errorDescription
        End Get
        Private Set(ByVal value As String)
            _errorDescription = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub Load(ByVal xmlString As String)
        Dim doc As New XmlDocument
        doc.LoadXml(xmlString)
        Me.Date = CDate(doc.SelectSingleNode("//Transaction/Date").InnerText)
        Me.TillId = CInt(doc.SelectSingleNode("//Transaction/TillId").InnerText)
        Me.TranNumber = doc.SelectSingleNode("//Transaction/TranNumber").InnerText
        Me.ErrorDescription = doc.SelectSingleNode("//Transaction/ErrorDescription").InnerText
    End Sub

    Public Overrides Function ToString() As String
        Dim sb As New StringBuilder
        sb.Append("Date:" & Me.Date & Space(1))
        sb.Append("Till:" & Me.TillId & Space(1))
        sb.Append("Tran:" & Me.TranNumber & Space(1))
        sb.Append("Error:" & Me.ErrorDescription)
        Return sb.ToString
    End Function

End Class