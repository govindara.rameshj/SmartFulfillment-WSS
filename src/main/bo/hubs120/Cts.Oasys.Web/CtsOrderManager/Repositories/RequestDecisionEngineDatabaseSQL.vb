﻿Namespace TpWickes

    Public Class RequestDecisionEngineDatabaseSQL
        Implements TpWickes.IRequestDecisionEngineDatabase

        Public Function NitmasCompleteTaskID() As Integer Implements IRequestDecisionEngineDatabase.NitmasCompleteTaskID

            NitmasCompleteTaskID = CType(Cts.Oasys.Core.System.Parameter.GetString(6001), Integer)

        End Function

        Public Function NitmasPendingTime() As Date Implements IRequestDecisionEngineDatabase.NitmasPendingTime

            Dim strSplit() As String

            strSplit = Cts.Oasys.Core.System.Parameter.GetString(6000).Split(CChar(":"))

            NitmasPendingTime = New System.DateTime(Now.Year, Now.Month, Now.Day, CType(strSplit(0), Integer), CType(strSplit(1), Integer), 0)

        End Function

        Public Function NitmasStarted(ByVal NitmasPending As Date) As Boolean Implements IRequestDecisionEngineDatabase.NitmasStarted
            Dim paramStarted As SqlParameter

            Using con As New Connection
                Using com As New Command(con)
                    com.StoredProcedureName = "NitmasStarted"
                    com.AddParameter("@NitmasPending", NitmasPending)
                    paramStarted = com.AddOutputParameter("@Started", SqlDbType.Bit)

                    com.ExecuteNonQuery()
                End Using
            End Using

            NitmasStarted = CType(paramStarted.Value, Boolean)
        End Function

        Public Function NitmasStopped(ByVal NitmasPending As Date, ByVal TaskID As Integer) As Boolean Implements IRequestDecisionEngineDatabase.NitmasStopped
            Dim paramStopped As SqlParameter

            Using con As New Connection
                Using com As New Command(con)
                    com.StoredProcedureName = "NitmasStopped"
                    com.AddParameter("@NitmasPending", NitmasPending)
                    com.AddParameter("@TaskID", TaskID)
                    paramStopped = com.AddOutputParameter("@Stopped", SqlDbType.Bit)

                    com.ExecuteNonQuery()
                End Using
            End Using

            NitmasStopped = CType(paramStopped.Value, Boolean)
        End Function

        'Public Function NitmasStoppedDate(ByVal TaskID As Integer) As Date Implements IRequestDecisionEngineDatabase.NitmasStoppedDate
        '    Dim paramStoppedDate As SqlParameter

        '    Using con As New Connection
        '        Using com As New Command(con)
        '            com.StoredProcedureName = "NitmasStoppedDate"
        '            com.AddParameter("@TaskID", TaskID)
        '            paramStoppedDate = com.AddOutputParameter("@StoppedDate", SqlDbType.DateTime)

        '            com.ExecuteNonQuery()
        '        End Using
        '    End Using

        '    NitmasStoppedDate = CType(paramStoppedDate.Value, DateTime)
        'End Function

    End Class

End Namespace