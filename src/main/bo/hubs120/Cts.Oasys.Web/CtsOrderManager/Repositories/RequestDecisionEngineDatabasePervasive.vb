﻿Namespace TpWickes

    Public Class RequestDecisionEngineDatabasePervasive
        Implements TpWickes.IRequestDecisionEngineDatabase

        Public Function NitmasCompleteTaskID() As Integer Implements IRequestDecisionEngineDatabase.NitmasCompleteTaskID

            NitmasCompleteTaskID = CType(Cts.Oasys.Core.System.Parameter.GetString(6001), Integer)

        End Function

        Public Function NitmasPendingTime() As Date Implements IRequestDecisionEngineDatabase.NitmasPendingTime

            Dim strSplit() As String

            strSplit = Cts.Oasys.Core.System.Parameter.GetString(6000).Split(CChar(":"))

            NitmasPendingTime = New System.DateTime(Now.Year, Now.Month, Now.Day, CType(strSplit(0), Integer), CType(strSplit(1), Integer), 0)

        End Function

        Public Function NitmasStarted(ByVal NitmasPending As Date) As Boolean Implements IRequestDecisionEngineDatabase.NitmasStarted
            Dim SB As New StringBuilder()
            Dim DT As DataTable

            NitmasStarted = False

            Using con As New Connection
                Using com As New Command(con)

                    'SB.Append("SELECT COUNT(*) ")
                    'SB.Append("FROM NITLOG ")
                    'SB.Append("WHERE substring(convert(SDAT, SQL_VARCHAR), 1, 4) >= ? ")
                    'SB.Append("AND   substring(convert(SDAT, SQL_VARCHAR), 6, 2) >= ? ")
                    'SB.Append("AND   substring(convert(SDAT, SQL_VARCHAR), 9, 2) >= ? ")
                    'SB.Append("AND   SUBSTRING(STIM, 1, 2)                       >= ? ")
                    'SB.Append("AND   SUBSTRING(STIM, 3, 2)                       >= ? ")
                    'SB.Append("AND   SUBSTRING(STIM, 5, 2)                       >= ? ")

                    'com.CommandText = SB.ToString
                    'com.AddParameter("Year", NitmasPending.Year)
                    'com.AddParameter("Month", NitmasPending.Month)
                    'com.AddParameter("Day", NitmasPending.Day)
                    'com.AddParameter("Hour", NitmasPending.Hour)
                    'com.AddParameter("Minute", NitmasPending.Minute)
                    'com.AddParameter("Second", NitmasPending.Second)

                    SB.Append("SELECT COUNT(*) ")
                    SB.Append("FROM NITLOG ")
                    SB.Append("WHERE SDAT >= ? ")
                    SB.Append("AND   STIM >= ? ")

                    com.CommandText = SB.ToString
                    com.AddParameter("Date", NitmasPending.Year.ToString & "-" & _
                                             NitmasPending.Month.ToString.PadLeft(2, CType("0", Char)) & "-" & _
                                             NitmasPending.Day.ToString.PadLeft(2, CType("0", Char)))
                    com.AddParameter("Time", NitmasPending.Hour.ToString.PadLeft(2, CType("0", Char)) & _
                                             NitmasPending.Minute.ToString.PadLeft(2, CType("0", Char)) & _
                                             NitmasPending.Second.ToString.PadLeft(2, CType("0", Char)))

                    DT = com.ExecuteDataTable
                End Using
            End Using
            If DT Is Nothing OrElse DT.Rows.Count = 0 Then Exit Function

            If CType(DT.Rows(0).Item(0), Integer) > 0 Then NitmasStarted = True

        End Function

        Public Function NitmasStopped(ByVal NitmasPending As Date, ByVal TaskID As Integer) As Boolean Implements IRequestDecisionEngineDatabase.NitmasStopped
            Dim SB As New StringBuilder()
            Dim DT As DataTable

            NitmasStopped = False

            Using con As New Connection
                Using com As New Command(con)

                    'SB.Append("SELECT COUNT(*) ")
                    'SB.Append("FROM NITLOG ")
                    'SB.Append("WHERE TASK                                                    >= ? ")
                    'SB.Append("AND substring(convert(ifnull(EDAT, ADAT), SQL_VARCHAR), 1, 4) >= ? ")
                    'SB.Append("AND substring(convert(ifnull(EDAT, ADAT), SQL_VARCHAR), 6, 2) >= ? ")
                    'SB.Append("AND substring(convert(ifnull(EDAT, ADAT), SQL_VARCHAR), 9, 2) >= ? ")
                    'SB.Append("AND SUBSTRING(IFNULL(STIM, ETIM), 1, 2)                       >= ? ")
                    'SB.Append("AND SUBSTRING(IFNULL(STIM, ETIM), 3, 2)                       >= ? ")
                    'SB.Append("AND SUBSTRING(IFNULL(STIM, ETIM), 5, 2)                       >= ? ")

                    'com.CommandText = SB.ToString
                    'com.AddParameter("TaskId", TaskID)
                    'com.AddParameter("Year", NitmasPending.Year)
                    'com.AddParameter("Month", NitmasPending.Month)
                    'com.AddParameter("Day", NitmasPending.Day)
                    'com.AddParameter("Hour", NitmasPending.Hour)
                    'com.AddParameter("Minute", NitmasPending.Minute)
                    'com.AddParameter("Second", NitmasPending.Second)

                    SB.Append("SELECT COUNT(*) ")
                    SB.Append("FROM NITLOG ")
                    SB.Append("WHERE TASK               >= ? ")
                    SB.Append("AND   (EDAT >= ? OR ADAT >= ?) ")
                    SB.Append("AND   IFNULL(ETIM, ATIM) >= ? ")

                    com.CommandText = SB.ToString
                    com.AddParameter("TaskId", TaskID)
                    com.AddParameter("EDAT", NitmasPending.Year.ToString & "-" & _
                                             NitmasPending.Month.ToString.PadLeft(2, CType("0", Char)) & "-" & _
                                             NitmasPending.Day.ToString.PadLeft(2, CType("0", Char)))
                    com.AddParameter("ADAT", NitmasPending.Year.ToString & "-" & _
                                             NitmasPending.Month.ToString.PadLeft(2, CType("0", Char)) & "-" & _
                                             NitmasPending.Day.ToString.PadLeft(2, CType("0", Char)))
                    com.AddParameter("Time", NitmasPending.Hour.ToString.PadLeft(2, CType("0", Char)) & _
                                             NitmasPending.Minute.ToString.PadLeft(2, CType("0", Char)) & _
                                             NitmasPending.Second.ToString.PadLeft(2, CType("0", Char)))

                    DT = com.ExecuteDataTable
                End Using
            End Using
            If DT Is Nothing OrElse DT.Rows.Count = 0 Then Exit Function

            If CType(DT.Rows(0).Item(0), Integer) > 0 Then NitmasStopped = True
        End Function

        'Public Function NitmasStarted(ByVal NitmasPending As Date) As Boolean Implements IRequestDecisionEngineDatabase.NitmasStarted
        '    Dim SB As New StringBuilder()
        '    Dim DT As DataTable

        '    NitmasStarted = False

        '    'any log entries greater or equal to nitmas pending date
        '    Using con As New Connection
        '        Using com As New Command(con)
        '            SB.Append("select SDAT, STIM from NITLOG where SDAT >= ?")

        '            com.CommandText = SB.ToString
        '            com.AddParameter("StartDate", NitmasPending)

        '            DT = com.ExecuteDataTable
        '        End Using
        '    End Using
        '    If DT Is Nothing OrElse DT.Rows.Count = 0 Then Exit Function

        '    'any log entries greater or equal to nitmas pending time
        '    For Each Row As DataRow In DT.Rows
        '        If System.DateTime.Compare(NitmasPending, CombineDateAndTime(CType(Row.Item(0), DateTime), Row.Item(1).ToString)) <= 0 Then
        '            'nitmas pending date <= nitlog entry
        '            NitmasStarted = True
        '            Exit For
        '        End If
        '    Next

        'End Function

        'Public Function NitmasStopped(ByVal NitmasPending As Date, ByVal TaskID As Integer) As Boolean Implements IRequestDecisionEngineDatabase.NitmasStopped
        '    Dim SB As New StringBuilder()
        '    Dim DT As DataTable

        '    NitmasStopped = False

        '    'any log entries greater or equal to nitmas pending date & task id
        '    Using con As New Connection
        '        Using com As New Command(con)
        '            SB.Append("select SDAT, STIM, TASK from NITLOG where SDAT >= ? and TASK > ?")

        '            com.CommandText = SB.ToString
        '            com.AddParameter("StartDate", NitmasPending)
        '            com.AddParameter("TaskID", TaskID)

        '            DT = com.ExecuteDataTable
        '        End Using
        '    End Using
        '    If DT IsNot Nothing AndAlso DT.Rows.Count > 0 Then
        '        'any log entries greater or equal to nitmas pending time
        '        For Each Row As DataRow In DT.Rows
        '            If System.DateTime.Compare(NitmasPending, CombineDateAndTime(CType(Row.Item(0), DateTime), Row.Item(1).ToString)) <= 0 Then
        '                NitmasStopped = True
        '                Exit Function
        '            End If
        '        Next
        '    End If

        '    'check if task id is the last task to be done
        '    DT = Nothing
        '    Using con As New Connection
        '        Using com As New Command(con)
        '            SB.Remove(0, SB.Length)
        '            SB.Append("select SDAT, STIM, TASK, EDAT, ADAT from NITLOG where SDAT >= ? and TASK = ? and (EDAT is not null or ADAT is not null)")

        '            com.CommandText = SB.ToString
        '            com.AddParameter("StartDate", NitmasPending)
        '            com.AddParameter("TaskID", TaskID)

        '            DT = com.ExecuteDataTable
        '        End Using
        '    End Using
        '    If DT IsNot Nothing AndAlso DT.Rows.Count > 0 Then
        '        'any log entries greater or equal to nitmas pending time
        '        For Each Row As DataRow In DT.Rows
        '            If System.DateTime.Compare(NitmasPending, CombineDateAndTime(CType(Row.Item(0), DateTime), Row.Item(1).ToString)) <= 0 Then
        '                NitmasStopped = True
        '                Exit Function
        '            End If
        '        Next
        '    End If
        'End Function

        'Public Function NitmasStoppedDate(ByVal TaskID As Integer) As Date Implements IRequestDecisionEngineDatabase.NitmasStoppedDate
        '    Dim SB As New StringBuilder()
        '    Dim DT As DataTable

        '    Using con As New Connection
        '        Using com As New Command(con)
        '            SB.Append("select top 1 EDAT, ETIM, ADAT, ATIM ")
        '            SB.Append("from NITLOG ")
        '            SB.Append("where TASK >= ? ")
        '            SB.Append("and  ((EDAT is not null and ETIM is not null) or (ADAT is not null and ATIM is not null)) ")
        '            SB.Append("order by SDAT desc, STIM desc")

        '            com.CommandText = SB.ToString
        '            com.AddParameter("TaskID", TaskID)

        '            DT = com.ExecuteDataTable
        '        End Using
        '    End Using
        '    If DT IsNot Nothing AndAlso DT.Rows.Count > 0 Then
        '        If DT.Rows(0).Item(0) IsNot Nothing Then
        '            'EDAT, ETIM 
        '            NitmasStoppedDate = CombineDateAndTime(CType(DT.Rows(0).Item(0), DateTime), DT.Rows(0).Item(1).ToString)
        '        Else
        '            'ADTA, ATIM
        '            NitmasStoppedDate = CombineDateAndTime(CType(DT.Rows(0).Item(2), DateTime), DT.Rows(0).Item(3).ToString)
        '        End If
        '    End If
        'End Function

#Region "Private Procedures And Functions"

        Private Function CombineDateAndTime(ByVal SDAT As DateTime, ByVal strTime As String) As Date
            CombineDateAndTime = SDAT.AddHours(CType(strTime.Substring(0, 2), Integer)).AddMinutes(CType(strTime.Substring(2, 2), Integer)).AddSeconds(CType(strTime.Substring(4, 2), Integer))
        End Function

#End Region

    End Class

End Namespace