﻿Namespace TpWickes

    Public Interface IRequestDecisionEngineDatabase

        Function NitmasPendingTime() As DateTime
        Function NitmasCompleteTaskID() As Integer
        Function NitmasStarted(ByVal NitmasPending As DateTime) As Boolean
        Function NitmasStopped(ByVal NitmasPending As DateTime, ByVal TaskID As Integer) As Boolean
        'Function NitmasStoppedDate(ByVal TaskID As Integer) As DateTime

    End Interface

    Public Interface IStubRequestDecisionEngineDatabase
        ' will form an extension of IRequestDecisionEngineDatabase
        ' has additional properties so that we can configure stub values in tests

        Property StubPendingTime() As Date
        Property StubCompleteTaskID() As Integer
        Property StubStarted() As Boolean
        Property StubStopped() As Boolean
        'Property StubStoppedDate() As Date

    End Interface

End Namespace