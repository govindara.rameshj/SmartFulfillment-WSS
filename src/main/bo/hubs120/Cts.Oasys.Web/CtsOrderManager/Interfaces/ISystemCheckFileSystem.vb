﻿Public Interface ISystemCheckFileSystem

    Function FolderExist() As Boolean
    Function FolderReadAccess() As Boolean
    Function FolderWriteAccess() As Boolean
    Function FolderModifyAccess() As Boolean
    Function FolderReadAndExecuteAccess() As Boolean
    Function FolderFullControlAccess() As Boolean

    Sub TestFileCreate()
    Sub TestFileUpdate()
    Sub TestFileDelete()
    Sub TestFileWrite()
    Sub TestFileClose()

End Interface







