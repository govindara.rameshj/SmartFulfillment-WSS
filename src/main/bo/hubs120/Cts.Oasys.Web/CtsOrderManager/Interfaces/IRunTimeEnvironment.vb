﻿Namespace TpWickes

    Public Interface IRunTimeEnvironment
        'Function Now() As Date
        ReadOnly Property Now() As Date
    End Interface

    Public Interface IStubRunTimeEnvironment
        ' will form an extension of IRunTimeEnvironment
        ' has additional properties so that we can configure stub values in tests
        Property StubNow() As Date
    End Interface

End Namespace