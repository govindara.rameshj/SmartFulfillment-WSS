﻿Public Interface ISystemCheckDatabase

    Function Check() As DataTable

End Interface

Public Interface ISystemCheckDatabaseStub

    WriteOnly Property CheckStubVersion() As DataTable

End Interface