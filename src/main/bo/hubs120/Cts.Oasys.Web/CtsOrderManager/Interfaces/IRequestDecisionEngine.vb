﻿Namespace TpWickes

    Public Interface IRequestDecisionEngine

        Property NitmasPending() As System.DateTime

        Function AcceptRequest() As Boolean

    End Interface

End Namespace