﻿<HideModuleName()> Public Module GlobalVariables

    Public AcceptRequest As TpWickes.RequestDecisionEngine

    Public Enum LoggingSource
        FileSystemSingleRequest = 1
        FileSystemBlock = 2
        Database = 4
        Debug = 8
    End Enum

    Public Enum LoggingInformation
        StandardLogic = 1
        DetailedLogic = 2
        BusinessObjectState = 4
        DatabaseState = 8
    End Enum

    Public Enum WebServiceType
        CtsFulfilmentRequest
        CtsStatusNotification
        CtsUpdateRefund
        CtsQoqCreate
        CtsRefundCreate
    End Enum

End Module