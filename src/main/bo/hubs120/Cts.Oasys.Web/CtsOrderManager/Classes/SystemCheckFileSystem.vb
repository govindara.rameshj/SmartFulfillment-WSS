﻿Public Class SystemCheckFileSystem
    Implements ISystemCheckFileSystem

    Private _TestFile As StreamWriter

#Region "Folder"

    Public Function FolderExist() As Boolean Implements ISystemCheckFileSystem.FolderExist

        Return System.IO.Directory.Exists(System.AppDomain.CurrentDomain.BaseDirectory & "Logs")

    End Function

    Public Function FolderReadAccess() As Boolean Implements ISystemCheckFileSystem.FolderReadAccess

        'read access to folder:  TO DO

        'Dim SecurityRights As System.Security.AccessControl.DirectorySecurity
        'SecurityRights = System.IO.Directory.GetAccessControl(System.AppDomain.CurrentDomain.BaseDirectory & "Logs")

    End Function

    Public Function FolderModifyAccess() As Boolean Implements ISystemCheckFileSystem.FolderModifyAccess

        'modify access to folder: TO DO

    End Function

    Public Function FolderWriteAccess() As Boolean Implements ISystemCheckFileSystem.FolderWriteAccess

        'write access to folder: TO DO

    End Function

    Public Function FolderReadAndExecuteAccess() As Boolean Implements ISystemCheckFileSystem.FolderReadAndExecuteAccess

        'read & execute access to folder: TO DO

    End Function

    Public Function FolderFullControlAccess() As Boolean Implements ISystemCheckFileSystem.FolderFullControlAccess

        'full control access to folder: TO DO

    End Function

#End Region

#Region "Test File"

    Public Sub TestFileCreate() Implements ISystemCheckFileSystem.TestFileCreate

        _TestFile = New StreamWriter(AppDomain.CurrentDomain.BaseDirectory.ToString & "Logs" & "\TestFile.txt", True)

    End Sub

    Public Sub TestFileUpdate() Implements ISystemCheckFileSystem.TestFileUpdate

        TestFileCreate()

    End Sub

    Public Sub TestFileDelete() Implements ISystemCheckFileSystem.TestFileDelete

        System.IO.File.Delete(System.AppDomain.CurrentDomain.BaseDirectory & "Logs" & "\TestFile.txt")

    End Sub

    Public Sub TestFileWrite() Implements ISystemCheckFileSystem.TestFileWrite

        _TestFile.WriteLine("New Line")

    End Sub

    Public Sub TestFileClose() Implements ISystemCheckFileSystem.TestFileClose

        _TestFile.Flush()
        _TestFile.Close()
        _TestFile.Dispose()

    End Sub

#End Region

End Class