﻿Public Class SystemCheckDatabase
    Implements ISystemCheckDatabase

    Public Function Check() As System.Data.DataTable Implements ISystemCheckDatabase.Check

        Dim DynamicSQL As New StringBuilder

        With DynamicSQL
            .AppendLine("select [Schema Name]   = s.name,                                     ")
            .AppendLine("       [Type]          = case a.type                                 ")
            .AppendLine("                           when 'U' then 'Table'                     ")
            .AppendLine("                           when 'P' then 'Stored Procedure'          ")
            .AppendLine("                        end,                                         ")
            .AppendLine("       [Name]          = a.name,                                     ")
            .AppendLine("       Permission      = b.permission_name,                          ")
            .AppendLine("       [Access Rights] = b.state_desc,                               ")
            .AppendLine("       [Assigned To]   = AssignedTo.name,                            ")
            .AppendLine("       [Assigned By]   = AssignedBy.name                             ")
            .AppendLine("from sys.all_objects a                                               ")
            .AppendLine("inner join sys.schemas s                                             ")
            .AppendLine("            on s.schema_id  = a.schema_id                            ")
            .AppendLine("left outer join sys.database_permissions b                           ")
            .AppendLine("                 on b.major_id = a.object_id                         ")
            .AppendLine("left outer join sys.database_principals AssignedTo                   ")
            .AppendLine("                 on AssignedTo.principal_id = b.grantee_principal_id ")
            .AppendLine("left outer join sys.database_principals AssignedBy                   ")
            .AppendLine("                 on AssignedBy.principal_id = b.grantor_principal_id ")
            .AppendLine("where a.type in ('U', 'P')                                           ")
            .AppendLine("order by a.type desc, s.name, a.Name                                 ")
        End With

        'get data
        Using con As New Connection
            Using com As New Command(con)
                'com.StoredProcedureName = "SystemCheckDatabasePermissions"
                com.CommandText = DynamicSQL.ToString

                Return com.ExecuteDataTable()
            End Using
        End Using

    End Function

End Class

Public Class SystemCheckDatabaseStubVersion
    Implements ISystemCheckDatabase, ISystemCheckDatabaseStub

    Private _LocalDT As DataTable

#Region "Stub Implementation"

    Public WriteOnly Property CheckStubVersion() As System.Data.DataTable Implements ISystemCheckDatabaseStub.CheckStubVersion
        Set(ByVal value As System.Data.DataTable)
            _LocalDT = value
        End Set
    End Property

#End Region

#Region "Actual Implementation"

    Public Function Check() As System.Data.DataTable Implements ISystemCheckDatabase.Check

        Return _LocalDT

    End Function

#End Region

End Class