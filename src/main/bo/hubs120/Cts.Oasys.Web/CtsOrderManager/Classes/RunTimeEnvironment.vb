﻿Namespace TpWickes

    Public Class RunTimeEnvironment
        Implements TpWickes.IRunTimeEnvironment

        Public ReadOnly Property Now() As Date Implements IRunTimeEnvironment.Now
            Get
                Return System.DateTime.Now()
            End Get
        End Property

    End Class

End Namespace
