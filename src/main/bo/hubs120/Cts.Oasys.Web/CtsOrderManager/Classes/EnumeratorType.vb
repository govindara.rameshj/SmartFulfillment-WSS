﻿Namespace TpWickes

    <HideModuleName()> Public Module EnumeratorType

        'Private Enum RequestType
        '    SaleOrder
        '    RefundOrder
        'End Enum

        Public Enum RequestMode
            MinimisedDatabaseInteraction
            NITMAS_Pending
            NITMAS_Running
        End Enum

    End Module

End Namespace


