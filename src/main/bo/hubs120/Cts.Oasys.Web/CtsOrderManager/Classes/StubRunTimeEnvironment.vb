﻿Namespace TpWickes
    Public Class StubRunTimeEnvironment
        Implements TpWickes.IRunTimeEnvironment, TpWickes.IStubRunTimeEnvironment

        Private m_Now As Date

        Public ReadOnly Property Now() As Date Implements IRunTimeEnvironment.Now
            Get
                Return m_Now
            End Get
        End Property

        Public Property StubNow() As Date Implements IStubRunTimeEnvironment.StubNow
            Get
                Return m_Now
            End Get
            Set(ByVal value As Date)
                m_Now = value
            End Set
        End Property

    End Class
End Namespace