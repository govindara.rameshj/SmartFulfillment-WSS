﻿Namespace TpWickes

    ' stub class to simulate database response
    Public Class StubRequestDecisionEngineDatabase
        Implements IRequestDecisionEngineDatabase, IStubRequestDecisionEngineDatabase

#Region "Member variable declaration"

        Private NITMASCompleteTaskID_Val As Integer
        Private NITMASPendingTime_Val As Date
        Private NITMASStarted_Val As Boolean
        Private NITMASFinished_Val As Boolean
        Private NITMASStoppedData_Val As Date

#End Region

#Region "property sets and gets"

        Public Property StubCompleteTaskID() As Integer Implements IStubRequestDecisionEngineDatabase.StubCompleteTaskID
            Get
                Return NITMASCompleteTaskID_Val
            End Get
            Set(ByVal value As Integer)
                NITMASCompleteTaskID_Val = value
            End Set
        End Property

        Public Property StubPendingTime() As Date Implements IStubRequestDecisionEngineDatabase.StubPendingTime
            Get
                Return NITMASPendingTime_Val
            End Get
            Set(ByVal value As Date)
                NITMASPendingTime_Val = value
            End Set
        End Property

        Public Property StubStarted() As Boolean Implements IStubRequestDecisionEngineDatabase.StubStarted
            Get
                Return NITMASStarted_Val
            End Get
            Set(ByVal value As Boolean)
                NITMASStarted_Val = value
            End Set
        End Property

        Public Property StubStopped() As Boolean Implements IStubRequestDecisionEngineDatabase.StubStopped
            Get
                Return NITMASFinished_Val
            End Get
            Set(ByVal value As Boolean)
                NITMASFinished_Val = value
            End Set
        End Property

        'Public Property StubStoppedDate() As Date Implements IStubRequestDecisionEngineDatabase.StubStoppedDate
        '    Get
        '        Return NITMASStoppedData_Val
        '    End Get
        '    Set(ByVal value As Date)
        '        NITMASStoppedData_Val = value
        '    End Set
        'End Property

#End Region

#Region "Interface implementation"

        Public Function NitmasCompleteTaskID() As Integer Implements TpWickes.IRequestDecisionEngineDatabase.NitmasCompleteTaskID
            Return NITMASCompleteTaskID_Val
        End Function

        Public Function NitmasPendingTime() As Date Implements TpWickes.IRequestDecisionEngineDatabase.NitmasPendingTime
            Return NITMASPendingTime_Val
        End Function

        Public Function NitmasStarted(ByVal NitmasPending As Date) As Boolean Implements TpWickes.IRequestDecisionEngineDatabase.NitmasStarted
            Return NITMASStarted_Val
        End Function

        Public Function NitmasStopped(ByVal NitmasPending As Date, ByVal TaskID As Integer) As Boolean Implements TpWickes.IRequestDecisionEngineDatabase.NitmasStopped
            Return NITMASFinished_Val
        End Function

        'Public Function NitmasStoppedDate(ByVal TaskID As Integer) As Date Implements IRequestDecisionEngineDatabase.NitmasStoppedDate
        '    Return NITMASStoppedData_Val
        'End Function

#End Region

    End Class

End Namespace