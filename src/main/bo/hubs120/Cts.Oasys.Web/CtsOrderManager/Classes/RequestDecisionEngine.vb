﻿Namespace TpWickes

    Public Class RequestDecisionEngine
        Implements TpWickes.IRequestDecisionEngine

#Region "Properties"

        Private _NitmasPendingDateTime As DateTime
        Private _NitmasCompleteTaskID As Integer
        Private _DB As TpWickes.IRequestDecisionEngineDatabase

        Public Property NitmasPending() As DateTime Implements IRequestDecisionEngine.NitmasPending
            Get
                Return _NitmasPendingDateTime
            End Get
            Set(ByVal value As DateTime)
                _NitmasPendingDateTime = value
            End Set
        End Property

        Public Property NitmasCompleteTaskID() As Integer
            Get
                Return _NitmasCompleteTaskID
            End Get
            Set(ByVal value As Integer)
                _NitmasCompleteTaskID = value
            End Set
        End Property

        Public Property Database() As IRequestDecisionEngineDatabase
            Get
                Return _DB
            End Get
            Set(ByVal value As IRequestDecisionEngineDatabase)
                _DB = value
            End Set
        End Property

#End Region

#Region "Public Procedures And Functions"

        'Author : Partha Dutta
        'Date   : 07/03/2011
        'Reason : Because of time pressure to deliver CR0026 before code freeze have not untilised "unit testing" testing process
        '         For now performed manual testing only
        '         This is the reason for the re-introduction of the new contructor

        Public Sub New()
            Trace.WriteLine("Request Engine: Creation")

            Database = RequestDecisionEngineDatabaseFactory.Create()

            InitialiseNitmasCompleteTask()
            InitialiseNitmasPending()
        End Sub

        Public Function AcceptRequest() As Boolean Implements IRequestDecisionEngine.AcceptRequest

            Select Case EstablishStatus()
                Case RequestMode.MinimisedDatabaseInteraction
                    AcceptRequest = True
                    Trace.WriteLine("Request Engine: Request accepted - Normal mode")

                Case RequestMode.NITMAS_Pending


                    'Referral : 803
                    'Author   : Partha Dutta
                    'Date     : 07/04/2011
                    'Notes    : Actual scenerio is LIVE
                    '              Nitmas Pending; 00:00
                    '              Nitmas Running; 00:15
                    '           When a request arrives after midnight, but before nitmas processing has started, all requests need to be rejected
                    '           Presently all requests in this "nitmas pending" period are being accepted
                    '
                    '           Present fix will deal with this specific scenerio
                    '           Other potential scenerios have not been considered, and further analysis is required 


                    'AcceptRequest = True
                    AcceptRequest = False



                    Trace.WriteLine("Request Engine: Request accepted - Pending mode")

                Case RequestMode.NITMAS_Running
                    AcceptRequest = False
                    Trace.WriteLine("Request Engine: Request accepted - Running mode")

            End Select

        End Function

#End Region

#Region "Private Procedures And Functions"

        Private Sub InitialiseNitmasCompleteTask()

            NitmasCompleteTaskID = Database.NitmasCompleteTaskID

            Trace.WriteLine("Request Engine: Nitmas Complete Task ID retrieved : " & NitmasCompleteTaskID.ToString)
        End Sub

        Private Sub InitialiseNitmasPending()
            Dim TempDateTime As DateTime

            TempDateTime = Database.NitmasPendingTime

            'Author : Partha Dutta
            'Date   : 06/07/2011
            'Reason : Refractor code to move system run time environment dependency behind an interface
            '         This will allow date dependent scenerios to be unit tested

            'TempDateTime = CombineDateAndTime(Now.Date, 0, TempDateTime.Hour, TempDateTime.Minute, TempDateTime.Second)
            TempDateTime = CombineDateAndTime(TpWickes.RunTimeEnvironmentFactory.Create.Now.Date, 0, TempDateTime.Hour, TempDateTime.Minute, TempDateTime.Second)

            ''''''''''''END''''''''''''''''

            'If System.DateTime.Compare(TempDateTime, Database.NitmasStoppedDate(NitmasCompleteTaskID)) < 0 Then
            If Database.NitmasStopped(TempDateTime, NitmasCompleteTaskID) = True Then
                NitmasPending = CombineDateAndTime(TempDateTime, 1, 0, 0, 0)  'bump forward one day

                Trace.WriteLine("Request Engine: Nitmas Pending bumped forward 1 day")
            Else
                NitmasPending = TempDateTime
            End If


            Trace.WriteLine("Request Engine: Nitmas Pending calculated : " & NitmasPending.ToShortDateString & " " & NitmasPending.ToShortTimeString)
        End Sub

        Private Function EstablishStatus() As RequestMode
            Dim PreviousNitmasPending As DateTime

            'Author : Partha Dutta
            'Date   : 06/07/2011
            'Reason : Refractor code to move system run time environment dependency behind an interface
            '         This will allow date dependent scenerios to be unit tested

            '< nitmas pending
            'If System.DateTime.Compare(Now, NitmasPending) < 0 Then Return RequestMode.MinimisedDatabaseInteraction
            If System.DateTime.Compare(TpWickes.RunTimeEnvironmentFactory.Create.Now, NitmasPending) < 0 Then Return RequestMode.MinimisedDatabaseInteraction

            ''''''''''''END''''''''''''''''

            PreviousNitmasPending = NitmasPending
            InitialiseNitmasPending()

            ' if NitmasPending has changed cince previous run, check again
            If System.DateTime.Compare(PreviousNitmasPending, NitmasPending) <> 0 Then

                'Author : Partha Dutta
                'Date   : 06/07/2011
                'Reason : Refractor code to move system run time environment dependency behind an interface
                '         This will allow date dependent scenerios to be unit tested

                'If System.DateTime.Compare(Now, NitmasPending) < 0 Then Return RequestMode.MinimisedDatabaseInteraction
                If System.DateTime.Compare(TpWickes.RunTimeEnvironmentFactory.Create.Now, NitmasPending) < 0 Then Return RequestMode.MinimisedDatabaseInteraction

                ''''''''''''END''''''''''''''''

            End If

            '>= nitmas pending; < nitmas start 
            If Database.NitmasStarted(NitmasPending) = False Then Return RequestMode.NITMAS_Pending
            '>= nitmas start; < nitmas end
            If Database.NitmasStopped(NitmasPending, NitmasCompleteTaskID) = False Then Return RequestMode.NITMAS_Running
            '>=nitmas end; recalculate nitmas pending, re-run establish  status processing

            Trace.WriteLine("Request Engine: Establish status - system date & time moved passed end of nitmas run, recalculate nitmas pending & re-establish status")

            Return RequestMode.MinimisedDatabaseInteraction

        End Function

        'Private Function EstablishStatus() As RequestMode
        '    '< nitmas pending
        '    If System.DateTime.Compare(Now, NitmasPending) < 0 Then Return RequestMode.MinimisedDatabaseInteraction
        '    '>= nitmas pending; < nitmas start 
        '    If Database.NitmasStarted(NitmasPending) = False Then Return RequestMode.NITMAS_Pending
        '    '>= nitmas start; < nitmas end
        '    If Database.NitmasStopped(NitmasPending, NitmasCompleteTaskID) = False Then Return RequestMode.NITMAS_Running
        '    '>=nitmas end; recalculate nitmas pending, re-run establish  status processing

        '    Trace.WriteLine("Request Engine: Establish status - system date & time moved passed end of nitmas run, recalculate nitmas pending & re-establish status")

        '    InitialiseNitmasPending()
        '    EstablishStatus()
        'End Function

        Private Function CombineDateAndTime(ByVal InitialDate As DateTime, ByVal Day As Integer, ByVal Hour As Integer, ByVal Minute As Integer, ByVal Second As Integer) As DateTime

            CombineDateAndTime = InitialDate.AddDays(Day).AddHours(Hour).AddMinutes(Minute).AddSeconds(Second)

        End Function

#End Region

    End Class

End Namespace