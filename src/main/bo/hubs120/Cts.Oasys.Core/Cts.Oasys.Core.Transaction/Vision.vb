﻿Imports System.Xml.Serialization
Imports System.IO
Imports Cts.Oasys.Data
Imports System.Xml

Namespace Vision

    Public Class Total
        Inherits Base
        Implements IXmlSerializable

#Region "       Table Fields"
        Private _tranDate As Date
        Private _tillId As Integer
        Private _tranNumber As Integer
        Private _cashierId As Integer
        Private _tranDateTime As String
        Private _type As String
        Private _reasonCode As String
        Private _reasonDescription As String
        Private _orderNumber As String
        Private _valueMerchandising As Decimal
        Private _valueNonMerchandising As Decimal
        Private _valueTax As Decimal
        Private _valueDiscount As Decimal
        Private _value As Decimal
        Private _isColleagueDiscount As Boolean
        Private _valueColleagueDiscount As Decimal
        Private _isPivotal As Boolean

        <ColumnMapping("TranDate")> Public Property TranDate() As Date
            Get
                Return _tranDate
            End Get
            Set(ByVal value As Date)
                _tranDate = value
            End Set
        End Property
        <ColumnMapping("TillId")> Public Property TillId() As Integer
            Get
                Return _tillId
            End Get
            Set(ByVal value As Integer)
                _tillId = value
            End Set
        End Property
        <ColumnMapping("TranNumber")> Public Property TranNumber() As Integer
            Get
                Return _tranNumber
            End Get
            Set(ByVal value As Integer)
                _tranNumber = value
            End Set
        End Property
        <ColumnMapping("CashierId")> Public Property CashierId() As Integer
            Get
                Return _cashierId
            End Get
            Set(ByVal value As Integer)
                _cashierId = value
            End Set
        End Property
        <ColumnMapping("TranDateTime")> Public Property TranDateTime() As String
            Get
                Return _tranDateTime
            End Get
            Set(ByVal value As String)
                _tranDateTime = value
            End Set
        End Property
        <ColumnMapping("Type")> Public Property Type() As String
            Get
                Return _type
            End Get
            Set(ByVal value As String)
                _type = value
            End Set
        End Property
        <ColumnMapping("ReasonCode")> Public Property ReasonCode() As String
            Get
                Return _reasonCode
            End Get
            Set(ByVal value As String)
                _reasonCode = value
            End Set
        End Property
        <ColumnMapping("ReasonDescription")> Public Property ReasonDescription() As String
            Get
                Return _reasonDescription
            End Get
            Set(ByVal value As String)
                _reasonDescription = value
            End Set
        End Property
        <ColumnMapping("OrderNumber")> Public Property OrderNumber() As String
            Get
                Return _orderNumber
            End Get
            Set(ByVal value As String)
                _orderNumber = value
            End Set
        End Property
        <ColumnMapping("ValueMerchandising")> Public Property ValueMerchandising() As Decimal
            Get
                Return _valueMerchandising
            End Get
            Set(ByVal value As Decimal)
                _valueMerchandising = value
            End Set
        End Property
        <ColumnMapping("ValueNonMerchandising")> Public Property ValueNonMerchandising() As Decimal
            Get
                Return _valueNonMerchandising
            End Get
            Set(ByVal value As Decimal)
                _valueNonMerchandising = value
            End Set
        End Property
        <ColumnMapping("ValueTax")> Public Property ValueTax() As Decimal
            Get
                Return _valueTax
            End Get
            Set(ByVal value As Decimal)
                _valueTax = value
            End Set
        End Property
        <ColumnMapping("ValueDiscount")> Public Property ValueDiscount() As Decimal
            Get
                Return _valueDiscount
            End Get
            Set(ByVal value As Decimal)
                _valueDiscount = value
            End Set
        End Property
        <ColumnMapping("Value")> Public Property Value() As Decimal
            Get
                Return _value
            End Get
            Set(ByVal value As Decimal)
                _value = value
            End Set
        End Property
        <ColumnMapping("IsColleagueDiscount")> Public Property IsColleagueDiscount() As Boolean
            Get
                Return _isColleagueDiscount
            End Get
            Set(ByVal value As Boolean)
                _isColleagueDiscount = value
            End Set
        End Property
        <ColumnMapping("ValueColleagueDiscount")> Public Property ValueColleagueDiscount() As Decimal
            Get
                Return _valueColleagueDiscount
            End Get
            Set(ByVal value As Decimal)
                _valueColleagueDiscount = value
            End Set
        End Property
        <ColumnMapping("IsPivotal")> Public Property IsPivotal() As Boolean
            Get
                Return _isPivotal
            End Get
            Set(ByVal value As Boolean)
                _isPivotal = value
            End Set
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function GetSchema() As Global.System.Xml.Schema.XmlSchema Implements Global.System.Xml.Serialization.IXmlSerializable.GetSchema
            Return Nothing
        End Function

        Public Sub ReadXml(ByVal reader As Global.System.Xml.XmlReader) Implements Global.System.Xml.Serialization.IXmlSerializable.ReadXml

            While (reader.Read())
                Select Case reader.LocalName
                    Case "DATE" : Me.TranDate = CDate(reader.ReadString)
                    Case "TILL" : Me.TillId = CInt(reader.ReadString)
                    Case "TRAN" : Me.TranNumber = CInt(reader.ReadString)
                    Case "CASH" : Me.CashierId = CInt(reader.ReadString)
                    Case "TIME"
                        Dim t As String = reader.ReadString.PadLeft(6, "0"c)
                        Me.TranDateTime = t.Substring(0, 2) + ":"
                        Me.TranDateTime += t.Substring(2, 2) + ":"
                        Me.TranDateTime += t.Substring(4, 2)
                    Case "TCOD" : Me.Type = reader.ReadString
                    Case "MISC" : Me.ReasonCode = reader.ReadString
                    Case "DESC" : Me.ReasonDescription = reader.ReadString
                    Case "ORDN" : Me.OrderNumber = reader.ReadString
                    Case "MERC" : Me.ValueMerchandising = CDec(reader.ReadString)
                    Case "NMER" : Me.ValueNonMerchandising = CDec(reader.ReadString)
                    Case "TAXA" : Me.ValueTax = CDec(reader.ReadString)
                    Case "DISC" : Me.ValueDiscount = CDec(reader.ReadString)
                    Case "TOTL" : Me.Value = CDec(reader.ReadString)
                    Case "IEMP" : Me.IsColleagueDiscount = (reader.ReadString = "Y")
                    Case "PVEM" : Me.ValueColleagueDiscount = CDec(reader.ReadString)
                    Case "PIVT" : Me.IsPivotal = (reader.ReadString = "Y")
                    Case Else
                        reader.ReadEndElement()
                        Exit While
                End Select

            End While

        End Sub

        Public Sub WriteXml(ByVal writer As Global.System.Xml.XmlWriter) Implements Global.System.Xml.Serialization.IXmlSerializable.WriteXml
            writer.WriteStartElement("PVTOTS")
            writer.WriteAttributeString("DATE", Me.TranDate.ToShortDateString)
            writer.WriteAttributeString("TILL", CStr(Me.TillId))
            writer.WriteAttributeString("TRAN", CStr(Me.TranNumber))
            writer.WriteAttributeString("CASH", CStr(Me.CashierId))
            writer.WriteAttributeString("TIME", Me.TranDateTime)
            writer.WriteAttributeString("TYPE", Me.Type)
            writer.WriteAttributeString("MISC", Me.ReasonCode)
            writer.WriteAttributeString("DESC", Me.ReasonDescription)
            writer.WriteAttributeString("ORDN", Me.OrderNumber)
            writer.WriteAttributeString("MERC", CStr(Me.ValueMerchandising))
            writer.WriteAttributeString("NMER", CStr(Me.ValueNonMerchandising))
            writer.WriteAttributeString("TAXA", CStr(Me.ValueTax))
            writer.WriteAttributeString("DISC", CStr(Me.ValueDiscount))
            writer.WriteAttributeString("TOTL", CStr(Me.Value))
            writer.WriteAttributeString("IEMP", CStr(Me.IsColleagueDiscount))
            writer.WriteAttributeString("PVEM", CStr(Me.ValueColleagueDiscount))
            writer.WriteAttributeString("PIVT", CStr(Me.IsPivotal))
            writer.WriteEndElement()
        End Sub

    End Class

    Public Class Line
        Inherits Base
        Implements IXmlSerializable

#Region "       Table Fields"
        Private _tranDate As Date
        Private _tillId As Integer
        Private _tranNumber As Integer
        Private _number As Integer
        Private _skuNumber As String
        Private _quantity As Integer
        Private _priceLookup As Decimal
        Private _price As Decimal
        Private _priceExVat As Decimal
        Private _priceExtended As Decimal
        Private _isRelatedItemSingle As Boolean
        Private _vatSymbol As String
        Private _isInStoreStockItem As Boolean
        Private _movementTypeCode As String
        Private _hieCategory As String
        Private _hieGroup As String
        Private _hieSubgroup As String
        Private _hieStyle As String
        Private _saleType As String
        Private _isPivotal As Boolean

        <ColumnMapping("TranDate")> Public Property TranDate() As Date
            Get
                Return _tranDate
            End Get
            Set(ByVal value As Date)
                _tranDate = value
            End Set
        End Property
        <ColumnMapping("TillId")> Public Property TillId() As Integer
            Get
                Return _tillId
            End Get
            Set(ByVal value As Integer)
                _tillId = value
            End Set
        End Property
        <ColumnMapping("TranNumber")> Public Property TranNumber() As Integer
            Get
                Return _tranNumber
            End Get
            Set(ByVal value As Integer)
                _tranNumber = value
            End Set
        End Property
        <ColumnMapping("Number")> Public Property Number() As Integer
            Get
                Return _number
            End Get
            Set(ByVal value As Integer)
                _number = value
            End Set
        End Property
        <ColumnMapping("SkuNumber")> Public Property SkuNumber() As String
            Get
                Return _skuNumber
            End Get
            Set(ByVal value As String)
                _skuNumber = value
            End Set
        End Property
        <ColumnMapping("Quantity")> Public Property Quantity() As Integer
            Get
                Return _quantity
            End Get
            Set(ByVal value As Integer)
                _quantity = value
            End Set
        End Property
        <ColumnMapping("PriceLookup")> Public Property PriceLookup() As Decimal
            Get
                Return _priceLookup
            End Get
            Set(ByVal value As Decimal)
                _priceLookup = value
            End Set
        End Property
        <ColumnMapping("Price")> Public Property Price() As Decimal
            Get
                Return _price
            End Get
            Set(ByVal value As Decimal)
                _price = value
            End Set
        End Property
        <ColumnMapping("PriceExVat")> Public Property PriceExVat() As Decimal
            Get
                Return _priceExVat
            End Get
            Set(ByVal value As Decimal)
                _priceExVat = value
            End Set
        End Property
        <ColumnMapping("PriceExtended")> Public Property PriceExtended() As Decimal
            Get
                Return _priceExtended
            End Get
            Set(ByVal value As Decimal)
                _priceExtended = value
            End Set
        End Property
        <ColumnMapping("IsRelatedItemSingle")> Public Property IsRelatedItemSingle() As Boolean
            Get
                Return _isRelatedItemSingle
            End Get
            Set(ByVal value As Boolean)
                _isRelatedItemSingle = value
            End Set
        End Property
        <ColumnMapping("VatSymbol")> Public Property VatSymbol() As String
            Get
                Return _vatSymbol
            End Get
            Set(ByVal value As String)
                _vatSymbol = value
            End Set
        End Property
        <ColumnMapping("IsInStoreStockItem")> Public Property IsInStoreStockItem() As Boolean
            Get
                Return _isInStoreStockItem
            End Get
            Set(ByVal value As Boolean)
                _isInStoreStockItem = value
            End Set
        End Property
        <ColumnMapping("MovementTypeCode")> Public Property MovementTypeCode() As String
            Get
                Return _movementTypeCode
            End Get
            Set(ByVal value As String)
                _movementTypeCode = value
            End Set
        End Property
        <ColumnMapping("HieCategory")> Public Property HieCategory() As String
            Get
                Return _hieCategory
            End Get
            Set(ByVal value As String)
                _hieCategory = value
            End Set
        End Property
        <ColumnMapping("HieGroup")> Public Property HieGroup() As String
            Get
                Return _hieGroup
            End Get
            Set(ByVal value As String)
                _hieGroup = value
            End Set
        End Property
        <ColumnMapping("HieSubgroup")> Public Property HieSubgroup() As String
            Get
                Return _hieSubgroup
            End Get
            Set(ByVal value As String)
                _hieSubgroup = value
            End Set
        End Property
        <ColumnMapping("HieStyle")> Public Property HieStyle() As String
            Get
                Return _hieStyle
            End Get
            Set(ByVal value As String)
                _hieStyle = value
            End Set
        End Property
        <ColumnMapping("SaleType")> Public Property SaleType() As String
            Get
                Return _saleType
            End Get
            Set(ByVal value As String)
                _saleType = value
            End Set
        End Property
        <ColumnMapping("IsPivotal")> Public Property IsPivotal() As Boolean
            Get
                Return _isPivotal
            End Get
            Set(ByVal value As Boolean)
                _isPivotal = value
            End Set
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function GetSchema() As Global.System.Xml.Schema.XmlSchema Implements Global.System.Xml.Serialization.IXmlSerializable.GetSchema
            Return Nothing
        End Function

        Public Sub ReadXml(ByVal reader As Global.System.Xml.XmlReader) Implements Global.System.Xml.Serialization.IXmlSerializable.ReadXml

            While (reader.Read())
                Select Case reader.LocalName
                    Case "DATE" : Me.TranDate = CDate(reader.ReadString)
                    Case "TILL" : Me.TillId = CInt(reader.ReadString)
                    Case "TRAN" : Me.TranNumber = CInt(reader.ReadString)
                    Case "NUMB" : Me.Number = CInt(reader.ReadString)
                    Case "SKUN" : Me.SkuNumber = reader.ReadString
                    Case "QUAN" : Me.Quantity = CInt(reader.ReadString)
                    Case "SPRI" : Me.PriceLookup = CDec(reader.ReadString)
                    Case "PRIC" : Me.Price = CDec(reader.ReadString)
                    Case "PRVE" : Me.PriceExVat = CDec(reader.ReadString)
                    Case "EXTP" : Me.PriceExtended = CDec(reader.ReadString)
                    Case "RITM" : Me.IsRelatedItemSingle = (reader.ReadString = "Y")
                    Case "VSYM" : Me.VatSymbol = reader.ReadString
                    Case "PIVI" : Me.IsInStoreStockItem = (reader.ReadString = "Y")
                    Case "PIVM" : Me.MovementTypeCode = reader.ReadString
                    Case "CTGY" : Me.HieCategory = reader.ReadString
                    Case "GRUP" : Me.HieGroup = reader.ReadString
                    Case "SGRP" : Me.HieSubgroup = reader.ReadString
                    Case "STYL" : Me.HieStyle = reader.ReadString
                    Case "SALT" : Me.SaleType = reader.ReadString
                    Case Is = "PIVT" : Me.IsPivotal = (reader.ReadString = "Y")
                    Case Else
                        reader.ReadEndElement()
                        Exit While
                End Select
            End While

        End Sub

        Public Sub WriteXml(ByVal writer As Global.System.Xml.XmlWriter) Implements Global.System.Xml.Serialization.IXmlSerializable.WriteXml
            writer.WriteStartElement("LINE")
            writer.WriteAttributeString("DATE", Me.TranDate.ToShortDateString)
            writer.WriteAttributeString("TILL", CStr(Me.TillId))
            writer.WriteAttributeString("TRAN", CStr(Me.TranNumber))
            writer.WriteAttributeString("NUMB", CStr(Me.Number))
            writer.WriteAttributeString("SKUN", Me.SkuNumber)
            writer.WriteAttributeString("QUAN", CStr(Me.Quantity))
            writer.WriteAttributeString("SPRI", CStr(Me.PriceLookup))
            writer.WriteAttributeString("PRIC", CStr(Me.Price))
            writer.WriteAttributeString("PRVE", CStr(Me.PriceExVat))
            writer.WriteAttributeString("EXTP", CStr(Me.PriceExtended))
            writer.WriteAttributeString("RITM", CStr(Me.IsRelatedItemSingle))
            writer.WriteAttributeString("VSYM", Me.VatSymbol)
            writer.WriteAttributeString("PIVI", CStr(Me.IsInStoreStockItem))
            writer.WriteAttributeString("PIVM", Me.MovementTypeCode)
            writer.WriteAttributeString("CTGY", Me.HieCategory)
            writer.WriteAttributeString("GRUP", Me.HieGroup)
            writer.WriteAttributeString("SGRP", Me.HieSubgroup)
            writer.WriteAttributeString("STYL", Me.HieStyle)
            writer.WriteAttributeString("SALT", Me.SaleType)
            writer.WriteAttributeString("PIVT", CStr(Me.IsPivotal))
            writer.WriteEndElement()
        End Sub


    End Class

    Public Class LineCollection
        Inherits BaseCollection(Of Line)
    End Class

    Public Class Payment
        Inherits Base
        Implements IXmlSerializable

#Region "       Table Fields"
        Private _tranDate As Date
        Private _tillId As Integer
        Private _tranNumber As Integer
        Private _number As Integer
        Private _tenderTypeId As Integer
        Private _valueTender As Decimal
        Private _isPivotal As Boolean

        <ColumnMapping("TranDate")> Public Property TranDate() As Date
            Get
                Return _tranDate
            End Get
            Set(ByVal value As Date)
                _tranDate = value
            End Set
        End Property
        <ColumnMapping("TillId")> Public Property TillId() As Integer
            Get
                Return _tillId
            End Get
            Set(ByVal value As Integer)
                _tillId = value
            End Set
        End Property
        <ColumnMapping("TranNumber")> Public Property TranNumber() As Integer
            Get
                Return _tranNumber
            End Get
            Set(ByVal value As Integer)
                _tranNumber = value
            End Set
        End Property
        <ColumnMapping("Number")> Public Property Number() As Integer
            Get
                Return _number
            End Get
            Set(ByVal value As Integer)
                _number = value
            End Set
        End Property
        <ColumnMapping("TenderTypeId")> Public Property TenderTypeId() As Integer
            Get
                Return _tenderTypeId
            End Get
            Set(ByVal value As Integer)
                _tenderTypeId = value
            End Set
        End Property
        <ColumnMapping("ValueTender")> Public Property ValueTender() As Decimal
            Get
                Return _valueTender
            End Get
            Set(ByVal value As Decimal)
                _valueTender = value
            End Set
        End Property
        <ColumnMapping("IsPivotal")> Public Property IsPivotal() As Boolean
            Get
                Return _isPivotal
            End Get
            Set(ByVal value As Boolean)
                _isPivotal = value
            End Set
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function GetSchema() As Global.System.Xml.Schema.XmlSchema Implements Global.System.Xml.Serialization.IXmlSerializable.GetSchema
            Return Nothing
        End Function

        Public Sub ReadXml(ByVal reader As Global.System.Xml.XmlReader) Implements Global.System.Xml.Serialization.IXmlSerializable.ReadXml

            While (reader.Read())
                Select Case reader.LocalName
                    Case "DATE" : Me.TranDate = CDate(reader.ReadString)
                    Case "TILL" : Me.TillId = CInt(reader.ReadString)
                    Case "TRAN" : Me.TranNumber = CInt(reader.ReadString)
                    Case "NUMB" : Me.Number = CInt(reader.ReadString)
                    Case "TYPE" : Me.TenderTypeId = CInt(reader.ReadString)
                    Case "AMNT" : Me.ValueTender = CDec(reader.ReadString)
                    Case "PIVT" : Me.IsPivotal = (reader.ReadString = "Y")
                    Case "CARD", "EXDT" : reader.ReadString()
                    Case Else
                        reader.ReadEndElement()
                        Exit While
                End Select
            End While

        End Sub

        Public Sub WriteXml(ByVal writer As Global.System.Xml.XmlWriter) Implements Global.System.Xml.Serialization.IXmlSerializable.WriteXml
            writer.WriteStartElement("TENDER")
            writer.WriteAttributeString("DATE", Me.TranDate.ToShortDateString)
            writer.WriteAttributeString("TILL", CStr(Me.TillId))
            writer.WriteAttributeString("TRAN", CStr(Me.TranNumber))
            writer.WriteAttributeString("NUMB", CStr(Me.Number))
            writer.WriteAttributeString("TYPE", CStr(Me.TenderTypeId))
            writer.WriteAttributeString("AMNT", CStr(Me.ValueTender))
            writer.WriteAttributeString("PIVT", CStr(Me.IsPivotal))
            writer.WriteEndElement()
        End Sub

    End Class

    Public Class PaymentCollection
        Inherits BaseCollection(Of Payment)
    End Class

    <XmlRoot("CTSINTEGRATION")> Public Class XmlWrapper
        Private _xmlTotal As Total
        Private _xmlLines As LineCollection
        Private _xmlPayments As PaymentCollection

        <XmlElement("PVTOTS")> Public Property XmlTotal() As Total
            Get
                Return _xmlTotal
            End Get
            Set(ByVal value As Total)
                _xmlTotal = value
            End Set
        End Property
        <XmlArray("PVLINE"), XmlArrayItem("LINE")> Public Property XmlLines() As LineCollection
            Get
                Return _xmlLines
            End Get
            Set(ByVal value As LineCollection)
                _xmlLines = value
            End Set
        End Property
        <XmlArray("PVPAID"), XmlArrayItem("TENDER")> Public Property XmlPayments() As PaymentCollection
            Get
                Return _xmlPayments
            End Get
            Set(ByVal value As PaymentCollection)
                _xmlPayments = value
            End Set
        End Property

        Public Sub New()
        End Sub

        Public Function Serialise() As String

            Dim serializer As New XmlSerializer(GetType(XmlWrapper))
            Dim sb As New Text.StringBuilder

            Using writer As TextWriter = New StringWriter(sb)
                serializer.Serialize(writer, Me)
            End Using
            Return sb.ToString

        End Function

        Public Shared Function Deserialise(ByVal xmlString As String) As XmlWrapper
            Dim response As XmlWrapper = Nothing
            Dim serializer As New XmlSerializer(GetType(XmlWrapper))

            Using reader As TextReader = New StringReader(xmlString)
                response = CType(serializer.Deserialize(reader), XmlWrapper)
            End Using
            Return response

        End Function

        Public Function Persist() As Boolean
            Return DataAccess.Persist(Me)
        End Function

        Public Function PersistAndUpdateFlashTotals() As Boolean
            Return DataAccess.PersistFlash(Me)
        End Function

    End Class


    <HideModuleName()> Friend Module DataAccess

        Public Function Persist(ByVal wrapper As XmlWrapper) As Boolean

            Using con As New Connection
                Try
                    Dim linesAffected As Integer = 0
                    con.StartTransaction()

                    linesAffected += TotalPersist(wrapper.XmlTotal, con)
                    linesAffected += LinesPersist(wrapper.XmlLines, con)
                    linesAffected += PaymentsPersist(wrapper.XmlPayments, con)

                    If linesAffected = (1 + wrapper.XmlLines.Count + wrapper.XmlPayments.Count) Then
                        con.CommitTransaction()
                        Return True
                    Else
                        con.RollbackTransaction()
                        Return False
                    End If

                Catch ex As Exception
                    con.RollbackTransaction()
                    Throw
                End Try
            End Using

        End Function

        Public Function PersistFlash(ByVal wrapper As XmlWrapper) As Boolean

            Using con As New Connection
                Try
                    Dim linesAffected As Integer = 0
                    con.StartTransaction()

                    linesAffected += TotalPersistFlash(wrapper.XmlTotal, con)
                    linesAffected += LinesPersist(wrapper.XmlLines, con)
                    linesAffected += PaymentsPersistFlash(wrapper.XmlPayments, con)

                    If linesAffected = (1 + wrapper.XmlLines.Count + wrapper.XmlPayments.Count) Then
                        con.CommitTransaction()
                        Return True
                    Else
                        con.RollbackTransaction()
                        Return False
                    End If

                Catch ex As Exception
                    con.RollbackTransaction()
                    Throw
                End Try
            End Using

        End Function


        Public Function TotalPersist(ByVal tot As Total) As Integer
            Using con As New Connection
                Return TotalPersist(tot, con)
            End Using
        End Function

        Public Function TotalPersist(ByVal tot As Total, ByRef con As Connection) As Integer
            Using com As Command = con.NewCommand(My.Resources.Procedures.VisionTotalPersist)
                com.AddParameter(My.Resources.Parameters.TranDate, tot.TranDate)
                com.AddParameter(My.Resources.Parameters.TillId, tot.TillId)
                com.AddParameter(My.Resources.Parameters.TranNumber, tot.TranNumber)
                com.AddParameter(My.Resources.Parameters.CashierId, tot.CashierId)
                com.AddParameter(My.Resources.Parameters.TranDateTime, tot.TranDateTime)
                com.AddParameter(My.Resources.Parameters.Type, tot.Type)
                com.AddParameter(My.Resources.Parameters.ReasonCode, tot.ReasonCode)
                com.AddParameter(My.Resources.Parameters.ReasonDescription, tot.ReasonDescription)
                com.AddParameter(My.Resources.Parameters.OrderNumber, tot.OrderNumber)
                com.AddParameter(My.Resources.Parameters.Value, tot.Value)
                com.AddParameter(My.Resources.Parameters.ValueMerchandising, tot.ValueMerchandising)
                com.AddParameter(My.Resources.Parameters.ValueNonMerchandising, tot.ValueNonMerchandising)
                com.AddParameter(My.Resources.Parameters.ValueTax, tot.ValueTax)
                com.AddParameter(My.Resources.Parameters.ValueDiscount, tot.ValueDiscount)
                com.AddParameter(My.Resources.Parameters.ValueColleagueDiscount, tot.ValueColleagueDiscount)
                com.AddParameter(My.Resources.Parameters.IsColleagueDiscount, tot.IsColleagueDiscount)
                com.AddParameter(My.Resources.Parameters.IsPivotal, tot.IsPivotal)
                Return com.ExecuteNonQuery
            End Using
        End Function

        Public Function TotalPersistFlash(ByVal tot As Total) As Integer
            Using con As New Connection
                Return TotalPersistFlash(tot, con)
            End Using
        End Function

        Public Function TotalPersistFlash(ByVal tot As Total, ByRef con As Connection) As Integer
            Using com As Command = con.NewCommand(My.Resources.Procedures.VisionTotalPersistFlash)
                com.AddParameter(My.Resources.Parameters.TranDate, tot.TranDate)
                com.AddParameter(My.Resources.Parameters.TillId, tot.TillId)
                com.AddParameter(My.Resources.Parameters.TranNumber, tot.TranNumber)
                com.AddParameter(My.Resources.Parameters.CashierId, tot.CashierId)
                com.AddParameter(My.Resources.Parameters.TranDateTime, tot.TranDateTime)
                com.AddParameter(My.Resources.Parameters.Type, tot.Type)
                com.AddParameter(My.Resources.Parameters.ReasonCode, tot.ReasonCode)
                com.AddParameter(My.Resources.Parameters.ReasonDescription, tot.ReasonDescription)
                com.AddParameter(My.Resources.Parameters.OrderNumber, tot.OrderNumber)
                com.AddParameter(My.Resources.Parameters.Value, tot.Value)
                com.AddParameter(My.Resources.Parameters.ValueMerchandising, tot.ValueMerchandising)
                com.AddParameter(My.Resources.Parameters.ValueNonMerchandising, tot.ValueNonMerchandising)
                com.AddParameter(My.Resources.Parameters.ValueTax, tot.ValueTax)
                com.AddParameter(My.Resources.Parameters.ValueDiscount, tot.ValueDiscount)
                com.AddParameter(My.Resources.Parameters.ValueColleagueDiscount, tot.ValueColleagueDiscount)
                com.AddParameter(My.Resources.Parameters.IsColleagueDiscount, tot.IsColleagueDiscount)
                com.AddParameter(My.Resources.Parameters.IsPivotal, tot.IsPivotal)
                Return com.ExecuteNonQuery
            End Using
        End Function


        Public Function LinesPersist(ByVal lines As LineCollection) As Integer
            Using con As New Connection
                Return LinesPersist(lines, con)
            End Using
        End Function

        Public Function LinesPersist(ByVal lines As LineCollection, ByRef con As Connection) As Integer

            Dim linesAffected As Integer = 0
            For Each l As Line In lines
                Using com As Command = con.NewCommand(My.Resources.Procedures.VisionLinePersist)
                    com.AddParameter(My.Resources.Parameters.TranDate, l.TranDate)
                    com.AddParameter(My.Resources.Parameters.TillId, l.TillId)
                    com.AddParameter(My.Resources.Parameters.TranNumber, l.TranNumber)
                    com.AddParameter(My.Resources.Parameters.Number, l.Number)
                    com.AddParameter(My.Resources.Parameters.SkuNumber, l.SkuNumber)
                    com.AddParameter(My.Resources.Parameters.Quantity, l.Quantity)
                    com.AddParameter(My.Resources.Parameters.PriceLookup, l.PriceLookup)
                    com.AddParameter(My.Resources.Parameters.Price, l.Price)
                    com.AddParameter(My.Resources.Parameters.PriceExVat, l.PriceExVat)
                    com.AddParameter(My.Resources.Parameters.PriceExtended, l.PriceExtended)
                    com.AddParameter(My.Resources.Parameters.IsRelatedItemSingle, l.IsRelatedItemSingle)
                    com.AddParameter(My.Resources.Parameters.VatSymbol, l.VatSymbol)
                    com.AddParameter(My.Resources.Parameters.HieCategory, l.HieCategory)
                    com.AddParameter(My.Resources.Parameters.HieGroup, l.HieGroup)
                    com.AddParameter(My.Resources.Parameters.HieSubgroup, l.HieSubgroup)
                    com.AddParameter(My.Resources.Parameters.HieStyle, l.HieStyle)
                    com.AddParameter(My.Resources.Parameters.SaleType, l.SaleType)
                    com.AddParameter(My.Resources.Parameters.IsInStoreStockItem, l.IsInStoreStockItem)
                    com.AddParameter(My.Resources.Parameters.MovementTypeCode, l.MovementTypeCode)
                    com.AddParameter(My.Resources.Parameters.IsPivotal, l.IsPivotal)
                    linesAffected += com.ExecuteNonQuery
                End Using
            Next
            Return linesAffected

        End Function


        Public Function PaymentsPersist(ByVal payments As PaymentCollection) As Integer
            Using con As New Connection
                Return PaymentsPersist(payments, con)
            End Using
        End Function

        Public Function PaymentsPersist(ByVal payments As PaymentCollection, ByRef con As Connection) As Integer

            Dim linesAffected As Integer = 0
            For Each l As Payment In payments
                Using com As Command = con.NewCommand(My.Resources.Procedures.VisionPaymentPersist)
                    com.AddParameter(My.Resources.Parameters.TranDate, l.TranDate)
                    com.AddParameter(My.Resources.Parameters.TillId, l.TillId)
                    com.AddParameter(My.Resources.Parameters.TranNumber, l.TranNumber)
                    com.AddParameter(My.Resources.Parameters.Number, l.Number)
                    com.AddParameter(My.Resources.Parameters.TenderTypeId, l.TenderTypeId)
                    com.AddParameter(My.Resources.Parameters.ValueTender, l.ValueTender)
                    com.AddParameter(My.Resources.Parameters.IsPivotal, l.IsPivotal)
                    linesAffected += com.ExecuteNonQuery
                End Using
            Next
            Return linesAffected

        End Function

        Public Function PaymentsPersistFlash(ByVal payments As PaymentCollection) As Integer
            Using con As New Connection
                Return PaymentsPersistFlash(payments, con)
            End Using
        End Function

        Public Function PaymentsPersistFlash(ByVal payments As PaymentCollection, ByRef con As Connection) As Integer

            Dim linesAffected As Integer = 0
            For Each l As Payment In payments
                Using com As Command = con.NewCommand(My.Resources.Procedures.VisionPaymentPersistFlash)
                    com.AddParameter(My.Resources.Parameters.TranDate, l.TranDate)
                    com.AddParameter(My.Resources.Parameters.TillId, l.TillId)
                    com.AddParameter(My.Resources.Parameters.TranNumber, l.TranNumber)
                    com.AddParameter(My.Resources.Parameters.Number, l.Number)
                    com.AddParameter(My.Resources.Parameters.TenderTypeId, l.TenderTypeId)
                    com.AddParameter(My.Resources.Parameters.ValueTender, l.ValueTender)
                    com.AddParameter(My.Resources.Parameters.IsPivotal, l.IsPivotal)
                    linesAffected += com.ExecuteNonQuery
                End Using
            Next
            Return linesAffected

        End Function

    End Module

End Namespace

