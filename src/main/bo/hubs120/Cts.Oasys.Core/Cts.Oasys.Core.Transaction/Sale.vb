﻿Imports Cts.Oasys.Data
Imports System.Text

Namespace Sale

    <HideModuleName()> Public Module SharedFunctions

        Public Function GetTotal(ByVal tranDate As Date, ByVal tillId As Integer, ByVal tranNumber As String) As Total
            Dim dt As DataTable = DataAccess.GetTransaction(tranDate, tillId, tranNumber)
            If (dt IsNot Nothing) AndAlso (dt.Rows.Count > 0) Then
                Dim tran As New Total(dt.Rows(0))
                Return tran
            End If
            Return Nothing
        End Function

    End Module

    Public Class Total
        Inherits Base

#Region "       Table Fields"
        Private _tranDate As Date
        Private _tillId As Integer
        Private _tranNumber As Integer
        Private _orderNumber As String
        Private _isVoided As Boolean
        Private _isTraining As Boolean
        Private _isComplete As Boolean

        <ColumnMapping("TranDate")> Public Property TranDate() As Date
            Get
                Return _tranDate
            End Get
            Set(ByVal value As Date)
                _tranDate = value
            End Set
        End Property
        <ColumnMapping("TillId")> Public Property TillId() As Integer
            Get
                Return _tillId
            End Get
            Set(ByVal value As Integer)
                _tillId = value
            End Set
        End Property
        <ColumnMapping("TranNumber")> Public Property TranNumber() As Integer
            Get
                Return _tranNumber
            End Get
            Set(ByVal value As Integer)
                _tranNumber = value
            End Set
        End Property
        <ColumnMapping("OrderNumber")> Public Property OrderNumber() As String
            Get
                Return _orderNumber
            End Get
            Set(ByVal value As String)
                _orderNumber = value
            End Set
        End Property
        <ColumnMapping("IsVoided")> Public Property IsVoided() As Boolean
            Get
                Return _isVoided
            End Get
            Set(ByVal value As Boolean)
                _isVoided = value
            End Set
        End Property
        <ColumnMapping("IsTraining")> Public Property IsTraining() As Boolean
            Get
                Return _isTraining
            End Get
            Set(ByVal value As Boolean)
                _isTraining = value
            End Set
        End Property
        <ColumnMapping("IsComplete")> Public Property IsComplete() As Boolean
            Get
                Return _isComplete
            End Get
            Set(ByVal value As Boolean)
                _isComplete = value
            End Set
        End Property
#End Region

#Region "       Properties"
        Private _lines As LineCollection = Nothing
        Private _payments As PaymentCollection = Nothing

        Public ReadOnly Property Lines() As LineCollection
            Get
                If _lines Is Nothing Then LoadLines()
                Return _lines
            End Get
        End Property
        Public ReadOnly Property Payments() As PaymentCollection
            Get
                If _payments Is Nothing Then LoadPayments()
                Return _payments
            End Get
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub

        Public Sub LoadLines()
            _lines = New LineCollection
            _lines.LoadLines(Me.TranDate, Me.TillId, Me.TranNumber)
        End Sub

        Public Sub LoadPayments()
            _payments = New PaymentCollection
            _payments.LoadPayments(Me.TranDate, Me.TillId, Me.TranNumber)
        End Sub

    End Class

    Public Class Line
        Inherits Base

#Region "       Table Fields"
        Private _tranDate As Date
        Private _tillId As Integer
        Private _tranNumber As Integer
        Private _number As Integer
        Private _skuNumber As String
        Private _quantity As Integer
        Private _price As Decimal

        <ColumnMapping("TranDate")> Public Property TranDate() As Date
            Get
                Return _tranDate
            End Get
            Set(ByVal value As Date)
                _tranDate = value
            End Set
        End Property
        <ColumnMapping("TillId")> Public Property TillId() As Integer
            Get
                Return _tillId
            End Get
            Set(ByVal value As Integer)
                _tillId = value
            End Set
        End Property
        <ColumnMapping("TranNumber")> Public Property TranNumber() As Integer
            Get
                Return _tranNumber
            End Get
            Set(ByVal value As Integer)
                _tranNumber = value
            End Set
        End Property
        <ColumnMapping("Number")> Public Property Number() As Integer
            Get
                Return _number
            End Get
            Set(ByVal value As Integer)
                _number = value
            End Set
        End Property
        <ColumnMapping("SkuNumber")> Public Property SkuNumber() As String
            Get
                Return _skuNumber
            End Get
            Set(ByVal value As String)
                _skuNumber = value
            End Set
        End Property
        <ColumnMapping("Quantity")> Public Property Quantity() As Integer
            Get
                Return _quantity
            End Get
            Set(ByVal value As Integer)
                _quantity = value
            End Set
        End Property
        <ColumnMapping("Price")> Public Property Price() As Decimal
            Get
                Return _price
            End Get
            Set(ByVal value As Decimal)
                _price = value
            End Set
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

    End Class

    Public Class LineCollection
        Inherits BaseCollection(Of Line)

        Public Sub LoadLines(ByVal tranDate As Date, ByVal tillId As Integer, ByVal tranNumber As Integer)
            Dim dt As DataTable = DataAccess.GetLines(tranDate, tillId, tranNumber)
            Me.Load(dt)
        End Sub

        Public Function GetLine(ByVal skuNumber As String) As Line
            For Each l As Line In Me.Items
                If l.SkuNumber = skuNumber Then Return l
            Next
            Return Nothing
        End Function

    End Class

    Public Class Payment
        Inherits Base

#Region "       Table Fields"
        Private _tranDate As Date
        Private _tillId As Integer
        Private _tranNumber As Integer
        Private _number As Integer

        <ColumnMapping("TranDate")> Public Property TranDate() As Date
            Get
                Return _tranDate
            End Get
            Set(ByVal value As Date)
                _tranDate = value
            End Set
        End Property
        <ColumnMapping("TillId")> Public Property TillId() As Integer
            Get
                Return _tillId
            End Get
            Set(ByVal value As Integer)
                _tillId = value
            End Set
        End Property
        <ColumnMapping("TranNumber")> Public Property TranNumber() As Integer
            Get
                Return _tranNumber
            End Get
            Set(ByVal value As Integer)
                _tranNumber = value
            End Set
        End Property
        <ColumnMapping("Number")> Public Property Number() As Integer
            Get
                Return _number
            End Get
            Set(ByVal value As Integer)
                _number = value
            End Set
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

    End Class

    Public Class PaymentCollection
        Inherits BaseCollection(Of Payment)

        Public Sub LoadPayments(ByVal tranDate As Date, ByVal tillId As Integer, ByVal tranNumber As Integer)
            Dim dt As DataTable = DataAccess.GetPayments(tranDate, tillId, tranNumber)
            Me.Load(dt)
        End Sub

    End Class

    Friend Module DataAccess

        Friend Function GetTransaction(ByVal tranDate As Date, ByVal tillId As Integer, ByVal tranNumber As String) As DataTable

            Using con As New Connection
                Using com As Command = con.NewCommand()
                    Select Case con.DataProvider
                        Case DataProvider.Odbc
                            Dim sb As New StringBuilder
                            sb.Append("SELECT ")
                            sb.Append("DATE1 as TranDate, ")
                            sb.Append("TILL as TillId, ")
                            sb.Append("[TRAN] as TranNumber, ")
                            sb.Append("ORDN as OrderNumber, ")
                            sb.Append("VOID as IsVoided, ")
                            sb.Append("TMOD as IsTraining, ")
                            sb.Append("ICOM as IsComplete ")
                            sb.Append("FROM DLTOTS ")
                            sb.Append("WHERE DATE1=? AND TILL=? AND TRAN=? ")
                            com.CommandText = sb.ToString
                            com.AddParameter("Date", tranDate)
                            com.AddParameter("Till", tillId)
                            com.AddParameter("Tran", tranNumber)

                        Case DataProvider.Sql
                            com.StoredProcedureName = My.Resources.Procedures.TransactionGet
                            com.AddParameter(My.Resources.Parameters.TranDate, tranDate)
                            com.AddParameter(My.Resources.Parameters.TillId, tillId)
                            com.AddParameter(My.Resources.Parameters.TranNumber, tranNumber)
                    End Select
                    Return com.ExecuteDataTable

                End Using
            End Using
            Return Nothing

        End Function

        Friend Function GetLines(ByVal tranDate As Date, ByVal tillId As Integer, ByVal tranNumber As Integer) As DataTable

            Using con As New Connection
                Using com As Command = con.NewCommand()
                    Select Case con.DataProvider
                        Case DataProvider.Odbc
                            Dim sb As New StringBuilder
                            sb.Append("SELECT ")
                            sb.Append("DATE1 as TranDate, ")
                            sb.Append("TILL as TillId, ")
                            sb.Append("[TRAN] as TranNumber, ")
                            sb.Append("NUMB as Number, ")
                            sb.Append("SKUN as SkuNumber, ")
                            sb.Append("QUAN as Quantity, ")
                            sb.Append("PRIC as Price ")
                            sb.Append("FROM DLLINE ")
                            sb.Append("WHERE DATE1=? AND TILL=? AND TRAN=? ")
                            sb.Append("ORDER BY NUMB")
                            com.CommandText = sb.ToString
                            com.AddParameter("Date", tranDate)
                            com.AddParameter("Till", tillId)
                            com.AddParameter("Tran", tranNumber)

                        Case DataProvider.Sql
                            com.StoredProcedureName = My.Resources.Procedures.TransactionLinesGet
                            com.AddParameter(My.Resources.Parameters.TranDate, tranDate)
                            com.AddParameter(My.Resources.Parameters.TillId, tillId)
                            com.AddParameter(My.Resources.Parameters.TranNumber, tranNumber)
                    End Select
                    Return com.ExecuteDataTable

                End Using
            End Using
            Return Nothing

        End Function

        Friend Function GetPayments(ByVal tranDate As Date, ByVal tillId As Integer, ByVal tranNumber As Integer) As DataTable

            Using con As New Connection
                Using com As Command = con.NewCommand()
                    Select Case con.DataProvider
                        Case DataProvider.Odbc
                            Dim sb As New StringBuilder
                            sb.Append("SELECT ")
                            sb.Append("DATE1 as TranDate, ")
                            sb.Append("TILL as TillId, ")
                            sb.Append("[TRAN] as TranNumber, ")
                            sb.Append("NUMB as Number ")
                            sb.Append("FROM DLPAID ")
                            sb.Append("WHERE DATE1=? AND TILL=? AND TRAN=? ")
                            sb.Append("ORDER BY NUMB")
                            com.CommandText = sb.ToString
                            com.AddParameter("Date", tranDate)
                            com.AddParameter("Till", tillId)
                            com.AddParameter("Tran", tranNumber)

                        Case DataProvider.Sql
                            com.StoredProcedureName = My.Resources.Procedures.TransactionPaymentsGet
                            com.AddParameter(My.Resources.Parameters.TranDate, tranDate)
                            com.AddParameter(My.Resources.Parameters.TillId, tillId)
                            com.AddParameter(My.Resources.Parameters.TranNumber, tranNumber)
                    End Select
                    Return com.ExecuteDataTable

                End Using
            End Using
            Return Nothing

        End Function

    End Module

End Namespace




