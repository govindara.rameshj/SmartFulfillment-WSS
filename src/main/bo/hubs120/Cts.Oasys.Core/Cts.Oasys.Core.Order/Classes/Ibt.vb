﻿Imports Cts.Oasys.Data
Imports Cts.Oasys.Core
Imports System.ComponentModel
Imports System.Text

Namespace Ibt

    Public Enum IBTType
        IBTIn = 1
        IBTOut = 2
    End Enum

    'For future use!
    Public Enum IBTClass
        IBTSale = 1
        IBTRefund = 2
    End Enum


    Public Class IBTHeader
        Inherits Base

#Region "Private Variables"
        Private _Number As String
        Private _Type As String
        Private _IBTDate As Date
        Private _Value As Decimal
        Private _Info As String
        Private _StoreNo As String
        Private _IBTNumber As String

        Private _Lines As IBTLineCollection = Nothing
#End Region

#Region "Properties"
        Public Property Number() As String
            Get
                Return _Number
            End Get
            Friend Set(ByVal value As String)
                _Number = value
            End Set
        End Property
        Public Property Type() As String
            Get
                Return _Type
            End Get
            Set(ByVal value As String)
                _Type = value
            End Set
        End Property
        Public Property IBTDate() As Date
            Get
                Return _IBTDate
            End Get
            Friend Set(ByVal value As Date)
                _IBTDate = value
            End Set
        End Property
        Public Property Value() As Decimal
            Get
                Return _Value
            End Get
            Set(ByVal value As Decimal)
                _Value = value
            End Set
        End Property

        Public Property Info() As String
            Get
                Return _Info
            End Get
            Set(ByVal value As String)
                _Info = value
            End Set
        End Property

        Public Property StoreNo() As String
            Get
                Return _StoreNo
            End Get
            Set(ByVal value As String)
                _StoreNo = value
            End Set
        End Property

        Public Property IBTNumber() As String
            Get
                Return _IBTNumber
            End Get
            Set(ByVal value As String)
                _IBTNumber = value
            End Set
        End Property

        Public ReadOnly Property Lines() As IBTLineCollection
            Get
                Return _Lines
            End Get
        End Property

#End Region
        ''' <summary>
        ''' Creates a new IBT for the CTS Fulfilment Request
        ''' </summary>
        ''' <param name="qodHeader"></param>
        ''' <history></history>
        ''' <remarks></remarks>
        Public Sub New(ByVal qodHeader As Qod.QodHeader)
            'Call the base class constructor
            MyBase.New()
            'Get a new IBT number
            Me.Number = GetIBTNumber()
            Dim ValueTotal As Decimal
            Dim Index As Integer = 1
            _Lines = New IBTLineCollection
            If ThisStore.Id4 <> qodHeader.SellingStoreId Then
                'Get the IBT Details from the SALE lines
                For Each QodLine As Qod.QodLine In qodHeader.Lines
                    If QodLine.DeliverySource = ThisStore.Id4.ToString Then
                        Dim IBTLine As New IBTLine
                        IBTLine.SequenceNumber = Index.ToString("0000")
                        IBTLine.SKUNumber = QodLine.SkuNumber
                        IBTLine.Price = QodLine.Price
                        IBTLine.Quantity = QodLine.QtyToDeliver
                        IBTLine.HeaderNumber = Me.Number
                        ValueTotal += IBTLine.Price * IBTLine.Quantity
                        _Lines.Add(IBTLine)
                        Index += 1
                    End If
                Next
            End If
            'Add the summary details
            Me.Type = CStr(CInt(IBTType.IBTOut))
            Me.IBTDate = Now.Date
            Me.Value = ValueTotal
            Me.Info = "OM Reference " & qodHeader.OmOrderNumber.ToString
            Me.StoreNo = qodHeader.SellingStoreId.ToString("0000").Substring(1)

        End Sub
        ''' <summary>
        ''' Creates a new IBT for the CTS Update Refund Request
        ''' </summary>
        ''' <param name="refundRequest"></param>
        ''' <history></history>
        ''' <remarks></remarks>
        Public Sub New(ByVal refundRequest As WebService.CTSUpdateRefundRequest)
            'Call the base class constructor
            MyBase.New()
            'Get a new IBT number
            Me.Number = GetIBTNumber()
            Dim ValueTotal As Decimal = 0
            Dim Index As Integer = 1
            _Lines = New IBTLineCollection
            'Loop round all details in the request from OM
            For Each Refund As WebService.CTSUpdateRefundRequestOrderRefund In _
                    refundRequest.OrderRefunds
                If Refund.FulfilmentSites Is Nothing Then Exit Sub
                If Refund.FulfilmentSites.Count > 0 Then
                    For Each FulfilmentSite As WebService.CTSUpdateRefundRequestOrderRefundFulfilmentSite In _
                            Refund.FulfilmentSites
                        If FulfilmentSite.FulfilmentSiteCode = ThisStore.Id4.ToString Then
                            Me.IBTNumber = FulfilmentSite.SellingStoreIBTOutNumber.PadLeft(6, "0"c)
                        End If
                    Next
                End If
                For Each RefundLine As WebService.CTSUpdateRefundRequestOrderRefundRefundLine In _
                        Refund.RefundLines
                    If RefundLine.FulfilmentSiteCode = ThisStore.Id4.ToString AndAlso _
                            CInt(RefundLine.QuantityCancelled) > 0 Then
                        Dim IBTLine As New IBTLine
                        IBTLine.SequenceNumber = Index.ToString("0000")
                        IBTLine.SKUNumber = RefundLine.ProductCode
                        IBTLine.Quantity = CInt(RefundLine.QuantityCancelled)
                        IBTLine.HeaderNumber = Me.Number
                        IBTLine.Price = RefundLine.RefundLineValue / _
                            (CInt(RefundLine.QuantityCancelled) + CInt(RefundLine.QuantityReturned))
                        ValueTotal += (IBTLine.Price * IBTLine.Quantity)
                        _Lines.Add(IBTLine)
                        Index += 1
                    End If
                Next
            Next
            Me.Type = CStr(CInt(IBTType.IBTIn))
            Me.IBTDate = Now.Date
            Me.Value = ValueTotal
            Me.Info = "OM Reference " & CInt(refundRequest.OMOrderNumber.ToString).ToString
            Me.StoreNo = refundRequest.OrderRefunds(0).RefundStoreCode.Substring(1)

        End Sub
        ''' <summary>
        ''' Creates a new IBT for the CTS Status Notification Request
        ''' </summary>
        ''' <param name="qodHeader"></param>
        ''' <param name="fulfilmentSite"></param>
        ''' <history></history>
        ''' <remarks></remarks>
        Public Sub New(ByVal qodHeader As Qod.QodHeader, _
                       ByVal fulfilmentSite As WebService.CTSStatusNotificationRequestFulfilmentSite)
            'Call the base class constructor
            MyBase.New()
            'Get a new IBT number
            Me.Number = GetIBTNumber()
            Dim ValueTotal As Decimal = 0
            Dim Index As Integer = 1
            _Lines = New IBTLineCollection
            If fulfilmentSite.FulfilmentSite.Value <> ThisStore.Id4.ToString Then
                For Each OrderLine As WebService.CTSStatusNotificationRequestFulfilmentSiteOrderLine In _
                        fulfilmentSite.OrderLines
                    Dim IBTLine As New IBTLine
                    IBTLine.SequenceNumber = Index.ToString("0000")
                    IBTLine.SKUNumber = OrderLine.ProductCode.Value
                    'Get the correct order line
                    Dim QodLine As Qod.QodLine = GetOrderLine(qodHeader, _
                                        OrderLine.SellingStoreLineNo.Value.ToString.PadLeft(4, "0"c))
                    IBTLine.Quantity = QodLine.QtyToDeliver
                    'Check if there is an "on hold" refund for this line and temporarily reverse it to get the correct quantities
                    ' Referral 758 - If lower than 'on hold', i.e. refunded at this store but not gone to OM for confirmation, then
                    ' also adjust the quantity to account for the refund
                    Dim QodRefunds As Qod.QodRefundCollection = GetRefundLines(qodHeader, _
                                        OrderLine.SellingStoreLineNo.Value.ToString.PadLeft(4, "0"c), _
                                        Qod.State.Refund.CreatedOnHold, True)
                    If QodRefunds.Count > 0 Then
                        For Each QodRefund As Qod.QodRefund In QodRefunds
                            'Ref 778: If a refund happened before a sale was sent to OM, the delivery quantity 
                            'will be zero if all refunded which would use this routine which totals up the 
                            'whole refunded items qty. We just total up the line were on now, by introducing the if below.
                            If CInt(OrderLine.OMOrderLineNo.Value) = CInt(QodRefund.Number) Then
                                IBTLine.Quantity = IBTLine.Quantity + QodRefund.QtyCancelled
                                Exit For
                            End If
                        Next
                    End If
                    IBTLine.Price = QodLine.Price
                    IBTLine.HeaderNumber = Me.Number
                    ValueTotal += (IBTLine.Price * IBTLine.Quantity)
                    _Lines.Add(IBTLine)
                    Index += 1
                Next
            End If
            Me.Type = CStr(CInt(IBTType.IBTIn))
            Me.IBTDate = Now.Date
            Me.IBTNumber = fulfilmentSite.FulfilmentSiteIBTOutNumber.Value.PadLeft(6, "0"c)
            Me.Value = ValueTotal
            Me.Info = "OM Reference " & qodHeader.OmOrderNumber.ToString
            Me.StoreNo = fulfilmentSite.FulfilmentSite.Value.Substring(1)

        End Sub

        Public Sub New(ByVal qodRefund As Qod.QodRefund)
            MyBase.New()
            Me.Number = GetIBTNumber()
            Me.Type = CStr(CInt(IBTType.IBTOut))
            Dim IBTLine As New IBTLine
            IBTLine.SequenceNumber = "0001"
            IBTLine.Price = qodRefund.Price
            IBTLine.Quantity = qodRefund.QtyCancelled
            IBTLine.SKUNumber = qodRefund.SkuNumber
            IBTLine.HeaderNumber = Me.Number
            Me.IBTDate = Now.Date
            Me.Value = IBTLine.Quantity * IBTLine.Price
            _Lines = New IBTLineCollection
            _Lines.Add(IBTLine)
        End Sub

        Public Function GetNextSequence() As String
            Dim MaxNumber As Integer = 0
            For Each IBTLine As IBTLine In Lines
                MaxNumber = Math.Max(CInt(IBTLine.SequenceNumber), MaxNumber)
            Next
            Return (MaxNumber + 1).ToString("0000")
        End Function

        Private Function GetOrderLine(ByVal qodHeader As Qod.QodHeader, ByVal lineNo As String) As Qod.QodLine
            Dim LineToFind As Qod.QodLine = Nothing
            For Each QodLine As Qod.QodLine In qodHeader.Lines
                If QodLine.Number = lineNo Then
                    LineToFind = QodLine
                End If
            Next
            Return LineToFind
        End Function

        ' Referral 758 - Allow for refunds at the specified Refund Status OR optionally equal to or below the specified Refund Status
        Private Function GetRefundLines(ByVal qodHeader As Qod.QodHeader, ByVal lineNo As String, ByVal RefundStatus As Integer, Optional ByVal IncludeLowerStatuses As Boolean = False) As Qod.QodRefundCollection
            Dim LinesToFind As New Qod.QodRefundCollection

            For Each QodRefund As Qod.QodRefund In qodHeader.Refunds
                If QodRefund.Number = lineNo _
                And (QodRefund.RefundStatus = RefundStatus) _
                Or (IncludeLowerStatuses And QodRefund.RefundStatus <= RefundStatus) Then
                    LinesToFind.Add(QodRefund)
                End If
            Next
            Return LinesToFind
        End Function

        Private Function GetIBTNumber() As String

            Dim linesUpdated As Integer = 0
            Dim IBTNumber As String = ""
            Using con As New Connection
                Try
                    con.StartTransaction()
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                'get next receipt number from system numbers (check not over limit)
                                com.CommandText = "Select NEXT4 from SYSNUM where FKEY='01'"
                                Dim orderNumber As Integer = CInt(com.ExecuteValue)
                                If orderNumber > 999998 Then orderNumber = 1
                                'update next number and set order item number
                                com.CommandText = "Update SYSNUM set NEXT4=? where FKEY='01'"
                                com.AddParameter("Number", (orderNumber + 1).ToString("000000"))
                                com.ExecuteNonQuery()
                                IBTNumber = orderNumber.ToString("000000")
                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.IBTNumberGet
                                Dim orderNumber As Integer = CInt(com.ExecuteValue)
                                IBTNumber = orderNumber.ToString("000000")
                        End Select
                    End Using
                    con.CommitTransaction()
                Catch ex As Exception
                    con.RollbackTransaction()
                End Try
            End Using
            Return IBTNumber
        End Function

        Public Function SelfPersist() As Integer
            Dim LinesUpdated As Integer = 0
            Using con As New Connection
                Try
                    con.StartTransaction()
                    LinesUpdated = Persist(con)
                    con.CommitTransaction()
                Catch ex As Exception
                    con.RollbackTransaction()
                End Try
            End Using
            Return LinesUpdated
        End Function

        Public Function Persist(ByVal con As Connection) As Integer

            Dim LinesUpdated As Integer = 0
            Dim sb As StringBuilder
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        'insert into drlsum
                        sb = New StringBuilder
                        sb.Append("Insert into DRLSUM (NUMB,TYPE,DATE1,INIT,INFO,VALU,""1STR"",""1PRT"",""1IBT"",RTI) ")
                        sb.Append("values (?,?,?,'Auto',?,?,?,0,?,'N')")

                        com.ClearParamters()
                        com.CommandText = sb.ToString
                        com.AddParameter("Number", Me.Number)
                        com.AddParameter("Type", Me.Type)
                        com.AddParameter("IBTDate", Me.IBTDate)
                        com.AddParameter("Info", Me.Info)
                        com.AddParameter("Value", Me.Value)
                        com.AddParameter("IbtStoreId", Right$(Me.StoreNo.PadLeft(3, "0"c), 3))
                        com.AddParameter("IbtConsignmentNumber", IIf(Me.IBTNumber IsNot Nothing, Me.IBTNumber, "000000"))
                        LinesUpdated += com.ExecuteNonQuery()

                        'Insert all lines
                        For Each IBTLine As IBTLine In Me.Lines

                            'insert drldet record
                            com.ClearParamters()
                            sb = New StringBuilder
                            sb.Append("Insert into DRLDET (NUMB,SEQN,SKUN,ORDQ,ORDP,RECQ,PRIC,IBTQ,RETQ,POLN,RTI) ")
                            sb.Append("values (?,?,?,0,0,0,?,?,0,0,'N')")

                            com.CommandText = sb.ToString
                            com.AddParameter("NUMB", IBTLine.HeaderNumber)
                            com.AddParameter("SEQN", IBTLine.SequenceNumber)
                            com.AddParameter("SKUN", IBTLine.SKUNumber)
                            com.AddParameter("PRIC", IBTLine.Price)
                            com.AddParameter("IBTQ", IBTLine.Quantity)
                            LinesUpdated += com.ExecuteNonQuery

                            'Get stock values
                            com.ClearParamters()
                            com.CommandText = "Select ONHA,RETQ,MDNQ,WTFQ,PRIC from STKMAS where SKUN=?"
                            com.AddParameter("SkuNumber", IBTLine.SKUNumber)

                            Dim dt As DataTable = com.ExecuteDataTable
                            Dim logType As String = "41"
                            Dim stockStart As Integer = CInt(dt.Rows(0)("ONHA"))
                            Dim returnsStart As Integer = CInt(dt.Rows(0)("RETQ"))
                            Dim markdownsStart As Integer = CInt(dt.Rows(0)("MDNQ"))
                            Dim writeOffsStart As Integer = CInt(dt.Rows(0)("WTFQ"))
                            Dim priceStart As Decimal = CDec(dt.Rows(0)("PRIC"))
                            Dim ibtQty As Integer = IBTLine.Quantity
                            Dim ibtValue As Decimal = IBTLine.Price

                            'If Type is IBTIn then change the sign of ibtQty
                            If CInt(Me.Type) = IBTType.IBTIn Then
                                logType = "42"
                                ibtQty *= -1
                            End If

                            'Include in sales
                            com.ClearParamters()
                            com.CommandText = "Update STKMAS set US001=US001+? where SKUN=?"
                            com.AddParameter("IbtQty", ibtQty)
                            com.AddParameter("SkuNumber", IBTLine.SKUNumber)
                            LinesUpdated += com.ExecuteNonQuery

                            'Update stock line
                            com.ClearParamters()
                            com.CommandText = "Update STKMAS set ONHA=ONHA-?, TACT=1, MIBQ1=MIBQ1-?, MIBV1=MIBV1-? where SKUN=?"
                            com.AddParameter("IbtQty", ibtQty)
                            com.AddParameter("IbtQty", ibtQty)
                            com.AddParameter("IbtVal", (ibtQty * ibtValue))
                            com.AddParameter("SkuNumber", IBTLine.SKUNumber)
                            LinesUpdated += com.ExecuteNonQuery

                            'Check for stock log 91
                            LinesUpdated += CheckStockLog91Odbc(con, IBTLine.SKUNumber, stockStart)

                            'Insert stock log line
                            sb = New StringBuilder
                            sb.Append("Insert into STKLOG (")
                            sb.Append("SKUN,DAYN,TYPE,DATE1,TIME,KEYS,EEID,ICOM,SSTK,ESTK,SRET,ERET,SMDN,EMDN,SWTF,EWTF,SPRI,EPRI,RTI")
                            sb.Append(") values (?,?,?,?,?,?,'0',0,?,?,?,?,?,?,?,?,?,?,'S' )")

                            com.ClearParamters()
                            com.CommandText = sb.ToString
                            com.AddParameter("SkuNumber", IBTLine.SKUNumber)
                            com.AddParameter("DayNumber", DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1)
                            com.AddParameter("Type", logType)
                            com.AddParameter("Date", Now.Date)
                            com.AddParameter("Time", Format(Now, "HHmmss"))
                            com.AddParameter("Keys", IBTLine.HeaderNumber & Space(1) & IBTLine.SequenceNumber)
                            com.AddParameter("StockStart", stockStart)
                            com.AddParameter("StockEnd", stockStart - ibtQty)
                            com.AddParameter("ReturnsStart", returnsStart)
                            com.AddParameter("ReturnsEnd", returnsStart)
                            com.AddParameter("MarkdownStart", markdownsStart)
                            com.AddParameter("MarkdownEnd", markdownsStart)
                            com.AddParameter("WriteoffStart", writeOffsStart)
                            com.AddParameter("WriteoffEnd", writeOffsStart)
                            com.AddParameter("PriceStart", priceStart)
                            com.AddParameter("PriceEnd", priceStart)
                            LinesUpdated += com.ExecuteNonQuery
                        Next

                    Case DataProvider.Sql
                        com.StoredProcedureName = My.Resources.Procedures.IBTInsert
                        com.ClearParamters()
                        com.AddParameter("Number", Me.Number)
                        com.AddParameter("Type", Me.Type)
                        com.AddParameter("Date", Me.IBTDate)
                        com.AddParameter("Info", Me.Info)
                        com.AddParameter("Value", Me.Value)
                        If Me.StoreNo.Length > 3 Then
                            com.AddParameter("1Str", Me.StoreNo.ToString.Substring(1, 3))
                        Else
                            com.AddParameter("1Str", Me.StoreNo)
                        End If
                        com.AddParameter("1IBT", IIf(Me.IBTNumber IsNot Nothing, Me.IBTNumber, "000000"))
                        LinesUpdated = com.ExecuteNonQuery()

                        'Insert all lines
                        For Each Line As IBTLine In Me.Lines
                            com.StoredProcedureName = My.Resources.Procedures.IbtInsertLine
                            com.ClearParamters()
                            com.AddParameter(My.Resources.Parameters.Type, Me.Type)
                            com.AddParameter(My.Resources.Parameters.ReceiptNumber, Line.HeaderNumber)
                            com.AddParameter(My.Resources.Parameters.Sequence, Line.SequenceNumber)
                            com.AddParameter(My.Resources.Parameters.SkuNumber, Line.SKUNumber)
                            com.AddParameter(My.Resources.Parameters.ReceivedPrice, Line.Price)
                            com.AddParameter(My.Resources.Parameters.IbtQty, Line.Quantity)
                            LinesUpdated += com.ExecuteNonQuery()
                        Next

                End Select
            End Using
            Return LinesUpdated

        End Function

        Friend Function CheckStockLog91Odbc(ByRef con As Connection, ByVal skuNumber As String, ByVal stockStart As Integer) As Integer

            Dim linesUpdated As Integer = 0

            Using com As New Command(con)
                Dim stockEnd As Integer = 0
                Dim markdownEnd As Integer = 0
                Dim writeoffEnd As Integer = 0
                Dim returnsEnd As Integer = 0
                Dim priceEnd As Decimal = 0

                'check that last log qty = last qty for sku
                com.CommandText = "Select top 2 ESTK,EMDN,EWTF,ERET,EPRI,TYPE from STKLOG where SKUN=? and DAYN>0 order by TKEY desc"
                com.AddParameter("SkuNumber", skuNumber)
                Dim dt As DataTable = com.ExecuteDataTable
                If IsDBNull(dt) Then Return 0

                Dim dr As DataRow = Nothing
                Select Case dt.Rows.Count
                    Case 0
                        Return 0
                    Case 1
                        dr = dt.Rows(0)
                    Case 2
                        dr = dt.Rows(0)
                        If dt.Rows(0).Item("TYPE").ToString = "99" Then dr = dt.Rows(1)
                    Case Else
                        Return 0
                End Select

                stockEnd = CInt(dr("ESTK"))
                markdownEnd = CInt(dr("EMDN"))
                writeoffEnd = CInt(dr("EWTF"))
                returnsEnd = CInt(dr("ERET"))
                priceEnd = CDec(dr("EPRI"))

                'insert adjustment if needed
                If stockEnd <> stockStart Then
                    Dim sb As New StringBuilder
                    sb.Append("Insert into STKLOG (")
                    sb.Append("SKUN,DAYN,TYPE,DATE1,TIME,KEYS,EEID,ICOM,SSTK,ESTK,SRET,ERET,SMDN,EMDN,SWTF,EWTF,SPRI,EPRI,RTI")
                    sb.Append(") values (?,?,'91',?,?,'System Adjustment 91','0',0,?,?,?,?,?,?,?,?,?,?,'S' )")

                    com.ClearParamters()
                    com.CommandText = sb.ToString
                    com.AddParameter("SkuNumber", skuNumber)
                    com.AddParameter("DayNumber", DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1)
                    com.AddParameter("Date", Now.Date)
                    com.AddParameter("Time", Format(Now, "HHmmss"))
                    com.AddParameter("StockEnd", stockEnd)
                    com.AddParameter("StockStart", stockStart)
                    com.AddParameter("ReturnsStart", returnsEnd)
                    com.AddParameter("ReturnsEnd", returnsEnd)
                    com.AddParameter("MarkdownStart", markdownEnd)
                    com.AddParameter("MarkdownEnd", markdownEnd)
                    com.AddParameter("WriteoffStart", writeoffEnd)
                    com.AddParameter("WriteoffEnd", writeoffEnd)
                    com.AddParameter("PriceStart", priceEnd)
                    com.AddParameter("PriceEnd", priceEnd)
                    linesUpdated += com.ExecuteNonQuery
                End If
            End Using

            Return linesUpdated

        End Function

    End Class


    Public Class IBTLine
        Inherits Base

#Region "Private Variables"
        Private _HeaderNumber As String
        Private _SequenceNumber As String
        Private _SKUNumber As String
        Private _Price As Decimal
        Private _Quantity As Integer
#End Region

#Region "Properties"
        Public Property HeaderNumber() As String
            Get
                Return _HeaderNumber
            End Get
            Friend Set(ByVal value As String)
                _HeaderNumber = value
            End Set
        End Property

        Public Property SequenceNumber() As String
            Get
                Return _SequenceNumber
            End Get
            Set(ByVal value As String)
                _SequenceNumber = value
            End Set
        End Property

        Public Property SKUNumber() As String
            Get
                Return _SKUNumber
            End Get
            Set(ByVal value As String)
                _SKUNumber = value
            End Set
        End Property

        Public Property Price() As Decimal
            Get
                Return _Price
            End Get
            Set(ByVal value As Decimal)
                _Price = value
            End Set
        End Property

        Public Property Quantity() As Integer
            Get
                Return _Quantity
            End Get
            Set(ByVal value As Integer)
                _Quantity = value
            End Set
        End Property

#End Region


    End Class


    Public Class IBTLineCollection
        Inherits BaseCollection(Of IBTLine)

    End Class

    Public Class IBTHeaderCollection
        Inherits BaseCollection(Of IBTHeader)

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal qodHeader As Qod.QodHeader, ByVal statusRequest As WebService.CTSStatusNotificationRequest)
            MyBase.New()
            'Check that we are the selling store or we don't want to create any ibt ins!
            If statusRequest.OrderHeader.SellingStoreCode.Value = ThisStore.Id4.ToString Then
                For Each FulfilmentSite As WebService.CTSStatusNotificationRequestFulfilmentSite In _
                        statusRequest.FulfilmentSites
                    If FulfilmentSite.FulfilmentSite.Value <> ThisStore.Id4.ToString Then
                        Dim IBTHeader As New IBTHeader(qodHeader, FulfilmentSite)
                        Me.Add(IBTHeader)
                    End If
                Next
            End If
        End Sub

        Public Sub New(ByVal qodHeader As Qod.QodHeader)
            MyBase.New()
            If ThisStore.Id4 = qodHeader.SellingStoreId Then
                'Create multiple IBT Outs for any refunds
                For Each QodRefund As Qod.QodRefund In qodHeader.Refunds
                    If QodRefund.RefundStatus < Qod.State.Refund.NotifyOk Then
                        'Ref 835 MO'C 17/05/2011 - Stop Multiple IBT Outs on Refund Retries 
                        If QodRefund.RefundStatus = Qod.State.Refund.NotifyFailedConnection OrElse QodRefund.RefundStatus = Qod.State.Refund.NotifyFailedState OrElse qodHeader.RefundStatus = Qod.State.Refund.NotifyFailedData And QodRefund.SellingStoreIbtOut <> "" Then
                            'IBT Out Already happened
                            Exit Sub
                        End If

                        Dim QodLine As Qod.QodLine = Nothing
                        QodLine = qodHeader.Lines.Find(QodRefund.Number)
                        If QodLine.DeliverySource = "" Then QodLine.DeliverySource = "0"
                        If QodLine IsNot Nothing AndAlso (QodLine.IsDeliveryChargeItem = False) Then
                            If CInt(QodLine.DeliverySource) <> ThisStore.Id4 AndAlso CInt(QodLine.DeliverySource) > 0 AndAlso _
                                    QodRefund.QtyCancelled > 0 Then 'Only do IBT if for fulfilling store
                                Dim IBTFound As Boolean = False
                                For Each IBTHeader As IBTHeader In Me.Items
                                    If IBTHeader.StoreNo = QodLine.DeliverySource Then
                                        'Add a new line to this IBT
                                        IBTFound = True
                                        Dim IBTLine As New IBTLine
                                        IBTLine.SKUNumber = QodRefund.SkuNumber
                                        IBTLine.Quantity = QodRefund.QtyCancelled
                                        IBTLine.Price = QodLine.Price
                                        IBTLine.HeaderNumber = IBTHeader.Number
                                        IBTLine.SequenceNumber = IBTHeader.GetNextSequence
                                        IBTHeader.Lines.Add(IBTLine)
                                        IBTHeader.Value += (IBTLine.Quantity * IBTLine.Price)
                                    End If
                                Next
                                If Not IBTFound Then
                                    'Add a new IBT to the collection
                                    QodRefund.Price = QodLine.Price 'QodRefund doesn't have a price recorded in the till so we need to use the line price
                                    Dim IBTHeader As New IBTHeader(QodRefund)
                                    IBTHeader.StoreNo = QodLine.DeliverySource
                                    IBTHeader.Info = "OM Reference " & qodHeader.OmOrderNumber.ToString
                                    Me.Add(IBTHeader)
                                End If
                            End If
                        End If
                    End If
                Next
            End If
        End Sub

        Public Function GetIBTNumber(ByVal storeNumber As String) As String
            Dim IBTNumber As String = String.Empty
            For Each IBTHeader As IBTHeader In Items
                If (CInt(IBTHeader.StoreNo) + 8000) = CInt(storeNumber) Then
                    IBTNumber = IBTHeader.Number
                    Exit For
                End If
            Next
            Return IBTNumber
        End Function

        Public Function SelfPersist() As Integer
            Dim LinesUpdated As Integer = 0
            Using con As New Connection
                Try
                    con.StartTransaction()
                    LinesUpdated = Persist(con)
                    con.CommitTransaction()
                Catch ex As Exception
                    con.RollbackTransaction()
                End Try
            End Using
            Return LinesUpdated
        End Function

        Public Function Persist(ByVal con As Connection) As Integer
            Dim LinesUpdated As Integer = 0
            For Each IBTHeader As IBTHeader In Items
                LinesUpdated += IBTHeader.Persist(con)
            Next
            Return LinesUpdated
        End Function
    End Class
End Namespace
