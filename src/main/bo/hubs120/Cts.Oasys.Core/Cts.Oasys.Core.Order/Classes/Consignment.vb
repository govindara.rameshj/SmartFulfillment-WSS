﻿Imports System.Data.SqlClient
Imports System.Text
Imports Cts.Oasys.Core

Public Class Consignment
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _employeeId As Integer
    Private _palletsIn As Integer
    Private _palletsOut As Integer
    Private _number As Integer
    Private _poNumber As Integer
    Private _releaseNumber As Integer
    Private _supplierNumber As String
    Private _deliveryNote1 As String
    Private _deliveryNote2 As String
    Private _deliveryNote3 As String
    Private _deliveryNote4 As String
    Private _deliveryNote5 As String
    Private _deliveryNote6 As String
    Private _deliveryNote7 As String
    Private _deliveryNote8 As String
    Private _deliveryNote9 As String
#End Region

#Region "Properties"
    <ColumnMapping("Number")> Public Property Number() As Integer
        Get
            Return _number
        End Get
        Private Set(ByVal value As Integer)
            _number = value
        End Set
    End Property
    <ColumnMapping("PoNumber")> Public Property PoNumber() As Integer
        Get
            Return _poNumber
        End Get
        Private Set(ByVal value As Integer)
            _poNumber = value
        End Set
    End Property
    <ColumnMapping("ReleaseNumber")> Public Property ReleaseNumber() As Integer
        Get
            Return _releaseNumber
        End Get
        Private Set(ByVal value As Integer)
            _releaseNumber = value
        End Set
    End Property
    <ColumnMapping("SupplierNumber")> Public Property SupplierNumber() As String
        Get
            Return _supplierNumber
        End Get
        Private Set(ByVal value As String)
            _supplierNumber = value
        End Set
    End Property
    <ColumnMapping("EmployeeId")> Public Property EmployeeId() As Integer
        Get
            Return _employeeId
        End Get
        Private Set(ByVal value As Integer)
            _employeeId = value
        End Set
    End Property
    <ColumnMapping("PalletsIn")> Public Property PalletsIn() As Integer
        Get
            Return _palletsIn
        End Get
        Set(ByVal value As Integer)
            _palletsIn = value
        End Set
    End Property
    <ColumnMapping("PalletsOut")> Public Property PalletsOut() As Integer
        Get
            Return _palletsOut
        End Get
        Set(ByVal value As Integer)
            _palletsOut = value
        End Set
    End Property
    <ColumnMapping("DeliveryNote1")> Public Property DeliveryNote1() As String
        Get
            If _deliveryNote1 Is Nothing Then _deliveryNote1 = String.Empty
            Return _deliveryNote1
        End Get
        Set(ByVal value As String)
            _deliveryNote1 = value
        End Set
    End Property
    <ColumnMapping("DeliveryNote2")> Public Property DeliveryNote2() As String
        Get
            If _deliveryNote2 Is Nothing Then _deliveryNote2 = String.Empty
            Return _deliveryNote2
        End Get
        Set(ByVal value As String)
            _deliveryNote2 = value
        End Set
    End Property
    <ColumnMapping("DeliveryNote3")> Public Property DeliveryNote3() As String
        Get
            If _deliveryNote3 Is Nothing Then _deliveryNote3 = String.Empty
            Return _deliveryNote3
        End Get
        Set(ByVal value As String)
            _deliveryNote3 = value
        End Set
    End Property
    <ColumnMapping("DeliveryNote4")> Public Property DeliveryNote4() As String
        Get
            If _deliveryNote4 Is Nothing Then _deliveryNote4 = String.Empty
            Return _deliveryNote4
        End Get
        Set(ByVal value As String)
            _deliveryNote4 = value
        End Set
    End Property
    <ColumnMapping("DeliveryNote5")> Public Property DeliveryNote5() As String
        Get
            If _deliveryNote5 Is Nothing Then _deliveryNote5 = String.Empty
            Return _deliveryNote5
        End Get
        Set(ByVal value As String)
            _deliveryNote5 = value
        End Set
    End Property
    <ColumnMapping("DeliveryNote6")> Public Property DeliveryNote6() As String
        Get
            If _deliveryNote6 Is Nothing Then _deliveryNote6 = String.Empty
            Return _deliveryNote6
        End Get
        Set(ByVal value As String)
            _deliveryNote6 = value
        End Set
    End Property
    <ColumnMapping("DeliveryNote7")> Public Property DeliveryNote7() As String
        Get
            If _deliveryNote7 Is Nothing Then _deliveryNote7 = String.Empty
            Return _deliveryNote7
        End Get
        Set(ByVal value As String)
            _deliveryNote7 = value
        End Set
    End Property
    <ColumnMapping("DeliveryNote8")> Public Property DeliveryNote8() As String
        Get
            If _deliveryNote8 Is Nothing Then _deliveryNote8 = String.Empty
            Return _deliveryNote8
        End Get
        Set(ByVal value As String)
            _deliveryNote8 = value
        End Set
    End Property
    <ColumnMapping("DeliveryNote9")> Public Property DeliveryNote9() As String
        Get
            If _deliveryNote9 Is Nothing Then _deliveryNote9 = String.Empty
            Return _deliveryNote9
        End Get
        Set(ByVal value As String)
            _deliveryNote9 = value
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

    Friend Sub New(ByVal employeeId As Integer, ByVal poNumber As Integer, ByVal releaseNumber As Integer, ByVal supplierNumber As String)
        MyBase.New()
        _employeeId = employeeId
        _poNumber = poNumber
        _releaseNumber = releaseNumber
        _supplierNumber = supplierNumber
    End Sub


    Friend Function Consign(ByVal orderId As Integer) As Integer
        _number = Po.DataAccess.PoConsign(orderId, Me)
        Return _number
    End Function

    Public Function DeliveryNotesString() As String

        Dim sb As New StringBuilder(_deliveryNote1.Trim)
        If _deliveryNote2.Trim.Length > 0 Then sb.Append(", " & _deliveryNote2.Trim)
        If _deliveryNote3.Trim.Length > 0 Then sb.Append(", " & _deliveryNote3.Trim)
        If _deliveryNote4.Trim.Length > 0 Then sb.Append(", " & _deliveryNote4.Trim)
        If _deliveryNote5.Trim.Length > 0 Then sb.Append(", " & _deliveryNote5.Trim)
        If _deliveryNote6.Trim.Length > 0 Then sb.Append(", " & _deliveryNote6.Trim)
        If _deliveryNote7.Trim.Length > 0 Then sb.Append(", " & _deliveryNote7.Trim)
        If _deliveryNote8.Trim.Length > 0 Then sb.Append(", " & _deliveryNote8.Trim)
        If _deliveryNote9.Trim.Length > 0 Then sb.Append(", " & _deliveryNote9.Trim)
        Return sb.ToString

    End Function

    Public Shared Function GetConsignment(ByVal consignNumber As String) As Consignment

        Dim dt As DataTable = DataAccess.GetConsignment(consignNumber)
        For Each dr As DataRow In dt.Rows
            Return New Consignment(dr)
            Exit Function
        Next
        Return Nothing

    End Function

End Class
