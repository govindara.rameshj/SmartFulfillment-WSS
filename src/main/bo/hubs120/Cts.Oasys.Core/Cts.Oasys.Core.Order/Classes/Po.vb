﻿Imports System.Data.SqlClient
Imports Cts.Oasys.Core
Imports System.ComponentModel

Namespace Po

    <HideModuleName()> Public Module SharedFunctions

        Public Function GetOrdersNotConsigned() As HeaderCollection
            Dim orders As New HeaderCollection
            orders.LoadAllNonConsigned()
            Return orders
        End Function

        Public Function GetOrdersConsignedByDateRange(ByVal startDate As Date, ByVal endDate As Date) As HeaderCollection
            Dim orders As New HeaderCollection
            orders.LoadConsignedByDateRange(startDate, endDate)
            Return orders
        End Function

        Public Function GetOrdersOutstanding() As HeaderCollection
            Dim orders As New HeaderCollection
            orders.LoadAllOutstanding()
            Return orders
        End Function

    End Module

    Public Class Header
        Inherits Base

#Region "Private Variables"
        Private _id As Integer
        Private _poNumber As Integer
        Private _releaseNumber As Integer
        Private _consignNumber As Nullable(Of Integer)
        Private _soqNumber As Nullable(Of Integer)
        Private _supplierNumber As String
        Private _supplierName As String
        Private _supplierBbc As String
        Private _supplierTradanet As Boolean
        Private _isDeleted As Boolean
        Private _isCommed As Boolean
        Private _sourceEntry As String
        Private _employeeId As Integer
        Private _dateCreated As Date
        Private _dateDue As Nullable(Of Date)
        Private _cartons As Integer
        Private _value As Decimal
        Private _units As Integer
        Private _palletCheck As Boolean

        Private _lines As LineCollection = Nothing
#End Region

#Region "Properties"
        <ColumnMapping("Id")> Public Property Id() As Integer
            Get
                Return _id
            End Get
            Friend Set(ByVal value As Integer)
                _id = value
            End Set
        End Property
        <ColumnMapping("PoNumber")> Public Property PoNumber() As Integer
            Get
                Return _poNumber
            End Get
            Friend Set(ByVal value As Integer)
                _poNumber = value
            End Set
        End Property
        <ColumnMapping("ConsignNumber")> Public Property ConsignNumber() As Nullable(Of Integer)
            Get
                Return _consignNumber
            End Get
            Private Set(ByVal value As Nullable(Of Integer))
                _consignNumber = value
            End Set
        End Property
        <ColumnMapping("SoqNumber")> Public Property SoqNumber() As Nullable(Of Integer)
            Get
                Return _soqNumber
            End Get
            Private Set(ByVal value As Nullable(Of Integer))
                _soqNumber = value
            End Set
        End Property
        <ColumnMapping("ReleaseNumber")> Public Property ReleaseNumber() As Integer
            Get
                Return _releaseNumber
            End Get
            Private Set(ByVal value As Integer)
                _releaseNumber = value
            End Set
        End Property
        <ColumnMapping("SupplierNumber")> Public Property SupplierNumber() As String
            Get
                If _supplierNumber Is Nothing Then _supplierNumber = String.Empty
                Return _supplierNumber
            End Get
            Private Set(ByVal value As String)
                _supplierNumber = value
            End Set
        End Property
        <ColumnMapping("SupplierName")> Public Property SupplierName() As String
            Get
                Return _supplierName
            End Get
            Private Set(ByVal value As String)
                _supplierName = value
            End Set
        End Property
        <ColumnMapping("SupplierBbc")> Public Property SupplierBbc() As String
            Get
                Return _supplierBbc
            End Get
            Private Set(ByVal value As String)
                _supplierBbc = value
            End Set
        End Property
        <ColumnMapping("SupplierTradanet")> Public Property SupplierTradanet() As Boolean
            Get
                Return _supplierTradanet
            End Get
            Private Set(ByVal value As Boolean)
                _supplierTradanet = value
            End Set
        End Property
        <ColumnMapping("SourceEntry")> Public Property SourceEntry() As String
            Get
                If _sourceEntry Is Nothing Then _sourceEntry = String.Empty
                Return _sourceEntry
            End Get
            Friend Set(ByVal value As String)
                _sourceEntry = value
            End Set
        End Property
        <ColumnMapping("EmployeeId")> Public Property EmployeeId() As Integer
            Get
                Return _employeeId
            End Get
            Private Set(ByVal value As Integer)
                _employeeId = value
            End Set
        End Property
        <ColumnMapping("IsDeleted")> Public Property IsDeleted() As Boolean
            Get
                Return _isDeleted
            End Get
            Friend Set(ByVal value As Boolean)
                _isDeleted = value
            End Set
        End Property
        <ColumnMapping("IsCommed")> Public Property IsCommed() As Boolean
            Get
                Return _isCommed
            End Get
            Set(ByVal value As Boolean)
                _isCommed = value
            End Set
        End Property
        <ColumnMapping("DateCreated")> Public Property DateCreated() As Date
            Get
                Return _dateCreated
            End Get
            Friend Set(ByVal value As Date)
                _dateCreated = value
            End Set
        End Property
        <ColumnMapping("DateDue")> Public Property DateDue() As Nullable(Of Date)
            Get
                Return _dateDue
            End Get
            Set(ByVal value As Nullable(Of Date))
                _dateDue = value
            End Set
        End Property
        <ColumnMapping("Cartons")> Public Property Cartons() As Integer
            Get
                Return _cartons
            End Get
            Friend Set(ByVal value As Integer)
                _cartons = value
            End Set
        End Property
        <ColumnMapping("Value")> Public Property Value() As Decimal
            Get
                Return _value
            End Get
            Friend Set(ByVal value As Decimal)
                _value = value
            End Set
        End Property
        <ColumnMapping("Units")> Public Property Units() As Integer
            Get
                Return _units
            End Get
            Friend Set(ByVal value As Integer)
                _units = value
            End Set
        End Property
        <ColumnMapping("PalletCheck")> Public Property PalletCheck() As Boolean
            Get
                Return _palletCheck
            End Get
            Private Set(ByVal value As Boolean)
                _palletCheck = value
            End Set
        End Property

        Public ReadOnly Property Lines() As LineCollection
            Get
                'If _lines Is Nothing Then _lines = New LineCollection(Me)
                If _lines Is Nothing Then _lines = New LineCollection()
                Return _lines
            End Get
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Private Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub

        Public Sub New(ByVal sup As Supplier, ByVal employeeId As Integer)
            MyBase.New()
            _supplierNumber = sup.Number
            _supplierBbc = sup.BbcNumber
            _supplierTradanet = sup.IsTradanet
            _soqNumber = sup.SoqNumber
            _employeeId = employeeId
            _isCommed = True
            DataAccess.PoInsertNew(Me)
        End Sub

        Public Function IsSupplierBbc() As Boolean
            Select Case _supplierBbc
                Case "W", "A", "C" : Return True
                Case Else : Return False
            End Select
        End Function


        Public Sub ConsignOrder(ByVal employeeId As Integer, ByVal palletsIn As Integer, ByVal palletsOut As Integer, ByVal deliveryNotes() As String)

            Dim consign As New Consignment(employeeId, _poNumber, _releaseNumber, _supplierNumber)
            consign.PalletsIn = palletsIn
            consign.PalletsOut = palletsOut
            consign.DeliveryNote1 = deliveryNotes(0)
            If deliveryNotes.GetUpperBound(0) >= 1 Then consign.DeliveryNote2 = deliveryNotes(1)
            If deliveryNotes.GetUpperBound(0) >= 2 Then consign.DeliveryNote3 = deliveryNotes(2)
            If deliveryNotes.GetUpperBound(0) >= 3 Then consign.DeliveryNote4 = deliveryNotes(3)
            If deliveryNotes.GetUpperBound(0) >= 4 Then consign.DeliveryNote5 = deliveryNotes(4)
            If deliveryNotes.GetUpperBound(0) >= 5 Then consign.DeliveryNote6 = deliveryNotes(5)
            If deliveryNotes.GetUpperBound(0) >= 6 Then consign.DeliveryNote7 = deliveryNotes(6)
            If deliveryNotes.GetUpperBound(0) >= 7 Then consign.DeliveryNote8 = deliveryNotes(7)
            If deliveryNotes.GetUpperBound(0) >= 8 Then consign.DeliveryNote9 = deliveryNotes(8)

            _consignNumber = CInt(consign.Consign(_id))

        End Sub

        Public Sub UpdateOrder(ByRef sup As Supplier)
            DataAccess.PoUpdate(Me)
            sup.SoqOrdered = True
            sup.SoqNumber = Nothing
        End Sub

    End Class

    Public Class HeaderCollection
        Inherits BaseCollection(Of Header)

        Public Sub LoadAllNonConsigned()
            Dim dt As DataTable = DataAccess.PoGetNotConsigned
            Me.Load(dt)
        End Sub

        Public Sub LoadConsignedByDateRange(ByVal startDate As Date, ByVal endDate As Date)
            Dim dt As DataTable = DataAccess.PoGetConsignedByDateRange(startDate, endDate)
            Me.Load(dt)
        End Sub

        Public Sub LoadAllOutstanding()
            Dim dt As DataTable = DataAccess.PoGetOutstanding
            Me.Load(dt)
        End Sub

    End Class

    Public Class Line
        Inherits Oasys.Core.Base

#Region "Private Variables"
        Private _id As Integer
        Private _orderId As Integer
        Private _skuNumber As String
        Private _skuDescription As String
        Private _skuProductCode As String
        Private _skuPackSize As Integer
        Private _orderQty As Integer
        Private _price As Decimal
        Private _cost As Decimal
#End Region

#Region "Properties"
        <ColumnMapping("Id")> Public Property Id() As Integer
            Get
                Return _id
            End Get
            Private Set(ByVal value As Integer)
                _id = value
            End Set
        End Property
        <ColumnMapping("OrderId")> Public Property OrderId() As Integer
            Get
                Return _orderId
            End Get
            Friend Set(ByVal value As Integer)
                _orderId = value
            End Set
        End Property
        <ColumnMapping("SkuNumber")> Public Property SkuNumber() As String
            Get
                Return _skuNumber
            End Get
            Private Set(ByVal value As String)
                _skuNumber = value
            End Set
        End Property
        <ColumnMapping("SkuDescription")> Public Property SkuDescription() As String
            Get
                Return _skuDescription
            End Get
            Private Set(ByVal value As String)
                _skuDescription = value.Trim
            End Set
        End Property
        <ColumnMapping("SkuProductCode")> Public Property SkuProductCode() As String
            Get
                Return _skuProductCode
            End Get
            Private Set(ByVal value As String)
                _skuProductCode = value.Trim
            End Set
        End Property
        <ColumnMapping("SkuPackSize")> Public Property SkuPackSize() As Integer
            Get
                Return _skuPackSize
            End Get
            Private Set(ByVal value As Integer)
                _skuPackSize = value
            End Set
        End Property
        <ColumnMapping("OrderQty")> Public Property OrderQty() As Integer
            Get
                Return _orderQty
            End Get
            Set(ByVal value As Integer)
                _orderQty = value
            End Set
        End Property
        <ColumnMapping("Price")> Public Property Price() As Decimal
            Get
                Return _price
            End Get
            Private Set(ByVal value As Decimal)
                _price = value
            End Set
        End Property
        <ColumnMapping("Cost")> Public Property Cost() As Decimal
            Get
                Return _cost
            End Get
            Private Set(ByVal value As Decimal)
                _cost = value
            End Set
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub

        'Public Sub New(ByVal stockItem As Stock.Stock)
        '    MyBase.New()
        '    _skuNumber = stockItem.SkuNumber
        '    _skuDescription = stockItem.Description
        '    _skuProductCode = stockItem.ProductCode
        '    _skuPackSize = stockItem.PackSize
        '    _price = stockItem.Price
        '    _cost = stockItem.Cost
        'End Sub

    End Class

    Public Class LineCollection
        Inherits BaseCollection(Of Line)

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub LoadLines(ByVal orderNumber As Integer)
            Dim dt As DataTable = DataAccess.PoGetLines(orderNumber)
            Me.Load(dt)
        End Sub

        Public Function Exists(ByVal skuNumber As String) As Boolean
            For Each line As Line In Me.Items
                If line.SkuNumber = skuNumber Then Return True
            Next
            Return False
        End Function


        Public Function OrderedQty() As Integer
            Dim qty As Integer = 0
            For Each line As Line In Me.Items
                qty += line.OrderQty
            Next
            Return qty
        End Function

        Public Function OrderedValue() As Decimal
            Dim value As Decimal = 0
            For Each line As Line In Me.Items
                value += (line.OrderQty * line.Price)
            Next
            Return value
        End Function

    End Class

End Namespace