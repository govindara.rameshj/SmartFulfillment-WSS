﻿Imports System.Text
Imports Cts.Oasys.Core
Imports System.ComponentModel

Namespace [Return]

    <HideModuleName()> Public Module SharedFunctions

        Public Function GetNonReleased() As HeaderCollection
            Dim returns As New HeaderCollection
            returns.LoadAllNonReleased()
            Return returns
        End Function

    End Module

    Public Class Header
        Inherits Oasys.Core.Base

#Region "Private Variables"
        Private _id As Integer
        Private _number As Integer
        Private _supplierNumber As String
        Private _supplierName As String
        Private _dateCreated As Date
        Private _dateCollected As Date
        Private _receiptNumber As String
        Private _value As Decimal
        Private _isDeleted As Boolean
        Private _notPrintedEntry As Boolean
        Private _notPrintedRelease As Boolean
        Private _lines As LineCollection = Nothing
#End Region

#Region "Properties"
        <ColumnMapping("Id")> Public Property Id() As Integer
            Get
                Return _id
            End Get
            Private Set(ByVal value As Integer)
                _id = value
            End Set
        End Property
        <ColumnMapping("Number")> Public Property Number() As Integer
            Get
                Return _number
            End Get
            Private Set(ByVal value As Integer)
                _number = value
            End Set
        End Property
        <ColumnMapping("SupplierNumber")> Public Property SupplierNumber() As String
            Get
                Return _supplierNumber
            End Get
            Private Set(ByVal value As String)
                _supplierNumber = value
            End Set
        End Property
        <ColumnMapping("SupplierName")> Public Property SupplierName() As String
            Get
                Return _supplierName
            End Get
            Private Set(ByVal value As String)
                _supplierName = value
            End Set
        End Property
        <ColumnMapping("DateCreated")> Public Property DateCreated() As Date
            Get
                Return _dateCreated
            End Get
            Private Set(ByVal value As Date)
                _dateCreated = value
            End Set
        End Property
        <ColumnMapping("DateCollected")> Public Property DateCollected() As Date
            Get
                Return _dateCollected
            End Get
            Private Set(ByVal value As Date)
                _dateCollected = value
            End Set
        End Property
        <ColumnMapping("ReceiptNumber")> Public Property ReceiptNumber() As String
            Get
                Return _receiptNumber
            End Get
            Private Set(ByVal value As String)
                _receiptNumber = value
            End Set
        End Property
        <ColumnMapping("Value")> Public Property Value() As Decimal
            Get
                Return _value
            End Get
            Private Set(ByVal value As Decimal)
                _value = value
            End Set
        End Property
        <ColumnMapping("IsDeleted")> Public Property IsDeleted() As Boolean
            Get
                Return _isDeleted
            End Get
            Private Set(ByVal value As Boolean)
                _isDeleted = value
            End Set
        End Property
        <ColumnMapping("NotPrintedEntry")> Public Property NotPrintedEntry() As Boolean
            Get
                Return _notPrintedEntry
            End Get
            Set(ByVal value As Boolean)
                _notPrintedEntry = value
            End Set
        End Property
        <ColumnMapping("NotPrintedRelease")> Public Property NotPrintedRelease() As Boolean
            Get
                Return _notPrintedRelease
            End Get
            Set(ByVal value As Boolean)
                _notPrintedRelease = value
            End Set
        End Property

        Public ReadOnly Property Lines() As LineCollection
            Get
                If _lines Is Nothing Then LoadLines()
                Return _lines
            End Get
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub LoadLines()
            _lines = New LineCollection
            _lines.LoadLines(Me.Id)
        End Sub

    End Class

    Public Class HeaderCollection
        Inherits BaseCollection(Of Header)

        Public Sub LoadAllNonReleased()
            Dim dt As DataTable = DataAccess.ReturnGetNonReleased
            Me.Load(dt)
        End Sub

    End Class

    Public Class Line
        Inherits Oasys.Core.Base

#Region "Private Variables"
        Private _id As Integer
        Private _returnId As Integer
        Private _skuNumber As String
        Private _skuDescription As String
        Private _qty As Integer
        Private _price As Decimal
        Private _cost As Decimal
        Private _reasonCode As String
        Private _reasonDescription As String
#End Region

#Region "Properties"
        <ColumnMapping("Id")> Public Property Id() As Integer
            Get
                Return _id
            End Get
            Friend Set(ByVal value As Integer)
                _id = value
            End Set
        End Property
        <ColumnMapping("ReturnId")> Public Property ReturnId() As Integer
            Get
                Return _returnId
            End Get
            Friend Set(ByVal value As Integer)
                _returnId = value
            End Set
        End Property
        <ColumnMapping("SkuNumber")> Public Property SkuNumber() As String
            Get
                Return _skuNumber
            End Get
            Set(ByVal value As String)
                _skuNumber = value
            End Set
        End Property
        <ColumnMapping("SkuDescription")> Public Property SkuDescription() As String
            Get
                Return _skuDescription
            End Get
            Private Set(ByVal value As String)
                _skuDescription = value
            End Set
        End Property
        <ColumnMapping("Qty")> Public Property Qty() As Integer
            Get
                Return _qty
            End Get
            Set(ByVal value As Integer)
                _qty = value
            End Set
        End Property
        <ColumnMapping("Price")> Public Property Price() As Decimal
            Get
                Return _price
            End Get
            Set(ByVal value As Decimal)
                _price = value
            End Set
        End Property
        <ColumnMapping("Cost")> Public Property Cost() As Decimal
            Get
                Return _cost
            End Get
            Private Set(ByVal value As Decimal)
                _cost = value
            End Set
        End Property
        <ColumnMapping("ReasonCode")> Public Property ReasonCode() As String
            Get
                Return _reasonCode
            End Get
            Set(ByVal value As String)
                _reasonCode = value
            End Set
        End Property
        <ColumnMapping("ReasonDescription")> Public Property ReasonDescription() As String
            Get
                Return _reasonDescription
            End Get
            Set(ByVal value As String)
                _reasonDescription = value
            End Set
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

    End Class

    Public Class LineCollection
        Inherits BaseCollection(Of Line)

        Public Sub LoadLines(ByVal returnId As Integer)
            Dim dt As DataTable = DataAccess.ReturnGetLines(returnId.ToString)
            Me.Load(dt)
        End Sub

        Public Function StockExists(ByVal skuNumber As String) As Boolean
            For Each line As Line In Me.Items
                If line.SkuNumber = skuNumber Then Return True
            Next
            Return False
        End Function

        Public Function Qty() As Integer
            Dim total As Integer = 0
            For Each line As Line In Me.Items
                total += line.Qty
            Next
            Return total
        End Function

        Public Function Value() As Decimal
            Dim total As Decimal = 0
            For Each line As Line In Me.Items
                total += (line.Qty * line.Price)
            Next
            Return total
        End Function

    End Class

End Namespace


