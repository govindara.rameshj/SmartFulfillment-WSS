﻿Imports System.Xml.Serialization
Imports System.Xml

Namespace WebService

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True), _
     Xml.Serialization.XmlRootAttribute([Namespace]:="", IsNullable:=False)> _
    Partial Public Class CTSFulfilmentRequest

        Private dateTimeStampField As CTSFulfilmentRequestDateTimeStamp

        Private targetFulfilmentSiteField As CTSFulfilmentRequestTargetFulfilmentSite

        Private orderHeaderField As CTSFulfilmentRequestOrderHeader

        Private orderLinesField() As CTSFulfilmentRequestOrderLine

        '''<remarks/>
        Public Property DateTimeStamp() As CTSFulfilmentRequestDateTimeStamp
            Get
                Return Me.dateTimeStampField
            End Get
            Set(ByVal value As CTSFulfilmentRequestDateTimeStamp)
                Me.dateTimeStampField = value
            End Set
        End Property

        '''<remarks/>
        Public Property TargetFulfilmentSite() As CTSFulfilmentRequestTargetFulfilmentSite
            Get
                Return Me.targetFulfilmentSiteField
            End Get
            Set(ByVal value As CTSFulfilmentRequestTargetFulfilmentSite)
                Me.targetFulfilmentSiteField = value
            End Set
        End Property

        '''<remarks/>
        Public Property OrderHeader() As CTSFulfilmentRequestOrderHeader
            Get
                Return Me.orderHeaderField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeader)
                Me.orderHeaderField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlArrayItemAttribute("OrderLine", IsNullable:=False)> _
        Public Property OrderLines() As CTSFulfilmentRequestOrderLine()
            Get
                Return Me.orderLinesField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderLine())
                Me.orderLinesField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestDateTimeStamp

        Private valueField As Date

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestTargetFulfilmentSite

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeader

        Private sourceField As String

        Private sourceOrderNumberField As String

        Private sellingStoreCodeField As CTSFulfilmentRequestOrderHeaderSellingStoreCode

        Private sellingStoreOrderNumberField As CTSFulfilmentRequestOrderHeaderSellingStoreOrderNumber

        Private sellingStoreTillField As String

        Private sellingStoreTransactionField As String

        Private requiredDeliveryDateField As CTSFulfilmentRequestOrderHeaderRequiredDeliveryDate

        Private deliveryChargeField As CTSFulfilmentRequestOrderHeaderDeliveryCharge

        Private totalOrderValueField As CTSFulfilmentRequestOrderHeaderTotalOrderValue

        Private saleDateField As CTSFulfilmentRequestOrderHeaderSaleDate

        Private oMOrderNumberField As CTSFulfilmentRequestOrderHeaderOMOrderNumber

        Private orderStatusField As CTSFulfilmentRequestOrderHeaderOrderStatus

        Private customerAccountNoField As CTSFulfilmentRequestOrderHeaderCustomerAccountNo

        Private customerNameField As CTSFulfilmentRequestOrderHeaderCustomerName

        Private customerAddressLine1Field As CTSFulfilmentRequestOrderHeaderCustomerAddressLine1

        Private customerAddressLine2Field As CTSFulfilmentRequestOrderHeaderCustomerAddressLine2

        Private customerAddressTownField As CTSFulfilmentRequestOrderHeaderCustomerAddressTown

        Private customerAddressLine4Field As CTSFulfilmentRequestOrderHeaderCustomerAddressLine4

        Private customerPostcodeField As CTSFulfilmentRequestOrderHeaderCustomerPostcode

        Private deliveryAddressLine1Field As CTSFulfilmentRequestOrderHeaderDeliveryAddressLine1

        Private deliveryAddressLine2Field As CTSFulfilmentRequestOrderHeaderDeliveryAddressLine2

        Private deliveryAddressTownField As CTSFulfilmentRequestOrderHeaderDeliveryAddressTown

        Private deliveryAddressLine4Field As CTSFulfilmentRequestOrderHeaderDeliveryAddressLine4

        Private deliveryPostcodeField As CTSFulfilmentRequestOrderHeaderDeliveryPostcode

        Private contactPhoneHomeField As CTSFulfilmentRequestOrderHeaderContactPhoneHome

        Private contactPhoneWorkField As CTSFulfilmentRequestOrderHeaderContactPhoneWork

        Private contactPhoneMobileField As CTSFulfilmentRequestOrderHeaderContactPhoneMobile

        Private contactEmailField As CTSFulfilmentRequestOrderHeaderContactEmail

        Private deliveryInstructionsField As CTSFulfilmentRequestOrderHeaderDeliveryInstructions

        Private toBeDeliveredField As CTSFulfilmentRequestOrderHeaderToBeDelivered

        Private extendedLeadTimeField As Boolean

        Private extendedLeadTimeFieldSpecified As Boolean

        Private deliveryContactNameField As String

        Private deliveryContactPhoneField As String

        '''<remarks/>
        Public Property Source() As String
            Get
                Return Me.sourceField
            End Get
            Set(ByVal value As String)
                Me.sourceField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SourceOrderNumber() As String
            Get
                Return Me.sourceOrderNumberField
            End Get
            Set(ByVal value As String)
                Me.sourceOrderNumberField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingStoreCode() As CTSFulfilmentRequestOrderHeaderSellingStoreCode
            Get
                Return Me.sellingStoreCodeField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderSellingStoreCode)
                Me.sellingStoreCodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingStoreOrderNumber() As CTSFulfilmentRequestOrderHeaderSellingStoreOrderNumber
            Get
                Return Me.sellingStoreOrderNumberField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderSellingStoreOrderNumber)
                Me.sellingStoreOrderNumberField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingStoreTill() As String
            Get
                Return Me.sellingStoreTillField
            End Get
            Set(ByVal value As String)
                Me.sellingStoreTillField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingStoreTransaction() As String
            Get
                Return Me.sellingStoreTransactionField
            End Get
            Set(ByVal value As String)
                Me.sellingStoreTransactionField = value
            End Set
        End Property

        '''<remarks/>
        Public Property RequiredDeliveryDate() As CTSFulfilmentRequestOrderHeaderRequiredDeliveryDate
            Get
                Return Me.requiredDeliveryDateField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderRequiredDeliveryDate)
                Me.requiredDeliveryDateField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryCharge() As CTSFulfilmentRequestOrderHeaderDeliveryCharge
            Get
                Return Me.deliveryChargeField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderDeliveryCharge)
                Me.deliveryChargeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property TotalOrderValue() As CTSFulfilmentRequestOrderHeaderTotalOrderValue
            Get
                Return Me.totalOrderValueField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderTotalOrderValue)
                Me.totalOrderValueField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SaleDate() As CTSFulfilmentRequestOrderHeaderSaleDate
            Get
                Return Me.saleDateField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderSaleDate)
                Me.saleDateField = value
             End Set
        End Property

        '''<remarks/>
        Public Property OMOrderNumber() As CTSFulfilmentRequestOrderHeaderOMOrderNumber
            Get
                Return Me.oMOrderNumberField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderOMOrderNumber)
                Me.oMOrderNumberField = value
            End Set
        End Property

        '''<remarks/>
        Public Property OrderStatus() As CTSFulfilmentRequestOrderHeaderOrderStatus
            Get
                Return Me.orderStatusField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderOrderStatus)
                Me.orderStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property CustomerAccountNo() As CTSFulfilmentRequestOrderHeaderCustomerAccountNo
            Get
                Return Me.customerAccountNoField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderCustomerAccountNo)
                Me.customerAccountNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property CustomerName() As CTSFulfilmentRequestOrderHeaderCustomerName
            Get
                Return Me.customerNameField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderCustomerName)
                Me.customerNameField = value
            End Set
        End Property

        '''<remarks/>
        Public Property CustomerAddressLine1() As CTSFulfilmentRequestOrderHeaderCustomerAddressLine1
            Get
                Return Me.customerAddressLine1Field
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderCustomerAddressLine1)
                Me.customerAddressLine1Field = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property CustomerAddressLine2() As CTSFulfilmentRequestOrderHeaderCustomerAddressLine2
            Get
                Return Me.customerAddressLine2Field
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderCustomerAddressLine2)
                Me.customerAddressLine2Field = value
            End Set
        End Property

        '''<remarks/>
        Public Property CustomerAddressTown() As CTSFulfilmentRequestOrderHeaderCustomerAddressTown
            Get
                Return Me.customerAddressTownField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderCustomerAddressTown)
                Me.customerAddressTownField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property CustomerAddressLine4() As CTSFulfilmentRequestOrderHeaderCustomerAddressLine4
            Get
                Return Me.customerAddressLine4Field
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderCustomerAddressLine4)
                Me.customerAddressLine4Field = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property CustomerPostcode() As CTSFulfilmentRequestOrderHeaderCustomerPostcode
            Get
                Return Me.customerPostcodeField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderCustomerPostcode)
                Me.customerPostcodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryAddressLine1() As CTSFulfilmentRequestOrderHeaderDeliveryAddressLine1
            Get
                Return Me.deliveryAddressLine1Field
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderDeliveryAddressLine1)
                Me.deliveryAddressLine1Field = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property DeliveryAddressLine2() As CTSFulfilmentRequestOrderHeaderDeliveryAddressLine2
            Get
                Return Me.deliveryAddressLine2Field
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderDeliveryAddressLine2)
                Me.deliveryAddressLine2Field = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryAddressTown() As CTSFulfilmentRequestOrderHeaderDeliveryAddressTown
            Get
                Return Me.deliveryAddressTownField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderDeliveryAddressTown)
                Me.deliveryAddressTownField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property DeliveryAddressLine4() As CTSFulfilmentRequestOrderHeaderDeliveryAddressLine4
            Get
                Return Me.deliveryAddressLine4Field
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderDeliveryAddressLine4)
                Me.deliveryAddressLine4Field = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryPostcode() As CTSFulfilmentRequestOrderHeaderDeliveryPostcode
            Get
                Return Me.deliveryPostcodeField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderDeliveryPostcode)
                Me.deliveryPostcodeField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property ContactPhoneHome() As CTSFulfilmentRequestOrderHeaderContactPhoneHome
            Get
                Return Me.contactPhoneHomeField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderContactPhoneHome)
                Me.contactPhoneHomeField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property ContactPhoneWork() As CTSFulfilmentRequestOrderHeaderContactPhoneWork
            Get
                Return Me.contactPhoneWorkField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderContactPhoneWork)
                Me.contactPhoneWorkField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property ContactPhoneMobile() As CTSFulfilmentRequestOrderHeaderContactPhoneMobile
            Get
                Return Me.contactPhoneMobileField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderContactPhoneMobile)
                Me.contactPhoneMobileField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property ContactEmail() As CTSFulfilmentRequestOrderHeaderContactEmail
            Get
                Return Me.contactEmailField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderContactEmail)
                Me.contactEmailField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryInstructions() As CTSFulfilmentRequestOrderHeaderDeliveryInstructions
            Get
                Return Me.deliveryInstructionsField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderDeliveryInstructions)
                Me.deliveryInstructionsField = value
            End Set
        End Property

        '''<remarks/>
        Public Property ToBeDelivered() As CTSFulfilmentRequestOrderHeaderToBeDelivered
            Get
                Return Me.toBeDeliveredField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderToBeDelivered)
                Me.toBeDeliveredField = value
            End Set
        End Property

        '''<remarks/>
        Public Property ExtendedLeadTime() As Boolean
            Get
                Return Me.extendedLeadTimeField
            End Get
            Set(ByVal value As Boolean)
                Me.extendedLeadTimeField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlIgnoreAttribute()> _
        Public Property ExtendedLeadTimeSpecified() As Boolean
            Get
                Return Me.extendedLeadTimeFieldSpecified
            End Get
            Set(ByVal value As Boolean)
                Me.extendedLeadTimeFieldSpecified = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryContactName() As String
            Get
                Return Me.deliveryContactNameField
            End Get
            Set(ByVal value As String)
                Me.deliveryContactNameField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryContactPhone() As String
            Get
                Return Me.deliveryContactPhoneField
            End Get
            Set(ByVal value As String)
                Me.deliveryContactPhoneField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderSellingStoreCode

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderSellingStoreOrderNumber

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderRequiredDeliveryDate

        Private valueField As Date

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="date")> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderDeliveryCharge

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderTotalOrderValue

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderSaleDate

        Private valueField As Date

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="date")> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderOMOrderNumber

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderOrderStatus

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderCustomerAccountNo

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderCustomerName

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderCustomerAddressLine1

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderCustomerAddressLine2

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderCustomerAddressTown

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderCustomerAddressLine4

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderCustomerPostcode

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderDeliveryAddressLine1

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderDeliveryAddressLine2

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderDeliveryAddressTown

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderDeliveryAddressLine4

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderDeliveryPostcode

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderContactPhoneHome

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderContactPhoneWork

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderContactPhoneMobile

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderContactEmail

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderDeliveryInstructions

        Private instructionLineField() As CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLine

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute("InstructionLine")> _
        Public Property InstructionLine() As CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLine()
            Get
                Return Me.instructionLineField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLine())
                Me.instructionLineField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLine

        Private lineNoField As CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLineLineNo

        Private lineTextField As CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLineLineText

        '''<remarks/>
        Public Property LineNo() As CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLineLineNo
            Get
                Return Me.lineNoField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLineLineNo)
                Me.lineNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property LineText() As CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLineLineText
            Get
                Return Me.lineTextField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLineLineText)
                Me.lineTextField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLineLineNo

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLineLineText

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderHeaderToBeDelivered

        Private valueField As Boolean

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Boolean
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Boolean)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderLine

        Private sellingStoreLineNoField As CTSFulfilmentRequestOrderLineSellingStoreLineNo

        Private oMOrderLineNoField As CTSFulfilmentRequestOrderLineOMOrderLineNo

        Private lineStatusField As CTSFulfilmentRequestOrderLineLineStatus

        Private productCodeField As CTSFulfilmentRequestOrderLineProductCode

        Private productDescriptionField As CTSFulfilmentRequestOrderLineProductDescription

        Private totalOrderQuantityField As CTSFulfilmentRequestOrderLineTotalOrderQuantity

        Private quantityTakenField As CTSFulfilmentRequestOrderLineQuantityTaken

        Private uOMField As CTSFulfilmentRequestOrderLineUOM

        Private lineValueField As CTSFulfilmentRequestOrderLineLineValue

        Private deliveryChargeItemField As CTSFulfilmentRequestOrderLineDeliveryChargeItem

        Private sellingPriceField As CTSFulfilmentRequestOrderLineSellingPrice

        Private fulfilmentSiteField As CTSFulfilmentRequestOrderLineFulfilmentSite

        '''<remarks/>
        Public Property SellingStoreLineNo() As CTSFulfilmentRequestOrderLineSellingStoreLineNo
            Get
                Return Me.sellingStoreLineNoField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderLineSellingStoreLineNo)
                Me.sellingStoreLineNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property OMOrderLineNo() As CTSFulfilmentRequestOrderLineOMOrderLineNo
            Get
                Return Me.oMOrderLineNoField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderLineOMOrderLineNo)
                Me.oMOrderLineNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property LineStatus() As CTSFulfilmentRequestOrderLineLineStatus
            Get
                Return Me.lineStatusField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderLineLineStatus)
                Me.lineStatusField = value
            End Set
        End Property

        '''<remarks/>
        Public Property ProductCode() As CTSFulfilmentRequestOrderLineProductCode
            Get
                Return Me.productCodeField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderLineProductCode)
                Me.productCodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property ProductDescription() As CTSFulfilmentRequestOrderLineProductDescription
            Get
                Return Me.productDescriptionField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderLineProductDescription)
                Me.productDescriptionField = value
            End Set
        End Property

        '''<remarks/>
        Public Property TotalOrderQuantity() As CTSFulfilmentRequestOrderLineTotalOrderQuantity
            Get
                Return Me.totalOrderQuantityField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderLineTotalOrderQuantity)
                Me.totalOrderQuantityField = value
            End Set
        End Property

        '''<remarks/>
        Public Property QuantityTaken() As CTSFulfilmentRequestOrderLineQuantityTaken
            Get
                Return Me.quantityTakenField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderLineQuantityTaken)
                Me.quantityTakenField = value
            End Set
        End Property

        '''<remarks/>
        Public Property UOM() As CTSFulfilmentRequestOrderLineUOM
            Get
                Return Me.uOMField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderLineUOM)
                Me.uOMField = value
            End Set
        End Property

        '''<remarks/>
        Public Property LineValue() As CTSFulfilmentRequestOrderLineLineValue
            Get
                Return Me.lineValueField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderLineLineValue)
                Me.lineValueField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryChargeItem() As CTSFulfilmentRequestOrderLineDeliveryChargeItem
            Get
                Return Me.deliveryChargeItemField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderLineDeliveryChargeItem)
                Me.deliveryChargeItemField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingPrice() As CTSFulfilmentRequestOrderLineSellingPrice
            Get
                Return Me.sellingPriceField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderLineSellingPrice)
                Me.sellingPriceField = value
            End Set
        End Property

        '''<remarks/>
        Public Property FulfilmentSite() As CTSFulfilmentRequestOrderLineFulfilmentSite
            Get
                Return Me.fulfilmentSiteField
            End Get
            Set(ByVal value As CTSFulfilmentRequestOrderLineFulfilmentSite)
                Me.fulfilmentSiteField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderLineSellingStoreLineNo

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderLineOMOrderLineNo

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderLineLineStatus

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderLineProductCode

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderLineProductDescription

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderLineTotalOrderQuantity

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderLineQuantityTaken

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderLineUOM

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderLineLineValue

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderLineDeliveryChargeItem

        Private valueField As Boolean

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Boolean
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Boolean)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderLineSellingPrice

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSFulfilmentRequestOrderLineFulfilmentSite

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

End Namespace
