﻿Imports System.Xml.Serialization

Namespace WebService
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     Global.System.SerializableAttribute(), _
     Global.System.Diagnostics.DebuggerStepThroughAttribute(), _
     Global.System.ComponentModel.DesignerCategoryAttribute("code"), _
     Global.System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True), _
     Global.System.Xml.Serialization.XmlRootAttribute([Namespace]:="", IsNullable:=False)> _
    Partial Public Class OMOrderReceiveStatusUpdateResponse

        Private dateTimeStampField As OMOrderReceiveStatusUpdateResponseDateTimeStamp

        Private successFlagField As OMOrderReceiveStatusUpdateResponseSuccessFlag

        Private oMOrderNumberField As OMOrderReceiveStatusUpdateResponseOMOrderNumber

        Private fulfilmentSiteField As OMOrderReceiveStatusUpdateResponseFulfilmentSite

        Private orderStatusField As OMOrderReceiveStatusUpdateResponseOrderStatus

        '''<remarks/>
        Public Property DateTimeStamp() As OMOrderReceiveStatusUpdateResponseDateTimeStamp
            Get
                Return Me.dateTimeStampField
            End Get
            Set(ByVal value As OMOrderReceiveStatusUpdateResponseDateTimeStamp)
                Me.dateTimeStampField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SuccessFlag() As OMOrderReceiveStatusUpdateResponseSuccessFlag
            Get
                Return Me.successFlagField
            End Get
            Set(ByVal value As OMOrderReceiveStatusUpdateResponseSuccessFlag)
                Me.successFlagField = value
            End Set
        End Property

        '''<remarks/>
        Public Property OMOrderNumber() As OMOrderReceiveStatusUpdateResponseOMOrderNumber
            Get
                Return Me.oMOrderNumberField
            End Get
            Set(ByVal value As OMOrderReceiveStatusUpdateResponseOMOrderNumber)
                Me.oMOrderNumberField = value
            End Set
        End Property

        '''<remarks/>
        Public Property FulfilmentSite() As OMOrderReceiveStatusUpdateResponseFulfilmentSite
            Get
                Return Me.fulfilmentSiteField
            End Get
            Set(ByVal value As OMOrderReceiveStatusUpdateResponseFulfilmentSite)
                Me.fulfilmentSiteField = value
            End Set
        End Property

        '''<remarks/>
        Public Property OrderStatus() As OMOrderReceiveStatusUpdateResponseOrderStatus
            Get
                Return Me.orderStatusField
            End Get
            Set(ByVal value As OMOrderReceiveStatusUpdateResponseOrderStatus)
                Me.orderStatusField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     Global.System.SerializableAttribute(), _
     Global.System.Diagnostics.DebuggerStepThroughAttribute(), _
     Global.System.ComponentModel.DesignerCategoryAttribute("code"), _
     Global.System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderReceiveStatusUpdateResponseDateTimeStamp

        Private validationStatusField As String

        Private valueField As Date

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     Global.System.SerializableAttribute(), _
     Global.System.Diagnostics.DebuggerStepThroughAttribute(), _
     Global.System.ComponentModel.DesignerCategoryAttribute("code"), _
     Global.System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderReceiveStatusUpdateResponseSuccessFlag

        Private validationStatusField As String

        Private valueField As Boolean

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Boolean
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Boolean)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     Global.System.SerializableAttribute(), _
     Global.System.Diagnostics.DebuggerStepThroughAttribute(), _
     Global.System.ComponentModel.DesignerCategoryAttribute("code"), _
     Global.System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderReceiveStatusUpdateResponseOMOrderNumber

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     Global.System.SerializableAttribute(), _
     Global.System.Diagnostics.DebuggerStepThroughAttribute(), _
     Global.System.ComponentModel.DesignerCategoryAttribute("code"), _
     Global.System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderReceiveStatusUpdateResponseFulfilmentSite

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     Global.System.SerializableAttribute(), _
     Global.System.Diagnostics.DebuggerStepThroughAttribute(), _
     Global.System.ComponentModel.DesignerCategoryAttribute("code"), _
     Global.System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderReceiveStatusUpdateResponseOrderStatus

        Private orderLinesField() As OMOrderReceiveStatusUpdateResponseOrderStatusOrderLine

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlArrayItemAttribute("OrderLine", IsNullable:=False)> _
        Public Property OrderLines() As OMOrderReceiveStatusUpdateResponseOrderStatusOrderLine()
            Get
                Return Me.orderLinesField
            End Get
            Set(ByVal value As OMOrderReceiveStatusUpdateResponseOrderStatusOrderLine())
                Me.orderLinesField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     Global.System.SerializableAttribute(), _
     Global.System.Diagnostics.DebuggerStepThroughAttribute(), _
     Global.System.ComponentModel.DesignerCategoryAttribute("code"), _
     Global.System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderReceiveStatusUpdateResponseOrderStatusOrderLine

        Private oMOrderLineNoField As OMOrderReceiveStatusUpdateResponseOrderStatusOrderLineOMOrderLineNo

        Private lineStatusField As OMOrderReceiveStatusUpdateResponseOrderStatusOrderLineLineStatus

        '''<remarks/>
        Public Property OMOrderLineNo() As OMOrderReceiveStatusUpdateResponseOrderStatusOrderLineOMOrderLineNo
            Get
                Return Me.oMOrderLineNoField
            End Get
            Set(ByVal value As OMOrderReceiveStatusUpdateResponseOrderStatusOrderLineOMOrderLineNo)
                Me.oMOrderLineNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property LineStatus() As OMOrderReceiveStatusUpdateResponseOrderStatusOrderLineLineStatus
            Get
                Return Me.lineStatusField
            End Get
            Set(ByVal value As OMOrderReceiveStatusUpdateResponseOrderStatusOrderLineLineStatus)
                Me.lineStatusField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     Global.System.SerializableAttribute(), _
     Global.System.Diagnostics.DebuggerStepThroughAttribute(), _
     Global.System.ComponentModel.DesignerCategoryAttribute("code"), _
     Global.System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderReceiveStatusUpdateResponseOrderStatusOrderLineOMOrderLineNo

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     Global.System.SerializableAttribute(), _
     Global.System.Diagnostics.DebuggerStepThroughAttribute(), _
     Global.System.ComponentModel.DesignerCategoryAttribute("code"), _
     Global.System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderReceiveStatusUpdateResponseOrderStatusOrderLineLineStatus

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class
End Namespace

