﻿Imports System.Xml.Serialization
Imports System.Xml

'PD 31/01/2011
'Generated code modified by hand  Inherits Object
'Class CTSRefundCreateResponse altered by removing the line "Inherits Object"


Namespace WebService

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True), _
     Xml.Serialization.XmlRootAttribute([Namespace]:="", IsNullable:=False)> _
    Partial Public Class CTSRefundCreateResponse
        Implements ComponentModel.INotifyPropertyChanged

        Private dateTimeStampField As CTSRefundCreateResponseDateTimeStamp

        Private successFlagField As CTSRefundCreateResponseSuccessFlag

        Private refundHeaderField As CTSRefundCreateResponseRefundHeader

        Private refundLinesField() As CTSRefundCreateResponseRefundLine

        '''<remarks/>
        Public Property DateTimeStamp() As CTSRefundCreateResponseDateTimeStamp
            Get
                Return Me.dateTimeStampField
            End Get
            Set(ByVal value As CTSRefundCreateResponseDateTimeStamp)
                Me.dateTimeStampField = value
                Me.RaisePropertyChanged("DateTimeStamp")
            End Set
        End Property

        '''<remarks/>
        Public Property SuccessFlag() As CTSRefundCreateResponseSuccessFlag
            Get
                Return Me.successFlagField
            End Get
            Set(ByVal value As CTSRefundCreateResponseSuccessFlag)
                Me.successFlagField = value
                Me.RaisePropertyChanged("SuccessFlag")
            End Set
        End Property

        '''<remarks/>
        Public Property RefundHeader() As CTSRefundCreateResponseRefundHeader
            Get
                Return Me.refundHeaderField
            End Get
            Set(ByVal value As CTSRefundCreateResponseRefundHeader)
                Me.refundHeaderField = value
                Me.RaisePropertyChanged("RefundHeader")
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlArrayItemAttribute("RefundLine", IsNullable:=False)> _
        Public Property RefundLines() As CTSRefundCreateResponseRefundLine()
            Get
                Return Me.refundLinesField
            End Get
            Set(ByVal value As CTSRefundCreateResponseRefundLine())
                Me.refundLinesField = value
                Me.RaisePropertyChanged("RefundLines")
            End Set
        End Property

        Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSRefundCreateResponseDateTimeStamp
        Inherits Object
        Implements ComponentModel.INotifyPropertyChanged

        Private validationStatusField As String

        Private valueField As Date

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
                Me.RaisePropertyChanged("ValidationStatus")
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
                Me.RaisePropertyChanged("Value")
            End Set
        End Property

        Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSRefundCreateResponseSuccessFlag
        Inherits Object
        Implements ComponentModel.INotifyPropertyChanged

        Private valueField As Boolean

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Boolean
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Boolean)
                Me.valueField = value
                Me.RaisePropertyChanged("Value")
            End Set
        End Property

        Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSRefundCreateResponseRefundHeader
        Inherits Object
        Implements ComponentModel.INotifyPropertyChanged

        Private sourceField As CTSRefundCreateResponseRefundHeaderSource

        Private sourceOrderNumberField As CTSRefundCreateResponseRefundHeaderSourceOrderNumber

        Private storeOrderNumberField As CTSRefundCreateResponseRefundHeaderStoreOrderNumber

        Private refundDateField As CTSRefundCreateResponseRefundHeaderRefundDate

        Private refundSeqField As CTSRefundCreateResponseRefundHeaderRefundSeq

        Private loyaltyCardNumberField As CTSRefundCreateResponseRefundHeaderLoyaltyCardNumber

        Private deliveryChargeRefundValueField As CTSRefundCreateResponseRefundHeaderDeliveryChargeRefundValue

        Private refundTotalField As CTSRefundCreateResponseRefundHeaderRefundTotal

        '''<remarks/>
        Public Property Source() As CTSRefundCreateResponseRefundHeaderSource
            Get
                Return Me.sourceField
            End Get
            Set(ByVal value As CTSRefundCreateResponseRefundHeaderSource)
                Me.sourceField = value
                Me.RaisePropertyChanged("Source")
            End Set
        End Property

        '''<remarks/>
        Public Property SourceOrderNumber() As CTSRefundCreateResponseRefundHeaderSourceOrderNumber
            Get
                Return Me.sourceOrderNumberField
            End Get
            Set(ByVal value As CTSRefundCreateResponseRefundHeaderSourceOrderNumber)
                Me.sourceOrderNumberField = value
                Me.RaisePropertyChanged("SourceOrderNumber")
            End Set
        End Property

        '''<remarks/>
        Public Property StoreOrderNumber() As CTSRefundCreateResponseRefundHeaderStoreOrderNumber
            Get
                Return Me.storeOrderNumberField
            End Get
            Set(ByVal value As CTSRefundCreateResponseRefundHeaderStoreOrderNumber)
                Me.storeOrderNumberField = value
                Me.RaisePropertyChanged("StoreOrderNumber")
            End Set
        End Property

        '''<remarks/>
        Public Property RefundDate() As CTSRefundCreateResponseRefundHeaderRefundDate
            Get
                Return Me.refundDateField
            End Get
            Set(ByVal value As CTSRefundCreateResponseRefundHeaderRefundDate)
                Me.refundDateField = value
                Me.RaisePropertyChanged("RefundDate")
            End Set
        End Property

        '''<remarks/>
        Public Property RefundSeq() As CTSRefundCreateResponseRefundHeaderRefundSeq
            Get
                Return Me.refundSeqField
            End Get
            Set(ByVal value As CTSRefundCreateResponseRefundHeaderRefundSeq)
                Me.refundSeqField = value
                Me.RaisePropertyChanged("RefundSeq")
            End Set
        End Property

        '''<remarks/>
        Public Property LoyaltyCardNumber() As CTSRefundCreateResponseRefundHeaderLoyaltyCardNumber
            Get
                Return Me.loyaltyCardNumberField
            End Get
            Set(ByVal value As CTSRefundCreateResponseRefundHeaderLoyaltyCardNumber)
                Me.loyaltyCardNumberField = value
                Me.RaisePropertyChanged("LoyaltyCardNumber")
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryChargeRefundValue() As CTSRefundCreateResponseRefundHeaderDeliveryChargeRefundValue
            Get
                Return Me.deliveryChargeRefundValueField
            End Get
            Set(ByVal value As CTSRefundCreateResponseRefundHeaderDeliveryChargeRefundValue)
                Me.deliveryChargeRefundValueField = value
                Me.RaisePropertyChanged("DeliveryChargeRefundValue")
            End Set
        End Property

        '''<remarks/>
        Public Property RefundTotal() As CTSRefundCreateResponseRefundHeaderRefundTotal
            Get
                Return Me.refundTotalField
            End Get
            Set(ByVal value As CTSRefundCreateResponseRefundHeaderRefundTotal)
                Me.refundTotalField = value
                Me.RaisePropertyChanged("RefundTotal")
            End Set
        End Property

        Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSRefundCreateResponseRefundHeaderSource
        Inherits Object
        Implements ComponentModel.INotifyPropertyChanged

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
                Me.RaisePropertyChanged("ValidationStatus")
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
                Me.RaisePropertyChanged("Value")
            End Set
        End Property

        Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSRefundCreateResponseRefundHeaderSourceOrderNumber
        Inherits Object
        Implements ComponentModel.INotifyPropertyChanged

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
                Me.RaisePropertyChanged("ValidationStatus")
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
                Me.RaisePropertyChanged("Value")
            End Set
        End Property

        Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSRefundCreateResponseRefundHeaderStoreOrderNumber
        Inherits Object
        Implements ComponentModel.INotifyPropertyChanged

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
                Me.RaisePropertyChanged("ValidationStatus")
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
                Me.RaisePropertyChanged("Value")
            End Set
        End Property

        Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSRefundCreateResponseRefundHeaderRefundDate
        Inherits Object
        Implements ComponentModel.INotifyPropertyChanged

        Private validationStatusField As String

        Private valueField As Date

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
                Me.RaisePropertyChanged("ValidationStatus")
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
                Me.RaisePropertyChanged("Value")
            End Set
        End Property

        Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSRefundCreateResponseRefundHeaderRefundSeq
        Inherits Object
        Implements ComponentModel.INotifyPropertyChanged

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
                Me.RaisePropertyChanged("ValidationStatus")
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
                Me.RaisePropertyChanged("Value")
            End Set
        End Property

        Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSRefundCreateResponseRefundHeaderLoyaltyCardNumber
        Inherits Object
        Implements ComponentModel.INotifyPropertyChanged

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
                Me.RaisePropertyChanged("ValidationStatus")
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
                Me.RaisePropertyChanged("Value")
            End Set
        End Property

        Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSRefundCreateResponseRefundHeaderDeliveryChargeRefundValue
        Inherits Object
        Implements ComponentModel.INotifyPropertyChanged

        Private validationStatusField As String

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
                Me.RaisePropertyChanged("ValidationStatus")
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
                Me.RaisePropertyChanged("Value")
            End Set
        End Property

        Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSRefundCreateResponseRefundHeaderRefundTotal
        Inherits Object
        Implements ComponentModel.INotifyPropertyChanged

        Private validationStatusField As String

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
                Me.RaisePropertyChanged("ValidationStatus")
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
                Me.RaisePropertyChanged("Value")
            End Set
        End Property

        Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSRefundCreateResponseRefundLine
        Inherits Object
        Implements ComponentModel.INotifyPropertyChanged

        Private storeLineNoField As CTSRefundCreateResponseRefundLineStoreLineNo

        Private productCodeField As CTSRefundCreateResponseRefundLineProductCode

        Private quantityCancelledField As CTSRefundCreateResponseRefundLineQuantityCancelled

        Private refundLineValueField As CTSRefundCreateResponseRefundLineRefundLineValue

        '''<remarks/>
        Public Property StoreLineNo() As CTSRefundCreateResponseRefundLineStoreLineNo
            Get
                Return Me.storeLineNoField
            End Get
            Set(ByVal value As CTSRefundCreateResponseRefundLineStoreLineNo)
                Me.storeLineNoField = value
                Me.RaisePropertyChanged("StoreLineNo")
            End Set
        End Property

        '''<remarks/>
        Public Property ProductCode() As CTSRefundCreateResponseRefundLineProductCode
            Get
                Return Me.productCodeField
            End Get
            Set(ByVal value As CTSRefundCreateResponseRefundLineProductCode)
                Me.productCodeField = value
                Me.RaisePropertyChanged("ProductCode")
            End Set
        End Property

        '''<remarks/>
        Public Property QuantityCancelled() As CTSRefundCreateResponseRefundLineQuantityCancelled
            Get
                Return Me.quantityCancelledField
            End Get
            Set(ByVal value As CTSRefundCreateResponseRefundLineQuantityCancelled)
                Me.quantityCancelledField = value
                Me.RaisePropertyChanged("QuantityCancelled")
            End Set
        End Property

        '''<remarks/>
        Public Property RefundLineValue() As CTSRefundCreateResponseRefundLineRefundLineValue
            Get
                Return Me.refundLineValueField
            End Get
            Set(ByVal value As CTSRefundCreateResponseRefundLineRefundLineValue)
                Me.refundLineValueField = value
                Me.RaisePropertyChanged("RefundLineValue")
            End Set
        End Property

        Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSRefundCreateResponseRefundLineStoreLineNo
        Inherits Object
        Implements ComponentModel.INotifyPropertyChanged

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
                Me.RaisePropertyChanged("ValidationStatus")
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
                Me.RaisePropertyChanged("Value")
            End Set
        End Property

        Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSRefundCreateResponseRefundLineProductCode
        Inherits Object
        Implements ComponentModel.INotifyPropertyChanged

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
                Me.RaisePropertyChanged("ValidationStatus")
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
                Me.RaisePropertyChanged("Value")
            End Set
        End Property

        Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSRefundCreateResponseRefundLineQuantityCancelled
        Inherits Object
        Implements ComponentModel.INotifyPropertyChanged

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
                Me.RaisePropertyChanged("ValidationStatus")
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
                Me.RaisePropertyChanged("Value")
            End Set
        End Property

        Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSRefundCreateResponseRefundLineRefundLineValue
        Inherits Object
        Implements ComponentModel.INotifyPropertyChanged

        Private validationStatusField As String

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
                Me.RaisePropertyChanged("ValidationStatus")
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
                Me.RaisePropertyChanged("Value")
            End Set
        End Property

        Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class

End Namespace
