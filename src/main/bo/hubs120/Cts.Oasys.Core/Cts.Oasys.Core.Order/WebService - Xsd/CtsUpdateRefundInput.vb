﻿Imports System.Xml
Imports System.Xml.Serialization

Namespace WebService

    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True), _
      Serialization.XmlRootAttribute([Namespace]:="", IsNullable:=False)> _
    Partial Public Class CTSUpdateRefundRequest
        Private dateTimeStampField As Date
        Private oMOrderNumberField As String
        Private orderRefundsField() As CTSUpdateRefundRequestOrderRefund

        Public Property DateTimeStamp() As Date
            Get
                Return Me.dateTimeStampField
            End Get
            Set(ByVal value As Date)
                Me.dateTimeStampField = value
            End Set
        End Property

        Public Property OMOrderNumber() As String
            Get
                Return Me.oMOrderNumberField
            End Get
            Set(ByVal value As String)
                Me.oMOrderNumberField = value
            End Set
        End Property

        <Serialization.XmlArrayItemAttribute("OrderRefund", IsNullable:=False)> _
        Public Property OrderRefunds() As CTSUpdateRefundRequestOrderRefund()
            Get
                Return Me.orderRefundsField
            End Get
            Set(ByVal value As CTSUpdateRefundRequestOrderRefund())
                Me.orderRefundsField = value
            End Set
        End Property
    End Class

    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundRequestOrderRefund
        Private refundDateField As Date
        Private refundStoreCodeField As String
        Private refundTransactionField As String
        Private refundTillField As String
        Private fulfilmentSitesField() As CTSUpdateRefundRequestOrderRefundFulfilmentSite
        Private refundLinesField() As CTSUpdateRefundRequestOrderRefundRefundLine

        Public Property RefundDate() As Date
            Get
                Return Me.refundDateField
            End Get
            Set(ByVal value As Date)
                Me.refundDateField = value
            End Set
        End Property

        <Serialization.XmlElementAttribute(DataType:="integer")> _
        Public Property RefundStoreCode() As String
            Get
                Return Me.refundStoreCodeField
            End Get
            Set(ByVal value As String)
                Me.refundStoreCodeField = value
            End Set
        End Property

        Public Property RefundTransaction() As String
            Get
                Return Me.refundTransactionField
            End Get
            Set(ByVal value As String)
                Me.refundTransactionField = value
            End Set
        End Property

        Public Property RefundTill() As String
            Get
                Return Me.refundTillField
            End Get
            Set(ByVal value As String)
                Me.refundTillField = value
            End Set
        End Property

        <Serialization.XmlArrayItemAttribute("FulfilmentSite", IsNullable:=False)> _
        Public Property FulfilmentSites() As CTSUpdateRefundRequestOrderRefundFulfilmentSite()
            Get
                Return Me.fulfilmentSitesField
            End Get
            Set(ByVal value As CTSUpdateRefundRequestOrderRefundFulfilmentSite())
                Me.fulfilmentSitesField = value
            End Set
        End Property

        <Serialization.XmlArrayItemAttribute("RefundLine", IsNullable:=False)> _
        Public Property RefundLines() As CTSUpdateRefundRequestOrderRefundRefundLine()
            Get
                Return Me.refundLinesField
            End Get
            Set(ByVal value As CTSUpdateRefundRequestOrderRefundRefundLine())
                Me.refundLinesField = value
            End Set
        End Property
    End Class

    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundRequestOrderRefundFulfilmentSite
        Private fulfilmentSiteCodeField As String
        Private sellingStoreIBTOutNumberField As String


        <Serialization.XmlElementAttribute(DataType:="integer")> _
        Public Property FulfilmentSiteCode() As String
            Get
                Return Me.fulfilmentSiteCodeField
            End Get
            Set(ByVal value As String)
                Me.fulfilmentSiteCodeField = value
            End Set
        End Property

        <Serialization.XmlElementAttribute(DataType:="integer")> _
        Public Property SellingStoreIBTOutNumber() As String
            Get
                Return Me.sellingStoreIBTOutNumberField
            End Get
            Set(ByVal value As String)
                Me.sellingStoreIBTOutNumberField = value
            End Set
        End Property
    End Class

    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSUpdateRefundRequestOrderRefundRefundLine
        Private sellingStoreLineNoField As String
        Private oMOrderLineNoField As String
        Private productCodeField As String
        Private fulfilmentSiteCodeField As String
        Private quantityReturnedField As String
        Private quantityCancelledField As String
        Private refundLineValueField As Decimal
        Private refundLineStatusField As String

        <Serialization.XmlElementAttribute(DataType:="integer")> _
        Public Property SellingStoreLineNo() As String
            Get
                Return Me.sellingStoreLineNoField
            End Get
            Set(ByVal value As String)
                Me.sellingStoreLineNoField = value
            End Set
        End Property

        <Serialization.XmlElementAttribute(DataType:="integer")> _
        Public Property OMOrderLineNo() As String
            Get
                Return Me.oMOrderLineNoField
            End Get
            Set(ByVal value As String)
                Me.oMOrderLineNoField = value
            End Set
        End Property

        <Serialization.XmlElementAttribute(DataType:="integer")> _
        Public Property ProductCode() As String
            Get
                Return Me.productCodeField
            End Get
            Set(ByVal value As String)
                Me.productCodeField = value
            End Set
        End Property

        <Serialization.XmlElementAttribute(DataType:="integer")> _
        Public Property FulfilmentSiteCode() As String
            Get
                Return Me.fulfilmentSiteCodeField
            End Get
            Set(ByVal value As String)
                Me.fulfilmentSiteCodeField = value
            End Set
        End Property

        <Serialization.XmlElementAttribute(DataType:="integer")> _
        Public Property QuantityReturned() As String
            Get
                Return Me.quantityReturnedField
            End Get
            Set(ByVal value As String)
                Me.quantityReturnedField = value
            End Set
        End Property

        <Serialization.XmlElementAttribute(DataType:="integer")> _
        Public Property QuantityCancelled() As String
            Get
                Return Me.quantityCancelledField
            End Get
            Set(ByVal value As String)
                Me.quantityCancelledField = value
            End Set
        End Property

        Public Property RefundLineValue() As Decimal
            Get
                Return Me.refundLineValueField
            End Get
            Set(ByVal value As Decimal)
                Me.refundLineValueField = value
            End Set
        End Property

        <Serialization.XmlElementAttribute(DataType:="integer")> _
        Public Property RefundLineStatus() As String
            Get
                Return Me.refundLineStatusField
            End Get
            Set(ByVal value As String)
                Me.refundLineStatusField = value
            End Set
        End Property
    End Class

End Namespace
