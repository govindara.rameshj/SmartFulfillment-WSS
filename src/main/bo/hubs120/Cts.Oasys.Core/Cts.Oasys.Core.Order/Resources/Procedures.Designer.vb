﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.3053
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System

Namespace My.Resources
    
    'This class was auto-generated by the StronglyTypedResourceBuilder
    'class via a tool like ResGen or Visual Studio.
    'To add or remove a member, edit your .ResX file then rerun ResGen
    'with the /str option, or rebuild your VS project.
    '''<summary>
    '''  A strongly-typed resource class, for looking up localized strings, etc.
    '''</summary>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0"),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute()>  _
    Friend Class Procedures
        
        Private Shared resourceMan As Global.System.Resources.ResourceManager
        
        Private Shared resourceCulture As Global.System.Globalization.CultureInfo
        
        <Global.System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>  _
        Friend Sub New()
            MyBase.New
        End Sub
        
        '''<summary>
        '''  Returns the cached ResourceManager instance used by this class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Shared ReadOnly Property ResourceManager() As Global.System.Resources.ResourceManager
            Get
                If Object.ReferenceEquals(resourceMan, Nothing) Then
                    Dim temp As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("Cts.Oasys.Core.Order.Procedures", GetType(Procedures).Assembly)
                    resourceMan = temp
                End If
                Return resourceMan
            End Get
        End Property
        
        '''<summary>
        '''  Overrides the current thread's CurrentUICulture property for all
        '''  resource lookups using this strongly typed resource class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Shared Property Culture() As Global.System.Globalization.CultureInfo
            Get
                Return resourceCulture
            End Get
            Set
                resourceCulture = value
            End Set
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to ConsignmentGetByNumber.
        '''</summary>
        Friend Shared ReadOnly Property ConsignmentGetByNumber() As String
            Get
                Return ResourceManager.GetString("ConsignmentGetByNumber", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to ContainerGetByDateRange.
        '''</summary>
        Friend Shared ReadOnly Property ContainerGetByDateRange() As String
            Get
                Return ResourceManager.GetString("ContainerGetByDateRange", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to ContainerGetLines.
        '''</summary>
        Friend Shared ReadOnly Property ContainerGetLines() As String
            Get
                Return ResourceManager.GetString("ContainerGetLines", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to IBTInsert.
        '''</summary>
        Friend Shared ReadOnly Property IBTInsert() As String
            Get
                Return ResourceManager.GetString("IBTInsert", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to IBTInsertLine.
        '''</summary>
        Friend Shared ReadOnly Property IBTInsertLine() As String
            Get
                Return ResourceManager.GetString("IBTInsertLine", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to IBTNumberGet.
        '''</summary>
        Friend Shared ReadOnly Property IBTNumberGet() As String
            Get
                Return ResourceManager.GetString("IBTNumberGet", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to PoConsign.
        '''</summary>
        Friend Shared ReadOnly Property PoConsign() As String
            Get
                Return ResourceManager.GetString("PoConsign", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to PoGetConsigned.
        '''</summary>
        Friend Shared ReadOnly Property PoGetConsigned() As String
            Get
                Return ResourceManager.GetString("PoGetConsigned", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to PoGetLines.
        '''</summary>
        Friend Shared ReadOnly Property PoGetLines() As String
            Get
                Return ResourceManager.GetString("PoGetLines", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to PoGetNotConsigned.
        '''</summary>
        Friend Shared ReadOnly Property PoGetNotConsigned() As String
            Get
                Return ResourceManager.GetString("PoGetNotConsigned", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to PoGetOutstanding.
        '''</summary>
        Friend Shared ReadOnly Property PoGetOutstanding() As String
            Get
                Return ResourceManager.GetString("PoGetOutstanding", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to PoInsertLine.
        '''</summary>
        Friend Shared ReadOnly Property PoInsertLine() As String
            Get
                Return ResourceManager.GetString("PoInsertLine", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to PoInsertNewOrder.
        '''</summary>
        Friend Shared ReadOnly Property PoInsertNewOrder() As String
            Get
                Return ResourceManager.GetString("PoInsertNewOrder", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to PoUpdate.
        '''</summary>
        Friend Shared ReadOnly Property PoUpdate() As String
            Get
                Return ResourceManager.GetString("PoUpdate", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to QodGetNonZeroStockOrOutstandingIbt.
        '''</summary>
        Friend Shared ReadOnly Property QodGetNonZeroStockOrOutstandingIbt() As String
            Get
                Return ResourceManager.GetString("QodGetNonZeroStockOrOutstandingIbt", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to QodHeaderUpdate.
        '''</summary>
        Friend Shared ReadOnly Property QodHeaderUpdate() As String
            Get
                Return ResourceManager.GetString("QodHeaderUpdate", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to ReceiptGetLines.
        '''</summary>
        Friend Shared ReadOnly Property ReceiptGetLines() As String
            Get
                Return ResourceManager.GetString("ReceiptGetLines", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to ReceiptGetMaintainableOrders.
        '''</summary>
        Friend Shared ReadOnly Property ReceiptGetMaintainableOrders() As String
            Get
                Return ResourceManager.GetString("ReceiptGetMaintainableOrders", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to ReceiptIbtInsert.
        '''</summary>
        Friend Shared ReadOnly Property ReceiptIbtInsert() As String
            Get
                Return ResourceManager.GetString("ReceiptIbtInsert", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to ReceiptIbtInsertLine.
        '''</summary>
        Friend Shared ReadOnly Property ReceiptIbtInsertLine() As String
            Get
                Return ResourceManager.GetString("ReceiptIbtInsertLine", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to ReceiptLineInsert.
        '''</summary>
        Friend Shared ReadOnly Property ReceiptLineInsert() As String
            Get
                Return ResourceManager.GetString("ReceiptLineInsert", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to ReceiptLineUpdate.
        '''</summary>
        Friend Shared ReadOnly Property ReceiptLineUpdate() As String
            Get
                Return ResourceManager.GetString("ReceiptLineUpdate", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to ReceiptOrderInsert.
        '''</summary>
        Friend Shared ReadOnly Property ReceiptOrderInsert() As String
            Get
                Return ResourceManager.GetString("ReceiptOrderInsert", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to ReceiptOrderUpdate.
        '''</summary>
        Friend Shared ReadOnly Property ReceiptOrderUpdate() As String
            Get
                Return ResourceManager.GetString("ReceiptOrderUpdate", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to ReturnGetLines.
        '''</summary>
        Friend Shared ReadOnly Property ReturnGetLines() As String
            Get
                Return ResourceManager.GetString("ReturnGetLines", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to ReturnGetNonReleased.
        '''</summary>
        Friend Shared ReadOnly Property ReturnGetNonReleased() As String
            Get
                Return ResourceManager.GetString("ReturnGetNonReleased", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleCustomerGet.
        '''</summary>
        Friend Shared ReadOnly Property SaleCustomerGet() As String
            Get
                Return ResourceManager.GetString("SaleCustomerGet", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleCustomerInsert.
        '''</summary>
        Friend Shared ReadOnly Property SaleCustomerInsert() As String
            Get
                Return ResourceManager.GetString("SaleCustomerInsert", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleCustomerUpdate.
        '''</summary>
        Friend Shared ReadOnly Property SaleCustomerUpdate() As String
            Get
                Return ResourceManager.GetString("SaleCustomerUpdate", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleGet.
        '''</summary>
        Friend Shared ReadOnly Property SaleGet() As String
            Get
                Return ResourceManager.GetString("SaleGet", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleInsert.
        '''</summary>
        Friend Shared ReadOnly Property SaleInsert() As String
            Get
                Return ResourceManager.GetString("SaleInsert", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleLineGet.
        '''</summary>
        Friend Shared ReadOnly Property SaleLineGet() As String
            Get
                Return ResourceManager.GetString("SaleLineGet", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleLineInsert.
        '''</summary>
        Friend Shared ReadOnly Property SaleLineInsert() As String
            Get
                Return ResourceManager.GetString("SaleLineInsert", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleLineUpdate.
        '''</summary>
        Friend Shared ReadOnly Property SaleLineUpdate() As String
            Get
                Return ResourceManager.GetString("SaleLineUpdate", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleOrderGet.
        '''</summary>
        Friend Shared ReadOnly Property SaleOrderGet() As String
            Get
                Return ResourceManager.GetString("SaleOrderGet", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleOrderInsert.
        '''</summary>
        Friend Shared ReadOnly Property SaleOrderInsert() As String
            Get
                Return ResourceManager.GetString("SaleOrderInsert", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleOrderLineGet.
        '''</summary>
        Friend Shared ReadOnly Property SaleOrderLineGet() As String
            Get
                Return ResourceManager.GetString("SaleOrderLineGet", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleOrderLineInsert.
        '''</summary>
        Friend Shared ReadOnly Property SaleOrderLineInsert() As String
            Get
                Return ResourceManager.GetString("SaleOrderLineInsert", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleOrderLineUpdate.
        '''</summary>
        Friend Shared ReadOnly Property SaleOrderLineUpdate() As String
            Get
                Return ResourceManager.GetString("SaleOrderLineUpdate", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleOrderRefundGet.
        '''</summary>
        Friend Shared ReadOnly Property SaleOrderRefundGet() As String
            Get
                Return ResourceManager.GetString("SaleOrderRefundGet", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleOrderRefundInsert.
        '''</summary>
        Friend Shared ReadOnly Property SaleOrderRefundInsert() As String
            Get
                Return ResourceManager.GetString("SaleOrderRefundInsert", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleOrderRefundStatusUpdate.
        '''</summary>
        Friend Shared ReadOnly Property SaleOrderRefundStatusUpdate() As String
            Get
                Return ResourceManager.GetString("SaleOrderRefundStatusUpdate", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleOrderTextDelete.
        '''</summary>
        Friend Shared ReadOnly Property SaleOrderTextDelete() As String
            Get
                Return ResourceManager.GetString("SaleOrderTextDelete", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleOrderTextGet.
        '''</summary>
        Friend Shared ReadOnly Property SaleOrderTextGet() As String
            Get
                Return ResourceManager.GetString("SaleOrderTextGet", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleOrderTextInsert.
        '''</summary>
        Friend Shared ReadOnly Property SaleOrderTextInsert() As String
            Get
                Return ResourceManager.GetString("SaleOrderTextInsert", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleOrderTextUpdate.
        '''</summary>
        Friend Shared ReadOnly Property SaleOrderTextUpdate() As String
            Get
                Return ResourceManager.GetString("SaleOrderTextUpdate", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleOrderUpdate.
        '''</summary>
        Friend Shared ReadOnly Property SaleOrderUpdate() As String
            Get
                Return ResourceManager.GetString("SaleOrderUpdate", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleOrderUpdateMaintainable.
        '''</summary>
        Friend Shared ReadOnly Property SaleOrderUpdateMaintainable() As String
            Get
                Return ResourceManager.GetString("SaleOrderUpdateMaintainable", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SalePaidGet.
        '''</summary>
        Friend Shared ReadOnly Property SalePaidGet() As String
            Get
                Return ResourceManager.GetString("SalePaidGet", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SalePaidInsert.
        '''</summary>
        Friend Shared ReadOnly Property SalePaidInsert() As String
            Get
                Return ResourceManager.GetString("SalePaidInsert", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SaleUpdate.
        '''</summary>
        Friend Shared ReadOnly Property SaleUpdate() As String
            Get
                Return ResourceManager.GetString("SaleUpdate", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to @SaleVendaGet.
        '''</summary>
        Friend Shared ReadOnly Property SaleVendaGet() As String
            Get
                Return ResourceManager.GetString("SaleVendaGet", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to StockGetPrice.
        '''</summary>
        Friend Shared ReadOnly Property StockGetPrice() As String
            Get
                Return ResourceManager.GetString("StockGetPrice", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to usp_StockGetStkmasDataForStockLog.
        '''</summary>
        Friend Shared ReadOnly Property StockGetStkmasDataForStockLog() As String
            Get
                Return ResourceManager.GetString("StockGetStkmasDataForStockLog", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to StockUpdateSoqInit.
        '''</summary>
        Friend Shared ReadOnly Property StockUpdateSoqInit() As String
            Get
                Return ResourceManager.GetString("StockUpdateSoqInit", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to StockUpdateSoqInventory.
        '''</summary>
        Friend Shared ReadOnly Property StockUpdateSoqInventory() As String
            Get
                Return ResourceManager.GetString("StockUpdateSoqInventory", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SupplierGetAll.
        '''</summary>
        Friend Shared ReadOnly Property SupplierGetAll() As String
            Get
                Return ResourceManager.GetString("SupplierGetAll", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SupplierGetAllForOrder.
        '''</summary>
        Friend Shared ReadOnly Property SupplierGetAllForOrder() As String
            Get
                Return ResourceManager.GetString("SupplierGetAllForOrder", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SupplierGetByNumber.
        '''</summary>
        Friend Shared ReadOnly Property SupplierGetByNumber() As String
            Get
                Return ResourceManager.GetString("SupplierGetByNumber", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SupplierGetByNumbers.
        '''</summary>
        Friend Shared ReadOnly Property SupplierGetByNumbers() As String
            Get
                Return ResourceManager.GetString("SupplierGetByNumbers", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SupplierUpdateSoqInventory.
        '''</summary>
        Friend Shared ReadOnly Property SupplierUpdateSoqInventory() As String
            Get
                Return ResourceManager.GetString("SupplierUpdateSoqInventory", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SystemCodesSelectType.
        '''</summary>
        Friend Shared ReadOnly Property SystemCodesSelectType() As String
            Get
                Return ResourceManager.GetString("SystemCodesSelectType", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to usp_CashBalCashierUpdate.
        '''</summary>
        Friend Shared ReadOnly Property UpdateCashBalCashier() As String
            Get
                Return ResourceManager.GetString("UpdateCashBalCashier", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to usp_CashBalCashTenUpdate.
        '''</summary>
        Friend Shared ReadOnly Property UpdateCashBalCashTen() As String
            Get
                Return ResourceManager.GetString("UpdateCashBalCashTen", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to VatGet.
        '''</summary>
        Friend Shared ReadOnly Property VatGet() As String
            Get
                Return ResourceManager.GetString("VatGet", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to VendaOrderGet.
        '''</summary>
        Friend Shared ReadOnly Property VendaOrderGet() As String
            Get
                Return ResourceManager.GetString("VendaOrderGet", resourceCulture)
            End Get
        End Property
    End Class
End Namespace
