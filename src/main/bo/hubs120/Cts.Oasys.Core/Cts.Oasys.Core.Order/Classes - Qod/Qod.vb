Imports Cts.Oasys.Core.Order.Qod.State
Imports Cts.Oasys.Core.Order.WebService
Imports Cts.Oasys.Data
Imports System.Text
Imports System.Xml

Namespace Qod 

    <HideModuleName()> Public Module SharedFunctions

        Public Function GetAllNewOrders() As QodHeaderCollection
            Dim oc As New QodHeaderCollection
            oc.LoadForState(Delivery.OrderCreated, False)
            oc.LoadForState(Delivery.DeliveryRequest, False)
            oc.LoadForState(Delivery.PickingListCreated, False)
            oc.LoadForState(Delivery.DespatchCreated, False)
            oc.LoadForState(Delivery.UndeliveredCreated, False)
            oc.LoadForState(Delivery.DeliveredCreated, False)
            For Each QodHeader As QodHeader In oc
                QodHeader.ExistsInDB = True
            Next
            Return oc
        End Function

        Public Function GetAllRetryOrders() As QodHeaderCollection
            Dim oc As New QodHeaderCollection
            oc.LoadForStateRange(Delivery.OrderFailedConnection, Delivery.OrderFailedData, False)
            oc.LoadForStateRange(Delivery.DeliveryRequestFailedConnection, Delivery.DeliveryRequestFailedData, False)
            oc.LoadForStateRange(Delivery.PickingNotifyFailedConnection, Delivery.PickingNotifyFailedData, False)
            oc.LoadForStateRange(Delivery.DespatchNotifyFailedConnection, Delivery.DespatchNotifyFailedData, False)
            oc.LoadForStateRange(Delivery.UndeliveredNotifyFailedConnection, Delivery.UndeliveredNotifyFailedData, False)
            oc.LoadForStateRange(Delivery.DeliveredNotifyFailedConnection, Delivery.DeliveredNotifyFailedData, False)
            For Each QodHeader As QodHeader In oc
                QodHeader.ExistsInDB = True
            Next
            Return oc
        End Function

        Public Function GetAllNewRefundOrders() As QodHeaderCollection
            Dim oc As New QodHeaderCollection
            oc.LoadRefundsForState(Refund.Created)
            For Each QodHeader As QodHeader In oc
                QodHeader.ExistsInDB = True
            Next
            Return oc
        End Function

        Public Function GetAllRetryRefundOrders() As QodHeaderCollection
            Dim oc As New QodHeaderCollection
            oc.LoadRefundsForStateRange(Refund.CreatedOnHold, Refund.NotifyFailedData)
            For Each QodHeader As QodHeader In oc
                QodHeader.ExistsInDB = True
            Next
            Return oc
        End Function

        Public Function GetAllOpenOrders() As QodHeaderCollection
            Dim oc As New QodHeaderCollection
            oc.LoadForStateRange(Delivery.OrderCreated, Delivery.DeliveredStatusFailedState)
            For Each QodHeader As QodHeader In oc
                QodHeader.ExistsInDB = True
            Next
            Return oc
        End Function

        Public Function GetAllForDateRange(ByVal dateStart As Date, ByVal dateEnd As Date) As QodHeaderCollection
            Dim oc As New QodHeaderCollection
            oc.LoadForDateRange(dateStart, dateEnd)
            For Each QodHeader As QodHeader In oc
                QodHeader.ExistsInDB = True
            Next
            Return oc
        End Function

        Public Function GetForQodNumber(ByVal qodNumber As String) As QodHeader
            Dim dt As DataTable = Qod.DataAccess.GetHeaderForQodNumber(qodNumber)
            If dt.Rows.Count > 0 Then
                Dim QodHeader As New QodHeader(dt.Rows(0))
                QodHeader.ExistsInDB = True
                Return QodHeader
            End If
            Return Nothing
        End Function

        Public Function GetForOmOrderNumber(ByVal omOrderNumber As Integer) As QodHeader
            Dim dt As DataTable = Qod.DataAccess.GetHeaderForOmOrderNumber(omOrderNumber)
            If dt.Rows.Count > 0 Then
                Dim QodHeader As New QodHeader(dt.Rows(0))
                QodHeader.ExistsInDB = True
                Return QodHeader
            End If
            Return Nothing
        End Function

        Public Function GetForVendaNumber(ByVal vendaNumber As String) As QodHeader
            Dim dt As DataTable = Qod.DataAccess.GetHeaderForVendaNumber(vendaNumber)
            If dt.Rows.Count > 0 Then
                Return New QodHeader(dt.Rows(0))
            End If
            Return Nothing
        End Function

        Public Function OrderExistsForOmOrderNumber(ByVal omOrderNumber As Integer) As Boolean
            Using dt As DataTable = DataAccess.GetHeaderForOmOrderNumber(omOrderNumber)
                Return ((dt IsNot Nothing) AndAlso (dt.Rows.Count > 0))
            End Using
        End Function

        Public Function GetSalesFromQod(ByVal tranDate As Date, ByVal tillId As String, ByVal tranNo As String) As DataTable
            Dim dt As DataTable = Qod.DataAccess.SaleGet(tranDate, tillId, tranNo)
            If dt.Rows.Count > 0 Then
                Return dt
            End If
            Return Nothing
        End Function

        Public Function GetNonZeroStockOrOutstandingIbt() As DataSet
            Dim ds As DataSet = DataAccess.GetNonZeroStockOrOutstandingIbt
            Return ds
        End Function

        Friend Function CreateFromOmFullStatus(ByVal orderDetail As OMSendFullStatusUpdateResponseOrderStatusOrderDetail) As QodHeader
            Dim qod As New QodHeader
            qod.VendaNumber = orderDetail.OrderHeader.CustomerAccountNo.Value
            qod.CustomerName = orderDetail.OrderHeader.CustomerName.Value
            qod.DateOrder = orderDetail.OrderHeader.SaleDate.Value
            qod.DateDelivery = orderDetail.OrderHeader.RequiredDeliveryDate.Value
            qod.IsForDelivery = orderDetail.OrderHeader.ToBeDelivered.Value
            qod.DeliveryAddress1 = orderDetail.OrderHeader.DeliveryAddressLine1.Value
            qod.DeliveryAddress2 = orderDetail.OrderHeader.DeliveryAddressLine2.Value
            qod.DeliveryAddress3 = orderDetail.OrderHeader.DeliveryAddressTown.Value
            qod.DeliveryAddress4 = orderDetail.OrderHeader.DeliveryAddressLine4.Value
            qod.DeliveryPostCode = orderDetail.OrderHeader.DeliveryPostcode.Value
            qod.PhoneNumber = orderDetail.OrderHeader.ContactPhoneHome.Value
            qod.MerchandiseValue = orderDetail.OrderHeader.TotalOrderValue.Value
            qod.DeliveryCharge = orderDetail.OrderHeader.DeliveryCharge.Value
            qod.TranDate = orderDetail.OrderHeader.SaleDate.Value
            qod.PhoneNumberMobile = orderDetail.OrderHeader.ContactPhoneMobile.Value
            qod.CustomerName = orderDetail.OrderHeader.CustomerName.Value

            qod.DeliveryStatus = CInt(orderDetail.OrderHeader.OrderStatus.Value)
            qod.SellingStoreId = CInt(orderDetail.OrderHeader.SellingStoreCode.Value)
            qod.SellingStoreOrderId = CInt(orderDetail.OrderHeader.SellingStoreOrderNumber.Value)
            qod.CustomerAddress1 = orderDetail.OrderHeader.CustomerAddressLine1.Value
            qod.CustomerAddress2 = orderDetail.OrderHeader.CustomerAddressLine2.Value
            qod.CustomerAddress3 = orderDetail.OrderHeader.CustomerAddressTown.Value
            qod.CustomerAddress4 = orderDetail.OrderHeader.CustomerAddressLine4.Value
            qod.CustomerPostcode = orderDetail.OrderHeader.CustomerPostcode.Value
            qod.CustomerEmail = orderDetail.OrderHeader.ContactEmail.Value
            qod.PhoneNumberWork = orderDetail.OrderHeader.ContactPhoneWork.Value
            qod.OmOrderNumber = CInt(orderDetail.OrderHeader.OMOrderNumber.Value)

            If orderDetail.OrderLines IsNot Nothing Then
                For Each sentline As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderLine In orderDetail.OrderLines
                    Dim line As New QodLine(qod, sentline)
                    qod.Lines.Add(line)
                Next
            End If

            If orderDetail.OrderHeader.DeliveryInstructions.InstructionLine IsNot Nothing Then
                For Each sentInstruct As OMSendFullStatusUpdateResponseOrderStatusOrderDetailOrderHeaderDeliveryInstructionsInstructionLine In orderDetail.OrderHeader.DeliveryInstructions.InstructionLine
                    Dim txt As New QodText(qod, sentInstruct)
                    qod.Texts.Add(txt)
                Next
            End If
            Return qod

        End Function

        Public Function CreateFromCtsFulfillmentResponse(ByVal response As CTSFulfilmentResponse) As QodHeader
            Dim qod As New QodHeader

            qod.VendaNumber = response.OrderHeader.CustomerAccountNo.Value
            qod.CustomerName = response.OrderHeader.CustomerName.Value
            qod.DateOrder = response.OrderHeader.SaleDate.Value
            qod.DateDelivery = response.OrderHeader.RequiredDeliveryDate.Value
            qod.IsForDelivery = response.OrderHeader.ToBeDelivered.Value
            qod.DeliveryAddress1 = response.OrderHeader.DeliveryAddressLine1.Value
            qod.DeliveryAddress2 = response.OrderHeader.DeliveryAddressLine2.Value
            qod.DeliveryAddress3 = response.OrderHeader.DeliveryAddressTown.Value
            qod.DeliveryAddress4 = response.OrderHeader.DeliveryAddressLine4.Value
            qod.DeliveryPostCode = response.OrderHeader.DeliveryPostcode.Value
            qod.PhoneNumber = response.OrderHeader.ContactPhoneHome.Value
            qod.MerchandiseValue = response.OrderHeader.TotalOrderValue.Value
            qod.DeliveryCharge = response.OrderHeader.DeliveryCharge.Value
            qod.TranDate = response.OrderHeader.SaleDate.Value
            qod.PhoneNumberMobile = response.OrderHeader.ContactPhoneMobile.Value
            qod.CustomerName = response.OrderHeader.CustomerName.Value

            qod.DeliveryStatus = CInt(response.OrderHeader.OrderStatus.Value)
            qod.SellingStoreId = CInt(response.OrderHeader.SellingStoreCode.Value)
            qod.SellingStoreOrderId = CInt(response.OrderHeader.SellingStoreOrderNumber.Value)
            qod.CustomerAddress1 = response.OrderHeader.CustomerAddressLine1.Value
            qod.CustomerAddress2 = response.OrderHeader.CustomerAddressLine2.Value
            qod.CustomerAddress3 = response.OrderHeader.CustomerAddressTown.Value
            qod.CustomerAddress4 = response.OrderHeader.CustomerAddressLine4.Value
            qod.CustomerPostcode = response.OrderHeader.CustomerPostcode.Value
            qod.CustomerEmail = response.OrderHeader.ContactEmail.Value
            qod.PhoneNumberWork = response.OrderHeader.ContactPhoneWork.Value
            qod.OmOrderNumber = CInt(response.OrderHeader.OMOrderNumber.Value)

            'venda specific data
            With qod
                If response.OrderHeader.Source IsNot Nothing Then .Source = response.OrderHeader.Source.Value
                If response.OrderHeader.SourceOrderNumber IsNot Nothing Then .SourceOrderNumber = response.OrderHeader.SourceOrderNumber.Value
                If response.OrderHeader.SellingStoreTill IsNot Nothing Then .TranTill = response.OrderHeader.SellingStoreTill.Value
                If response.OrderHeader.SellingStoreTransaction IsNot Nothing Then .TranNumber = response.OrderHeader.SellingStoreTransaction.Value
                If response.OrderHeader.ExtendedLeadTime IsNot Nothing AndAlso response.OrderHeader.Source.Value = "WO" Then .ExtendedLeadTime = response.OrderHeader.ExtendedLeadTime.Value
                If response.OrderHeader.DeliveryContactName IsNot Nothing Then .DeliveryContactName = response.OrderHeader.DeliveryContactName.Value
                If response.OrderHeader.DeliveryContactPhone IsNot Nothing Then .DeliveryContactPhone = response.OrderHeader.DeliveryContactPhone.Value
            End With

            Return qod
        End Function

        Public Function CreateFromOmOrderCreateResponse(ByVal response As OMOrderCreateResponse) As QodHeader
            Dim qod As New QodHeader
            qod.VendaNumber = response.OrderHeader.CustomerAccountNo.Value
            qod.CustomerName = response.OrderHeader.CustomerName.Value
            qod.DateOrder = response.OrderHeader.SaleDate.Value
            qod.DateDelivery = response.OrderHeader.RequiredDeliveryDate.Value
            qod.IsForDelivery = response.OrderHeader.ToBeDelivered.Value
            qod.DeliveryAddress1 = response.OrderHeader.DeliveryAddressLine1.Value
            qod.DeliveryAddress2 = response.OrderHeader.DeliveryAddressLine2.Value
            qod.DeliveryAddress3 = response.OrderHeader.DeliveryAddressTown.Value
            qod.DeliveryAddress4 = response.OrderHeader.DeliveryAddressLine4.Value
            qod.DeliveryPostCode = response.OrderHeader.DeliveryPostcode.Value
            qod.PhoneNumber = response.OrderHeader.ContactPhoneHome.Value
            qod.MerchandiseValue = response.OrderHeader.TotalOrderValue.Value
            qod.DeliveryCharge = response.OrderHeader.DeliveryCharge.Value
            qod.TranDate = response.OrderHeader.SaleDate.Value
            qod.PhoneNumberMobile = response.OrderHeader.ContactPhoneMobile.Value
            qod.CustomerName = response.OrderHeader.CustomerName.Value

            qod.SellingStoreId = CInt(response.OrderHeader.SellingStoreCode.Value)
            qod.SellingStoreOrderId = CInt(response.OrderHeader.SellingStoreOrderNumber.Value)
            qod.CustomerAddress1 = response.OrderHeader.CustomerAddressLine1.Value
            qod.CustomerAddress2 = response.OrderHeader.CustomerAddressLine2.Value
            qod.CustomerAddress3 = response.OrderHeader.CustomerAddressTown.Value
            qod.CustomerAddress4 = response.OrderHeader.CustomerAddressLine4.Value
            qod.CustomerPostcode = response.OrderHeader.CustomerPostcode.Value
            qod.CustomerEmail = response.OrderHeader.ContactEmail.Value
            qod.PhoneNumberWork = response.OrderHeader.ContactPhoneWork.Value
            qod.OmOrderNumber = CInt(response.OrderHeader.OMOrderNumber.Value)

            If response.OrderLines IsNot Nothing Then
                For Each sentline As OMOrderCreateResponseOrderLine In response.OrderLines
                    Dim line As New QodLine(qod, sentline)
                    qod.Lines.Add(line)
                Next
            End If

            If response.OrderHeader.DeliveryInstructions.InstructionLine IsNot Nothing Then
                For Each sentInstruct As OMOrderCreateResponseOrderHeaderDeliveryInstructionsInstructionLine In response.OrderHeader.DeliveryInstructions.InstructionLine
                    Dim txt As New QodText(qod, sentInstruct)
                    qod.Texts.Add(txt)
                Next
            End If

            Return qod
        End Function

        Public Function CreateFromCtsQodCreateResponse(ByVal response As CTSQODCreateResponse) As QodHeader
            Dim qod As New QodHeader
            qod.VendaNumber = response.OrderHeader.Source.Value & response.OrderHeader.SourceOrderNumber.Value
            qod.CustomerName = response.OrderHeader.CustomerName.Value
            qod.DateOrder = response.OrderHeader.SaleDate.Value
            qod.DateDelivery = response.OrderHeader.RequiredDeliveryDate.Value
            qod.IsForDelivery = response.OrderHeader.ToBeDelivered.Value
            qod.DeliveryAddress1 = response.OrderHeader.DeliveryAddressLine1.Value
            qod.DeliveryAddress2 = response.OrderHeader.DeliveryAddressLine2.Value
            qod.DeliveryAddress3 = response.OrderHeader.DeliveryAddressTown.Value
            qod.DeliveryAddress4 = response.OrderHeader.DeliveryAddressLine4.Value
            qod.DeliveryPostCode = response.OrderHeader.DeliveryPostcode.Value
            qod.PhoneNumber = response.OrderHeader.ContactPhoneHome.Value
            qod.MerchandiseValue = response.OrderHeader.TotalOrderValue.Value
            qod.DeliveryCharge = response.OrderHeader.DeliveryCharge.Value
            qod.TranDate = response.OrderHeader.SaleDate.Value
            qod.PhoneNumberMobile = response.OrderHeader.ContactPhoneMobile.Value
            qod.CustomerName = response.OrderHeader.CustomerName.Value

            qod.SellingStoreId = ThisStore.Id4
            qod.SellingStoreOrderId = 0
            qod.CustomerAddress1 = response.OrderHeader.CustomerAddressLine1.Value
            qod.CustomerAddress2 = response.OrderHeader.CustomerAddressLine2.Value
            qod.CustomerAddress3 = response.OrderHeader.CustomerAddressTown.Value
            qod.CustomerAddress4 = response.OrderHeader.CustomerAddressLine4.Value
            qod.CustomerPostcode = response.OrderHeader.CustomerPostcode.Value
            qod.CustomerEmail = response.OrderHeader.ContactEmail.Value
            qod.PhoneNumberWork = response.OrderHeader.ContactPhoneWork.Value

            If response.OrderLines IsNot Nothing Then
                For Each sentline As CTSQODCreateResponseOrderLine In response.OrderLines
                    Dim line As New QodLine(qod, sentline)
                    qod.Lines.Add(line)
                Next
            End If

            'If response.OrderHeader.DeliveryInstructions.InstructionLine IsNot Nothing Then
            '    For Each sentInstruct As CTSQODCreateResponseOrderHeaderDeliveryInstructionsInstructionLine In response.OrderHeader.DeliveryInstructions.InstructionLine
            '        Dim txt As New QodText(qod, sentInstruct)
            '        qod.Texts.Add(txt)
            '    Next
            'End If

            Return qod
        End Function

        Public Function GetConfigXML() As XmlDocument
            Dim Doc As XmlDocument = New XmlDocument()
            Doc.Load(".\Resources\Config.xml")
            Return Doc
        End Function

        Public Function VendaOrderNoInUse(ByVal strVendaOrderNo As String) As Boolean

            VendaOrderNoInUse = True

            Using con As New Connection
                Using com As New Command(con)
                    Select Case con.DataProvider
                        Case DataProvider.Odbc
                            Dim DT As DataTable

                            com.CommandText = "select count(*) as RecordCount from DLTOTS where TCOD = 'SA' and DOCN = ? "
                            com.AddParameter(My.Resources.Parameters.DocumentNumber, strVendaOrderNo)

                            DT = com.ExecuteDataTable()   'will return only one record
                            If CType(DT.Rows(0).Item("RecordCount"), Integer) = 0 Then VendaOrderNoInUse = False

                        Case DataProvider.Sql
                            com.StoredProcedureName = My.Resources.Procedures.VendaOrderGet
                            com.AddParameter(My.Resources.Parameters.DocumentNumber, strVendaOrderNo)
                            If (CType(com.ExecuteDataTable().Rows(0).Item("RecordCount"), Integer) = 0) Then
                                VendaOrderNoInUse = False
                            End If
                    End Select
                End Using
            End Using

        End Function

    End Module

    Namespace State

        Public Enum Refund
            None = 0
            RefundFailedConnection = 10
            RefundFailedState = 20
            RefundFailedData = 30
            RefundOk = 99

            Created = 100
            CreatedOnHold = 105
            NotifyFailedConnection = 110
            NotifyFailedState = 120
            NotifyFailedData = 130
            NotifyOk = 150
            StatusUpdateFailedConnection = 160
            StatusUpdateFailedState = 170
            StatusUpdateFailedData = 180
            StatusUpdateOk = 199
        End Enum

        Public Enum Delivery

            NULL = -1

            OrderCreated = 0
            OrderFailedConnection = 10
            OrderFailedState = 20
            OrderFailedData = 30
            OrderOk = 99

            DeliveryRequest = 100
            DeliveryRequestFailedConnection = 110
            DeliveryRequestFailedState = 120
            DeliveryRequestFailedData = 130

            DeliveryRequestAccepted = 200
            FulfilRequestFailedConnection = 210
            FulfilRequestFailedState = 220
            FulfilRequestFailedData = 230

            IbtOutAllStock = 300
            IbtOutFailedConnection = 310
            IbtOutFailedState = 320
            IbtOutFailedData = 330
            IbtInAllStock = 399

            ReceiptNotifyCreate = 400
            ReceiptNotifyFailedConnection = 410
            ReceiptNotifyFailedState = 420
            ReceiptNotifyFailedData = 430
            ReceiptNotifyRecorded = 450
            ReceiptStatusFailedConnection = 460
            ReceiptStatusFailedState = 470
            ReceiptStatusFailedData = 480
            ReceiptStatusOk = 499

            PickingListCreated = 500
            PickingNotifyFailedConnection = 510
            PickingNotifyFailedState = 520
            PickingNotifyFailedData = 530
            PickingNotifyOk = 550
            PickingStatusFailedConnection = 560
            PickingStatusFailedState = 570
            PickingStatusFailedData = 580
            PickingStatusOk = 599

            DespatchCreated = 700
            DespatchNotifyFailedConnection = 710
            DespatchNotifyFailedState = 720
            DespatchNotifyFailedData = 730
            DespatchNotifyOk = 750
            DespatchStatusFailedConnection = 760
            DespatchStatusFailedState = 770
            DespatchStatusFailedData = 780
            DespatchStatusOk = 799

            UndeliveredCreated = 800
            UndeliveredNotifyFailedConnection = 810
            UndeliveredNotifyFailedState = 820
            UndeliveredNotifyFailedData = 830
            UndeliveredNotifyOk = 850
            UndeliveredStatusFailedConnection = 860
            UndeliveredStatusFailedState = 870
            UndeliveredStatusFailedData = 880
            UndeliveredStatusOk = 899

            DeliveredCreated = 900
            DeliveredNotifyFailedConnection = 910
            DeliveredNotifyFailedState = 920
            DeliveredNotifyFailedData = 930
            DeliveredNotifyOk = 950
            DeliveredStatusFailedConnection = 960
            DeliveredStatusFailedState = 970
            DeliveredStatusFailedData = 980
            DeliveredStatusOk = 999

            Legacy = 9999
        End Enum

        Public Structure DeliveryDescription
            Private _value As String
            Public Shared Phase1 As String = My.Resources.DeliveryState.Phase1
            Public Shared Phase2 As String = My.Resources.DeliveryState.Phase2
            Public Shared Phase3 As String = My.Resources.DeliveryState.Phase3
            Public Shared Phase4 As String = My.Resources.DeliveryState.Phase4
            Public Shared Phase5 As String = My.Resources.DeliveryState.Phase5
            Public Shared Phase6 As String = My.Resources.DeliveryState.Phase6
            Public Shared Phase7 As String = My.Resources.DeliveryState.Phase7
            Public Shared Phase8 As String = My.Resources.DeliveryState.Phase8


            Public Shared Function GetDescription(ByVal deliveryState As Delivery) As String

                Select Case deliveryState
                    Case Delivery.OrderCreated : Return My.Resources.DeliveryState.State000
                    Case Delivery.OrderFailedConnection : Return My.Resources.DeliveryState.State010
                    Case Delivery.OrderFailedState : Return My.Resources.DeliveryState.State020
                    Case Delivery.OrderFailedConnection : Return My.Resources.DeliveryState.State030

                    Case Delivery.DeliveryRequest : Return My.Resources.DeliveryState.State100
                    Case Delivery.DeliveryRequestFailedConnection : Return My.Resources.DeliveryState.State110
                    Case Delivery.DeliveryRequestFailedState : Return My.Resources.DeliveryState.State120
                    Case Delivery.DeliveryRequestFailedData : Return My.Resources.DeliveryState.State130

                    Case Delivery.DeliveryRequestAccepted : Return My.Resources.DeliveryState.State200
                    Case Delivery.FulfilRequestFailedConnection : Return My.Resources.DeliveryState.State210
                    Case Delivery.FulfilRequestFailedState : Return My.Resources.DeliveryState.State220
                    Case Delivery.FulfilRequestFailedData : Return My.Resources.DeliveryState.State230

                    Case Delivery.IbtOutAllStock : Return My.Resources.DeliveryState.State300
                    Case Delivery.IbtOutFailedConnection : Return My.Resources.DeliveryState.State310
                    Case Delivery.IbtOutFailedState : Return My.Resources.DeliveryState.State320
                    Case Delivery.IbtOutFailedData : Return My.Resources.DeliveryState.State330
                    Case Delivery.IbtInAllStock : Return My.Resources.DeliveryState.State399

                    Case Delivery.ReceiptNotifyCreate : Return My.Resources.DeliveryState.State400
                    Case Delivery.ReceiptNotifyFailedConnection : Return My.Resources.DeliveryState.State410
                    Case Delivery.ReceiptNotifyFailedState : Return My.Resources.DeliveryState.State420
                    Case Delivery.ReceiptNotifyFailedData : Return My.Resources.DeliveryState.State430
                    Case Delivery.ReceiptNotifyRecorded : Return My.Resources.DeliveryState.State450
                    Case Delivery.ReceiptStatusFailedConnection : Return My.Resources.DeliveryState.State460
                    Case Delivery.ReceiptStatusFailedState : Return My.Resources.DeliveryState.State470
                    Case Delivery.ReceiptStatusFailedData : Return My.Resources.DeliveryState.State480
                    Case Delivery.ReceiptStatusOk : Return My.Resources.DeliveryState.State499

                    Case Delivery.PickingListCreated : Return My.Resources.DeliveryState.State500
                    Case Delivery.PickingNotifyFailedConnection : Return My.Resources.DeliveryState.State510
                    Case Delivery.PickingNotifyFailedState : Return My.Resources.DeliveryState.State520
                    Case Delivery.PickingNotifyFailedData : Return My.Resources.DeliveryState.State530
                    Case Delivery.PickingNotifyOk : Return My.Resources.DeliveryState.State550
                    Case Delivery.PickingStatusFailedConnection : Return My.Resources.DeliveryState.State560
                    Case Delivery.PickingStatusFailedState : Return My.Resources.DeliveryState.State570
                    Case Delivery.PickingStatusFailedData : Return My.Resources.DeliveryState.State580
                    Case Delivery.PickingStatusOk : Return My.Resources.DeliveryState.State599

                    Case Delivery.DespatchCreated : Return My.Resources.DeliveryState.State700
                    Case Delivery.DespatchNotifyFailedConnection : Return My.Resources.DeliveryState.State710
                    Case Delivery.DespatchNotifyFailedState : Return My.Resources.DeliveryState.State720
                    Case Delivery.DespatchNotifyFailedData : Return My.Resources.DeliveryState.State730
                    Case Delivery.DespatchNotifyOk : Return My.Resources.DeliveryState.State750
                    Case Delivery.DespatchStatusFailedConnection : Return My.Resources.DeliveryState.State760
                    Case Delivery.DespatchStatusFailedState : Return My.Resources.DeliveryState.State770
                    Case Delivery.DespatchStatusFailedData : Return My.Resources.DeliveryState.State780
                    Case Delivery.DespatchStatusOk : Return My.Resources.DeliveryState.State799

                    Case Delivery.UndeliveredCreated : Return My.Resources.DeliveryState.State800
                    Case Delivery.UndeliveredNotifyFailedConnection : Return My.Resources.DeliveryState.State810
                    Case Delivery.UndeliveredNotifyFailedState : Return My.Resources.DeliveryState.State820
                    Case Delivery.UndeliveredNotifyFailedData : Return My.Resources.DeliveryState.State830
                    Case Delivery.UndeliveredNotifyOk : Return My.Resources.DeliveryState.State850
                    Case Delivery.UndeliveredStatusFailedConnection : Return My.Resources.DeliveryState.State860
                    Case Delivery.UndeliveredStatusFailedState : Return My.Resources.DeliveryState.State870
                    Case Delivery.UndeliveredStatusFailedData : Return My.Resources.DeliveryState.State880
                    Case Delivery.UndeliveredStatusOk : Return My.Resources.DeliveryState.State899

                    Case Delivery.DeliveredCreated : Return My.Resources.DeliveryState.State900
                    Case Delivery.DeliveredNotifyFailedConnection : Return My.Resources.DeliveryState.State910
                    Case Delivery.DeliveredNotifyFailedState : Return My.Resources.DeliveryState.State920
                    Case Delivery.DeliveredNotifyFailedData : Return My.Resources.DeliveryState.State930
                    Case Delivery.DeliveredNotifyOk : Return My.Resources.DeliveryState.State950
                    Case Delivery.DeliveredStatusFailedConnection : Return My.Resources.DeliveryState.State960
                    Case Delivery.DeliveredStatusFailedState : Return My.Resources.DeliveryState.State970
                    Case Delivery.DeliveredStatusFailedData : Return My.Resources.DeliveryState.State980
                    Case Delivery.DeliveredStatusOk : Return My.Resources.DeliveryState.State999

                    Case Else : Return CapitalSpaceInsert(deliveryState.ToString).ToString
                End Select

            End Function

            Public Shared Function GetPhase(ByVal deliveryState As Delivery) As String
                Select Case deliveryState
                    Case Is < Delivery.DeliveryRequestAccepted : Return My.Resources.DeliveryState.Phase1
                    Case Is < Delivery.IbtOutAllStock : Return My.Resources.DeliveryState.Phase2
                    Case Is < Delivery.ReceiptNotifyCreate : Return My.Resources.DeliveryState.Phase3
                    Case Is < Delivery.PickingListCreated : Return My.Resources.DeliveryState.Phase4
                    Case Is < Delivery.DespatchCreated : Return My.Resources.DeliveryState.Phase5
                    Case Is < Delivery.UndeliveredCreated : Return My.Resources.DeliveryState.Phase6
                    Case Is < Delivery.DeliveredCreated : Return My.Resources.DeliveryState.Phase7
                    Case Is < Delivery.Legacy : Return My.Resources.DeliveryState.Phase8
                    Case Else : Return String.Empty
                End Select
            End Function

        End Structure

    End Namespace

    Namespace DataAccess

        <HideModuleName()> Friend Module DataAccess

            Private Function SaleOrderGetOdbcCommandText() As String
                Dim sb As New StringBuilder
                sb.Append("SELECT ")
                sb.Append("ch.NUMB		as Number,")
                sb.Append("ch.CUST		as CustomerNumber ,")
                sb.Append("ch.NAME		as CustomerName ,")
                sb.Append("ch.DATE1		as DateOrder ,")
                sb.Append("ch.DELD		as DateDelivery ,")
                sb.Append("ch.CANC		as Status,")
                sb.Append("ch.DELI		as IsForDelivery ,")
                sb.Append("ch.DELC		as DeliveryConfirmed ,")
                sb.Append("ch.AMDT		as TimesAmended ,")
                sb.Append("ch.ADDR1		as DeliveryAddress1 ,")
                sb.Append("ch.ADDR2		as DeliveryAddress2 ,")
                sb.Append("ch.ADDR3		as DeliveryAddress3 ,")
                sb.Append("ch.ADDR4		as DeliveryAddress4 ,")
                sb.Append("ch.POST		as DeliveryPostCode ,")
                sb.Append("ch.PHON		as PhoneNumber ,")
                sb.Append("ch.MOBP		as PhoneNumberMobile ,")
                sb.Append("ch.PRNT		as IsPrinted ,")
                sb.Append("ch.RPRN		as NumberReprints ,")
                sb.Append("ch.REVI		as RevisionNumber ,")
                sb.Append("ch.MVST		as MerchandiseValue ,")
                sb.Append("ch.DCST		as DeliveryCharge ,")
                sb.Append("ch.QTYO		as QtyOrdered ,")
                sb.Append("ch.QTYT		as QtyTaken ,")
                sb.Append("ch.QTYR		as QtyRefunded ,")
                sb.Append("ch.WGHT		as OrderWeight ,")
                sb.Append("ch.VOLU		as OrderVolume ,")
                sb.Append("ch.SDAT		as TranDate ,")
                sb.Append("ch.STIL		as TranTill ,")
                sb.Append("ch.STRN		as TranNumber ,")
                sb.Append("ch.RDAT		as RefundTranDate ,")
                sb.Append("ch.RTIL		as RefundTill ,")
                sb.Append("ch.RTRN		as RefundTranNumber,")

                sb.Append("ch4.DDAT		as DateDespatch,")
                sb.Append("ch4.OEID		as OrderTakerId,")
                sb.Append("ch4.FDID		as ManagerId,")
                sb.Append("ch4.DeliveryStatus, ")
                sb.Append("ch4.RefundStatus, ")
                sb.Append("ch4.SellingStoreId, ")
                sb.Append("ch4.SellingStoreOrderId, ")
                sb.Append("ch4.OMOrderNumber, ")
                sb.Append("ch4.CustomerAddress1, ")
                sb.Append("ch4.CustomerAddress2, ")
                sb.Append("ch4.CustomerAddress3, ")
                sb.Append("ch4.CustomerAddress4, ")
                sb.Append("ch4.CustomerPostcode, ")
                sb.Append("ch4.CustomerEmail, ")
                sb.Append("ch4.PhoneNumberWork, ")
                sb.Append("ch4.IsSuspended, ")

                sb.Append("ch5.Source, ")
                sb.Append("ch5.SourceOrderNumber, ")
                sb.Append("ch5.ExtendedLeadTime, ")
                sb.Append("ch5.DeliveryContactName, ")
                sb.Append("ch5.DeliveryContactPhone, ")
                sb.Append("ch5.CustomerAccountNo ")

                sb.Append("FROM CORHDR ch ")
                sb.Append("INNER JOIN CORHDR4 ch4 ")
                sb.Append("      on ch.NUMB = ch4.NUMB ")
                'venda sale will have an entry in corhdr5 only!
                sb.Append("LEFT OUTER JOIN CORHDR5 ch5 ")
                sb.Append("      on ch5.NUMB = ch4.NUMB ")

                Return sb.ToString
            End Function

            Friend Function GetHeaderForQodNumber(ByVal orderNumber As String) As DataTable

                Using con As New Connection
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder(SaleOrderGetOdbcCommandText)
                                sb.Append("WHERE ch.NUMB = ?")
                                com.CommandText = sb.ToString
                                com.AddParameter("OrderNumber", orderNumber.PadLeft(6, "0"c))
                                Return com.ExecuteDataTable

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleOrderGet
                                com.AddParameter(My.Resources.Parameters.OrderNumber, orderNumber.PadLeft(6, "0"c))
                                Return com.ExecuteDataTable
                        End Select
                    End Using
                End Using
                Return Nothing

            End Function

            Friend Function GetHeaderForOmOrderNumber(ByVal omOrderNumber As Integer) As DataTable

                Using con As New Connection
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder(SaleOrderGetOdbcCommandText)
                                sb.Append("WHERE ch4.OMOrderNumber = ?")
                                com.CommandText = sb.ToString
                                com.AddParameter("OrderNumber", omOrderNumber)
                                Return com.ExecuteDataTable

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleOrderGet
                                com.AddParameter(My.Resources.Parameters.OmOrderNumber, omOrderNumber)
                                Return com.ExecuteDataTable
                        End Select
                    End Using
                End Using
                Return Nothing

            End Function

            Friend Function GetHeaderForVendaNumber(ByVal vendaNumber As String) As DataTable

                Using con As New Connection
                    Using com As Command = con.NewCommand
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder(SaleOrderGetOdbcCommandText)
                                sb.Append("WHERE ch4.OMOrderNumber = ?")
                                com.CommandText = sb.ToString
                                com.AddParameter("VendaNumber", vendaNumber)
                                Return com.ExecuteDataTable

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleOrderGet
                                com.AddParameter(My.Resources.Parameters.VendaNumber, vendaNumber)
                                Return com.ExecuteDataTable
                        End Select
                    End Using
                End Using
                Return Nothing

            End Function

            Friend Function GetHeadersForStateRange(ByVal minState As Delivery, ByVal maxState As Delivery, ByVal includeSuspended As Boolean) As DataTable

                Using con As New Connection
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder(SaleOrderGetOdbcCommandText)
                                If includeSuspended Then
                                    sb.Append("WHERE ch4.DeliveryStatus>=? and ch4.DeliveryStatus<=?")
                                Else
                                    sb.Append("WHERE ch4.DeliveryStatus>=? and ch4.DeliveryStatus<=? AND ch4.IsSuspended = 0")
                                End If

                                com.CommandText = sb.ToString
                                com.AddParameter("Min", CInt(minState))
                                com.AddParameter("Max", CInt(maxState))
                                Return com.ExecuteDataTable

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleOrderGet
                                com.AddParameter(My.Resources.Parameters.DeliveryStatusMin, CInt(minState))
                                com.AddParameter(My.Resources.Parameters.DeliveryStatusMax, CInt(maxState))
                                Return com.ExecuteDataTable

                        End Select

                    End Using
                End Using
                Return Nothing

            End Function

            Friend Function GetHeadersForDateRange(ByVal dateStart As Date, ByVal dateEnd As Date) As DataTable

                Using con As New Connection
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder(SaleOrderGetOdbcCommandText)
                                sb.Append("WHERE ch.DATE1>=? and ch.DATE1<=?")

                                com.CommandText = sb.ToString
                                com.AddParameter("start", dateStart)
                                com.AddParameter("end", dateEnd)
                                Return com.ExecuteDataTable

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleOrderGet
                                com.AddParameter(My.Resources.Parameters.DateStart, dateStart)
                                com.AddParameter(My.Resources.Parameters.DateEnd, dateEnd)
                                Return com.ExecuteDataTable

                        End Select

                    End Using
                End Using

                Return Nothing

            End Function

            Friend Function GetNonZeroStockOrOutstandingIbt() As DataSet

                Using con As New Connection
                    Using com As Command = con.NewCommand()
                        Dim ds As New DataSet

                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                com.CommandText = "select * from vwQodNonZeroStock where not (QtyOnHand=0) "
                                Dim dt1 As DataTable = com.ExecuteDataTable

                                Dim sb As New StringBuilder("Select ")
                                sb.Append("cl.SKUN SkuNumber, ")
                                sb.Append("cl.NUMB OrderNumber, ")
                                sb.Append("cl.LINE Number, ")
                                sb.Append("ch4.OMOrderNumber, ")
                                sb.Append("ch5.SourceOrderNumber VendaNumber, ")
                                sb.Append("cl.QTYO QtyOrdered, ")
                                sb.Append("cl.QTYR QtyReturned, ")
                                sb.Append("cl.QTYT QtyTaken, ")
                                sb.Append("round(cl.QTYO-cl.QTYR-cl.QTYT,0) QtyOutstanding, ")
                                sb.Append("cl.DeliveryStatus ")
                                sb.Append("from corlin cl ")
                                sb.Append("left join CORHDR4 ch4 on cl.NUMB=ch4.NUMB ")
                                sb.Append("left join CORHDR5 ch5 on ch5.NUMB=cl.NUMB ")
                                sb.Append("where cl.DeliveryStatus<399 and cl.QTYO-cl.QTYR-cl.QTYT <>0 ")
                                sb.Append("Order by cl.SKUN")

                                com.CommandText = sb.ToString
                                Dim dt2 As DataTable = com.ExecuteDataTable

                                ds.Tables.Add(dt1)
                                ds.Tables.Add(dt2)

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.QodGetNonZeroStockOrOutstandingIbt
                                ds = com.ExecuteDataSet

                        End Select
                        ds.EnforceConstraints = False 'Ref:815 Store 120 issue - If a CORHDR Sku record does not exist in STKMAS then we will get a Constraint Error this line stops this issue.
                        ds.Relations.Add("relation", ds.Tables(0).Columns("SkuNumber"), ds.Tables(1).Columns("SkuNumber"))
                        Return ds
                    End Using
                End Using

                Return Nothing

            End Function

            Friend Function GetRefundsForStateRange(ByVal minState As Refund, ByVal maxState As Refund) As DataTable

                Using con As New Connection
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder(SaleOrderGetOdbcCommandText)
                                sb.Append("WHERE ch4.RefundStatus>=? and ch4.RefundStatus<=? and ch4.DeliveryStatus < 9999")

                                com.CommandText = sb.ToString
                                com.AddParameter("Min", CInt(minState))
                                com.AddParameter("Max", CInt(maxState))
                                Return com.ExecuteDataTable

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleOrderGet
                                com.AddParameter(My.Resources.Parameters.RefundStatusMin, CInt(minState))
                                com.AddParameter(My.Resources.Parameters.RefundStatusMax, CInt(maxState))
                                Return com.ExecuteDataTable

                        End Select

                    End Using
                End Using
                Return Nothing

            End Function

            Friend Function GetRefundForQodNumber(ByVal qodNumber As Integer) As DataTable

                Using con As New Connection
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder
                                sb.Append("SELECT ")
                                sb.Append("cr.NUMB	as OrderNumber,")
                                sb.Append("cr.LINE	as Number,")
                                sb.Append("cl.SKUN  as SkuNumber, ")
                                sb.Append("st.DESCR	as SkuDescription,")
                                sb.Append("st.BUYU	as SkuUnitMeasure,")
                                sb.Append("cr.RefundStoreId,")
                                sb.Append("cr.RefundDate,")
                                sb.Append("cr.RefundTill,")
                                sb.Append("cr.RefundTransaction,")
                                sb.Append("cr.QtyReturned,")
                                sb.Append("cr.QtyCancelled,")
                                sb.Append("st.ONHA	as QtyOnHand,")
                                sb.Append("st.ONOR	as QtyOnOrder,")
                                sb.Append("cr.RefundStatus,")
                                sb.Append("cr.SellingStoreIbtOut, ")
                                sb.Append("cl.DeliverySource, ")
                                sb.Append("cl.Price ")
                                sb.Append("FROM CORREFUND cr ")
                                sb.Append("INNER JOIN CORLIN cl on cr.NUMB=cl.NUMB and cr.LINE=cl.LINE ")
                                sb.Append("INNER JOIN STKMAS st ON st.SKUN=cl.SKUN ")
                                sb.Append("WHERE cr.NUMB=?")
                                sb.Append("ORDER BY cr.RefundStoreId, cr.RefundTill, cr.RefundTransaction, cr.LINE")
                                com.CommandText = sb.ToString
                                Dim qodNo As String = qodNumber.ToString("000000")
                                com.AddParameter("OrderNumber", qodNo)
                                Return com.ExecuteDataTable

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleOrderRefundGet
                                Dim qodNo As String = qodNumber.ToString("000000")
                                com.AddParameter(My.Resources.Parameters.OrderNumber, qodNo)
                                Return com.ExecuteDataTable
                        End Select
                    End Using
                End Using
                Return Nothing

            End Function

            ''' <summary>
            ''' Overload function just to get Refunds where the refund status is not complete less than 150
            ''' </summary>
            ''' <param name="qodNumber"></param>
            ''' <param name="RefundStatus"></param>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Friend Function GetRefundForQodNumber(ByVal qodNumber As Integer, ByVal RefundStatus As Integer) As DataTable

                Using con As New Connection
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder
                                sb.Append("SELECT ")
                                sb.Append("cr.NUMB	as OrderNumber,")
                                sb.Append("cr.LINE	as Number,")
                                sb.Append("cl.SKUN  as SkuNumber, ")
                                sb.Append("st.DESCR	as SkuDescription,")
                                sb.Append("st.BUYU	as SkuUnitMeasure,")
                                sb.Append("cr.RefundStoreId,")
                                sb.Append("cr.RefundDate,")
                                sb.Append("cr.RefundTill,")
                                sb.Append("cr.RefundTransaction,")
                                sb.Append("cr.QtyReturned,")
                                sb.Append("cr.QtyCancelled,")
                                sb.Append("st.ONHA	as QtyOnHand,")
                                sb.Append("st.ONOR	as QtyOnOrder,")
                                sb.Append("cr.RefundStatus,")
                                sb.Append("cr.SellingStoreIbtOut, ")
                                sb.Append("cl.DeliverySource, ")
                                sb.Append("cl.Price ")
                                sb.Append("FROM CORREFUND cr ")
                                sb.Append("INNER JOIN CORLIN cl on cr.NUMB=cl.NUMB and cr.LINE=cl.LINE ")
                                sb.Append("INNER JOIN STKMAS st ON st.SKUN=cl.SKUN ")
                                sb.Append("WHERE cr.NUMB=? and cr.RefundStatus = ?")
                                sb.Append("ORDER BY cr.RefundStoreId, cr.RefundTill, cr.RefundTransaction, cr.LINE")
                                com.CommandText = sb.ToString
                                Dim qodNo As String = qodNumber.ToString("000000")
                                com.AddParameter("OrderNumber", qodNo)
                                com.AddParameter("RefundStatus", RefundStatus)
                                Return com.ExecuteDataTable

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleOrderRefundGet
                                Dim qodNo As String = qodNumber.ToString("000000")
                                com.AddParameter(My.Resources.Parameters.OrderNumber, qodNo)
                                com.AddParameter(My.Resources.Parameters.RefundStatus, RefundStatus)
                                Return com.ExecuteDataTable
                        End Select
                    End Using
                End Using
                Return Nothing

            End Function

            Friend Function GetUnProcessedReadyRefundsForQodNumber(ByVal qodNumber As Integer) As DataTable

                Dim qodNo As String = qodNumber.ToString("000000")

                Using con As New Connection
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder
                                sb.Append("SELECT ")
                                sb.Append("cr.NUMB	as OrderNumber,")
                                sb.Append("cr.LINE	as Number,")
                                sb.Append("cl.SKUN  as SkuNumber, ")
                                sb.Append("st.DESCR	as SkuDescription,")
                                sb.Append("st.BUYU	as SkuUnitMeasure,")
                                sb.Append("cr.RefundStoreId,")
                                sb.Append("cr.RefundDate,")
                                sb.Append("cr.RefundTill,")
                                sb.Append("cr.RefundTransaction,")
                                sb.Append("cr.QtyReturned,")
                                sb.Append("cr.QtyCancelled,")
                                sb.Append("st.ONHA	as QtyOnHand,")
                                sb.Append("st.ONOR	as QtyOnOrder,")
                                sb.Append("cr.RefundStatus,")
                                sb.Append("cr.SellingStoreIbtOut, ")
                                sb.Append("cl.DeliverySource, ")
                                sb.Append("cl.Price ")
                                sb.Append("FROM CORREFUND cr ")
                                sb.Append("INNER JOIN CORLIN cl on cr.NUMB=cl.NUMB and cr.LINE=cl.LINE ")
                                sb.Append("INNER JOIN STKMAS st ON st.SKUN=cl.SKUN ")
                                sb.Append("WHERE cr.NUMB=?")
                                sb.Append("ORDER BY cr.RefundStoreId, cr.RefundTill, cr.RefundTransaction, cr.LINE")
                                com.CommandText = sb.ToString
                                com.AddParameter("OrderNumber", qodNo)
                                Return com.ExecuteDataTable

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleOrderRefundGet
                                com.AddParameter(My.Resources.Parameters.OrderNumber, qodNo)
                                Return com.ExecuteDataTable
                        End Select
                    End Using
                End Using
                Return Nothing

            End Function

            Friend Function GetSystemCodes(ByVal codeType As String) As DataTable
                Using con As New Connection
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder
                                sb.Append("SELECT ")
                                sb.Append("CODE,")
                                sb.Append("DESCR AS DESCRIPTION, ")
                                sb.Append("ATTR, ")
                                sb.Append("LABL, ")
                                sb.Append("IRPO, ")
                                sb.Append("SUPV, ")
                                sb.Append("MANA, ")
                                sb.Append("IPER, ")
                                sb.Append("ACTIVE, ")
                                sb.Append("PMATCH, ")
                                sb.Append("VALUE1, ")
                                sb.Append("VALUE2, ")
                                sb.Append("MFLEX ")
                                sb.Append("FROM SYSCOD ")
                                sb.Append("WHERE TYPE=?")
                                sb.Append("ORDER BY CODE")
                                com.CommandText = sb.ToString
                                com.AddParameter("CODE", codeType)
                                Return com.ExecuteDataTable

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SystemCodesSelectType
                                com.AddParameter(My.Resources.Parameters.Type, codeType)
                                Return com.ExecuteDataTable
                        End Select
                    End Using
                End Using
                Return Nothing

            End Function

            Friend Function GetLines(ByVal orderNumber As String) As DataTable

                Using con As New Connection
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder

                                sb.Append("SELECT ")
                                sb.Append("cl.NUMB		as OrderNumber, ")
                                sb.Append("cl.LINE		as Number,")
                                sb.Append("cl.SKUN		as SkuNumber,")
                                sb.Append("st.DESCR	    as SkuDescription,")
                                sb.Append("st.BUYU		as SkuunitMeasure,")
                                sb.Append("cl.QTYO		as QtyOrdered,")
                                sb.Append("cl.QTYT		as QtyTaken,")
                                sb.Append("cl.QTYR		as QtyRefunded,")
                                sb.Append("cl.QtyToBeDelivered  as QtyToDeliver, ")
                                sb.Append("st.ONHA		as QtyOnHand, ")
                                sb.Append("st.ONOR		as QtyOnOrder, ")
                                sb.Append("cl.Price,")
                                sb.Append("cl.IsDeliveryChargeItem,	")
                                sb.Append("cl.WGHT		as Weight,")
                                sb.Append("cl.VOLU		as Volume,")
                                sb.Append("cl.PORC		as PriceOverrideCode,")
                                sb.Append("cl.DeliveryStatus,")
                                sb.Append("cl.SellingStoreId,")
                                sb.Append("cl.SellingStoreOrderId,")
                                'sb.Append("cl.QtyToBeDelivered as QtyTodDeliver,")
                                sb.Append("cl.DeliverySource,")
                                sb.Append("cl.DeliverySourceIbtOut,")
                                sb.Append("cl.SellingStoreIbtIn, ")
                                sb.Append("cl.QuantityScanned, ")

                                sb.Append("cl2.SourceOrderLineNo ")

                                sb.Append("FROM CORLIN cl ")
                                'venda sale will have an entry in corlin2 only!
                                sb.Append("LEFT OUTER JOIN CORLIN2 cl2 ")
                                sb.Append("      on  cl2.NUMB = cl.NUMB ")
                                sb.Append("      and cl2.LINE = cl.LINE ")
                                sb.Append("INNER JOIN STKMAS st ")
                                sb.Append("      on st.SKUN = cl.SKUN ")
                                sb.Append("WHERE cl.NUMB=? ")
                                sb.Append("ORDER BY cl.LINE")

                                com.CommandText = sb.ToString
                                com.AddParameter("OrderNumber", orderNumber.PadLeft(6, "0"c))
                                Return com.ExecuteDataTable

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleOrderLineGet
                                com.AddParameter(My.Resources.Parameters.OrderNumber, orderNumber.PadLeft(6, "0"c))
                                Return com.ExecuteDataTable
                        End Select
                    End Using
                End Using
                Return Nothing

            End Function

            ''' <summary>
            ''' Function used to get Line for the sale
            ''' Hubs 2.0
            ''' </summary>
            ''' <param name="tranDate"></param>
            ''' <param name="tranNo"></param>
            ''' <param name="tillId"></param>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Friend Function SaleLineGet(ByVal tranDate As Date, ByVal tranNo As String, ByVal tillId As String) As DataTable

                Using con As New Connection
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder
                                sb.Append("SELECT ")
                                sb.Append("dl.DATE1,dl.TILL,dl.TRAN,dl.NUMB,dl.SKUN,dl.DEPT,")
                                sb.Append("dl.IBAR,dl.SUPV,dl.QUAN,dl.SPRI,dl.PRIC,dl.PRVE,")
                                sb.Append("dl.EXTP,dl.EXTC,dl.RITM,dl.PORC,dl.ITAG,dl.CATA,")
                                sb.Append("dl.VSYM,dl.TPPD,dl.TPME,dl.POPD,dl.POME,dl.QBPD,")
                                sb.Append("dl.QBME,dl.DGPD,dl.DGME,dl.MBPD,dl.MBME,dl.HSPD, ")
                                sb.Append("dl.HSME,dl.ESPD,dl.ESME,dl.LREV,dl.ESEQ,dl.CTGY,")
                                sb.Append("dl.GRUP,dl.SGRP,dl.STYL,dl.QSUP,dl.ESEV,dl.IMDN,")
                                sb.Append("dl.SALT,dl.VATN,dl.VATV,dl.BDCO,dl.BDCOInd,dl.RCOD,")
                                sb.Append("st.WSEQ,dl.RTI ,st.WRAT,dl.WEECOST ")
                                sb.Append("FROM DLLINE  dl ")
                                sb.Append("INNER JOIN STKMAS st on st.SKUN=dl.SKUN ")
                                sb.Append("WHERE dl.DATE1=? ")
                                sb.Append("and dl.TRAN=? ")
                                sb.Append("and dl.TILL=? ")
                                com.CommandText = sb.ToString
                                com.AddParameter(My.Resources.Parameters.TransactionDate, tranDate)
                                com.AddParameter(My.Resources.Parameters.TransactionNumber, tranNo)
                                com.AddParameter(My.Resources.Parameters.TillNumber, tillId)
                                Return com.ExecuteDataTable

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleLineGet
                                com.AddParameter(My.Resources.Parameters.TranDate, tranDate)
                                com.AddParameter(My.Resources.Parameters.TranNumber, tranNo)
                                com.AddParameter(My.Resources.Parameters.TillId, tillId)
                                Return com.ExecuteDataTable
                        End Select
                    End Using
                End Using
                Return Nothing
            End Function

            ''' <summary>
            ''' Function used to get Sale
            ''' Hubs 2.0
            ''' </summary>
            ''' <param name="tranDate"></param>
            ''' <param name="tranNo"></param>
            ''' <param name="tillId"></param>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Friend Function SaleGet(ByVal tranDate As Date, ByVal tranNo As String, ByVal tillId As String) As DataTable

                Using con As New Connection
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder

                                sb.Append("SELECT ")
                                sb.Append("dt.DATE1,dt.TILL,dt.TRAN,dt.CASH,dt.TIME,dt.SUPV,")
                                sb.Append("dt.TCOD,dt.OPEN,dt.MISC,dt.DESCR,dt.ORDN,dt.ACCT,")
                                sb.Append("dt.VOID,dt.VSUP,dt.TMOD,dt.PROC,dt.DOCN,dt.SUSE,")
                                sb.Append("dt.STOR,dt.MERC,dt.NMER,dt.TAXA,dt.DISC,dt.DSUP,")
                                sb.Append("dt.TOTL,dt.ACCN,dt.CARD,dt.AUPD,dt.BACK,dt.ICOM, ")
                                sb.Append("dt.IEMP,dt.RCAS,dt.RSUP,dt.PARK,dt.RMAN,dt.TOCD,")
                                sb.Append("dt.PKRC,dt.REMO,dt.GTPN,dt.CCRD,dt.SSTA,dt.SSEQ,")
                                sb.Append("dt.CBBU,dt.CARD_NO,dt.RTI,dt.VATR1,dt.VATR2,dt.VATR3,")
                                sb.Append("dt.VATR4,dt.VATR5,dt.VATR6,dt.VATR7,dt.VATR8,dt.VATR9,")
                                sb.Append("dt.VSYM1,dt.VSYM2,dt.VSYM3,dt.VSYM4,dt.VSYM5,dt.VSYM6,")
                                sb.Append("dt.VSYM7,dt.VSYM8,dt.VSYM9,dt.XVAT1,dt.XVAT2,dt.XVAT3,")
                                sb.Append("dt.XVAT4,dt.XVAT5,dt.XVAT6,dt.XVAT7,dt.XVAT8,dt.XVAT9,")
                                sb.Append("dt.VATV1,dt.VATV2,dt.VATV3,dt.VATV4,dt.VATV5,dt.VATV6,")
                                sb.Append("dt.VATV7,dt.VATV8,dt.VATV9 ")
                                sb.Append("FROM DLTOTS dt ")
                                sb.Append("WHERE dt.DATE1=? ")
                                sb.Append("and dt.TRAN=? ")
                                sb.Append("and dt.TILL=? ")

                                com.CommandText = sb.ToString
                                com.AddParameter(My.Resources.Parameters.TransactionDate, tranDate)
                                com.AddParameter(My.Resources.Parameters.TransactionNumber, tranNo)
                                com.AddParameter(My.Resources.Parameters.TillNumber, tillId)
                                Return com.ExecuteDataTable

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleGet
                                com.AddParameter(My.Resources.Parameters.TranDate, tranDate)
                                com.AddParameter(My.Resources.Parameters.TranNumber, tranNo)
                                com.AddParameter(My.Resources.Parameters.TillId, tillId)
                                Return com.ExecuteDataTable
                        End Select
                    End Using
                End Using
                Return Nothing

            End Function

            Friend Function SaleGet(ByVal strVendaOrderNo As String) As DataTable

                Using con As New Connection
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder

                                sb.Append("select ")
                                sb.Append("DATE1,TILL,TRAN,CASH,TIME,SUPV,")
                                sb.Append("TCOD,OPEN,MISC,DESCR,ORDN,ACCT,")
                                sb.Append("VOID,VSUP,TMOD,PROC,DOCN,SUSE,")
                                sb.Append("STOR,MERC,NMER,TAXA,DISC,DSUP,")
                                sb.Append("TOTL,ACCN,CARD,AUPD,BACK,ICOM,")
                                sb.Append("IEMP,RCAS,RSUP,PARK,RMAN,TOCD,")
                                sb.Append("PKRC,REMO,GTPN,CCRD,SSTA,SSEQ,")
                                sb.Append("CBBU,CARD_NO,RTI,")
                                sb.Append("VATR1,VATR2,VATR3,VATR4,VATR5,VATR6,VATR7,VATR8,VATR9,")
                                sb.Append("VSYM1,VSYM2,VSYM3,VSYM4,VSYM5,VSYM6,VSYM7,VSYM8,VSYM9,")
                                sb.Append("XVAT1,XVAT2,XVAT3,XVAT4,XVAT5,XVAT6,XVAT7,XVAT8,XVAT9,")
                                sb.Append("VATV1,VATV2,VATV3,VATV4,VATV5,VATV6,VATV7,VATV8,VATV9 ")

                                sb.Append("from DLTOTS ")
                                sb.Append("where DOCN = ? ")
                                sb.Append("and   TCOD = 'SA' ")

                                com.CommandText = sb.ToString
                                com.AddParameter(My.Resources.Parameters.DocumentNumber, strVendaOrderNo)
                                Return com.ExecuteDataTable

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleVendaGet
                                com.AddParameter(My.Resources.Parameters.DocumentNumber, strVendaOrderNo)
                                Return com.ExecuteDataTable
                        End Select
                    End Using
                End Using
                Return Nothing

            End Function



            ''' <summary>
            ''' Function used to get Customer for the sale
            ''' Hubs 2.0
            ''' </summary>
            ''' <param name="tranDate"></param>
            ''' <param name="tranNo"></param>
            ''' <param name="tillId"></param>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Friend Function SaleCustomerGet(ByVal tranDate As Date, ByVal tranNo As String, ByVal tillId As String) As DataTable

                Using con As New Connection
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder
                                sb.Append("SELECT ")
                                sb.Append("dc.DATE1,dc.TILL,dc.TRAN,dc.NUMB,dc.HNUM,dc.POST,")
                                sb.Append("dc.OSTR,dc.ODAT,dc.OTIL,dc.OTRN,dc.HNAM,dc.HAD1,")
                                sb.Append("dc.HAD2,dc.HAD3,dc.PHON,dc.NUMB,dc.OVAL,dc.OTEN,")
                                sb.Append("dc.RCOD,dc.LABL,dc.MOBP,dc.RTI ")
                                sb.Append("FROM DLRCUS dc")
                                sb.Append("WHERE dl.DATE1=? ")
                                sb.Append("and dl.TRAN=? ")
                                sb.Append("and dl.TILL=? ")
                                com.CommandText = sb.ToString
                                com.AddParameter(My.Resources.Parameters.TransactionDate, tranDate)
                                com.AddParameter(My.Resources.Parameters.TransactionNumber, tranNo)
                                com.AddParameter(My.Resources.Parameters.TillNumber, tillId)
                                Return com.ExecuteDataTable

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleCustomerGet
                                com.AddParameter(My.Resources.Parameters.TransactionDate, tranDate)
                                com.AddParameter(My.Resources.Parameters.TransactionNumber, tranNo)
                                com.AddParameter(My.Resources.Parameters.TillNumber, tillId)
                                Return com.ExecuteDataTable
                        End Select
                    End Using
                End Using
                Return Nothing

            End Function

            ''' <summary>
            ''' Function used to Paid entries for the sale
            ''' Hubs 2.0
            ''' </summary>
            ''' <param name="tranDate"></param>
            ''' <param name="tranNo"></param>
            ''' <param name="tillId"></param>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Friend Function SalePaidGet(ByVal tranDate As Date, ByVal tranNo As String, ByVal tillId As String) As DataTable

                Using con As New Connection
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder
                                sb.Append("SELECT ")
                                sb.Append("dP.DATE1,dP.TILL,dP.TRAN,dP.NUMB,dP.TYPE,dP.AMNT,")
                                sb.Append("dP.CARD,dP.EXDT,dP.COPN,dP.CLAS,dP.AUTH,dP.CKEY,")
                                sb.Append("dP.SUPV,dP.POST,dP.CKAC,dP.CKSC,dP.CKNO,dP.DBRF,")
                                sb.Append("dP.SEQN,dP.ISSU,dP.ATYP,dP.MERC,dP.CBAM,dP.DIGC, ")
                                sb.Append("dP.ECOM,dP.CTYP,dP.EFID,dP.EFTC,dP.STDT,dP.AUTHDESC, ")
                                sb.Append("dP.SECCODE,dP.TENC,dP.MPOW,dP.MRAT,dP.TENV,dP.RTI ")
                                sb.Append("FROM DLPAID   dP ")
                                sb.Append("WHERE dP.DATE1=? ")
                                sb.Append("and dP.TRAN=? ")
                                sb.Append("and dP.TILL=? ")
                                com.CommandText = sb.ToString
                                com.AddParameter(My.Resources.Parameters.TransactionDate, tranDate)
                                com.AddParameter(My.Resources.Parameters.TransactionNumber, tranNo)
                                com.AddParameter(My.Resources.Parameters.TillNumber, tillId)
                                Return com.ExecuteDataTable

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SalePaidGet
                                com.AddParameter(My.Resources.Parameters.TranDate, tranDate)
                                com.AddParameter(My.Resources.Parameters.TranNumber, tranNo)
                                com.AddParameter(My.Resources.Parameters.TillId, tillId)
                                Return com.ExecuteDataTable
                        End Select
                    End Using
                End Using
                Return Nothing

            End Function

            Friend Function GetStkmasPrice(ByVal SkuNumber As String) As DataTable

                Using con As New Connection
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder
                                sb.Append("SELECT ")
                                sb.Append("st.PRICE		as Price ")
                                sb.Append("FROM STKMAS st ")
                                sb.Append("WHERE st.SKUN = ? ")
                                sb.Append("ORDER BY st.SKUN")
                                com.CommandText = sb.ToString
                                com.AddParameter("SkuNumber", SkuNumber.PadLeft(6, "0"c))
                                Return com.ExecuteDataTable

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.StockGetPrice
                                com.AddParameter(My.Resources.Parameters.SkuNumber, SkuNumber.PadLeft(6, "0"c))
                                Return com.ExecuteDataTable
                        End Select
                    End Using
                End Using
                Return Nothing

            End Function

            Friend Function GetTexts(ByVal orderNumber As String) As DataTable

                Using con As New Connection
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder
                                sb.Append("SELECT ")
                                sb.Append("ct.NUMB		as OrderNumber, ")
                                sb.Append("ct.LINE		as Number, ")
                                sb.Append("ct.TYPE  	as Type,")
                                sb.Append("ct.TEXT	    as Text,")
                                sb.Append("ct.SellingStoreId,")
                                sb.Append("ct.SellingStoreOrderId ")
                                sb.Append("FROM CORTXT ct ")
                                sb.Append("WHERE ct.NUMB = ? ")
                                sb.Append("ORDER BY ct.LINE")
                                com.CommandText = sb.ToString
                                com.AddParameter("OrderNumber", orderNumber.PadLeft(6, "0"c))
                                Return com.ExecuteDataTable

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleOrderTextGet
                                com.AddParameter(My.Resources.Parameters.OrderNumber, orderNumber.PadLeft(6, "0"c))
                                Return com.ExecuteDataTable
                        End Select
                    End Using
                End Using
                Return Nothing

            End Function

            Friend Function GetRefunds(ByVal orderNumber As Integer) As DataTable

                Using con As New Connection
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder
                                sb.Append("SELECT ")
                                sb.Append("cr.NUMB	as OrderNumber,")
                                sb.Append("cr.LINE	as Number,")
                                sb.Append("cl.SKUN	as SkuNumber,")
                                sb.Append("st.DESCR	as SkuDescription,")
                                sb.Append("st.BUYU	as SkuUnitMeasure,")
                                sb.Append("cr.RefundStoreId,")
                                sb.Append("cr.RefundDate,")
                                sb.Append("cr.RefundTill,")
                                sb.Append("cr.RefundTran,")
                                sb.Append("cr.QtyRefunded,")
                                sb.Append("st.ONHA	as QtyOnHand,")
                                sb.Append("st.ONOR	as QtyOnOrder,")
                                sb.Append("cl.Price,")
                                sb.Append("cr.RefundStatus,")
                                sb.Append("cl.IsDeliveryChargeItem,")
                                sb.Append("cr.SellingStoreIbtOut")
                                sb.Append("FROM CORREFUND cr ")
                                sb.Append("inner join CORLIN cl ")
                                sb.Append("on cl.NUMB=cr.NUMB and cl.LINE=cr.LINE ")
                                sb.Append("INNER JOIN STKMAS st ON st.SKUN=cr.SKUN ")
                                sb.Append("WHERE cr.NUMB=? ")
                                sb.Append("ORDER BY cr.LINE")
                                com.CommandText = sb.ToString
                                com.AddParameter("OrderNumber", orderNumber.ToString.PadLeft(6, "0"c))
                                Return com.ExecuteDataTable

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleOrderRefundGet
                                Dim qodNo As String = orderNumber.ToString("000000")
                                com.AddParameter(My.Resources.Parameters.OrderNumber, qodNo)
                                Return com.ExecuteDataTable
                        End Select
                    End Using
                End Using
                Return Nothing

            End Function

            Private Function OrderRefundStatusUpdate(ByRef con As Connection, ByRef Refund As QodRefund) As Integer

                Dim linesAffected As Integer = 0
                If (Refund IsNot Nothing) Then
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                If Not Refund.SellingStoreIbtOut Is Nothing Then
                                    com.CommandText = "Update CORREFUND set RefundStatus=" & Refund.RefundStatus.ToString & ", SellingStoreIBTOut='" & Refund.SellingStoreIbtOut.ToString & "'  where NUMB='" & Refund.OrderNumber.ToString & "' AND LINE='" & Refund.Number.ToString & "' AND (SellingStoreIBTOut='' Or SellingStoreIBTOut='0')"
                                Else
                                    com.CommandText = "Update CORREFUND set RefundStatus=" & Refund.RefundStatus.ToString & " where NUMB='" & Refund.OrderNumber.ToString & "' and LINE='" & Refund.Number.ToString & "'"
                                End If
                                'com.AddParameter("NUMB", Refund.OrderNumber.ToString)
                                'com.AddParameter("LINE", Refund.Number.ToString)
                                'com.AddParameter("RefundStatus", Refund.RefundStatus)
                                Trace.WriteLine("NUMB = " & Refund.OrderNumber.ToString)
                                Trace.WriteLine("LINE = " & Refund.Number.ToString)
                                Trace.WriteLine("RefundStatus = " & Refund.RefundStatus.ToString)
                                linesAffected += com.ExecuteNonQuery()
                                Trace.WriteLine("Lines Affected: = " & linesAffected.ToString)

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleOrderRefundStatusUpdate
                                com.AddParameter(My.Resources.Parameters.OrderNumber, Refund.OrderNumber)
                                com.AddParameter(My.Resources.Parameters.LineNumber, Refund.Number)
                                com.AddParameter(My.Resources.Parameters.RefundStatus, Refund.RefundStatus)
                                com.AddParameter(My.Resources.Parameters.SellingStoreIbtOut, Refund.SellingStoreIbtOut)
                                linesAffected += com.ExecuteNonQuery()

                        End Select
                    End Using
                End If
                Return linesAffected

            End Function

            ''' <summary>
            ''' This routine is used work out if the Order is fully refunded Status = 1, partially refunded Status 2,
            ''' or no refunds Status = 0.
            ''' </summary>
            ''' <param name="Qod"></param>
            ''' <returns>Status</returns>
            ''' <remarks>Ref: 506</remarks>
            Private Function GetStatusOfRefund(ByVal Qod As QodHeader) As Integer

                Dim Status As Integer = 0                        'Default to no refunds

                If Qod.QtyOrdered + Qod.QtyRefunded >= 0 Then    'Fully Refunded - May have Delivery Charge as this is ignored from total hence >=
                    Status = 1
                    Return Status
                End If

                If (Qod.QtyOrdered + Qod.QtyRefunded) > 0 Then   'Part Refunded
                    Status = 2
                End If

                Return Status

            End Function

        End Module

    End Namespace

    ''' <summary>
    ''' Controls how ActionConfirmationForm is presented and behaves
    ''' </summary>
    ''' <history>
    ''' <created author="Charles McMahon" date="07/10/2010"></created>
    ''' </history>
    ''' <remarks></remarks>
    Public Enum ActionFormBehaviour
        PrintDialog = 1
        DespatchDialog = 2
        DeliveryDialog = 4
        DeliveryConfirm = 32
        DeliveryFail = 64
    End Enum

End Namespace