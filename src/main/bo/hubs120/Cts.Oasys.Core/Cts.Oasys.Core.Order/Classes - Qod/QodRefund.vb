﻿Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.Order.Qod.State
Imports Cts.Oasys.Data
Imports System.Text

Namespace Qod

    Public Class QodRefund
        Inherits Base

#Region "       Private Members"
        Private _orderNumber As String
        Private _number As String
        Private _refundStoreId As Integer
        Private _refundDate As Date
        Private _refundTill As String
        Private _refundTransaction As String
        Private _skuNumber As String
        Private _skuDescription As String
        Private _skuUnitMeasure As String
        Private _qtyReturned As Integer
        Private _qtyCancelled As Integer
        Private _qtyOnHand As Integer
        Private _qtyOnOrder As Integer
        Private _price As Decimal
        Private _refundStatus As Integer
        Private _sellingStoreIbtOut As String
        Private _fulfillingStoreIbtIn As String
        Private _ExistsInDB As Boolean
#End Region

#Region "       Public Properties"
        <ColumnMapping("OrderNumber")> Public Property OrderNumber() As String
            Get
                Return _orderNumber
            End Get
            Set(ByVal value As String)
                _orderNumber = value
            End Set
        End Property
        <ColumnMapping("Number")> Public Property Number() As String
            Get
                Return _number
            End Get
            Set(ByVal value As String)
                _number = value
            End Set
        End Property
        <ColumnMapping("RefundStoreId")> Public Property RefundStoreId() As Integer
            Get
                Return _refundStoreId
            End Get
            Set(ByVal value As Integer)
                _refundStoreId = value
            End Set
        End Property
        <ColumnMapping("RefundDate")> Public Property RefundDate() As Date
            Get
                Return _refundDate
            End Get
            Set(ByVal value As Date)
                _refundDate = value
            End Set
        End Property
        <ColumnMapping("RefundTill")> Public Property RefundTill() As String
            Get
                Return _refundTill
            End Get
            Set(ByVal value As String)
                _refundTill = value
            End Set
        End Property
        <ColumnMapping("RefundTransaction")> Public Property RefundTransaction() As String
            Get
                Return _refundTransaction
            End Get
            Set(ByVal value As String)
                _refundTransaction = value
            End Set
        End Property
        <ColumnMapping("SkuNumber")> Public Property SkuNumber() As String
            Get
                Return _skuNumber
            End Get
            Set(ByVal value As String)
                _skuNumber = value.Trim
            End Set
        End Property
        <ColumnMapping("SkuDescription")> Public Property SkuDescription() As String
            Get
                Return _skuDescription
            End Get
            Set(ByVal value As String)
                _skuDescription = value.Trim
            End Set
        End Property
        <ColumnMapping("SkuUnitMeasure")> Public Property SkuUnitMeasure() As String
            Get
                Return _skuUnitMeasure
            End Get
            Set(ByVal value As String)
                _skuUnitMeasure = value.Trim
            End Set
        End Property
        <ColumnMapping("Price")> Public Property Price() As Decimal
            Get
                Return _price
            End Get
            Set(ByVal value As Decimal)
                _price = value
            End Set
        End Property
        <ColumnMapping("QtyReturned")> Public Property QtyReturned() As Integer
            Get
                Return _qtyReturned
            End Get
            Set(ByVal value As Integer)
                _qtyReturned = value
            End Set
        End Property
        <ColumnMapping("QtyCancelled")> Public Property QtyCancelled() As Integer
            Get
                Return _qtyCancelled
            End Get
            Set(ByVal value As Integer)
                _qtyCancelled = value
            End Set
        End Property
        <ColumnMapping("QtyOnHand")> Public Property QtyOnHand() As Integer
            Get
                Return _qtyOnHand
            End Get
            Set(ByVal value As Integer)
                _qtyOnHand = value
            End Set
        End Property
        <ColumnMapping("QtyOnOrder")> Public Property QtyOnOrder() As Integer
            Get
                Return _qtyOnOrder
            End Get
            Set(ByVal value As Integer)
                _qtyOnOrder = value
            End Set
        End Property
        <ColumnMapping("RefundStatus")> Public Property RefundStatus() As Integer
            Get
                Return _refundStatus
            End Get
            Set(ByVal value As Integer)
                _refundStatus = value
            End Set
        End Property
        <ColumnMapping("SellingStoreIbtOut")> Public Property SellingStoreIbtOut() As String
            Get
                Return _sellingStoreIbtOut
            End Get
            Set(ByVal value As String)
                _sellingStoreIbtOut = value
            End Set
        End Property
        <ColumnMapping("FulfillingStoreIbtIn")> Public Property FulfillingStoreIbtIn() As String
            Get
                Return _fulfillingStoreIbtIn
            End Get
            Set(ByVal value As String)
                _fulfillingStoreIbtIn = value
            End Set
        End Property
        Public ReadOnly Property ValueRefunded() As Decimal
            Get
                Return (_qtyReturned + _qtyCancelled) * _price
            End Get
        End Property
        Public Property ExistsInDB() As Boolean
            Get
                Return _ExistsInDB
            End Get
            Set(ByVal value As Boolean)
                _ExistsInDB = value
            End Set
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub

        Public Sub SetStatusUpdateOk()
            _refundStatus = Qod.State.Refund.StatusUpdateOk
        End Sub

        Public Sub SetRefundStatus(ByVal NewStatus As Integer)
            If NewStatus >= State.Refund.None And NewStatus <= Delivery.OrderOk Then
                Me.RefundStatus = CInt((Me.RefundStatus \ 100) & NewStatus.ToString)
                Exit Sub
            End If

            If NewStatus > Me.RefundStatus Then
                Me.RefundStatus = CInt(IIf(NewStatus = Refund.NotifyOk, Refund.StatusUpdateOk, NewStatus))
            End If

        End Sub

        Private Function PersistNew(ByVal con As Connection) As Integer
            Dim LinesInserted As Integer = 0
            If (Me IsNot Nothing) Then
                Dim linesAffected As Integer = 0
                If (Me IsNot Nothing) Then
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder
                                sb.Append("Insert into CORREFUND (")
                                sb.Append("NUMB,LINE,RefundStoreId,RefundDate,RefundTill,RefundTransaction,RefundStatus,SellingStoreIbtOut,QtyReturned,QtyCancelled,FulfillingStoreIbtIn")
                                sb.Append(") values (")
                                sb.Append("?,?,?,?,?,?,?,?,?,?,?)")
                                com.CommandText = sb.ToString
                                com.AddParameter("NUMB", Me.OrderNumber)
                                com.AddParameter("LINE", Me.Number.ToString.PadLeft(4, "0"c))
                                com.AddParameter("RefundStoreId", Me.RefundStoreId)
                                com.AddParameter("RefundDate", Me.RefundDate)
                                com.AddParameter("RefundTill", Me.RefundTill)
                                com.AddParameter("RefundTransaction", Me.RefundTransaction)
                                com.AddParameter("RefundStatus", Me.RefundStatus)
                                com.AddParameter("SellingStoreIbtOut", Me.SellingStoreIbtOut)
                                com.AddParameter("QtyReturned", Me.QtyReturned)
                                com.AddParameter("QtyCancelled", Me.QtyCancelled)
                                com.AddParameter("FulfillingStoreIbtIn", IIf(Me.FulfillingStoreIbtIn = Nothing, "", Me.FulfillingStoreIbtIn))
                                LinesInserted += com.ExecuteNonQuery()

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleOrderRefundInsert
                                com.AddParameter(My.Resources.Parameters.OrderNumber, Me.OrderNumber)
                                com.AddParameter(My.Resources.Parameters.Number, Me.Number)
                                com.AddParameter(My.Resources.Parameters.RefundStoreId, Me.RefundStoreId)
                                com.AddParameter(My.Resources.Parameters.RefundDate, Me.RefundDate)
                                com.AddParameter(My.Resources.Parameters.RefundTill, Me.RefundTill)
                                com.AddParameter(My.Resources.Parameters.RefundTransaction, Me.RefundTransaction)
                                com.AddParameter(My.Resources.Parameters.RefundStatus, Me.RefundStatus)
                                com.AddParameter(My.Resources.Parameters.SellingStoreIbtOut, Me.SellingStoreIbtOut)
                                com.AddParameter(My.Resources.Parameters.QtyReturned, Me.QtyReturned)
                                com.AddParameter(My.Resources.Parameters.QtyCancelled, Me.QtyCancelled)
                                com.AddParameter(My.Resources.Parameters.FulfillingStoreIbtIn, Me.FulfillingStoreIbtIn)
                                LinesInserted += com.ExecuteNonQuery()
                        End Select
                    End Using
                End If
            End If
            Trace.WriteLine("Lines Affected: = " & LinesInserted.ToString)
            Return LinesInserted
        End Function

        Public Function Persist(ByVal con As Connection) As Integer
            If Not _ExistsInDB Then
                Return PersistNew(con)
            Else
                Dim LinesUpdated As Integer = 0
                If (Me IsNot Nothing) Then
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                If Not Me.SellingStoreIbtOut Is Nothing AndAlso Me.SellingStoreIbtOut.ToString <> String.Empty Then
                                    com.CommandText = "Update CORREFUND set RefundStatus=" & _refundStatus.ToString & ", SellingStoreIBTOut='" & Me.SellingStoreIbtOut.ToString & "'  where NUMB='" & Me.OrderNumber.ToString & "' AND LINE='" & Me.Number.ToString & "' AND (SellingStoreIBTOut='' Or SellingStoreIBTOut='0')"
                                Else
                                    com.CommandText = "Update CORREFUND set RefundStatus=" & _refundStatus.ToString & " where NUMB='" & _orderNumber.ToString & "' and LINE='" & _number.ToString & "'"
                                End If
                                LinesUpdated += com.ExecuteNonQuery()

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.SaleOrderRefundStatusUpdate
                                com.AddParameter(My.Resources.Parameters.OrderNumber, Me.OrderNumber)
                                com.AddParameter(My.Resources.Parameters.LineNumber, Me.Number)
                                com.AddParameter(My.Resources.Parameters.RefundStatus, Me.RefundStatus)
                                com.AddParameter(My.Resources.Parameters.SellingStoreIbtOut, Me.SellingStoreIbtOut)
                                LinesUpdated += com.ExecuteNonQuery()

                        End Select
                    End Using
                End If

                Trace.WriteLine("Lines Affected: = " & LinesUpdated.ToString)
                Return LinesUpdated
            End If
        End Function

    End Class

    Public Class QodRefundCollection
        Inherits BaseCollection(Of QodRefund)

        Public Sub New()
            MyBase.New()
        End Sub

        Public Overloads Sub Load(ByVal orderNumber As Integer)
            Dim dt As DataTable = Qod.DataAccess.GetUnProcessedReadyRefundsForQodNumber(orderNumber)
            Me.Load(dt)
            For Each Refund As QodRefund In Me.Items
                Refund.ExistsInDB = True
            Next
        End Sub

        Public Overloads Sub Load(ByVal orderNumber As Integer, ByVal RefundStatus As Integer)
            Dim dt As DataTable = Qod.DataAccess.GetRefundForQodNumber(orderNumber, RefundStatus)
            Me.Load(dt)
            For Each Refund As QodRefund In Me.Items
                Refund.ExistsInDB = True
            Next
        End Sub

        Public Property MinimumRefundStatus() As Refund
            Get
                If Me.Items.Count = 0 Then Return Refund.None

                Dim min As Integer = Me.Items(0).RefundStatus
                For Each line As QodRefund In Me.Items
                    min = Math.Min(min, line.RefundStatus)
                Next

                Return CType(min, Refund)
            End Get
            Set(ByVal value As Refund)
                For Each refund As QodRefund In Me.Items
                    If refund.RefundStatus < value Then
                        refund.RefundStatus = value
                    End If
                Next
            End Set
        End Property


        Public Sub SetHolding()
            Me.MinimumRefundStatus = Refund.CreatedOnHold
        End Sub

        Public Sub SetSellingStoreIbtOutIfEmpty(ByVal ibtNumber As String)
            For Each ref As QodRefund In Me.Items
                If Not StringIsSomething(ref.SellingStoreIbtOut) Then
                    ref.SellingStoreIbtOut = ibtNumber
                End If
            Next
        End Sub

        Public Function Find(ByVal number As Integer, ByVal refundStore As Integer, ByVal refundDate As Date, ByVal refundTill As String, ByVal refundTransation As String) As QodRefund
            For Each refund As QodRefund In Me.Items
                If CInt(refund.Number) = number _
                AndAlso refund.RefundStoreId = refundStore _
                AndAlso refund.RefundDate = refundDate _
                AndAlso refund.RefundTill = refundTill _
                AndAlso refund.RefundTransaction = refundTransation Then
                    Return refund
                End If
            Next
            Return Nothing
        End Function

        Public Function FindAll(ByVal number As Integer) As QodRefundCollection
            Dim QodRefundCollection As QodRefundCollection = New QodRefundCollection
            If Items.Count > 0 Then
                For Each QodRefund As QodRefund In Items
                    If CInt(QodRefund.Number) = number Then
                        QodRefundCollection.Add(QodRefund)
                    End If
                Next
            End If
            Return QodRefundCollection
        End Function

        Public Function FindOrCreate(ByVal number As Integer, ByVal refundStore As Integer, ByVal refundDate As Date, ByVal refundTill As String, ByVal refundTransation As String) As QodRefund
            Dim refund As QodRefund = Find(number, refundStore, refundDate, refundTill, refundTransation)
            If refund Is Nothing Then
                refund = Me.AddNew
                refund.Number = number.ToString
                refund.RefundStoreId = refundStore
                refund.RefundDate = refundDate
                refund.RefundTill = refundTill
                refund.RefundTransaction = refundTransation
            End If
            Return refund
        End Function

        Public Sub SetRefundStatus(ByVal NewStatus As Integer)
            For Each refund As QodRefund In Me.Items
                refund.SetRefundStatus(NewStatus)
            Next refund
        End Sub

        Public Function TotalCancelled() As Integer
            Dim Total As Integer = 0
            If Items.Count > 0 Then
                For Each QodRefund As QodRefund In Items
                    Total += QodRefund.QtyCancelled
                Next
            End If
            Return Total
        End Function

        Public Function TotalReturned() As Integer
            Dim Total As Integer = 0
            If Items.Count > 0 Then
                For Each QodRefund As QodRefund In Items
                    Total += QodRefund.QtyReturned
                Next
            End If
            Return Total
        End Function

    End Class

End Namespace


