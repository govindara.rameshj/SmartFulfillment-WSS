﻿Imports System.Text
Imports Cts.Oasys.Core.Order.Qod.State

Namespace Qod
    Public Class QodLineCollection
        Inherits BaseCollection(Of QodLine)

        Public Sub New()
            MyBase.New()
        End Sub

        ''' <summary>
        ''' Loads all lines for given order number into collection
        ''' </summary>
        ''' <param name="orderNumber"></param>
        ''' <remarks></remarks>
        Public Overloads Sub Load(ByVal orderNumber As String)
            Dim dt As DataTable = DataAccess.GetLines(orderNumber)
            Me.Load(dt)
            For Each QodLine As QodLine In Items
                QodLine.ExistsInDB = True
            Next
        End Sub

        Public Overloads Function AddNew() As QodLine
            Dim line As QodLine = MyBase.AddNew
            line.Number = (Me.Items.Count).ToString("0000")
            Return line
        End Function

        Public Overloads Function AddNew(ByVal skuNumber As String, ByVal skuPrice As Decimal) As QodLine
            Dim line As QodLine = Me.AddNew
            line.SkuNumber = skuNumber
            line.Price = skuPrice
            Return line
        End Function

        Public Function Find(ByVal number As String) As QodLine
            number = number.PadLeft(4, "0"c)
            For Each line As QodLine In Me.Items
                If line.Number = number Then Return line
            Next
            Return Nothing
        End Function

        Public Function Find(ByVal strOrderNo As String, ByVal strSKU As String) As QodLine
            For Each line As QodLine In Me.Items
                If line.OrderNumber = strOrderNo And line.SkuNumber = strSKU Then Return line
            Next

            Return Nothing
        End Function

        Public Function Find(ByVal strOrderNo As String, ByVal strLineNo As String, ByVal strSKU As String) As QodLine
            For Each line As QodLine In Me.Items
                If line.OrderNumber = strOrderNo And line.Number = strLineNo And line.SkuNumber = strSKU Then Return line
            Next

            Return Nothing
        End Function

        Public Function CountDeliverySource(ByVal storeId As Integer) As Integer
            Dim total As Integer = 0
            For Each line As QodLine In Me.Items
                Dim lineStoreId As Integer
                If Integer.TryParse(line.DeliverySource, lineStoreId) Then
                    If lineStoreId = storeId Then total += 1
                End If
            Next
            Return total
        End Function

        Public Sub RemoveDeliveryItems()
            If Me.Items.Count > 0 Then
                For index As Integer = Me.Items.Count - 1 To 0 Step -1
                    If Me.Items(index).IsDeliveryChargeItem Then Me.Items.RemoveAt(index)
                Next
            End If
        End Sub

        Public Sub RemoveNonDeliveryStoreItems(ByVal storeId As Integer)
            If Me.Items.Count > 0 Then
                For index As Integer = Me.Items.Count - 1 To 0 Step -1
                    If CInt(Me.Items(index).DeliverySource) <> storeId Then Me.Items.RemoveAt(index)
                Next
            End If
        End Sub

        Public Property DeliveryStatus() As State.Delivery
            Get
                If Me.Items.Count = 0 Then Return State.Delivery.OrderCreated

                Dim min As Integer = Me.Items(0).DeliveryStatus
                For Each line As QodLine In Me.Items
                    min = Math.Min(min, line.DeliveryStatus)
                Next
                Return CType(min, State.Delivery)
            End Get
            Set(ByVal value As State.Delivery)
                For Each line As QodLine In Me.Items
                    ' Check for the various allowed status changes
                    UpdateLineStatus(line, value)
                Next
            End Set
        End Property

        Public Property DeliveryStatus(ByVal storeId As Integer) As State.Delivery
            Get
                If Me.Items.Count = 0 Then Return State.Delivery.OrderCreated

                Dim min As Integer = Delivery.NULL
                For Each line As QodLine In Me.Items
                    If Integer.TryParse(line.DeliverySource, New Integer) Then
                        If CInt(line.DeliverySource) = storeId Then
                            If min = Delivery.NULL Then
                                min = line.DeliveryStatus
                            Else
                            min = Math.Min(min, line.DeliveryStatus)
                        End If
                    End If
                    End If
                Next
                If min = Delivery.NULL Then min = Me.DeliveryStatus
                Return CType(min, State.Delivery)
            End Get
            Set(ByVal value As State.Delivery)
                For Each line As QodLine In Me.Items
                    If line.DeliverySource = storeId.ToString Then
                        ' Check for the various allowed status changes
                        UpdateLineStatus(line, value)
                    End If
                Next
            End Set
        End Property

        Public WriteOnly Property DeliverySourceIbtOut() As String
            Set(ByVal value As String)
                For Each line As QodLine In Me.Items
                    line.DeliverySourceIbtOut = value
                Next
            End Set
        End Property

        '***** The following 7 methods have to be checked against the status flow logic - All lines should not always be set to these statuses - Charles McMahon
        Public Sub SetDeliveryStatusToSendOM()
            Me.DeliveryStatus = Delivery.DeliveryRequest
        End Sub

        Public Sub SetDeliveryStatusFailedConnection()

            For Each line As QodLine In Me.Items
                Dim value As Delivery = CType(line.DeliveryStatus, Delivery)
                Select Case CType(line.DeliveryStatus, Delivery)
                    Case Is <= Delivery.OrderFailedConnection : value = Delivery.OrderFailedConnection
                    Case Is <= Delivery.DeliveryRequestFailedConnection : value = Delivery.DeliveryRequestFailedConnection
                    Case Is <= Delivery.FulfilRequestFailedConnection : value = Delivery.FulfilRequestFailedConnection

                    Case Is <= Delivery.IbtOutFailedConnection : value = Delivery.IbtOutFailedConnection
                    Case Is <= Delivery.ReceiptNotifyFailedConnection : value = Delivery.ReceiptNotifyFailedConnection
                    Case Is <= Delivery.ReceiptStatusFailedConnection : value = Delivery.ReceiptStatusFailedConnection
                    Case Is <= Delivery.PickingNotifyFailedConnection : value = Delivery.PickingNotifyFailedConnection
                    Case Is <= Delivery.PickingStatusFailedConnection : value = Delivery.PickingStatusFailedConnection

                    Case Is <= Delivery.DespatchNotifyFailedConnection : value = Delivery.DespatchNotifyFailedConnection
                    Case Is <= Delivery.DespatchStatusFailedConnection : value = Delivery.DespatchStatusFailedConnection
                    Case Is <= Delivery.UndeliveredNotifyFailedConnection : value = Delivery.UndeliveredNotifyFailedConnection
                    Case Is <= Delivery.UndeliveredStatusFailedConnection : value = Delivery.UndeliveredStatusFailedConnection
                    Case Is <= Delivery.DeliveredNotifyFailedConnection : value = Delivery.DeliveredNotifyFailedConnection
                    Case Is <= Delivery.DeliveredStatusFailedConnection : value = Delivery.DeliveredStatusFailedConnection
                End Select
                If value > line.DeliveryStatus Then line.DeliveryStatus = value
            Next

        End Sub

        Public Sub SetDeliveryStatusFailedState()

            For Each line As QodLine In Me.Items
                Dim value As Delivery = CType(line.DeliveryStatus, Delivery)
                Select Case CType(line.DeliveryStatus, Delivery)
                    Case Is <= Delivery.OrderFailedState : value = Delivery.OrderFailedState
                    Case Is <= Delivery.DeliveryRequestFailedState : value = Delivery.DeliveryRequestFailedState
                    Case Is <= Delivery.FulfilRequestFailedState : value = Delivery.FulfilRequestFailedState

                    Case Is <= Delivery.IbtOutFailedState : value = Delivery.IbtOutFailedState
                    Case Is <= Delivery.ReceiptNotifyFailedState : value = Delivery.ReceiptNotifyFailedState
                    Case Is <= Delivery.ReceiptStatusFailedState : value = Delivery.ReceiptStatusFailedState
                    Case Is <= Delivery.PickingNotifyFailedState : value = Delivery.PickingNotifyFailedState
                    Case Is <= Delivery.PickingStatusFailedState : value = Delivery.PickingStatusFailedState

                    Case Is <= Delivery.DespatchNotifyFailedState : value = Delivery.DespatchNotifyFailedState
                    Case Is <= Delivery.DespatchStatusFailedState : value = Delivery.DespatchStatusFailedState
                    Case Is <= Delivery.UndeliveredNotifyFailedState : value = Delivery.UndeliveredNotifyFailedState
                    Case Is <= Delivery.UndeliveredStatusFailedState : value = Delivery.UndeliveredStatusFailedState
                    Case Is <= Delivery.DeliveredNotifyFailedState : value = Delivery.DeliveredNotifyFailedState
                    Case Is <= Delivery.DeliveredStatusFailedState : value = Delivery.DeliveredStatusFailedState
                End Select
                If value > line.DeliveryStatus Then line.DeliveryStatus = value
            Next

        End Sub

        Public Sub SetDeliveryStatusFailedData()

            For Each line As QodLine In Me.Items
                Dim value As Delivery = CType(line.DeliveryStatus, Delivery)
                Select Case CType(line.DeliveryStatus, Delivery)
                    Case Is <= Delivery.OrderFailedData : value = Delivery.OrderFailedData
                    Case Is <= Delivery.DeliveryRequestFailedData : value = Delivery.DeliveryRequestFailedData
                    Case Is <= Delivery.FulfilRequestFailedData : value = Delivery.FulfilRequestFailedData

                    Case Is <= Delivery.IbtOutFailedData : value = Delivery.IbtOutFailedData
                    Case Is <= Delivery.ReceiptNotifyFailedData : value = Delivery.ReceiptNotifyFailedData
                    Case Is <= Delivery.ReceiptStatusFailedData : value = Delivery.ReceiptStatusFailedData
                    Case Is <= Delivery.PickingNotifyFailedData : value = Delivery.PickingNotifyFailedData
                    Case Is <= Delivery.PickingStatusFailedData : value = Delivery.PickingStatusFailedData

                    Case Is <= Delivery.DespatchNotifyFailedData : value = Delivery.DespatchNotifyFailedData
                    Case Is <= Delivery.DespatchStatusFailedData : value = Delivery.DespatchStatusFailedData
                    Case Is <= Delivery.UndeliveredNotifyFailedData : value = Delivery.UndeliveredNotifyFailedData
                    Case Is <= Delivery.UndeliveredStatusFailedData : value = Delivery.UndeliveredStatusFailedData
                    Case Is <= Delivery.DeliveredNotifyFailedData : value = Delivery.DeliveredNotifyFailedData
                    Case Is <= Delivery.DeliveredStatusFailedData : value = Delivery.DeliveredStatusFailedData
                End Select
                If value > line.DeliveryStatus Then line.DeliveryStatus = value
            Next

        End Sub

        Public Sub SetDeliveryStatusOk()

            For Each line As QodLine In Me.Items
                Dim value As Delivery = CType(line.DeliveryStatus, Delivery)
                Select Case CType(line.DeliveryStatus, Delivery)
                    Case Is < Delivery.DeliveryRequestAccepted : value = Delivery.DeliveryRequestAccepted
                    Case Is < Delivery.PickingNotifyOk : value = Delivery.PickingNotifyOk
                    Case Is < Delivery.DespatchNotifyOk : value = Delivery.DespatchNotifyOk
                    Case Is < Delivery.UndeliveredNotifyOk : value = Delivery.UndeliveredNotifyOk
                    Case Is < Delivery.DeliveredNotifyOk : value = Delivery.DeliveredNotifyOk
                End Select
                If value > line.DeliveryStatus Then line.DeliveryStatus = value
            Next

        End Sub

        Public Sub SetDeliveryStatusOk(ByVal storeId As Integer)

            For Each line As QodLine In Me.Items
                If CInt(line.DeliverySource) = storeId Then
                    line.SetDeliveryNextStatusOk()
                End If
            Next

        End Sub

        Public Sub SetDeliveryNotifyOk(ByVal storeId As Integer)

            For Each line As QodLine In Me.Items
                If CInt(line.DeliverySource) = storeId Then
                    line.SetDeliveryNextNotifyOk()
                End If
            Next

        End Sub

        Public Function DeliverySourceMissing() As Boolean
            For Each QodLine As QodLine In Me.Items
                If QodLine.DeliverySource = "" OrElse QodLine.DeliverySource = "0" Then
                    Return True
                End If
            Next
            Return False
        End Function

        Public Function OrderValue() As Decimal
            Dim total As Decimal = 0
            For Each line As QodLine In Me.Items
                total += (line.QtyOrdered * line.Price)
            Next
            Return total
        End Function

        Public Function QtyOrdered() As Decimal
            Dim total As Decimal = 0
            For Each line As QodLine In Me.Items
                If line.IsDeliveryChargeItem = False Then
                total += line.QtyOrdered
                End If
            Next
            Return total
        End Function

        Public Function QtyTaken() As Decimal
            Dim total As Decimal = 0
            For Each line As QodLine In Me.Items
                total += line.QtyTaken
            Next
            Return total
        End Function

        Public Function Qtyrefunded() As Decimal
            Dim total As Decimal = 0
            For Each line As QodLine In Me.Items
                total += line.QtyRefunded
            Next
            Return total
        End Function

        Public Function NumberStockIssues() As Integer
            Dim total As Integer = 0
            For Each line As QodLine In Me.Items
                If line.IsDeliveryChargeItem Then Continue For
                If line.DeliverySource <> ThisStore.Id4.ToString Then Continue For
                If line.QtyToDeliver > line.QtyOnHand Then total += 1
            Next
            Return total
        End Function

        ''' <summary>
        ''' Returns comma separated string of all despatch lines in lines collection
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function DespatchSites() As String
            Dim sites As New ArrayList
            For Each line As QodLine In Me.Items
                If line.DeliverySource IsNot Nothing AndAlso line.DeliverySource.Trim.Length > 0 Then
                    If Not sites.Contains(line.DeliverySource) Then sites.Add(line.DeliverySource)
                End If
            Next

            Dim sb As New StringBuilder
            If sites.Count > 0 Then
                For Each site As String In sites
                    If sb.Length > 0 Then sb.Append(", ")
                    sb.Append(site)
                Next
            End If

            Return sb.ToString

        End Function

        ''' <summary>
        ''' Set each lines status to the new status provided
        ''' </summary>
        ''' <param name="NewStatus"></param>
        ''' <param name="storeId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function SetDeliveryStatus(ByVal NewStatus As Integer, Optional ByVal StoreId As Integer = 0) As Integer

            For Each line As QodLine In Me.Items
                line.setDeliveryStatus(NewStatus, StoreId)
            Next
            Return DeliveryStatus
        End Function

        Private Sub UpdateLineStatus(ByRef line As QodLine, ByVal value As State.Delivery)

            ' Check for the various allowed status changes
            If value > line.DeliveryStatus Then
                ' If progressing the status forward
                line.DeliveryStatus = value
            ElseIf value = State.Delivery.DespatchCreated _
            AndAlso line.DeliveryStatus >= State.Delivery.UndeliveredCreated _
            AndAlso line.DeliveryStatus <= State.Delivery.UndeliveredStatusOk Then
                'allow setting BACK to despatch from in undelivered state
                line.DeliveryStatus = value
            ElseIf value = State.Delivery.IbtInAllStock _
            AndAlso line.DeliveryStatus >= State.Delivery.PickingListCreated _
            AndAlso line.DeliveryStatus <= State.Delivery.PickingStatusOk Then
                ' setting BACK to ready to fulfill from picking - Referral 631
                line.DeliveryStatus = value
            ElseIf value = State.Delivery.IbtInAllStock _
            AndAlso line.DeliveryStatus >= State.Delivery.UndeliveredCreated _
            AndAlso line.DeliveryStatus <= State.Delivery.UndeliveredStatusOk Then
                ' setting BACK to ready to fulfill from failed delivery  - Referral 711
                line.DeliveryStatus = value
            ElseIf value = State.Delivery.ReceiptStatusOk _
            AndAlso line.DeliveryStatus >= State.Delivery.PickingListCreated _
            AndAlso line.DeliveryStatus <= State.Delivery.PickingStatusOk Then
                ' setting BACK to ready to collect from despatch - Referral 740
                ' NB Must check that is NOT for Delivery in the header if not got a ref to parent header
                line.DeliveryStatus = value
            ElseIf value = State.Delivery.ReceiptStatusOk _
            AndAlso line.DeliveryStatus >= State.Delivery.UndeliveredCreated _
            AndAlso line.DeliveryStatus <= State.Delivery.UndeliveredStatusOk Then
                ' setting BACK to ready to collect from failed delivery - Referral 740
                ' NB Must check that is NOT for Delivery in the header if not got a ref to parent header
                line.DeliveryStatus = value
            End If
        End Sub
    End Class
End Namespace