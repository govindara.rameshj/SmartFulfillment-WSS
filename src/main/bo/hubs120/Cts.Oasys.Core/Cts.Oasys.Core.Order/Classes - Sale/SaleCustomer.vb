﻿Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports System.Text
Imports System.ComponentModel
Imports Cts.Oasys.Data
Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.Order.WebService

#Region "QodNamespace"

Namespace Qod

#Region "SaleCustomerClass"

    ''' <summary>
    ''' Sale Customer Class used to hold customer data
    ''' </summary>
    ''' <remarks></remarks>
    Public Class SaleCustomer
        Inherits Oasys.Core.Base
#Region "Table Fields"
        Private _transactionDate As Date
        Private _tillNumber As String
        Private _transactionNumber As String
        Private _customerName As String
        Private _houseNumber As String = String.Empty
        Private _postCode As String
        Private _storeNumber As String = "100"
        Private _saleDate As Date?
        Private _saleTill As String
        Private _saleTransaction As String
        Private _houseNumberName As String
        Private _address1 As String
        Private _address2 As String
        Private _address3 As String
        Private _phoneNumber As String
        Private _lineNumber As Integer
        Private _transactionValidated As Boolean = False
        Private _originalTenderType As Decimal = 0
        Private _refundReasonCode As String = "00"
        Private _labels As Boolean = False
        Private _phoneNumberMobile As String
        Private _rtiFlag As String = "S"
        Private _emailAddresss As String = String.Empty
        Private _phoneNumberWork As String = String.Empty

        <ColumnMapping("DATE1")> Public Property TransactionDate() As Date
            Get
                Return _transactionDate
            End Get
            Set(ByVal value As Date)
                _transactionDate = value
            End Set
        End Property
        <ColumnMapping("TILL")> Public Property TillNumber() As String
            Get
                Return _tillNumber
            End Get
            Set(ByVal value As String)
                _tillNumber = value
            End Set
        End Property
        <ColumnMapping("TRAN")> Public Property TransactionNumber() As String
            Get
                Return _transactionNumber
            End Get
            Set(ByVal value As String)
                _transactionNumber = value
            End Set
        End Property
        <ColumnMapping("NAME")> Public Property CustomerName() As String
            Get
                Return _customerName
            End Get
            Set(ByVal value As String)
                _customerName = value
            End Set
        End Property
        <ColumnMapping("HNUM")> Public Property HouseNumber() As String
            Get
                Return _houseNumber
            End Get
            Set(ByVal value As String)
                _houseNumber = value
            End Set
        End Property
        <ColumnMapping("POST")> Public Property PostCode() As String
            Get
                Return _postCode
            End Get
            Set(ByVal value As String)
                _postCode = value
            End Set
        End Property
        <ColumnMapping("OSTR")> Public Property StoreNumber() As String
            Get
                Return _storeNumber
            End Get
            Set(ByVal value As String)
                _storeNumber = value
            End Set
        End Property
        <ColumnMapping("ODAT")> Public Property SaleDate() As Date?
            Get
                Return _saleDate
            End Get
            Set(ByVal value As Date?)
                _saleDate = value
            End Set
        End Property
        <ColumnMapping("OTIL")> Public Property SaleTill() As String
            Get
                Return _saleTill
            End Get
            Set(ByVal value As String)
                _saleTill = value
            End Set
        End Property
        <ColumnMapping("OTRN")> Public Property SaleTransaction() As String
            Get
                Return _saleTransaction
            End Get
            Set(ByVal value As String)
                _saleTransaction = value
            End Set
        End Property
        <ColumnMapping("HNAM")> Public Property HouseNameNo() As String
            Get
                Return _houseNumberName
            End Get
            Set(ByVal value As String)
                _houseNumberName = value
            End Set
        End Property
        <ColumnMapping("HAD1")> Public Property Address1() As String
            Get
                Return _address1
            End Get
            Set(ByVal value As String)
                _address1 = value
            End Set
        End Property
        <ColumnMapping("HAD2")> Public Property Address2() As String
            Get
                Return _address2
            End Get
            Set(ByVal value As String)
                _address2 = value
            End Set
        End Property
        <ColumnMapping("HAD3")> Public Property Address3() As String
            Get
                Return _address3
            End Get
            Set(ByVal value As String)
                _address3 = value
            End Set
        End Property
        <ColumnMapping("PHON")> Public Property PhoneNumber() As String
            Get
                Return _phoneNumber
            End Get
            Set(ByVal value As String)
                _phoneNumber = value
            End Set
        End Property
        <ColumnMapping("NUMB")> Public Property LineNumber() As Integer
            Get
                Return _lineNumber
            End Get
            Set(ByVal value As Integer)
                _lineNumber = value
            End Set
        End Property
        <ColumnMapping("OVAL")> Public Property TransactionValidated() As Boolean
            Get
                Return _transactionValidated
            End Get
            Set(ByVal value As Boolean)
                _transactionValidated = value
            End Set
        End Property
        <ColumnMapping("OTEN")> Public Property OriginalTenderType() As Decimal
            Get
                Return _originalTenderType
            End Get
            Set(ByVal value As Decimal)
                _originalTenderType = value
            End Set
        End Property
        <ColumnMapping("RCOD")> Public Property RefundReasonCode() As String
            Get
                Return _refundReasonCode
            End Get
            Set(ByVal value As String)
                _refundReasonCode = value
            End Set
        End Property
        <ColumnMapping("LABL")> Public Property Labels() As Boolean
            Get
                Return _labels
            End Get
            Set(ByVal value As Boolean)
                _labels = value
            End Set
        End Property
        <ColumnMapping("MOBP")> Public Property PhoneNumberMobile() As String
            Get
                Return _phoneNumberMobile
            End Get
            Set(ByVal value As String)
                _phoneNumberMobile = value
            End Set
        End Property
        <ColumnMapping("RTI")> Public Property RTIFlag() As String
            Get
                Return _rtiFlag
            End Get
            Set(ByVal value As String)
                _rtiFlag = value
            End Set
        End Property
        <ColumnMapping("EmailAddress")> Public Property EmailAddress() As String
            Get
                Return _emailAddresss
            End Get
            Set(ByVal value As String)
                _emailAddresss = value
            End Set
        End Property
        <ColumnMapping("PhoneNumberWork")> Public Property PhoneNumberWork() As String
            Get
                Return _phoneNumberWork
            End Get
            Set(ByVal value As String)
                _phoneNumberWork = value
            End Set
        End Property

#Region "Methods"

        Public Overloads Sub Load(ByVal tranDate As Date, ByVal tranNumber As String, ByVal tillId As String)
            Dim dt As DataTable = DataAccess.SaleCustomerGet(tranDate, tranNumber, tillId)
            Me.Load(dt.Rows(0))
        End Sub


        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub
        Public Sub New()
            MyBase.New()
        End Sub
        'Other methods to be added once the webservice is ready
        'and partial classes are complete

#End Region

#End Region

#Region "Properties"
        Private _ExistsInDB As Boolean
        Public Property ExistsInDB() As Boolean
            Get
                Return _ExistsInDB
            End Get
            Set(ByVal value As Boolean)
                _ExistsInDB = value
            End Set
        End Property
#End Region

#Region "Methods"
        Public Function PersistNew(ByVal con As Connection) As Integer

            Dim LinesUpdated As Integer = 0

            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Odbc

                        Dim sb As New StringBuilder
                        sb.Append("INSERT INTO DLRCUS (")
                        sb.Append("DATE1, TILL, TRAN, NAME, HNUM, POST, OSTR, ODAT, OTIL, OTRN,")
                        sb.Append("HNAM, HAD1, HAD2, HAD3, PHON, NUMB, OVAL, OTEN, RCOD, LABL,")
                        sb.Append("MOBP, PhoneNumberWork, EmailAddress, RTI")
                        sb.Append(") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")

                        com.CommandText = sb.ToString
                        com.AddParameter("DATE1", TransactionDate)
                        com.AddParameter("TILL", TillNumber)
                        com.AddParameter("TRAN", TransactionNumber)
                        com.AddParameter("NAME", CustomerName)
                        com.AddParameter("HNUM", HouseNumber)
                        com.AddParameter("POST", PostCode)
                        com.AddParameter("OSTR", StoreNumber)

                        If SaleDate.HasValue = True Then
                            com.AddParameter("ODAT", SaleDate)
                        Else
                            com.AddParameter("ODAT", DBNull.Value)
                        End If

                        com.AddParameter("OTIL", SaleTill)
                        com.AddParameter("OTRN", SaleTransaction)
                        com.AddParameter("HNAM", HouseNameNo)
                        com.AddParameter("HAD1", Address1)
                        com.AddParameter("HAD2", Address2)
                        com.AddParameter("HAD3", Address3)
                        com.AddParameter("PHON", PhoneNumber)
                        com.AddParameter("NUMB", LineNumber)
                        com.AddParameter("OVAL", TransactionValidated)
                        com.AddParameter("OTEN", OriginalTenderType)
                        com.AddParameter("RCOD", RefundReasonCode)
                        com.AddParameter("LABL", Labels)
                        com.AddParameter("MOBP", PhoneNumberMobile)
                        com.AddParameter("PhoneNumberWork", PhoneNumberWork)
                        com.AddParameter("EmailAddress", EmailAddress)
                        com.AddParameter("RTI", RTIFlag)
                        LinesUpdated += com.ExecuteNonQuery()

                    Case DataProvider.Sql
                        'Added for hubs 2.0
                        com.StoredProcedureName = My.Resources.Procedures.SaleCustomerInsert
                        com.AddParameter(My.Resources.Parameters.TransactionDate, TransactionDate)
                        com.AddParameter(My.Resources.Parameters.TillNumber, TillNumber)
                        com.AddParameter(My.Resources.Parameters.TransactionNumber, TransactionNumber)
                        com.AddParameter(My.Resources.Parameters.CustomerName, CustomerName)
                        com.AddParameter(My.Resources.Parameters.HouseNumber, HouseNumber)
                        com.AddParameter(My.Resources.Parameters.PostCode, PostCode)
                        com.AddParameter(My.Resources.Parameters.StoreNumber, StoreNumber)
                        If SaleDate.HasValue Then
                            com.AddParameter(My.Resources.Parameters.SaleDate, SaleDate)
                        End If
                        com.AddParameter(My.Resources.Parameters.SaleTill, SaleTill)
                        com.AddParameter(My.Resources.Parameters.SaleTransaction, SaleTransaction)
                        com.AddParameter(My.Resources.Parameters.HouseNameNo, HouseNameNo)
                        com.AddParameter(My.Resources.Parameters.Address1, Address1)
                        com.AddParameter(My.Resources.Parameters.Address2, Address2)
                        com.AddParameter(My.Resources.Parameters.Address3, Address3)
                        com.AddParameter(My.Resources.Parameters.PhoneNumber, PhoneNumber)
                        com.AddParameter(My.Resources.Parameters.LineNumber, LineNumber)
                        com.AddParameter(My.Resources.Parameters.TransactionValidated, TransactionValidated)
                        com.AddParameter(My.Resources.Parameters.OriginalTenderType, OriginalTenderType)
                        com.AddParameter(My.Resources.Parameters.RefundReasonCode, RefundReasonCode)
                        com.AddParameter(My.Resources.Parameters.Labels, Labels)
                        com.AddParameter(My.Resources.Parameters.PhoneNumberMobile, PhoneNumberMobile)
                        com.AddParameter(My.Resources.Parameters.PhoneNumberWork, PhoneNumberWork)
                        com.AddParameter(My.Resources.Parameters.EmailAddress, EmailAddress)
                        com.AddParameter(My.Resources.Parameters.RTIFlag, RTIFlag)
                        LinesUpdated += com.ExecuteNonQuery()

                End Select
            End Using
            Return LinesUpdated

        End Function

        Public Function Persist(ByVal con As Connection) As Integer

            If Not ExistsInDB Then
                Return PersistNew(con)
            Else
                Dim LinesUpdated As Integer = 0

                Using com As New Command(con)
                    Select Case con.DataProvider
                        Case DataProvider.Odbc

                            Dim sb As New StringBuilder
                            sb.Append("UPDATE [DLRCUS] SET ")
                            sb.Append("[NAME] = ?, [HNUM] = ?, [POST] = ?, [OSTR] = ?, [ODAT] = ?, [OTIL] = ?, [OTRN] = ?,")
                            sb.Append("[HNAM] = ?, [HAD1] = ?, [HAD2] = ?, [HAD3] = ?, [PHON] = ?, [OVAL] = ?, [OTEN] = ?, [RCOD] = ?, [LABL] = ?,")
                            sb.Append("[MOBP] = ?, [PhoneNumberWork] = ?, [EmailAddress] = ?, [RTI] = ? ")
                            sb.Append("WHERE [DATE1] = ? AND [TILL] = ? AND [TRAN] = ? AND [NUMB] = ?")

                            com.CommandText = sb.ToString
                            com.AddParameter("NAME", CustomerName)
                            com.AddParameter("HNUM", HouseNumber)
                            com.AddParameter("POST", PostCode)
                            com.AddParameter("OSTR", StoreNumber)
                            com.AddParameter("ODAT", SaleDate)
                            com.AddParameter("OTIL", SaleTill)
                            com.AddParameter("OTRN", SaleTransaction)
                            com.AddParameter("HNAM", HouseNameNo)
                            com.AddParameter("HAD1", Address1)
                            com.AddParameter("HAD2", Address2)
                            com.AddParameter("HAD3", Address3)
                            com.AddParameter("PHON", PhoneNumber)
                            com.AddParameter("OVAL", TransactionValidated)
                            com.AddParameter("OTEN", OriginalTenderType)
                            com.AddParameter("RCOD", RefundReasonCode)
                            com.AddParameter("LABL", Labels)
                            com.AddParameter("MOBP", PhoneNumberMobile)
                            com.AddParameter("PhoneNumberWork", PhoneNumberWork)
                            com.AddParameter("EmailAddress", EmailAddress)
                            com.AddParameter("RTI", RTIFlag)
                            com.AddParameter("DATE1", TransactionDate)
                            com.AddParameter("TILL", TillNumber)
                            com.AddParameter("TRAN", TransactionNumber)
                            com.AddParameter("NUMB", LineNumber)
                            LinesUpdated += com.ExecuteNonQuery()

                        Case DataProvider.Sql
                            'Added for hubs 2.0
                            com.StoredProcedureName = My.Resources.Procedures.SaleCustomerUpdate
                            com.AddParameter(My.Resources.Parameters.TransactionDate, TransactionDate)
                            com.AddParameter(My.Resources.Parameters.TillNumber, TillNumber)
                            com.AddParameter(My.Resources.Parameters.TransactionNumber, TransactionNumber)
                            com.AddParameter(My.Resources.Parameters.CustomerName, CustomerName)
                            com.AddParameter(My.Resources.Parameters.HouseNumber, HouseNumber)
                            com.AddParameter(My.Resources.Parameters.PostCode, PostCode)
                            com.AddParameter(My.Resources.Parameters.StoreNumber, StoreNumber)
                            com.AddParameter(My.Resources.Parameters.SaleDate, SaleDate)
                            com.AddParameter(My.Resources.Parameters.SaleTill, SaleTill)
                            com.AddParameter(My.Resources.Parameters.SaleTransaction, SaleTransaction)
                            com.AddParameter(My.Resources.Parameters.HouseNameNo, HouseNameNo)
                            com.AddParameter(My.Resources.Parameters.Address1, Address1)
                            com.AddParameter(My.Resources.Parameters.Address2, Address2)
                            com.AddParameter(My.Resources.Parameters.Address3, Address3)
                            com.AddParameter(My.Resources.Parameters.PhoneNumber, PhoneNumber)
                            com.AddParameter(My.Resources.Parameters.LineNumber, LineNumber)
                            com.AddParameter(My.Resources.Parameters.TransactionValidated, TransactionValidated)
                            com.AddParameter(My.Resources.Parameters.OriginalTenderType, OriginalTenderType)
                            com.AddParameter(My.Resources.Parameters.RefundReasonCode, RefundReasonCode)
                            com.AddParameter(My.Resources.Parameters.Labels, Labels)
                            com.AddParameter(My.Resources.Parameters.PhoneNumberMobile, PhoneNumberMobile)
                            com.AddParameter(My.Resources.Parameters.PhoneNumberWork, PhoneNumberWork)
                            com.AddParameter(My.Resources.Parameters.EmailAddress, EmailAddress)
                            com.AddParameter(My.Resources.Parameters.RTIFlag, RTIFlag)
                            LinesUpdated += com.ExecuteNonQuery()

                    End Select
                End Using
                Return LinesUpdated
            End If

        End Function

#End Region
    End Class

    Public Class SaleCustomerCollection
        Inherits Oasys.Core.BaseCollection(Of SaleCustomer)

        Public Sub New()
            MyBase.New()
        End Sub
        Public Overloads Sub Load(ByVal tranDate As Date, ByVal tranNumber As String, ByVal tillId As String)
            Dim dt As DataTable = DataAccess.SaleCustomerGet(tranDate, tranNumber, tillId)
            Me.Load(dt)
        End Sub

    End Class

#End Region

End Namespace

#End Region

