﻿Imports System.Text
Imports System.Reflection
Imports Cts.Oasys.Data
Imports Cts.Oasys.Core.Order.WebService

#Region "QodNamespace"

Namespace Qod

#Region "Class SaleHeader"

    ''' <summary>
    ''' The Sale Header Class used to hold Sale Header data
    ''' </summary>
    ''' <remarks></remarks>
    Public Class SaleHeader
        Inherits Oasys.Core.Base

#Region "Table Fields"

        Private _transactionDate As Date
        Private _tillNumber As String
        Private _transactionNumber As String
        Private _cashierId As String = "000"
        Private _transactionTime As String = Now.ToString("hhmmss")
        Private _supervisorCashierNo As String = "000"
        Private _transactionCode As String = "SA"
        Private _odCode As Integer = 0
        Private _reasonCode As Integer = 0
        Private _description As String = String.Empty
        Private _orderNumber As String = "000000"
        Private _accountSale As Boolean = False
        Private _void As Boolean = False
        Private _voidSupervisor As String = "000"
        Private _trainingMode As Boolean = False
        Private _processed As Boolean = False
        Private _documentNumber As String = "00000000"
        Private _supervisorUsed As Boolean = False
        Private _storeNumber As String = String.Empty
        Private _merchandiseAmount As Decimal
        Private _nonMerchandiseAmount As Decimal = 0
        Private _taxAmount As Decimal
        Private _discount As Decimal = 0
        Private _discountSupervisor As String = "000"
        Private _totalSaleAmount As Decimal
        Private _accountNumber As String = "000000"
        Private _accountCardNumber As String = "00"
        Private _pcCollection As Boolean = False
        Private _fromDcOrders As Boolean = False
        Private _transactionComplete As Boolean = True
        Private _employeeDiscountOnly As Boolean = False
        Private _refundCashrNo As String = "000"
        Private _refundSupervisor As String = "000"
        Private _parked As Boolean = False
        Private _refundManager As String = "000"
        Private _tenderOverrideCode As String = "00"
        Private _unparked As Boolean = False
        Private _transactionOnline As Boolean = True
        Private _tokensPrinted As Integer = 0
        Private _colleagueNumber As String = "000000000"
        Private _saveStatus As String = "99"
        Private _saveSequence As String = String.Empty
        Private _cbbUpdate As String = String.Empty
        Private _discountCardNo As String = String.Empty
        Private _rti As String = "S"
        Private _vatr1 As Decimal
        Private _vatr2 As Decimal
        Private _vatr3 As Decimal
        Private _vatr4 As Decimal
        Private _vatr5 As Decimal
        Private _vatr6 As Decimal
        Private _vatr7 As Decimal
        Private _vatr8 As Decimal
        Private _vatr9 As Decimal
        Private _vatSymb1 As String = "a"
        Private _vatSymb2 As String = "b"
        Private _vatSymb3 As String = "c"
        Private _vatSymb4 As String = "d"
        Private _vatSymb5 As String = "e"
        Private _vatSymb6 As String = "f"
        Private _vatSymb7 As String = "g"
        Private _vatSymb8 As String = "h"
        Private _vatSymb9 As String = "i"
        Private _xVatAmnt1 As Decimal
        Private _xVatAmnt2 As Decimal
        Private _xVatAmnt3 As Decimal
        Private _xVatAmnt4 As Decimal
        Private _xVatAmnt5 As Decimal
        Private _xVatAmnt6 As Decimal
        Private _xVatAmnt7 As Decimal
        Private _xVatAmnt8 As Decimal
        Private _xVatAmnt9 As Decimal
        Private _vatAmnt1 As Decimal
        Private _vatAmnt2 As Decimal
        Private _vatAmnt3 As Decimal
        Private _vatAmnt4 As Decimal
        Private _vatAmnt5 As Decimal
        Private _vatAmnt6 As Decimal
        Private _vatAmnt7 As Decimal
        Private _vatAmnt8 As Decimal
        Private _vatAmnt9 As Decimal

        'column mapping

        <ColumnMapping("DATE1")> Public Property TransactionDate() As Date
            Get
                Return _transactionDate
            End Get
            Set(ByVal value As Date)
                _transactionDate = value
            End Set
        End Property
        <ColumnMapping("TILL")> Public Property TillNumber() As String
            Get
                Return _tillNumber
            End Get
            Set(ByVal value As String)
                _tillNumber = value
            End Set
        End Property
        <ColumnMapping("TRAN")> Public Property TransactionNumber() As String
            Get
                Return _transactionNumber
            End Get
            Set(ByVal value As String)
                _transactionNumber = value
            End Set
        End Property
        <ColumnMapping("CASH")> Public Property CashierID() As String
            Get
                Return _cashierId
            End Get
            Set(ByVal value As String)
                _cashierId = value
            End Set
        End Property
        <ColumnMapping("TIME")> Public Property TransactionTime() As String
            Get
                Return _transactionTime
            End Get
            Set(ByVal value As String)
                _transactionTime = value
            End Set
        End Property
        <ColumnMapping("SUPV")> Public Property SupervisorCashierNo() As String
            Get
                Return _supervisorCashierNo
            End Get
            Set(ByVal value As String)
                _supervisorCashierNo = value
            End Set
        End Property
        <ColumnMapping("TCOD")> Public Property TransactionCode() As String
            Get
                Return _transactionCode
            End Get
            Set(ByVal value As String)
                _transactionCode = value
            End Set
        End Property
        <ColumnMapping("OPEN")> Public Property ODCode() As Integer
            Get
                Return _odCode
            End Get
            Set(ByVal value As Integer)
                _odCode = value
            End Set
        End Property
        <ColumnMapping("MISC")> Public Property ReasonCode() As Integer
            Get
                Return _reasonCode
            End Get
            Set(ByVal value As Integer)
                _reasonCode = value
            End Set
        End Property
        <ColumnMapping("DESCR")> Public Property Description() As String
            Get
                Return _description
            End Get
            Set(ByVal value As String)
                _description = value
            End Set
        End Property
        <ColumnMapping("ORDN")> Public Property OrderNumber() As String
            Get
                Return _orderNumber
            End Get
            Set(ByVal value As String)
                _orderNumber = value
            End Set
        End Property
        <ColumnMapping("ACCT")> Public Property AccountSale() As Boolean
            Get
                Return _accountSale
            End Get
            Set(ByVal value As Boolean)
                _accountSale = value
            End Set
        End Property
        <ColumnMapping("VOID")> Public Property Void() As Boolean
            Get
                Return _void
            End Get
            Set(ByVal value As Boolean)
                _void = value
            End Set
        End Property
        <ColumnMapping("VSUP")> Public Property VoidSupervisor() As String
            Get
                Return _voidSupervisor
            End Get
            Set(ByVal value As String)
                _voidSupervisor = value
            End Set
        End Property
        <ColumnMapping("TMOD")> Public Property TrainingMode() As Boolean
            Get
                Return _trainingMode
            End Get
            Set(ByVal value As Boolean)
                _trainingMode = value
            End Set
        End Property
        <ColumnMapping("PROC")> Public Property Processed() As Boolean
            Get
                Return _processed
            End Get
            Set(ByVal value As Boolean)
                _processed = value
            End Set
        End Property
        <ColumnMapping("DOCN")> Public Property DocumentNumber() As String
            Get
                Return _documentNumber
            End Get
            Set(ByVal value As String)
                _documentNumber = value
            End Set
        End Property
        <ColumnMapping("SUSE")> Public Property SupervisorUsed() As Boolean
            Get
                Return _supervisorUsed
            End Get
            Set(ByVal value As Boolean)
                _supervisorUsed = value
            End Set
        End Property
        <ColumnMapping("STOR")> Public Property StoreNumber() As String
            Get
                Return _storeNumber
            End Get
            Set(ByVal value As String)
                _storeNumber = value
            End Set
        End Property
        <ColumnMapping("MERC")> Public Property MerchandiseAmount() As Decimal
            Get
                Return _merchandiseAmount
            End Get
            Set(ByVal value As Decimal)
                _merchandiseAmount = value
            End Set
        End Property
        <ColumnMapping("NMER")> Public Property NonMerchandiseAmount() As Decimal
            Get
                Return _nonMerchandiseAmount
            End Get
            Set(ByVal value As Decimal)
                _nonMerchandiseAmount = value
            End Set
        End Property
        <ColumnMapping("TAXA")> Public Property TaxAmount() As Decimal
            Get
                Return _taxAmount
            End Get
            Set(ByVal value As Decimal)
                _taxAmount = value
            End Set
        End Property
        <ColumnMapping("DISC")> Public Property Discount() As Decimal
            Get
                Return _discount
            End Get
            Set(ByVal value As Decimal)
                _discount = value
            End Set
        End Property
        <ColumnMapping("DSUP")> Public Property DiscountSupervisor() As String
            Get
                Return _discountSupervisor
            End Get
            Set(ByVal value As String)
                _discountSupervisor = value
            End Set
        End Property
        <ColumnMapping("TOTL")> Public Property TotalSaleAmount() As Decimal
            Get
                Return (MerchandiseAmount - Discount)
            End Get
            Set(ByVal value As Decimal)
                _totalSaleAmount = value
            End Set
        End Property
        <ColumnMapping("ACCN")> Public Property AccountNumber() As String
            Get
                Return _accountNumber
            End Get
            Set(ByVal value As String)
                _accountNumber = value
            End Set
        End Property
        <ColumnMapping("CARD")> Public Property AccountCardNumber() As String
            Get
                Return _accountCardNumber
            End Get
            Set(ByVal value As String)
                _accountCardNumber = value
            End Set
        End Property
        <ColumnMapping("AUPD")> Public Property PCCollection() As Boolean
            Get
                Return _pcCollection
            End Get
            Set(ByVal value As Boolean)
                _pcCollection = value
            End Set
        End Property
        <ColumnMapping("BACK")> Public Property FromDCOrders() As Boolean
            Get
                Return _fromDcOrders
            End Get
            Set(ByVal value As Boolean)
                _fromDcOrders = value
            End Set
        End Property
        <ColumnMapping("ICOM")> Public Property TransactionComplete() As Boolean
            Get
                Return _transactionComplete
            End Get
            Set(ByVal value As Boolean)
                _transactionComplete = value
            End Set
        End Property
        <ColumnMapping("IEMP")> Public Property EmployeeDiscountOnly() As Boolean
            Get
                Return _employeeDiscountOnly
            End Get
            Set(ByVal value As Boolean)
                _employeeDiscountOnly = value
            End Set
        End Property
        <ColumnMapping("RSUP")> Public Property RefundSupervisor() As String
            Get
                Return _refundSupervisor
            End Get
            Set(ByVal value As String)
                _refundSupervisor = value
            End Set
        End Property
        <ColumnMapping("RCAS")> Public Property RefundCashierNo() As String
            Get
                Return _refundCashrNo
            End Get
            Set(ByVal value As String)
                _refundCashrNo = value
            End Set
        End Property
        <ColumnMapping("PARK")> Public Property Parked() As Boolean
            Get
                Return _parked
            End Get
            Set(ByVal value As Boolean)
                _parked = value
            End Set
        End Property
        <ColumnMapping("RMAN")> Public Property RefundManager() As String
            Get
                Return _refundManager
            End Get
            Set(ByVal value As String)
                _refundManager = value
            End Set
        End Property
        <ColumnMapping("TOCD")> Public Property TenderOverrideCode() As String
            Get
                Return _tenderOverrideCode
            End Get
            Set(ByVal value As String)
                _tenderOverrideCode = value
            End Set
        End Property
        <ColumnMapping("PKRC")> Public Property Unparked() As Boolean
            Get
                Return _unparked
            End Get
            Set(ByVal value As Boolean)
                _unparked = value
            End Set
        End Property
        <ColumnMapping("REMO")> Public Property TransactionOnLine() As Boolean
            Get
                Return _transactionOnline
            End Get
            Set(ByVal value As Boolean)
                _transactionOnline = value
            End Set
        End Property
        <ColumnMapping("GTPN")> Public Property TokensPrinted() As Integer
            Get
                Return _tokensPrinted
            End Get
            Set(ByVal value As Integer)
                _tokensPrinted = value
            End Set
        End Property
        <ColumnMapping("CCRD")> Public Property ColleagueNumber() As String
            Get
                Return _colleagueNumber
            End Get
            Set(ByVal value As String)
                _colleagueNumber = value
            End Set
        End Property
        <ColumnMapping("SSTA")> Public Property SaveStatus() As String
            Get
                Return _saveStatus
            End Get
            Set(ByVal value As String)
                _saveStatus = value
            End Set
        End Property
        <ColumnMapping("SSEQ")> Public Property SaveSequence() As String
            Get
                Return _saveSequence
            End Get
            Set(ByVal value As String)
                _saveSequence = value
            End Set
        End Property
        <ColumnMapping("CBBU")> Public Property CBBUpdate() As String
            Get
                Return _cbbUpdate
            End Get
            Set(ByVal value As String)
                _cbbUpdate = value
            End Set
        End Property
        <ColumnMapping("CARD_NO")> Public Property DiscountCardNo() As String
            Get
                Return _discountCardNo
            End Get
            Set(ByVal value As String)
                _discountCardNo = value
            End Set
        End Property
        <ColumnMapping("RTI")> Public Property RTI() As String
            Get
                Return _rti
            End Get
            Set(ByVal value As String)
                _rti = value
            End Set
        End Property

        <ColumnMapping("VATR1")> Public Property VATRate1() As Decimal
            Get
                Return _vatr1
            End Get
            Set(ByVal value As Decimal)
                _vatr1 = value
            End Set
        End Property
        <ColumnMapping("VATR2")> Public Property VATRate2() As Decimal
            Get
                Return _vatr2
            End Get
            Set(ByVal value As Decimal)
                _vatr2 = value
            End Set
        End Property
        <ColumnMapping("VATR3")> Public Property VATRate3() As Decimal
            Get
                Return _vatr3
            End Get
            Set(ByVal value As Decimal)
                _vatr3 = value
            End Set
        End Property
        <ColumnMapping("VATR4")> Public Property VATRate4() As Decimal
            Get
                Return _vatr4
            End Get
            Set(ByVal value As Decimal)
                _vatr4 = value
            End Set
        End Property
        <ColumnMapping("VATR5")> Public Property VATRate5() As Decimal
            Get
                Return _vatr5
            End Get
            Set(ByVal value As Decimal)
                _vatr5 = value
            End Set
        End Property
        <ColumnMapping("VATR6")> Public Property VATRate6() As Decimal
            Get
                Return _vatr6
            End Get
            Set(ByVal value As Decimal)
                _vatr6 = value
            End Set
        End Property
        <ColumnMapping("VATR7")> Public Property VATRate7() As Decimal
            Get
                Return _vatr7
            End Get
            Set(ByVal value As Decimal)
                _vatr7 = value
            End Set
        End Property
        <ColumnMapping("VATR8")> Public Property VATRate8() As Decimal
            Get
                Return _vatr8
            End Get
            Set(ByVal value As Decimal)
                _vatr8 = value
            End Set
        End Property
        <ColumnMapping("VATR9")> Public Property VATRate9() As Decimal
            Get
                Return _vatr9
            End Get
            Set(ByVal value As Decimal)
                _vatr9 = value
            End Set
        End Property

        <ColumnMapping("VSYM1")> Public Property VATSymb1() As String
            Get
                Return _vatSymb1
            End Get
            Set(ByVal value As String)
                _vatSymb1 = value
            End Set
        End Property
        <ColumnMapping("VSYM2")> Public Property VATSymb2() As String
            Get
                Return _vatSymb2
            End Get
            Set(ByVal value As String)
                _vatSymb2 = value
            End Set
        End Property
        <ColumnMapping("VSYM3")> Public Property VATSymb3() As String
            Get
                Return _vatSymb3
            End Get
            Set(ByVal value As String)
                _vatSymb3 = value
            End Set
        End Property
        <ColumnMapping("VSYM4")> Public Property VATSymb4() As String
            Get
                Return _vatSymb4
            End Get
            Set(ByVal value As String)
                _vatSymb4 = value
            End Set
        End Property
        <ColumnMapping("VSYM5")> Public Property VATSymb5() As String
            Get
                Return _vatSymb5
            End Get
            Set(ByVal value As String)
                _vatSymb5 = value
            End Set
        End Property
        <ColumnMapping("VSYM6")> Public Property VATSymb6() As String
            Get
                Return _vatSymb6
            End Get
            Set(ByVal value As String)
                _vatSymb6 = value
            End Set
        End Property
        <ColumnMapping("VSYM7")> Public Property VATSymb7() As String
            Get
                Return _vatSymb7
            End Get
            Set(ByVal value As String)
                _vatSymb7 = value
            End Set
        End Property
        <ColumnMapping("VSYM8")> Public Property VATSymb8() As String
            Get
                Return _vatSymb8
            End Get
            Set(ByVal value As String)
                _vatSymb8 = value
            End Set
        End Property
        <ColumnMapping("VSYM9")> Public Property VATSymb9() As String
            Get
                Return _vatSymb9
            End Get
            Set(ByVal value As String)
                _vatSymb9 = value
            End Set
        End Property

        <ColumnMapping("XVAT1")> Public Property XVATAmnt1() As Decimal
            Get
                Return _xVatAmnt1
            End Get
            Set(ByVal value As Decimal)
                _xVatAmnt1 = value
            End Set
        End Property
        <ColumnMapping("XVAT2")> Public Property XVATAmnt2() As Decimal
            Get
                Return _xVatAmnt2
            End Get
            Set(ByVal value As Decimal)
                _xVatAmnt2 = value
            End Set
        End Property
        <ColumnMapping("XVAT3")> Public Property XVATAmnt3() As Decimal
            Get
                Return _xVatAmnt3
            End Get
            Set(ByVal value As Decimal)
                _xVatAmnt3 = value
            End Set
        End Property
        <ColumnMapping("XVAT4")> Public Property XVATAmnt4() As Decimal
            Get
                Return _xVatAmnt4
            End Get
            Set(ByVal value As Decimal)
                _xVatAmnt4 = value
            End Set
        End Property
        <ColumnMapping("XVAT5")> Public Property XVATAmnt5() As Decimal
            Get
                Return _xVatAmnt5
            End Get
            Set(ByVal value As Decimal)
                _xVatAmnt5 = value
            End Set
        End Property
        <ColumnMapping("XVAT6")> Public Property XVATAmnt6() As Decimal
            Get
                Return _xVatAmnt6
            End Get
            Set(ByVal value As Decimal)
                _xVatAmnt6 = value
            End Set
        End Property
        <ColumnMapping("XVAT7")> Public Property XVATAmnt7() As Decimal
            Get
                Return _xVatAmnt7
            End Get
            Set(ByVal value As Decimal)
                _xVatAmnt7 = value
            End Set
        End Property
        <ColumnMapping("XVAT8")> Public Property XVATAmnt8() As Decimal
            Get
                Return _xVatAmnt8
            End Get
            Set(ByVal value As Decimal)
                _xVatAmnt8 = value
            End Set
        End Property
        <ColumnMapping("XVAT9")> Public Property XVATAmnt9() As Decimal
            Get
                Return _xVatAmnt9
            End Get
            Set(ByVal value As Decimal)
                _xVatAmnt9 = value
            End Set
        End Property

        <ColumnMapping("VATV1")> Public Property VATAmnt1() As Decimal
            Get
                Return _vatAmnt1
            End Get
            Set(ByVal value As Decimal)
                _vatAmnt1 = value
            End Set
        End Property
        <ColumnMapping("VATV2")> Public Property VATAmnt2() As Decimal
            Get
                Return _vatAmnt2
            End Get
            Set(ByVal value As Decimal)
                _vatAmnt2 = value
            End Set
        End Property
        <ColumnMapping("VATV3")> Public Property VATAmnt3() As Decimal
            Get
                Return _vatAmnt3
            End Get
            Set(ByVal value As Decimal)
                _vatAmnt3 = value
            End Set
        End Property
        <ColumnMapping("VATV4")> Public Property VATAmnt4() As Decimal
            Get
                Return _vatAmnt4
            End Get
            Set(ByVal value As Decimal)
                _vatAmnt4 = value
            End Set
        End Property
        <ColumnMapping("VATV5")> Public Property VATAmnt5() As Decimal
            Get
                Return _vatAmnt5
            End Get
            Set(ByVal value As Decimal)
                _vatAmnt5 = value
            End Set
        End Property
        <ColumnMapping("VATV6")> Public Property VATAmnt6() As Decimal
            Get
                Return _vatAmnt6
            End Get
            Set(ByVal value As Decimal)
                _vatAmnt6 = value
            End Set
        End Property
        <ColumnMapping("VATV7")> Public Property VATAmnt7() As Decimal
            Get
                Return _vatAmnt7
            End Get
            Set(ByVal value As Decimal)
                _vatAmnt7 = value
            End Set
        End Property
        <ColumnMapping("VATV8")> Public Property VATAmnt8() As Decimal
            Get
                Return _vatAmnt8
            End Get
            Set(ByVal value As Decimal)
                _vatAmnt8 = value
            End Set
        End Property
        <ColumnMapping("VATV9")> Public Property VATAmnt9() As Decimal
            Get
                Return _vatAmnt9
            End Get
            Set(ByVal value As Decimal)
                _vatAmnt9 = value
            End Set
        End Property

#End Region

#Region "Properties"

        Public Property VATRates() As Decimal()
            Get
                Dim vatRateCollection As Decimal() = {Me.VATRate1, Me.VATRate2, Me.VATRate3, Me.VATRate4, Me.VATRate5, Me.VATRate6, Me.VATRate7, Me.VATRate8, Me.VATRate9}
                Return vatRateCollection
            End Get
            Set(ByVal value As Decimal())
                Dim PropertyItem As PropertyInfo = Nothing
                Dim Index As Integer = 1
                For Each VatValue In value
                    PropertyItem = Me.GetType.GetProperty("VATRate" & Index.ToString)
                    PropertyItem.SetValue(Me, value(Index - 1), Nothing)
                    Index += 1
                Next
            End Set
        End Property

        'Public ReadOnly Property VATSymbols() As String()
        '    Get
        '        Dim vatSymbolCollection As String() = {Me.VATSymb1, Me.VATSymb2, Me.VATSymb3, Me.VATSymb4, Me.VATSymb5, Me.VATSymb6, Me.VATSymb7, Me.VATSymb8, Me.VATSymb9}
        '        Return vatSymbolCollection
        '    End Get
        'End Property

        Public ReadOnly Property XVATAmounts() As Decimal()
            Get
                Dim xvatAmntCollection As Decimal() = {Me.XVATAmnt1, Me.XVATAmnt2, Me.XVATAmnt3, Me.XVATAmnt4, Me.XVATAmnt5, Me.XVATAmnt6, Me.XVATAmnt7, Me.XVATAmnt8, Me.XVATAmnt9}
                Return xvatAmntCollection
            End Get
        End Property

        Public ReadOnly Property VATAmounts() As Decimal()
            Get
                Dim vatAmntCollection As Decimal() = {Me.VATAmnt1, Me.VATAmnt2, Me.VATAmnt3, Me.VATAmnt4, Me.VATAmnt5, Me.VATAmnt6, Me.VATAmnt7, Me.XVATAmnt8, Me.XVATAmnt9}
                Return vatAmntCollection
            End Get
        End Property

        Private _SaleLines As SaleLineCollection = Nothing
        Private _SalePaid As SalePaidCollection = Nothing
        Private _SaleCustomers As SaleCustomerCollection = Nothing

        Private _ExistsInDB As Boolean = False

        Public Property SaleLines() As SaleLineCollection
            Get
                If _SaleLines Is Nothing Then LoadSaleLines()
                Return _SaleLines
            End Get
            Set(ByVal value As SaleLineCollection)
                _SaleLines = value
            End Set
        End Property

        Public Property SaleTenders() As SalePaidCollection
            Get
                If _SalePaid Is Nothing Then LoadSalePaid()
                Return _SalePaid
            End Get
            Set(ByVal value As SalePaidCollection)
                _SalePaid = value
            End Set
        End Property

        Public Property SaleCustomers() As SaleCustomerCollection
            Get
                If _SaleCustomers Is Nothing Then LoadSaleCustomers()
                Return _SaleCustomers
            End Get
            Set(ByVal value As SaleCustomerCollection)
                _SaleCustomers = value
            End Set

        End Property

        Public Property ExistsInDB() As Boolean
            Get
                Return _ExistsInDB
            End Get
            Set(ByVal value As Boolean)
                _ExistsInDB = value
            End Set
        End Property

#End Region

#Region "Methods"

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub

        Public Sub New(ByRef VendaSale As WebService.CTSQODCreateResponse, ByVal TranTill As String, ByVal TranNumber As String)
            MyBase.New()

            Dim decTotalVat As Decimal
            Dim VatRates As New VATRates

            Dim SaleLines As New SaleLineCollection
            Dim SaleLine As SaleLine
            Dim CheckStock As Stock.Stock
            Dim Finder As VATRateFinder
            Dim VatRate As VATRate

            Dim SaleCustomer As New SaleCustomer
            Dim SalePaid As New SalePaid

            Trace.WriteLine("Creating Sale Line(s) - DLLINE")

            For Each QODCreateLine As CTSQODCreateResponseOrderLine In VendaSale.OrderLines
                Trace.WriteLine("Creating Sale Line - DLLINE : " & QODCreateLine.SourceOrderLineNo.Value)

                SaleLine = New SaleLine
                CheckStock = Stock.Stock.GetStock(QODCreateLine.ProductCode.Value)

                Finder = New VATRateFinder(CheckStock.VatCode)
                VatRate = VatRates.VatRateCollection.Find(AddressOf Finder.FindVATFromIndex)

                SaleLine.TransactionDate = VendaSale.OrderHeader.SaleDate.Value
                SaleLine.TillNumber = TranTill
                SaleLine.TransactionNumber = TranNumber
                SaleLine.LineNumber = CInt(QODCreateLine.StoreOrderLineNo.Value)   'this has been set when the response object was created & initialised
                SaleLine.SKUNumber = QODCreateLine.ProductCode.Value
                'DEPT
                'IBAR
                'SUPV
                SaleLine.Quantity = QODCreateLine.TotalOrderQuantity.Value
                If QODCreateLine.ProductCode.Value = "805111" And CInt(QODCreateLine.SourceOrderLineNo.Value) = 0 Then
                    'delivery line
                    SaleLine.SystemLookupPrice = QODCreateLine.SellingPrice.Value
                Else
                    'sale line
                    SaleLine.SystemLookupPrice = CheckStock.Price
                End If

                SaleLine.ItemPrice = QODCreateLine.SellingPrice.Value
                SaleLine.ItemExcVatPrice = 0                       'till does not populate this field
                SaleLine.ExtendedValue = SaleLine.Quantity * SaleLine.ItemPrice
                'EXTC
                SaleLine.RelatedItemsSingle = CheckStock.IsItemSingle
                If SaleLine.SystemLookupPrice - SaleLine.ItemPrice <> 0 Then SaleLine.PriceOverrideCode = 10
                SaleLine.TaggedItem = CheckStock.IsTaggedItem
                'CATA
                Select Case CheckStock.VatCode
                    Case 1
                        SaleLine.VatSymbol = Me.VATSymb1
                    Case 2
                        SaleLine.VatSymbol = Me.VATSymb2
                    Case 3
                        SaleLine.VatSymbol = Me.VATSymb3
                    Case 4
                        SaleLine.VatSymbol = Me.VATSymb4
                    Case 5
                        SaleLine.VatSymbol = Me.VATSymb5
                    Case 6
                        SaleLine.VatSymbol = Me.VATSymb6
                    Case 7
                        SaleLine.VatSymbol = Me.VATSymb7
                    Case 8
                        SaleLine.VatSymbol = Me.VATSymb8
                    Case 9
                        SaleLine.VatSymbol = Me.VATSymb9
                End Select
                'TPPD
                'TPME
                SaleLine.PriceOverrideDiff = SaleLine.SystemLookupPrice - SaleLine.ItemPrice
                If SaleLine.SystemLookupPrice - SaleLine.ItemPrice <> 0 Then SaleLine.POMarginErosionCode = "000010"
                'QBPD
                'QBME
                'DGPD
                'DGME
                'MBPD
                'MBME
                'HSPD
                'HSME
                'ESPD
                'ESME
                'LREV
                'ESEQ
                SaleLine.HierCategory = CheckStock.HieCategory
                SaleLine.HierGroup = CheckStock.HieGroup
                SaleLine.HierSubGroup = CheckStock.HieSubgroup
                SaleLine.HierStyle = CheckStock.HieStyle
                'QSUP
                'ESEV
                'IMDN
                SaleLine.SaleTypeAtt = CheckStock.SalesType
                SaleLine.VatCode = CheckStock.VatCode
                SaleLine.VatValue = QODCreateLine.LineValue.Value - CType(Math.Round((QODCreateLine.LineValue.Value / (1.0 + (VatRate.VatRate / 100))), 2), Decimal)
                'BDCO
                'BDCOInd
                'RCOD
                'RTI
                'WEERATE
                'WEESEQN
                'WEECOST
                'SPARE

                'update sale summary totals
                'XVAT1 - XVAT9 : exclude vat
                'VATV1 - VATV9 : 'vat only
                AddSaleAmounts(SaleLine, QODCreateLine.LineValue.Value)
                decTotalVat += SaleLine.VatValue    'DLTOTS:TAXA

                SaleLines.Add(SaleLine)
            Next
            Me.SaleLines = SaleLines

            Trace.WriteLine("Creating Sales Customer - DLRCUS")
            With SaleCustomer
                .TransactionDate = VendaSale.OrderHeader.SaleDate.Value
                .TillNumber = TranTill
                .TransactionNumber = TranNumber
                .CustomerName = VendaSale.OrderHeader.CustomerName.Value
                'HNUM
                .PostCode = VendaSale.OrderHeader.CustomerPostcode.Value
                .StoreNumber = "000"
                .SaleDate = New Nullable(Of Date)
                .SaleTill = "00"
                .SaleTransaction = "0000"
                .HouseNameNo = VendaSale.OrderHeader.CustomerAddressLine1.Value
                .Address1 = VendaSale.OrderHeader.CustomerAddressLine2.Value
                .Address2 = VendaSale.OrderHeader.CustomerAddressTown.Value
                .Address3 = VendaSale.OrderHeader.CustomerAddressLine4.Value
                .PhoneNumber = VendaSale.OrderHeader.ContactPhoneHome.Value
                .LineNumber = 0
                'OVAL
                'OTEN
                'RCOD
                'LABL
                .PhoneNumberMobile = VendaSale.OrderHeader.ContactPhoneMobile.Value
                'RTI
                .EmailAddress = String.Empty
                .PhoneNumberWork = VendaSale.OrderHeader.ContactPhoneWork.Value
            End With
            Me.SaleCustomers.Add(SaleCustomer)

            Trace.WriteLine("Creating Sale Paid - DLPAID")
            With SalePaid
                .TransactionDate = VendaSale.OrderHeader.SaleDate.Value
                .TillNumber = TranTill
                .TransactionNumber = TranNumber
                .SequenceNumber = 1
                .TenderAmount = VendaSale.OrderHeader.TotalOrderValue.Value * -1
                .CCKeyed = False
            End With
            Me.SaleTenders.Add(SalePaid)

            Trace.WriteLine("Creating Sale - DLTOTS")

            Me.TransactionDate = VendaSale.OrderHeader.SaleDate.Value
            Me.TillNumber = TranTill
            Me.TransactionNumber = TranNumber
            Me.CashierID = "800"
            Me.TransactionTime = Now.ToString("HHmmss")
            'SUPV
            'TCOD
            'OPEN
            'MISC
            'DESCR
            'ORDN
            'ACCT
            'VOID
            'VSUP
            'TMOD
            'PROC
            Me.DocumentNumber = VendaSale.OrderHeader.SourceOrderNumber.Value
            'SUSE
            'STOR
            Me.MerchandiseAmount = VendaSale.OrderHeader.TotalOrderValue.Value
            'NMER
            Me.TaxAmount = decTotalVat
            'DISC
            'DSUP
            'TOTL
            'ACCN
            'CARD
            'AUPD
            'BACK
            'ICOM
            'IEMP
            'RCAS
            'RSUP
            Me.VATRates = VatRates.VatRates
            'VSYM1 - VSYM9
            'XVAT1 - XVAT9   : exclude vat        --worked out already
            'VATV1 - VATV9   : vat only           --worked out already
            'PARK
            'RMAN
            'TOCD
            'PKPC
            'REMO
            'GTPN
            'CCRD
            'SSTA
            'SSEQ
            If Now.Date <> VendaSale.OrderHeader.SaleDate.Value.Date Then
                'system date different to order date - DD/MM/YY
                Me.CBBUpdate = Now.Date.ToString("dd/MM/yy")
            End If
            If VendaSale.OrderHeader.LoyaltyCardNumber.Value IsNot Nothing Then Me.DiscountCardNo = VendaSale.OrderHeader.LoyaltyCardNumber.Value
            'RT1

            Trace.WriteLine("Finished Sale")
        End Sub

        Public Sub New(ByRef VendaRefund As WebService.CTSRefundCreateResponse, ByVal TranTill As String, ByVal TranNumber As String, _
                       ByVal blnHomeDeliveryOrder As Boolean, ByRef ExistingQODOrder As QodHeader, ByRef ExistingSale As SaleHeader)
            MyBase.New()

            Dim ExistingSaleLine As SaleLine
            Dim ExistingDeliveryChargeLine As SaleLine

            Dim NewSaleLine As SaleLine
            Dim NewSalePaid As SalePaid
            Dim NewSaleCustomer As SaleCustomer

            Dim Sku As Stock.Stock
            Dim intNewLineNo As Integer
            Dim decTotalVat As Decimal
            Dim strTemp As String

            Trace.WriteLine("Creating Sale Line(s) - DLLINE")
            intNewLineNo = 0
            decTotalVat = 0
            For Each VendaRefundLine As CTSRefundCreateResponseRefundLine In VendaRefund.RefundLinesCollection
                intNewLineNo += 1

                'find existing line
                ExistingSaleLine = Nothing
                For Each L As SaleLine In ExistingSale.SaleLines
                    If VendaRefundLine.StoreLineNo IsNot Nothing AndAlso VendaRefundLine.StoreLineNo.Value IsNot Nothing Then
                        'normal store line no processing
                        If L.LineNumber = CInt(VendaRefundLine.StoreLineNo.Value) Then ExistingSaleLine = L
                    Else
                        '"store line no" empty, use sku
                        If L.SKUNumber = VendaRefundLine.ProductCode.Value Then ExistingSaleLine = L
                    End If

                    'If VendaRefundLine.StoreLineNo.Value Is Nothing Then
                    '    '"store line no" empty, use sku
                    '    If L.SKUNumber = VendaRefundLine.ProductCode.Value Then ExistingSaleLine = L
                    'Else
                    '    'normal store line no processing
                    '    If L.LineNumber = VendaRefundLine.StoreLineNo.Value Then ExistingSaleLine = L
                    'End If
                Next

                'get stock
                Sku = Stock.Stock.GetStock(VendaRefundLine.ProductCode.Value)

                'create new line
                Trace.WriteLine("Creating Sale Line - DLLINE")

                NewSaleLine = New SaleLine
                With NewSaleLine
                    .TransactionDate = VendaRefund.RefundHeader.RefundDate.Value
                    .TillNumber = TranTill
                    .TransactionNumber = TranNumber
                    .LineNumber = intNewLineNo
                    .SKUNumber = VendaRefundLine.ProductCode.Value
                    'DEPT
                    'IBAR
                    'SUPV
                    .Quantity = CDec(VendaRefundLine.QuantityCancelled.Value) * -1
                    .SystemLookupPrice = Sku.Price
                    .ItemPrice = VendaRefundLine.RefundLineValue.Value / CDec(VendaRefundLine.QuantityCancelled.Value)
                    .ItemExcVatPrice = 0
                    .ExtendedValue = .Quantity * .ItemPrice
                    'EXTC
                    .RelatedItemsSingle = ExistingSaleLine.RelatedItemsSingle
                    If .SystemLookupPrice - .ItemPrice <> 0 Then .PriceOverrideCode = 10
                    .TaggedItem = ExistingSaleLine.TaggedItem
                    'CATA
                    .VatSymbol = ExistingSaleLine.VatSymbol           'use existing vat
                    'TPPD
                    'TPME
                    .PriceOverrideDiff = .SystemLookupPrice - .ItemPrice
                    If .SystemLookupPrice - .ItemPrice <> 0 Then .POMarginErosionCode = "000010"
                    'QBPD
                    'QBME
                    'DGPD
                    'DGME
                    'MBPD
                    'MBME
                    'HSPD
                    'HSME
                    'ESPD
                    'ESME
                    'LREV
                    'ESEQ
                    .HierCategory = Sku.HieCategory
                    .HierGroup = Sku.HieGroup
                    .HierSubGroup = Sku.HieSubgroup
                    .HierStyle = Sku.HieStyle
                    'QSUP
                    'ESEV
                    'IMDN
                    .SaleTypeAtt = Sku.SalesType
                    .VatCode = ExistingSaleLine.VatCode             'use existing vat
                    .VatValue = Math.Round((ExistingSaleLine.VatValue / ExistingSaleLine.Quantity), 2) * .Quantity

                    decTotalVat += .VatValue
                    'BDCO
                    'BDCOInd
                    'RCOD
                    'RTI
                    'WEERATE
                    'WEESEQN
                    'WEECOST
                    'SPARE

                    'update sale summary totals
                    'XVAT1 - XVAT9 : exclude vat
                    'VATV1 - VATV9 : 'vat only
                    VendaRefundBrokenDownVatTotals(NewSaleLine, VendaRefundLine.RefundLineValue.Value)
                End With
                Me.SaleLines.Add(NewSaleLine)

                Trace.WriteLine("Creating Sales Customer Refund Line - DLRCUS : ")

                NewSaleCustomer = New SaleCustomer
                With NewSaleCustomer
                    .TransactionDate = VendaRefund.RefundHeader.RefundDate.Value
                    .TillNumber = TranTill
                    .TransactionNumber = TranNumber
                    .CustomerName = String.Empty
                    'HNUM
                    .PostCode = String.Empty

                    strTemp = String.Empty
                    If blnHomeDeliveryOrder = False Then
                        strTemp = ExistingQODOrder.SellingStoreId.ToString
                        If strTemp.Length - 3 > 0 Then strTemp = strTemp.Substring(strTemp.Length - 3, 3)
                    End If
                    .StoreNumber = strTemp

                    .SaleDate = ExistingSale.TransactionDate
                    .SaleTill = ExistingSale.TillNumber
                    .SaleTransaction = ExistingSale.TransactionNumber
                    .HouseNameNo = String.Empty
                    .Address1 = String.Empty
                    .Address2 = String.Empty
                    .Address3 = String.Empty
                    .PhoneNumber = String.Empty
                    .LineNumber = intNewLineNo
                    'OVAL
                    'OTEN
                    .RefundReasonCode = "06"
                    'LABL
                    .PhoneNumberMobile = If(blnHomeDeliveryOrder = False, ExistingQODOrder.PhoneNumberMobile, String.Empty)

                    'RTI
                    .EmailAddress = String.Empty
                    .PhoneNumberWork = If(blnHomeDeliveryOrder = False, ExistingQODOrder.PhoneNumberWork, String.Empty)
                End With
                Me.SaleCustomers.Add(NewSaleCustomer)
            Next

            'delivery line
            If VendaRefund.RefundHeader.DeliveryChargeRefundValue.Value > 0 Then
                intNewLineNo += 1

                Trace.WriteLine("Creating Sale Line (Delivery) - DLLINE")
                'find existing delivery line
                ExistingDeliveryChargeLine = Nothing
                For Each L As SaleLine In ExistingSale.SaleLines
                    If L.SKUNumber = "805111" Then
                        ExistingDeliveryChargeLine = L
                    End If
                Next
                'get stock
                Sku = Stock.Stock.GetStock("805111")

                NewSaleLine = New SaleLine
                With NewSaleLine
                    .TransactionDate = VendaRefund.RefundHeader.RefundDate.Value
                    .TillNumber = TranTill
                    .TransactionNumber = TranNumber
                    .LineNumber = intNewLineNo
                    .SKUNumber = "805111"
                    'DEPT
                    'IBAR
                    'SUPV
                    .Quantity = -1
                    .SystemLookupPrice = VendaRefund.RefundHeader.DeliveryChargeRefundValue.Value
                    .ItemPrice = VendaRefund.RefundHeader.DeliveryChargeRefundValue.Value
                    .ItemExcVatPrice = 0

                    .ExtendedValue = .Quantity * .ItemPrice
                    'EXTC
                    .RelatedItemsSingle = ExistingDeliveryChargeLine.RelatedItemsSingle
                    '.PriceOverrideCode = 10
                    If .SystemLookupPrice - .ItemPrice <> 0 Then .PriceOverrideCode = 10

                    .TaggedItem = ExistingDeliveryChargeLine.TaggedItem
                    'CATA
                    .VatSymbol = ExistingDeliveryChargeLine.VatSymbol              'use existing vat
                    'TPPD
                    'TPME
                    .PriceOverrideDiff = .SystemLookupPrice - .ItemPrice
                    '.POMarginErosionCode = "000010"
                    If .SystemLookupPrice - .ItemPrice <> 0 Then .POMarginErosionCode = "000010"
                    'QBPD
                    'QBME
                    'DGPD
                    'DGME
                    'MBPD
                    'MBME
                    'HSPD
                    'HSME
                    'ESPD
                    'ESME
                    'LREV
                    'ESEQ
                    .HierCategory = Sku.HieCategory
                    .HierGroup = Sku.HieGroup
                    .HierSubGroup = Sku.HieSubgroup
                    .HierStyle = Sku.HieStyle
                    'QSUP
                    'ESEV
                    'IMDN
                    .SaleTypeAtt = Sku.SalesType
                    .VatCode = ExistingDeliveryChargeLine.VatCode          'use existing vat
                    .VatValue = Math.Round((ExistingDeliveryChargeLine.VatValue / ExistingDeliveryChargeLine.Quantity), 2) * .Quantity

                    decTotalVat += .VatValue
                    'BDCO
                    'BDCOInd
                    'RCOD
                    'RTI
                    'WEERATE
                    'WEESEQN
                    'WEECOST
                    'SPARE

                    'update sale summary totals
                    'XVAT1 - XVAT9 : exclude vat
                    'VATV1 - VATV9 : 'vat only
                    VendaRefundBrokenDownVatTotals(NewSaleLine, VendaRefund.RefundHeader.DeliveryChargeRefundValue.Value)
                End With
                Me.SaleLines.Add(NewSaleLine)
            End If

            Trace.WriteLine("Creating Sales Customer - DLRCUS")

            NewSaleCustomer = New SaleCustomer
            With NewSaleCustomer
                .TransactionDate = VendaRefund.RefundHeader.RefundDate.Value
                .TillNumber = TranTill
                .TransactionNumber = TranNumber
                .CustomerName = If(blnHomeDeliveryOrder = False, ExistingQODOrder.CustomerName, String.Empty)
                'HNUM
                .PostCode = If(blnHomeDeliveryOrder = False, ExistingQODOrder.CustomerPostcode, String.Empty)
                .StoreNumber = "000"
                .SaleDate = New Nullable(Of Date)
                .SaleTill = "00"
                .SaleTransaction = "0000"
                .HouseNameNo = If(blnHomeDeliveryOrder = False, ExistingQODOrder.CustomerAddress1, String.Empty)
                .Address1 = If(blnHomeDeliveryOrder = False, ExistingQODOrder.CustomerAddress2, String.Empty)
                .Address2 = If(blnHomeDeliveryOrder = False, ExistingQODOrder.CustomerAddress3, String.Empty)
                .Address3 = If(blnHomeDeliveryOrder = False, ExistingQODOrder.CustomerAddress4, String.Empty)
                .PhoneNumber = If(blnHomeDeliveryOrder = False, ExistingQODOrder.PhoneNumber, String.Empty)
                .LineNumber = 0
                'OVAL
                'OTEN
                'RCOD
                'LABL
                .PhoneNumberMobile = If(blnHomeDeliveryOrder = False, ExistingQODOrder.PhoneNumberMobile, String.Empty)
                'RTI
                .EmailAddress = String.Empty
                .PhoneNumberWork = If(blnHomeDeliveryOrder = False, ExistingQODOrder.PhoneNumberWork, String.Empty)
            End With
            Me.SaleCustomers.Add(NewSaleCustomer)

            Trace.WriteLine("Creating Sale Paid - DLPAID")
            NewSalePaid = New SalePaid
            With NewSalePaid
                .TransactionDate = VendaRefund.RefundHeader.RefundDate.Value
                .TillNumber = TranTill
                .TransactionNumber = TranNumber
                .SequenceNumber = 1
                .TenderAmount = VendaRefund.RefundHeader.RefundTotal.Value
                .CCKeyed = False
            End With
            Me.SaleTenders.Add(NewSalePaid)

            Trace.WriteLine("Creating Sale - DLTOTS")
            TransactionDate = VendaRefund.RefundHeader.RefundDate.Value
            TillNumber = TranTill
            TransactionNumber = TranNumber
            CashierID = ExistingSale.CashierID
            TransactionTime = Now.ToString("HHmmss")
            'SUPV
            TransactionCode = "RF"       'should be in an lookup
            'OPEN
            'MISC
            'DESCR
            OrderNumber = ExistingSale.OrderNumber
            'ACCT
            'VOID
            'VSUP
            'TMOD
            'PROC
            Me.DocumentNumber = VendaRefund.RefundHeader.SourceOrderNumber.Value
            'SUSE
            'STOR
            MerchandiseAmount = VendaRefund.RefundHeader.RefundTotal.Value * -1
            'NMER
            TaxAmount = decTotalVat
            'DISC
            'DSUP
            TotalSaleAmount = MerchandiseAmount
            'ACCN
            'CARD
            'AUPD
            'BACK
            'ICOM
            'IEMP
            'RCAS
            RefundCashierNo = ExistingSale.CashierID
            'RSUP
            VATRate1 = ExistingSale.VATRate1
            VATRate2 = ExistingSale.VATRate2
            VATRate3 = ExistingSale.VATRate3
            VATRate4 = ExistingSale.VATRate4
            VATRate5 = ExistingSale.VATRate5
            VATRate6 = ExistingSale.VATRate6
            VATRate7 = ExistingSale.VATRate7
            VATRate8 = ExistingSale.VATRate8
            VATRate9 = ExistingSale.VATRate9

            VATSymb1 = ExistingSale.VATSymb1
            VATSymb2 = ExistingSale.VATSymb2
            VATSymb3 = ExistingSale.VATSymb3
            VATSymb4 = ExistingSale.VATSymb4
            VATSymb5 = ExistingSale.VATSymb5
            VATSymb6 = ExistingSale.VATSymb6
            VATSymb7 = ExistingSale.VATSymb7
            VATSymb8 = ExistingSale.VATSymb8
            VATSymb9 = ExistingSale.VATSymb9
            'XVAT1 - XVAT9 : exclude vat        --worked out already
            'VATV1 - VATV9 : vat only           --worked out already
            'PARK
            'RMAN
            'TOCD
            'PKPC
            'REMO
            'GTPN
            'CCRD
            'SSTA
            'SSEQ
            If Now.Date <> VendaRefund.RefundHeader.RefundDate.Value.Date Then
                'system date different to order date - DD/MM/YY
                Me.CBBUpdate = Now.Date.ToString("dd/MM/yy")
            End If
            If VendaRefund.RefundHeader.LoyaltyCardNumber.Value IsNot Nothing Then Me.DiscountCardNo = VendaRefund.RefundHeader.LoyaltyCardNumber.Value
            'RT1

            Trace.WriteLine("Finished Sale")
        End Sub

        Private Sub BuildSaleStructure()

        End Sub

        Private Sub AddSaleAmounts(ByVal saleLine As SaleLine, ByVal decLineValue As Decimal)
            Select Case saleLine.VatCode
                Case 1
                    'Me.XVATAmnt1 += saleLine.ItemExcVatPrice * saleLine.Quantity
                    Me.XVATAmnt1 += (decLineValue - saleLine.VatValue)
                    Me.VATAmnt1 += saleLine.VatValue
                Case 2
                    'Me.XVATAmnt2 += saleLine.ItemExcVatPrice * saleLine.Quantity
                    Me.XVATAmnt2 += (decLineValue - saleLine.VatValue)
                    Me.VATAmnt2 += saleLine.VatValue
                Case 3
                    'Me.XVATAmnt3 += saleLine.ItemExcVatPrice * saleLine.Quantity
                    Me.XVATAmnt3 += (decLineValue - saleLine.VatValue)
                    Me.VATAmnt3 += saleLine.VatValue
                Case 4
                    'Me.XVATAmnt4 += saleLine.ItemExcVatPrice * saleLine.Quantity
                    Me.XVATAmnt4 += (decLineValue - saleLine.VatValue)
                    Me.VATAmnt4 += saleLine.VatValue
                Case 5
                    'Me.XVATAmnt5 += saleLine.ItemExcVatPrice * saleLine.Quantity
                    Me.XVATAmnt5 += (decLineValue - saleLine.VatValue)
                    Me.VATAmnt5 += saleLine.VatValue
                Case 6
                    'Me.XVATAmnt6 += saleLine.ItemExcVatPrice * saleLine.Quantity
                    Me.XVATAmnt6 += (decLineValue - saleLine.VatValue)
                    Me.VATAmnt6 += saleLine.VatValue
                Case 7
                    'Me.XVATAmnt7 += saleLine.ItemExcVatPrice * saleLine.Quantity
                    Me.XVATAmnt7 += (decLineValue - saleLine.VatValue)
                    Me.VATAmnt7 += saleLine.VatValue
                Case 8
                    'Me.XVATAmnt8 += saleLine.ItemExcVatPrice * saleLine.Quantity
                    Me.XVATAmnt8 += (decLineValue - saleLine.VatValue)
                    Me.VATAmnt8 += saleLine.VatValue
                Case 9
                    'Me.XVATAmnt9 += saleLine.ItemExcVatPrice * saleLine.Quantity
                    Me.XVATAmnt9 += (decLineValue - saleLine.VatValue)
                    Me.VATAmnt9 += saleLine.VatValue
            End Select
        End Sub

        Private Sub VendaRefundBrokenDownVatTotals(ByRef SL As SaleLine, ByVal decLineValue As Decimal)
            With SL
                Select Case .VatCode
                    Case 1
                        'Me.XVATAmnt1 += .ItemExcVatPrice * .Quantity
                        Me.XVATAmnt1 += (decLineValue - Math.Abs(.VatValue)) * -1
                        Me.VATAmnt1 += .VatValue
                    Case 2
                        'Me.XVATAmnt2 += .ItemExcVatPrice * .Quantity
                        Me.XVATAmnt2 += (decLineValue - Math.Abs(.VatValue)) * -1
                        Me.VATAmnt2 += .VatValue
                    Case 3
                        'Me.XVATAmnt3 += .ItemExcVatPrice * .Quantity
                        Me.XVATAmnt3 += (decLineValue - Math.Abs(.VatValue)) * -1
                        Me.VATAmnt3 += .VatValue
                    Case 4
                        'Me.XVATAmnt4 += .ItemExcVatPrice * .Quantity
                        Me.XVATAmnt4 += (decLineValue - Math.Abs(.VatValue)) * -1
                        Me.VATAmnt4 += .VatValue
                    Case 5
                        'Me.XVATAmnt5 += .ItemExcVatPrice * .Quantity
                        Me.XVATAmnt5 += (decLineValue - Math.Abs(.VatValue)) * -1
                        Me.VATAmnt5 += .VatValue
                    Case 6
                        'Me.XVATAmnt6 += .ItemExcVatPrice * .Quantity
                        Me.XVATAmnt6 += (decLineValue - Math.Abs(.VatValue)) * -1
                        Me.VATAmnt6 += .VatValue
                    Case 7
                        'Me.XVATAmnt7 += .ItemExcVatPrice * .Quantity
                        Me.XVATAmnt7 += (decLineValue - Math.Abs(.VatValue)) * -1
                        Me.VATAmnt7 += .VatValue
                    Case 8
                        'Me.XVATAmnt8 += .ItemExcVatPrice * .Quantity
                        Me.XVATAmnt8 += (decLineValue - Math.Abs(.VatValue)) * -1
                        Me.VATAmnt8 += .VatValue
                    Case 9
                        'Me.XVATAmnt9 += .ItemExcVatPrice * .Quantity
                        Me.XVATAmnt9 += (decLineValue - Math.Abs(.VatValue)) * -1
                        Me.VATAmnt9 += .VatValue
                End Select
            End With
        End Sub

        Public Sub LoadSaleLines()
            _SaleLines = New SaleLineCollection
            'Only try to load if we have some values to recall a transaction !
            If Not Me.TransactionNumber = Nothing AndAlso Not Me.TillNumber = Nothing Then
                _SaleLines.Load(Me.TransactionDate, Me.TransactionNumber, Me.TillNumber)
            End If
        End Sub

        Public Sub LoadSalePaid()
            _SalePaid = New SalePaidCollection
            If Not Me.TransactionNumber = Nothing AndAlso Not Me.TillNumber = Nothing Then
                _SalePaid.Load(Me.TransactionDate, Me.TransactionNumber, Me.TillNumber)
            End If
        End Sub

        Public Sub LoadSaleCustomers()
            _SaleCustomers = New SaleCustomerCollection
            '_salecustomers.Load(Me.TransactionDate, Me.TransactionNumber, Me.TillNumber)
        End Sub

        Public Function PersistNew(ByVal con As Connection) As Integer

            Dim LinesUpdated As Integer = 0

            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Odbc

                        Dim sb As New StringBuilder
                        sb.Append("Insert into DLTOTS (")
                        sb.Append("DATE1,TILL,TRAN,CASH,TIME,SUPV,TCOD,OPEN,MISC,DESCR,ORDN,")
                        sb.Append("ACCT,VOID,VSUP,TMOD,PROC,DOCN,SUSE,STOR,MERC,NMER,TAXA,DISC,DSUP,")
                        sb.Append("TOTL,ACCN,CARD,AUPD,BACK,ICOM,IEMP,RCAS,RSUP,PARK,")
                        sb.Append("VATR1,VATR2,VATR3,VATR4,VATR5,VATR6,VATR7,VATR8,VATR9,")
                        sb.Append("VSYM1,VSYM2,VSYM3,VSYM4,VSYM5,VSYM6,VSYM7,VSYM8,VSYM9,")
                        sb.Append("XVAT1,XVAT2,XVAT3,XVAT4,XVAT5,XVAT6,XVAT7,XVAT8,XVAT9,")
                        sb.Append("VATV1,VATV2,VATV3,VATV4,VATV5,VATV6,VATV7,VATV8,VATV9,")
                        sb.Append("RMAN, TOCD, PKRC,")
                        sb.Append("REMO,GTPN,CCRD,SSTA,SSEQ,CBBU,CARD_NO,RTI")
                        sb.Append(") values	(")
                        sb.Append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )")

                        com.CommandText = sb.ToString
                        com.AddParameter("TransactionDate", TransactionDate)
                        com.AddParameter("TillNumber", TillNumber)
                        com.AddParameter("TransactionNumber", TransactionNumber)
                        com.AddParameter("CashierID", CashierID)
                        com.AddParameter("TransactionTime", TransactionTime)
                        com.AddParameter("SupervisorCashierNo", SupervisorCashierNo)
                        com.AddParameter("TransactionCode", TransactionCode)
                        com.AddParameter("ODCode", ODCode)
                        com.AddParameter("ReasonCode", ReasonCode)
                        com.AddParameter("Description", Description)
                        com.AddParameter("OrderNumber", OrderNumber)
                        com.AddParameter("AccountSale", AccountSale)
                        com.AddParameter("Void", Void)
                        com.AddParameter("VoidSupervisor", VoidSupervisor)
                        com.AddParameter("TrainingMode", TrainingMode)
                        com.AddParameter("Processed", Processed)
                        com.AddParameter("DocumentNumber", DocumentNumber)
                        com.AddParameter("SupervisorUsed", SupervisorUsed)
                        com.AddParameter("StoreNumber", StoreNumber)
                        com.AddParameter("MerchandiseAmount", MerchandiseAmount)
                        com.AddParameter("NonMerchandiseAmount", NonMerchandiseAmount)
                        com.AddParameter("TaxAmount", TaxAmount)
                        com.AddParameter("Discount", Discount)
                        com.AddParameter("DiscountSupervisor", DiscountSupervisor)
                        com.AddParameter("Total", TotalSaleAmount)
                        com.AddParameter("AccountNumber", AccountNumber)
                        com.AddParameter("AccountCardNumber", AccountCardNumber)
                        com.AddParameter("PCCollection", PCCollection)
                        com.AddParameter("FromDCOrders", FromDCOrders)
                        com.AddParameter("TransactionComplete", TransactionComplete)
                        com.AddParameter("EmployeeDiscountOnly", EmployeeDiscountOnly)
                        com.AddParameter("RefundCashierNo", RefundCashierNo)
                        com.AddParameter("RefundSupervisor", RefundSupervisor)
                        com.AddParameter("Parked", Parked)
                        com.AddParameter("VATR1", VATRate1)
                        com.AddParameter("VATR2", VATRate2)
                        com.AddParameter("VATR3", VATRate3)
                        com.AddParameter("VATR4", VATRate4)
                        com.AddParameter("VATR5", VATRate5)
                        com.AddParameter("VATR6", VATRate6)
                        com.AddParameter("VATR7", VATRate7)
                        com.AddParameter("VATR8", VATRate8)
                        com.AddParameter("VATR9", VATRate9)
                        com.AddParameter("VSYM1", VATSymb1)
                        com.AddParameter("VSYM2", VATSymb2)
                        com.AddParameter("VSYM3", VATSymb3)
                        com.AddParameter("VSYM4", VATSymb4)
                        com.AddParameter("VSYM5", VATSymb5)
                        com.AddParameter("VSYM6", VATSymb6)
                        com.AddParameter("VSYM7", VATSymb7)
                        com.AddParameter("VSYM8", VATSymb8)
                        com.AddParameter("VSYM9", VATSymb9)
                        com.AddParameter("XVAT1", XVATAmnt1)
                        com.AddParameter("XVAT2", XVATAmnt2)
                        com.AddParameter("XVAT3", XVATAmnt3)
                        com.AddParameter("XVAT4", XVATAmnt4)
                        com.AddParameter("XVAT5", XVATAmnt5)
                        com.AddParameter("XVAT6", XVATAmnt6)
                        com.AddParameter("XVAT7", XVATAmnt7)
                        com.AddParameter("XVAT8", XVATAmnt8)
                        com.AddParameter("XVAT9", XVATAmnt9)
                        com.AddParameter("VATV1", VATAmnt1)
                        com.AddParameter("VATV2", VATAmnt2)
                        com.AddParameter("VATV3", VATAmnt3)
                        com.AddParameter("VATV4", VATAmnt4)
                        com.AddParameter("VATV5", VATAmnt5)
                        com.AddParameter("VATV6", VATAmnt6)
                        com.AddParameter("VATV7", VATAmnt7)
                        com.AddParameter("VATV8", VATAmnt8)
                        com.AddParameter("VATV9", VATAmnt9)
                        com.AddParameter("RefundManager", RefundManager)
                        com.AddParameter("TenderOverrideCode", TenderOverrideCode)
                        com.AddParameter("Unparked", Unparked)
                        com.AddParameter("TransactionOnLine", TransactionOnLine)
                        com.AddParameter("TokensPrinted", TokensPrinted)
                        com.AddParameter("ColleagueNumber", ColleagueNumber)
                        com.AddParameter("SaveStatus", SaveStatus)
                        com.AddParameter("SaveSequence", SaveSequence)
                        com.AddParameter("CBBUpdate", CBBUpdate)
                        com.AddParameter("DiscountCardNo", DiscountCardNo)
                        com.AddParameter("RTI", RTI)
                        LinesUpdated += com.ExecuteNonQuery()

                    Case DataProvider.Sql
                        'Added for hubs 2.0
                        com.StoredProcedureName = My.Resources.Procedures.SaleInsert
                        com.AddParameter(My.Resources.Parameters.TransactionDate, TransactionDate)
                        com.AddParameter(My.Resources.Parameters.TillNumber, TillNumber)
                        com.AddParameter(My.Resources.Parameters.TransactionNumber, TransactionNumber)
                        com.AddParameter(My.Resources.Parameters.TransactionTime, TransactionTime)
                        com.AddParameter(My.Resources.Parameters.CashierID, CashierID)
                        com.AddParameter(My.Resources.Parameters.SupervisorCashierNo, SupervisorCashierNo)
                        com.AddParameter(My.Resources.Parameters.TransactionCode, TransactionCode)
                        com.AddParameter(My.Resources.Parameters.ODCode, ODCode)
                        com.AddParameter(My.Resources.Parameters.ReasonCode, ReasonCode)
                        com.AddParameter(My.Resources.Parameters.Description, Description)
                        com.AddParameter(My.Resources.Parameters.OrderNumber, OrderNumber)
                        com.AddParameter(My.Resources.Parameters.AccountSale, AccountSale)
                        com.AddParameter(My.Resources.Parameters.Void, Void)
                        com.AddParameter(My.Resources.Parameters.VoidSupervisor, VoidSupervisor)
                        com.AddParameter(My.Resources.Parameters.TrainingMode, TrainingMode)
                        com.AddParameter(My.Resources.Parameters.Processed, Processed)
                        com.AddParameter(My.Resources.Parameters.DocumentNumber, DocumentNumber)
                        com.AddParameter(My.Resources.Parameters.SupervisorUsed, SupervisorUsed)
                        com.AddParameter(My.Resources.Parameters.StoreNumber, StoreNumber)
                        com.AddParameter(My.Resources.Parameters.MerchandiseAmount, MerchandiseAmount)
                        com.AddParameter(My.Resources.Parameters.NonMerchandiseAmount, NonMerchandiseAmount)
                        com.AddParameter(My.Resources.Parameters.TaxAmount, TaxAmount)
                        com.AddParameter(My.Resources.Parameters.Discount, Discount)
                        com.AddParameter(My.Resources.Parameters.DiscountSupervisor, DiscountSupervisor)
                        com.AddParameter(My.Resources.Parameters.TotalSaleAmount, TotalSaleAmount)
                        com.AddParameter(My.Resources.Parameters.AccountNumber, AccountNumber)
                        com.AddParameter(My.Resources.Parameters.AccountCardNumber, AccountCardNumber)
                        com.AddParameter(My.Resources.Parameters.PCCollection, PCCollection)
                        com.AddParameter(My.Resources.Parameters.FromDCOrders, FromDCOrders)
                        com.AddParameter(My.Resources.Parameters.TransactionComplete, TransactionComplete)
                        com.AddParameter(My.Resources.Parameters.EmployeeDiscountOnly, EmployeeDiscountOnly)
                        com.AddParameter(My.Resources.Parameters.RefundCashierNo, RefundCashierNo)
                        com.AddParameter(My.Resources.Parameters.RefundSupervisor, RefundSupervisor)
                        com.AddParameter(My.Resources.Parameters.Parked, Parked)
                        com.AddParameter(My.Resources.Parameters.VATRate1, VATRate1)
                        com.AddParameter(My.Resources.Parameters.VATRate2, VATRate2)
                        com.AddParameter(My.Resources.Parameters.VATRate3, VATRate3)
                        com.AddParameter(My.Resources.Parameters.VATRate4, VATRate4)
                        com.AddParameter(My.Resources.Parameters.VATRate5, VATRate5)
                        com.AddParameter(My.Resources.Parameters.VATRate6, VATRate6)
                        com.AddParameter(My.Resources.Parameters.VATRate7, VATRate7)
                        com.AddParameter(My.Resources.Parameters.VATRate8, VATRate8)
                        com.AddParameter(My.Resources.Parameters.VATRate9, VATRate9)
                        com.AddParameter(My.Resources.Parameters.VATSymb1, VATSymb1)
                        com.AddParameter(My.Resources.Parameters.VATSymb2, VATSymb2)
                        com.AddParameter(My.Resources.Parameters.VATSymb3, VATSymb3)
                        com.AddParameter(My.Resources.Parameters.VATSymb4, VATSymb4)
                        com.AddParameter(My.Resources.Parameters.VATSymb5, VATSymb5)
                        com.AddParameter(My.Resources.Parameters.VATSymb6, VATSymb6)
                        com.AddParameter(My.Resources.Parameters.VATSymb7, VATSymb7)
                        com.AddParameter(My.Resources.Parameters.VATSymb8, VATSymb8)
                        com.AddParameter(My.Resources.Parameters.VATSymb9, VATSymb9)
                        com.AddParameter(My.Resources.Parameters.XVATAmnt1, XVATAmnt1)
                        com.AddParameter(My.Resources.Parameters.XVATAmnt2, XVATAmnt2)
                        com.AddParameter(My.Resources.Parameters.XVATAmnt3, XVATAmnt3)
                        com.AddParameter(My.Resources.Parameters.XVATAmnt4, XVATAmnt4)
                        com.AddParameter(My.Resources.Parameters.XVATAmnt5, XVATAmnt5)
                        com.AddParameter(My.Resources.Parameters.XVATAmnt6, XVATAmnt6)
                        com.AddParameter(My.Resources.Parameters.XVATAmnt7, XVATAmnt7)
                        com.AddParameter(My.Resources.Parameters.XVATAmnt8, XVATAmnt8)
                        com.AddParameter(My.Resources.Parameters.XVATAmnt9, XVATAmnt9)
                        com.AddParameter(My.Resources.Parameters.VATAmnt1, VATAmnt1)
                        com.AddParameter(My.Resources.Parameters.VATAmnt2, VATAmnt2)
                        com.AddParameter(My.Resources.Parameters.VATAmnt3, VATAmnt3)
                        com.AddParameter(My.Resources.Parameters.VATAmnt4, VATAmnt4)
                        com.AddParameter(My.Resources.Parameters.VATAmnt5, VATAmnt5)
                        com.AddParameter(My.Resources.Parameters.VATAmnt6, VATAmnt6)
                        com.AddParameter(My.Resources.Parameters.VATAmnt7, VATAmnt7)
                        com.AddParameter(My.Resources.Parameters.VATAmnt8, VATAmnt8)
                        com.AddParameter(My.Resources.Parameters.VATAmnt9, VATAmnt9)
                        com.AddParameter(My.Resources.Parameters.RefundManager, RefundManager)
                        com.AddParameter(My.Resources.Parameters.TenderOverrideCode, TenderOverrideCode)
                        com.AddParameter(My.Resources.Parameters.Unparked, Unparked)
                        com.AddParameter(My.Resources.Parameters.TransactionOnLine, TransactionOnLine)
                        com.AddParameter(My.Resources.Parameters.TokensPrinted, TokensPrinted)
                        com.AddParameter(My.Resources.Parameters.ColleagueNumber, ColleagueNumber)
                        com.AddParameter(My.Resources.Parameters.SaveStatus, SaveStatus)
                        com.AddParameter(My.Resources.Parameters.SaveSequence, SaveSequence)
                        com.AddParameter(My.Resources.Parameters.CBBUpdate, CBBUpdate)
                        com.AddParameter(My.Resources.Parameters.DiscountCardNo, DiscountCardNo)
                        com.AddParameter(My.Resources.Parameters.RTI, RTI)
                        LinesUpdated += com.ExecuteNonQuery()

                End Select
            End Using
            Return LinesUpdated

        End Function

        Public Function Persist(ByVal con As Connection) As Integer

            If Not ExistsInDB Then
                Return PersistNew(con)
            Else
                Dim LinesUpdated As Integer = 0

                Using com As New Command(con)
                    Select Case con.DataProvider
                        Case DataProvider.Odbc

                            Dim sb As New StringBuilder
                            sb.Append("Update into DLTOTS Set ")
                            sb.Append("CASH = ?,[TIME] = ?,SUPV = ?,TCOD = ?,[OPEN] = ?,MISC = ?,DESCR = ?,ORDN = ?,")
                            sb.Append("ACCT = ?,VOID = ?,VSUP = ?,TMOD = ?,[PROC] = ?,DOCN = ?,SUSE = ?,STOR = ?,MERC = ?,NMER = ?,TAXA = ?,DISC = ?,DSUP = ?,")
                            sb.Append("TOTL = ?,ACCN = ?,[CARD] = ?,AUPD = ?,BACK = ?,ICOM = ?,IEMP = ?,RCAS = ?,RSUP = ?,PARK = ?,RMAN = ?,TOCD = ?,PKRC = ?,")
                            sb.Append("REMO = ?,GTPN = ?,CCRD = ?,SSTA = ?,SSEQ = ?,CBBU = ?,CARD_NO = ?,RTI = ? ")
                            sb.Append("WHERE DATE1=? AND TILL=? AND [TRAN]=?")

                            com.CommandText = sb.ToString
                            com.AddParameter("CashierID", CashierID)
                            com.AddParameter("TransactionTime", TransactionTime)
                            com.AddParameter("SupervisorCashierNo", SupervisorCashierNo)
                            com.AddParameter("TransactionCode", TransactionCode)
                            com.AddParameter("ODCode", ODCode)
                            com.AddParameter("ReasonCode", ReasonCode)
                            com.AddParameter("Description", Description)
                            com.AddParameter("OrderNumber", OrderNumber)
                            com.AddParameter("AccountSale", AccountSale)
                            com.AddParameter("Void", Void)
                            com.AddParameter("VoidSupervisor", VoidSupervisor)
                            com.AddParameter("TrainingMode", TrainingMode)
                            com.AddParameter("Processed", Processed)
                            com.AddParameter("DocumentNumber", DocumentNumber)
                            com.AddParameter("SupervisorUsed", SupervisorUsed)
                            com.AddParameter("StoreNumber", StoreNumber)
                            com.AddParameter("MerchandiseAmount", MerchandiseAmount)
                            com.AddParameter("NonMerchandiseAmount", NonMerchandiseAmount)
                            com.AddParameter("TaxAmount", TaxAmount)
                            com.AddParameter("Discount", Discount)
                            com.AddParameter("DiscountSupervisor", DiscountSupervisor)
                            com.AddParameter("AccountNumber", AccountNumber)
                            com.AddParameter("AccountCardNumber", AccountCardNumber)
                            com.AddParameter("PCCollection", PCCollection)
                            com.AddParameter("FromDCOrders", FromDCOrders)
                            com.AddParameter("TransactionComplete", TransactionComplete)
                            com.AddParameter("EmployeeDiscountOnly", EmployeeDiscountOnly)
                            com.AddParameter("RefundCashierNo", RefundCashierNo)
                            com.AddParameter("RefundSupervisor", RefundSupervisor)
                            com.AddParameter("Parked", Parked)
                            com.AddParameter("RefundManager", RefundManager)
                            com.AddParameter("TenderOverrideCode", TenderOverrideCode)
                            com.AddParameter("Unparked", Unparked)
                            com.AddParameter("TransactionOnLine", TransactionOnLine)
                            com.AddParameter("TokensPrinted", TokensPrinted)
                            com.AddParameter("ColleagueNumber", ColleagueNumber)
                            com.AddParameter("SaveStatus", SaveStatus)
                            com.AddParameter("SaveSequence", SaveSequence)
                            com.AddParameter("CBBUpdate", CBBUpdate)
                            com.AddParameter("DiscountCardNo", Discount)
                            com.AddParameter("RTI", RTI)
                            com.AddParameter("TransactionDate", TransactionDate)
                            com.AddParameter("TillNumber", TillNumber)
                            com.AddParameter("TransactionNumber", TransactionNumber)
                            LinesUpdated += com.ExecuteNonQuery()

                        Case DataProvider.Sql
                            'Added for hubs 2.0
                            com.StoredProcedureName = My.Resources.Procedures.SaleUpdate
                            com.AddParameter(My.Resources.Parameters.TransactionDate, TransactionDate)
                            com.AddParameter(My.Resources.Parameters.TillNumber, TillNumber)
                            com.AddParameter(My.Resources.Parameters.TransactionNumber, TransactionNumber)
                            com.AddParameter(My.Resources.Parameters.TransactionTime, TransactionTime)
                            com.AddParameter(My.Resources.Parameters.CashierID, CashierID)
                            com.AddParameter(My.Resources.Parameters.SupervisorCashierNo, SupervisorCashierNo)
                            com.AddParameter(My.Resources.Parameters.TransactionCode, TransactionCode)
                            com.AddParameter(My.Resources.Parameters.ODCode, ODCode)
                            com.AddParameter(My.Resources.Parameters.ReasonCode, ReasonCode)
                            com.AddParameter(My.Resources.Parameters.Description, Description)
                            com.AddParameter(My.Resources.Parameters.OrderNumber, OrderNumber)
                            com.AddParameter(My.Resources.Parameters.AccountSale, AccountSale)
                            com.AddParameter(My.Resources.Parameters.Void, Void)
                            com.AddParameter(My.Resources.Parameters.VoidSupervisor, VoidSupervisor)
                            com.AddParameter(My.Resources.Parameters.TrainingMode, TrainingMode)
                            com.AddParameter(My.Resources.Parameters.Processed, Processed)
                            com.AddParameter(My.Resources.Parameters.DocumentNumber, DocumentNumber)
                            com.AddParameter(My.Resources.Parameters.SupervisorUsed, SupervisorUsed)
                            com.AddParameter(My.Resources.Parameters.StoreNumber, StoreNumber)
                            com.AddParameter(My.Resources.Parameters.MerchandiseAmount, MerchandiseAmount)
                            com.AddParameter(My.Resources.Parameters.NonMerchandiseAmount, NonMerchandiseAmount)
                            com.AddParameter(My.Resources.Parameters.TaxAmount, TaxAmount)
                            com.AddParameter(My.Resources.Parameters.Discount, Discount)
                            com.AddParameter(My.Resources.Parameters.DiscountSupervisor, DiscountSupervisor)
                            com.AddParameter(My.Resources.Parameters.AccountNumber, AccountNumber)
                            com.AddParameter(My.Resources.Parameters.AccountCardNumber, AccountCardNumber)
                            com.AddParameter(My.Resources.Parameters.PCCollection, PCCollection)
                            com.AddParameter(My.Resources.Parameters.FromDCOrders, FromDCOrders)
                            com.AddParameter(My.Resources.Parameters.TransactionComplete, TransactionComplete)
                            com.AddParameter(My.Resources.Parameters.EmployeeDiscountOnly, EmployeeDiscountOnly)
                            com.AddParameter(My.Resources.Parameters.RefundCashierNo, RefundCashierNo)
                            com.AddParameter(My.Resources.Parameters.RefundSupervisor, RefundSupervisor)
                            com.AddParameter(My.Resources.Parameters.Parked, Parked)
                            com.AddParameter(My.Resources.Parameters.RefundManager, RefundManager)
                            com.AddParameter(My.Resources.Parameters.TenderOverrideCode, TenderOverrideCode)
                            com.AddParameter(My.Resources.Parameters.Unparked, Unparked)
                            com.AddParameter(My.Resources.Parameters.TransactionOnLine, TransactionOnLine)
                            com.AddParameter(My.Resources.Parameters.TokensPrinted, TokensPrinted)
                            com.AddParameter(My.Resources.Parameters.ColleagueNumber, ColleagueNumber)
                            com.AddParameter(My.Resources.Parameters.SaveStatus, SaveStatus)
                            com.AddParameter(My.Resources.Parameters.SaveSequence, SaveSequence)
                            com.AddParameter(My.Resources.Parameters.CBBUpdate, CBBUpdate)
                            com.AddParameter(My.Resources.Parameters.DiscountCardNo, DiscountCardNo)
                            com.AddParameter(My.Resources.Parameters.RTI, RTI)
                            LinesUpdated += com.ExecuteNonQuery()

                    End Select
                End Using
                Return LinesUpdated
            End If

        End Function

        Public Function SelfPersist() As Integer
            Dim LinesUpdated As Integer = 0
            Using con As New Connection
                Try
                    con.StartTransaction()
                    LinesUpdated = PersistNew(con)
                    con.CommitTransaction()
                Catch ex As Exception
                    con.RollbackTransaction()
                End Try
            End Using
            Return LinesUpdated
        End Function

        Public Function PersistTree(ByVal con As Connection) As Integer
            Dim LinesUpdated As Integer = 0
            LinesUpdated += Persist(con)
            For Each SaleLine As SaleLine In SaleLines
                LinesUpdated += SaleLine.Persist(con)
            Next
            For Each SaleCustomer As SaleCustomer In SaleCustomers
                LinesUpdated += SaleCustomer.Persist(con)
            Next

            For Each SP As SalePaid In SaleTenders
                LinesUpdated += SP.Persist(con)
            Next

            Return LinesUpdated
        End Function

        ' Migration from Pervasive to SQL.
        ' BO-120-05.  Task 2075
        Public Function UpdateCashierBalancingTables(ByVal con As Connection) As Integer
            Dim Linesupdated As Integer = 0

            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        ' No functionality available for Pervasive/odbc
                    Case DataProvider.Sql
                        com.StoredProcedureName = My.Resources.Procedures.UpdateCashBalCashier
                        com.AddParameter(My.Resources.Parameters.TransactionDate, TransactionDate)
                        com.AddParameter(My.Resources.Parameters.TillNumber, TillNumber)
                        com.AddParameter(My.Resources.Parameters.TransactionNumber, TransactionNumber)
                        Linesupdated += com.ExecuteNonQuery()
                        com.StoredProcedureName = My.Resources.Procedures.UpdateCashBalCashTen
                        Linesupdated += com.ExecuteNonQuery()
                End Select
            End Using

        End Function
#End Region

    End Class

#End Region

#Region "Class SaleHeaderCollection"

    Public Class SaleHeaderCollection
        Inherits BaseCollection(Of SaleHeader)

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub LoadForDateTillTran(ByVal tranDate As Date, ByVal tillId As String, ByVal tranNo As String)
            Dim dt As DataTable = Qod.DataAccess.SaleGet(tranDate, tillId, tranNo)
            Me.Load(dt)
        End Sub

        Public Sub LoadExistingSale(ByVal DATE1 As Date, ByVal TILL As String, ByVal TRAN As String)
            Dim dt As DataTable

            dt = Qod.DataAccess.SaleGet(DATE1, TRAN, TILL)

            If dt.Rows.Count > 0 Then
                Me.Load(dt)
                Me.Items(0).LoadSaleLines()

                'do not need to retrive paid lines; please note that this does not work presently
                Me.Items(0).LoadSalePaid()
            End If

        End Sub

        Public Sub LoadExistingSale(ByVal VendaOrderNumber As String)
            Dim dt As DataTable

            dt = Qod.DataAccess.SaleGet(VendaOrderNumber)

            If dt.Rows.Count > 0 Then
                Me.Load(dt)
                Me.Items(0).LoadSaleLines()
            End If

        End Sub

    End Class

#End Region

End Namespace

#End Region


