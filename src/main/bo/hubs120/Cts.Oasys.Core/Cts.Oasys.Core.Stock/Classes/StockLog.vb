﻿Option Explicit On

Imports Cts.Oasys.Core
Imports Cts.Oasys.Data
Imports System.ComponentModel
Imports System.Text

<CLSCompliant(True)> Public Class StockLog
    Inherits Oasys.Core.Base
    Implements IStockLog

#Region "Private Variables"
    Private _PartCode As String
    Private _QuantityOnHand As Integer
    Private _UnitsInOpenReturns As Integer
    Private _MarkDownQuantity As Integer
    Private _WriteOffQuantity As Integer
    Private _NormalSellPrice As Decimal
    Private _UnitsSoldYesterday As Integer
    Private _UnitsSoldThisWeek As Integer
    Private _UnitsSoldThisPeriod As Integer
    Private _UnitsSoldThisYear As Integer
    Private _ValueSoldYesterday As Decimal
    Private _ValueSoldThisWeek As Decimal
    Private _ValueSoldThisPeriod As Decimal
    Private _ValueSoldThisYear As Decimal
    Private _SaleTypeAttribute As String
    Private _StockLogDayNumber As Decimal
    Private _StockLogKey As Integer
    Private _StockLogType As String
    Private _StockLogDate As Date
    Private _StockLogTime As String
    Private _StockLogKeys As String
    Private _StockLogPrice As Decimal
    Private _StockLogCashier As String
    Private _StockLogStartingStock As Decimal
    Private _StockLogStartingOpenReturns As Decimal
    Private _StockLogStartingPrice As Decimal
    Private _StockLogEndingStock As Decimal
    Private _StockLogEndingOpenReturns As Decimal
    Private _StockLogEndingPrice As Decimal
    Private _StockLogStartingMarkDown As Decimal
    Private _StockLogStartingWriteOff As Decimal
    Private _StockLogEndingMarkdown As Decimal
    Private _StockLogEndingWriteOff As Decimal
    Private _StockLogEndingRTI As String
#End Region

#Region "Properties"
    <ColumnMapping("PartCode")> Public Property PartCode() As String Implements IStockLog.PartCode
        Get
            PartCode = _PartCode
        End Get
        Set(ByVal value As String)
            _PartCode = value
        End Set
    End Property

    <ColumnMapping("QuantityOnHand")> Public Property QuantityOnHand() As Integer Implements IStockLog.QuantityOnHand
        Get
            QuantityOnHand = _QuantityOnHand
        End Get
        Set(ByVal value As Integer)
            _QuantityOnHand = value
        End Set
    End Property

    <ColumnMapping("UnitsInOpenReturns")> Public Property UnitsInOpenReturns() As Integer Implements IStockLog.UnitsInOpenReturns
        Get
            UnitsInOpenReturns = _UnitsInOpenReturns
        End Get
        Set(ByVal value As Integer)
            _UnitsInOpenReturns = value
        End Set
    End Property

    <ColumnMapping("MarkDownQuantity")> Public Property MarkDownQuantity() As Integer Implements IStockLog.MarkDownQuantity
        Get
            MarkDownQuantity = _MarkDownQuantity
        End Get
        Set(ByVal value As Integer)
            _MarkDownQuantity = value
        End Set
    End Property

    <ColumnMapping("WriteOffQuantity")> Public Property WriteOffQuantity() As Integer Implements IStockLog.WriteOffQuantity
        Get
            WriteOffQuantity = _WriteOffQuantity
        End Get
        Set(ByVal value As Integer)
            _WriteOffQuantity = value
        End Set
    End Property

    <ColumnMapping("NormalSellPrice")> Public Property NormalSellPrice() As Decimal Implements IStockLog.NormalSellPrice
        Get
            NormalSellPrice = _NormalSellPrice
        End Get
        Set(ByVal value As Decimal)
            _NormalSellPrice = value
        End Set
    End Property

    <ColumnMapping("UnitsSoldYesterday")> Public Property UnitsSoldYesterday() As Integer Implements IStockLog.UnitsSoldYesterday
        Get
            UnitsSoldYesterday = _UnitsSoldYesterday
        End Get
        Set(ByVal value As Integer)
            _UnitsSoldYesterday = value
        End Set
    End Property

    <ColumnMapping("UnitsSoldThisWeek")> Public Property UnitsSoldThisWeek() As Integer Implements IStockLog.UnitsSoldThisWeek
        Get
            UnitsSoldThisWeek = _UnitsSoldThisWeek
        End Get
        Set(ByVal value As Integer)
            _UnitsSoldThisWeek = value
        End Set
    End Property

    <ColumnMapping("UnitsSoldThisPeriod")> Public Property UnitsSoldThisPeriod() As Integer Implements IStockLog.UnitsSoldThisPeriod
        Get
            UnitsSoldThisPeriod = _UnitsSoldThisPeriod
        End Get
        Set(ByVal value As Integer)
            _UnitsSoldThisPeriod = value
        End Set
    End Property

    <ColumnMapping("UnitsSoldThisYear")> Public Property UnitsSoldThisYear() As Integer Implements IStockLog.UnitsSoldThisYear
        Get
            UnitsSoldThisYear = _UnitsSoldThisYear
        End Get
        Set(ByVal value As Integer)
            _UnitsSoldThisYear = value
        End Set
    End Property

    <ColumnMapping("ValueSoldYesterday")> Public Property ValueSoldYesterday() As Decimal Implements IStockLog.ValueSoldYesterday
        Get
            ValueSoldYesterday = _ValueSoldYesterday
        End Get
        Set(ByVal value As Decimal)
            _ValueSoldYesterday = value
        End Set
    End Property

    <ColumnMapping("ValueSoldThisWeek")> Public Property ValueSoldThisWeek() As Decimal Implements IStockLog.ValueSoldThisWeek
        Get
            ValueSoldThisWeek = _ValueSoldThisWeek
        End Get
        Set(ByVal value As Decimal)
            _ValueSoldThisWeek = value
        End Set
    End Property

    <ColumnMapping("ValueSoldThisPeriod")> Public Property ValueSoldThisPeriod() As Decimal Implements IStockLog.ValueSoldThisPeriod
        Get
            ValueSoldThisPeriod = _ValueSoldThisPeriod
        End Get
        Set(ByVal value As Decimal)
            _ValueSoldThisPeriod = value
        End Set
    End Property

    <ColumnMapping("ValueSoldThisYear")> Public Property ValueSoldThisYear() As Decimal Implements IStockLog.ValueSoldThisYear
        Get
            ValueSoldThisYear = _ValueSoldThisYear
        End Get
        Set(ByVal value As Decimal)
            _ValueSoldThisYear = value
        End Set
    End Property

    <ColumnMapping("SaleTypeAttribute")> Public Property SaleTypeAttribute() As String Implements IStockLog.SaleTypeAttribute
        Get
            SaleTypeAttribute = _SaleTypeAttribute
        End Get
        Set(ByVal value As String)
            _SaleTypeAttribute = value
        End Set
    End Property

    <ColumnMapping("MaxKey")> Public Property StockLogKey() As Integer Implements IStockLog.StockLogKey
        Get
            StockLogKey = _StockLogKey
        End Get
        Set(ByVal value As Integer)
            _StockLogKey = value
        End Set
    End Property

    <ColumnMapping("StockLogDayNumber")> Public Property StockLogDayNumber() As Decimal Implements IStockLog.StockLogDayNumber
        Get
            StockLogDayNumber = _StockLogDayNumber
        End Get
        Set(ByVal value As Decimal)
            _StockLogDayNumber = value
        End Set
    End Property

    <ColumnMapping("StockLogType")> Public Property StockLogType() As String Implements IStockLog.StockLogType
        Get
            StockLogType = _StockLogType
        End Get
        Set(ByVal value As String)
            _StockLogType = value
        End Set
    End Property

    <ColumnMapping("StockLogDate")> Public Property StockLogDate() As Date Implements IStockLog.StockLogDate
        Get
            StockLogDate = _StockLogDate
        End Get
        Set(ByVal value As Date)
            _StockLogDate = value
        End Set
    End Property

    <ColumnMapping("StockLogTime")> Public Property StockLogTime() As String Implements IStockLog.StockLogTime
        Get
            StockLogTime = _StockLogTime
        End Get
        Set(ByVal value As String)
            _StockLogTime = value
        End Set
    End Property

    <ColumnMapping("StockLogKeys")> Public Property StockLogKeys() As String Implements IStockLog.StockLogKeys
        Get
            StockLogKeys = _StockLogKeys
        End Get
        Set(ByVal value As String)
            _StockLogKeys = value
        End Set
    End Property

    <ColumnMapping("StockLogCashier")> Public Property StockLogCashier() As String Implements IStockLog.StockLogCashier
        Get
            StockLogCashier = _StockLogCashier
        End Get
        Set(ByVal value As String)
            _StockLogCashier = value
        End Set
    End Property

    <ColumnMapping("StockLogStartingStock")> Public Property StockLogStartingStock() As Decimal Implements IStockLog.StockLogStartingStock
        Get
            StockLogStartingStock = _StockLogStartingStock
        End Get
        Set(ByVal value As Decimal)
            _StockLogStartingStock = value
        End Set
    End Property

    <ColumnMapping("StockLogStartingOpenReturns")> Public Property StockLogStartingOpenReturns() As Decimal Implements IStockLog.StockLogStartingOpenReturns
        Get
            StockLogStartingOpenReturns = _StockLogStartingOpenReturns
        End Get
        Set(ByVal value As Decimal)
            _StockLogStartingStock = value
        End Set
    End Property

    <ColumnMapping("StockLogStartingPrice")> Public Property StockLogStartingPrice() As Decimal Implements IStockLog.StockLogStartingPrice
        Get
            StockLogStartingPrice = _StockLogStartingPrice
        End Get
        Set(ByVal value As Decimal)
            _StockLogStartingPrice = value
        End Set
    End Property

    <ColumnMapping("StockLogEndingStock")> Public Property StockLogEndingStock() As Decimal Implements IStockLog.StockLogEndingStock
        Get
            StockLogEndingStock = _StockLogEndingStock
        End Get
        Set(ByVal value As Decimal)
            _StockLogEndingStock = value
        End Set
    End Property

    <ColumnMapping("StockLogEndingOpenReturns")> Public Property StockLogEndingOpenReturns() As Decimal Implements IStockLog.StockLogEndingOpenReturns
        Get
            StockLogEndingOpenReturns = _StockLogEndingOpenReturns
        End Get
        Set(ByVal value As Decimal)
            _StockLogEndingOpenReturns = value
        End Set
    End Property

    <ColumnMapping("StockLogEndingPrice")> Public Property StockLogEndingPrice() As Decimal Implements IStockLog.StockLogEndingPrice
        Get
            StockLogEndingPrice = _StockLogEndingPrice
        End Get
        Set(ByVal value As Decimal)
            _StockLogEndingPrice = value
        End Set
    End Property

    <ColumnMapping("StockLogStartingMarkDown")> Public Property StockLogStartingMarkDown() As Decimal Implements IStockLog.StockLogStartingMarkDown
        Get
            StockLogStartingMarkDown = _StockLogStartingMarkDown
        End Get
        Set(ByVal value As Decimal)
            _StockLogStartingMarkDown = value
        End Set
    End Property

    <ColumnMapping("StockLogStartingWriteOff")> Public Property StockLogStartingWriteOff() As Decimal Implements IStockLog.StockLogStartingWriteOff
        Get
            StockLogStartingWriteOff = _StockLogStartingWriteOff
        End Get
        Set(ByVal value As Decimal)
            _StockLogStartingWriteOff = value
        End Set
    End Property

    <ColumnMapping("StockLogEndingMarkdown")> Public Property StockLogEndingMarkdown() As Decimal Implements IStockLog.StockLogEndingMarkdown
        Get
            StockLogEndingMarkdown = _StockLogEndingMarkdown
        End Get
        Set(ByVal value As Decimal)
            _StockLogEndingMarkdown = value
        End Set
    End Property

    <ColumnMapping("StockLogEndingWriteOff")> Public Property StockLogEndingWriteOff() As Decimal Implements IStockLog.StockLogEndingWriteOff
        Get
            StockLogEndingWriteOff = _StockLogEndingWriteOff
        End Get
        Set(ByVal value As Decimal)
            _StockLogEndingWriteOff = value
        End Set
    End Property

    <ColumnMapping("StockLogEndingRTI")> Public Property StockLogEndingRTI() As String Implements IStockLog.StockLogEndingRTI
        Get
            StockLogEndingRTI = _StockLogEndingRTI
        End Get
        Set(ByVal value As String)
            _StockLogEndingRTI = value
        End Set
    End Property
#End Region

#Region "Constructors"
    Public Sub New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub
#End Region

#Region "Methods"

    Private Function GetStockLogKeyForSku(ByVal skuNumber As String) As DataTable

        'Call rsStock.Open("select max(TKEY) as ""MaxKey"" from stklog where type <> '99' and dayn > 0 and skun ='" & mPartCode & "'", oConnection, adOpenStatic)
        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        Dim sb As New StringBuilder
                        sb.Append("Select ")
                        sb.Append("Max(TKEY) as MaxKey ")
                        sb.Append(" FROM STKLOG where TYPE <> '99' and DAYN > 0 and SKUN = ? ")

                        com.CommandText = sb.ToString
                        com.AddParameter("SKUN", skuNumber)
                        Return com.ExecuteDataTable

                    Case DataProvider.Sql
                        com.StoredProcedureName = My.Resources.Procedures.StockGetStockLogKey
                        com.AddParameter(My.Resources.Parameters.SkuNumber, skuNumber)
                        Return com.ExecuteDataTable
                End Select
            End Using
        End Using

        Return Nothing
    End Function

    Private Function LoadStockLogLastRecordForSku(ByVal Key As Integer) As DataTable

        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        Dim sb As New StringBuilder
                        sb.Append("SELECT ")
                        sb.Append("st.SKUN		as PartCode,")
                        sb.Append("st.ONHA		as QuantityOnHand,")
                        sb.Append("st.RETQ		as UnitsInOpenReturns,")
                        sb.Append("st.MDNQ		as MarkDownQuantity,")
                        sb.Append("st.WTFQ		as WriteOffQuantity,")
                        sb.Append("st.PRIC		as NormalSellPrice,")
                        sb.Append("st.SALU1	    as UnitsSoldYesterday,")
                        sb.Append("st.SALU2	    as UnitsSoldThisWeek,")
                        sb.Append("st.SALU4	    as UnitsSoldThisPeriod,")
                        sb.Append("st.SALU6	    as UnitsSoldThisYear,")
                        sb.Append("st.SALV1	    as ValueSoldYesterday,")
                        sb.Append("st.SALV2	    as ValueSoldThisWeek,")
                        sb.Append("st.SALV4	    as ValueSoldThisPeriod,")
                        sb.Append("st.SALV6	    as ValueSoldThisYear,")
                        sb.Append("st.SALT		as SaleTypeAttribute,")
                        sb.Append("sl.DATE1	    as StockLogDate,")
                        sb.Append("sl.TIME  	as StockLogTime,")
                        sb.Append("sl.TYPE	    as StockLogType,")
                        sb.Append("sl.KEYS		as StockLogKeys,")
                        sb.Append("sl.EEID		as StockLogCashier,")
                        sb.Append("sl.SSTK		as StockLogStartingStock,")
                        sb.Append("sl.SRET		as StockLogStartingOpenReturns,")
                        sb.Append("sl.SPRI		as StockLogStartingPrice,")
                        sb.Append("sl.ESTK		as StockLogEndingStock,")
                        sb.Append("sl.ERET		as StockLogEndingOpenReturns,")
                        sb.Append("sl.EPRI		as StockLogEndingPrice,")
                        sb.Append("sl.SMDN		as StockLogStartingMarkDown,")
                        sb.Append("sl.SWTF		as StockLogStartingWriteOff,")
                        sb.Append("sl.EMDN		as StockLogEndingMarkdown,")
                        sb.Append("sl.EWTF		as StockLogEndingWriteOff,")
                        sb.Append("sl.RTI		as StockLogEndingRTI, ")
                        sb.Append("sl.DAYN		as StockLogDayNumber, ")
                        sb.Append("sl.TKEY		as MaxKey ")
                        sb.Append("FROM STKLOG sl Inner join STKMAS st on sl.SKUN = st.SKUN ")
                        sb.Append("WHERE sl.TKEY = ? ")

                        com.CommandText = sb.ToString
                        com.AddParameter("TKEY", Key)
                        Return com.ExecuteDataTable

                    Case DataProvider.Sql
                        com.StoredProcedureName = My.Resources.Procedures.StockGetLatestStockLogByKey
                        com.AddParameter(My.Resources.Parameters.TKEY, Key)
                        Return com.ExecuteDataTable

                End Select
            End Using
        End Using
        Return Nothing

    End Function

    Private Function LoadStockDataForSku(ByVal SkuNumber As String) As DataTable

        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        Dim sb As New StringBuilder
                        sb.Append("SELECT ")
                        sb.Append("st.SKUN		as PartCode,")
                        sb.Append("st.ONHA		as QuantityOnHand,")
                        sb.Append("st.RETQ		as UnitsInOpenReturns,")
                        sb.Append("st.MDNQ		as MarkDownQuantity,")
                        sb.Append("st.WTFQ		as WriteOffQuantity,")
                        sb.Append("st.PRIC		as NormalSellPrice,")
                        sb.Append("st.SALU1	    as UnitsSoldYesterday,")
                        sb.Append("st.SALU2	    as UnitsSoldThisWeek,")
                        sb.Append("st.SALU4	    as UnitsSoldThisPeriod,")
                        sb.Append("st.SALU6	    as UnitsSoldThisYear,")
                        sb.Append("st.SALV1	    as ValueSoldYesterday,")
                        sb.Append("st.SALV2	    as ValueSoldThisWeek,")
                        sb.Append("st.SALV4	    as ValueSoldThisPeriod,")
                        sb.Append("st.SALV6	    as ValueSoldThisYear,")
                        sb.Append("st.SALT		as SaleTypeAttribute ")
                        sb.Append("FROM STKMAS st ")
                        sb.Append("WHERE st.SKUN = ? ")
                        com.CommandText = sb.ToString
                        com.AddParameter("SKUN", SkuNumber)
                        Return com.ExecuteDataTable

                    Case DataProvider.Sql
                        com.StoredProcedureName = My.Resources.Procedures.StockGetStkmasDataForStockLog
                        com.AddParameter(My.Resources.Parameters.SkuNumber, SkuNumber)
                        Return com.ExecuteDataTable

                End Select
            End Using
        End Using
        Return Nothing

    End Function

    Public Function InsertStockLogRecord() As Integer

        Dim LinesUpdated As Integer = 0

        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        Dim sb As New StringBuilder
                        sb.Append("INSERT INTO STKLOG (DATE1, TIME, SKUN, TYPE, KEYS, ICOM, EEID, ")
                        sb.Append("SSTK, SRET, SMDN, SWTF, SPRI, ESTK, ERET, EMDN, EWTF, EPRI, DAYN, RTI)")
                        sb.Append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")

                        com.CommandText = sb.ToString
                        com.ClearParamters()
                        com.AddParameter("DATE1", Me._StockLogDate)
                        com.AddParameter("TIME", Me._StockLogTime)
                        com.AddParameter("SKUN", Me._PartCode)
                        com.AddParameter("TYPE", Me._StockLogType)
                        com.AddParameter("KEYS", Me._StockLogKeys)
                        com.AddParameter("ICOM", 0)
                        com.AddParameter("EEID", Me._StockLogCashier)
                        com.AddParameter("SSTK", Me._StockLogStartingStock)
                        com.AddParameter("SRET", Me._StockLogStartingOpenReturns)
                        com.AddParameter("SMDN", Me._StockLogStartingMarkDown)
                        com.AddParameter("SWTF", Me._StockLogStartingWriteOff)
                        com.AddParameter("SPRI", Me._StockLogStartingPrice)
                        com.AddParameter("ESTK", Me._StockLogEndingStock)
                        com.AddParameter("ERET", Me._StockLogEndingOpenReturns)
                        com.AddParameter("EMDN", Me._StockLogEndingMarkdown)
                        com.AddParameter("EWTF", Me._StockLogEndingWriteOff)
                        com.AddParameter("EPRI", Me._StockLogEndingPrice)
                        com.AddParameter("DAYN", Me._StockLogDayNumber)
                        com.AddParameter("RTI", "S")
                        LinesUpdated += com.ExecuteNonQuery

                    Case DataProvider.Sql
                        com.StoredProcedureName = My.Resources.Procedures.InsertStockLogForSku
                        com.AddParameter(My.Resources.Parameters.AdjustmentDate, Me._StockLogDate)
                        com.AddParameter(My.Resources.Parameters.AdjustmentTime, Me._StockLogTime)
                        com.AddParameter(My.Resources.Parameters.SkuNumber, Me._PartCode)
                        com.AddParameter(My.Resources.Parameters.StockLogType, Me._StockLogType)
                        com.AddParameter(My.Resources.Parameters.StockLogKeys, Me._StockLogKeys)
                        com.AddParameter(My.Resources.Parameters.UserId, Me._StockLogCashier)
                        com.AddParameter(My.Resources.Parameters.StartingStock, Me._StockLogStartingStock)
                        com.AddParameter(My.Resources.Parameters.StartingReturns, Me._StockLogStartingOpenReturns)
                        com.AddParameter(My.Resources.Parameters.StartingMarkdown, Me._StockLogStartingMarkDown)
                        com.AddParameter(My.Resources.Parameters.StartingWriteOff, Me._StockLogStartingWriteOff)
                        com.AddParameter(My.Resources.Parameters.StartingPrice, Me._StockLogPrice)
                        com.AddParameter(My.Resources.Parameters.EndingStock, Me._StockLogEndingStock)
                        com.AddParameter(My.Resources.Parameters.EndingReturns, Me._StockLogEndingOpenReturns)
                        com.AddParameter(My.Resources.Parameters.EndingMarkdown, Me._StockLogEndingMarkdown)
                        com.AddParameter(My.Resources.Parameters.EndingWriteOff, Me._StockLogEndingWriteOff)
                        com.AddParameter(My.Resources.Parameters.EndingPrice, Me._StockLogEndingPrice)
                        com.AddParameter(My.Resources.Parameters.DayNumber, Me._StockLogDayNumber)
                        LinesUpdated += com.ExecuteNonQuery

                End Select
            End Using
        End Using
        Return Nothing

    End Function

    Private Function UpdateSTKMASRecord() As Integer
        Dim LinesUpdated As Integer = 0

        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        Dim sb As New StringBuilder

                        'Update stkmas set " & lngQuantityAtHand & ", " & lngMarkDownQuantity & ", " & 
                        'lngUnitsSoldYesterday & ", " & lngUnitsSoldThisWeek & ", " & lngUnitsSoldThisPeriod & "," & 
                        'lngUnitsSoldThisYear & ", " & curValueSoldYesterday & ", " & curValueSoldThisWeek & ", SALV4 = " & curValueSoldThisPeriod & ", SALV6 = " 
                        '& curValueSoldThisYear & " where SKUN = '" & mPartCode & "'")
                        sb.Append("UPDATE STKMAS SET ")
                        sb.Append("ONHA = ?, ")
                        sb.Append("MDNQ = ?, ")
                        sb.Append("SALU1 = ?, ")
                        sb.Append("SALU2 = ?, ")
                        sb.Append("SALU4 = ?, ")
                        sb.Append("SALU6 = ?, ")
                        sb.Append("SALV1 = ?, ")
                        sb.Append("SALV2 = ?, ")
                        sb.Append("SALV4 = ?, ")
                        sb.Append("SALV6 = ? ")
                        sb.Append("WHERE SKUN = ? ")

                        com.CommandText = sb.ToString
                        com.ClearParamters()
                        com.AddParameter("ONHA", Me._QuantityOnHand)
                        com.AddParameter("MDNQ", Me._MarkDownQuantity)
                        com.AddParameter("SALU1", Me._UnitsSoldYesterday)
                        com.AddParameter("SALU2", Me._UnitsSoldThisWeek)
                        com.AddParameter("SALU4", Me._UnitsSoldThisPeriod)
                        com.AddParameter("SALU6", Me._UnitsSoldThisYear)
                        com.AddParameter("SALV1", Me._ValueSoldYesterday)
                        com.AddParameter("SALV2", Me._ValueSoldThisWeek)
                        com.AddParameter("SALV4", Me._ValueSoldThisPeriod)
                        com.AddParameter("SALV6", Me._ValueSoldThisYear)
                        com.AddParameter("SKUN", Me._PartCode)
                        LinesUpdated += com.ExecuteNonQuery
                        Return LinesUpdated

                    Case DataProvider.Sql
                        com.StoredProcedureName = My.Resources.Procedures.StockUpdateSTKMASStkLogValues
                        com.AddParameter(My.Resources.Parameters.ONHA, Me._QuantityOnHand)
                        com.AddParameter(My.Resources.Parameters.MDNQ, Me._MarkDownQuantity)
                        com.AddParameter(My.Resources.Parameters.SALU1, Me._UnitsSoldYesterday)
                        com.AddParameter(My.Resources.Parameters.SALU2, Me._UnitsSoldThisWeek)
                        com.AddParameter(My.Resources.Parameters.SALU4, Me._UnitsSoldThisPeriod)
                        com.AddParameter(My.Resources.Parameters.SALU6, Me._UnitsSoldThisYear)
                        com.AddParameter(My.Resources.Parameters.SALV1, Me._ValueSoldYesterday)
                        com.AddParameter(My.Resources.Parameters.SALV2, Me._ValueSoldThisWeek)
                        com.AddParameter(My.Resources.Parameters.SALV4, Me._ValueSoldThisPeriod)
                        com.AddParameter(My.Resources.Parameters.SALV6, Me._ValueSoldThisYear)
                        com.AddParameter(My.Resources.Parameters.PartCode, Me._PartCode)
                        LinesUpdated += com.ExecuteNonQuery
                        Return LinesUpdated
                End Select
            End Using
        End Using
        Return 0

    End Function

    ''' <summary>
    ''' Public function to return the last StockLog Record
    ''' for the SkuNumber passed in. (It ignores Type 99 records).
    ''' </summary>
    ''' <param name="skuNumber"></param>
    ''' <returns>StockLog Record</returns>
    ''' <remarks>Michael O'Cain - 1st February, 2011</remarks>
    Public Function GetLatestStockLogForSku(ByVal skuNumber As String) As StockLog Implements IStockLog.GetLatestStockLogForSku

        'Get the latest StockLog key for the sku
        Dim dtKey As DataTable = GetStockLogKeyForSku(skuNumber)

        If Not IsDBNull(dtKey.Rows(0).Item(0)) Then
            _StockLogKey = CInt(dtKey.Rows(0).Item(0))
            'Now load the record
            Dim dt As DataTable = LoadStockLogLastRecordForSku(_StockLogKey)
            For Each dr As DataRow In dt.Rows
                Return New StockLog(dr)
            Next
        Else
            _StockLogKey = 0
            'We need to load the stkmas data in that case
            Dim dt As DataTable = LoadStockDataForSku(skuNumber)
            For Each dr As DataRow In dt.Rows
                Return New StockLog(dr)
            Next
        End If

        If _StockLogKey > 0 Then
        End If
        Return Me

    End Function

    ''' <summary>
    ''' Public function CalculateStockMovementForSale this requires information 
    ''' from the sales line and then calculates the stock movement by sku.
    ''' </summary>
    ''' <param name="TranDate"></param>
    ''' <param name="TranTill"></param>
    ''' <param name="TranNumber"></param>
    ''' <param name="TranSeqNo"></param>
    ''' <param name="CashierID"></param>
    ''' <param name="QuantitySold"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CalculateStockMovementForSale(ByVal TranDate As Date, ByVal TranTill As String, ByVal TranNumber As String, ByVal TranSeqNo As String, ByVal CashierID As String, ByVal QuantitySold As Long) As Integer Implements IStockLog.CalculateStockMovementForSale

        Dim LinesUpdated As Integer = 0

        _StockLogStartingStock = _QuantityOnHand
        _StockLogStartingOpenReturns = _UnitsInOpenReturns
        _StockLogStartingMarkDown = _MarkDownQuantity
        _StockLogStartingWriteOff = _WriteOffQuantity
        _StockLogStartingPrice = _NormalSellPrice

        If _StockLogKey > 0 Then
            'See if we need to do a Stock Adjustment 91 if some STKMAS Values don't match the last STKLOG entry for sku
            If _StockLogEndingStock <> _QuantityOnHand Or _
               _StockLogEndingOpenReturns <> _UnitsInOpenReturns Or _
               _StockLogEndingMarkdown <> _MarkDownQuantity Or _
               _StockLogEndingWriteOff <> _WriteOffQuantity Then
                _StockLogKeys = "System Adjustment 91"
                _StockLogType = "91"
                _StockLogCashier = CashierID
                _StockLogDate = Today
                _StockLogTime = Now.ToString("hhmmss")
                _StockLogEndingStock = _QuantityOnHand
                _StockLogEndingOpenReturns = _UnitsInOpenReturns
                _StockLogEndingMarkdown = _MarkDownQuantity
                _StockLogEndingWriteOff = _WriteOffQuantity
                _StockLogEndingPrice = _NormalSellPrice
                _StockLogDayNumber = DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1
                LinesUpdated += InsertStockLogRecord()
            End If
        End If

        If _SaleTypeAttribute <> "D" AndAlso _SaleTypeAttribute <> "I" Then
            _QuantityOnHand = CInt(_QuantityOnHand - QuantitySold)
            _StockLogEndingStock -= QuantitySold
        End If

        'Update the Stock Master table if Stkmas was sucessful
        If UpdateSTKMASRecord() <> 0 Then
            Dim strDate As String = TranDate.ToString("dd-MM-yy")
            strDate = strDate.Replace("-", "/") 'Fix for referral 720 incorrect Keys format for Uptrac code
            _StockLogKeys = strDate & TranTill & TranNumber & TranSeqNo
            If QuantitySold > 0 Then
                _StockLogType = "01"
            Else
                _StockLogType = "02"
            End If

            _StockLogCashier = CashierID
            _StockLogDate = Today
            _StockLogTime = Now.ToString("hhmmss")
            _StockLogEndingStock = _QuantityOnHand
            _StockLogEndingOpenReturns = _UnitsInOpenReturns
            _StockLogEndingMarkdown = _MarkDownQuantity
            _StockLogEndingWriteOff = _WriteOffQuantity
            _StockLogEndingPrice = _NormalSellPrice
            _StockLogDayNumber = DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1
            LinesUpdated += InsertStockLogRecord()

        End If

    End Function

#End Region

End Class
