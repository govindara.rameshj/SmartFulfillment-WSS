﻿Public Interface IStockLog

    Property PartCode() As String
    Property QuantityOnHand() As Integer
    Property UnitsInOpenReturns() As Integer
    Property MarkDownQuantity() As Integer
    Property WriteOffQuantity() As Integer
    Property NormalSellPrice() As Decimal
    Property UnitsSoldYesterday() As Integer
    Property UnitsSoldThisWeek() As Integer
    Property UnitsSoldThisPeriod() As Integer
    Property UnitsSoldThisYear() As Integer
    Property ValueSoldYesterday() As Decimal
    Property ValueSoldThisWeek() As Decimal
    Property ValueSoldThisPeriod() As Decimal
    Property ValueSoldThisYear() As Decimal
    Property SaleTypeAttribute() As String
    Property StockLogKey() As Integer
    Property StockLogDayNumber() As Decimal
    Property StockLogType() As String
    Property StockLogDate() As Date
    Property StockLogTime() As String
    Property StockLogKeys() As String
    Property StockLogCashier() As String
    Property StockLogStartingStock() As Decimal
    Property StockLogStartingOpenReturns() As Decimal
    Property StockLogStartingPrice() As Decimal
    Property StockLogEndingStock() As Decimal
    Property StockLogEndingOpenReturns() As Decimal
    Property StockLogEndingPrice() As Decimal
    Property StockLogStartingMarkDown() As Decimal
    Property StockLogStartingWriteOff() As Decimal
    Property StockLogEndingMarkdown() As Decimal
    Property StockLogEndingWriteOff() As Decimal
    Property StockLogEndingRTI() As String
    Function GetLatestStockLogForSku(ByVal skuNumber As String) As StockLog
    Function CalculateStockMovementForSale(ByVal TranDate As Date, ByVal TranTill As String, ByVal TranNumber As String, ByVal TranSeqNo As String, ByVal CashierID As String, ByVal QuantitySold As Long) As Integer

End Interface
