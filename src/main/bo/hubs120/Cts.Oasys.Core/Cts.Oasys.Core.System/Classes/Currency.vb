﻿Imports Cts.Oasys.Data

Namespace Currency

    <HideModuleName()> Public Module SharedFunctions

        Public Function GetCurrencies() As HeaderCollection
            Dim hc As New HeaderCollection
            hc.LoadAll()
            Return hc
        End Function

        Public Function GetDefaultCurrencyId() As String
            Dim currencies As HeaderCollection = GetCurrencies()
            If currencies.Count > 0 Then
                Return currencies(0).Id
            End If
            Return String.Empty
        End Function

    End Module

    Public Class Header
        Inherits Base

#Region "       Table Fields"
        Private _id As String
        Private _description As String
        Private _symbol As String
        Private _isDefault As Boolean
        Private _displayOrder As Integer

        <ColumnMapping("Id")> Public Property Id() As String
            Get
                Return _id
            End Get
            Set(ByVal value As String)
                _id = value
            End Set
        End Property
        <ColumnMapping("Description")> Public Property Description() As String
            Get
                Return _description
            End Get
            Set(ByVal value As String)
                _description = value
            End Set
        End Property
        <ColumnMapping("Symbol")> Public Property Symbol() As String
            Get
                Return _symbol
            End Get
            Set(ByVal value As String)
                _symbol = value
            End Set
        End Property
        <ColumnMapping("IsDefault")> Public Property IsDefault() As Boolean
            Get
                Return _isDefault
            End Get
            Set(ByVal value As Boolean)
                _isDefault = value
            End Set
        End Property
        <ColumnMapping("DisplayOrder")> Public Property DisplayOrder() As Integer
            Get
                Return _displayOrder
            End Get
            Set(ByVal value As Integer)
                _displayOrder = value
            End Set
        End Property
#End Region

#Region "       Properties"
        Private _lines As LineCollection = Nothing
        Public ReadOnly Property Lines() As LineCollection
            Get
                If _lines Is Nothing Then LoadLines()
                Return _lines
            End Get
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub LoadLines()
            _lines = New LineCollection
            _lines.Load(Me.Id)
        End Sub

    End Class

    Public Class HeaderCollection
        Inherits BaseCollection(Of Header)

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub LoadAll()
            Dim dt As DataTable = DataAccess.CurrencyGet
            Me.Load(dt)
        End Sub

    End Class

    Public Class Line
        Inherits Base

#Region "       Table Fields"
        Private _id As Decimal
        Private _currencyId As String
        Private _tenderId As Integer
        Private _displayText As String
        Private _bullionMultiple As Decimal
        Private _bankingBagLimit As Decimal
        Private _safeMinimum As Decimal

        <ColumnMapping("Id")> Public Property Id() As Decimal
            Get
                Return _id
            End Get
            Set(ByVal value As Decimal)
                _id = value
            End Set
        End Property
        <ColumnMapping("CurrencyId")> Public Property CurrencyId() As String
            Get
                Return _currencyId
            End Get
            Set(ByVal value As String)
                _currencyId = value
            End Set
        End Property
        <ColumnMapping("TenderId")> Public Property TenderId() As Integer
            Get
                Return _tenderId
            End Get
            Set(ByVal value As Integer)
                _tenderId = value
            End Set
        End Property
        <ColumnMapping("DisplayText")> Public Property DisplayText() As String
            Get
                Return _displayText
            End Get
            Set(ByVal value As String)
                _displayText = value
            End Set
        End Property
        <ColumnMapping("BullionMultiple")> Public Property BullionMultiple() As Decimal
            Get
                Return _bullionMultiple
            End Get
            Set(ByVal value As Decimal)
                _bullionMultiple = value
            End Set
        End Property
        <ColumnMapping("BankingBagLimit")> Public Property BankingBagLimit() As Decimal
            Get
                Return _bankingBagLimit
            End Get
            Set(ByVal value As Decimal)
                _bankingBagLimit = value
            End Set
        End Property
        <ColumnMapping("SafeMinimum")> Public Property SafeMinimum() As Decimal
            Get
                Return _safeMinimum
            End Get
            Set(ByVal value As Decimal)
                _safeMinimum = value
            End Set
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function IsMultiple(ByVal value As Decimal) As Boolean
            Return (value Mod Me.Id = 0)
        End Function

    End Class

    Public Class LineCollection
        Inherits BaseCollection(Of Line)

        Public Sub New()
            MyBase.New()
        End Sub

        Public Overloads Sub Load(ByVal currencyId As String)
            Dim dt As DataTable = DataAccess.CurrencyLinesGet(currencyId)
            Me.Load(dt)
        End Sub

    End Class

    <HideModuleName()> Friend Module DataAccess

        Public Function CurrencyGet() As DataTable
            Using con As New Connection
                Using com As Command = con.NewCommand(My.Resources.Procedures.CurrencyGet)
                    Return com.ExecuteDataTable
                End Using
            End Using
        End Function

        Public Function CurrencyGet(ByVal currencyId As String) As DataTable
            Using con As New Connection
                Using com As Command = con.NewCommand(My.Resources.Procedures.CurrencyGet)
                    com.AddParameter(My.Resources.Parameters.CurrencyId, currencyId)
                    Return com.ExecuteDataTable
                End Using
            End Using
        End Function

        Public Function CurrencyLinesGet(ByVal currencyId As String) As DataTable
            Using con As New Connection
                Using com As Command = con.NewCommand(My.Resources.Procedures.CurrencyLinesGet)
                    com.AddParameter(My.Resources.Parameters.CurrencyId, currencyId)
                    Return com.ExecuteDataTable
                End Using
            End Using
        End Function

    End Module

End Namespace