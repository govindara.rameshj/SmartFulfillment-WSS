﻿Imports System.Configuration
Imports Cts.Oasys.Data

Namespace Workstation

    <HideModuleName()> Public Module SharedFunctions

        Public Function GetWorkstations() As WorkstationCollection
            Dim wc As New WorkstationCollection
            wc.LoadAll()
            Return wc
        End Function

        Public Function GetProfileId(ByVal workstationId As Integer) As Integer
            Dim dt As DataTable = DataAccess.WorkstationGet(workstationId)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                Return CInt(dt.Rows(0).Item(GetPropertyName(Function(f As Workstation) f.ProfileId)))
            End If
            Return Nothing
        End Function

        Public Function GetActive(ByVal workstationId As Integer) As Boolean
            Dim dt As DataTable = DataAccess.WorkstationGet(workstationId)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                Return CBool(dt.Rows(0).Item(GetPropertyName(Function(f As Workstation) f.IsActive)))
            End If
            Return False
        End Function

        Public Function GetIdDescription(ByVal workstationId As Integer) As String
            Dim dt As DataTable = DataAccess.WorkstationGet(workstationId)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                Dim w As New Workstation(dt.Rows(0))
                Return w.IdDescription
            End If
            Return String.Empty
        End Function

    End Module

    Public Class Workstation
        Inherits Cts.Oasys.Core.Base

#Region "       Table Fields"
        Private _id As Integer
        Private _description As String
        Private _profileId As Integer
        Private _primaryFunction As String
        Private _dateLastLoggedIn As Date
        Private _isActive As Boolean
        Private _isBarcodeBroken As Boolean
        Private _isTouchScreen As Boolean

        <ColumnMapping("Id")> Public Property Id() As Integer
            Get
                Return _id
            End Get
            Set(ByVal value As Integer)
                _id = value
            End Set
        End Property
        <ColumnMapping("Description")> Public Property Description() As String
            Get
                Return _description
            End Get
            Set(ByVal value As String)
                _description = value
            End Set
        End Property
        <ColumnMapping("ProfileID")> Public Property ProfileId() As Integer
            Get
                Return _profileId
            End Get
            Set(ByVal value As Integer)
                _profileId = value
            End Set
        End Property
        <ColumnMapping("PrimaryFunction")> Public Property PrimaryFunction() As String
            Get
                Return _primaryFunction
            End Get
            Set(ByVal value As String)
                _primaryFunction = value
            End Set
        End Property
        <ColumnMapping("DateLastLoggedIn")> Public Property DateLastLoggedIn() As Date
            Get
                Return _dateLastLoggedIn
            End Get
            Set(ByVal value As Date)
                _dateLastLoggedIn = value
            End Set
        End Property
        <ColumnMapping("IsActive")> Public Property IsActive() As Boolean
            Get
                Return _isActive
            End Get
            Set(ByVal value As Boolean)
                _isActive = value
            End Set
        End Property
        <ColumnMapping("IsBarcodeBroken")> Public Property IsBarcodeBroken() As Boolean
            Get
                Return _isBarcodeBroken
            End Get
            Set(ByVal value As Boolean)
                _isBarcodeBroken = value
            End Set
        End Property
        <ColumnMapping("IsTouchScreen")> Public Property IsTouchScreen() As Boolean
            Get
                Return _isTouchScreen
            End Get
            Set(ByVal value As Boolean)
                _isTouchScreen = value
            End Set
        End Property

        Public ReadOnly Property IdDescription() As String
            Get
                Return Me.Id.ToString("00") & Space(1) & Me.Description
            End Get
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub

    End Class

    Public Class WorkstationCollection
        Inherits BaseCollection(Of Workstation)

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub LoadAll()
            Dim dt As DataTable = DataAccess.WorkstationGet
            Me.Load(dt)
        End Sub

    End Class

    <HideModuleName()> Friend Module DataAccess

        Friend Function WorkstationGet() As DataTable
            Using con As New Connection
                Using com As Command = con.NewCommand(My.Resources.Procedures.WorkstationGet)
                    Return com.ExecuteDataTable
                End Using
            End Using
        End Function

        Friend Function WorkstationGet(ByVal workstationId As Integer) As DataTable
            Using con As New Connection
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        Using com As Command = con.NewCommand("	select ID as Id, Description, SecurityProfileID as ProfileId, PrimaryFunction, DateLastLoggedIn, IsActive, IsBarcodeBroken, UseTouchScreen as IsTouchScreen from WorkStationConfig where ID = ? ")
                            com.AddParameter(My.Resources.Parameters.Id, workstationId)
                            Return com.ExecuteDataTable
                        End Using
                    Case DataProvider.Sql
                        Using com As Command = con.NewCommand(My.Resources.Procedures.WorkstationGet)
                            com.AddParameter(My.Resources.Parameters.Id, workstationId)
                            Return com.ExecuteDataTable
                        End Using
                End Select
            End Using
            Return Nothing
        End Function

    End Module

End Namespace

