﻿Imports Cts.Oasys.Core
Imports Cts.Oasys.Data
Imports System.Text

Namespace Store

    <HideModuleName()> Public Module SharedFunctions

        Public Function GetStore() As Store
            Dim id As Integer = GetId()
            Dim dt As DataTable = DataAccess.StoreGet(id)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                Return New Store(dt.Rows(0))
            End If
            Return Nothing
        End Function

        Public Function GetStore(ByVal id As Integer) As Store
            Dim dt As DataTable = DataAccess.StoreGet(id)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                Return New Store(dt.Rows(0))
            End If
            Return Nothing
        End Function

        Public Function IsCurrentStore(ByVal storeId As Integer) As Boolean
            Dim currentStore As Integer = GetId()
            If storeId = currentStore Then Return True
            If storeId = (currentStore + 8000) Then Return True
            Return False
        End Function

        Public Function IsUKStore() As Boolean
            Dim storeId As Integer = GetId()
            Dim dt As DataTable = DataAccess.StoreGet(storeId)
            Dim newStore As Store
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                newStore = New Store(DataAccess.StoreGet(storeId).Rows(0))
                Return (newStore.CountryCode.ToUpper = "UK")
            End If
            Return False
        End Function

        Public Function GetIdName() As String
            Return SystemOption.GetIdName
        End Function

        Public Function GetId() As Integer
            Return SystemOption.GetStoreId
        End Function

        Public Function GetId4() As Integer
            Dim storeId As Integer = SystemOption.GetStoreId
            If storeId < 1000 Then storeId += 8000
            Return storeId
        End Function

        Public Function GetDaysOpen() As Integer

            Dim storeId As Integer = Parameter.GetInteger(100)
            Dim newStore As New Store(DataAccess.StoreGet(storeId).Rows(0))
            Dim days As Integer = 0
            If newStore.IsOpenMon Then days += 1
            If newStore.IsOpenTue Then days += 1
            If newStore.IsOpenWed Then days += 1
            If newStore.IsOpenThu Then days += 1
            If newStore.IsOpenFri Then days += 1
            If newStore.IsOpenSat Then days += 1
            If newStore.IsOpenSun Then days += 1
            Return days

        End Function

    End Module
    Public Class SaleWeight
        Inherits Cts.Oasys.Core.Base

#Region "Private Variables"
        Private _id As Integer
        Private _dateActive As Date
        Private _week As Integer
        Private _value As Decimal
#End Region

#Region "Properties"
        <ColumnMapping("Id")> Public Property Id() As Integer
            Get
                Return _id
            End Get
            Private Set(ByVal value As Integer)
                _id = value
            End Set
        End Property
        <ColumnMapping("DateActive")> Public Property DateActive() As Date
            Get
                Return _dateActive
            End Get
            Private Set(ByVal value As Date)
                _dateActive = value
            End Set
        End Property
        <ColumnMapping("Week")> Public Property Week() As Integer
            Get
                Return _week
            End Get
            Private Set(ByVal value As Integer)
                _week = value
            End Set
        End Property
        <ColumnMapping("Value")> Public Property Value() As Decimal
            Get
                Return _value
            End Get
            Private Set(ByVal value As Decimal)
                _value = value
            End Set
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub

        Public Shared Function GetActiveWeights(ByVal ids As String) As SaleWeightCollection
            Return New SaleWeightCollection(CInt(ids))
        End Function

    End Class

    Public Class SaleWeightCollection
        Inherits Cts.Oasys.Core.BaseCollection(Of SaleWeight)

        Public Sub New()
            MyBase.New()
        End Sub

        Friend Sub New(ByVal id As Integer)
            Dim dt As DataTable = DataAccess.StoreSaleWeightGetActive(id)
            Me.Load(dt)
        End Sub

        Public Sub AddActiveWeights(ByVal id As Integer)

            'check id not already in collection
            For Each saleWeight As Cts.Oasys.Core.System.Store.SaleWeight In Me.Items
                If saleWeight.Id = id Then Exit Sub
            Next

            'add new items
            Dim dt As DataTable = DataAccess.StoreSaleWeightGetActive(id)
            For Each dr As DataRow In dt.Rows
                Me.Items.Add(New SaleWeight(dr))
            Next

        End Sub

        Public Function Adjuster(ByVal week As Integer) As Decimal

            Dim adj As Decimal = 1
            For Each saleWeight As SaleWeight In Me.Items
                If saleWeight.Week = week AndAlso saleWeight.Value <> 0 Then
                    adj *= saleWeight.Value
                End If
            Next

            If adj = 0 Then adj = 1
            Return adj

        End Function

    End Class

    <HideModuleName()> Friend Module DataAccess

        Friend Function StoreGet(ByVal id As Integer) As DataTable

            If id > 8000 Then id -= 8000

            Dim strmas As DataTable = StoreGetStrMas(id)
            If strmas.Rows.Count = 0 AndAlso id = GetId() Then  'The local store data cannot be found!
                Dim retopt As DataTable
                Using con As New Connection
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                Dim sb As New StringBuilder
                                'Get the data from RETOPT and add it to STRMAS
                                sb.Append("Select TOP 1 ")
                                sb.Append("STOR as Id, ")
                                sb.Append("SNAM as Address1, ")
                                sb.Append("SAD1 as Address2, ")
                                sb.Append("SAD2 as Address3, ")
                                sb.Append("SAD3 as Address4, ")
                                sb.Append("SNAM as Name, ")
                                sb.Append("CTEL as PhoneNumber ")
                                'sb.Append("CountryCode ") 7/2/2011 - Removed as deemed as not required
                                sb.Append("from RETOPT")
                                com.CommandText = sb.ToString
                                retopt = com.ExecuteDataTable
                                If retopt IsNot Nothing AndAlso retopt.Rows.Count > 0 Then
                                    Dim Store As New Store(retopt.Rows(0))
                                    StoreSetStrMas(Store)
                                End If
                            Case DataProvider.Sql
                                'Get the data from RETOPT and add it to Store/STRMAS
                                com.StoredProcedureName = My.Resources.Procedures.LocalStoreDetailsGet
                                com.AddParameter(My.Resources.Parameters.Id, id)
                                retopt = com.ExecuteDataTable
                            Case Else
                                retopt = Nothing
                        End Select
                    End Using
                End Using
                Return retopt
            Else
                Return strmas
            End If

        End Function

        Private Function StoreGetStrMas(ByVal storeId As Integer) As DataTable

            Using con As New Connection
                Using com As New Command(con)
                    Select Case con.DataProvider
                        Case DataProvider.Odbc
                            Dim sb As New StringBuilder
                            sb.Append("Select ")
                            sb.Append("NUMB as Id, ")
                            sb.Append("ADD1 as Address1, ")
                            sb.Append("ADD2 as Address2, ")
                            sb.Append("ADD3 as Address3, ")
                            sb.Append("ADD4 as Address4, ")
                            sb.Append("TILD as Name, ")
                            sb.Append("PHON as PhoneNumber, ")
                            sb.Append("SFAX as FaxNumber, ")
                            sb.Append("MANG as Manager, ")
                            sb.Append("REGC as RegionCode, ")
                            sb.Append("DELC as IsClosed ")
                            'sb.Append("CountryCode ") 7/2/2011 - Deemed as not required
                            sb.Append("from STRMAS where NUMB=?")

                            com.CommandText = sb.ToString
                            com.AddParameter("NUMB", storeId.ToString("000"))
                            Return com.ExecuteDataTable

                        Case DataProvider.Sql
                            com.StoredProcedureName = My.Resources.Procedures.StoreGetStore
                            com.AddParameter(My.Resources.Parameters.Id, storeId)
                            Return com.ExecuteDataTable

                        Case Else
                            Return Nothing
                    End Select
                End Using
            End Using

        End Function

        Private Sub StoreSetStrMas(ByVal store As Store)
            Using con As New Connection
                Using com As New Command(con)
                    Select con.DataProvider
                        Case DataProvider.Odbc
                            Dim sb As New StringBuilder
                            sb.Append("Insert into STRMAS (")
                            sb.Append("NUMB, ")
                            sb.Append("ADD1, ")
                            sb.Append("ADD2, ")
                            sb.Append("ADD3, ")
                            sb.Append("ADD4, ")
                            sb.Append("TILD, ")
                            sb.Append("PHON, ")
                            sb.Append("SFAX, ")
                            sb.Append("MANG, ")
                            sb.Append("REGC, ")
                            sb.Append("DELC ")
                            'sb.Append("CountryCode) ")  7/2/2011 - Removed as deemed as no longer required
                            sb.Append("VALUES (?,?,?,?,?,?,?,?,?,?,?) ")

                            com.CommandText = sb.ToString
                            com.AddParameter("NUMB", store.Id)
                            com.AddParameter("ADD1", store.Address1)
                            com.AddParameter("ADD2", store.Address2)
                            com.AddParameter("ADD3", store.Address3)
                            com.AddParameter("ADD4", store.Address4)
                            com.AddParameter("TILD", store.Name)
                            com.AddParameter("PHON", store.PhoneNumber)
                            com.AddParameter("SFAX", store.FaxNumber)
                            com.AddParameter("MANG", store.Manager)
                            com.AddParameter("REGC", store.RegionCode)
                            com.AddParameter("DELC", store.IsClosed)
                            'com.AddParameter("CountryCode", store.CountryCode)
                            com.ExecuteDataTable()
                    End Select
                End Using
            End Using
        End Sub

        Friend Function StoreSaleWeightGetActive(ByVal id As Integer) As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.StoreSaleWeightGetActiveForId)
                    com.AddParameter(My.Resources.Parameters.Id, id)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

    End Module

End Namespace