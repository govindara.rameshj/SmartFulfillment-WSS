﻿Imports Cts.Oasys.Core
Imports System.ComponentModel

Public Class Parameter
    Inherits Base

#Region "   Table Fields"
    Private _id As Integer
    Private _description As String
    Private _integerValue As Integer?
    Private _decimalValue As Decimal?
    Private _stringValue As String
    Private _booleanValue As Boolean
    Private _valueType As Integer

    <ColumnMapping("ParameterID")> Public Property Id() As Integer
        Get
            Return _id
        End Get
        Private Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    <ColumnMapping("Description")> Public Property Description() As String
        Get
            Return _description
        End Get
        Private Set(ByVal value As String)
            _description = value
        End Set
    End Property
    <ColumnMapping("LongValue")> Public Property IntegerValue() As Integer?
        Get
            Return _integerValue
        End Get
        Private Set(ByVal value As Integer?)
            _integerValue = value
        End Set
    End Property
    <ColumnMapping("DecimalValue")> Public Property DecimalValue() As Decimal?
        Get
            Return _decimalValue
        End Get
        Private Set(ByVal value As Decimal?)
            _decimalValue = value
        End Set
    End Property
    <ColumnMapping("StringValue")> Public Property StringValue() As String
        Get
            Return _stringValue
        End Get
        Private Set(ByVal value As String)
            _stringValue = value
        End Set
    End Property
    <ColumnMapping("BooleanValue")> Public Property BooleanValue() As Boolean
        Get
            Return _booleanValue
        End Get
        Private Set(ByVal value As Boolean)
            _booleanValue = value
        End Set
    End Property
    <ColumnMapping("ValueType")> Public Property ValueType() As Integer
        Get
            Return _valueType
        End Get
        Private Set(ByVal value As Integer)
            _valueType = value
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

    Public Shared Function GetInteger(ByVal id As Integer) As Integer
        Dim dt As DataTable = DataAccess.ParameterGet(id)
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Return CInt(dt.Rows(0).Item("LongValue"))
        End If
        Return Nothing
    End Function

    Public Shared Function GetDecimal(ByVal id As Integer) As Decimal
        Dim dt As DataTable = DataAccess.ParameterGet(id)
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Return CDec(dt.Rows(0).Item(GetPropertyName(Function(f As Parameter) f.DecimalValue)))
        End If
        Return Nothing
    End Function

    Public Shared Function GetString(ByVal id As Integer) As String
        Dim dt As DataTable = DataAccess.ParameterGet(id)
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(GetPropertyName(Function(f As Parameter) f.StringValue)).ToString
        End If
        Return Nothing
    End Function

    Public Shared Function GetBoolean(ByVal id As Integer) As Boolean
        Dim dt As DataTable = DataAccess.ParameterGet(id)
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Return CBool(dt.Rows(0).Item(GetPropertyName(Function(f As Parameter) f.BooleanValue)))
        End If
        Return Nothing
    End Function

    Public Shared Function GetAuthorisationParameters() As Boolean()

        Dim auths(15) As Boolean
        Dim params() As Integer = {2100, 2110, 2111, 2112, 2120, 2121, 2122, 2130, 2131, 2132, 2140, 2141, 2142, 2150, 2151, 2152}

        For index As Integer = 0 To params.Length - 1
            auths(index) = Parameter.GetBoolean(params(index))
        Next

        Return auths

    End Function


    Private Enum ParameterId
        QodMonitorSwitch = 3000
        QodVendaStore = 3010
        HolidayDatesFilePath = 143
        eStoreSellingStoreNumber = 3020
        eStoreWhareHouseStoreNumber = 3030
    End Enum



    Public Shared Function IsQodMonitorSwitchedOn() As Boolean
        Return Parameter.GetBoolean(Parameter.ParameterId.QodMonitorSwitch)
    End Function

    Public Shared Function IsQodVendaStore() As Boolean
        Return Parameter.GetBoolean(Parameter.ParameterId.QodVendaStore)
    End Function

    ''' <summary>
    ''' Get the parameter from the database that will return the Estore selling store number for the eStore
    ''' </summary>
    ''' <returns>Estore Selling Store Number </returns>
    ''' <remarks></remarks>
    Public Shared Function EstoreSellingStoreNumber() As Integer
        Dim eStoreSellingNumber As Integer = Parameter.GetInteger(Parameter.ParameterId.eStoreSellingStoreNumber)
        If (eStoreSellingNumber <> 0) Then
            If eStoreSellingNumber < 8000 Then eStoreSellingNumber += 8000
        Else
            eStoreSellingNumber = -1
        End If
        Return eStoreSellingNumber
    End Function

    ''' <summary>
    ''' Get the parameter from the database that will return the Estore selling store number for the eStore Warehouse
    ''' </summary>
    ''' <returns>Estore Selling Store Warehouse Number</returns>
    ''' <remarks></remarks>
    Public Shared Function EstoreWarehouseNumber() As Integer
        Dim eStoreWarehouseStoreNumber As Integer = Parameter.GetInteger(Parameter.ParameterId.eStoreWhareHouseStoreNumber)
        If (eStoreWarehouseStoreNumber <> 0) Then
            If eStoreWarehouseStoreNumber < 8000 Then eStoreWarehouseStoreNumber += 8000
        Else
            eStoreWarehouseStoreNumber = -1
        End If
        Return eStoreWarehouseStoreNumber
    End Function

    Public Shared Function GetHolidayDatesFilePath() As String
        Dim holidays As String = Parameter.GetString(Parameter.ParameterId.HolidayDatesFilePath)
        If (holidays Is Nothing) OrElse (holidays.Length = 0) Then holidays = "C:\holidays.txt"
        Return holidays
    End Function

End Class