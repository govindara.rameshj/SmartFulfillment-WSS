﻿Imports Cts.Oasys.Core
Imports System.ComponentModel

Public Class ActivityLog
    Inherits Base

#Region "       Table Fields"
    Private _id As String
    Private _appStart As Date
    Private _appEnd As Date?
    Private _menuId As Integer
    Private _userId As Integer
    Private _workstationId As Integer
    Private _loggedIn As Boolean
    Private _loggedOutForced As Boolean

    <ColumnMapping("Id")> Public Property Id() As String
        Get
            Return _id
        End Get
        Set(ByVal value As String)
            _id = value
        End Set
    End Property
    <ColumnMapping("AppStart")> Public Property AppStart() As Date
        Get
            Return _appStart
        End Get
        Set(ByVal value As Date)
            _appStart = value
        End Set
    End Property
    <ColumnMapping("AppEnd")> Public Property AppEnd() As Date?
        Get
            Return _appEnd
        End Get
        Set(ByVal value As Date?)
            _appEnd = value
        End Set
    End Property
    <ColumnMapping("MenuId")> Public Property MenuId() As Integer
        Get
            Return _menuId
        End Get
        Set(ByVal value As Integer)
            _menuId = value
        End Set
    End Property
    <ColumnMapping("UserId")> Public Property UserId() As Integer
        Get
            Return _userId
        End Get
        Set(ByVal value As Integer)
            _userId = value
        End Set
    End Property
    <ColumnMapping("WorkstationId")> Public Property WorkstationId() As Integer
        Get
            Return _workstationId
        End Get
        Set(ByVal value As Integer)
            _workstationId = value
        End Set
    End Property
    <ColumnMapping("LoggedIn")> Public Property LoggedIn() As Boolean
        Get
            Return _loggedIn
        End Get
        Set(ByVal value As Boolean)
            _loggedIn = value
        End Set
    End Property
    <ColumnMapping("LoggedOutForced")> Public Property LoggedOutForced() As Boolean
        Get
            Return _loggedOutForced
        End Get
        Set(ByVal value As Boolean)
            _loggedOutForced = value
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' Records application started in activity log and returns log id or nothing if failed
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Function RecordAppStarted(ByVal menuId As Integer, ByVal userId As Integer, ByVal workstationId As Integer) As Integer
        Dim log As New ActivityLog
        log.MenuId = menuId
        log.UserId = userId
        log.WorkstationId = userId
        log.LoggedIn = True
        log.LoggedOutForced = False

        If DataAccess.ActivityLogInsert(log) > 0 Then
            Return CInt(log.Id)
        End If
        Return Nothing
    End Function

    ''' <summary>
    ''' Records application ended in activity log, returning number of records affected
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Function RecordAppEnded(ByVal Id As Integer) As Integer
        Return DataAccess.ActivityLogEnded(Id)
    End Function

    ''' <summary>
    ''' Records user logging in in activity log returning number of records affected
    ''' </summary>
    ''' <param name="userId"></param>
    ''' <param name="workstationId"></param>
    ''' <remarks></remarks>
    Public Shared Function RecordUserLogIn(ByVal userId As Integer, ByVal workStationId As Integer) As Integer
        Dim log As New ActivityLog
        log.MenuId = 0
        log.UserId = userId
        log.WorkstationId = userId
        log.LoggedIn = True
        log.LoggedOutForced = False
        Return DataAccess.ActivityLogInsert(log)
    End Function

    ''' <summary>
    ''' Records user logging out in activity log returning number of records affected
    ''' </summary>
    ''' <param name="userId"></param>
    ''' <param name="workstationId"></param>
    ''' <remarks></remarks>
    Public Shared Function RecordUserLogOut(ByVal userId As Integer, ByVal workStationId As String, ByVal logOutforced As Boolean) As Integer
        Dim log As New ActivityLog
        log.MenuId = 0
        log.UserId = userId
        log.WorkstationId = userId
        log.LoggedIn = False
        log.LoggedOutForced = logOutforced
        Return DataAccess.ActivityLogInsert(log)
    End Function

End Class