﻿Imports System.Linq.Expressions
Imports System.Text

<HideModuleName()> Public Module Common

    Public ThisStore As New System.Store.Store(0) 'Passing 0 gets the local store!
    Public AllStores As New System.Store.StoreCollection
    Public ConfigXMLDoc As New ConfigXMLDocument

    Public Class ConfigXMLDocument
        Inherits Xml.XmlDocument

        Public Path As String = ""

        Public Sub New()
            MyBase.New()
            Path = FindConfigFile("\Resources\Config.xml")
            Me.Load(Path)
        End Sub

        Private Function FindConfigFile(ByVal fileName As String) As String
            Dim TempName As String = ""
            TempName = AppDomain.CurrentDomain.BaseDirectory & fileName
            If IO.File.Exists(TempName) Then Return TempName
            TempName = "C:\Program Files\CTS Retail\Oasys3" & fileName
            If IO.File.Exists(TempName) Then Return TempName
            TempName = "C:\Program Files\CTS\Oasys3" & fileName
            If IO.File.Exists(TempName) Then Return TempName
            TempName = "C:\Projects\HUBS 2.0\Back Office\Staging Area" & fileName
            If IO.File.Exists(TempName) Then Return TempName
            TempName = "C:\Projects\Wickes SS\HUBS 2.0\Back Office\Staging Area" & fileName
            If IO.File.Exists(TempName) Then Return TempName
            Return TempName
        End Function
    End Class
    Public Enum BaseClassState
        Unchanged = 0
        Modified
        Added
        Deleted
    End Enum

    ''' <summary>
    ''' Gets name of property, parameter syntax as (function (f as 'classname') f.'propertyname')
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <typeparam name="R"></typeparam>
    ''' <param name="expression"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPropertyName(Of T, R)(ByVal expression As Expression(Of Func(Of T, R))) As String
        Dim memberExpression As MemberExpression = DirectCast(expression.Body, MemberExpression)
        Return (memberExpression.Member.Name)
    End Function

    ''' <summary>
    ''' Concetenates given array of strings into comma separated string
    ''' </summary>
    ''' <param name="strings"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Concetenate(ByVal strings As String()) As String

        Dim sb As New StringBuilder
        Dim first As Boolean = True

        For Each s As String In strings
            If s IsNot Nothing AndAlso s.Trim.Length > 0 Then
                If Not first Then sb.Append(", ")
                sb.Append(s.Trim)
                first = False
            End If
        Next

        Return sb.ToString

    End Function

    ''' <summary>
    ''' Concetenates given array of strings using new lines
    ''' </summary>
    ''' <param name="strings"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ConcetenateNewLines(ByVal strings As String()) As String

        Dim sb As New StringBuilder
        Dim first As Boolean = True

        For Each s As String In strings
            If s IsNot Nothing AndAlso s.Trim.Length > 0 Then
                If Not first Then sb.Append(Environment.NewLine)
                sb.Append(s.Trim)
                first = False
            End If
        Next

        Return sb.ToString

    End Function

    ''' <summary>
    ''' Returns string representation of input string with spaces inserted before any uppercase characters
    ''' </summary>
    ''' <param name="input"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CapitalSpaceInsert(ByVal input As String) As String

        Dim sb As New StringBuilder

        If input IsNot Nothing AndAlso input.Length > 0 Then
            For index As Integer = 0 To input.Length - 1
                If index > 1 Then
                    If Char.IsUpper(input.Chars(index)) Then sb.Append(Space(1))
                End If
                sb.Append(input.Chars(index))
            Next
        End If

        Return sb.ToString

    End Function


    Public Function StringIsSomething(ByVal testString As String) As Boolean
        Return (testString IsNot Nothing AndAlso testString.Length > 0)
    End Function

End Module

Namespace Rti

    Public Enum State
        None = 0
        Pending
        Sending
        Completed
    End Enum

    Public Structure Description
        Private _value As String
        Public Shared None As String = ""
        Public Shared Pending As String = "N"
        Public Shared Sending As String = "S"
        Public Shared Completed As String = "C"

        Public Shared Function GetDescription(ByVal rtiState As State) As String
            Select Case rtiState
                Case State.None : Return Description.None
                Case State.Pending : Return Description.Pending
                Case State.Sending : Return Description.Sending
                Case State.Completed : Return Description.Completed
                Case Else : Return String.Empty
            End Select
        End Function

    End Structure

End Namespace