﻿Imports System.ComponentModel

Public MustInherit Class BaseCollection(Of T As Base)
    Inherits BindingList(Of T)
    Implements IDisposable

    Public Sub New()
    End Sub

    Public Sub Load(ByVal dt As DataTable)
        If dt Is Nothing Then Exit Sub
        For Each dr As DataRow In dt.Rows
            Dim b As T = Me.AddNew
            'b.ClassState = BaseClassState.Unchanged
            b.Load(dr)
        Next
    End Sub

    Overloads Sub Add(ByVal item As T)
        'item.ClassState = BaseClassState.Added
        MyBase.Add(item)
    End Sub

    Overloads Function AddNew() As T
        Dim item As T = MyBase.AddNew
        'item.ClassState = BaseClassState.Added
        Return item
    End Function

    Public Overridable Sub ClearAdded()
        If Me.Items.Count = 0 Then Exit Sub
        For index As Integer = Me.Items.Count - 1 To 0 Step -1
            'If Me.Items(index).ClassState = BaseClassState.Added Then
            '    Me.Items.RemoveAt(index)
            'End If
        Next
    End Sub


    'Public Sub BeginEdit()
    '    For Each item As T In Me.Items
    '        item.BeginEdit()
    '    Next
    'End Sub

    'Public Sub CancelEdit()
    '    For Each item As T In Me.Items
    '        item.CancelEdit()
    '    Next
    'End Sub

    'Public Sub EndEdit()
    '    For Each item As T In Me.Items
    '        item.EndEdit()
    '    Next
    'End Sub


#Region " IDisposable Support "
    Private disposedValue As Boolean = False        ' To detect redundant calls
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                Me.Items.Clear()
            End If
        End If
        Me.disposedValue = True
    End Sub
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class