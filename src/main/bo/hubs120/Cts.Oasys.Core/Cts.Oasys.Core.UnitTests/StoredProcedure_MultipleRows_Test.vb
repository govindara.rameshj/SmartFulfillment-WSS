﻿Imports System
Imports System.Data
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public MustInherit Class StoredProcedure_MultipleRows_Test
    Inherits StoredProcedure_Test

    Private Const TC_KEY_EXISTINGMULTIPLE As String = "ExistingMultiple"
    Private Const TC_KEY_EXISTINGMULTIPLEKEY As String = "ExistingMultipleKey"
    Private Const TC_KEY_STOREDPROCEDUREEXISTINGMULTIPLE As String = "StoredProcedureExistingMultiple"

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    MustOverride Function GetExistingMultipleDataAndKeyWithDynamicSQL(ByRef ExistingMultipleData As System.Data.DataTable, ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean
    MustOverride Overrides Function GetExistingDataAndKeyWithDynamicSQL(ByRef ExistingData As System.Data.DataTable, ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean
    MustOverride Overrides Function GetExistingSQL() As String
    MustOverride Overrides Function GetKeyFieldsNoValueRequired(ByRef KeyFields As Microsoft.VisualBasic.Collection) As Boolean
    MustOverride Overrides Function GetNonExistingKeyWithDynamicSQL(ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean
    MustOverride Overrides Function GetNonExistingSQL() As String
    MustOverride Overrides ReadOnly Property StoredProcedureName() As String

    <TestMethod()> _
    Public Overridable Sub WhenDataExists_StoredProcedureCanGetMultipleRecords()

        If InitialiseTests() Then
            Dim TrySPExistingMultiple As Object = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTINGMULTIPLE, GetType(DataTable))

            If TrySPExistingMultiple IsNot Nothing AndAlso TypeOf TrySPExistingMultiple Is DataTable Then
                Dim SPExistingMultiple As DataTable = CType(TrySPExistingMultiple, DataTable)

                If SPExistingMultiple.Rows.Count < 2 Then
                    Assert.Fail(StoredProcedureName & " cannot retrieve multiple records")
                End If
            Else
                Assert.Inconclusive("Intialisation failed as did not get a datatable using " & StoredProcedureName & " so cannot perform this test")
            End If
        End If
    End Sub

    <TestMethod()> _
    Public Overridable Sub WhenDataExists_StoredProcedureGetsRecordsByKey()
        Dim Message As String = ""

        If HaveDataToCompare(True) Then
            Dim TryExistingMultiple As DataTable = CType(SafeGetTestContextProperty(TC_KEY_EXISTINGMULTIPLE, GetType(DataTable)), DataTable)

            If TryExistingMultiple IsNot Nothing AndAlso TypeOf TryExistingMultiple Is DataTable Then
                Dim ExistingMultiple As DataTable = TryExistingMultiple

                If ExistingMultiple.Rows.Count > 1 Then
                    Dim TrySPExistingMultiple As DataTable = CType(SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTINGMULTIPLE, GetType(DataTable)), DataTable)

                    If TrySPExistingMultiple IsNot Nothing Then
                        Dim SPExistingMultiple As DataTable = TrySPExistingMultiple

                        If SPExistingMultiple.Rows.Count > 1 Then
                            Dim KeyFields As New Collection

                            If GetKeyFieldsNoValueRequired(KeyFields) Then
                                Dim Failed As Boolean

                                For Each ExistingRow As DataRow In ExistingMultiple.Rows
                                    Dim Found As Boolean

                                    For Each SPExistingRow As DataRow In SPExistingMultiple.Rows
                                        Found = True
                                        For Each SPKey As Key In KeyFields
                                            Select Case SPKey.Type
                                                Case FieldType.ftBoolean
                                                    If Not BooleansMatch(ExistingRow.Item(SPKey.DynamicSQLName), SPExistingRow.Item(SPKey.DynamicSQLName), Message) Then
                                                        Found = False
                                                        Exit For
                                                    End If
                                                Case FieldType.ftDate
                                                    If Not DatesMatch(ExistingRow.Item(SPKey.DynamicSQLName), SPExistingRow.Item(SPKey.DynamicSQLName), Message) Then
                                                        Found = False
                                                        Exit For
                                                    End If
                                                Case FieldType.ftDecimal
                                                    If Not DecimalsMatch(ExistingRow.Item(SPKey.DynamicSQLName), SPExistingRow.Item(SPKey.DynamicSQLName), Message) Then
                                                        Found = False
                                                        Exit For
                                                    End If
                                                Case FieldType.ftInteger
                                                    If Not IntegersMatch(ExistingRow.Item(SPKey.DynamicSQLName), SPExistingRow.Item(SPKey.DynamicSQLName), Message) Then
                                                        Found = False
                                                        Exit For
                                                    End If
                                                Case FieldType.ftString
                                                    If Not StringsMatch(ExistingRow.Item(SPKey.DynamicSQLName), SPExistingRow.Item(SPKey.DynamicSQLName), Message) Then
                                                        Found = False
                                                        Exit For
                                                    End If
                                                Case Else
                                                    Assert.Inconclusive("Cannot check field " & SPKey.DynamicSQLName & " as field type specified is no recognised")
                                                    Found = False
                                                    Exit For
                                            End Select
                                        Next
                                        If Found Then
                                            Exit For
                                        End If
                                    Next
                                    If Not Found Then
                                        Assert.Fail(StoredProcedureName & " failed to bring back multiple records that matched on key(s) the records retrieved by dynamic sql.  " & Message)
                                        Failed = True
                                        Exit For
                                    End If
                                Next
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Protected Overrides Function InitialiseTests() As Boolean

        If MyBase.InitialiseTests() Then
            If GetExistingMultiple Then
                If GetExistingMultipleUsingStoredProcedure() Then
                    SafeAddTestContextProperty(TC_KEY_INITIALISED, True)
                Else
                    Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an multiple existing records using stored procedure")
                End If
            Else
                Assert.Inconclusive("Test initialisation failed whilst trying to retrieve multiple existing records using dynamic sql")
            End If
        End If
        InitialiseTests = CBool(SafeGetTestContextProperty(TC_KEY_INITIALISED, GetType(Boolean)))
    End Function


#Region "Common Tests"

    Protected Overrides Function HaveDataToCompare(Optional ByVal MultipleRowsAllowed As Boolean = False) As Boolean

        If InitialiseTests() Then
            Dim TryExisting As DataTable

            If MultipleRowsAllowed Then
                TryExisting = CType(SafeGetTestContextProperty(TC_KEY_EXISTINGMULTIPLE, GetType(DataTable)), DataTable)
            Else
                TryExisting = CType(SafeGetTestContextProperty(TC_KEY_EXISTING, GetType(DataTable)), DataTable)
            End If

            If TryExisting IsNot Nothing AndAlso TypeOf TryExisting Is DataTable Then
                Dim Existing As DataTable = TryExisting

                If Existing.Rows.Count = 1 Or (MultipleRowsAllowed And Existing.Rows.Count > 0) Then
                    Dim TrySPExisting As DataTable

                    If MultipleRowsAllowed Then
                        TrySPExisting = CType(SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTINGMULTIPLE, GetType(DataTable)), DataTable)
                    Else
                        TrySPExisting = CType(SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable)), DataTable)
                    End If

                    If TrySPExisting IsNot Nothing Then
                        Dim SPExisting As DataTable = TrySPExisting

                        If SPExisting.Rows.Count = 1 Or (MultipleRowsAllowed And SPExisting.Rows.Count > 0) Then
                            HaveDataToCompare = True
                        Else
                            Assert.Inconclusive(StoredProcedureName & " failed to return an existing record so this test has nothing to compare")
                        End If
                    Else
                        Assert.Inconclusive(StoredProcedureName & " failed to return a datatable so this test has nothing to compare")
                    End If
                Else
                    Assert.Inconclusive("Dynamic sql failed to return an existing record so this test has nothing to compare against")
                End If
            Else
                Assert.Inconclusive("Dynamic sql failed to return a datatable so this test has nothing to compare against")
            End If
        End If
    End Function
#End Region

    Protected Overridable Function GetExistingMultiple() As Boolean

        Try
            Dim ExistingMultipleData As New DataTable
            Dim Keys As Collection = New Collection

            If GetExistingMultipleDataAndKeyWithDynamicSQL(ExistingMultipleData, Keys) Then
                ' Keep the key for use in retrieval by stored procedure
                SafeAddTestContextProperty(TC_KEY_EXISTINGMULTIPLEKEY, New StoredProcedureKey(Keys))
                SafeAddTestContextProperty(TC_KEY_EXISTINGMULTIPLE, ExistingMultipleData)
                GetExistingMultiple = True
            End If
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record.")
        End Try
    End Function

    Protected Overridable Function GetExistingMultipleUsingStoredProcedure() As Boolean

        Using con As New Connection
            Using com As New Command(con)
                Dim TrySPKey As Object

                com.StoredProcedureName = StoredProcedureName
                TrySPKey = SafeGetTestContextProperty(TC_KEY_EXISTINGMULTIPLEKEY, GetType(StoredProcedureKey))
                If TrySPKey IsNot Nothing AndAlso TypeOf TrySPKey Is StoredProcedureKey Then
                    Dim SPKey As StoredProcedureKey = CType(TrySPKey, StoredProcedureKey)

                    If SPKey.AddParameters(com) Then
                        Dim SPExistingMultiple As DataTable

                        SPExistingMultiple = com.ExecuteDataTable
                        SafeAddTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTINGMULTIPLE, SPExistingMultiple)
                        GetExistingMultipleUsingStoredProcedure = True
                    End If
                End If
            End Using
        End Using
    End Function
End Class
