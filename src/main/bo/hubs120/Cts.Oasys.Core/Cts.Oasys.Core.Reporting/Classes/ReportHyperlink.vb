﻿Public Class ReportHyperlink
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _reportId As Integer
    Private _tableId As Integer
    Private _rowId As Integer
    Private _columnName As String
    Private _value As String
#End Region

#Region "Properties"
    <ColumnMapping("ReportId")> Public Property ReportId() As Integer
        Get
            Return _reportId
        End Get
        Private Set(ByVal value As Integer)
            _reportId = value
        End Set
    End Property
    <ColumnMapping("TableId")> Public Property TableId() As Integer
        Get
            Return _tableId
        End Get
        Private Set(ByVal value As Integer)
            _tableId = value
        End Set
    End Property
    <ColumnMapping("RowId")> Public Property RowId() As Integer
        Get
            Return _rowId
        End Get
        Private Set(ByVal value As Integer)
            _rowId = value
        End Set
    End Property
    <ColumnMapping("ColumnName")> Public Property ColumnName() As String
        Get
            If _columnName Is Nothing Then _columnName = String.Empty
            Return _columnName
        End Get
        Private Set(ByVal value As String)
            _columnName = value
        End Set
    End Property
    <ColumnMapping("Value")> Public Property Value() As String
        Get
            If _value Is Nothing Then _value = String.Empty
            Return _value
        End Get
        Private Set(ByVal value As String)
            _value = value
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

End Class

Public Class ReportHyperlinkCollection
    Inherits BaseCollection(Of ReportHyperlink)

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub LoadHyperlinks(ByVal reportId As Integer, ByVal tableId As Integer)
        Dim dt As DataTable = DataAccess.GetReportHyperlinks(reportId, tableId)
        Me.Load(dt)
    End Sub

End Class
