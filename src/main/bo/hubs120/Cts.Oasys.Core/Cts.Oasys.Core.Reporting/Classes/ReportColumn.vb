﻿Public Class ReportColumn
    Inherits Base

#Region "Private Variables"
    Private _id As Integer
    Private _name As String
    Private _caption As String
    Private _formatType As Integer
    Private _format As String
    Private _isVisible As Boolean
    Private _isBold As Boolean
    Private _isImagePath As Boolean
    Private _minWidth As Integer
    Private _maxWidth As Integer
    Private _alignment As Integer
#End Region

#Region "Properties"
    <ColumnMapping("Id")> Public Property Id() As Integer
        Get
            Return _id
        End Get
        Private Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    <ColumnMapping("Name")> Public Property Name() As String
        Get
            Return _name
        End Get
        Private Set(ByVal value As String)
            _name = value
        End Set
    End Property
    <ColumnMapping("Caption")> Public Property Caption() As String
        Get
            Return _caption
        End Get
        Private Set(ByVal value As String)
            _caption = value
        End Set
    End Property
    <ColumnMapping("FormatType")> Public Property FormatType() As Integer
        Get
            Return _formatType
        End Get
        Private Set(ByVal value As Integer)
            _formatType = value
        End Set
    End Property
    <ColumnMapping("Format")> Public Property Format() As String
        Get
            If _format Is Nothing Then _format = String.Empty
            Return _format
        End Get
        Private Set(ByVal value As String)
            _format = value
        End Set
    End Property
    <ColumnMapping("IsVisible")> Public Property IsVisible() As Boolean
        Get
            Return _isVisible
        End Get
        Private Set(ByVal value As Boolean)
            _isVisible = value
        End Set
    End Property
    <ColumnMapping("IsBold")> Public Property IsBold() As Boolean
        Get
            Return _isBold
        End Get
        Private Set(ByVal value As Boolean)
            _isBold = value
        End Set
    End Property
    <ColumnMapping("IsImagePath")> Public Property IsImagePath() As Boolean
        Get
            Return _isImagePath
        End Get
        Private Set(ByVal value As Boolean)
            _isImagePath = value
        End Set
    End Property
    <ColumnMapping("MinWidth")> Public Property MinWidth() As Integer
        Get
            Return _minWidth
        End Get
        Private Set(ByVal value As Integer)
            _minWidth = value
        End Set
    End Property
    <ColumnMapping("MaxWidth")> Public Property MaxWidth() As Integer
        Get
            Return _maxWidth
        End Get
        Private Set(ByVal value As Integer)
            _maxWidth = value
        End Set
    End Property
    <ColumnMapping("Alignment")> Public Property Alignment() As Integer
        Get
            Return _alignment
        End Get
        Private Set(ByVal value As Integer)
            _alignment = value
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

End Class

Public Class ReportColumnCollection
    Inherits BaseCollection(Of ReportColumn)

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub LoadColumns(ByVal reportId As Integer, ByVal tableId As Integer)
        Dim dt As DataTable = DataAccess.GetReportColumns(reportId, tableId)
        Me.Load(dt)
    End Sub

    Public Function Column(ByVal columnId As Integer) As ReportColumn
        For Each col As ReportColumn In Me.Items
            If col.Id = columnId Then Return col
        Next
        Return Nothing
    End Function

    Public Function Column(ByVal columnName As String) As ReportColumn
        For Each col As ReportColumn In Me.Items
            If col.Name = columnName Then Return col
        Next
        Return Nothing
    End Function

    Public Function ColumnName(ByVal columnId As Integer) As String
        For Each col As ReportColumn In Me.Items
            If col.Id = columnId Then Return col.Name
        Next
        Return Nothing
    End Function

End Class