﻿Public Class ReportParameter
    Inherits Base

#Region "Private Variables"
    Private _id As Integer
    Private _description As String
    Private _name As String
    Private _dataType As SqlDbType
    Private _size As Integer
    Private _lookupValuesProcedure As String
    Private _mask As String
    Private _sequence As Integer
    Private _defaultValue As Object
    Private _allowMultiple As Boolean
    Private _value As Object
#End Region

#Region "Properties"
    <ColumnMapping("Id")> Public Property Id() As Integer
        Get
            Return _id
        End Get
        Private Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    <ColumnMapping("Description")> Public Property Description() As String
        Get
            Return _description
        End Get
        Private Set(ByVal value As String)
            _description = value
        End Set
    End Property
    <ColumnMapping("Name")> Public Property Name() As String
        Get
            Return _name
        End Get
        Private Set(ByVal value As String)
            _name = value
        End Set
    End Property
    <ColumnMapping("DataType")> Public Property DataType() As SqlDbType
        Get
            Return _dataType
        End Get
        Private Set(ByVal value As SqlDbType)
            _dataType = value
        End Set
    End Property
    <ColumnMapping("Size")> Public Property Size() As Integer
        Get
            Return _size
        End Get
        Private Set(ByVal value As Integer)
            _size = value
        End Set
    End Property
    <ColumnMapping("LookupValuesProcedure")> Public Property LookupValuesProcedure() As String
        Get
            Return _lookupValuesProcedure
        End Get
        Private Set(ByVal value As String)
            _lookupValuesProcedure = value
        End Set
    End Property
    <ColumnMapping("Mask")> Public Property Mask() As String
        Get
            If _mask Is Nothing Then _mask = String.Empty
            Return _mask
        End Get
        Private Set(ByVal value As String)
            _mask = value
        End Set
    End Property
    <ColumnMapping("Sequence")> Public Property Sequence() As Integer
        Get
            Return _sequence
        End Get
        Private Set(ByVal value As Integer)
            _sequence = value
        End Set
    End Property
    <ColumnMapping("DefaultValue")> Public Property DefaultValue() As Object
        Get
            Return _defaultValue
        End Get
        Private Set(ByVal value As Object)
            _defaultValue = value
        End Set
    End Property
    <ColumnMapping("AllowMultiple")> Public Property AllowMultiple() As Boolean
        Get
            Return _allowMultiple
        End Get
        Private Set(ByVal value As Boolean)
            _allowMultiple = value
        End Set
    End Property
    Public Property Value() As Object
        Get
            Return _value
        End Get
        Set(ByVal value As Object)
            _value = value
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub

    Public Function GetLookupValues() As DataTable

        Dim dt As New DataTable
        If _lookupValuesProcedure IsNot Nothing Then
            dt = DataAccess.GetProcedureResult(_lookupValuesProcedure)
        End If
        Return dt

    End Function

End Class

Public Class ReportParameterCollection
    Inherits BaseCollection(Of ReportParameter)

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub LoadParameters(ByVal reportId As Integer)
        Dim dt As DataTable = DataAccess.GetReportParameters(reportId)
        Me.Load(dt)
    End Sub

End Class