﻿Public Class ReportSummary
    Inherits Base

#Region "Table Fields"
    Private _reportId As Integer
    Private _tableId As Integer
    Private _id As Integer
    Private _name As String
    Private _summaryType As Integer
    Private _applyToGroups As Boolean
    Private _format As String
    Private _numeratorId As Integer
    Private _numeratorName As String
    Private _denominatorId As Integer
    Private _denominatorName As String

    <ColumnMapping("ReportId")> Public Property ReportId() As Integer
        Get
            Return _reportId
        End Get
        Private Set(ByVal value As Integer)
            _reportId = value
        End Set
    End Property
    <ColumnMapping("TableId")> Public Property TableId() As Integer
        Get
            Return _tableId
        End Get
        Private Set(ByVal value As Integer)
            _tableId = value
        End Set
    End Property
    <ColumnMapping("Id")> Public Property Id() As Integer
        Get
            Return _id
        End Get
        Private Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    <ColumnMapping("Name")> Public Property Name() As String
        Get
            Return _name
        End Get
        Private Set(ByVal value As String)
            _name = value
        End Set
    End Property
    <ColumnMapping("SummaryType")> Public Property SummaryType() As Integer
        Get
            Return _summaryType
        End Get
        Private Set(ByVal value As Integer)
            _summaryType = value
        End Set
    End Property
    <ColumnMapping("ApplyToGroups")> Public Property ApplyToGroups() As Boolean
        Get
            Return _applyToGroups
        End Get
        Private Set(ByVal value As Boolean)
            _applyToGroups = value
        End Set
    End Property
    <ColumnMapping("Format")> Public Property Format() As String
        Get
            Return _format
        End Get
        Private Set(ByVal value As String)
            _format = value
        End Set
    End Property
    <ColumnMapping("NumeratorId")> Public Property NumeratorId() As Integer
        Get
            Return _numeratorId
        End Get
        Set(ByVal value As Integer)
            _numeratorId = value
        End Set
    End Property
    <ColumnMapping("NumeratorName")> Public Property NumeratorName() As String
        Get
            Return _numeratorName
        End Get
        Private Set(ByVal value As String)
            _numeratorName = value
        End Set
    End Property
    <ColumnMapping("DenominatorId")> Public Property DenominatorId() As Integer
        Get
            Return _denominatorId
        End Get
        Set(ByVal value As Integer)
            _denominatorId = value
        End Set
    End Property
    <ColumnMapping("DenominatorName")> Public Property DenominatorName() As String
        Get
            Return _denominatorName
        End Get
        Private Set(ByVal value As String)
            _denominatorName = value
        End Set
    End Property
#End Region

    Private _numeratorValue As Decimal
    Private _denominatorValue As Decimal

    Public Property NumeratorValue() As Decimal
        Get
            Return _numeratorValue
        End Get
        Set(ByVal value As Decimal)
            _numeratorValue = value
        End Set
    End Property
    Public Property DenominatorValue() As Decimal
        Get
            Return _denominatorValue
        End Get
        Set(ByVal value As Decimal)
            _denominatorValue = value
        End Set
    End Property

    Public Sub New()
        MyBase.New()
    End Sub

End Class

Public Class ReportSummaryCollection
    Inherits BaseCollection(Of ReportSummary)

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub LoadSummaries(ByVal reportId As Integer, ByVal tableId As Integer)
        Dim dt As DataTable = DataAccess.GetReportSummaries(reportId, tableId)
        Me.Load(dt)
    End Sub

End Class