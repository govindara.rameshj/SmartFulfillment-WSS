﻿Public Class ReportRelation
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _reportId As Integer
    Private _name As String
    Private _parentTable As String
    Private _parentColumns As String
    Private _childTable As String
    Private _childColumns As String
#End Region

#Region "Properties"
    <ColumnMapping("ReportId")> Public Property ReportId() As Integer
        Get
            Return _reportId
        End Get
        Private Set(ByVal value As Integer)
            _reportId = value
        End Set
    End Property
    <ColumnMapping("Name")> Public Property Name() As String
        Get
            Return _name
        End Get
        Private Set(ByVal value As String)
            _name = value
        End Set
    End Property
    <ColumnMapping("ParentTable")> Public Property ParentTable() As String
        Get
            Return _parentTable
        End Get
        Private Set(ByVal value As String)
            _parentTable = value
        End Set
    End Property
    <ColumnMapping("ParentColumns")> Public Property ParentColumns() As String
        Get
            Return _parentColumns
        End Get
        Private Set(ByVal value As String)
            _parentColumns = value
        End Set
    End Property
    <ColumnMapping("ChildTable")> Public Property ChildTable() As String
        Get
            Return _childTable
        End Get
        Private Set(ByVal value As String)
            _childTable = value
        End Set
    End Property
    <ColumnMapping("ChildColumns")> Public Property ChildColumns() As String
        Get
            Return _childColumns
        End Get
        Private Set(ByVal value As String)
            _childColumns = value
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

End Class

Public Class ReportRelationCollection
    Inherits BaseCollection(Of ReportRelation)

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub LoadRelations(ByVal reportId As Integer)
        Dim dt As DataTable = DataAccess.GetReportRelations(reportId)
        Me.Load(dt)
    End Sub

End Class