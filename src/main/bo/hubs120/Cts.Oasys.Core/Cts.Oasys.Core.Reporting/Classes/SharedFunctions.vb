﻿<HideModuleName()> Public Module SharedFunctions

    Public Function GetReports() As ReportCollection
        Dim reports As New ReportCollection
        reports.LoadReports()
        Return reports
    End Function

End Module
