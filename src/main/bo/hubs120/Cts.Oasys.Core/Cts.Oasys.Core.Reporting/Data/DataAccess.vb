﻿Imports Cts.Oasys.Data

Namespace DataAccess

    <HideModuleName()> Friend Module DataReport

        Friend Function GetReport(ByVal id As Integer) As DataTable
            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.ReportGetReport)
                    com.AddParameter(My.Resources.Parameters.Id, id)
                    Return com.ExecuteDataTable
                End Using
            End Using
        End Function

        Friend Function GetReports() As DataTable
            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.ReportGetReports)
                    Return com.ExecuteDataTable
                End Using
            End Using
        End Function

        Friend Function GetReports(ByVal ids As String) As DataTable
            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.ReportGetReports)
                    com.AddParameter(My.Resources.Parameters.Ids, ids)
                    Return com.ExecuteDataTable
                End Using
            End Using
        End Function


        Friend Function GetReportTables(ByVal reportId As Integer) As DataTable

            Using com As New Command(New Connection, My.Resources.Procedures.ReportGetReportTables)
                com.AddParameter(My.Resources.Parameters.ReportId, reportId)
                Return com.ExecuteDataTable
            End Using

        End Function

        Friend Function GetReportColumns(ByVal reportId As Integer, ByVal tableId As Integer) As DataTable

            Using com As New Command(New Connection, My.Resources.Procedures.ReportGetReportColumns)
                com.AddParameter(My.Resources.Parameters.ReportId, reportId)
                com.AddParameter(My.Resources.Parameters.TableId, tableId)
                Return com.ExecuteDataTable
            End Using

        End Function

        Friend Function GetReportParameters(ByVal reportId As Integer) As DataTable

            Using com As New Command(New Connection, My.Resources.Procedures.ReportGetReportParameters)
                com.AddParameter(My.Resources.Parameters.ReportId, reportId)
                Return com.ExecuteDataTable
            End Using

        End Function

        Friend Function GetReportRelations(ByVal reportId As Integer) As DataTable

            Using com As New Command(New Connection, My.Resources.Procedures.ReportGetReportRelations)
                com.AddParameter(My.Resources.Parameters.ReportId, reportId)
                Return com.ExecuteDataTable
            End Using

        End Function

        Friend Function GetReportGrouping(ByVal reportId As Integer, ByVal tableId As Integer) As DataTable

            Using com As New Command(New Connection, My.Resources.Procedures.ReportGetReportGrouping)
                com.AddParameter(My.Resources.Parameters.ReportId, reportId)
                com.AddParameter(My.Resources.Parameters.TableId, tableId)
                Return com.ExecuteDataTable
            End Using

        End Function

        Friend Function GetReportSummaries(ByVal reportId As Integer, ByVal tableId As Integer) As DataTable

            Using com As New Command(New Connection, My.Resources.Procedures.ReportGetReportSummaries)
                com.AddParameter(My.Resources.Parameters.ReportId, reportId)
                com.AddParameter(My.Resources.Parameters.TableId, tableId)
                Return com.ExecuteDataTable
            End Using

        End Function

        Friend Function GetReportHyperlinks(ByVal reportId As Integer, ByVal tableId As Integer) As DataTable

            Using com As New Command(New Connection, My.Resources.Procedures.ReportGetReportHyperlinks)
                com.AddParameter(My.Resources.Parameters.ReportId, reportId)
                com.AddParameter(My.Resources.Parameters.TableId, tableId)
                Return com.ExecuteDataTable
            End Using

        End Function

    End Module

    <HideModuleName()> Friend Module DataProcedure

        Friend Function GetProcedureDataset(ByVal rep As Report) As DataSet

            Using con As New Connection
                Using com As New Command(con, rep.ProcedureName)
                    For Each param As ReportParameter In rep.Parameters
                        com.AddParameter(param.Name, param.Value, param.DataType, param.Size)
                    Next
                    Return com.ExecuteDataSet
                End Using
            End Using
            Return Nothing

        End Function

        Friend Function GetProcedureResult(ByVal procedureName As String) As DataTable
            Using con As New Connection
                Using com As New Command(con, procedureName)
                    Return com.ExecuteDataTable
                End Using
            End Using
            Return Nothing
        End Function

    End Module

End Namespace


