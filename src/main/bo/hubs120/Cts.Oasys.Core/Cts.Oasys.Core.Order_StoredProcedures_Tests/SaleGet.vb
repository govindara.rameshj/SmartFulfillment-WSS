﻿Imports Cts.Oasys.Data
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

Namespace SaleGetTests

    <TestClass()> _
    Public Class SaleGet

        Private Const ASSEMBLYNAME As String = "Cts.Oasys.Core.Order_StoredProcedures_Tests"
        Private Const STOREDPROCEDURENAME As String = "SaleGet"

        Private Const TC_KEY_ASSEMBLYNAME As String = "AssemblyName"
        Private Const TC_KEY_STOREDPROCEDURENAME As String = "StoredProcedureName"
        Private Const TC_KEY_FOUNDSTOREDPROCEDURE As String = "FoundStoredProcedure"
        Private Const TC_KEY_INITIALISED As String = "Initialised"
        Private Const TC_KEY_EXISTING As String = "Existing"
        Private Const TC_KEY_NONEXISTINGKEY As String = "NonExistingKey"
        Private Const TC_KEY_STOREDPROCEDUREEXISTING As String = "StoredProcedureExisting"
        Private Const TC_KEY_STOREDPROCEDURENONEXISTING As String = "StoredProcedureNonExisting"

        Private testContextInstance As TestContext

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext
            Get
                Return testContextInstance
            End Get
            Set(ByVal value As TestContext)
                testContextInstance = value
            End Set
        End Property

#Region "Additional test attributes"
        '
        ' You can use the following additional attributes as you write your tests:
        '
        ' Use ClassInitialize to run code before running the first test in the class
        <ClassInitialize()> _
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)

            Using con As New Connection
                Using com As New Command(con)
                    Select Case con.DataProvider
                        Case DataProvider.Sql
                            'Assert.IsTrue(InitialiseTests(testContext), "Failed initialising for the " & SafeGetTestContextProperty(testContext, TC_KEY_ASSEMBLYNAME, GetType(String)) & " Stored Procedures tests.")
                        Case Else
                            Assert.Fail("Cannot perform Stored Procedures tests as not connected to a SQL Server database")
                    End Select
                End Using
            End Using
        End Sub
        '
        ' Use ClassCleanup to run code after all tests in a class have run
        ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
        ' End Sub
        '
        ' Use TestInitialize to run code before running each test
        ' <TestInitialize()> Public Sub MyTestInitialize()
        ' End Sub
        '
        ' Use TestCleanup to run code after each test has run
        ' <TestCleanup()> Public Sub MyTestCleanup()
        ' End Sub
        '
#End Region


#Region "Tests"

        <TestMethod()> _
        Public Sub StoredProcedureExists()

            If InitialiseTests() Then
                Assert.IsTrue(CBool(SafeGetTestContextProperty(TC_KEY_FOUNDSTOREDPROCEDURE, GetType(Boolean))), "Stored procedure '" & STOREDPROCEDURENAME & "' does not exist.")
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureGetsDataTable()

            If InitialiseTests() Then
                Assert.IsNotNull(SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable)), String.Format("{0} did not retrieve a datatable", SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String))))
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureGetsAtLeastOneRecord()

            If InitialiseTests() Then
                Dim TrySPExisting As Object = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable))
                Dim SPName As String = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String)).ToString

                If TrySPExisting IsNot Nothing AndAlso TypeOf TrySPExisting Is DataTable Then
                    Dim SPExisting As DataTable = CType(TrySPExisting, DataTable)

                    If SPExisting.Rows.Count = 0 Then
                        Assert.Fail(SPName & " did not retrieve any records")
                    End If
                Else
                    Assert.Inconclusive("Intialisation failed as did not get a datatable using " & SPName & " so cannot perform this test")
                End If
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureGetsOnlyOneRecord()

            If InitialiseTests() Then
                Dim TrySPExisting As Object = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable))
                Dim SPName As String = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String)).ToString

                If TrySPExisting IsNot Nothing AndAlso TypeOf TrySPExisting Is DataTable Then
                    Dim SPExisting As DataTable = CType(TrySPExisting, DataTable)

                    If SPExisting.Rows.Count > 1 Then
                        Assert.Fail(SPName & " retrieved multiple records")
                    End If
                Else
                    Assert.Inconclusive("Intialisation failed as did not get a datatable using " & SPName & " so cannot perform this test")
                End If
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureGetsRecordByKey()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue( _
                                  FieldsMatch("DATE1", Message, FieldType.ftDate) _
                              And FieldsMatch("TRAN", Message, FieldType.ftString) _
                              And FieldsMatch("TILL", Message, FieldType.ftString), _
                              Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordDATE1Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("DATE1", Message, FieldType.ftDate), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordTILLMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("TILL", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordTRANMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("TRAN", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordCASHMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("CASH", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordTIMEMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("TIME", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordSUPVMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("SUPV", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordTCODMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("TCOD", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordOPENMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("OPEN", Message, FieldType.ftInteger), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordMISCMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("MISC", Message, FieldType.ftInteger), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordDESCRMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("DESCR", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordORDNMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("ORDN", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordACCTMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("ACCT", Message, FieldType.ftBoolean), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVOIDMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VOID", Message, FieldType.ftBoolean), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedVSUPMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VSUP", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordTMODMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("TMOD", Message, FieldType.ftBoolean), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordPROCMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("PROC", Message, FieldType.ftBoolean), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordDOCNMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("DOCN", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordSUSEMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("SUSE", Message, FieldType.ftBoolean), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordSTORMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("STOR", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordMERCMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("MERC", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordNMERMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("NMER", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordTAXAMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("TAXA", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordDISCMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("DISC", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordDSUPMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("DSUP", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordTOTLMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("TOTL", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordACCNMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("ACCN", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordCARDMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("CARD", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordAUPDMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("AUPD", Message, FieldType.ftBoolean), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordBACKMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("BACK", Message, FieldType.ftBoolean), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordICOMMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("ICOM", Message, FieldType.ftBoolean), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordIEMPMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("IEMP", Message, FieldType.ftBoolean), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordRCASMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("RCAS", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordRSUPMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("RSUP", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordPARKMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("PARK", Message, FieldType.ftBoolean), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordRMANMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("RMAN", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordTOCDMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("TOCD", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordPKRCMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("PKRC", Message, FieldType.ftBoolean), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordREMOMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("REMO", Message, FieldType.ftBoolean), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordGTPNMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("GTPN", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordCCRDMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("CCRD", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordSSTAMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("SSTA", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordSSEQMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("SSEQ", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordCBBUMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("CBBU", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordCARD_NOMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("CARD_NO", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordRTIMatches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("RTI", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATR1Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VATR1", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATR2Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VATR2", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATR3Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VATR3", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATR4Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VATR4", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATR5Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VATR5", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATR6Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VATR6", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATR7Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VATR7", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATR8Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VATR8", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATR9Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VATR9", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVSYM1Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VSYM1", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVSYM2Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VSYM2", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVSYM3Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VSYM3", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVSYM4Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VSYM4", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVSYM5Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VSYM5", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVSYM6Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VSYM6", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVSYM7Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VSYM7", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVSYM8Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VSYM8", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVSYM9Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VSYM9", Message, FieldType.ftString), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordXVAT1Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("XVAT1", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordXVAT2Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("XVAT2", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordXVAT3Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("XVAT3", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordXVAT4Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("XVAT4", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordXVAT5Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("XVAT5", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordXVAT6Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("XVAT6", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordXVAT7Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("XVAT7", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordXVAT8Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("XVAT8", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordXVAT9Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("XVAT9", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATV1Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VATV1", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATV2Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VATV2", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATV3Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VATV3", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATV4Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VATV4", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATV5Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VATV5", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATV6Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VATV6", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATV7Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VATV7", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATV8Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VATV8", Message, FieldType.ftDecimal), Message)
            End If
        End Sub

        <TestMethod()> _
        Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATV9Matches()
            Dim Message As String = ""

            If HaveDataToCompare() Then
                Assert.IsTrue(FieldsMatch("VATV9", Message, FieldType.ftDecimal), Message)
            End If
        End Sub
#End Region

#Region "Common Tests"

        Private Function HaveDataToCompare() As Boolean

            If InitialiseTests() Then
                Dim TryExisting As DataTable = CType(SafeGetTestContextProperty(TC_KEY_EXISTING, GetType(DataTable)), DataTable)

                If TryExisting IsNot Nothing AndAlso TypeOf TryExisting Is DataTable Then
                    Dim Existing As DataTable = TryExisting

                    If Existing.Rows.Count = 1 Then
                        Dim TrySPExisting As DataTable = CType(SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable)), DataTable)
                        Dim SPName As String = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String)).ToString

                        If TrySPExisting IsNot Nothing Then
                            Dim SPExisting As DataTable = TrySPExisting

                            If SPExisting.Rows.Count = 1 Then
                                HaveDataToCompare = True
                            Else
                                Assert.Inconclusive(SPName & " failed to return an existing record so this test has nothing to compare")
                            End If
                        Else
                            Assert.Inconclusive(SPName & " failed to return a datatable so this test has nothing to compare")
                        End If
                    Else
                        Assert.Inconclusive("Dynamic sql failed to return an existing single record so this test has nothing to compare against")
                    End If
                Else
                    Assert.Inconclusive("Dynamic sql failed to return a datatable so this test has nothing to compare against")
                End If
            End If
        End Function
#End Region

#Region "Initialise Test Context methods"

        Private Function CheckForStoredProcedure() As Boolean

            Try
                Using con As New Connection
                    Using com As New Command(con)
                        Dim spCheck As DataTable

                        com.CommandText = "Select * From sys.objects Where type = 'P' And name = '" & STOREDPROCEDURENAME & "'"
                        spCheck = com.ExecuteDataTable
                        If spCheck IsNot Nothing AndAlso spCheck.Rows.Count = 1 Then
                            SafeAddTestContextProperty(TC_KEY_FOUNDSTOREDPROCEDURE, True)
                        Else
                            SafeAddTestContextProperty(TC_KEY_FOUNDSTOREDPROCEDURE, False)
                        End If
                        CheckForStoredProcedure = True
                    End Using
                End Using
            Catch ex As Exception
                Assert.Inconclusive("Initialisation for tests failed.  Failed in the attempt to check for existence of the stored procedure (" & STOREDPROCEDURENAME & ".")
            End Try
        End Function

        Private Function GetExisting() As Boolean

            Try
                Using con As New SqlConnection(GetConnectionString())
                    con.Open()
                    Using com As New SqlCommand(GetExistingSQL(), con)
                        Dim TranDate As Date = Now
                        Dim ExistingData As DataTable

                        com.Parameters.Add("@TranDate", SqlDbType.Date)
                        ' Find a date which brings back an existing item
                        Do While TranDate > Date.MinValue
                            com.Parameters.Item("@TranDate").Value = TranDate

                            Dim reader As SqlDataReader = com.ExecuteReader

                            ExistingData = New DataTable
                            ExistingData.Load(reader)
                            reader.Close()

                            If ExistingData.Rows.Count = 1 Then
                                SafeAddTestContextProperty(TC_KEY_EXISTING, ExistingData)
                                GetExisting = True
                                Exit Do
                            End If
                            TranDate = DateAdd(DateInterval.Day, -1, TranDate)
                        Loop
                    End Using
                End Using
            Catch ex As Exception
                Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record.")
            End Try
        End Function

        Private Function GetNonExistingKey() As Boolean

            Try
                Using con As New SqlConnection(GetConnectionString())
                    con.Open()
                    Using com As New SqlCommand(GetExistingSQL(), con)
                        Dim TranDate As Date = Now
                        Dim TranNo As Integer
                        Dim TillNo As Integer

                        Dim ReturnedData As DataTable

                        com.Parameters.Add("@TranDate", SqlDbType.Date)
                        com.Parameters.Add("@TranNo", SqlDbType.NChar)
                        com.Parameters.Add("@TillId", SqlDbType.NChar)
                        ' Find a date, tran and till which does not bring back any existing sales transactions
                        Do While TranDate > Date.MinValue And Not GetNonExistingKey
                            com.Parameters.Item("@TranDate").Value = TranDate
                            TranNo = 1
                            Do While TranNo < 9999 And Not GetNonExistingKey
                                com.Parameters.Item("@TranNo").Value = TranNo.ToString.PadLeft(4, "0"c)
                                TillNo = 1
                                Do While TillNo <= 20 And Not GetNonExistingKey
                                    com.Parameters.Item("@TillId").Value = TillNo.ToString.PadLeft(2, "0"c)

                                    Dim reader As SqlDataReader = com.ExecuteReader

                                    ReturnedData = New DataTable
                                    ReturnedData.Load(reader)
                                    reader.Close()

                                    If ReturnedData.Rows.Count = 0 Then
                                        SafeAddTestContextProperty(TC_KEY_NONEXISTINGKEY, New SaleGetKey(TranDate, CStr(TranNo), CStr(TillNo)))
                                        GetNonExistingKey = True
                                    End If
                                    TillNo += 1
                                Loop
                                TranNo += 1
                            Loop
                            TranDate = DateAdd(DateInterval.Day, -1, TranDate)
                        Loop
                    End Using
                End Using
            Catch ex As Exception
                Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record.")
            End Try
        End Function

        Private Function GetExistingUsingStoredProcedure() As Boolean

            Using con As New Connection
                Using com As New Command(con)
                    Dim TryExisting As Object

                    com.StoredProcedureName = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String)).ToString
                    TryExisting = SafeGetTestContextProperty(TC_KEY_EXISTING, GetType(DataTable))
                    If TryExisting IsNot Nothing AndAlso TypeOf TryExisting Is DataTable Then
                        Dim Existing As DataTable = CType(TryExisting, DataTable)
                        Dim SPExisting As DataTable

                        com.AddParameter("@TranDate", Existing.Rows(0).Item("DATE1"))
                        com.AddParameter("@TillId", Existing.Rows(0).Item("TILL"))
                        com.AddParameter("@TranNumber", Existing.Rows(0).Item("TRAN"))
                        SPExisting = com.ExecuteDataTable
                        SafeAddTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, SPExisting)
                        GetExistingUsingStoredProcedure = True
                    End If
                End Using
            End Using
        End Function

        Private Function GetNonExistingUsingStoredProcedure() As Boolean

            Using con As New Connection
                Using com As New Command(con)
                    Dim TrySPKey As Object

                    com.StoredProcedureName = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String)).ToString
                    TrySPKey = SafeGetTestContextProperty(TC_KEY_NONEXISTINGKEY, GetType(SaleGetKey))
                    If TrySPKey IsNot Nothing AndAlso TypeOf TrySPKey Is SaleGetKey Then
                        Dim SPKey As SaleGetKey = CType(TrySPKey, SaleGetKey)
                        Dim SPNonExisting As DataTable

                        com.AddParameter("@TranDate", SPKey.TransactionDate)
                        com.AddParameter("@TillId", SPKey.TillId)
                        com.AddParameter("@TranNumber", SPKey.TransactionNumber)
                        SPNonExisting = com.ExecuteDataTable
                        SafeAddTestContextProperty(TC_KEY_STOREDPROCEDURENONEXISTING, SPNonExisting)
                        GetNonExistingUsingStoredProcedure = True
                    End If
                End Using
            End Using
        End Function

        Private Function GetNonExistingSQL() As String
            Dim sb As New StringBuilder

            sb.Append("SELECT ")
            sb.Append("dt.DATE1,dt.TILL,dt.[TRAN],dt.CASH,dt.[TIME],dt.SUPV,")
            sb.Append("dt.TCOD,dt.[OPEN],dt.MISC,dt.DESCR,dt.ORDN,dt.ACCT,")
            sb.Append("dt.VOID,dt.VSUP,dt.TMOD,dt.[PROC],dt.DOCN,dt.SUSE,")
            sb.Append("dt.STOR,dt.MERC,dt.NMER,dt.TAXA,dt.DISC,dt.DSUP,")
            sb.Append("dt.TOTL,dt.ACCN,dt.[CARD],dt.AUPD,dt.BACK,dt.ICOM, ")
            sb.Append("dt.IEMP,dt.RCAS,dt.RSUP,dt.PARK,dt.RMAN,dt.TOCD,")
            sb.Append("dt.PKRC,dt.REMO,dt.GTPN,dt.CCRD,dt.SSTA,dt.SSEQ,")
            sb.Append("dt.CBBU,dt.CARD_NO,dt.RTI,dt.VATR1,dt.VATR2,dt.VATR3,")
            sb.Append("dt.VATR4,dt.VATR5,dt.VATR6,dt.VATR7,dt.VATR8,dt.VATR9,")
            sb.Append("dt.VSYM1,dt.VSYM2,dt.VSYM3,dt.VSYM4,dt.VSYM5,dt.VSYM6,")
            sb.Append("dt.VSYM7,dt.VSYM8,dt.VSYM9,dt.XVAT1,dt.XVAT2,dt.XVAT3,")
            sb.Append("dt.XVAT4,dt.XVAT5,dt.XVAT6,dt.XVAT7,dt.XVAT8,dt.XVAT9,")
            sb.Append("dt.VATV1,dt.VATV2,dt.VATV3,dt.VATV4,dt.VATV5,dt.VATV6,")
            sb.Append("dt.VATV7,dt.VATV8,dt.VATV9 ")
            sb.Append("FROM DLTOTS dt ")
            sb.Append("WHERE dt.DATE1 = @TranDate ")
            sb.Append("and dt.TRAN = @TranNo ")
            sb.Append("and dt.TILL= @TillId ")

            Return sb.ToString
        End Function

        Private Function GetExistingSQL() As String
            Dim sb As New StringBuilder

            sb.Append("SELECT ")
            sb.Append("dt.DATE1,dt.TILL,dt.[TRAN],dt.CASH,dt.[TIME],dt.SUPV,")
            sb.Append("dt.TCOD,dt.[OPEN],dt.MISC,dt.DESCR,dt.ORDN,dt.ACCT,")
            sb.Append("dt.VOID,dt.VSUP,dt.TMOD,dt.[PROC],dt.DOCN,dt.SUSE,")
            sb.Append("dt.STOR,dt.MERC,dt.NMER,dt.TAXA,dt.DISC,dt.DSUP,")
            sb.Append("dt.TOTL,dt.ACCN,dt.[CARD],dt.AUPD,dt.BACK,dt.ICOM, ")
            sb.Append("dt.IEMP,dt.RCAS,dt.RSUP,dt.PARK,dt.RMAN,dt.TOCD,")
            sb.Append("dt.PKRC,dt.REMO,dt.GTPN,dt.CCRD,dt.SSTA,dt.SSEQ,")
            sb.Append("dt.CBBU,dt.CARD_NO,dt.RTI,dt.VATR1,dt.VATR2,dt.VATR3,")
            sb.Append("dt.VATR4,dt.VATR5,dt.VATR6,dt.VATR7,dt.VATR8,dt.VATR9,")
            sb.Append("dt.VSYM1,dt.VSYM2,dt.VSYM3,dt.VSYM4,dt.VSYM5,dt.VSYM6,")
            sb.Append("dt.VSYM7,dt.VSYM8,dt.VSYM9,dt.XVAT1,dt.XVAT2,dt.XVAT3,")
            sb.Append("dt.XVAT4,dt.XVAT5,dt.XVAT6,dt.XVAT7,dt.XVAT8,dt.XVAT9,")
            sb.Append("dt.VATV1,dt.VATV2,dt.VATV3,dt.VATV4,dt.VATV5,dt.VATV6,")
            sb.Append("dt.VATV7,dt.VATV8,dt.VATV9 ")
            sb.Append("FROM DLTOTS dt ")
            sb.Append("WHERE dt.DATE1 = @TranDate ")
            '        sb.Append("and dt.TRAN = @TranNo ")
            '       sb.Append("and dt.TILL= @TillId ")

            Return sb.ToString
        End Function

        Private Function InitialiseTests() As Boolean

            If TestContext.Properties(TC_KEY_INITIALISED) Is Nothing Then
                SafeAddTestContextProperty(TC_KEY_ASSEMBLYNAME, ASSEMBLYNAME)
                SafeAddTestContextProperty(TC_KEY_STOREDPROCEDURENAME, STOREDPROCEDURENAME)
                If CheckForStoredProcedure() Then
                    If GetExisting() Then
                        If GetNonExistingKey() Then
                            If GetExistingUsingStoredProcedure() Then
                                If GetNonExistingUsingStoredProcedure() Then
                                    SafeAddTestContextProperty(TC_KEY_INITIALISED, True)
                                Else
                                    Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an non existing record using stored procedure")
                                End If
                            Else
                                Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using stored procedure")
                            End If
                        Else
                            Assert.Inconclusive("Test initialisation failed whilst trying to retrieve a key for a non existing record using dynamic sql")
                        End If
                    Else
                        Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using dynamic sql")
                    End If
                Else
                    Assert.Inconclusive("Test initialisation failed whilst trying to check for the stored procedure (" & SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String)).ToString & ")")
                End If
            End If
            InitialiseTests = CBool(SafeGetTestContextProperty(TC_KEY_INITIALISED, GetType(Boolean)))
        End Function

        Private Sub SafeAddTestContextProperty(ByVal PropertyKey As String, ByVal PropertyValue As Object)

            If TestContext.Properties(PropertyKey) IsNot Nothing Then
                TestContext.Properties.Remove(PropertyKey)
            End If
            TestContext.Properties.Add(PropertyKey, PropertyValue)
        End Sub

        Private Function SafeGetTestContextProperty(ByVal PropertyKey As String, ByVal PropertyType As Type) As Object

            SafeGetTestContextProperty = Nothing
            If TestContext IsNot Nothing AndAlso TestContext.Properties(PropertyKey) IsNot Nothing Then
                If PropertyType IsNot Nothing Then
                    Select Case PropertyType.Name
                        Case GetType(String).Name
                            SafeGetTestContextProperty = TestContext.Properties(PropertyKey).ToString()
                        Case GetType(Integer).Name
                            Dim Prop As Integer

                            If Integer.TryParse(TestContext.Properties(PropertyKey).ToString(), Prop) Then
                                SafeGetTestContextProperty = Prop
                            End If
                        Case GetType(Boolean).Name
                            Dim Prop As Boolean

                            If Boolean.TryParse(TestContext.Properties(PropertyKey).ToString(), Prop) Then
                                SafeGetTestContextProperty = Prop
                            End If
                        Case GetType(Date).Name
                            Dim Prop As Date

                            If Date.TryParse(TestContext.Properties(PropertyKey).ToString(), Prop) Then
                                SafeGetTestContextProperty = Prop
                            End If
                        Case GetType(DataTable).Name
                            Dim TryDataTable As Object = TestContext.Properties(PropertyKey)
                            Dim Prop As DataTable

                            If TypeOf TryDataTable Is DataTable Then
                                Prop = CType(TryDataTable, DataTable)
                                SafeGetTestContextProperty = Prop
                            End If
                        Case Else
                            SafeGetTestContextProperty = TestContext.Properties(PropertyKey)
                    End Select
                Else
                    SafeGetTestContextProperty = TestContext.Properties(PropertyKey)
                End If
            End If
        End Function

        'Private Shared Function CheckForStoredProcedure(ByRef testContext As TestContext) As Boolean

        '    Try
        '        Using con As New Connection
        '            Using com As New Command(con)
        '                Dim spCheck As DataTable

        '                com.CommandText = "Select * From sys.objects Where type = 'P' And name = '" & STOREDPROCEDURENAME & "'"
        '                spCheck = com.ExecuteDataTable
        '                If spCheck IsNot Nothing AndAlso spCheck.Rows.Count = 1 Then
        '                    SafeAddTestContextProperty(testContext, TC_KEY_FOUNDSTOREDPROCEDURE, True)
        '                Else
        '                    SafeAddTestContextProperty(testContext, TC_KEY_FOUNDSTOREDPROCEDURE, False)
        '                End If
        '                CheckForStoredProcedure = True
        '            End Using
        '        End Using
        '    Catch ex As Exception
        '        Assert.Inconclusive("Initialisation for tests failed.  Failed in the attempt to check for existence of the stored procedure (" & STOREDPROCEDURENAME & ".")
        '    End Try
        'End Function

        'Private Shared Function GetExisting(ByRef testContext As TestContext) As Boolean

        '    Try
        '        Using con As New SqlConnection(GetConnectionString())
        '            con.Open()
        '            Using com As New SqlCommand(GetExistingSQL(), con)
        '                Dim TranDate As Date = Now
        '                Dim ExistingData As DataTable

        '                com.Parameters.Add("@TranDate", SqlDbType.Date)
        '                ' Find a date which brings back an existing item
        '                Do While TranDate > Date.MinValue
        '                    com.Parameters.Item("@TranDate").Value = TranDate

        '                    Dim reader As SqlDataReader = com.ExecuteReader

        '                    ExistingData = New DataTable
        '                    ExistingData.Load(reader)
        '                    reader.Close()

        '                    If ExistingData.Rows.Count = 1 Then
        '                        SafeAddTestContextProperty(testContext, TC_KEY_EXISTING, ExistingData)
        '                        GetExisting = True
        '                        Exit Do
        '                    End If
        '                    TranDate = DateAdd(DateInterval.Day, -1, TranDate)
        '                Loop
        '            End Using
        '        End Using
        '    Catch ex As Exception
        '        Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record.")
        '    End Try
        'End Function

        'Private Shared Function GetNonExistingKey(ByRef testContext As TestContext) As Boolean
        '    Try
        '        Using con As New SqlConnection(GetConnectionString())
        '            con.Open()
        '            Using com As New SqlCommand(GetExistingSQL(), con)
        '                Dim TranDate As Date = Now
        '                Dim TranNo As Integer
        '                Dim TillNo As Integer

        '                Dim ReturnedData As DataTable

        '                com.Parameters.Add("@TranDate", SqlDbType.Date)
        '                com.Parameters.Add("@TranNo", SqlDbType.NChar)
        '                com.Parameters.Add("@TillId", SqlDbType.NChar)
        '                ' Find a date, tran and till which does not bring back any existing sales transactions
        '                Do While TranDate > Date.MinValue And Not GetNonExistingKey
        '                    com.Parameters.Item("@TranDate").Value = TranDate
        '                    TranNo = 1
        '                    Do While TranNo < 9999 And Not GetNonExistingKey
        '                        com.Parameters.Item("@TranNo").Value = TranNo.ToString.PadLeft(4, "0"c)
        '                        TillNo = 1
        '                        Do While TillNo <= 20 And Not GetNonExistingKey
        '                            com.Parameters.Item("@TillId").Value = TillNo.ToString.PadLeft(2, "0"c)

        '                            Dim reader As SqlDataReader = com.ExecuteReader

        '                            ReturnedData = New DataTable
        '                            ReturnedData.Load(reader)
        '                            reader.Close()

        '                            If ReturnedData.Rows.Count = 0 Then
        '                                SafeAddTestContextProperty(testContext, TC_KEY_NONEXISTINGKEY, New SaleGetKey(TranDate, TranNo, TillNo))
        '                                GetNonExistingKey = True
        '                            End If
        '                            TillNo += 1
        '                        Loop
        '                        TranNo += 1
        '                    Loop
        '                    TranDate = DateAdd(DateInterval.Day, -1, TranDate)
        '                Loop
        '            End Using
        '        End Using
        '    Catch ex As Exception
        '        Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record.")
        '    End Try
        'End Function

        'Private Shared Function GetExistingUsingStoredProcedure(ByRef testContext As TestContext) As Boolean

        '    Using con As New Connection
        '        Using com As New Command(con)
        '            Dim TryExisting As Object

        '            com.StoredProcedureName = SafeGetTestContextProperty(testContext, TC_KEY_STOREDPROCEDURENAME, GetType(String))
        '            TryExisting = SafeGetTestContextProperty(testContext, TC_KEY_EXISTING, GetType(DataTable))
        '            If TryExisting IsNot Nothing AndAlso TypeOf TryExisting Is DataTable Then
        '                Dim Existing As DataTable = TryExisting
        '                Dim SPExisting As DataTable

        '                com.AddParameter("@TranDate", Existing.Rows(0).Item("DATE1"))
        '                com.AddParameter("@TillId", Existing.Rows(0).Item("TILL"))
        '                com.AddParameter("@TranNumber", Existing.Rows(0).Item("TRAN"))
        '                SPExisting = com.ExecuteDataTable
        '                SafeAddTestContextProperty(testContext, TC_KEY_STOREDPROCEDUREEXISTING, SPExisting)
        '                GetExistingUsingStoredProcedure = True
        '            End If
        '        End Using
        '    End Using
        'End Function

        'Private Shared Function GetNonExistingUsingStoredProcedure(ByRef testContext As TestContext) As Boolean

        '    Using con As New Connection
        '        Using com As New Command(con)
        '            Dim SPKey As SaleGetKey

        '            com.StoredProcedureName = SafeGetTestContextProperty(testContext, TC_KEY_STOREDPROCEDURENAME, GetType(String))
        '            SPKey = SafeGetTestContextProperty(testContext, TC_KEY_NONEXISTINGKEY, GetType(SaleGetKey))
        '            If SPKey IsNot Nothing AndAlso TypeOf SPKey Is SaleGetKey Then
        '                Dim SPNonExisting As DataTable

        '                com.AddParameter("@TranDate", SPKey.TransactionDate)
        '                com.AddParameter("@TillId", SPKey.TillId)
        '                com.AddParameter("@TranNumber", SPKey.TransactionNumber)
        '                SPNonExisting = com.ExecuteDataTable
        '                SafeAddTestContextProperty(testContext, TC_KEY_STOREDPROCEDURENONEXISTING, SPNonExisting)
        '                GetNonExistingUsingStoredProcedure = True
        '            End If
        '        End Using
        '    End Using
        'End Function

        'Private Shared Function GetNonExistingSQL() As String
        '    Dim sb As New StringBuilder

        '    sb.Append("SELECT ")
        '    sb.Append("dt.DATE1,dt.TILL,dt.[TRAN],dt.CASH,dt.[TIME],dt.SUPV,")
        '    sb.Append("dt.TCOD,dt.[OPEN],dt.MISC,dt.DESCR,dt.ORDN,dt.ACCT,")
        '    sb.Append("dt.VOID,dt.VSUP,dt.TMOD,dt.[PROC],dt.DOCN,dt.SUSE,")
        '    sb.Append("dt.STOR,dt.MERC,dt.NMER,dt.TAXA,dt.DISC,dt.DSUP,")
        '    sb.Append("dt.TOTL,dt.ACCN,dt.[CARD],dt.AUPD,dt.BACK,dt.ICOM, ")
        '    sb.Append("dt.IEMP,dt.RCAS,dt.RSUP,dt.PARK,dt.RMAN,dt.TOCD,")
        '    sb.Append("dt.PKRC,dt.REMO,dt.GTPN,dt.CCRD,dt.SSTA,dt.SSEQ,")
        '    sb.Append("dt.CBBU,dt.CARD_NO,dt.RTI,dt.VATR1,dt.VATR2,dt.VATR3,")
        '    sb.Append("dt.VATR4,dt.VATR5,dt.VATR6,dt.VATR7,dt.VATR8,dt.VATR9,")
        '    sb.Append("dt.VSYM1,dt.VSYM2,dt.VSYM3,dt.VSYM4,dt.VSYM5,dt.VSYM6,")
        '    sb.Append("dt.VSYM7,dt.VSYM8,dt.VSYM9,dt.XVAT1,dt.XVAT2,dt.XVAT3,")
        '    sb.Append("dt.XVAT4,dt.XVAT5,dt.XVAT6,dt.XVAT7,dt.XVAT8,dt.XVAT9,")
        '    sb.Append("dt.VATV1,dt.VATV2,dt.VATV3,dt.VATV4,dt.VATV5,dt.VATV6,")
        '    sb.Append("dt.VATV7,dt.VATV8,dt.VATV9 ")
        '    sb.Append("FROM DLTOTS dt ")
        '    sb.Append("WHERE dt.DATE1 = @TranDate ")
        '    sb.Append("and dt.TRAN = @TranNo ")
        '    sb.Append("and dt.TILL= @TillId ")

        '    Return sb.ToString
        'End Function

        'Private Shared Function GetExistingSQL() As String
        '    Dim sb As New StringBuilder

        '    sb.Append("SELECT ")
        '    sb.Append("dt.DATE1,dt.TILL,dt.[TRAN],dt.CASH,dt.[TIME],dt.SUPV,")
        '    sb.Append("dt.TCOD,dt.[OPEN],dt.MISC,dt.DESCR,dt.ORDN,dt.ACCT,")
        '    sb.Append("dt.VOID,dt.VSUP,dt.TMOD,dt.[PROC],dt.DOCN,dt.SUSE,")
        '    sb.Append("dt.STOR,dt.MERC,dt.NMER,dt.TAXA,dt.DISC,dt.DSUP,")
        '    sb.Append("dt.TOTL,dt.ACCN,dt.[CARD],dt.AUPD,dt.BACK,dt.ICOM, ")
        '    sb.Append("dt.IEMP,dt.RCAS,dt.RSUP,dt.PARK,dt.RMAN,dt.TOCD,")
        '    sb.Append("dt.PKRC,dt.REMO,dt.GTPN,dt.CCRD,dt.SSTA,dt.SSEQ,")
        '    sb.Append("dt.CBBU,dt.CARD_NO,dt.RTI,dt.VATR1,dt.VATR2,dt.VATR3,")
        '    sb.Append("dt.VATR4,dt.VATR5,dt.VATR6,dt.VATR7,dt.VATR8,dt.VATR9,")
        '    sb.Append("dt.VSYM1,dt.VSYM2,dt.VSYM3,dt.VSYM4,dt.VSYM5,dt.VSYM6,")
        '    sb.Append("dt.VSYM7,dt.VSYM8,dt.VSYM9,dt.XVAT1,dt.XVAT2,dt.XVAT3,")
        '    sb.Append("dt.XVAT4,dt.XVAT5,dt.XVAT6,dt.XVAT7,dt.XVAT8,dt.XVAT9,")
        '    sb.Append("dt.VATV1,dt.VATV2,dt.VATV3,dt.VATV4,dt.VATV5,dt.VATV6,")
        '    sb.Append("dt.VATV7,dt.VATV8,dt.VATV9 ")
        '    sb.Append("FROM DLTOTS dt ")
        '    sb.Append("WHERE dt.DATE1 = @TranDate ")
        '    '        sb.Append("and dt.TRAN = @TranNo ")
        '    '       sb.Append("and dt.TILL= @TillId ")

        '    Return sb.ToString
        'End Function

        'Private Shared Function InitialiseTests(ByRef testContext As TestContext) As Boolean

        '    If testContext.Properties(TC_KEY_INITIALISED) Is Nothing Then
        '        SafeAddTestContextProperty(testContext, TC_KEY_ASSEMBLYNAME, ASSEMBLYNAME)
        '        SafeAddTestContextProperty(testContext, TC_KEY_STOREDPROCEDURENAME, STOREDPROCEDURENAME)
        '        If CheckForStoredProcedure(testContext) Then
        '            If GetExisting(testContext) Then
        '                If GetNonExistingKey(testContext) Then
        '                    If GetExistingUsingStoredProcedure(testContext) Then
        '                        If GetNonExistingUsingStoredProcedure(testContext) Then
        '                            SafeAddTestContextProperty(testContext, TC_KEY_INITIALISED, True)
        '                        End If
        '                    End If
        '                End If
        '            End If
        '        End If
        '    End If
        '    InitialiseTests = SafeGetTestContextProperty(testContext, TC_KEY_INITIALISED, GetType(Boolean))
        'End Function

        'Private Shared Sub SafeAddTestContextProperty(ByRef testContext As TestContext, ByVal PropertyKey As String, ByVal PropertyValue As Object)

        '    If testContext.Properties(PropertyKey) IsNot Nothing Then
        '        testContext.Properties.Remove(PropertyKey)
        '    End If
        '    testContext.Properties.Add(PropertyKey, PropertyValue)
        'End Sub

        'Private Shared Function SafeGetTestContextProperty(ByRef testContext As TestContext, ByVal PropertyKey As String, ByVal PropertyType As Type) As Object

        '    SafeGetTestContextProperty = Nothing
        '    If testContext IsNot Nothing AndAlso testContext.Properties(PropertyKey) IsNot Nothing Then
        '        If PropertyType IsNot Nothing Then
        '            Select Case PropertyType.Name
        '                Case GetType(String).Name
        '                    SafeGetTestContextProperty = testContext.Properties(PropertyKey).ToString()
        '                Case GetType(Integer).Name
        '                    Dim Prop As Integer

        '                    If Integer.TryParse(testContext.Properties(PropertyKey).ToString(), Prop) Then
        '                        SafeGetTestContextProperty = Prop
        '                    End If
        '                Case GetType(Boolean).Name
        '                    Dim Prop As Boolean

        '                    If Boolean.TryParse(testContext.Properties(PropertyKey).ToString(), Prop) Then
        '                        SafeGetTestContextProperty = Prop
        '                    End If
        '                Case GetType(Date).Name
        '                    Dim Prop As Date

        '                    If Date.TryParse(testContext.Properties(PropertyKey).ToString(), Prop) Then
        '                        SafeGetTestContextProperty = Prop
        '                    End If
        '                Case GetType(DataTable).Name
        '                    Dim TryDataTable As Object = testContext.Properties(PropertyKey)
        '                    Dim Prop As DataTable

        '                    If TypeOf TryDataTable Is DataTable Then
        '                        Prop = TryDataTable
        '                        SafeGetTestContextProperty = Prop
        '                    End If
        '                Case Else
        '                    SafeGetTestContextProperty = testContext.Properties(PropertyKey)
        '            End Select
        '        Else
        '            SafeGetTestContextProperty = testContext.Properties(PropertyKey)
        '        End If
        '    End If
        'End Function
#End Region

#Region "Field Comparisons"

        Private Enum FieldType
            ftString = 0
            ftInteger
            ftDecimal
            ftBoolean
            ftDate
        End Enum

        Private Function FieldsMatch(ByVal FieldName As String, ByRef Message As String, ByVal Type As FieldType) As Boolean
            Dim Marker As String = "Start"
            Dim SPName As String = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String)).ToString

            Try
                Dim TryExisting As Object = SafeGetTestContextProperty(TC_KEY_EXISTING, GetType(DataTable))

                Marker = "Existing"
                If TryExisting IsNot Nothing AndAlso TypeOf TryExisting Is DataTable Then
                    Dim Existing As DataTable = CType(TryExisting, DataTable)

                    If Existing.Rows.Count = 1 Then
                        If Existing.Rows(0).Item(FieldName) IsNot Nothing Then
                            Dim TrySPExisting As Object = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable))

                            Marker = "Retreived"
                            If TrySPExisting IsNot Nothing AndAlso TypeOf TrySPExisting Is DataTable Then
                                Dim SPExisting As DataTable = CType(TrySPExisting, DataTable)

                                If SPExisting.Rows.Count = 1 Then
                                    If SPExisting.Rows(0).Item(FieldName) IsNot Nothing Then
                                        Marker = "DBNull"
                                        If TypeOf Existing.Rows(0).Item(FieldName) Is DBNull Then
                                            If TypeOf SPExisting.Rows(0).Item(FieldName) Is DBNull Then
                                                FieldsMatch = True
                                            Else
                                                Assert.Fail(FieldName & " values do not match as 'existing' is Null and retreived by stored procedure is not Null")
                                            End If
                                        Else
                                            If TypeOf SPExisting.Rows(0).Item(FieldName) Is DBNull Then
                                                Assert.Fail(FieldName & " values do not match as 'existing' is not Null and retreived by stored procedure is Null")
                                            Else
                                                Marker = "Select"
                                                Select Case Type
                                                    Case FieldType.ftString
                                                        Marker = "String"
                                                        FieldsMatch = StringsMatch(Existing.Rows(0).Item(FieldName), SPExisting.Rows(0).Item(FieldName), Message)
                                                    Case FieldType.ftInteger
                                                        Marker = "Integer"
                                                        FieldsMatch = IntegersMatch(Existing.Rows(0).Item(FieldName), SPExisting.Rows(0).Item(FieldName), Message)
                                                    Case FieldType.ftBoolean
                                                        Marker = "Boolean"
                                                        FieldsMatch = BooleansMatch(Existing.Rows(0).Item(FieldName), SPExisting.Rows(0).Item(FieldName), Message)
                                                    Case FieldType.ftDecimal
                                                        Marker = "Decimal"
                                                        FieldsMatch = DecimalsMatch(Existing.Rows(0).Item(FieldName), SPExisting.Rows(0).Item(FieldName), Message)
                                                    Case FieldType.ftDate
                                                        Marker = "Date"
                                                        FieldsMatch = DatesMatch(Existing.Rows(0).Item(FieldName), SPExisting.Rows(0).Item(FieldName), Message)
                                                    Case Else
                                                        Marker = "Not recognised"
                                                        Assert.Inconclusive("Cannot check field " & FieldName & " as field type specified is no recognised")
                                                End Select
                                            End If
                                        End If
                                    Else
                                        Assert.Fail("Stored Procedure " & SPName & " field name " & FieldName & " is not a field name in the original dynamic sql")
                                    End If
                                Else
                                    Assert.Inconclusive("Stored Procedure " & SPName & " failed to bring back the record so cannot compare this field")
                                End If
                            Else
                                Assert.Inconclusive("Stored Procedure " & SPName & " failed to bring back any data so cannot compare this field")
                            End If
                        Else
                            Assert.Inconclusive("Cannot check field " & FieldName & ", it is not a field in the original dynamic sql row.")
                        End If
                    Else
                        Assert.Inconclusive("Dynamic sql failed to bring back the record so cannot compare this field")
                    End If
                Else
                    Assert.Inconclusive("Dynamic sql failed to bring back any data so cannot compare this field")
                End If
            Catch ex As Exception
                Select Case Marker
                    Case "Start"
                        Assert.Inconclusive("Cannot check field " & FieldName & " problem at start of FieldsMatch function. " & ex.Message)
                    Case "Existing"
                        Assert.Inconclusive("Cannot check field " & FieldName & ", it is not a field in the original dynamic sql row.")
                    Case "Retreived"
                        Assert.Fail("Stored Procedure " & SPName & " uses a different field name to the one (" & FieldName & ") used in the original dynamic sql.")
                    Case "DBNull"
                        Assert.Inconclusive("Cannot check field " & FieldName & " problem whilst determining if field values are Null. " & ex.Message)
                    Case "Select"
                        Assert.Inconclusive("Cannot check field " & FieldName & " problem processing field type. " & ex.Message)
                    Case "String"
                        Assert.Inconclusive("Cannot check field " & FieldName & " problem processing string field type. " & ex.Message)
                    Case "Integer"
                        Assert.Inconclusive("Cannot check field " & FieldName & " problem processing integer field type. " & ex.Message)
                    Case "Boolean"
                        Assert.Inconclusive("Cannot check field " & FieldName & " problem processing boolean field type. " & ex.Message)
                    Case "Decimal"
                        Assert.Inconclusive("Cannot check field " & FieldName & " problem processing decimal field type. " & ex.Message)
                    Case "Date"
                        Assert.Inconclusive("Cannot check field " & FieldName & " problem processing date field type. " & ex.Message)
                    Case "Not recognised"
                        Assert.Inconclusive("Cannot check field " & FieldName & " problem processing unrecognised field type. " & ex.Message)
                    Case Else
                        Assert.Inconclusive("Cannot check field " & FieldName & " problem with FieldsMatch function. " & ex.Message)
                End Select
            End Try
        End Function

        Private Function BooleansMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
            Dim FailReason As String = ""

            If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
                FailReason = "Booleans Match fail (" & FailReason & ")"
            Else
                If TypeOf Against Is Boolean Then
                    Dim AgainstAsBoolean As Boolean = CBool(Against)

                    If TypeOf Compare Is Boolean Then
                        Dim CompareAsBoolean As Boolean = CBool(Compare)

                        If AgainstAsBoolean = CompareAsBoolean Then
                            BooleansMatch = True
                        Else
                            FailReason = "Booleans Match fail as 'Against' (" & AgainstAsBoolean.ToString & ") and 'Compare' (" & CompareAsBoolean.ToString & ") are not the same"
                        End If
                    Else
                        FailReason = "Booleans Match fail as 'Compare' is not a Boolean"
                    End If
                Else
                    FailReason = "Booleans Match fail as 'Against' is not a Boolean"
                End If
            End If
            Message = FailReason
        End Function

        Private Function DatesMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String, Optional ByVal DateInterval As DateInterval = DateInterval.Second, Optional ByVal AreNullable As Boolean = False) As Boolean
            Dim FailReason As String = ""

            If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
                FailReason = "Dates Match fail (" & FailReason & ")"
            Else
                Dim AgainstDate As Date
                Dim CompareDate As Date

                If AreNullable Then
                    If TypeOf Against Is Date? Then
                        Dim AgainstAsNullableDate As Date?

                        If TypeOf Compare Is Date? Then
                            Dim CompareAsNullableDate As Date?

                            If AgainstAsNullableDate.HasValue Then
                                AgainstDate = AgainstAsNullableDate.Value
                                If CompareAsNullableDate.HasValue Then
                                    CompareDate = CompareAsNullableDate.Value
                                Else
                                    FailReason = "Dates Match fail as 'Against' is not Null but 'Compare' is Null"
                                End If
                            Else
                                If CompareAsNullableDate.HasValue Then
                                    FailReason = "Dates Match fail as 'Against' is Null but 'Compare' is not Null"
                                Else
                                    Message = FailReason
                                    DatesMatch = True
                                    Exit Function
                                End If
                            End If
                        Else
                            FailReason = "Dates Match fail as 'Compare' is not a Nullable Date"
                        End If
                    Else
                        FailReason = "Dates Match fail as 'Compare' is not a Nullable Date"
                    End If
                Else
                    If TypeOf Against Is Date Then
                        AgainstDate = CDate(Against)
                        If TypeOf Compare Is Date Then
                            CompareDate = CDate(Compare)
                        Else
                            FailReason = "Dates Match fail as 'Compare' is not a Date"
                        End If
                    Else
                        FailReason = "Dates Match fail as 'Compare' is not a Date"
                    End If
                End If
                Dim Diff As Boolean = DateDiff(DateInterval, AgainstDate, CompareDate) <> 0

                If Diff Then
                    Dim Interval As String

                    Select Case DateInterval
                        Case Microsoft.VisualBasic.DateInterval.Day
                            Interval = "days"
                        Case Microsoft.VisualBasic.DateInterval.DayOfYear
                            Interval = "days (of year)"
                        Case Microsoft.VisualBasic.DateInterval.Hour
                            Interval = "hours"
                        Case Microsoft.VisualBasic.DateInterval.Month
                            Interval = "months"
                        Case Microsoft.VisualBasic.DateInterval.Quarter
                            Interval = "quarters"
                        Case Microsoft.VisualBasic.DateInterval.Second
                            Interval = "seconds"
                        Case Microsoft.VisualBasic.DateInterval.Weekday
                            Interval = "week days"
                        Case Microsoft.VisualBasic.DateInterval.WeekOfYear
                            Interval = "weeks (of year)"
                        Case Microsoft.VisualBasic.DateInterval.Year
                            Interval = "years"
                        Case Else
                            Interval = "unknown units"
                    End Select
                    FailReason = "Dates Match fail as 'Against' (" & AgainstDate.ToString("F") & ") is " & CInt(Diff).ToString & " " & Interval & " different to 'Compare' (" & AgainstDate.ToString("F") & ")"
                Else
                    DatesMatch = True
                End If
            End If
            Message = FailReason
        End Function

        Private Function DecimalsMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
            Dim FailReason As String = ""

            If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
                FailReason = "Integers Match fail (" & FailReason & ")"
            Else
                Dim AgainstAsDecimal As Decimal

                If Decimal.TryParse(Against.ToString, AgainstAsDecimal) Then
                    Dim CompareAsDecimal As Decimal

                    If Decimal.TryParse(Compare.ToString, CompareAsDecimal) Then
                        If AgainstAsDecimal <> CompareAsDecimal Then
                            FailReason = "Decimals Match fail as 'Against' (= " & AgainstAsDecimal & ") and 'Compare' (= " & CompareAsDecimal & ") are not the same"
                        Else
                            DecimalsMatch = True
                        End If
                    Else
                        FailReason = "Decimals Match fail as 'Compare' is not a Decimal"
                    End If
                Else
                    FailReason = "Decimals Match fail as 'Against' is not a Decimal"
                End If
            End If
            Message = FailReason
        End Function

        Private Function IntegersMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
            Dim FailReason As String = ""

            If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
                FailReason = "Integers Match fail (" & FailReason & ")"
            Else
                Dim AgainstAsInteger As Integer

                If Integer.TryParse(Against.ToString, AgainstAsInteger) Then
                    Dim CompareAsInteger As Integer

                    If Integer.TryParse(Compare.ToString, CompareAsInteger) Then
                        If AgainstAsInteger <> CompareAsInteger Then
                            FailReason = "Integers Match fail as 'Against' (= " & AgainstAsInteger & ") and 'Compare' (= " & CompareAsInteger & ") are not the same"
                        Else
                            IntegersMatch = True
                        End If
                    Else
                        FailReason = "Integers Match fail as 'Compare' is not an Integer"
                    End If
                Else
                    FailReason = "Integers Match fail as 'Against' is not an Integer"
                End If
            End If
            Message = FailReason
        End Function

        Private Function StringsMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String, Optional ByVal StringComparisonType As System.StringComparison = StringComparison.CurrentCulture) As Boolean
            Dim FailReason As String = ""

            If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
                FailReason = "Strings Match fail (" & FailReason & ")"
            Else
                If TypeOf Against Is String Then
                    Dim AgainstAsString As String = Against.ToString

                    If TypeOf Compare Is String Then
                        Dim CompareAsString As String = Compare.ToString

                        If AgainstAsString = String.Empty Then
                            If CompareAsString = String.Empty Then
                                StringsMatch = True
                            Else
                                FailReason = "Strings Match fail as 'Against' is Empty but 'Compare' is not Empty"
                            End If
                        Else
                            If CompareAsString = String.Empty Then
                                FailReason = "Strings Match fail as 'Against' is not Empty but 'Compare' is Empty"
                            Else
                                If String.Equals(AgainstAsString, CompareAsString, StringComparisonType) Then
                                    StringsMatch = True
                                Else
                                    FailReason = "Strings Match fail as 'Against' (= " & AgainstAsString & ") and 'Compare' (= " & CompareAsString & ") are not the same"
                                End If
                            End If
                        End If
                    Else
                        FailReason = "Strings Match fail as 'Compare' is not a string"
                    End If
                Else
                    FailReason = "Strings Match fail as 'Against' is not a string"
                End If
            End If
            Message = FailReason
        End Function

        Private Function ObjectsBothNotNothingOrBothSomething(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
            Dim FailReason As String

            If Against Is Nothing Then
                If Compare IsNot Nothing Then
                    FailReason = "Against is 'Nothing', Compare is not 'Nothing'"
                Else
                    ObjectsBothNotNothingOrBothSomething = True
                End If
            Else
                If Compare Is Nothing Then
                    FailReason = "Against is not 'Nothing', Compare is 'Nothing'"
                Else
                    ObjectsBothNotNothingOrBothSomething = True
                End If
            End If
        End Function
#End Region
    End Class

    Friend Class SaleGetKey
        Private TranDate As Date
        Private TranNo As String
        Private Till As String

        Public Property TransactionDate() As Date
            Get
                TransactionDate = TranDate
            End Get
            Set(ByVal value As Date)
                TranDate = value
            End Set
        End Property

        Public Property TransactionNumber() As String
            Get
                TransactionNumber = TranNo
            End Get
            Set(ByVal value As String)
                TranNo = value
            End Set
        End Property

        Public Property TillId() As String
            Get
                TillId = Till
            End Get
            Set(ByVal value As String)
                Till = value
            End Set
        End Property

        Public Sub New(ByVal TransDate As Date, ByVal TransNo As String, ByVal TillNo As String)

            TranDate = TransDate
            TranNo = TransNo
            Till = TillNo
        End Sub

        Public Overrides Function ToString() As String

            ToString = "Transaction Date = " & TranDate.ToShortDateString & "; Transaction Number = '" & TranNo & "'; Till Id = '" & Till & "'"
        End Function

        Public Function ToShortString() As String

            ToShortString = "Date=" & TranDate.ToShortDateString & "; Number='" & TranNo & "'; Till='" & Till & "'"
        End Function

        Public Shared Function GetKeyString(ByRef From As DataRow) As String

            GetKeyString = "Transaction Date = " & CDate(From.Item("DATE1")).ToShortDateString & "; Transaction Number = '" & CStr(From.Item("TRAN")) & "'; Till Id = '" & CStr(From.Item("TILL")) & "'"
        End Function

        Public Shared Function GetKeyShortString(ByRef From As DataRow) As String

            GetKeyShortString = "Date=" & CDate(From.Item("DATE1")).ToShortDateString & "; Number='" & CStr(From.Item("TRAN")) & "'; Till='" & CStr(From.Item("TILL")) & "'"
        End Function
    End Class
End Namespace
