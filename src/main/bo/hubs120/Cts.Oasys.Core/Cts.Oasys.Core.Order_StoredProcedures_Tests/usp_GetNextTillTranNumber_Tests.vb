﻿Imports System
Imports System.Text
Imports System.Data
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports Cts.Oasys.Data
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class usp_GetNextTillTranNumber_Tests

    Private testContextInstance As TestContext
    Private Const CONNECTION_STRING As String = "Server=S130372\SQLEXPRESS;Database=Oasys;User ID=sa;Password=gandalf;Trusted_Connection=False;"
    Private Const SP_NAME As String = "[usp_getNextTillTranNumber]"

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub usp_GetNextTillTranNumber_Called10000Times_AllValidValidTillTranReturned()
        ' the transaction number increments from 0001 to 9999 and then rolls around to 0001
        '   0000 is not a valid transaction number and if found the test will fail
        '   calling the stored procedure 10000 times guarantees that a roll around will occur

        ' arrange
        Dim tillTranValue As String

        Using con As New SqlConnection(CONNECTION_STRING)
            con.Open()
            Using com As New SqlCommand(SP_NAME, con)
                com.CommandType = CommandType.StoredProcedure
                com.Parameters.Add("@TillTranNumber", SqlDbType.Char)
                com.Parameters("@TillTranNumber").Size = 6
                com.Parameters("@TillTranNumber").Direction = ParameterDirection.Output

                For i As Integer = 1 To 10000
                    ' act
                    com.ExecuteNonQuery()

                    tillTranValue = com.Parameters("@TillTranNumber").Value.ToString

                    ' assert
                    If InValidTranNumber(tillTranValue) Or InValidTillNumber(tillTranValue) Then
                        Assert.Fail()
                        Exit Sub
                    End If
                Next

            End Using
        End Using

        ' assert
        Assert.AreEqual(1, 1)

    End Sub

    Private Function InValidTranNumber(ByVal tillTranNumber As String) As Boolean
        Return Right(tillTranNumber, 4) = "0000"
    End Function

    Private Function InValidTillNumber(ByVal tillTranNumber As String) As Boolean
        Return Left(tillTranNumber, 2) = "00"
    End Function
    <TestMethod()> Public Sub InValidTillNumber_InvalidTranNumber_ReturnsTrue()
        ' arrange
        Dim tillTranValue As String = "000000"

        ' act
        Dim expected As Boolean = True, actual As Boolean = InValidTranNumber(tillTranValue)

        ' assert
        Assert.AreEqual(expected, actual)

    End Sub
End Class
