﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class SaleOrderLineGet
    Inherits Cts.Oasys.Core.UnitTests.StoredProcedure_Test


#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "StoredProcedure_Test Overridable Tests"
    ' Seems it is necessary to override these tests (and just call the base test with the override),
    ' just to get base test to run.

    <TestMethod()> _
    Public Overrides Sub StoredProcedureExists()

        MyBase.StoredProcedureExists()
    End Sub

    <TestMethod()> _
    Public Overrides Sub WhenDataExists_StoredProcedureGetsDataTable()

        MyBase.WhenDataExists_StoredProcedureGetsDataTable()
    End Sub

    <TestMethod()> _
    Public Overrides Sub WhenDataExists_StoredProcedureGetsAtLeastOneRecord()

        MyBase.WhenDataExists_StoredProcedureGetsAtLeastOneRecord()
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureDoesNotGetMultipleRecords()
        'Public Overrides Sub WhenDataExists_StoredProcedureDoesNotGetMultipleRecords()

        'MyBase.WhenDataExists_StoredProcedureDoesNotGetMultipleRecords()
        Assert.Fail("Code error; needs to be fixed!")

    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureGetsRecordByKey()
        '    Public Overrides Sub WhenDataExists_StoredProcedureGetsRecordByKey()

        'MyBase.WhenDataExists_StoredProcedureGetsRecordByKey()
        Assert.Fail("Code error; needs to be fixed!")

    End Sub

#End Region

#Region "Field Tests"

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedOrderNumber()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("OrderNumber", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedNumber()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("Number", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedSkuNumber()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("SkuNumber", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedSkuDescription()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("SkuDescription", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedSkuunitMeasure()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("SkuunitMeasure", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedQtyOrdered()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("QtyOrdered", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedQtyTaken()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("QtyTaken", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedQtyRefunded()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("QtyRefunded", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedQtyToDeliver()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("QtyToDeliver", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedQtyOnHand()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("QtyOnHand", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedQtyOnOrder()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("QtyOnOrder", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedPrice()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("Price", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedIsDeliveryChargeItem()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("IsDeliveryChargeItem", Message, FieldType.ftBoolean), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedWeight()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("Weight", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedVolume()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("Volume", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedPriceOverrideCode()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("PriceOverrideCode", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDeliveryStatus()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("DeliveryStatus", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedSellingStoreId()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("SellingStoreId", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedSellingStoreOrderId()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("SellingStoreOrderId", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDeliverySource()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("DeliverySource", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedDeliverySourceIbtOut()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("DeliverySourceIbtOut", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedSellingStoreIbtIn()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("SellingStoreIbtIn", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedQuantityScanned()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("QuantityScanned", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedSourceOrderLineNo()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("SourceOrderLineNo", Message, FieldType.ftInteger), Message)
        End If
    End Sub
#End Region

#Region "StoredProcedure_Test MustOverrides"

    Public Overrides Function GetExistingDataAndKeyWithDynamicSQL(ByRef ExistingData As System.Data.DataTable, ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL(), con)
                    Dim OrderNumber As Integer = 9000
                    Dim Found As Boolean

                    com.Parameters.Add("@OrderNumber", SqlDbType.NChar)
                    ' Find a date which brings back an existing item
                    Do While OrderNumber < 1000000 And Not Found
                        com.Parameters.Item("@OrderNumber").Value = OrderNumber.ToString.PadLeft(6, "0"c)

                        Dim reader As SqlDataReader = com.ExecuteReader

                        ExistingData = New DataTable
                        ExistingData.Load(reader)
                        reader.Close()

                        If ExistingData.Rows.Count = 1 Then
                            Dim OMONKey As New Key

                            With OMONKey
                                .SPParamName = "OrderNumber"
                                .DynamicSQLName = "Number"
                                .Caption = "Order Number"
                                .Value = OrderNumber.ToString.PadLeft(6, "0"c)
                                .Type = FieldType.ftString
                            End With
                            Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                            Found = True
                        End If
                        OrderNumber += 1
                    Loop
                    GetExistingDataAndKeyWithDynamicSQL = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record.")
        End Try
    End Function

    Public Overrides Function GetExistingSQL() As String
        Dim sb As New StringBuilder

        sb.Append("SELECT ")
        sb.Append("cl.NUMB		as OrderNumber, ")
        sb.Append("cl.LINE		as Number,")
        sb.Append("cl.SKUN		as SkuNumber,")
        sb.Append("st.DESCR	    as SkuDescription,")
        sb.Append("st.BUYU		as SkuunitMeasure,")
        sb.Append("cl.QTYO		as QtyOrdered,")
        sb.Append("cl.QTYT		as QtyTaken,")
        sb.Append("cl.QTYR		as QtyRefunded,")
        sb.Append("cl.QtyToBeDelivered  as QtyToDeliver, ")
        sb.Append("st.ONHA		as QtyOnHand, ")
        sb.Append("st.ONOR		as QtyOnOrder, ")
        sb.Append("cl.Price,")
        sb.Append("cl.IsDeliveryChargeItem,	")
        sb.Append("cl.WGHT		as Weight,")
        sb.Append("cl.VOLU		as Volume,")
        sb.Append("cl.PORC		as PriceOverrideCode,")
        sb.Append("cl.DeliveryStatus,")
        sb.Append("cl.SellingStoreId,")
        sb.Append("cl.SellingStoreOrderId,")
        'sb.Append("cl.QtyToBeDelivered as QtyTodDeliver,")
        sb.Append("cl.DeliverySource,")
        sb.Append("cl.DeliverySourceIbtOut,")
        sb.Append("cl.SellingStoreIbtIn, ")
        sb.Append("cl.QuantityScanned, ")

        sb.Append("cl2.SourceOrderLineNo ")

        sb.Append("FROM CORLIN cl ")
        'venda sale will have an entry in corlin2 only!
        sb.Append("LEFT OUTER JOIN CORLIN2 cl2 ")
        sb.Append("      on  cl2.NUMB = cl.NUMB ")
        sb.Append("      and cl2.LINE = cl.LINE ")
        sb.Append("INNER JOIN STKMAS st ")
        sb.Append("      on st.SKUN = cl.SKUN ")
        sb.Append("WHERE cl.NUMB=@OrderNumber ")
        sb.Append("ORDER BY cl.LINE")

        GetExistingSQL = sb.ToString
    End Function

    Public Overrides Function GetNonExistingKeyWithDynamicSQL(ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetNonExistingSQL(), con)
                    Dim OrderNumber As Integer = 9000
                    Dim ReturnedData As DataTable
                    Dim Found As Boolean

                    com.Parameters.Add("@OrderNumber", SqlDbType.NChar)
                    ' Find an order number which does not bring back an existing item
                    Do While OrderNumber < 1000000 And Not Found
                        com.Parameters.Item("@OrderNumber").Value = OrderNumber.ToString.PadLeft(6, "0"c)

                        Dim reader As SqlDataReader = com.ExecuteReader

                        ReturnedData = New DataTable
                        ReturnedData.Load(reader)
                        reader.Close()

                        If ReturnedData.Rows.Count = 0 Then
                            Dim OMONKey As New Key

                            With OMONKey
                                .SPParamName = "OrderNumber"
                                .DynamicSQLName = "Number"
                                .Caption = "Order Number"
                                .Value = OrderNumber.ToString.PadLeft(6, "0"c)
                                .Type = FieldType.ftString
                            End With
                            Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                            Found = True
                        End If
                        OrderNumber += 1
                    Loop
                    GetNonExistingKeyWithDynamicSQL = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a non existing record key.")
        End Try
    End Function

    Public Overrides Function GetNonExistingSQL() As String

        GetNonExistingSQL = GetExistingSQL()
    End Function

    Public Overrides ReadOnly Property StoredProcedureName() As String
        Get
            StoredProcedureName = "SaleOrderLineGet"
        End Get
    End Property

    Public Overrides Function GetKeyFieldsNoValueRequired(ByRef KeyFields As Collection) As Boolean
        Dim KeyField As Key

        KeyFields = New Collection
        KeyField = New Key("OrderNumber", "OrderNumber", "Order Number", Nothing, FieldType.ftString)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        GetKeyFieldsNoValueRequired = True
    End Function
#End Region

End Class