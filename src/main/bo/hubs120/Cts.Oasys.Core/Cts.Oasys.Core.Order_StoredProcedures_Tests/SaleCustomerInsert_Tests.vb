﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Data
Imports System.Data.SqlClient

<TestClass()> Public Class SaleCustomerInsert_Tests

    Private testContextInstance As TestContext

    Private Const dateToCopyFrom As String = "16 Jan 2010"
    Private Const tillToCopyFrom As String = "99"
    Private Const tranToCopyFrom As String = "0001"

    Private Const dateToCreate As String = "16 Jan 2010"
    Private Const tillToCreate As String = "99"
    Private Const tranToCreate As String = "0001"

    ' Principle of testing for INSERT stored procedure
    '   Delete existing test data
    '   Get known values from an existing row
    '   Call stored procedure to insert known values
    '   Select and compare

    Private Const CONNECTION_STRING As String = "Server=S130372\SQLEXPRESS;Database=Oasys;User ID=sa;Password=gandalf;Trusted_Connection=False;"
    'Private Const _INSERT_SQL As String = "INSERT INTO DLRCUS SELECT " & dateToCreate & ", " & tillToCreate & ", " & tranToCreate & "',  [PLUD], [DESCR], [FILL], [DEPT], [GROU], [VATC], [BUYU], [SUPP], [PROD], [PACK], [PRIC], [PPRI], [COST], [DSOL], [DREC], [DORD], [DPRC], [DSET], [DOBS], [DDEL], [ONHA], [ONOR], [MINI], [MAXI], [SOLD], [ISTA], [IRIS], [IRIB], [IMDN], [ICAT], [IOBS], [IDEL], [ILOC], [IEAN], [IPPC], [ITAG], [INON], [SALV1], [SALV2], [SALV3], [SALV4], [SALV5], [SALV6], [SALV7], [SALU1], [SALU2], [SALU3], [SALU4], [SALU5], [SALU6], [SALU7], [CPDO], [CYDO], [AWSF], [AW13], [DATS], [UWEK], [FLAG], [CFLG], [US001], [US002], [US003], [US004], [US005], [US006], [US007], [US008], [US009], [US0010], [US0011], [US0012], [US0013], [US0014], [DO001], [DO002], [DO003], [DO004], [DO005], [DO006], [DO007], [DO008], [DO009], [DO0010], [DO0011], [DO0012], [DO0013], [DO0014], [SQ04], [SQ13], [SQ01], [SQOR], [TREQ], [TREV], [TACT], [RETQ], [RETV], [CHKD], [LABN], [LABS], [STON], [STOQ], [SOD1], [SOD2], [SOD3], [LABM], [LABL], [WGHT], [VOLU], [NOOR], [SUP1], [IODT], [FODT], [PMIN], [PMSD], [PMED], [PMCP], [SMAN], [DFLC], [IMCP], [FOLT], [IDEA], [QADJ], [TAGF], [ALPH], [CTGY], [GRUP], [SGRP], [STYL], [EQUV], [HFIL], [REVT], [RPRI], [IWAR], [IOFF], [ISOL], [QUAR], [IPRD], [EQPM], [EQPU], [MDNQ], [WTFQ], [SALT], [MODT], [AAPC], [IPSK], [AADJ], [SUP2], [FRAG], [TIMB], [ELEC], [SOFS], [WQTY], [WRAT], [WSEQ], [BALI], [PRFSKU], [MSPR1], [MSPR2], [MSPR3], [MSPR4], [MSTQ1], [MSTQ2], [MSTQ3], [MSTQ4], [MREQ1], [MREQ2], [MREQ3], [MREQ4], [MREV1], [MREV2], [MREV3], [MREV4], [MADQ1], [MADQ2], [MADQ3], [MADQ4], [MADV1], [MADV2], [MADV3], [MADV4], [MPVV1], [MPVV2], [MPVV3], [MPVV4], [MIBQ1], [MIBQ2], [MIBQ3], [MIBQ4], [MIBV1], [MIBV2], [MIBV3], [MIBV4], [MRTQ1], [MRTQ2], [MRTQ3], [MRTQ4], [MRTV1], [MRTV2], [MRTV3], [MRTV4], [MCCV1], [MCCV2], [MCCV3], [MCCV4], [MDRV1], [MDRV2], [MDRV3], [MDRV4], [MBSQ1], [MBSQ2], [MBSQ3], [MBSQ4], [MBSV1], [MBSV2], [MBSV3], [MBSV4], [MSTP1], [MSTP2], [MSTP3], [MSTP4], [FIL11], [FIL12], [FIL13], [FIL14], [ToForecast], [IsInitialised], [DemandPattern], [PeriodDemand], [PeriodTrend], [ErrorForecast], [ErrorSmoothed], [BufferConversion], [BufferStock], [ServiceLevelOverride], [ValueAnnualUsage], [OrderLevel], [SaleWeightSeason], [SaleWeightBank], [SaleWeightPromo], [DateLastSoqInit], [StockLossPerWeek], [FlierPeriod1], [FlierPeriod2], [FlierPeriod3], [FlierPeriod4], [FlierPeriod5], [FlierPeriod6], [FlierPeriod7], [FlierPeriod8], [FlierPeriod9], [FlierPeriod10], [FlierPeriod11], [FlierPeriod12], [SalesBias0], [SalesBias1], [SalesBias2], [SalesBias3], [SalesBias4], [SalesBias5], [SalesBias6] FROM STKMAS WHERE SKUN = '" & skuToCopyFrom & "'"
    Private Const _DELETE_SQL As String = "DELETE FROM STKMAS WHERE DATE1 = '" & dateToCreate & "' AND TILL = '" & tillToCreate & "' AND [TRAN] = '" & tranToCreate & "'"

    Private Const _SELECT_SQL As String = "SELECT **FIELD_TOKEN** FROM STKMAS WHERE DATE1  = '" & dateToCreate & "' AND TILL = '" & tillToCreate & "', '" & tranToCreate & "'"

    Private Const SP_NAME As String = "[SaleCustomerInsert]"

    Shared _mappings As New StoredProcedureFieldMappings()

    Private _transactionDate As Date        ' @TransactionDate DATE DATE1
    Private _tillNumber As String           ' @TillNumber CHAR (2) TILL 
    Private _transactionNumber As String    ' @TransactionNumber [TRAN]
    Private _customerName As String         ' @CustomerName CHAR(30) NAME
    Private _houseNumber As String          ' @HouseNumber CHAR(4) = null HNUM
    Private _postCode As String             ' @PostCode CHAR(8) POST
    Private _storeNumber As String          ' @StoreNumber CHAR(3)='000' OSTR
    Private _saleDate As Date               ' @SaleDate DATE=null ODAT
    Private _saleTill As String             ' @SaleTill CHAR(2)= '00' OTIL
    Private _saleTransaction As String      ' @SaleTransaction CHAR(4) = '0000' OTRN
    Private _houseNameNumber As String      ' @HouseNameNo CHAR(15) HNAM
    Private _address1 As String             ' @Address1 CHAR(30)= NULL HAD1
    Private _address2 As String             ' @Address2 CHAR(30)= NULL HAD2
    Private _address3 As String             ' @Address3 CHAR(30)=NULL HAD3
    Private _phoneNumber As String          ' @PhoneNumber CHAR(15) PHON
    Private _lineNumber As Integer          ' @LineNumber INT = 0 NUMB
    Private _transactionValidated As Boolean ' @TransactionValidated BIT=0 OVAL
    Private _originalTenderType As Integer  ' @OriginalTenderType DECIMAL(3,0)= 0 OTEN
    Private _refundReasonCode As String     ' @RefundReasonCode CHAR(2) = '00' RCOD
    Private _labels As Boolean              ' @Labels BIT=0 LABL
    Private _phoneNumberMobile As String    ' @PhoneNumberMobile CHAR(15)=NULL MOBP
    Private _RTIFlag As String              ' @RTIFlag CHAR(1)='S' RTI
    Private _EmailAddress As String         ' @EmailAddress CHAR(1) EmailAddress
    Private _PhoneNumberWork As String      ' @PhoneNumberWork CHAR(1)=NULL PhoneNumberWork

    Friend Class StoredProcedureFieldMappings
        Private _list As List(Of StoredProcedureFieldMapping)

        Friend Sub Add(ByVal physicalFieldName As String, ByVal physicalDataType As DbType, ByVal aliasedName As String, ByVal aliasedType As System.TypeCode, Optional ByVal value As String = "")
            _list.Add(New StoredProcedureFieldMapping(physicalFieldName, physicalDataType, aliasedName, aliasedType, value))
        End Sub

        Friend Property FieldMapping() As List(Of StoredProcedureFieldMapping)
            Get
                Return _list
            End Get
            Set(ByVal value As List(Of StoredProcedureFieldMapping))
                _list = value
            End Set
        End Property

        Friend Class StoredProcedureFieldMapping
            Private _physicalFieldName As String
            Private _physicalDataType As DbType
            Private _aliasedFieldName As String
            Private _aliasedDataType As TypeCode
            Private _value As String

            ReadOnly Property PhysicalFieldName() As String
                Get
                    Return _physicalFieldName
                End Get
            End Property
            ReadOnly Property PhysicalDataType() As DbType
                Get
                    Return _physicalDataType
                End Get
            End Property
            ReadOnly Property AliasedFieldName() As String
                Get
                    Return _aliasedFieldName
                End Get
            End Property
            ReadOnly Property AliasedDataType() As TypeCode
                Get
                    Return _aliasedDataType
                End Get
            End Property
            Property Value() As String
                Get
                    Return _value
                End Get
                Set(ByVal value As String)
                    _value = value
                End Set
            End Property

            Friend Sub New(ByVal physicalFieldName As String, ByVal physicalDataType As DbType, ByVal aliasedFieldName As String, ByVal aliasedDataType As System.TypeCode, Optional ByVal value As String = "")
                _physicalFieldName = physicalFieldName
                _physicalDataType = physicalDataType
                _aliasedFieldName = aliasedFieldName
                _aliasedDataType = aliasedDataType
                _value = value
            End Sub

        End Class
    End Class

    Private Sub PopulateLocalValues()
        ' run SQL command to populate the STKMAS table
        Using con As New SqlConnection(CONNECTION_STRING)
            con.Open()
            Using com As New SqlCommand(_SELECT_SQL, con)
                com.CommandType = CommandType.Text
                Dim reader As SqlDataReader = com.ExecuteReader

                reader.Read()

                For Each map In _mappings.FieldMapping
                    map.Value = reader(map.PhysicalFieldName).ToString
                Next
                '_mappings.FieldMapping.
                '_transactionDate = reader("DATE1")
                '_tillNumber = reader("TILL")
                '_transactionNumber = reader("[TRAN]")
                '_customerName = reader("NAME")
                '_houseNumber = reader("HNUM")
                '_postCode = reader("POST")
                '_storeNumber = reader("OSTR")
                '_saleDate = reader("ODAT")
                '_saleTill = reader("OTIL")
                '_saleTransaction = reader("OTRN")
                '_houseNameNumber = reader("HNAM")
                '_address1 = reader("HAD1")
                '_address2 = reader("HAD2")
                '_address3 = reader("HAD3")
                '_phoneNumber = reader("PHON")
                '_lineNumber = reader("NUMB")
                '_transactionValidated = reader("OVAL")
                '_originalTenderType = reader("OTEN")
                '_refundReasonCode = reader("RCOD")
                '_labels = reader("LABL")
                '_phoneNumberMobile = reader("MOBP")
                '_RTIFlag = reader("RTI")
                '_EmailAddress = reader("EmailAddress")
                '_PhoneNumberWork = reader("PhoneNumberWork")
                'results.Add(reader(physicalFieldName))

                reader.Close()
            End Using
        End Using
    End Sub

    'VALUES
    '(
    ' @TransactionDate ,
    ' @TillNumber, 
    ' @TransactionNumber ,
    ' @CustomerName,
    ' @HouseNumber,
    ' @PostCode,
    ' @StoreNumber ,
    ' @SaleDate,
    ' @SaleTill ,
    ' @SaleTransaction ,
    ' @HouseNameNo,
    ' @Address1 ,
    ' @Address2,
    ' @Address3 ,
    ' @PhoneNumber,
    ' @LineNumber,
    ' @TransactionValidated ,
    ' @OriginalTenderType,
    ' @RefundReasonCode,
    ' @Labels,
    ' @PhoneNumberMobile,
    ' @RTIFlag,
    ' @EmailAddress,
    ' @PhoneNumberWork 

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        With _mappings
            .Add("DATE1", DbType.Date, "TransactionDate", TypeCode.String)
            .Add("TILL", DbType.StringFixedLength, "TillNumber", TypeCode.String)
            .Add("[TRAN]", DbType.StringFixedLength, "TransactionNumber", TypeCode.String)
            .Add("NAME", DbType.StringFixedLength, "CustomerName", TypeCode.String)
            .Add("HNUM", DbType.StringFixedLength, "HouseNumber", TypeCode.String)
            .Add("POST", DbType.StringFixedLength, "PostCode", TypeCode.String)
            .Add("OSTR", DbType.StringFixedLength, "StoreNumber", TypeCode.String)
            .Add("ODAT", DbType.Date, "SaleDate", TypeCode.String)
            .Add("OTIL", DbType.StringFixedLength, "SaleTill", TypeCode.String)
            .Add("OTRN", DbType.StringFixedLength, "SaleTransaction", TypeCode.String)
            .Add("HNAM", DbType.StringFixedLength, "HouseNameNo", TypeCode.String)
            .Add("HAD1", DbType.StringFixedLength, "Address1", TypeCode.String)
            .Add("HAD2", DbType.StringFixedLength, "Address2", TypeCode.String)
            .Add("HAD3", DbType.StringFixedLength, "Address3", TypeCode.String)
            .Add("PHON", DbType.StringFixedLength, "PhoneNumber", TypeCode.String)
            .Add("NUMB", DbType.StringFixedLength, "LineNumber", TypeCode.String)
            .Add("OVAL", DbType.StringFixedLength, "TransactionValidated", TypeCode.String)
            .Add("OTEN", DbType.StringFixedLength, "OriginalTenderType", TypeCode.String)
            .Add("RCOD", DbType.StringFixedLength, "RefundReasonCode", TypeCode.String)
            .Add("LABL", DbType.StringFixedLength, "Labels", TypeCode.String)
            .Add("MOBP", DbType.StringFixedLength, "PhoneNumberMobile", TypeCode.String)
            .Add("RTI", DbType.StringFixedLength, "RTIFlag", TypeCode.String)
            .Add("EmailAddress", DbType.StringFixedLength, "EmailAddress", TypeCode.String)
            .Add("PhoneNumberWork", DbType.StringFixedLength, "PhoneNumberWork", TypeCode.String)
        End With
    End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub TestMethod1()
        ' TODO: Add test logic here
    End Sub

End Class
