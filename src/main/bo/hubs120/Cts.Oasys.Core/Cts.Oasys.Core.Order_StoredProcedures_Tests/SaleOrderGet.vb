﻿Imports Cts.Oasys.Data
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class SaleOrderGet
    Private Const ASSEMBLYNAME As String = "Cts.Oasys.Core.Order_StoredProcedures_Tests"
    Private Const STOREDPROCEDURENAME As String = "SaleOrderGet"

    Private Const TC_KEY_ASSEMBLYNAME As String = "AssemblyName"
    Private Const TC_KEY_STOREDPROCEDURENAME As String = "StoredProcedureName"
    Private Const TC_KEY_FOUNDSTOREDPROCEDURE As String = "FoundStoredProcedure"
    Private Const TC_KEY_INITIALISED As String = "Initialised"
    Private Const TC_KEY_EXISTING As String = "Existing"
    Private Const TC_KEY_NONEXISTINGKEY As String = "NonExistingKey"
    Private Const TC_KEY_EXISTINGKEY As String = "ExistingKey"
    Private Const TC_KEY_STOREDPROCEDUREEXISTING As String = "StoredProcedureExisting"
    Private Const TC_KEY_STOREDPROCEDURENONEXISTING As String = "StoredProcedureNonExisting"

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    <ClassInitialize()> _
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)

        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Sql
                        'Assert.IsTrue(InitialiseTests(testContext), "Failed initialising for the " & SafeGetTestContextProperty(testContext, TC_KEY_ASSEMBLYNAME, GetType(String)) & " Stored Procedures tests.")
                    Case Else
                        Assert.Fail("Cannot perform Stored Procedures tests as not connected to a SQL Server database")
                End Select
            End Using
        End Using
    End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region


#Region "Tests"

    <TestMethod()> _
    Public Sub StoredProcedureExists()

        If InitialiseTests() Then
            Assert.IsTrue(CBool(SafeGetTestContextProperty(TC_KEY_FOUNDSTOREDPROCEDURE, GetType(Boolean))), "Stored procedure '" & STOREDPROCEDURENAME & "' does not exist.")
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureGetsDataTable()

        If InitialiseTests() Then
            Assert.IsNotNull(SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable)), String.Format("{0} did not retrieve a datatable", SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String))))
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureGetsAtLeastOneRecord()

        If InitialiseTests() Then
            Dim TrySPExisting As Object = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable))
            Dim SPName As String = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String)).ToString

            If TrySPExisting IsNot Nothing AndAlso TypeOf TrySPExisting Is DataTable Then
                Dim SPExisting As DataTable = CType(TrySPExisting, DataTable)

                If SPExisting.Rows.Count = 0 Then
                    Assert.Fail(SPName & " did not retrieve any records")
                End If
            Else
                Assert.Inconclusive("Intialisation failed as did not get a datatable using " & SPName & " so cannot perform this test")
            End If
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureDoesNotGetMultipleRecords()

        If InitialiseTests() Then
            Dim TrySPExisting As Object = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable))
            Dim SPName As String = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String)).ToString

            If TrySPExisting IsNot Nothing AndAlso TypeOf TrySPExisting Is DataTable Then
                Dim SPExisting As DataTable = CType(TrySPExisting, DataTable)

                If SPExisting.Rows.Count > 1 Then
                    Assert.Fail(SPName & " retrieved multiple records")
                End If
            Else
                Assert.Inconclusive("Intialisation failed as did not get a datatable using " & SPName & " so cannot perform this test")
            End If
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureGetsRecordByKey()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("Number", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureGetsRecordByVendaNumber()
        Dim Message As String = ""

        If IntialiseForVendaNumber() Then
            Assert.IsTrue(FieldsMatch("Number", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureGetsRecordByDateRange()
        Dim Message As String = ""

        If IntialiseForDateRange() Then
            Dim Existing As DataTable = CType(SafeGetTestContextProperty(TC_KEY_EXISTING, GetType(DataTable)), DataTable)

            If Existing IsNot Nothing AndAlso TypeOf Existing Is DataTable Then
                Dim NonExisting As DataTable = CType(SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable)), DataTable)

                If NonExisting IsNot Nothing AndAlso TypeOf NonExisting Is DataTable Then
                    If Existing.Rows.Count = NonExisting.Rows.Count Then
                        Dim ExistingRow As Integer

                        For ExistingRow = 0 To Existing.Rows.Count - 1
                            Dim Found As Boolean = False

                            For NonExistingRow = 0 To NonExisting.Rows.Count - 1
                                If CInt(Existing.Rows(ExistingRow).Item("Number")) = CInt(NonExisting.Rows(NonExistingRow).Item("Number")) Then
                                    Found = True
                                    Exit For
                                End If
                            Next
                            If Not Found Then
                                Assert.Fail(STOREDPROCEDURENAME & " (using Date Range parameters) failed to return the row with a Number value  = " & Existing.Rows(ExistingRow).Item("Number").ToString.PadLeft(6, "0"c) & " which was brought back the the existing dynamic sql")
                                Exit For
                            End If
                        Next
                    Else
                        Assert.Fail(STOREDPROCEDURENAME & " (using Date Range parameters) did not return the same number of rows (" & NonExisting.Rows.Count.ToString & " as the existing pervasive sql (" & Existing.Rows.Count.ToString & ")")
                    End If
                End If
            End If
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureGetsRecordByDeliveryStatusRange()
        Dim Message As String = ""

        If IntialiseForDeliveryStatusRange() Then
            Dim Existing As DataTable = CType(SafeGetTestContextProperty(TC_KEY_EXISTING, GetType(DataTable)), DataTable)

            If Existing IsNot Nothing AndAlso TypeOf Existing Is DataTable Then
                Dim NonExisting As DataTable = CType(SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable)), DataTable)

                If NonExisting IsNot Nothing AndAlso TypeOf NonExisting Is DataTable Then
                    If Existing.Rows.Count = NonExisting.Rows.Count Then
                        Dim ExistingRow As Integer

                        For ExistingRow = 0 To Existing.Rows.Count - 1
                            Dim Found As Boolean = False

                            For NonExistingRow = 0 To NonExisting.Rows.Count - 1
                                If CInt(Existing.Rows(ExistingRow).Item("Number")) = CInt(NonExisting.Rows(NonExistingRow).Item("Number")) Then
                                    Found = True
                                    Exit For
                                End If
                            Next
                            If Not Found Then
                                Assert.Fail(STOREDPROCEDURENAME & " (using Delivery Status Range parameters) failed to return the row with a Number value  = " & Existing.Rows(ExistingRow).Item("Number").ToString.PadLeft(6, "0"c) & " which was brought back the the existing dynamic sql")
                                Exit For
                            End If
                        Next
                    Else
                        Assert.Fail(STOREDPROCEDURENAME & " (using Delivery Status Range parameters) did not return the same number of rows (" & NonExisting.Rows.Count.ToString & " as the existing pervasive sql (" & Existing.Rows.Count.ToString & ")")
                    End If
                End If
            End If
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureGetsRecordByOMOrderNumber()
        Dim Message As String = ""

        If IntialiseForOMOrderNumber() Then
            Assert.IsTrue(FieldsMatch("Number", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureGetsRecordByRefundStatusRange()
        Dim Message As String = ""

        If IntialiseForRefundStatusRange() Then
            Dim Existing As DataTable = CType(SafeGetTestContextProperty(TC_KEY_EXISTING, GetType(DataTable)), DataTable)

            If Existing IsNot Nothing AndAlso TypeOf Existing Is DataTable Then
                Dim NonExisting As DataTable = CType(SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable)), DataTable)

                If NonExisting IsNot Nothing AndAlso TypeOf NonExisting Is DataTable Then
                    If Existing.Rows.Count = NonExisting.Rows.Count Then
                        Dim ExistingRow As Integer

                        For ExistingRow = 0 To Existing.Rows.Count - 1
                            Dim Found As Boolean = False

                            For NonExistingRow = 0 To NonExisting.Rows.Count - 1
                                If CInt(Existing.Rows(ExistingRow).Item("Number")) = CInt(NonExisting.Rows(NonExistingRow).Item("Number")) Then
                                    Found = True
                                    Exit For
                                End If
                            Next
                            If Not Found Then
                                Assert.Fail(STOREDPROCEDURENAME & " (using Refund Status Range parameters) failed to return the row with a Number value  = " & Existing.Rows(ExistingRow).Item("Number").ToString.PadLeft(6, "0"c) & " which was brought back the the existing dynamic sql")
                                Exit For
                            End If
                        Next
                    Else
                        Assert.Fail(STOREDPROCEDURENAME & " (using Refund Status Range parameters) did not return the same number of rows (" & NonExisting.Rows.Count.ToString & " as the existing pervasive sql (" & Existing.Rows.Count.ToString & ")")
                    End If
                End If
            End If
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordNumber()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("Number", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordCustomerNumber()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("CustomerNumber", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordCustomerName()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("CustomerName", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordDateOrder()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("DateOrder", Message, FieldType.ftDate), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordDateDelivery()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("DateDelivery", Message, FieldType.ftDate), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordStatus()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("Status", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordIsForDelivery()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("IsForDelivery", Message, FieldType.ftBoolean), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordDeliveryConfirmed()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("DeliveryConfirmed", Message, FieldType.ftBoolean), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordTimesAmended()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("TimesAmended", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordDeliveryAddress1()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("DeliveryAddress1", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordDeliveryAddress2()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("DeliveryAddress2", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordDeliveryAddress3()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("DeliveryAddress3", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordDeliveryAddress4()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("DeliveryAddress4", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordDeliveryPostCode()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("DeliveryPostCode", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordPhoneNumber()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("PhoneNumber", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordPhoneNumberMobile()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("PhoneNumberMobile", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordIsPrinted()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("IsPrinted", Message, FieldType.ftBoolean), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordNumberReprints()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("NumberReprints", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordRevisionNumber()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("RevisionNumber", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordMerchandiseValue()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("MerchandiseValue", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordDeliveryCharge()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("DeliveryCharge", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordQtyOrdered()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("QtyOrdered", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordQtyTaken()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("QtyTaken", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordQtyRefunded()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("QtyRefunded", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordOrderWeight()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("OrderWeight", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordOrderVolume()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("OrderVolume", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordTranDate()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("TranDate", Message, FieldType.ftDate), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordTranTill()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("TranTill", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordTranNumber()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("TranNumber", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordRefundTranDate()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("RefundTranDate", Message, FieldType.ftDate), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordRefundTill()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("RefundTill", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordRefundTranNumber()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("RefundTranNumber", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordDateDespatch()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("DateDespatch", Message, FieldType.ftDate), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordOrderTakerId()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("OrderTakerId", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordManagerId()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("ManagerId", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordDeliveryStatus()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("DeliveryStatus", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordRefundStatus()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("RefundStatus", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordSellingStoreId()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("SellingStoreId", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordSellingStoreOrderId()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("SellingStoreOrderId", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordOMOrderNumber()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("OMOrderNumber", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordCustomerAddress1()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("CustomerAddress1", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordCustomerAddress2()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("CustomerAddress2", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordCustomerAddress3()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("CustomerAddress3", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordCustomerAddress4()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("CustomerAddress4", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordCustomerPostcode()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("CustomerPostcode", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordCustomerEmail()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("CustomerEmail", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordPhoneNumberWork()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("PhoneNumberWork", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordIsSuspended()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("IsSuspended", Message, FieldType.ftBoolean), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordSource()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("Source", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordSourceOrderNumber()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("SourceOrderNumber", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordExtendedLeadTime()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("ExtendedLeadTime", Message, FieldType.ftBoolean), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordDeliveryContactName()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("DeliveryContactName", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordDeliveryContactPhone()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("DeliveryContactPhone", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> _
    Public Sub WhenDataExists_StoredProcedureRetrievedRecordCustomerAccountNo()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("CustomerAccountNo", Message, FieldType.ftString), Message)
        End If
    End Sub
#End Region

#Region "Common Tests"

    Private Function HaveDataToCompare() As Boolean

        If InitialiseTests() Then
            Dim TryExisting As DataTable = CType(SafeGetTestContextProperty(TC_KEY_EXISTING, GetType(DataTable)), DataTable)

            If TryExisting IsNot Nothing AndAlso TypeOf TryExisting Is DataTable Then
                Dim Existing As DataTable = TryExisting

                If Existing.Rows.Count = 1 Then
                    Dim TrySPExisting As DataTable = CType(SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable)), DataTable)
                    Dim SPName As String = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String)).ToString

                    If TrySPExisting IsNot Nothing Then
                        Dim SPExisting As DataTable = TrySPExisting

                        If SPExisting.Rows.Count = 1 Then
                            HaveDataToCompare = True
                        Else
                            Assert.Inconclusive(SPName & " failed to return an existing record so this test has nothing to compare")
                        End If
                    Else
                        Assert.Inconclusive(SPName & " failed to return a datatable so this test has nothing to compare")
                    End If
                Else
                    Assert.Inconclusive("Dynamic sql failed to return an existing single record so this test has nothing to compare against")
                End If
            Else
                Assert.Inconclusive("Dynamic sql failed to return a datatable so this test has nothing to compare against")
            End If
        End If
    End Function
#End Region

#Region "Initialise Test Context methods"

    Private Function CheckForStoredProcedure() As Boolean

        Try
            Using con As New Connection
                Using com As New Command(con)
                    Dim spCheck As DataTable

                    com.CommandText = "Select * From sys.objects Where type = 'P' And name = '" & STOREDPROCEDURENAME & "'"
                    spCheck = com.ExecuteDataTable
                    If spCheck IsNot Nothing AndAlso spCheck.Rows.Count = 1 Then
                        SafeAddTestContextProperty(TC_KEY_FOUNDSTOREDPROCEDURE, True)
                    Else
                        SafeAddTestContextProperty(TC_KEY_FOUNDSTOREDPROCEDURE, False)
                    End If
                    CheckForStoredProcedure = True
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Failed in the attempt to check for existence of the stored procedure (" & STOREDPROCEDURENAME & ".")
        End Try
    End Function

    Private Function GetExisting() As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL(), con)
                    Dim OrderNumber As Integer = 9000
                    Dim ExistingData As DataTable
                    Dim Found As Boolean

                    com.Parameters.Add("@OrderNumber", SqlDbType.NChar)
                    ' Find a date which brings back an existing item
                    Do While OrderNumber < 1000000 And Not Found
                        com.Parameters.Item("@OrderNumber").Value = OrderNumber.ToString.PadLeft(6, "0"c)

                        Dim reader As SqlDataReader = com.ExecuteReader

                        ExistingData = New DataTable
                        ExistingData.Load(reader)
                        reader.Close()

                        If ExistingData.Rows.Count = 1 Then
                            Dim OMONKey As New Key
                            Dim Keys As New Collection

                            With OMONKey
                                .SPName = "OrderNumber"
                                .DBName = "Number"
                                .Caption = "Order Number"
                                .Value = OrderNumber.ToString.PadLeft(6, "0"c)
                            End With
                            Keys.Add(OMONKey, OMONKey.SPName)
                            ' Keep the key for use in retrieval by stored procedure
                            SafeAddTestContextProperty(TC_KEY_EXISTINGKEY, New StoredProcedureKey(Keys))
                            SafeAddTestContextProperty(TC_KEY_EXISTING, ExistingData)
                            Found = True
                        End If
                        OrderNumber += 1
                    Loop
                    GetExisting = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record.")
        End Try
    End Function

    Private Function GetExistingByVendaNumber() As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL("VendaNumber"), con)
                    'Dim VendaNumber As Integer = 726300
                    ' Seeded with value closer to OMOrderNumber range (see below) as tyhis is being incorrectly used in perv dynamic sql
                    Dim VendaNumber As Integer = 10000
                    Dim ExistingData As DataTable
                    Dim Found As Boolean

                    com.Parameters.Add("@VendaNumber", SqlDbType.Int)
                    ' Find a Venda Number which brings back an existing item
                    Do While VendaNumber < 800000 And Not Found
                        com.Parameters.Item("@VendaNumber").Value = VendaNumber

                        Dim reader As SqlDataReader = com.ExecuteReader

                        ExistingData = New DataTable
                        ExistingData.Load(reader)
                        reader.Close()

                        If ExistingData.Rows.Count = 1 Then
                            Dim OMONKey As New Key
                            Dim Keys As New Collection

                            With OMONKey
                                .SPName = "VendaNumber"
                                ' Not correct, but incorrect in the same way as existing pervasive sql is ioncorrect!
                                .DBName = "OMOrderNumber"
                                .Caption = "Venda Number"
                                .Value = VendaNumber
                            End With
                            Keys.Add(OMONKey, OMONKey.SPName)
                            ' Keep the key for use in retrieval by stored procedure
                            SafeAddTestContextProperty(TC_KEY_EXISTINGKEY, New StoredProcedureKey(Keys))
                            SafeAddTestContextProperty(TC_KEY_EXISTING, ExistingData)
                            Found = True
                        End If
                        VendaNumber += 1
                    Loop
                    GetExistingByVendaNumber = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record by Venda Number.")
        End Try
    End Function

    Private Function GetExistingByOMOrderNumber() As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL("OMOrderNumber"), con)
                    Dim OMOrderNumber As Integer = 10000
                    Dim ExistingData As DataTable
                    Dim Found As Boolean

                    com.Parameters.Add("@OMOrderNumber", SqlDbType.Int)
                    ' Find an OM Order Number which brings back an existing item
                    Do While OMOrderNumber < 99999 And Not Found
                        com.Parameters.Item("@OMOrderNumber").Value = OMOrderNumber

                        Dim reader As SqlDataReader = com.ExecuteReader

                        ExistingData = New DataTable
                        ExistingData.Load(reader)
                        reader.Close()

                        If ExistingData.Rows.Count = 1 Then
                            Dim OMONKey As New Key
                            Dim Keys As New Collection

                            With OMONKey
                                .SPName = "OMOrderNumber"
                                .DBName = "OMOrderNumber"
                                .Caption = "OM Order Number"
                                .Value = OMOrderNumber
                            End With
                            Keys.Add(OMONKey, OMONKey.SPName)
                            ' Keep the key for use in retrieval by stored procedure
                            SafeAddTestContextProperty(TC_KEY_EXISTINGKEY, New StoredProcedureKey(Keys))
                            SafeAddTestContextProperty(TC_KEY_EXISTING, ExistingData)
                            Found = True
                        End If
                        OMOrderNumber += 1
                    Loop
                    GetExistingByOMOrderNumber = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record by OM Order Number.")
        End Try
    End Function

    Private Function GetExistingByDeliveryStatusRange() As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL("DeliveryStatus"), con)
                    Dim DeliveryStatusMin As Integer = 100
                    Dim DeliveryStatusMax As Integer = 199
                    Dim ExistingData As DataTable
                    Dim Found As Boolean

                    com.Parameters.Add("@DeliveryStatusMin", SqlDbType.Int)
                    com.Parameters.Add("@DeliveryStatusMax", SqlDbType.Int)
                    ' Find a refund range which brings back an existing item
                    Do While DeliveryStatusMin < 1000 And Not Found
                        com.Parameters.Item("@DeliveryStatusMin").Value = DeliveryStatusMin
                        com.Parameters.Item("@DeliveryStatusMax").Value = DeliveryStatusMax

                        Dim reader As SqlDataReader = com.ExecuteReader

                        ExistingData = New DataTable
                        ExistingData.Load(reader)
                        reader.Close()

                        If ExistingData.Rows.Count > 0 Then
                            Dim OMONKey As New Key
                            Dim Keys As New Collection

                            With OMONKey
                                .SPName = "DeliveryStatusMin"
                                .DBName = "DeliveryStatus"
                                .Caption = "Delivery Status"
                                .Value = DeliveryStatusMin
                            End With
                            Keys.Add(OMONKey, OMONKey.SPName)
                            With OMONKey
                                .SPName = "DeliveryStatusMax"
                                .Value = DeliveryStatusMax
                            End With
                            Keys.Add(OMONKey, OMONKey.SPName)
                            ' Keep the key for use in retrieval by stored procedure
                            SafeAddTestContextProperty(TC_KEY_EXISTINGKEY, New StoredProcedureKey(Keys))
                            SafeAddTestContextProperty(TC_KEY_EXISTING, ExistingData)
                            Found = True
                        End If
                        DeliveryStatusMin += 100
                        DeliveryStatusMax += 100
                    Loop
                    GetExistingByDeliveryStatusRange = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record by Delivery Status Range.")
        End Try
    End Function

    Private Function GetExistingByRefundStatusRange() As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL("RefundStatus"), con)
                    Dim RefundStatusMin As Integer = 100
                    Dim RefundStatusMax As Integer = 199
                    Dim ExistingData As DataTable
                    Dim Found As Boolean

                    com.Parameters.Add("@RefundStatusMin", SqlDbType.Int)
                    com.Parameters.Add("@RefundStatusMax", SqlDbType.Int)
                    ' Find a refund range which brings back an existing item
                    Do While RefundStatusMin < 1000 And Not Found
                        com.Parameters.Item("@RefundStatusMin").Value = RefundStatusMin
                        com.Parameters.Item("@RefundStatusMax").Value = RefundStatusMax

                        Dim reader As SqlDataReader = com.ExecuteReader

                        ExistingData = New DataTable
                        ExistingData.Load(reader)
                        reader.Close()

                        If ExistingData.Rows.Count > 0 Then
                            Dim OMONKey As New Key
                            Dim Keys As New Collection

                            With OMONKey
                                .SPName = "RefundStatusMin"
                                .DBName = "RefundStatus"
                                .Caption = "Refund Status"
                                .Value = RefundStatusMin
                            End With
                            Keys.Add(OMONKey, OMONKey.SPName)
                            With OMONKey
                                .SPName = "RefundStatusMax"
                                .Value = RefundStatusMax
                            End With
                            Keys.Add(OMONKey, OMONKey.SPName)
                            ' Keep the key for use in retrieval by stored procedure
                            SafeAddTestContextProperty(TC_KEY_EXISTINGKEY, New StoredProcedureKey(Keys))
                            SafeAddTestContextProperty(TC_KEY_EXISTING, ExistingData)
                            Found = True
                        End If
                        RefundStatusMin += 100
                        RefundStatusMax += 100
                    Loop
                    GetExistingByRefundStatusRange = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record by Refund Status Range.")
        End Try
    End Function

    Private Function GetExistingByDateRange() As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL("Date"), con)
                    Dim DateMax As Date = Now
                    Dim DateMin As Date = DateAdd(DateInterval.Month, -1, DateMax)
                    Dim ExistingData As DataTable
                    Dim Found As Boolean

                    com.Parameters.Add("@DateStart", SqlDbType.Date)
                    com.Parameters.Add("@DateEnd", SqlDbType.Date)
                    ' Find a date range which brings back an existing item
                    Do While DateMax > DateAdd(DateInterval.Month, 1, Date.MinValue) And Not Found
                        com.Parameters.Item("@DateStart").Value = DateMin
                        com.Parameters.Item("@DateEnd").Value = DateMax

                        Dim reader As SqlDataReader = com.ExecuteReader

                        ExistingData = New DataTable
                        ExistingData.Load(reader)
                        reader.Close()

                        If ExistingData.Rows.Count > 0 Then
                            Dim OMONKey As New Key
                            Dim Keys As New Collection

                            With OMONKey
                                .SPName = "DateStart"
                                .DBName = "DateOrder"
                                .Caption = "Order Date"
                                .Value = DateMin
                            End With
                            Keys.Add(OMONKey, OMONKey.SPName)
                            With OMONKey
                                .SPName = "DateEnd"
                                .Value = DateMax
                            End With
                            Keys.Add(OMONKey, OMONKey.SPName)
                            ' Keep the key for use in retrieval by stored procedure
                            SafeAddTestContextProperty(TC_KEY_EXISTINGKEY, New StoredProcedureKey(Keys))
                            SafeAddTestContextProperty(TC_KEY_EXISTING, ExistingData)
                            Found = True
                        End If
                        DateMax = DateAdd(DateInterval.Month, -1, DateMax)
                        DateMin = DateAdd(DateInterval.Month, -1, DateMax)
                    Loop
                    GetExistingByDateRange = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record by Date Range.")
        End Try
    End Function

    Private Function GetNonExistingKey() As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL(), con)
                    Dim OrderNumber As Integer = 9000
                    Dim ReturnedData As DataTable

                    com.Parameters.Add("@OrderNumber", SqlDbType.NChar)
                    ' Find a date which brings back an existing item
                    Do While OrderNumber < 1000000 And Not GetNonExistingKey
                        com.Parameters.Item("@OrderNumber").Value = OrderNumber.ToString.PadLeft(6, "0"c)

                        Dim reader As SqlDataReader = com.ExecuteReader

                        ReturnedData = New DataTable
                        ReturnedData.Load(reader)
                        reader.Close()

                        If ReturnedData.Rows.Count = 0 Then
                            Dim OMONKey As New Key
                            Dim Keys As New Collection

                            With OMONKey
                                .SPName = "OrderNumber"
                                .DBName = "Number"
                                .Caption = "Order Number"
                                .Value = OrderNumber.ToString.PadLeft(6, "0"c)
                            End With
                            Keys.Add(OMONKey, OMONKey.SPName)
                            SafeAddTestContextProperty(TC_KEY_NONEXISTINGKEY, New StoredProcedureKey(Keys))
                            'SafeAddTestContextProperty(TC_KEY_NONEXISTINGKEY, New StoredProcedureKey(OrderNumber))
                            GetNonExistingKey = True
                        End If
                        OrderNumber += 1
                    Loop
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a non existing record key.")
        End Try
    End Function

    Private Function GetNonExistingKeyByVendaNumber() As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL("VendaNumber"), con)
                    Dim VendaNumber As Integer = 726300
                    Dim ReturnedData As DataTable
                    Dim Found As Boolean

                    com.Parameters.Add("@VendaNumber", SqlDbType.Int)
                    ' Find a date which brings back an existing item
                    Do While VendaNumber < 800000 And Not Found
                        com.Parameters.Item("@VendaNumber").Value = VendaNumber

                        Dim reader As SqlDataReader = com.ExecuteReader

                        ReturnedData = New DataTable
                        ReturnedData.Load(reader)
                        reader.Close()

                        If ReturnedData.Rows.Count = 0 Then
                            Dim OMONKey As New Key
                            Dim Keys As New Collection

                            With OMONKey
                                .SPName = "VendaNumber"
                                ' Not correct, but incorrect in the same way as existing pervasive sql is ioncorrect!
                                .DBName = "OMOrderNumber"
                                .Caption = "Venda Number"
                                .Value = VendaNumber
                            End With
                            Keys.Add(OMONKey, OMONKey.SPName)
                            SafeAddTestContextProperty(TC_KEY_NONEXISTINGKEY, New StoredProcedureKey(Keys))
                            'SafeAddTestContextProperty(TC_KEY_NONEXISTINGKEY, New StoredProcedureKey(VendaNumber))
                            Found = True
                        End If
                        VendaNumber += 1
                    Loop
                    GetNonExistingKeyByVendaNumber = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a nom existing record by Venda Number.")
        End Try
    End Function

    Private Function GetNonExistingKeyByOMOrderNumber() As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL("OMOrderNumber"), con)
                    Dim OMOrderNumber As Integer = 10000
                    Dim ReturnedData As DataTable
                    Dim Found As Boolean

                    com.Parameters.Add("@OMOrderNumber", SqlDbType.Int)
                    ' Find a date which brings back an existing item
                    Do While OMOrderNumber < 99999 And Not Found
                        com.Parameters.Item("@OMOrderNumber").Value = OMOrderNumber

                        Dim reader As SqlDataReader = com.ExecuteReader

                        ReturnedData = New DataTable
                        ReturnedData.Load(reader)
                        reader.Close()

                        If ReturnedData.Rows.Count = 0 Then
                            Dim OMONKey As New Key
                            Dim Keys As New Collection

                            With OMONKey
                                .SPName = "OMOrderNumber"
                                .DBName = "OMOrderNumber"
                                .Caption = "OM Order Number"
                                .Value = OMOrderNumber
                            End With
                            Keys.Add(OMONKey, OMONKey.SPName)
                            SafeAddTestContextProperty(TC_KEY_NONEXISTINGKEY, New StoredProcedureKey(Keys))
                            Found = True
                        End If
                        OMOrderNumber += 1
                    Loop
                    GetNonExistingKeyByOMOrderNumber = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a non existing record key by OM Order Number.")
        End Try
    End Function

    Private Function GetNonExistingKeyByDeliveryStatusRange() As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL("DeliveryStatus"), con)
                    Dim DeliveryStatusMin As Integer = 100
                    Dim DeliveryStatusMax As Integer = 199
                    Dim ExistingData As DataTable
                    Dim Found As Boolean

                    com.Parameters.Add("@DeliveryStatusMin", SqlDbType.Int)
                    com.Parameters.Add("@DeliveryStatusMax", SqlDbType.Int)
                    ' Find a delivery status range which does not bring back an existing item
                    ' artficially high upper limit to ensure get a 0 result if all else fails (normally refund status would not exceed 999)
                    Do While DeliveryStatusMin < 1100 And Not Found
                        com.Parameters.Item("@DeliveryStatusMin").Value = DeliveryStatusMin
                        com.Parameters.Item("@DeliveryStatusMax").Value = DeliveryStatusMax

                        Dim reader As SqlDataReader = com.ExecuteReader

                        ExistingData = New DataTable
                        ExistingData.Load(reader)
                        reader.Close()

                        If ExistingData.Rows.Count = 0 Then
                            Dim OMONKey As New Key
                            Dim Keys As New Collection

                            With OMONKey
                                .SPName = "DeliveryStatusMin"
                                .DBName = "DeliveryStatus"
                                .Caption = "Delivery Status"
                                .Value = DeliveryStatusMin
                            End With
                            Keys.Add(OMONKey, OMONKey.SPName)
                            With OMONKey
                                .SPName = "DeliveryStatusMax"
                                .Value = DeliveryStatusMax
                            End With
                            Keys.Add(OMONKey, OMONKey.SPName)

                            SafeAddTestContextProperty(TC_KEY_NONEXISTINGKEY, New StoredProcedureKey(Keys))
                            Found = True
                        End If
                        DeliveryStatusMin += 100
                        DeliveryStatusMax += 100
                    Loop
                    GetNonExistingKeyByDeliveryStatusRange = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record by Delivery Status Range.")
        End Try
    End Function

    Private Function GetNonExistingKeyByRefundStatusRange() As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL("RefundStatus"), con)
                    Dim RefundStatusMin As Integer = 100
                    Dim RefundStatusMax As Integer = 199
                    Dim ExistingData As DataTable
                    Dim Found As Boolean

                    com.Parameters.Add("@RefundStatusMin", SqlDbType.Int)
                    com.Parameters.Add("@RefundStatusMax", SqlDbType.Int)
                    ' Find a refund status range which does not bring back an existing item
                    ' artficially high upper limit to ensure get a 0 result if all else fails (normally refund status would not exceed 999)
                    Do While RefundStatusMin < 1100 And Not Found
                        com.Parameters.Item("@RefundStatusMin").Value = RefundStatusMin
                        com.Parameters.Item("@RefundStatusMax").Value = RefundStatusMax

                        Dim reader As SqlDataReader = com.ExecuteReader

                        ExistingData = New DataTable
                        ExistingData.Load(reader)
                        reader.Close()

                        If ExistingData.Rows.Count = 0 Then
                            Dim OMONKey As New Key
                            Dim Keys As New Collection

                            With OMONKey
                                .SPName = "RefundStatusMin"
                                .DBName = "RefundStatus"
                                .Caption = "Refund Status"
                                .Value = RefundStatusMin
                            End With
                            Keys.Add(OMONKey, OMONKey.SPName)
                            With OMONKey
                                .SPName = "RefundStatusMax"
                                .Value = RefundStatusMax
                            End With
                            Keys.Add(OMONKey, OMONKey.SPName)
                            SafeAddTestContextProperty(TC_KEY_NONEXISTINGKEY, New StoredProcedureKey(Keys))
                            Found = True
                        End If
                        RefundStatusMin += 100
                        RefundStatusMax += 100
                    Loop
                    GetNonExistingKeyByRefundStatusRange = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record by Refund Status Range.")
        End Try
    End Function

    Private Function GetNonExistingKeyByDateRange() As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL("Date"), con)
                    Dim DateMax As Date = Now
                    Dim DateMin As Date = DateAdd(DateInterval.Month, -1, DateMax)
                    Dim ExistingData As DataTable
                    Dim Found As Boolean

                    com.Parameters.Add("@DateStart", SqlDbType.Date)
                    com.Parameters.Add("@DateEnd", SqlDbType.Date)
                    ' Find a date range which does not bring back an existing item
                    Do While DateMax > DateAdd(DateInterval.Month, 1, Date.MinValue) And Not Found
                        com.Parameters.Item("@DateStart").Value = DateMin
                        com.Parameters.Item("@DateEnd").Value = DateMax

                        Dim reader As SqlDataReader = com.ExecuteReader

                        ExistingData = New DataTable
                        ExistingData.Load(reader)
                        reader.Close()

                        If ExistingData.Rows.Count = 0 Then
                            Dim OMONKey As New Key
                            Dim Keys As New Collection

                            With OMONKey
                                .SPName = "DateStart"
                                .DBName = "DateOrder"
                                .Caption = "Order Date"
                                .Value = DateMin
                            End With
                            Keys.Add(OMONKey, OMONKey.SPName)
                            With OMONKey
                                .SPName = "DateEnd"
                                .Value = DateMax
                            End With
                            Keys.Add(OMONKey, OMONKey.SPName)

                            SafeAddTestContextProperty(TC_KEY_NONEXISTINGKEY, New StoredProcedureKey(Keys))
                            Found = True
                        End If
                        DateMax = DateAdd(DateInterval.Month, -1, DateMax)
                        DateMin = DateAdd(DateInterval.Month, -1, DateMax)
                    Loop
                    GetNonExistingKeyByDateRange = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record by Date Range.")
        End Try
    End Function

    Private Function GetExistingUsingStoredProcedure(Optional ByVal SPParameterName As String = "OrderNumber", Optional ByVal ExistingFieldName As String = "Number") As Boolean

        Using con As New Connection
            Using com As New Command(con)
                Dim TrySPKey As Object

                com.StoredProcedureName = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String)).ToString
                TrySPKey = SafeGetTestContextProperty(TC_KEY_EXISTINGKEY, GetType(StoredProcedureKey))
                If TrySPKey IsNot Nothing AndAlso TypeOf TrySPKey Is StoredProcedureKey Then
                    Dim SPKey As StoredProcedureKey = CType(TrySPKey, StoredProcedureKey)
                    Dim TryExisting As Object

                    'com.AddParameter("@" & SPParameterName, Existing.Rows(0).Item(ExistingFieldName))
                    If SPKey.AddParameters(com) Then
                        TryExisting = SafeGetTestContextProperty(TC_KEY_EXISTING, GetType(DataTable))
                        If TryExisting IsNot Nothing AndAlso TypeOf TryExisting Is DataTable Then
                            Dim Existing As DataTable = CType(TryExisting, DataTable)
                            Dim SPExisting As DataTable

                            SPExisting = com.ExecuteDataTable
                            SafeAddTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, SPExisting)
                            GetExistingUsingStoredProcedure = True
                        End If
                    End If
                End If
            End Using
        End Using
    End Function

    Private Function GetNonExistingUsingStoredProcedure(Optional ByVal SPParameterName As String = "OrderNumber", Optional ByVal ExistingKeyValue As Object = Nothing) As Boolean

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String)).ToString
                '                If ExistingKeyValue Is Nothing Then
                Dim TrySPKey As Object

                TrySPKey = SafeGetTestContextProperty(TC_KEY_NONEXISTINGKEY, GetType(StoredProcedureKey))
                If TrySPKey IsNot Nothing AndAlso TypeOf TrySPKey Is StoredProcedureKey Then
                    Dim SPKey As StoredProcedureKey = CType(TrySPKey, StoredProcedureKey)
                    Dim SPNonExisting As DataTable

                    'com.AddParameter("@" & SPParameterName, SPKey.OrderNumber)
                    If SPKey.AddParameters(com) Then
                        SPNonExisting = com.ExecuteDataTable
                        SafeAddTestContextProperty(TC_KEY_STOREDPROCEDURENONEXISTING, SPNonExisting)
                        GetNonExistingUsingStoredProcedure = True
                    End If
                End If
                'Else
                'Dim SPNonExisting As DataTable

                'com.AddParameter("@" & SPParameterName, ExistingKeyValue)
                'SPNonExisting = com.ExecuteDataTable
                'SafeAddTestContextProperty(TC_KEY_STOREDPROCEDURENONEXISTING, SPNonExisting)
                'GetNonExistingUsingStoredProcedure = True
                'End If
            End Using
        End Using
    End Function

    Private Function GetNonExistingSQL() As String

        Return GetExistingSQL()
    End Function

    Private Function GetExistingSQL(Optional ByVal Key As String = "OrderNumber") As String
        Dim sb As New StringBuilder

        sb.Append("SELECT ")
        sb.Append("ch.NUMB		as Number,")
        sb.Append("ch.CUST		as CustomerNumber ,")
        sb.Append("ch.NAME		as CustomerName ,")
        sb.Append("ch.DATE1		as DateOrder ,")
        sb.Append("ch.DELD		as DateDelivery ,")
        sb.Append("ch.CANC		as Status,")
        sb.Append("ch.DELI		as IsForDelivery ,")
        sb.Append("ch.DELC		as DeliveryConfirmed ,")
        sb.Append("ch.AMDT		as TimesAmended ,")
        sb.Append("ch.ADDR1		as DeliveryAddress1 ,")
        sb.Append("ch.ADDR2		as DeliveryAddress2 ,")
        sb.Append("ch.ADDR3		as DeliveryAddress3 ,")
        sb.Append("ch.ADDR4		as DeliveryAddress4 ,")
        sb.Append("ch.POST		as DeliveryPostCode ,")
        sb.Append("ch.PHON		as PhoneNumber ,")
        sb.Append("ch.MOBP		as PhoneNumberMobile ,")
        sb.Append("ch.PRNT		as IsPrinted ,")
        sb.Append("ch.RPRN		as NumberReprints ,")
        sb.Append("ch.REVI		as RevisionNumber ,")
        sb.Append("ch.MVST		as MerchandiseValue ,")
        sb.Append("ch.DCST		as DeliveryCharge ,")
        sb.Append("ch.QTYO		as QtyOrdered ,")
        sb.Append("ch.QTYT		as QtyTaken ,")
        sb.Append("ch.QTYR		as QtyRefunded ,")
        sb.Append("ch.WGHT		as OrderWeight ,")
        sb.Append("ch.VOLU		as OrderVolume ,")
        sb.Append("ch.SDAT		as TranDate ,")
        sb.Append("ch.STIL		as TranTill ,")
        sb.Append("ch.STRN		as TranNumber ,")
        sb.Append("ch.RDAT		as RefundTranDate ,")
        sb.Append("ch.RTIL		as RefundTill ,")
        sb.Append("ch.RTRN		as RefundTranNumber,")

        sb.Append("ch4.DDAT		as DateDespatch,")
        sb.Append("ch4.OEID		as OrderTakerId,")
        sb.Append("ch4.FDID		as ManagerId,")
        sb.Append("ch4.DeliveryStatus, ")
        sb.Append("ch4.RefundStatus, ")
        sb.Append("ch4.SellingStoreId, ")
        sb.Append("ch4.SellingStoreOrderId, ")
        sb.Append("ch4.OMOrderNumber, ")
        sb.Append("ch4.CustomerAddress1, ")
        sb.Append("ch4.CustomerAddress2, ")
        sb.Append("ch4.CustomerAddress3, ")
        sb.Append("ch4.CustomerAddress4, ")
        sb.Append("ch4.CustomerPostcode, ")
        sb.Append("ch4.CustomerEmail, ")
        sb.Append("ch4.PhoneNumberWork, ")
        sb.Append("ch4.IsSuspended, ")

        sb.Append("ch5.Source, ")
        sb.Append("ch5.SourceOrderNumber, ")
        sb.Append("ch5.ExtendedLeadTime, ")
        sb.Append("ch5.DeliveryContactName, ")
        sb.Append("ch5.DeliveryContactPhone, ")
        sb.Append("ch5.CustomerAccountNo ")

        sb.Append("FROM CORHDR ch ")
        sb.Append("INNER JOIN CORHDR4 ch4 ")
        sb.Append("      on ch.NUMB = ch4.NUMB ")
        'venda sale will have an entry in corhdr5 only!
        sb.Append("LEFT OUTER JOIN CORHDR5 ch5 ")
        sb.Append("      on ch5.NUMB = ch4.NUMB ")
        ' Not in original dynamic sql, but this value set on old orders when converting to sql, so don't want to pick up
        ' any of this as 'existing' as will be filtered out by the stored procedure
        sb.Append("WHERE ch4.DeliveryStatus < 9999 ")
        Select Case Key
            Case "OrderNumber"
                sb.Append("AND ch.NUMB = @OrderNumber")
            Case "VendaNumber"
                ' NB This is wrong, but its is wrong in the same way that Pervasive system is wrong
                ' should be chacking against ch5.SourceOrderNumber
                sb.Append("AND ch4.OMOrderNumber = @VendaNumber")
            Case "OMOrderNumber"
                sb.Append("AND ch4.OMOrderNumber = @OMOrderNumber")
            Case "DeliveryStatus"
                sb.Append("AND ch4.DeliveryStatus >= @DeliveryStatusMin ")
                sb.Append("AND ch4.DeliveryStatus <= @DeliveryStatusMax")
            Case "RefundStatus"
                sb.Append("AND ch4.RefundStatus >= @RefundStatusMin ")
                sb.Append("AND ch4.RefundStatus <= @RefundStatusMax")
            Case "Date"
                sb.Append("AND ch.DATE1 >= @DateStart ")
                sb.Append("AND ch.DATE1 <= @DateEnd")
        End Select

        Return sb.ToString
    End Function

    Private Function InitialiseTests() As Boolean

        If TestContext.Properties(TC_KEY_INITIALISED) Is Nothing Then
            SafeAddTestContextProperty(TC_KEY_ASSEMBLYNAME, ASSEMBLYNAME)
            SafeAddTestContextProperty(TC_KEY_STOREDPROCEDURENAME, STOREDPROCEDURENAME)
            If CheckForStoredProcedure() Then
                If GetExisting() Then
                    If GetNonExistingKey() Then
                        If GetExistingUsingStoredProcedure() Then
                            If GetNonExistingUsingStoredProcedure() Then
                                SafeAddTestContextProperty(TC_KEY_INITIALISED, True)
                            Else
                                Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an non existing record using stored procedure")
                            End If
                        Else
                            Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using stored procedure")
                        End If
                    Else
                        Assert.Inconclusive("Test initialisation failed whilst trying to retrieve a key for a non existing record using dynamic sql")
                    End If
                Else
                    Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using dynamic sql")
                End If
            Else
                Assert.Inconclusive("Test initialisation failed whilst trying to check for the stored procedure (" & SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String)).ToString & ")")
            End If
        End If
        InitialiseTests = CBool(SafeGetTestContextProperty(TC_KEY_INITIALISED, GetType(Boolean)))
    End Function

    Private Function IntialiseForVendaNumber() As Boolean

        If TestContext.Properties(TC_KEY_INITIALISED) Is Nothing Then
            SafeAddTestContextProperty(TC_KEY_ASSEMBLYNAME, ASSEMBLYNAME)
            SafeAddTestContextProperty(TC_KEY_STOREDPROCEDURENAME, STOREDPROCEDURENAME)
            If CheckForStoredProcedure() Then
                If GetExistingByVendaNumber Then
                    If GetNonExistingKeyByVendaNumber() Then
                        ' NB This is wrong, but its is wrong in the same way that Pervasive system is wrong
                        ' should be chacking against ch5.SourceOrderNumber
                        If GetExistingUsingStoredProcedure("VendaNumber", "OMOrderNumber") Then
                            If GetNonExistingUsingStoredProcedure("VendaNumber", "OMOrderNumber") Then
                                SafeAddTestContextProperty(TC_KEY_INITIALISED, True)
                            Else
                                Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an non existing record using stored procedure")
                            End If
                        Else
                            Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using stored procedure")
                        End If
                    Else
                        Assert.Inconclusive("Test initialisation failed whilst trying to retrieve a key for a non existing record using dynamic sql")
                    End If
                Else
                    Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using dynamic sql")
                End If
            Else
                Assert.Inconclusive("Test initialisation failed whilst trying to check for the stored procedure (" & SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String)).ToString & ")")
            End If
        End If
        IntialiseForVendaNumber = CBool(SafeGetTestContextProperty(TC_KEY_INITIALISED, GetType(Boolean)))
    End Function

    Private Function IntialiseForDateRange() As Boolean

        If TestContext.Properties(TC_KEY_INITIALISED) Is Nothing Then
            SafeAddTestContextProperty(TC_KEY_ASSEMBLYNAME, ASSEMBLYNAME)
            SafeAddTestContextProperty(TC_KEY_STOREDPROCEDURENAME, STOREDPROCEDURENAME)
            If CheckForStoredProcedure() Then
                If GetExistingByDateRange() Then
                    If GetNonExistingKeyByDateRange() Then
                        If GetExistingUsingStoredProcedure() Then
                            If GetNonExistingUsingStoredProcedure() Then
                                SafeAddTestContextProperty(TC_KEY_INITIALISED, True)
                            Else
                                Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an non existing record using stored procedure")
                            End If
                        Else
                            Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using stored procedure")
                        End If
                    Else
                        Assert.Inconclusive("Test initialisation failed whilst trying to retrieve a key for a non existing record using dynamic sql")
                    End If
                Else
                    Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using dynamic sql")
                End If
            Else
                Assert.Inconclusive("Test initialisation failed whilst trying to check for the stored procedure (" & SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String)).ToString & ")")
            End If
        End If
        IntialiseForDateRange = CBool(SafeGetTestContextProperty(TC_KEY_INITIALISED, GetType(Boolean)))
    End Function

    Private Function IntialiseForDeliveryStatusRange() As Boolean

        If TestContext.Properties(TC_KEY_INITIALISED) Is Nothing Then
            SafeAddTestContextProperty(TC_KEY_ASSEMBLYNAME, ASSEMBLYNAME)
            SafeAddTestContextProperty(TC_KEY_STOREDPROCEDURENAME, STOREDPROCEDURENAME)
            If CheckForStoredProcedure() Then
                If GetExistingByDeliveryStatusRange() Then
                    If GetNonExistingKeyByDeliveryStatusRange() Then
                        If GetExistingUsingStoredProcedure() Then
                            If GetNonExistingUsingStoredProcedure() Then
                                SafeAddTestContextProperty(TC_KEY_INITIALISED, True)
                            Else
                                Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an non existing record using stored procedure")
                            End If
                        Else
                            Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using stored procedure")
                        End If
                    Else
                        Assert.Inconclusive("Test initialisation failed whilst trying to retrieve a key for a non existing record using dynamic sql")
                    End If
                Else
                    Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using dynamic sql")
                End If
            Else
                Assert.Inconclusive("Test initialisation failed whilst trying to check for the stored procedure (" & SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String)).ToString & ")")
            End If
        End If
        IntialiseForDeliveryStatusRange = CBool(SafeGetTestContextProperty(TC_KEY_INITIALISED, GetType(Boolean)))
    End Function

    Private Function IntialiseForOMOrderNumber() As Boolean

        If TestContext.Properties(TC_KEY_INITIALISED) Is Nothing Then
            SafeAddTestContextProperty(TC_KEY_ASSEMBLYNAME, ASSEMBLYNAME)
            SafeAddTestContextProperty(TC_KEY_STOREDPROCEDURENAME, STOREDPROCEDURENAME)
            If CheckForStoredProcedure() Then
                If GetExistingByOMOrderNumber() Then
                    If GetNonExistingKeyByOMOrderNumber() Then
                        If GetExistingUsingStoredProcedure() Then
                            If GetNonExistingUsingStoredProcedure() Then
                                SafeAddTestContextProperty(TC_KEY_INITIALISED, True)
                            Else
                                Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an non existing record using stored procedure")
                            End If
                        Else
                            Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using stored procedure")
                        End If
                    Else
                        Assert.Inconclusive("Test initialisation failed whilst trying to retrieve a key for a non existing record using dynamic sql")
                    End If
                Else
                    Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using dynamic sql")
                End If
            Else
                Assert.Inconclusive("Test initialisation failed whilst trying to check for the stored procedure (" & SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String)).ToString & ")")
            End If
        End If
        IntialiseForOMOrderNumber = CBool(SafeGetTestContextProperty(TC_KEY_INITIALISED, GetType(Boolean)))
    End Function

    Private Function IntialiseForRefundStatusRange() As Boolean

        If TestContext.Properties(TC_KEY_INITIALISED) Is Nothing Then
            SafeAddTestContextProperty(TC_KEY_ASSEMBLYNAME, ASSEMBLYNAME)
            SafeAddTestContextProperty(TC_KEY_STOREDPROCEDURENAME, STOREDPROCEDURENAME)
            If CheckForStoredProcedure() Then
                If GetExistingByRefundStatusRange() Then
                    If GetNonExistingKeyByRefundStatusRange() Then
                        If GetExistingUsingStoredProcedure() Then
                            If GetNonExistingUsingStoredProcedure() Then
                                SafeAddTestContextProperty(TC_KEY_INITIALISED, True)
                            Else
                                Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an non existing record using stored procedure")
                            End If
                        Else
                            Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using stored procedure")
                        End If
                    Else
                        Assert.Inconclusive("Test initialisation failed whilst trying to retrieve a key for a non existing record using dynamic sql")
                    End If
                Else
                    Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using dynamic sql")
                End If
            Else
                Assert.Inconclusive("Test initialisation failed whilst trying to check for the stored procedure (" & SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String)).ToString & ")")
            End If
        End If
        IntialiseForRefundStatusRange = CBool(SafeGetTestContextProperty(TC_KEY_INITIALISED, GetType(Boolean)))
    End Function

    Private Sub SafeAddTestContextProperty(ByVal PropertyKey As String, ByVal PropertyValue As Object)

        If TestContext.Properties(PropertyKey) IsNot Nothing Then
            TestContext.Properties.Remove(PropertyKey)
        End If
        TestContext.Properties.Add(PropertyKey, PropertyValue)
    End Sub

    Private Function SafeGetTestContextProperty(ByVal PropertyKey As String, ByVal PropertyType As Type) As Object

        SafeGetTestContextProperty = Nothing
        If TestContext IsNot Nothing AndAlso TestContext.Properties(PropertyKey) IsNot Nothing Then
            If PropertyType IsNot Nothing Then
                Select Case PropertyType.Name
                    Case GetType(String).Name
                        SafeGetTestContextProperty = TestContext.Properties(PropertyKey).ToString()
                    Case GetType(Integer).Name
                        Dim Prop As Integer

                        If Integer.TryParse(TestContext.Properties(PropertyKey).ToString(), Prop) Then
                            SafeGetTestContextProperty = Prop
                        End If
                    Case GetType(Boolean).Name
                        Dim Prop As Boolean

                        If Boolean.TryParse(TestContext.Properties(PropertyKey).ToString(), Prop) Then
                            SafeGetTestContextProperty = Prop
                        End If
                    Case GetType(Date).Name
                        Dim Prop As Date

                        If Date.TryParse(TestContext.Properties(PropertyKey).ToString(), Prop) Then
                            SafeGetTestContextProperty = Prop
                        End If
                    Case GetType(DataTable).Name
                        Dim TryDataTable As Object = TestContext.Properties(PropertyKey)
                        Dim Prop As DataTable

                        If TypeOf TryDataTable Is DataTable Then
                            Prop = CType(TryDataTable, DataTable)
                            SafeGetTestContextProperty = Prop
                        End If
                    Case Else
                        SafeGetTestContextProperty = TestContext.Properties(PropertyKey)
                End Select
            Else
                SafeGetTestContextProperty = TestContext.Properties(PropertyKey)
            End If
        End If
    End Function
#End Region

#Region "Field Comparisons"

    Private Enum FieldType
        ftString = 0
        ftInteger
        ftDecimal
        ftBoolean
        ftDate
    End Enum

    Private Function FieldsMatch(ByVal FieldName As String, ByRef Message As String, ByVal Type As FieldType) As Boolean
        Dim Marker As String = "Start"
        Dim SPName As String = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDURENAME, GetType(String)).ToString

        Try
            Dim TryExisting As Object = SafeGetTestContextProperty(TC_KEY_EXISTING, GetType(DataTable))

            Marker = "Existing"
            If TryExisting IsNot Nothing AndAlso TypeOf TryExisting Is DataTable Then
                Dim Existing As DataTable = CType(TryExisting, DataTable)

                If Existing.Rows.Count = 1 Then
                    If Existing.Rows(0).Item(FieldName) IsNot Nothing Then
                        Dim TrySPExisting As Object = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable))

                        Marker = "Retreived"
                        If TrySPExisting IsNot Nothing AndAlso TypeOf TrySPExisting Is DataTable Then
                            Dim SPExisting As DataTable = CType(TrySPExisting, DataTable)

                            If SPExisting.Rows.Count = 1 Then
                                If SPExisting.Rows(0).Item(FieldName) IsNot Nothing Then
                                    Marker = "DBNull"
                                    If TypeOf Existing.Rows(0).Item(FieldName) Is DBNull Then
                                        If TypeOf SPExisting.Rows(0).Item(FieldName) Is DBNull Then
                                            FieldsMatch = True
                                        Else
                                            Message = FieldName & " values do not match as 'existing' is Null and retreived by stored procedure is not Null"
                                        End If
                                    Else
                                        If TypeOf SPExisting.Rows(0).Item(FieldName) Is DBNull Then
                                            Message = FieldName & " values do not match as 'existing' is not Null and retreived by stored procedure is Null"
                                        Else
                                            Marker = "Select"
                                            Select Case Type
                                                Case FieldType.ftString
                                                    Marker = "String"
                                                    FieldsMatch = StringsMatch(Existing.Rows(0).Item(FieldName), SPExisting.Rows(0).Item(FieldName), Message)
                                                Case FieldType.ftInteger
                                                    Marker = "Integer"
                                                    FieldsMatch = IntegersMatch(Existing.Rows(0).Item(FieldName), SPExisting.Rows(0).Item(FieldName), Message)
                                                Case FieldType.ftBoolean
                                                    Marker = "Boolean"
                                                    FieldsMatch = BooleansMatch(Existing.Rows(0).Item(FieldName), SPExisting.Rows(0).Item(FieldName), Message)
                                                Case FieldType.ftDecimal
                                                    Marker = "Decimal"
                                                    FieldsMatch = DecimalsMatch(Existing.Rows(0).Item(FieldName), SPExisting.Rows(0).Item(FieldName), Message)
                                                Case FieldType.ftDate
                                                    Marker = "Date"
                                                    FieldsMatch = DatesMatch(Existing.Rows(0).Item(FieldName), SPExisting.Rows(0).Item(FieldName), Message)
                                                Case Else
                                                    Marker = "Not recognised"
                                                    Assert.Inconclusive("Cannot check field " & FieldName & " as field type specified is no recognised")
                                            End Select
                                        End If
                                    End If
                                Else
                                    Marker = "Error"
                                    Message = "Stored Procedure " & SPName & " field name " & FieldName & " is not a field name in the original dynamic sql"
                                End If
                            Else
                                Marker = "Error"
                                Message = "Stored Procedure " & SPName & " failed to bring back the record so cannot compare this field"
                            End If
                        Else
                            Marker = "Error"
                            Message = "Stored Procedure " & SPName & " failed to bring back any data so cannot compare this field"
                        End If
                    Else
                        Marker = "Error"
                        Message = "Cannot check field " & FieldName & ", it is not a field in the original dynamic sql row."
                    End If
                Else
                    Marker = "Error"
                    Message = "Dynamic sql failed to bring back the record so cannot compare this field"
                End If
            Else
                Marker = "Error"
                Message = "Dynamic sql failed to bring back any data so cannot compare this field"
            End If
        Catch ex As Exception
            Select Case Marker
                Case "Start"
                    Message = "Cannot check field " & FieldName & " problem at start of FieldsMatch function. " & ex.Message
                Case "Existing"
                    Message = "Cannot check field " & FieldName & ", it is not a field in the original dynamic sql row."
                Case "Retreived"
                    Message = "Stored Procedure " & SPName & " uses a different field name to the one (" & FieldName & ") used in the original dynamic sql."
                Case "DBNull"
                    Message = "Cannot check field " & FieldName & " problem whilst determining if field values are Null. " & ex.Message
                Case "Select"
                    Message = "Cannot check field " & FieldName & " problem processing field type. " & ex.Message
                Case "String"
                    Message = "Cannot check field " & FieldName & " problem processing string field type. " & ex.Message
                Case "Integer"
                    Message = "Cannot check field " & FieldName & " problem processing integer field type. " & ex.Message
                Case "Boolean"
                    Message = "Cannot check field " & FieldName & " problem processing boolean field type. " & ex.Message
                Case "Decimal"
                    Message = "Cannot check field " & FieldName & " problem processing decimal field type. " & ex.Message
                Case "Date"
                    Message = "Cannot check field " & FieldName & " problem processing date field type. " & ex.Message
                Case "Not recognised"
                    Message = "Cannot check field " & FieldName & " problem processing unrecognised field type. " & ex.Message
                Case "Error"
                    ' Already got a message, so nothing to do here
                Case Else
                    Message = "Cannot check field " & FieldName & " problem with FieldsMatch function. " & ex.Message
            End Select
        End Try
    End Function

    Private Function BooleansMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Booleans Match fail (" & FailReason & ")"
        Else
            If TypeOf Against Is Boolean Then
                Dim AgainstAsBoolean As Boolean = CBool(Against)

                If TypeOf Compare Is Boolean Then
                    Dim CompareAsBoolean As Boolean = CBool(Compare)

                    If AgainstAsBoolean = CompareAsBoolean Then
                        BooleansMatch = True
                    Else
                        FailReason = "Booleans Match fail as 'Against' (" & AgainstAsBoolean.ToString & ") and 'Compare' (" & CompareAsBoolean.ToString & ") are not the same"
                    End If
                Else
                    FailReason = "Booleans Match fail as 'Compare' is not a Boolean"
                End If
            Else
                FailReason = "Booleans Match fail as 'Against' is not a Boolean"
            End If
        End If
        Message = FailReason
    End Function

    Private Function DatesMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String, Optional ByVal DateInterval As DateInterval = DateInterval.Second, Optional ByVal AreNullable As Boolean = False) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Dates Match fail (" & FailReason & ")"
        Else
            Dim AgainstDate As Date
            Dim CompareDate As Date

            If AreNullable Then
                If TypeOf Against Is Date? Then
                    Dim AgainstAsNullableDate As Date?

                    If TypeOf Compare Is Date? Then
                        Dim CompareAsNullableDate As Date?

                        If AgainstAsNullableDate.HasValue Then
                            AgainstDate = AgainstAsNullableDate.Value
                            If CompareAsNullableDate.HasValue Then
                                CompareDate = CompareAsNullableDate.Value
                            Else
                                FailReason = "Dates Match fail as 'Against' is not Null but 'Compare' is Null"
                            End If
                        Else
                            If CompareAsNullableDate.HasValue Then
                                FailReason = "Dates Match fail as 'Against' is Null but 'Compare' is not Null"
                            Else
                                Message = FailReason
                                DatesMatch = True
                                Exit Function
                            End If
                        End If
                    Else
                        FailReason = "Dates Match fail as 'Compare' is not a Nullable Date"
                    End If
                Else
                    FailReason = "Dates Match fail as 'Compare' is not a Nullable Date"
                End If
            Else
                If TypeOf Against Is Date Then
                    AgainstDate = CDate(Against)
                    If TypeOf Compare Is Date Then
                        CompareDate = CDate(Compare)
                    Else
                        FailReason = "Dates Match fail as 'Compare' is not a Date"
                    End If
                Else
                    FailReason = "Dates Match fail as 'Compare' is not a Date"
                End If
            End If
            Dim Diff As Boolean = DateDiff(DateInterval, AgainstDate, CompareDate) <> 0

            If Diff Then
                Dim Interval As String

                Select Case DateInterval
                    Case Microsoft.VisualBasic.DateInterval.Day
                        Interval = "days"
                    Case Microsoft.VisualBasic.DateInterval.DayOfYear
                        Interval = "days (of year)"
                    Case Microsoft.VisualBasic.DateInterval.Hour
                        Interval = "hours"
                    Case Microsoft.VisualBasic.DateInterval.Month
                        Interval = "months"
                    Case Microsoft.VisualBasic.DateInterval.Quarter
                        Interval = "quarters"
                    Case Microsoft.VisualBasic.DateInterval.Second
                        Interval = "seconds"
                    Case Microsoft.VisualBasic.DateInterval.Weekday
                        Interval = "week days"
                    Case Microsoft.VisualBasic.DateInterval.WeekOfYear
                        Interval = "weeks (of year)"
                    Case Microsoft.VisualBasic.DateInterval.Year
                        Interval = "years"
                    Case Else
                        Interval = "unknown units"
                End Select
                FailReason = "Dates Match fail as 'Against' (" & AgainstDate.ToString("F") & ") is " & CInt(Diff).ToString & " " & Interval & " different to 'Compare' (" & AgainstDate.ToString("F") & ")"
            Else
                DatesMatch = True
            End If
        End If
        Message = FailReason
    End Function

    Private Function DecimalsMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Integers Match fail (" & FailReason & ")"
        Else
            Dim AgainstAsDecimal As Decimal

            If Decimal.TryParse(Against.ToString, AgainstAsDecimal) Then
                Dim CompareAsDecimal As Decimal

                If Decimal.TryParse(Compare.ToString, CompareAsDecimal) Then
                    If AgainstAsDecimal <> CompareAsDecimal Then
                        FailReason = "Decimals Match fail as 'Against' (= " & AgainstAsDecimal & ") and 'Compare' (= " & CompareAsDecimal & ") are not the same"
                    Else
                        DecimalsMatch = True
                    End If
                Else
                    FailReason = "Decimals Match fail as 'Compare' is not a Decimal"
                End If
            Else
                FailReason = "Decimals Match fail as 'Against' is not a Decimal"
            End If
        End If
        Message = FailReason
    End Function

    Private Function IntegersMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Integers Match fail (" & FailReason & ")"
        Else
            Dim AgainstAsInteger As Integer

            If Integer.TryParse(Against.ToString, AgainstAsInteger) Then
                Dim CompareAsInteger As Integer

                If Integer.TryParse(Compare.ToString, CompareAsInteger) Then
                    If AgainstAsInteger <> CompareAsInteger Then
                        FailReason = "Integers Match fail as 'Against' (= " & AgainstAsInteger & ") and 'Compare' (= " & CompareAsInteger & ") are not the same"
                    Else
                        IntegersMatch = True
                    End If
                Else
                    FailReason = "Integers Match fail as 'Compare' is not an Integer"
                End If
            Else
                FailReason = "Integers Match fail as 'Against' is not an Integer"
            End If
        End If
        Message = FailReason
    End Function

    Private Function StringsMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String, Optional ByVal StringComparisonType As System.StringComparison = StringComparison.CurrentCulture) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Strings Match fail (" & FailReason & ")"
        Else
            If TypeOf Against Is String Then
                Dim AgainstAsString As String = Against.ToString

                If TypeOf Compare Is String Then
                    Dim CompareAsString As String = Compare.ToString

                    If AgainstAsString = String.Empty Then
                        If CompareAsString = String.Empty Then
                            StringsMatch = True
                        Else
                            FailReason = "Strings Match fail as 'Against' is Empty but 'Compare' is not Empty"
                        End If
                    Else
                        If CompareAsString = String.Empty Then
                            FailReason = "Strings Match fail as 'Against' is not Empty but 'Compare' is Empty"
                        Else
                            If String.Equals(AgainstAsString, CompareAsString, StringComparisonType) Then
                                StringsMatch = True
                            Else
                                FailReason = "Strings Match fail as 'Against' (= " & AgainstAsString & ") and 'Compare' (= " & CompareAsString & ") are not the same"
                            End If
                        End If
                    End If
                Else
                    FailReason = "Strings Match fail as 'Compare' is not a string"
                End If
            Else
                FailReason = "Strings Match fail as 'Against' is not a string"
            End If
        End If
        Message = FailReason
    End Function

    Private Function ObjectsBothNotNothingOrBothSomething(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
        Dim FailReason As String

        If Against Is Nothing Then
            If Compare IsNot Nothing Then
                FailReason = "Against is 'Nothing', Compare is not 'Nothing'"
            Else
                ObjectsBothNotNothingOrBothSomething = True
            End If
        Else
            If Compare Is Nothing Then
                FailReason = "Against is not 'Nothing', Compare is 'Nothing'"
            Else
                ObjectsBothNotNothingOrBothSomething = True
            End If
        End If
    End Function
#End Region
End Class

Friend Structure Key

    Private storedProcedureName As String
    Private databaseName As String
    Private friendlyName As String
    Private keyValue As Object

    Public Property SPName() As String
        Get
            SPName = storedProcedureName
        End Get
        Set(ByVal value As String)
            storedProcedureName = value
        End Set
    End Property

    Public Property DBName() As String
        Get
            DBName = databaseName
        End Get
        Set(ByVal value As String)
            databaseName = value
        End Set
    End Property

    Public Property Caption() As String
        Get
            Caption = friendlyName
        End Get
        Set(ByVal value As String)
            friendlyName = value
        End Set
    End Property

    Public Property Value() As Object
        Get
            Value = keyValue
        End Get
        Set(ByVal value As Object)
            keyvalue = value
        End Set
    End Property
End Structure

Friend Class StoredProcedureKey
    Private OrderNo As String

    Private Keys As Collection

    Public Property OrderNumber() As String
        Get
            OrderNumber = OrderNo.PadLeft(6, "0"c)
        End Get
        Set(ByVal value As String)
            OrderNo = value
        End Set
    End Property

    Public Sub New(ByVal OrderNumb As String)

        OrderNo = OrderNumb
    End Sub

    Public Sub New(ByVal SPKeys As Collection)

        Keys = New Collection
        If SPKeys IsNot Nothing Then
            For Each SPKey As Key In SPKeys
                Keys.Add(SPKey, SPKey.SPName)
            Next
        End If
    End Sub

    Public Overrides Function ToString() As String

        ToString = ""
        If Keys IsNot Nothing Then
            For Each SPKey As Key In Keys
                If ToString.Length > 0 Then
                    ToString &= ";"
                End If
                ToString &= SPKey.Caption & " = '" & SPKey.Value.ToString & "'"
            Next
            'ToString = "Order Number = '" & OrderNumber & "'"
        Else
            ToString = "Empty StoreProcedureKey"
        End If
    End Function

    Public Function ToShortString() As String

        'ToShortString = "Number = '" & OrderNumber & "'"
        ToShortString = ToString()
    End Function

    Public Function GetKeyString(ByRef From As DataRow) As String

        GetKeyString = ""
        If Keys IsNot Nothing Then
            For Each SPKey As Key In Keys
                If ToString.Length > 0 Then
                    GetKeyString &= ";"
                End If
                GetKeyString &= SPKey.Caption & " = '" & From.Item(SPKey.DBName).ToString & "'"
            Next
        Else
            GetKeyString = "Empty StoreProcedureKey"
        End If
        'GetKeyString = "Order Number = '" & From.Item("NUMB").ToString.PadLeft(6, "0"c) & "'"
    End Function

    Public Function GetKeyShortString(ByRef From As DataRow) As String

        GetKeyShortString = GetKeyString(From)
        'GetKeyShortString = "Number = '" & From.Item("NUMB").ToString.PadLeft(6, "0"c) & "'"
    End Function

    Public Function AddParameters(ByRef ComToAddTo As Command) As Boolean

        If Keys IsNot Nothing And ComToAddTo IsNot Nothing Then
            For Each SPKey As Key In Keys
                ComToAddTo.AddParameter("@" & SPKey.SPName, SPKey.Value)
            Next
            AddParameters = True
        End If
    End Function

    Public Function AddParameters(ByRef ComToAddTo As SqlCommand) As Boolean

        If Keys IsNot Nothing And ComToAddTo IsNot Nothing Then
            For Each SPKey As Key In Keys
                ComToAddTo.Parameters.Add(New SqlParameter("@" & SPKey.SPName, SPKey.Value))
            Next
            AddParameters = True
        End If
    End Function
End Class
