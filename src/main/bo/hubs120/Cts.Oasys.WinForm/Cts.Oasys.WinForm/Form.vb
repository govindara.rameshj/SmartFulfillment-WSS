﻿Imports System.Threading

<System.ComponentModel.ToolboxItem(False)> Public Class Form
    Private _userId As Int32
    Private _workstationId As Int32
    Private _securityLevel As Int32
    Private _runParameters As String = String.Empty
    Private _icon As Icon
    Private _appName As String = String.Empty
    Private _StatusMessage As ToolStripLabel
    Private _StatusProgress As ToolStripProgressBar

    Protected ReadOnly Property RunParameters() As String
        Get
            Return _runParameters
        End Get
    End Property
    Protected ReadOnly Property UserId() As Int32
        Get
            Return _userId
        End Get
    End Property
    Protected ReadOnly Property WorkstationId() As Int32
        Get
            Return _workstationId
        End Get
    End Property
    Protected ReadOnly Property SecurityLevel() As Int32
        Get
            Return _securityLevel
        End Get
    End Property
    Public Property AppName() As String
        Get
            Return _appName
        End Get
        Set(ByVal value As String)
            _appName = value
        End Set
    End Property
    Public Property Icon() As Icon
        Get
            If _icon Is Nothing Then
                Return New Icon(My.Resources.CTS, 16, 16)
            End If
            Return _icon
        End Get
        Set(ByVal value As Icon)
            _icon = value
        End Set
    End Property


    Public Sub New()
        InitializeComponent()
        AddGlobalErrorHandler()
    End Sub

    Public Sub New(ByVal userId As Int32, ByVal workstationId As Int32, ByVal securityLevel As Int32, ByVal runParameters As String)
        InitializeComponent()
        AddGlobalErrorHandler()
        _userId = userId
        _workstationId = workstationId
        _securityLevel = securityLevel
        _runParameters = runParameters
    End Sub


    Private Sub AddGlobalErrorHandler()
        AddHandler Application.ThreadException, New ThreadExceptionEventHandler(AddressOf Application_ThreadException)
    End Sub

    Private Sub Application_ThreadException(ByVal sender As Object, ByVal e As System.Threading.ThreadExceptionEventArgs)
        'trace log and write to event log
        Trace.WriteLine(e.Exception.ToString, _appName)
        EventLog.WriteEntry("Application", e.Exception.ToString, EventLogEntryType.Error)

        'display exception message to user in dialog box
        MessageBox.Show(e.Exception.Message, _appName, MessageBoxButtons.OK, MessageBoxIcon.Error)

    End Sub


    Protected Friend Sub SetStatusMessage(ByVal statusMessage As ToolStripLabel)
        _StatusMessage = statusMessage
    End Sub

    Protected Friend Sub SetStatusProgress(ByVal statusProgress As ToolStripProgressBar)
        _StatusProgress = statusProgress
    End Sub


    Protected Friend Overridable Sub DoProcessing()
    End Sub

    Protected Friend Overridable Sub Form_Load(ByVal sender As Object, ByVal e As EventArgs)
        Trace.WriteLine(My.Resources.FormLoad, _appName)
    End Sub

    Protected Friend Overridable Sub Form_Closed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs)
        Trace.WriteLine(My.Resources.FormUnload, _appName)
    End Sub

    Protected Friend Overridable Sub Form_Closing(ByVal sender As Object, ByVal e As FormClosingEventArgs)
    End Sub

    Protected Friend Overridable Sub Form_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs)
    End Sub

    Protected Friend Overridable Sub Form_KeyUp(ByVal sender As Object, ByVal e As KeyEventArgs)
    End Sub

    Protected Friend Overridable Sub Form_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
    End Sub

    Protected Friend Overridable Function Form_ProcessCmdKey(ByRef msg As System.Windows.Forms.Message, ByVal keyData As System.Windows.Forms.Keys) As Boolean

    End Function



    '''' <summary>
    '''' Gets name of property, parameter syntax as (function (f as 'classname') f.'propertyname')
    '''' </summary>
    '''' <typeparam name="T"></typeparam>
    '''' <typeparam name="R"></typeparam>
    '''' <param name="expression"></param>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Public Shared Function GetPropertyName(Of T, R)(ByVal expression As Expression(Of Func(Of T, R))) As String
    '    Dim memberExpression As MemberExpression = DirectCast(expression.Body, MemberExpression)
    '    Return (memberExpression.Member.Name)
    'End Function

    ''' <summary>
    ''' Clears status bar
    ''' </summary>
    ''' <remarks></remarks>
    Protected Friend Sub DisplayStatus()

        If _StatusMessage IsNot Nothing Then
            _StatusMessage.Text = String.Empty
            _StatusMessage.ToolTipText = String.Empty
            _StatusMessage.GetCurrentParent.Refresh()
        End If

    End Sub

    ''' <summary>
    ''' Displays message in status bar
    ''' </summary>
    ''' <param name="message"></param>
    ''' <remarks></remarks>
    Protected Friend Sub DisplayStatus(ByVal message As String)

        If _StatusMessage IsNot Nothing Then
            _StatusMessage.Text = message
            _StatusMessage.ToolTipText = message
            _StatusMessage.GetCurrentParent.Refresh()
        End If

    End Sub
    ''' <summary>
    ''' Displays a friendly informational message to the user
    ''' </summary>
    ''' <param name="message"></param>
    ''' <history></history>
    ''' <remarks></remarks>
    Protected Friend Sub FriendlyMessage(ByVal message As String)
        MessageBox.Show(message, _appName, MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    ''' <summary>
    ''' Display a warning with an option to continue based on a yes or no answer
    ''' </summary>
    ''' <param name="message"></param>
    ''' <returns>DialogResult</returns>
    ''' <remarks></remarks>
    Protected Friend Function YesNoWarning(ByVal message As String) As DialogResult
        Return MessageBox.Show(message, _appName, MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
    End Function

    ''' <summary>
    ''' Display warning message in dialog box
    ''' </summary>
    ''' <param name="message"></param>
    ''' <remarks></remarks>
    Protected Friend Sub DisplayWarning(ByVal message As String)
        MessageBox.Show(message, _appName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
    End Sub

    ''' <summary>
    ''' Clears progress and status bar
    ''' </summary>
    ''' <remarks></remarks>
    Protected Friend Sub DisplayProgress()

        If _StatusProgress IsNot Nothing Then
            _StatusProgress.Visible = False
            _StatusProgress.Value = 0

            'reset status label
            _StatusMessage.Text = ""
            _StatusMessage.ToolTipText = ""
            _StatusMessage.BackColor = Drawing.SystemColors.Control
        End If

    End Sub

    ''' <summary>
    ''' Displays progress percentage
    ''' </summary>
    ''' <param name="percent"></param>
    ''' <param name="message"></param>
    ''' <remarks></remarks>
    Protected Friend Sub DisplayProgress(ByVal percent As Integer, Optional ByVal message As String = "")

        If _StatusProgress IsNot Nothing Then
            _StatusProgress.Visible = True
            _StatusProgress.Value = percent

            If message <> "" Then DisplayStatus(message)
        End If

    End Sub


    Public Overloads Sub Show()
        'checks if the control has a parent and creates host form if not
        If Me.ParentForm Is Nothing Then
            Dim host As New HostForm()
            host.AppPanel.Controls.Add(Me)
            host.Show()
        End If

    End Sub

End Class
