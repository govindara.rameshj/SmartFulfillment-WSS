Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.Order
Imports Cts.Oasys.Core.Order.Qod
Imports Cts.Oasys.Core.Order.Qod.State
Imports Cts.Oasys.Core.System.Store
Imports System.Data

Public Class DespatchNote

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal qod As QodHeader, ByVal sellingStore As Store, ByVal fulfillStoreId As Integer)
        InitializeComponent()

        'add header details
        xrSellingStoreId.Text = qod.SellingStoreId.ToString("0000")
        xrSellingStoreOrderId.Text = qod.SellingStoreOrderId.ToString("000000")
        xrStoreId.Text = fulfillStoreId.ToString("0000")
        xrOrderNumber.Text = qod.Number
        'Check if this is a delivery or a collection
        If qod.IsForDelivery Then
            XrLabel13.Text = "Delivery Id:"
            XrDelColNew.Text = "Delivery Date:"
        End If

        'add customer details
        xrCustName.Text = qod.CustomerName
        xrCustAddress.Text = ConcetenateNewLines(New String() {qod.DeliveryAddress1, qod.DeliveryAddress2, qod.DeliveryAddress3, qod.DeliveryAddress4, qod.DeliveryPostCode})
        xrCustPhone.Text = qod.PhoneNumber
        xrCustPhoneMobile.Text = qod.PhoneNumberMobile
        xrCustPhoneWork.Text = qod.PhoneNumberWork
        xrCustEmail.Text = qod.CustomerEmail

        xrDateOrder.Text = qod.DateOrder.ToShortDateString
        If qod.DateDelivery.HasValue Then
            xrDateDelivery.Text = qod.DateDelivery.Value.ToShortDateString
            xrDateDeliveryDay.Text = qod.DateDelivery.Value.DayOfWeek.ToString
        End If
        xrOrderValue.Text = qod.MerchandiseValue.ToString("c2")

        'add store info and rest of order totals
        xrStoreName.Text = sellingStore.Address1
        xrStoreAddr1.Text = sellingStore.Address2
        xrStoreAddr2.Text = sellingStore.Address3
        xrStoreAddr3.Text = sellingStore.Address4
        xrStorePhone.Text = sellingStore.PhoneNumber
        xrStoreFax.Text = sellingStore.FaxNumber
        xrDatePrinted.Text = Format(Now, "dd/MM/yyyy")
        xrDeliveryCharge.Text = qod.DeliveryCharge.ToString("c2")
        xrTotalCost.Text = qod.MerchandiseValue.ToString("c2")

        'Merge lines for same SKUs and sort them for printing
        Dim PrintLineList As New QodPrintLineList
        For Each QodLine As QodLine In qod.Lines
            If (Not QodLine.IsDeliveryChargeItem) And QodLine.DeliverySource = fulfillStoreId.ToString("0000") And _
            QodLine.QtyToDeliver > 0 Then
                PrintLineList.Add(QodLine)
            End If
        Next

        Me.DataSource = PrintLineList
        xrSkuNumber.DataBindings.Add("Text", PrintLineList, GetPropertyName(Function(f As QodPrintLine) f.SkuNumberDisplay))
        xrSkuDescription.DataBindings.Add("Text", PrintLineList, GetPropertyName(Function(f As QodPrintLine) f.Description))
        xrSkuQty.DataBindings.Add("Text", PrintLineList, GetPropertyName(Function(f As QodPrintLine) f.QuantityDisplay))
        XrSkuPicked.DataBindings.Add("Text", PrintLineList, GetPropertyName(Function(f As QodPrintLine) f.DisplayQtyPicked))
        xrSkuPrice.DataBindings.Add("Text", PrintLineList, GetPropertyName(Function(f As QodPrintLine) f.PriceDisplay))
        xrSkuWeight.DataBindings.Add("Text", PrintLineList, GetPropertyName(Function(f As QodPrintLine) f.WeightDisplay))
        xrSkuVolume.DataBindings.Add("Text", PrintLineList, GetPropertyName(Function(f As QodPrintLine) f.VolumeDisplay))

        'add delivery instructions and order totals
        Dim sb As New StringBuilder
        For Each txt As QodText In qod.Texts.GetTexts(QodTextType.Type.DeliveryInstruction)
            sb.Append(txt.Text.Trim & Space(1))
        Next
        xrInstructions.Text = sb.ToString

        'add watermark if reprint
        If qod.RevisionNumber > 0 Then
            xrRevisionNumber.Text = qod.RevisionNumber.ToString("n0")
            xrRevisionNumber.Visible = True
            xrRevisionNumberLabel.Visible = True

            Watermark.Text = "REVISION " & qod.RevisionNumber
            Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal
            Watermark.Font = New Font(Me.Watermark.Font.FontFamily, 40)
            Watermark.ForeColor = Color.Black
            Watermark.TextTransparency = 230
            Watermark.ShowBehind = True
            Watermark.PageRange = "1-5"
        End If

        'Add barcode info
        xrOrderBarCode.Text = Microsoft.VisualBasic.Right(qod.SellingStoreId.ToString("000"), 3) & _
        qod.TranTill & qod.TranNumber & Microsoft.VisualBasic.Format(qod.TranDate, "ddMMyy")
        If xrOrderBarCode.Text.Length < 15 Then 'Some info is missing - may be an old order, so don't print the barcode
            xrOrderBarCode.Visible = False
        End If

        If qod.ExtendedLeadTime Then XrLabel3.Visible = True
        XrSubreport1.ReportSource = New Cts.Oasys.WinForm.Order.SaleOrders.OtherDelivery(qod, sellingStore, fulfillStoreId)
    End Sub

    Private Sub DespatchNote_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles MyBase.BeforePrint
    End Sub
End Class