<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits Cts.Oasys.WinForm.Form

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.uxOpenOrderGrid = New DevExpress.XtraGrid.GridControl
        Me.uxOpenOrderView = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.uxExitButton = New DevExpress.XtraEditors.SimpleButton
        Me.uxRetrieveButton = New DevExpress.XtraEditors.SimpleButton
        Me.uxMaintainButton = New DevExpress.XtraEditors.SimpleButton
        Me.uxOrderTabControl = New DevExpress.XtraTab.XtraTabControl
        Me.uxNonZeroStockTab = New DevExpress.XtraTab.XtraTabPage
        Me.uxNonZeroGrid = New DevExpress.XtraGrid.GridControl
        Me.uxNonZeroView = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.uxOpenOrderTab = New DevExpress.XtraTab.XtraTabPage
        Me.uxOMOrderNumber = New System.Windows.Forms.TextBox
        Me.lblOMNumber = New System.Windows.Forms.Label
        Me.uxShowFulfilCheck = New DevExpress.XtraEditors.CheckEdit
        Me.uxAllOrderTab = New DevExpress.XtraTab.XtraTabPage
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.uxEndDate = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.uxStartDate = New DevExpress.XtraEditors.DateEdit
        Me.uxAllOrderGrid = New DevExpress.XtraGrid.GridControl
        Me.uxAllOrderView = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.uxFullOrderTab = New DevExpress.XtraTab.XtraTabPage
        Me.uxFullStoreOrderErrorLabel = New DevExpress.XtraEditors.LabelControl
        Me.uxFullStoreIdErrorLabel = New DevExpress.XtraEditors.LabelControl
        Me.uxFullOmOrderErrorLabel = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.uxFullStoreOrderText = New DevExpress.XtraEditors.TextEdit
        Me.uxFullStoreIdText = New DevExpress.XtraEditors.TextEdit
        Me.uxFullOmOrderText = New DevExpress.XtraEditors.TextEdit
        Me.uxFullOrderGrid = New DevExpress.XtraGrid.GridControl
        Me.uxFullOrderView = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.uxPrintButton = New DevExpress.XtraEditors.SimpleButton
        Me.uxConfirmOrderButton = New DevExpress.XtraEditors.SimpleButton
        Me.uxPrintDespatchButton = New DevExpress.XtraEditors.SimpleButton
        Me.uxDespatchButton = New DevExpress.XtraEditors.SimpleButton
        Me.uxConfirmPickingButton = New DevExpress.XtraEditors.SimpleButton
        Me.uxDeliveredButton = New DevExpress.XtraEditors.SimpleButton
        Me.uxResetButton = New DevExpress.XtraEditors.SimpleButton
        Me.uxRePrintDespatchNote = New DevExpress.XtraEditors.SimpleButton
        CType(Me.uxOpenOrderGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxOpenOrderView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxOrderTabControl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxOrderTabControl.SuspendLayout()
        Me.uxNonZeroStockTab.SuspendLayout()
        CType(Me.uxNonZeroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxNonZeroView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxOpenOrderTab.SuspendLayout()
        CType(Me.uxShowFulfilCheck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxAllOrderTab.SuspendLayout()
        CType(Me.uxEndDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxEndDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxStartDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxStartDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxAllOrderGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxAllOrderView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uxFullOrderTab.SuspendLayout()
        CType(Me.uxFullStoreOrderText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxFullStoreIdText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxFullOmOrderText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxFullOrderGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uxFullOrderView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'uxOpenOrderGrid
        '
        Me.uxOpenOrderGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxOpenOrderGrid.Location = New System.Drawing.Point(3, 55)
        Me.uxOpenOrderGrid.MainView = Me.uxOpenOrderView
        Me.uxOpenOrderGrid.Name = "uxOpenOrderGrid"
        Me.uxOpenOrderGrid.Size = New System.Drawing.Size(912, 394)
        Me.uxOpenOrderGrid.TabIndex = 1
        Me.uxOpenOrderGrid.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uxOpenOrderView})
        '
        'uxOpenOrderView
        '
        Me.uxOpenOrderView.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxOpenOrderView.Appearance.HeaderPanel.Options.UseFont = True
        Me.uxOpenOrderView.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.uxOpenOrderView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uxOpenOrderView.Appearance.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxOpenOrderView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxOpenOrderView.Appearance.Row.Font = New System.Drawing.Font("Tahoma", 7.5!)
        Me.uxOpenOrderView.Appearance.Row.Options.UseFont = True
        Me.uxOpenOrderView.ColumnPanelRowHeight = 35
        Me.uxOpenOrderView.GridControl = Me.uxOpenOrderGrid
        Me.uxOpenOrderView.Name = "uxOpenOrderView"
        Me.uxOpenOrderView.OptionsBehavior.Editable = False
        Me.uxOpenOrderView.OptionsFilter.AllowColumnMRUFilterList = False
        Me.uxOpenOrderView.OptionsFilter.AllowFilterEditor = False
        Me.uxOpenOrderView.OptionsFilter.AllowMRUFilterList = False
        Me.uxOpenOrderView.OptionsPrint.UsePrintStyles = True
        Me.uxOpenOrderView.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.uxOpenOrderView.OptionsSelection.MultiSelect = True
        Me.uxOpenOrderView.OptionsView.ColumnAutoWidth = False
        Me.uxOpenOrderView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.uxOpenOrderView.OptionsView.ShowGroupedColumns = True
        '
        'uxExitButton
        '
        Me.uxExitButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxExitButton.Location = New System.Drawing.Point(855, 492)
        Me.uxExitButton.Name = "uxExitButton"
        Me.uxExitButton.Size = New System.Drawing.Size(75, 39)
        Me.uxExitButton.TabIndex = 10
        Me.uxExitButton.Text = "F12 Exit"
        '
        'uxRetrieveButton
        '
        Me.uxRetrieveButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxRetrieveButton.Appearance.Options.UseTextOptions = True
        Me.uxRetrieveButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxRetrieveButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxRetrieveButton.Location = New System.Drawing.Point(3, 492)
        Me.uxRetrieveButton.Name = "uxRetrieveButton"
        Me.uxRetrieveButton.Size = New System.Drawing.Size(75, 39)
        Me.uxRetrieveButton.TabIndex = 1
        Me.uxRetrieveButton.Text = "F2 Retrieve Orders"
        '
        'uxMaintainButton
        '
        Me.uxMaintainButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxMaintainButton.Appearance.Options.UseTextOptions = True
        Me.uxMaintainButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxMaintainButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxMaintainButton.Location = New System.Drawing.Point(84, 492)
        Me.uxMaintainButton.Name = "uxMaintainButton"
        Me.uxMaintainButton.Size = New System.Drawing.Size(75, 39)
        Me.uxMaintainButton.TabIndex = 2
        Me.uxMaintainButton.Text = "F3 Maintain Order"
        '
        'uxOrderTabControl
        '
        Me.uxOrderTabControl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxOrderTabControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.uxOrderTabControl.Location = New System.Drawing.Point(3, 3)
        Me.uxOrderTabControl.Name = "uxOrderTabControl"
        Me.uxOrderTabControl.SelectedTabPage = Me.uxNonZeroStockTab
        Me.uxOrderTabControl.Size = New System.Drawing.Size(927, 483)
        Me.uxOrderTabControl.TabIndex = 0
        Me.uxOrderTabControl.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.uxOpenOrderTab, Me.uxAllOrderTab, Me.uxFullOrderTab, Me.uxNonZeroStockTab})
        '
        'uxNonZeroStockTab
        '
        Me.uxNonZeroStockTab.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.uxNonZeroStockTab.Controls.Add(Me.uxNonZeroGrid)
        Me.uxNonZeroStockTab.Name = "uxNonZeroStockTab"
        Me.uxNonZeroStockTab.PageVisible = False
        Me.uxNonZeroStockTab.Size = New System.Drawing.Size(918, 452)
        Me.uxNonZeroStockTab.Text = "Non Zero Stock"
        '
        'uxNonZeroGrid
        '
        Me.uxNonZeroGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxNonZeroGrid.Location = New System.Drawing.Point(3, 55)
        Me.uxNonZeroGrid.MainView = Me.uxNonZeroView
        Me.uxNonZeroGrid.Name = "uxNonZeroGrid"
        Me.uxNonZeroGrid.Size = New System.Drawing.Size(912, 394)
        Me.uxNonZeroGrid.TabIndex = 7
        Me.uxNonZeroGrid.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uxNonZeroView})
        '
        'uxNonZeroView
        '
        Me.uxNonZeroView.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxNonZeroView.Appearance.HeaderPanel.Options.UseFont = True
        Me.uxNonZeroView.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.uxNonZeroView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uxNonZeroView.Appearance.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxNonZeroView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxNonZeroView.ColumnPanelRowHeight = 35
        Me.uxNonZeroView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.uxNonZeroView.GridControl = Me.uxNonZeroGrid
        Me.uxNonZeroView.Name = "uxNonZeroView"
        Me.uxNonZeroView.OptionsBehavior.Editable = False
        Me.uxNonZeroView.OptionsDetail.ShowDetailTabs = False
        Me.uxNonZeroView.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.uxNonZeroView.OptionsSelection.MultiSelect = True
        Me.uxNonZeroView.OptionsView.ColumnAutoWidth = False
        Me.uxNonZeroView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        '
        'uxOpenOrderTab
        '
        Me.uxOpenOrderTab.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.uxOpenOrderTab.Controls.Add(Me.uxOMOrderNumber)
        Me.uxOpenOrderTab.Controls.Add(Me.lblOMNumber)
        Me.uxOpenOrderTab.Controls.Add(Me.uxOpenOrderGrid)
        Me.uxOpenOrderTab.Controls.Add(Me.uxShowFulfilCheck)
        Me.uxOpenOrderTab.Name = "uxOpenOrderTab"
        Me.uxOpenOrderTab.Size = New System.Drawing.Size(918, 452)
        Me.uxOpenOrderTab.Text = "Open Orders"
        '
        'uxOMOrderNumber
        '
        Me.uxOMOrderNumber.Location = New System.Drawing.Point(445, 5)
        Me.uxOMOrderNumber.MaxLength = 11
        Me.uxOMOrderNumber.Name = "uxOMOrderNumber"
        Me.uxOMOrderNumber.Size = New System.Drawing.Size(104, 20)
        Me.uxOMOrderNumber.TabIndex = 1
        '
        'lblOMNumber
        '
        Me.lblOMNumber.AutoSize = True
        Me.lblOMNumber.Location = New System.Drawing.Point(369, 5)
        Me.lblOMNumber.Name = "lblOMNumber"
        Me.lblOMNumber.Size = New System.Drawing.Size(70, 13)
        Me.lblOMNumber.TabIndex = 2
        Me.lblOMNumber.Text = "OM Number :"
        '
        'uxShowFulfilCheck
        '
        Me.uxShowFulfilCheck.Location = New System.Drawing.Point(3, 28)
        Me.uxShowFulfilCheck.Name = "uxShowFulfilCheck"
        Me.uxShowFulfilCheck.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uxShowFulfilCheck.Properties.Appearance.Options.UseFont = True
        Me.uxShowFulfilCheck.Properties.Caption = "Only show orders and lines fulfilled by this store"
        Me.uxShowFulfilCheck.Size = New System.Drawing.Size(341, 21)
        Me.uxShowFulfilCheck.TabIndex = 0
        '
        'uxAllOrderTab
        '
        Me.uxAllOrderTab.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.uxAllOrderTab.Controls.Add(Me.LabelControl2)
        Me.uxAllOrderTab.Controls.Add(Me.uxEndDate)
        Me.uxAllOrderTab.Controls.Add(Me.LabelControl1)
        Me.uxAllOrderTab.Controls.Add(Me.uxStartDate)
        Me.uxAllOrderTab.Controls.Add(Me.uxAllOrderGrid)
        Me.uxAllOrderTab.Name = "uxAllOrderTab"
        Me.uxAllOrderTab.Size = New System.Drawing.Size(918, 452)
        Me.uxAllOrderTab.Text = "All Orders"
        '
        'LabelControl2
        '
        Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl2.Location = New System.Drawing.Point(6, 29)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl2.TabIndex = 5
        Me.LabelControl2.Text = "Order End Date"
        '
        'uxEndDate
        '
        Me.uxEndDate.EditValue = Nothing
        Me.uxEndDate.Location = New System.Drawing.Point(102, 29)
        Me.uxEndDate.Name = "uxEndDate"
        Me.uxEndDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.uxEndDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.uxEndDate.Size = New System.Drawing.Size(106, 20)
        Me.uxEndDate.TabIndex = 4
        '
        'LabelControl1
        '
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl1.Location = New System.Drawing.Point(6, 3)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(90, 20)
        Me.LabelControl1.TabIndex = 3
        Me.LabelControl1.Text = "Order Start Date"
        '
        'uxStartDate
        '
        Me.uxStartDate.EditValue = Nothing
        Me.uxStartDate.Location = New System.Drawing.Point(102, 3)
        Me.uxStartDate.Name = "uxStartDate"
        Me.uxStartDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.uxStartDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.uxStartDate.Size = New System.Drawing.Size(106, 20)
        Me.uxStartDate.TabIndex = 2
        '
        'uxAllOrderGrid
        '
        Me.uxAllOrderGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxAllOrderGrid.Location = New System.Drawing.Point(3, 55)
        Me.uxAllOrderGrid.MainView = Me.uxAllOrderView
        Me.uxAllOrderGrid.Name = "uxAllOrderGrid"
        Me.uxAllOrderGrid.Size = New System.Drawing.Size(912, 394)
        Me.uxAllOrderGrid.TabIndex = 1
        Me.uxAllOrderGrid.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uxAllOrderView})
        '
        'uxAllOrderView
        '
        Me.uxAllOrderView.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxAllOrderView.Appearance.HeaderPanel.Options.UseFont = True
        Me.uxAllOrderView.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.uxAllOrderView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uxAllOrderView.Appearance.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxAllOrderView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxAllOrderView.ColumnPanelRowHeight = 35
        Me.uxAllOrderView.GridControl = Me.uxAllOrderGrid
        Me.uxAllOrderView.Name = "uxAllOrderView"
        Me.uxAllOrderView.OptionsBehavior.Editable = False
        Me.uxAllOrderView.OptionsFilter.AllowColumnMRUFilterList = False
        Me.uxAllOrderView.OptionsFilter.AllowFilterEditor = False
        Me.uxAllOrderView.OptionsFilter.AllowMRUFilterList = False
        Me.uxAllOrderView.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.uxAllOrderView.OptionsView.ColumnAutoWidth = False
        Me.uxAllOrderView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        '
        'uxFullOrderTab
        '
        Me.uxFullOrderTab.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.uxFullOrderTab.Controls.Add(Me.uxFullStoreOrderErrorLabel)
        Me.uxFullOrderTab.Controls.Add(Me.uxFullStoreIdErrorLabel)
        Me.uxFullOrderTab.Controls.Add(Me.uxFullOmOrderErrorLabel)
        Me.uxFullOrderTab.Controls.Add(Me.LabelControl5)
        Me.uxFullOrderTab.Controls.Add(Me.LabelControl4)
        Me.uxFullOrderTab.Controls.Add(Me.LabelControl3)
        Me.uxFullOrderTab.Controls.Add(Me.uxFullStoreOrderText)
        Me.uxFullOrderTab.Controls.Add(Me.uxFullStoreIdText)
        Me.uxFullOrderTab.Controls.Add(Me.uxFullOmOrderText)
        Me.uxFullOrderTab.Controls.Add(Me.uxFullOrderGrid)
        Me.uxFullOrderTab.Name = "uxFullOrderTab"
        Me.uxFullOrderTab.Size = New System.Drawing.Size(918, 452)
        Me.uxFullOrderTab.Text = "OM Enquiry"
        '
        'uxFullStoreOrderErrorLabel
        '
        Me.uxFullStoreOrderErrorLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.uxFullStoreOrderErrorLabel.Location = New System.Drawing.Point(262, 49)
        Me.uxFullStoreOrderErrorLabel.Name = "uxFullStoreOrderErrorLabel"
        Me.uxFullStoreOrderErrorLabel.Size = New System.Drawing.Size(316, 20)
        Me.uxFullStoreOrderErrorLabel.TabIndex = 9
        '
        'uxFullStoreIdErrorLabel
        '
        Me.uxFullStoreIdErrorLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.uxFullStoreIdErrorLabel.Location = New System.Drawing.Point(262, 26)
        Me.uxFullStoreIdErrorLabel.Name = "uxFullStoreIdErrorLabel"
        Me.uxFullStoreIdErrorLabel.Size = New System.Drawing.Size(316, 20)
        Me.uxFullStoreIdErrorLabel.TabIndex = 8
        '
        'uxFullOmOrderErrorLabel
        '
        Me.uxFullOmOrderErrorLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.uxFullOmOrderErrorLabel.Location = New System.Drawing.Point(262, 3)
        Me.uxFullOmOrderErrorLabel.Name = "uxFullOmOrderErrorLabel"
        Me.uxFullOmOrderErrorLabel.Size = New System.Drawing.Size(340, 20)
        Me.uxFullOmOrderErrorLabel.TabIndex = 7
        '
        'LabelControl5
        '
        Me.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl5.Location = New System.Drawing.Point(6, 49)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(147, 20)
        Me.LabelControl5.TabIndex = 4
        Me.LabelControl5.Text = "Selling Store Order &Number"
        '
        'LabelControl4
        '
        Me.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl4.Location = New System.Drawing.Point(6, 26)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(147, 20)
        Me.LabelControl4.TabIndex = 2
        Me.LabelControl4.Text = "Selling Store &ID"
        '
        'LabelControl3
        '
        Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl3.Location = New System.Drawing.Point(6, 3)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(147, 20)
        Me.LabelControl3.TabIndex = 0
        Me.LabelControl3.Text = "&OM Order Reference"
        '
        'uxFullStoreOrderText
        '
        Me.uxFullStoreOrderText.EnterMoveNextControl = True
        Me.uxFullStoreOrderText.Location = New System.Drawing.Point(159, 49)
        Me.uxFullStoreOrderText.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.uxFullStoreOrderText.Name = "uxFullStoreOrderText"
        Me.uxFullStoreOrderText.Properties.Mask.EditMask = "\d{0,6}"
        Me.uxFullStoreOrderText.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uxFullStoreOrderText.Properties.Mask.ShowPlaceHolders = False
        Me.uxFullStoreOrderText.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.uxFullStoreOrderText.Size = New System.Drawing.Size(97, 20)
        Me.uxFullStoreOrderText.TabIndex = 5
        '
        'uxFullStoreIdText
        '
        Me.uxFullStoreIdText.EnterMoveNextControl = True
        Me.uxFullStoreIdText.Location = New System.Drawing.Point(159, 26)
        Me.uxFullStoreIdText.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.uxFullStoreIdText.Name = "uxFullStoreIdText"
        Me.uxFullStoreIdText.Properties.Mask.EditMask = "\d{0,4}"
        Me.uxFullStoreIdText.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uxFullStoreIdText.Properties.Mask.ShowPlaceHolders = False
        Me.uxFullStoreIdText.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.uxFullStoreIdText.Size = New System.Drawing.Size(97, 20)
        Me.uxFullStoreIdText.TabIndex = 3
        '
        'uxFullOmOrderText
        '
        Me.uxFullOmOrderText.EnterMoveNextControl = True
        Me.uxFullOmOrderText.Location = New System.Drawing.Point(159, 3)
        Me.uxFullOmOrderText.Name = "uxFullOmOrderText"
        Me.uxFullOmOrderText.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.uxFullOmOrderText.Properties.Appearance.Options.UseBackColor = True
        Me.uxFullOmOrderText.Properties.Mask.EditMask = "\d{0,9}"
        Me.uxFullOmOrderText.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uxFullOmOrderText.Properties.Mask.SaveLiteral = False
        Me.uxFullOmOrderText.Properties.Mask.ShowPlaceHolders = False
        Me.uxFullOmOrderText.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.uxFullOmOrderText.Size = New System.Drawing.Size(97, 20)
        Me.uxFullOmOrderText.TabIndex = 1
        '
        'uxFullOrderGrid
        '
        Me.uxFullOrderGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxFullOrderGrid.Location = New System.Drawing.Point(3, 75)
        Me.uxFullOrderGrid.MainView = Me.uxFullOrderView
        Me.uxFullOrderGrid.Name = "uxFullOrderGrid"
        Me.uxFullOrderGrid.Size = New System.Drawing.Size(912, 374)
        Me.uxFullOrderGrid.TabIndex = 6
        Me.uxFullOrderGrid.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uxFullOrderView})
        '
        'uxFullOrderView
        '
        Me.uxFullOrderView.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.uxFullOrderView.Appearance.HeaderPanel.Options.UseFont = True
        Me.uxFullOrderView.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.uxFullOrderView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uxFullOrderView.Appearance.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxFullOrderView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxFullOrderView.ColumnPanelRowHeight = 35
        Me.uxFullOrderView.GridControl = Me.uxFullOrderGrid
        Me.uxFullOrderView.Name = "uxFullOrderView"
        Me.uxFullOrderView.OptionsBehavior.Editable = False
        Me.uxFullOrderView.OptionsFilter.AllowColumnMRUFilterList = False
        Me.uxFullOrderView.OptionsFilter.AllowFilterEditor = False
        Me.uxFullOrderView.OptionsFilter.AllowMRUFilterList = False
        Me.uxFullOrderView.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.uxFullOrderView.OptionsView.ColumnAutoWidth = False
        Me.uxFullOrderView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        '
        'uxPrintButton
        '
        Me.uxPrintButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxPrintButton.Location = New System.Drawing.Point(693, 492)
        Me.uxPrintButton.Name = "uxPrintButton"
        Me.uxPrintButton.Size = New System.Drawing.Size(75, 39)
        Me.uxPrintButton.TabIndex = 8
        Me.uxPrintButton.Text = "F9 Print"
        '
        'uxConfirmOrderButton
        '
        Me.uxConfirmOrderButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxConfirmOrderButton.Appearance.Options.UseTextOptions = True
        Me.uxConfirmOrderButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxConfirmOrderButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxConfirmOrderButton.Enabled = False
        Me.uxConfirmOrderButton.Location = New System.Drawing.Point(165, 492)
        Me.uxConfirmOrderButton.Name = "uxConfirmOrderButton"
        Me.uxConfirmOrderButton.Size = New System.Drawing.Size(97, 39)
        Me.uxConfirmOrderButton.TabIndex = 3
        Me.uxConfirmOrderButton.Text = "F4 Confirm New Fulfilment"
        Me.uxConfirmOrderButton.Visible = False
        '
        'uxPrintDespatchButton
        '
        Me.uxPrintDespatchButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxPrintDespatchButton.Appearance.Options.UseTextOptions = True
        Me.uxPrintDespatchButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxPrintDespatchButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxPrintDespatchButton.Location = New System.Drawing.Point(165, 492)
        Me.uxPrintDespatchButton.Name = "uxPrintDespatchButton"
        Me.uxPrintDespatchButton.Size = New System.Drawing.Size(97, 39)
        Me.uxPrintDespatchButton.TabIndex = 4
        Me.uxPrintDespatchButton.Text = "F5 Print Pick Despatch Note"
        '
        'uxDespatchButton
        '
        Me.uxDespatchButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxDespatchButton.Appearance.Options.UseTextOptions = True
        Me.uxDespatchButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxDespatchButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxDespatchButton.Location = New System.Drawing.Point(349, 492)
        Me.uxDespatchButton.Name = "uxDespatchButton"
        Me.uxDespatchButton.Size = New System.Drawing.Size(75, 39)
        Me.uxDespatchButton.TabIndex = 5
        Me.uxDespatchButton.Text = "F7 Despatch Order"
        '
        'uxConfirmPickingButton
        '
        Me.uxConfirmPickingButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxConfirmPickingButton.Appearance.Options.UseTextOptions = True
        Me.uxConfirmPickingButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxConfirmPickingButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxConfirmPickingButton.Location = New System.Drawing.Point(268, 492)
        Me.uxConfirmPickingButton.Name = "uxConfirmPickingButton"
        Me.uxConfirmPickingButton.Size = New System.Drawing.Size(75, 39)
        Me.uxConfirmPickingButton.TabIndex = 6
        Me.uxConfirmPickingButton.Text = "F6 Confirm Picking"
        '
        'uxDeliveredButton
        '
        Me.uxDeliveredButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxDeliveredButton.Appearance.Options.UseTextOptions = True
        Me.uxDeliveredButton.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxDeliveredButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxDeliveredButton.Location = New System.Drawing.Point(430, 492)
        Me.uxDeliveredButton.Name = "uxDeliveredButton"
        Me.uxDeliveredButton.Size = New System.Drawing.Size(75, 39)
        Me.uxDeliveredButton.TabIndex = 7
        Me.uxDeliveredButton.Text = "F8 Confirm or Fail Delivery"
        '
        'uxResetButton
        '
        Me.uxResetButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uxResetButton.Location = New System.Drawing.Point(774, 492)
        Me.uxResetButton.Name = "uxResetButton"
        Me.uxResetButton.Size = New System.Drawing.Size(75, 39)
        Me.uxResetButton.TabIndex = 9
        Me.uxResetButton.Text = "F11 Reset"
        '
        'uxRePrintDespatchNote
        '
        Me.uxRePrintDespatchNote.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.uxRePrintDespatchNote.Appearance.Options.UseTextOptions = True
        Me.uxRePrintDespatchNote.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.uxRePrintDespatchNote.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.uxRePrintDespatchNote.Location = New System.Drawing.Point(349, 492)
        Me.uxRePrintDespatchNote.Name = "uxRePrintDespatchNote"
        Me.uxRePrintDespatchNote.Size = New System.Drawing.Size(75, 39)
        Me.uxRePrintDespatchNote.TabIndex = 11
        Me.uxRePrintDespatchNote.Text = "F7 Re-Print Despatch Note"
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Controls.Add(Me.uxRePrintDespatchNote)
        Me.Controls.Add(Me.uxResetButton)
        Me.Controls.Add(Me.uxDeliveredButton)
        Me.Controls.Add(Me.uxConfirmPickingButton)
        Me.Controls.Add(Me.uxDespatchButton)
        Me.Controls.Add(Me.uxPrintDespatchButton)
        Me.Controls.Add(Me.uxConfirmOrderButton)
        Me.Controls.Add(Me.uxPrintButton)
        Me.Controls.Add(Me.uxOrderTabControl)
        Me.Controls.Add(Me.uxMaintainButton)
        Me.Controls.Add(Me.uxRetrieveButton)
        Me.Controls.Add(Me.uxExitButton)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "MainForm"
        Me.Size = New System.Drawing.Size(933, 534)
        CType(Me.uxOpenOrderGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxOpenOrderView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxOrderTabControl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxOrderTabControl.ResumeLayout(False)
        Me.uxNonZeroStockTab.ResumeLayout(False)
        CType(Me.uxNonZeroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxNonZeroView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxOpenOrderTab.ResumeLayout(False)
        Me.uxOpenOrderTab.PerformLayout()
        CType(Me.uxShowFulfilCheck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxAllOrderTab.ResumeLayout(False)
        CType(Me.uxEndDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxEndDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxStartDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxStartDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxAllOrderGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxAllOrderView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uxFullOrderTab.ResumeLayout(False)
        CType(Me.uxFullStoreOrderText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxFullStoreIdText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxFullOmOrderText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxFullOrderGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uxFullOrderView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents uxOpenOrderGrid As DevExpress.XtraGrid.GridControl
    Friend WithEvents uxOpenOrderView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents uxExitButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxRetrieveButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxMaintainButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxOrderTabControl As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents uxOpenOrderTab As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uxAllOrderTab As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uxAllOrderGrid As DevExpress.XtraGrid.GridControl
    Friend WithEvents uxAllOrderView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxEndDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxStartDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents uxPrintButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxConfirmOrderButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxPrintDespatchButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxDespatchButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxConfirmPickingButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxDeliveredButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxShowFulfilCheck As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents uxFullOrderTab As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uxFullOrderGrid As DevExpress.XtraGrid.GridControl
    Friend WithEvents uxFullOrderView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxFullStoreOrderText As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uxFullStoreIdText As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uxFullOmOrderText As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxResetButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxFullStoreOrderErrorLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxFullStoreIdErrorLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxFullOmOrderErrorLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents uxNonZeroStockTab As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uxNonZeroGrid As DevExpress.XtraGrid.GridControl
    Friend WithEvents uxNonZeroView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents uxRePrintDespatchNote As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uxOMOrderNumber As System.Windows.Forms.TextBox
    Friend WithEvents lblOMNumber As System.Windows.Forms.Label

End Class
