Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.Order
Imports Cts.Oasys.Core.Order.Qod
Imports Cts.Oasys.Core.System
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraEditors
Imports System
Imports System.Diagnostics
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports System.ComponentModel


Public Class ActionConfirmationForm

    Private _ActionFilter As QodHeaderActionFilter
    Private _FormBehaviour As Integer

    ''' <summary>
    ''' QodHeaderActionFilter used to drive the contents of the form grids
    ''' </summary>
    ''' <value>QodHeaderActionFilter</value>
    ''' <history>
    ''' <created author="Charles McMahon" date="08/09/2010"></created>
    ''' </history>
    ''' <remarks>This is passed to the form on load and controls what Qods appear in each of the grids</remarks>
    Private Property ActionFilter() As QodHeaderActionFilter
        Get
            Return _ActionFilter
        End Get
        Set(ByVal value As QodHeaderActionFilter)
            _ActionFilter = value
        End Set
    End Property

    ''' <summary>
    ''' Integer defining how the form is presented and behaves
    ''' </summary>
    ''' <value>Bitwise built Integer</value>
    ''' <history>
    ''' <created author="Charles McMahon" date="08/09/2010"></created>
    ''' </history>
    ''' <remarks>Passed as a bitwise integer to allow more behaviours to be added without changing the class interface</remarks>
    Public Property FormBehaviour() As Integer
        Get
            Return _FormBehaviour
        End Get
        Set(ByVal value As Integer)
            _FormBehaviour = value
        End Set
    End Property

    ''' <summary>
    ''' Constructor for the form.
    ''' </summary>
    ''' <param name="newActionFilter">Value for ActionFilter property</param>
    ''' <param name="newFormBehaviour">Value for FormBehaviour property</param>
    ''' <history>
    ''' <created author="Charles McMahon" date="08/09/2010"></created>
    ''' </history>
    ''' <remarks>Initiates the property values and calls code to set the form up for use</remarks>
    Public Sub New(ByRef newActionFilter As QodHeaderActionFilter, ByVal newFormBehaviour As Integer)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        ActionFilter = newActionFilter
        FormBehaviour = newFormBehaviour
        SetUpForm()
        SetButtonStatus()

    End Sub

    ''' <summary>
    ''' Handles the key down events to capture the hot keys being pressed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <history>
    ''' <created author="Charles McMahon" date="08/09/2010"></created>
    ''' </history>
    ''' <remarks></remarks>
    Private Sub OrderForm_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        e.Handled = True
        Select Case e.KeyData
            Case Keys.F8 : uxAcceptButton.PerformClick()
            Case Keys.F10 : uxCancelButton.PerformClick()
            Case Else : e.Handled = False
        End Select
    End Sub

    ''' <summary>
    ''' Changes check box values when binary check boxes are in use
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <history>
    ''' <created author="Charles McMahon" date="08/09/2010"></created>
    ''' </history>
    ''' <remarks>When the Positive Action Grid has two editable check boxes, maximum of one can be selected, so when the user selects one,
    ''' if the other one for that row is already selected then it must be de-selected.  Currently this is only for Delivery Dialog,
    ''' but more options may be added in the future.  This event fires when the check box is clicked, but the data source does not yet
    ''' reflect the changes, so this must be taken into account.</remarks>
    Private Sub ChangeCheckBoxes(ByVal sender As Object, ByVal e As CellValueChangedEventArgs) Handles uxPositiveActionView.CellValueChanging
        'Check the FormBehaviour to see if this is a Delivery Dialog and swap the check box states if any are already selected for that row
        If (FormBehaviour And ActionFormBehaviour.DeliveryDialog) = ActionFormBehaviour.DeliveryDialog Then
            If e.Column.Name = "colIncludeInPositiveAction" Then
                If CBool(e.Value) = True Then
                    If CBool(uxPositiveActionView.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As QodHeader) f.IncludeInNegativeAction))) = True Then
                        uxPositiveActionView.SetRowCellValue(e.RowHandle, GetPropertyName(Function(f As QodHeader) f.IncludeInNegativeAction), False)
                    End If
                End If
            ElseIf e.Column.Name = "colIncludeInNegativeAction" Then
                If CBool(e.Value) = True Then
                    If CBool(uxPositiveActionView.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As QodHeader) f.IncludeInPositiveAction))) = True Then
                        uxPositiveActionView.SetRowCellValue(e.RowHandle, GetPropertyName(Function(f As QodHeader) f.IncludeInPositiveAction), False)
                    End If
                End If
            End If

        End If
        'Now update the button states and pass in the new values, as the data will not yet reflect the grid
        SetButtonStatus(uxPositiveActionView.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As QodHeader) f.Number)).ToString, CBool(e.Value))

    End Sub

    ''' <summary>
    ''' Sets the status of the buttons on the form
    ''' </summary>
    ''' <param name="overRideQodNo">If needed, we have the QodNo that a cell has just changed on</param>
    ''' <param name="overRideValue">If needed, we have the value that a cell has just changed to</param>
    ''' <history>
    ''' <created author="Charles McMahon" date="08/09/2010"></created>
    ''' </history>
    ''' <remarks></remarks>
    Private Sub SetButtonStatus(Optional ByVal overRideQodNo As String = "", Optional ByVal overRideValue As Boolean = False)

        'If there are no selected actions then hide the Accept button
        Dim ActionSelected As Boolean = False
        'Check that there are Positive values that could be checked
        If ActionFilter.PositiveActionList.Count > 0 Then
            For Each QodHeader As QodHeader In ActionFilter.PositiveActionList
                'If we have passed override values in then check them for the appropriate row
                If overRideQodNo <> String.Empty AndAlso overRideQodNo = QodHeader.Number Then
                    If overRideValue = True Then
                        ActionSelected = True
                    End If
                Else
                    If QodHeader.IncludeInPositiveAction OrElse QodHeader.IncludeInNegativeAction Then
                        ActionSelected = True
                    End If
                End If
            Next

        End If
        'Set the button enabled depending on the outcome of the data check
        uxAcceptButton.Enabled = ActionSelected

    End Sub

    ''' <summary>
    ''' Sets the form up to be used depending on the FormBehaviour property
    ''' </summary>
    ''' <history>
    ''' <created author="Charles McMahon" date="08/09/2010"></created>
    ''' </history>
    ''' <remarks></remarks>
    Private Sub SetUpForm()

        'Set the prompts to the appropriate values depending on the function of the action form
        Select Case True
            Case (FormBehaviour And ActionFormBehaviour.PrintDialog) = ActionFormBehaviour.PrintDialog
                Me.Text = My.Resources.Strings.PrintDialogCaption
                uxPositiveActionGroup.Text = My.Resources.Strings.PrintDialogPositiveGridCaption
                uxNegativeActionGroup.Text = My.Resources.Strings.PrintDialogNegativeGridCaption
                uxTextLabel.Text = My.Resources.Strings.PrintDialogText
            Case (FormBehaviour And ActionFormBehaviour.DespatchDialog) = ActionFormBehaviour.DespatchDialog
                Me.Text = My.Resources.Strings.DespatchDialogCaption
                uxPositiveActionGroup.Text = My.Resources.Strings.DespatchDialogPositiveGridCaption
                uxNegativeActionGroup.Text = My.Resources.Strings.DespatchDialogNegativeGridCaption
                uxTextLabel.Text = My.Resources.Strings.DespatchDialogText
            Case (FormBehaviour And ActionFormBehaviour.DeliveryDialog) = ActionFormBehaviour.DeliveryDialog
                Me.Text = My.Resources.Strings.DeliveryDialogCaption
                uxPositiveActionGroup.Text = My.Resources.Strings.DeliveryDialogPositiveGridCaption
                uxNegativeActionGroup.Text = My.Resources.Strings.DeliveryDialogNegativeGridCaption
                uxTextLabel.Text = My.Resources.Strings.DeliveryDialogText
        End Select

        'Now load the grids with data
        GridLoadQods(uxPositiveActionGrid, ActionFilter.PositiveActionList)
        GridLoadQods(uxNegativeActionGrid, ActionFilter.NegativeActionList)

        'Set the visibility of the panels
        If ActionFilter.PositiveActionList.Count = 0 Then
            uxSplitContainer.PanelVisibility = SplitPanelVisibility.Panel2
        ElseIf ActionFilter.NegativeActionList.Count = 0 Then
            uxSplitContainer.PanelVisibility = SplitPanelVisibility.Panel1
        Else
            uxSplitContainer.PanelVisibility = SplitPanelVisibility.Both
            'Now set the distance of the spliter, based on the number of rows in the positive grid
            uxSplitContainer.SplitterPosition = (ActionFilter.PositiveActionList.Count * 20) + 61
        End If

    End Sub

    ''' <summary>
    ''' Loads a grid with data
    ''' </summary>
    ''' <param name="grid">The grid that is to be populated with data</param>
    ''' <param name="qodHeaderList">The data source for the grid</param>
    ''' <history>
    ''' <created author="Charles McMahon" date="08/09/2010"></created>
    ''' </history>
    ''' <remarks>Loads certain columns depending on the form behaviour and the grid function</remarks>
    Private Sub GridLoadQods(ByVal grid As GridControl, ByVal qodHeaderList As QodHeaderList)

        'If the is the positive action print grid, then set the positive action check box on if the Qod has not already been printed
        If grid Is uxPositiveActionGrid AndAlso (FormBehaviour And ActionFormBehaviour.PrintDialog) = ActionFormBehaviour.PrintDialog Then
            For Each QodHeader As QodHeader In qodHeaderList
                If Not QodHeader.IsPrinted Then QodHeader.IncludeInPositiveAction = True
            Next
        End If

        'If this is the positive action delivery grid, then set the positive or positive-negate action check boxes on, depending on the button
        'that was pressed to get here - Corfirm Delivery or Fail Delivery
        If grid Is uxPositiveActionGrid AndAlso (FormBehaviour And ActionFormBehaviour.DeliveryDialog) = ActionFormBehaviour.DeliveryDialog Then
            If (FormBehaviour And ActionFormBehaviour.DeliveryConfirm) = ActionFormBehaviour.DeliveryConfirm Then
                For Each QodHeader As QodHeader In qodHeaderList
                    QodHeader.IncludeInPositiveAction = True
                Next
            Else
                For Each QodHeader As QodHeader In qodHeaderList
                    QodHeader.IncludeInNegativeAction = True
                Next
            End If
        End If

        'Load the grid with the data
        grid.DataSource = qodHeaderList

        'Hide ALL default columns (there will be one fo each property in the QodHeader)
        Dim view As GridView = CType(grid.MainView, GridView)
        For Each col As GridColumn In view.Columns
            col.VisibleIndex = -1
        Next

        'New select which columns to show (some depending on FormBehaviour)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.OmOrderNumber), My.Resources.Columns.OMOrderNumber, False, 40, 80)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.Number), My.Resources.Columns.DeliveryNumber, False, 55, 70)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.LocalStorePhase), My.Resources.Columns.LocalStorePhase, False, 300, 300)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.CustomerName), My.Resources.Columns.CustomerName)
        If grid Is uxNegativeActionGrid Then
            GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.IsSuspended), My.Resources.Columns.IsSuspended, False, 80)
        End If
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.IsForDelivery), My.Resources.Columns.DeliveryOrder)
        If (FormBehaviour And ActionFormBehaviour.PrintDialog) = ActionFormBehaviour.PrintDialog Then
            GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.IsPrinted), My.Resources.Columns.IsPrinted, False, 80)
            If grid Is uxPositiveActionGrid Then
                GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.IncludeInPositiveAction), My.Resources.Columns.Print, True, 80)
            End If
        End If
        If (FormBehaviour And ActionFormBehaviour.DespatchDialog) = ActionFormBehaviour.DespatchDialog Then
            GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.IsPrinted), My.Resources.Columns.IsPrinted, False, 80)
            If grid Is uxPositiveActionGrid Then
                GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.IncludeInPositiveAction), My.Resources.Columns.Despatch, True, 80)
            End If
        End If
        If (FormBehaviour And ActionFormBehaviour.DeliveryDialog) = ActionFormBehaviour.DeliveryDialog Then
            If grid Is uxPositiveActionGrid Then
                GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.IncludeInPositiveAction), My.Resources.Columns.ConfirmDelivery, True, 80)
                GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.IncludeInNegativeAction), My.Resources.Columns.FailDelivery, True, 80)
            End If
        End If
        'Adjust the grid to fit the columns
        view.BestFitColumns()

    End Sub

    ''' <summary>
    ''' Sets the visible columns apperance
    ''' </summary>
    ''' <param name="view">The grid view to set the column active on</param>
    ''' <param name="columnName">The column name that is to be set to visible (derived from the property name of the data source object)</param>
    ''' <param name="caption">The caption of the column</param>
    ''' <param name="editable">True if the column can be edited</param>
    ''' <param name="minWidth">The minimum width of the column</param>
    ''' <param name="maxWidth">The maximum width of the column</param>
    ''' <history>
    ''' <created author="Charles McMahon" date="08/09/2010"></created>
    ''' </history>
    ''' <remarks></remarks>
    Private Sub GridViewSetColumn(ByRef view As GridView, ByVal columnName As String, Optional ByVal caption As String = "", Optional ByVal editable As Boolean = False, Optional ByVal minWidth As Integer = 0, Optional ByVal maxWidth As Integer = 0)
        view.Columns(columnName).VisibleIndex = view.VisibleColumns.Count
        view.Columns(columnName).OptionsColumn.AllowEdit = editable
        If caption.Length > 0 Then view.Columns(columnName).Caption = caption
        If minWidth > 0 Then view.Columns(columnName).MinWidth = minWidth
        If maxWidth > 0 Then view.Columns(columnName).MaxWidth = maxWidth
    End Sub

    ''' <summary>
    ''' Handles the cancel button being clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <history>
    ''' <created author="Charles McMahon" date="08/09/2010"></created>
    ''' </history>
    ''' <remarks>References to the check boxes must be removed from the QodHeaders, so that they do not appear checked incorrectly if they
    ''' appear in another Action Form afterwards</remarks>
    Private Sub uxCancelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxCancelButton.Click
        'Remove all references to the action lists that the Qods are in
        For Each QodHeader As QodHeader In ActionFilter.PositiveActionList
            QodHeader.IncludeInPositiveAction = False
            QodHeader.IncludeInNegativeAction = False
        Next
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    ''' <summary>
    ''' Handles the accept button being clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <history>
    ''' <created author="Charles McMahon" date="08/09/2010"></created>
    ''' </history>
    ''' <remarks>References to the check boxes must be removed from the QodHeaders, so that they do not appear checked incorrectly if they
    ''' appear in another Action Form afterwards</remarks>
    Private Sub uxAcceptButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxAcceptButton.Click

        'Add all Qods checked with a negative action to the PositiveActionNegateList from the PostitiveActionList
        For Each QodHeader As QodHeader In ActionFilter.PositiveActionList
            If QodHeader.IncludeInNegativeAction Then
                ActionFilter.PositiveActionNegateList.Add(QodHeader)
                'Remove the reference to the list after adding to the list
                QodHeader.IncludeInNegativeAction = False
            End If
        Next
        'Remove all Qods NOT checked with a positive action from the PositiveActionList
        ActionFilter.PositiveActionList.RemoveAll(AddressOf NotInPositiveList)

        'Remove the references to the list after all other Qods have been removed from the list
        For Each QodHeader As QodHeader In ActionFilter.PositiveActionList
            QodHeader.IncludeInPositiveAction = False
        Next

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()

    End Sub

    ''' <summary>
    ''' Predicate used to for List.RemoveAll method
    ''' </summary>
    ''' <param name="qodHeader"></param>
    ''' <returns></returns>
    ''' <history>
    ''' <created author="Charles McMahon" date="08/09/2010"></created>
    ''' </history>
    ''' <remarks></remarks>
    Private Shared Function NotInPositiveList(ByVal qodHeader As QodHeader) As Boolean
        Return Not qodHeader.IncludeInPositiveAction
    End Function

End Class