﻿Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.Order
Imports Cts.Oasys.Core.Order.Qod
Imports Cts.Oasys.Core.System
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraEditors
Imports System
Imports System.Diagnostics

Public Class OrderForm
    Private _qod As QodHeader
    Private blnFormLoading As Boolean
    Private blnNewInstruction As Boolean = False

    Public Sub New(ByRef qod As QodHeader)
        Trace.WriteLine("OrderForm_New (START)")
        blnFormLoading = True
        InitializeComponent()
        _qod = qod
        InitialiseMasks()
        InitialiseDeliveryInstructions()
        AddControlEditValueChangedHandlers()
        AddControlLeaveHandlers()
        SetControlsToQodValues()
        blnFormLoading = False
        uxAcceptButton.Enabled = ValidateInputs()
        Trace.WriteLine("OrderForm_New (FINISH)")
    End Sub

    Private Sub InitialiseMasks()
        Trace.WriteLine("InitialiseMasks (START)")
        If Store.IsUKStore Then
            uxPhoneHomeText.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            uxPhoneHomeText.Properties.Mask.EditMask = My.Resources.Strings.RegexUkPhone
            uxPhoneHomeText.Properties.Mask.UseMaskAsDisplayFormat = True
            uxPhoneWorkText.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            uxPhoneWorkText.Properties.Mask.EditMask = My.Resources.Strings.RegexUkPhone
            uxPhoneWorkText.Properties.Mask.UseMaskAsDisplayFormat = True
            uxPhoneMobileText.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            uxPhoneMobileText.Properties.Mask.EditMask = My.Resources.Strings.RegexUkMobile
            uxPhoneMobileText.Properties.Mask.UseMaskAsDisplayFormat = True
            Trace.WriteLine("InitialiseMasks (FINISH)")
        End If
    End Sub

    Private Sub InitialiseDeliveryInstructions()
        Trace.WriteLine("InitialiseDeliveryInstructions (START)")
        uxInstructionsList.Properties.ValueMember = GetPropertyName(Function(f As QodText) f.Number)
        uxInstructionsList.Properties.DisplayMember = GetPropertyName(Function(f As QodText) f.Number)
        uxInstructionsList.Properties.Columns.Add(New LookUpColumnInfo(GetPropertyName(Function(f As QodText) f.Number)))
        uxInstructionsList.Properties.Columns.Add(New LookUpColumnInfo(GetPropertyName(Function(f As QodText) f.Text)))
        uxInstructionsList.Properties.PopupWidth = uxInstructionsList.Width
        Trace.WriteLine("InitialiseDeliveryInstructions (FINISH)")
    End Sub

    Private Sub AddControlEditValueChangedHandlers()
        Trace.WriteLine("AddControlEditValueChangedHandlers (START)")
        AddHandler uxDelivery1Text.EditValueChanged, AddressOf ControlEditValueChanged
        AddHandler uxDelivery2Text.EditValueChanged, AddressOf ControlEditValueChanged
        AddHandler uxDelivery3Text.EditValueChanged, AddressOf ControlEditValueChanged
        AddHandler uxDelivery4Text.EditValueChanged, AddressOf ControlEditValueChanged
        AddHandler uxDeliveryPostText.EditValueChanged, AddressOf ControlEditValueChanged
        AddHandler uxDeliveryDate.EditValueChanged, AddressOf ControlEditValueChanged
        AddHandler uxPhoneHomeText.EditValueChanged, AddressOf ControlEditValueChanged
        AddHandler uxPhoneMobileText.EditValueChanged, AddressOf ControlEditValueChanged
        AddHandler uxPhoneWorkText.EditValueChanged, AddressOf ControlEditValueChanged
        AddHandler uxEmailText.EditValueChanged, AddressOf ControlEditValueChanged
        AddHandler uxDeliveryStatusCheck.EditValueChanged, AddressOf ControlEditValueChanged
        AddHandler uxDelivery1Text.EditValueChanged, AddressOf ControlEditValueChanged
        AddHandler uxInstructionsText.EditValueChanged, AddressOf ControlEditValueChanged
        Trace.WriteLine("AddControlEditValueChangedHandlers (FINISH)")
    End Sub

    Private Sub AddControlLeaveHandlers()
        Trace.WriteLine("AddControlLeaveHandlers (START)")
        AddHandler uxDeliveryPostText.Leave, AddressOf ControlLeave
        Trace.WriteLine("AddControlLeaveHandlers (FINISH)")
    End Sub

    Private Sub SetControlsToQodValues()

        Trace.WriteLine("SetControlsToQodValues (START)")
        Trace.WriteLine("SetControlsToQodValues (0)")
        uxDelivery1Text.EditValue = _qod.DeliveryAddress1
        Trace.WriteLine("SetControlsToQodValues (1)")
        uxDelivery2Text.EditValue = _qod.DeliveryAddress2
        Trace.WriteLine("SetControlsToQodValues (2)")
        uxDelivery3Text.EditValue = _qod.DeliveryAddress3
        Trace.WriteLine("SetControlsToQodValues (3)")
        uxDelivery4Text.EditValue = _qod.DeliveryAddress4
        Trace.WriteLine("SetControlsToQodValues (4)")
        uxDeliveryPostText.EditValue = FormatPostcode(_qod.DeliveryPostCode)
        Trace.WriteLine("SetControlsToQodValues (5)")
        uxDeliveryDate.EditValue = _qod.DateDelivery
        Trace.WriteLine("SetControlsToQodValues (6)")
        uxDeliveryDate.Properties.MinValue = Now.Date
        Trace.WriteLine("SetControlsToQodValues (7)")

        uxPhoneHomeText.EditValue = _qod.PhoneNumber
        Trace.WriteLine("SetControlsToQodValues (8)")
        uxPhoneMobileText.EditValue = _qod.PhoneNumberMobile
        Trace.WriteLine("SetControlsToQodValues (9)")
        uxPhoneWorkText.EditValue = _qod.PhoneNumberWork
        Trace.WriteLine("SetControlsToQodValues (10)")
        uxEmailText.EditValue = _qod.CustomerEmail
        Trace.WriteLine("SetControlsToQodValues (11)")

        uxDeliveryContactName.EditValue = _qod.CustomerName
        Trace.WriteLine("SetControlsToQodValues (12)")
        uxDeliveryContactNumber.EditValue = _qod.PhoneNumber
        Trace.WriteLine("SetControlsToQodValues (13)")

        uxDeliveryStatusLabel.Text = _qod.DeliveryStateDescription
        Trace.WriteLine("SetControlsToQodValues (14)")
        uxDeliveryStatusCheck.Checked = _qod.IsSuspended
        Trace.WriteLine("SetControlsToQodValues (15)")
        'uxDeliveryStatusCheck.Enabled = _qod.IsSuspendable
        SetDeliveryInstructions()
        Trace.WriteLine("SetControlsToQodValues (16)")
        Trace.WriteLine("SetControlsToQodValues (FINISH)")

    End Sub

    Private Function FormatPostcode(ByVal postCode As String) As String
        If Not postCode.Contains(" ") Then
            postCode = postCode.Substring(0, postCode.Length - 3) & " " & postCode.Substring(postCode.Length - 3)
        End If
        Return postCode
    End Function

    Private Sub SetDeliveryInstructions()
        Trace.WriteLine("SetDeliveryInstructions (START)")
        Dim txts As QodTextCollection = _qod.Texts.GetTexts(QodTextType.Type.DeliveryInstruction)
        uxInstructionsList.Properties.DataSource = Nothing
        uxInstructionsList.Properties.DataSource = txts
        If txts.Count > 0 Then uxInstructionsList.EditValue = txts(0).Number
        Trace.WriteLine("SetDeliveryInstructions (FINISH)")
    End Sub

    ''' <summary>
    ''' Captures key presses as hot keys for the forms buttons
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <history>
    ''' <updated author="Charles McMahon" date="24/08/2010">Added support for the F5 key</updated>
    ''' </history>
    ''' <remarks></remarks>
    Private Sub OrderForm_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        e.Handled = True
        Select Case e.KeyData
            Case Keys.F3 : uxResetButton.PerformClick()
            Case Keys.F4 : uxAddNewButton.PerformClick()
            Case Keys.F5 : uxRemoveButton.PerformClick()
            Case Keys.F8 : uxAcceptButton.PerformClick()
            Case Keys.F10 : uxCancelButton.PerformClick()
            Case Else : e.Handled = False
        End Select
    End Sub

    Private Sub uxInstructionsList_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxInstructionsList.EditValueChanged
        'get selected text from collection and display in text box
        Dim txts As QodTextCollection = CType(uxInstructionsList.Properties.DataSource, QodTextCollection)
        Dim txt As QodText = txts.Find(uxInstructionsList.EditValue.ToString, QodTextType.Type.DeliveryInstruction)
        If txt IsNot Nothing Then
            uxInstructionsText.EditValue = txt.Text
        End If
        uxAcceptButton.Enabled = ValidateInputs()
    End Sub

    Private Sub uxInstructionsText_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxInstructionsText.Validated
        'update selected instruction with textbox value
        Dim txts As QodTextCollection = CType(uxInstructionsList.Properties.DataSource, QodTextCollection)
        If txts IsNot Nothing AndAlso txts.Count > 0 Then
            Dim txt As QodText = txts.Find(uxInstructionsList.EditValue.ToString, QodTextType.Type.DeliveryInstruction)
            If txt IsNot Nothing Then
                If uxInstructionsText.EditValue IsNot Nothing Then txt.Text = uxInstructionsText.EditValue.ToString
            End If
        End If
        uxAcceptButton.Enabled = ValidateInputs()
    End Sub

    Private Sub ControlLeave(ByVal sender As Object, ByVal e As EventArgs)

        If sender Is uxDeliveryPostText Then
            uxAcceptButton.Enabled = ValidateInputs()
        End If

    End Sub

    ''' <summary>
    ''' Handles all edit value changed events for controls on the form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <history>
    ''' <updated author="Charles McMahon" date="24/08/2010">Changed to fix all of the input validation</updated>
    ''' </history>
    ''' <remarks></remarks>
    Private Sub ControlEditValueChanged(ByVal sender As Object, ByVal e As EventArgs)

        Trace.WriteLine("ControlEditValueChanged (START)")
        'If the control being edited was one of our key fields for validating then validate the inputs
        If (sender Is uxDelivery1Text) Or (sender Is uxDelivery3Text) Or (sender Is uxPhoneHomeText) Or _
        (sender Is uxPhoneMobileText) Or (sender Is uxPhoneWorkText) Or (sender Is uxDeliveryPostText) Or _
        (sender Is uxInstructionsText) Then
            uxAcceptButton.Enabled = ValidateInputs()
        End If

        'If we are updating the mobile number then turn off auto complete if the number of characters already entered is less than 2
        'and turn auto complete on if the number is greater than 2.  This stops the 07 at the start of the mask being entered
        'automatically, as this could result in an incorrect number being entered (077 instead of 07), and still allows the space to
        'be automatically entered after the first 5 digits - Charles McMahon
        If sender Is uxPhoneMobileText Then
            If uxPhoneMobileText.EditValue.ToString.Length > 2 Then
                uxPhoneMobileText.Properties.Mask.AutoComplete = Mask.AutoCompleteType.Default
            Else
                uxPhoneMobileText.Properties.Mask.AutoComplete = Mask.AutoCompleteType.None
            End If
        End If
        Trace.WriteLine("ControlEditValueChanged (FINISH)")

    End Sub

    ''' <summary>
    ''' Validates that all mandatory fields are completed and all fields are in the correct format
    ''' </summary>
    ''' <returns>Boolean - True if all inputs pass validation</returns>
    ''' <history>
    ''' <created author="Charles McMahon" date="17/08/2010">Added as part of Referral 321</created>
    ''' </history>
    ''' <remarks></remarks>
    Private Function ValidateInputs() As Boolean

        Trace.WriteLine("ValidateInputs (START)")
        'As we are checking a group, assume they are all correct until we find any that are not
        Dim blnInputOk As Boolean = True

        'No need to check when the form is loading
        If Not blnFormLoading Then
            If uxDelivery1Text.EditValue Is Nothing OrElse uxDelivery1Text.EditValue.ToString.Trim.Length = 0 Then
                Err.SetError(uxDelivery1Text, My.Resources.Strings.InformationRequired)
                blnInputOk = False
            Else
                Err.SetError(uxDelivery1Text, String.Empty)
            End If

            If uxDelivery3Text.EditValue Is Nothing OrElse uxDelivery3Text.EditValue.ToString.Trim.Length = 0 Then
                Err.SetError(uxDelivery3Text, My.Resources.Strings.InformationRequired)
                blnInputOk = False
            Else
                Err.SetError(uxDelivery3Text, String.Empty)
            End If

            'We need at least one phone number, but don't care which one we have, so treat the phone numbers as a "sub-group"
            If (uxPhoneHomeText.EditValue Is Nothing OrElse uxPhoneHomeText.EditValue.ToString.Trim.Length = 0) And _
                (uxPhoneMobileText.EditValue Is Nothing OrElse uxPhoneMobileText.EditValue.ToString.Trim.Length = 0) And _
                (uxPhoneWorkText.EditValue Is Nothing OrElse uxPhoneWorkText.EditValue.ToString.Trim.Length = 0) Then
                Err.SetError(uxPhoneHomeText, My.Resources.Strings.InformationRequired)
                Err.SetError(uxPhoneMobileText, My.Resources.Strings.InformationRequired)
                Err.SetError(uxPhoneWorkText, My.Resources.Strings.InformationRequired)
                blnInputOk = False
            Else
                Err.SetError(uxPhoneHomeText, String.Empty)
                Err.SetError(uxPhoneMobileText, String.Empty)
                Err.SetError(uxPhoneWorkText, String.Empty)
            End If

            If uxDeliveryPostText.EditValue Is Nothing Then
                Err.SetError(uxDeliveryPostText, My.Resources.Strings.InformationRequired)
                blnInputOk = False
            Else
                'Check that the post-code has been completed, the mask on the control controls the input, but doesn't check for completeness
                If uxDeliveryPostText.EditValue.ToString Like "[A-Z]# #[A-Z][A-Z]" Or _
                uxDeliveryPostText.EditValue.ToString Like "[A-Z]## #[A-Z][A-Z]" Or _
                uxDeliveryPostText.EditValue.ToString Like "[A-Z][A-Z]# #[A-Z][A-Z]" Or _
                uxDeliveryPostText.EditValue.ToString Like "[A-Z][A-Z]## #[A-Z][A-Z]" Or _
                uxDeliveryPostText.EditValue.ToString Like "[A-Z][A-Z][A-Z] #[A-Z][A-Z]" Or _
                uxDeliveryPostText.EditValue.ToString Like "[A-Z]#[A-Z] #[A-Z][A-Z]" Or _
                uxDeliveryPostText.EditValue.ToString Like "[A-Z][A-Z]#[A-Z] #[A-Z][A-Z]" Then
                    Err.SetError(uxDeliveryPostText, String.Empty)
                Else
                    Err.SetError(uxDeliveryPostText, My.Resources.Strings.InformationRequired)
                    blnInputOk = False
                End If
            End If

            'Check that there are no blank delivery text lines, unless it is a new one that we are adding
            Dim blnAddButtonEnabled As Boolean = True
            Dim blnDeliveryTextProblem As Boolean = False
            Dim txts As QodTextCollection = _qod.Texts.GetTexts(QodTextType.Type.DeliveryInstruction)
            If txts.Count > 0 AndAlso (uxInstructionsText.EditValue Is Nothing OrElse uxInstructionsText.EditValue.ToString = String.Empty) Then
                'If the text input is empty then the buttons should be disabled regardless of whether we display an error or not
                blnAddButtonEnabled = False
                blnInputOk = False
                blnDeliveryTextProblem = True
                If blnNewInstruction Then
                    'We are adding a new intruction, which by default will be blank, so no need to display an error at this time
                    Err.SetError(uxInstructionsText, String.Empty)
                Else
                    'It's blank and shouldn't be!
                    Err.SetError(uxInstructionsText, My.Resources.Strings.InformationRequired)
                End If
            Else
                'There is a value here so no error!
                Err.SetError(uxInstructionsText, String.Empty)
            End If

            Dim blnProblemFound As Boolean = False
            For Each txt As QodText In txts
                If txt.Text = String.Empty AndAlso txt.Number <> uxInstructionsList.EditValue.ToString Then
                    blnProblemFound = True
                End If
            Next
            If blnProblemFound Then
                'Only show an error here if there is no error in uxDeliveryText
                If Not blnDeliveryTextProblem Then
                    Err.SetError(uxInstructionsList, My.Resources.Strings.InformationRequired)
                Else
                    Err.SetError(uxInstructionsList, String.Empty)
                End If
                'Regardless of whether we show an error, we still want to disable the buttons until all data is correct
                blnAddButtonEnabled = False
                blnInputOk = False
            Else
                Err.SetError(uxInstructionsList, String.Empty)
            End If

            uxAddNewButton.Enabled = blnAddButtonEnabled
            'Check if there are no text lines and disable the remove button accordingly
            If txts.Count > 0 Then
                uxRemoveButton.Enabled = True
            Else
                uxRemoveButton.Enabled = False
            End If

        End If
        Trace.WriteLine("ValidateInputs (FINISH) " & CStr(blnInputOk))
        Return blnInputOk

    End Function

    Private Sub uxResetButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxResetButton.Click
        '_qod.Texts.ClearAdded()
        SetControlsToQodValues()
        uxAcceptButton.Enabled = False
    End Sub

    Private Sub uxAddNewButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxAddNewButton.Click

        Trace.WriteLine("uxAddNewButton_Click (START)")
        blnNewInstruction = True
        Dim txt As QodText = _qod.Texts.AddNew(QodTextType.Type.DeliveryInstruction)
        txt.SellingStoreId = _qod.SellingStoreId
        txt.SellingStoreOrderId = _qod.SellingStoreOrderId

        SetDeliveryInstructions()
        uxInstructionsList.EditValue = txt.Number
        uxInstructionsText.Focus()
        uxAcceptButton.Enabled = ValidateInputs()
        blnNewInstruction = False
        Trace.WriteLine("uxAddNewButton_Click (FINISH)")

    End Sub

    Private Sub uxRemoveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxRemoveButton.Click

        Dim txts As QodTextCollection = CType(_qod.Texts, QodTextCollection)
        Dim txt As QodText = txts.Find(uxInstructionsList.EditValue.ToString, QodTextType.Type.DeliveryInstruction)
        Dim intIndex As Integer
        intIndex = txts.IndexOf(txt)
        txts.RemoveAt(intIndex)
        uxInstructionsText.EditValue = String.Empty
        SetDeliveryInstructions()

    End Sub

    Private Sub uxAcceptButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxAcceptButton.Click
        If uxDelivery1Text.EditValue IsNot Nothing Then _qod.DeliveryAddress1 = uxDelivery1Text.EditValue.ToString
        If uxDelivery2Text.EditValue IsNot Nothing Then _qod.DeliveryAddress2 = uxDelivery2Text.EditValue.ToString
        If uxDelivery3Text.EditValue IsNot Nothing Then _qod.DeliveryAddress3 = uxDelivery3Text.EditValue.ToString
        If uxDelivery4Text.EditValue IsNot Nothing Then _qod.DeliveryAddress4 = uxDelivery4Text.EditValue.ToString
        If uxDeliveryPostText.EditValue IsNot Nothing Then _qod.DeliveryPostCode = uxDeliveryPostText.EditValue.ToString
        If uxDeliveryDate.EditValue IsNot Nothing Then _qod.DateDelivery = CDate(uxDeliveryDate.EditValue)

        If uxPhoneHomeText.EditValue IsNot Nothing Then _qod.PhoneNumber = uxPhoneHomeText.EditValue.ToString
        If uxPhoneMobileText.EditValue IsNot Nothing Then _qod.PhoneNumberMobile = uxPhoneMobileText.EditValue.ToString
        If uxPhoneWorkText.EditValue IsNot Nothing Then _qod.PhoneNumberWork = uxPhoneWorkText.EditValue.ToString
        If uxEmailText.EditValue IsNot Nothing Then _qod.CustomerEmail = uxEmailText.EditValue.ToString
        _qod.IsSuspended = uxDeliveryStatusCheck.Checked
        _qod.IncrementRevisionNumber()

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub uxCancelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxCancelButton.Click
        '_qod.Texts.ClearAdded()
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    ''' <summary>
    ''' This stops the date being entered by typing the date in
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <history>
    ''' <created author="Charles McMahon" date="12/08/2010">Added as part of referral 324</created>
    ''' </history>
    ''' <remarks>
    ''' It displays the calendar when the user presses a key, forcing them to use
    ''' the calendar to pick a date, ensuring that the date will be valid and correct
    ''' </remarks>
    Private Sub uxDeliveryDate_PreviewKeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PreviewKeyDownEventArgs) Handles uxDeliveryDate.PreviewKeyDown
        uxDeliveryDate.ShowPopup()
    End Sub

    ''' <summary>
    ''' Stops the custom dev express errors appearing, as these are tied to the controls
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <history>
    ''' <created author="Charles McMahon" date="24/08/2010">Added as part of referral number 321</created>
    ''' </history>
    ''' <remarks>
    ''' Here it is better to have a vb.NET error provider to check all fields together as a group because we are
    ''' enabling and disabling the accept button based on the overall state of input errors, not just one field.
    ''' </remarks>
    Private Sub uxDeliveryPostText_InvalidValue(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles uxDeliveryPostText.InvalidValue
        e.ExceptionMode = ExceptionMode.NoAction
    End Sub

End Class