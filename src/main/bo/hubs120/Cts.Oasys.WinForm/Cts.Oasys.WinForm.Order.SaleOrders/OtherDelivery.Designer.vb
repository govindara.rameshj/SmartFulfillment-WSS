<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class OtherDelivery
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.XrPriceDifStore = New DevExpress.XtraReports.UI.XRLabel
        Me.XrQtyDifStore = New DevExpress.XtraReports.UI.XRLabel
        Me.XrDescDifStore = New DevExpress.XtraReports.UI.XRLabel
        Me.XrSkuDifStore = New DevExpress.XtraReports.UI.XRLabel
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.XrSeperateDeliveryInfo = New DevExpress.XtraReports.UI.XRLabel
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.Detail.BorderWidth = 2
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPriceDifStore, Me.XrQtyDifStore, Me.XrDescDifStore, Me.XrSkuDifStore})
        Me.Detail.Height = 19
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StylePriority.UseBorders = False
        Me.Detail.StylePriority.UseBorderWidth = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrPriceDifStore
        '
        Me.XrPriceDifStore.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrPriceDifStore.Font = New System.Drawing.Font("Trebuchet MS", 8.25!)
        Me.XrPriceDifStore.Location = New System.Drawing.Point(500, 0)
        Me.XrPriceDifStore.Name = "XrPriceDifStore"
        Me.XrPriceDifStore.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPriceDifStore.Size = New System.Drawing.Size(67, 17)
        Me.XrPriceDifStore.StylePriority.UseBorders = False
        Me.XrPriceDifStore.StylePriority.UseFont = False
        Me.XrPriceDifStore.StylePriority.UseTextAlignment = False
        Me.XrPriceDifStore.Text = "XrPriceDifStore"
        Me.XrPriceDifStore.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrQtyDifStore
        '
        Me.XrQtyDifStore.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrQtyDifStore.Font = New System.Drawing.Font("Trebuchet MS", 8.25!)
        Me.XrQtyDifStore.Location = New System.Drawing.Point(350, 0)
        Me.XrQtyDifStore.Name = "XrQtyDifStore"
        Me.XrQtyDifStore.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrQtyDifStore.Size = New System.Drawing.Size(67, 17)
        Me.XrQtyDifStore.StylePriority.UseBorders = False
        Me.XrQtyDifStore.StylePriority.UseFont = False
        Me.XrQtyDifStore.StylePriority.UseTextAlignment = False
        Me.XrQtyDifStore.Text = "XrQtyDifStore"
        Me.XrQtyDifStore.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrDescDifStore
        '
        Me.XrDescDifStore.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrDescDifStore.Font = New System.Drawing.Font("Trebuchet MS", 8.25!)
        Me.XrDescDifStore.Location = New System.Drawing.Point(58, 0)
        Me.XrDescDifStore.Name = "XrDescDifStore"
        Me.XrDescDifStore.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrDescDifStore.Size = New System.Drawing.Size(275, 17)
        Me.XrDescDifStore.StylePriority.UseBorders = False
        Me.XrDescDifStore.StylePriority.UseFont = False
        Me.XrDescDifStore.Text = "XrDescDifStore"
        '
        'XrSkuDifStore
        '
        Me.XrSkuDifStore.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrSkuDifStore.Font = New System.Drawing.Font("Trebuchet MS", 8.25!)
        Me.XrSkuDifStore.Location = New System.Drawing.Point(0, 0)
        Me.XrSkuDifStore.Name = "XrSkuDifStore"
        Me.XrSkuDifStore.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrSkuDifStore.Size = New System.Drawing.Size(50, 17)
        Me.XrSkuDifStore.StylePriority.UseBorders = False
        Me.XrSkuDifStore.StylePriority.UseFont = False
        Me.XrSkuDifStore.Text = "XrSkuDifStore"
        '
        'PageHeader
        '
        Me.PageHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSeperateDeliveryInfo})
        Me.PageHeader.Height = 19
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.StylePriority.UseBorderColor = False
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrSeperateDeliveryInfo
        '
        Me.XrSeperateDeliveryInfo.CanGrow = False
        Me.XrSeperateDeliveryInfo.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrSeperateDeliveryInfo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.XrSeperateDeliveryInfo.Location = New System.Drawing.Point(0, 0)
        Me.XrSeperateDeliveryInfo.Name = "XrSeperateDeliveryInfo"
        Me.XrSeperateDeliveryInfo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrSeperateDeliveryInfo.Size = New System.Drawing.Size(333, 17)
        Me.XrSeperateDeliveryInfo.StylePriority.UseFont = False
        Me.XrSeperateDeliveryInfo.StylePriority.UseForeColor = False
        Me.XrSeperateDeliveryInfo.Text = "The following items are to be delivered separately:  "
        '
        'OtherDelivery
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader})
        Me.Version = "9.1"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents XrSeperateDeliveryInfo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPriceDifStore As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrQtyDifStore As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrDescDifStore As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrSkuDifStore As DevExpress.XtraReports.UI.XRLabel
End Class
