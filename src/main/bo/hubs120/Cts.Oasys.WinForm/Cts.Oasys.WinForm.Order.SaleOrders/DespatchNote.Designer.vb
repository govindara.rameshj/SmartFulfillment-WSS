<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class DespatchNote
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Code39Generator1 As DevExpress.XtraPrinting.BarCode.Code39Generator = New DevExpress.XtraPrinting.BarCode.Code39Generator
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DespatchNote))
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.xrSkuVolume = New DevExpress.XtraReports.UI.XRLabel
        Me.xrSkuWeight = New DevExpress.XtraReports.UI.XRLabel
        Me.xrSkuPrice = New DevExpress.XtraReports.UI.XRLabel
        Me.XrSkuPicked = New DevExpress.XtraReports.UI.XRLabel
        Me.FormattingRule1 = New DevExpress.XtraReports.UI.FormattingRule
        Me.xrSkuQty = New DevExpress.XtraReports.UI.XRLabel
        Me.xrSkuDescription = New DevExpress.XtraReports.UI.XRLabel
        Me.xrSkuNumber = New DevExpress.XtraReports.UI.XRLabel
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrSellingStoreId = New DevExpress.XtraReports.UI.XRLabel
        Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
        Me.xrCustAddress = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel33 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel34 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel35 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel36 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel37 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrDelColNew = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel39 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrCustName = New DevExpress.XtraReports.UI.XRLabel
        Me.xrCustPhone = New DevExpress.XtraReports.UI.XRLabel
        Me.xrCustPhoneMobile = New DevExpress.XtraReports.UI.XRLabel
        Me.xrDateOrder = New DevExpress.XtraReports.UI.XRLabel
        Me.xrDateDelivery = New DevExpress.XtraReports.UI.XRLabel
        Me.xrOrderValue = New DevExpress.XtraReports.UI.XRLabel
        Me.xrDateDeliveryDay = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel21 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel23 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrCustEmail = New DevExpress.XtraReports.UI.XRLabel
        Me.xrCustPhoneWork = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel22 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrPanel2 = New DevExpress.XtraReports.UI.XRPanel
        Me.xrOrderBarCode = New DevExpress.XtraReports.UI.XRBarCode
        Me.xrRevisionNumber = New DevExpress.XtraReports.UI.XRLabel
        Me.xrRevisionNumberLabel = New DevExpress.XtraReports.UI.XRLabel
        Me.xrStoreFax = New DevExpress.XtraReports.UI.XRLabel
        Me.xrStorePhone = New DevExpress.XtraReports.UI.XRLabel
        Me.xrStoreAddr3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrStoreAddr2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrStoreAddr1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrStoreName = New DevExpress.XtraReports.UI.XRLabel
        Me.xrDatePrinted = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
        Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox
        Me.xrOrderNumber = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrStoreId = New DevExpress.XtraReports.UI.XRLabel
        Me.xrSellingStoreOrderId = New DevExpress.XtraReports.UI.XRLabel
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand
        Me.xrPageNumber = New DevExpress.XtraReports.UI.XRPageInfo
        Me.XrPanel4 = New DevExpress.XtraReports.UI.XRPanel
        Me.xrInstructions = New DevExpress.XtraReports.UI.XRLabel
        Me.xrTotalCost = New DevExpress.XtraReports.UI.XRLabel
        Me.xrDeliveryCharge = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel31 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrPanel3 = New DevExpress.XtraReports.UI.XRPanel
        Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine
        Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine3 = New DevExpress.XtraReports.UI.XRLine
        Me.XrLabel28 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine4 = New DevExpress.XtraReports.UI.XRLine
        Me.XrLabel29 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine5 = New DevExpress.XtraReports.UI.XRLine
        Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
        Me.QtyToTake = New DevExpress.XtraReports.UI.CalculatedField
        Me.QtyMinusRefund = New DevExpress.XtraReports.UI.CalculatedField
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
        Me.XrLine6 = New DevExpress.XtraReports.UI.XRLine
        Me.XrSubreport1 = New DevExpress.XtraReports.UI.XRSubreport
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrSkuVolume, Me.xrSkuWeight, Me.xrSkuPrice, Me.XrSkuPicked, Me.xrSkuQty, Me.xrSkuDescription, Me.xrSkuNumber})
        Me.Detail.Height = 18
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrSkuVolume
        '
        Me.xrSkuVolume.Location = New System.Drawing.Point(642, 0)
        Me.xrSkuVolume.Name = "xrSkuVolume"
        Me.xrSkuVolume.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrSkuVolume.Size = New System.Drawing.Size(67, 15)
        Me.xrSkuVolume.StylePriority.UseTextAlignment = False
        Me.xrSkuVolume.Text = "xrSkuVolume"
        Me.xrSkuVolume.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'xrSkuWeight
        '
        Me.xrSkuWeight.Location = New System.Drawing.Point(575, 0)
        Me.xrSkuWeight.Name = "xrSkuWeight"
        Me.xrSkuWeight.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrSkuWeight.Size = New System.Drawing.Size(67, 15)
        Me.xrSkuWeight.StylePriority.UseTextAlignment = False
        Me.xrSkuWeight.Text = "xrSkuWeight"
        Me.xrSkuWeight.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'xrSkuPrice
        '
        Me.xrSkuPrice.Location = New System.Drawing.Point(508, 0)
        Me.xrSkuPrice.Name = "xrSkuPrice"
        Me.xrSkuPrice.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrSkuPrice.Size = New System.Drawing.Size(67, 17)
        Me.xrSkuPrice.StylePriority.UseTextAlignment = False
        Me.xrSkuPrice.Text = "xrSkuPrice"
        Me.xrSkuPrice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrSkuPicked
        '
        Me.XrSkuPicked.FormattingRules.Add(Me.FormattingRule1)
        Me.XrSkuPicked.Location = New System.Drawing.Point(433, 0)
        Me.XrSkuPicked.Name = "XrSkuPicked"
        Me.XrSkuPicked.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrSkuPicked.Size = New System.Drawing.Size(67, 17)
        Me.XrSkuPicked.StylePriority.UseTextAlignment = False
        Me.XrSkuPicked.Text = "xrSkuPicked"
        Me.XrSkuPicked.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'FormattingRule1
        '
        Me.FormattingRule1.Condition = "Iif(xrSkuPicked.text=0,False,true)"
        '
        '
        '
        Me.FormattingRule1.Formatting.Visible = DevExpress.Utils.DefaultBoolean.[False]
        Me.FormattingRule1.Name = "FormattingRule1"
        '
        'xrSkuQty
        '
        Me.xrSkuQty.Location = New System.Drawing.Point(358, 0)
        Me.xrSkuQty.Name = "xrSkuQty"
        Me.xrSkuQty.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrSkuQty.Size = New System.Drawing.Size(67, 17)
        Me.xrSkuQty.StylePriority.UseTextAlignment = False
        Me.xrSkuQty.Text = "xrSkuQty"
        Me.xrSkuQty.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'xrSkuDescription
        '
        Me.xrSkuDescription.Location = New System.Drawing.Point(67, 0)
        Me.xrSkuDescription.Name = "xrSkuDescription"
        Me.xrSkuDescription.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrSkuDescription.Size = New System.Drawing.Size(275, 17)
        Me.xrSkuDescription.StylePriority.UseTextAlignment = False
        Me.xrSkuDescription.Text = "XrLabel21"
        Me.xrSkuDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrSkuNumber
        '
        Me.xrSkuNumber.Location = New System.Drawing.Point(8, 0)
        Me.xrSkuNumber.Name = "xrSkuNumber"
        Me.xrSkuNumber.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrSkuNumber.Size = New System.Drawing.Size(50, 15)
        Me.xrSkuNumber.StylePriority.UseTextAlignment = False
        Me.xrSkuNumber.Text = "XrLabel15"
        Me.xrSkuNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'PageHeader
        '
        Me.PageHeader.BorderWidth = 2
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.XrLabel2, Me.xrSellingStoreId, Me.XrPanel1, Me.XrLabel22, Me.XrPanel2, Me.XrLabel16, Me.XrLabel17, Me.XrLabel18, Me.XrLabel19, Me.XrLabel20, Me.XrLine1, Me.XrPictureBox1, Me.xrOrderNumber, Me.XrLabel1, Me.XrLabel13, Me.xrStoreId, Me.xrSellingStoreOrderId})
        Me.PageHeader.Height = 434
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.StylePriority.UseBorderWidth = False
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel3
        '
        Me.XrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel3.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel3.Location = New System.Drawing.Point(8, 75)
        Me.XrLabel3.Multiline = True
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.Size = New System.Drawing.Size(308, 17)
        Me.XrLabel3.StylePriority.UseBorders = False
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "This order contains 7 day delivery lead time line items"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel3.Visible = False
        '
        'XrLabel2
        '
        Me.XrLabel2.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel2.Location = New System.Drawing.Point(433, 400)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.Size = New System.Drawing.Size(67, 17)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.Text = "Picked"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'xrSellingStoreId
        '
        Me.xrSellingStoreId.Font = New System.Drawing.Font("Trebuchet MS", 14.0!, System.Drawing.FontStyle.Bold)
        Me.xrSellingStoreId.Location = New System.Drawing.Point(167, 8)
        Me.xrSellingStoreId.Name = "xrSellingStoreId"
        Me.xrSellingStoreId.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrSellingStoreId.Size = New System.Drawing.Size(83, 33)
        Me.xrSellingStoreId.StylePriority.UseFont = False
        Me.xrSellingStoreId.StylePriority.UseTextAlignment = False
        Me.xrSellingStoreId.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrPanel1
        '
        Me.XrPanel1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.XrPanel1.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrPanel1.BorderWidth = 2
        Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrCustAddress, Me.XrLabel33, Me.XrLabel34, Me.XrLabel35, Me.XrLabel36, Me.XrLabel37, Me.XrDelColNew, Me.XrLabel39, Me.xrCustName, Me.xrCustPhone, Me.xrCustPhoneMobile, Me.xrDateOrder, Me.xrDateDelivery, Me.xrOrderValue, Me.xrDateDeliveryDay, Me.XrLabel21, Me.XrLabel23, Me.xrCustEmail, Me.xrCustPhoneWork})
        Me.XrPanel1.Location = New System.Drawing.Point(8, 92)
        Me.XrPanel1.Name = "XrPanel1"
        Me.XrPanel1.Size = New System.Drawing.Size(333, 292)
        Me.XrPanel1.StylePriority.UseBorderColor = False
        Me.XrPanel1.StylePriority.UseBorders = False
        Me.XrPanel1.StylePriority.UseBorderWidth = False
        '
        'xrCustAddress
        '
        Me.xrCustAddress.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top
        Me.xrCustAddress.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrCustAddress.Location = New System.Drawing.Point(117, 25)
        Me.xrCustAddress.Multiline = True
        Me.xrCustAddress.Name = "xrCustAddress"
        Me.xrCustAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrCustAddress.Size = New System.Drawing.Size(208, 83)
        Me.xrCustAddress.StylePriority.UseBorders = False
        Me.xrCustAddress.StylePriority.UseTextAlignment = False
        Me.xrCustAddress.Text = "xrCustAddress"
        Me.xrCustAddress.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel33
        '
        Me.XrLabel33.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel33.Location = New System.Drawing.Point(8, 8)
        Me.XrLabel33.Name = "XrLabel33"
        Me.XrLabel33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel33.Size = New System.Drawing.Size(101, 17)
        Me.XrLabel33.StylePriority.UseBorders = False
        Me.XrLabel33.StylePriority.UseTextAlignment = False
        Me.XrLabel33.Text = "Customer Name:"
        '
        'XrLabel34
        '
        Me.XrLabel34.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel34.Location = New System.Drawing.Point(9, 25)
        Me.XrLabel34.Name = "XrLabel34"
        Me.XrLabel34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel34.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel34.StylePriority.UseBorders = False
        Me.XrLabel34.StylePriority.UseTextAlignment = False
        Me.XrLabel34.Text = "Address:"
        '
        'XrLabel35
        '
        Me.XrLabel35.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel35.Location = New System.Drawing.Point(9, 133)
        Me.XrLabel35.Name = "XrLabel35"
        Me.XrLabel35.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel35.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel35.StylePriority.UseBorders = False
        Me.XrLabel35.StylePriority.UseTextAlignment = False
        Me.XrLabel35.Text = "Telephone:"
        Me.XrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel36
        '
        Me.XrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel36.Location = New System.Drawing.Point(8, 150)
        Me.XrLabel36.Name = "XrLabel36"
        Me.XrLabel36.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel36.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel36.StylePriority.UseBorders = False
        Me.XrLabel36.StylePriority.UseTextAlignment = False
        Me.XrLabel36.Text = "Mobile:"
        Me.XrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel37
        '
        Me.XrLabel37.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel37.Location = New System.Drawing.Point(8, 216)
        Me.XrLabel37.Multiline = True
        Me.XrLabel37.Name = "XrLabel37"
        Me.XrLabel37.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel37.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel37.StylePriority.UseBorders = False
        Me.XrLabel37.StylePriority.UseTextAlignment = False
        Me.XrLabel37.Text = "Order Date:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.XrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrDelColNew
        '
        Me.XrDelColNew.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrDelColNew.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrDelColNew.Location = New System.Drawing.Point(9, 250)
        Me.XrDelColNew.Name = "XrDelColNew"
        Me.XrDelColNew.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrDelColNew.Size = New System.Drawing.Size(100, 17)
        Me.XrDelColNew.StylePriority.UseBorders = False
        Me.XrDelColNew.StylePriority.UseFont = False
        Me.XrDelColNew.StylePriority.UseTextAlignment = False
        Me.XrDelColNew.Text = "Collection Date:"
        Me.XrDelColNew.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel39
        '
        Me.XrLabel39.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel39.Location = New System.Drawing.Point(8, 267)
        Me.XrLabel39.Name = "XrLabel39"
        Me.XrLabel39.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel39.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel39.StylePriority.UseBorders = False
        Me.XrLabel39.StylePriority.UseTextAlignment = False
        Me.XrLabel39.Text = "Order Value:"
        Me.XrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrCustName
        '
        Me.xrCustName.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrCustName.Location = New System.Drawing.Point(117, 8)
        Me.xrCustName.Name = "xrCustName"
        Me.xrCustName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrCustName.Size = New System.Drawing.Size(208, 17)
        Me.xrCustName.StylePriority.UseBorders = False
        Me.xrCustName.StylePriority.UseTextAlignment = False
        Me.xrCustName.Text = "xrCustName"
        '
        'xrCustPhone
        '
        Me.xrCustPhone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrCustPhone.Location = New System.Drawing.Point(117, 133)
        Me.xrCustPhone.Name = "xrCustPhone"
        Me.xrCustPhone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrCustPhone.Size = New System.Drawing.Size(100, 17)
        Me.xrCustPhone.StylePriority.UseBorders = False
        Me.xrCustPhone.StylePriority.UseTextAlignment = False
        Me.xrCustPhone.Text = "xrCustPhone"
        Me.xrCustPhone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrCustPhoneMobile
        '
        Me.xrCustPhoneMobile.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrCustPhoneMobile.Location = New System.Drawing.Point(117, 150)
        Me.xrCustPhoneMobile.Name = "xrCustPhoneMobile"
        Me.xrCustPhoneMobile.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrCustPhoneMobile.Size = New System.Drawing.Size(100, 17)
        Me.xrCustPhoneMobile.StylePriority.UseBorders = False
        Me.xrCustPhoneMobile.StylePriority.UseTextAlignment = False
        Me.xrCustPhoneMobile.Text = "xrCustPhoneMobile"
        Me.xrCustPhoneMobile.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrDateOrder
        '
        Me.xrDateOrder.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrDateOrder.Location = New System.Drawing.Point(117, 216)
        Me.xrDateOrder.Name = "xrDateOrder"
        Me.xrDateOrder.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrDateOrder.Size = New System.Drawing.Size(100, 17)
        Me.xrDateOrder.StylePriority.UseBorders = False
        Me.xrDateOrder.StylePriority.UseTextAlignment = False
        Me.xrDateOrder.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrDateDelivery
        '
        Me.xrDateDelivery.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrDateDelivery.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xrDateDelivery.Location = New System.Drawing.Point(117, 250)
        Me.xrDateDelivery.Name = "xrDateDelivery"
        Me.xrDateDelivery.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrDateDelivery.Size = New System.Drawing.Size(100, 17)
        Me.xrDateDelivery.StylePriority.UseBorders = False
        Me.xrDateDelivery.StylePriority.UseFont = False
        Me.xrDateDelivery.StylePriority.UseTextAlignment = False
        Me.xrDateDelivery.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrOrderValue
        '
        Me.xrOrderValue.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrOrderValue.Location = New System.Drawing.Point(117, 267)
        Me.xrOrderValue.Name = "xrOrderValue"
        Me.xrOrderValue.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrOrderValue.Size = New System.Drawing.Size(100, 17)
        Me.xrOrderValue.StylePriority.UseBorders = False
        Me.xrOrderValue.StylePriority.UseTextAlignment = False
        Me.xrOrderValue.Text = "xrOrderValue"
        Me.xrOrderValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrDateDeliveryDay
        '
        Me.xrDateDeliveryDay.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrDateDeliveryDay.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xrDateDeliveryDay.Location = New System.Drawing.Point(225, 250)
        Me.xrDateDeliveryDay.Name = "xrDateDeliveryDay"
        Me.xrDateDeliveryDay.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrDateDeliveryDay.Size = New System.Drawing.Size(100, 17)
        Me.xrDateDeliveryDay.StylePriority.UseBorders = False
        Me.xrDateDeliveryDay.StylePriority.UseFont = False
        Me.xrDateDeliveryDay.StylePriority.UseTextAlignment = False
        Me.xrDateDeliveryDay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel21
        '
        Me.XrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel21.Location = New System.Drawing.Point(8, 167)
        Me.XrLabel21.Name = "XrLabel21"
        Me.XrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel21.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel21.StylePriority.UseBorders = False
        Me.XrLabel21.StylePriority.UseTextAlignment = False
        Me.XrLabel21.Text = "Work Phone:"
        Me.XrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel23
        '
        Me.XrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel23.Location = New System.Drawing.Point(9, 184)
        Me.XrLabel23.Name = "XrLabel23"
        Me.XrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel23.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel23.StylePriority.UseBorders = False
        Me.XrLabel23.StylePriority.UseTextAlignment = False
        Me.XrLabel23.Text = "Email:"
        Me.XrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrCustEmail
        '
        Me.xrCustEmail.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrCustEmail.Location = New System.Drawing.Point(117, 184)
        Me.xrCustEmail.Name = "xrCustEmail"
        Me.xrCustEmail.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrCustEmail.Size = New System.Drawing.Size(208, 17)
        Me.xrCustEmail.StylePriority.UseBorders = False
        Me.xrCustEmail.Text = "xrCustEmail"
        '
        'xrCustPhoneWork
        '
        Me.xrCustPhoneWork.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrCustPhoneWork.Location = New System.Drawing.Point(117, 167)
        Me.xrCustPhoneWork.Name = "xrCustPhoneWork"
        Me.xrCustPhoneWork.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrCustPhoneWork.Size = New System.Drawing.Size(100, 17)
        Me.xrCustPhoneWork.StylePriority.UseBorders = False
        Me.xrCustPhoneWork.Text = "xrCustPhoneWork"
        '
        'XrLabel22
        '
        Me.XrLabel22.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel22.Location = New System.Drawing.Point(508, 400)
        Me.XrLabel22.Name = "XrLabel22"
        Me.XrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel22.Size = New System.Drawing.Size(67, 17)
        Me.XrLabel22.StylePriority.UseFont = False
        Me.XrLabel22.StylePriority.UseTextAlignment = False
        Me.XrLabel22.Text = "Price"
        Me.XrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrPanel2
        '
        Me.XrPanel2.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.XrPanel2.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrPanel2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrOrderBarCode, Me.xrRevisionNumber, Me.xrRevisionNumberLabel, Me.xrStoreFax, Me.xrStorePhone, Me.xrStoreAddr3, Me.xrStoreAddr2, Me.xrStoreAddr1, Me.xrStoreName, Me.xrDatePrinted, Me.XrLabel14, Me.XrLabel12, Me.XrLabel11})
        Me.XrPanel2.Location = New System.Drawing.Point(375, 92)
        Me.XrPanel2.Name = "XrPanel2"
        Me.XrPanel2.Size = New System.Drawing.Size(333, 292)
        Me.XrPanel2.StylePriority.UseBorderColor = False
        Me.XrPanel2.StylePriority.UseBorders = False
        '
        'xrOrderBarCode
        '
        Me.xrOrderBarCode.AutoModule = True
        Me.xrOrderBarCode.BorderColor = System.Drawing.Color.Transparent
        Me.xrOrderBarCode.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrOrderBarCode.Location = New System.Drawing.Point(42, 175)
        Me.xrOrderBarCode.Name = "xrOrderBarCode"
        Me.xrOrderBarCode.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
        Me.xrOrderBarCode.Size = New System.Drawing.Size(258, 75)
        Me.xrOrderBarCode.StylePriority.UseBorderColor = False
        Me.xrOrderBarCode.StylePriority.UseBorders = False
        Me.xrOrderBarCode.StylePriority.UseTextAlignment = False
        Code39Generator1.CalcCheckSum = False
        Code39Generator1.WideNarrowRatio = 3.0!
        Me.xrOrderBarCode.Symbology = Code39Generator1
        Me.xrOrderBarCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        '
        'xrRevisionNumber
        '
        Me.xrRevisionNumber.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrRevisionNumber.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrRevisionNumber.Location = New System.Drawing.Point(108, 250)
        Me.xrRevisionNumber.Name = "xrRevisionNumber"
        Me.xrRevisionNumber.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrRevisionNumber.Size = New System.Drawing.Size(100, 17)
        Me.xrRevisionNumber.StylePriority.UseBorders = False
        Me.xrRevisionNumber.StylePriority.UseFont = False
        Me.xrRevisionNumber.StylePriority.UseTextAlignment = False
        Me.xrRevisionNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrRevisionNumber.Visible = False
        '
        'xrRevisionNumberLabel
        '
        Me.xrRevisionNumberLabel.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrRevisionNumberLabel.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrRevisionNumberLabel.Location = New System.Drawing.Point(8, 250)
        Me.xrRevisionNumberLabel.Name = "xrRevisionNumberLabel"
        Me.xrRevisionNumberLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrRevisionNumberLabel.Size = New System.Drawing.Size(100, 17)
        Me.xrRevisionNumberLabel.StylePriority.UseBorders = False
        Me.xrRevisionNumberLabel.StylePriority.UseFont = False
        Me.xrRevisionNumberLabel.StylePriority.UseTextAlignment = False
        Me.xrRevisionNumberLabel.Text = "Revision Number:"
        Me.xrRevisionNumberLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrRevisionNumberLabel.Visible = False
        '
        'xrStoreFax
        '
        Me.xrStoreFax.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrStoreFax.Location = New System.Drawing.Point(108, 150)
        Me.xrStoreFax.Name = "xrStoreFax"
        Me.xrStoreFax.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrStoreFax.Size = New System.Drawing.Size(100, 17)
        Me.xrStoreFax.StylePriority.UseBorders = False
        Me.xrStoreFax.StylePriority.UseTextAlignment = False
        Me.xrStoreFax.Text = "0123456789"
        Me.xrStoreFax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrStorePhone
        '
        Me.xrStorePhone.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrStorePhone.Location = New System.Drawing.Point(108, 133)
        Me.xrStorePhone.Name = "xrStorePhone"
        Me.xrStorePhone.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrStorePhone.Size = New System.Drawing.Size(100, 17)
        Me.xrStorePhone.StylePriority.UseBorders = False
        Me.xrStorePhone.StylePriority.UseTextAlignment = False
        Me.xrStorePhone.Text = "0123456789"
        Me.xrStorePhone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrStoreAddr3
        '
        Me.xrStoreAddr3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrStoreAddr3.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrStoreAddr3.Location = New System.Drawing.Point(8, 83)
        Me.xrStoreAddr3.Name = "xrStoreAddr3"
        Me.xrStoreAddr3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrStoreAddr3.Size = New System.Drawing.Size(317, 25)
        Me.xrStoreAddr3.StylePriority.UseBorders = False
        Me.xrStoreAddr3.StylePriority.UseFont = False
        Me.xrStoreAddr3.StylePriority.UseTextAlignment = False
        Me.xrStoreAddr3.Text = "Hampshire"
        Me.xrStoreAddr3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrStoreAddr2
        '
        Me.xrStoreAddr2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrStoreAddr2.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrStoreAddr2.Location = New System.Drawing.Point(8, 58)
        Me.xrStoreAddr2.Name = "xrStoreAddr2"
        Me.xrStoreAddr2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrStoreAddr2.Size = New System.Drawing.Size(317, 25)
        Me.xrStoreAddr2.StylePriority.UseBorders = False
        Me.xrStoreAddr2.StylePriority.UseFont = False
        Me.xrStoreAddr2.StylePriority.UseTextAlignment = False
        Me.xrStoreAddr2.Text = "Farnborough"
        Me.xrStoreAddr2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrStoreAddr1
        '
        Me.xrStoreAddr1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrStoreAddr1.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrStoreAddr1.Location = New System.Drawing.Point(8, 33)
        Me.xrStoreAddr1.Name = "xrStoreAddr1"
        Me.xrStoreAddr1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrStoreAddr1.Size = New System.Drawing.Size(317, 25)
        Me.xrStoreAddr1.StylePriority.UseBorders = False
        Me.xrStoreAddr1.StylePriority.UseFont = False
        Me.xrStoreAddr1.StylePriority.UseTextAlignment = False
        Me.xrStoreAddr1.Text = "13b Invincible Road"
        Me.xrStoreAddr1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrStoreName
        '
        Me.xrStoreName.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrStoreName.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrStoreName.Location = New System.Drawing.Point(8, 8)
        Me.xrStoreName.Name = "xrStoreName"
        Me.xrStoreName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrStoreName.Size = New System.Drawing.Size(317, 25)
        Me.xrStoreName.StylePriority.UseBorders = False
        Me.xrStoreName.StylePriority.UseFont = False
        Me.xrStoreName.StylePriority.UseTextAlignment = False
        Me.xrStoreName.Text = "Wickes Farnborough"
        Me.xrStoreName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrDatePrinted
        '
        Me.xrDatePrinted.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrDatePrinted.Location = New System.Drawing.Point(108, 267)
        Me.xrDatePrinted.Name = "xrDatePrinted"
        Me.xrDatePrinted.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrDatePrinted.Size = New System.Drawing.Size(100, 17)
        Me.xrDatePrinted.StylePriority.UseBorders = False
        Me.xrDatePrinted.StylePriority.UseTextAlignment = False
        Me.xrDatePrinted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel14
        '
        Me.XrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel14.Location = New System.Drawing.Point(8, 267)
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel14.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel14.StylePriority.UseBorders = False
        Me.XrLabel14.StylePriority.UseTextAlignment = False
        Me.XrLabel14.Text = "Date Printed:"
        Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel12
        '
        Me.XrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel12.Location = New System.Drawing.Point(8, 150)
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel12.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel12.StylePriority.UseBorders = False
        Me.XrLabel12.StylePriority.UseTextAlignment = False
        Me.XrLabel12.Text = "Store Fax:"
        Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel11
        '
        Me.XrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel11.Location = New System.Drawing.Point(8, 133)
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel11.StylePriority.UseBorders = False
        Me.XrLabel11.StylePriority.UseTextAlignment = False
        Me.XrLabel11.Text = "Store Telephone:"
        Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel16
        '
        Me.XrLabel16.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel16.Location = New System.Drawing.Point(8, 400)
        Me.XrLabel16.Name = "XrLabel16"
        Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel16.Size = New System.Drawing.Size(50, 17)
        Me.XrLabel16.StylePriority.UseFont = False
        Me.XrLabel16.StylePriority.UseTextAlignment = False
        Me.XrLabel16.Text = "SKU #"
        Me.XrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel17
        '
        Me.XrLabel17.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel17.Location = New System.Drawing.Point(67, 400)
        Me.XrLabel17.Name = "XrLabel17"
        Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel17.Size = New System.Drawing.Size(275, 17)
        Me.XrLabel17.StylePriority.UseFont = False
        Me.XrLabel17.StylePriority.UseTextAlignment = False
        Me.XrLabel17.Text = "Description"
        Me.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel18
        '
        Me.XrLabel18.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel18.Location = New System.Drawing.Point(358, 400)
        Me.XrLabel18.Name = "XrLabel18"
        Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel18.Size = New System.Drawing.Size(67, 17)
        Me.XrLabel18.StylePriority.UseFont = False
        Me.XrLabel18.StylePriority.UseTextAlignment = False
        Me.XrLabel18.Text = "Quantity"
        Me.XrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel19
        '
        Me.XrLabel19.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel19.Location = New System.Drawing.Point(575, 400)
        Me.XrLabel19.Multiline = True
        Me.XrLabel19.Name = "XrLabel19"
        Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel19.Size = New System.Drawing.Size(67, 17)
        Me.XrLabel19.StylePriority.UseFont = False
        Me.XrLabel19.StylePriority.UseTextAlignment = False
        Me.XrLabel19.Text = "Weight"
        Me.XrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel20
        '
        Me.XrLabel20.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel20.Location = New System.Drawing.Point(642, 400)
        Me.XrLabel20.Multiline = True
        Me.XrLabel20.Name = "XrLabel20"
        Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel20.Size = New System.Drawing.Size(67, 17)
        Me.XrLabel20.StylePriority.UseFont = False
        Me.XrLabel20.StylePriority.UseTextAlignment = False
        Me.XrLabel20.Text = "Volume"
        Me.XrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLine1
        '
        Me.XrLine1.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.XrLine1.LineWidth = 2
        Me.XrLine1.Location = New System.Drawing.Point(8, 417)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.Size = New System.Drawing.Size(700, 17)
        Me.XrLine1.StylePriority.UseForeColor = False
        '
        'XrPictureBox1
        '
        Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
        Me.XrPictureBox1.Location = New System.Drawing.Point(442, 0)
        Me.XrPictureBox1.Name = "XrPictureBox1"
        Me.XrPictureBox1.Size = New System.Drawing.Size(267, 83)
        '
        'xrOrderNumber
        '
        Me.xrOrderNumber.Font = New System.Drawing.Font("Trebuchet MS", 14.0!, System.Drawing.FontStyle.Bold)
        Me.xrOrderNumber.Location = New System.Drawing.Point(250, 42)
        Me.xrOrderNumber.Name = "xrOrderNumber"
        Me.xrOrderNumber.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrOrderNumber.Size = New System.Drawing.Size(108, 33)
        Me.xrOrderNumber.StylePriority.UseFont = False
        '
        'XrLabel1
        '
        Me.XrLabel1.Font = New System.Drawing.Font("Trebuchet MS", 14.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel1.Location = New System.Drawing.Point(8, 8)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.Size = New System.Drawing.Size(158, 33)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "Customer Order:"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel13
        '
        Me.XrLabel13.Font = New System.Drawing.Font("Trebuchet MS", 14.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel13.Location = New System.Drawing.Point(8, 42)
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel13.Size = New System.Drawing.Size(158, 33)
        Me.XrLabel13.StylePriority.UseFont = False
        Me.XrLabel13.StylePriority.UseTextAlignment = False
        Me.XrLabel13.Text = "Collection Id:"
        Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrStoreId
        '
        Me.xrStoreId.Font = New System.Drawing.Font("Trebuchet MS", 14.0!, System.Drawing.FontStyle.Bold)
        Me.xrStoreId.Location = New System.Drawing.Point(167, 42)
        Me.xrStoreId.Name = "xrStoreId"
        Me.xrStoreId.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrStoreId.Size = New System.Drawing.Size(83, 33)
        Me.xrStoreId.StylePriority.UseFont = False
        Me.xrStoreId.StylePriority.UseTextAlignment = False
        Me.xrStoreId.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrSellingStoreOrderId
        '
        Me.xrSellingStoreOrderId.Font = New System.Drawing.Font("Trebuchet MS", 14.0!, System.Drawing.FontStyle.Bold)
        Me.xrSellingStoreOrderId.Location = New System.Drawing.Point(250, 8)
        Me.xrSellingStoreOrderId.Name = "xrSellingStoreOrderId"
        Me.xrSellingStoreOrderId.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrSellingStoreOrderId.Size = New System.Drawing.Size(108, 33)
        Me.xrSellingStoreOrderId.StylePriority.UseFont = False
        Me.xrSellingStoreOrderId.StylePriority.UseTextAlignment = False
        Me.xrSellingStoreOrderId.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrPageNumber})
        Me.PageFooter.Height = 22
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrPageNumber
        '
        Me.xrPageNumber.Format = "Page {0} of {1}"
        Me.xrPageNumber.Location = New System.Drawing.Point(608, 0)
        Me.xrPageNumber.Name = "xrPageNumber"
        Me.xrPageNumber.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrPageNumber.Size = New System.Drawing.Size(100, 17)
        Me.xrPageNumber.StylePriority.UseTextAlignment = False
        Me.xrPageNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        '
        'XrPanel4
        '
        Me.XrPanel4.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.XrPanel4.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrPanel4.BorderWidth = 2
        Me.XrPanel4.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrInstructions, Me.xrTotalCost, Me.xrDeliveryCharge, Me.XrLabel31, Me.XrLabel30})
        Me.XrPanel4.Location = New System.Drawing.Point(383, 67)
        Me.XrPanel4.Name = "XrPanel4"
        Me.XrPanel4.Size = New System.Drawing.Size(325, 225)
        Me.XrPanel4.StylePriority.UseBorderColor = False
        Me.XrPanel4.StylePriority.UseBorders = False
        Me.XrPanel4.StylePriority.UseBorderWidth = False
        '
        'xrInstructions
        '
        Me.xrInstructions.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrInstructions.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xrInstructions.Location = New System.Drawing.Point(8, 83)
        Me.xrInstructions.Name = "xrInstructions"
        Me.xrInstructions.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrInstructions.Size = New System.Drawing.Size(309, 133)
        Me.xrInstructions.StylePriority.UseBorders = False
        Me.xrInstructions.StylePriority.UseFont = False
        Me.xrInstructions.StylePriority.UseTextAlignment = False
        Me.xrInstructions.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrTotalCost
        '
        Me.xrTotalCost.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrTotalCost.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xrTotalCost.Location = New System.Drawing.Point(133, 42)
        Me.xrTotalCost.Name = "xrTotalCost"
        Me.xrTotalCost.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrTotalCost.Size = New System.Drawing.Size(83, 25)
        Me.xrTotalCost.StylePriority.UseBorders = False
        Me.xrTotalCost.StylePriority.UseFont = False
        Me.xrTotalCost.StylePriority.UseTextAlignment = False
        Me.xrTotalCost.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'xrDeliveryCharge
        '
        Me.xrDeliveryCharge.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrDeliveryCharge.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xrDeliveryCharge.Location = New System.Drawing.Point(133, 8)
        Me.xrDeliveryCharge.Name = "xrDeliveryCharge"
        Me.xrDeliveryCharge.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrDeliveryCharge.Size = New System.Drawing.Size(83, 25)
        Me.xrDeliveryCharge.StylePriority.UseBorders = False
        Me.xrDeliveryCharge.StylePriority.UseFont = False
        Me.xrDeliveryCharge.StylePriority.UseTextAlignment = False
        Me.xrDeliveryCharge.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel31
        '
        Me.XrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel31.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel31.Location = New System.Drawing.Point(8, 42)
        Me.XrLabel31.Name = "XrLabel31"
        Me.XrLabel31.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel31.Size = New System.Drawing.Size(117, 25)
        Me.XrLabel31.StylePriority.UseBorders = False
        Me.XrLabel31.StylePriority.UseFont = False
        Me.XrLabel31.StylePriority.UseTextAlignment = False
        Me.XrLabel31.Text = "Order Total:"
        Me.XrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel30
        '
        Me.XrLabel30.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel30.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel30.Location = New System.Drawing.Point(8, 8)
        Me.XrLabel30.Name = "XrLabel30"
        Me.XrLabel30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel30.Size = New System.Drawing.Size(117, 25)
        Me.XrLabel30.StylePriority.UseBorders = False
        Me.XrLabel30.StylePriority.UseFont = False
        Me.XrLabel30.StylePriority.UseTextAlignment = False
        Me.XrLabel30.Text = "Delivery Charge:"
        Me.XrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrPanel3
        '
        Me.XrPanel3.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.XrPanel3.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrPanel3.BorderWidth = 2
        Me.XrPanel3.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel25, Me.XrLabel26, Me.XrLine2, Me.XrLabel27, Me.XrLine3, Me.XrLabel28, Me.XrLine4, Me.XrLabel29, Me.XrLine5})
        Me.XrPanel3.Location = New System.Drawing.Point(8, 67)
        Me.XrPanel3.Name = "XrPanel3"
        Me.XrPanel3.Size = New System.Drawing.Size(333, 225)
        Me.XrPanel3.StylePriority.UseBorderColor = False
        Me.XrPanel3.StylePriority.UseBorders = False
        Me.XrPanel3.StylePriority.UseBorderWidth = False
        '
        'XrLabel25
        '
        Me.XrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel25.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel25.Location = New System.Drawing.Point(8, 8)
        Me.XrLabel25.Name = "XrLabel25"
        Me.XrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel25.Size = New System.Drawing.Size(242, 25)
        Me.XrLabel25.StylePriority.UseBorders = False
        Me.XrLabel25.StylePriority.UseFont = False
        Me.XrLabel25.Text = "Delivery Service Control"
        '
        'XrLabel26
        '
        Me.XrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel26.Location = New System.Drawing.Point(8, 42)
        Me.XrLabel26.Multiline = True
        Me.XrLabel26.Name = "XrLabel26"
        Me.XrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel26.Size = New System.Drawing.Size(100, 25)
        Me.XrLabel26.StylePriority.UseBorders = False
        Me.XrLabel26.Text = "Drivers Signature" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'XrLine2
        '
        Me.XrLine2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLine2.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.XrLine2.Location = New System.Drawing.Point(125, 58)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.Size = New System.Drawing.Size(200, 25)
        Me.XrLine2.StylePriority.UseBorders = False
        Me.XrLine2.StylePriority.UseForeColor = False
        '
        'XrLabel27
        '
        Me.XrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel27.Location = New System.Drawing.Point(8, 83)
        Me.XrLabel27.Multiline = True
        Me.XrLabel27.Name = "XrLabel27"
        Me.XrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel27.Size = New System.Drawing.Size(117, 25)
        Me.XrLabel27.StylePriority.UseBorders = False
        Me.XrLabel27.Text = "Store Pick Signature"
        '
        'XrLine3
        '
        Me.XrLine3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLine3.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.XrLine3.Location = New System.Drawing.Point(125, 108)
        Me.XrLine3.Name = "XrLine3"
        Me.XrLine3.Size = New System.Drawing.Size(200, 17)
        Me.XrLine3.StylePriority.UseBorders = False
        Me.XrLine3.StylePriority.UseForeColor = False
        '
        'XrLabel28
        '
        Me.XrLabel28.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel28.Location = New System.Drawing.Point(8, 133)
        Me.XrLabel28.Multiline = True
        Me.XrLabel28.Name = "XrLabel28"
        Me.XrLabel28.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel28.Size = New System.Drawing.Size(117, 25)
        Me.XrLabel28.StylePriority.UseBorders = False
        Me.XrLabel28.Text = "Store Check Signature"
        '
        'XrLine4
        '
        Me.XrLine4.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLine4.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.XrLine4.Location = New System.Drawing.Point(125, 158)
        Me.XrLine4.Name = "XrLine4"
        Me.XrLine4.Size = New System.Drawing.Size(200, 17)
        Me.XrLine4.StylePriority.UseBorders = False
        Me.XrLine4.StylePriority.UseForeColor = False
        '
        'XrLabel29
        '
        Me.XrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel29.Location = New System.Drawing.Point(9, 175)
        Me.XrLabel29.Multiline = True
        Me.XrLabel29.Name = "XrLabel29"
        Me.XrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel29.Size = New System.Drawing.Size(117, 25)
        Me.XrLabel29.StylePriority.UseBorders = False
        Me.XrLabel29.Text = "Customers Signature"
        '
        'XrLine5
        '
        Me.XrLine5.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLine5.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.XrLine5.Location = New System.Drawing.Point(125, 200)
        Me.XrLine5.Name = "XrLine5"
        Me.XrLine5.Size = New System.Drawing.Size(200, 17)
        Me.XrLine5.StylePriority.UseBorders = False
        Me.XrLine5.StylePriority.UseForeColor = False
        '
        'XrLabel10
        '
        Me.XrLabel10.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel10.Location = New System.Drawing.Point(533, 456)
        Me.XrLabel10.Name = "XrLabel10"
        Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel10.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel10.StylePriority.UseFont = False
        Me.XrLabel10.StylePriority.UseTextAlignment = False
        Me.XrLabel10.Text = "Quantity Ordered"
        Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'QtyToTake
        '
        Me.QtyToTake.Name = "QtyToTake"
        '
        'QtyMinusRefund
        '
        Me.QtyMinusRefund.Name = "QtyMinusRefund"
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine6, Me.XrSubreport1, Me.XrPanel3, Me.XrPanel4})
        Me.ReportFooter.Height = 296
        Me.ReportFooter.KeepTogether = True
        Me.ReportFooter.Name = "ReportFooter"
        Me.ReportFooter.PrintAtBottom = True
        Me.ReportFooter.StylePriority.UseTextAlignment = False
        '
        'XrLine6
        '
        Me.XrLine6.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.XrLine6.LineWidth = 2
        Me.XrLine6.Location = New System.Drawing.Point(8, 0)
        Me.XrLine6.Name = "XrLine6"
        Me.XrLine6.Size = New System.Drawing.Size(700, 17)
        Me.XrLine6.StylePriority.UseForeColor = False
        '
        'XrSubreport1
        '
        Me.XrSubreport1.CanShrink = True
        Me.XrSubreport1.Location = New System.Drawing.Point(8, 17)
        Me.XrSubreport1.Name = "XrSubreport1"
        Me.XrSubreport1.Size = New System.Drawing.Size(683, 42)
        '
        'DespatchNote
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter})
        Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.QtyToTake, Me.QtyMinusRefund})
        Me.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormattingRuleSheet.AddRange(New DevExpress.XtraReports.UI.FormattingRule() {Me.FormattingRule1})
        Me.Margins = New System.Drawing.Printing.Margins(60, 55, 35, 49)
        Me.PageHeight = 1169
        Me.PageWidth = 827
        Me.PaperKind = System.Drawing.Printing.PaperKind.A4
        Me.Version = "9.1"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPanel2 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrPanel3 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine4 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel28 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine3 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel27 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrDeliveryCharge As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel31 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel30 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine5 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel29 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrTotalCost As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrSkuVolume As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrDatePrinted As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrStorePhone As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrStoreAddr3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrStoreAddr2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrStoreAddr1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrStoreName As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrStoreFax As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents xrOrderNumber As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrSkuNumber As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrSkuDescription As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrSkuWeight As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents QtyToTake As DevExpress.XtraReports.UI.CalculatedField
    Friend WithEvents QtyMinusRefund As DevExpress.XtraReports.UI.CalculatedField
    Friend WithEvents xrSkuQty As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrRevisionNumberLabel As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrRevisionNumber As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrInstructions As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrStoreId As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrSellingStoreOrderId As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel22 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrSkuPrice As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel33 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel34 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel35 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel36 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel37 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrDelColNew As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel39 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrCustName As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrCustPhone As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrCustPhoneMobile As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrDateOrder As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrDateDelivery As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrOrderValue As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrDateDeliveryDay As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel21 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel23 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrCustEmail As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrCustPhoneWork As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrCustAddress As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrSellingStoreId As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPanel4 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents xrPageNumber As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrOrderBarCode As DevExpress.XtraReports.UI.XRBarCode
    Friend WithEvents XrSkuPicked As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrSubreport1 As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents XrLine6 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents FormattingRule1 As DevExpress.XtraReports.UI.FormattingRule
End Class
