Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.Order
Imports Cts.Oasys.Core.Order.Qod
Imports Cts.Oasys.Core.Order.Qod.State
Imports Cts.Oasys.Core.System
Imports Cts.Oasys.Core.System.Store
Imports System.Xml
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports System.ComponentModel
Imports DevExpress.XtraTab
Imports Cts.Oasys.Core.Order.WebService

Public Class MainForm
    Private _storeId As Integer
    Private _localState As String = "LocalState"
    Private _localPhase As String = "LocalPhase"
    Private _localCount As String = "LocalCount"
    Private _DeliverySourceMissing As String = "DeliverySourceMissing"
    Private Const PICKCONFIRM_BUTTON As Long = 970100
    Private Const STORE8120 As Long = 3020

    Private _COLOR_PASTEL_GREEN As System.Drawing.Color = Color.PaleGreen ' System.Drawing.ColorTranslator.FromHtml("#92CCA6")
    Private _COLOR_DEFAULT_BACKCOLOR As System.Drawing.Color = Color.White
    Private _ConfirmPickingAvailable As Boolean = False
    Private _Store8120 As Integer = 0
    Private _ParentQod As QodHeader = Nothing

    Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, ByVal RunParameters As String)
        MyBase.New(userId, workstationId, securityLevel, RunParameters)
        InitializeComponent()

        If Me.AppName Is Nothing OrElse Me.AppName = String.Empty Then Me.AppName = My.Resources.Strings.AppName
        uxStartDate.EditValue = Now.Date
        uxEndDate.EditValue = Now.Date
        uxRePrintDespatchNote.Visible = False

        ' determine if pick confirmation functionality is available
        _ConfirmPickingAvailable = Parameter.GetBoolean(PICKCONFIRM_BUTTON)
 
        If _ConfirmPickingAvailable Then
            ' enable/disable buttons depending on availability of pick confirmation functionality
            uxMaintainButton.Enabled = False
            uxPrintDespatchButton.Enabled = False
            uxConfirmPickingButton.Visible = True
            uxRePrintDespatchNote.Visible = True
            uxConfirmOrderButton.Enabled = True
            uxRePrintDespatchNote.Enabled = False
            uxDeliveredButton.Enabled = False
            uxPrintButton.Enabled = False
            uxDespatchButton.Visible = False
        Else
            uxConfirmPickingButton.Visible = False
            uxRePrintDespatchNote.Visible = False
            uxDespatchButton.Visible = True
        End If

        ' default focus to OM Order Number text box
        uxOMOrderNumber.Focus()

    End Sub

    Protected Overrides Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        e.Handled = True

        'ref 777 : if on "om enquiry" tab then ignore F3, F5, F6, F7, F8; these buttons will be disabled, this is just a safety backup
        Select Case uxOrderTabControl.SelectedTabPage.Name
            Case uxOpenOrderTab.Name, uxAllOrderTab.Name, uxNonZeroStockTab.Name

                Select Case e.KeyData
                    Case Keys.F2 : uxRetrieveButton.PerformClick()
                    Case Keys.F3 : uxMaintainButton.PerformClick()
                        'Case Keys.F4 : uxConfirmOrderButton.PerformClick()
                    Case Keys.F5 : uxPrintDespatchButton.PerformClick()
                    Case Keys.F6 : uxConfirmPickingButton.PerformClick()
                    Case Keys.F7
                        If uxConfirmPickingButton.Visible = True Then
                            uxRePrintDespatchNote.PerformClick()
                        Else
                            uxDespatchButton.PerformClick()
                        End If

                    Case Keys.F8 : uxDeliveredButton.PerformClick()
                    Case Keys.F9 : uxPrintButton.PerformClick()
                    Case Keys.F11 : uxResetButton.PerformClick()
                    Case Keys.F12 : uxExitButton.PerformClick()
                    Case Else
                        e.Handled = False
                End Select

            Case uxFullOrderTab.Name

                'ignore function keys F3, F5, F6, F7, F8
                Select Case e.KeyData
                    Case Keys.F2 : uxRetrieveButton.PerformClick()
                    Case Keys.F9 : uxPrintButton.PerformClick()
                    Case Keys.F11 : uxResetButton.PerformClick()
                    Case Keys.F12 : uxExitButton.PerformClick()
                    Case Else
                        e.Handled = False
                End Select

        End Select

    End Sub

    Protected Overrides Sub DoProcessing()
        _storeId = ThisStore.Id4
        _Store8120 = Parameter.GetInteger(STORE8120)

        uxNonZeroStockTab.PageVisible = Core.System.Parameter.IsQodVendaStore
        If uxNonZeroStockTab.PageVisible = True AndAlso _storeId = _Store8120 Then
            uxOrderTabControl.SelectedTabPageIndex = 3
        End If
        RetrieveOrders()
    End Sub

    Private Sub RetrieveOrders() Handles uxRetrieveButton.Click

        Try
            Cursor = Cursors.WaitCursor

            Dim grid As GridControl = GetFocusedGrid()
            If grid.Name = uxNonZeroGrid.Name Then
                Dim ds As Data.DataSet = Qod.GetNonZeroStockOrOutstandingIbt
                grid.DataSource = ds
                grid.DataMember = ds.Tables(0).TableName

                Dim mainView As GridView = CType(grid.MainView, GridView)
                mainView.Columns(0).Caption = My.Resources.Columns.SkuNumber
                mainView.BestFitColumns()
                'uxNonZeroVarianceZero.Checked = True     'Ref 815 - Removed Checkbox as required

            Else

                Dim qods As QodHeaderCollection = GetQods()
                grid.DataSource = qods

                GridLoadQods(grid)
                GridLoadQodLines(grid)
                GridLoadQodTexts(grid)
                GridLoadQodRefunds(grid)

                If grid.Name = uxFullOrderGrid.Name Then
                    Dim view As GridView = CType(grid.MainView, GridView)
                    If view.RowCount > 0 Then
                        view.ExpandAllGroups()
                        For rowHandle As Integer = 0 To view.RowCount - 1
                            view.SetMasterRowExpanded(rowHandle, True)
                        Next
                    End If
                End If

                If Not StringIsSomething(RunParameters) Then uxShowFulfilCheck.Checked = True
            End If

            CheckButtonEnableStatus()
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Overloads Function GetQods() As QodHeaderCollection

        Dim grid As GridControl = GetFocusedGrid()
        If grid IsNot Nothing Then

            Select Case grid.Name
                Case uxOpenOrderGrid.Name
                    If _ConfirmPickingAvailable = False Then
                        'default route, used by most stores
                        Return Qod.GetAllOpenOrders
                    Else
                        ' this is eStore warehouse do not get get open orders, the users will search for orders instead
                        If Me.uxOMOrderNumber.Text.Length = 0 Then
                            ' return an empty collection
                            Return New QodHeaderCollection
                        Else
                            ' get all open orders with matching order number
                            Return GetQods(CInt(Me.uxOMOrderNumber.Text))
                        End If
                    End If

                Case uxAllOrderGrid.Name
                    Return Qod.GetAllForDateRange(CDate(uxStartDate.EditValue), CDate(uxEndDate.EditValue))

                Case uxFullOrderGrid.Name
                    Dim omOrder As Integer = 0
                    Dim storeId As Integer = 0
                    Dim storeOrder As Integer = 0
                    If (uxFullOmOrderText.EditValue IsNot Nothing AndAlso uxFullOmOrderText.EditValue.ToString.Length > 0) Then _
                        omOrder = CInt(uxFullOmOrderText.EditValue)
                    If (uxFullStoreIdText.EditValue IsNot Nothing AndAlso uxFullStoreIdText.EditValue.ToString.Length > 0) Then _
                        storeId = CInt(uxFullStoreIdText.EditValue)
                    If (uxFullStoreOrderText.EditValue IsNot Nothing AndAlso uxFullStoreOrderText.EditValue.ToString.Length > 0) Then _
                        storeOrder = CInt(uxFullStoreOrderText.EditValue)

                    If omOrder > 0 OrElse (storeId > 0 AndAlso storeOrder > 0) Then
                        Dim qods As New QodHeaderCollection
                        If grid.DataSource IsNot Nothing Then qods = CType(grid.DataSource, QodHeaderCollection)

                        Dim orderManagerService As New FullStatusService
                        Dim request As OMSendFullStatusUpdateRequest = Nothing

                        request = CType(orderManagerService.GetRequest, OMSendFullStatusUpdateRequest)
                        request.SetFields(omOrder, storeId, storeOrder)

                        Dim requestXml As String = request.Serialise
                        Dim responseXml As String = orderManagerService.GetResponseXml(requestXml)

                        If responseXml IsNot Nothing AndAlso responseXml.Length > 0 Then
                            Dim response As New OMSendFullStatusUpdateResponse
                            response = CType(response.Deserialise(responseXml), OMSendFullStatusUpdateResponse)

                            If response.IsSuccessful Then
                                Dim qod As QodHeader = response.GetQod
                                If qod IsNot Nothing Then qods.Add(qod)
                            Else
                                Dim errorDetail As OMSendFullStatusUpdateResponseOrderStatusErrorDetail = response.GetErrorDetail
                                If errorDetail IsNot Nothing Then
                                    uxFullOmOrderErrorLabel.Text = errorDetail.OMOrderNumber.ValidationStatus
                                    uxFullStoreIdErrorLabel.Text = errorDetail.SellingStoreCode.ValidationStatus
                                    uxFullStoreOrderErrorLabel.Text = errorDetail.SellingStoreOrderNumber.ValidationStatus
                                End If
                            End If

                        End If

                        Return qods

                    End If
            End Select
        End If
        Return Nothing

    End Function

    Private Overloads Function GetQods(ByVal OMOrderNumber As Integer) As QodHeaderCollection
        ' get qod given the OM Order Number
        Dim col As New QodHeaderCollection
        col.Add(Qod.GetForOmOrderNumber(OMOrderNumber))
        Return col
    End Function


    Private Sub GridLoadQods(ByVal grid As GridControl)

        Dim view As GridView = CType(grid.MainView, GridView)
        For Each col As GridColumn In view.Columns
            col.VisibleIndex = -1
        Next

        view.Columns.AddField(_localState).UnboundType = DevExpress.Data.UnboundColumnType.String
        view.Columns.AddField(_localPhase).UnboundType = DevExpress.Data.UnboundColumnType.String
        view.Columns.AddField(_localCount).UnboundType = DevExpress.Data.UnboundColumnType.Integer

        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.OmOrderNumber), My.Resources.Columns.OMOrderNumber, 40, 80)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.Number), My.Resources.Columns.DeliveryNumber, 55, 70)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.IsForDelivery), My.Resources.Columns.DeliveryOrder)
        If uxNonZeroStockTab.PageVisible Then GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.VendaNumber))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.SellingStoreId))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.SellingStoreName))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.SellingStorePhone))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.SellingStoreOrderId), My.Resources.Columns.SellingStoreOrderId)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.DespatchSite))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.CustomerName))
        'GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.DateDelivery), My.Resources.Columns.DateDelivery)
        GridViewSetColumn(view, _localPhase, My.Resources.Columns.DeliveryPhase)
        GridViewSetColumn(view, _localState, My.Resources.Columns.DeliveryState)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.NumberStockIssues), My.Resources.Columns.StockIssues)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.IsSuspended), My.Resources.Columns.IsSuspended, 80)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.RevisionNumber))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.PhoneNumber), My.Resources.Columns.PhoneNumber)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.PhoneNumberMobile), My.Resources.Columns.PhoneMobile)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.CustomerAddress))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.DeliveryAddress))
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.PhoneNumberWork), My.Resources.Columns.PhoneWork)
        GridViewSetColumn(view, GetPropertyName(Function(f As QodHeader) f.CustomerEmail), My.Resources.Columns.CustomerEmail)
        view.BestFitColumns()

        'add view sorting (if not order manager enquiry)
        If grid.Name <> uxFullOrderGrid.Name Then
            Try
                view.BeginSort()
                view.ClearGrouping()

                If StringIsSomething(RunParameters) Then
                    view.Columns(_localPhase).GroupIndex = 0
                    view.GroupedColumns(0).SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
                Else
                    If _ConfirmPickingAvailable = False Then
                        ' standard route
                        view.Columns(GetPropertyName(Function(f As QodHeader) f.DateDelivery)).GroupIndex = 0
                        view.Columns(_localPhase).GroupIndex = 1
                    Else
                        ' eStore route 
                        view.Columns(_localPhase).GroupIndex = 0
                        view.Columns(GetPropertyName(Function(f As QodHeader) f.DateDelivery)).GroupIndex = 1
                    End If

                    view.GroupedColumns(0).SortOrder = DevExpress.Data.ColumnSortOrder.Descending
                End If
            Finally
                view.EndSort()
            End Try

            If StringIsSomething(RunParameters) Then
                Dim groupCount As Integer = GetGroupRowCount(view)
                If groupCount > 0 Then
                    For groupHandle As Integer = 1 To groupCount
                        Dim text As String = view.GetGroupRowValue(groupHandle * -1).ToString.ToLower
                        If text = RunParameters.ToLower Then
                            view.SetRowExpanded(groupHandle * -1, True)
                            Exit For
                        End If
                    Next
                End If
            End If

        End If

    End Sub

    Private Sub GridLoadQodLines(ByVal grid As GridControl)

        Dim view As New GridView(grid)
        grid.LevelTree.Nodes.Add(GetPropertyName(Function(f As QodHeader) f.Lines), view)

        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.SkuNumber), My.Resources.Columns.SkuNumber, 45, 45)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.SkuDescription), My.Resources.Columns.SkuDescription, 190)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.DeliveryPhase), My.Resources.Columns.DeliveryPhase, 130)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.DeliveryStatus), My.Resources.Columns.DeliveryState, 55, 55)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.QtyOrdered), My.Resources.Columns.QtyOrdered)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.QtyTaken), My.Resources.Columns.QtyTaken)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.QtyRefunded), My.Resources.Columns.Refunded, 55)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.QtyToDeliver), My.Resources.Columns.QtyToDeliver)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.QtyOnHand), My.Resources.Columns.QtyOnHand, 50, 50)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.QtyOnOrder), My.Resources.Columns.QtyOnOrder, 55, 55)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.NextPoDateDelivery), My.Resources.Columns.NextPoDeliveryDate, 55)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.DeliverySource), My.Resources.Columns.DeliverySource, 55)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.FulfillingStoreName), My.Resources.Columns.DeliverySource, 55)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodLine) f.FulfillingStorePhone), My.Resources.Columns.FulfillingStorePhone, 55)

        view.Columns(GetPropertyName(Function(f As QodLine) f.NextPoDateDelivery)).Visible = (grid.Name = uxOpenOrderGrid.Name)
        If grid.Name <> uxFullOrderGrid.Name Then
            view.Columns(GetPropertyName(Function(f As QodLine) f.DeliverySource)).FilterInfo = New ColumnFilterInfo("[" & GetPropertyName(Function(f As QodLine) f.DeliverySource) & "]=" & _storeId)
        End If

        view.Appearance.Assign(grid.MainView.Appearance)
        view.OptionsView.ShowGroupPanel = False
        view.OptionsBehavior.Editable = False
        view.ColumnPanelRowHeight = 35
        view.OptionsFilter.AllowFilterEditor = False
        view.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.ShowAlways
        view.OptionsView.ShowIndicator = False
        view.OptionsSelection.EnableAppearanceFocusedCell = False
        view.OptionsSelection.EnableAppearanceFocusedRow = False
        view.FocusRectStyle = DrawFocusRectStyle.None

        AddHandler view.RowStyle, AddressOf uxViewLines_RowStyle

    End Sub

    Private Sub GridLoadQodTexts(ByVal grid As GridControl)

        Dim view As New GridView(grid)
        grid.LevelTree.Nodes.Add(GetPropertyName(Function(f As QodHeader) f.Texts), view)

        GridViewAddColumn(view, GetPropertyName(Function(f As QodText) f.TextType), "", 150, 150)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodText) f.Number), "", 80, 80)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodText) f.Text))

        view.Appearance.Assign(grid.MainView.Appearance)
        view.OptionsView.ShowGroupPanel = False
        view.OptionsBehavior.Editable = False
        view.ColumnPanelRowHeight = 35
        view.OptionsFilter.AllowFilterEditor = False
        view.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Never
        view.OptionsView.ShowIndicator = False
        view.OptionsSelection.EnableAppearanceFocusedCell = False
        view.OptionsSelection.EnableAppearanceFocusedRow = False
        view.FocusRectStyle = DrawFocusRectStyle.None

    End Sub

    Private Sub GridLoadQodRefunds(ByVal grid As GridControl)

        Dim view As New GridView(grid)
        grid.LevelTree.Nodes.Add(GetPropertyName(Function(f As QodHeader) f.Refunds), view)

        GridViewAddColumn(view, GetPropertyName(Function(f As QodRefund) f.SkuNumber), My.Resources.Columns.SkuNumber)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodRefund) f.SkuDescription), My.Resources.Columns.SkuDescription)
        GridViewAddColumn(view, GetPropertyName(Function(f As QodRefund) f.RefundStoreId))
        GridViewAddColumn(view, GetPropertyName(Function(f As QodRefund) f.RefundDate))
        GridViewAddColumn(view, GetPropertyName(Function(f As QodRefund) f.RefundTill))
        GridViewAddColumn(view, GetPropertyName(Function(f As QodRefund) f.RefundTransaction))
        GridViewAddColumn(view, GetPropertyName(Function(f As QodRefund) f.QtyReturned))
        GridViewAddColumn(view, GetPropertyName(Function(f As QodRefund) f.QtyCancelled))
        GridViewAddColumn(view, GetPropertyName(Function(f As QodRefund) f.ValueRefunded))

        view.Appearance.Assign(grid.MainView.Appearance)
        view.OptionsView.ShowGroupPanel = False
        view.OptionsBehavior.Editable = False
        view.ColumnPanelRowHeight = 35
        view.OptionsFilter.AllowFilterEditor = False
        view.OptionsView.ShowIndicator = False
        view.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Never
        view.OptionsSelection.EnableAppearanceFocusedCell = False
        view.OptionsSelection.EnableAppearanceFocusedRow = False
        view.FocusRectStyle = DrawFocusRectStyle.None

    End Sub

    Private Sub GridViewSetColumn(ByRef view As GridView, ByVal columnName As String, Optional ByVal caption As String = "", Optional ByVal minWidth As Integer = 0, Optional ByVal maxWidth As Integer = 0)
        view.Columns(columnName).VisibleIndex = view.VisibleColumns.Count
        If caption.Length > 0 Then view.Columns(columnName).Caption = caption
        If minWidth > 0 Then view.Columns(columnName).MinWidth = minWidth
        If maxWidth > 0 Then view.Columns(columnName).MaxWidth = maxWidth
    End Sub

    Private Sub GridViewAddColumn(ByRef view As GridView, ByVal columnName As String, Optional ByVal caption As String = "", Optional ByVal minWidth As Integer = 0, Optional ByVal maxWidth As Integer = 0)
        view.Columns.AddField(columnName).VisibleIndex = view.VisibleColumns.Count
        If caption.Length > 0 Then view.Columns(columnName).Caption = caption
        If minWidth > 0 Then view.Columns(columnName).MinWidth = minWidth
        If maxWidth > 0 Then view.Columns(columnName).MaxWidth = maxWidth
    End Sub


    'Private Sub SelectionChanged() Handles uxOpenOrderView.SelectionChanged

    '    Dim grid As GridControl = GetFocusedGrid()
    '    Dim view As GridView = CType(grid.MainView, GridView)
    '    For Each rowHandle As Integer In view.GetSelectedRows
    '        Trace.WriteLine("rowHandle " & rowHandle.ToString)
    '        Trace.WriteLine(view.GetDataSourceRowIndex(rowHandle).ToString)
    '    Next

    'End Sub

    ''' <summary>
    ''' Sets the button statuses (enabled or disabled) when the grid selection changes
    ''' </summary>
    ''' <history></history>
    ''' <remarks></remarks>
    Private Sub SetButtonStatus() Handles uxAllOrderView.SelectionChanged, uxOpenOrderView.SelectionChanged

        'Assume all buttons should be disabled
        SetButtonEnabledFalse()
        Dim PrintButtonOn As Boolean = False
        Dim DespatchButtonOn As Boolean = False
        Dim ConfirmPickingButtonOn As Boolean = False
        Dim ConfirmDeliveryButtonOn As Boolean = False
        Dim RePrintDispatchButtonOn As Boolean = False

        Dim grid As GridControl = GetFocusedGrid()
        Dim qods As QodHeaderCollection = GetSelectedQods(grid)



        'ref 777 : reset button visibility incase user is coming from the om enquiry tab
        uxMaintainButton.Visible = True         'F3
        uxPrintDespatchButton.Visible = True    'F5
        uxConfirmPickingButton.Visible = True   'F6
        uxRePrintDespatchNote.Visible = True    'F7
        uxDespatchButton.Visible = True         'F7
        uxDeliveredButton.Visible = True        'F8
        ConfirmPickingAvailableButtonVisibility(_ConfirmPickingAvailable)

        'Check if we have selected any QODs
        If qods.Count > 0 Then
            'If a single maintainable order is selected then allow the maintain button to be pressed
            If qods.Count = 1 AndAlso qods(0).IsLocalStoreMaintainable Then uxMaintainButton.Enabled = True

            'Check for the different QOD LINE states and update the buttons accordingly
            For Each QodHeader As QodHeader In qods
                If Not QodHeader.IsSuspended And QodHeader.DespatchSite.Contains(_storeId.ToString) Then
                    For Each QodLine As QodLine In QodHeader.Lines
                        If QodLine.DeliverySource = _storeId.ToString Then
                            Select Case QodLine.DeliveryStatePhase
                                Case Qod.State.DeliveryDescription.Phase3
                                    PrintButtonOn = True

                                Case Qod.State.DeliveryDescription.Phase4
                                    PrintButtonOn = True

                                Case Qod.State.DeliveryDescription.Phase5
                                    PrintButtonOn = True
                                    If uxConfirmPickingButton.Visible = True Then
                                        ConfirmPickingButtonOn = True
                                    End If
                                    DespatchButtonOn = True

                                Case Qod.State.DeliveryDescription.Phase6
                                    ConfirmDeliveryButtonOn = True
                                    If uxConfirmPickingButton.Visible = True Then
                                        RePrintDispatchButtonOn = True
                                    End If

                                Case Qod.State.DeliveryDescription.Phase7
                                    If _ConfirmPickingAvailable = True Then     'MO'C Change Request 0038
                                        ConfirmPickingButtonOn = True
                                        RePrintDispatchButtonOn = True
                                    Else
                                        DespatchButtonOn = True
                                        PrintButtonOn = True                    'MO'C Change Request 0038
                                    End If

                            End Select
                        End If
                    Next
                End If
            Next
        End If

        'Check there is only one order selected for picking confirmation
        If qods.Count > 1 Then
            ConfirmPickingButtonOn = False
        End If
        uxConfirmPickingButton.Enabled = ConfirmPickingButtonOn

        'Switch the required buttons on
        If _ConfirmPickingAvailable Then
            ' enable/disable buttons depending on availability of pick confirmation functionality
            uxMaintainButton.Enabled = False
            uxPrintDespatchButton.Enabled = False
            uxRePrintDespatchNote.Enabled = False
            uxDeliveredButton.Enabled = False
            uxPrintButton.Enabled = False
        Else
            uxPrintDespatchButton.Enabled = PrintButtonOn
            uxDespatchButton.Enabled = DespatchButtonOn
            uxDeliveredButton.Enabled = ConfirmDeliveryButtonOn

            If uxRePrintDespatchNote.Visible = True Then
                uxRePrintDespatchNote.Enabled = RePrintDispatchButtonOn
            End If

        End If

        'ref 777 : if on "om enquiry" tab then hide following buttons
        If uxOrderTabControl.SelectedTabPage.Name = uxFullOrderTab.Name Then
            uxMaintainButton.Visible = False         'F3
            uxPrintDespatchButton.Visible = False    'F5
            uxConfirmPickingButton.Visible = False   'F6
            uxRePrintDespatchNote.Visible = False    'F7
            uxDespatchButton.Visible = False         'F7
            uxDeliveredButton.Visible = False        'F8
        End If

    End Sub

    Private Sub SetButtonEnabledFalse()
        uxMaintainButton.Enabled = False
        'uxConfirmOrderButton.Enabled = False
        uxPrintDespatchButton.Enabled = False
        uxDespatchButton.Enabled = False
        uxConfirmPickingButton.Enabled = False
        uxDeliveredButton.Enabled = False
    End Sub

    Private Sub uxView_CustomUnboundColumnData(ByVal sender As Object, ByVal e As CustomColumnDataEventArgs) Handles uxOpenOrderView.CustomUnboundColumnData

        If e.RowHandle = GridControl.InvalidRowHandle Then Exit Sub
        If e.ListSourceRowIndex = GridControl.InvalidRowHandle Then Exit Sub

        'get order in question
        Dim view As GridView = CType(sender, GridView)
        Dim qods As QodHeaderCollection = CType(view.GridControl.DataSource, QodHeaderCollection)
        Dim qod As QodHeader = qods(e.ListSourceRowIndex)

        If uxShowFulfilCheck.Checked Then
            Dim status As Delivery= qod.Lines.DeliveryStatus(_storeId)
            Select Case e.Column.FieldName
                Case _localState : e.Value = DeliveryDescription.GetDescription(status)
                Case _localPhase : e.Value = DeliveryDescription.GetPhase(status)
                Case _localCount : e.Value = qod.Lines.CountDeliverySource(_storeId)
            End Select
        Else
            Select Case e.Column.FieldName
                Case _localState : e.Value = qod.DeliveryStateDescription
                Case _localPhase : e.Value = qod.DeliveryStatePhase
                Case _localCount : e.Value = qod.Lines.Count
            End Select
        End If

    End Sub

    Private Sub uxView_RowStyle(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles uxOpenOrderView.RowStyle, uxAllOrderView.RowStyle

        If e.RowHandle < 0 Then Exit Sub

        Dim view As GridView = CType(sender, GridView)
        Dim qods As QodHeaderCollection = CType(view.GridControl.DataSource, QodHeaderCollection)
        Dim qod As QodHeader = qods(view.GetDataSourceRowIndex(e.RowHandle))
        Dim col As GridColumn = view.Columns(_localState)

        If qod.IsDeliveryFailure Then
            e.Appearance.ForeColor = Color.Red
        End If

        If qod.ExtendedLeadTime Then
            e.Appearance.BackColor = Color.Cyan
        End If

    End Sub

    Private Sub uxViewLines_RowStyle(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs)

        'check if any rows loaded first
        If e.RowHandle < 0 Then Exit Sub

        'get view and sale order line for this row
        Dim view As GridView = CType(sender, GridView)


        'if delivery charge item then exit
        If CBool(view.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As QodLine) f.IsDeliveryChargeItem))) Then
            Exit Sub
        End If

        'get delivery source and try to convert to integer
        If view.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As QodLine) f.DeliverySource)) Is Nothing Then
            Exit Sub
        End If

        Dim deliverySource As String = view.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As QodLine) f.DeliverySource)).ToString
        Dim deliveryStore As Integer = 0
        If Integer.TryParse(deliverySource, deliveryStore) Then
            If deliveryStore = _storeId Then
                'check for sale line IsDependentOnOrders
                If CBool(view.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As QodLine) f.IsDependentOnOrders))) Then
                    e.Appearance.BackColor = Color.Gold
                End If

                'check for sale line IsNotFilfullable
                If CBool(view.GetRowCellValue(e.RowHandle, GetPropertyName(Function(f As QodLine) f.IsNotFulfillable))) Then
                    e.Appearance.BackColor = Color.Red
                End If

                'CR0043 - Decided not to do at line level just remarked out in case they change there mind again.
                ''check for Extended lead time from parent line
                'Dim ParentRowHandle As Integer = view.SourceRowHandle
                'Dim Qod As QodHeader = CType(view.ParentView.GetRow(ParentRowHandle), QodHeader)
                'If Qod.ExtendedLeadTime Then
                '    e.Appearance.BackColor = Color.Cyan
                'End If
            End If
        End If

    End Sub

    Private Sub CheckButtonEnableStatus() Handles uxOrderTabControl.SelectedPageChanged, uxFullOmOrderText.EditValueChanged, uxFullStoreIdText.EditValueChanged, uxFullStoreOrderText.EditValueChanged

        SetButtonStatus()
        uxRetrieveButton.Enabled = False
        uxPrintButton.Enabled = False


        'ref 777 : reset button visibility incase user is coming from the om enquiry tab
        uxMaintainButton.Visible = True         'F3
        uxPrintDespatchButton.Visible = True    'F5
        uxConfirmPickingButton.Visible = True   'F6
        uxRePrintDespatchNote.Visible = True    'F7
        uxDespatchButton.Visible = True         'F7
        uxDeliveredButton.Visible = True        'F8
        ConfirmPickingAvailableButtonVisibility(_ConfirmPickingAvailable)


        'check which tab is open
        Dim rowCount As Integer = 0
        Select Case uxOrderTabControl.SelectedTabPage.Name
            Case uxOpenOrderTab.Name
                rowCount = uxOpenOrderView.RowCount
                uxRetrieveButton.Enabled = True

                ' give the OM Number search textbox the focus
                uxOMOrderNumber.Focus()

            Case uxAllOrderTab.Name
                uxPrintDespatchButton.Enabled = False
                rowCount = uxAllOrderView.RowCount
                If uxStartDate.EditValue IsNot Nothing AndAlso uxEndDate.EditValue IsNot Nothing Then
                    uxRetrieveButton.Enabled = True
                End If

            Case uxFullOrderTab.Name
                uxPrintDespatchButton.Enabled = False
                rowCount = uxFullOrderView.RowCount
                If (uxFullOmOrderText.EditValue IsNot Nothing) AndAlso (uxFullOmOrderText.EditValue.ToString.Length > 0) _
                OrElse ((uxFullStoreIdText.EditValue IsNot Nothing) AndAlso (uxFullStoreIdText.EditValue.ToString.Length > 0) _
                AndAlso (uxFullStoreOrderText.EditValue IsNot Nothing) AndAlso (uxFullStoreOrderText.EditValue.ToString.Length > 0)) Then
                    uxRetrieveButton.Enabled = True
                End If

                'ref 777 : if on "om enquiry" tab then hide following buttons
                uxMaintainButton.Visible = False         'F3
                uxPrintDespatchButton.Visible = False    'F5
                uxConfirmPickingButton.Visible = False   'F6
                uxRePrintDespatchNote.Visible = False    'F7
                uxDespatchButton.Visible = False         'F7
                uxDeliveredButton.Visible = False        'F8

            Case uxNonZeroStockTab.Name
                uxPrintDespatchButton.Enabled = False
                rowCount = uxNonZeroView.RowCount
                uxRetrieveButton.Enabled = True
        End Select

        uxPrintButton.Enabled = (rowCount > 0) AndAlso _ConfirmPickingAvailable = False

    End Sub

    Private Sub uxShowFulfilCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxShowFulfilCheck.CheckedChanged

        'check which tab is open to get grid (and view) in question
        Dim grid As GridControl = GetFocusedGrid()
        If grid Is Nothing Then Exit Sub
        Dim view As GridView = CType(grid.MainView, GridView)

        If uxShowFulfilCheck.Checked Then
            view.Columns(_localCount).FilterInfo = New ColumnFilterInfo("[" & _localCount & "]>0 OR [" & _localPhase & "] Like '1%' OR [" & _
                                                                        _localPhase & "] Like '2%' And [" & _DeliverySourceMissing & "] = True")
        Else
            view.Columns(_localCount).ClearFilter()
        End If

        'Now make sure the correct buttons are showing
        SetButtonStatus()

    End Sub

    'Ref 815  - Removed Check box as per referral
    'Private Sub uxNonZeroVarianceZero_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim grid As GridControl = GetFocusedGrid()
    '    Dim view As GridView = CType(grid.MainView, GridView)

    '    If view.Columns.Count > 0 Then
    '        If uxNonZeroVarianceZero.Checked Then
    '            view.Columns(4).FilterInfo = New ColumnFilterInfo("[" & view.Columns(4).FieldName & "]<>0")
    '        Else
    '            view.Columns(4).ClearFilter()
    '        End If
    '    End If

    'End Sub

    Private Function GetFocusedGrid() As GridControl
        For Each ctl As Control In uxOrderTabControl.SelectedTabPage.Controls
            If TypeOf ctl Is GridControl Then Return CType(ctl, GridControl)
        Next
        Return Nothing
    End Function

    Private Function GetRowCount(ByVal grid As GridControl) As Integer
        Dim view As GridView = CType(grid.MainView, GridView)
        Return view.RowCount
    End Function

    Private Function GetSelectedQods(ByVal grid As GridControl) As QodHeaderCollection
        Dim qods As New QodHeaderCollection
        If grid.Name <> uxNonZeroGrid.Name Then
            Dim gridQods As QodHeaderCollection = CType(grid.DataSource, QodHeaderCollection)
            Dim view As GridView = CType(grid.MainView, GridView)

            If view.DataRowCount > 0 Then
                For Each rowHandle As Integer In view.GetSelectedRows
                    If rowHandle < 0 Then Continue For
                    Dim qod As QodHeader = gridQods(view.GetDataSourceRowIndex(rowHandle))
                    qods.Add(qod)
                Next
            End If
        End If
        Return qods

    End Function

    Private Function GetUnprintedPickNoteQods(ByVal grid As GridControl) As QodHeaderCollection
        Dim qods As New QodHeaderCollection
        If grid.Name <> uxNonZeroGrid.Name Then
            Dim allqods As QodHeaderCollection = CType(grid.DataSource, QodHeaderCollection)
            For Each qod As QodHeader In allqods
                If (Not qod.IsPrinted) AndAlso (Not qod.IsSuspended) AndAlso (qod.IsAwaitingDespatch OrElse qod.IsAwaitingPicking) Then qods.Add(qod)
            Next
        End If
        Return qods
    End Function

    Private Function GetGroupRowCount(ByVal view As GridView) As Integer
        For index As Integer = 0 To Integer.MinValue Step -1
            If Not view.IsValidRowHandle(index) Then Return ((index + 1) * -1)
        Next
        Return 0
    End Function

    Private Sub ResetFullOmOrderErrorLabel() Handles uxFullOmOrderText.EditValueChanged
        uxFullOmOrderErrorLabel.Text = String.Empty
    End Sub

    Private Sub ResetFullStoreErrorLabel() Handles uxFullStoreIdText.EditValueChanged, uxFullStoreOrderText.EditValueChanged
        uxFullStoreIdErrorLabel.Text = String.Empty
        uxFullStoreOrderErrorLabel.Text = String.Empty
    End Sub

    Private Sub uxMaintainButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxMaintainButton.Click

        Dim grid As GridControl = GetFocusedGrid()
        Dim view As GridView = CType(grid.MainView, GridView)
        Dim _qods As QodHeaderCollection = CType(grid.DataSource, QodHeaderCollection)
        Dim _qod As QodHeader = _qods(view.GetDataSourceRowIndex(view.FocusedRowHandle))

        Trace.WriteLine("About to Cloned Data.")
        Dim _tempqod As QodHeader = _qod.Clone
        Trace.WriteLine("Cloned Data.")
        Using form As New OrderForm(_tempqod)
            If form.ShowDialog = DialogResult.OK Then
                _qods(view.GetDataSourceRowIndex(view.FocusedRowHandle)) = _tempqod
                _tempqod.SelfPersistTree()
                grid.RefreshDataSource()
                view.CollapseMasterRow(view.FocusedRowHandle)
                view.ExpandMasterRow(view.FocusedRowHandle)
                SetButtonStatus()
            End If
        End Using
        'End If

    End Sub

    Private Sub uxConfirmOrderButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxConfirmOrderButton.Click

        'Removed functionality as per refarral 312 - Charles McMahon
        'Dim grid As GridControl = GetFocusedGrid()
        'For Each qod As QodHeader In GetSelectedQods(grid)
        '    qod.Lines.DeliveryStatus(_storeId) = Delivery.ReceiptNotifyCreate
        '    qod.Persist()
        'Next
        'grid.RefreshDataSource()
        'SetButtonStatus()

    End Sub

    ''' <summary>
    ''' Controls what happens when the print despatch button or hotkey (F5) is pressed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <history>
    ''' <updated author="Charles McMahon" date="07/10/2010"></updated>
    ''' </history>
    ''' <remarks>
    ''' Creates a QodHeaderActionFilter to filter Qods that are not ready to be printed.  Then calls the ActionConfirmationForm to allow
    ''' the user to de-select Qods for printing or select Qods for re-printing as required.
    ''' </remarks>
    Private Sub uxPrintDespatchButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxPrintDespatchButton.Click

        Dim grid As GridControl = GetFocusedGrid()
        Dim qods As QodHeaderCollection = GetSelectedQods(grid)
        Dim printCount As Integer = -2147483648  'Set this to the lowest value so we do not show a number of prints dialog when not needed

        'First check the database to see if anything else (till, webservice, etc) has updated the data while we've been making tea!
        Dim ConfigXml As XmlNode = ConfigXMLDoc.SelectSingleNode("Configuration/DataCheck/PrintPickNote")
        For Each Header As QodHeader In qods
            If Header.HasChanged(ConfigXml) Then
                MessageBox.Show(My.Resources.Strings.DataChanged, "Data has changed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                uxRetrieveButton.PerformClick()
                Exit Sub
            End If
        Next

        'Check to see if more than one order is selected
        If qods.Count > 1 Then
            printCount = 0  ' Set this to 0 now as we want to show a number of prints dialog when done
        End If

        'Now we must check the action statuses and show the correct dialog, if needed
        'Firstly including Qods that have already been printed and could be printed again as well as Qods that have not yet been printed and are ready
        'CR0038(Point 2) MO'C 19/07/2011: This has been changed to allow reprinting of a pick note at failed delivery Status (800-899)
        'Dim QodHeaderActionFilterFull As New QodHeaderActionFilter(qods, Delivery.IbtOutAllStock, Delivery.PickingStatusOk, _storeId.ToString)
        Dim QodHeaderActionFilterFull As New QodHeaderActionFilter(qods, Delivery.IbtOutAllStock, Delivery.UndeliveredStatusOk, _storeId.ToString)

        'Secondly including Qods that have not yet been printed and are ready to be printed
        Dim QodHeaderActionFilter As New QodHeaderActionFilter(qods, Delivery.IbtOutAllStock, Delivery.ReceiptStatusOk, _storeId.ToString)
        'Check if any qods are in the non-print list or if any qods have already been printed
        If QodHeaderActionFilterFull.NegativeActionList.Count > 0 OrElse _
        (qods.Count > 1 And QodHeaderActionFilterFull.PositiveActionList.Count > QodHeaderActionFilter.PositiveActionList.Count) Then
            'Show the dialog and allow the user to select qods for printing and whether to continue or not
            Using Form As New ActionConfirmationForm(QodHeaderActionFilterFull, ActionFormBehaviour.PrintDialog)
                If Not Form.ShowDialog = DialogResult.OK Then
                    Exit Sub
                End If
            End Using
        End If

        'Now we must print the positive action QODs
        For Each qod As QodHeader In QodHeaderActionFilterFull.PositiveActionList

            'If there was only one selected initially then the rules must be applied to that one (the dialog would not have been called)
            If qods.Count = 1 Then
                If qod.IsSuspended Then Continue For
                If qod.IsPrinted AndAlso YesNoWarning(My.Resources.Strings.PickingNotePrinted) = DialogResult.No Then Continue For
            End If

            'Check the local store id
            Using store As Store = AllStores.GetStore(qod.SellingStoreId)
                If store Is Nothing Then
                    DisplayWarning(String.Format(My.Resources.Strings.NoStoreExists, qod.SellingStoreId))
                Else
                    'Print the Qods and update the flag and counters as required
                    For NoCopies As Integer = 0 To 1
                        Using despatch As New DespatchNote(qod, store, _storeId)
                            despatch.Print()
                            printCount += 1
                            If Not qod.IsPrinted Then
                                qod.IsPrinted = True
                            Else
                                qod.NumberReprints += 1
                            End If
                        End Using
                    Next

                    'Live store issue reprinting of the picking note caused a reset of the header to 500
                    'This caused an error 530, as the lines were not included in the xml for a status
                    'notification.
                    If qod.Lines.DeliveryStatus(_storeId) < Delivery.PickingStatusOk Then
                        'Update the delivery status!
                        qod.Lines.DeliveryStatus(_storeId) = Delivery.PickingListCreated
                        qod.DeliveryStatus = Delivery.PickingListCreated
                    End If
                    qod.SelfPersistTree()
                End If
            End Using
        Next

        'If this was a "print multiple" action then tell the user how many pick notes they should lift from the printer
        If printCount > -1 Then
            If printCount = 0 Then
                FriendlyMessage(My.Resources.Strings.PrintPickNotesNone)
            ElseIf printCount = 1 Then
                FriendlyMessage(My.Resources.Strings.PrintPickNotesOne)
            Else
                FriendlyMessage(String.Format(My.Resources.Strings.PrintPickNotesNumber, printCount))
            End If
        End If

        'Clear the action filter, as it is no longer needed!
        QodHeaderActionFilter = Nothing
        grid.RefreshDataSource()
        Dim view As GridView = CType(grid.MainView, GridView)
        For Each rowHandle As Integer In view.GetSelectedRows
            If rowHandle < 0 Then Continue For
            view.CollapseMasterRow(rowHandle)
            view.ExpandMasterRow(rowHandle)
        Next
        SetButtonStatus()

    End Sub

    ''' <summary>
    ''' Controls what happens when the despatch button or hotkey (F6) is pressed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <history>
    ''' <updated author="Charles McMahon" date="07/10/2010"></updated>
    ''' </history>
    ''' <remarks>
    ''' Creates a QodHeaderActionFilter to filter Qods that are not ready to be despatched.  Then calls the ActionConfirmationForm to allow
    ''' the user to de-select Qods for despatching.
    ''' </remarks>
    Private Sub uxDespatchButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxDespatchButton.Click

        Dim grid As GridControl = GetFocusedGrid()
        Dim qods As QodHeaderCollection = GetSelectedQods(grid)

        'First check the database to see if anything else (till, webservice, etc) has updated the data while we've been making tea!
        Dim ConfigXml As XmlNode = ConfigXMLDoc.SelectSingleNode("Configuration/DataCheck/DespatchOrder")
        For Each Header As QodHeader In qods
            'Ref 813 - Don't Allow Dispatch if Delivery status < 599
            If Header.DeliveryStatus < Qod.State.Delivery.PickingStatusOk Then
                MessageBox.Show("OM has not confirmed Picking. Please wait 10 minutes and try again.", "Invalid Delivery Status", MessageBoxButtons.OK)
                RetrieveOrders()
                Exit Sub
            End If

            If Header.HasChanged(ConfigXml) Then
                MessageBox.Show(My.Resources.Strings.DataChanged, "Data has changed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                uxRetrieveButton.PerformClick()
                Exit Sub
            End If
        Next

        'Now we must check the action statuses and show the correct dialog, if needed
        Dim QodHeaderActionFilter As New QodHeaderActionFilter(qods, Delivery.PickingListCreated, Delivery.PickingStatusOk, _storeId.ToString)
        Dim TempActionFilter As New QodHeaderActionFilter(qods, Delivery.UndeliveredCreated, Delivery.UndeliveredStatusOk, _storeId.ToString)
        QodHeaderActionFilter.Union(TempActionFilter)
        'Check if any qods are in the negative action list
        If QodHeaderActionFilter.NegativeActionList.Count > 0 Then
            'Show the dialog and allow the user to select whether to continue or not
            Using Form As New ActionConfirmationForm(QodHeaderActionFilter, ActionFormBehaviour.DespatchDialog)
                If Not Form.ShowDialog = DialogResult.OK Then
                    Exit Sub
                End If
            End Using
        Else
            'Check the number of qods that were selected and confirm if the user wants to continue
            If qods.Count = 1 Then
                If Not MessageBox.Show(My.Resources.Strings.YesNoDespatch, Me.AppName, MessageBoxButtons.YesNo) = DialogResult.Yes Then
                    Exit Sub
                End If
            Else
                If Not MessageBox.Show(My.Resources.Strings.YesNoDespatchMultiple, Me.AppName, MessageBoxButtons.YesNo) = DialogResult.Yes Then
                    Exit Sub
                End If
            End If
        End If

        'If we get this far then we are good to go, so set all the relevant lines to despatched!
        For Each QodHeader As QodHeader In QodHeaderActionFilter.PositiveActionList
            QodHeader.Lines.DeliveryStatus(_storeId) = Delivery.DespatchCreated
            QodHeader.DeliveryStatus = Delivery.DespatchCreated
            QodHeader.DateDespatch = Now.Date
            QodHeader.DeliveryConfirmed = True
            QodHeader.SelfPersistTree()
        Next

        'Clear the filter as it is no longer required!
        QodHeaderActionFilter = Nothing
        grid.RefreshDataSource()
        Dim view As GridView = CType(grid.MainView, GridView)
        For Each rowHandle As Integer In view.GetSelectedRows
            If rowHandle < 0 Then Continue For
            view.CollapseMasterRow(rowHandle)
            view.ExpandMasterRow(rowHandle)
        Next
        SetButtonStatus()

    End Sub

    ''' <summary>
    ''' Controls what happens when the confirm picking button or hotkey (F7) is pressed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <history>
    ''' <updated author="Charles McMahon" date="07/10/2010"></updated>
    ''' </history>
    ''' <remarks>
    ''' This is a place holder to be completed in HUBS 2.0
    ''' </remarks>
    Private Sub uxConfirmPickingButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxConfirmPickingButton.Click

        Dim grid As GridControl = GetFocusedGrid()
        Dim qods As QodHeaderCollection = GetSelectedQods(grid)
        Dim printCount As Integer = -2147483648  'Set this to the lowest value so we do not show a number of prints dialog when not needed

        'First check the database to see if anything else (till, webservice, etc) has updated the data while we've been making tea!
        'Dim ConfigXml As XmlNode = ConfigXMLDoc.SelectSingleNode("Configuration/DataCheck/ConfirmPicking")
        'For Each Header As QodHeader In qods
        '    If Header.HasChanged(ConfigXml) Then
        '        MessageBox.Show(My.Resources.Strings.DataChanged, "Data has changed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '        uxRetrieveButton.PerformClick()
        '        Exit Sub
        '    End If
        'Next

        'There should only be one QodHeader selected, but loop anyway in case that changes in the future!
        For Each QodHeader As QodHeader In qods

            If QodHeader.IsSuspended = True Then
                MessageBox.Show("Order is suspended, can't pick yet.", "Suspended Order", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                Continue For
            End If

            Trace.WriteLine("About to Clone Data.")
            Dim _tempqod As New QodHeader
            _tempqod = QodHeader.Clone()
            Trace.WriteLine("About to read from the clone.")
            If _tempqod Is Nothing Then
                Trace.WriteLine("_tempqod is nothing.")
            Else
                Trace.WriteLine("_tempqod is not nothing *(Cloned ok).")
            End If
            Trace.WriteLine("Cloned Data. (Order No: " & _tempqod.Number & ")")
            Using Form As New PickConfirmation(_tempqod)
                If Form.ShowDialog = DialogResult.OK Then
                    'Need to re-print the dispatch Note if Allowed and set the status to 700 dispatched
                    If Form._Completed Then
                        'Check the local store id
                        Using store As Store = Core.System.Store.GetStore(_tempqod.SellingStoreId)
                            If store Is Nothing Then
                                DisplayWarning(String.Format(My.Resources.Strings.NoStoreExists, _tempqod.SellingStoreId))
                            Else
                                'Print the Qods and update the flag and counters as required
                                Using despatch As New DespatchNote(_tempqod, store, _storeId)
                                    despatch.Print()
                                    printCount += 1
                                    If Not _tempqod.IsPrinted Then
                                        _tempqod.IsPrinted = True
                                    Else
                                        _tempqod.NumberReprints += 1
                                    End If
                                End Using
                            End If
                        End Using
                        'Update the Delivery Status to Dispatched awaiting Conformation
                        _tempqod.Lines.DeliveryStatus(_storeId) = Delivery.DespatchCreated
                        _tempqod.DeliveryStatus = Delivery.DespatchCreated
                        _tempqod.DateDespatch = Now.Date
                        _tempqod.DeliveryConfirmed = True
                    End If

                    'Save the existing scanned info
                    For Each line As QodLine In _tempqod.Lines
                        line.SelfPersist()
                    Next
                    _tempqod.SelfPersist()

                    'Store the new data - update the recordset
                    For Each line As QodLine In QodHeader.Lines
                        line.QuantityScanned = _tempqod.Lines(CInt(line.Number.ToString) - 1).QuantityScanned
                        line.DeliveryStatus = _tempqod.Lines(CInt(line.Number.ToString) - 1).DeliveryStatus
                    Next
                    QodHeader.DeliveryStatus = _tempqod.DeliveryStatus

                End If
            End Using
        Next

        grid.RefreshDataSource()
        Dim view As GridView = CType(grid.MainView, GridView)
        For Each rowHandle As Integer In view.GetSelectedRows
            If rowHandle < 0 Then Continue For
            view.CollapseMasterRow(rowHandle)
            view.ExpandMasterRow(rowHandle)
        Next
        SetButtonStatus()

    End Sub

    ''' <summary>
    ''' Controls what happens when the confirmed delivery button or hotkey (F8) is pressed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <history>
    ''' <updated author="Charles McMahon" date="07/10/2010"></updated>
    ''' </history>
    ''' <remarks>
    ''' Creates a QodHeaderActionFilter to filter Qods that have not been despatched.  Then calls the ActionConfirmationForm to allow
    ''' the user to select the delivery status, Confirmed or Failed, as required.  All Qods are defaulted to "Confirmed" in the delivery dialog.
    ''' </remarks>
    Private Sub uxDeliveredButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxDeliveredButton.Click

        Dim grid As GridControl = GetFocusedGrid()
        Dim qods As QodHeaderCollection = GetSelectedQods(grid)

        'Now we must check the action statuses and show the correct dialog
        Dim QodHeaderActionFilter As New QodHeaderActionFilter(qods, Delivery.DespatchCreated, Delivery.DespatchStatusOk, _storeId.ToString)
        'We should always show the dialog when more than one QOD is selected
        If qods.Count > 1 Then

            'Check Delivery Status of order first to see if we can continue with confirmation
            For Each QodHeader As QodHeader In QodHeaderActionFilter.PositiveActionList
                'Ref 813 - Don't Allow Dispatch if Delivery status < 799
                If QodHeader.DeliveryStatus < Qod.State.Delivery.DespatchStatusOk Then
                    MessageBox.Show("OM has not confirmed Despatch. Please wait 10 minutes and try again.", "Invalid Delivery Status", MessageBoxButtons.OK)
                    RetrieveOrders()
                    Exit Sub
                End If
            Next

            'Show the dialog and allow the user to select whether to continue or not
            Using Form As New ActionConfirmationForm(QodHeaderActionFilter, (ActionFormBehaviour.DeliveryDialog Or ActionFormBehaviour.DeliveryConfirm))
                If Not Form.ShowDialog = DialogResult.OK Then
                    Exit Sub
                End If
            End Using

            'If we get this far then we are good to go, so set all the relevant lines to their appropriate states!
            'First all lines the user has selected to be delivered
            For Each QodHeader As QodHeader In QodHeaderActionFilter.PositiveActionList
                QodHeader.Lines.DeliveryStatus(_storeId) = Delivery.DeliveredCreated
                QodHeader.DeliveryStatus = Delivery.DeliveredCreated
                QodHeader.SelfPersistTree()
            Next

            'Now all lines the user has selected to be undelivered
            For Each QodHeader As QodHeader In QodHeaderActionFilter.PositiveActionNegateList
                QodHeader.Lines.DeliveryStatus(_storeId) = Delivery.UndeliveredCreated
                QodHeader.DeliveryStatus = Delivery.UndeliveredCreated
                QodHeader.DeliveryConfirmed = False
                QodHeader.SelfPersistTree()
            Next

        ElseIf qods.Count = 1 Then

            Dim QodHeader As QodHeader
            QodHeader = qods(0)

            'Ref 813 - Don't Allow Dispatch if Delivery status < 799
            If QodHeader.DeliveryStatus < Qod.State.Delivery.DespatchStatusOk Then
                MessageBox.Show("OM has not confirmed Despatch. Please wait 10 minutes and try again.", "Invalid Delivery Status", MessageBoxButtons.OK)
                RetrieveOrders()
                Exit Sub
            End If

            Using Form As New ConfirmFailOrder
                Select Case Form.ShowDialog
                    Case DialogResult.Yes
                        'Confirm the delivery
                        QodHeader.Lines.DeliveryStatus(_storeId) = Delivery.DeliveredCreated
                        QodHeader.DeliveryStatus = Delivery.DeliveredCreated
                        QodHeader.SelfPersistTree()
                    Case DialogResult.No
                        'Fail the delivery
                        QodHeader.Lines.DeliveryStatus(_storeId) = Delivery.UndeliveredCreated
                        QodHeader.DeliveryStatus = Delivery.UndeliveredCreated
                        QodHeader.DeliveryConfirmed = False
                        QodHeader.SelfPersistTree()
                    Case Else
                        'Cancel the action
                        Exit Sub
                End Select
            End Using
        End If

        'Clear the filter as it is no longer required!
        QodHeaderActionFilter = Nothing
        grid.RefreshDataSource()
        Dim view As GridView = CType(grid.MainView, GridView)
        For Each rowHandle As Integer In view.GetSelectedRows
            If rowHandle < 0 Then Continue For
            view.CollapseMasterRow(rowHandle)
            view.ExpandMasterRow(rowHandle)
        Next
        SetButtonStatus()

    End Sub

    Private Sub uxPrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxPrintButton.Click

        'check which tab is open to get grid in question
        Dim grid As GridControl = GetFocusedGrid()
        If grid Is Nothing Then Exit Sub

        'get sub header if all order grid
        Dim headerSub As String = My.Resources.Strings.ReportHeaderSubOpen
        If grid.Name = uxAllOrderGrid.Name Then headerSub = String.Format(My.Resources.Strings.ReportHeaderSubDates, uxStartDate.DateTime.ToShortDateString, uxEndDate.DateTime.ToShortDateString)

        'get columns to make non visible
        Dim view As GridView = CType(grid.MainView, GridView)
        If view Is Nothing Then Exit Sub

        Dim deliveryCol As GridColumn = view.Columns(GetPropertyName(Function(f As QodHeader) f.IsForDelivery))
        Dim customerCol As GridColumn = view.Columns(GetPropertyName(Function(f As QodHeader) f.CustomerAddress))
        Dim workPhoneCol As GridColumn = view.Columns(GetPropertyName(Function(f As QodHeader) f.PhoneNumberWork))
        Dim emailCol As GridColumn = view.Columns(GetPropertyName(Function(f As QodHeader) f.CustomerEmail))
        Dim suspendCol As GridColumn = view.Columns(GetPropertyName(Function(f As QodHeader) f.IsSuspended))

        Dim deliveryVisible As Boolean = deliveryCol.Visible
        Dim customerVisible As Boolean = customerCol.Visible
        Dim workPhoneVisible As Boolean = workPhoneCol.Visible
        Dim emailVisible As Boolean = emailCol.Visible
        Dim suspendVisible As Boolean = suspendCol.Visible
        deliveryCol.Visible = False
        customerCol.Visible = False
        workPhoneCol.Visible = False
        emailCol.Visible = False
        suspendCol.Visible = False

        'load print preview
        Using print As New Printing.Print(grid, UserId, WorkstationId)
            print.PrintHeader = My.Resources.Strings.ReportHeader
            print.PrintHeaderSub = headerSub
            print.PrintLandscape = True
            print.ShowPreviewDialog()
        End Using

        'reset column visibility
        deliveryCol.Visible = deliveryVisible
        customerCol.Visible = customerVisible
        workPhoneCol.Visible = workPhoneVisible
        emailCol.Visible = emailVisible
        suspendCol.Visible = suspendVisible

    End Sub

    Private Sub uxResetButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxResetButton.Click
        uxFullOmOrderText.EditValue = Nothing
        uxFullStoreIdText.EditValue = Nothing
        uxFullStoreOrderText.EditValue = Nothing

        Dim grid As GridControl = GetFocusedGrid()
        If grid IsNot Nothing Then grid.DataSource = Nothing
        RetrieveOrders()   'CR0020 - After a reset make it re-load the grid (refresh)
        SetButtonStatus()
        CheckButtonEnableStatus()
    End Sub

    Private Sub uxExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxExitButton.Click
        FindForm.Close()
    End Sub

    Private Sub uxRePrintDespatchNote_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxRePrintDespatchNote.Click

        Dim grid As GridControl = GetFocusedGrid()
        Dim qods As QodHeaderCollection = GetSelectedQods(grid)
        Dim printCount As Integer = -2147483648  'Set this to the lowest value so we do not show a number of prints dialog when not needed

        For Each QodHeader As QodHeader In qods
            'Check the local store id
            Using store As Store = Core.System.Store.GetStore(QodHeader.SellingStoreId)
                If store Is Nothing Then
                    DisplayWarning(String.Format(My.Resources.Strings.NoStoreExists, QodHeader.SellingStoreId))
                Else
                    'Print the Qods and update the flag and counters as required
                    Using despatch As New DespatchNote(QodHeader, store, _storeId)
                        despatch.Print()
                        printCount += 1
                        If Not QodHeader.IsPrinted Then
                            QodHeader.IsPrinted = True
                        Else
                            QodHeader.NumberReprints += 1
                        End If
                    End Using

                    'Save the existing scanned info
                    For Each line As QodLine In QodHeader.Lines
                        line.SelfPersist()
                    Next
                    QodHeader.SelfPersist()

                End If
            End Using
        Next

    End Sub

    Private Sub uxOMOrderNumber_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles uxOMOrderNumber.KeyUp
        Dim rowHandle As Integer = -1
        Dim searchText As String
        Dim view As DevExpress.XtraGrid.Views.Base.ColumnView = uxOpenOrderView
        Dim col As DevExpress.XtraGrid.Columns.GridColumn = view.Columns(39)
        Dim omTextBox As TextBox = uxOMOrderNumber
        Dim integerOMNumber As Integer

        If e.KeyCode = Keys.Enter Then
            Try
                ' show an hourglass cursor
                Me.Cursor = Cursors.WaitCursor

                ' establish search value
                searchText = omTextBox.Text.Replace(" ", "") ' remove spaces

                ' populate the grid with any matching order
                Dim qods As QodHeaderCollection
                If _ConfirmPickingAvailable = True Then
                    'if ConfirmPickingAvailable is true, this is eStore
                    '   either search by text or return no QOD Headers
                    If omTextBox.TextLength > 0 Then
                        If Integer.TryParse(searchText, integerOMNumber) Then
                            qods = GetQods(integerOMNumber)
                        Else
                            Exit Try
                        End If
                    Else
                        qods = New QodHeaderCollection()
                    End If

                    Dim grid As GridControl = GetFocusedGrid()

                    If qods.Count = 1 AndAlso qods(0) Is Nothing Then
                        ' no entries found
                        omTextBox.BackColor = Color.White
                        grid.DataSource = Nothing
                        Exit Try
                    End If

                    grid.DataSource = qods

                    GridLoadQods(grid)
                    GridLoadQodLines(grid)
                    GridLoadQodTexts(grid)
                    GridLoadQodRefunds(grid)

                End If

                ' select the order in the grid, if found
                ' search for the value in specified column
                rowHandle = uxOpenOrderView.LocateByDisplayText(0, col, searchText)

                If Not rowHandle = DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
                    ' value found - set backcolor of text box to indicate success
                    omTextBox.BackColor = Color.LightGreen

                    ' select the order which was found
                    view.ClearSelection()
                    view.SelectRow(rowHandle)
                    view.FocusedRowHandle = rowHandle

                    ' enable/disable buttons according to status
                    SetButtonStatus()
                Else
                    ' value not found - reset backcolor of text box
                    omTextBox.BackColor = Color.White
                End If

            Finally
                Me.Cursor = Cursors.Default
            End Try
        End If
    End Sub

    Private Sub MainForm_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus
        If _ConfirmPickingAvailable = True Then
            uxOMOrderNumber.Focus()
        End If
    End Sub

    'Ref 777 : on "om enquiry" tab setting visibility of these buttons to false in every case, need to reset if moving to different tab
    Private Sub ConfirmPickingAvailableButtonVisibility(ByVal blnVisible As Boolean)
        If blnVisible = True Then
            uxConfirmPickingButton.Visible = True     'F6
            uxRePrintDespatchNote.Visible = True      'F7
            uxDespatchButton.Visible = False          'F7
        Else
            uxConfirmPickingButton.Visible = False    'F6
            uxRePrintDespatchNote.Visible = False     'F7
            uxDespatchButton.Visible = True           'F7
        End If

    End Sub


    Private Sub LabelControl1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LabelControl1.Click

    End Sub

    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class

