Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.Order
Imports Cts.Oasys.Core.Order.Qod
Imports Cts.Oasys.Core.Order.Qod.State
Imports Cts.Oasys.Core.System.Store

Public Class OtherDelivery

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal qod As QodHeader, ByVal sellingStore As Store, ByVal fulfillStoreId As Integer)
        InitializeComponent()

        Dim PrintLinesDifStore As New QodPrintLineList
        For Each QodLine As QodLine In qod.Lines
            If (Not QodLine.IsDeliveryChargeItem) And QodLine.DeliverySource <> fulfillStoreId.ToString("0000") And _
            QodLine.QtyToDeliver > 0 And QodLine.DeliveryStatus < Delivery.DeliveredStatusOk Then
                PrintLinesDifStore.Add(QodLine)
            End If
        Next

        'Merge lines for same SKUs and sort them for printing
        Me.DataSource = PrintLinesDifStore
        XrSkuDifStore.DataBindings.Add("Text", PrintLinesDifStore, GetPropertyName(Function(f As QodPrintLine) f.SkuNumberDisplay))
        XrDescDifStore.DataBindings.Add("Text", PrintLinesDifStore, GetPropertyName(Function(f As QodPrintLine) f.Description))
        XrQtyDifStore.DataBindings.Add("Text", PrintLinesDifStore, GetPropertyName(Function(f As QodPrintLine) f.QuantityDisplay))
        XrPriceDifStore.DataBindings.Add("Text", PrintLinesDifStore, GetPropertyName(Function(f As QodPrintLine) f.PriceDisplay))

    End Sub

End Class