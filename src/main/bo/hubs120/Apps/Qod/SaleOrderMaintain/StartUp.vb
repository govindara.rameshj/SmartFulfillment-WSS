﻿Imports Cts.Oasys.WinForm.Order.SaleOrders
Imports Cts.Oasys.WinForm

<HideModuleName()> Module StartUp

    Sub Main()
        Dim userId As Integer = 0
        Dim workstationId As Integer = CInt(My.Computer.Registry.GetValue(My.Settings.registry, "WorkstationID", 20))
        Dim securityLevel As Integer = 9
        Dim runParameters As String = String.Empty

        If My.Application.CommandLineArgs IsNot Nothing Then
            For Each s As String In My.Application.CommandLineArgs
                runParameters &= Space(1) & runParameters
            Next
        End If

        Using app As New MainForm(userId, workstationId, securityLevel, runParameters)
            Using host As New HostForm(app)
                host.WindowState = FormWindowState.Maximized
                host.ShowDialog()
            End Using
        End Using

    End Sub

End Module
