﻿Imports Till.Oasys.Core
Imports System.ComponentModel

<CLSCompliant(True)> Public Class Stock
    Inherits Oasys.Core.Base

#Region "Private Variables"
    Private _skuNumber As String
    Private _description As String
    Private _productCode As String
    Private _packSize As Integer
    Private _price As Decimal
    Private _cost As Decimal
    Private _weight As Decimal
    Private _onHandQty As Integer
    Private _onOrderQty As Integer
    Private _minimumQty As Integer
    Private _maximumQty As Integer
    Private _openReturnsQty As Integer
    Private _markdownQty As Integer
    Private _writeOffQty As Integer
    Private _receivedTodayQty As Integer
    Private _receivedTodayValue As Decimal
    Private _isNonStock As Boolean
    Private _isObsolete As Boolean
    Private _isDeleted As Boolean
    Private _isNonOrder As Boolean
    Private _isItemSingle As Boolean
    Private _dateFirstStocked As Nullable(Of Date)
    Private _dateLastReceived As Nullable(Of Date)
    Private _dateLastSoqInit As Nullable(Of Date)
    Private _datePromoStart As Nullable(Of Date)
    Private _datePromoEnd As Nullable(Of Date)
    Private _dateFirstOrder As Nullable(Of Date)
    Private _dateFinalOrder As Nullable(Of Date)
    Private _supplierNumber As String
    Private _supplierName As String
    Private _hieCategory As String
    Private _hieCategoryName As String
    Private _hieGroup As String
    Private _hieSubgroup As String
    Private _hieStyle As String
    Private _minimumPromoQty As Integer
    Private _activityToday As Boolean
    Private _isInitialised As Boolean
    Private _toForecast As Boolean
    Private _flierPeriod1 As String
    Private _flierPeriod2 As String
    Private _flierPeriod3 As String
    Private _flierPeriod4 As String
    Private _flierPeriod5 As String
    Private _flierPeriod6 As String
    Private _flierPeriod7 As String
    Private _flierPeriod8 As String
    Private _flierPeriod9 As String
    Private _flierPeriod10 As String
    Private _flierPeriod11 As String
    Private _flierPeriod12 As String
    Private _unitSales1 As Integer
    Private _unitSales2 As Integer
    Private _unitSales3 As Integer
    Private _unitSales4 As Integer
    Private _unitSales5 As Integer
    Private _unitSales6 As Integer
    Private _unitSales7 As Integer
    Private _unitSales8 As Integer
    Private _unitSales9 As Integer
    Private _unitSales10 As Integer
    Private _unitSales11 As Integer
    Private _unitSales12 As Integer
    Private _unitSales13 As Integer
    Private _unitSales14 As Integer
    Private _daysOutStock1 As Integer
    Private _daysOutStock2 As Integer
    Private _daysOutStock3 As Integer
    Private _daysOutStock4 As Integer
    Private _daysOutStock5 As Integer
    Private _daysOutStock6 As Integer
    Private _daysOutStock7 As Integer
    Private _daysOutStock8 As Integer
    Private _daysOutStock9 As Integer
    Private _daysOutStock10 As Integer
    Private _daysOutStock11 As Integer
    Private _daysOutStock12 As Integer
    Private _daysOutStock13 As Integer
    Private _daysOutStock14 As Integer
    Private _SalesBias0 As Decimal
    Private _SalesBias1 As Decimal
    Private _SalesBias2 As Decimal
    Private _SalesBias3 As Decimal
    Private _SalesBias4 As Decimal
    Private _SalesBias5 As Decimal
    Private _SalesBias6 As Decimal
    Private _demandPattern As String
    Private _periodDemand As Decimal
    Private _periodTrend As Decimal
    Private _errorForecast As Decimal
    Private _errorSmoothed As Decimal
    Private _bufferConversion As Decimal
    Private _bufferStock As Integer
    Private _orderLevel As Integer
    Private _serviceLevelOverride As Decimal
    Private _valueAnnualUsage As String
    Private _saleWeightBank As Integer
    Private _saleWeightSeason As Integer
    Private _saleWeightPromo As Integer
    Private _stockLossPerWeek As Decimal
    Private _VatCode As Integer
    Private _SalesType As String
    Private _IsTaggedItem As Boolean

    Private _orderQty As Integer
    Private _capacity As Integer
    Private _stockEans As StockEanCollection = Nothing
#End Region

#Region "Properties"
    <ColumnMapping("SkuNumber")> Public Property SkuNumber() As String
        Get
            Return _skuNumber
        End Get
        Private Set(ByVal value As String)
            _skuNumber = value
        End Set
    End Property
    <ColumnMapping("Description")> Public Property Description() As String
        Get
            Return _description
        End Get
        Private Set(ByVal value As String)
            _description = value
        End Set
    End Property
    <ColumnMapping("ProductCode")> Public Property ProductCode() As String
        Get
            Return _productCode
        End Get
        Private Set(ByVal value As String)
            _productCode = value
        End Set
    End Property
    <ColumnMapping("PackSize")> Public Property PackSize() As Integer
        Get
            Return _packSize
        End Get
        Private Set(ByVal value As Integer)
            _packSize = value
        End Set
    End Property
    <ColumnMapping("Price")> Public Property Price() As Decimal
        Get
            Return _price
        End Get
        Set(ByVal value As Decimal)
            _price = value
        End Set
    End Property
    <ColumnMapping("Cost")> Public Property Cost() As Decimal
        Get
            Return _cost
        End Get
        Private Set(ByVal value As Decimal)
            _cost = value
        End Set
    End Property
    <ColumnMapping("Weight")> Public Property Weight() As Decimal
        Get
            Return _weight
        End Get
        Private Set(ByVal value As Decimal)
            _weight = value
        End Set
    End Property
    <ColumnMapping("OnHandQty")> Public Property OnHandQty() As Integer
        Get
            Return _onHandQty
        End Get
        Private Set(ByVal value As Integer)
            _onHandQty = value
        End Set
    End Property
    <ColumnMapping("OnOrderQty")> Public Property OnOrderQty() As Integer
        Get
            Return _onOrderQty
        End Get
        Private Set(ByVal value As Integer)
            _onOrderQty = value
        End Set
    End Property
    <ColumnMapping("MinimumQty")> Public Property MinimumQty() As Integer
        Get
            Return _minimumQty
        End Get
        Private Set(ByVal value As Integer)
            _minimumQty = value
        End Set
    End Property
    <ColumnMapping("MaximumQty")> Public Property MaximumQty() As Integer
        Get
            Return _maximumQty
        End Get
        Private Set(ByVal value As Integer)
            _maximumQty = value
        End Set
    End Property
    <ColumnMapping("OpenReturnsQty")> Public Property OpenReturnsQty() As Integer
        Get
            Return _openReturnsQty
        End Get
        Private Set(ByVal value As Integer)
            _openReturnsQty = value
        End Set
    End Property
    <ColumnMapping("MarkdownQty")> Public Property MarkdownQty() As Integer
        Get
            Return _markdownQty
        End Get
        Private Set(ByVal value As Integer)
            _markdownQty = value
        End Set
    End Property
    <ColumnMapping("WriteOffQty")> Public Property WriteOffQty() As Integer
        Get
            Return _writeOffQty
        End Get
        Private Set(ByVal value As Integer)
            _writeOffQty = value
        End Set
    End Property
    <ColumnMapping("ReceivedTodayQty")> Public Property ReceivedTodayQty() As Integer
        Get
            Return _receivedTodayQty
        End Get
        Private Set(ByVal value As Integer)
            _receivedTodayQty = value
        End Set
    End Property
    <ColumnMapping("ReceivedTodayValue")> Public Property ReceivedTodayValue() As Decimal
        Get
            Return _receivedTodayValue
        End Get
        Private Set(ByVal value As Decimal)
            _receivedTodayValue = value
        End Set
    End Property
    <ColumnMapping("IsNonStock")> Public Property IsNonStock() As Boolean
        Get
            Return _isNonStock
        End Get
        Private Set(ByVal value As Boolean)
            _isNonStock = value
        End Set
    End Property
    <ColumnMapping("IsObsolete")> Public Property IsObsolete() As Boolean
        Get
            Return _isObsolete
        End Get
        Private Set(ByVal value As Boolean)
            _isObsolete = value
        End Set
    End Property
    <ColumnMapping("IsDeleted")> Public Property IsDeleted() As Boolean
        Get
            Return _isDeleted
        End Get
        Private Set(ByVal value As Boolean)
            _isDeleted = value
        End Set
    End Property
    <ColumnMapping("IsNonOrder")> Public Property IsNonOrder() As Boolean
        Get
            Return _isNonOrder
        End Get
        Private Set(ByVal value As Boolean)
            _isNonOrder = value
        End Set
    End Property
    <ColumnMapping("IsItemSingle")> Public Property IsItemSingle() As Boolean
        Get
            Return _isItemSingle
        End Get
        Private Set(ByVal value As Boolean)
            _isItemSingle = value
        End Set
    End Property
    <ColumnMapping("DateFirstStocked")> Public Property DateFirstStocked() As Nullable(Of Date)
        Get
            Return _dateFirstStocked
        End Get
        Private Set(ByVal value As Nullable(Of Date))
            _dateFirstStocked = value
        End Set
    End Property
    <ColumnMapping("DateLastReceived")> Public Property DateLastReceived() As Nullable(Of Date)
        Get
            Return _dateLastReceived
        End Get
        Private Set(ByVal value As Nullable(Of Date))
            _dateLastReceived = value
        End Set
    End Property
    <ColumnMapping("DateLastSoqInit")> Public Property DateLastSoqInit() As Nullable(Of Date)
        Get
            Return _dateLastSoqInit
        End Get
        Set(ByVal value As Nullable(Of Date))
            _dateLastSoqInit = value
        End Set
    End Property
    <ColumnMapping("DatePromoStart")> Public Property DatePromoStart() As Nullable(Of Date)
        Get
            Return _datePromoStart
        End Get
        Private Set(ByVal value As Nullable(Of Date))
            _datePromoStart = value
        End Set
    End Property
    <ColumnMapping("DatePromoEnd")> Public Property DatePromoEnd() As Nullable(Of Date)
        Get
            Return _datePromoEnd
        End Get
        Private Set(ByVal value As Nullable(Of Date))
            _datePromoEnd = value
        End Set
    End Property
    <ColumnMapping("DateFirstOrder")> Public Property DateFirstOrder() As Nullable(Of Date)
        Get
            Return _dateFirstOrder
        End Get
        Private Set(ByVal value As Nullable(Of Date))
            _dateFirstOrder = value
        End Set
    End Property
    <ColumnMapping("DateFinalOrder")> Public Property DateFinalOrder() As Nullable(Of Date)
        Get
            Return _dateFinalOrder
        End Get
        Private Set(ByVal value As Nullable(Of Date))
            _dateFinalOrder = value
        End Set
    End Property
    <ColumnMapping("SupplierNumber")> Public Property SupplierNumber() As String
        Get
            Return _supplierNumber
        End Get
        Private Set(ByVal value As String)
            _supplierNumber = value
        End Set
    End Property
    <ColumnMapping("SupplierName")> Public Property SupplierName() As String
        Get
            Return _supplierName
        End Get
        Private Set(ByVal value As String)
            _supplierName = value
        End Set
    End Property
    <ColumnMapping("HieCategory")> Public Property HieCategory() As String
        Get
            Return _hieCategory
        End Get
        Private Set(ByVal value As String)
            _hieCategory = value
        End Set
    End Property
    <ColumnMapping("HieCategoryName")> Public Property HieCategoryName() As String
        Get
            Return _hieCategoryName
        End Get
        Private Set(ByVal value As String)
            _hieCategoryName = value
        End Set
    End Property
    <ColumnMapping("HieGroup")> Public Property HieGroup() As String
        Get
            Return _hieGroup
        End Get
        Private Set(ByVal value As String)
            _hieGroup = value
        End Set
    End Property
    <ColumnMapping("HieSubgroup")> Public Property HieSubgroup() As String
        Get
            Return _hieSubgroup
        End Get
        Private Set(ByVal value As String)
            _hieSubgroup = value
        End Set
    End Property
    <ColumnMapping("HieStyle")> Public Property HieStyle() As String
        Get
            Return _hieStyle
        End Get
        Private Set(ByVal value As String)
            _hieStyle = value
        End Set
    End Property
    <ColumnMapping("MinimumPromoQty")> Public Property MinimumPromoQty() As Integer
        Get
            Return _minimumPromoQty
        End Get
        Private Set(ByVal value As Integer)
            _minimumPromoQty = value
        End Set
    End Property
    <ColumnMapping("ActivityToday")> Public Property ActivityToday() As Boolean
        Get
            Return _activityToday
        End Get
        Private Set(ByVal value As Boolean)
            _activityToday = value
        End Set
    End Property
    <ColumnMapping("IsInitialised")> Public Property IsInitialised() As Boolean
        Get
            Return _isInitialised
        End Get
        Set(ByVal value As Boolean)
            _isInitialised = value
        End Set
    End Property
    <ColumnMapping("ToForecast")> Public Property ToForecast() As Boolean
        Get
            Return _toForecast
        End Get
        Set(ByVal value As Boolean)
            _toForecast = value
        End Set
    End Property
    <ColumnMapping("FlierPeriod1")> Public Property FlierPeriod1() As String
        Get
            If _flierPeriod1 Is Nothing Then _flierPeriod1 = String.Empty
            Return _flierPeriod1
        End Get
        Set(ByVal value As String)
            _flierPeriod1 = value
        End Set
    End Property
    <ColumnMapping("FlierPeriod2")> Public Property FlierPeriod2() As String
        Get
            If _flierPeriod2 Is Nothing Then _flierPeriod2 = String.Empty
            Return _flierPeriod2
        End Get
        Set(ByVal value As String)
            _flierPeriod2 = value
        End Set
    End Property
    <ColumnMapping("FlierPeriod3")> Public Property FlierPeriod3() As String
        Get
            If _flierPeriod3 Is Nothing Then _flierPeriod3 = String.Empty
            Return _flierPeriod3
        End Get
        Set(ByVal value As String)
            _flierPeriod3 = value
        End Set
    End Property
    <ColumnMapping("FlierPeriod4")> Public Property FlierPeriod4() As String
        Get
            If _flierPeriod4 Is Nothing Then _flierPeriod4 = String.Empty
            Return _flierPeriod4
        End Get
        Set(ByVal value As String)
            _flierPeriod4 = value
        End Set
    End Property
    <ColumnMapping("FlierPeriod5")> Public Property FlierPeriod5() As String
        Get
            If _flierPeriod5 Is Nothing Then _flierPeriod5 = String.Empty
            Return _flierPeriod5
        End Get
        Private Set(ByVal value As String)
            _flierPeriod5 = value
        End Set
    End Property
    <ColumnMapping("FlierPeriod6")> Public Property FlierPeriod6() As String
        Get
            If _flierPeriod6 Is Nothing Then _flierPeriod6 = String.Empty
            Return _flierPeriod6
        End Get
        Set(ByVal value As String)
            _flierPeriod6 = value
        End Set
    End Property
    <ColumnMapping("FlierPeriod7")> Public Property FlierPeriod7() As String
        Get
            If _flierPeriod7 Is Nothing Then _flierPeriod7 = String.Empty
            Return _flierPeriod7
        End Get
        Set(ByVal value As String)
            _flierPeriod7 = value
        End Set
    End Property
    <ColumnMapping("FlierPeriod8")> Public Property FlierPeriod8() As String
        Get
            If _flierPeriod8 Is Nothing Then _flierPeriod8 = String.Empty
            Return _flierPeriod8
        End Get
        Private Set(ByVal value As String)
            _flierPeriod8 = value
        End Set
    End Property
    <ColumnMapping("FlierPeriod9")> Public Property FlierPeriod9() As String
        Get
            If _flierPeriod9 Is Nothing Then _flierPeriod9 = String.Empty
            Return _flierPeriod9
        End Get
        Set(ByVal value As String)
            _flierPeriod9 = value
        End Set
    End Property
    <ColumnMapping("FlierPeriod10")> Public Property FlierPeriod10() As String
        Get
            If _flierPeriod10 Is Nothing Then _flierPeriod10 = String.Empty
            Return _flierPeriod10
        End Get
        Set(ByVal value As String)
            _flierPeriod10 = value
        End Set
    End Property
    <ColumnMapping("FlierPeriod11")> Public Property FlierPeriod11() As String
        Get
            If _flierPeriod11 Is Nothing Then _flierPeriod11 = String.Empty
            Return _flierPeriod11
        End Get
        Set(ByVal value As String)
            _flierPeriod11 = value
        End Set
    End Property
    <ColumnMapping("FlierPeriod12")> Public Property FlierPeriod12() As String
        Get
            If _flierPeriod12 Is Nothing Then _flierPeriod12 = String.Empty
            Return _flierPeriod12
        End Get
        Set(ByVal value As String)
            _flierPeriod12 = value
        End Set
    End Property
    Public Property FlierPeriod(ByVal index As Integer) As String
        Get
            Select Case index
                Case 1 : Return FlierPeriod1
                Case 2 : Return FlierPeriod2
                Case 3 : Return FlierPeriod3
                Case 4 : Return FlierPeriod4
                Case 5 : Return FlierPeriod5
                Case 6 : Return FlierPeriod6
                Case 7 : Return FlierPeriod7
                Case 8 : Return FlierPeriod8
                Case 9 : Return FlierPeriod9
                Case 10 : Return FlierPeriod10
                Case 11 : Return FlierPeriod11
                Case 12 : Return FlierPeriod12
                Case Else : Return Nothing
            End Select
        End Get
        Set(ByVal value As String)
            Select Case index
                Case 1 : _flierPeriod1 = value
                Case 2 : _flierPeriod2 = value
                Case 3 : _flierPeriod3 = value
                Case 4 : _flierPeriod4 = value
                Case 5 : _flierPeriod5 = value
                Case 6 : _flierPeriod6 = value
                Case 7 : _flierPeriod7 = value
                Case 8 : _flierPeriod8 = value
                Case 9 : _flierPeriod9 = value
                Case 10 : _flierPeriod10 = value
                Case 11 : _flierPeriod11 = value
                Case 12 : _flierPeriod12 = value
                Case Else
            End Select
        End Set
    End Property
    <ColumnMapping("UnitSales1")> Public Property UnitSales1() As Integer
        Get
            Return _unitSales1
        End Get
        Private Set(ByVal value As Integer)
            _unitSales1 = value
        End Set
    End Property
    <ColumnMapping("UnitSales2")> Public Property UnitSales2() As Integer
        Get
            Return _unitSales2
        End Get
        Private Set(ByVal value As Integer)
            _unitSales2 = value
        End Set
    End Property
    <ColumnMapping("UnitSales3")> Public Property UnitSales3() As Integer
        Get
            Return _unitSales3
        End Get
        Private Set(ByVal value As Integer)
            _unitSales3 = value
        End Set
    End Property
    <ColumnMapping("UnitSales4")> Public Property UnitSales4() As Integer
        Get
            Return _unitSales4
        End Get
        Private Set(ByVal value As Integer)
            _unitSales4 = value
        End Set
    End Property
    <ColumnMapping("UnitSales5")> Public Property UnitSales5() As Integer
        Get
            Return _unitSales5
        End Get
        Private Set(ByVal value As Integer)
            _unitSales5 = value
        End Set
    End Property
    <ColumnMapping("UnitSales6")> Public Property UnitSales6() As Integer
        Get
            Return _unitSales6
        End Get
        Private Set(ByVal value As Integer)
            _unitSales6 = value
        End Set
    End Property
    <ColumnMapping("UnitSales7")> Public Property UnitSales7() As Integer
        Get
            Return _unitSales7
        End Get
        Private Set(ByVal value As Integer)
            _unitSales7 = value
        End Set
    End Property
    <ColumnMapping("UnitSales8")> Public Property UnitSales8() As Integer
        Get
            Return _unitSales8
        End Get
        Private Set(ByVal value As Integer)
            _unitSales8 = value
        End Set
    End Property
    <ColumnMapping("UnitSales9")> Public Property UnitSales9() As Integer
        Get
            Return _unitSales9
        End Get
        Private Set(ByVal value As Integer)
            _unitSales9 = value
        End Set
    End Property
    <ColumnMapping("UnitSales10")> Public Property UnitSales10() As Integer
        Get
            Return _unitSales10
        End Get
        Private Set(ByVal value As Integer)
            _unitSales10 = value
        End Set
    End Property
    <ColumnMapping("UnitSales11")> Public Property UnitSales11() As Integer
        Get
            Return _unitSales11
        End Get
        Private Set(ByVal value As Integer)
            _unitSales11 = value
        End Set
    End Property
    <ColumnMapping("UnitSales12")> Public Property UnitSales12() As Integer
        Get
            Return _unitSales12
        End Get
        Private Set(ByVal value As Integer)
            _unitSales12 = value
        End Set
    End Property
    <ColumnMapping("UnitSales13")> Public Property UnitSales13() As Integer
        Get
            Return _unitSales13
        End Get
        Private Set(ByVal value As Integer)
            _unitSales13 = value
        End Set
    End Property
    <ColumnMapping("UnitSales14")> Public Property UnitSales14() As Integer
        Get
            Return _unitSales14
        End Get
        Private Set(ByVal value As Integer)
            _unitSales14 = value
        End Set
    End Property
    Public ReadOnly Property UnitSales(ByVal index As Integer) As Integer
        Get
            Select Case index
                Case 1 : Return _unitSales1
                Case 2 : Return _unitSales2
                Case 3 : Return _unitSales3
                Case 4 : Return _unitSales4
                Case 5 : Return _unitSales5
                Case 6 : Return _unitSales6
                Case 7 : Return _unitSales7
                Case 8 : Return _unitSales8
                Case 9 : Return _unitSales9
                Case 10 : Return _unitSales10
                Case 11 : Return _unitSales11
                Case 12 : Return _unitSales12
                Case 13 : Return _unitSales13
                Case 14 : Return _unitSales14
                Case Else : Return Nothing
            End Select
        End Get
    End Property
    <ColumnMapping("DaysOutStock1")> Public Property DaysOutStock1() As Integer
        Get
            Return _daysOutStock1
        End Get
        Private Set(ByVal value As Integer)
            _daysOutStock1 = value
        End Set
    End Property
    <ColumnMapping("DaysOutStock2")> Public Property DaysOutStock2() As Integer
        Get
            Return _daysOutStock2
        End Get
        Private Set(ByVal value As Integer)
            _daysOutStock2 = value
        End Set
    End Property
    <ColumnMapping("DaysOutStock3")> Public Property DaysOutStock3() As Integer
        Get
            Return _daysOutStock3
        End Get
        Private Set(ByVal value As Integer)
            _daysOutStock3 = value
        End Set
    End Property
    <ColumnMapping("DaysOutStock4")> Public Property DaysOutStock4() As Integer
        Get
            Return _daysOutStock4
        End Get
        Private Set(ByVal value As Integer)
            _daysOutStock4 = value
        End Set
    End Property
    <ColumnMapping("DaysOutStock5")> Public Property DaysOutStock5() As Integer
        Get
            Return _daysOutStock5
        End Get
        Private Set(ByVal value As Integer)
            _daysOutStock5 = value
        End Set
    End Property
    <ColumnMapping("DaysOutStock6")> Public Property DaysOutStock6() As Integer
        Get
            Return _daysOutStock6
        End Get
        Private Set(ByVal value As Integer)
            _daysOutStock6 = value
        End Set
    End Property
    <ColumnMapping("DaysOutStock7")> Public Property DaysOutStock7() As Integer
        Get
            Return _daysOutStock7
        End Get
        Private Set(ByVal value As Integer)
            _daysOutStock7 = value
        End Set
    End Property
    <ColumnMapping("DaysOutStock8")> Public Property DaysOutStock8() As Integer
        Get
            Return _daysOutStock8
        End Get
        Private Set(ByVal value As Integer)
            _daysOutStock8 = value
        End Set
    End Property
    <ColumnMapping("DaysOutStock9")> Public Property DaysOutStock9() As Integer
        Get
            Return _daysOutStock9
        End Get
        Private Set(ByVal value As Integer)
            _daysOutStock9 = value
        End Set
    End Property
    <ColumnMapping("DaysOutStock10")> Public Property DaysOutStock10() As Integer
        Get
            Return _daysOutStock10
        End Get
        Private Set(ByVal value As Integer)
            _daysOutStock10 = value
        End Set
    End Property
    <ColumnMapping("DaysOutStock11")> Public Property DaysOutStock11() As Integer
        Get
            Return _daysOutStock11
        End Get
        Private Set(ByVal value As Integer)
            _daysOutStock11 = value
        End Set
    End Property
    <ColumnMapping("DaysOutStock12")> Public Property DaysOutStock12() As Integer
        Get
            Return _daysOutStock12
        End Get
        Private Set(ByVal value As Integer)
            _daysOutStock12 = value
        End Set
    End Property
    <ColumnMapping("DaysOutStock13")> Public Property DaysOutStock13() As Integer
        Get
            Return _daysOutStock13
        End Get
        Private Set(ByVal value As Integer)
            _daysOutStock13 = value
        End Set
    End Property
    <ColumnMapping("DaysOutStock14")> Public Property DaysOutStock14() As Integer
        Get
            Return _daysOutStock14
        End Get
        Private Set(ByVal value As Integer)
            _daysOutStock14 = value
        End Set
    End Property
    Public ReadOnly Property DaysOutStock(ByVal index As Integer) As Integer
        Get
            Select Case index
                Case 1 : Return _daysOutStock1
                Case 2 : Return _daysOutStock2
                Case 3 : Return _daysOutStock3
                Case 4 : Return _daysOutStock4
                Case 5 : Return _daysOutStock5
                Case 6 : Return _daysOutStock6
                Case 7 : Return _daysOutStock7
                Case 8 : Return _daysOutStock8
                Case 9 : Return _daysOutStock8
                Case 10 : Return _daysOutStock10
                Case 11 : Return _daysOutStock11
                Case 12 : Return _daysOutStock12
                Case 13 : Return _daysOutStock13
                Case 14 : Return _daysOutStock14
                Case Else : Return Nothing
            End Select
        End Get
    End Property
    <ColumnMapping("SalesBias0")> Public Property SalesBias0() As Decimal
        Get
            Return _SalesBias0
        End Get
        Private Set(ByVal value As Decimal)
            _SalesBias0 = value
        End Set
    End Property
    <ColumnMapping("SalesBias1")> Public Property SalesBias1() As Decimal
        Get
            Return _SalesBias1
        End Get
        Private Set(ByVal value As Decimal)
            _SalesBias1 = value
        End Set
    End Property
    <ColumnMapping("SalesBias2")> Public Property SalesBias2() As Decimal
        Get
            Return _SalesBias2
        End Get
        Private Set(ByVal value As Decimal)
            _SalesBias2 = value
        End Set
    End Property
    <ColumnMapping("SalesBias3")> Public Property SalesBias3() As Decimal
        Get
            Return _SalesBias3
        End Get
        Private Set(ByVal value As Decimal)
            _SalesBias3 = value
        End Set
    End Property
    <ColumnMapping("SalesBias4")> Public Property SalesBias4() As Decimal
        Get
            Return _SalesBias4
        End Get
        Private Set(ByVal value As Decimal)
            _SalesBias4 = value
        End Set
    End Property
    <ColumnMapping("SalesBias5")> Public Property SalesBias5() As Decimal
        Get
            Return _SalesBias5
        End Get
        Private Set(ByVal value As Decimal)
            _SalesBias5 = value
        End Set
    End Property
    <ColumnMapping("SalesBias6")> Public Property SalesBias6() As Decimal
        Get
            Return _SalesBias6
        End Get
        Private Set(ByVal value As Decimal)
            _SalesBias6 = value
        End Set
    End Property
    Public ReadOnly Property SalesBias(ByVal dayWeek As DayOfWeek) As Decimal
        Get
            Select Case dayWeek
                Case DayOfWeek.Sunday : Return _SalesBias0
                Case DayOfWeek.Monday : Return _SalesBias1
                Case DayOfWeek.Tuesday : Return _SalesBias2
                Case DayOfWeek.Wednesday : Return _SalesBias3
                Case DayOfWeek.Thursday : Return _SalesBias4
                Case DayOfWeek.Friday : Return _SalesBias5
                Case DayOfWeek.Saturday : Return _SalesBias6
            End Select
        End Get
    End Property
    <ColumnMapping("DemandPattern")> Public Property DemandPattern() As String
        Get
            If _demandPattern Is Nothing Then _demandPattern = String.Empty
            Return _demandPattern
        End Get
        Set(ByVal value As String)
            _demandPattern = value
        End Set
    End Property
    <ColumnMapping("PeriodDemand")> Public Property PeriodDemand() As Decimal
        Get
            Return _periodDemand
        End Get
        Set(ByVal value As Decimal)
            _periodDemand = value
        End Set
    End Property
    <ColumnMapping("PeriodTrend")> Public Property PeriodTrend() As Decimal
        Get
            Return _periodTrend
        End Get
        Set(ByVal value As Decimal)
            _periodTrend = value
        End Set
    End Property
    <ColumnMapping("ErrorForecast")> Public Property ErrorForecast() As Decimal
        Get
            Return _errorForecast
        End Get
        Set(ByVal value As Decimal)
            _errorForecast = value
        End Set
    End Property
    <ColumnMapping("ErrorSmoothed")> Public Property ErrorSmoothed() As Decimal
        Get
            Return _errorSmoothed
        End Get
        Set(ByVal value As Decimal)
            _errorSmoothed = value
        End Set
    End Property
    <ColumnMapping("BufferConversion")> Public Property BufferConversion() As Decimal
        Get
            Return _bufferConversion
        End Get
        Set(ByVal value As Decimal)
            _bufferConversion = value
        End Set
    End Property
    <ColumnMapping("BufferStock")> Public Property BufferStock() As Integer
        Get
            Return _bufferStock
        End Get
        Set(ByVal value As Integer)
            _bufferStock = value
        End Set
    End Property
    <ColumnMapping("OrderLevel")> Public Property OrderLevel() As Integer
        Get
            Return _orderLevel
        End Get
        Set(ByVal value As Integer)
            _orderLevel = value
        End Set
    End Property
    <ColumnMapping("ValueAnnualUsage")> Public Property ValueAnnualUsage() As String
        Get
            Return _valueAnnualUsage
        End Get
        Set(ByVal value As String)
            _valueAnnualUsage = value
        End Set
    End Property
    <ColumnMapping("ServiceLevelOverride")> Public Property ServiceLevelOverride() As Decimal
        Get
            Return _serviceLevelOverride
        End Get
        Set(ByVal value As Decimal)
            _serviceLevelOverride = value
        End Set
    End Property
    <ColumnMapping("SaleWeightBank")> Public Property SaleWeightBank() As Integer
        Get
            Return _saleWeightBank
        End Get
        Private Set(ByVal value As Integer)
            _saleWeightBank = value
        End Set
    End Property
    <ColumnMapping("SaleWeightSeason")> Public Property SaleWeightSeason() As Integer
        Get
            Return _saleWeightSeason
        End Get
        Private Set(ByVal value As Integer)
            _saleWeightSeason = value
        End Set
    End Property
    <ColumnMapping("SaleWeightPromo")> Public Property SaleWeightPromo() As Integer
        Get
            Return _saleWeightPromo
        End Get
        Private Set(ByVal value As Integer)
            _saleWeightPromo = value
        End Set
    End Property
    <ColumnMapping("StockLossPerWeek")> Public Property StockLossPerWeek() As Decimal
        Get
            Return _stockLossPerWeek
        End Get
        Private Set(ByVal value As Decimal)
            _stockLossPerWeek = value
        End Set
    End Property
    <ColumnMapping("VatCode")> Public Property VatCode() As Integer
        Get
            Return _VatCode
        End Get
        Private Set(ByVal value As Integer)
            _VatCode = value
        End Set
    End Property
    <ColumnMapping("SalesType")> Public Property SalesType() As String
        Get
            Return _SalesType
        End Get
        Private Set(ByVal value As String)
            _SalesType = value
        End Set
    End Property
    <ColumnMapping("IsTaggedItem")> Public Property IsTaggedItem() As Boolean
        Get
            Return _IsTaggedItem
        End Get
        Private Set(ByVal value As Boolean)
            _IsTaggedItem = value
        End Set
    End Property

    ''' <summary>
    ''' Taken from PLANGRAM.CAPACITY
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <ColumnMapping("Capacity")> Public Property Capacity() As Integer
        Get
            Return _capacity
        End Get
        Private Set(ByVal value As Integer)
            _capacity = value
        End Set
    End Property
    Public Property OrderQty() As Integer
        Get
            Return _orderQty
        End Get
        Set(ByVal value As Integer)
            _orderQty = value
        End Set
    End Property

    ''' <summary>
    ''' Returns collection of ean numbers for this stock item
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property StockEans() As StockEanCollection
        Get
            If _stockEans Is Nothing Then _stockEans = New StockEanCollection(_skuNumber)
            Return _stockEans
        End Get
    End Property

    '''' <summary>
    '''' Returns comma separated string of sale weight Ids
    '''' </summary>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Public ReadOnly Property SaleWeightIds() As String
    '    Get
    '        Return _saleWeightBank & "," & _saleWeightPromo & "," & _saleWeightSeason
    '    End Get
    'End Property
#End Region

#Region "Constructors"
    Public Sub New()
    End Sub

    Friend Sub New(ByVal dr As DataRow)
        MyBase.New(dr)
    End Sub
#End Region

    Public Overrides Function ToString() As String
        Return _skuNumber & Space(1) & _description.Trim
    End Function

    ''' <summary>
    ''' Returns minimum stock level taking promotional activity into account
    ''' </summary>
    ''' <param name="OrderDue"></param>
    ''' <param name="OrderNextDue"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function MinimumStockQty(ByVal OrderDue As Date, ByVal OrderNextDue As Date) As Integer

        Select Case True
            Case _datePromoStart Is Nothing OrElse _datePromoStart Is Nothing
                Return _minimumQty

            Case OrderDue >= _datePromoStart And OrderDue < _datePromoEnd
                Return _minimumPromoQty

            Case OrderNextDue >= _datePromoStart And OrderNextDue < _datePromoEnd
                Return _minimumPromoQty

            Case Else
                Return _minimumQty
        End Select

    End Function


    ''' <summary>
    ''' Returns stock collection for given supplier number
    ''' </summary>
    ''' <param name="supplierNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetStocksBySupplier(ByVal supplierNumber As String) As StockCollection
        Dim sc As New StockCollection
        sc.LoadSupplierStocks(supplierNumber)
        Return sc
    End Function

    ''' <summary>
    ''' Returns stock collection for given sku or ean number
    ''' </summary>
    ''' <param name="skuNumber"></param>
    ''' <param name="eanNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetStocksByNumberEan(ByVal skuNumber As String, ByVal eanNumber As String) As StockCollection
        Dim sc As New StockCollection
        sc.Load(skuNumber, eanNumber)
        Return sc
    End Function

    ''' <summary>
    ''' Returns stock object for given sku number. Returns nothing if not in database
    ''' </summary>
    ''' <param name="skuNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetStock(ByVal skuNumber As String) As Stock
        Dim dt As DataTable = DataAccess.GetStock(skuNumber)
        For Each dr As DataRow In dt.Rows
            Return New Stock(dr)
        Next
        Return Nothing
    End Function

    ''' <summary>
    ''' Returns arraylist of sku numbers of given stock items that do not exist on file
    ''' </summary>
    ''' <param name="skuNumbers"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetStockItemsNotExist(ByVal skuNumbers As ArrayList) As ArrayList

        Dim dt As DataTable = DataAccess.GetStocks(skuNumbers)
        For Each dr As DataRow In dt.Rows
            skuNumbers.Remove(dr.Item(GetPropertyName(Function(f As Stock) f.SkuNumber)))
        Next

        Return skuNumbers

    End Function

    Public Shared Function IsStockItemInDatabase(ByVal skuNumber As String) As Boolean
        Dim dt As DataTable = DataAccess.GetStock(skuNumber)
        Return dt.Rows.Count > 0
    End Function

    Public Shared Function GetStockbyEAN(ByVal eanNumber As String) As Stock
        Try
            Dim dt As DataTable = DataAccess.GetStocksByEANNumber(eanNumber)

            For Each dr As DataRow In dt.Rows
                Return New Stock(dr)
            Next
            Return Nothing

        Catch ex As Exception
            Trace.WriteLine(String.Format("GetStockbyEAN: {0}", ex.Message))
            Return Nothing

        End Try

    End Function

End Class
