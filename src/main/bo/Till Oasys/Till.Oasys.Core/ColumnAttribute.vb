﻿<AttributeUsage(AttributeTargets.Property, allowmultiple:=False)> Public Class ColumnMappingAttribute
    Inherits Attribute
    Private _columnName As String = String.Empty

    Public Sub New(ByVal name As String)
        _columnName = name
    End Sub

    Public Property ColumnName() As String
        Get
            Return _columnName
        End Get
        Set(ByVal value As String)
            _columnName = value
        End Set
    End Property

End Class

<AttributeUsage(AttributeTargets.Property, allowmultiple:=False)> Public Class ColumnNullableAttribute
    Inherits Attribute
    Private _columnNull As Boolean

    Public Sub New(ByVal isNullable As Boolean)
        _columnNull = isNullable
    End Sub

    Public Property ColumnNullable() As Boolean
        Get
            Return _columnNull
        End Get
        Set(ByVal value As Boolean)
            _columnNull = value
        End Set
    End Property

End Class