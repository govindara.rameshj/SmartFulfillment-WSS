﻿<AttributeUsage(AttributeTargets.Class, allowmultiple:=False)> Public Class TableMappingAttribute
    Inherits Attribute
    Private _tableName As String = String.Empty

    Public Sub New(ByVal tableName As String)
        _tableName = tableName
    End Sub

    Public Property TableName() As String
        Get
            Return _tableName
        End Get
        Set(ByVal value As String)
            _tableName = value
        End Set
    End Property

End Class