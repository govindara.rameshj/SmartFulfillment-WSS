﻿Imports System
Imports System.Reflection
Imports System.ComponentModel

<CLSCompliant(True)> Public MustInherit Class Base
    Implements IDisposable
    Implements IDataErrorInfo
    Implements INotifyPropertyChanged
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As Global.System.ComponentModel.PropertyChangedEventArgs) Implements Global.System.ComponentModel.INotifyPropertyChanged.PropertyChanged
    Public Event Notify(ByVal message As String)
    Private _errors As New Dictionary(Of String, String)
    Private _classState As BaseClassState
    Private _deleteState As DeleteState

    Public Sub New()
    End Sub

    Public Sub New(ByVal dr As DataRow)
        Load(dr)
    End Sub

    Protected Friend Sub Load(ByVal dr As DataRow)

        '_classInitialised = False

        For Each pi As PropertyInfo In Me.GetType.GetProperties((BindingFlags.Public Or BindingFlags.NonPublic Or BindingFlags.Instance))

            'check if any custom attributes present
            Dim mappingAttributes As Object() = pi.GetCustomAttributes(GetType(ColumnMappingAttribute), True)
            If mappingAttributes.Count > 0 Then

                'check that columnname attribute exists
                For Each mappingAttribute As Object In mappingAttributes
                    If TypeOf mappingAttribute Is ColumnMappingAttribute Then
                        Dim mappingName As String = CType(mappingAttribute, ColumnMappingAttribute).ColumnName

                        'check that column exists in datarow and not null and set property value
                        If dr.Table.Columns.Contains(mappingName) AndAlso Not IsDBNull(dr(mappingName)) Then
                            Select Case pi.PropertyType.Name
                                Case GetType(Integer).Name : pi.SetValue(Me, CInt(dr(mappingName)), Nothing)
                                Case GetType(String).Name : pi.SetValue(Me, dr(mappingName).ToString.Trim, Nothing)
                                Case Else : pi.SetValue(Me, dr(mappingName), Nothing)
                            End Select
                        End If

                        Exit For
                    End If
                Next

            End If
        Next

    End Sub


    'Public Property ClassState() As BaseClassState
    '    Get
    '        Return _classState
    '    End Get
    '    Friend Set(ByVal value As BaseClassState)
    '        _classState = value
    '    End Set
    'End Property

    Private Enum DeleteState
        None
        Modified
        Added
    End Enum


    Friend Sub SetAdded()
        _classState = BaseClassState.Added
    End Sub

    Public Sub SetDeleted()
        Select Case _classState
            Case BaseClassState.Added : _deleteState = DeleteState.Added
            Case BaseClassState.Modified : _deleteState = DeleteState.Modified
        End Select
        _classState = BaseClassState.Deleted
    End Sub

    Public Sub SetUnDeleted()
        Select Case _deleteState
            Case DeleteState.Added : _classState = BaseClassState.Added
            Case DeleteState.Modified : _classState = BaseClassState.Modified
            Case Else : _classState = BaseClassState.Unchanged
        End Select
        _deleteState = DeleteState.None
    End Sub


    Protected Sub ErrorSet(ByVal propertyName As String, ByVal message As String)
        If Not _errors.ContainsKey(propertyName) Then
            _errors.Add(propertyName, message)
        End If
    End Sub

    Protected Sub ErrorClear(ByVal propertyName As String)
        If _errors.ContainsKey(propertyName) Then
            _errors.Remove(propertyName)
        End If
    End Sub

    Protected Sub ErrorClearAll()
        _errors.Clear()
    End Sub

    Protected Function ErrorCount() As Integer
        Return _errors.Count
    End Function


#Region " IDataErrorInfo Methods"
    Public ReadOnly Property [Error]() As String Implements Global.System.ComponentModel.IDataErrorInfo.Error
        Get
            If _errors.Count > 0 Then
                Return String.Format("{0} data is invalid.", Me.ToString)
            Else
                Return Nothing
            End If
        End Get
    End Property

    Default Public ReadOnly Property Item(ByVal propertyName As String) As String Implements Global.System.ComponentModel.IDataErrorInfo.Item
        Get
            If _errors.ContainsKey(propertyName) Then
                Return _errors(propertyName).ToString
            Else
                Return Nothing
            End If
        End Get
    End Property
#End Region

#Region " IDisposable Support "
    Private disposedValue As Boolean = False

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
            End If
        End If
        Me.disposedValue = True
    End Sub
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

End Class