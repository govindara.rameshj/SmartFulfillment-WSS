Imports Till.Oasys.Core
Imports Till.Oasys.Data
Imports System.Text

Namespace System
    Namespace Store

        Public Class Store
            Inherits Oasys.Core.Base

#Region "Private Variables"
            Private _id As Integer
            Private _name As String
            Private _address1 As String
            Private _address2 As String
            Private _address3 As String
            Private _address4 As String
            Private _address5 As String
            Private _postcode As String
            Private _phoneNumber As String
            Private _faxNumber As String
            Private _manager As String
            Private _regionCode As String
            Private _countryCode As String = "UK"
            Private _isClosed As Boolean
            Private _isOpenMon As Boolean = True
            Private _isOpenTue As Boolean = True
            Private _isOpenWed As Boolean = True
            Private _isOpenThu As Boolean = True
            Private _isOpenFri As Boolean = True
            Private _isOpenSat As Boolean = True
            Private _isOpenSun As Boolean = True
            Private _isHeadOffice As Boolean = False
#End Region

#Region "Properties"
            <ColumnMapping("Id")> Public Property Id() As Integer
                Get
                    Return _id
                End Get
                Private Set(ByVal value As Integer)
                    _id = value
                End Set
            End Property
            Public ReadOnly Property Id4() As Integer
                Get
                    Dim tempId As Integer = _id
                    If tempId < 1000 Then tempId += 8000
                    Return tempId
                End Get
            End Property
            <ColumnMapping("Name")> Public Property Name() As String
                Get
                    Return _name
                End Get
                Private Set(ByVal value As String)
                    _name = value
                End Set
            End Property
            <ColumnMapping("Address1")> Public Property Address1() As String
                Get
                    Return _address1
                End Get
                Private Set(ByVal value As String)
                    _address1 = value
                End Set
            End Property
            <ColumnMapping("Address2")> Public Property Address2() As String
                Get
                    Return _address2
                End Get
                Private Set(ByVal value As String)
                    _address2 = value
                End Set
            End Property
            <ColumnMapping("Address3")> Public Property Address3() As String
                Get
                    Return _address3
                End Get
                Private Set(ByVal value As String)
                    _address3 = value
                End Set
            End Property
            <ColumnMapping("Address4")> Public Property Address4() As String
                Get
                    Return _address4
                End Get
                Private Set(ByVal value As String)
                    _address4 = value
                End Set
            End Property
            <ColumnMapping("Address5")> Public Property Address5() As String
                Get
                    Return _address5
                End Get
                Private Set(ByVal value As String)
                    _address5 = value
                End Set
            End Property
            <ColumnMapping("PostCode")> Public Property PostCode() As String
                Get
                    Return _postcode
                End Get
                Private Set(ByVal value As String)
                    _postcode = value
                End Set
            End Property
            <ColumnMapping("PhoneNumber")> Public Property PhoneNumber() As String
                Get
                    Return _phoneNumber
                End Get
                Private Set(ByVal value As String)
                    _phoneNumber = value
                End Set
            End Property
            <ColumnMapping("FaxNumber")> Public Property FaxNumber() As String
                Get
                    Return _faxNumber
                End Get
                Private Set(ByVal value As String)
                    _faxNumber = value
                End Set
            End Property
            <ColumnMapping("Manager")> Public Property Manager() As String
                Get
                    Return _manager
                End Get
                Private Set(ByVal value As String)
                    _manager = value
                End Set
            End Property
            <ColumnMapping("RegionCode")> Public Property RegionCode() As String
                Get
                    Return _regionCode
                End Get
                Private Set(ByVal value As String)
                    _regionCode = value
                End Set
            End Property
            <ColumnMapping("CountryCode")> Public Property CountryCode() As String
                Get
                    Return _countryCode
                End Get
                Private Set(ByVal value As String)
                    _countryCode = value
                End Set
            End Property
            <ColumnMapping("IsClosed")> Public Property IsClosed() As Boolean
                Get
                    Return _isClosed
                End Get
                Private Set(ByVal value As Boolean)
                    _isClosed = value
                End Set
            End Property
            <ColumnMapping("IsOpenMon")> Public Property IsOpenMon() As Boolean
                Get
                    Return _isOpenMon
                End Get
                Private Set(ByVal value As Boolean)
                    _isOpenMon = value
                End Set
            End Property
            <ColumnMapping("IsOpenTue")> Public Property IsOpenTue() As Boolean
                Get
                    Return _isOpenTue
                End Get
                Private Set(ByVal value As Boolean)
                    _isOpenTue = value
                End Set
            End Property
            <ColumnMapping("IsOpenWed")> Public Property IsOpenWed() As Boolean
                Get
                    Return _isOpenWed
                End Get
                Private Set(ByVal value As Boolean)
                    _isOpenWed = value
                End Set
            End Property
            <ColumnMapping("IsOpenThu")> Public Property IsOpenThu() As Boolean
                Get
                    Return _isOpenThu
                End Get
                Private Set(ByVal value As Boolean)
                    _isOpenThu = value
                End Set
            End Property
            <ColumnMapping("IsOpenFri")> Public Property IsOpenFri() As Boolean
                Get
                    Return _isOpenFri
                End Get
                Private Set(ByVal value As Boolean)
                    _isOpenFri = value
                End Set
            End Property
            <ColumnMapping("IsOpenSat")> Public Property IsOpenSat() As Boolean
                Get
                    Return _isOpenSat
                End Get
                Private Set(ByVal value As Boolean)
                    _isOpenSat = value
                End Set
            End Property
            <ColumnMapping("IsOpenSun")> Public Property IsOpenSun() As Boolean
                Get
                    Return _isOpenSun
                End Get
                Private Set(ByVal value As Boolean)
                    _isOpenSun = value
                End Set
            End Property
            <ColumnMapping("IsHeadOffice")> Public Property IsHeadOffice() As Boolean
                Get
                    Return _isHeadOffice
                End Get
                Private Set(ByVal value As Boolean)
                    _isHeadOffice = value
                End Set
            End Property
            Public ReadOnly Property IsUKStore() As Boolean
                Get
                    Return (_countryCode.ToUpper = "UK")
                End Get
            End Property
#End Region

            Public Sub New()
                MyBase.New()
            End Sub

            Public Sub New(ByVal dr As DataRow)
                MyBase.New(dr)
            End Sub

            Public Sub New(ByVal storeId As Integer)
                MyBase.New()
                Dim DataRow As DataRow = GetStore(storeId)
                If DataRow IsNot Nothing Then
                    MyBase.Load(DataRow)
                End If
            End Sub

            Private Function GetStore(ByVal storeId As Integer) As DataRow
                Dim StoreDetails As DataRow = Nothing
                Dim RetOpt As DataTable = Nothing
                If storeId = 0 Then
                    RetOpt = StoreGet()
                    If RetOpt.Rows.Count > 0 Then
                        storeId = CInt(RetOpt.Rows(0).Item(0))
                    End If
                End If
                Dim DataTable As DataTable = StoreGetStore(storeId)
                If DataTable.Rows.Count > 0 Then
                    StoreDetails = DataTable.Rows(0)
                Else
                    If RetOpt IsNot Nothing Then
                        StoreDetails = RetOpt.Rows(0)
                        UpdateStore(StoreDetails)
                    Else
                        StoreDetails = Nothing
                    End If
                End If
                Return StoreDetails

            End Function

            Private Sub UpdateStore(ByVal RetOpt As DataRow)
                MyBase.Load(RetOpt)
                Try
                    Using Con As New Connection
                        Using Com As New Command(Con)
                            Select Case Con.DataProvider
                                Case DataProvider.Odbc  'Only Pervasive needs updated as the SQL Stored Proc has already done this
                                    Dim sb As New StringBuilder
                                    sb.Append("Insert into STRMAS (")
                                    sb.Append("NUMB, ")
                                    sb.Append("ADD1, ")
                                    sb.Append("ADD2, ")
                                    sb.Append("ADD3, ")
                                    sb.Append("ADD4, ")
                                    sb.Append("TILD, ")
                                    sb.Append("PHON, ")
                                    sb.Append("SFAX, ")
                                    sb.Append("MANG, ")
                                    sb.Append("REGC, ")
                                    sb.Append("DELC, ")
                                    sb.Append("CountryCode) ")
                                    sb.Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ")

                                    Com.CommandText = sb.ToString
                                    Com.AddParameter("NUMB", Me.Id)
                                    Com.AddParameter("ADD1", Me.Address1)
                                    Com.AddParameter("ADD2", Me.Address2)
                                    Com.AddParameter("ADD3", Me.Address3)
                                    Com.AddParameter("ADD4", Me.Address4)
                                    Com.AddParameter("TILD", Me.Name)
                                    Com.AddParameter("PHON", Me.PhoneNumber)
                                    Com.AddParameter("SFAX", Me.FaxNumber)
                                    Com.AddParameter("MANG", Me.Manager)
                                    Com.AddParameter("REGC", Me.RegionCode)
                                    Com.AddParameter("DELC", Me.IsClosed)
                                    Com.AddParameter("CountryCode", Me.CountryCode)
                                    Com.ExecuteDataTable()
                            End Select
                        End Using
                    End Using
                Catch
                    Using Con As New Connection
                        Using Com As New Command(Con)
                            Select Case Con.DataProvider
                                Case DataProvider.Odbc  'Only Pervasive needs updated as the SQL Stored Proc has already done this
                                    Dim sb As New StringBuilder
                                    sb.Append("Insert into STRMAS (")
                                    sb.Append("NUMB, ")
                                    sb.Append("ADD1, ")
                                    sb.Append("ADD2, ")
                                    sb.Append("ADD3, ")
                                    sb.Append("ADD4, ")
                                    sb.Append("TILD, ")
                                    sb.Append("PHON, ")
                                    sb.Append("SFAX, ")
                                    sb.Append("MANG, ")
                                    sb.Append("REGC, ")
                                    sb.Append("DELC) ")
                                    sb.Append("VALUES (?,?,?,?,?,?,?,?,?,?,?) ")

                                    Com.CommandText = sb.ToString
                                    Com.AddParameter("NUMB", Me.Id)
                                    Com.AddParameter("ADD1", Me.Address1)
                                    Com.AddParameter("ADD2", Me.Address2)
                                    Com.AddParameter("ADD3", Me.Address3)
                                    Com.AddParameter("ADD4", Me.Address4)
                                    Com.AddParameter("TILD", Me.Name)
                                    Com.AddParameter("PHON", Me.PhoneNumber)
                                    Com.AddParameter("SFAX", Me.FaxNumber)
                                    Com.AddParameter("MANG", Me.Manager)
                                    Com.AddParameter("REGC", Me.RegionCode)
                                    Com.AddParameter("DELC", Me.IsClosed)
                                    Com.ExecuteDataTable()
                            End Select
                        End Using
                    End Using
                End Try
            End Sub

            Private Function StoreGet() As DataTable

                Dim RetOpt As DataTable
                Try
                    Using con As New Connection
                        Using com As New Command(con)
                            Select Case con.DataProvider
                                Case DataProvider.Odbc
                                    Dim sb As New StringBuilder
                                    'Get the data from RETOPT and add it to STRMAS
                                    sb.Append("Select TOP 1 ")
                                    sb.Append("STOR as Id, ")
                                    sb.Append("SNAM as Address1, ")
                                    sb.Append("SAD1 as Address2, ")
                                    sb.Append("SAD2 as Address3, ")
                                    sb.Append("SAD3 as Address4, ")
                                    sb.Append("SNAM as Name, ")
                                    sb.Append("CTEL as PhoneNumber, ")
                                    sb.Append("CountryCode ")
                                    sb.Append("from RETOPT")
                                    com.CommandText = sb.ToString
                                    RetOpt = com.ExecuteDataTable
                                Case DataProvider.Sql
                                    'Get the data from RETOPT and add it to Store/STRMAS
                                    com.StoredProcedureName = My.Resources.Procedures.LocalStoreDetailsGet
                                    com.AddParameter(My.Resources.Parameters.Id, Id)
                                    RetOpt = com.ExecuteDataTable
                                Case Else
                                    RetOpt = Nothing
                            End Select
                        End Using
                    End Using
                Catch
                    Using con As New Connection
                        Using com As New Command(con)
                            Select Case con.DataProvider
                                Case DataProvider.Odbc
                                    Dim sb As New StringBuilder
                                    'Get the data from RETOPT and add it to STRMAS
                                    sb.Append("Select TOP 1 ")
                                    sb.Append("STOR as Id, ")
                                    sb.Append("SNAM as Address1, ")
                                    sb.Append("SAD1 as Address2, ")
                                    sb.Append("SAD2 as Address3, ")
                                    sb.Append("SAD3 as Address4, ")
                                    sb.Append("SNAM as Name, ")
                                    sb.Append("CTEL as PhoneNumber ")
                                    sb.Append("from RETOPT")
                                    com.CommandText = sb.ToString
                                    RetOpt = com.ExecuteDataTable
                                Case DataProvider.Sql
                                    'Get the data from RETOPT and add it to Store/STRMAS
                                    com.StoredProcedureName = My.Resources.Procedures.LocalStoreDetailsGet
                                    com.AddParameter(My.Resources.Parameters.Id, Id)
                                    RetOpt = com.ExecuteDataTable
                                Case Else
                                    RetOpt = Nothing
                            End Select
                        End Using
                    End Using
                End Try
                Return RetOpt

            End Function

            Private Function StoreGetStore(ByVal storeId As Integer) As DataTable

                Dim DataTable As DataTable = Nothing
                If storeId > 0 Then
                    Try
                        Using con As New Connection
                            Using com As New Command(con)
                                Select Case con.DataProvider
                                    Case DataProvider.Odbc
                                        Dim sb As New StringBuilder
                                        sb.Append("Select ")
                                        sb.Append("NUMB as Id, ")
                                        sb.Append("ADD1 as Address1, ")
                                        sb.Append("ADD2 as Address2, ")
                                        sb.Append("ADD3 as Address3, ")
                                        sb.Append("ADD4 as Address4, ")
                                        sb.Append("TILD as Name, ")
                                        sb.Append("PHON as PhoneNumber, ")
                                        sb.Append("SFAX as FaxNumber, ")
                                        sb.Append("MANG as Manager, ")
                                        sb.Append("REGC as RegionCode, ")
                                        sb.Append("DELC as IsClosed, ")
                                        sb.Append("CountryCode ")
                                        sb.Append("from STRMAS where NUMB=?")

                                        com.CommandText = sb.ToString
                                        com.AddParameter("NUMB", storeId.ToString("000"))
                                        DataTable = com.ExecuteDataTable

                                    Case DataProvider.Sql
                                        com.StoredProcedureName = My.Resources.Procedures.StoreGetStore
                                        com.AddParameter(My.Resources.Parameters.Id, storeId)
                                        DataTable = com.ExecuteDataTable

                                    Case Else
                                        DataTable = Nothing
                                End Select
                            End Using
                        End Using
                    Catch
                        Using con As New Connection
                            Using com As New Command(con)
                                Select Case con.DataProvider
                                    Case DataProvider.Odbc
                                        Dim sb As New StringBuilder
                                        sb.Append("Select ")
                                        sb.Append("NUMB as Id, ")
                                        sb.Append("ADD1 as Address1, ")
                                        sb.Append("ADD2 as Address2, ")
                                        sb.Append("ADD3 as Address3, ")
                                        sb.Append("ADD4 as Address4, ")
                                        sb.Append("TILD as Name, ")
                                        sb.Append("PHON as PhoneNumber, ")
                                        sb.Append("SFAX as FaxNumber, ")
                                        sb.Append("MANG as Manager, ")
                                        sb.Append("REGC as RegionCode, ")
                                        sb.Append("DELC as IsClosed ")
                                        sb.Append("from STRMAS where NUMB=?")

                                        com.CommandText = sb.ToString
                                        com.AddParameter("NUMB", storeId.ToString("000"))
                                        DataTable = com.ExecuteDataTable

                                    Case DataProvider.Sql
                                        com.StoredProcedureName = My.Resources.Procedures.StoreGetStore
                                        com.AddParameter(My.Resources.Parameters.Id, storeId)
                                        DataTable = com.ExecuteDataTable

                                    Case Else
                                        DataTable = Nothing
                                End Select
                            End Using
                        End Using
                    End Try
                End If
                Return DataTable
            End Function

        End Class

        Public Class StoreCollection
            Inherits Oasys.Core.BaseCollection(Of Store)

            Public Sub New()
                MyBase.New()
                Dim DT As DataTable = StoreGetStrMas()
                Me.Load(DT)
            End Sub

            Public Function GetStore(ByVal storeNumber As Integer) As Store
                Dim TheStore As Store = Nothing
                For Each Store As Store In Items
                    If Store.Id4 = storeNumber Then
                        TheStore = Store
                        Exit For
                    End If
                Next
                'Try
                '    TheStore = FindStore(storeNumber, 0, Items.Count - 1)
                'Catch ex As Exception
                '    TheStore = Nothing
                'End Try
                Return TheStore
            End Function

            Private Function FindStore(ByVal storeNumber As Integer, ByVal min As Integer, ByVal max As Integer) As Store
                If min = max Then
                    If storeNumber = Item(min).Id4 Then Return Item(min) Else Return Nothing
                End If
                Dim midno As Integer = (max + min) \ 2
                Dim MidItem As Store = Item(midno)

                Select Case storeNumber
                    Case Is < MidItem.Id4
                        Return FindStore(storeNumber, min, midno - 1)
                    Case Is > MidItem.Id4
                        Return FindStore(storeNumber, midno + 1, max)
                    Case Else
                        Return MidItem
                End Select
            End Function

            Private Function StoreGetStrMas() As DataTable

                Try
                    Using con As New Connection
                        Using com As New Command(con)
                            Select Case con.DataProvider
                                Case DataProvider.Odbc
                                    Dim sb As New StringBuilder
                                    sb.Append("Select ")
                                    sb.Append("NUMB as Id, ")
                                    sb.Append("ADD1 as Address1, ")
                                    sb.Append("ADD2 as Address2, ")
                                    sb.Append("ADD3 as Address3, ")
                                    sb.Append("ADD4 as Address4, ")
                                    sb.Append("TILD as Name, ")
                                    sb.Append("PHON as PhoneNumber, ")
                                    sb.Append("SFAX as FaxNumber, ")
                                    sb.Append("MANG as Manager, ")
                                    sb.Append("REGC as RegionCode, ")
                                    sb.Append("DELC as IsClosed, ")
                                    sb.Append("CountryCode ")
                                    sb.Append("from STRMAS ORDER BY NUMB")

                                    com.CommandText = sb.ToString
                                    Return com.ExecuteDataTable

                                Case DataProvider.Sql
                                    com.StoredProcedureName = My.Resources.Procedures.GetAllStores
                                    Return com.ExecuteDataTable

                                Case Else
                                    Return Nothing
                            End Select
                        End Using
                    End Using
                Catch
                    Using con As New Connection
                        Using com As New Command(con)
                            Select Case con.DataProvider
                                Case DataProvider.Odbc
                                    Dim sb As New StringBuilder
                                    sb.Append("Select ")
                                    sb.Append("NUMB as Id, ")
                                    sb.Append("ADD1 as Address1, ")
                                    sb.Append("ADD2 as Address2, ")
                                    sb.Append("ADD3 as Address3, ")
                                    sb.Append("ADD4 as Address4, ")
                                    sb.Append("TILD as Name, ")
                                    sb.Append("PHON as PhoneNumber, ")
                                    sb.Append("SFAX as FaxNumber, ")
                                    sb.Append("MANG as Manager, ")
                                    sb.Append("REGC as RegionCode, ")
                                    sb.Append("DELC as IsClosed ")
                                    sb.Append("from STRMAS ORDER BY NUMB")

                                    com.CommandText = sb.ToString
                                    Return com.ExecuteDataTable

                                Case DataProvider.Sql
                                    com.StoredProcedureName = My.Resources.Procedures.GetAllStores
                                    Return com.ExecuteDataTable

                                Case Else
                                    Return Nothing
                            End Select
                        End Using
                    End Using
                End Try
            End Function

        End Class
    End Namespace
End Namespace