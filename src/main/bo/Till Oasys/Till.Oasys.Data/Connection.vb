﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.Odbc

Public Class Connection
    Implements IDisposable
    Private _dataprovider As DataProvider = DataProvider.None
    Private _sqlConnection As SqlConnection = Nothing
    Private _sqlTransaction As SqlTransaction = Nothing
    Private _odbcConnection As OdbcConnection = Nothing
    Private _odbcTransaction As OdbcTransaction = Nothing

    Public Sub New()
        _dataprovider = GetDataProvider()
        Select Case _dataprovider
            Case DataProvider.Odbc
                _odbcConnection = New OdbcConnection(GetConnectionString())
                _odbcConnection.Open()
            Case DataProvider.Sql
                _sqlConnection = New SqlConnection(GetConnectionString())
                _sqlConnection.Open()
        End Select
    End Sub


    Public Function NewCommand() As Command
        Dim com As New Command(Me)
        Return com
    End Function

    Public Function NewCommand(ByVal storedProcedure As String) As Command
        Dim com As New Command(Me, storedProcedure)
        Return com
    End Function

    Public ReadOnly Property DataProvider() As DataProvider
        Get
            Return _dataprovider
        End Get
    End Property


    Friend ReadOnly Property SqlConnection() As SqlConnection
        Get
            Return _sqlConnection
        End Get
    End Property

    Friend ReadOnly Property SqlTransaction() As SqlTransaction
        Get
            Return _sqlTransaction
        End Get
    End Property

    Friend ReadOnly Property OdbcConnection() As OdbcConnection
        Get
            Return _odbcConnection
        End Get
    End Property

    Friend ReadOnly Property OdbcTransaction() As OdbcTransaction
        Get
            Return _odbcTransaction
        End Get
    End Property


    Public Sub StartTransaction()
        Select Case _dataprovider
            Case DataProvider.Odbc : _odbcTransaction = _odbcConnection.BeginTransaction
            Case DataProvider.Sql : _sqlTransaction = _sqlConnection.BeginTransaction
        End Select
    End Sub

    Public Sub CommitTransaction()
        Select Case _dataprovider
            Case DataProvider.Odbc
                If _odbcTransaction IsNot Nothing Then _odbcTransaction.Commit()
                _odbcTransaction.Dispose()
            Case DataProvider.Sql
                If _sqlTransaction IsNot Nothing Then _sqlTransaction.Commit()
                _sqlTransaction.Dispose()
        End Select
    End Sub

    Public Sub RollbackTransaction()
        Select Case _dataprovider
            Case DataProvider.Odbc
                If _odbcTransaction IsNot Nothing Then _odbcTransaction.Rollback()
                _odbcTransaction.Dispose()
            Case DataProvider.Sql
                If _sqlTransaction IsNot Nothing Then _sqlTransaction.Rollback()
                _sqlTransaction.Dispose()
        End Select
    End Sub


#Region " IDisposable Support "
    Private disposedValue As Boolean = False        ' To detect redundant calls

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                If _sqlTransaction IsNot Nothing Then
                    _sqlTransaction.Dispose()
                End If

                If _sqlConnection IsNot Nothing Then
                    If _sqlConnection.State <> ConnectionState.Closed Then _sqlConnection.Close()
                    _sqlConnection.Dispose()
                End If

                If _odbcTransaction IsNot Nothing Then
                    _odbcTransaction.Dispose()
                End If

                If _odbcConnection IsNot Nothing Then
                    If _odbcConnection.State <> ConnectionState.Closed Then _odbcConnection.Close()
                    _odbcConnection.Dispose()
                End If
            End If
        End If
        Me.disposedValue = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class

