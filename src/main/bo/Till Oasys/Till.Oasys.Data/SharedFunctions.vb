﻿Imports System.Xml
Imports System.Reflection
Imports System.IO

<HideModuleName()> Public Module SharedFunctions

    Private _strConnectionStrings As String = "configuration/connectionStrings/String"
    Private _strURLs As String = "configuration/urls/string"
    Private _configPath As String = ""
    Private _connectionString As String = ""
    Private _dataProvider As DataProvider = DataProvider.None
    Private _url As String = ""
    Private _urlOrderManager As String = ""

    Private Function GetConfigPath() As String

        Try
            ' if the config path has not been established, read it from an appropriate location
            If _configPath.Length = 0 Then
                'get current location of this assembly

                Dim codeBase As String = Assembly.GetExecutingAssembly().CodeBase
                Dim uriBuilder As New UriBuilder(codeBase)
                Dim assemblyPath As String = Uri.UnescapeDataString(uriBuilder.Path)

                'Dim configFilename As String = Directory.GetParent(assemblyPath).Parent.FullName & "\Resources\Connection.xml"
                'configFilename = Directory.GetParent(assemblyPath).FullName & "\Resources\Connection.xml"
                _configPath = System.AppDomain.CurrentDomain.BaseDirectory & "Resources\Connection.xml"
                If IO.File.Exists(_configPath) Then Return _configPath

                _configPath = "C:\Program Files\CTS Retail\Oasys3\Resources\Connection.xml"
                If IO.File.Exists(_configPath) Then Return _configPath

                _configPath = "C:\Program Files\CTS\Oasys3\Resources\Connection.xml"
                If IO.File.Exists(_configPath) Then Return _configPath

                ''for development only
                _configPath = "C:\Projects\OasysHW\VB.NET2008\Staging Area\Resources\Connection.xml"
                If IO.File.Exists(_configPath) Then Return _configPath

                ''Partha Dutta - 02/08/2010
                ''Development location has moved
                'Start change
                _configPath = "C:\Projects\Dev\Back Office\Staging Area\Resources\Connection.xml"
                If IO.File.Exists(_configPath) Then Return _configPath
                _configPath = "C:\Projects\Wickes SS\Dev\Back Office\Staging Area\Resources\Connection.xml"
                If IO.File.Exists(_configPath) Then Return _configPath
                ''End change

            End If

            ' return the value
            Return _configPath

        Catch ex As Exception
            Throw New Exception(My.Resources.Messages.NoConnectionFile, ex)

        End Try

    End Function


    Public Function GetConnectionString() As String

        Try
            ' if the connection string has not been established, read it from the config file
            If _connectionString.Length = 0 Then
                Dim configFilename As String = GetConfigPath()
                Trace.WriteLine("Connection String =: " & configFilename.ToString)
                Dim doc As XmlDocument = New XmlDocument()
                Dim connectionString As String = ""
                 doc.Load(configFilename)
                'search for sql connection ONLY!!!!
                For Each node As XmlNode In doc.SelectNodes(_strConnectionStrings)
                    If node.Attributes.GetNamedItem("name").Value.ToLower = "sqlconnection" Then
                        _connectionString = node.Attributes.GetNamedItem("connectionString").Value
                        Trace.WriteLine("OLEDB Connection String =: " & _connectionString)
                        Return _connectionString 'Fix for referral 776 - Hubs 2.0 connection string issue
                    End If
                Next
                'search for odbc dsn name
                For Each node As XmlNode In doc.SelectNodes("configuration/connectionStrings/String")
                    If node.Attributes.GetNamedItem("name").Value.ToLower = "odbc" Then
                        _connectionString = node.Attributes.GetNamedItem("connectionString").Value
                        Trace.WriteLine("ODBC Connection String =: " & _connectionString)
                        Return _connectionString 'Fix for referral 776 - Hubs 2.0 connection string issue
                    End If
                Next
            End If

            ' return the value
            Trace.WriteLine("Already read Connection String =: " & _connectionString)
            Return _connectionString

        Catch ex As Exception
            Throw New Exception(My.Resources.Messages.NoConnectionString, ex)
        End Try
    End Function

    Public Function GetDataProvider() As DataProvider

        Try
            ' if the data provider has not been established, read it from the config file
            If _dataProvider = DataProvider.None Then
                Dim configFilename As String = GetConfigPath()
                Dim doc As XmlDocument = New XmlDocument()
                doc.Load(configFilename)

                'search for sql connection string
                For Each node As XmlNode In doc.SelectNodes(_strConnectionStrings)
                    If node.Attributes.GetNamedItem("name").Value.ToLower = "sqlconnection" Then
                        _dataProvider = DataProvider.Sql
                        Exit For
                    End If
                Next

                'search for odbc dsn name
                For Each node As XmlNode In doc.SelectNodes(_strConnectionStrings)
                    If node.Attributes.GetNamedItem("name").Value.ToLower = "odbc" Then
                        _dataProvider = DataProvider.Odbc
                        Exit For
                    End If
                Next
            End If

            Return _dataProvider

        Catch ex As Exception
            Throw New Exception("Error establishing data provider", ex)
        End Try
    End Function

    Public Function GetUrl(ByVal name As String) As String

        Try
            ' if the url has not been established, read it from the config file
            If _url.Length = 0 Then
                Dim configFilename As String = GetConfigPath()
                Dim doc As XmlDocument = New XmlDocument()
                doc.Load(configFilename)

                'search for url
                For Each node As XmlNode In doc.SelectNodes(_strURLs)
                    If node.Attributes.GetNamedItem("name").Value.ToLower = name.ToLower Then
                        _url = node.Attributes.GetNamedItem("url").Value
                        Exit For
                    End If
                Next
            End If

            Return _url

        Catch ex As Exception
            Throw New Exception(String.Format(My.Resources.Messages.NoUrl, name))
        End Try

    End Function

    Public Function GetUrlOrderManager() As String

        Try
            ' if the url for order manager has not been established, read it from the config file
            If _urlOrderManager.Length = 0 Then
                Dim configFilename As String = GetConfigPath()
                Dim doc As XmlDocument = New XmlDocument()
                doc.Load(configFilename)

                'search for order manager url
                For Each node As XmlNode In doc.SelectNodes(_strURLs)
                    If node.Attributes.GetNamedItem("name").Value.ToLower = "ordermanager" Then
                        _urlOrderManager = node.Attributes.GetNamedItem("url").Value
                        Exit For
                    End If
                Next

            End If

            Return _urlOrderManager

        Catch ex As Exception
            Throw New Exception(My.Resources.Messages.NoUrlOrderManager, ex)

        End Try

    End Function

End Module

Public Enum DataProvider
    None
    Sql
    Odbc
End Enum