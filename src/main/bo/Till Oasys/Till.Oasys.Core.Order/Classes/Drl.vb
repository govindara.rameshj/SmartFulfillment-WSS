﻿Imports Till.Oasys.Core
Imports System.ComponentModel
Imports System.Text

Namespace Drl

    <HideModuleName()> Public Module SharedFunctions

        Public Function GetMaintablePurchaseReceipts() As HeaderCollection
            Dim receipts As New HeaderCollection
            receipts.LoadAllMaintainable()
            Return receipts
        End Function

    End Module

    Public Class Header
        Inherits Oasys.Core.Base

#Region "Private Variables"
        Private _number As String
        Private _type As String
        Private _dateReceipt As Date
        Private _employeeId As Integer
        Private _value As Decimal
        Private _initials As String
        Private _comments As String
        Private _isProjectSalesOrder As Boolean
        Private _rtiState As String

        Private _poId As Integer
        Private _poNumber As Nullable(Of Integer)
        Private _poConsignNumber As Nullable(Of Integer)
        Private _poSupplierNumber As String
        Private _poSupplierName As String
        Private _poSupplierBbc As Boolean
        Private _poSoqNumber As Nullable(Of Integer)
        Private _poReleaseNumber As Integer
        Private _poOrderDate As Nullable(Of Date)
        Private _poDeliveryNote1 As String
        Private _poDeliveryNote2 As String
        Private _poDeliveryNote3 As String
        Private _poDeliveryNote4 As String
        Private _poDeliveryNote5 As String
        Private _poDeliveryNote6 As String
        Private _poDeliveryNote7 As String
        Private _poDeliveryNote8 As String
        Private _poDeliveryNote9 As String

        Private _ibtStoreId As String
        Private _ibtKeyedIn As String
        Private _ibtNeedsPrinting As Boolean
        Private _ibtConsignmentNumber As String
        Private _returnSupplierNumber As String
        Private _returnDate As Nullable(Of Date)
        Private _returnNumber As String

        Private _lines As LineCollection = Nothing
#End Region

#Region "Properties"
        <ColumnMapping("Number")> Public Property Number() As String
            Get
                Return _number
            End Get
            Friend Set(ByVal value As String)
                _number = value
            End Set
        End Property
        <ColumnMapping("Type")> Public Property Type() As String
            Get
                Return _type
            End Get
            Set(ByVal value As String)
                _type = value
            End Set
        End Property
        <ColumnMapping("DateReceipt")> Public Property DateReceipt() As Date
            Get
                Return _dateReceipt
            End Get
            Friend Set(ByVal value As Date)
                _dateReceipt = value
            End Set
        End Property
        <ColumnMapping("EmployeeId")> Public Property EmployeeId() As Integer
            Get
                Return _employeeId
            End Get
            Private Set(ByVal value As Integer)
                _employeeId = value
            End Set
        End Property
        <ColumnMapping("Value")> Public Property Value() As Decimal
            Get
                Return _value
            End Get
            Set(ByVal value As Decimal)
                _value = value
            End Set
        End Property
        <ColumnMapping("Initials")> Public Property Initials() As String
            Get
                Return _initials
            End Get
            Set(ByVal value As String)
                _initials = value
            End Set
        End Property
        <ColumnMapping("Comments")> Public Property Comments() As String
            Get
                Return _comments
            End Get
            Set(ByVal value As String)
                _comments = value
            End Set
        End Property
        <ColumnMapping("IsProjectSalesOrder")> Public Property IsProjectSalesOrder() As Boolean
            Get
                Return _isProjectSalesOrder
            End Get
            Set(ByVal value As Boolean)
                _isProjectSalesOrder = value
            End Set
        End Property
        <ColumnMapping("RtiState")> Public Property RtiState() As String
            Get
                Return _rtiState
            End Get
            Set(ByVal value As String)
                _rtiState = value
            End Set
        End Property

        <ColumnMapping("PoId")> Public Property PoId() As Integer
            Get
                Return _poId
            End Get
            Private Set(ByVal value As Integer)
                _poId = value
            End Set
        End Property
        <ColumnMapping("PoNumber")> Public Property PoNumber() As Nullable(Of Integer)
            Get
                Return _poNumber
            End Get
            Private Set(ByVal value As Nullable(Of Integer))
                _poNumber = value
            End Set
        End Property
        <ColumnMapping("PoConsignNumber")> Public Property PoConsignNumber() As Nullable(Of Integer)
            Get
                Return _poConsignNumber
            End Get
            Private Set(ByVal value As Nullable(Of Integer))
                _poConsignNumber = value
            End Set
        End Property
        <ColumnMapping("PoSupplierNumber")> Public Property PoSupplierNumber() As String
            Get
                Return _poSupplierNumber
            End Get
            Private Set(ByVal value As String)
                _poSupplierNumber = value
            End Set
        End Property
        <ColumnMapping("PoSupplierName")> Public Property PoSupplierName() As String
            Get
                Return _poSupplierName
            End Get
            Private Set(ByVal value As String)
                _poSupplierName = value
            End Set
        End Property
        <ColumnMapping("PoSupplierBbc")> Public Property PoSupplierBbc() As Boolean
            Get
                Return _poSupplierBbc
            End Get
            Private Set(ByVal value As Boolean)
                _poSupplierBbc = value
            End Set
        End Property
        <ColumnMapping("PoSoqNumber")> Public Property PoSoqNumber() As Nullable(Of Integer)
            Get
                Return _poSoqNumber
            End Get
            Private Set(ByVal value As Nullable(Of Integer))
                _poSoqNumber = value
            End Set
        End Property
        <ColumnMapping("PoReleaseNumber")> Public Property PoReleaseNumber() As Integer
            Get
                Return _poReleaseNumber
            End Get
            Private Set(ByVal value As Integer)
                _poReleaseNumber = value
            End Set
        End Property
        <ColumnMapping("PoOrderDate")> Public Property PoOrderDate() As Nullable(Of Date)
            Get
                Return _poOrderDate
            End Get
            Private Set(ByVal value As Nullable(Of Date))
                _poOrderDate = value
            End Set
        End Property

        <ColumnMapping("PoDeliveryNote1")> Public Property PoDeliveryNote1() As String
            Get
                If _poDeliveryNote1 Is Nothing Then _poDeliveryNote1 = String.Empty
                Return _poDeliveryNote1
            End Get
            Set(ByVal value As String)
                _poDeliveryNote1 = value
            End Set
        End Property
        <ColumnMapping("PoDeliveryNote2")> Public Property PoDeliveryNote2() As String
            Get
                If _poDeliveryNote2 Is Nothing Then _poDeliveryNote2 = String.Empty
                Return _poDeliveryNote2
            End Get
            Set(ByVal value As String)
                _poDeliveryNote2 = value
            End Set
        End Property
        <ColumnMapping("PoDeliveryNote3")> Public Property PoDeliveryNote3() As String
            Get
                If _poDeliveryNote3 Is Nothing Then _poDeliveryNote3 = String.Empty
                Return _poDeliveryNote3
            End Get
            Set(ByVal value As String)
                _poDeliveryNote3 = value
            End Set
        End Property
        <ColumnMapping("PoDeliveryNote4")> Public Property PoDeliveryNote4() As String
            Get
                If _poDeliveryNote4 Is Nothing Then _poDeliveryNote4 = String.Empty
                Return _poDeliveryNote4
            End Get
            Set(ByVal value As String)
                _poDeliveryNote4 = value
            End Set
        End Property
        <ColumnMapping("PoDeliveryNote5")> Public Property PoDeliveryNote5() As String
            Get
                If _poDeliveryNote5 Is Nothing Then _poDeliveryNote5 = String.Empty
                Return _poDeliveryNote5
            End Get
            Set(ByVal value As String)
                _poDeliveryNote5 = value
            End Set
        End Property
        <ColumnMapping("PoDeliveryNote6")> Public Property PoDeliveryNote6() As String
            Get
                If _poDeliveryNote6 Is Nothing Then _poDeliveryNote6 = String.Empty
                Return _poDeliveryNote6
            End Get
            Set(ByVal value As String)
                _poDeliveryNote6 = value
            End Set
        End Property
        <ColumnMapping("PoDeliveryNote7")> Public Property PoDeliveryNote7() As String
            Get
                If _poDeliveryNote7 Is Nothing Then _poDeliveryNote7 = String.Empty
                Return _poDeliveryNote7
            End Get
            Set(ByVal value As String)
                _poDeliveryNote7 = value
            End Set
        End Property
        <ColumnMapping("PoDeliveryNote8")> Public Property PoDeliveryNote8() As String
            Get
                If _poDeliveryNote8 Is Nothing Then _poDeliveryNote8 = String.Empty
                Return _poDeliveryNote8
            End Get
            Set(ByVal value As String)
                _poDeliveryNote8 = value
            End Set
        End Property
        <ColumnMapping("PoDeliveryNote9")> Public Property PoDeliveryNote9() As String
            Get
                If _poDeliveryNote9 Is Nothing Then _poDeliveryNote9 = String.Empty
                Return _poDeliveryNote9
            End Get
            Set(ByVal value As String)
                _poDeliveryNote9 = value
            End Set
        End Property
        Public ReadOnly Property PoDeliveryNoteString() As String
            Get
                If _poDeliveryNote1.Trim.Length = 0 Then
                    Return String.Empty
                Else
                    Dim sb As New StringBuilder(_poDeliveryNote1.Trim)
                    If _poDeliveryNote2.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote2.Trim)
                    If _poDeliveryNote3.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote3.Trim)
                    If _poDeliveryNote4.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote4.Trim)
                    If _poDeliveryNote5.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote5.Trim)
                    If _poDeliveryNote6.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote6.Trim)
                    If _poDeliveryNote7.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote7.Trim)
                    If _poDeliveryNote8.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote8.Trim)
                    If _poDeliveryNote9.Trim.Length > 0 Then sb.Append(", " & _poDeliveryNote9.Trim)
                    Return sb.ToString
                End If
            End Get
        End Property

        <ColumnMapping("IbtStoreId")> Public Property IbtStoreId() As String
            Get
                Return _ibtStoreId
            End Get
            Set(ByVal value As String)
                _ibtStoreId = value
            End Set
        End Property
        <ColumnMapping("IbtKeyedIn")> Public Property IbtKeyedIn() As String
            Get
                Return _ibtKeyedIn
            End Get
            Set(ByVal value As String)
                _ibtKeyedIn = value
            End Set
        End Property
        <ColumnMapping("IbtNeedsPrinting")> Public Property IbtNeedsPrinting() As Boolean
            Get
                Return _ibtNeedsPrinting
            End Get
            Set(ByVal value As Boolean)
                _ibtNeedsPrinting = value
            End Set
        End Property
        <ColumnMapping("IbtConsignmentNumber")> Public Property IbtConsignmentNumber() As String
            Get
                Return _ibtConsignmentNumber
            End Get
            Set(ByVal value As String)
                _ibtConsignmentNumber = value
            End Set
        End Property
        <ColumnMapping("ReturnSupplierNumber")> Public Property ReturnSupplierNumber() As String
            Get
                Return _returnSupplierNumber
            End Get
            Set(ByVal value As String)
                _returnSupplierNumber = value
            End Set
        End Property
        <ColumnMapping("ReturnDate")> Public Property ReturnDate() As Nullable(Of Date)
            Get
                Return _returnDate
            End Get
            Set(ByVal value As Nullable(Of Date))
                _returnDate = value
            End Set
        End Property
        <ColumnMapping("ReturnNumber")> Public Property ReturnNumber() As String
            Get
                Return _returnNumber
            End Get
            Set(ByVal value As String)
                _returnNumber = value
            End Set
        End Property

        Public Property Lines() As LineCollection
            Get
                If _lines Is Nothing Then LoadLines()
                Return _lines
            End Get
            Friend Set(ByVal value As LineCollection)
                _lines = value
            End Set
        End Property

#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Private Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub

        Public Sub New(ByVal order As Po.Header, ByVal userId As Integer)
            MyBase.New()
            _poNumber = order.PoNumber
            _poId = order.Id
            _poConsignNumber = order.ConsignNumber
            _poSupplierNumber = order.SupplierNumber
            _poSupplierName = order.SupplierName
            _poSupplierBbc = order.IsSupplierBbc
            _poOrderDate = order.DateCreated
            _poSoqNumber = order.SoqNumber
            _dateReceipt = Now.Date
            _employeeId = userId
        End Sub


        Public Sub ResetValues()
            _number = Nothing
            _type = Nothing
            _dateReceipt = Now.Date
            _employeeId = 0
            _value = 0
            _comments = Nothing
            _isProjectSalesOrder = False
            _rtiState = Nothing

            _poId = 0
            _poNumber = Nothing
            _poConsignNumber = Nothing
            _poSupplierNumber = Nothing
            _poSupplierName = Nothing
            _poSupplierBbc = False
            _poSoqNumber = Nothing
            _poReleaseNumber = 0
            _poOrderDate = Nothing
            _poDeliveryNote1 = Nothing
            _poDeliveryNote2 = Nothing
            _poDeliveryNote3 = Nothing
            _poDeliveryNote4 = Nothing
            _poDeliveryNote5 = Nothing
            _poDeliveryNote6 = Nothing
            _poDeliveryNote7 = Nothing
            _poDeliveryNote8 = Nothing
            _poDeliveryNote9 = Nothing

            _ibtStoreId = Nothing
            _ibtKeyedIn = Nothing
            _ibtNeedsPrinting = False
            _ibtConsignmentNumber = Nothing
            _returnSupplierNumber = Nothing
            _returnDate = Nothing
            _returnNumber = Nothing

            _lines = New LineCollection
        End Sub

        Public Sub LoadLines()
            _lines = New LineCollection
            _lines.LoadLines(_number)
        End Sub

        Public Function HasLines() As Boolean
            Return (Lines.Count > 0)
        End Function

        Public Function Persist() As Integer
            If _number Is Nothing OrElse _number.Length = 0 Then
                Return DataAccess.Insert(Me)
            Else
                Return DataAccess.Update(Me, _employeeId)
            End If
        End Function


        Public Function Update(ByVal userId As Integer) As Integer
            _value = Lines.ReceivedValue()
            Return DataAccess.Update(Me, userId)
        End Function

        Public Function Insert() As Integer
            _value = Lines.ReceivedValue()
            Return DataAccess.Insert(Me)
        End Function

        Public Function InsertIbt(ByVal includeInSales As Boolean) As Integer
            Return DataAccess.IbtInsert(Me, includeInSales)
        End Function

        Public Shared Function CreateAutoIbtIn(ByVal fulfilmentStoreId As Integer, ByVal ibtOutNumber As String, ByVal OmOrderNumber As Integer) As Header
            If fulfilmentStoreId > 8000 Then fulfilmentStoreId -= 8000

            Dim rec As New Header
            rec.Type = "1"
            rec.Value = 0
            rec.DateReceipt = Now.Date
            rec.Initials = "Auto"
            rec.Comments = "OM Reference " & OmOrderNumber.ToString("000000")
            rec.RtiState = Rti.Description.Pending
            rec.IbtStoreId = fulfilmentStoreId.ToString("000")
            rec.IbtConsignmentNumber = ibtOutNumber
            rec.Lines = New LineCollection
            rec.Lines.Clear()
            Return rec

        End Function

        Public Shared Function CreateAutoIbtOut(ByVal storeId As Integer, ByVal OmOrderNumber As Integer) As Header
            If storeId > 8000 Then storeId -= 8000

            Dim ibt As New Header
            ibt.Type = "2"
            ibt.Value = 0
            ibt.DateReceipt = Now.Date
            ibt.Initials = "Auto"
            ibt.Comments = "OM Reference " & OmOrderNumber.ToString("000000")
            ibt.RtiState = Rti.Description.Pending
            ibt.IbtStoreId = storeId.ToString("000")
            ibt.IbtConsignmentNumber = "000000"
            ibt.IbtNeedsPrinting = True
            ibt.Lines = New LineCollection
            ibt.Lines.Clear()
            Return ibt

        End Function

        Public Shared Function CreateAutoIbtOutRefundFromQod(ByVal qod As Qod.QodHeader, Optional ByVal strDeliverySource As String = "0") As Header

            Dim blnLinesAdded As Boolean = False

            Trace.WriteLine("Create Auto Ibt Out: About to Create.")
            Dim DeliveryStore As Integer = CInt(strDeliverySource)
            Dim ibt As Header = CreateAutoIbtOut(DeliveryStore, qod.OmOrderNumber)
            Trace.WriteLine("Create Auto Ibt Out: Created.")
            For Each refund As Qod.QodRefund In qod.Refunds
                If refund.SellingStoreIbtOut Is Nothing OrElse refund.SellingStoreIbtOut.Length = 0 Then
                    If CanAddLine(qod, strDeliverySource, refund.Number) = True Then
                        If refund.SellingStoreIbtOut <> "" Then
                            Continue For
                        End If
                        Dim ibtLine As Line = ibt.Lines.AddNew(refund.SkuNumber)
                        Trace.WriteLine("Create Auto Ibt Out: Added new line.")

                        Dim stockItem As Stock.Stock = Stock.Stock.GetStock(refund.SkuNumber)
                        ibtLine.ReceivedPrice = stockItem.Price
                        Trace.WriteLine("Create Auto Ibt Out: Added Price.")
                        ibtLine.IbtQty = refund.QtyCancelled
                        Trace.WriteLine("Create Auto Ibt Out: Added Qty.")
                        blnLinesAdded = True
                    End If
                End If
            Next

            Trace.WriteLine("Create Auto Ibt Out: Testing result.")
            If blnLinesAdded = False Then ibt = Nothing
            Trace.WriteLine("Create Auto Ibt Out: Finished.")
            Return ibt

        End Function

        Public Shared Function CreateAutoIbtInRefundFromQod(ByVal qod As Qod.QodHeader, ByVal ibtOutNumber As String, Optional ByVal strDeliverySource As String = "", Optional ByVal strSellingStore As String = "") As Header

            Dim blnLinesAdded As Boolean = False

            Trace.WriteLine("Create Auto Ibt In: About to Create.")
            Dim ibt As Header = CreateAutoIbtIn(CInt(strSellingStore), ibtOutNumber, qod.OmOrderNumber)
            Trace.WriteLine("Create Auto Ibt In: Created.")
            For Each line As Qod.QodLine In qod.Lines
                If CanAddLine(qod, strDeliverySource, line.Number) = True Then
                    Dim ibtLine As Line = ibt.Lines.AddNew(line.SkuNumber)
                    Trace.WriteLine("Create Auto Ibt In: Added new line.")
                    ibtLine.ReceivedPrice = line.Price
                    Trace.WriteLine("Create Auto Ibt In: Added Price.")
                    blnLinesAdded = True
                End If
            Next

            Trace.WriteLine("Create Auto Ibt Out: Testing result.")
            If blnLinesAdded = False Then ibt = Nothing
            Trace.WriteLine("Create Auto Ibt Out: Finished.")
            Return ibt

        End Function

        Private Shared Function CanAddLine(ByVal qod As Qod.QodHeader, ByVal strDeliverySource As String, ByVal LineNum As String) As Boolean

            If strDeliverySource = "" Then Return True

            For Each Line As Qod.QodLine In qod.Lines
                If Val(Line.Number) = Val(LineNum) Then
                    Trace.WriteLine("Can Add Line: Line Number Matches.")
                    If Val(strDeliverySource) = Val(Line.DeliverySource) Then
                        Trace.WriteLine("Can Add Line: Returns True.")
                        Return True
                    Else
                        Trace.WriteLine("Can Add Line: Returns False.")
                        Return False
                    End If
                End If
            Next
        End Function
    End Class

    Public Class HeaderCollection
        Inherits BaseCollection(Of Header)

        Public Sub LoadAllMaintainable()
            Dim dt As DataTable = DataAccess.GetMaintainableOrders
            Me.Load(dt)
        End Sub

    End Class

    Public Class Line
        Inherits Oasys.Core.Base

#Region "Private Variables"
        Private _receiptNumber As String
        Private _sequence As String
        Private _skuNumber As String
        Private _skuDescription As String
        Private _skuProductCode As String
        Private _skuPack As Integer
        Private _orderLineId As Integer
        Private _orderPrice As Decimal
        Private _orderQty As Integer
        Private _receivedQty As Integer
        Private _receivedPrice As Decimal
        Private _returnQty As Integer
        Private _reasonCode As String
        Private _receivedOriginal As Integer
        Private _ibtQty As Integer
        Private _rtiState As String
#End Region

#Region "Properties"
        <ColumnMapping("ReceiptNumber")> Public Property ReceiptNumber() As String
            Get
                Return _receiptNumber
            End Get
            Friend Set(ByVal value As String)
                _receiptNumber = value
            End Set
        End Property
        <ColumnMapping("Sequence")> Public Property Sequence() As String
            Get
                Return _sequence
            End Get
            Friend Set(ByVal value As String)
                _sequence = value
            End Set
        End Property
        <ColumnMapping("SkuNumber")> Public Property SkuNumber() As String
            Get
                Return _skuNumber
            End Get
            Set(ByVal value As String)
                _skuNumber = value
            End Set
        End Property
        <ColumnMapping("SkuDescription")> Public Property SkuDescription() As String
            Get
                Return _skuDescription
            End Get
            Private Set(ByVal value As String)
                _skuDescription = value
            End Set
        End Property
        <ColumnMapping("SkuProductCode")> Public Property SkuProductCode() As String
            Get
                Return _skuProductCode
            End Get
            Private Set(ByVal value As String)
                _skuProductCode = value
            End Set
        End Property
        <ColumnMapping("SkuPack")> Public Property SkuPack() As Integer
            Get
                Return _skuPack
            End Get
            Private Set(ByVal value As Integer)
                _skuPack = value
            End Set
        End Property
        <ColumnMapping("OrderLineId")> Public Property OrderLineId() As Integer
            Get
                Return _orderLineId
            End Get
            Private Set(ByVal value As Integer)
                _orderLineId = value
            End Set
        End Property
        <ColumnMapping("OrderPrice")> Public Property OrderPrice() As Decimal
            Get
                Return _orderPrice
            End Get
            Set(ByVal value As Decimal)
                _orderPrice = value
            End Set
        End Property
        <ColumnMapping("OrderQty")> Public Property OrderQty() As Integer
            Get
                Return _orderQty
            End Get
            Set(ByVal value As Integer)
                _orderQty = value
            End Set
        End Property
        <ColumnMapping("ReceivedQty")> Public Property ReceivedQty() As Integer
            Get
                Return _receivedQty
            End Get
            Set(ByVal value As Integer)
                _receivedQty = value
            End Set
        End Property
        <ColumnMapping("ReceivedPrice")> Public Property ReceivedPrice() As Decimal
            Get
                Return _receivedPrice
            End Get
            Set(ByVal value As Decimal)
                _receivedPrice = value
            End Set
        End Property
        <ColumnMapping("ReturnQty")> Public Property ReturnQty() As Integer
            Get
                Return _returnQty
            End Get
            Set(ByVal value As Integer)
                _returnQty = value
            End Set
        End Property
        <ColumnMapping("ReasonCode")> Public Property ReasonCode() As String
            Get
                Return _reasonCode
            End Get
            Set(ByVal value As String)
                _reasonCode = value
            End Set
        End Property
        <ColumnMapping("IbtQty")> Public Property IbtQty() As Integer
            Get
                Return _ibtQty
            End Get
            Set(ByVal value As Integer)
                _ibtQty = value
            End Set
        End Property
        <ColumnMapping("RtiState")> Public Property RtiState() As String
            Get
                Return _rtiState
            End Get
            Set(ByVal value As String)
                _rtiState = value
            End Set
        End Property
        Public ReadOnly Property ReceivedOriginal() As Integer
            Get
                Return _receivedOriginal
            End Get
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
            _receivedOriginal = _receivedQty
        End Sub

        Friend Sub New(ByVal orderLine As Po.Line)
            MyBase.New()
            _skuNumber = orderLine.SkuNumber
            _skuDescription = orderLine.SkuDescription
            _skuProductCode = orderLine.SkuProductCode
            _skuPack = orderLine.SkuPackSize
            _orderLineId = orderLine.Id
            _orderPrice = orderLine.Price
            _orderQty = orderLine.OrderQty
            _receivedQty = orderLine.OrderQty
        End Sub

        Public Sub SetSkuValues(ByVal description As String, ByVal productCode As String, ByVal pack As Integer, ByVal orderPrice As Decimal)
            _skuDescription = description.Trim
            _skuProductCode = productCode.Trim
            _skuPack = pack
            _orderPrice = orderPrice
        End Sub

    End Class

    Public Class LineCollection
        Inherits BaseCollection(Of Line)

        Public Sub New()
            MyBase.New()
        End Sub

        ''' <summary>
        ''' Loads receipt lines for given receipt number
        ''' </summary>
        ''' <param name="receiptNumber"></param>
        ''' <remarks></remarks>
        Public Sub LoadLines(ByVal receiptNumber As String)
            Dim dt As DataTable = DataAccess.GetLines(receiptNumber)
            Me.Load(dt)
        End Sub

        ''' <summary>
        ''' Sets rti status for all lines in collection
        ''' </summary>
        ''' <value></value>
        ''' <remarks></remarks>
        Public WriteOnly Property RtiState() As Rti.State
            Set(ByVal value As Rti.State)
                For Each line As Line In Me.Items
                    line.RtiState = Rti.Description.GetDescription(value)
                Next
            End Set
        End Property



        ''' <summary>
        ''' Creates, adds to collection and returns new order line, setting sku number and receipt number
        ''' </summary>
        ''' <param name="receiptNumber"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overloads Function AddNew(ByVal skuNumber As String, ByVal receiptNumber As String) As Line
            Dim line As Line = Me.AddNew(skuNumber)
            line.ReceiptNumber = receiptNumber
            Return line
        End Function

        ''' <summary>
        ''' Creates, adds to collection and returns new order line, setting sku number
        ''' </summary>
        ''' <param name="skuNumber"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overloads Function AddNew(ByVal skuNumber As String) As Line
            Dim line As Line = MyBase.AddNew
            line.Sequence = Me.Items.Count.ToString("0000")
            line.OrderQty = 0
            line.OrderPrice = 0
            line.ReceivedQty = 0
            line.ReceivedPrice = 0
            line.ReturnQty = 0
            line.IbtQty = 0
            line.SkuNumber = skuNumber
            line.RtiState = Rti.Description.Pending
            Return line
        End Function


        ''' <summary>
        ''' Returns whether item exists with sku number
        ''' </summary>
        ''' <param name="skuNumber"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Exists(ByVal skuNumber As String) As Boolean

            For Each line As Line In Me.Items
                If line.SkuNumber = skuNumber Then
                    Return True
                End If
            Next
            Return False

        End Function

        ''' <summary>
        ''' Returns next sequence number
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function NextSequenceNumber() As String
            Dim sequence As Integer = CInt(Me.Items.Max(Function(f As Line) f.Sequence))
            Return (sequence + 1).ToString("0000")
        End Function


        ''' <summary>
        ''' 
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function OrderedQty() As Integer

            Dim qty As Integer = 0
            For Each line As Line In Me.Items
                qty += line.OrderQty
            Next

            Return qty

        End Function

        Public Function OrderedValue() As Decimal

            Dim value As Decimal = 0
            For Each line As Line In Me.Items
                value += (line.OrderQty * line.OrderPrice)
            Next

            Return value

        End Function

        Public Function ReceivedQty() As Integer

            Dim qty As Integer = 0
            For Each line As Line In Me.Items
                qty += line.ReceivedQty
            Next

            Return qty

        End Function

        Public Function ReceivedValue() As Decimal

            Dim value As Decimal = 0
            For Each line As Line In Me.Items
                value += (line.ReceivedQty * line.ReceivedPrice)
            Next

            Return value

        End Function

        Public Function IbtQty() As Integer

            Dim qty As Integer = 0
            For Each line As Line In Me.Items
                qty += line.IbtQty
            Next
            Return qty

        End Function

        Public Function IbtValue() As Decimal

            Dim value As Decimal = 0
            For Each line As Line In Me.Items
                value += (line.IbtQty * line.ReceivedPrice)
            Next
            Return value

        End Function


        ''' <summary>
        ''' Calculates receipt order qtys and values
        ''' </summary>
        ''' <param name="orderqty"></param>
        ''' <param name="ordervalue"></param>
        ''' <remarks></remarks>
        Public Sub Totals(ByRef orderQty As Integer, ByRef orderValue As Decimal)

            orderQty = 0
            orderValue = 0

            For Each line As Line In Me.Items
                orderQty += line.OrderQty
                orderValue += (line.OrderQty * line.OrderPrice)
            Next

        End Sub

        ''' <summary>
        ''' Calculates receipt order/received qtys and values
        ''' </summary>
        ''' <param name="orderQty"></param>
        ''' <param name="ordervalue"></param>
        ''' <remarks></remarks>
        Public Sub Totals(ByRef orderQty As Integer, ByRef orderValue As Decimal, ByRef receivedQty As Integer, ByRef receivedValue As Decimal)

            orderQty = 0
            orderValue = 0
            receivedQty = 0
            receivedValue = 0

            For Each line As Line In Me.Items
                orderQty += line.OrderQty
                orderValue += (line.OrderQty * line.OrderPrice)
                receivedQty += line.ReceivedQty
                receivedValue += (line.ReceivedQty * line.ReceivedPrice)
            Next

        End Sub

    End Class

End Namespace