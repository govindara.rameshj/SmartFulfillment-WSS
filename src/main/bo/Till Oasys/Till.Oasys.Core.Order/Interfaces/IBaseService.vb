﻿Namespace WebService

    Public Interface IBaseService

        ReadOnly Property Name() As String

        Function GetRequest() As BaseServiceArgument
        Function GetResponse() As BaseServiceArgument
        Function GetResponseXml(ByVal requestXml As String) As String

    End Interface

End Namespace