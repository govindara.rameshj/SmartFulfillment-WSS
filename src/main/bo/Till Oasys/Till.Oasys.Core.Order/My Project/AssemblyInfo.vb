﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic.Strings


' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Till.Oasys.Core.Order")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Till-Retail")> 
<Assembly: AssemblyProduct("Till.Oasys.Core.Order")> 
<Assembly: AssemblyCopyright("Copyright " & Chrw(169) & " CTS-Retail 2009")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("f57959b0-b65d-4c36-9f82-80ab56dc4e7b")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.1.0")> 
<Assembly: AssemblyFileVersion("1.0.1.0")> 
