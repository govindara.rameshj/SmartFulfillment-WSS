﻿Namespace WebService

    Public Class BaseServiceFactory

        Private Shared _mFactoryMember As IBaseService

        Public Shared Function FactoryGet() As IBaseService

            If _mFactoryMember Is Nothing Then

                Return New CheckFulfimentService

            Else

                Return _mFactoryMember

            End If

        End Function

        Public Shared Sub FactorySet(ByVal obj As IBaseService)

            _mFactoryMember = obj

        End Sub

    End Class

End Namespace