﻿Imports Till.Oasys.Core.Order.Qod
Imports System.IO
Imports System.Text
Imports System.Xml.Serialization
Imports Till.Oasys.Core.Order.Qod.State
Imports System.Xml

Namespace WebService

    Public MustInherit Class BaseResponse
        Inherits BaseServiceArgument
        Public MustOverride Function IsSuccessful() As Boolean

        Public Overridable Sub SetFieldsFromRequest(ByVal request As BaseRequest)

        End Sub

    End Class

#Region "   Cts Fulfilment"

    Partial Public Class CTSFulfilmentResponse
        Inherits BaseResponse

        Public Sub New()
            Me.DateTimeStamp = New CTSFulfilmentResponseDateTimeStamp
            Me.FulfillingSiteOrderNumber = New CTSFulfilmentResponseFulfillingSiteOrderNumber
            Me.IBTOutNumber = New CTSFulfilmentResponseIBTOutNumber
            Me.SuccessFlag = New CTSFulfilmentResponseSuccessFlag
            Me.TargetFulfilmentSite = New CTSFulfilmentResponseTargetFulfilmentSite

            Me.OrderHeader = New CTSFulfilmentResponseOrderHeader
            Me.OrderHeader.SellingStoreCode = New CTSFulfilmentResponseOrderHeaderSellingStoreCode
            Me.OrderHeader.SellingStoreOrderNumber = New CTSFulfilmentResponseOrderHeaderSellingStoreOrderNumber
            Me.OrderHeader.RequiredDeliveryDate = New CTSFulfilmentResponseOrderHeaderRequiredDeliveryDate
            Me.OrderHeader.DeliveryCharge = New CTSFulfilmentResponseOrderHeaderDeliveryCharge
            Me.OrderHeader.TotalOrderValue = New CTSFulfilmentResponseOrderHeaderTotalOrderValue
            Me.OrderHeader.SaleDate = New CTSFulfilmentResponseOrderHeaderSaleDate
            Me.OrderHeader.OMOrderNumber = New CTSFulfilmentResponseOrderHeaderOMOrderNumber
            Me.OrderHeader.OrderStatus = New CTSFulfilmentResponseOrderHeaderOrderStatus
            Me.OrderHeader.CustomerAccountNo = New CTSFulfilmentResponseOrderHeaderCustomerAccountNo
            Me.OrderHeader.CustomerName = New CTSFulfilmentResponseOrderHeaderCustomerName
            Me.OrderHeader.CustomerAddressLine1 = New CTSFulfilmentResponseOrderHeaderCustomerAddressLine1
            Me.OrderHeader.CustomerAddressLine2 = New CTSFulfilmentResponseOrderHeaderCustomerAddressLine2
            Me.OrderHeader.CustomerAddressTown = New CTSFulfilmentResponseOrderHeaderCustomerAddressTown
            Me.OrderHeader.CustomerAddressLine4 = New CTSFulfilmentResponseOrderHeaderCustomerAddressLine4
            Me.OrderHeader.CustomerPostcode = New CTSFulfilmentResponseOrderHeaderCustomerPostcode
            Me.OrderHeader.DeliveryAddressLine1 = New CTSFulfilmentResponseOrderHeaderDeliveryAddressLine1
            Me.OrderHeader.DeliveryAddressLine2 = New CTSFulfilmentResponseOrderHeaderDeliveryAddressLine2
            Me.OrderHeader.DeliveryAddressTown = New CTSFulfilmentResponseOrderHeaderDeliveryAddressTown
            Me.OrderHeader.DeliveryAddressLine4 = New CTSFulfilmentResponseOrderHeaderDeliveryAddressLine4
            Me.OrderHeader.DeliveryPostcode = New CTSFulfilmentResponseOrderHeaderDeliveryPostcode
            Me.OrderHeader.ContactPhoneHome = New CTSFulfilmentResponseOrderHeaderContactPhoneHome
            Me.OrderHeader.ContactPhoneMobile = New CTSFulfilmentResponseOrderHeaderContactPhoneMobile
            Me.OrderHeader.ContactPhoneWork = New CTSFulfilmentResponseOrderHeaderContactPhoneWork
            Me.OrderHeader.ContactEmail = New CTSFulfilmentResponseOrderHeaderContactEmail
            Me.OrderHeader.ToBeDelivered = New CTSFulfilmentResponseOrderHeaderToBeDelivered
            Me.OrderHeader.DeliveryInstructions = New CTSFulfilmentResponseOrderHeaderDeliveryInstructions

        End Sub

        Public Overrides Sub SetFieldsFromRequest(ByVal request As BaseRequest)
            Try
                Dim thisRequest = CType(request, CTSFulfilmentRequest)
                Me.DateTimeStamp.Value = Now
                Me.SuccessFlag.Value = False
                Me.TargetFulfilmentSite.Value = thisRequest.TargetFulfilmentSite.Value
                Me.FulfillingSiteOrderNumber.Value = thisRequest.OrderHeader.SellingStoreOrderNumber.Value
                Me.IBTOutNumber.Value = "0"

                Me.OrderHeader.SellingStoreCode.Value = thisRequest.OrderHeader.SellingStoreCode.Value
                Me.OrderHeader.SellingStoreOrderNumber.Value = thisRequest.OrderHeader.SellingStoreOrderNumber.Value
                Me.OrderHeader.RequiredDeliveryDate.Value = thisRequest.OrderHeader.RequiredDeliveryDate.Value
                Me.OrderHeader.DeliveryCharge.Value = thisRequest.OrderHeader.DeliveryCharge.Value
                Me.OrderHeader.TotalOrderValue.Value = thisRequest.OrderHeader.TotalOrderValue.Value
                Me.OrderHeader.SaleDate.Value = thisRequest.OrderHeader.SaleDate.Value
                Me.OrderHeader.OMOrderNumber.Value = thisRequest.OrderHeader.OMOrderNumber.Value
                Me.OrderHeader.OrderStatus.Value = thisRequest.OrderHeader.OrderStatus.Value
                Me.OrderHeader.CustomerAccountNo.Value = thisRequest.OrderHeader.CustomerAccountNo.Value
                Me.OrderHeader.CustomerName.Value = thisRequest.OrderHeader.CustomerName.Value
                Me.OrderHeader.CustomerAddressLine1.Value = thisRequest.OrderHeader.CustomerAddressLine1.Value
                Me.OrderHeader.CustomerAddressLine2.Value = thisRequest.OrderHeader.CustomerAddressLine2.Value
                Me.OrderHeader.CustomerAddressTown.Value = thisRequest.OrderHeader.CustomerAddressTown.Value
                Me.OrderHeader.CustomerAddressLine4.Value = thisRequest.OrderHeader.CustomerAddressLine4.Value
                Me.OrderHeader.CustomerPostcode.Value = thisRequest.OrderHeader.CustomerPostcode.Value
                Me.OrderHeader.DeliveryAddressLine1.Value = thisRequest.OrderHeader.DeliveryAddressLine1.Value
                Me.OrderHeader.DeliveryAddressLine2.Value = thisRequest.OrderHeader.DeliveryAddressLine2.Value
                Me.OrderHeader.DeliveryAddressTown.Value = thisRequest.OrderHeader.DeliveryAddressTown.Value
                Me.OrderHeader.DeliveryAddressLine4.Value = thisRequest.OrderHeader.DeliveryAddressLine4.Value
                Me.OrderHeader.DeliveryPostcode.Value = thisRequest.OrderHeader.DeliveryPostcode.Value
                Me.OrderHeader.ContactPhoneHome.Value = thisRequest.OrderHeader.ContactPhoneHome.Value
                Me.OrderHeader.ContactPhoneMobile.Value = thisRequest.OrderHeader.ContactPhoneMobile.Value
                Me.OrderHeader.ContactPhoneWork.Value = thisRequest.OrderHeader.ContactPhoneWork.Value
                Me.OrderHeader.ContactEmail.Value = thisRequest.OrderHeader.ContactEmail.Value
                Me.OrderHeader.ToBeDelivered.Value = thisRequest.OrderHeader.ToBeDelivered.Value

                If thisRequest.OrderHeader.DeliveryInstructions.InstructionLine IsNot Nothing Then
                    Dim upper As Integer = thisRequest.OrderHeader.DeliveryInstructions.InstructionLine.Count
                    If upper > 0 Then
                        ReDim Me.OrderHeader.DeliveryInstructions.InstructionLine(upper - 1)
                        For index As Integer = 0 To upper - 1
                            Me.OrderHeader.DeliveryInstructions.InstructionLine(index) = New CTSFulfilmentResponseOrderHeaderDeliveryInstructionsInstructionLine
                            Me.OrderHeader.DeliveryInstructions.InstructionLine(index).LineNo.Value = thisRequest.OrderHeader.DeliveryInstructions.InstructionLine(index).LineNo.Value
                            Me.OrderHeader.DeliveryInstructions.InstructionLine(index).LineText.Value = thisRequest.OrderHeader.DeliveryInstructions.InstructionLine(index).LineText.Value
                        Next
                    End If
                End If

                If thisRequest.OrderLines IsNot Nothing Then
                    Dim upper As Integer = thisRequest.OrderLines.Count
                    If upper > 0 Then
                        Dim lines(upper - 1) As CTSFulfilmentResponseOrderLine
                        For index As Integer = 0 To upper - 1
                            lines(index) = New CTSFulfilmentResponseOrderLine
                            lines(index).FulfilmentSiteOrderLineNumber.Value = thisRequest.OrderLines(index).OMOrderLineNo.Value
                            lines(index).DeliveryChargeItem.Value = thisRequest.OrderLines(index).DeliveryChargeItem.Value
                            lines(index).FulfilmentSite.Value = thisRequest.OrderLines(index).FulfilmentSite.Value
                            lines(index).LineStatus.Value = thisRequest.OrderLines(index).LineStatus.Value
                            lines(index).LineValue.Value = thisRequest.OrderLines(index).LineValue.Value
                            lines(index).OMOrderLineNo.Value = thisRequest.OrderLines(index).OMOrderLineNo.Value
                            lines(index).ProductCode.Value = thisRequest.OrderLines(index).ProductCode.Value
                            lines(index).ProductDescription.Value = thisRequest.OrderLines(index).ProductDescription.Value
                            lines(index).QuantityTaken.Value = thisRequest.OrderLines(index).QuantityTaken.Value
                            lines(index).SellingStoreLineNo.Value = thisRequest.OrderLines(index).SellingStoreLineNo.Value
                            lines(index).TotalOrderQuantity.Value = thisRequest.OrderLines(index).TotalOrderQuantity.Value
                            lines(index).UOM.Value = thisRequest.OrderLines(index).UOM.Value
                            lines(index).SellingPrice.Value = thisRequest.OrderLines(index).SellingPrice.Value
                        Next

                        Me.OrderLines = lines
                    End If
                End If

            Catch ex As Exception
                Throw New InvalidFieldSettingException(ex)
            End Try
        End Sub

        Public Overrides Sub SetFieldsFromQod(ByVal qod As QodHeader)

            Me.OrderHeader.ContactEmail.Value = qod.CustomerEmail
            Me.OrderHeader.ContactPhoneHome.Value = qod.PhoneNumber
            Me.OrderHeader.ContactPhoneMobile.Value = qod.PhoneNumberMobile
            Me.OrderHeader.ContactPhoneWork.Value = qod.PhoneNumberWork
            Me.OrderHeader.CustomerAccountNo.Value = qod.VendaNumber
            Me.OrderHeader.CustomerAddressLine1.Value = qod.CustomerAddress1
            Me.OrderHeader.CustomerAddressLine2.Value = qod.CustomerAddress2
            Me.OrderHeader.CustomerAddressLine4.Value = qod.CustomerAddress4
            Me.OrderHeader.CustomerAddressTown.Value = qod.CustomerAddress3
            Me.OrderHeader.CustomerName.Value = qod.CustomerName
            Me.OrderHeader.CustomerPostcode.Value = qod.CustomerPostcode
            Me.OrderHeader.DeliveryAddressLine1.Value = qod.DeliveryAddress1
            Me.OrderHeader.DeliveryAddressLine2.Value = qod.DeliveryAddress2
            Me.OrderHeader.DeliveryAddressLine4.Value = qod.DeliveryAddress4
            Me.OrderHeader.DeliveryAddressTown.Value = qod.DeliveryAddress3
            Me.OrderHeader.DeliveryCharge.Value = qod.DeliveryCharge
            Me.OrderHeader.DeliveryPostcode.Value = qod.DeliveryPostCode
            Me.OrderHeader.OMOrderNumber.Value = CStr(qod.OmOrderNumber)
            Me.OrderHeader.OrderStatus.Value = CStr(qod.DeliveryStatus)
            Me.OrderHeader.RequiredDeliveryDate.Value = CDate(qod.DateDelivery)
            Me.OrderHeader.SaleDate.Value = qod.DateOrder
            Me.OrderHeader.SellingStoreCode.Value = CStr(qod.SellingStoreId)
            Me.OrderHeader.SellingStoreOrderNumber.Value = CStr(qod.SellingStoreOrderId)
            Me.OrderHeader.ToBeDelivered.Value = qod.IsForDelivery
            Me.OrderHeader.TotalOrderValue.Value = qod.Lines.OrderValue

            For Each line As CTSFulfilmentResponseOrderLine In Me.OrderLines
                Dim qodline As QodLine = qod.Lines.Find(line.SellingStoreLineNo.Value)
                If qodline IsNot Nothing Then
                    line.LineStatus.Value = CStr(qodline.DeliveryStatus)
                    line.QuantityTaken.Value = qodline.QtyTaken
                    line.TotalOrderQuantity.Value = qodline.QtyOrdered
                End If
            Next

        End Sub

        Public Overrides Function IsSuccessful() As Boolean
            Return Me.SuccessFlag.Value
        End Function

        Public Function IsOmOrderNumberProvided() As Boolean
            If Not StringIsSomething(Me.OrderHeader.OMOrderNumber.Value) Then
                Me.OrderHeader.OMOrderNumber.ValidationStatus = "No OM order number provided"
                Return False
            End If
            Return True
        End Function

        Public Function IsDeliveryAddressValid() As Boolean

            Dim allok As Boolean = True

            If Not StringIsSomething(Me.OrderHeader.DeliveryAddressLine1.Value) Then
                Me.OrderHeader.DeliveryAddressLine1.ValidationStatus = "No delivery address line 1 supplied"
                allok = False
            End If

            If Not StringIsSomething(Me.OrderHeader.DeliveryAddressTown.Value) Then
                Me.OrderHeader.DeliveryAddressTown.ValidationStatus = "No delivery address town supplied"
                allok = False
            End If

            Return allok

        End Function

        Public Function IsCustomerAddressValid() As Boolean

            Dim allok As Boolean = True

            If Not StringIsSomething(Me.OrderHeader.CustomerAddressLine1.Value) Then
                Me.OrderHeader.CustomerAddressLine1.ValidationStatus = "No customer address line 1 supplied"
                allok = False
            End If

            If Not StringIsSomething(Me.OrderHeader.CustomerAddressTown.Value) Then
                Me.OrderHeader.CustomerAddressTown.ValidationStatus = "No customer address town supplied"
                allok = False
            End If

            Return allok

        End Function

        Public Function IsTargetFulfilmentSiteValid(ByVal targetSite As Integer) As Boolean

            If Not StringIsSomething(Me.TargetFulfilmentSite.Value) Then
                Me.TargetFulfilmentSite.ValidationStatus = "No target site supplied"
                Return False
            End If

            If CInt(Me.TargetFulfilmentSite.Value) <> targetSite Then
                Me.TargetFulfilmentSite.ValidationStatus = "Store is not target fulfilment site"
                Return False
            End If
            Return True

        End Function

        Public Function IsSellingStoreCodeProvided() As Boolean
            If Not StringIsSomething(Me.OrderHeader.SellingStoreCode.Value) Then
                Me.OrderHeader.SellingStoreCode.ValidationStatus = "No selling store code supplied"
                Return False
            End If
            Return True
        End Function

        Public Function IsSellingStoreOrderNumberProvided() As Boolean
            If Not StringIsSomething(Me.OrderHeader.SellingStoreOrderNumber.Value) Then
                Me.OrderHeader.SellingStoreOrderNumber.ValidationStatus = "No selling store order number supplied"
                Return False
            End If
            Return True
        End Function


        Public Function AreOrderLinesPopulated() As Boolean
            If (Me.OrderLines Is Nothing OrElse Me.OrderLines.Length = 0) Then
                Me.SuccessFlag.ValidationStatus = "No order lines have been provided"
                Return False
            End If
            Return True
        End Function

        Public Function AreOrderLinesInQod(ByVal qodLines As QodLineCollection) As Boolean

            Dim allLinesInQod As Boolean = True

            For Each fulfilLine As CTSFulfilmentResponseOrderLine In Me.OrderLines
                Dim qodLine As QodLine = qodLines.Find(fulfilLine.SellingStoreLineNo.Value)
                If qodLine Is Nothing Then
                    fulfilLine.SetSellingStoreLineNoNotFound()
                    allLinesInQod = False
                End If
            Next

            Return allLinesInQod

        End Function

        Public Sub SetStatusCodeNotExpected()
            Me.OrderHeader.OrderStatus.ValidationStatus = "Status code not expected"
        End Sub

        Public Sub SetFulfilmentSiteOrderNotCreated()
            Me.FulfillingSiteOrderNumber.ValidationStatus = "QOD not created at target store"
        End Sub

        Public Sub SetQodNotFound()
            Me.OrderHeader.OMOrderNumber.ValidationStatus = "No order found for this OM order number at store"
        End Sub

        Public Sub SetQodAlreadyExists()
            Me.OrderHeader.OMOrderNumber.ValidationStatus = "Order already exists for OM order number at store"
        End Sub


    End Class

    Partial Public Class CTSFulfilmentResponseOrderHeaderDeliveryInstructionsInstructionLine
        Public Sub New()
            Me.LineNo = New CTSFulfilmentResponseOrderHeaderDeliveryInstructionsInstructionLineLineNo
            Me.LineText = New CTSFulfilmentResponseOrderHeaderDeliveryInstructionsInstructionLineLineText
        End Sub
    End Class

    Partial Public Class CTSFulfilmentResponseOrderLine

        Public Sub New()
            Me.DeliveryChargeItem = New CTSFulfilmentResponseOrderLineDeliveryChargeItem
            Me.FulfilmentSite = New CTSFulfilmentResponseOrderLineFulfilmentSite
            Me.fulfilmentSiteOrderLineNumberField = New CTSFulfilmentResponseOrderLineFulfilmentSiteOrderLineNumber
            Me.LineStatus = New CTSFulfilmentResponseOrderLineLineStatus
            Me.LineValue = New CTSFulfilmentResponseOrderLineLineValue
            Me.OMOrderLineNo = New CTSFulfilmentResponseOrderLineOMOrderLineNo
            Me.ProductCode = New CTSFulfilmentResponseOrderLineProductCode
            Me.ProductDescription = New CTSFulfilmentResponseOrderLineProductDescription
            Me.QuantityTaken = New CTSFulfilmentResponseOrderLineQuantityTaken
            Me.SellingStoreLineNo = New CTSFulfilmentResponseOrderLineSellingStoreLineNo
            Me.TotalOrderQuantity = New CTSFulfilmentResponseOrderLineTotalOrderQuantity
            Me.UOM = New CTSFulfilmentResponseOrderLineUOM
            Me.SellingPrice = New CTSFulfilmentResponseOrderLineSellingPrice
        End Sub

        Public Function IsProductCodeProvided() As Boolean
            If Not StringIsSomething(Me.ProductCode.Value) Then
                Me.ProductCode.ValidationStatus = "No product code supplied"
                Return False
            End If
            Return True
        End Function

        Public Sub SetSellingStoreLineNoNotFound()
            Me.SellingStoreLineNo.ValidationStatus = "Selling store line not found in order"
            Me.LineStatus.Value = CStr(CInt(Delivery.FulfilRequestFailedData))
        End Sub

        Public Sub SetProductCodeNotFound()
            Me.ProductCode.ValidationStatus = "Product cannot be found"
            Me.LineStatus.Value = CStr(CInt(Delivery.FulfilRequestFailedData))
        End Sub

    End Class

#End Region

#Region "   Cts Status Notification"

    Partial Public Class CTSStatusNotificationResponse
        Inherits BaseResponse

        Public Overrides Function IsSuccessful() As Boolean
            Return Me.SuccessFlag.Value
        End Function

        Public Sub New()
            Me.DateTimeStamp = New CTSStatusNotificationResponseDateTimeStamp
            Me.SuccessFlag = New CTSStatusNotificationResponseSuccessFlag
            Me.OMOrderNumber = New CTSStatusNotificationResponseOMOrderNumber
            Me.OrderStatus = New CTSStatusNotificationResponseOrderStatus

            Me.OrderHeader = New CTSStatusNotificationResponseOrderHeader
            Me.OrderHeader.SellingStoreCode = New CTSStatusNotificationResponseOrderHeaderSellingStoreCode
            Me.OrderHeader.SellingStoreOrderNumber = New CTSStatusNotificationResponseOrderHeaderSellingStoreOrderNumber

        End Sub

        Public Overrides Sub SetFieldsFromRequest(ByVal request As BaseRequest)
            Try
                Dim thisRequest As CTSStatusNotificationRequest = CType(request, CTSStatusNotificationRequest)
                Me.DateTimeStamp.Value = Now
                Me.OMOrderNumber.Value = thisRequest.OMOrderNumber.Value
                Me.OrderStatus.Value = (CInt(thisRequest.OrderStatus.Value) \ 100).ToString & "99"
                Me.SuccessFlag.Value = False
                Me.OrderHeader.SellingStoreCode.Value = thisRequest.OrderHeader.SellingStoreCode.Value
                Me.OrderHeader.SellingStoreOrderNumber.Value = thisRequest.OrderHeader.SellingStoreOrderNumber.Value

                If thisRequest.FulfilmentSites IsNot Nothing Then
                    Dim upper As Integer = thisRequest.FulfilmentSites.Count
                    If upper > 0 Then
                        ReDim Me.FulfilmentSites(upper - 1)
                        For index As Integer = 0 To upper - 1
                            Me.FulfilmentSites(index) = New CTSStatusNotificationResponseFulfilmentSite
                            Me.FulfilmentSites(index).FulfilmentSite.Value = thisRequest.FulfilmentSites(index).FulfilmentSite.Value
                            Me.FulfilmentSites(index).FulfilmentSiteOrderNumber.Value = thisRequest.FulfilmentSites(index).FulfilmentSiteOrderNumber.Value
                            Me.FulfilmentSites(index).FulfilmentSiteIBTOutNumber.Value = thisRequest.FulfilmentSites(index).FulfilmentSiteIBTOutNumber.Value.PadLeft(6, "0"c)
                            Me.FulfilmentSites(index).SellingStoreIBTInNumber.Value = "0"
                            Me.FulfilmentSites(index).FulfilmentSite.Value = thisRequest.FulfilmentSites(index).FulfilmentSite.Value

                            If thisRequest.FulfilmentSites(index).OrderLines IsNot Nothing Then
                                Dim lineUpper As Integer = thisRequest.FulfilmentSites(index).OrderLines.Count
                                If lineUpper > 0 Then
                                    ReDim Me.FulfilmentSites(index).OrderLines(lineUpper - 1)
                                    For lineIndex As Integer = 0 To lineUpper - 1
                                        Me.FulfilmentSites(index).OrderLines(lineIndex) = New CTSStatusNotificationResponseFulfilmentSiteOrderLine
                                        Me.FulfilmentSites(index).OrderLines(lineIndex).SellingStoreLineNo.Value = thisRequest.FulfilmentSites(index).OrderLines(lineIndex).SellingStoreLineNo.Value
                                        Me.FulfilmentSites(index).OrderLines(lineIndex).OMOrderLineNo.Value = thisRequest.FulfilmentSites(index).OrderLines(lineIndex).OMOrderLineNo.Value
                                        Me.FulfilmentSites(index).OrderLines(lineIndex).ProductCode.Value = thisRequest.FulfilmentSites(index).OrderLines(lineIndex).ProductCode.Value
                                        Me.FulfilmentSites(index).OrderLines(lineIndex).LineStatus.Value = _
                                            (CInt(thisRequest.FulfilmentSites(index).OrderLines(lineIndex).LineStatus.Value) \ 100).ToString & "99"
                                    Next
                                End If
                            End If

                        Next
                    End If
                End If

            Catch ex As Exception
                Throw New InvalidFieldSettingException(ex)
            End Try
        End Sub


        Public Function IsSellingStoreCodeValid() As Boolean

            If Me.OrderHeader.SellingStoreCode.Value Is Nothing OrElse Me.OrderHeader.SellingStoreCode.Value.Length = 0 Then
                Me.OrderHeader.SellingStoreCode.ValidationStatus = "No selling store code supplied"
                Return False
            End If
            Return True

        End Function

        Public Sub SetQodNotFound()
            Me.OMOrderNumber.ValidationStatus = "No order found for this OM order number at store"
        End Sub

        Public Sub SetNothingSaved()
            Me.SuccessFlag.ValidationStatus = "Order not saved at store"
        End Sub


    End Class

    Partial Public Class CTSStatusNotificationResponseFulfilmentSite

        Public Sub New()
            Me.FulfilmentSite = New CTSStatusNotificationResponseFulfilmentSiteFulfilmentSite
            Me.FulfilmentSiteIBTOutNumber = New CTSStatusNotificationResponseFulfilmentSiteFulfilmentSiteIBTOutNumber
            Me.FulfilmentSiteOrderNumber = New CTSStatusNotificationResponseFulfilmentSiteFulfilmentSiteOrderNumber
            Me.SellingStoreIBTInNumber = New CTSStatusNotificationResponseFulfilmentSiteSellingStoreIBTInNumber
        End Sub

        Public Function RequiresIbtIn() As Boolean
            Dim minLineStatus As Integer = Me.MinimumLineStatus
            Return ((minLineStatus >= Delivery.IbtOutAllStock) AndAlso (minLineStatus <= Delivery.IbtOutFailedData))
        End Function

        Public Function RequiresPickingNotification() As Boolean
            Dim minLineStatus As Integer = Me.MinimumLineStatus
            Return ((minLineStatus >= Delivery.PickingNotifyOk) AndAlso (minLineStatus <= Delivery.PickingStatusFailedData))
        End Function

        Public Function RequiresDespatchNotification() As Boolean
            Dim minLineStatus As Integer = Me.MinimumLineStatus
            Return ((minLineStatus >= Delivery.DespatchNotifyOk) AndAlso (minLineStatus <= Delivery.DespatchStatusFailedData))
        End Function

        Public Function RequiresUndeliveredNotification() As Boolean
            Dim minLineStatus As Integer = Me.MinimumLineStatus
            Return ((minLineStatus >= Delivery.UndeliveredNotifyOk) AndAlso (minLineStatus <= Delivery.UndeliveredStatusFailedData))
        End Function

        Public Function RequiresDeliveredNotification() As Boolean
            Dim minLineStatus As Integer = Me.MinimumLineStatus
            Return ((minLineStatus >= Delivery.DeliveredNotifyOk) AndAlso (minLineStatus <= Delivery.DeliveredNotifyFailedData))
        End Function

        Private Function MinimumLineStatus() As Integer

            Dim min As Integer = CInt(Me.OrderLines(0).LineStatus.Value)
            For Each line As CTSStatusNotificationResponseFulfilmentSiteOrderLine In Me.OrderLines
                If CInt(line.LineStatus.Value) < min Then min = CInt(line.LineStatus.Value)
            Next
            Return min

        End Function

        Public Function AreOrderLinesPopulated() As Boolean
            If (Me.OrderLines Is Nothing OrElse Me.OrderLines.Length = 0) Then
                Me.FulfilmentSite.ValidationStatus = "No order lines have been provided"
                Return False
            End If
            Return True
        End Function

        Public Function AreOrderLinesInQod(ByVal qodLines As QodLineCollection) As Boolean

            Dim allLinesInQod As Boolean = True

            For Each line As CTSStatusNotificationResponseFulfilmentSiteOrderLine In Me.OrderLines
                Dim qodLine As QodLine = qodLines.Find(line.SellingStoreLineNo.Value)
                If qodLine Is Nothing Then
                    line.SetSellingStoreLineNoNotFound()
                    allLinesInQod = False
                End If
            Next

            Return allLinesInQod

        End Function

        Public Function AreOrderLinesMatchingProductCodes(ByVal qodLines As QodLineCollection) As Boolean

            Dim allMatching As Boolean = True

            For Each line As CTSStatusNotificationResponseFulfilmentSiteOrderLine In Me.OrderLines
                Dim qodLine As QodLine = qodLines.Find(line.SellingStoreLineNo.Value)
                If qodLine Is Nothing Then
                    allMatching = False
                Else
                    If qodLine.SkuNumber <> line.ProductCode.Value Then
                        line.SetProductCodeNotMatching()
                        allMatching = False
                    End If
                End If
            Next

            Return allMatching

        End Function

    End Class

    Partial Public Class CTSStatusNotificationResponseFulfilmentSiteOrderLine

        Public Sub New()
            Me.SellingStoreLineNo = New CTSStatusNotificationResponseFulfilmentSiteOrderLineSellingStoreLineNo
            Me.OMOrderLineNo = New CTSStatusNotificationResponseFulfilmentSiteOrderLineOMOrderLineNo
            Me.ProductCode = New CTSStatusNotificationResponseFulfilmentSiteOrderLineProductCode
            Me.LineStatus = New CTSStatusNotificationResponseFulfilmentSiteOrderLineLineStatus
        End Sub

        Public Function RequiresUpdating() As Boolean
            If RequiresIbtIn() Then Return True
            If RequiresPickingNotification() Then Return True
            If RequiresDespatchNotification() Then Return True
            If RequiresUndeliveredNotification() Then Return True
            If RequiresDeliveredNotification() Then Return True
            Return False
        End Function

        Public Function RequiresIbtIn() As Boolean
            Return ((CInt(Me.LineStatus.Value) >= Delivery.IbtOutAllStock) AndAlso (CInt(Me.LineStatus.Value) <= Delivery.IbtOutFailedData))
        End Function

        Public Function RequiresPickingNotification() As Boolean
            Return ((CInt(Me.LineStatus.Value) >= Delivery.PickingNotifyOk) AndAlso (CInt(Me.LineStatus.Value) <= Delivery.PickingStatusFailedData))
        End Function

        Public Function RequiresDespatchNotification() As Boolean
            Return ((CInt(Me.LineStatus.Value) >= Delivery.DespatchNotifyOk) AndAlso (CInt(Me.LineStatus.Value) <= Delivery.DespatchStatusFailedData))
        End Function

        Public Function RequiresUndeliveredNotification() As Boolean
            Return ((CInt(Me.LineStatus.Value) >= Delivery.UndeliveredNotifyOk) AndAlso (CInt(Me.LineStatus.Value) <= Delivery.UndeliveredStatusFailedData))
        End Function

        Public Function RequiresDeliveredNotification() As Boolean
            Return ((CInt(Me.LineStatus.Value) >= Delivery.DeliveredNotifyOk) AndAlso (CInt(Me.LineStatus.Value) <= Delivery.DeliveredNotifyFailedData))
        End Function

        Public Sub SetSellingStoreLineNoNotFound()
            Me.SellingStoreLineNo.ValidationStatus = "Selling store line not found"
        End Sub

        Public Sub SetProductCodeNotMatching()
            Me.ProductCode.ValidationStatus = "Product codes do not match for this line"
        End Sub

    End Class

#End Region

#Region "   Cts Update Refund"

    Partial Public Class CTSUpdateRefundResponse
        Inherits BaseResponse

        Private FulfilmentSiteIndex As Integer = 0

        Public Sub New()
            Me.DateTimeStamp = New CTSUpdateRefundResponseDateTimeStamp
            Me.OMOrderNumber = New CTSUpdateRefundResponseOMOrderNumber

        End Sub

        Public Overrides Function IsSuccessful() As Boolean
            Dim success As Boolean = True
            If Me.OrderRefunds IsNot Nothing Then
                For Each refund As CTSUpdateRefundResponseOrderRefund In Me.OrderRefunds
                    If refund.SuccessFlag = False Then success = False
                Next
            End If
            Return success
        End Function

        Public Overrides Sub SetFieldsInQod(ByRef qod As Qod.QodHeader)
            MyBase.SetFieldsInQod(qod)
            If IsSuccessful() Then
                qod.RefundStatus = Refund.StatusUpdateOk
            Else
                qod.RefundStatus = Refund.RefundFailedState
            End If
        End Sub

        Public Overrides Sub SetFieldsFromRequest(ByVal request As BaseRequest)
            Try
                Dim thisRequest As CTSUpdateRefundRequest = CType(request, CTSUpdateRefundRequest)
 
                Me.DateTimeStamp.Value = Now
                Me.OMOrderNumber.Value = thisRequest.OMOrderNumber

                If thisRequest.OrderRefunds IsNot Nothing Then
                    Dim upper As Integer = thisRequest.OrderRefunds.Count
                    If upper > 0 Then
                        ReDim Me.OrderRefunds(upper - 1)
                        For index As Integer = 0 To upper - 1
                            Me.OrderRefunds(index) = New CTSUpdateRefundResponseOrderRefund
                            Me.OrderRefunds(index).SuccessFlag = False
                            Me.OrderRefunds(index).RefundDate.Value = thisRequest.OrderRefunds(index).RefundDate
                            Me.OrderRefunds(index).RefundStoreCode.Value = thisRequest.OrderRefunds(index).RefundStoreCode
                            Me.OrderRefunds(index).RefundTransaction.Value = thisRequest.OrderRefunds(index).RefundTransaction
                            Me.OrderRefunds(index).RefundTill.Value = thisRequest.OrderRefunds(index).RefundTill

                            If thisRequest.OrderRefunds(index).FulfilmentSites IsNot Nothing Then
                                Dim lineUpper As Integer = thisRequest.OrderRefunds(index).FulfilmentSites.Count
                                If lineUpper > 0 Then
                                    ReDim Me.OrderRefunds(index).FulfilmentSites(lineUpper - 1)
                                    For lineIndex As Integer = 0 To lineUpper - 1
                                        Me.OrderRefunds(index).FulfilmentSites(lineIndex) = New CTSUpdateRefundResponseOrderRefundFulfilmentSite
                                        Me.OrderRefunds(index).FulfilmentSites(lineIndex).FulfilmentSiteCode.Value = thisRequest.OrderRefunds(index).FulfilmentSites(lineIndex).FulfilmentSiteCode
                                        Me.OrderRefunds(index).FulfilmentSites(lineIndex).SellingStoreIBTOutNumber.Value = thisRequest.OrderRefunds(index).FulfilmentSites(lineIndex).SellingStoreIBTOutNumber
                                        Me.OrderRefunds(index).FulfilmentSites(lineIndex).FulfilmentSiteIBTInNumber = "0"
                                    Next
                                End If
                            End If

                            If thisRequest.OrderRefunds(index).RefundLines IsNot Nothing Then
                                Dim lineUpper As Integer = thisRequest.OrderRefunds(index).RefundLines.Count
                                If lineUpper > 0 Then
                                    ReDim Me.OrderRefunds(index).RefundLines(lineUpper - 1)
                                    For lineIndex As Integer = 0 To lineUpper - 1
                                        Me.OrderRefunds(index).RefundLines(lineIndex) = New CTSUpdateRefundResponseOrderRefundRefundLine
                                        Me.OrderRefunds(index).RefundLines(lineIndex).FulfilmentSiteCode.Value = thisRequest.OrderRefunds(index).RefundLines(lineIndex).FulfilmentSiteCode
                                        Me.OrderRefunds(index).RefundLines(lineIndex).OMOrderLineNo.Value = thisRequest.OrderRefunds(index).RefundLines(lineIndex).OMOrderLineNo
                                        Me.OrderRefunds(index).RefundLines(lineIndex).ProductCode.Value = thisRequest.OrderRefunds(index).RefundLines(lineIndex).ProductCode
                                        Me.OrderRefunds(index).RefundLines(lineIndex).QuantityCancelled.Value = thisRequest.OrderRefunds(index).RefundLines(lineIndex).QuantityCancelled
                                        Me.OrderRefunds(index).RefundLines(lineIndex).QuantityReturned.Value = thisRequest.OrderRefunds(index).RefundLines(lineIndex).QuantityReturned
                                        Me.OrderRefunds(index).RefundLines(lineIndex).RefundLineStatus.Value = thisRequest.OrderRefunds(index).RefundLines(lineIndex).RefundLineStatus
                                        Me.OrderRefunds(index).RefundLines(lineIndex).RefundLineValue.Value = thisRequest.OrderRefunds(index).RefundLines(lineIndex).RefundLineValue
                                        Me.OrderRefunds(index).RefundLines(lineIndex).SellingStoreLineNo.Value = thisRequest.OrderRefunds(index).RefundLines(lineIndex).SellingStoreLineNo
                                    Next
                                End If
                            End If
                        Next
                    End If
                End If

            Catch ex As Exception
                Throw New InvalidFieldSettingException(ex)
            End Try

        End Sub

        Public Function GetIndex(ByVal StoreId As String) As Integer

            For intLine As Integer = 0 To Me.OrderRefunds(0).FulfilmentSites.GetUpperBound(0)
                If Me.OrderRefunds(0).FulfilmentSites(intLine).FulfilmentSiteCode.Value = StoreId Then
                    Return intLine
                End If
            Next
            Return 0
        End Function

        Public Sub SetIBTInNumber(ByVal IBTInNumber As String, ByVal Index As Integer)

            If Me.OrderRefunds IsNot Nothing Then
                Dim LineIndex As Integer = 0
                For Each refund As CTSUpdateRefundResponseOrderRefund In Me.OrderRefunds
                    refund.FulfilmentSites(Index).FulfilmentSiteIBTInNumber = IBTInNumber
                Next
            End If

        End Sub

        Public Sub SetQodNotFound()
            Me.OMOrderNumber.ValidationStatus = "No order found for this OM order number at store"
        End Sub

        Public Sub SetQodNotRefundable()
            If Me.OrderRefunds IsNot Nothing Then
                For Each refund As CTSUpdateRefundResponseOrderRefund In Me.OrderRefunds
                    refund.NotRefundableFlag = True
                Next
            End If
        End Sub

    End Class

    Partial Public Class CTSUpdateRefundResponseOrderRefund

        Public Sub New()
            Me.RefundDate = New CTSUpdateRefundResponseOrderRefundRefundDate
            Me.RefundStoreCode = New CTSUpdateRefundResponseOrderRefundRefundStoreCode
            Me.RefundTransaction = New CTSUpdateRefundResponseOrderRefundRefundTransaction
            Me.RefundTill = New CTSUpdateRefundResponseOrderRefundRefundTill
        End Sub

        Public Function AreFulfillSitesPopulated() As Boolean
            Return (Me.FulfilmentSites IsNot Nothing AndAlso Me.FulfilmentSites.Count > 0)
        End Function

        Public Function GetFulfillSiteOrNothing(ByVal siteId As Integer) As CTSUpdateRefundResponseOrderRefundFulfilmentSite
            If Not AreFulfillSitesPopulated() Then Return Nothing
            For Each fulfillSite As CTSUpdateRefundResponseOrderRefundFulfilmentSite In Me.FulfilmentSites
                If CInt(fulfillSite.FulfilmentSiteCode.Value) = siteId Then
                    Return fulfillSite
                End If
            Next
            Return Nothing
        End Function

        Public Sub SetSuccessful()
            Me.SuccessFlag = True
        End Sub

    End Class

    Partial Public Class CTSUpdateRefundResponseOrderRefundFulfilmentSite
        Public Sub New()
            Me.FulfilmentSiteCode = New CTSUpdateRefundResponseOrderRefundFulfilmentSiteFulfilmentSiteCode
            Me.SellingStoreIBTOutNumber = New CTSUpdateRefundResponseOrderRefundFulfilmentSiteSellingStoreIBTOutNumber
        End Sub
    End Class

    Partial Public Class CTSUpdateRefundResponseOrderRefundRefundLine

        Public Sub New()
            Me.FulfilmentSiteCode = New CTSUpdateRefundResponseOrderRefundRefundLineFulfilmentSiteCode
            Me.OMOrderLineNo = New CTSUpdateRefundResponseOrderRefundRefundLineOMOrderLineNo
            Me.ProductCode = New CTSUpdateRefundResponseOrderRefundRefundLineProductCode
            Me.QuantityCancelled = New CTSUpdateRefundResponseOrderRefundRefundLineQuantityCancelled
            Me.QuantityReturned = New CTSUpdateRefundResponseOrderRefundRefundLineQuantityReturned
            Me.RefundLineStatus = New CTSUpdateRefundResponseOrderRefundRefundLineRefundLineStatus
            Me.RefundLineValue = New CTSUpdateRefundResponseOrderRefundRefundLineRefundLineValue
            Me.SellingStoreLineNo = New CTSUpdateRefundResponseOrderRefundRefundLineSellingStoreLineNo
        End Sub

    End Class

#End Region

#Region "   Cts Qod"

    Partial Public Class CTSQODCreateResponse
        Inherits BaseResponse

        Public Sub New()
            Me.DateTimeStamp = New CTSQODCreateResponseDateTimeStamp
            Me.SuccessFlag = New CTSQODCreateResponseSuccessFlag

            Me.OrderHeader = New CTSQODCreateResponseOrderHeader
            Me.OrderHeader.Source = New CTSQODCreateResponseOrderHeaderSource
            Me.OrderHeader.SourceOrderNumber = New CTSQODCreateResponseOrderHeaderSourceOrderNumber
            Me.OrderHeader.StoreOrderNumber = New CTSQODCreateResponseOrderHeaderStoreOrderNumber
            Me.OrderHeader.RequiredDeliveryDate = New CTSQODCreateResponseOrderHeaderRequiredDeliveryDate
            Me.OrderHeader.DeliveryCharge = New CTSQODCreateResponseOrderHeaderDeliveryCharge
            Me.OrderHeader.TotalOrderValue = New CTSQODCreateResponseOrderHeaderTotalOrderValue
            Me.OrderHeader.SaleDate = New CTSQODCreateResponseOrderHeaderSaleDate
            Me.OrderHeader.CustomerAccountNo = New CTSQODCreateResponseOrderHeaderCustomerAccountNo
            Me.OrderHeader.CustomerName = New CTSQODCreateResponseOrderHeaderCustomerName
            Me.OrderHeader.CustomerAddressLine1 = New CTSQODCreateResponseOrderHeaderCustomerAddressLine1
            Me.OrderHeader.CustomerAddressLine2 = New CTSQODCreateResponseOrderHeaderCustomerAddressLine2
            Me.OrderHeader.CustomerAddressTown = New CTSQODCreateResponseOrderHeaderCustomerAddressTown
            Me.OrderHeader.CustomerAddressLine4 = New CTSQODCreateResponseOrderHeaderCustomerAddressLine4
            Me.OrderHeader.CustomerPostcode = New CTSQODCreateResponseOrderHeaderCustomerPostcode
            Me.OrderHeader.DeliveryAddressLine1 = New CTSQODCreateResponseOrderHeaderDeliveryAddressLine1
            Me.OrderHeader.DeliveryAddressLine2 = New CTSQODCreateResponseOrderHeaderDeliveryAddressLine2
            Me.OrderHeader.DeliveryAddressTown = New CTSQODCreateResponseOrderHeaderDeliveryAddressTown
            Me.OrderHeader.DeliveryAddressLine4 = New CTSQODCreateResponseOrderHeaderDeliveryAddressLine4
            Me.OrderHeader.DeliveryPostcode = New CTSQODCreateResponseOrderHeaderDeliveryPostcode
            Me.OrderHeader.ContactPhoneHome = New CTSQODCreateResponseOrderHeaderContactPhoneHome
            Me.OrderHeader.ContactPhoneMobile = New CTSQODCreateResponseOrderHeaderContactPhoneMobile
            Me.OrderHeader.ContactPhoneWork = New CTSQODCreateResponseOrderHeaderContactPhoneWork
            Me.OrderHeader.ContactEmail = New CTSQODCreateResponseOrderHeaderContactEmail
            Me.OrderHeader.ToBeDelivered = New CTSQODCreateResponseOrderHeaderToBeDelivered
            Me.OrderHeader.CreateDeliveryChargeItem = New CTSQODCreateResponseOrderHeaderCreateDeliveryChargeItem
            Me.OrderHeader.LoyaltyCardNumber = New CTSQODCreateResponseOrderHeaderLoyaltyCardNumber
            Me.OrderHeader.ExtendedLeadTime = New CTSQODCreateResponseOrderHeaderExtendedLeadTime
            Me.OrderHeader.DeliveryContactName = New CTSQODCreateResponseOrderHeaderDeliveryContactName
            Me.OrderHeader.DeliveryContactPhone = New CTSQODCreateResponseOrderHeaderDeliveryContactPhone
            Me.OrderHeader.RecordSaleOnly = New CTSQODCreateResponseOrderHeaderRecordSaleOnly
            Me.OrderHeader.DeliveryInstructions = New CTSQODCreateResponseOrderHeaderDeliveryInstructions

        End Sub

        Public Overrides Function IsSuccessful() As Boolean
            Return Me.SuccessFlag.Value
        End Function

        Public Overrides Sub SetFieldsFromRequest(ByVal request As BaseRequest)
            Try
                Dim thisRequest = CType(request, CTSQODCreateRequest)
                Dim DeliveryCharge As Boolean = False

                Me.DateTimeStamp.Value = Now
                Me.SuccessFlag.Value = False

                Me.OrderHeader.Source.Value = thisRequest.OrderHeader.Source.Value
                Me.OrderHeader.SourceOrderNumber.Value = thisRequest.OrderHeader.SourceOrderNumber.Value
                Me.OrderHeader.StoreOrderNumber.Value = ""
                Me.OrderHeader.RequiredDeliveryDate.Value = thisRequest.OrderHeader.RequiredDeliveryDate.Value
                Me.OrderHeader.DeliveryCharge.Value = thisRequest.OrderHeader.DeliveryCharge.Value
                Me.OrderHeader.TotalOrderValue.Value = thisRequest.OrderHeader.TotalOrderValue.Value
                Me.OrderHeader.SaleDate.Value = thisRequest.OrderHeader.SaleDate.Value
                Me.OrderHeader.CustomerAccountNo.Value = thisRequest.OrderHeader.CustomerAccountNo.Value
                Me.OrderHeader.CustomerName.Value = thisRequest.OrderHeader.CustomerName.Value
                Me.OrderHeader.CustomerAddressLine1.Value = thisRequest.OrderHeader.CustomerAddressLine1.Value
                Me.OrderHeader.CustomerAddressLine2.Value = thisRequest.OrderHeader.CustomerAddressLine2.Value
                Me.OrderHeader.CustomerAddressTown.Value = thisRequest.OrderHeader.CustomerAddressTown.Value
                Me.OrderHeader.CustomerAddressLine4.Value = thisRequest.OrderHeader.CustomerAddressLine4.Value
                Me.OrderHeader.CustomerPostcode.Value = thisRequest.OrderHeader.CustomerPostcode.Value
                Me.OrderHeader.DeliveryAddressLine1.Value = thisRequest.OrderHeader.DeliveryAddressLine1.Value
                Me.OrderHeader.DeliveryAddressLine2.Value = thisRequest.OrderHeader.DeliveryAddressLine2.Value
                Me.OrderHeader.DeliveryAddressTown.Value = thisRequest.OrderHeader.DeliveryAddressTown.Value
                Me.OrderHeader.DeliveryAddressLine4.Value = thisRequest.OrderHeader.DeliveryAddressLine4.Value
                Me.OrderHeader.DeliveryPostcode.Value = thisRequest.OrderHeader.DeliveryPostcode.Value
                Me.OrderHeader.ContactPhoneHome.Value = thisRequest.OrderHeader.ContactPhoneHome.Value
                Me.OrderHeader.ContactPhoneMobile.Value = thisRequest.OrderHeader.ContactPhoneMobile.Value
                Me.OrderHeader.ContactPhoneWork.Value = thisRequest.OrderHeader.ContactPhoneWork.Value
                Me.OrderHeader.ContactEmail.Value = thisRequest.OrderHeader.ContactEmail.Value
                Me.OrderHeader.ToBeDelivered.Value = thisRequest.OrderHeader.ToBeDelivered.Value
                Me.OrderHeader.CreateDeliveryChargeItem.Value = thisRequest.OrderHeader.CreateDeliveryChargeItem.Value
                Me.OrderHeader.LoyaltyCardNumber.Value = thisRequest.OrderHeader.LoyaltyCardNumber.Value
                Me.OrderHeader.ExtendedLeadTime.Value = thisRequest.OrderHeader.ExtendedLeadTime.Value
                Me.OrderHeader.DeliveryContactName.Value = thisRequest.OrderHeader.DeliveryContactName.Value
                Me.OrderHeader.DeliveryContactPhone.Value = thisRequest.OrderHeader.DeliveryContactPhone.Value
                Me.OrderHeader.RecordSaleOnly.Value = thisRequest.OrderHeader.RecordSaleOnly.Value

                If thisRequest.OrderHeader.DeliveryInstructions.InstructionLine IsNot Nothing Then
                    Dim upper As Integer = thisRequest.OrderHeader.DeliveryInstructions.InstructionLine.Count
                    If upper > 0 Then
                        ReDim Me.OrderHeader.DeliveryInstructions.InstructionLine(upper - 1)
                        For index As Integer = 0 To upper - 1
                            Me.OrderHeader.DeliveryInstructions.InstructionLine(index) = New CTSQODCreateResponseOrderHeaderDeliveryInstructionsInstructionLine

                            'Me.OrderHeader.DeliveryInstructions.InstructionLine(index).LineNo.Value = thisRequest.OrderHeader.DeliveryInstructions.InstructionLine(index).LineNo.Value
                            Me.OrderHeader.DeliveryInstructions.InstructionLine(index).LineNo = New CTSQODCreateResponseOrderHeaderDeliveryInstructionsInstructionLineLineNo
                            Me.OrderHeader.DeliveryInstructions.InstructionLine(index).LineNo.Value = thisRequest.OrderHeader.DeliveryInstructions.InstructionLine(index).LineNo.Value

                            'Me.OrderHeader.DeliveryInstructions.InstructionLine(index).LineText.Value = thisRequest.OrderHeader.DeliveryInstructions.InstructionLine(index).LineText.Value
                            Me.OrderHeader.DeliveryInstructions.InstructionLine(index).LineText = New CTSQODCreateResponseOrderHeaderDeliveryInstructionsInstructionLineLineText
                            Me.OrderHeader.DeliveryInstructions.InstructionLine(index).LineText.Value = thisRequest.OrderHeader.DeliveryInstructions.InstructionLine(index).LineText.Value
                        Next
                    End If
                End If

                'Check for a delivery charge
                If thisRequest.OrderHeader.CreateDeliveryChargeItem.Value Then
                    DeliveryCharge = True
                End If

                Dim LinesNeeded As Integer = 0
                If thisRequest.OrderLines IsNot Nothing Then LinesNeeded = thisRequest.OrderLines.Count
                If DeliveryCharge Then LinesNeeded += 1

                If LinesNeeded > 0 Then
                    Dim Lines(LinesNeeded - 1) As CTSQODCreateResponseOrderLine
                    For Index As Integer = 0 To LinesNeeded - 1
                        Lines(Index) = New CTSQODCreateResponseOrderLine
                        If Index = LinesNeeded - 1 AndAlso DeliveryCharge Then
                            'Create the delivery charge line
                            Lines(Index).SourceOrderLineNo.Value = (Index + 1).ToString.PadLeft(4, "0"c)
                            Lines(Index).StoreOrderLineNo.Value = (Index + 1).ToString.PadLeft(4, "0"c)
                            Lines(Index).ProductCode.Value = "805111"
                            Lines(Index).ProductDescription.Value = "Delivery Charge"
                            Lines(Index).TotalOrderQuantity.Value = 1
                            Lines(Index).LineValue.Value = thisRequest.OrderHeader.DeliveryCharge.Value
                            Lines(Index).DeliveryChargeItem.Value = True
                            Lines(Index).SellingPrice.Value = thisRequest.OrderHeader.DeliveryCharge.Value
                            Lines(Index).RecordSaleOnly.Value = Me.OrderHeader.RecordSaleOnly.Value
                        Else
                            Lines(Index).SourceOrderLineNo.Value = thisRequest.OrderLines(Index).SourceOrderLineNo.Value
                            Lines(Index).StoreOrderLineNo.Value = (Index + 1).ToString.PadLeft(4, "0"c)
                            Lines(Index).ProductCode.Value = thisRequest.OrderLines(Index).ProductCode.Value
                            Lines(Index).ProductDescription.Value = thisRequest.OrderLines(Index).ProductDescription.Value
                            Lines(Index).TotalOrderQuantity.Value = thisRequest.OrderLines(Index).TotalOrderQuantity.Value
                            Lines(Index).LineValue.Value = thisRequest.OrderLines(Index).LineValue.Value
                            Lines(Index).DeliveryChargeItem.Value = thisRequest.OrderLines(Index).DeliveryChargeItem.Value
                            Lines(Index).SellingPrice.Value = thisRequest.OrderLines(Index).SellingPrice.Value
                            Lines(Index).RecordSaleOnly.Value = thisRequest.OrderLines(Index).RecordSaleOnly.Value
                        End If
                    Next
                    Me.OrderLines = Lines
                End If

            Catch ex As Exception
                Throw New InvalidFieldSettingException(ex)
            End Try
        End Sub

        Public Sub SetError(ByVal description As String)
            Me.DateTimeStamp.ValidationStatus = description
            Me.SuccessFlag.Value = False
        End Sub

        Public Sub CreateDeliveryChargeItem(ByVal skuNumber As String, ByVal skuDescription As String)
            ReDim Me.OrderLines(Me.OrderLines.Count)
            Me.OrderLines(Me.OrderLines.Count - 1) = New CTSQODCreateResponseOrderLine
            Me.OrderLines(Me.OrderLines.Count - 1).DeliveryChargeItem.Value = True
            Me.OrderLines(Me.OrderLines.Count - 1).LineValue.Value = Me.OrderHeader.DeliveryCharge.Value
            Me.OrderLines(Me.OrderLines.Count - 1).ProductCode.Value = skuNumber
            Me.OrderLines(Me.OrderLines.Count - 1).ProductDescription.Value = skuDescription
            Me.OrderLines(Me.OrderLines.Count - 1).SellingPrice.Value = Me.OrderHeader.DeliveryCharge.Value
            Me.OrderLines(Me.OrderLines.Count - 1).TotalOrderQuantity.Value = 1
        End Sub

        Public Sub SetStoreOrderLineNumbers()
            If Me.OrderLines.Count > 0 Then
                For index As Integer = 0 To Me.OrderLines.Count - 1
                    Me.OrderLines(index).StoreOrderLineNo.Value = CStr(index + 1)
                Next
            End If

        End Sub

        Public Function CreateVendaXmlString() As String

            Dim doc As New XmlDocument
            Dim docOrder As XmlElement = doc.CreateElement("order")

            For Each line As CTSQODCreateResponseOrderLine In Me.OrderLines
                Dim docLine As XmlElement = doc.CreateElement("line")
                Dim docLineQty As XmlElement = doc.CreateElement("quantity")
                Dim docLineItem As XmlElement = doc.CreateElement("item")
                Dim docLinePrice As XmlElement = doc.CreateElement("sellingprice")
                docLineQty.InnerText = CStr(line.TotalOrderQuantity.Value)
                docLineItem.InnerText = line.ProductCode.Value
                docLinePrice.InnerText = CStr(line.SellingPrice.Value)

                docLine.AppendChild(docLineQty)
                docLine.AppendChild(docLineItem)
                docLine.AppendChild(docLinePrice)
                docOrder.AppendChild(docLine)
            Next

            Dim docHeader As XmlElement = doc.CreateElement("header")
            Dim docHeaderNum As XmlElement = doc.CreateElement("ordernumber")
            Dim docHeaderTime As XmlElement = doc.CreateElement("time")
            Dim docHeaderDate As XmlElement = doc.CreateElement("date")
            Dim docHeaderType As XmlElement = doc.CreateElement("type")
            Dim docHeaderDelivery As XmlElement = doc.CreateElement("deliverycharge")
            Dim docHeaderTotal As XmlElement = doc.CreateElement("total")
            docHeaderNum.InnerText = Me.OrderHeader.SourceOrderNumber.Value
            docHeaderTime.InnerText = "00:00:00"
            docHeaderDate.InnerText = Me.OrderHeader.SaleDate.Value.ToString("dd/MM/yyyy")
            docHeaderType.InnerText = "SALE"
            docHeaderDelivery.InnerText = CStr(Me.OrderHeader.DeliveryCharge.Value)
            docHeaderTotal.InnerText = CStr(Me.OrderHeader.TotalOrderValue.Value)

            docHeader.AppendChild(docHeaderNum)
            docHeader.AppendChild(docHeaderTime)
            docHeader.AppendChild(docHeaderDate)
            docHeader.AppendChild(docHeaderType)
            docHeader.AppendChild(docHeaderDelivery)
            docHeader.AppendChild(docHeaderTotal)
            docOrder.AppendChild(docHeader)

            Dim docLoyalty As XmlElement = doc.CreateElement("loyaltycard")
            docLoyalty.InnerText = Me.OrderHeader.LoyaltyCardNumber.Value
            docOrder.AppendChild(docLoyalty)

            doc.AppendChild(docOrder)
            Return doc.OuterXml.ToString

        End Function

    End Class

    Partial Public Class CTSQODCreateResponseOrderLine

        Public Sub New()
            Me.DeliveryChargeItem = New CTSQODCreateResponseOrderLineDeliveryChargeItem
            Me.LineValue = New CTSQODCreateResponseOrderLineLineValue
            Me.ProductCode = New CTSQODCreateResponseOrderLineProductCode
            Me.ProductDescription = New CTSQODCreateResponseOrderLineProductDescription
            Me.SellingPrice = New CTSQODCreateResponseOrderLineSellingPrice
            Me.SourceOrderLineNo = New CTSQODCreateResponseOrderLineSourceOrderLineNo
            Me.StoreOrderLineNo = New CTSQODCreateResponseOrderLineStoreOrderLineNo
            Me.TotalOrderQuantity = New CTSQODCreateResponseOrderLineTotalOrderQuantity
            Me.RecordSaleOnly = New CTSQODCreateResponseOrderLineRecordSaleOnly
        End Sub

    End Class

#End Region

#Region "   Om"

    Partial Public Class OMOrderCreateResponse
        Inherits BaseResponse

        Public Function ValidationErrors() As List(Of String)
            Dim ls As New List(Of String)
            If Me.DateTimeStamp.ValidationStatus IsNot Nothing Then ls.Add(Me.DateTimeStamp.ValidationStatus)
            If Me.OrderHeader.ContactEmail.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.ContactEmail.ValidationStatus)
            If Me.OrderHeader.ContactPhoneHome.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.ContactPhoneHome.ValidationStatus)
            If Me.OrderHeader.ContactPhoneMobile.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.ContactPhoneMobile.ValidationStatus)
            If Me.OrderHeader.ContactPhoneWork.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.ContactPhoneWork.ValidationStatus)
            If Me.OrderHeader.CustomerAccountNo.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.CustomerAccountNo.ValidationStatus)
            If Me.OrderHeader.CustomerAddressLine1.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.CustomerAddressLine1.ValidationStatus)
            If Me.OrderHeader.CustomerAddressLine2.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.CustomerAddressLine2.ValidationStatus)
            If Me.OrderHeader.CustomerAddressTown.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.CustomerAddressTown.ValidationStatus)
            If Me.OrderHeader.CustomerAddressLine4.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.CustomerAddressLine4.ValidationStatus)
            If Me.OrderHeader.CustomerName.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.CustomerName.ValidationStatus)
            If Me.OrderHeader.CustomerPostcode.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.CustomerPostcode.ValidationStatus)
            If Me.OrderHeader.DeliveryAddressLine1.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.DeliveryAddressLine1.ValidationStatus)
            If Me.OrderHeader.DeliveryAddressLine2.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.DeliveryAddressLine2.ValidationStatus)
            If Me.OrderHeader.DeliveryAddressTown.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.DeliveryAddressTown.ValidationStatus)
            If Me.OrderHeader.DeliveryAddressLine4.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.DeliveryAddressLine4.ValidationStatus)
            If Me.OrderHeader.DeliveryCharge.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.DeliveryCharge.ValidationStatus)
            If Me.OrderHeader.DeliveryPostcode.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.DeliveryPostcode.ValidationStatus)
            If Me.OrderHeader.RequiredDeliveryDate.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.RequiredDeliveryDate.ValidationStatus)
            If Me.OrderHeader.SaleDate.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.SaleDate.ValidationStatus)
            If Me.OrderHeader.SellingStoreCode.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.SellingStoreCode.ValidationStatus)
            If Me.OrderHeader.SellingStoreOrderNumber.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.SellingStoreOrderNumber.ValidationStatus)
            If Me.OrderHeader.ToBeDelivered.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.ToBeDelivered.ValidationStatus)
            If Me.OrderHeader.TotalOrderValue.ValidationStatus IsNot Nothing Then ls.Add(Me.OrderHeader.TotalOrderValue.ValidationStatus)

            If Me.OrderHeader.DeliveryInstructions.InstructionLine IsNot Nothing Then
                For Each di As OMOrderCreateResponseOrderHeaderDeliveryInstructionsInstructionLine In Me.OrderHeader.DeliveryInstructions.InstructionLine
                    If di.LineNo.ValidationStatus IsNot Nothing Then ls.Add(di.LineNo.ValidationStatus)
                    If di.LineText.ValidationStatus IsNot Nothing Then ls.Add(di.LineText.ValidationStatus)
                Next
            End If

            If Me.OrderLines IsNot Nothing Then
                For Each ol As OMOrderCreateResponseOrderLine In Me.OrderLines
                    If ol.DeliveryChargeItem.ValidationStatus IsNot Nothing Then ls.Add(ol.DeliveryChargeItem.ValidationStatus)
                    If ol.LineValue.ValidationStatus IsNot Nothing Then ls.Add(ol.LineValue.ValidationStatus)
                    If ol.ProductCode.ValidationStatus IsNot Nothing Then ls.Add(ol.ProductCode.ValidationStatus)
                    If ol.ProductDescription.ValidationStatus IsNot Nothing Then ls.Add(ol.ProductDescription.ValidationStatus)
                    If ol.QuantityTaken.ValidationStatus IsNot Nothing Then ls.Add(ol.QuantityTaken.ValidationStatus)
                    If ol.SellingPrice.ValidationStatus IsNot Nothing Then ls.Add(ol.SellingPrice.ValidationStatus)
                    If ol.SellingStoreLineNo.ValidationStatus IsNot Nothing Then ls.Add(ol.SellingStoreLineNo.ValidationStatus)
                    If ol.TotalOrderQuantity.ValidationStatus IsNot Nothing Then ls.Add(ol.TotalOrderQuantity.ValidationStatus)
                    If ol.UOM.ValidationStatus IsNot Nothing Then ls.Add(ol.UOM.ValidationStatus)
                Next
            End If

            Return ls

        End Function

        Public Overrides Sub SetFieldsInQod(ByRef _qod As QodHeader)

            If Me.SuccessFlag.Value Then
                _qod.SetDeliveryStatusOk()
                _qod.Lines.SetDeliveryStatusOk()
                _qod.OmOrderNumber = CInt(Me.OrderHeader.OMOrderNumber.Value)
                'check if collection item and set straight to 499 if so
                If Not Me.OrderHeader.ToBeDelivered.Value Then
                    _qod.Lines.DeliveryStatus = Delivery.ReceiptStatusOk
                End If
                'Set the header to the minium delivery status
                _qod.DeliveryStatus = _qod.Lines.DeliveryStatus
            Else
                Dim errors As List(Of String) = Me.ValidationErrors
                _qod.Texts.AddValidationErrors(errors)
                _qod.SetDeliveryStatus(Qod.State.Delivery.OrderFailedState)
                _qod.Lines.SetDeliveryStatus(Qod.State.Delivery.OrderFailedState)
            End If
        End Sub

        Public Overrides Function IsSuccessful() As Boolean
            Return Me.SuccessFlag.Value
        End Function

        Public Overrides Sub SetFieldsFromRequest(ByVal request As BaseRequest)
            Try
                Dim thisRequest As OMOrderCreateRequest = CType(request, OMOrderCreateRequest)
                Me.DateTimeStamp.Value = Now
                Me.SuccessFlag.Value = False

                Me.OrderHeader.SellingStoreCode.Value = thisRequest.OrderHeader.SellingStoreCode.Value
                Me.OrderHeader.SellingStoreOrderNumber.Value = thisRequest.OrderHeader.SellingStoreOrderNumber.Value
                Me.OrderHeader.RequiredDeliveryDate.Value = thisRequest.OrderHeader.RequiredDeliveryDate.Value
                Me.OrderHeader.DeliveryCharge.Value = thisRequest.OrderHeader.DeliveryCharge.Value
                Me.OrderHeader.TotalOrderValue.Value = thisRequest.OrderHeader.TotalOrderValue.Value
                Me.OrderHeader.SaleDate.Value = thisRequest.OrderHeader.SaleDate.Value
                Me.OrderHeader.OMOrderNumber.Value = ""
                Me.OrderHeader.CustomerAccountNo.Value = thisRequest.OrderHeader.CustomerAccountNo.Value
                Me.OrderHeader.CustomerName.Value = thisRequest.OrderHeader.CustomerName.Value
                Me.OrderHeader.CustomerAddressLine1.Value = thisRequest.OrderHeader.CustomerAddressLine1.Value
                Me.OrderHeader.CustomerAddressLine2.Value = thisRequest.OrderHeader.CustomerAddressLine2.Value
                Me.OrderHeader.CustomerAddressTown.Value = thisRequest.OrderHeader.CustomerAddressTown.Value
                Me.OrderHeader.CustomerAddressLine4.Value = thisRequest.OrderHeader.CustomerAddressLine4.Value
                Me.OrderHeader.CustomerPostcode.Value = thisRequest.OrderHeader.CustomerPostcode.Value
                Me.OrderHeader.DeliveryAddressLine1.Value = thisRequest.OrderHeader.DeliveryAddressLine1.Value
                Me.OrderHeader.DeliveryAddressLine2.Value = thisRequest.OrderHeader.DeliveryAddressLine2.Value
                Me.OrderHeader.DeliveryAddressTown.Value = thisRequest.OrderHeader.DeliveryAddressTown.Value
                Me.OrderHeader.DeliveryAddressLine4.Value = thisRequest.OrderHeader.DeliveryAddressLine4.Value
                Me.OrderHeader.DeliveryPostcode.Value = thisRequest.OrderHeader.DeliveryPostcode.Value
                Me.OrderHeader.ContactPhoneHome.Value = thisRequest.OrderHeader.ContactPhoneHome.Value
                Me.OrderHeader.ContactPhoneMobile.Value = thisRequest.OrderHeader.ContactPhoneMobile.Value
                Me.OrderHeader.ContactPhoneWork.Value = thisRequest.OrderHeader.ContactPhoneWork.Value
                Me.OrderHeader.ContactEmail.Value = thisRequest.OrderHeader.ContactEmail.Value
                Me.OrderHeader.ToBeDelivered.Value = thisRequest.OrderHeader.ToBeDelivered.Value

                If thisRequest.OrderHeader.DeliveryInstructions.InstructionLine IsNot Nothing Then
                    Dim upper As Integer = thisRequest.OrderHeader.DeliveryInstructions.InstructionLine.Count
                    If upper > 0 Then
                        ReDim Me.OrderHeader.DeliveryInstructions.InstructionLine(upper - 1)
                        For index As Integer = 0 To upper - 1
                            Me.OrderHeader.DeliveryInstructions.InstructionLine(index) = New OMOrderCreateResponseOrderHeaderDeliveryInstructionsInstructionLine
                            Me.OrderHeader.DeliveryInstructions.InstructionLine(index).LineNo.Value = thisRequest.OrderHeader.DeliveryInstructions.InstructionLine(index).LineNo.Value
                            Me.OrderHeader.DeliveryInstructions.InstructionLine(index).LineText.Value = thisRequest.OrderHeader.DeliveryInstructions.InstructionLine(index).LineText.Value
                        Next
                    End If
                End If

                If thisRequest.OrderLines IsNot Nothing Then
                    Dim upper As Integer = thisRequest.OrderLines.Count
                    If upper > 0 Then
                        Dim lines(upper - 1) As OMOrderCreateResponseOrderLine
                        For index As Integer = 0 To upper - 1
                            lines(index) = New OMOrderCreateResponseOrderLine
                            lines(index).SellingStoreLineNo.Value = thisRequest.OrderLines(index).SellingStoreLineNo.Value
                            lines(index).OMOrderLineNo.Value = ""
                            lines(index).ProductCode.Value = thisRequest.OrderLines(index).ProductCode.Value
                            lines(index).ProductDescription.Value = thisRequest.OrderLines(index).ProductDescription.Value
                            lines(index).TotalOrderQuantity.Value = thisRequest.OrderLines(index).TotalOrderQuantity.Value
                            lines(index).QuantityTaken.Value = thisRequest.OrderLines(index).QuantityTaken.Value
                            lines(index).UOM.Value = thisRequest.OrderLines(index).UOM.Value
                            lines(index).LineValue.Value = thisRequest.OrderLines(index).LineValue.Value
                            lines(index).DeliveryChargeItem.Value = thisRequest.OrderLines(index).DeliveryChargeItem.Value
                            lines(index).SellingPrice.Value = thisRequest.OrderLines(index).SellingPrice.Value
                        Next

                        Me.OrderLines = lines
                    End If
                End If

            Catch ex As Exception
                Throw New InvalidFieldSettingException(ex)
            End Try
        End Sub

        Public Sub SetError(ByVal description As String)
            Me.DateTimeStamp.ValidationStatus = description
            Me.SuccessFlag.Value = False
        End Sub

    End Class


    Partial Public Class OMCheckFulfilmentResponse
        Inherits BaseResponse

        Public Overrides Function IsSuccessful() As Boolean
            Return Me.SuccessFlag
        End Function

        Public Function GetEstoreFulfiller() As String
            Return Me.EstoreFulfiller
        End Function

        Public Function GetHubFulfiller() As String
            Return Me.HubFulfiller
        End Function

        Public Function ValidationErrors() As List(Of String)
            Dim ls As New List(Of String)

            If Not IsSuccessful() Then
                CheckAndAdd(Me.EstoreFulfiller, ls)
                CheckAndAdd(Me.HubFulfiller, ls)
            End If
            Return ls
        End Function

        Private Sub CheckAndAdd(ByVal text As String, ByRef list As List(Of String))
            If text IsNot Nothing AndAlso text.Length > 0 Then
                list.Add(text)
            End If
        End Sub

    End Class


    Partial Public Class OMOrderReceiveStatusUpdateResponse
        Inherits BaseResponse

        Public Overrides Sub SetFieldsInQod(ByRef qod As QodHeader)
            If Me.SuccessFlag.Value Then
                Dim storeId As Integer = ThisStore.Id4
                qod.Lines.SetDeliveryNotifyOk(storeId)
                qod.Lines.SetDeliveryStatusOk(storeId)
                qod.SetDeliveryStatusOk()
            Else
                Dim errors As List(Of String) = Me.ValidationErrors
                qod.Texts.AddValidationErrors(errors)
                qod.Lines.SetDeliveryStatusFailedState()
                qod.SetDeliveryStatus(Delivery.OrderFailedState)
            End If
        End Sub

        Public Function ValidationErrors() As List(Of String)
            Dim ls As New List(Of String)
            If Me.DateTimeStamp.ValidationStatus IsNot Nothing Then ls.Add(Me.DateTimeStamp.ValidationStatus)
            If Me.SuccessFlag.ValidationStatus IsNot Nothing Then ls.Add(Me.SuccessFlag.ValidationStatus)
            If Me.FulfilmentSite.ValidationStatus IsNot Nothing Then ls.Add(Me.FulfilmentSite.ValidationStatus)
            If Me.OMOrderNumber.ValidationStatus IsNot Nothing Then ls.Add(Me.OMOrderNumber.ValidationStatus)

            For Each ol As OMOrderReceiveStatusUpdateResponseOrderStatusOrderLine In Me.OrderStatus.OrderLines
                If ol.LineStatus.ValidationStatus IsNot Nothing Then ls.Add(ol.LineStatus.ValidationStatus)
                If ol.OMOrderLineNo.ValidationStatus IsNot Nothing Then ls.Add(ol.OMOrderLineNo.ValidationStatus)
            Next

            Return ls

        End Function

        Public Overrides Function IsSuccessful() As Boolean
            Return Me.SuccessFlag.Value
        End Function
    End Class

    Partial Public Class OMOrderReceiveStatusUpdateRequestOrderStatusOrderLine
        Public Sub New()
            Me.LineStatus = New OMOrderReceiveStatusUpdateRequestOrderStatusOrderLineLineStatus
            Me.OMOrderLineNo = New OMOrderReceiveStatusUpdateRequestOrderStatusOrderLineOMOrderLineNo
        End Sub
    End Class


    Partial Public Class OMOrderRefundResponse
        Inherits BaseResponse

        Public Overrides Function IsSuccessful() As Boolean
            Dim success As Boolean = True
            If Me.OrderRefunds IsNot Nothing Then
                For Each refund As OMOrderRefundResponseOrderRefund In Me.OrderRefunds
                    If refund.SuccessFlag = False Then success = False
                Next
            End If
            Return success
        End Function

        Public Overrides Sub SetFieldsInQod(ByRef _qod As QodHeader)

            'Get the Refund Status
            If Me.IsSuccessful Then

                For Each orderRefund As OMOrderRefundResponseOrderRefund In Me.OrderRefunds
                    _qod.RefundStatus = Qod.State.Refund.StatusUpdateOk
                    For Each refundLine As OMOrderRefundResponseOrderRefundRefundLine In orderRefund.RefundLines
                        Dim refund As QodRefund = _qod.Refunds.Find(CInt(refundLine.SellingStoreLineNo.Value), CInt(orderRefund.RefundStoreCode.Value), orderRefund.RefundDate.Value, orderRefund.RefundTill.Value, orderRefund.RefundTransaction.Value)
                        For Each line As QodRefund In _qod.Refunds
                            If Val(refundLine.OMOrderLineNo.Value) = Val(line.Number) Then
                                If CInt(refundLine.RefundLineStatus.Value) = Qod.State.Refund.NotifyOk Then
                                    line.RefundStatus = Qod.State.Refund.StatusUpdateOk
                                End If
                            End If
                        Next
                    Next
                Next

            Else

                If Not _qod.Refunds Is Nothing Then
                    _qod.SetRefundStatus(Qod.State.Refund.RefundFailedData)
                    _qod.Refunds.SetRefundStatus(Qod.State.Refund.RefundFailedData)
                Else
                    _qod.SetDeliveryStatus(Qod.State.Delivery.OrderFailedData)
                    _qod.Lines.SetDeliveryStatus(Qod.State.Delivery.OrderFailedData)
                End If

            End If

        End Sub

        Public Function ValidationErrors() As List(Of String)
            Dim ls As New List(Of String)
            CheckAndAdd(Me.DateTimeStamp.ValidationStatus, ls)
            CheckAndAdd(Me.OMOrderNumber.ValidationStatus, ls)

            For Each orderRefund As OMOrderRefundResponseOrderRefund In Me.OrderRefunds
                CheckAndAdd(orderRefund.RefundDate.ValidationStatus, ls)
                CheckAndAdd(orderRefund.RefundStoreCode.ValidationStatus, ls)
                CheckAndAdd(orderRefund.RefundTransaction.ValidationStatus, ls)
                CheckAndAdd(orderRefund.RefundTill.ValidationStatus, ls)

                For Each site As OMOrderRefundResponseOrderRefundFulfilmentSite In orderRefund.FulfilmentSites
                    CheckAndAdd(site.FulfilmentSiteCode.ValidationStatus, ls)
                    CheckAndAdd(site.SellingStoreIBTOutNumber.ValidationStatus, ls)
                Next

                For Each refundLine As OMOrderRefundResponseOrderRefundRefundLine In orderRefund.RefundLines
                    CheckAndAdd(refundLine.SellingStoreLineNo.ValidationStatus, ls)
                    CheckAndAdd(refundLine.OMOrderLineNo.ValidationStatus, ls)
                    CheckAndAdd(refundLine.ProductCode.ValidationStatus, ls)
                    CheckAndAdd(refundLine.FulfilmentSiteCode.ValidationStatus, ls)
                    CheckAndAdd(refundLine.QuantityReturned.ValidationStatus, ls)
                    CheckAndAdd(refundLine.QuantityCancelled.ValidationStatus, ls)
                    CheckAndAdd(refundLine.RefundLineValue.ValidationStatus, ls)
                    CheckAndAdd(refundLine.RefundLineStatus.ValidationStatus, ls)
                Next
            Next

            Return ls

        End Function

        Private Sub CheckAndAdd(ByVal text As String, ByRef list As List(Of String))
            If text IsNot Nothing AndAlso text.Length > 0 Then list.Add(text)
        End Sub

    End Class

    Partial Public Class OMSendFullStatusUpdateResponse
        Inherits BaseResponse

        Public Function GetQod() As QodHeader

            If Me.SuccessFlag = True Then
                If TypeOf Me.OrderStatus.Item Is OMSendFullStatusUpdateResponseOrderStatusOrderDetail Then
                    Dim orderDetail As OMSendFullStatusUpdateResponseOrderStatusOrderDetail = CType(Me.OrderStatus.Item, OMSendFullStatusUpdateResponseOrderStatusOrderDetail)
                    Dim qod As QodHeader = CreateFromOmFullStatus(orderDetail)
                    Return qod
                End If
            End If
            Return Nothing

        End Function

        Public Overrides Function IsSuccessful() As Boolean
            Return Me.SuccessFlag
        End Function

        Public Function GetErrorDetail() As OMSendFullStatusUpdateResponseOrderStatusErrorDetail

            If TypeOf Me.OrderStatus.Item Is OMSendFullStatusUpdateResponseOrderStatusErrorDetail Then
                Dim errorDetail As OMSendFullStatusUpdateResponseOrderStatusErrorDetail = CType(Me.OrderStatus.Item, OMSendFullStatusUpdateResponseOrderStatusErrorDetail)
                Return errorDetail
            End If
            Return Nothing

        End Function


    End Class

    Partial Public Class OMSourceTransactionLookupResponse
        Inherits BaseResponse

        Public Overrides Function IsSuccessful() As Boolean

            Return Me.SuccessFlag.Value
        End Function

        Public Function GetErrorDetail() As OMSourceTransactionLookupResponseSourceTransactionDetailErrorDetail

            If TypeOf Me.SourceTransactionDetail.Item Is OMSourceTransactionLookupResponseSourceTransactionDetailErrorDetail Then
                Dim errorDetail As OMSourceTransactionLookupResponseSourceTransactionDetailErrorDetail = CType(Me.SourceTransactionDetail.Item, OMSourceTransactionLookupResponseSourceTransactionDetailErrorDetail)

                Return errorDetail
            Else
                Return Nothing
            End If
        End Function

        Public Function GetOrderDetail() As OMSourceTransactionLookupResponseSourceTransactionDetailOrderDetail

            If TypeOf Me.SourceTransactionDetail.Item Is OMSourceTransactionLookupResponseSourceTransactionDetailOrderDetail Then
                Dim orderDetail As OMSourceTransactionLookupResponseSourceTransactionDetailOrderDetail = CType(Me.SourceTransactionDetail.Item, OMSourceTransactionLookupResponseSourceTransactionDetailOrderDetail)

                Return orderDetail
            Else
                Return Nothing
            End If
        End Function

        Public Function ValidationErrors() As List(Of String)
            Dim ls As New List(Of String)

            If Not IsSuccessful() Then
                CheckAndAdd(Me.DateTimeStamp.ValidationStatus, ls)
                With GetErrorDetail()
                    CheckAndAdd(.Source.ValidationStatus, ls)
                    CheckAndAdd(.SourceOrderNumber.ValidationStatus, ls)
                End With
            End If

            Return ls
        End Function

        Private Sub CheckAndAdd(ByVal text As String, ByRef list As List(Of String))

            If text IsNot Nothing AndAlso text.Length > 0 Then
                list.Add(text)
            End If
        End Sub
    End Class
#End Region

End Namespace