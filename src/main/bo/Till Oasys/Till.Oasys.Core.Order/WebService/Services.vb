﻿Imports Till.Oasys.Core.Order.Qod
Imports System.Text
Imports System.IO
Imports System.Xml.Serialization

Namespace WebService

#Region "BaseService Classes: Must Be Overridden"

    Public MustInherit Class BaseService
        Public MustOverride ReadOnly Property Name() As String
        Public MustOverride Function GetRequest() As BaseServiceArgument
        Public MustOverride Function GetResponse() As BaseServiceArgument
        Public MustOverride Function GetResponseXml(ByVal RequestXml As String) As String

        Protected Sub TraceInstantiatedProxy(ByVal url As String)
            TraceOutput("Instantiated with url " & url)
        End Sub

        Protected Sub TraceCallingService(ByVal requestXml As String)
            TraceOutput(String.Format("Calling webservice {0} with {1}", Name, requestXml))
        End Sub

        Protected Sub TraceOutput(ByVal message As String)
            Trace.WriteLine("Order Manager: " & message)
        End Sub

    End Class

    Public MustInherit Class BaseServiceArgument

        Public Function Serialise() As String
            Try
                Dim serializer As New XmlSerializer(Me.GetType)
                Dim sb As New StringBuilder

                Using writer As TextWriter = New StringWriter(sb)
                    serializer.Serialize(writer, Me)
                End Using
                Return sb.ToString

            Catch ex As Exception
                Throw New SerialiseException(ex)
            End Try
        End Function

        Public Function Deserialise(ByVal responseXml As String) As BaseServiceArgument
            Try
                Dim serializer As New XmlSerializer(Me.GetType)
                Dim response As BaseServiceArgument = Nothing

                Using reader As TextReader = New StringReader(responseXml)
                    response = CType(serializer.Deserialize(reader), BaseServiceArgument)
                End Using
                Return response

            Catch ex As Exception
                Throw New DeserialiseException(ex, responseXml)
            End Try
        End Function

        Public Overridable Sub SetFieldsInQod(ByRef qod As QodHeader)

        End Sub

        Public Overridable Sub SetFieldsFromQod(ByVal qod As QodHeader)

        End Sub

    End Class

#End Region

#Region "CreateService"

    'Public Class CreateService
    '    Inherits BaseService

    '    Public Overrides ReadOnly Property Name() As String
    '        Get
    '            Return "OM_Order_Manager"
    '        End Get
    '    End Property

    '    Public Overrides Function GetRequest() As BaseServiceArgument
    '        Return New OMOrderCreateRequest
    '    End Function

    '    Public Overrides Function GetResponse() As BaseServiceArgument
    '        Return New OMOrderCreateResponse
    '    End Function

    '    Public Overrides Function GetResponseXml(ByVal requestXml As String) As String
    '        Using om As New Order_Manager
    '            TraceInstantiatedProxy(om.Url)
    '            TraceCallingService(requestXml)
    '            Return om.OM_Order_Create(requestXml)
    '        End Using
    '    End Function

    'End Class

#End Region

#Region "UpdateService"

    'Public Class UpdateService
    '    Inherits BaseService

    '    Public Overrides ReadOnly Property Name() As String
    '        Get
    '            Return "OM_Order_Receive_Status_Update"
    '        End Get
    '    End Property

    '    Public Overrides Function GetRequest() As BaseServiceArgument
    '        Return New OMOrderReceiveStatusUpdateRequest
    '    End Function

    '    Public Overrides Function GetResponse() As BaseServiceArgument
    '        Return New OMOrderReceiveStatusUpdateResponse
    '    End Function

    '    Public Overrides Function GetResponseXml(ByVal requestXml As String) As String
    '        Using om As New Order_Manager
    '            TraceInstantiatedProxy(om.Url)
    '            TraceCallingService(requestXml)
    '            Return om.OM_Order_Receive_Status_Update(requestXml)
    '        End Using
    '    End Function

    'End Class

#End Region

#Region "RefundService"

    'Public Class RefundService
    '    Inherits BaseService

    '    Public Overrides ReadOnly Property Name() As String
    '        Get
    '            Return "OM_Order_Refund"
    '        End Get
    '    End Property

    '    Public Overrides Function GetRequest() As BaseServiceArgument
    '        Return New OMOrderRefundRequest
    '    End Function

    '    Public Overrides Function GetResponse() As BaseServiceArgument
    '        Return New OMOrderRefundResponse
    '    End Function

    '    Public Overrides Function GetResponseXml(ByVal requestXml As String) As String
    '        Using om As New Order_Manager
    '            TraceInstantiatedProxy(om.Url)
    '            TraceCallingService(requestXml)
    '            Return om.OM_Order_Refund(requestXml) ' TODO when web service created
    '        End Using
    '    End Function

    'End Class

#End Region

#Region "FullStatusService"

    'Public Class FullStatusService
    '    Inherits BaseService

    '    Public Overrides ReadOnly Property Name() As String
    '        Get
    '            Return "OM_Send_Full_Status_Update"
    '        End Get
    '    End Property

    '    Public Overrides Function GetRequest() As BaseServiceArgument
    '        Return New OMSendFullStatusUpdateRequest
    '    End Function

    '    Public Overrides Function GetResponse() As BaseServiceArgument
    '        Return New OMSendFullStatusUpdateResponse
    '    End Function

    '    Public Overrides Function GetResponseXml(ByVal requestXml As String) As String
    '        Using om As New Order_Manager
    '            TraceInstantiatedProxy(om.Url)
    '            TraceCallingService(requestXml)
    '            Return om.OM_Send_Full_Status_Update(requestXml)
    '        End Using
    '    End Function

    'End Class

#End Region

#Region "SourceTransactionLookupService"

    Public Class SourceTransactionLookupService
        Inherits BaseService

        Public Enum DefaultSourceTypes
            WebOrder = 0
            Venda = 1
            Till = 2
        End Enum

        ' 
        ''' <summary>
        ''' Array of possible Source values.
        ''' These might appear as prefix to Web Order Number so provide a list to check for and manage such a prefix.
        ''' Can override the default values by using the appropriate constructor overload.
        ''' </summary>
        ''' <history></history>
        ''' <remarks>
        ''' Using array to allow it to be serialisable, for use in web service.
        ''' Other types (Dictionary, ArrayList, etc) are NOT serialisable.
        ''' </remarks>
        Public Sources As String()

        Public Sub New()

            MyBase.New()
            ReDim Sources(2)
            Sources(DefaultSourceTypes.WebOrder) = "WO"
            Sources(DefaultSourceTypes.Venda) = "VE"
            Sources(DefaultSourceTypes.Till) = "TL"
        End Sub

        Public Sub New(ByVal SourcesOverride As String())

            MyBase.New()
            ' Overwrite default sources list with override values
            ReDim Sources(SourcesOverride.GetUpperBound(0))
            For SourceCount As Integer = 0 To SourcesOverride.GetUpperBound(0)
                Sources(SourceCount) = SourcesOverride(SourceCount)
            Next
        End Sub

        Public Overrides ReadOnly Property Name() As String
            Get
                Return "OM_Source_Transaction_Lookup"
            End Get
        End Property

        Public Overrides Function GetRequest() As BaseServiceArgument

            Return New OMSourceTransactionLookupRequest
        End Function

        Public Overrides Function GetResponse() As BaseServiceArgument

            Return New OMSourceTransactionLookupResponse
        End Function

        Public Overrides Function GetResponseXml(ByVal requestXml As String) As String

            Using om As New Order_Manager
                TraceInstantiatedProxy(om.Url)
                TraceCallingService(requestXml)
                Return om.OM_Source_Transaction_Lookup(requestXml)
            End Using
        End Function
    End Class

#End Region

#Region "CheckFulfimentService"

    Public Class CheckFulfimentService
        Inherits BaseService
        Implements IBaseService

        Public Overrides ReadOnly Property Name() As String Implements IBaseService.Name

            Get
                Return "OM_Check_Fulfilment"
            End Get

        End Property

        Public Overrides Function GetRequest() As BaseServiceArgument Implements IBaseService.GetRequest

            Return New OMCheckFulfilmentRequest

        End Function

        Public Overrides Function GetResponse() As BaseServiceArgument Implements IBaseService.GetResponse

            Return New OMCheckFulfilmentResponse

        End Function

        Public Overrides Function GetResponseXml(ByVal requestXml As String) As String Implements IBaseService.GetResponseXml

            Using OM As New Order_Manager

                TraceInstantiatedProxy(OM.Url)
                TraceCallingService(requestXml)
                Return OM.OM_Check_Fulfilment(requestXml)

            End Using

        End Function

    End Class

#End Region

#Region "Error classes"

    Public Class SerialiseException
        Inherits Exception

        Public Sub New(ByVal ex As Exception)

            MyBase.New("Error serialising object", ex)
        End Sub
    End Class

    Public Class DeserialiseException
        Inherits Exception

        Private _deserialisationString As String = String.Empty

        Public ReadOnly Property DeserialisationString() As String
            Get
                Return _deserialisationString
            End Get
        End Property

        Public Sub New(ByVal ex As Exception, ByVal deserialisationString As String)

            MyBase.New("Error deserialising object", ex)
            _deserialisationString = deserialisationString
        End Sub
    End Class

    Public Class InvalidFieldSettingException
        Inherits Exception

        Public Sub New(ByVal ex As Exception)

            MyBase.New("Error setting fields in object", ex)
        End Sub
    End Class

#End Region

End Namespace


