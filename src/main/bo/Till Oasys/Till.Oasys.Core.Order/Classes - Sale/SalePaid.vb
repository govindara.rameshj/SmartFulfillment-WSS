﻿Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports System.Text
Imports System.ComponentModel
Imports Till.Oasys.Data
Imports Till.Oasys.Core
Imports Till.Oasys.Core.Order.WebService

#Region "Namespace QOD"

Namespace Qod

#Region "ClassSalePaid"

    ''' <summary>
    ''' Sale Paid class 
    ''' </summary>
    ''' <remarks></remarks>
    Public Class SalePaid
        Inherits Oasys.Core.Base

#Region "Table Fields"

        Private _transactionDate As Date
        Private _tillNumber As String
        Private _transactionNumber As String
        Private _sequenceNumber As Integer
        Private _tenderType As Decimal = 1
        Private _tenderAmount As Decimal
        Private _creditCardNumber As String = "0000000000000000000"
        Private _creditCardExp As String = "0000"
        Private _couponNumber As String = "000000"
        Private _couponClass As String = "00"
        Private _ccAuthCode As String = String.Empty
        Private _ccKeyed As Boolean = True
        Private _supervisorNumber As String = "000"
        Private _customerPostCode As String = String.Empty
        Private _chequeAccNo As String = "0000000000"
        Private _sortCode As String = "000000"
        Private _chequeNumber As String = "000000"
        Private _dbrProcessed As Boolean = False
        Private _voucherSeqNumber As String = "0000"
        Private _dcIssueNumber As String = String.Empty
        Private _authType As String = String.Empty
        Private _merchantNo As String = String.Empty
        Private _cashBackAmount As Decimal = 0
        Private _digitCount As Integer = 0
        Private _commsPrepared As Boolean = True
        Private _cardDescription As String = String.Empty
        Private _eftPosTrans As String = String.Empty
        Private _pinAcknowledge As String = String.Empty
        Private _cardStartDate As String = "0000"
        Private _authDescription As String = String.Empty
        Private _securityCode As String = String.Empty
        Private _tenderStatusCode As String = String.Empty
        Private _conversionFactor As Integer = 0
        Private _conversionRate As Decimal = 0
        Private _value As Decimal = 0
        Private _rtiFlag As String = "S"

        <ColumnMapping("DATE1")> Public Property TransactionDate() As Date
            Get
                Return _transactionDate
            End Get
            Set(ByVal value As Date)
                _transactionDate = value
            End Set
        End Property
        <ColumnMapping("TILL")> Public Property TillNumber() As String
            Get
                Return _tillNumber
            End Get
            Set(ByVal value As String)
                _tillNumber = value
            End Set
        End Property
        <ColumnMapping("TRAN")> Public Property TransactionNumber() As String
            Get
                Return _transactionNumber
            End Get
            Set(ByVal value As String)
                _transactionNumber = value
            End Set
        End Property
        <ColumnMapping("NUMB")> Public Property SequenceNumber() As Integer
            Get
                Return _sequenceNumber
            End Get
            Set(ByVal value As Integer)
                _sequenceNumber = value
            End Set
        End Property
        <ColumnMapping("TYPE")> Public Property TenderType() As Decimal
            Get
                Return _tenderType
            End Get
            Set(ByVal value As Decimal)
                _tenderType = value
            End Set
        End Property
        <ColumnMapping("AMNT")> Public Property TenderAmount() As Decimal
            Get
                Return _tenderAmount
            End Get
            Set(ByVal value As Decimal)
                _tenderAmount = value
            End Set
        End Property
        <ColumnMapping("CARD")> Public Property CreditCardNumber() As String
            Get
                Return _creditCardNumber
            End Get
            Set(ByVal value As String)
                _creditCardNumber = value
            End Set
        End Property
        <ColumnMapping("EXDT")> Public Property CreditCardExp() As String
            Get
                Return _creditCardExp
            End Get
            Set(ByVal value As String)
                _creditCardExp = value
            End Set
        End Property
        <ColumnMapping("COPN")> Public Property CouponNumber() As String
            Get
                Return _couponNumber
            End Get
            Set(ByVal value As String)
                _couponNumber = value
            End Set
        End Property
        <ColumnMapping("CLAS")> Public Property CouponClass() As String
            Get
                Return _couponClass
            End Get
            Set(ByVal value As String)
                _couponClass = value
            End Set
        End Property
        <ColumnMapping("AUTH")> Public Property CCAuthCode() As String
            Get
                Return _ccAuthCode
            End Get
            Set(ByVal value As String)
                _ccAuthCode = value
            End Set
        End Property
        <ColumnMapping("CKEY")> Public Property CCKeyed() As Boolean
            Get
                Return _ccKeyed
            End Get
            Set(ByVal value As Boolean)
                _ccKeyed = value
            End Set
        End Property
        <ColumnMapping("SUPV")> Public Property SupervisorNumber() As String
            Get
                Return _supervisorNumber
            End Get
            Set(ByVal value As String)
                _supervisorNumber = value
            End Set
        End Property
        <ColumnMapping("POST")> Public Property CustomerPostCode() As String
            Get
                Return _customerPostCode
            End Get
            Set(ByVal value As String)
                _customerPostCode = value
            End Set
        End Property
        <ColumnMapping("CKAC")> Public Property ChequeAccNo() As String
            Get
                Return _chequeAccNo
            End Get
            Set(ByVal value As String)
                _chequeAccNo = value
            End Set
        End Property
        <ColumnMapping("CKSC")> Public Property SortCode() As String
            Get
                Return _sortCode
            End Get
            Set(ByVal value As String)
                _sortCode = value
            End Set
        End Property
        <ColumnMapping("CKNO")> Public Property ChequeNumber() As String
            Get
                Return _chequeNumber
            End Get
            Set(ByVal value As String)
                _chequeNumber = value
            End Set
        End Property
        <ColumnMapping("DBRF")> Public Property DBRProcessed() As Boolean
            Get
                Return _dbrProcessed
            End Get
            Set(ByVal value As Boolean)
                _dbrProcessed = value
            End Set
        End Property
        <ColumnMapping("SEQN")> Public Property VoucherSeqNo() As String
            Get
                Return _voucherSeqNumber
            End Get
            Set(ByVal value As String)
                _voucherSeqNumber = value
            End Set
        End Property
        <ColumnMapping("ISSU")> Public Property DCIssueNo() As String
            Get
                Return _dcIssueNumber
            End Get
            Set(ByVal value As String)
                _dcIssueNumber = value
            End Set
        End Property
        <ColumnMapping("MERC")> Public Property MerchantNo() As String
            Get
                Return _merchantNo
            End Get
            Set(ByVal value As String)
                _merchantNo = value
            End Set
        End Property
        <ColumnMapping("CBAM")> Public Property CashBackAmount() As Decimal
            Get
                Return _cashBackAmount
            End Get
            Set(ByVal value As Decimal)
                _cashBackAmount = value
            End Set
        End Property
        <ColumnMapping("DIGC")> Public Property DigitCount() As Integer
            Get
                Return _digitCount
            End Get
            Set(ByVal value As Integer)
                _digitCount = value
            End Set
        End Property

        <ColumnMapping("RTI")> Public Property RTIFlag() As String
            Get
                Return _rtiFlag
            End Get
            Set(ByVal value As String)
                _rtiFlag = value
            End Set
        End Property
        <ColumnMapping("ECOM")> Public Property CommsPrepared() As Boolean
            Get
                Return _commsPrepared
            End Get
            Set(ByVal value As Boolean)
                _commsPrepared = value
            End Set
        End Property
        <ColumnMapping("CTYP")> Public Property CardDescription() As String
            Get
                Return _cardDescription
            End Get
            Set(ByVal value As String)
                _cardDescription = value
            End Set
        End Property
        <ColumnMapping("EFID")> Public Property EFTPOSTransID() As String
            Get
                Return _eftPosTrans
            End Get
            Set(ByVal value As String)
                _eftPosTrans = value
            End Set
        End Property
        <ColumnMapping("EFTC")> Public Property PinAcknowledge() As String
            Get
                Return _pinAcknowledge
            End Get
            Set(ByVal value As String)
                _pinAcknowledge = value
            End Set
        End Property
        <ColumnMapping("STDT")> Public Property CardStartDate() As String
            Get
                Return _cardStartDate
            End Get
            Set(ByVal value As String)
                _cardStartDate = value
            End Set
        End Property
        <ColumnMapping("AUTHDESC")> Public Property AuthDescription() As String
            Get
                Return _authDescription
            End Get
            Set(ByVal value As String)
                _authDescription = value
            End Set
        End Property
        <ColumnMapping("ATYP")> Public Property AuthType() As String
            Get
                Return _authType
            End Get
            Set(ByVal value As String)
                _authType = value
            End Set
        End Property
        <ColumnMapping("SECCODE")> Public Property SecurityCode() As String
            Get
                Return _securityCode
            End Get
            Set(ByVal value As String)
                _securityCode = value
            End Set
        End Property
        <ColumnMapping("TENC")> Public Property TenderStatusCode() As String
            Get
                Return _tenderStatusCode
            End Get
            Set(ByVal value As String)
                _tenderStatusCode = value
            End Set
        End Property
        <ColumnMapping("MPOW")> Public Property ConversionFactor() As Integer
            Get
                Return _conversionFactor
            End Get
            Set(ByVal value As Integer)
                _conversionFactor = value
            End Set
        End Property

        <ColumnMapping("MRAT")> Public Property ConversionRate() As Decimal
            Get
                Return _conversionRate
            End Get
            Set(ByVal value As Decimal)
                _conversionRate = value
            End Set
        End Property
        <ColumnMapping("TENV")> Public Property Value() As Decimal
            Get
                Return _value
            End Get
            Set(ByVal value As Decimal)
                _value = value
            End Set
        End Property
#End Region

#Region "Properties"
        Private _ExistsInDB As Boolean
        Public Property ExistsInDB() As Boolean
            Get
                Return _ExistsInDB
            End Get
            Set(ByVal value As Boolean)
                _ExistsInDB = value
            End Set
        End Property
#End Region

#Region "Methods"

        Public Sub New()
            MyBase.New()
        End Sub

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub

        Public Overloads Sub Load(ByVal tranDate As Date, ByVal tranNumber As String, ByVal tillId As String)
            Dim dt As DataTable = DataAccess.SalePaidGet(tranDate, tranNumber, tillId)
            Me.Load(dt.Rows(0))
        End Sub

        Public Function PersistNew(ByVal con As Connection) As Integer

            Dim LinesUpdated As Integer = 0

            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        Dim sb As New StringBuilder
                        sb.Append("Insert into DLPAID (")
                        sb.Append("DATE1,TILL,TRAN,NUMB,TYPE,AMNT,CARD,EXDT,COPN,CLAS,AUTH,")
                        sb.Append("CKEY,SUPV,POST,CKAC,CKSC,CKNO,DBRF,SEQN,ISSU,ATYP,MERC,CBAM,DIGC,ECOM,CTYP,")
                        sb.Append("EFID,EFTC,STDT,AUTHDESC,SECCODE,TENC,MPOW,MRAT,TENV,RTI")
                        sb.Append(") values	(")
                        sb.Append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )")

                        com.CommandText = sb.ToString
                        com.AddParameter("TransactionDate", TransactionDate)
                        com.AddParameter("TillNumber", TillNumber)
                        com.AddParameter("TransactionNumber", TransactionNumber)
                        com.AddParameter("SequenceNumber", SequenceNumber)
                        com.AddParameter("TenderType", TenderType)
                        com.AddParameter("TenderAmount", TenderAmount)
                        com.AddParameter("CreditCardNumber", IIf(CreditCardNumber IsNot Nothing, CreditCardNumber, "0000000000000000000"))
                        com.AddParameter("CreditCardExp", IIf(CreditCardExp IsNot Nothing, CreditCardExp, "0000"))
                        com.AddParameter("CouponNumber", IIf(CouponNumber IsNot Nothing, CouponNumber, "000000"))
                        com.AddParameter("CouponClass", IIf(CouponClass IsNot Nothing, CouponClass, "00"))
                        com.AddParameter("CCAuthCode", IIf(CCAuthCode IsNot Nothing, CCAuthCode, String.Empty))
                        com.AddParameter("CCKeyed", CCKeyed)
                        com.AddParameter("SupervisorNumber", IIf(SupervisorNumber IsNot Nothing, SupervisorNumber, "000"))
                        com.AddParameter("CustomerPostcode", IIf(CustomerPostCode IsNot Nothing, CustomerPostCode, String.Empty))
                        com.AddParameter("ChequeAccNo", IIf(ChequeAccNo IsNot Nothing, ChequeAccNo, "0000000000"))
                        com.AddParameter("SortCode", IIf(SortCode IsNot Nothing, SortCode, "000000"))
                        com.AddParameter("ChequeNumber", IIf(ChequeNumber IsNot Nothing, ChequeNumber, "000000"))
                        com.AddParameter("DBRProcessed", DBRProcessed)
                        com.AddParameter("VoucherSeqNo", IIf(VoucherSeqNo IsNot Nothing, VoucherSeqNo, "0000"))
                        com.AddParameter("DCIssueNo", IIf(DCIssueNo IsNot Nothing, DCIssueNo, String.Empty))
                        com.AddParameter("AuthType", IIf(AuthType IsNot Nothing, AuthType, String.Empty))
                        com.AddParameter("MerchantNo", IIf(MerchantNo IsNot Nothing, MerchantNo, String.Empty))
                        com.AddParameter("CashBackAmount", CashBackAmount)
                        com.AddParameter("DigitCount", DigitCount)
                        com.AddParameter("CommsPrepared", CommsPrepared)
                        com.AddParameter("CardDescription", IIf(CardDescription IsNot Nothing, CardDescription, String.Empty))
                        com.AddParameter("EFTPOSTransID", IIf(EFTPOSTransID IsNot Nothing, EFTPOSTransID, String.Empty))
                        com.AddParameter("PinAcknowledge", IIf(PinAcknowledge IsNot Nothing, PinAcknowledge, String.Empty))
                        com.AddParameter("CardStartDate", IIf(CardStartDate IsNot Nothing, CardStartDate, "0000"))
                        com.AddParameter("AuthDescription", IIf(AuthDescription IsNot Nothing, AuthDescription, String.Empty))
                        com.AddParameter("SecurityCode", IIf(SecurityCode IsNot Nothing, SecurityCode, String.Empty))
                        com.AddParameter("TenderStatusCode", IIf(TenderStatusCode IsNot Nothing, TenderStatusCode, String.Empty))
                        com.AddParameter("ConversionFactor", ConversionFactor)
                        com.AddParameter("ConversionRate", ConversionRate)
                        com.AddParameter("Value", Value)
                        com.AddParameter("RTIFlag", "S")
                        LinesUpdated += com.ExecuteNonQuery()

                    Case DataProvider.Sql
                        com.StoredProcedureName = My.Resources.Procedures.SalePaidInsert
                        com.AddParameter(My.Resources.Parameters.TransactionDate, TransactionDate)
                        com.AddParameter(My.Resources.Parameters.TillNumber, TillNumber)
                        com.AddParameter(My.Resources.Parameters.TransactionNumber, TransactionNumber)
                        com.AddParameter(My.Resources.Parameters.SequenceNumber, SequenceNumber)
                        com.AddParameter(My.Resources.Parameters.TenderType, TenderType)
                        com.AddParameter(My.Resources.Parameters.TenderAmount, TenderAmount)
                        If CreditCardNumber IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.CreditCardNumber, CreditCardNumber)
                        If CreditCardExp IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.CreditCardExp, CreditCardExp)
                        If CouponNumber IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.CouponNumber, CouponNumber)
                        If CouponClass IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.CouponClass, CouponClass)
                        If CCAuthCode IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.CCAuthCode, CCAuthCode)
                        com.AddParameter(My.Resources.Parameters.CCKeyed, CCKeyed)
                        If SupervisorNumber IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.SupervisorNumber, SupervisorNumber)
                        If CustomerPostCode IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.CustomerPostcode, CustomerPostCode)
                        If ChequeAccNo IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.ChequeAccNo, ChequeAccNo)
                        If SortCode IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.SortCode, SortCode)
                        If ChequeNumber IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.ChequeNumber, ChequeNumber)
                        com.AddParameter(My.Resources.Parameters.DBRProcessed, DBRProcessed)
                        If VoucherSeqNo IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.VoucherSeqNo, VoucherSeqNo)
                        If DCIssueNo IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.DCIssueNo, DCIssueNo)
                        If AuthType IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.AuthType, AuthType)
                        If MerchantNo IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.MerchantNo, MerchantNo)
                        com.AddParameter(My.Resources.Parameters.CashBackAmount, CashBackAmount)
                        com.AddParameter(My.Resources.Parameters.DigitCount, DigitCount)
                        com.AddParameter(My.Resources.Parameters.CommsPrepared, CommsPrepared)
                        If CardDescription IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.CardDescription, CardDescription)
                        If EFTPOSTransID IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.EFTPOSTransID, EFTPOSTransID)
                        If PinAcknowledge IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.PinAcknowledge, PinAcknowledge)
                        If CardStartDate IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.CardStartDate, CardStartDate)
                        If AuthDescription IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.AuthDescription, AuthDescription)
                        If SecurityCode IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.SecurityCode, SecurityCode)
                        If TenderStatusCode IsNot Nothing Then com.AddParameter _
                        (My.Resources.Parameters.TenderStatusCode, TenderStatusCode)
                        com.AddParameter(My.Resources.Parameters.ConversionFactor, ConversionFactor)
                        com.AddParameter(My.Resources.Parameters.ConversionRate, ConversionRate)
                        com.AddParameter(My.Resources.Parameters.Value, Value)
                        LinesUpdated += com.ExecuteNonQuery()

                End Select
            End Using
            Return LinesUpdated

        End Function

        Public Function Persist(ByVal con As Connection) As Integer

            If Not ExistsInDB Then
                Return PersistNew(con)
            Else
                Dim LinesUpdated As Integer = 0

                Using com As New Command(con)
                    Select Case con.DataProvider
                        Case DataProvider.Odbc
                            Dim sb As New StringBuilder
                            sb.Append("Insert into DLPAID (")
                            sb.Append("DATE1,TILL,TRAN,NUMB,TYPE,AMNT,CARD,EXDT,COPN,CLAS,AUTH,")
                            sb.Append("CKEY,SUPV,POST,CKAC,CKSC,CKNO,DBRF,SEQN,ISSU,ATYP,MERC,CBAM,DIGC,ECOM,CTYP,")
                            sb.Append("EFID,EFTC,STDT,AUTHDESC,SECCODE,TENC,MPOW,MRAT,TENV,RTI")
                            sb.Append(") values	(")
                            sb.Append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )")

                            com.CommandText = sb.ToString
                            com.AddParameter("TransactionDate", TransactionDate)
                            com.AddParameter("TillNumber", TillNumber)
                            com.AddParameter("TransactionNumber", TransactionNumber)
                            com.AddParameter("SequenceNumber", SequenceNumber)
                            com.AddParameter("TenderType", TenderType)
                            com.AddParameter("TenderAmount", TenderAmount)
                            com.AddParameter("CreditCardNumber", IIf(CreditCardNumber IsNot Nothing, CreditCardNumber, "0000000000000000000"))
                            com.AddParameter("CreditCardExp", IIf(CreditCardExp IsNot Nothing, CreditCardExp, "0000"))
                            com.AddParameter("CouponNumber", IIf(CouponNumber IsNot Nothing, CouponNumber, "000000"))
                            com.AddParameter("CouponClass", IIf(CouponClass IsNot Nothing, CouponClass, "00"))
                            com.AddParameter("CCAuthCode", IIf(CouponClass IsNot Nothing, CouponClass, String.Empty))
                            com.AddParameter("CCKeyed", CCKeyed)
                            com.AddParameter("SupervisorNumber", IIf(SupervisorNumber IsNot Nothing, SupervisorNumber, "000"))
                            com.AddParameter("CustomerPostcode", IIf(CustomerPostCode IsNot Nothing, CustomerPostCode, String.Empty))
                            com.AddParameter("ChequeAccNo", IIf(ChequeAccNo IsNot Nothing, ChequeAccNo, "0000000000"))
                            com.AddParameter("SortCode", IIf(SortCode IsNot Nothing, SortCode, "000000"))
                            com.AddParameter("ChequeNumber", IIf(ChequeNumber IsNot Nothing, ChequeNumber, "000000"))
                            com.AddParameter("DBRProcessed", CCKeyed)
                            com.AddParameter("VoucherSeqNo", IIf(VoucherSeqNo IsNot Nothing, VoucherSeqNo, "0000"))
                            com.AddParameter("DCIssueNo", IIf(DCIssueNo IsNot Nothing, DCIssueNo, String.Empty))
                            com.AddParameter("AuthType", IIf(AuthType IsNot Nothing, AuthType, String.Empty))
                            com.AddParameter("MerchantNo", IIf(MerchantNo IsNot Nothing, MerchantNo, String.Empty))
                            com.AddParameter("CashBackAmount", CashBackAmount)
                            com.AddParameter("DigitCount", DigitCount)
                            com.AddParameter("CommsPrepared", CommsPrepared)
                            com.AddParameter("CardDescription", IIf(CardDescription IsNot Nothing, CardDescription, String.Empty))
                            com.AddParameter("EFTPOSTransID", IIf(EFTPOSTransID IsNot Nothing, EFTPOSTransID, String.Empty))
                            com.AddParameter("PinAcknowledge", IIf(PinAcknowledge IsNot Nothing, PinAcknowledge, String.Empty))
                            com.AddParameter("CardStartDate", IIf(CardStartDate IsNot Nothing, CardStartDate, "0000"))
                            com.AddParameter("AuthDescription", IIf(AuthDescription IsNot Nothing, AuthDescription, String.Empty))
                            com.AddParameter("SecurityCode", IIf(SecurityCode IsNot Nothing, SecurityCode, String.Empty))
                            com.AddParameter("TenderStatusCode", IIf(TenderStatusCode IsNot Nothing, TenderStatusCode, String.Empty))
                            com.AddParameter("ConversionFactor", ConversionFactor)
                            com.AddParameter("ConversionRate", ConversionRate)
                            com.AddParameter("Value", Value)
                            com.AddParameter("RTIFlag", "S")
                            LinesUpdated += com.ExecuteNonQuery()

                        Case DataProvider.Sql
                            com.StoredProcedureName = My.Resources.Procedures.SalePaidInsert
                            com.AddParameter(My.Resources.Parameters.TransactionDate, TransactionDate)
                            com.AddParameter(My.Resources.Parameters.TillNumber, TillNumber)
                            com.AddParameter(My.Resources.Parameters.TransactionNumber, TransactionNumber)
                            com.AddParameter(My.Resources.Parameters.SequenceNumber, SequenceNumber)
                            com.AddParameter(My.Resources.Parameters.TenderType, TenderType)
                            com.AddParameter(My.Resources.Parameters.TenderAmount, TenderAmount)
                            If CreditCardNumber IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.CreditCardNumber, CreditCardNumber)
                            If CreditCardExp IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.CreditCardExp, CreditCardExp)
                            If CouponNumber IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.CouponNumber, CouponNumber)
                            If CouponClass IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.CouponClass, CouponClass)
                            If CCAuthCode IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.CCAuthCode, CCAuthCode)
                            com.AddParameter(My.Resources.Parameters.CCKeyed, CCKeyed)
                            If SupervisorNumber IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.SupervisorNumber, SupervisorNumber)
                            If CustomerPostCode IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.CustomerPostcode, CustomerPostCode)
                            If ChequeAccNo IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.ChequeAccNo, ChequeAccNo)
                            If SortCode IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.SortCode, SortCode)
                            If ChequeNumber IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.ChequeNumber, ChequeNumber)
                            com.AddParameter(My.Resources.Parameters.DBRProcessed, DBRProcessed)
                            If VoucherSeqNo IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.VoucherSeqNo, VoucherSeqNo)
                            If DCIssueNo IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.DCIssueNo, DCIssueNo)
                            If AuthType IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.AuthType, AuthType)
                            If MerchantNo IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.MerchantNo, MerchantNo)
                            com.AddParameter(My.Resources.Parameters.CashBackAmount, CashBackAmount)
                            com.AddParameter(My.Resources.Parameters.DigitCount, DigitCount)
                            com.AddParameter(My.Resources.Parameters.CommsPrepared, CommsPrepared)
                            If CardDescription IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.CardDescription, CardDescription)
                            If EFTPOSTransID IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.EFTPOSTransID, EFTPOSTransID)
                            If PinAcknowledge IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.PinAcknowledge, PinAcknowledge)
                            If CardStartDate IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.CardStartDate, CardStartDate)
                            If AuthDescription IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.AuthDescription, AuthDescription)
                            If SecurityCode IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.SecurityCode, SecurityCode)
                            If TenderStatusCode IsNot Nothing Then com.AddParameter _
                            (My.Resources.Parameters.TenderStatusCode, TenderStatusCode)
                            com.AddParameter(My.Resources.Parameters.ConversionFactor, ConversionFactor)
                            com.AddParameter(My.Resources.Parameters.ConversionRate, ConversionRate)
                            com.AddParameter(My.Resources.Parameters.Value, Value)
                            LinesUpdated += com.ExecuteNonQuery()

                    End Select
                End Using
                Return LinesUpdated

            End If

        End Function

#End Region

    End Class

#End Region

End Namespace

#End Region
