﻿Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports System.Text
Imports System.ComponentModel
Imports Till.Oasys.Data
Imports Till.Oasys.Core
Imports Till.Oasys.Core.Order.WebService

#Region "QodNamespace"
Namespace Qod

#Region "ClassSaleLine"

    ''' <summary>
    ''' SaleLineClass used to hold Sale Line Data
    ''' </summary>
    ''' <remarks></remarks>

    Public Class SaleLine
        Inherits Oasys.Core.Base

#Region "       Table Fields"

        Private _transactionDate As Date
        Private _tillNumber As String
        Private _transactionNumber As String
        Private _lineNumber As Integer
        Private _skuNumber As String
        Private _department As String = "00"
        Private _barcodeUsed As Boolean = False
        Private _supervisorNo As String = "000"
        Private _quantity As Decimal
        Private _systemLookupPrice As Decimal
        Private _itemPrice As Decimal
        Private _itemExcVatPrice As Decimal
        Private _extendedValue As Decimal
        Private _extendedCost As Decimal = 0
        Private _relatedItems As Boolean?
        Private _priceOverride As Integer = 0
        Private _taggedItem As Boolean?
        Private _catchAllItem As Boolean = False
        Private _vatSymbol As String
        Private _tempPrceChangeDiff As Decimal = 0
        Private _tpMarginErosion As String = "000000"
        Private _priceOverrideDiff As Decimal = 0
        Private _poMarginErosion As String = "000000"
        Private _qtyBreakErosion As Decimal = 0
        Private _qbMarginErosion As String = "000000"
        Private _dealGroupErosion As Decimal = 0
        Private _dgMarginErosion As String = "000000"
        Private _multiBuyErosion As Decimal = 0
        Private _mbMarginErosion As String = "000000"
        Private _hierarchyErosion As Decimal = 0
        Private _hiMarginErosion As String = "000000"
        Private _priEmpDiscPriceDiff As Decimal = 0
        Private _empMarginErosion As String = "000000"
        Private _lineReversed As Boolean = False
        Private _lastEventSeqNo As String = "000000"
        Private _hierCategory As String = String.Empty
        Private _hierGroup As String = String.Empty
        Private _hierSubGroup As String = String.Empty
        Private _hierStyle As String = String.Empty
        Private _partial As String = "000"
        Private _secondEmpSale As Decimal = 0
        Private _markDownStock As Boolean = False
        Private _saleTypeAtt As String
        Private _vatCode As Decimal
        Private _vatValue As Decimal
        Private _backDoor As String = "N"
        Private _backDoorBool As Boolean = False
        Private _lineReverseCode As String = String.Empty
        Private _rtiFlag As String = "S"
        Private _weeRate As String = "00"
        Private _weeSequence As String = "00"
        Private _weeCost As Decimal = 0

        <ColumnMapping("DATE1")> Public Property TransactionDate() As Date
            Get
                Return _transactionDate
            End Get
            Set(ByVal value As Date)
                _transactionDate = value
            End Set
        End Property
        <ColumnMapping("TILL")> Public Property TillNumber() As String
            Get
                Return _tillNumber
            End Get
            Set(ByVal value As String)
                _tillNumber = value
            End Set
        End Property
        <ColumnMapping("TRAN")> Public Property TransactionNumber() As String
            Get
                Return _transactionNumber
            End Get
            Set(ByVal value As String)
                _transactionNumber = value
            End Set
        End Property
        <ColumnMapping("NUMB")> Public Property LineNumber() As Integer
            Get
                Return _lineNumber
            End Get
            Set(ByVal value As Integer)
                _lineNumber = value
            End Set
        End Property
        <ColumnMapping("SKUN")> Public Property SKUNumber() As String
            Get
                Return _skuNumber
            End Get
            Set(ByVal value As String)
                _skuNumber = value
            End Set
        End Property
        <ColumnMapping("DEPT")> Public Property Department() As String
            Get
                Return _department
            End Get
            Set(ByVal value As String)
                _department = value
            End Set
        End Property
        <ColumnMapping("IBAR")> Public Property BarcodeUsed() As Boolean
            Get
                Return _barcodeUsed
            End Get
            Set(ByVal value As Boolean)
                _barcodeUsed = value
            End Set
        End Property
        <ColumnMapping("SUPV")> Public Property SupervisorNumber() As String
            Get
                Return _supervisorNo
            End Get
            Set(ByVal value As String)
                _supervisorNo = value
            End Set
        End Property
        <ColumnMapping("QUAN")> Public Property Quantity() As Decimal
            Get
                Return _quantity
            End Get
            Set(ByVal value As Decimal)
                _quantity = value
            End Set
        End Property
        <ColumnMapping("SPRI")> Public Property SystemLookupPrice() As Decimal
            Get
                Return _systemLookupPrice
            End Get
            Set(ByVal value As Decimal)
                _systemLookupPrice = value
            End Set
        End Property
        <ColumnMapping("PRIC")> Public Property ItemPrice() As Decimal
            Get
                Return _itemPrice
            End Get
            Set(ByVal value As Decimal)
                _itemPrice = value
            End Set
        End Property
        <ColumnMapping("PRVE")> Public Property ItemExcVatPrice() As Decimal
            Get
                Return _itemExcVatPrice
            End Get
            Set(ByVal value As Decimal)
                _itemExcVatPrice = value
            End Set
        End Property
        <ColumnMapping("EXTP")> Public Property ExtendedValue() As Decimal
            Get
                Return _extendedValue
            End Get
            Set(ByVal value As Decimal)
                _extendedValue = value
            End Set
        End Property
        <ColumnMapping("EXTC")> Public Property ExtendedCost() As Decimal
            Get
                Return _extendedCost
            End Get
            Set(ByVal value As Decimal)
                _extendedCost = value
            End Set
        End Property
        <ColumnMapping("RITM")> Public Property RelatedItemsSingle() As Boolean?
            Get
                Return _relatedItems
            End Get
            Set(ByVal value As Boolean?)
                _relatedItems = value
            End Set
        End Property
        <ColumnMapping("PORC")> Public Property PriceOverrideCode() As Integer
            Get
                Return _priceOverride
            End Get
            Set(ByVal value As Integer)
                _priceOverride = value
            End Set
        End Property
        <ColumnMapping("ITAG")> Public Property TaggedItem() As Boolean?
            Get
                Return _taggedItem
            End Get
            Set(ByVal value As Boolean?)
                _taggedItem = value
            End Set
        End Property
        <ColumnMapping("CATA")> Public Property CatchAllItem() As Boolean
            Get
                Return _catchAllItem
            End Get
            Set(ByVal value As Boolean)
                _catchAllItem = value
            End Set
        End Property
        <ColumnMapping("VSYM")> Public Property VatSymbol() As String
            Get
                Return _vatSymbol
            End Get
            Set(ByVal value As String)
                _vatSymbol = value
            End Set
        End Property
        <ColumnMapping("TPPD")> Public Property TempPriceChangeDiff() As Decimal
            Get
                Return _tempPrceChangeDiff
            End Get
            Set(ByVal value As Decimal)
                _tempPrceChangeDiff = value
            End Set
        End Property
        <ColumnMapping("TPME")> Public Property TPMarginErosionCode() As String
            Get
                Return _tpMarginErosion
            End Get
            Set(ByVal value As String)
                _tpMarginErosion = value
            End Set
        End Property
        <ColumnMapping("POPD")> Public Property PriceOverrideDiff() As Decimal
            Get
                Return _priceOverrideDiff
            End Get
            Set(ByVal value As Decimal)
                _priceOverrideDiff = value
            End Set
        End Property
        <ColumnMapping("POME")> Public Property POMarginErosionCode() As String
            Get
                Return _poMarginErosion
            End Get
            Set(ByVal value As String)
                _poMarginErosion = value
            End Set
        End Property
        <ColumnMapping("QBPD")> Public Property QuantityBreakErosion() As Decimal
            Get
                Return _qtyBreakErosion
            End Get
            Set(ByVal value As Decimal)
                _qtyBreakErosion = value
            End Set
        End Property
        <ColumnMapping("QBME")> Public Property QBMarginErosionCode() As String
            Get
                Return _qbMarginErosion
            End Get
            Set(ByVal value As String)
                _qbMarginErosion = value
            End Set
        End Property
        <ColumnMapping("DGPD")> Public Property DealGroupErosion() As Decimal
            Get
                Return _dealGroupErosion
            End Get
            Set(ByVal value As Decimal)
                _dealGroupErosion = value
            End Set
        End Property
        <ColumnMapping("DGME")> Public Property DGMarginErosionCode() As String
            Get
                Return _dgMarginErosion
            End Get
            Set(ByVal value As String)
                _dgMarginErosion = value
            End Set
        End Property
        <ColumnMapping("MBPD")> Public Property MultibuyErosion() As Decimal
            Get
                Return _multiBuyErosion
            End Get
            Set(ByVal value As Decimal)
                _multiBuyErosion = value
            End Set
        End Property
        <ColumnMapping("MBME")> Public Property MBMarginErosionCode() As String
            Get
                Return _mbMarginErosion
            End Get
            Set(ByVal value As String)
                _mbMarginErosion = value
            End Set
        End Property
        <ColumnMapping("HSPD")> Public Property HierarchyErosion() As Decimal
            Get
                Return _hierarchyErosion
            End Get
            Set(ByVal value As Decimal)
                _hierarchyErosion = value
            End Set
        End Property
        <ColumnMapping("HSME")> Public Property HIMarginErosionCode() As String
            Get
                Return _hiMarginErosion
            End Get
            Set(ByVal value As String)
                _hiMarginErosion = value
            End Set
        End Property
        <ColumnMapping("ESPD")> Public Property PriEmpDiscPriceDiff() As Decimal
            Get
                Return _priEmpDiscPriceDiff
            End Get
            Set(ByVal value As Decimal)
                _priEmpDiscPriceDiff = value
            End Set
        End Property
        <ColumnMapping("ESME")> Public Property EmpMarginErosionCode() As String
            Get
                Return _empMarginErosion
            End Get
            Set(ByVal value As String)
                _empMarginErosion = value
            End Set
        End Property
        <ColumnMapping("LREV")> Public Property LineReversed() As Boolean
            Get
                Return _lineReversed
            End Get
            Set(ByVal value As Boolean)
                _lineReversed = value
            End Set
        End Property
        <ColumnMapping("ESEQ")> Public Property LastEventSeqNo() As String
            Get
                Return _lastEventSeqNo
            End Get
            Set(ByVal value As String)
                _lastEventSeqNo = value
            End Set
        End Property
        <ColumnMapping("CTGY")> Public Property HierCategory() As String
            Get
                Return _hierCategory
            End Get
            Set(ByVal value As String)
                _hierCategory = value
            End Set
        End Property
        <ColumnMapping("GRUP")> Public Property HierGroup() As String
            Get
                Return _hierGroup
            End Get
            Set(ByVal value As String)
                _hierGroup = value
            End Set
        End Property
        <ColumnMapping("SGRP")> Public Property HierSubGroup() As String
            Get
                Return _hierSubGroup
            End Get
            Set(ByVal value As String)
                _hierSubGroup = value
            End Set
        End Property
        <ColumnMapping("STYL")> Public Property HierStyle() As String
            Get
                Return _hierStyle
            End Get
            Set(ByVal value As String)
                _hierStyle = value
            End Set
        End Property
        <ColumnMapping("QSUP")> Public Property PartialQuarantine() As String
            Get
                Return _partial
            End Get
            Set(ByVal value As String)
                _partial = value
            End Set
        End Property
        <ColumnMapping("ESEV")> Public Property SecondEmpSale() As Decimal
            Get
                Return _secondEmpSale
            End Get
            Set(ByVal value As Decimal)
                _secondEmpSale = value
            End Set
        End Property
        <ColumnMapping("IMDN")> Public Property MarkDownStock() As Boolean
            Get
                Return _markDownStock
            End Get
            Set(ByVal value As Boolean)
                _markDownStock = value
            End Set
        End Property
        <ColumnMapping("SALT")> Public Property SaleTypeAtt() As String
            Get
                Return _saleTypeAtt
            End Get
            Set(ByVal value As String)
                _saleTypeAtt = value
            End Set
        End Property
        <ColumnMapping("VATN")> Public Property VatCode() As Decimal
            Get
                Return _vatCode
            End Get
            Set(ByVal value As Decimal)
                _vatCode = value
            End Set
        End Property
        <ColumnMapping("VATV")> Public Property VatValue() As Decimal
            Get
                Return _vatValue
            End Get
            Set(ByVal value As Decimal)
                _vatValue = value
            End Set
        End Property

        <ColumnMapping("BDCO")> Public Property BackDoor() As String
            Get
                Return _backDoor
            End Get
            Set(ByVal value As String)
                _backDoor = value
            End Set
        End Property
        <ColumnMapping("BDCOInd")> Public Property BackDoorBool() As Boolean
            Get
                Return _backDoorBool
            End Get
            Set(ByVal value As Boolean)
                _backDoorBool = value
            End Set
        End Property
        <ColumnMapping("RCOD")> Public Property LineReverseCode() As String
            Get
                Return _lineReverseCode
            End Get
            Set(ByVal value As String)
                _lineReverseCode = value
            End Set
        End Property
        <ColumnMapping("RTI")> Public Property RTIFlag() As String
            Get
                Return _rtiFlag
            End Get
            Set(ByVal value As String)
                _rtiFlag = value
            End Set
        End Property
        <ColumnMapping("WEERATE")> Public Property WeeRate() As String
            Get
                Return _weeRate
            End Get
            Set(ByVal value As String)
                _weeRate = value
            End Set
        End Property
        <ColumnMapping("WEESEQN")> Public Property WeeSequence() As String
            Get
                Return _weeSequence
            End Get
            Set(ByVal value As String)
                _weeSequence = value
            End Set
        End Property
        <ColumnMapping("WEECOST")> Public Property WeeCost() As Decimal
            Get
                Return _weeCost
            End Get
            Set(ByVal value As Decimal)
                _weeCost = value
            End Set
        End Property

#End Region

#Region "Properties"
        Private _ExistsInDB As Boolean
        Public Property ExistsInDB() As Boolean
            Get
                Return _ExistsInDB
            End Get
            Set(ByVal value As Boolean)
                _ExistsInDB = value
            End Set
        End Property
#End Region

#Region "Methods"

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub
        Public Sub New()
            MyBase.New()
        End Sub

        Public Function PersistNew(ByVal con As Connection) As Integer

            Dim LinesUpdated As Integer = 0

            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Odbc

                        Dim sb As New StringBuilder
                        sb.Append("Insert into DLLINE (")
                        sb.Append("DATE1 ,TILL ,TRAN ,NUMB ,SKUN ,DEPT ,IBAR ,SUPV ,QUAN ,SPRI ,PRIC ,PRVE ,")
                        sb.Append("EXTP ,EXTC ,RITM ,PORC ,ITAG ,CATA ,VSYM ,TPPD ,TPME ,POPD ,POME ,QBPD ,")
                        sb.Append("QBME ,DGPD ,DGME ,MBPD ,MBME ,HSPD ,HSME ,ESPD ,ESME ,LREV ,ESEQ ,CTGY ,")
                        sb.Append("GRUP ,SGRP ,STYL ,QSUP ,ESEV ,IMDN ,SALT ,VATN ,VATV ,BDCO ,BDCOInd ,RCOD ,")
                        sb.Append("RTI ,WEERATE ,WEESEQN ,WEECOST")
                        sb.Append(") values (")
                        sb.Append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")

                        com.CommandText = sb.ToString
                        com.AddParameter("TranDate", TransactionDate)
                        com.AddParameter("TillNo", TillNumber)
                        com.AddParameter("TranNo", TransactionNumber)
                        com.AddParameter("Numb", LineNumber)
                        com.AddParameter("SKU", SKUNumber)
                        com.AddParameter("Dept", Department)
                        com.AddParameter("Barcode", BarcodeUsed)
                        com.AddParameter("SupNo", SupervisorNumber)
                        com.AddParameter("Quantity", Quantity)
                        com.AddParameter("SPrice", SystemLookupPrice)
                        com.AddParameter("Price", ItemPrice)
                        com.AddParameter("ItemPrice", ItemExcVatPrice)
                        com.AddParameter("ExtendValue", ExtendedValue)
                        com.AddParameter("ExtendCost", ExtendedCost)
                        com.AddParameter("RelItem", RelatedItemsSingle)
                        com.AddParameter("POReasCode", PriceOverrideCode)
                        com.AddParameter("Tagged", TaggedItem)
                        com.AddParameter("CatchAll", CatchAllItem)
                        com.AddParameter("VatSymbol", VatSymbol)
                        com.AddParameter("TempPriceChange", TempPriceChangeDiff)
                        com.AddParameter("TPCErosion", TPMarginErosionCode)
                        com.AddParameter("PriceDiff", PriceOverrideDiff)
                        com.AddParameter("PDErosion", POMarginErosionCode)
                        com.AddParameter("QuantBreak", QuantityBreakErosion)
                        com.AddParameter("QBErosion", QBMarginErosionCode)
                        com.AddParameter("DealGroup", DealGroupErosion)
                        com.AddParameter("DGErosion", DGMarginErosionCode)
                        com.AddParameter("MultiBuy", MultibuyErosion)
                        com.AddParameter("MBErosion", MBMarginErosionCode)
                        com.AddParameter("Hiearchy", HierarchyErosion)
                        com.AddParameter("HErosion", HIMarginErosionCode)
                        com.AddParameter("PrimEmpSale", PriEmpDiscPriceDiff)
                        com.AddParameter("PESErosion", EmpMarginErosionCode)
                        com.AddParameter("Reversed", LineReversed)
                        com.AddParameter("EventSeq", LastEventSeqNo)
                        com.AddParameter("HCat", HierCategory)
                        com.AddParameter("HGroup", HierGroup)
                        com.AddParameter("HSubGroup", HierSubGroup)
                        com.AddParameter("HStyle", HierStyle)
                        com.AddParameter("PQSup", PartialQuarantine)
                        com.AddParameter("SecondEmpSale", SecondEmpSale)
                        com.AddParameter("MarkDown", MarkDownStock)
                        com.AddParameter("SaleType", SaleTypeAtt)
                        com.AddParameter("VatCode", VatCode)
                        com.AddParameter("VatAmount", VatValue)
                        com.AddParameter("BackDoor", BackDoor)
                        com.AddParameter("BackDoorBool", BackDoorBool)
                        com.AddParameter("ReverseReasCode", LineReverseCode)
                        com.AddParameter("RTI", RTIFlag)
                        com.AddParameter("WeeRate", WeeRate)
                        com.AddParameter("WeeSeq", WeeSequence)
                        com.AddParameter("WeeCost", WeeCost)
                        LinesUpdated += com.ExecuteNonQuery()

                    Case DataProvider.Sql

                        LinesUpdated += com.ExecuteNonQuery()

                End Select
            End Using
            Return LinesUpdated

        End Function

        Public Function Persist(ByVal con As Connection) As Integer

            If Not ExistsInDB Then
                Return PersistNew(con)
            Else
                Dim LinesUpdated As Integer = 0

                Using com As New Command(con)
                    Select Case con.DataProvider
                        Case DataProvider.Odbc

                            Dim sb As New StringBuilder
                            sb.Append("Update DLLINE Set")
                            sb.Append("[SKUN] = ?,[DEPT] = ?,[IBAR] = ?,[SUPV] = ?,[QUAN] = ?,[SPRI] = ?,[PRIC] = ?,[PRVE] = ?,")
                            sb.Append("[EXTP] = ?,[EXTC] = ?,[RITM] = ?,[PORC] = ?,[ITAG] = ?,[CATA] = ?,[VSYM] = ?,[TPPD] = ?,[TPME] = ?,[POPD] = ?,[POME] = ?,[QBPD] = ?,")
                            sb.Append("[QBME] = ?,[DGPD] = ?,[DGME] = ?,[MBPD] = ?,[MBME] = ?,[HSPD] = ?,[HSME] = ?,[ESPD] = ?,[ESME] = ?,[LREV] = ?,[ESEQ] = ?,[CTGY] = ?,")
                            sb.Append("[GRUP] = ?,[SGRP] = ?,[STYL] = ?,[QSUP] = ?,[ESEV] = ?,[IMDN] = ?,[SALT] = ?,[VATN] = ?,[VATV] = ?,[BDCO] = ?,[BDCOInd] = ?,[RCOD] = ?,")
                            sb.Append("[RTI] = ?,[WEERATE] = ?,[WEESEQN] = ?,[WEECOST] = ? ")
                            sb.Append("WHERE [DATE1] = ? AND [TILL] = ? AND [TRAN] = ? AND [NUMB] = ?")

                            com.CommandText = sb.ToString
                            com.AddParameter("SKU", SKUNumber)
                            com.AddParameter("Dept", Department)
                            com.AddParameter("Barcode", BarcodeUsed)
                            com.AddParameter("SupNo", SupervisorNumber)
                            com.AddParameter("Quantity", Quantity)
                            com.AddParameter("SPrice", SystemLookupPrice)
                            com.AddParameter("Price", ItemPrice)
                            com.AddParameter("ItemPrice", ItemExcVatPrice)
                            com.AddParameter("ExtendValue", ExtendedValue)
                            com.AddParameter("ExtendCost", ExtendedCost)
                            com.AddParameter("RelItem", RelatedItemsSingle)
                            com.AddParameter("POReasCode", PriceOverrideCode)
                            com.AddParameter("Tagged", TaggedItem)
                            com.AddParameter("CatchAll", CatchAllItem)
                            com.AddParameter("VatSymbol", VatSymbol)
                            com.AddParameter("TempPriceChange", TempPriceChangeDiff)
                            com.AddParameter("TPCErosion", TPMarginErosionCode)
                            com.AddParameter("PriceDiff", PriceOverrideDiff)
                            com.AddParameter("PDErosion", POMarginErosionCode)
                            com.AddParameter("QuantBreak", QuantityBreakErosion)
                            com.AddParameter("QBErosion", QBMarginErosionCode)
                            com.AddParameter("DealGroup", DealGroupErosion)
                            com.AddParameter("DGErosion", DGMarginErosionCode)
                            com.AddParameter("MultiBuy", MultibuyErosion)
                            com.AddParameter("MBErosion", MBMarginErosionCode)
                            com.AddParameter("Hiearchy", HierarchyErosion)
                            com.AddParameter("HErosion", HIMarginErosionCode)
                            com.AddParameter("PrimEmpSale", PriEmpDiscPriceDiff)
                            com.AddParameter("PESErosion", EmpMarginErosionCode)
                            com.AddParameter("Reversed", LineReversed)
                            com.AddParameter("EventSeq", LastEventSeqNo)
                            com.AddParameter("HCat", HierCategory)
                            com.AddParameter("HGroup", HierGroup)
                            com.AddParameter("HSubGroup", HierSubGroup)
                            com.AddParameter("HStyle", HierStyle)
                            com.AddParameter("PQSup", PartialQuarantine)
                            com.AddParameter("SecondEmpSale", SecondEmpSale)
                            com.AddParameter("MarkDown", MarkDownStock)
                            com.AddParameter("SaleType", SaleTypeAtt)
                            com.AddParameter("VatCode", VatCode)
                            com.AddParameter("VatAmount", VatValue)
                            com.AddParameter("BackDoor", BackDoor)
                            com.AddParameter("BackDoorBool", BackDoorBool)
                            com.AddParameter("ReverseReasCode", LineReverseCode)
                            com.AddParameter("RTI", RTIFlag)
                            com.AddParameter("WeeRate", WeeRate)
                            com.AddParameter("WeeSeq", WeeSequence)
                            com.AddParameter("WeeCost", WeeCost)
                            com.AddParameter("TranDate", TransactionDate)
                            com.AddParameter("TillNo", TillNumber)
                            com.AddParameter("TranNo", TransactionNumber)
                            com.AddParameter("Numb", LineNumber)
                            LinesUpdated += com.ExecuteNonQuery()

                        Case DataProvider.Sql

                            LinesUpdated += com.ExecuteNonQuery()

                    End Select
                End Using
                Return LinesUpdated
            End If

        End Function

#End Region

    End Class
#End Region

#Region "ClassSaleLineCollection"
    Public Class SaleLineCollection
        Inherits BaseCollection(Of SaleLine)

        Public Sub New()
            MyBase.New()
        End Sub

        Public Overloads Sub Load(ByVal tranDate As Date, ByVal tranNumber As String, ByVal tillId As String)
            Dim dt As DataTable = DataAccess.SaleLineGet(tranDate, tranNumber, tillId)
            Me.Load(dt)
        End Sub

    End Class
#End Region

End Namespace
#End Region

