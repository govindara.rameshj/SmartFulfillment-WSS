﻿Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports System.Text
Imports System.ComponentModel
Imports System.Reflection
Imports Till.Oasys.Data
Imports Till.Oasys.Core
Imports Till.Oasys.Core.Order.WebService

#Region "QodNamespace"
Namespace Qod
#Region "Class SaleHeader"
    ''' <summary>
    ''' The Sale Header Class used to hold Sale Header data
    ''' </summary>
    ''' <remarks></remarks>
    Public Class SaleHeader
        Inherits Oasys.Core.Base

#Region "Table Fields"

        Private _transactionDate As Date
        Private _tillNumber As String
        Private _transactionNumber As String
        Private _cashierId As String = "000"
        Private _transactionTime As String = Now.ToString("HHmmss")
        Private _supervisorCashierNo As String = "000"
        Private _transactionCode As String = "SA"
        Private _odCode As Integer = 0
        Private _reasonCode As Integer = 0
        Private _description As String = String.Empty
        Private _orderNumber As String = "000000"
        Private _accountSale As Boolean = False
        Private _void As Boolean = False
        Private _voidSupervisor As String = "000"
        Private _trainingMode As Boolean = False
        Private _processed As Boolean = False
        Private _documentNumber As String = "00000000"
        Private _supervisorUsed As Boolean = False
        Private _storeNumber As String = String.Empty
        Private _merchandiseAmount As Decimal
        Private _nonMerchandiseAmount As Decimal = 0
        Private _taxAmount As Decimal
        Private _discount As Decimal = 0
        Private _discountSupervisor As String = "000"
        Private _totalSaleAmount As Decimal
        Private _accountNumber As String = "000000"
        Private _accountCardNumber As String = "00"
        Private _pcCollection As Boolean = False
        Private _fromDcOrders As Boolean = False
        Private _transactionComplete As Boolean = True
        Private _employeeDiscountOnly As Boolean = False
        Private _refundCashrNo As String = "000"
        Private _refundSupervisor As String = "000"
        Private _parked As Boolean = False
        Private _refundManager As String = "000"
        Private _tenderOverrideCode As String = "00"
        Private _unparked As Boolean = False
        Private _transactionOnline As Boolean = True
        Private _tokensPrinted As Integer = 0
        Private _colleagueNumber As String = "000000000"
        Private _saveStatus As String = "99"
        Private _saveSequence As String = String.Empty
        Private _cbbUpdate As String = String.Empty
        Private _discountCardNo As String = String.Empty
        Private _rti As String = "S"
        Private _vatr1 As Decimal
        Private _vatr2 As Decimal
        Private _vatr3 As Decimal
        Private _vatr4 As Decimal
        Private _vatr5 As Decimal
        Private _vatr6 As Decimal
        Private _vatr7 As Decimal
        Private _vatr8 As Decimal
        Private _vatr9 As Decimal
        Private _vatSymb1 As String = "a"
        Private _vatSymb2 As String = "b"
        Private _vatSymb3 As String = "c"
        Private _vatSymb4 As String = "d"
        Private _vatSymb5 As String = "e"
        Private _vatSymb6 As String = "f"
        Private _vatSymb7 As String = "g"
        Private _vatSymb8 As String = "h"
        Private _vatSymb9 As String = "i"
        Private _xVatAmnt1 As Decimal
        Private _xVatAmnt2 As Decimal
        Private _xVatAmnt3 As Decimal
        Private _xVatAmnt4 As Decimal
        Private _xVatAmnt5 As Decimal
        Private _xVatAmnt6 As Decimal
        Private _xVatAmnt7 As Decimal
        Private _xVatAmnt8 As Decimal
        Private _xVatAmnt9 As Decimal
        Private _vatAmnt1 As Decimal
        Private _vatAmnt2 As Decimal
        Private _vatAmnt3 As Decimal
        Private _vatAmnt4 As Decimal
        Private _vatAmnt5 As Decimal
        Private _vatAmnt6 As Decimal
        Private _vatAmnt7 As Decimal
        Private _vatAmnt8 As Decimal
        Private _vatAmnt9 As Decimal

        'column mapping

        <ColumnMapping("DATE1")> Public Property TransactionDate() As Date
            Get
                Return _transactionDate
            End Get
            Set(ByVal value As Date)
                _transactionDate = value
            End Set
        End Property
        <ColumnMapping("TILL")> Public Property TillNumber() As String
            Get
                Return _tillNumber
            End Get
            Set(ByVal value As String)
                _tillNumber = value
            End Set
        End Property
        <ColumnMapping("TRAN")> Public Property TransactionNumber() As String
            Get
                Return _transactionNumber
            End Get
            Set(ByVal value As String)
                _transactionNumber = value
            End Set
        End Property
        <ColumnMapping("CASH")> Public Property CashierID() As String
            Get
                Return _cashierId
            End Get
            Set(ByVal value As String)
                _cashierId = value
            End Set
        End Property
        <ColumnMapping("TIME")> Public Property TransactionTime() As String
            Get
                Return _transactionTime
            End Get
            Set(ByVal value As String)
                _transactionTime = value
            End Set
        End Property
        <ColumnMapping("SUPV")> Public Property SupervisorCashierNo() As String
            Get
                Return _supervisorCashierNo
            End Get
            Set(ByVal value As String)
                _supervisorCashierNo = value
            End Set
        End Property
        <ColumnMapping("TCOD")> Public Property TransactionCode() As String
            Get
                Return _transactionCode
            End Get
            Set(ByVal value As String)
                _transactionCode = value
            End Set
        End Property
        <ColumnMapping("OPEN")> Public Property ODCode() As Integer
            Get
                Return _odCode
            End Get
            Set(ByVal value As Integer)
                _odCode = value
            End Set
        End Property
        <ColumnMapping("MISC")> Public Property ReasonCode() As Integer
            Get
                Return _reasonCode
            End Get
            Set(ByVal value As Integer)
                _reasonCode = value
            End Set
        End Property
        <ColumnMapping("DESCR")> Public Property Description() As String
            Get
                Return _description
            End Get
            Set(ByVal value As String)
                _description = value
            End Set
        End Property
        <ColumnMapping("ORDN")> Public Property OrderNumber() As String
            Get
                Return _orderNumber
            End Get
            Set(ByVal value As String)
                _orderNumber = value
            End Set
        End Property
        <ColumnMapping("ACCT")> Public Property AccountSale() As Boolean
            Get
                Return _accountSale
            End Get
            Set(ByVal value As Boolean)
                _accountSale = value
            End Set
        End Property
        <ColumnMapping("VOID")> Public Property Void() As Boolean
            Get
                Return _void
            End Get
            Set(ByVal value As Boolean)
                _void = value
            End Set
        End Property
        <ColumnMapping("VSUP")> Public Property VoidSupervisor() As String
            Get
                Return _voidSupervisor
            End Get
            Set(ByVal value As String)
                _voidSupervisor = value
            End Set
        End Property
        <ColumnMapping("TMOD")> Public Property TrainingMode() As Boolean
            Get
                Return _trainingMode
            End Get
            Set(ByVal value As Boolean)
                _trainingMode = value
            End Set
        End Property
        <ColumnMapping("PROC")> Public Property Processed() As Boolean
            Get
                Return _processed
            End Get
            Set(ByVal value As Boolean)
                _processed = value
            End Set
        End Property
        <ColumnMapping("DOCN")> Public Property DocumentNumber() As String
            Get
                Return _documentNumber
            End Get
            Set(ByVal value As String)
                _documentNumber = value
            End Set
        End Property
        <ColumnMapping("SUSE")> Public Property SupervisorUsed() As Boolean
            Get
                Return _supervisorUsed
            End Get
            Set(ByVal value As Boolean)
                _supervisorUsed = value
            End Set
        End Property
        <ColumnMapping("STOR")> Public Property StoreNumber() As String
            Get
                Return _storeNumber
            End Get
            Set(ByVal value As String)
                _storeNumber = value
            End Set
        End Property
        <ColumnMapping("MERC")> Public Property MerchandiseAmount() As Decimal
            Get
                Return _merchandiseAmount
            End Get
            Set(ByVal value As Decimal)
                _merchandiseAmount = value
            End Set
        End Property
        <ColumnMapping("NMER")> Public Property NonMerchandiseAmount() As Decimal
            Get
                Return _nonMerchandiseAmount
            End Get
            Set(ByVal value As Decimal)
                _nonMerchandiseAmount = value
            End Set
        End Property
        <ColumnMapping("TAXA")> Public Property TaxAmount() As Decimal
            Get
                Return _taxAmount
            End Get
            Set(ByVal value As Decimal)
                _taxAmount = value
            End Set
        End Property
        <ColumnMapping("DISC")> Public Property Discount() As Decimal
            Get
                Return _discount
            End Get
            Set(ByVal value As Decimal)
                _discount = value
            End Set
        End Property
        <ColumnMapping("DSUP")> Public Property DiscountSupervisor() As String
            Get
                Return _discountSupervisor
            End Get
            Set(ByVal value As String)
                _discountSupervisor = value
            End Set
        End Property
        <ColumnMapping("TOTL")> Public Property TotalSaleAmount() As Decimal
            Get
                Return (MerchandiseAmount - Discount)
            End Get
            Set(ByVal value As Decimal)
                _totalSaleAmount = value
            End Set
        End Property
        <ColumnMapping("ACCN")> Public Property AccountNumber() As String
            Get
                Return _accountNumber
            End Get
            Set(ByVal value As String)
                _accountNumber = value
            End Set
        End Property
        <ColumnMapping("CARD")> Public Property AccountCardNumber() As String
            Get
                Return _accountCardNumber
            End Get
            Set(ByVal value As String)
                _accountCardNumber = value
            End Set
        End Property
        <ColumnMapping("AUPD")> Public Property PCCollection() As Boolean
            Get
                Return _pcCollection
            End Get
            Set(ByVal value As Boolean)
                _pcCollection = value
            End Set
        End Property
        <ColumnMapping("BACK")> Public Property FromDCOrders() As Boolean
            Get
                Return _fromDcOrders
            End Get
            Set(ByVal value As Boolean)
                _fromDcOrders = value
            End Set
        End Property
        <ColumnMapping("ICOM")> Public Property TransactionComplete() As Boolean
            Get
                Return _transactionComplete
            End Get
            Set(ByVal value As Boolean)
                _transactionComplete = value
            End Set
        End Property
        <ColumnMapping("IEMP")> Public Property EmployeeDiscountOnly() As Boolean
            Get
                Return _employeeDiscountOnly
            End Get
            Set(ByVal value As Boolean)
                _employeeDiscountOnly = value
            End Set
        End Property
        <ColumnMapping("RSUP")> Public Property RefundSupervisor() As String
            Get
                Return _refundSupervisor
            End Get
            Set(ByVal value As String)
                _refundSupervisor = value
            End Set
        End Property
        <ColumnMapping("RCAS")> Public Property RefundCashierNo() As String
            Get
                Return _refundCashrNo
            End Get
            Set(ByVal value As String)
                _refundCashrNo = value
            End Set
        End Property
        <ColumnMapping("PARK")> Public Property Parked() As Boolean
            Get
                Return _parked
            End Get
            Set(ByVal value As Boolean)
                _parked = value
            End Set
        End Property
        <ColumnMapping("RMAN")> Public Property RefundManager() As String
            Get
                Return _refundManager
            End Get
            Set(ByVal value As String)
                _refundManager = value
            End Set
        End Property
        <ColumnMapping("TOCD")> Public Property TenderOverrideCode() As String
            Get
                Return _tenderOverrideCode
            End Get
            Set(ByVal value As String)
                _tenderOverrideCode = value
            End Set
        End Property
        <ColumnMapping("PKRC")> Public Property Unparked() As Boolean
            Get
                Return _unparked
            End Get
            Set(ByVal value As Boolean)
                _unparked = value
            End Set
        End Property
        <ColumnMapping("REMO")> Public Property TransactionOnLine() As Boolean
            Get
                Return _transactionOnline
            End Get
            Set(ByVal value As Boolean)
                _transactionOnline = value
            End Set
        End Property
        <ColumnMapping("GTPN")> Public Property TokensPrinted() As Integer
            Get
                Return _tokensPrinted
            End Get
            Set(ByVal value As Integer)
                _tokensPrinted = value
            End Set
        End Property
        <ColumnMapping("CCRD")> Public Property ColleagueNumber() As String
            Get
                Return _colleagueNumber
            End Get
            Set(ByVal value As String)
                _colleagueNumber = value
            End Set
        End Property
        <ColumnMapping("SSTA")> Public Property SaveStatus() As String
            Get
                Return _saveStatus
            End Get
            Set(ByVal value As String)
                _saveStatus = value
            End Set
        End Property
        <ColumnMapping("SSEQ")> Public Property SaveSequence() As String
            Get
                Return _saveSequence
            End Get
            Set(ByVal value As String)
                _saveSequence = value
            End Set
        End Property
        <ColumnMapping("CBBU")> Public Property CBBUpdate() As String
            Get
                Return _cbbUpdate
            End Get
            Set(ByVal value As String)
                _cbbUpdate = value
            End Set
        End Property
        <ColumnMapping("CARD_NO")> Public Property DiscountCardNo() As String
            Get
                Return _discountCardNo
            End Get
            Set(ByVal value As String)
                _discountCardNo = value
            End Set
        End Property
        <ColumnMapping("RTI")> Public Property RTI() As String
            Get
                Return _rti
            End Get
            Set(ByVal value As String)
                _rti = value
            End Set
        End Property

        <ColumnMapping("VATR1")> Public Property VATRate1() As Decimal
            Get
                Return _vatr1
            End Get
            Set(ByVal value As Decimal)
                _vatr1 = value
            End Set
        End Property
        <ColumnMapping("VATR2")> Public Property VATRate2() As Decimal
            Get
                Return _vatr2
            End Get
            Set(ByVal value As Decimal)
                _vatr2 = value
            End Set
        End Property
        <ColumnMapping("VATR3")> Public Property VATRate3() As Decimal
            Get
                Return _vatr3
            End Get
            Set(ByVal value As Decimal)
                _vatr3 = value
            End Set
        End Property
        <ColumnMapping("VATR4")> Public Property VATRate4() As Decimal
            Get
                Return _vatr4
            End Get
            Set(ByVal value As Decimal)
                _vatr4 = value
            End Set
        End Property
        <ColumnMapping("VATR5")> Public Property VATRate5() As Decimal
            Get
                Return _vatr5
            End Get
            Set(ByVal value As Decimal)
                _vatr5 = value
            End Set
        End Property
        <ColumnMapping("VATR6")> Public Property VATRate6() As Decimal
            Get
                Return _vatr6
            End Get
            Set(ByVal value As Decimal)
                _vatr6 = value
            End Set
        End Property
        <ColumnMapping("VATR7")> Public Property VATRate7() As Decimal
            Get
                Return _vatr7
            End Get
            Set(ByVal value As Decimal)
                _vatr7 = value
            End Set
        End Property
        <ColumnMapping("VATR8")> Public Property VATRate8() As Decimal
            Get
                Return _vatr8
            End Get
            Set(ByVal value As Decimal)
                _vatr8 = value
            End Set
        End Property
        <ColumnMapping("VATR9")> Public Property VATRate9() As Decimal
            Get
                Return _vatr9
            End Get
            Set(ByVal value As Decimal)
                _vatr9 = value
            End Set
        End Property

        <ColumnMapping("VSYM1")> Public Property VATSymb1() As String
            Get
                Return _vatSymb1
            End Get
            Set(ByVal value As String)
                _vatSymb1 = value
            End Set
        End Property
        <ColumnMapping("VSYM2")> Public Property VATSymb2() As String
            Get
                Return _vatSymb2
            End Get
            Set(ByVal value As String)
                _vatSymb2 = value
            End Set
        End Property
        <ColumnMapping("VSYM3")> Public Property VATSymb3() As String
            Get
                Return _vatSymb3
            End Get
            Set(ByVal value As String)
                _vatSymb3 = value
            End Set
        End Property
        <ColumnMapping("VSYM4")> Public Property VATSymb4() As String
            Get
                Return _vatSymb4
            End Get
            Set(ByVal value As String)
                _vatSymb4 = value
            End Set
        End Property
        <ColumnMapping("VSYM5")> Public Property VATSymb5() As String
            Get
                Return _vatSymb5
            End Get
            Set(ByVal value As String)
                _vatSymb5 = value
            End Set
        End Property
        <ColumnMapping("VSYM6")> Public Property VATSymb6() As String
            Get
                Return _vatSymb6
            End Get
            Set(ByVal value As String)
                _vatSymb6 = value
            End Set
        End Property
        <ColumnMapping("VSYM7")> Public Property VATSymb7() As String
            Get
                Return _vatSymb7
            End Get
            Set(ByVal value As String)
                _vatSymb7 = value
            End Set
        End Property
        <ColumnMapping("VSYM8")> Public Property VATSymb8() As String
            Get
                Return _vatSymb8
            End Get
            Set(ByVal value As String)
                _vatSymb8 = value
            End Set
        End Property
        <ColumnMapping("VSYM9")> Public Property VATSymb9() As String
            Get
                Return _vatSymb9
            End Get
            Set(ByVal value As String)
                _vatSymb9 = value
            End Set
        End Property

        <ColumnMapping("XVAT1")> Public Property XVATAmnt1() As Decimal
            Get
                Return _xVatAmnt1
            End Get
            Set(ByVal value As Decimal)
                _xVatAmnt1 = value
            End Set
        End Property
        <ColumnMapping("XVAT2")> Public Property XVATAmnt2() As Decimal
            Get
                Return _xVatAmnt2
            End Get
            Set(ByVal value As Decimal)
                _xVatAmnt2 = value
            End Set
        End Property
        <ColumnMapping("XVAT3")> Public Property XVATAmnt3() As Decimal
            Get
                Return _xVatAmnt3
            End Get
            Set(ByVal value As Decimal)
                _xVatAmnt3 = value
            End Set
        End Property
        <ColumnMapping("XVAT4")> Public Property XVATAmnt4() As Decimal
            Get
                Return _xVatAmnt4
            End Get
            Set(ByVal value As Decimal)
                _xVatAmnt4 = value
            End Set
        End Property
        <ColumnMapping("XVAT5")> Public Property XVATAmnt5() As Decimal
            Get
                Return _xVatAmnt5
            End Get
            Set(ByVal value As Decimal)
                _xVatAmnt5 = value
            End Set
        End Property
        <ColumnMapping("XVAT6")> Public Property XVATAmnt6() As Decimal
            Get
                Return _xVatAmnt6
            End Get
            Set(ByVal value As Decimal)
                _xVatAmnt6 = value
            End Set
        End Property
        <ColumnMapping("XVAT7")> Public Property XVATAmnt7() As Decimal
            Get
                Return _xVatAmnt7
            End Get
            Set(ByVal value As Decimal)
                _xVatAmnt7 = value
            End Set
        End Property
        <ColumnMapping("XVAT8")> Public Property XVATAmnt8() As Decimal
            Get
                Return _xVatAmnt8
            End Get
            Set(ByVal value As Decimal)
                _xVatAmnt8 = value
            End Set
        End Property
        <ColumnMapping("XVAT9")> Public Property XVATAmnt9() As Decimal
            Get
                Return _xVatAmnt9
            End Get
            Set(ByVal value As Decimal)
                _xVatAmnt9 = value
            End Set
        End Property

        <ColumnMapping("VATV1")> Public Property VATAmnt1() As Decimal
            Get
                Return _vatAmnt1
            End Get
            Set(ByVal value As Decimal)
                _vatAmnt1 = value
            End Set
        End Property
        <ColumnMapping("VATV2")> Public Property VATAmnt2() As Decimal
            Get
                Return _vatAmnt2
            End Get
            Set(ByVal value As Decimal)
                _vatAmnt2 = value
            End Set
        End Property
        <ColumnMapping("VATV3")> Public Property VATAmnt3() As Decimal
            Get
                Return _vatAmnt3
            End Get
            Set(ByVal value As Decimal)
                _vatAmnt3 = value
            End Set
        End Property
        <ColumnMapping("VATV4")> Public Property VATAmnt4() As Decimal
            Get
                Return _vatAmnt4
            End Get
            Set(ByVal value As Decimal)
                _vatAmnt4 = value
            End Set
        End Property
        <ColumnMapping("VATV5")> Public Property VATAmnt5() As Decimal
            Get
                Return _vatAmnt5
            End Get
            Set(ByVal value As Decimal)
                _vatAmnt5 = value
            End Set
        End Property
        <ColumnMapping("VATV6")> Public Property VATAmnt6() As Decimal
            Get
                Return _vatAmnt6
            End Get
            Set(ByVal value As Decimal)
                _vatAmnt6 = value
            End Set
        End Property
        <ColumnMapping("VATV7")> Public Property VATAmnt7() As Decimal
            Get
                Return _vatAmnt7
            End Get
            Set(ByVal value As Decimal)
                _vatAmnt7 = value
            End Set
        End Property
        <ColumnMapping("VATV8")> Public Property VATAmnt8() As Decimal
            Get
                Return _vatAmnt8
            End Get
            Set(ByVal value As Decimal)
                _vatAmnt8 = value
            End Set
        End Property
        <ColumnMapping("VATV9")> Public Property VATAmnt9() As Decimal
            Get
                Return _vatAmnt9
            End Get
            Set(ByVal value As Decimal)
                _vatAmnt9 = value
            End Set
        End Property

#End Region

#Region "Properties"

        Public Property VATRates() As Decimal()
            Get
                Dim vatRateCollection As Decimal() = {Me.VATRate1, Me.VATRate2, Me.VATRate3, Me.VATRate4, Me.VATRate5, Me.VATRate6, Me.VATRate7, Me.VATRate8, Me.VATRate9}
                Return vatRateCollection
            End Get
            Set(ByVal value As Decimal())
                Dim PropertyItem As PropertyInfo = Nothing
                Dim Index As Integer = 1
                For Each VatValue In value
                    PropertyItem = Me.GetType.GetProperty("VATRate" & Index.ToString)
                    PropertyItem.SetValue(Me, value(Index - 1), Nothing)
                    Index += 1
                Next
            End Set
        End Property

        Public ReadOnly Property VATSymbols() As String()
            Get
                Dim vatSymbolCollection As String() = {Me.VATSymb1, Me.VATSymb2, Me.VATSymb3, Me.VATSymb4, Me.VATSymb5, Me.VATSymb6, Me.VATSymb7, Me.VATSymb8, Me.VATSymb9}
                Return vatSymbolCollection
            End Get
        End Property

        Public ReadOnly Property XVATAmounts() As Decimal()
            Get
                Dim xvatAmntCollection As Decimal() = {Me.XVATAmnt1, Me.XVATAmnt2, Me.XVATAmnt3, Me.XVATAmnt4, Me.XVATAmnt5, Me.XVATAmnt6, Me.XVATAmnt7, Me.XVATAmnt8, Me.XVATAmnt9}
                Return xvatAmntCollection
            End Get
        End Property

        Public ReadOnly Property VATAmounts() As Decimal()
            Get
                Dim vatAmntCollection As Decimal() = {Me.VATAmnt1, Me.VATAmnt2, Me.VATAmnt3, Me.VATAmnt4, Me.VATAmnt5, Me.VATAmnt6, Me.VATAmnt7, Me.XVATAmnt8, Me.XVATAmnt9}
                Return vatAmntCollection
            End Get
        End Property

        Private _SaleLines As SaleLineCollection = Nothing
        Private _SalePaid As SalePaid = Nothing
        Private _SaleCustomers As SaleCustomerCollection = Nothing
        Private _ExistsInDB As Boolean = False

        Public Property SaleLines() As SaleLineCollection
            Get
                If _salelines Is Nothing Then LoadSaleLines()
                Return _salelines
            End Get
            Set(ByVal value As SaleLineCollection)
                _salelines = value
            End Set
        End Property

        Public Property SalePaid() As SalePaid
            Get
                If _salepaid Is Nothing Then LoadSalePaid()
                Return _salepaid
            End Get
            Set(ByVal value As SalePaid)
                _salepaid = value
            End Set
        End Property

        Public Property SaleCustomers() As SaleCustomerCollection
            Get
                If _salecustomers Is Nothing Then LoadSaleCustomers()
                Return _salecustomers
            End Get
            Set(ByVal value As SaleCustomerCollection)
                _salecustomers = value
            End Set

        End Property

        Public Property ExistsInDB() As Boolean
            Get
                Return _ExistsInDB
            End Get
            Set(ByVal value As Boolean)
                _ExistsInDB = value
            End Set
        End Property

#End Region

#Region "Methods"

        Public Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub

        Public Sub New(ByRef qodCreate As WebService.CTSQODCreateResponse, ByVal TranTill As String, ByVal TranNumber As String)
            MyBase.New()
            Trace.WriteLine("Getting VAT Details")
            Dim VatRates As New VATRates
            Trace.WriteLine("Got VAT")
            Me.TransactionDate = qodCreate.OrderHeader.SaleDate.Value
            Me.TillNumber = TranTill
            Me.TransactionNumber = TranNumber
            Me.MerchandiseAmount = qodCreate.OrderHeader.TotalOrderValue.Value
            Me.VATRates = VatRates.VatRates
            Trace.WriteLine("Creating Lines")
            'Now create the Sale Line items - we need to do this so that we can calculate the VAT amounts correctly
            Dim SaleLines As New SaleLineCollection
            Dim SaleCustomers As New SaleCustomerCollection
            For Each QODCreateLine As CTSQODCreateResponseOrderLine In qodCreate.OrderLines
                Dim SaleLine As SaleLine = New SaleLine
                Trace.WriteLine("Creating Line " & QODCreateLine.SourceOrderLineNo.Value)
                SaleLine.TransactionDate = Me.TransactionDate
                SaleLine.TillNumber = TranTill
                SaleLine.TransactionNumber = TranNumber
                SaleLine.LineNumber = CInt(QODCreateLine.SourceOrderLineNo.Value)
                SaleLine.SKUNumber = QODCreateLine.ProductCode.Value
                SaleLine.Quantity = QODCreateLine.TotalOrderQuantity.Value
                SaleLine.SystemLookupPrice = QODCreateLine.SellingPrice.Value
                SaleLine.ItemPrice = QODCreateLine.SellingPrice.Value
                Dim CheckStock As Stock.Stock = Stock.Stock.GetStock(SaleLine.SKUNumber)
                SaleLine.VatCode = CheckStock.VatCode
                Dim Finder As New VATRateFinder(CheckStock.VatCode)
                Dim VatRate As VATRate = VatRates.VatRateCollection.Find(AddressOf Finder.FindVATFromIndex)
                Dim LineVateRate As Decimal = VatRate.VatRate
                SaleLine.ItemExcVatPrice = SaleLine.ItemPrice / (LineVateRate + 1)
                SaleLine.ExtendedValue = SaleLine.Quantity * SaleLine.ItemPrice
                SaleLine.RelatedItemsSingle = CheckStock.IsItemSingle
                SaleLine.TaggedItem = CheckStock.IsTaggedItem
                SaleLine.VatSymbol = Me.VATSymbols(CheckStock.VatCode)
                SaleLine.SaleTypeAtt = CheckStock.SalesType
                SaleLine.VatValue = (SaleLine.ItemPrice - SaleLine.ItemExcVatPrice) * SaleLine.Quantity
                SaleLine.HierCategory = CheckStock.HieCategory
                SaleLine.HierGroup = CheckStock.HieGroup
                SaleLine.HierSubGroup = CheckStock.HieSubgroup
                SaleLine.HierStyle = CheckStock.HieStyle
                AddSaleAmounts(SaleLine)
                SaleLines.Add(SaleLine)

                'Now create the Sale Customer for each line (why?)
                Trace.WriteLine("Creating Customer " & QODCreateLine.SourceOrderLineNo.Value)
                Dim SaleCustomer As New SaleCustomer
                SaleCustomer.TransactionDate = qodCreate.OrderHeader.SaleDate.Value
                SaleCustomer.TillNumber = TranTill
                SaleCustomer.TransactionNumber = TranNumber
                SaleCustomer.CustomerName = qodCreate.OrderHeader.CustomerName.Value
                SaleCustomer.PostCode = qodCreate.OrderHeader.CustomerPostcode.Value
                SaleCustomer.StoreNumber = ThisStore.Id.ToString.PadLeft(3, "0"c)
                SaleCustomer.SaleDate = qodCreate.OrderHeader.SaleDate.Value
                SaleCustomer.SaleTill = TranTill
                SaleCustomer.SaleTransaction = TranNumber
                SaleCustomer.HouseNameNo = qodCreate.OrderHeader.CustomerAddressLine1.Value
                SaleCustomer.Address1 = qodCreate.OrderHeader.CustomerAddressLine2.Value
                SaleCustomer.Address2 = qodCreate.OrderHeader.CustomerAddressTown.Value
                SaleCustomer.Address3 = qodCreate.OrderHeader.CustomerAddressLine4.Value
                SaleCustomer.PhoneNumber = qodCreate.OrderHeader.ContactPhoneHome.Value
                SaleCustomer.LineNumber = SaleLine.LineNumber
                SaleCustomer.PhoneNumberMobile = qodCreate.OrderHeader.ContactPhoneMobile.Value
                SaleCustomer.EmailAddress = qodCreate.OrderHeader.ContactEmail.Value
                SaleCustomer.PhoneNumberWork = qodCreate.OrderHeader.ContactPhoneWork.Value
                SaleCustomers.Add(SaleCustomer)
            Next

            Me.SaleLines = SaleLines
            Me.SaleCustomers = SaleCustomers

            Trace.WriteLine("Creating SalePaid")
            'Now create the Sale Paid
            Dim SalePaid As New SalePaid
            SalePaid.TransactionDate = qodCreate.OrderHeader.SaleDate.Value
            SalePaid.TillNumber = TillNumber
            SalePaid.TransactionNumber = TranNumber
            SalePaid.SequenceNumber = 1
            SalePaid.TenderAmount = qodCreate.OrderHeader.TotalOrderValue.Value * -1

            Me.SalePaid = SalePaid
            Trace.WriteLine("Finished Sale")
        End Sub

        Private Sub BuildSaleStructure()

        End Sub
        Private Sub AddSaleAmounts(ByVal saleLine As SaleLine)
            Select Case saleLine.VatCode
                Case 1
                    Me.XVATAmnt1 += saleLine.ItemExcVatPrice * saleLine.Quantity
                    Me.VATAmnt1 += saleLine.VatValue
                Case 2
                    Me.XVATAmnt2 += saleLine.ItemExcVatPrice * saleLine.Quantity
                    Me.VATAmnt2 += saleLine.VatValue
                Case 3
                    Me.XVATAmnt3 += saleLine.ItemExcVatPrice * saleLine.Quantity
                    Me.VATAmnt3 += saleLine.VatValue
                Case 4
                    Me.XVATAmnt4 += saleLine.ItemExcVatPrice * saleLine.Quantity
                    Me.VATAmnt4 += saleLine.VatValue
                Case 5
                    Me.XVATAmnt5 += saleLine.ItemExcVatPrice * saleLine.Quantity
                    Me.VATAmnt5 += saleLine.VatValue
                Case 6
                    Me.XVATAmnt6 += saleLine.ItemExcVatPrice * saleLine.Quantity
                    Me.VATAmnt6 += saleLine.VatValue
                Case 7
                    Me.XVATAmnt7 += saleLine.ItemExcVatPrice * saleLine.Quantity
                    Me.VATAmnt7 += saleLine.VatValue
                Case 8
                    Me.XVATAmnt8 += saleLine.ItemExcVatPrice * saleLine.Quantity
                    Me.VATAmnt8 += saleLine.VatValue
                Case 9
                    Me.XVATAmnt9 += saleLine.ItemExcVatPrice * saleLine.Quantity
                    Me.VATAmnt9 += saleLine.VatValue
            End Select

        End Sub

        Public Sub LoadSaleLines()
            _SaleLines = New SaleLineCollection
            _SaleLines.Load(Me.TransactionDate, Me.TransactionNumber, Me.TillNumber)
        End Sub

        Public Sub LoadSalePaid()
            _SalePaid = New SalePaid
            _SalePaid.Load(Me.TransactionDate, Me.TransactionNumber, Me.TillNumber)
        End Sub

        Public Sub LoadSaleCustomers()
            _SaleCustomers = New SaleCustomerCollection
            '_salecustomers.Load(Me.TransactionDate, Me.TransactionNumber, Me.TillNumber)
        End Sub

        Public Function PersistNew(ByVal con As Connection) As Integer

            Dim LinesUpdated As Integer = 0

            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Odbc

                        Dim sb As New StringBuilder
                        sb.Append("Insert into DLTOTS (")
                        sb.Append("DATE1,TILL,TRAN,CASH,TIME,SUPV,TCOD,OPEN,MISC,DESCR,ORDN,")
                        sb.Append("ACCT,VOID,VSUP,TMOD,PROC,DOCN,SUSE,STOR,MERC,NMER,TAXA,DISC,DSUP,")
                        sb.Append("TOTL,ACCN,CARD,AUPD,BACK,ICOM,IEMP,RCAS,RSUP,PARK,")
                        sb.Append("VATR1,VATR2,VATR3,VATR4,VATR5,VATR6,VATR7,VATR8,VATR9,")
                        sb.Append("VSYM1,VSYM2,VSYM3,VSYM4,VSYM5,VSYM6,VSYM7,VSYM8,VSYM9,")
                        sb.Append("XVAT1,XVAT2,XVAT3,XVAT4,XVAT5,XVAT6,XVAT7,XVAT8,XVAT9,")
                        sb.Append("VATV1,VATV2,VATV3,VATV4,VATV5,VATV6,VATV7,VATV8,VATV9,")
                        sb.Append("RMAN, TOCD, PKRC,")
                        sb.Append("REMO,GTPN,CCRD,SSTA,SSEQ,CBBU,CARD_NO,RTI")
                        sb.Append(") values	(")
                        sb.Append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )")

                        com.CommandText = sb.ToString
                        com.AddParameter("TransactionDate", TransactionDate)
                        com.AddParameter("TillNumber", TillNumber)
                        com.AddParameter("TransactionNumber", TransactionNumber)
                        com.AddParameter("CashierID", CashierID)
                        com.AddParameter("TransactionTime", TransactionTime)
                        com.AddParameter("SupervisorCashierNo", SupervisorCashierNo)
                        com.AddParameter("TransactionCode", TransactionCode)
                        com.AddParameter("ODCode", ODCode)
                        com.AddParameter("ReasonCode", ReasonCode)
                        com.AddParameter("Description", Description)
                        com.AddParameter("OrderNumber", OrderNumber)
                        com.AddParameter("AccountSale", AccountSale)
                        com.AddParameter("Void", Void)
                        com.AddParameter("VoidSupervisor", VoidSupervisor)
                        com.AddParameter("TrainingMode", TrainingMode)
                        com.AddParameter("Processed", Processed)
                        com.AddParameter("DocumentNumber", DocumentNumber)
                        com.AddParameter("SupervisorUsed", SupervisorUsed)
                        com.AddParameter("StoreNumber", StoreNumber)
                        com.AddParameter("MerchandiseAmount", MerchandiseAmount)
                        com.AddParameter("NonMerchandiseAmount", NonMerchandiseAmount)
                        com.AddParameter("TaxAmount", TaxAmount)
                        com.AddParameter("Discount", Discount)
                        com.AddParameter("DiscountSupervisor", DiscountSupervisor)
                        com.AddParameter("Total", TotalSaleAmount)
                        com.AddParameter("AccountNumber", AccountNumber)
                        com.AddParameter("AccountCardNumber", AccountCardNumber)
                        com.AddParameter("PCCollection", PCCollection)
                        com.AddParameter("FromDCOrders", FromDCOrders)
                        com.AddParameter("TransactionComplete", TransactionComplete)
                        com.AddParameter("EmployeeDiscountOnly", EmployeeDiscountOnly)
                        com.AddParameter("RefundCashierNo", RefundCashierNo)
                        com.AddParameter("RefundSupervisor", RefundSupervisor)
                        com.AddParameter("Parked", Parked)
                        com.AddParameter("VATR1", VATRate1)
                        com.AddParameter("VATR2", VATRate2)
                        com.AddParameter("VATR3", VATRate3)
                        com.AddParameter("VATR4", VATRate4)
                        com.AddParameter("VATR5", VATRate5)
                        com.AddParameter("VATR6", VATRate6)
                        com.AddParameter("VATR7", VATRate7)
                        com.AddParameter("VATR8", VATRate8)
                        com.AddParameter("VATR9", VATRate9)
                        com.AddParameter("VSYM1", VATSymb1)
                        com.AddParameter("VSYM2", VATSymb2)
                        com.AddParameter("VSYM3", VATSymb3)
                        com.AddParameter("VSYM4", VATSymb4)
                        com.AddParameter("VSYM5", VATSymb5)
                        com.AddParameter("VSYM6", VATSymb6)
                        com.AddParameter("VSYM7", VATSymb7)
                        com.AddParameter("VSYM8", VATSymb8)
                        com.AddParameter("VSYM9", VATSymb9)
                        com.AddParameter("XVAT1", XVATAmnt1)
                        com.AddParameter("XVAT2", XVATAmnt2)
                        com.AddParameter("XVAT3", XVATAmnt3)
                        com.AddParameter("XVAT4", XVATAmnt4)
                        com.AddParameter("XVAT5", XVATAmnt5)
                        com.AddParameter("XVAT6", XVATAmnt6)
                        com.AddParameter("XVAT7", XVATAmnt7)
                        com.AddParameter("XVAT8", XVATAmnt8)
                        com.AddParameter("XVAT9", XVATAmnt9)
                        com.AddParameter("VATV1", VATAmnt1)
                        com.AddParameter("VATV2", VATAmnt2)
                        com.AddParameter("VATV3", VATAmnt3)
                        com.AddParameter("VATV4", VATAmnt4)
                        com.AddParameter("VATV5", VATAmnt5)
                        com.AddParameter("VATV6", VATAmnt6)
                        com.AddParameter("VATV7", VATAmnt7)
                        com.AddParameter("VATV8", VATAmnt8)
                        com.AddParameter("VATV9", VATAmnt9)
                        com.AddParameter("RefundManager", RefundManager)
                        com.AddParameter("TenderOverrideCode", TenderOverrideCode)
                        com.AddParameter("Unparked", Unparked)
                        com.AddParameter("TransactionOnLine", TransactionOnLine)
                        com.AddParameter("TokensPrinted", TokensPrinted)
                        com.AddParameter("ColleagueNumber", ColleagueNumber)
                        com.AddParameter("SaveStatus", SaveStatus)
                        com.AddParameter("SaveSequence", SaveSequence)
                        com.AddParameter("CBBUpdate", CBBUpdate)
                        com.AddParameter("DiscountCardNo", DiscountCardNo)
                        com.AddParameter("RTI", RTI)
                        LinesUpdated += com.ExecuteNonQuery()

                    Case DataProvider.Sql
                        com.StoredProcedureName = My.Resources.Procedures.SaleInsert
                        com.AddParameter(My.Resources.Parameters.TransactionDate, TransactionDate)
                        com.AddParameter(My.Resources.Parameters.TillNumber, TillNumber)
                        com.AddParameter(My.Resources.Parameters.TransactionNumber, TransactionNumber)
                        com.AddParameter(My.Resources.Parameters.TransactionTime, TransactionTime)
                        com.AddParameter(My.Resources.Parameters.OrderNumber, OrderNumber)
                        com.AddParameter(My.Resources.Parameters.MerchandiseAmount, MerchandiseAmount)
                        com.AddParameter(My.Resources.Parameters.TaxAmount, TaxAmount)
                        com.AddParameter(My.Resources.Parameters.TotalSaleAmount, TotalSaleAmount)
                        LinesUpdated += com.ExecuteNonQuery()

                End Select
            End Using
            Return LinesUpdated

        End Function

        Public Function Persist(ByVal con As Connection) As Integer

            If Not ExistsInDB Then
                Return PersistNew(con)
            Else
                Dim LinesUpdated As Integer = 0

                Using com As New Command(con)
                    Select Case con.DataProvider
                        Case DataProvider.Odbc

                            Dim sb As New StringBuilder
                            sb.Append("Update into DLTOTS Set ")
                            sb.Append("CASH = ?,[TIME] = ?,SUPV = ?,TCOD = ?,[OPEN] = ?,MISC = ?,DESCR = ?,ORDN = ?,")
                            sb.Append("ACCT = ?,VOID = ?,VSUP = ?,TMOD = ?,[PROC] = ?,DOCN = ?,SUSE = ?,STOR = ?,MERC = ?,NMER = ?,TAXA = ?,DISC = ?,DSUP = ?,")
                            sb.Append("TOTL = ?,ACCN = ?,[CARD] = ?,AUPD = ?,BACK = ?,ICOM = ?,IEMP = ?,RCAS = ?,RSUP = ?,PARK = ?,RMAN = ?,TOCD = ?,PKRC = ?,")
                            sb.Append("REMO = ?,GTPN = ?,CCRD = ?,SSTA = ?,SSEQ = ?,CBBU = ?,CARD_NO = ?,RTI = ? ")
                            sb.Append("WHERE DATE1=? AND TILL=? AND [TRAN]=?")

                            com.CommandText = sb.ToString
                            com.AddParameter("CashierID", CashierID)
                            com.AddParameter("TransactionTime", TransactionTime)
                            com.AddParameter("SupervisorCashierNo", SupervisorCashierNo)
                            com.AddParameter("TransactionCode", TransactionCode)
                            com.AddParameter("ODCode", ODCode)
                            com.AddParameter("ReasonCode", ReasonCode)
                            com.AddParameter("Description", Description)
                            com.AddParameter("OrderNumber", OrderNumber)
                            com.AddParameter("AccountSale", AccountSale)
                            com.AddParameter("Void", Void)
                            com.AddParameter("VoidSupervisor", VoidSupervisor)
                            com.AddParameter("TrainingMode", TrainingMode)
                            com.AddParameter("Processed", Processed)
                            com.AddParameter("DocumentNumber", DocumentNumber)
                            com.AddParameter("SupervisorUsed", SupervisorUsed)
                            com.AddParameter("StoreNumber", StoreNumber)
                            com.AddParameter("MerchandiseAmount", MerchandiseAmount)
                            com.AddParameter("NonMerchandiseAmount", NonMerchandiseAmount)
                            com.AddParameter("TaxAmount", TaxAmount)
                            com.AddParameter("Discount", Discount)
                            com.AddParameter("DiscountSupervisor", DiscountSupervisor)
                            com.AddParameter("AccountNumber", AccountNumber)
                            com.AddParameter("AccountCardNumber", AccountCardNumber)
                            com.AddParameter("PCCollection", PCCollection)
                            com.AddParameter("FromDCOrders", FromDCOrders)
                            com.AddParameter("TransactionComplete", TransactionComplete)
                            com.AddParameter("EmployeeDiscountOnly", EmployeeDiscountOnly)
                            com.AddParameter("RefundCashierNo", RefundCashierNo)
                            com.AddParameter("RefundSupervisor", RefundSupervisor)
                            com.AddParameter("Parked", Parked)
                            com.AddParameter("RefundManager", RefundManager)
                            com.AddParameter("TenderOverrideCode", TenderOverrideCode)
                            com.AddParameter("Unparked", Unparked)
                            com.AddParameter("TransactionOnLine", TransactionOnLine)
                            com.AddParameter("TokensPrinted", TokensPrinted)
                            com.AddParameter("ColleagueNumber", ColleagueNumber)
                            com.AddParameter("SaveStatus", SaveStatus)
                            com.AddParameter("SaveSequence", SaveSequence)
                            com.AddParameter("CBBUpdate", CBBUpdate)
                            com.AddParameter("DiscountCardNo", Discount)
                            com.AddParameter("RTI", RTI)
                            com.AddParameter("TransactionDate", TransactionDate)
                            com.AddParameter("TillNumber", TillNumber)
                            com.AddParameter("TransactionNumber", TransactionNumber)
                            LinesUpdated += com.ExecuteNonQuery()

                        Case DataProvider.Sql
                            com.StoredProcedureName = My.Resources.Procedures.SaleInsert
                            com.AddParameter(My.Resources.Parameters.TransactionDate, TransactionDate)
                            com.AddParameter(My.Resources.Parameters.TillNumber, TillNumber)
                            com.AddParameter(My.Resources.Parameters.TransactionNumber, TransactionNumber)
                            com.AddParameter(My.Resources.Parameters.TransactionTime, TransactionTime)
                            com.AddParameter(My.Resources.Parameters.OrderNumber, OrderNumber)
                            com.AddParameter(My.Resources.Parameters.MerchandiseAmount, MerchandiseAmount)
                            com.AddParameter(My.Resources.Parameters.TaxAmount, TaxAmount)
                            com.AddParameter(My.Resources.Parameters.TotalSaleAmount, TotalSaleAmount)
                            LinesUpdated += com.ExecuteNonQuery()

                    End Select
                End Using
                Return LinesUpdated
            End If

        End Function

        Public Function SelfPersist() As Integer
            Dim LinesUpdated As Integer = 0
            Using con As New Connection
                Try
                    con.StartTransaction()
                    LinesUpdated = PersistNew(con)
                    con.CommitTransaction()
                Catch ex As Exception
                    con.RollbackTransaction()
                End Try
            End Using
            Return LinesUpdated
        End Function

        Public Function PersistTree(ByVal con As Connection) As Integer
            Dim LinesUpdated As Integer = 0
            LinesUpdated += Persist(con)
            For Each SaleLine As SaleLine In SaleLines
                LinesUpdated += SaleLine.Persist(con)
            Next
            For Each SaleCustomer As SaleCustomer In SaleCustomers
                LinesUpdated += SaleCustomer.Persist(con)
            Next
            LinesUpdated += SalePaid.Persist(con)
            Return LinesUpdated
        End Function

#End Region

    End Class
#End Region

#Region "Class SaleHeaderCollection"

    Public Class SaleHeaderCollection
        Inherits BaseCollection(Of SaleHeader)

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub LoadForDateTillTran(ByVal tranDate As Date, ByVal tillId As String, ByVal tranNo As String)
            Dim dt As DataTable = Qod.DataAccess.SaleGet(tranDate, tillId, tranNo)
            Me.Load(dt)
        End Sub
    End Class

#End Region
End Namespace

#End Region


