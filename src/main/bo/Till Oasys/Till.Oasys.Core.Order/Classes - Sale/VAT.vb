﻿Imports Till.Oasys.Core
Imports Till.Oasys.Core.System
Imports Till.Oasys.Data
Imports System.Text

Public Class VATRates
    Inherits Till.Oasys.Core.Base
#Region "Private Variables"
    Private _NoVatRates As Integer
    Private _VatRateCollection As VATRateCollection
#End Region
#Region "Properties"
    Public ReadOnly Property NoVatRates() As Integer
        Get
            Return _NoVatRates
        End Get
    End Property
    Public ReadOnly Property VatRateCollection() As VATRateCollection
        Get
            Return _VatRateCollection
        End Get
    End Property
    Public ReadOnly Property VatRates() As Decimal()
        Get
            Dim TempRates(VatRateCollection.Count - 1) As Decimal
            Dim Index As Integer = 0
            For Each VATRate As VATRate In VatRateCollection
                TempRates(Index) = VATRate.VatRate
                Index += 1
            Next
            Return TempRates
        End Get
    End Property
#End Region
    Public Sub New()
        MyBase.New()
        Dim DT As DataTable = VatGet()
        If DT IsNot Nothing AndAlso DT.Rows.Count > 0 Then
            Dim DR As DataRow = DT.Rows(0)
            _NoVatRates = CInt(DR.Item(0).ToString)
            Dim VatRates As New VATRateCollection
            For Index As Integer = 1 To (_NoVatRates)
                Dim VatRate As New VATRate(Index, CDec(DR.Item(Index)))
                VatRates.Add(VatRate)
            Next
            _VatRateCollection = VatRates
        End If
    End Sub
    Private Function VatGet() As DataTable

        Dim retopt As DataTable
        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        Dim sb As New StringBuilder
                        'Get the VAT data from RETOPT
                        sb.Append("Select TOP 1 ")
                        sb.Append("VATN as NoVatRates, ")
                        sb.Append("VATR1, ")
                        sb.Append("VATR2, ")
                        sb.Append("VATR3, ")
                        sb.Append("VATR4, ")
                        sb.Append("VATR5, ")
                        sb.Append("VATR6, ")
                        sb.Append("VATR7, ")
                        sb.Append("VATR8, ")
                        sb.Append("VATR9 ")
                        sb.Append("from RETOPT")
                        com.CommandText = sb.ToString
                        retopt = com.ExecuteDataTable
                    Case DataProvider.Sql
                        'Get the VAT data from RETOPT
                        com.StoredProcedureName = My.Resources.Procedures.VatGet
                        retopt = com.ExecuteDataTable
                    Case Else
                        retopt = Nothing
                End Select
            End Using
        End Using
        Return retopt

    End Function

End Class

Public Class VATRateCollection
    Inherits List(Of VATRate)
End Class

Public Class VATRate
    Inherits Till.Oasys.Core.Base
#Region "Private Variables"
    Private _Index As Integer
    Private _VatRate As Decimal
#End Region
#Region "Properties"
    Public ReadOnly Property Index() As Integer
        Get
            Return _Index
        End Get
    End Property
    Public ReadOnly Property VatRate() As Decimal
        Get
            Return _VatRate
        End Get
    End Property
#End Region
    Public Sub New(ByVal index As Integer, ByVal vatRate As Decimal)
        MyBase.New()
        _Index = index
        _VatRate = vatRate
    End Sub
End Class
''' <summary>
''' Finder for locating a VAT Rate within a list of VAT Rates
''' </summary>
''' <history>
''' <created author="Charles McMahon" date="17/01/2011">Initial creation of VAT Finder Predicate</created>
''' </history>
''' <remarks>
''' Compares based on Index to allow the List class to return the correct VAT Rate
''' </remarks>
Public Class VATRateFinder
    Private _Index As Integer
    Public Sub New(ByVal index As Integer)
        _Index = index
    End Sub
    Public Function FindVATFromIndex(ByVal vatRate As VATRate) As Boolean
        Return (vatRate.Index = _Index)
    End Function
End Class