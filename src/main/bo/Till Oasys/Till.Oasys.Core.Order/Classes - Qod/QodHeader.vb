﻿Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports System.Text
Imports System.Reflection
Imports Till.Oasys.Data
Imports Till.Oasys.Core.Order.Qod.State

Namespace Qod

    Public Class QodHeader
        Inherits Oasys.Core.Base

#Region "       Table Fields"
        Private _number As String
        Private _vendaNumber As String
        Private _customerName As String
        Private _dateOrder As Date
        Private _dateDelivery As Date?
        Private _status As String
        Private _isForDelivery As Boolean
        Private _timesAmended As String
        Private _deliveryConfirmed As Boolean
        Private _deliveryAddress1 As String
        Private _deliveryAddress2 As String
        Private _deliveryAddress3 As String
        Private _deliveryAddress4 As String
        Private _deliveryPostCode As String
        Private _phoneNumber As String
        Private _phoneNumberMobile As String
        Private _isPrinted As Boolean
        Private _numberReprints As Decimal
        Private _revisionNumber As Decimal
        Private _merchandiseValue As Decimal
        Private _deliveryCharge As Decimal
        Private _qtyOrdered As Decimal
        Private _qtyTaken As Decimal
        Private _qtyRefunded As Decimal
        Private _orderWeight As Decimal
        Private _orderVolume As Decimal
        Private _tranDate As Date?
        Private _tranTill As String
        Private _tranNumber As String
        Private _refundTranDate As Date?
        Private _refundTill As String
        Private _refundTranNumber As String
        Private _dateDespatch As Date?
        Private _orderTakerId As String
        Private _managerId As String
        Private _deliveryStatus As Integer
        Private _refundStatus As Integer
        Private _sellingStoreId As Integer
        Private _sellingStoreOrderId As Integer
        Private _omOrderNumber As Integer
        Private _phoneNumberWork As String
        Private _customerAddress1 As String
        Private _customerAddress2 As String
        Private _customerAddress3 As String
        Private _customerAddress4 As String
        Private _customerPostCode As String
        Private _customerEmail As String
        Private _IsSuspended As Boolean
        'Changes for Hubs 2.0
        Private _sourceTransaction As String
        Private _sourceOrderNumber As String
        Private _extendedLeadTime As Boolean
        Private _deliveryContactName As String
        Private _deliveryContactPhone As String

        'Changes for Hubs 2.0 ends

        <ColumnMapping("Number")> Public Property Number() As String
            Get
                Return _number
            End Get
            Set(ByVal value As String)
                _number = value
            End Set
        End Property
        <ColumnMapping("CustomerNumber")> Public Property VendaNumber() As String
            Get
                Return _vendaNumber
            End Get
            Set(ByVal value As String)
                _vendaNumber = value
            End Set
        End Property
        <ColumnMapping("CustomerName")> Public Property CustomerName() As String
            Get
                Return _customerName
            End Get
            Set(ByVal value As String)
                _customerName = value
            End Set
        End Property
        <ColumnMapping("DateOrder")> Public Property DateOrder() As Date
            Get
                Return _dateOrder
            End Get
            Set(ByVal value As Date)
                _dateOrder = value
            End Set
        End Property
        <ColumnMapping("DateDelivery")> Public Property DateDelivery() As Date?
            Get
                Return _dateDelivery
            End Get
            Set(ByVal value As Date?)
                _dateDelivery = value
            End Set
        End Property
        <ColumnMapping("Status")> Public Property Status() As String
            Get
                Return _status
            End Get
            Set(ByVal value As String)
                _status = value
            End Set
        End Property
        <ColumnMapping("IsForDelivery")> Public Property IsForDelivery() As Boolean
            Get
                Return _isForDelivery
            End Get
            Set(ByVal value As Boolean)
                _isForDelivery = value
            End Set
        End Property
        <ColumnMapping("DeliveryConfirmed")> Public Property DeliveryConfirmed() As Boolean
            Get
                Return _deliveryConfirmed
            End Get
            Set(ByVal value As Boolean)
                _deliveryConfirmed = value
            End Set
        End Property
        <ColumnMapping("TimesAmended")> Public Property TimesAmended() As String
            Get
                Return _timesAmended
            End Get
            Set(ByVal value As String)
                _timesAmended = value
            End Set
        End Property
        <ColumnMapping("DeliveryAddress1")> Public Property DeliveryAddress1() As String
            Get
                Return _deliveryAddress1
            End Get
            Set(ByVal value As String)
                If (value Is Nothing) OrElse (value.Trim.Length = 0) Then
                    Me.ErrorSet(GetPropertyName(Function(f As QodHeader) f.DeliveryAddress1), My.Resources.Validation.DeliveryLineNothing)
                Else
                    Me.ErrorClear(GetPropertyName(Function(f As QodHeader) f.DeliveryAddress1))
                End If
                If value <> _deliveryAddress1 Then
                    _deliveryAddress1 = value
                End If
            End Set
        End Property
        <ColumnMapping("DeliveryAddress2")> Public Property DeliveryAddress2() As String
            Get
                Return _deliveryAddress2
            End Get
            Set(ByVal value As String)
                _deliveryAddress2 = value
            End Set
        End Property
        <ColumnMapping("DeliveryAddress3")> Public Property DeliveryAddress3() As String
            Get
                Return _deliveryAddress3
            End Get
            Set(ByVal value As String)
                If (value Is Nothing) OrElse (value.Trim.Length = 0) Then
                    Me.ErrorSet(GetPropertyName(Function(f As QodHeader) f.DeliveryAddress3), My.Resources.Validation.DeliveryLineNothing)
                Else
                    Me.ErrorClear(GetPropertyName(Function(f As QodHeader) f.DeliveryAddress3))
                End If
                If value <> _deliveryAddress3 Then
                    _deliveryAddress3 = value
                End If
            End Set
        End Property
        <ColumnMapping("DeliveryAddress4")> Public Property DeliveryAddress4() As String
            Get
                Return _deliveryAddress4
            End Get
            Set(ByVal value As String)
                _deliveryAddress4 = value
            End Set
        End Property
        <ColumnMapping("DeliveryPostCode")> Public Property DeliveryPostCode() As String
            Get
                Return _deliveryPostCode
            End Get
            Set(ByVal value As String)
                _deliveryPostCode = value
            End Set
        End Property
        <ColumnMapping("PhoneNumber")> Public Property PhoneNumber() As String
            Get
                Return _phoneNumber
            End Get
            Set(ByVal value As String)
                _phoneNumber = value
            End Set
        End Property
        <ColumnMapping("PhoneNumberMobile")> Public Property PhoneNumberMobile() As String
            Get
                Return _phoneNumberMobile
            End Get
            Set(ByVal value As String)
                _phoneNumberMobile = value
            End Set
        End Property
        <ColumnMapping("IsPrinted")> Public Property IsPrinted() As Boolean
            Get
                Return _isPrinted
            End Get
            Set(ByVal value As Boolean)
                _isPrinted = value
            End Set
        End Property
        <ColumnMapping("NumberReprints")> Public Property NumberReprints() As Decimal
            Get
                Return _numberReprints
            End Get
            Set(ByVal value As Decimal)
                _numberReprints = value
            End Set
        End Property
        <ColumnMapping("RevisionNumber")> Public Property RevisionNumber() As Decimal
            Get
                Return _revisionNumber
            End Get
            Set(ByVal value As Decimal)
                _revisionNumber = value
            End Set
        End Property
        <ColumnMapping("MerchandiseValue")> Public Property MerchandiseValue() As Decimal
            Get
                Return _merchandiseValue
            End Get
            Set(ByVal value As Decimal)
                _merchandiseValue = value
            End Set
        End Property
        <ColumnMapping("DeliveryCharge")> Public Property DeliveryCharge() As Decimal
            Get
                Return _deliveryCharge
            End Get
            Set(ByVal value As Decimal)
                _deliveryCharge = value
            End Set
        End Property
        <ColumnMapping("QtyOrdered")> Public Property QtyOrdered() As Decimal
            Get
                Return _qtyOrdered
            End Get
            Set(ByVal value As Decimal)
                _qtyOrdered = value
            End Set
        End Property
        <ColumnMapping("QtyTaken")> Public Property QtyTaken() As Decimal
            Get
                Return _qtyTaken
            End Get
            Set(ByVal value As Decimal)
                _qtyTaken = value
            End Set
        End Property
        <ColumnMapping("QtyRefunded")> Public Property QtyRefunded() As Decimal
            Get
                Return _qtyRefunded
            End Get
            Set(ByVal value As Decimal)
                _qtyRefunded = value
            End Set
        End Property
        <ColumnMapping("OrderWeight")> Public Property OrderWeight() As Decimal
            Get
                Return _orderWeight
            End Get
            Set(ByVal value As Decimal)
                _orderWeight = value
            End Set
        End Property
        <ColumnMapping("OrderVolume")> Public Property OrderVolume() As Decimal
            Get
                Return _orderVolume
            End Get
            Set(ByVal value As Decimal)
                _orderVolume = value
            End Set
        End Property
        <ColumnMapping("TranDate")> Public Property TranDate() As Date?
            Get
                Return _tranDate
            End Get
            Set(ByVal value As Date?)
                _tranDate = value
            End Set
        End Property
        <ColumnMapping("TranTill")> Public Property TranTill() As String
            Get
                Return _tranTill
            End Get
            Set(ByVal value As String)
                _tranTill = value
            End Set
        End Property
        <ColumnMapping("TranNumber")> Public Property TranNumber() As String
            Get
                Return _tranNumber
            End Get
            Set(ByVal value As String)
                _tranNumber = value
            End Set
        End Property
        <ColumnMapping("RefundTranDate")> Public Property RefundTranDate() As Date?
            Get
                Return _refundTranDate
            End Get
            Set(ByVal value As Date?)
                _refundTranDate = value
            End Set
        End Property
        <ColumnMapping("RefundTill")> Public Property RefundTill() As String
            Get
                Return _refundTill
            End Get
            Set(ByVal value As String)
                _refundTill = value
            End Set
        End Property
        <ColumnMapping("RefundTranNumber")> Public Property RefundTranNumber() As String
            Get
                Return _refundTranNumber
            End Get
            Set(ByVal value As String)
                _refundTranNumber = value
            End Set
        End Property
        <ColumnMapping("DateDespatch")> Public Property DateDespatch() As Date?
            Get
                Return _dateDespatch
            End Get
            Set(ByVal value As Date?)
                _dateDespatch = value
            End Set
        End Property
        <ColumnMapping("OrderTakerId")> Public Property OrderTakerId() As String
            Get
                Return _orderTakerId
            End Get
            Set(ByVal value As String)
                _orderTakerId = value
            End Set
        End Property
        <ColumnMapping("ManagerId")> Public Property ManagerId() As String
            Get
                Return _managerId
            End Get
            Set(ByVal value As String)
                _managerId = value
            End Set
        End Property
        <ColumnMapping("DeliveryStatus")> Public Property DeliveryStatus() As Integer
            Get
                Return _deliveryStatus
            End Get
            Set(ByVal value As Integer)
                _deliveryStatus = value
            End Set
        End Property
        <ColumnMapping("RefundStatus")> Public Property RefundStatus() As Integer
            Get
                Return _refundStatus
            End Get
            Set(ByVal value As Integer)
                _refundStatus = value
            End Set
        End Property
        <ColumnMapping("SellingStoreId")> Public Property SellingStoreId() As Integer
            Get
                Return _sellingStoreId
            End Get
            Set(ByVal value As Integer)
                _sellingStoreId = value
            End Set
        End Property
        <ColumnMapping("SellingStoreOrderId")> Public Property SellingStoreOrderId() As Integer
            Get
                Return _sellingStoreOrderId
            End Get
            Set(ByVal value As Integer)
                _sellingStoreOrderId = value
            End Set
        End Property
        <ColumnMapping("OMOrderNumber")> Public Property OmOrderNumber() As Integer
            Get
                Return _omOrderNumber
            End Get
            Set(ByVal value As Integer)
                _omOrderNumber = value
            End Set
        End Property
        <ColumnMapping("PhoneNumberWork")> Public Property PhoneNumberWork() As String
            Get
                Return _phoneNumberWork
            End Get
            Set(ByVal value As String)
                _phoneNumberWork = value
            End Set
        End Property
        <ColumnMapping("CustomerAddress1")> Public Property CustomerAddress1() As String
            Get
                Return _customerAddress1
            End Get
            Set(ByVal value As String)
                If (value Is Nothing) OrElse (value.Trim.Length = 0) Then
                    Me.ErrorSet(GetPropertyName(Function(f As QodHeader) f.CustomerAddress1), My.Resources.Validation.DeliveryLineNothing)
                Else
                    Me.ErrorClear(GetPropertyName(Function(f As QodHeader) f.CustomerAddress1))
                End If
                If value <> _customerAddress1 Then
                    _customerAddress1 = value
                End If
            End Set
        End Property
        <ColumnMapping("CustomerAddress2")> Public Property CustomerAddress2() As String
            Get
                Return _customerAddress2
            End Get
            Set(ByVal value As String)
                _customerAddress2 = value
            End Set
        End Property
        <ColumnMapping("CustomerAddress3")> Public Property CustomerAddress3() As String
            Get
                Return _customerAddress3
            End Get
            Set(ByVal value As String)
                If (value Is Nothing) OrElse (value.Trim.Length = 0) Then
                    Me.ErrorSet(GetPropertyName(Function(f As QodHeader) f.CustomerAddress3), My.Resources.Validation.DeliveryLineNothing)
                Else
                    Me.ErrorClear(GetPropertyName(Function(f As QodHeader) f.CustomerAddress3))
                End If
                If value <> _customerAddress3 Then
                    _customerAddress3 = value
                End If
            End Set
        End Property
        <ColumnMapping("CustomerAddress4")> Public Property CustomerAddress4() As String
            Get
                Return _customerAddress4
            End Get
            Set(ByVal value As String)
                _customerAddress4 = value
            End Set
        End Property
        <ColumnMapping("CustomerPostcode")> Public Property CustomerPostcode() As String
            Get
                Return _customerPostCode
            End Get
            Set(ByVal value As String)
                _customerPostCode = value
            End Set
        End Property
        <ColumnMapping("CustomerEmail")> Public Property CustomerEmail() As String
            Get
                Return _customerEmail
            End Get
            Set(ByVal value As String)
                _customerEmail = value
            End Set
        End Property
        <ColumnMapping("IsSuspended")> Public Property IsSuspended() As Boolean
            Get
                Return _IsSuspended
            End Get
            Set(ByVal value As Boolean)
                _IsSuspended = value
            End Set
        End Property

        'Added for hubs 2.0

        <ColumnMapping("Source")> Public Property Source() As String
            Get
                Return _sourceTransaction
            End Get
            Set(ByVal value As String)
                _sourceTransaction = value
            End Set
        End Property
        <ColumnMapping("SourceOrderNumber")> Public Property SourceOrderNumber() As String
            Get
                Return _sourceOrderNumber
            End Get
            Set(ByVal value As String)
                _sourceOrderNumber = value
            End Set
        End Property
        <ColumnMapping("ExtendedLeadTime")> Public Property ExtendedLeadTime() As Boolean
            Get
                Return _extendedLeadTime
            End Get
            Set(ByVal value As Boolean)
                _extendedLeadTime = value
            End Set
        End Property
        <ColumnMapping("DeliveryContactName")> Public Property DeliveryContactName() As String
            Get
                Return _deliveryContactName
            End Get
            Set(ByVal value As String)
                _deliveryContactName = value
            End Set
        End Property
        <ColumnMapping("DeliveryContactPhone")> Public Property DeliveryContactPhone() As String
            Get
                Return _deliveryContactPhone
            End Get
            Set(ByVal value As String)
                _deliveryContactPhone = value
            End Set
        End Property

        'Changes ends for hubs 2.0

#End Region

#Region "       Properties"
        Private _texts As QodTextCollection = Nothing
        Private _lines As QodLineCollection = Nothing
        Private _refunds As QodRefundCollection = Nothing
        Private _IncludeInPositiveAction As Boolean = False
        Private _IncludeInNegativeAction As Boolean = False
        Private _LocalStorePhase As String = String.Empty
        Private _StoreId As Integer = 0
        Private _ExistsInDB As Boolean = False
        Private _SellingStore As System.Store.Store
        Private _SearchedSellingStore As Boolean = False
        Private _SellingStoreName As String = String.Empty
        Private _SellingStorePhone As String = String.Empty

        Public ReadOnly Property Lines() As QodLineCollection
            Get
                If _lines Is Nothing Then LoadLines()
                Return _lines
            End Get
        End Property
        Public ReadOnly Property Texts() As QodTextCollection
            Get
                If _texts Is Nothing Then LoadTexts()
                Return _texts
            End Get
        End Property
        Public ReadOnly Property Refunds() As QodRefundCollection
            Get
                If _refunds Is Nothing Then LoadRefunds()
                Return _refunds
            End Get
        End Property

        ''' <summary>
        ''' Allows the QodHeaderActionFilter class to use this value as a means of filtering
        ''' </summary>
        ''' <value></value>
        ''' <history>
        ''' <created author="Charles McMahon" date="07/10/2010"></created>
        ''' </history>
        ''' <remarks></remarks>
        Public Property IncludeInPositiveAction() As Boolean
            Get
                Return _IncludeInPositiveAction
            End Get
            Set(ByVal value As Boolean)
                _IncludeInPositiveAction = value
            End Set
        End Property

        ''' <summary>
        ''' Allows the QodHeaderActionFilter class to use this value as a means of filtering
        ''' </summary>
        ''' <value></value>
        ''' <history>
        ''' <created author="Charles McMahon" date="07/10/2010"></created>
        ''' </history>
        ''' <remarks></remarks>
        Public Property IncludeInNegativeAction() As Boolean
            Get
                Return _IncludeInNegativeAction
            End Get
            Set(ByVal value As Boolean)
                _IncludeInNegativeAction = value
            End Set
        End Property

        ''' <summary>
        ''' Return the delivery phase relavent to the local store, not the QodHeader
        ''' </summary>
        ''' <value></value>
        ''' <history>
        ''' <created author="Charles McMahon" date="07/10/2010"></created>
        ''' </history>
        ''' <remarks></remarks>
        Public ReadOnly Property LocalStorePhase() As String
            Get
                _LocalStorePhase = State.DeliveryDescription.GetPhase(Me.Lines.DeliveryStatus(ThisStore.Id4))
                Return _LocalStorePhase
            End Get
        End Property

        Public ReadOnly Property NumberStockIssues() As Integer
            Get
                Return Lines.NumberStockIssues
            End Get
        End Property
        Public ReadOnly Property DespatchSite() As String
            Get
                Return Me.Lines.DespatchSites
            End Get
        End Property
        Public ReadOnly Property DeliveryAddress() As String
            Get
                Dim strings As String() = {Me.DeliveryAddress1, Me.DeliveryAddress2, Me.DeliveryAddress3, Me.DeliveryAddress4, Me.DeliveryPostCode}
                Return Concetenate(strings)
            End Get
        End Property
        Public ReadOnly Property CustomerAddress() As String
            Get
                Dim strings As String() = {Me.CustomerAddress1, Me.CustomerAddress2, Me.CustomerAddress3, Me.CustomerAddress4, Me.CustomerPostcode}
                Return Concetenate(strings)
            End Get
        End Property
        Public ReadOnly Property DeliverySourceMissing() As Boolean
            Get
                Return Lines.DeliverySourceMissing
            End Get
        End Property
        Public Property DeliveryState() As State.Delivery
            Get
                Return CType(Me.DeliveryStatus, State.Delivery)
            End Get
            Set(ByVal value As State.Delivery)
                Me.DeliveryStatus = CInt(value)
            End Set
        End Property
        Public ReadOnly Property DeliveryStateDescription() As String
            Get
                Return State.DeliveryDescription.GetDescription(Me.DeliveryState)
            End Get
        End Property
        Public ReadOnly Property DeliveryStatePhase() As String
            Get
                Return State.DeliveryDescription.GetPhase(Me.DeliveryState)
            End Get
        End Property

        ''' <summary>
        ''' This is the local store id to check despatch stores against
        ''' </summary>
        ''' <value></value>
        ''' <history>
        ''' <updated author="Charles McMahon" date="07/10/2010"></updated>
        ''' </history>
        ''' <remarks>This did exist before, but did not return or store any values</remarks>
        Public Property StoreId() As Integer
            Get
                Return _StoreId
            End Get
            Set(ByVal value As Integer)
                _StoreId = value
            End Set
        End Property

        Public Property ExistsInDB() As Boolean
            Get
                Return _ExistsInDB
            End Get
            Set(ByVal value As Boolean)
                _ExistsInDB = value
            End Set
        End Property

        Public ReadOnly Property IsLocalStoreMaintainable() As Boolean
            Get
                Dim IsMaintainable As Boolean = False
                For Each QodLine As QodLine In Me.Lines
                    If QodLine.DeliverySource = StoreId.ToString("0000") AndAlso _
                    (QodLine.DeliveryStatus >= Delivery.IbtInAllStock AndAlso QodLine.DeliveryStatus <= Delivery.PickingStatusOk) OrElse _
                    (QodLine.DeliveryStatus >= Delivery.UndeliveredCreated AndAlso QodLine.DeliveryStatus <= Delivery.UndeliveredStatusOk) Then
                        IsMaintainable = True
                    End If
                Next
                Return IsMaintainable
            End Get
        End Property

        Public ReadOnly Property SellingStoreName() As String
            Get
                If _SellingStoreName = String.Empty Then
                    If SellingStore Is Nothing Then
                        _SellingStoreName = "Unavailable"
                    Else
                        _SellingStoreName = SellingStore.Name
                    End If
                End If
                Return _SellingStoreName
            End Get
        End Property

        Public ReadOnly Property SellingStorePhone() As String
            Get
                If _SellingStorePhone = String.Empty Then
                    If SellingStore Is Nothing Then
                        _SellingStorePhone = "Unavailable"
                    Else
                        _SellingStorePhone = SellingStore.PhoneNumber
                    End If
                End If
                Return _SellingStorePhone
            End Get
        End Property

        Public ReadOnly Property SellingStore() As System.Store.Store
            Get
                If _SellingStore Is Nothing AndAlso Not _SearchedSellingStore Then
                    _SellingStore = AllStores.GetStore(_sellingStoreId)
                    _SearchedSellingStore = True
                End If
                Return _SellingStore
            End Get
        End Property
#End Region

        Public Function Clone() As QodHeader

            Try
                'Create a string writer and serialise the object to be cloned (Me) to XML
                Dim objStringWriter As New StringWriter
                Dim xmlQod As New XmlSerializer(Me.GetType)
                xmlQod.Serialize(objStringWriter, Me)
                'Create a string reader and deserialise the XML into a new object (_tempQod)
                Dim objStringReader As New StringReader(objStringWriter.ToString)
                Dim _tempQod As QodHeader = CType(xmlQod.Deserialize(objStringReader), QodHeader)
                Return _tempQod
            Catch ex As Exception
                Return Nothing
            End Try

        End Function

        Public Sub SetDeliveryStatusOk()

            Dim value As Delivery = CType(Me.DeliveryStatus, Delivery)
            Select Case CType(Me.DeliveryStatus, Delivery)
                Case Is < Delivery.DeliveryRequestAccepted : value = Delivery.DeliveryRequestAccepted
                Case Is < Delivery.PickingStatusOk : value = Delivery.PickingStatusOk
                Case Is < Delivery.DespatchStatusOk : value = Delivery.DespatchStatusOk
                Case Is < Delivery.UndeliveredStatusOk : value = Delivery.UndeliveredStatusOk
                Case Is < Delivery.DeliveredStatusOk : value = Delivery.DeliveredStatusOk
            End Select
            If value > Me.DeliveryStatus Then Me.DeliveryStatus = value

        End Sub

        Public Sub SetRefundStatusOk()

            Dim value As Refund = CType(Me.RefundStatus, Refund)
            Select Case CType(Me.RefundStatus, Refund)
                Case Is < Refund.StatusUpdateOk : value = Refund.StatusUpdateOk
            End Select
            If value > Me.RefundStatus Then Me.RefundStatus = value

        End Sub

        Public Function SetDeliveryStatus(ByVal newStatus As Integer) As Integer

            If Me.DeliveryStatus = Delivery.DeliveredStatusOk Then
                Return Me.DeliveryStatus
            End If

            If newStatus >= State.Delivery.OrderFailedConnection And newStatus <= Delivery.OrderFailedData Then
                Me.DeliveryStatus = CInt((Me.DeliveryStatus \ 100) & newStatus.ToString)
                Return Me.DeliveryStatus
            End If

            If newStatus > Me.DeliveryStatus Then
                Me.DeliveryStatus = newStatus
            ElseIf Me.DeliveryStatus > State.Delivery.DespatchStatusOk AndAlso Me.DeliveryStatus < State.Delivery.DeliveredCreated Then
                Me.DeliveryStatus = newStatus
            End If

            Return Me.DeliveryStatus

        End Function

        Public Function SetDeliveryStatusAsLines(ByVal storeId As Integer) As Integer
            DeliveryStatus = Lines.DeliveryStatus(storeId)
            Return DeliveryStatus
        End Function

        Public Function SetRefundStatus(ByVal newStatus As Integer) As Integer

            If newStatus >= State.Refund.None And newStatus <= Delivery.OrderOk Then
                Me.RefundStatus = CInt((Me.RefundStatus \ 100) & newStatus.ToString)
                Return Me.RefundStatus
            End If

            If newStatus > Me.RefundStatus Then
                Me.RefundStatus = CInt(IIf(newStatus = Refund.NotifyOk, Refund.StatusUpdateOk, newStatus))
                Return Me.RefundStatus
            End If

            Return Me.RefundStatus

        End Function

        Public Sub New()
            MyBase.New()
        End Sub

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub

        Public Sub New(ByRef fulfilmentRequest As WebService.CTSFulfilmentResponse)
            MyBase.New()
            Me.VendaNumber = fulfilmentRequest.OrderHeader.CustomerAccountNo.Value
            Me.CustomerName = fulfilmentRequest.OrderHeader.CustomerName.Value
            Me.DateOrder = fulfilmentRequest.OrderHeader.SaleDate.Value
            Me.DateDelivery = fulfilmentRequest.OrderHeader.RequiredDeliveryDate.Value
            Me.IsForDelivery = fulfilmentRequest.OrderHeader.ToBeDelivered.Value
            Me.DeliveryAddress1 = fulfilmentRequest.OrderHeader.DeliveryAddressLine1.Value
            Me.DeliveryAddress2 = fulfilmentRequest.OrderHeader.DeliveryAddressLine2.Value
            Me.DeliveryAddress3 = fulfilmentRequest.OrderHeader.DeliveryAddressTown.Value
            Me.DeliveryAddress4 = fulfilmentRequest.OrderHeader.DeliveryAddressLine4.Value
            Me.DeliveryPostCode = fulfilmentRequest.OrderHeader.DeliveryPostcode.Value
            Me.PhoneNumber = fulfilmentRequest.OrderHeader.ContactPhoneHome.Value
            Me.MerchandiseValue = fulfilmentRequest.OrderHeader.TotalOrderValue.Value
            Me.DeliveryCharge = fulfilmentRequest.OrderHeader.DeliveryCharge.Value
            Me.TranDate = fulfilmentRequest.OrderHeader.SaleDate.Value
            Me.PhoneNumberMobile = fulfilmentRequest.OrderHeader.ContactPhoneMobile.Value
            Me.CustomerName = fulfilmentRequest.OrderHeader.CustomerName.Value

            Me.DeliveryStatus = CInt(fulfilmentRequest.OrderHeader.OrderStatus.Value)
            Me.SellingStoreId = CInt(fulfilmentRequest.OrderHeader.SellingStoreCode.Value)
            Me.SellingStoreOrderId = CInt(fulfilmentRequest.OrderHeader.SellingStoreOrderNumber.Value)
            Me.CustomerAddress1 = fulfilmentRequest.OrderHeader.CustomerAddressLine1.Value
            Me.CustomerAddress2 = fulfilmentRequest.OrderHeader.CustomerAddressLine2.Value
            Me.CustomerAddress3 = fulfilmentRequest.OrderHeader.CustomerAddressTown.Value
            Me.CustomerAddress4 = fulfilmentRequest.OrderHeader.CustomerAddressLine4.Value
            Me.CustomerPostcode = fulfilmentRequest.OrderHeader.CustomerPostcode.Value
            Me.CustomerEmail = fulfilmentRequest.OrderHeader.ContactEmail.Value
            Me.PhoneNumberWork = fulfilmentRequest.OrderHeader.ContactPhoneWork.Value
            Me.OmOrderNumber = CInt(fulfilmentRequest.OrderHeader.OMOrderNumber.Value)
        End Sub

        Public Sub New(ByVal qodNumber As String)
            MyBase.New()
            Dim dt As DataTable = Qod.DataAccess.GetHeaderForQodNumber(qodNumber)
            MyBase.Load(dt.Rows(0))
        End Sub

        Public Sub New(ByRef qodCreate As WebService.CTSQODCreateResponse, ByVal TranTill As String, ByVal TranNumber As String)
            MyBase.New()

            Using log As New Output("QodHeader.New")
                log.Write()
                log.Write("Creating Header for " & TranTill & " " & TranNumber)

                Me.TranDate = Now
                Me.TranTill = TranTill
                Me.TranNumber = TranNumber
                Me.Source = qodCreate.OrderHeader.Source.Value
                Me.SourceOrderNumber = qodCreate.OrderHeader.SourceOrderNumber.Value
                Me.Number = qodCreate.OrderHeader.StoreOrderNumber.Value
                Me.DateDelivery = qodCreate.OrderHeader.RequiredDeliveryDate.Value
                Me.DeliveryCharge = qodCreate.OrderHeader.DeliveryCharge.Value
                Me.MerchandiseValue = qodCreate.OrderHeader.TotalOrderValue.Value
                Me.DateOrder = qodCreate.OrderHeader.SaleDate.Value
                Me.SellingStoreId = CInt(ThisStore.Id4.ToString)
                Me.CustomerName = qodCreate.OrderHeader.CustomerName.Value
                Me.CustomerAddress1 = qodCreate.OrderHeader.CustomerAddressLine1.Value
                Me.CustomerAddress2 = qodCreate.OrderHeader.CustomerAddressLine2.Value
                Me.CustomerAddress3 = qodCreate.OrderHeader.CustomerAddressTown.Value
                Me.CustomerAddress4 = qodCreate.OrderHeader.CustomerAddressLine4.Value
                Me.CustomerPostcode = qodCreate.OrderHeader.CustomerPostcode.Value
                Me.DeliveryAddress1 = qodCreate.OrderHeader.DeliveryAddressLine1.Value
                Me.DeliveryAddress2 = qodCreate.OrderHeader.DeliveryAddressLine2.Value
                Me.DeliveryAddress3 = qodCreate.OrderHeader.DeliveryAddressTown.Value
                Me.DeliveryAddress4 = qodCreate.OrderHeader.DeliveryAddressLine4.Value
                Me.DeliveryPostCode = qodCreate.OrderHeader.DeliveryPostcode.Value
                Me.PhoneNumber = qodCreate.OrderHeader.ContactPhoneHome.Value
                Me.PhoneNumberMobile = qodCreate.OrderHeader.ContactPhoneMobile.Value
                Me.PhoneNumberWork = qodCreate.OrderHeader.ContactPhoneWork.Value
                Me.CustomerEmail = qodCreate.OrderHeader.ContactEmail.Value
                Me.IsForDelivery = qodCreate.OrderHeader.ToBeDelivered.Value
                Me.ExtendedLeadTime = qodCreate.OrderHeader.ExtendedLeadTime.Value
                Me.DeliveryContactName = qodCreate.OrderHeader.DeliveryContactName.Value
                Me.DeliveryContactPhone = qodCreate.OrderHeader.DeliveryContactPhone.Value
                Me.DeliveryStatus = State.Delivery.DeliveryRequest
                Me.SellingStoreId = ThisStore.Id4
                Me.OrderTakerId = "000"
                Me.ManagerId = "000"
                Me.IsForDelivery = True

                log.Write("Creating Lines")
                'Now create all related lines and texts
                Dim OrderQuantity As Integer = 0
                Dim Index As Integer = 1
                For Each QodCreateLine As WebService.CTSQODCreateResponseOrderLine In qodCreate.OrderLines
                    If Not QodCreateLine.RecordSaleOnly.Value Then
                        Dim QodLine As New QodLine(QodCreateLine)
                        OrderQuantity += QodLine.QtyOrdered
                        QodLine.DeliveryStatus = State.Delivery.DeliveryRequest
                        QodLine.SellingStoreId = Me.SellingStoreId
                        QodLine.OrderNumber = Me.Number
                        QodLine.Number = Index.ToString.PadLeft(4, "0"c)
                        QodCreateLine.StoreOrderLineNo.Value = QodLine.Number
                        Me.Lines.Add(QodLine)
                    End If
                    Index += 1
                Next

                Me.QtyOrdered = OrderQuantity

                log.Write("Creating Texts")

                Index = 1
                If qodCreate.OrderHeader.DeliveryInstructions.InstructionLine IsNot Nothing Then
                    For Each QodCreateText As WebService.CTSQODCreateResponseOrderHeaderDeliveryInstructionsInstructionLine In _
                            qodCreate.OrderHeader.DeliveryInstructions.InstructionLine
                        Dim QodText As QodText = Me.Texts.AddNew(QodTextType.Type.DeliveryInstruction)
                        QodText.SellingStoreId = ThisStore.Id4

                        'this falls over because me.number is an empty string but the "SellingStoreOrderId" property expects an integer
                        'QodText.SellingStoreOrderId = Me.Number

                        QodText.OrderNumber = Me.Number
                        QodText.Type = CStr(QodTextType.Type.DeliveryInstruction)
                        QodText.Text = QodCreateText.LineText.Value
                        QodText.Number = Index.ToString.PadLeft(2, "0"c)
                        Index += 1
                    Next
                End If
            End Using
        End Sub

        Public Function HasChanged(ByVal configxml As XmlNode) As Boolean

            Try
                Dim myType As Type = Me.GetType
                Dim myProperties() As PropertyInfo = Me.GetType.GetProperties
                Dim PropertyNames As XmlNodeList = configxml.SelectNodes("QodHeader/Properties/Name")
                Dim PropertyItem As PropertyInfo
                Dim KeyName() As String
                KeyName = configxml.SelectSingleNode("QodHeader").Attributes("Key").Value.Split(";"c)
                Dim KeyValue As String = CStr(myType.GetProperty(KeyName(0)).GetValue(Me, Nothing))
                Dim TempHeader As New QodHeader(KeyValue)

                'First check the header
                For Each PropertyName As XmlNode In PropertyNames
                    PropertyItem = Me.GetType.GetProperty(PropertyName.InnerText)
                    Try
                        If PropertyItem.GetValue(Me, Nothing) IsNot Nothing AndAlso PropertyItem.GetValue(TempHeader, Nothing) IsNot Nothing Then
                            If PropertyItem.GetValue(Me, Nothing).ToString <> PropertyItem.GetValue(TempHeader, Nothing).ToString Then
                                Return True
                            End If
                        ElseIf PropertyItem.GetValue(Me, Nothing) Is Nothing And PropertyItem.GetValue(TempHeader, Nothing) IsNot Nothing Then
                            Return True
                        ElseIf PropertyItem.GetValue(Me, Nothing) IsNot Nothing And PropertyItem.GetValue(TempHeader, Nothing) Is Nothing Then
                            Return True
                        End If
                    Catch
                    End Try
                Next

                'Now check the lines
                KeyName = configxml.SelectSingleNode("QodLine").Attributes("Key").Value.Split(";"c)
                For Each QodLine As QodLine In Me.Lines
                    myType = QodLine.GetType
                    For Each TempLine As QodLine In TempHeader.Lines
                        Dim FoundMatch As Boolean = True
                        For Index = 0 To UBound(KeyName)
                            If Not Equals(myType.GetProperty(KeyName(Index)).GetValue(QodLine, Nothing), myType.GetProperty(KeyName(Index)).GetValue(TempLine, Nothing)) Then
                                FoundMatch = False
                            End If
                        Next
                        If FoundMatch AndAlso QodLine.HasChanged(configxml, TempLine) Then
                            Return True
                        End If
                    Next
                Next

                'Now check the delivery instructions
                KeyName = configxml.SelectSingleNode("QodText").Attributes("Key").Value.Split(";"c)
                For Each QodText As QodText In Me.Texts
                    myType = QodText.GetType
                    For Each TempText As QodText In TempHeader.Texts
                        Dim FoundMatch As Boolean = True
                        For Index = 0 To UBound(KeyName)
                            If Not Equals(myType.GetProperty(KeyName(Index)).GetValue(QodText, Nothing), myType.GetProperty(KeyName(Index)).GetValue(TempText, Nothing)) Then
                                FoundMatch = False
                            End If
                        Next
                        If FoundMatch AndAlso QodText.HasChanged(configxml, TempText) Then
                            Return True
                        End If
                    Next
                Next

            Catch
                Return True
            End Try
            Return False

        End Function

        Public Function IsDeliveryFailure() As Boolean

            Dim delivery As Integer = CInt(Me.DeliveryStatus)
            Do
                delivery -= 50
            Loop Until (delivery < 0)
            delivery += 50

            Select Case delivery
                Case 10, 20, 30 : Return True
                Case Else : Return False
            End Select

        End Function

        Public Function IsSuspendable() As Boolean
            Return (Me.DeliveryStatus >= Delivery.IbtOutAllStock AndAlso Me.DeliveryStatus < Delivery.DespatchCreated)
        End Function

        Public Function IsMaintainable() As Boolean
            Return (Me.DeliveryStatus >= Delivery.IbtInAllStock AndAlso Me.DeliveryStatus <= Delivery.PickingStatusOk) OrElse _
            (Me.DeliveryStatus >= Delivery.UndeliveredCreated AndAlso Me.DeliveryStatus <= Delivery.UndeliveredStatusOk)
        End Function

        Public Function IsRefundable() As Boolean
            Return (Me.DeliveryStatus < Delivery.DespatchCreated)
        End Function

        Public Function HasDeliverySourceMissing() As Boolean
            Return Lines.DeliverySourceMissing
        End Function

        Public Function IsAwaitingOmAssignment() As Boolean
            Return (Me.DeliveryStatus < Delivery.DeliveryRequestAccepted)
        End Function

        Public Function IsAwaitingOmConfirmation() As Boolean
            Return (Me.DeliveryStatus >= Delivery.DeliveryRequestAccepted AndAlso Me.DeliveryStatus < Delivery.IbtOutAllStock)
        End Function

        Public Function IsAwaitingStoreConfirmation() As Boolean
            Return (Me.DeliveryStatus >= Delivery.IbtOutAllStock AndAlso Me.DeliveryStatus < Delivery.ReceiptNotifyCreate)
        End Function

        Public Function IsAwaitingPicking() As Boolean
            Return (Me.DeliveryStatus >= Delivery.ReceiptNotifyCreate AndAlso Me.DeliveryStatus < Delivery.PickingListCreated)
        End Function

        Public Function IsAwaitingDespatch() As Boolean
            Return (Me.DeliveryStatus >= Delivery.PickingListCreated AndAlso Me.DeliveryStatus < Delivery.DespatchCreated)
        End Function

        Public Function IsAwaitingDeliveryConfirmation() As Boolean
            Return (Me.DeliveryStatus >= Delivery.DespatchCreated AndAlso Me.DeliveryStatus < Delivery.UndeliveredCreated)
        End Function

        Public Function IsFailedDelivery() As Boolean
            Return (Me.DeliveryStatus >= Delivery.UndeliveredCreated AndAlso Me.DeliveryStatus < Delivery.DeliveredCreated)
        End Function

        Public Function IsCompletedDelivery() As Boolean
            Return (Me.DeliveryStatus >= Delivery.DeliveredCreated AndAlso Me.DeliveryStatus < Delivery.Legacy)
        End Function

        Public Function IsPickedAndNotDespatched() As Boolean
            Return (Me.DeliveryStatus >= Delivery.PickingStatusOk AndAlso Me.DeliveryStatus < Delivery.DespatchCreated)
        End Function

        Public Function RequiresRefundHolding() As Boolean
            Return ((Me.RefundStatus > Refund.None) AndAlso (Me.RefundStatus < Refund.NotifyOk) AndAlso (Me.DeliveryStatus < Delivery.IbtOutAllStock))
        End Function

        Public Function RequiresOrderManagerCreation() As Boolean
            Return (Me.DeliveryStatus < Delivery.DeliveryRequestAccepted)
        End Function

        Public Sub IncrementRevisionNumber()
            Me.RevisionNumber += 1
        End Sub

        Public Sub LoadLines()
            _lines = New QodLineCollection
            If Me.Number IsNot Nothing Then _lines.Load(Me.Number)
        End Sub

        Public Sub LoadTexts()
            _texts = New QodTextCollection
            If Me.Number IsNot Nothing Then _texts.Load(Me.Number)
        End Sub

        Public Sub LoadRefunds()
            _refunds = New QodRefundCollection
            If Me.Number IsNot Nothing Then _refunds.Load(CInt(Me.Number))
        End Sub

        Public Sub LoadRefunds(ByVal RefundStatus As Integer)
            _refunds = New QodRefundCollection
            If Me.Number IsNot Nothing Then _refunds.Load(CInt(Me.Number), RefundStatus)
        End Sub

        Public Function SelfPersist() As Integer
            Dim LinesUpdated As Integer = 0
            Using con As New Connection
                Try
                    con.StartTransaction()
                    LinesUpdated = Persist(con)
                    con.CommitTransaction()
                Catch ex As Exception
                    con.RollbackTransaction()
                End Try
            End Using
            Return LinesUpdated
        End Function

        Public Function SelfPersistTree() As Integer
            Dim LinesUpdated As Integer = 0
            Using con As New Connection
                Try
                    con.StartTransaction()
                    LinesUpdated = PersistTree(con)
                    con.CommitTransaction()
                Catch ex As Exception
                    con.RollbackTransaction()
                End Try
            End Using
            Return LinesUpdated
        End Function

        Public Function Persist(ByVal con As Connection) As Integer
            If Not ExistsInDB Then
                Return PersistNew(con)
            Else
                Dim LinesUpdated As Integer = 0
                Dim sb As StringBuilder
                Using com As New Command(con)
                    Select Case con.DataProvider
                        Case DataProvider.Odbc
                            sb = New StringBuilder
                            sb.Append("update CORHDR set ")
                            sb.Append("DELD=?, ")
                            sb.Append("DELC=?, ")
                            sb.Append("ADDR1=?, ")
                            sb.Append("ADDR2=?, ")
                            sb.Append("ADDR3=?, ")
                            sb.Append("ADDR4=?, ")
                            sb.Append("POST=?, ")
                            sb.Append("PHON=?, ")
                            sb.Append("MOBP=?, ")
                            sb.Append("REVI=?, ")
                            sb.Append("PRNT=?, ")
                            sb.Append("RPRN=?, ")
                            sb.Append("QTYR=?, ")
                            sb.Append("QTYT=?, ")
                            sb.Append("QTYO=?, ")
                            sb.Append("CANC=?, ")
                            sb.Append("STIL=?, ")
                            sb.Append("STRN=? ")
                            sb.Append("where NUMB=?")

                            com.CommandText = sb.ToString
                            com.AddParameter("DELD", Me.DateDelivery)
                            com.AddParameter("DELC", Me.DeliveryConfirmed)
                            com.AddParameter("ADDR1", Me.DeliveryAddress1)
                            com.AddParameter("ADDR2", IIf(Me.DeliveryAddress2 IsNot Nothing, Me.DeliveryAddress2, ""))
                            com.AddParameter("ADDR3", Me.DeliveryAddress3)
                            com.AddParameter("ADDR4", IIf(Me.DeliveryAddress4 IsNot Nothing, Me.DeliveryAddress4, ""))
                            com.AddParameter("POST", Me.DeliveryPostCode)
                            com.AddParameter("PHON", IIf(Me.PhoneNumber IsNot Nothing, Me.PhoneNumber, ""))
                            com.AddParameter("MOBP", IIf(Me.PhoneNumberMobile IsNot Nothing, Me.PhoneNumberMobile, ""))
                            com.AddParameter("REVI", Me.RevisionNumber)
                            com.AddParameter("PRNT", Me.IsPrinted)
                            com.AddParameter("RPRN", Me.NumberReprints)
                            com.AddParameter("QTYR", Me.QtyRefunded)
                            com.AddParameter("QTYT", Me.QtyTaken)
                            com.AddParameter("QTYO", Me.QtyOrdered)
                            com.AddParameter("CANC", Me.Status)
                            com.AddParameter("STIL", Me.TranTill)
                            com.AddParameter("STRN", Me.TranNumber)
                            com.AddParameter("NUMB", Me.Number)

                            LinesUpdated += com.ExecuteNonQuery

                            sb = New StringBuilder
                            sb.Append("Update CORHDR4 set ")
                            sb.Append("DELD=?, ")
                            If Me.DateDespatch.HasValue Then sb.Append("DDAT=?, ")
                            sb.Append("DeliveryStatus=?, ")
                            sb.Append("RefundStatus=?, ")
                            sb.Append("CustomerEmail=?, ")
                            sb.Append("PhoneNumberWork=?, ")
                            sb.Append("OMOrderNumber=?, ")
                            sb.Append("IsSuspended=? ")
                            sb.Append("where NUMB=?")

                            com.CommandText = sb.ToString
                            com.ClearParamters()
                            com.AddParameter("DELD", Me.DateDelivery)
                            If Me.DateDespatch.HasValue Then com.AddParameter("DDAT", Me.DateDespatch)
                            com.AddParameter("DeliveryStatus", Me.DeliveryStatus)
                            com.AddParameter("RefundStatus", Me.RefundStatus)
                            com.AddParameter("CustomerEmail", IIf(Me.CustomerEmail IsNot Nothing, Me.CustomerEmail, ""))
                            com.AddParameter("PhoneNumberWork", IIf(Me.PhoneNumberWork IsNot Nothing, Me.PhoneNumberWork, ""))
                            com.AddParameter("OMOrderNumber", Me.OmOrderNumber)
                            com.AddParameter("IsSuspended", IIf(Me.IsSuspended, 1, 0))
                            com.AddParameter("NUMB", Me.Number)
                            LinesUpdated += com.ExecuteNonQuery

                            'added for hubs 2
                            sb = New StringBuilder
                            sb.Append("Update CORHDR5 set ")
                            sb.Append("Source=?, ")
                            sb.Append("SourceOrderNumber=?, ")

                            'these fields are no longer required
                            'sb.Append("SellingStoreTill=?, ")
                            'sb.Append("SellingStoreTransaction=?, ")

                            sb.Append("where NUMB=?")
                            com.AddParameter("Source", Source)
                            com.AddParameter("SourceOrderNumber", SourceOrderNumber)
                            com.AddParameter("NUMB", Number)
                            LinesUpdated += com.ExecuteNonQuery

                        Case DataProvider.Sql
                            'com.StoredProcedureName = My.Resources.Procedures.QodHeaderUpdate
                            com.AddParameter(My.Resources.Parameters.OrderNumber, Me.Number)
                            If Me.DateDelivery.HasValue Then com.AddParameter(My.Resources.Parameters.DateDelivery, Me.DateDelivery.Value)
                            com.AddParameter(My.Resources.Parameters.DeliveryConfirmed, Me.DeliveryConfirmed)
                            If Me.DateDespatch.HasValue Then com.AddParameter(My.Resources.Parameters.DateDespatch, Me.DateDespatch.Value)
                            If Me.DeliveryAddress1 IsNot Nothing Then com.AddParameter(My.Resources.Parameters.DeliveryAddress1, Me.DeliveryAddress1)
                            If Me.DeliveryAddress2 IsNot Nothing Then com.AddParameter(My.Resources.Parameters.DeliveryAddress2, Me.DeliveryAddress2)
                            If Me.DeliveryAddress3 IsNot Nothing Then com.AddParameter(My.Resources.Parameters.DeliveryAddress3, Me.DeliveryAddress3)
                            If Me.DeliveryAddress4 IsNot Nothing Then com.AddParameter(My.Resources.Parameters.DeliveryAddress4, Me.DeliveryAddress4)
                            If Me.DeliveryPostCode IsNot Nothing Then com.AddParameter(My.Resources.Parameters.DeliveryPostCode, Me.DeliveryPostCode)
                            If Me.PhoneNumber IsNot Nothing Then com.AddParameter(My.Resources.Parameters.PhoneNumber, Me.PhoneNumber)
                            If Me.PhoneNumberMobile IsNot Nothing Then com.AddParameter(My.Resources.Parameters.PhoneNumberMobile, Me.PhoneNumberMobile)
                            com.AddParameter(My.Resources.Parameters.DeliveryStatus, Me.DeliveryStatus)
                            com.AddParameter(My.Resources.Parameters.RefundStatus, Me.RefundStatus)
                            If Me.CustomerEmail IsNot Nothing Then com.AddParameter(My.Resources.Parameters.CustomerEmail, Me.CustomerEmail)
                            If Me.PhoneNumberWork IsNot Nothing Then com.AddParameter(My.Resources.Parameters.PhoneNumberWork, Me.PhoneNumberWork)
                            com.AddParameter(My.Resources.Parameters.OmOrderNumber, Me.OmOrderNumber)
                            com.AddParameter(My.Resources.Parameters.IsSuspended, Me.IsSuspended)
                            com.AddParameter(My.Resources.Parameters.RevisionNumber, Me.RevisionNumber)
                            com.AddParameter(My.Resources.Parameters.IsPrinted, Me.IsPrinted)
                            'added for hubs2
                            com.AddParameter(My.Resources.Parameters.Source, Source)
                            com.AddParameter(My.Resources.Parameters.SourceOrderNumber, SourceOrderNumber)


                            'stored procedure will fail because the following 2 parameters have been commented out
                            'do not have time to fix at this moment, sorry PD 20110125

                            'com.AddParameter(My.Resources.Parameters.SellingStoreTill, TranTill)
                            'com.AddParameter(My.Resources.Parameters.SellingStoreTransaction, TranNumber)

                            com.AddParameter(My.Resources.Parameters.NumberReprints, Me.NumberReprints)
                            com.AddParameter(My.Resources.Parameters.QtyOrdered, Me.QtyOrdered)
                            com.AddParameter(My.Resources.Parameters.QtyReturned, Me.QtyRefunded)
                            com.AddParameter(My.Resources.Parameters.QtyTaken, Me.QtyTaken)
                            com.AddParameter(My.Resources.Parameters.CancelledStatus, Me.Status)
                            LinesUpdated += com.ExecuteNonQuery()

                    End Select
                End Using
                Return LinesUpdated
            End If

        End Function

        Private Function PersistNew(ByVal con As Connection) As Integer
            Dim isSellingStore As Boolean = (SellingStoreId = ThisStore.Id4)
            Dim LinesUpdated As Integer = 0
            Dim sb As StringBuilder
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        'get next receipt number from system numbers (check not over limit)
                        com.CommandText = "Select NEXT15 from SYSNUM where FKEY='01'"
                        Dim orderNumber As Integer = CInt(com.ExecuteValue)
                        If orderNumber > 999998 Then orderNumber = 1

                        'update next number and set order item number
                        com.CommandText = "Update SYSNUM set NEXT15=? where FKEY='01'"
                        com.AddParameter("Number", (orderNumber + 1).ToString("000000"))
                        LinesUpdated += com.ExecuteNonQuery()
                        Me.Number = orderNumber.ToString("000000")
                        If isSellingStore Then Me.SellingStoreOrderId = orderNumber

                        'insert into corhdr
                        sb = New StringBuilder
                        sb.Append("Insert into CORHDR (")
                        sb.Append("NUMB,CUST,NAME,DATE1,DELD,CANC,DELI,DELC,AMDT,ADDR1,ADDR2,ADDR3,ADDR4,POST,PHON,MOBP,")
                        sb.Append("PRNT,RPRN,REVI,MVST,DCST,QTYO,QTYT,QTYR,WGHT,VOLU,SDAT,STIL,STRN ")
                        sb.Append(") values (")
                        sb.Append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )")
                        com.CommandText = sb.ToString
                        com.ClearParamters()
                        com.AddParameter("NUMB", Me.Number)
                        com.AddParameter("CUST", IIf(Me.VendaNumber IsNot Nothing, Me.VendaNumber, ""))
                        com.AddParameter("NAME", IIf(Me.CustomerName IsNot Nothing, Me.CustomerName, ""))
                        com.AddParameter("DATE1", Me.DateOrder)
                        com.AddParameter("DELD", IIf(Me.DateDelivery IsNot Nothing, Me.DateDelivery, ""))
                        com.AddParameter("CANC", IIf(Me.Status IsNot Nothing, Me.Status, ""))
                        com.AddParameter("DELI", Me.IsForDelivery)
                        com.AddParameter("DELC", Me.DeliveryConfirmed)
                        com.AddParameter("AMDT", IIf(Me.TimesAmended IsNot Nothing, Me.TimesAmended, ""))
                        com.AddParameter("DeliveryAddress1", Me.DeliveryAddress1)
                        com.AddParameter("DeliveryAddress2", IIf(Me.DeliveryAddress2 IsNot Nothing, Me.DeliveryAddress2, ""))
                        com.AddParameter("DeliveryAddress3", Me.DeliveryAddress3)
                        com.AddParameter("DeliveryAddress4", IIf(Me.DeliveryAddress4 IsNot Nothing, Me.DeliveryAddress4, ""))
                        com.AddParameter("DeliveryPostCode", Me.DeliveryPostCode)
                        com.AddParameter("PhoneNumber", IIf(Me.PhoneNumber IsNot Nothing, Me.PhoneNumber, ""))
                        com.AddParameter("PhoneNumberMobile", IIf(Me.PhoneNumberMobile IsNot Nothing, Me.PhoneNumberMobile, ""))
                        com.AddParameter("IsPrinted", Me.IsPrinted)
                        com.AddParameter("NumberReprints", Me.NumberReprints)
                        com.AddParameter("RevisionNumber", Me.RevisionNumber)
                        com.AddParameter("MerchandiseValue", Me.MerchandiseValue)
                        com.AddParameter("DeliveryCharge", Me.DeliveryCharge)
                        com.AddParameter("QtyOrdered", Me.QtyOrdered)
                        com.AddParameter("QtyTaken", Me.QtyTaken)
                        com.AddParameter("QtyRefunded", Me.QtyRefunded)
                        com.AddParameter("OrderWeight", Me.OrderWeight)
                        com.AddParameter("OrderVolume", Me.OrderVolume)
                        com.AddParameter("TranDate", IIf(Me.TranDate IsNot Nothing, Me.TranDate, ""))
                        com.AddParameter("TranTill", IIf(Me.TranTill IsNot Nothing, Me.TranTill, ""))
                        com.AddParameter("TranNumber", IIf(Me.TranNumber IsNot Nothing, Me.TranNumber, ""))
                        LinesUpdated += com.ExecuteNonQuery

                        'Insert into CORHDR4
                        sb = New StringBuilder
                        sb.Append("Insert into CORHDR4 (")
                        If Me.DateDespatch.HasValue Then
                            sb.Append("NUMB,DATE1,DELD,NAME,DDAT,OEID,FDID,DeliveryStatus,SellingStoreId,SellingStoreOrderId,")
                            sb.Append("CustomerAddress1,CustomerAddress2,CustomerAddress3,CustomerAddress4,CustomerPostcode,CustomerEmail,PhoneNumberWork,IsSuspended,OMOrderNumber")
                            sb.Append(") values ( ")
                            sb.Append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )")
                        Else
                            sb.Append("NUMB,DATE1,DELD,NAME,OEID,FDID,DeliveryStatus,SellingStoreId,SellingStoreOrderId,")
                            sb.Append("CustomerAddress1,CustomerAddress2,CustomerAddress3,CustomerAddress4,CustomerPostcode,CustomerEmail,PhoneNumberWork,IsSuspended,OMOrderNumber")
                            sb.Append(") values ( ")
                            sb.Append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )")
                        End If

                        com.ClearParamters()
                        com.CommandText = sb.ToString
                        com.AddParameter("NUMB", Me.Number)
                        com.AddParameter("DATE1", Me.DateOrder)
                        com.AddParameter("DELD", IIf(Me.DateDelivery.HasValue, Me.DateDelivery.Value, ""))
                        com.AddParameter("NAME", IIf(Me.CustomerName IsNot Nothing, Me.CustomerName, ""))
                        If Me.DateDespatch.HasValue Then com.AddParameter("DDAT", Me.DateDespatch)
                        com.AddParameter("OEID", IIf(Me.OrderTakerId IsNot Nothing, Me.OrderTakerId, ""))
                        com.AddParameter("FDID", IIf(Me.ManagerId IsNot Nothing, Me.ManagerId, ""))
                        com.AddParameter("DeliveryStatus", Me.DeliveryStatus)
                        com.AddParameter("SellingStoreId", Me.SellingStoreId)
                        com.AddParameter("SellingStoreOrderId", Me.SellingStoreOrderId)
                        com.AddParameter("CustomerAddress1", Me.CustomerAddress1)
                        com.AddParameter("CustomerAddress2", IIf(Me.CustomerAddress2 IsNot Nothing, Me.CustomerAddress2, ""))
                        com.AddParameter("CustomerAddress3", Me.CustomerAddress3)
                        com.AddParameter("CustomerAddress4", IIf(Me.CustomerAddress4 IsNot Nothing, Me.CustomerAddress4, ""))
                        com.AddParameter("CustomerPostcode", Me.CustomerPostcode)
                        com.AddParameter("CustomerEmail", IIf(Me.CustomerEmail IsNot Nothing, Me.CustomerEmail, ""))
                        com.AddParameter("PhoneNumberWork", IIf(Me.PhoneNumberWork IsNot Nothing, Me.PhoneNumberWork, ""))
                        com.AddParameter("IsSuspended", IIf(Me.IsSuspended = False, "0", "1"))
                        com.AddParameter("OmOrderNumber", Me.OmOrderNumber)
                        LinesUpdated += com.ExecuteNonQuery

                        'Insert into CORHDR5
                        sb = New StringBuilder
                        sb.Append("Insert into CORHDR5 (")
                        sb.Append("NUMB,Source,SourceOrderNumber,ExtendedLeadTime,DeliveryContactName,DeliveryContactPhone")
                        sb.Append(") values ( ")
                        sb.Append("?,?,?,?,?,? )")

                        com.ClearParamters()
                        com.CommandText = sb.ToString
                        com.AddParameter("NUMB", Me.Number)
                        com.AddParameter("Source", Me.Source)
                        com.AddParameter("SourceOrderNumber", Me.SourceOrderNumber)
                        com.AddParameter("ExtendedLeadTime", IIf(Me.ExtendedLeadTime = False, "0", "1"))
                        com.AddParameter("DeliveryContactName", Me.DeliveryContactName)
                        com.AddParameter("DeliveryContactPhone", Me.DeliveryContactPhone)
                        LinesUpdated += com.ExecuteNonQuery

                    Case DataProvider.Sql
                        Dim number As String = String.Empty

                        com.StoredProcedureName = My.Resources.Procedures.SaleOrderInsert
                        If Me.VendaNumber IsNot Nothing Then com.AddParameter(My.Resources.Parameters.CustomerNumber, Me.VendaNumber)
                        If Me.CustomerName IsNot Nothing Then com.AddParameter(My.Resources.Parameters.CustomerName, Me.CustomerName)
                        com.AddParameter(My.Resources.Parameters.DateOrder, Me.DateOrder)
                        If Me.DateDelivery.HasValue Then com.AddParameter(My.Resources.Parameters.DateDelivery, Me.DateDelivery)
                        If Me.DateDespatch.HasValue Then com.AddParameter(My.Resources.Parameters.DateDespatch, Me.DateDespatch)
                        If Me.Status IsNot Nothing Then com.AddParameter(My.Resources.Parameters.Status, Me.Status)
                        com.AddParameter(My.Resources.Parameters.IsForDelivery, Me.IsForDelivery)
                        com.AddParameter(My.Resources.Parameters.DeliveryConfirmed, Me.DeliveryConfirmed)
                        If Me.TimesAmended IsNot Nothing Then com.AddParameter(My.Resources.Parameters.TimesAmended, Me.TimesAmended)
                        com.AddParameter(My.Resources.Parameters.DeliveryAddress1, Me.DeliveryAddress1)
                        If Me.DeliveryAddress2 IsNot Nothing Then com.AddParameter(My.Resources.Parameters.DeliveryAddress2, Me.DeliveryAddress2)
                        com.AddParameter(My.Resources.Parameters.DeliveryAddress3, Me.DeliveryAddress3)
                        If Me.DeliveryAddress4 IsNot Nothing Then com.AddParameter(My.Resources.Parameters.DeliveryAddress4, Me.DeliveryAddress4)
                        com.AddParameter(My.Resources.Parameters.DeliveryPostCode, Me.DeliveryPostCode)
                        If Me.PhoneNumber IsNot Nothing Then com.AddParameter(My.Resources.Parameters.PhoneNumber, Me.PhoneNumber)
                        If Me.PhoneNumberMobile IsNot Nothing Then com.AddParameter(My.Resources.Parameters.PhoneNumberMobile, Me.PhoneNumberMobile)
                        com.AddParameter(My.Resources.Parameters.IsPrinted, Me.IsPrinted)
                        com.AddParameter(My.Resources.Parameters.NumberReprints, Me.NumberReprints)
                        com.AddParameter(My.Resources.Parameters.RevisionNumber, Me.RevisionNumber)
                        com.AddParameter(My.Resources.Parameters.MerchandiseValue, Me.MerchandiseValue)
                        com.AddParameter(My.Resources.Parameters.DeliveryCharge, Me.DeliveryCharge)
                        com.AddParameter(My.Resources.Parameters.QtyOrdered, Me.QtyOrdered)
                        com.AddParameter(My.Resources.Parameters.QtyTaken, Me.QtyTaken)
                        com.AddParameter(My.Resources.Parameters.QtyRefunded, Me.QtyRefunded)
                        com.AddParameter(My.Resources.Parameters.OrderWeight, Me.OrderWeight)
                        com.AddParameter(My.Resources.Parameters.OrderVolume, Me.OrderVolume)
                        If Me.TranDate.HasValue Then com.AddParameter(My.Resources.Parameters.TranDate, Me.TranDate)
                        If Me.TranTill IsNot Nothing Then com.AddParameter(My.Resources.Parameters.TranTill, Me.TranTill)
                        If Me.TranNumber IsNot Nothing Then com.AddParameter(My.Resources.Parameters.TranNumber, Me.TranNumber)
                        If Me.RefundTranDate.HasValue Then com.AddParameter(My.Resources.Parameters.RefundTranDate, Me.RefundTranDate)
                        If Me.RefundTill IsNot Nothing Then com.AddParameter(My.Resources.Parameters.RefundTill, Me.RefundTill)
                        If Me.RefundTranNumber IsNot Nothing Then com.AddParameter(My.Resources.Parameters.RefundTranNumber, Me.RefundTranNumber)
                        If Me.OrderTakerId IsNot Nothing Then com.AddParameter(My.Resources.Parameters.OrderTakerId, Me.OrderTakerId)
                        If Me.ManagerId IsNot Nothing Then com.AddParameter(My.Resources.Parameters.ManagerId, Me.ManagerId)
                        com.AddParameter(My.Resources.Parameters.DeliveryStatus, Me.DeliveryStatus)
                        com.AddParameter(My.Resources.Parameters.RefundStatus, Me.RefundStatus)
                        com.AddParameter(My.Resources.Parameters.SellingStoreId, Me.SellingStoreId)
                        com.AddParameter(My.Resources.Parameters.SellingStoreOrderId, Me.SellingStoreOrderId)
                        com.AddParameter(My.Resources.Parameters.CustomerAddress1, Me.CustomerAddress1)
                        If Me.CustomerAddress2 IsNot Nothing Then com.AddParameter(My.Resources.Parameters.CustomerAddress2, Me.CustomerAddress2)
                        com.AddParameter(My.Resources.Parameters.CustomerAddress3, Me.CustomerAddress3)
                        If Me.CustomerAddress4 IsNot Nothing Then com.AddParameter(My.Resources.Parameters.CustomerAddress4, Me.CustomerAddress4)
                        com.AddParameter(My.Resources.Parameters.CustomerPostcode, Me.CustomerPostcode)
                        If Me.CustomerEmail IsNot Nothing Then com.AddParameter(My.Resources.Parameters.CustomerEmail, Me.CustomerEmail)
                        If Me.PhoneNumberWork IsNot Nothing Then com.AddParameter(My.Resources.Parameters.PhoneNumberWork, Me.PhoneNumberWork)
                        com.AddParameter(My.Resources.Parameters.IsSuspended, Me.IsSuspended)
                        com.AddParameter(My.Resources.Parameters.OmOrderNumber, Me.OmOrderNumber)
                        com.AddParameter(My.Resources.Parameters.Number, number, SqlDbType.Char, 6, ParameterDirection.Output)

                        LinesUpdated += com.ExecuteNonQuery()

                        number = com.GetParameterValue(My.Resources.Parameters.Number).ToString
                        Me.Number = number
                        If isSellingStore Then Me.SellingStoreOrderId = CInt(number)

                End Select
            End Using
            Return LinesUpdated

        End Function

        Public Function PersistTree(ByVal con As Connection) As Integer
            Dim LinesUpdated As Integer = 0
            LinesUpdated += Persist(con)
            For Each QodLine As QodLine In Lines
                QodLine.OrderNumber = Number
                LinesUpdated += QodLine.Persist(con)
            Next
            For Each QodRefund As QodRefund In Refunds
                QodRefund.OrderNumber = Number
                LinesUpdated += QodRefund.Persist(con)
            Next
            'Load in current list of texts in the database for comparison, some may have been removed
            Dim TempTexts As New QodTextCollection
            TempTexts.Load(Number)

            For Each QodText As QodText In Texts
                QodText.OrderNumber = Number
                LinesUpdated += QodText.Persist(con)
            Next

            'Check for lines that have been removed and update the database accordingly
            For Each QodText As QodText In TempTexts
                If Texts.Find(QodText.Number, QodTextType.Type.DeliveryInstruction) Is Nothing Then
                    'This has been deleted, so remove it from the database
                    LinesUpdated += OrderTextDelete(con, QodText)
                End If
            Next

            Return LinesUpdated
        End Function

        Private Function OrderTextDelete(ByRef con As Connection, ByRef qodText As QodText) As Integer

            Dim linesAffected As Integer = 0
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        com.CommandText = "Delete FROM CORTXT where NUMB=? and LINE=? and TYPE=?"
                        com.AddParameter("NUMB", qodText.OrderNumber)
                        com.AddParameter("LINE", qodText.Number)
                        com.AddParameter("TYPE", qodText.Type)
                        linesAffected += com.ExecuteNonQuery()

                    Case DataProvider.Sql
                        com.StoredProcedureName = My.Resources.Procedures.SaleOrderTextDelete
                        com.AddParameter(My.Resources.Parameters.OrderNumber, qodText.OrderNumber)
                        com.AddParameter(My.Resources.Parameters.Number, qodText.Number)
                        com.AddParameter(My.Resources.Parameters.Type, qodText.Type)
                        linesAffected += com.ExecuteNonQuery()

                End Select
            End Using
            Return linesAffected

        End Function

    End Class

    Public Class QodHeaderCollection
        Inherits BaseCollection(Of QodHeader)

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub LoadForState(ByVal state As State.Delivery, Optional ByVal includeSuspended As Boolean = True)
            Dim storeId As Integer = ThisStore.Id4
            Dim dt As DataTable = Qod.DataAccess.GetHeadersForStateRange(state, state, includeSuspended)
            Me.Load(dt)
            For Each QodHeader As QodHeader In Me.Items
                QodHeader.StoreId = storeId
                QodHeader.ExistsInDB = True
            Next
        End Sub

        Public Sub LoadForStateRange(ByVal minState As State.Delivery, ByVal maxState As State.Delivery, Optional ByVal includeSuspended As Boolean = True)
            Dim storeId As Integer = ThisStore.Id4
            Dim dt As DataTable = Qod.DataAccess.GetHeadersForStateRange(minState, maxState, includeSuspended)
            Me.Load(dt)
            For Each QodHeader As QodHeader In Me.Items
                QodHeader.StoreId = storeId
                QodHeader.ExistsInDB = True
            Next
        End Sub

        Public Sub LoadForDateRange(ByVal dateStart As Date, ByVal dateEnd As Date)
            Dim storeId As Integer = ThisStore.Id4
            Dim dt As DataTable = Qod.DataAccess.GetHeadersForDateRange(dateStart, dateEnd)
            Me.Load(dt)
            For Each QodHeader As QodHeader In Me.Items
                QodHeader.StoreId = storeId
                QodHeader.ExistsInDB = True
            Next
        End Sub

        Public Sub LoadRefundsForState(ByVal state As State.Refund)
            Dim storeId As Integer = ThisStore.Id4
            Dim dt As DataTable = Qod.DataAccess.GetRefundsForStateRange(state, state)
            Me.Load(dt)
            For Each QodHeader As QodHeader In Me.Items
                QodHeader.StoreId = storeId
                QodHeader.ExistsInDB = True
            Next
        End Sub

        Public Sub LoadRefundsForStateRange(ByVal minState As State.Refund, ByVal maxState As State.Refund)
            Dim storeId As Integer = ThisStore.Id4
            Dim dt As DataTable = Qod.DataAccess.GetRefundsForStateRange(minState, maxState)
            Me.Load(dt)
            For Each QodHeader As QodHeader In Me.Items
                QodHeader.StoreId = storeId
                QodHeader.ExistsInDB = True
            Next
        End Sub

        Public Sub LoadNonZeroStock()

        End Sub

        Public Function IsEmpty() As Boolean
            Return (Me.Count = 0)
        End Function

    End Class

    Friend Class Output
        Implements IDisposable

        Private _header As String = String.Empty
        Private _log As StreamWriter = Nothing

        Public Sub New(ByVal header As String)
            _header = header
            SetLogPath()
        End Sub

        Private Sub SetLogPath()
            Dim sb As New StringBuilder(AppDomain.CurrentDomain.BaseDirectory.ToString)
            sb.Append("logs\\" & _header & ".")
            sb.Append(Now.Date.ToString("ddMMyy") & ".txt")

            Dim logPath As String = sb.ToString
            _log = New StreamWriter(logPath, True)
        End Sub

        Public Sub WriteServiceCalled()
            Write("Web Service called: " & Now.ToLongTimeString)
        End Sub

        Public Sub WriteInput(ByVal input As String)
            Write("Input " & input)
        End Sub

        Public Sub WriteReturnEmptyString()
            Write("Returning empty string")
        End Sub

        Public Sub WriteCannotCreateRequest()
            Write("Cannot create request object")
        End Sub

        Public Sub WriteCannotCreateResponse()
            Write("Cannot create response object")
        End Sub

        Public Sub WriteGettingQod()
            Write("Retrieving qod order")
        End Sub

        Public Sub WriteQodNotFound()
            Write("Qod order not found")
        End Sub

        Public Sub WriteQodAlreadyExists()
            Write("Qod already exists")
        End Sub

        Public Sub WriteQodLineNotFound(ByVal lineNumber As String)
            Write("Qod line not found for line " & lineNumber)
        End Sub

        Public Sub WriteCreatingQod()
            Write("Creating qod order")
        End Sub

        Public Sub WriteCreatingIbtOut()
            Write("Creating ibt out")
        End Sub

        Public Sub WriteCreatingIbtIn()
            Write("Creating ibt in")
        End Sub

        Public Sub WriteSaveSuccessful()
            Write("Saved successfully")
        End Sub

        Public Sub WriteProblemSaving()
            Write("Problem saving to database")
        End Sub

        Public Sub Write()
            If _log IsNot Nothing Then _log.WriteLine(Environment.NewLine)
        End Sub

        Public Sub Write(ByVal message As String)
            Dim sb As New StringBuilder()
            If _header IsNot Nothing Then sb.Append(_header & ": ")
            If message IsNot Nothing Then sb.Append(message.Trim)
            If _log IsNot Nothing Then _log.WriteLine(sb.ToString)
        End Sub

#Region " IDisposable Support "
        Private disposedValue As Boolean = False        ' To detect redundant calls

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    If _log IsNot Nothing Then
                        _log.Flush()
                        _log.Close()
                        _log.Dispose()
                    End If
                End If
            End If
            Me.disposedValue = True
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

#End Region

    End Class
End Namespace