﻿Imports System.Xml.Serialization

Namespace WebService

    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     Global.System.SerializableAttribute(), _
     Global.System.ComponentModel.DesignerCategoryAttribute("code"), _
     Global.System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True), _
     Global.System.Xml.Serialization.XmlRootAttribute([Namespace]:="", IsNullable:=False)> _
    Partial Public Class OMOrderReceiveStatusUpdateRequest

        Private dateTimeStampField As OMOrderReceiveStatusUpdateRequestDateTimeStamp

        Private oMOrderNumberField As OMOrderReceiveStatusUpdateRequestOMOrderNumber

        Private fulfilmentSiteField As OMOrderReceiveStatusUpdateRequestFulfilmentSite

        Private orderStatusField As OMOrderReceiveStatusUpdateRequestOrderStatus

        '''<remarks/>
        Public Property DateTimeStamp() As OMOrderReceiveStatusUpdateRequestDateTimeStamp
            Get
                Return Me.dateTimeStampField
            End Get
            Set(ByVal value As OMOrderReceiveStatusUpdateRequestDateTimeStamp)
                Me.dateTimeStampField = value
            End Set
        End Property

        '''<remarks/>
        Public Property OMOrderNumber() As OMOrderReceiveStatusUpdateRequestOMOrderNumber
            Get
                Return Me.oMOrderNumberField
            End Get
            Set(ByVal value As OMOrderReceiveStatusUpdateRequestOMOrderNumber)
                Me.oMOrderNumberField = value
            End Set
        End Property

        '''<remarks/>
        Public Property FulfilmentSite() As OMOrderReceiveStatusUpdateRequestFulfilmentSite
            Get
                Return Me.fulfilmentSiteField
            End Get
            Set(ByVal value As OMOrderReceiveStatusUpdateRequestFulfilmentSite)
                Me.fulfilmentSiteField = value
            End Set
        End Property

        '''<remarks/>
        Public Property OrderStatus() As OMOrderReceiveStatusUpdateRequestOrderStatus
            Get
                Return Me.orderStatusField
            End Get
            Set(ByVal value As OMOrderReceiveStatusUpdateRequestOrderStatus)
                Me.orderStatusField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     Global.System.SerializableAttribute(), _
     Global.System.Diagnostics.DebuggerStepThroughAttribute(), _
     Global.System.ComponentModel.DesignerCategoryAttribute("code"), _
     Global.System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderReceiveStatusUpdateRequestDateTimeStamp

        Private validationStatusField As String

        Private valueField As Date

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     Global.System.SerializableAttribute(), _
     Global.System.Diagnostics.DebuggerStepThroughAttribute(), _
     Global.System.ComponentModel.DesignerCategoryAttribute("code"), _
     Global.System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderReceiveStatusUpdateRequestOMOrderNumber

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     Global.System.SerializableAttribute(), _
     Global.System.Diagnostics.DebuggerStepThroughAttribute(), _
     Global.System.ComponentModel.DesignerCategoryAttribute("code"), _
     Global.System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderReceiveStatusUpdateRequestFulfilmentSite

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     Global.System.SerializableAttribute(), _
     Global.System.Diagnostics.DebuggerStepThroughAttribute(), _
     Global.System.ComponentModel.DesignerCategoryAttribute("code"), _
     Global.System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderReceiveStatusUpdateRequestOrderStatus

        Private orderLinesField() As OMOrderReceiveStatusUpdateRequestOrderStatusOrderLine

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlArrayItemAttribute("OrderLine", IsNullable:=False)> _
        Public Property OrderLines() As OMOrderReceiveStatusUpdateRequestOrderStatusOrderLine()
            Get
                Return Me.orderLinesField
            End Get
            Set(ByVal value As OMOrderReceiveStatusUpdateRequestOrderStatusOrderLine())
                Me.orderLinesField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     Global.System.SerializableAttribute(), _
     Global.System.Diagnostics.DebuggerStepThroughAttribute(), _
     Global.System.ComponentModel.DesignerCategoryAttribute("code"), _
     Global.System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderReceiveStatusUpdateRequestOrderStatusOrderLine

        Private oMOrderLineNoField As OMOrderReceiveStatusUpdateRequestOrderStatusOrderLineOMOrderLineNo

        Private lineStatusField As OMOrderReceiveStatusUpdateRequestOrderStatusOrderLineLineStatus

        '''<remarks/>
        Public Property OMOrderLineNo() As OMOrderReceiveStatusUpdateRequestOrderStatusOrderLineOMOrderLineNo
            Get
                Return Me.oMOrderLineNoField
            End Get
            Set(ByVal value As OMOrderReceiveStatusUpdateRequestOrderStatusOrderLineOMOrderLineNo)
                Me.oMOrderLineNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property LineStatus() As OMOrderReceiveStatusUpdateRequestOrderStatusOrderLineLineStatus
            Get
                Return Me.lineStatusField
            End Get
            Set(ByVal value As OMOrderReceiveStatusUpdateRequestOrderStatusOrderLineLineStatus)
                Me.lineStatusField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     Global.System.SerializableAttribute(), _
     Global.System.Diagnostics.DebuggerStepThroughAttribute(), _
     Global.System.ComponentModel.DesignerCategoryAttribute("code"), _
     Global.System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderReceiveStatusUpdateRequestOrderStatusOrderLineOMOrderLineNo

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     Global.System.SerializableAttribute(), _
     Global.System.Diagnostics.DebuggerStepThroughAttribute(), _
     Global.System.ComponentModel.DesignerCategoryAttribute("code"), _
     Global.System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderReceiveStatusUpdateRequestOrderStatusOrderLineLineStatus

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Global.System.Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

End Namespace