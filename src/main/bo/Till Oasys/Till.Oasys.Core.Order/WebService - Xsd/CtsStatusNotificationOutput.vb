﻿Imports System.Xml.Serialization
Imports System.Xml

Namespace WebService

    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True), _
      Serialization.XmlRootAttribute([Namespace]:="", IsNullable:=False)> _
    Partial Public Class CTSStatusNotificationResponse

        Private dateTimeStampField As CTSStatusNotificationResponseDateTimeStamp

        Private successFlagField As CTSStatusNotificationResponseSuccessFlag

        Private oMOrderNumberField As CTSStatusNotificationResponseOMOrderNumber

        Private orderStatusField As CTSStatusNotificationResponseOrderStatus

        Private orderHeaderField As CTSStatusNotificationResponseOrderHeader

        Private fulfilmentSitesField() As CTSStatusNotificationResponseFulfilmentSite


        Public Property DateTimeStamp() As CTSStatusNotificationResponseDateTimeStamp
            Get
                Return Me.dateTimeStampField
            End Get
            Set(ByVal value As CTSStatusNotificationResponseDateTimeStamp)
                Me.dateTimeStampField = value
            End Set
        End Property


        Public Property SuccessFlag() As CTSStatusNotificationResponseSuccessFlag
            Get
                Return Me.successFlagField
            End Get
            Set(ByVal value As CTSStatusNotificationResponseSuccessFlag)
                Me.successFlagField = value
            End Set
        End Property


        Public Property OMOrderNumber() As CTSStatusNotificationResponseOMOrderNumber
            Get
                Return Me.oMOrderNumberField
            End Get
            Set(ByVal value As CTSStatusNotificationResponseOMOrderNumber)
                Me.oMOrderNumberField = value
            End Set
        End Property


        Public Property OrderStatus() As CTSStatusNotificationResponseOrderStatus
            Get
                Return Me.orderStatusField
            End Get
            Set(ByVal value As CTSStatusNotificationResponseOrderStatus)
                Me.orderStatusField = value
            End Set
        End Property


        Public Property OrderHeader() As CTSStatusNotificationResponseOrderHeader
            Get
                Return Me.orderHeaderField
            End Get
            Set(ByVal value As CTSStatusNotificationResponseOrderHeader)
                Me.orderHeaderField = value
            End Set
        End Property


        <Serialization.XmlArrayItemAttribute("FulfilmentSite", IsNullable:=False)> _
        Public Property FulfilmentSites() As CTSStatusNotificationResponseFulfilmentSite()
            Get
                Return Me.fulfilmentSitesField
            End Get
            Set(ByVal value As CTSStatusNotificationResponseFulfilmentSite())
                Me.fulfilmentSitesField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationResponseDateTimeStamp

        Private validationStatusField As String

        Private valueField As Date


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute()> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationResponseSuccessFlag

        Private validationStatusField As String

        Private valueField As Boolean


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute()> _
        Public Property Value() As Boolean
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Boolean)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationResponseOMOrderNumber

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationResponseOrderStatus

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationResponseOrderHeader

        Private sellingStoreCodeField As CTSStatusNotificationResponseOrderHeaderSellingStoreCode

        Private sellingStoreOrderNumberField As CTSStatusNotificationResponseOrderHeaderSellingStoreOrderNumber


        Public Property SellingStoreCode() As CTSStatusNotificationResponseOrderHeaderSellingStoreCode
            Get
                Return Me.sellingStoreCodeField
            End Get
            Set(ByVal value As CTSStatusNotificationResponseOrderHeaderSellingStoreCode)
                Me.sellingStoreCodeField = value
            End Set
        End Property


        Public Property SellingStoreOrderNumber() As CTSStatusNotificationResponseOrderHeaderSellingStoreOrderNumber
            Get
                Return Me.sellingStoreOrderNumberField
            End Get
            Set(ByVal value As CTSStatusNotificationResponseOrderHeaderSellingStoreOrderNumber)
                Me.sellingStoreOrderNumberField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationResponseOrderHeaderSellingStoreCode

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationResponseOrderHeaderSellingStoreOrderNumber

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationResponseFulfilmentSite

        Private fulfilmentSiteField As CTSStatusNotificationResponseFulfilmentSiteFulfilmentSite

        Private fulfilmentSiteOrderNumberField As CTSStatusNotificationResponseFulfilmentSiteFulfilmentSiteOrderNumber

        Private fulfilmentSiteIBTOutNumberField As CTSStatusNotificationResponseFulfilmentSiteFulfilmentSiteIBTOutNumber

        Private sellingStoreIBTInNumberField As CTSStatusNotificationResponseFulfilmentSiteSellingStoreIBTInNumber

        Private orderLinesField() As CTSStatusNotificationResponseFulfilmentSiteOrderLine


        Public Property FulfilmentSite() As CTSStatusNotificationResponseFulfilmentSiteFulfilmentSite
            Get
                Return Me.fulfilmentSiteField
            End Get
            Set(ByVal value As CTSStatusNotificationResponseFulfilmentSiteFulfilmentSite)
                Me.fulfilmentSiteField = value
            End Set
        End Property


        Public Property FulfilmentSiteOrderNumber() As CTSStatusNotificationResponseFulfilmentSiteFulfilmentSiteOrderNumber
            Get
                Return Me.fulfilmentSiteOrderNumberField
            End Get
            Set(ByVal value As CTSStatusNotificationResponseFulfilmentSiteFulfilmentSiteOrderNumber)
                Me.fulfilmentSiteOrderNumberField = value
            End Set
        End Property


        Public Property FulfilmentSiteIBTOutNumber() As CTSStatusNotificationResponseFulfilmentSiteFulfilmentSiteIBTOutNumber
            Get
                Return Me.fulfilmentSiteIBTOutNumberField
            End Get
            Set(ByVal value As CTSStatusNotificationResponseFulfilmentSiteFulfilmentSiteIBTOutNumber)
                Me.fulfilmentSiteIBTOutNumberField = value
            End Set
        End Property


        Public Property SellingStoreIBTInNumber() As CTSStatusNotificationResponseFulfilmentSiteSellingStoreIBTInNumber
            Get
                Return Me.sellingStoreIBTInNumberField
            End Get
            Set(ByVal value As CTSStatusNotificationResponseFulfilmentSiteSellingStoreIBTInNumber)
                Me.sellingStoreIBTInNumberField = value
            End Set
        End Property


        <Serialization.XmlArrayItemAttribute("OrderLine", IsNullable:=False)> _
        Public Property OrderLines() As CTSStatusNotificationResponseFulfilmentSiteOrderLine()
            Get
                Return Me.orderLinesField
            End Get
            Set(ByVal value As CTSStatusNotificationResponseFulfilmentSiteOrderLine())
                Me.orderLinesField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationResponseFulfilmentSiteFulfilmentSite

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationResponseFulfilmentSiteFulfilmentSiteOrderNumber

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationResponseFulfilmentSiteFulfilmentSiteIBTOutNumber

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationResponseFulfilmentSiteSellingStoreIBTInNumber

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationResponseFulfilmentSiteOrderLine

        Private sellingStoreLineNoField As CTSStatusNotificationResponseFulfilmentSiteOrderLineSellingStoreLineNo

        Private oMOrderLineNoField As CTSStatusNotificationResponseFulfilmentSiteOrderLineOMOrderLineNo

        Private productCodeField As CTSStatusNotificationResponseFulfilmentSiteOrderLineProductCode

        Private lineStatusField As CTSStatusNotificationResponseFulfilmentSiteOrderLineLineStatus


        Public Property SellingStoreLineNo() As CTSStatusNotificationResponseFulfilmentSiteOrderLineSellingStoreLineNo
            Get
                Return Me.sellingStoreLineNoField
            End Get
            Set(ByVal value As CTSStatusNotificationResponseFulfilmentSiteOrderLineSellingStoreLineNo)
                Me.sellingStoreLineNoField = value
            End Set
        End Property


        Public Property OMOrderLineNo() As CTSStatusNotificationResponseFulfilmentSiteOrderLineOMOrderLineNo
            Get
                Return Me.oMOrderLineNoField
            End Get
            Set(ByVal value As CTSStatusNotificationResponseFulfilmentSiteOrderLineOMOrderLineNo)
                Me.oMOrderLineNoField = value
            End Set
        End Property


        Public Property ProductCode() As CTSStatusNotificationResponseFulfilmentSiteOrderLineProductCode
            Get
                Return Me.productCodeField
            End Get
            Set(ByVal value As CTSStatusNotificationResponseFulfilmentSiteOrderLineProductCode)
                Me.productCodeField = value
            End Set
        End Property


        Public Property LineStatus() As CTSStatusNotificationResponseFulfilmentSiteOrderLineLineStatus
            Get
                Return Me.lineStatusField
            End Get
            Set(ByVal value As CTSStatusNotificationResponseFulfilmentSiteOrderLineLineStatus)
                Me.lineStatusField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationResponseFulfilmentSiteOrderLineSellingStoreLineNo

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationResponseFulfilmentSiteOrderLineOMOrderLineNo

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationResponseFulfilmentSiteOrderLineProductCode

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationResponseFulfilmentSiteOrderLineLineStatus

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

End Namespace