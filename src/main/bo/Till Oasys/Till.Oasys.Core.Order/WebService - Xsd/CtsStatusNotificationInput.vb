﻿Imports System.Xml.Serialization
Imports System.Xml

Namespace WebService

    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True), _
      Serialization.XmlRootAttribute([Namespace]:="", IsNullable:=False)> _
    Partial Public Class CTSStatusNotificationRequest

        Private dateTimeStampField As CTSStatusNotificationRequestDateTimeStamp

        Private oMOrderNumberField As CTSStatusNotificationRequestOMOrderNumber

        Private orderStatusField As CTSStatusNotificationRequestOrderStatus

        Private orderHeaderField As CTSStatusNotificationRequestOrderHeader

        Private fulfilmentSitesField() As CTSStatusNotificationRequestFulfilmentSite


        Public Property DateTimeStamp() As CTSStatusNotificationRequestDateTimeStamp
            Get
                Return Me.dateTimeStampField
            End Get
            Set(ByVal value As CTSStatusNotificationRequestDateTimeStamp)
                Me.dateTimeStampField = value
            End Set
        End Property


        Public Property OMOrderNumber() As CTSStatusNotificationRequestOMOrderNumber
            Get
                Return Me.oMOrderNumberField
            End Get
            Set(ByVal value As CTSStatusNotificationRequestOMOrderNumber)
                Me.oMOrderNumberField = value
            End Set
        End Property


        Public Property OrderStatus() As CTSStatusNotificationRequestOrderStatus
            Get
                Return Me.orderStatusField
            End Get
            Set(ByVal value As CTSStatusNotificationRequestOrderStatus)
                Me.orderStatusField = value
            End Set
        End Property


        Public Property OrderHeader() As CTSStatusNotificationRequestOrderHeader
            Get
                Return Me.orderHeaderField
            End Get
            Set(ByVal value As CTSStatusNotificationRequestOrderHeader)
                Me.orderHeaderField = value
            End Set
        End Property


        <Serialization.XmlArrayItemAttribute("FulfilmentSite", IsNullable:=False)> _
        Public Property FulfilmentSites() As CTSStatusNotificationRequestFulfilmentSite()
            Get
                Return Me.fulfilmentSitesField
            End Get
            Set(ByVal value As CTSStatusNotificationRequestFulfilmentSite())
                Me.fulfilmentSitesField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationRequestDateTimeStamp

        Private validationStatusField As String

        Private valueField As Date


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute()> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationRequestOMOrderNumber

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationRequestOrderStatus

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationRequestOrderHeader

        Private sellingStoreCodeField As CTSStatusNotificationRequestOrderHeaderSellingStoreCode

        Private sellingStoreOrderNumberField As CTSStatusNotificationRequestOrderHeaderSellingStoreOrderNumber


        Public Property SellingStoreCode() As CTSStatusNotificationRequestOrderHeaderSellingStoreCode
            Get
                Return Me.sellingStoreCodeField
            End Get
            Set(ByVal value As CTSStatusNotificationRequestOrderHeaderSellingStoreCode)
                Me.sellingStoreCodeField = value
            End Set
        End Property


        Public Property SellingStoreOrderNumber() As CTSStatusNotificationRequestOrderHeaderSellingStoreOrderNumber
            Get
                Return Me.sellingStoreOrderNumberField
            End Get
            Set(ByVal value As CTSStatusNotificationRequestOrderHeaderSellingStoreOrderNumber)
                Me.sellingStoreOrderNumberField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationRequestOrderHeaderSellingStoreCode

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationRequestOrderHeaderSellingStoreOrderNumber

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationRequestFulfilmentSite

        Private fulfilmentSiteField As CTSStatusNotificationRequestFulfilmentSiteFulfilmentSite

        Private fulfilmentSiteOrderNumberField As CTSStatusNotificationRequestFulfilmentSiteFulfilmentSiteOrderNumber

        Private fulfilmentSiteIBTOutNumberField As CTSStatusNotificationRequestFulfilmentSiteFulfilmentSiteIBTOutNumber

        Private orderLinesField() As CTSStatusNotificationRequestFulfilmentSiteOrderLine


        Public Property FulfilmentSite() As CTSStatusNotificationRequestFulfilmentSiteFulfilmentSite
            Get
                Return Me.fulfilmentSiteField
            End Get
            Set(ByVal value As CTSStatusNotificationRequestFulfilmentSiteFulfilmentSite)
                Me.fulfilmentSiteField = value
            End Set
        End Property


        Public Property FulfilmentSiteOrderNumber() As CTSStatusNotificationRequestFulfilmentSiteFulfilmentSiteOrderNumber
            Get
                Return Me.fulfilmentSiteOrderNumberField
            End Get
            Set(ByVal value As CTSStatusNotificationRequestFulfilmentSiteFulfilmentSiteOrderNumber)
                Me.fulfilmentSiteOrderNumberField = value
            End Set
        End Property


        Public Property FulfilmentSiteIBTOutNumber() As CTSStatusNotificationRequestFulfilmentSiteFulfilmentSiteIBTOutNumber
            Get
                Return Me.fulfilmentSiteIBTOutNumberField
            End Get
            Set(ByVal value As CTSStatusNotificationRequestFulfilmentSiteFulfilmentSiteIBTOutNumber)
                Me.fulfilmentSiteIBTOutNumberField = value
            End Set
        End Property


        <Serialization.XmlArrayItemAttribute("OrderLine", IsNullable:=False)> _
        Public Property OrderLines() As CTSStatusNotificationRequestFulfilmentSiteOrderLine()
            Get
                Return Me.orderLinesField
            End Get
            Set(ByVal value As CTSStatusNotificationRequestFulfilmentSiteOrderLine())
                Me.orderLinesField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationRequestFulfilmentSiteFulfilmentSite

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationRequestFulfilmentSiteFulfilmentSiteOrderNumber

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationRequestFulfilmentSiteFulfilmentSiteIBTOutNumber

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationRequestFulfilmentSiteOrderLine

        Private sellingStoreLineNoField As CTSStatusNotificationRequestFulfilmentSiteOrderLineSellingStoreLineNo

        Private oMOrderLineNoField As CTSStatusNotificationRequestFulfilmentSiteOrderLineOMOrderLineNo

        Private productCodeField As CTSStatusNotificationRequestFulfilmentSiteOrderLineProductCode

        Private lineStatusField As CTSStatusNotificationRequestFulfilmentSiteOrderLineLineStatus


        Public Property SellingStoreLineNo() As CTSStatusNotificationRequestFulfilmentSiteOrderLineSellingStoreLineNo
            Get
                Return Me.sellingStoreLineNoField
            End Get
            Set(ByVal value As CTSStatusNotificationRequestFulfilmentSiteOrderLineSellingStoreLineNo)
                Me.sellingStoreLineNoField = value
            End Set
        End Property


        Public Property OMOrderLineNo() As CTSStatusNotificationRequestFulfilmentSiteOrderLineOMOrderLineNo
            Get
                Return Me.oMOrderLineNoField
            End Get
            Set(ByVal value As CTSStatusNotificationRequestFulfilmentSiteOrderLineOMOrderLineNo)
                Me.oMOrderLineNoField = value
            End Set
        End Property


        Public Property ProductCode() As CTSStatusNotificationRequestFulfilmentSiteOrderLineProductCode
            Get
                Return Me.productCodeField
            End Get
            Set(ByVal value As CTSStatusNotificationRequestFulfilmentSiteOrderLineProductCode)
                Me.productCodeField = value
            End Set
        End Property


        Public Property LineStatus() As CTSStatusNotificationRequestFulfilmentSiteOrderLineLineStatus
            Get
                Return Me.lineStatusField
            End Get
            Set(ByVal value As CTSStatusNotificationRequestFulfilmentSiteOrderLineLineStatus)
                Me.lineStatusField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationRequestFulfilmentSiteOrderLineSellingStoreLineNo

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationRequestFulfilmentSiteOrderLineOMOrderLineNo

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationRequestFulfilmentSiteOrderLineProductCode

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class


    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class CTSStatusNotificationRequestFulfilmentSiteOrderLineLineStatus

        Private validationStatusField As String

        Private valueField As String


        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property


        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

End Namespace