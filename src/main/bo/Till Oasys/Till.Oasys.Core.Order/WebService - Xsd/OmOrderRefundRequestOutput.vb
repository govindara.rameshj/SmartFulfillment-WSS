﻿Imports System.Xml
Imports System.Xml.Serialization

Namespace WebService

    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True), _
      Serialization.XmlRootAttribute([Namespace]:="", IsNullable:=False)> _
    Partial Public Class OMOrderRefundResponse

        Private dateTimeStampField As OMOrderRefundResponseDateTimeStamp

        Private oMOrderNumberField As OMOrderRefundResponseOMOrderNumber

        Private orderRefundsField() As OMOrderRefundResponseOrderRefund

        '''<remarks/>
        Public Property DateTimeStamp() As OMOrderRefundResponseDateTimeStamp
            Get
                Return Me.dateTimeStampField
            End Get
            Set(ByVal value As OMOrderRefundResponseDateTimeStamp)
                Me.dateTimeStampField = value
            End Set
        End Property

        '''<remarks/>
        Public Property OMOrderNumber() As OMOrderRefundResponseOMOrderNumber
            Get
                Return Me.oMOrderNumberField
            End Get
            Set(ByVal value As OMOrderRefundResponseOMOrderNumber)
                Me.oMOrderNumberField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlArrayItemAttribute("OrderRefund", IsNullable:=False)> _
        Public Property OrderRefunds() As OMOrderRefundResponseOrderRefund()
            Get
                Return Me.orderRefundsField
            End Get
            Set(ByVal value As OMOrderRefundResponseOrderRefund())
                Me.orderRefundsField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderRefundResponseDateTimeStamp

        Private validationStatusField As String

        Private valueField As Date

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderRefundResponseOMOrderNumber

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderRefundResponseOrderRefund

        Private successFlagField As Boolean

        Private refundDateField As OMOrderRefundResponseOrderRefundRefundDate

        Private refundStoreCodeField As OMOrderRefundResponseOrderRefundRefundStoreCode

        Private refundTransactionField As OMOrderRefundResponseOrderRefundRefundTransaction

        Private refundTillField As OMOrderRefundResponseOrderRefundRefundTill

        Private fulfilmentSitesField() As OMOrderRefundResponseOrderRefundFulfilmentSite

        Private refundLinesField() As OMOrderRefundResponseOrderRefundRefundLine

        '''<remarks/>
        Public Property SuccessFlag() As Boolean
            Get
                Return Me.successFlagField
            End Get
            Set(ByVal value As Boolean)
                Me.successFlagField = value
            End Set
        End Property

        '''<remarks/>
        Public Property RefundDate() As OMOrderRefundResponseOrderRefundRefundDate
            Get
                Return Me.refundDateField
            End Get
            Set(ByVal value As OMOrderRefundResponseOrderRefundRefundDate)
                Me.refundDateField = value
            End Set
        End Property

        '''<remarks/>
        Public Property RefundStoreCode() As OMOrderRefundResponseOrderRefundRefundStoreCode
            Get
                Return Me.refundStoreCodeField
            End Get
            Set(ByVal value As OMOrderRefundResponseOrderRefundRefundStoreCode)
                Me.refundStoreCodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property RefundTransaction() As OMOrderRefundResponseOrderRefundRefundTransaction
            Get
                Return Me.refundTransactionField
            End Get
            Set(ByVal value As OMOrderRefundResponseOrderRefundRefundTransaction)
                Me.refundTransactionField = value
            End Set
        End Property

        '''<remarks/>
        Public Property RefundTill() As OMOrderRefundResponseOrderRefundRefundTill
            Get
                Return Me.refundTillField
            End Get
            Set(ByVal value As OMOrderRefundResponseOrderRefundRefundTill)
                Me.refundTillField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlArrayItemAttribute("FulfilmentSite", IsNullable:=False)> _
        Public Property FulfilmentSites() As OMOrderRefundResponseOrderRefundFulfilmentSite()
            Get
                Return Me.fulfilmentSitesField
            End Get
            Set(ByVal value As OMOrderRefundResponseOrderRefundFulfilmentSite())
                Me.fulfilmentSitesField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlArrayItemAttribute("RefundLine", IsNullable:=False)> _
        Public Property RefundLines() As OMOrderRefundResponseOrderRefundRefundLine()
            Get
                Return Me.refundLinesField
            End Get
            Set(ByVal value As OMOrderRefundResponseOrderRefundRefundLine())
                Me.refundLinesField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderRefundResponseOrderRefundRefundDate

        Private validationStatusField As String

        Private valueField As Date

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderRefundResponseOrderRefundRefundStoreCode

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderRefundResponseOrderRefundRefundTransaction

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderRefundResponseOrderRefundRefundTill

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderRefundResponseOrderRefundFulfilmentSite

        Private fulfilmentSiteCodeField As OMOrderRefundResponseOrderRefundFulfilmentSiteFulfilmentSiteCode

        Private sellingStoreIBTOutNumberField As OMOrderRefundResponseOrderRefundFulfilmentSiteSellingStoreIBTOutNumber

        '''<remarks/>
        Public Property FulfilmentSiteCode() As OMOrderRefundResponseOrderRefundFulfilmentSiteFulfilmentSiteCode
            Get
                Return Me.fulfilmentSiteCodeField
            End Get
            Set(ByVal value As OMOrderRefundResponseOrderRefundFulfilmentSiteFulfilmentSiteCode)
                Me.fulfilmentSiteCodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingStoreIBTOutNumber() As OMOrderRefundResponseOrderRefundFulfilmentSiteSellingStoreIBTOutNumber
            Get
                Return Me.sellingStoreIBTOutNumberField
            End Get
            Set(ByVal value As OMOrderRefundResponseOrderRefundFulfilmentSiteSellingStoreIBTOutNumber)
                Me.sellingStoreIBTOutNumberField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderRefundResponseOrderRefundFulfilmentSiteFulfilmentSiteCode

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderRefundResponseOrderRefundFulfilmentSiteSellingStoreIBTOutNumber

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderRefundResponseOrderRefundRefundLine

        Private sellingStoreLineNoField As OMOrderRefundResponseOrderRefundRefundLineSellingStoreLineNo

        Private oMOrderLineNoField As OMOrderRefundResponseOrderRefundRefundLineOMOrderLineNo

        Private productCodeField As OMOrderRefundResponseOrderRefundRefundLineProductCode

        Private fulfilmentSiteCodeField As OMOrderRefundResponseOrderRefundRefundLineFulfilmentSiteCode

        Private quantityReturnedField As OMOrderRefundResponseOrderRefundRefundLineQuantityReturned

        Private quantityCancelledField As OMOrderRefundResponseOrderRefundRefundLineQuantityCancelled

        Private refundLineValueField As OMOrderRefundResponseOrderRefundRefundLineRefundLineValue

        Private refundLineStatusField As OMOrderRefundResponseOrderRefundRefundLineRefundLineStatus

        '''<remarks/>
        Public Property SellingStoreLineNo() As OMOrderRefundResponseOrderRefundRefundLineSellingStoreLineNo
            Get
                Return Me.sellingStoreLineNoField
            End Get
            Set(ByVal value As OMOrderRefundResponseOrderRefundRefundLineSellingStoreLineNo)
                Me.sellingStoreLineNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property OMOrderLineNo() As OMOrderRefundResponseOrderRefundRefundLineOMOrderLineNo
            Get
                Return Me.oMOrderLineNoField
            End Get
            Set(ByVal value As OMOrderRefundResponseOrderRefundRefundLineOMOrderLineNo)
                Me.oMOrderLineNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property ProductCode() As OMOrderRefundResponseOrderRefundRefundLineProductCode
            Get
                Return Me.productCodeField
            End Get
            Set(ByVal value As OMOrderRefundResponseOrderRefundRefundLineProductCode)
                Me.productCodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property FulfilmentSiteCode() As OMOrderRefundResponseOrderRefundRefundLineFulfilmentSiteCode
            Get
                Return Me.fulfilmentSiteCodeField
            End Get
            Set(ByVal value As OMOrderRefundResponseOrderRefundRefundLineFulfilmentSiteCode)
                Me.fulfilmentSiteCodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property QuantityReturned() As OMOrderRefundResponseOrderRefundRefundLineQuantityReturned
            Get
                Return Me.quantityReturnedField
            End Get
            Set(ByVal value As OMOrderRefundResponseOrderRefundRefundLineQuantityReturned)
                Me.quantityReturnedField = value
            End Set
        End Property

        '''<remarks/>
        Public Property QuantityCancelled() As OMOrderRefundResponseOrderRefundRefundLineQuantityCancelled
            Get
                Return Me.quantityCancelledField
            End Get
            Set(ByVal value As OMOrderRefundResponseOrderRefundRefundLineQuantityCancelled)
                Me.quantityCancelledField = value
            End Set
        End Property

        '''<remarks/>
        Public Property RefundLineValue() As OMOrderRefundResponseOrderRefundRefundLineRefundLineValue
            Get
                Return Me.refundLineValueField
            End Get
            Set(ByVal value As OMOrderRefundResponseOrderRefundRefundLineRefundLineValue)
                Me.refundLineValueField = value
            End Set
        End Property

        '''<remarks/>
        Public Property RefundLineStatus() As OMOrderRefundResponseOrderRefundRefundLineRefundLineStatus
            Get
                Return Me.refundLineStatusField
            End Get
            Set(ByVal value As OMOrderRefundResponseOrderRefundRefundLineRefundLineStatus)
                Me.refundLineStatusField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderRefundResponseOrderRefundRefundLineSellingStoreLineNo

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderRefundResponseOrderRefundRefundLineOMOrderLineNo

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderRefundResponseOrderRefundRefundLineProductCode

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderRefundResponseOrderRefundRefundLineFulfilmentSiteCode

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderRefundResponseOrderRefundRefundLineQuantityReturned

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderRefundResponseOrderRefundRefundLineQuantityCancelled

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderRefundResponseOrderRefundRefundLineRefundLineValue

        Private validationStatusField As String

        Private valueField As Decimal

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
      SerializableAttribute(), _
      Diagnostics.DebuggerStepThroughAttribute(), _
      ComponentModel.DesignerCategoryAttribute("code"), _
      Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderRefundResponseOrderRefundRefundLineRefundLineStatus

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

End Namespace
