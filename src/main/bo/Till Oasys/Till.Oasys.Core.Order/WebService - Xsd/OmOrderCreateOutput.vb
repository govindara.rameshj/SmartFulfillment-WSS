﻿Imports System.Xml.Serialization
Imports System.Xml

Namespace WebService

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True), _
     Xml.Serialization.XmlRootAttribute([Namespace]:="", IsNullable:=False)> _
    Partial Public Class OMOrderCreateResponse

        Private dateTimeStampField As OMOrderCreateResponseDateTimeStamp

        Private successFlagField As OMOrderCreateResponseSuccessFlag

        Private orderHeaderField As OMOrderCreateResponseOrderHeader

        Private orderLinesField() As OMOrderCreateResponseOrderLine

        '''<remarks/>
        Public Property DateTimeStamp() As OMOrderCreateResponseDateTimeStamp
            Get
                Return Me.dateTimeStampField
            End Get
            Set(ByVal value As OMOrderCreateResponseDateTimeStamp)
                Me.dateTimeStampField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SuccessFlag() As OMOrderCreateResponseSuccessFlag
            Get
                Return Me.successFlagField
            End Get
            Set(ByVal value As OMOrderCreateResponseSuccessFlag)
                Me.successFlagField = value
            End Set
        End Property

        '''<remarks/>
        Public Property OrderHeader() As OMOrderCreateResponseOrderHeader
            Get
                Return Me.orderHeaderField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeader)
                Me.orderHeaderField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlArrayItemAttribute("OrderLine", IsNullable:=False)> _
        Public Property OrderLines() As OMOrderCreateResponseOrderLine()
            Get
                Return Me.orderLinesField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderLine())
                Me.orderLinesField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseDateTimeStamp

        Private validationStatusField As String

        Private valueField As Date

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseSuccessFlag

        Private valueField As Boolean

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Boolean
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Boolean)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeader

        Private sourceField As OMOrderCreateResponseOrderHeaderSource

        Private sourceOrderNumberField As OMOrderCreateResponseOrderHeaderSourceOrderNumber

        Private sellingStoreCodeField As OMOrderCreateResponseOrderHeaderSellingStoreCode

        Private sellingStoreOrderNumberField As OMOrderCreateResponseOrderHeaderSellingStoreOrderNumber

        Private sellingStoreTillField As OMOrderCreateResponseOrderHeaderSellingStoreTill

        Private sellingStoreTransactionField As OMOrderCreateResponseOrderHeaderSellingStoreTransaction

        Private requiredDeliveryDateField As OMOrderCreateResponseOrderHeaderRequiredDeliveryDate

        Private deliveryChargeField As OMOrderCreateResponseOrderHeaderDeliveryCharge

        Private totalOrderValueField As OMOrderCreateResponseOrderHeaderTotalOrderValue

        Private saleDateField As OMOrderCreateResponseOrderHeaderSaleDate

        Private oMOrderNumberField As OMOrderCreateResponseOrderHeaderOMOrderNumber

        Private customerAccountNoField As OMOrderCreateResponseOrderHeaderCustomerAccountNo

        Private customerNameField As OMOrderCreateResponseOrderHeaderCustomerName

        Private customerAddressLine1Field As OMOrderCreateResponseOrderHeaderCustomerAddressLine1

        Private customerAddressLine2Field As OMOrderCreateResponseOrderHeaderCustomerAddressLine2

        Private customerAddressTownField As OMOrderCreateResponseOrderHeaderCustomerAddressTown

        Private customerAddressLine4Field As OMOrderCreateResponseOrderHeaderCustomerAddressLine4

        Private customerPostcodeField As OMOrderCreateResponseOrderHeaderCustomerPostcode

        Private deliveryAddressLine1Field As OMOrderCreateResponseOrderHeaderDeliveryAddressLine1

        Private deliveryAddressLine2Field As OMOrderCreateResponseOrderHeaderDeliveryAddressLine2

        Private deliveryAddressTownField As OMOrderCreateResponseOrderHeaderDeliveryAddressTown

        Private deliveryAddressLine4Field As OMOrderCreateResponseOrderHeaderDeliveryAddressLine4

        Private deliveryPostcodeField As OMOrderCreateResponseOrderHeaderDeliveryPostcode

        Private contactPhoneHomeField As OMOrderCreateResponseOrderHeaderContactPhoneHome

        Private contactPhoneMobileField As OMOrderCreateResponseOrderHeaderContactPhoneMobile

        Private contactPhoneWorkField As OMOrderCreateResponseOrderHeaderContactPhoneWork

        Private contactEmailField As OMOrderCreateResponseOrderHeaderContactEmail

        Private deliveryInstructionsField As OMOrderCreateResponseOrderHeaderDeliveryInstructions

        Private toBeDeliveredField As OMOrderCreateResponseOrderHeaderToBeDelivered

        Private extendedLeadTimeField As OMOrderCreateResponseOrderHeaderExtendedLeadTime

        Private deliveryContactNameField As OMOrderCreateResponseOrderHeaderDeliveryContactName

        Private deliveryContactPhoneField As OMOrderCreateResponseOrderHeaderDeliveryContactPhone

        '''<remarks/>
        Public Property Source() As OMOrderCreateResponseOrderHeaderSource
            Get
                Return Me.sourceField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderSource)
                Me.sourceField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SourceOrderNumber() As OMOrderCreateResponseOrderHeaderSourceOrderNumber
            Get
                Return Me.sourceOrderNumberField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderSourceOrderNumber)
                Me.sourceOrderNumberField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingStoreCode() As OMOrderCreateResponseOrderHeaderSellingStoreCode
            Get
                Return Me.sellingStoreCodeField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderSellingStoreCode)
                Me.sellingStoreCodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingStoreOrderNumber() As OMOrderCreateResponseOrderHeaderSellingStoreOrderNumber
            Get
                Return Me.sellingStoreOrderNumberField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderSellingStoreOrderNumber)
                Me.sellingStoreOrderNumberField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingStoreTill() As OMOrderCreateResponseOrderHeaderSellingStoreTill
            Get
                Return Me.sellingStoreTillField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderSellingStoreTill)
                Me.sellingStoreTillField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingStoreTransaction() As OMOrderCreateResponseOrderHeaderSellingStoreTransaction
            Get
                Return Me.sellingStoreTransactionField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderSellingStoreTransaction)
                Me.sellingStoreTransactionField = value
            End Set
        End Property

        '''<remarks/>
        Public Property RequiredDeliveryDate() As OMOrderCreateResponseOrderHeaderRequiredDeliveryDate
            Get
                Return Me.requiredDeliveryDateField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderRequiredDeliveryDate)
                Me.requiredDeliveryDateField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryCharge() As OMOrderCreateResponseOrderHeaderDeliveryCharge
            Get
                Return Me.deliveryChargeField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderDeliveryCharge)
                Me.deliveryChargeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property TotalOrderValue() As OMOrderCreateResponseOrderHeaderTotalOrderValue
            Get
                Return Me.totalOrderValueField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderTotalOrderValue)
                Me.totalOrderValueField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SaleDate() As OMOrderCreateResponseOrderHeaderSaleDate
            Get
                Return Me.saleDateField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderSaleDate)
                Me.saleDateField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property OMOrderNumber() As OMOrderCreateResponseOrderHeaderOMOrderNumber
            Get
                Return Me.oMOrderNumberField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderOMOrderNumber)
                Me.oMOrderNumberField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property CustomerAccountNo() As OMOrderCreateResponseOrderHeaderCustomerAccountNo
            Get
                Return Me.customerAccountNoField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderCustomerAccountNo)
                Me.customerAccountNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property CustomerName() As OMOrderCreateResponseOrderHeaderCustomerName
            Get
                Return Me.customerNameField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderCustomerName)
                Me.customerNameField = value
            End Set
        End Property

        '''<remarks/>
        Public Property CustomerAddressLine1() As OMOrderCreateResponseOrderHeaderCustomerAddressLine1
            Get
                Return Me.customerAddressLine1Field
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderCustomerAddressLine1)
                Me.customerAddressLine1Field = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property CustomerAddressLine2() As OMOrderCreateResponseOrderHeaderCustomerAddressLine2
            Get
                Return Me.customerAddressLine2Field
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderCustomerAddressLine2)
                Me.customerAddressLine2Field = value
            End Set
        End Property

        '''<remarks/>
        Public Property CustomerAddressTown() As OMOrderCreateResponseOrderHeaderCustomerAddressTown
            Get
                Return Me.customerAddressTownField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderCustomerAddressTown)
                Me.customerAddressTownField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property CustomerAddressLine4() As OMOrderCreateResponseOrderHeaderCustomerAddressLine4
            Get
                Return Me.customerAddressLine4Field
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderCustomerAddressLine4)
                Me.customerAddressLine4Field = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property CustomerPostcode() As OMOrderCreateResponseOrderHeaderCustomerPostcode
            Get
                Return Me.customerPostcodeField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderCustomerPostcode)
                Me.customerPostcodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryAddressLine1() As OMOrderCreateResponseOrderHeaderDeliveryAddressLine1
            Get
                Return Me.deliveryAddressLine1Field
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderDeliveryAddressLine1)
                Me.deliveryAddressLine1Field = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property DeliveryAddressLine2() As OMOrderCreateResponseOrderHeaderDeliveryAddressLine2
            Get
                Return Me.deliveryAddressLine2Field
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderDeliveryAddressLine2)
                Me.deliveryAddressLine2Field = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryAddressTown() As OMOrderCreateResponseOrderHeaderDeliveryAddressTown
            Get
                Return Me.deliveryAddressTownField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderDeliveryAddressTown)
                Me.deliveryAddressTownField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property DeliveryAddressLine4() As OMOrderCreateResponseOrderHeaderDeliveryAddressLine4
            Get
                Return Me.deliveryAddressLine4Field
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderDeliveryAddressLine4)
                Me.deliveryAddressLine4Field = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property DeliveryPostcode() As OMOrderCreateResponseOrderHeaderDeliveryPostcode
            Get
                Return Me.deliveryPostcodeField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderDeliveryPostcode)
                Me.deliveryPostcodeField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property ContactPhoneHome() As OMOrderCreateResponseOrderHeaderContactPhoneHome
            Get
                Return Me.contactPhoneHomeField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderContactPhoneHome)
                Me.contactPhoneHomeField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property ContactPhoneMobile() As OMOrderCreateResponseOrderHeaderContactPhoneMobile
            Get
                Return Me.contactPhoneMobileField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderContactPhoneMobile)
                Me.contactPhoneMobileField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property ContactPhoneWork() As OMOrderCreateResponseOrderHeaderContactPhoneWork
            Get
                Return Me.contactPhoneWorkField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderContactPhoneWork)
                Me.contactPhoneWorkField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
        Public Property ContactEmail() As OMOrderCreateResponseOrderHeaderContactEmail
            Get
                Return Me.contactEmailField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderContactEmail)
                Me.contactEmailField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryInstructions() As OMOrderCreateResponseOrderHeaderDeliveryInstructions
            Get
                Return Me.deliveryInstructionsField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderDeliveryInstructions)
                Me.deliveryInstructionsField = value
            End Set
        End Property

        '''<remarks/>
        Public Property ToBeDelivered() As OMOrderCreateResponseOrderHeaderToBeDelivered
            Get
                Return Me.toBeDeliveredField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderToBeDelivered)
                Me.toBeDeliveredField = value
            End Set
        End Property

        '''<remarks/>
        Public Property ExtendedLeadTime() As OMOrderCreateResponseOrderHeaderExtendedLeadTime
            Get
                Return Me.extendedLeadTimeField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderExtendedLeadTime)
                Me.extendedLeadTimeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryContactName() As OMOrderCreateResponseOrderHeaderDeliveryContactName
            Get
                Return Me.deliveryContactNameField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderDeliveryContactName)
                Me.deliveryContactNameField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryContactPhone() As OMOrderCreateResponseOrderHeaderDeliveryContactPhone
            Get
                Return Me.deliveryContactPhoneField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderDeliveryContactPhone)
                Me.deliveryContactPhoneField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderSource

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderSourceOrderNumber

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderSellingStoreCode

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderSellingStoreOrderNumber

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderSellingStoreTill

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderSellingStoreTransaction

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderRequiredDeliveryDate

        Private validationStatusField As String

        Private valueField As Date

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="date")> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderDeliveryCharge

        Private validationStatusField As String

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderTotalOrderValue

        Private validationStatusField As String

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderSaleDate

        Private validationStatusField As String

        Private valueField As Date

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="date")> _
        Public Property Value() As Date
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Date)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderOMOrderNumber

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderCustomerAccountNo

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderCustomerName

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property
    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderCustomerAddressLine1

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderCustomerAddressLine2

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderCustomerAddressTown

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderCustomerAddressLine4

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderCustomerPostcode

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderDeliveryAddressLine1

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderDeliveryAddressLine2

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderDeliveryAddressTown

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderDeliveryAddressLine4

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderDeliveryPostcode

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderContactPhoneHome

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderContactPhoneMobile

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderContactPhoneWork

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderContactEmail

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderDeliveryInstructions

        Private instructionLineField() As OMOrderCreateResponseOrderHeaderDeliveryInstructionsInstructionLine

        '''<remarks/>
        <Xml.Serialization.XmlElementAttribute("InstructionLine")> _
        Public Property InstructionLine() As OMOrderCreateResponseOrderHeaderDeliveryInstructionsInstructionLine()
            Get
                Return Me.instructionLineField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderDeliveryInstructionsInstructionLine())
                Me.instructionLineField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderDeliveryInstructionsInstructionLine

        Private lineNoField As OMOrderCreateResponseOrderHeaderDeliveryInstructionsInstructionLineLineNo

        Private lineTextField As OMOrderCreateResponseOrderHeaderDeliveryInstructionsInstructionLineLineText

        '''<remarks/>
        Public Property LineNo() As OMOrderCreateResponseOrderHeaderDeliveryInstructionsInstructionLineLineNo
            Get
                Return Me.lineNoField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderDeliveryInstructionsInstructionLineLineNo)
                Me.lineNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property LineText() As OMOrderCreateResponseOrderHeaderDeliveryInstructionsInstructionLineLineText
            Get
                Return Me.lineTextField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderHeaderDeliveryInstructionsInstructionLineLineText)
                Me.lineTextField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderDeliveryInstructionsInstructionLineLineNo

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderDeliveryInstructionsInstructionLineLineText

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderToBeDelivered

        Private validationStatusField As String

        Private valueField As Boolean

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Boolean
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Boolean)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderExtendedLeadTime

        Private validationStatusField As String

        Private valueField As Boolean

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Boolean
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Boolean)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderDeliveryContactName

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderHeaderDeliveryContactPhone

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderLine

        Private sourceOrderLineNoField As OMOrderCreateResponseOrderLineSourceOrderLineNo

        Private sellingStoreLineNoField As OMOrderCreateResponseOrderLineSellingStoreLineNo

        Private oMOrderLineNoField As OMOrderCreateResponseOrderLineOMOrderLineNo

        Private productCodeField As OMOrderCreateResponseOrderLineProductCode

        Private productDescriptionField As OMOrderCreateResponseOrderLineProductDescription

        Private totalOrderQuantityField As OMOrderCreateResponseOrderLineTotalOrderQuantity

        Private quantityTakenField As OMOrderCreateResponseOrderLineQuantityTaken

        Private uOMField As OMOrderCreateResponseOrderLineUOM

        Private lineValueField As OMOrderCreateResponseOrderLineLineValue

        Private deliveryChargeItemField As OMOrderCreateResponseOrderLineDeliveryChargeItem

        Private sellingPriceField As OMOrderCreateResponseOrderLineSellingPrice

        '''<remarks/>
        Public Property SourceOrderLineNo() As OMOrderCreateResponseOrderLineSourceOrderLineNo
            Get
                Return Me.sourceOrderLineNoField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderLineSourceOrderLineNo)
                Me.sourceOrderLineNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingStoreLineNo() As OMOrderCreateResponseOrderLineSellingStoreLineNo
            Get
                Return Me.sellingStoreLineNoField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderLineSellingStoreLineNo)
                Me.sellingStoreLineNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property OMOrderLineNo() As OMOrderCreateResponseOrderLineOMOrderLineNo
            Get
                Return Me.oMOrderLineNoField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderLineOMOrderLineNo)
                Me.oMOrderLineNoField = value
            End Set
        End Property

        '''<remarks/>
        Public Property ProductCode() As OMOrderCreateResponseOrderLineProductCode
            Get
                Return Me.productCodeField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderLineProductCode)
                Me.productCodeField = value
            End Set
        End Property

        '''<remarks/>
        Public Property ProductDescription() As OMOrderCreateResponseOrderLineProductDescription
            Get
                Return Me.productDescriptionField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderLineProductDescription)
                Me.productDescriptionField = value
            End Set
        End Property

        '''<remarks/>
        Public Property TotalOrderQuantity() As OMOrderCreateResponseOrderLineTotalOrderQuantity
            Get
                Return Me.totalOrderQuantityField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderLineTotalOrderQuantity)
                Me.totalOrderQuantityField = value
            End Set
        End Property

        '''<remarks/>
        Public Property QuantityTaken() As OMOrderCreateResponseOrderLineQuantityTaken
            Get
                Return Me.quantityTakenField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderLineQuantityTaken)
                Me.quantityTakenField = value
            End Set
        End Property

        '''<remarks/>
        Public Property UOM() As OMOrderCreateResponseOrderLineUOM
            Get
                Return Me.uOMField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderLineUOM)
                Me.uOMField = value
            End Set
        End Property

        '''<remarks/>
        Public Property LineValue() As OMOrderCreateResponseOrderLineLineValue
            Get
                Return Me.lineValueField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderLineLineValue)
                Me.lineValueField = value
            End Set
        End Property

        '''<remarks/>
        Public Property DeliveryChargeItem() As OMOrderCreateResponseOrderLineDeliveryChargeItem
            Get
                Return Me.deliveryChargeItemField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderLineDeliveryChargeItem)
                Me.deliveryChargeItemField = value
            End Set
        End Property

        '''<remarks/>
        Public Property SellingPrice() As OMOrderCreateResponseOrderLineSellingPrice
            Get
                Return Me.sellingPriceField
            End Get
            Set(ByVal value As OMOrderCreateResponseOrderLineSellingPrice)
                Me.sellingPriceField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderLineSourceOrderLineNo

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderLineSellingStoreLineNo

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderLineOMOrderLineNo

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderLineProductCode

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderLineProductDescription

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderLineTotalOrderQuantity

        Private validationStatusField As String

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderLineQuantityTaken

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute(DataType:="integer")> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderLineUOM

        Private validationStatusField As String

        Private valueField As String

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As String
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As String)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderLineLineValue

        Private validationStatusField As String

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderLineDeliveryChargeItem

        Private validationStatusField As String

        Private valueField As Boolean

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Boolean
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Boolean)
                Me.valueField = value
            End Set
        End Property

    End Class

    '''<remarks/>
    <CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"), _
     SerializableAttribute(), _
     Diagnostics.DebuggerStepThroughAttribute(), _
     ComponentModel.DesignerCategoryAttribute("code"), _
     Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
    Partial Public Class OMOrderCreateResponseOrderLineSellingPrice

        Private validationStatusField As String

        Private valueField As Decimal

        '''<remarks/>
        <Xml.Serialization.XmlAttributeAttribute()> _
        Public Property ValidationStatus() As String
            Get
                Return Me.validationStatusField
            End Get
            Set(ByVal value As String)
                Me.validationStatusField = value
            End Set
        End Property

        '''<remarks/>
        <Xml.Serialization.XmlTextAttribute()> _
        Public Property Value() As Decimal
            Get
                Return Me.valueField
            End Get
            Set(ByVal value As Decimal)
                Me.valueField = value
            End Set
        End Property

    End Class

End Namespace