﻿Imports Till.Oasys.Data

Namespace Po
    <HideModuleName()> Friend Module DataAccess

        Friend Function PoGetNotConsigned() As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.PoGetNotConsigned)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

        Friend Function PoGetConsignedByDateRange(ByVal startDate As Date, ByVal endDate As Date) As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.PoGetConsigned)
                    com.AddParameter(My.Resources.Parameters.StartDate, startDate)
                    com.AddParameter(My.Resources.Parameters.EndDate, endDate)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

        Friend Function PoGetOutstanding() As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.PoGetOutstanding)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

        Friend Function PoGetLines(ByVal orderId As Integer) As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.PoGetLines)
                    com.AddParameter(My.Resources.Parameters.OrderId, orderId)
                    Return com.ExecuteDataTable
                End Using
            End Using

        End Function

        Friend Function PoConsign(ByVal orderId As Integer, ByVal consign As Consignment) As Integer

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.PoConsign)
                    com.AddParameter(My.Resources.Parameters.OrderId, orderId)
                    com.AddParameter(My.Resources.Parameters.PoNumber, consign.PoNumber)
                    com.AddParameter(My.Resources.Parameters.ReleaseNumber, consign.ReleaseNumber)
                    com.AddParameter(My.Resources.Parameters.SupplierNumber, consign.SupplierNumber)
                    com.AddParameter(My.Resources.Parameters.EmployeeId, consign.EmployeeId)
                    com.AddParameter(My.Resources.Parameters.PalletsIn, consign.PalletsIn)
                    com.AddParameter(My.Resources.Parameters.PalletsOut, consign.PalletsOut)
                    com.AddParameter(My.Resources.Parameters.DeliveryNote1, consign.DeliveryNote1)
                    com.AddParameter(My.Resources.Parameters.DeliveryNote2, consign.DeliveryNote2)
                    com.AddParameter(My.Resources.Parameters.DeliveryNote3, consign.DeliveryNote3)
                    com.AddParameter(My.Resources.Parameters.DeliveryNote4, consign.DeliveryNote4)
                    com.AddParameter(My.Resources.Parameters.DeliveryNote5, consign.DeliveryNote5)
                    com.AddParameter(My.Resources.Parameters.DeliveryNote6, consign.DeliveryNote6)
                    com.AddParameter(My.Resources.Parameters.DeliveryNote7, consign.DeliveryNote7)
                    com.AddParameter(My.Resources.Parameters.DeliveryNote8, consign.DeliveryNote8)
                    com.AddParameter(My.Resources.Parameters.DeliveryNote9, consign.DeliveryNote9)
                    com.AddParameter(My.Resources.Parameters.Number, consign.Number, SqlDbType.Int, 4, ParameterDirection.Output)
                    com.ExecuteNonQuery()

                    Return CInt(com.GetParameterValue(My.Resources.Parameters.Number))
                End Using
            End Using

        End Function

        Friend Sub PoInsertNew(ByVal po As Po.Header)

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.PoInsertNewOrder)
                    com.AddParameter(My.Resources.Parameters.SupplierNumber, po.SupplierNumber)
                    com.AddParameter(My.Resources.Parameters.SupplierBbc, po.SupplierBbc)
                    com.AddParameter(My.Resources.Parameters.SupplierTradanet, po.SupplierTradanet)
                    com.AddParameter(My.Resources.Parameters.SoqNumber, po.SoqNumber, SqlDbType.Int)
                    com.AddParameter(My.Resources.Parameters.EmployeeId, po.EmployeeId)
                    com.AddParameter(My.Resources.Parameters.Id, po.Id, SqlDbType.Int, 10, ParameterDirection.Output)
                    com.AddParameter(My.Resources.Parameters.Number, po.PoNumber, SqlDbType.Int, 4, ParameterDirection.Output)
                    com.AddParameter(My.Resources.Parameters.SourceEntry, po.SourceEntry, SqlDbType.Char, 1, ParameterDirection.Output)
                    com.AddParameter(My.Resources.Parameters.IsDeleted, po.IsDeleted, SqlDbType.Bit, 4, ParameterDirection.Output)
                    com.AddParameter(My.Resources.Parameters.DateCreated, po.DateCreated, SqlDbType.Date, 10, ParameterDirection.Output)
                    com.ExecuteNonQuery()

                    po.Id = CInt(com.GetParameterValue(My.Resources.Parameters.Id))
                    po.PoNumber = CInt(com.GetParameterValue(My.Resources.Parameters.Number))
                    po.SourceEntry = com.GetParameterValue(My.Resources.Parameters.SourceEntry).ToString
                    po.IsDeleted = CBool(com.GetParameterValue(My.Resources.Parameters.IsDeleted))
                    po.DateCreated = CDate(com.GetParameterValue(My.Resources.Parameters.DateCreated))
                End Using
            End Using

        End Sub

        Friend Sub PoUpdate(ByVal po As Po.Header)

            'initialise order header values
            po.IsDeleted = False
            po.IsCommed = False
            po.Cartons = 0
            po.Value = 0
            po.Units = 0

            Using con As New Connection
                Try
                    con.StartTransaction()

                    'insert all lines (and update stock as ordered)
                    For Each line As Po.Line In po.Lines
                        line.OrderId = po.Id
                        po.Cartons += CInt((line.OrderQty / line.SkuPackSize))
                        po.Units += line.OrderQty
                        po.Value += (line.OrderQty * line.Price)

                        Using com As New Command(con, My.Resources.Procedures.PoInsertLine)
                            com.AddParameter(My.Resources.Parameters.OrderId, line.OrderId)
                            com.AddParameter(My.Resources.Parameters.SkuNumber, line.SkuNumber)
                            com.AddParameter(My.Resources.Parameters.SkuProductCode, line.SkuProductCode)
                            com.AddParameter(My.Resources.Parameters.OrderQty, line.OrderQty)
                            com.AddParameter(My.Resources.Parameters.Price, line.Price)
                            com.AddParameter(My.Resources.Parameters.Cost, line.Cost)
                            com.ExecuteNonQuery()
                        End Using
                    Next

                    'update header and supplier as ordered (and reset all stock order levels)
                    Using com As New Command(con, My.Resources.Procedures.PoUpdate)
                        com.AddParameter(My.Resources.Parameters.Id, po.Id)
                        com.AddParameter(My.Resources.Parameters.IsDeleted, po.IsDeleted)
                        com.AddParameter(My.Resources.Parameters.DateDue, po.DateDue)
                        com.AddParameter(My.Resources.Parameters.Cartons, po.Cartons)
                        com.AddParameter(My.Resources.Parameters.Value, po.Value)
                        com.AddParameter(My.Resources.Parameters.Units, po.Units)
                        com.AddParameter(My.Resources.Parameters.IsCommed, po.IsCommed)
                        com.ExecuteNonQuery()
                    End Using

                    con.CommitTransaction()

                Catch ex As Exception
                    con.RollbackTransaction()
                    Throw
                End Try

            End Using

        End Sub

    End Module
End Namespace
