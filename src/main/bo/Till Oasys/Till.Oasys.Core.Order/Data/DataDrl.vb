﻿Imports Till.Oasys.Data
Imports System.Text

Namespace Drl
    Namespace DataAccess

        <HideModuleName()> Friend Module DataAccess

            Friend Function GetMaintainableOrders() As DataTable

                Using con As New Connection
                    Using com As New Command(con, My.Resources.Procedures.ReceiptGetMaintainableOrders)
                        Return com.ExecuteDataTable
                    End Using
                End Using

            End Function

            Friend Function GetLines(ByVal receiptNumber As String) As DataTable

                Using con As New Connection
                    Using com As New Command(con, My.Resources.Procedures.ReceiptGetLines)
                        com.AddParameter(My.Resources.Parameters.ReceiptNumber, receiptNumber)
                        Return com.ExecuteDataTable
                    End Using
                End Using

            End Function

            Friend Function Insert(ByRef rec As Header) As Integer

                Dim linesInserted As Integer = 0

                Using con As New Connection
                    Try
                        con.StartTransaction()
                        Dim number As String = String.Empty

                        Using com As New Command(con, My.Resources.Procedures.ReceiptOrderInsert)
                            com.AddParameter(My.Resources.Parameters.EmployeeId, rec.EmployeeId)
                            com.AddParameter(My.Resources.Parameters.Comments, rec.Comments)
                            com.AddParameter(My.Resources.Parameters.Value, rec.Value)
                            com.AddParameter(My.Resources.Parameters.Quantity, rec.Lines.ReceivedQty)
                            com.AddParameter(My.Resources.Parameters.PoNumber, rec.PoNumber, SqlDbType.Int)
                            com.AddParameter(My.Resources.Parameters.PoId, rec.PoId)
                            com.AddParameter(My.Resources.Parameters.PoConsignNumber, rec.PoConsignNumber, SqlDbType.Int)
                            com.AddParameter(My.Resources.Parameters.PoSupplierNumber, rec.PoSupplierNumber)
                            com.AddParameter(My.Resources.Parameters.PoSupplierBbc, rec.PoSupplierBbc)
                            com.AddParameter(My.Resources.Parameters.PoOrderDate, rec.PoOrderDate)
                            com.AddParameter(My.Resources.Parameters.PoSoqNumber, rec.PoSoqNumber, SqlDbType.Int)
                            com.AddParameter(My.Resources.Parameters.PoReleaseNumber, rec.PoReleaseNumber)
                            com.AddParameter(My.Resources.Parameters.PoDeliveryNote1, rec.PoDeliveryNote1)
                            com.AddParameter(My.Resources.Parameters.PoDeliveryNote2, rec.PoDeliveryNote2)
                            com.AddParameter(My.Resources.Parameters.PoDeliveryNote3, rec.PoDeliveryNote3)
                            com.AddParameter(My.Resources.Parameters.PoDeliveryNote4, rec.PoDeliveryNote4)
                            com.AddParameter(My.Resources.Parameters.PoDeliveryNote5, rec.PoDeliveryNote5)
                            com.AddParameter(My.Resources.Parameters.PoDeliveryNote6, rec.PoDeliveryNote6)
                            com.AddParameter(My.Resources.Parameters.PoDeliveryNote7, rec.PoDeliveryNote7)
                            com.AddParameter(My.Resources.Parameters.PoDeliveryNote8, rec.PoDeliveryNote8)
                            com.AddParameter(My.Resources.Parameters.PoDeliveryNote9, rec.PoDeliveryNote9)
                            com.AddParameter(My.Resources.Parameters.Number, number, SqlDbType.Int, 4, ParameterDirection.Output)
                            linesInserted += com.ExecuteNonQuery()

                            number = com.GetParameterValue(My.Resources.Parameters.Number).ToString
                        End Using

                        'insert all lines
                        For Each line As Line In rec.Lines
                            line.ReceiptNumber = number
                            line.Sequence = rec.Lines.NextSequenceNumber

                            Using com As New Command(con, My.Resources.Procedures.ReceiptLineInsert)
                                com.AddParameter(My.Resources.Parameters.ReceiptNumber, line.ReceiptNumber)
                                com.AddParameter(My.Resources.Parameters.Sequence, line.Sequence)
                                com.AddParameter(My.Resources.Parameters.SkuNumber, line.SkuNumber)
                                com.AddParameter(My.Resources.Parameters.OrderQty, line.OrderQty)
                                com.AddParameter(My.Resources.Parameters.OrderPrice, line.OrderPrice)
                                com.AddParameter(My.Resources.Parameters.ReceivedQty, line.ReceivedQty)
                                com.AddParameter(My.Resources.Parameters.ReceivedPrice, line.ReceivedPrice)
                                com.AddParameter(My.Resources.Parameters.OrderLineId, line.OrderLineId)
                                com.AddParameter(My.Resources.Parameters.UserId, rec.EmployeeId)
                                linesInserted += com.ExecuteNonQuery()
                            End Using
                        Next

                        con.CommitTransaction()

                        rec.Number = number

                    Catch ex As Exception
                        con.RollbackTransaction()
                        Throw
                    End Try

                End Using

                Return linesInserted

            End Function

            Friend Function Update(ByRef receipt As Header, ByVal userId As Integer) As Integer

                Dim linesUpdated As Integer = 0

                Using con As New Connection
                    con.StartTransaction()

                    Try
                        'update receipt header first
                        Using com As New Command(con, My.Resources.Procedures.ReceiptOrderUpdate)
                            com.AddParameter(My.Resources.Parameters.Number, receipt.Number)
                            com.AddParameter(My.Resources.Parameters.DeliveryNote1, receipt.PoDeliveryNote1)
                            com.AddParameter(My.Resources.Parameters.DeliveryNote2, receipt.PoDeliveryNote2)
                            com.AddParameter(My.Resources.Parameters.DeliveryNote3, receipt.PoDeliveryNote3)
                            com.AddParameter(My.Resources.Parameters.DeliveryNote4, receipt.PoDeliveryNote4)
                            com.AddParameter(My.Resources.Parameters.DeliveryNote5, receipt.PoDeliveryNote5)
                            com.AddParameter(My.Resources.Parameters.DeliveryNote6, receipt.PoDeliveryNote6)
                            com.AddParameter(My.Resources.Parameters.DeliveryNote7, receipt.PoDeliveryNote7)
                            com.AddParameter(My.Resources.Parameters.DeliveryNote8, receipt.PoDeliveryNote8)
                            com.AddParameter(My.Resources.Parameters.DeliveryNote9, receipt.PoDeliveryNote9)
                            com.AddParameter(My.Resources.Parameters.Value, receipt.Value)
                            com.AddParameter(My.Resources.Parameters.Comments, receipt.Comments)
                            linesUpdated += com.ExecuteNonQuery()
                        End Using

                        'check status of each line add or update accordingly
                        For Each line As Line In receipt.Lines
                            If line.Sequence Is Nothing Then
                                line.ReceiptNumber = receipt.Number
                                line.Sequence = receipt.Lines.NextSequenceNumber
                                Using com As New Command(con, My.Resources.Procedures.ReceiptLineInsert)
                                    com.AddParameter(My.Resources.Parameters.ReceiptNumber, line.ReceiptNumber)
                                    com.AddParameter(My.Resources.Parameters.Sequence, line.Sequence)
                                    com.AddParameter(My.Resources.Parameters.SkuNumber, line.SkuNumber)
                                    com.AddParameter(My.Resources.Parameters.OrderQty, line.OrderQty)
                                    com.AddParameter(My.Resources.Parameters.OrderPrice, line.OrderPrice)
                                    com.AddParameter(My.Resources.Parameters.ReceivedQty, line.ReceivedQty)
                                    com.AddParameter(My.Resources.Parameters.ReceivedPrice, line.ReceivedPrice)
                                    com.AddParameter(My.Resources.Parameters.OrderLineId, line.OrderLineId)
                                    com.AddParameter(My.Resources.Parameters.UserId, userId)
                                    linesUpdated += com.ExecuteNonQuery()
                                End Using

                            Else
                                If line.ReceivedQty <> line.ReceivedOriginal Then
                                    Using com As New Command(con, My.Resources.Procedures.ReceiptLineUpdate)
                                        com.AddParameter(My.Resources.Parameters.ReceiptNumber, line.ReceiptNumber)
                                        com.AddParameter(My.Resources.Parameters.Sequence, line.Sequence)
                                        com.AddParameter(My.Resources.Parameters.SkuNumber, line.SkuNumber)
                                        com.AddParameter(My.Resources.Parameters.ReceivedChange, line.ReceivedQty - line.ReceivedOriginal)
                                        com.AddParameter(My.Resources.Parameters.UserId, userId)
                                        linesUpdated += com.ExecuteNonQuery()
                                    End Using
                                End If
                            End If
                        Next

                        con.CommitTransaction()

                    Catch ex As Exception
                        con.RollbackTransaction()
                        Throw
                    End Try
                End Using

                Return linesUpdated

            End Function



            Friend Function IbtInsert(ByRef rec As Header, ByVal includeInSales As Boolean) As Integer

                Dim linesUpdated As Integer = 0
                Using con As New Connection
                    Try
                        con.StartTransaction()
                        linesUpdated += IbtInsert(con, rec, includeInSales)
                        con.CommitTransaction()

                    Catch ex As Exception
                        con.RollbackTransaction()
                        rec.Number = Nothing
                        Throw
                    End Try
                End Using

                Return linesUpdated

            End Function

            Friend Function IbtInsert(ByRef con As Connection, ByRef rec As Header, ByVal includeInSales As Boolean) As Integer

                Dim linesAffected As Integer = 0
                rec.Value = rec.Lines.IbtValue

                Using com As New Command(con)
                    Select Case con.DataProvider
                        Case DataProvider.Odbc
                            'get next receipt number from system numbers (check not over limit)
                            com.CommandText = "Select NEXT4 from SYSNUM where FKEY='01'"
                            Dim orderNumber As Integer = CInt(com.ExecuteValue)
                            If orderNumber > 999998 Then orderNumber = 1

                            'update next number and set order item number
                            com.CommandText = "Update SYSNUM set NEXT4=? where FKEY='01'"
                            com.AddParameter("Number", (orderNumber + 1).ToString("000000"))
                            linesAffected += com.ExecuteNonQuery()
                            rec.Number = orderNumber.ToString("000000")

                            'insert into drlsum
                            Dim sb As New StringBuilder
                            sb.Append("Insert into DRLSUM (NUMB,TYPE,DATE1,INIT,INFO,VALU,""1STR"",""1PRT"",""1IBT"",RTI) ")
                            sb.Append("values (?,?,?,?,?,?,?,?,?,?)")

                            com.ClearParamters()
                            com.CommandText = sb.ToString
                            com.AddParameter("Number", rec.Number)
                            com.AddParameter("Type", rec.Type)
                            com.AddParameter("DateReceipt", rec.DateReceipt)
                            com.AddParameter("Initials", rec.Initials)
                            com.AddParameter("Comments", rec.Comments)
                            com.AddParameter("Value", rec.Value)
                            com.AddParameter("IbtStoreId", rec.IbtStoreId)
                            com.AddParameter("IbtNeedsPrinting", rec.IbtNeedsPrinting)
                            com.AddParameter("IbtConsignmentNumber", IIf(rec.IbtConsignmentNumber IsNot Nothing, rec.IbtConsignmentNumber, "000000"))
                            com.AddParameter("RtiState", rec.RtiState)
                            linesAffected += com.ExecuteNonQuery()

                        Case DataProvider.Sql
                            Dim number As String = String.Empty
                            com.StoredProcedureName = My.Resources.Procedures.ReceiptIbtInsert
                            com.AddParameter(My.Resources.Parameters.Type, rec.Type)
                            com.AddParameter(My.Resources.Parameters.DateReceipt, rec.DateReceipt)
                            com.AddParameter(My.Resources.Parameters.UserId, rec.EmployeeId)
                            com.AddParameter(My.Resources.Parameters.Initials, rec.Initials)
                            com.AddParameter(My.Resources.Parameters.Comments, rec.Comments)
                            com.AddParameter(My.Resources.Parameters.Value, rec.Value)
                            com.AddParameter(My.Resources.Parameters.IbtStoreId, rec.IbtStoreId)
                            com.AddParameter(My.Resources.Parameters.IbtConsignmentNumber, rec.IbtConsignmentNumber)
                            com.AddParameter(My.Resources.Parameters.IbtNeedsPrinting, rec.IbtNeedsPrinting)
                            com.AddParameter(My.Resources.Parameters.RtiState, rec.RtiState)
                            com.AddParameter(My.Resources.Parameters.Number, number, SqlDbType.Int, 4, ParameterDirection.Output)
                            linesAffected += com.ExecuteNonQuery()

                            number = com.GetParameterValue(My.Resources.Parameters.Number).ToString
                            rec.Number = number
                    End Select
                End Using


                'insert all lines
                For Each line As Line In rec.Lines
                    line.ReceiptNumber = rec.Number

                    'insert line
                    Using com As New Command(con)
                        Select Case con.DataProvider
                            Case DataProvider.Odbc
                                'insert drldet record
                                Dim sb As New StringBuilder
                                sb.Append("Insert into DRLDET (NUMB,SEQN,SKUN,ORDQ,ORDP,RECQ,PRIC,POLN,IBTQ,RTI,RETQ) ")
                                sb.Append("values (?,?,?,?,?,?,?,?,?,?,?)")

                                com.CommandText = sb.ToString
                                com.AddParameter("NUMB", line.ReceiptNumber)
                                com.AddParameter("SEQN", line.Sequence)
                                com.AddParameter("SKUN", line.SkuNumber)
                                com.AddParameter("ORDQ", line.OrderQty)
                                com.AddParameter("ORDP", line.OrderPrice)
                                com.AddParameter("RECQ", line.ReceivedQty)
                                com.AddParameter("PRIC", line.ReceivedPrice)
                                com.AddParameter("POLN", line.OrderLineId)
                                com.AddParameter("IBTQ", line.IbtQty)
                                com.AddParameter("RTI", line.RtiState)
                                com.AddParameter("RETQ", line.ReceivedQty)
                                linesAffected += com.ExecuteNonQuery

                                'get stock values
                                com.ClearParamters()
                                com.CommandText = "Select ONHA,RETQ,MDNQ,WTFQ,PRIC from STKMAS where SKUN=?"
                                com.AddParameter("SkuNumber", line.SkuNumber)

                                Dim dt As DataTable = com.ExecuteDataTable
                                Dim logType As String = "41"
                                Dim stockStart As Integer = CInt(dt.Rows(0)("ONHA"))
                                Dim returnsStart As Integer = CInt(dt.Rows(0)("RETQ"))
                                Dim markdownsStart As Integer = CInt(dt.Rows(0)("MDNQ"))
                                Dim writeOffsStart As Integer = CInt(dt.Rows(0)("WTFQ"))
                                Dim priceStart As Decimal = CDec(dt.Rows(0)("PRIC"))
                                Dim ibtQty As Integer = line.IbtQty
                                Dim ibtValue As Decimal = line.ReceivedPrice

                                'if type=1 then is ibt in so change ibt qty sign
                                If rec.Type = "1" Then
                                    logType = "42"
                                    ibtQty *= -1
                                End If

                                'check if need to include in sales
                                If includeInSales Then
                                    com.ClearParamters()
                                    com.CommandText = "Update STKMAS set US001=US001+? where SKUN=?"
                                    com.AddParameter("IbtQty", ibtQty)
                                    com.AddParameter("SkuNumber", line.SkuNumber)
                                    linesAffected += com.ExecuteNonQuery
                                End If

                                'update stock line
                                com.ClearParamters()
                                com.CommandText = "Update STKMAS set ONHA=ONHA-?, TACT=1, MIBQ1=MIBQ1-?, MIBV1=MIBV1-? where SKUN=?"
                                com.AddParameter("IbtQty", ibtQty)
                                com.AddParameter("IbtQty", ibtQty)                  'MO'C Ref 397
                                com.AddParameter("IbtVal", (ibtQty * ibtValue))
                                com.AddParameter("SkuNumber", line.SkuNumber)
                                linesAffected += com.ExecuteNonQuery

                                'check for stock log 91
                                linesAffected += CheckStockLog91Odbc(con, line.SkuNumber, stockStart, rec.EmployeeId)

                                'insert stock log line
                                sb = New StringBuilder
                                sb.Append("Insert into STKLOG (")
                                sb.Append("SKUN,DAYN,TYPE,DATE1,TIME,KEYS,EEID,ICOM,SSTK,ESTK,SRET,ERET,SMDN,EMDN,SWTF,EWTF,SPRI,EPRI,RTI")
                                sb.Append(") values (?,?,?,?,?,?,?,0,?,?,?,?,?,?,?,?,?,?,'S' )")

                                com.ClearParamters()
                                com.CommandText = sb.ToString
                                com.AddParameter("SkuNumber", line.SkuNumber)
                                com.AddParameter("DayNumber", DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1)
                                com.AddParameter("Type", logType)
                                com.AddParameter("Date", Now.Date)
                                com.AddParameter("Time", Format(Now, "HHmmss"))
                                com.AddParameter("Keys", line.ReceiptNumber & Space(1) & line.Sequence)
                                com.AddParameter("UserId", rec.EmployeeId)
                                com.AddParameter("StockStart", stockStart)
                                com.AddParameter("StockEnd", stockStart - ibtQty)
                                com.AddParameter("ReturnsStart", returnsStart)
                                com.AddParameter("ReturnsEnd", returnsStart)
                                com.AddParameter("MarkdownStart", markdownsStart)
                                com.AddParameter("MarkdownEnd", markdownsStart)
                                com.AddParameter("WriteoffStart", writeOffsStart)
                                com.AddParameter("WriteoffEnd", writeOffsStart)
                                com.AddParameter("PriceStart", priceStart)
                                com.AddParameter("PriceEnd", priceStart)
                                linesAffected += com.ExecuteNonQuery

                            Case DataProvider.Sql
                                com.StoredProcedureName = My.Resources.Procedures.ReceiptIbtInsertLine
                                com.AddParameter(My.Resources.Parameters.Type, rec.Type)
                                com.AddParameter(My.Resources.Parameters.ReceiptNumber, line.ReceiptNumber)
                                com.AddParameter(My.Resources.Parameters.Sequence, line.Sequence)
                                com.AddParameter(My.Resources.Parameters.SkuNumber, line.SkuNumber)
                                com.AddParameter(My.Resources.Parameters.OrderQty, line.OrderQty)
                                com.AddParameter(My.Resources.Parameters.OrderPrice, line.OrderPrice)
                                com.AddParameter(My.Resources.Parameters.ReceivedQty, line.ReceivedQty)
                                com.AddParameter(My.Resources.Parameters.ReceivedPrice, line.ReceivedPrice)
                                com.AddParameter(My.Resources.Parameters.OrderLineId, line.OrderLineId)
                                com.AddParameter(My.Resources.Parameters.IbtQty, line.IbtQty)
                                com.AddParameter(My.Resources.Parameters.RtiState, line.RtiState)
                                com.AddParameter(My.Resources.Parameters.IncludeInSales, includeInSales)
                                com.AddParameter(My.Resources.Parameters.UserId, rec.EmployeeId)
                                linesAffected += com.ExecuteNonQuery()
                        End Select
                    End Using
                Next
                Return linesAffected

            End Function

            Friend Function CheckStockLog91Odbc(ByRef con As Connection, ByVal skuNumber As String, ByVal stockStart As Integer, ByVal userId As Integer) As Integer

                Dim linesUpdated As Integer = 0

                Using com As New Command(con)
                    Dim stockEnd As Integer = 0
                    Dim markdownEnd As Integer = 0
                    Dim writeoffEnd As Integer = 0
                    Dim returnsEnd As Integer = 0
                    Dim priceEnd As Decimal = 0

                    'check that last log qty = last qty for sku
                    com.CommandText = "Select top 2 ESTK,EMDN,EWTF,ERET,EPRI,TYPE from STKLOG where SKUN=? and DAYN>0 order by TKEY desc"
                    com.AddParameter("SkuNumber", skuNumber)
                    Dim dt As DataTable = com.ExecuteDataTable
                    If IsDBNull(dt) Then Return 0

                    Dim dr As DataRow = Nothing
                    Select Case dt.Rows.Count
                        Case 0
                            Return 0
                        Case 1
                            dr = dt.Rows(0)
                        Case 2
                            dr = dt.Rows(0)
                            If CStr(dt.Rows(0).Item("TYPE")) = "99" Then dr = dt.Rows(1)
                        Case Else
                            Return 0
                    End Select

                    stockEnd = CInt(dr("ESTK"))
                    markdownEnd = CInt(dr("EMDN"))
                    writeoffEnd = CInt(dr("EWTF"))
                    returnsEnd = CInt(dr("ERET"))
                    priceEnd = CDec(dr("EPRI"))

                    'insert adjustment if needed
                    If stockEnd <> stockStart Then
                        Dim sb As New StringBuilder
                        sb.Append("Insert into STKLOG (")
                        sb.Append("SKUN,DAYN,TYPE,DATE1,TIME,KEYS,EEID,ICOM,SSTK,ESTK,SRET,ERET,SMDN,EMDN,SWTF,EWTF,SPRI,EPRI,RTI")
                        sb.Append(") values (?,?,'91',?,?,'System Adjustment 91',?,0,?,?,?,?,?,?,?,?,?,?,'S' )")

                        com.ClearParamters()
                        com.CommandText = sb.ToString
                        com.AddParameter("SkuNumber", skuNumber)
                        com.AddParameter("DayNumber", DateDiff(DateInterval.Day, CDate("01/01/1900"), Now.Date) + 1)
                        com.AddParameter("Date", Now.Date)
                        com.AddParameter("Time", Format(Now, "HHmmss"))
                        com.AddParameter("UserId", userId)
                        com.AddParameter("StockEnd", stockEnd)
                        com.AddParameter("StockStart", stockStart)
                        com.AddParameter("ReturnsStart", returnsEnd)
                        com.AddParameter("ReturnsEnd", returnsEnd)
                        com.AddParameter("MarkdownStart", markdownEnd)
                        com.AddParameter("MarkdownEnd", markdownEnd)
                        com.AddParameter("WriteoffStart", writeoffEnd)
                        com.AddParameter("WriteoffEnd", writeoffEnd)
                        com.AddParameter("PriceStart", priceEnd)
                        com.AddParameter("PriceEnd", priceEnd)
                        linesUpdated += com.ExecuteNonQuery
                    End If
                End Using

                Return linesUpdated

            End Function


        End Module

    End Namespace
End Namespace