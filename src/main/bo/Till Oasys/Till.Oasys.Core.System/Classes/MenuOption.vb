﻿Imports System.Configuration
Imports Till.Oasys.Data

Namespace MenuOption

    Public Enum LoadType
        System = 0
        Process
        OasysFormOdbc
        MenuGroup
        OasysForm
        CtsCoreForm
    End Enum

    Public Class Item
        Inherits Base

#Region "   Table Fields"
        Private _id As Integer
        Private _masterId As Integer
        Private _description As String
        Private _assemblyName As String
        Private _className As String
        Private _loadType As Integer
        Private _parameters As String
        Private _imagePath As String
        Private _isMaximised As Boolean
        Private _isModal As Boolean
        Private _displayOrder As Integer

        <ColumnMapping("Id")> Public Property Id() As Integer
            Get
                Return _id
            End Get
            Private Set(ByVal value As Integer)
                _id = value
            End Set
        End Property
        <ColumnMapping("MasterId")> Public Property MasterId() As Integer
            Get
                Return _masterId
            End Get
            Private Set(ByVal value As Integer)
                _masterId = value
            End Set
        End Property
        <ColumnMapping("Description")> Public Property Description() As String
            Get
                Return _description
            End Get
            Private Set(ByVal value As String)
                _description = value
            End Set
        End Property
        <ColumnMapping("AssemblyName")> Public Property AssemblyName() As String
            Get
                Return _assemblyName
            End Get
            Private Set(ByVal value As String)
                _assemblyName = value
            End Set
        End Property
        <ColumnMapping("ClassName")> Public Property ClassName() As String
            Get
                Return _className
            End Get
            Private Set(ByVal value As String)
                _className = value
            End Set
        End Property
        <ColumnMapping("LoadType")> Public Property LoadType() As Integer
            Get
                Return _loadType
            End Get
            Private Set(ByVal value As Integer)
                _loadType = value
            End Set
        End Property
        <ColumnMapping("Parameters")> Public Property Parameters() As String
            Get
                Return _parameters
            End Get
            Private Set(ByVal value As String)
                _parameters = value
            End Set
        End Property
        <ColumnMapping("ImagePath")> Public Property ImagePath() As String
            Get
                Return _imagePath
            End Get
            Private Set(ByVal value As String)
                _imagePath = value
            End Set
        End Property
        <ColumnMapping("IsMaximised")> Public Property IsMaximised() As Boolean
            Get
                Return _isMaximised
            End Get
            Private Set(ByVal value As Boolean)
                _isMaximised = value
            End Set
        End Property
        <ColumnMapping("IsModal")> Public Property IsModal() As Boolean
            Get
                Return _isModal
            End Get
            Private Set(ByVal value As Boolean)
                _isModal = value
            End Set
        End Property
        <ColumnMapping("DisplayOrder")> Public Property DisplayOrder() As Integer
            Get
                Return _displayOrder
            End Get
            Private Set(ByVal value As Integer)
                _displayOrder = value
            End Set
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub


        Public Shared Function GetMenu(ByVal menuId As Integer) As Item
            Dim dt As DataTable = DataAccess.MenuGetForMenuId(menuId)
            Dim m As New Item
            If dt IsNot Nothing AndAlso dt.Rows.Count = 1 Then
                m.Load(dt.Rows(0))
            End If
            Return m
        End Function

        Public Shared Function GetMenus() As ItemCollection
            Dim mc As New ItemCollection
            mc.Load()
            Return mc
        End Function

        Public Shared Function GetMenus(ByVal masterId As Integer) As ItemCollection
            Dim mc As New ItemCollection
            mc.LoadForMasterId(masterId)
            Return mc
        End Function

    End Class

    Public Class ItemCollection
        Inherits BaseCollection(Of Item)

        Public Sub New()
            MyBase.New()
        End Sub

        Public Overloads Sub Load()
            Dim dt As DataTable = DataAccess.MenuGet
            Me.Load(dt)
        End Sub

        Public Overloads Sub LoadForMasterId(ByVal masterId As Integer)
            Dim dt As DataTable = DataAccess.MenuGetForMasterId(masterId)
            Me.Load(dt)
        End Sub

        Public Overloads Sub LoadForMenuId(ByVal menuId As Integer)
            Dim dt As DataTable = DataAccess.MenuGetForMenuId(menuId)
            Me.Load(dt)
        End Sub



        ''' <summary>
        ''' Sets menu collection to be union of itself and given profiles collection 
        ''' </summary>
        ''' <param name="profiles"></param>
        ''' <remarks></remarks>
        Public Sub Union(ByVal profiles As ProfileCollection)
            If Me.Items.Count = 0 Then Exit Sub

            For index As Integer = Me.Items.Count - 1 To 0 Step -1
                Dim itemFound As Boolean = False

                For Each profile As Profile In profiles
                    If profile.MenuId = Me.Items(index).Id Then
                        itemFound = True
                        Exit For
                    End If
                Next

                If Not itemFound Then
                    Me.Items.RemoveAt(index)
                End If
            Next

        End Sub

        ''' <summary>
        ''' Returns menu item for given id or nothing if not found
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Find(ByVal id As Integer) As Item
            For Each mi As Item In Me.Items
                If mi.Id = id Then Return mi
            Next
            Return Nothing
        End Function

        ''' <summary>
        ''' Returns all master menus from collection
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function FindMasterMenus(ByVal masterId As Integer) As ItemCollection
            Dim mc As New ItemCollection
            For Each mi As Item In Me.Items
                If mi.MasterId = masterId Then
                    mc.Add(mi)
                End If
            Next
            Return mc
        End Function

    End Class

    Public Class Profile
        Inherits Base

#Region "   Table Fields"
        Private _profileId As Integer
        Private _menuId As Integer
        Private _securityLevel As Integer
        Private _accessAllowed As Boolean
        Private _parameters As String

        <ColumnMapping("ProfileId")> Public Property ProfileId() As Integer
            Get
                Return _profileId
            End Get
            Private Set(ByVal value As Integer)
                _profileId = value
            End Set
        End Property
        <ColumnMapping("MenuId")> Public Property MenuId() As Integer
            Get
                Return _menuId
            End Get
            Private Set(ByVal value As Integer)
                _menuId = value
            End Set
        End Property
        <ColumnMapping("SecurityLevel")> Public Property SecurityLevel() As Integer
            Get
                Return _securityLevel
            End Get
            Private Set(ByVal value As Integer)
                _securityLevel = value
            End Set
        End Property
        <ColumnMapping("AccessAllowed")> Public Property AccessAllowed() As Boolean
            Get
                Return _accessAllowed
            End Get
            Private Set(ByVal value As Boolean)
                _accessAllowed = value
            End Set
        End Property
        <ColumnMapping("Parameters")> Public Property Parameters() As String
            Get
                Return _parameters
            End Get
            Private Set(ByVal value As String)
                _parameters = value
            End Set
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub

        Public Shared Function GetProfile(ByVal profileId As Integer, ByVal menuId As Integer) As Profile
            Dim dt As DataTable = DataAccess.MenuProfileGet(profileId, menuId)
            Dim p As New Profile
            If dt IsNot Nothing AndAlso dt.Rows.Count = 1 Then
                p.Load(dt.Rows(0))
            End If
            Return p
        End Function

        Public Shared Function GetProfiles() As ProfileCollection
            Dim mc As New ProfileCollection
            mc.Load()
            Return mc
        End Function

        Public Shared Function GetProfiles(ByVal profileId As Integer) As ProfileCollection
            Dim mc As New ProfileCollection
            mc.Load(profileId)
            Return mc
        End Function

    End Class

    Public Class ProfileCollection
        Inherits BaseCollection(Of Profile)

        Public Sub New()
            MyBase.New()
        End Sub

        Public Overloads Sub Load()
            Dim dt As DataTable = DataAccess.MenuProfileGet()
            Me.Load(dt)
        End Sub

        Public Overloads Sub Load(ByVal profileId As Integer)
            Dim dt As DataTable = DataAccess.MenuProfileGet(profileId)
            Me.Load(dt)
        End Sub

        Public Function Find(ByVal menuId As Integer, ByVal profileId As Integer) As Profile
            For Each mi As Profile In Me.Items
                If mi.MenuId = menuId AndAlso mi.ProfileId = profileId Then
                    Return mi
                End If
            Next
            Return Nothing
        End Function

    End Class

    <HideModuleName()> Friend Module DataAccess

        Friend Function MenuGet() As DataTable

            Using con As New Connection
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        Using com As New Command(con, "select ID as Id, MasterID As MasterId, AppName as Description, AssemblyName, ClassName, MenuType as LoadType, Parameters, ImagePath, LoadMaximised	as IsMaximised, DisplaySequence	as DisplayOrder from MenuConfig ")
                            Return com.ExecuteDataTable
                        End Using
                    Case DataProvider.Sql
                        Using com As New Command(con, My.Resources.Procedures.MenuGet)
                            Return com.ExecuteDataTable
                        End Using
                End Select
            End Using
            Return Nothing

        End Function

        Friend Function MenuGetForMenuId(ByVal menuId As Integer) As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.MenuGet)
                    com.AddParameter(My.Resources.Parameters.MenuId, menuId)
                    Return com.ExecuteDataTable
                End Using
            End Using
            Return Nothing

        End Function

        Friend Function MenuGetForMasterId(ByVal masterId As Integer) As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.MenuGet)
                    com.AddParameter(My.Resources.Parameters.MasterId, masterId)
                    Return com.ExecuteDataTable
                End Using
            End Using
            Return Nothing

        End Function


        Friend Function MenuProfileGet() As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.MenuProfileGet)
                    Return com.ExecuteDataTable
                End Using
            End Using
            Return Nothing

        End Function

        Friend Function MenuProfileGet(ByVal profileId As Integer) As DataTable

            Using con As New Connection
                Select Case con.DataProvider
                    Case DataProvider.Odbc
                        Using com As New Command(con, "select MenuConfigID as MenuId, ID as ProfileId, SecurityLevel, AccessAllowed from ProfilemenuAccess where ID=?")
                            com.AddParameter(My.Resources.Parameters.ProfileId, profileId)
                            Return com.ExecuteDataTable
                        End Using
                    Case DataProvider.Sql
                        Using com As New Command(con, My.Resources.Procedures.MenuProfileGet)
                            com.AddParameter(My.Resources.Parameters.ProfileId, profileId)
                            Return com.ExecuteDataTable
                        End Using
                End Select
            End Using
            Return Nothing

        End Function

        Friend Function MenuProfileGet(ByVal profileId As Integer, ByVal menuId As Integer) As DataTable

            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.MenuProfileGet)
                    com.AddParameter(My.Resources.Parameters.ProfileId, profileId)
                    com.AddParameter(My.Resources.Parameters.MenuId, menuId)
                    Return com.ExecuteDataTable
                End Using
            End Using
            Return Nothing

        End Function

    End Module

End Namespace