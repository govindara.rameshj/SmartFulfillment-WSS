﻿Public Class SystemNumber
    Inherits Base

#Region "       Table Fields"
    Private _id As Integer
    Private _description As String
    Private _nextNumber As Integer
    Private _size As Integer
    Private _prefix As String
    Private _suffix As String

    <ColumnMapping("ID")> Public Property Id() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    <ColumnMapping("Description")> Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property
    <ColumnMapping("NextNumber")> Public Property NextNumber() As Integer
        Get
            Return _nextNumber
        End Get
        Set(ByVal value As Integer)
            _nextNumber = value
        End Set
    End Property
    <ColumnMapping("Size")> Public Property Size() As Integer
        Get
            Return _size
        End Get
        Set(ByVal value As Integer)
            _size = value
        End Set
    End Property
    <ColumnMapping("Prefix")> Public Property Prefix() As String
        Get
            Return _prefix
        End Get
        Set(ByVal value As String)
            _prefix = value
        End Set
    End Property
    <ColumnMapping("Suffix")> Public Property Suffix() As String
        Get
            Return _suffix
        End Get
        Set(ByVal value As String)
            _suffix = value
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

    Private Enum NumberType
        PurchaseOrder = 1
        [Return] = 2
        InterStoreTransfer = 3
        Drl = 4
        Consignment = 5
        Soq = 6
        ProjectSalesQuote = 7
        ProjectSalesOrder = 8
        VisionSales = 9
        CustomerOrderQuote = 10
    End Enum


    Public Shared Function GetPONumber() As Integer
        Return GetNextNumber(NumberType.PurchaseOrder)
    End Function

    Public Shared Function GetSoqNumber() As Integer
        Return GetNextNumber(NumberType.Soq)
    End Function

    Public Shared Function GetConsignmentNumber() As Integer
        Return GetNextNumber(NumberType.Consignment)
    End Function

    Public Shared Function GetDrlNumber() As Integer
        Return GetNextNumber(NumberType.Drl)
    End Function

    Public Shared Function GetReturnNumber() As Integer
        Return GetNextNumber(NumberType.Return)
    End Function

    Public Shared Function GetVisionSalesNumber() As Integer
        Return GetNextNumber(NumberType.VisionSales)
    End Function

    Private Shared Function GetNextNumber(ByVal t As NumberType) As Integer
        Return DataAccess.SystemNumberGetNextNumber(t)
    End Function


End Class
