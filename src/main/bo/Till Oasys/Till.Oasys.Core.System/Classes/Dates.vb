﻿Imports Till.Oasys.Core
Imports System.ComponentModel
Imports System.IO
Imports System.Text
Imports Till.Oasys.Data

Namespace Dates

    <HideModuleName()> Public Module SharedFunctions

        Public Function GetDates() As Dates.Date
            Dim dt As DataTable = DataAccess.DateGet
            If dt.Rows.Count > 0 Then Return New Dates.Date(dt.Rows(0))
            Return Nothing
        End Function

        Public Function GetToday() As Date
            Dim dt As DataTable = DataAccess.DateGet
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                Return CDate(dt.Rows(0).Item(GetPropertyName(Function(f As [Date]) f.Today)))
            End If
            Return Nothing
        End Function

        Public Function GetTomorrow() As Date
            Dim dt As DataTable = DataAccess.DateGet
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                Return CDate(dt.Rows(0).Item(GetPropertyName(Function(f As [Date]) f.Tomorrow)))
            End If
            Return Nothing
        End Function

        Public Function GetNightlyTask() As String
            Dim dt As DataTable = DataAccess.DateGet
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                Return dt.Rows(0).Item(GetPropertyName(Function(f As [Date]) f.NightlyTask)).ToString
            End If
            Return Nothing
        End Function

        Public Function GetHolidayDates() As String
            'read the data from the date check file and create a string e.g. 24/12/02,25/12/02,26/12/02,01/01/03
            Dim holidaysFilePath As String = Parameter.GetHolidayDatesFilePath
            Dim sb As New StringBuilder

            Using sr As StreamReader = File.OpenText(holidaysFilePath)
                While Not sr.EndOfStream
                    If sb.Length > 0 Then sb.Append(",")
                    sb.Append(sr.ReadLine)
                End While
            End Using

            Return sb.ToString

        End Function

    End Module

    Public Class [Date]
        Inherits Base

#Region "   Table Fields"
        Private _id As String
        Private _daysOpen As Integer
        Private _weekEndingDay As Integer
        Private _today As Date
        Private _todayDayNumber As Integer
        Private _tomorrow As Date
        Private _tomorrowDayNumber As Integer
        Private _weekFromToday As Date
        Private _weekEndProcessed As Boolean
        Private _periodEndProcessed As Boolean
        Private _currentPeriodIsYearEnd As Boolean
        Private _currentPeriodEnd As Date
        Private _cycleWeeksTotal As Integer
        Private _cycleWeeksCurrent As Integer
        Private _nightlyTask As String
        Private _weekEnd1 As Date
        Private _weekEnd2 As Date
        Private _weekEnd3 As Date
        Private _weekEnd4 As Date
        Private _weekEnd5 As Date
        Private _weekEnd6 As Date
        Private _weekEnd7 As Date
        Private _weekEnd8 As Date
        Private _weekEnd9 As Date
        Private _weekEnd10 As Date
        Private _weekEnd11 As Date
        Private _weekEnd12 As Date
        Private _weekEnd13 As Date

        Private _setInUse As Integer
        Private _set1PeriodEnd1 As Date
        Private _set1PeriodEnd2 As Date
        Private _set1PeriodEnd3 As Date
        Private _set1PeriodEnd4 As Date
        Private _set1PeriodEnd5 As Date
        Private _set1PeriodEnd6 As Date
        Private _set1PeriodEnd7 As Date
        Private _set1PeriodEnd8 As Date
        Private _set1PeriodEnd9 As Date
        Private _set1PeriodEnd10 As Date
        Private _set1PeriodEnd11 As Date
        Private _set1PeriodEnd12 As Date
        Private _set1PeriodEnd13 As Date
        Private _set1PeriodEnd14 As Date
        Private _set2PeriodEnd1 As Date
        Private _set2PeriodEnd2 As Date
        Private _set2PeriodEnd3 As Date
        Private _set2PeriodEnd4 As Date
        Private _set2PeriodEnd5 As Date
        Private _set2PeriodEnd6 As Date
        Private _set2PeriodEnd7 As Date
        Private _set2PeriodEnd8 As Date
        Private _set2PeriodEnd9 As Date
        Private _set2PeriodEnd10 As Date
        Private _set2PeriodEnd11 As Date
        Private _set2PeriodEnd12 As Date
        Private _set2PeriodEnd13 As Date
        Private _set2PeriodEnd14 As Date

        Private _set1IsYearEnd1 As Boolean
        Private _set1IsYearEnd2 As Boolean
        Private _set1IsYearEnd3 As Boolean
        Private _set1IsYearEnd4 As Boolean
        Private _set1IsYearEnd5 As Boolean
        Private _set1IsYearEnd6 As Boolean
        Private _set1IsYearEnd7 As Boolean
        Private _set1IsYearEnd8 As Boolean
        Private _set1IsYearEnd9 As Boolean
        Private _set1IsYearEnd10 As Boolean
        Private _set1IsYearEnd11 As Boolean
        Private _set1IsYearEnd12 As Boolean
        Private _set1IsYearEnd13 As Boolean
        Private _set1IsYearEnd14 As Boolean
        Private _set2IsYearEnd1 As Boolean
        Private _set2IsYearEnd2 As Boolean
        Private _set2IsYearEnd3 As Boolean
        Private _set2IsYearEnd4 As Boolean
        Private _set2IsYearEnd5 As Boolean
        Private _set2IsYearEnd6 As Boolean
        Private _set2IsYearEnd7 As Boolean
        Private _set2IsYearEnd8 As Boolean
        Private _set2IsYearEnd9 As Boolean
        Private _set2IsYearEnd10 As Boolean
        Private _set2IsYearEnd11 As Boolean
        Private _set2IsYearEnd12 As Boolean
        Private _set2IsYearEnd13 As Boolean
        Private _set2IsYearEnd14 As Boolean

        <ColumnMapping("Id")> Public Property Id() As String
            Get
                Return _id
            End Get
            Private Set(ByVal value As String)
                _id = value
            End Set
        End Property
        <ColumnMapping("DaysOpen")> Public Property DaysOpen() As Integer
            Get
                Return _daysOpen
            End Get
            Private Set(ByVal value As Integer)
                _daysOpen = value
            End Set
        End Property
        <ColumnMapping("WeekEndingDay")> Public Property WeekEndingDay() As Integer
            Get
                Return _weekEndingDay
            End Get
            Set(ByVal value As Integer)
                _weekEndingDay = value
            End Set
        End Property
        <ColumnMapping("Today")> Public Property Today() As Date
            Get
                Return _today
            End Get
            Set(ByVal value As Date)
                _today = value
            End Set
        End Property
        <ColumnMapping("TodayDayNumber")> Public Property TodayDayNumber() As Integer
            Get
                Return _todayDayNumber
            End Get
            Private Set(ByVal value As Integer)
                _todayDayNumber = value
            End Set
        End Property
        <ColumnMapping("Tomorrow")> Public Property Tomorrow() As Date
            Get
                Return _tomorrow
            End Get
            Private Set(ByVal value As Date)
                _tomorrow = value
            End Set
        End Property
        <ColumnMapping("TomorrowDayNumber")> Public Property TomorrowDayNumber() As Integer
            Get
                Return _tomorrowDayNumber
            End Get
            Private Set(ByVal value As Integer)
                _tomorrowDayNumber = value
            End Set
        End Property

        <ColumnMapping("WeekFromToday")> Public Property WeekFromToday() As Date
            Get
                Return _weekFromToday
            End Get
            Set(ByVal value As Date)
                _weekFromToday = value
            End Set
        End Property
        <ColumnMapping("WeekEndProcessed")> Public Property WeekEndProcessed() As Boolean
            Get
                Return _weekEndProcessed
            End Get
            Set(ByVal value As Boolean)
                _weekEndProcessed = value
            End Set
        End Property
        <ColumnMapping("PeriodEndProcessed")> Public Property PeriodEndProcessed() As Boolean
            Get
                Return _periodEndProcessed
            End Get
            Set(ByVal value As Boolean)
                _periodEndProcessed = value
            End Set
        End Property
        <ColumnMapping("CurrentPeriodIsYearEnd")> Public Property CurrentPeriodIsYearEnd() As Boolean
            Get
                Return _currentPeriodIsYearEnd
            End Get
            Set(ByVal value As Boolean)
                _currentPeriodIsYearEnd = value
            End Set
        End Property
        <ColumnMapping("CurrentPeriodEnd")> Public Property CurrentPeriodEnd() As Date
            Get
                Return _currentPeriodEnd
            End Get
            Set(ByVal value As Date)
                _currentPeriodEnd = value
            End Set
        End Property
        <ColumnMapping("CycleWeeksTotal")> Public Property CycleWeeksTotal() As Integer
            Get
                Return _cycleWeeksTotal
            End Get
            Private Set(ByVal value As Integer)
                _cycleWeeksTotal = value
            End Set
        End Property
        <ColumnMapping("CycleWeeksCurrent")> Public Property CycleWeeksCurrent() As Integer
            Get
                Return _cycleWeeksCurrent
            End Get
            Private Set(ByVal value As Integer)
                _cycleWeeksCurrent = value
            End Set
        End Property
        <ColumnMapping("NightlyTask")> Public Property NightlyTask() As String
            Get
                Return _nightlyTask
            End Get
            Private Set(ByVal value As String)
                _nightlyTask = value
            End Set
        End Property

        <ColumnMapping("WeekEnd1")> Public Property WeekEnd1() As Date
            Get
                Return _weekEnd1
            End Get
            Set(ByVal value As Date)
                _weekEnd1 = value
            End Set
        End Property
        <ColumnMapping("WeekEnd2")> Public Property WeekEnd2() As Date
            Get
                Return _weekEnd2
            End Get
            Set(ByVal value As Date)
                _weekEnd2 = value
            End Set
        End Property
        <ColumnMapping("WeekEnd3")> Public Property WeekEnd3() As Date
            Get
                Return _weekEnd3
            End Get
            Set(ByVal value As Date)
                _weekEnd3 = value
            End Set
        End Property
        <ColumnMapping("WeekEnd4")> Public Property WeekEnd4() As Date
            Get
                Return _weekEnd4
            End Get
            Set(ByVal value As Date)
                _weekEnd4 = value
            End Set
        End Property
        <ColumnMapping("WeekEnd5")> Public Property WeekEnd5() As Date
            Get
                Return _weekEnd5
            End Get
            Set(ByVal value As Date)
                _weekEnd5 = value
            End Set
        End Property
        <ColumnMapping("WeekEnd6")> Public Property WeekEnd6() As Date
            Get
                Return _weekEnd6
            End Get
            Set(ByVal value As Date)
                _weekEnd6 = value
            End Set
        End Property
        <ColumnMapping("WeekEnd7")> Public Property WeekEnd7() As Date
            Get
                Return _weekEnd7
            End Get
            Set(ByVal value As Date)
                _weekEnd7 = value
            End Set
        End Property
        <ColumnMapping("WeekEnd8")> Public Property WeekEnd8() As Date
            Get
                Return _weekEnd8
            End Get
            Set(ByVal value As Date)
                _weekEnd8 = value
            End Set
        End Property
        <ColumnMapping("WeekEnd9")> Public Property WeekEnd9() As Date
            Get
                Return _weekEnd9
            End Get
            Set(ByVal value As Date)
                _weekEnd9 = value
            End Set
        End Property
        <ColumnMapping("WeekEnd10")> Public Property WeekEnd10() As Date
            Get
                Return _weekEnd10
            End Get
            Set(ByVal value As Date)
                _weekEnd10 = value
            End Set
        End Property
        <ColumnMapping("WeekEnd11")> Public Property WeekEnd11() As Date
            Get
                Return _weekEnd11
            End Get
            Set(ByVal value As Date)
                _weekEnd11 = value
            End Set
        End Property
        <ColumnMapping("WeekEnd12")> Public Property WeekEnd12() As Date
            Get
                Return _weekEnd12
            End Get
            Set(ByVal value As Date)
                _weekEnd12 = value
            End Set
        End Property
        <ColumnMapping("WeekEnd13")> Public Property WeekEnd13() As Date
            Get
                Return _weekEnd13
            End Get
            Set(ByVal value As Date)
                _weekEnd13 = value
            End Set
        End Property

        <ColumnMapping("SetInUse")> Public Property SetInUse() As Integer
            Get
                Return _setInUse
            End Get
            Set(ByVal value As Integer)
                _setInUse = value
            End Set
        End Property
        <ColumnMapping("Set1PeriodEnd1")> Public Property Set1PeriodEnd1() As Date
            Get
                Return _set1PeriodEnd1
            End Get
            Set(ByVal value As Date)
                _set1PeriodEnd1 = value
            End Set
        End Property
        <ColumnMapping("Set1PeriodEnd2")> Public Property Set1PeriodEnd2() As Date
            Get
                Return _set1PeriodEnd2
            End Get
            Set(ByVal value As Date)
                _set1PeriodEnd2 = value
            End Set
        End Property
        <ColumnMapping("Set1PeriodEnd3")> Public Property Set1PeriodEnd3() As Date
            Get
                Return _set1PeriodEnd3
            End Get
            Set(ByVal value As Date)
                _set1PeriodEnd3 = value
            End Set
        End Property
        <ColumnMapping("Set1PeriodEnd4")> Public Property Set1PeriodEnd4() As Date
            Get
                Return _set1PeriodEnd4
            End Get
            Set(ByVal value As Date)
                _set1PeriodEnd4 = value
            End Set
        End Property
        <ColumnMapping("Set1PeriodEnd5")> Public Property Set1PeriodEnd5() As Date
            Get
                Return _set1PeriodEnd5
            End Get
            Set(ByVal value As Date)
                _set1PeriodEnd5 = value
            End Set
        End Property
        <ColumnMapping("Set1PeriodEnd6")> Public Property Set1PeriodEnd6() As Date
            Get
                Return _set1PeriodEnd6
            End Get
            Set(ByVal value As Date)
                _set1PeriodEnd6 = value
            End Set
        End Property
        <ColumnMapping("Set1PeriodEnd7")> Public Property Set1PeriodEnd7() As Date
            Get
                Return _set1PeriodEnd7
            End Get
            Set(ByVal value As Date)
                _set1PeriodEnd7 = value
            End Set
        End Property
        <ColumnMapping("Set1PeriodEnd8")> Public Property Set1PeriodEnd8() As Date
            Get
                Return _set1PeriodEnd8
            End Get
            Set(ByVal value As Date)
                _set1PeriodEnd8 = value
            End Set
        End Property
        <ColumnMapping("Set1PeriodEnd9")> Public Property Set1PeriodEnd9() As Date
            Get
                Return _set1PeriodEnd9
            End Get
            Set(ByVal value As Date)
                _set1PeriodEnd9 = value
            End Set
        End Property
        <ColumnMapping("Set1PeriodEnd10")> Public Property Set1PeriodEnd10() As Date
            Get
                Return _set1PeriodEnd10
            End Get
            Set(ByVal value As Date)
                _set1PeriodEnd10 = value
            End Set
        End Property
        <ColumnMapping("Set1PeriodEnd11")> Public Property Set1PeriodEnd11() As Date
            Get
                Return _set1PeriodEnd11
            End Get
            Set(ByVal value As Date)
                _set1PeriodEnd11 = value
            End Set
        End Property
        <ColumnMapping("Set1PeriodEnd12")> Public Property Set1PeriodEnd12() As Date
            Get
                Return _set1PeriodEnd12
            End Get
            Set(ByVal value As Date)
                _set1PeriodEnd12 = value
            End Set
        End Property
        <ColumnMapping("Set1PeriodEnd13")> Public Property Set1PeriodEnd13() As Date
            Get
                Return _set1PeriodEnd13
            End Get
            Set(ByVal value As Date)
                _set1PeriodEnd13 = value
            End Set
        End Property
        <ColumnMapping("Set1PeriodEnd14")> Public Property Set1PeriodEnd14() As Date
            Get
                Return _set1PeriodEnd14
            End Get
            Set(ByVal value As Date)
                _set1PeriodEnd14 = value
            End Set
        End Property
        <ColumnMapping("Set2PeriodEnd1")> Public Property Set2PeriodEnd1() As Date
            Get
                Return _set2PeriodEnd1
            End Get
            Set(ByVal value As Date)
                _set2PeriodEnd1 = value
            End Set
        End Property
        <ColumnMapping("Set2PeriodEnd2")> Public Property Set2PeriodEnd2() As Date
            Get
                Return _set2PeriodEnd2
            End Get
            Set(ByVal value As Date)
                _set2PeriodEnd2 = value
            End Set
        End Property
        <ColumnMapping("Set2PeriodEnd3")> Public Property Set2PeriodEnd3() As Date
            Get
                Return _set2PeriodEnd3
            End Get
            Set(ByVal value As Date)
                _set2PeriodEnd3 = value
            End Set
        End Property
        <ColumnMapping("Set2PeriodEnd4")> Public Property Set2PeriodEnd4() As Date
            Get
                Return _set2PeriodEnd4
            End Get
            Set(ByVal value As Date)
                _set2PeriodEnd4 = value
            End Set
        End Property
        <ColumnMapping("Set2PeriodEnd5")> Public Property Set2PeriodEnd5() As Date
            Get
                Return _set2PeriodEnd5
            End Get
            Set(ByVal value As Date)
                _set2PeriodEnd5 = value
            End Set
        End Property
        <ColumnMapping("Set2PeriodEnd6")> Public Property Set2PeriodEnd6() As Date
            Get
                Return _set2PeriodEnd6
            End Get
            Set(ByVal value As Date)
                _set2PeriodEnd6 = value
            End Set
        End Property
        <ColumnMapping("Set2PeriodEnd7")> Public Property Set2PeriodEnd7() As Date
            Get
                Return _set2PeriodEnd7
            End Get
            Set(ByVal value As Date)
                _set2PeriodEnd7 = value
            End Set
        End Property
        <ColumnMapping("Set2PeriodEnd8")> Public Property Set2PeriodEnd8() As Date
            Get
                Return _set2PeriodEnd8
            End Get
            Set(ByVal value As Date)
                _set2PeriodEnd8 = value
            End Set
        End Property
        <ColumnMapping("Set2PeriodEnd9")> Public Property Set2PeriodEnd9() As Date
            Get
                Return _set2PeriodEnd9
            End Get
            Set(ByVal value As Date)
                _set2PeriodEnd9 = value
            End Set
        End Property
        <ColumnMapping("Set2PeriodEnd10")> Public Property Set2PeriodEnd10() As Date
            Get
                Return _set2PeriodEnd10
            End Get
            Set(ByVal value As Date)
                _set2PeriodEnd10 = value
            End Set
        End Property
        <ColumnMapping("Set2PeriodEnd11")> Public Property Set2PeriodEnd11() As Date
            Get
                Return _set2PeriodEnd11
            End Get
            Set(ByVal value As Date)
                _set2PeriodEnd11 = value
            End Set
        End Property
        <ColumnMapping("Set2PeriodEnd12")> Public Property Set2PeriodEnd12() As Date
            Get
                Return _set2PeriodEnd12
            End Get
            Set(ByVal value As Date)
                _set2PeriodEnd12 = value
            End Set
        End Property
        <ColumnMapping("Set2PeriodEnd13")> Public Property Set2PeriodEnd13() As Date
            Get
                Return _set2PeriodEnd13
            End Get
            Set(ByVal value As Date)
                _set2PeriodEnd13 = value
            End Set
        End Property
        <ColumnMapping("Set2PeriodEnd14")> Public Property Set2PeriodEnd14() As Date
            Get
                Return _set2PeriodEnd14
            End Get
            Set(ByVal value As Date)
                _set2PeriodEnd14 = value
            End Set
        End Property

        <ColumnMapping("Set1IsYearEnd1")> Public Property Set1IsYearEnd1() As Boolean
            Get
                Return _set1IsYearEnd1
            End Get
            Set(ByVal value As Boolean)
                _set1IsYearEnd1 = value
            End Set
        End Property
        <ColumnMapping("Set1IsYearEnd2")> Public Property Set1IsYearEnd2() As Boolean
            Get
                Return _set1IsYearEnd2
            End Get
            Set(ByVal value As Boolean)
                _set1IsYearEnd2 = value
            End Set
        End Property
        <ColumnMapping("Set1IsYearEnd3")> Public Property Set1IsYearEnd3() As Boolean
            Get
                Return _set1IsYearEnd3
            End Get
            Set(ByVal value As Boolean)
                _set1IsYearEnd3 = value
            End Set
        End Property
        <ColumnMapping("Set1IsYearEnd4")> Public Property Set1IsYearEnd4() As Boolean
            Get
                Return _set1IsYearEnd4
            End Get
            Set(ByVal value As Boolean)
                _set1IsYearEnd4 = value
            End Set
        End Property
        <ColumnMapping("Set1IsYearEnd5")> Public Property Set1IsYearEnd5() As Boolean
            Get
                Return _set1IsYearEnd5
            End Get
            Set(ByVal value As Boolean)
                _set1IsYearEnd5 = value
            End Set
        End Property
        <ColumnMapping("Set1IsYearEnd6")> Public Property Set1IsYearEnd6() As Boolean
            Get
                Return _set1IsYearEnd6
            End Get
            Set(ByVal value As Boolean)
                _set1IsYearEnd6 = value
            End Set
        End Property
        <ColumnMapping("Set1IsYearEnd7")> Public Property Set1IsYearEnd7() As Boolean
            Get
                Return _set1IsYearEnd7
            End Get
            Set(ByVal value As Boolean)
                _set1IsYearEnd7 = value
            End Set
        End Property
        <ColumnMapping("Set1IsYearEnd8")> Public Property Set1IsYearEnd8() As Boolean
            Get
                Return _set1IsYearEnd8
            End Get
            Set(ByVal value As Boolean)
                _set1IsYearEnd8 = value
            End Set
        End Property
        <ColumnMapping("Set1IsYearEnd9")> Public Property Set1IsYearEnd9() As Boolean
            Get
                Return _set1IsYearEnd9
            End Get
            Set(ByVal value As Boolean)
                _set1IsYearEnd9 = value
            End Set
        End Property
        <ColumnMapping("Set1IsYearEnd10")> Public Property Set1IsYearEnd10() As Boolean
            Get
                Return _set1IsYearEnd10
            End Get
            Set(ByVal value As Boolean)
                _set1IsYearEnd10 = value
            End Set
        End Property
        <ColumnMapping("Set1IsYearEnd11")> Public Property Set1IsYearEnd11() As Boolean
            Get
                Return _set1IsYearEnd11
            End Get
            Set(ByVal value As Boolean)
                _set1IsYearEnd11 = value
            End Set
        End Property
        <ColumnMapping("Set1IsYearEnd12")> Public Property Set1IsYearEnd12() As Boolean
            Get
                Return _set1IsYearEnd12
            End Get
            Set(ByVal value As Boolean)
                _set1IsYearEnd12 = value
            End Set
        End Property
        <ColumnMapping("Set1IsYearEnd13")> Public Property Set1IsYearEnd13() As Boolean
            Get
                Return _set1IsYearEnd13
            End Get
            Set(ByVal value As Boolean)
                _set1IsYearEnd13 = value
            End Set
        End Property
        <ColumnMapping("Set1IsYearEnd14")> Public Property Set1IsYearEnd14() As Boolean
            Get
                Return _set1IsYearEnd14
            End Get
            Set(ByVal value As Boolean)
                _set1IsYearEnd14 = value
            End Set
        End Property
        <ColumnMapping("Set2IsYearEnd1")> Public Property Set2IsYearEnd1() As Boolean
            Get
                Return _set2IsYearEnd1
            End Get
            Set(ByVal value As Boolean)
                _set2IsYearEnd1 = value
            End Set
        End Property
        <ColumnMapping("Set2IsYearEnd2")> Public Property Set2IsYearEnd2() As Boolean
            Get
                Return _set2IsYearEnd2
            End Get
            Set(ByVal value As Boolean)
                _set2IsYearEnd2 = value
            End Set
        End Property
        <ColumnMapping("Set2IsYearEnd3")> Public Property Set2IsYearEnd3() As Boolean
            Get
                Return _set2IsYearEnd3
            End Get
            Set(ByVal value As Boolean)
                _set2IsYearEnd3 = value
            End Set
        End Property
        <ColumnMapping("Set2IsYearEnd4")> Public Property Set2IsYearEnd4() As Boolean
            Get
                Return _set2IsYearEnd4
            End Get
            Set(ByVal value As Boolean)
                _set2IsYearEnd4 = value
            End Set
        End Property
        <ColumnMapping("Set2IsYearEnd5")> Public Property Set2IsYearEnd5() As Boolean
            Get
                Return _set2IsYearEnd5
            End Get
            Set(ByVal value As Boolean)
                _set2IsYearEnd5 = value
            End Set
        End Property
        <ColumnMapping("Set2IsYearEnd6")> Public Property Set2IsYearEnd6() As Boolean
            Get
                Return _set2IsYearEnd6
            End Get
            Set(ByVal value As Boolean)
                _set2IsYearEnd6 = value
            End Set
        End Property
        <ColumnMapping("Set2IsYearEnd7")> Public Property Set2IsYearEnd7() As Boolean
            Get
                Return _set2IsYearEnd7
            End Get
            Set(ByVal value As Boolean)
                _set2IsYearEnd7 = value
            End Set
        End Property
        <ColumnMapping("Set2IsYearEnd8")> Public Property Set2IsYearEnd8() As Boolean
            Get
                Return _set2IsYearEnd8
            End Get
            Set(ByVal value As Boolean)
                _set2IsYearEnd8 = value
            End Set
        End Property
        <ColumnMapping("Set2IsYearEnd9")> Public Property Set2IsYearEnd9() As Boolean
            Get
                Return _set2IsYearEnd9
            End Get
            Set(ByVal value As Boolean)
                _set2IsYearEnd9 = value
            End Set
        End Property
        <ColumnMapping("Set2IsYearEnd10")> Public Property Set2IsYearEnd10() As Boolean
            Get
                Return _set2IsYearEnd10
            End Get
            Set(ByVal value As Boolean)
                _set2IsYearEnd10 = value
            End Set
        End Property
        <ColumnMapping("Set2IsYearEnd11")> Public Property Set2IsYearEnd11() As Boolean
            Get
                Return _set2IsYearEnd11
            End Get
            Set(ByVal value As Boolean)
                _set2IsYearEnd11 = value
            End Set
        End Property
        <ColumnMapping("Set2IsYearEnd12")> Public Property Set2IsYearEnd12() As Boolean
            Get
                Return _set2IsYearEnd12
            End Get
            Set(ByVal value As Boolean)
                _set2IsYearEnd12 = value
            End Set
        End Property
        <ColumnMapping("Set2IsYearEnd13")> Public Property Set2IsYearEnd13() As Boolean
            Get
                Return _set2IsYearEnd13
            End Get
            Set(ByVal value As Boolean)
                _set2IsYearEnd13 = value
            End Set
        End Property
        <ColumnMapping("Set2IsYearEnd14")> Public Property Set2IsYearEnd14() As Boolean
            Get
                Return _set2IsYearEnd14
            End Get
            Set(ByVal value As Boolean)
                _set2IsYearEnd14 = value
            End Set
        End Property

        Public Property Set1PeriodEnd(ByVal index As Integer) As Date
            Get
                Select Case index
                    Case 1 : Return Me.Set1PeriodEnd1
                    Case 2 : Return Me.Set1PeriodEnd2
                    Case 3 : Return Me.Set1PeriodEnd3
                    Case 4 : Return Me.Set1PeriodEnd4
                    Case 5 : Return Me.Set1PeriodEnd5
                    Case 6 : Return Me.Set1PeriodEnd6
                    Case 7 : Return Me.Set1PeriodEnd7
                    Case 8 : Return Me.Set1PeriodEnd8
                    Case 9 : Return Me.Set1PeriodEnd9
                    Case 10 : Return Me.Set1PeriodEnd10
                    Case 11 : Return Me.Set1PeriodEnd11
                    Case 12 : Return Me.Set1PeriodEnd12
                    Case 13 : Return Me.Set1PeriodEnd13
                    Case 14 : Return Me.Set1PeriodEnd14
                    Case Else : Return Nothing
                End Select
            End Get
            Set(ByVal value As Date)
                Select Case index
                    Case 1 : Me.Set1PeriodEnd1 = value
                    Case 2 : Me.Set1PeriodEnd2 = value
                    Case 3 : Me.Set1PeriodEnd3 = value
                    Case 4 : Me.Set1PeriodEnd4 = value
                    Case 5 : Me.Set1PeriodEnd5 = value
                    Case 6 : Me.Set1PeriodEnd6 = value
                    Case 7 : Me.Set1PeriodEnd7 = value
                    Case 8 : Me.Set1PeriodEnd8 = value
                    Case 9 : Me.Set1PeriodEnd9 = value
                    Case 10 : Me.Set1PeriodEnd10 = value
                    Case 11 : Me.Set1PeriodEnd11 = value
                    Case 12 : Me.Set1PeriodEnd12 = value
                    Case 13 : Me.Set1PeriodEnd13 = value
                    Case 14 : Me.Set1PeriodEnd14 = value
                End Select
            End Set
        End Property
        Public Property Set2PeriodEnd(ByVal index As Integer) As Date
            Get
                Select Case index
                    Case 1 : Return Me.Set2PeriodEnd1
                    Case 2 : Return Me.Set2PeriodEnd2
                    Case 3 : Return Me.Set2PeriodEnd3
                    Case 4 : Return Me.Set2PeriodEnd4
                    Case 5 : Return Me.Set2PeriodEnd5
                    Case 6 : Return Me.Set2PeriodEnd6
                    Case 7 : Return Me.Set2PeriodEnd7
                    Case 8 : Return Me.Set2PeriodEnd8
                    Case 9 : Return Me.Set2PeriodEnd9
                    Case 10 : Return Me.Set2PeriodEnd10
                    Case 11 : Return Me.Set2PeriodEnd11
                    Case 12 : Return Me.Set2PeriodEnd12
                    Case 13 : Return Me.Set2PeriodEnd13
                    Case 14 : Return Me.Set2PeriodEnd14
                    Case Else : Return Nothing
                End Select
            End Get
            Set(ByVal value As Date)
                Select Case index
                    Case 1 : Me.Set2PeriodEnd1 = value
                    Case 2 : Me.Set2PeriodEnd2 = value
                    Case 3 : Me.Set2PeriodEnd3 = value
                    Case 4 : Me.Set2PeriodEnd4 = value
                    Case 5 : Me.Set2PeriodEnd5 = value
                    Case 6 : Me.Set2PeriodEnd6 = value
                    Case 7 : Me.Set2PeriodEnd7 = value
                    Case 8 : Me.Set2PeriodEnd8 = value
                    Case 9 : Me.Set2PeriodEnd9 = value
                    Case 10 : Me.Set2PeriodEnd10 = value
                    Case 11 : Me.Set2PeriodEnd11 = value
                    Case 12 : Me.Set2PeriodEnd12 = value
                    Case 13 : Me.Set2PeriodEnd13 = value
                    Case 14 : Me.Set2PeriodEnd14 = value
                End Select
            End Set
        End Property

        Public Property Set1IsYearEnd(ByVal index As Integer) As Boolean
            Get
                Select Case index
                    Case 1 : Return Me.Set1IsYearEnd1
                    Case 2 : Return Me.Set1IsYearEnd2
                    Case 3 : Return Me.Set1IsYearEnd3
                    Case 4 : Return Me.Set1IsYearEnd4
                    Case 5 : Return Me.Set1IsYearEnd5
                    Case 6 : Return Me.Set1IsYearEnd6
                    Case 7 : Return Me.Set1IsYearEnd7
                    Case 8 : Return Me.Set1IsYearEnd8
                    Case 9 : Return Me.Set1IsYearEnd9
                    Case 10 : Return Me.Set1IsYearEnd10
                    Case 11 : Return Me.Set1IsYearEnd11
                    Case 12 : Return Me.Set1IsYearEnd12
                    Case 13 : Return Me.Set1IsYearEnd13
                    Case 14 : Return Me.Set1IsYearEnd14
                    Case Else : Return Nothing
                End Select
            End Get
            Set(ByVal value As Boolean)
                Select Case index
                    Case 1 : Me.Set1IsYearEnd1 = value
                    Case 2 : Me.Set1IsYearEnd2 = value
                    Case 3 : Me.Set1IsYearEnd3 = value
                    Case 4 : Me.Set1IsYearEnd4 = value
                    Case 5 : Me.Set1IsYearEnd5 = value
                    Case 6 : Me.Set1IsYearEnd6 = value
                    Case 7 : Me.Set1IsYearEnd7 = value
                    Case 8 : Me.Set1IsYearEnd8 = value
                    Case 9 : Me.Set1IsYearEnd9 = value
                    Case 10 : Me.Set1IsYearEnd10 = value
                    Case 11 : Me.Set1IsYearEnd11 = value
                    Case 12 : Me.Set1IsYearEnd12 = value
                    Case 13 : Me.Set1IsYearEnd13 = value
                    Case 14 : Me.Set1IsYearEnd14 = value
                End Select
            End Set
        End Property
        Public Property Set2IsYearEnd(ByVal index As Integer) As Boolean
            Get
                Select Case index
                    Case 1 : Return Me.Set2IsYearEnd1
                    Case 2 : Return Me.Set2IsYearEnd2
                    Case 3 : Return Me.Set2IsYearEnd3
                    Case 4 : Return Me.Set2IsYearEnd4
                    Case 5 : Return Me.Set2IsYearEnd5
                    Case 6 : Return Me.Set2IsYearEnd6
                    Case 7 : Return Me.Set2IsYearEnd7
                    Case 8 : Return Me.Set2IsYearEnd8
                    Case 9 : Return Me.Set2IsYearEnd9
                    Case 10 : Return Me.Set2IsYearEnd10
                    Case 11 : Return Me.Set2IsYearEnd11
                    Case 12 : Return Me.Set2IsYearEnd12
                    Case 13 : Return Me.Set2IsYearEnd13
                    Case 14 : Return Me.Set2IsYearEnd14
                    Case Else : Return Nothing
                End Select
            End Get
            Set(ByVal value As Boolean)
                Select Case index
                    Case 1 : Me.Set2IsYearEnd1 = value
                    Case 2 : Me.Set2IsYearEnd2 = value
                    Case 3 : Me.Set2IsYearEnd3 = value
                    Case 4 : Me.Set2IsYearEnd4 = value
                    Case 5 : Me.Set2IsYearEnd5 = value
                    Case 6 : Me.Set2IsYearEnd6 = value
                    Case 7 : Me.Set2IsYearEnd7 = value
                    Case 8 : Me.Set2IsYearEnd8 = value
                    Case 9 : Me.Set2IsYearEnd9 = value
                    Case 10 : Me.Set2IsYearEnd10 = value
                    Case 11 : Me.Set2IsYearEnd11 = value
                    Case 12 : Me.Set2IsYearEnd12 = value
                    Case 13 : Me.Set2IsYearEnd13 = value
                    Case 14 : Me.Set2IsYearEnd14 = value
                End Select
            End Set
        End Property

#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub


        Public Sub RollDates()
            ' move the Tomorrows date to todays and move the tomorrows code to today.
            ' add 1 to tomorrows date and see if it is greater than end of week if true say tommorrow to start of the week
            ' find if there are any days to be excluded if so reset tommorrow.
            ' if today is end of week seton end of week & if today is end of month seton end of month
            'NB Today is really Yesterday and Tommorrow is really today

            Me.Today = Me.Tomorrow
            Me.TodayDayNumber = Weekday(Me.Today, vbMonday)
            Me.Tomorrow = GetNextOpenDate(Me.Today)
            Me.TomorrowDayNumber = Weekday(Me.Tomorrow, vbMonday)

            If Me.TodayDayNumber = Me.WeekEndingDay Then
                Me.WeekEnd13 = Me.WeekEnd12
                Me.WeekEnd12 = Me.WeekEnd11
                Me.WeekEnd11 = Me.WeekEnd10
                Me.WeekEnd10 = Me.WeekEnd9
                Me.WeekEnd9 = Me.WeekEnd8
                Me.WeekEnd8 = Me.WeekEnd7
                Me.WeekEnd7 = Me.WeekEnd6
                Me.WeekEnd6 = Me.WeekEnd5
                Me.WeekEnd5 = Me.WeekEnd4
                Me.WeekEnd4 = Me.WeekEnd3
                Me.WeekEnd3 = Me.WeekEnd2
                Me.WeekEnd2 = Me.WeekEnd1
                Me.WeekEnd1 = Today
            End If

            If IsPeriodEnd(Me.Today) Then
                Me.CurrentPeriodEnd = Today

                If IsYearEnd(Me.Today) Then
                    Me.CurrentPeriodIsYearEnd = True
                    If Me.SetInUse = 1 Then
                        Me.SetInUse = 2
                    Else
                        Me.SetInUse = 1
                    End If
                End If
            End If

            Me.Save()

        End Sub

        Public Function GetNextOpenDate(ByVal startDate As Date) As Date

            Dim holidayDates As String = GetHolidayDates()
            Dim nextOpenDate As Date = startDate
            Dim isStoreOpen As Boolean

            Do
                nextOpenDate = nextOpenDate.AddDays(1)

                ' first day of week is Monday (by definition)
                Dim dayWeek As Integer = Weekday(nextOpenDate, vbMonday)
                If dayWeek > Me.DaysOpen Then
                    isStoreOpen = False
                Else
                    ' check this is not a holiday
                    Dim dateString As String = Format(nextOpenDate, "dd/MM/yy")
                    isStoreOpen = Not (holidayDates.Contains(dateString))
                End If

            Loop Until (isStoreOpen = True)

            Return nextOpenDate

        End Function


        Public Function IsPeriodEnd(ByVal testDate As Date) As Boolean
            Select Case Me.SetInUse
                Case 1
                    For index As Integer = 1 To 14
                        If Me.Set1PeriodEnd(index) = testDate Then Return True
                    Next

                Case Else
                    For index As Integer = 1 To 14
                        If Me.Set2PeriodEnd(index) = testDate Then Return True
                    Next
            End Select
            Return False
        End Function

        Public Function IsYearEnd(ByVal testDate As Date) As Boolean
            Select Case Me.SetInUse
                Case 1
                    For index As Integer = 1 To 14
                        If Me.Set1PeriodEnd(index) = testDate Then Return Me.Set1IsYearEnd(index)
                    Next

                Case Else
                    For index As Integer = 1 To 14
                        If Me.Set2PeriodEnd(index) = testDate Then Return Me.Set2IsYearEnd(index)
                    Next
            End Select
            Return False
        End Function


        Public Function Save() As Boolean
            Return SaveUpdate()
        End Function

        Private Function SaveUpdate() As Boolean
            Return DataAccess.DateUpdate(Me)
        End Function

    End Class

    <HideModuleName()> Friend Module DataAccess

        Friend Function DateGet() As DataTable
            Using con As New Connection
                Using com As Command = con.NewCommand(My.Resources.Procedures.DateGet)
                    Return com.ExecuteDataTable
                End Using
            End Using
        End Function

        Friend Function DateUpdate(ByRef sysDate As [Date]) As Boolean
            Using con As New Connection
                Using com As Command = con.NewCommand(My.Resources.Procedures.DateUpdate)
                    com.AddParameter(My.Resources.Parameters.Id, sysDate.Id)
                    com.AddParameter(My.Resources.Parameters.Today, sysDate.Today)
                    com.AddParameter(My.Resources.Parameters.TodayDayNumber, sysDate.TodayDayNumber)
                    com.AddParameter(My.Resources.Parameters.Tomorrow, sysDate.Tomorrow)
                    com.AddParameter(My.Resources.Parameters.TomorrowDayNumber, sysDate.TomorrowDayNumber)
                    com.AddParameter(My.Resources.Parameters.WeekFromToday, sysDate.WeekFromToday)
                    com.AddParameter(My.Resources.Parameters.WeekEndProcessed, sysDate.WeekEndProcessed)
                    com.AddParameter(My.Resources.Parameters.PeriodEndProcessed, sysDate.PeriodEndProcessed)
                    com.AddParameter(My.Resources.Parameters.CurrentPeriodIsYearEnd, sysDate.CurrentPeriodIsYearEnd)
                    com.AddParameter(My.Resources.Parameters.CurrentPeriodEnd, sysDate.CurrentPeriodEnd)
                    com.AddParameter(My.Resources.Parameters.SetInUse, sysDate.SetInUse)

                    com.AddParameter(My.Resources.Parameters.WeekEnd1, sysDate.WeekEnd1)
                    com.AddParameter(My.Resources.Parameters.WeekEnd2, sysDate.WeekEnd2)
                    com.AddParameter(My.Resources.Parameters.WeekEnd3, sysDate.WeekEnd3)
                    com.AddParameter(My.Resources.Parameters.WeekEnd4, sysDate.WeekEnd4)
                    com.AddParameter(My.Resources.Parameters.WeekEnd5, sysDate.WeekEnd5)
                    com.AddParameter(My.Resources.Parameters.WeekEnd6, sysDate.WeekEnd6)
                    com.AddParameter(My.Resources.Parameters.WeekEnd7, sysDate.WeekEnd7)
                    com.AddParameter(My.Resources.Parameters.WeekEnd8, sysDate.WeekEnd8)
                    com.AddParameter(My.Resources.Parameters.WeekEnd9, sysDate.WeekEnd9)
                    com.AddParameter(My.Resources.Parameters.WeekEnd10, sysDate.WeekEnd10)
                    com.AddParameter(My.Resources.Parameters.WeekEnd11, sysDate.WeekEnd11)
                    com.AddParameter(My.Resources.Parameters.WeekEnd12, sysDate.WeekEnd12)
                    com.AddParameter(My.Resources.Parameters.WeekEnd13, sysDate.WeekEnd13)
                    Return (com.ExecuteNonQuery > 0)
                End Using
            End Using
        End Function

    End Module

End Namespace

