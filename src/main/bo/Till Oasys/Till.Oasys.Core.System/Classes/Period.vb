﻿Imports Till.Oasys.Data

Namespace Period

    <HideModuleName()> Public Module SharedFunctions

        Public Function GetPeriod(ByVal periodId As Integer) As Period
            Dim dt As DataTable = DataAccess.SystemPeriodGet(periodId)
            If dt.Rows.Count > 0 Then Return New Period(dt.Rows(0))
            Return Nothing
        End Function

        Public Function GetCurrent() As Period
            Dim dt As DataTable = DataAccess.SystemPeriodGet(Now.Date, Now.AddDays(1).Date, False)
            If dt.Rows.Count > 0 Then Return New Period(dt.Rows(0))
            Return Nothing
        End Function

        Public Function GetAllToDate() As PeriodCollection
            Dim pc As New PeriodCollection
            pc.LoadAllToDate()
            Return pc
        End Function

        Public Function GetAllSafeOpenToDate() As PeriodCollection
            Dim pc As New PeriodCollection
            pc.LoadAllSafeOpenToDate()
            Return pc
        End Function

    End Module

    Public Class Period
        Inherits Base

#Region "       Table Fields"
        Private _id As Integer
        Private _startDate As Date
        Private _endDate As Date
        Private _isClosed As Boolean

        <ColumnMapping("ID")> Public Property Id() As Integer
            Get
                Return _id
            End Get
            Set(ByVal value As Integer)
                _id = value
            End Set
        End Property
        <ColumnMapping("StartDate")> Public Property StartDate() As Date
            Get
                Return _startDate
            End Get
            Set(ByVal value As Date)
                _startDate = value
            End Set
        End Property
        <ColumnMapping("EndDate")> Public Property EndDate() As Date
            Get
                Return _endDate
            End Get
            Set(ByVal value As Date)
                _endDate = value
            End Set
        End Property
        <ColumnMapping("IsClosed")> Public Property IsClosed() As Boolean
            Get
                Return _isClosed
            End Get
            Set(ByVal value As Boolean)
                _isClosed = value
            End Set
        End Property

        Public ReadOnly Property IdStartDate() As String
            Get
                Return Me.Id & Space(1) & Me.StartDate.ToShortDateString
            End Get
        End Property
#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Friend Sub New(ByVal dr As DataRow)
            MyBase.New(dr)
        End Sub

    End Class

    Public Class PeriodCollection
        Inherits BaseCollection(Of Period)

        Public Sub LoadAllToDate()
            Dim dt As DataTable = DataAccess.SystemPeriodGet(Nothing, Now.AddDays(1).Date, False)
            Me.Load(dt)
        End Sub

        Public Sub LoadAllSafeOpenToDate()
            Dim dt As DataTable = DataAccess.SystemPeriodSafeGetOpenClosed(Nothing, Now.AddDays(1).Date, False)
            Me.Load(dt)
        End Sub

    End Class

    <HideModuleName()> Friend Module DataAccess

        Friend Function SystemPeriodGet(ByVal startDate As Date?, ByVal endDate As Date?, ByVal IsClosed As Boolean?) As DataTable
            Using con As New Connection
                Using com As Command = con.NewCommand(My.Resources.Procedures.SystemPeriodGet)
                    If startDate.HasValue Then com.AddParameter(My.Resources.Parameters.StartDate, startDate.Value)
                    If endDate.HasValue Then com.AddParameter(My.Resources.Parameters.EndDate, endDate.Value)
                    If IsClosed.HasValue Then com.AddParameter(My.Resources.Parameters.IsClosed, IsClosed.Value)
                    Return com.ExecuteDataTable
                End Using
            End Using
            Return Nothing
        End Function

        Friend Function SystemPeriodGet(ByVal periodId As Integer) As DataTable
            Using con As New Connection
                Using com As Command = con.NewCommand(My.Resources.Procedures.SystemPeriodGet)
                    com.AddParameter(My.Resources.Parameters.Id, periodId)
                    Return com.ExecuteDataTable
                End Using
            End Using
            Return Nothing
        End Function

        Friend Function SystemPeriodSafeGetOpenClosed(ByVal startDate As Date?, ByVal endDate As Date?, ByVal isClosed As Boolean) As DataTable
            Using con As New Connection
                Using com As Command = con.NewCommand(My.Resources.Procedures.SystemPeriodSafeGet)
                    If startDate.HasValue Then com.AddParameter(My.Resources.Parameters.StartDate, startDate.Value)
                    If endDate.HasValue Then com.AddParameter(My.Resources.Parameters.EndDate, endDate.Value)
                    com.AddParameter(My.Resources.Parameters.IsClosed, isClosed)
                    Return com.ExecuteDataTable
                End Using
            End Using
            Return Nothing
        End Function

    End Module

End Namespace

