﻿Imports Till.Oasys.Data
Imports System.Text

Namespace DataAccess

    <HideModuleName()> Friend Module DataSystem

        Friend Function ParameterGet(ByVal id As Integer) As DataTable

            Using con As New Connection
                Using com As New Command(con)
                    Select Case con.DataProvider
                        Case DataProvider.Odbc
                            com.CommandText = "Select * from Parameters where ParameterId=?"
                            com.AddParameter("Id", id)
                            Return com.ExecuteDataTable

                        Case DataProvider.Sql
                            com.StoredProcedureName = My.Resources.Procedures.SystemGetParameter
                            com.AddParameter(My.Resources.Parameters.Id, id)
                            Return com.ExecuteDataTable

                        Case Else
                            Return Nothing
                    End Select
                End Using
            End Using

        End Function

        Friend Function SystemOptionGet() As DataTable

            Using con As New Connection
                Using com As New Command(con)
                    Select Case con.DataProvider
                        Case DataProvider.Odbc
                            com.CommandText = "Select STOR as StoreNumber, SNAM as StoreName, MAST as MasterWorkstationId from SYSOPT"
                            Return com.ExecuteDataTable

                        Case DataProvider.Sql
                            com.StoredProcedureName = My.Resources.Procedures.SystemOptionGet
                            Return com.ExecuteDataTable

                        Case Else
                            Return Nothing
                    End Select
                End Using
            End Using

        End Function

        Friend Function SystemOptionUpdate(ByVal dateLastReformat As Date) As Integer
            Using con As New Connection
                Using com As Command = con.NewCommand(My.Resources.Procedures.SystemOptionUpdate)
                    com.AddParameter(My.Resources.Parameters.DateLastReformat, dateLastReformat)
                    Return com.ExecuteNonQuery
                End Using
            End Using
        End Function

        Friend Function SystemNetworkGet() As Boolean

            Using con As New Connection
                Using com As New Command(con)
                    Select Case con.DataProvider
                        Case DataProvider.Odbc
                            com.CommandText = "Select MAST From SYSNID where FKEY='01'"
                            Return CBool(com.ExecuteValue)

                        Case DataProvider.Sql
                            com.StoredProcedureName = My.Resources.Procedures.SystemNetworkGet
                            Return CBool(com.ExecuteValue)

                        Case Else
                            Return False
                    End Select
                End Using
            End Using

        End Function

        Friend Function SystemNetworkUpdate(ByVal masterOpen As Boolean) As Integer

            Using con As New Connection
                Using com As New Command(con)
                    Select Case con.DataProvider
                        Case DataProvider.Odbc
                            com.CommandText = "UPDATE SYSNID SET MAST=? WHERE FKEY = 01"
                            com.AddParameter("MAST", masterOpen)
                            Return com.ExecuteNonQuery

                        Case DataProvider.Sql
                            com.StoredProcedureName = My.Resources.Procedures.SystemNetworkUpdate
                            com.AddParameter(My.Resources.Parameters.MasterOpen, masterOpen)
                            Return com.ExecuteNonQuery

                        Case Else
                            Return 0
                    End Select
                End Using
            End Using

        End Function

        Friend Function ActivityLogInsert(ByRef log As ActivityLog) As Integer

            Dim linesAffected As Integer = 0
            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.ActivityLogInsert)
                    com.AddParameter(My.Resources.Parameters.MenuId, log.MenuId)
                    com.AddParameter(My.Resources.Parameters.UserId, log.UserId)
                    com.AddParameter(My.Resources.Parameters.WorkstationId, log.WorkstationId)
                    com.AddParameter(My.Resources.Parameters.LoggedIn, log.LoggedIn)
                    com.AddParameter(My.Resources.Parameters.LoggedOutForced, log.LoggedOutForced)
                    com.AddParameterOutput(My.Resources.Parameters.Id, GetType(Integer), 4)
                    linesAffected = com.ExecuteNonQuery

                    log.Id = CStr(com.GetParameterValue(My.Resources.Parameters.Id))
                End Using
            End Using
            Return linesAffected

        End Function

        Friend Function ActivityLogEnded(ByVal id As Integer) As Integer

            Dim linesAffected As Integer = 0
            Using con As New Connection
                Using com As New Command(con, My.Resources.Procedures.ActivityLogEnded)
                    com.AddParameter(My.Resources.Parameters.Id, id)
                    linesAffected = com.ExecuteNonQuery
                End Using
            End Using
            Return linesAffected

        End Function

        Friend Function SystemNumberGetNextNumber(ByVal id As Integer) As Integer
            Using con As New Connection
                Using com As Command = con.NewCommand(My.Resources.Procedures.SystemNumberGetNextNumber)
                    com.AddParameter(My.Resources.Parameters.Id, id)
                    Return CInt(com.ExecuteValue)
                End Using
            End Using
        End Function


    End Module

End Namespace

