VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.UserControl ucItemFilter 
   ClientHeight    =   435
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   705
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   ScaleHeight     =   435
   ScaleWidth      =   705
   Begin MSAdodcLib.Adodc adodcSearch 
      Height          =   495
      Left            =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   1200
      _ExtentX        =   2117
      _ExtentY        =   873
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "OASYS"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
End
Attribute VB_Name = "ucItemFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'*  Module : ucItemFilter
'*  Date   : 14/10/02
'*  Author : mauricem
' $Archive: /Projects/OasysHW/VB/Item Filter UC/ucItemFilter.ctl $
'**********************************************************************************************
'*  Application Summary
'*  ===================
'*  This provides a reusable Control that can be accessed from the VB-IDE toolbox for
'*  the Item Fuzzy Matching.  The control loads the Item Filter exe which is used for the item
'*  selection.
'*
'**********************************************************************************************
'* $ Author: $ $ Date: $ $ Revision: $
'*
'*  Date        Author      Vers.   Comments
'*  ========    ========    =====   =================================
'*  14/10/02    mauricem    1.0.0   Header added.
'*  ...         ...         ...     Unknown updates up to this point. Last version: 1.2.126.
'*  24/03/05    tjnorris    1.3.0   Updates added:
'*                                  1. Search by alt. description.
'*                                  2. Search by hierarchy structure.
'*                                  3. Added 'On Order' field to results list.
'*                                  4. Included options to include/exclude 'del/obs/non' items.
'*                                  5. Removed functionality for cmdISTItems.
'*                                  6. Removed auto-search on 5 characters or more in description.
'*                                  7. Show bulk and single item relationships in search results.
'*  07/04/05    tjnorris    1.3.1   Minor changes to keypress and keydown event handling.
'*
'*  03/07/06    DaveF       1.4.0   WIX1156 - Added PIM image functionality to show images for
'*                                      PIM and full listing products. Also uses a treeview to
'*                                      show and select the hierarchy information.
'*
'*  23/11/06    DaveF       1.4.4   Changed so just used as an interface to call the Item Filter
'*                                      exe which does the actual work.
'*
'*'</CAMH>***************************************************************************************

Option Explicit

Private Const MODULE_NAME As String = "ucItemFilter"

Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Private Const ITEM_FILTER_EXE_NAME As String = "ItemFilter.exe"

Private Const WINSOCK_MESSAGE_TERMINATOR As String = "~"

Private WithEvents frmItemFilter As frmItemFilter
Attribute frmItemFilter.VB_VarHelpID = -1

'Event Declarations:
Public Event Cancel()                              'MappingInfo=cmdClose,cmdClose,-1,Click
Public Event Apply(PartCode As String)
Public Event UseList(SearchCriteria As Collection, ByRef oMatchingItems As Object)
Public Event Duress()

Private mblnPreview       As Boolean
Private mblnShowUseList   As Boolean
Private moParent          As Form
Private colSearchCriteria As Collection

' Milliseconds to pause the Winsock calls.
Private Const SLEEP_TIME As Long = 1

' SKU enquiry type, PIM items only or full product listing.
Public Enum EnquiryType
    etPIMOnly = 1
    etFullItems = 2
    etNotSet = 3
End Enum
Private metEnquiryType         As EnquiryType

Private mblnItemFilterConnnected  As Boolean
Private mstrWinsockMessage        As String
Private mblnIsFirstWinsockData    As Boolean
Private mblnShowSupplier          As Boolean
Private mblnShowObsoleteButton    As Boolean
Private mblnShowNonStockButton    As Boolean
Private mblnISInitialised         As Boolean
Private mblnItemFilterExists      As Boolean

Private mblnStartupError As Boolean

Public ViewOnly As Boolean

Private Sub frmItemFilter_ProcessMessage(MessageString As String)
    
    If (mblnIsFirstWinsockData = True) Then
        Call DebugMsg(MODULE_NAME, "wsckComms_DataArrival", endlDebug, "New Message")
        mstrWinsockMessage = ""
        mblnIsFirstWinsockData = False
    End If
    
    Call DebugMsg(MODULE_NAME, "wsckComms_DataArrival", endlDebug, _
            "Message Received - " & MessageString)
    
    If (Right(MessageString, 1) = WINSOCK_MESSAGE_TERMINATOR) Then
        mblnIsFirstWinsockData = True
        mstrWinsockMessage = mstrWinsockMessage & Left(MessageString, Len(MessageString) - 1)
        Call ProcessWinsockMessage(mstrWinsockMessage)
    Else
        mstrWinsockMessage = mstrWinsockMessage & MessageString
    End If

End Sub

Private Sub UserControl_Initialize()

Const PROCEDURE_NAME As String = "UserControl_Initialize"

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlTraceIn)
    
    mblnShowSupplier = True
    mblnShowObsoleteButton = True
    mblnShowNonStockButton = True
    mblnShowUseList = False
      
    Call DebugMsg(MODULE_NAME, "UC_Initialize", endlTraceOut)
    
End Sub

Private Sub UserControl_GotFocus()

Const PROCEDURE_NAME As String = "UserControl_GotFocus"
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    Call UserControl_Show

End Sub

'Private Sub UserControl_LostFocus()
'
'    Call UserControl_Hide
'
'End Sub

'Private Sub UserControl_Hide()
'
'    Call DebugMsg(MODULE_NAME, "UC_Hide", endlDebug, "Resetting preview")
'
'    ' Reset and hide the Item Filter exe.
'    Call SendWinsockMessage(PIM_ITEM_FILTER_HIDE)
'
'    If (Not moParent Is Nothing) Then
'        moParent.KeyPreview = mblnPreview
'    End If
'
'End Sub

Private Sub UserControl_Show()
    
Const PROCEDURE_NAME As String = "UserControl_Show"
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    If Not (moParent Is Nothing) Then
        mblnPreview = moParent.KeyPreview
        moParent.KeyPreview = False
    Else
        Call DebugMsg(MODULE_NAME, "UserControl_Show", endlDebug, "No Parent to set KeyPreview")
    End If
    
End Sub

' Flag if there was an error on start up.
Public Property Get StartupError() As Boolean

    StartupError = mblnStartupError
    
End Property

' Store the type of item enquiry to use.
Public Property Let ItemEnquiryType(etEnquiryType As EnquiryType)

Const PROCEDURE_NAME As String = "Let ItemEnquiryType"
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    If (mblnItemFilterConnnected = False) Then
        Call TestItemFilterIsReceiving
    End If
    If (etEnquiryType = etPIMOnly) Then
        metEnquiryType = etPIMOnly
        Call SendWinsockMessage(PIM_ITEM_FILTER_START_MODE_PIM)
    Else
        metEnquiryType = etFullItems
        Call SendWinsockMessage(PIM_ITEM_FILTER_START_MODE_FULL)
    End If
    
End Property

Public Property Let LookupSKU(ByVal strEAN As String)

Const PROCEDURE_NAME As String = "Let LookupSKU"
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    If (mblnItemFilterConnnected = False) Then
        Call TestItemFilterIsReceiving
    End If
    ' Send the SKU details to the Item Filter to lookup.
    Call SendWinsockMessage(PIM_ITEM_FILTER_LOOKUP_SKU & strEAN)
    'Call ShowItemFilter
    
End Property

Public Property Get UseList() As Boolean
    
    UseList = mblnShowUseList
    
End Property

Public Property Let UseList(ByVal VisibleFlag As Boolean)

Const PROCEDURE_NAME As String = "Let UseList"
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    mblnShowUseList = VisibleFlag
    If (mblnItemFilterConnnected = False) Then
        Call TestItemFilterIsReceiving
    End If
    ' Send the message to the Item Filter.
    If (VisibleFlag = True) Then
        Call SendWinsockMessage(PIM_ITEM_FILTER_USE_LIST_TRUE)
    Else
        Call SendWinsockMessage(PIM_ITEM_FILTER_USE_LIST_FALSE)
    End If
    
End Property

Public Property Let SupplierNo(ByVal sSupplierCode As String)
    
Const PROCEDURE_NAME As String = "Let SupplierNo"
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    If (mblnItemFilterConnnected = False) Then
        Call TestItemFilterIsReceiving
    End If
    ' Send the SKU details to the Item Filter to lookup.
    Call SendWinsockMessage(PIM_ITEM_FILTER_SUPPLIER_CODE & sSupplierCode)
        
End Property

Public Property Let ShowSupplier(ByVal ShowSupplier As Boolean)

Const PROCEDURE_NAME As String = "Let ShowSupplier"
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    ' Send the message on whether to show the Supplier combo to the Item Filter.
    mblnShowSupplier = ShowSupplier
    If (mblnItemFilterConnnected = False) Then
        Call TestItemFilterIsReceiving
    End If
    If (ShowSupplier = True) Then
        Call SendWinsockMessage(PIM_ITEM_FILTER_SHOW_SUPPLIER_TRUE)
    Else
        Call SendWinsockMessage(PIM_ITEM_FILTER_SHOW_SUPPLIER_FALSE)
    End If

End Property

Public Property Let ShowSelectActiveOnly(ByVal blnSelectActive As Boolean)
    
Const PROCEDURE_NAME As String = "Let ShowSelectActiveOnly"
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    ' Send the message on whether to show the Obsolete button to the Item Filter.
    mblnShowObsoleteButton = blnSelectActive
    If (mblnItemFilterConnnected = False) Then
        Call TestItemFilterIsReceiving
    End If
    If (blnSelectActive = True) Then
        Call SendWinsockMessage(PIM_ITEM_FILTER_SHOW_OBSOLETE_TRUE)
    Else
        Call SendWinsockMessage(PIM_ITEM_FILTER_SHOW_OBSOLETE_FALSE)
    End If

End Property

Public Property Let ShowSelectNonStockItems(ByVal blnSelectNonStock As Boolean)

Const PROCEDURE_NAME As String = "Let ShowSelectNonStockItems"
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    ' Send the message on whether to show the Non-Stock button to the Item Filter.
    mblnShowNonStockButton = blnSelectNonStock
    If (mblnItemFilterConnnected = False) Then
        Call TestItemFilterIsReceiving
    End If
    If (blnSelectNonStock = True) Then
        Call SendWinsockMessage(PIM_ITEM_FILTER_SHOW_OBSOLETE_TRUE)
    Else
        Call SendWinsockMessage(PIM_ITEM_FILTER_SHOW_OBSOLETE_FALSE)
    End If
    
End Property

Public Sub Initialise(Optional ByRef CurrentSession As OasysRootCom_Wickes.Session = Nothing, _
                      Optional ByRef ParentForm As Object = Nothing)
                      
Const PROCEDURE_NAME As String = "Initialise"
   
    Call DebugMsg(MODULE_NAME, "Initialise", endlTraceIn, "Start")
    mblnItemFilterConnnected = True
                    
    If (CurrentSession Is Nothing) Then
        If (mblnISInitialised = False) Then
            Set goRoot = GetRoot
        End If
    Else
        Set goSession = CurrentSession
    End If
    Set goDatabase = goSession.Database
    
    If (mblnISInitialised = False) Then
        Call CreateErrorObject(SYSTEM_NAME & "{" & App.Title & "}", VBA.Err, Err)
           
        ' Start the Winsock connection to the Item Filter exe.
        If (StartItemFilter = False) Then
            mblnStartupError = True
        End If
        
        mblnISInitialised = True
        
        If (mblnItemFilterConnnected = False) Then
            Call TestItemFilterIsReceiving
        End If
    End If
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "End")

End Sub

Public Sub FillScreen(ByRef ucMe As Object)

Const PROCEDURE_NAME As String = "FillScreen"

Dim ScreenControl As Control
Dim etEnquiryType As EnquiryType
   
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
   
    ' Start the Winsock connection to the Item Filter exe.
    If (TestItemFilterIsReceiving = False) Then
         Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Connect to Item Filter failed")
         Call MsgBoxEx("Connect to Item Filter failed", vbCritical, "Item Filter Error", _
                         , , , , RGBMsgBox_WarnColour)
         Exit Sub
    End If
   
    If (ViewOnly = True) Then
        Call SendWinsockMessage(PIM_ITEM_FILTER_START_MODE_VIEW_ONLY)
    End If

    ' Show the Item Filter exe.
    Call ShowItemFilter
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "End")

End Sub

' Processes the received winsock message.
Private Sub ProcessWinsockMessage(strMessage As String)

Const PROCEDURE_NAME As String = "Let ShowSelectNonStockItems"
        
    Dim strMessageType As String

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Message - " & strMessage)
    
    If (InStr(strMessage, "|") > 0) Then
        strMessageType = Left(strMessage, InStr(strMessage, "|"))
    Else
        strMessageType = strMessage
    End If
    
    ' Process the message.
    Select Case strMessageType
        Case PIM_ITEM_FILTER_RECEIVING
            mblnItemFilterConnnected = True
        Case PIM_ITEM_FILTER_EVENT_APPLY
            ' Retrieve the passed sku value from the message.
            RaiseEvent Apply(Right(strMessage, Len(strMessage) - InStr(strMessage, "|")))
        Case PIM_ITEM_FILTER_EVENT_CANCEL
            RaiseEvent Cancel
        Case PIM_ITEM_FILTER_EVENT_DURESS
            RaiseEvent Duress
        Case PIM_ITEM_FILTER_EVENT_USELIST
            RaiseEvent UseList(colSearchCriteria, adodcSearch)
    End Select
    mstrWinsockMessage = ""
    
End Sub

'   Send the message through the Winsock control.
Public Function SendWinsockMessage(strMessage) As Boolean

Const PROCEDURE_NAME As String = "SendWinsockMessage"
    
    Call DebugMsg(MODULE_NAME, "SendWinsockMessage", endlDebug, _
            "Winsock Message - " & strMessage)
    
    Call frmItemFilter.PerformAction(strMessage & WINSOCK_MESSAGE_TERMINATOR)
    DoEvents

End Function
    
' Tests if the Item Filter exe is running, if not start it.
Private Function StartItemFilter() As Boolean

Const PROCEDURE_NAME As String = "StartItemFilter"
    
    Set frmItemFilter = New frmItemFilter
    Load frmItemFilter
'    frmItemFilter.oParent = Me
    mblnItemFilterExists = True
    StartItemFilter = True

    Exit Function
    
    
    Dim objFSO As Scripting.FileSystemObject
        
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    ' See if the Item Filter exe is running, if not start it.
    If (GetProcesses(ITEM_FILTER_EXE_NAME) = 0) Then
        ' Test if the Item Filter exe exists.
        Set objFSO = New Scripting.FileSystemObject
        If (objFSO.FileExists(App.Path & "\" & ITEM_FILTER_EXE_NAME) = False) Then
            mblnItemFilterExists = False
            StartItemFilter = False
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, ITEM_FILTER_EXE_NAME & " NOT FOUND")
            Call MsgBoxEx("The Item Filter exe cannot be found." & vbNewLine & vbNewLine & _
                    "Path - " & App.Path & "\" & ITEM_FILTER_EXE_NAME, vbCritical + vbOKOnly, _
                    "Item Filter Not Found", , , , , RGBMsgBox_WarnColour)
            Exit Function
        Else
            mblnItemFilterExists = True
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, ITEM_FILTER_EXE_NAME & " not running so starting")
            Call Shell(App.Path & "\" & ITEM_FILTER_EXE_NAME, vbHide)
            StartItemFilter = True
        End If
        Set objFSO = Nothing
    Else
        mblnItemFilterExists = True
        StartItemFilter = True
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, ITEM_FILTER_EXE_NAME & " running")
    End If

End Function

' Test if the Item Filter is receiving the winsock communications.
Private Function TestItemFilterIsReceiving() As Boolean

Const PROCEDURE_NAME As String = "TestItemFilterIsReceiving"

    On Error GoTo TestItemFilterIsReceiving_Error
        
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    ' Test if the system has been initialised yet.
    If (mblnISInitialised = False) Then
        Call Initialise
    End If
    
    If (mblnItemFilterExists = False) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "ItemFilter exe does not exist")
        TestItemFilterIsReceiving = False
        Exit Function
    End If
    
    TestItemFilterIsReceiving = True
    
Exit Function

TestItemFilterIsReceiving_Error:

    TestItemFilterIsReceiving = False
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Connect to Item Filter process failed, " & _
            "Error - " & Err.Number & " " & Err.Description)
    Call MsgBoxEx("Connect to Item Filter process failed, Error - " & Err.Number & " " & Err.Description, _
            vbCritical, "Item Filter Error", , , , , RGBMsgBox_WarnColour)

End Function

Private Sub ShowItemFilter()

Const PROCEDURE_NAME As String = "ShowItemFilter"
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    If (mblnItemFilterExists = False) Then
        RaiseEvent Cancel
    End If
    
    ' Show the Item Filter exe.
    If (mblnShowSupplier = False) Then
        Call SendWinsockMessage(PIM_ITEM_FILTER_SHOW_SUPPLIER_FALSE)
    End If
    If (ViewOnly = True) Then
        Call SendWinsockMessage(PIM_ITEM_FILTER_START_MODE_VIEW_ONLY)
    End If
    If (mblnShowUseList = True) Then
        Call SendWinsockMessage(PIM_ITEM_FILTER_USE_LIST_TRUE)
    Else
        Call SendWinsockMessage(PIM_ITEM_FILTER_USE_LIST_FALSE)
    End If
    If (mblnShowObsoleteButton = True) Then
        Call SendWinsockMessage(PIM_ITEM_FILTER_SHOW_OBSOLETE_TRUE)
    Else
        Call SendWinsockMessage(PIM_ITEM_FILTER_SHOW_OBSOLETE_FALSE)
    End If
    If (mblnShowNonStockButton = True) Then
        Call SendWinsockMessage(PIM_ITEM_FILTER_SHOW_NON_STOCK_TRUE)
    Else
        Call SendWinsockMessage(PIM_ITEM_FILTER_SHOW_NON_STOCK_FALSE)
    End If
    If (metEnquiryType = etPIMOnly) Then
        Call SendWinsockMessage(PIM_ITEM_FILTER_START_MODE_PIM)
    Else
        Call SendWinsockMessage(PIM_ITEM_FILTER_START_MODE_FULL)
    End If
    Call SendWinsockMessage(PIM_ITEM_FILTER_SHOW)

End Sub
