VERSION 5.00
Begin VB.Form frmItemViewer 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Item Viewer"
   ClientHeight    =   8295
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8895
   ControlBox      =   0   'False
   Icon            =   "frmItemViewer.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   553
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   593
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSelect 
      Caption         =   "F5 - Se&lect"
      Default         =   -1  'True
      Height          =   570
      Left            =   667
      TabIndex        =   0
      Top             =   7650
      Width           =   1200
   End
   Begin VB.PictureBox picFullImage 
      Height          =   7560
      Left            =   667
      ScaleHeight     =   500
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   500
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   45
      Width           =   7560
   End
   Begin VB.CommandButton cmdClose 
      Cancel          =   -1  'True
      Caption         =   "F12 - &Close"
      Height          =   570
      Left            =   7012
      TabIndex        =   1
      Top             =   7650
      Width           =   1200
   End
   Begin VB.Label lblItemDescription 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   570
      Left            =   1972
      TabIndex        =   3
      Top             =   7650
      Width           =   4950
   End
End
Attribute VB_Name = "frmItemViewer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'*  Module : frmItemViewer
'*  Date   : 30/06/06
'*  Author : DaveF
' $Archive: /Projects/OasysHW/VB/Item Filter UC/frmItemViewer.frm $
'*
'**********************************************************************************************
'*  Application Summary
'*  ===================
'*  Displays the enlarged product image.
'*
'**********************************************************************************************
'* $ Author: $ $ Date: $ $ Revision: $
'*
'*  Date        Author      Vers.   Comments
'*  ========    ========    =====   =================================
'*  30/06/06    DaveF       1.0.0   Initial version.
'*
'</CAMH>***************************************************************************************

Option Explicit

Private mstrImagePath    As String
Private mblnItemSelected As Boolean

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If Shift = 0 Then
        Select Case (KeyCode)
            Case vbKeyF5
                KeyCode = 0
                If (cmdSelect.Visible = True) Then
                    Call cmdSelect_Click    ' Select image and close image viewer.
                End If
            Case vbKeyF12
                KeyCode = 0
                Call cmdClose_Click     ' Close Image Viewer.
        End Select
    End If

End Sub

Public Function LoadImage(ByVal strSKU As String) As Boolean

Dim lngImageHeight As Long
Dim lngImageWidth  As Long
Dim strImageFile   As String

    On Error GoTo LoadImage_error
    
    ' Load the large image to the picture box.
    
           
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author   : Partha Dutta
    ' Date     : 07/06/2011
    ' Referral : 26
    ' Notes    : Thumbnail & large version of image uses the same file
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    'new image file name
    strImageFile = mstrImagePath & strSKU & ".jpg"
   
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    
    picFullImage.Picture = LoadPicture(strImageFile)
    LoadImage = True
       
    Exit Function
    
LoadImage_error:

    LoadImage = False
    Call MsgBoxEx("The enlarged image could not be found.", vbCritical + vbOKOnly, "Image Not Found", , , , , RGBMsgBox_WarnColour)

End Function

Public Property Let Description(strDescription As String)

    ' Show the SKU description.
    Me.lblItemDescription = strDescription

End Property

Public Property Let ImagePath(strImagePath As String)

    mstrImagePath = strImagePath

End Property

Public Property Let ShowSelectButton(blnShowSelectButton As Boolean)

    cmdSelect.Visible = blnShowSelectButton

End Property

Public Property Get ItemSelected() As Boolean

    ItemSelected = mblnItemSelected
    
End Property

Private Sub cmdClose_Click()

    mblnItemSelected = False
    Me.Hide
    
End Sub

Private Sub cmdSelect_Click()

    mblnItemSelected = True
    Me.Hide
    
End Sub

