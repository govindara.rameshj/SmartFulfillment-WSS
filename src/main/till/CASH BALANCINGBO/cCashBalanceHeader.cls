VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cCashBalanceHeader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D5D07A702E4"
'<CAMH>****************************************************************************************
'* Module : cCashBalanceHeader
'* Date   : 13/04/03
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Cash Balance BO/cCashBalanceHeader.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 7/05/03 9:39 $ $Revision: 3 $
'* Versions:
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "CashBalanceHeader"

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=3D5D186303DE
Private mFileKey As String

'##ModelId=3D5D186C0262
Private mCurrentDate As Date

'##ModelId=3D5D18720258
Private mStoreBankTotal As Currency

'##ModelId=3D5D187F00E6
Private mGiftVoucherStock As Currency

'##ModelId=3D5D18870050
Private mCustomerDeposits As Currency

'##ModelId=3D5D188D029E
Private mStoreFloatAmount As Currency

'##ModelId=3D5D189501D6
Private mDepositInDeptNo As String

'##ModelId=3D5D18C1001E
Private mDepositOutDeptNo As String

'##ModelId=3D5D18C7023A
Private mNoSummaryKept As Long

'##ModelId=3D5D18DA0348
Private mNoSummaryNow As Long

'##ModelId=3D5D18ED02E4
Private mSalesOfGiftVoucher As Long

'##ModelId=3D5D190D0064
Private mTenderType(40) As String

'##ModelId=3D5D191A00AA
Private mNoReturns(40) As Long

'##ModelId=3D5D1928030C
Private mDepositInBank(40) As Boolean

'##ModelId=3D5D19330032
Private mSequenceTendered(40) As Long

'##ModelId=3D5D19470032
Private mRequestSlipForTender(40) As Boolean

'##ModelId=3D5D1957038E
Private mMaximumPerSlip As Currency

'##ModelId=3D5D19660316
Private mNoSummaryInFile As Long

'##ModelId=3E99A47A0149
Public Property Get NoSummaryInFile() As Long
   Let NoSummaryInFile = mNoSummaryInFile
End Property

'##ModelId=3E99A47A0063
Public Property Let NoSummaryInFile(ByVal Value As Long)
    Let mNoSummaryInFile = Value
End Property

'##ModelId=3E99A47903B4
Public Property Get MaximumPerSlip() As Currency
   Let MaximumPerSlip = mMaximumPerSlip
End Property

'##ModelId=3E99A47902CE
Public Property Let MaximumPerSlip(ByVal Value As Currency)
    Let mMaximumPerSlip = Value
End Property

'##ModelId=3E99A47901DE
Public Property Get RequestSlipForTender(Index As Variant) As Boolean
    Let RequestSlipForTender = mRequestSlipForTender(Index)
End Property

'##ModelId=3E99A479009D
Public Property Let RequestSlipForTender(Index As Variant, ByVal Value As Boolean)
    Let mRequestSlipForTender(Index) = Value
End Property

'##ModelId=3E99A4780395
Public Property Get SequenceTendered(Index As Variant) As Long
    Let SequenceTendered = mSequenceTendered(Index)
End Property

'##ModelId=3E99A478025E
Public Property Let SequenceTendered(Index As Variant, ByVal Value As Long)
    Let mSequenceTendered(Index) = Value
End Property

'##ModelId=3E99A4780178
Public Property Get DepositInBank(Index As Variant) As Boolean
    Let DepositInBank = mDepositInBank(Index)
End Property

'##ModelId=3E99A4780042
Public Property Let DepositInBank(Index As Variant, ByVal Value As Boolean)
    Let mDepositInBank(Index) = Value
End Property

'##ModelId=3E99A4770361
Public Property Get NoReturns(Index As Variant) As Long
    Let NoReturns = mNoReturns(Index)
End Property

'##ModelId=3E99A4770253
Public Property Let NoReturns(Index As Variant, ByVal Value As Long)
    Let mNoReturns(Index) = Value
End Property

'##ModelId=3E99A477018B
Public Property Get TenderType(Index As Variant) As String
    Let TenderType = mTenderType(Index)
End Property

'##ModelId=3E99A4770068
Public Property Let TenderType(Index As Variant, ByVal Value As String)
    Let mTenderType(Index) = Value
End Property

'##ModelId=3E99A47603E2
Public Property Get SalesOfGiftVoucher() As Long
   Let SalesOfGiftVoucher = mSalesOfGiftVoucher
End Property

'##ModelId=3E99A4760324
Public Property Let SalesOfGiftVoucher(ByVal Value As Long)
    Let mSalesOfGiftVoucher = Value
End Property

'##ModelId=3E99A47602AC
Public Property Get NoSummaryNow() As Long
   Let NoSummaryNow = mNoSummaryNow
End Property

'##ModelId=3E99A47601F7
Public Property Let NoSummaryNow(ByVal Value As Long)
    Let mNoSummaryNow = Value
End Property

'##ModelId=3E99A476017F
Public Property Get NoSummaryKept() As Long
   Let NoSummaryKept = mNoSummaryKept
End Property

'##ModelId=3E99A47600D5
Public Property Let NoSummaryKept(ByVal Value As Long)
    Let mNoSummaryKept = Value
End Property

'##ModelId=3E99A4760067
Public Property Get DepositOutDeptNo() As String
   Let DepositOutDeptNo = mDepositOutDeptNo
End Property

'##ModelId=3E99A475039B
Public Property Let DepositOutDeptNo(ByVal Value As String)
    Let mDepositOutDeptNo = Value
End Property

'##ModelId=3E99A4750336
Public Property Get DepositInDeptNo() As String
   Let DepositInDeptNo = mDepositInDeptNo
End Property

'##ModelId=3E99A475028C
Public Property Let DepositInDeptNo(ByVal Value As String)
    Let mDepositInDeptNo = Value
End Property

'##ModelId=3E99A475021E
Public Property Get StoreFloatAmount() As Currency
   Let StoreFloatAmount = mStoreFloatAmount
End Property

'##ModelId=3E99A475017E
Public Property Let StoreFloatAmount(ByVal Value As Currency)
    Let mStoreFloatAmount = Value
End Property

'##ModelId=3E99A475011A
Public Property Get CustomerDeposits() As Currency
   Let CustomerDeposits = mCustomerDeposits
End Property

'##ModelId=3E99A4750079
Public Property Let CustomerDeposits(ByVal Value As Currency)
    Let mCustomerDeposits = Value
End Property

'##ModelId=3E99A4750015
Public Property Get GiftVoucherStock() As Currency
   Let GiftVoucherStock = mGiftVoucherStock
End Property

'##ModelId=3E99A4740367
Public Property Let GiftVoucherStock(ByVal Value As Currency)
    Let mGiftVoucherStock = Value
End Property

'##ModelId=3E99A4740303
Public Property Get StoreBankTotal() As Currency
   Let StoreBankTotal = mStoreBankTotal
End Property

'##ModelId=3E99A474026D
Public Property Let StoreBankTotal(ByVal Value As Currency)
    Let mStoreBankTotal = Value
End Property

'##ModelId=3E99A4740209
Public Property Get CurrentDate() As Date
   Let CurrentDate = mCurrentDate
End Property

'##ModelId=3E99A474017C
Public Property Let CurrentDate(ByVal Value As Date)
    Let mCurrentDate = Value
End Property

'##ModelId=3E99A47400E6
Public Property Get FileKey() As String
    Let FileKey = mFileKey
End Property


'##ModelId=3E99A474001E
Public Property Let FileKey(ByVal Value As String)
    Let mFileKey = Value
End Property

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_CASHBALANCEHDR * &H10000) + 1 To FID_CASHBALANCEHDR_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()
    
    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cCashBalanceHeader

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_CASHBALANCEHDR, FID_CASHBALANCEHDR_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_CASHBALANCEHDR_FileKey):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mFileKey
        Case (FID_CASHBALANCEHDR_CurrentDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mCurrentDate
        Case (FID_CASHBALANCEHDR_StoreBankTotal):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mStoreBankTotal
        Case (FID_CASHBALANCEHDR_GiftVoucherStock):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mGiftVoucherStock
        Case (FID_CASHBALANCEHDR_CustomerDeposits):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mCustomerDeposits
        Case (FID_CASHBALANCEHDR_StoreFloatAmount):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mStoreFloatAmount
        Case (FID_CASHBALANCEHDR_DepositInDeptNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDepositInDeptNo
        Case (FID_CASHBALANCEHDR_DepositOutDeptNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDepositOutDeptNo
        Case (FID_CASHBALANCEHDR_NoSummaryKept):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mNoSummaryKept
        Case (FID_CASHBALANCEHDR_NoSummaryNow):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mNoSummaryNow
        Case (FID_CASHBALANCEHDR_SalesOfGiftVoucher):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mSalesOfGiftVoucher
        Case (FID_CASHBALANCEHDR_TenderType):       Set GetField = SaveStringArray(mTenderType, m_oSession)
        Case (FID_CASHBALANCEHDR_NoReturns):        Set GetField = SaveLongArray(mNoReturns, m_oSession)
        Case (FID_CASHBALANCEHDR_DepositInBank):    Set GetField = SaveBooleanArray(mDepositInBank, m_oSession)
        Case (FID_CASHBALANCEHDR_SequenceTendered): Set GetField = SaveLongArray(mSequenceTendered, m_oSession)
        Case (FID_CASHBALANCEHDR_RequestSlipForTender): Set GetField = SaveBooleanArray(mRequestSlipForTender, m_oSession)
        Case (FID_CASHBALANCEHDR_MaximumPerSlip):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mMaximumPerSlip
        Case (FID_CASHBALANCEHDR_NoSummaryInFile):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mNoSummaryInFile
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cCashBalanceHeader
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_CASHBALANCEHDR

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Cash Balance Header " & mCurrentDate

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_CASHBALANCEHDR_FileKey):              mFileKey = oField.ValueAsVariant
            Case (FID_CASHBALANCEHDR_CurrentDate):          mCurrentDate = oField.ValueAsVariant
            Case (FID_CASHBALANCEHDR_StoreBankTotal):       mStoreBankTotal = oField.ValueAsVariant
            Case (FID_CASHBALANCEHDR_GiftVoucherStock):     mGiftVoucherStock = oField.ValueAsVariant
            Case (FID_CASHBALANCEHDR_CustomerDeposits):     mCustomerDeposits = oField.ValueAsVariant
            Case (FID_CASHBALANCEHDR_StoreFloatAmount):     mStoreFloatAmount = oField.ValueAsVariant
            Case (FID_CASHBALANCEHDR_DepositInDeptNo):      mDepositInDeptNo = oField.ValueAsVariant
            Case (FID_CASHBALANCEHDR_DepositOutDeptNo):     mDepositOutDeptNo = oField.ValueAsVariant
            Case (FID_CASHBALANCEHDR_NoSummaryKept):        mNoSummaryKept = oField.ValueAsVariant
            Case (FID_CASHBALANCEHDR_NoSummaryNow):         mNoSummaryNow = oField.ValueAsVariant
            Case (FID_CASHBALANCEHDR_SalesOfGiftVoucher):   mSalesOfGiftVoucher = oField.ValueAsVariant
            Case (FID_CASHBALANCEHDR_TenderType):           Call LoadStringArray(mTenderType, oField, m_oSession)
            Case (FID_CASHBALANCEHDR_NoReturns):            Call LoadLongArray(mNoReturns, oField, m_oSession)
            Case (FID_CASHBALANCEHDR_DepositInBank):        Call LoadBooleanArray(mDepositInBank, oField, m_oSession)
            Case (FID_CASHBALANCEHDR_SequenceTendered):     Call LoadLongArray(mSequenceTendered, oField, m_oSession)
            Case (FID_CASHBALANCEHDR_RequestSlipForTender): Call LoadBooleanArray(mRequestSlipForTender, oField, m_oSession)
            Case (FID_CASHBALANCEHDR_MaximumPerSlip):       mMaximumPerSlip = oField.ValueAsVariant
            Case (FID_CASHBALANCEHDR_NoSummaryInFile):      mNoSummaryInFile = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow
        
Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_CASHBALANCEHDR_END_OF_STATIC

End Function

Public Function Interface(Optional eInterfaceType As Long) As cCashBalanceHeader

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function


