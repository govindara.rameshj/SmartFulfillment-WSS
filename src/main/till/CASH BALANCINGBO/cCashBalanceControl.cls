VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cCashBalanceControl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3E94A4C40391"
'<CAMH>****************************************************************************************
'* Module : cCashBalanceControl
'* Date   : 13/04/03
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Cash Balance BO/cCashBalanceControl.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 7/05/03 9:39 $ $Revision: 3 $
'* Versions:
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "CashBalanceControl"

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=3E96738E013D
Private mControlDate As Date

'##ModelId=3E9673A00252
Private mDayBalanced As Boolean

'##ModelId=3E9673A9031D
Private mCommNumber As String

'##ModelId=3E9673B10089
Private mOtherIncome(10) As Double

'##ModelId=3E9673BB017E
Private mMiscOutgoings(10) As Double

'##ModelId=3E9673C902C9
Private mTotalDeposits As Double

'##ModelId=3E9673D002E7
Private mTotalVouchers As Double

'##ModelId=3E9673D70391
Private mTotalFloats As Double

'##ModelId=3E9673DD02BD
Private mBankingAmount(40) As Double

'##ModelId=3E9673E800B1
Private mBankSlipNumbers(40) As String

'##ModelId=3E9673FA016B
Private mComment1 As String

'##ModelId=3E96740202CB
Private mComment2 As String

'##ModelId=3E96740503B5
Private mComment3 As String

'##ModelId=3E99A4730153
Public Property Get Comment3() As String
   Let Comment3 = mComment3
End Property

'##ModelId=3E99A47300BD
Public Property Let Comment3(ByVal Value As String)
    Let mComment3 = Value
End Property

'##ModelId=3E99A4730063
Public Property Get Comment2() As String
   Let Comment2 = mComment2
End Property

'##ModelId=3E99A47203B4
Public Property Let Comment2(ByVal Value As String)
    Let mComment2 = Value
End Property

'##ModelId=3E99A472035A
Public Property Get Comment1() As String
   Let Comment1 = mComment1
End Property

'##ModelId=3E99A47202CE
Public Property Let Comment1(ByVal Value As String)
    Let mComment1 = Value
End Property

'##ModelId=3E99A472022E
Public Property Get BankSlipNumbers(Index As Variant) As String
    Let BankSlipNumbers = mBankSlipNumbers(Index)
End Property

'##ModelId=3E99A472015B
Public Property Let BankSlipNumbers(Index As Variant, ByVal Value As String)
    Let mBankSlipNumbers(Index) = Value
End Property

'##ModelId=3E99A47200A7
Public Property Get BankingAmount(Index As Variant) As Double
    Let BankingAmount = mBankingAmount(Index)
End Property

'##ModelId=3E99A47103BD
Public Property Let BankingAmount(Index As Variant, ByVal Value As Double)
    Let mBankingAmount(Index) = Value
End Property

'##ModelId=3E99A471036D
Public Property Get TotalFloats() As Double
   Let TotalFloats = mTotalFloats
End Property

'##ModelId=3E99A47102E1
Public Property Let TotalFloats(ByVal Value As Double)
    Let mTotalFloats = Value
End Property

'##ModelId=3E99A4710290
Public Property Get TotalVouchers() As Double
   Let TotalVouchers = mTotalVouchers
End Property

'##ModelId=3E99A471020E
Public Property Let TotalVouchers(ByVal Value As Double)
    Let mTotalVouchers = Value
End Property

'##ModelId=3E99A47101C8
Public Property Get TotalDeposits() As Double
   Let TotalDeposits = mTotalDeposits
End Property

'##ModelId=3E99A4710146
Public Property Let TotalDeposits(ByVal Value As Double)
    Let mTotalDeposits = Value
End Property

'##ModelId=3E99A47100BA
Public Property Get MiscOutgoings(Index As Variant) As Double
    Let MiscOutgoings = mMiscOutgoings(Index)
End Property

'##ModelId=3E99A47003E4
Public Property Let MiscOutgoings(Index As Variant, ByVal Value As Double)
    Let mMiscOutgoings(Index) = Value
End Property

'##ModelId=3E99A4700361
Public Property Get OtherIncome(Index As Variant) As Double
    Let OtherIncome = mOtherIncome(Index)
End Property

'##ModelId=3E99A47002AD
Public Property Let OtherIncome(Index As Variant, ByVal Value As Double)
    Let mOtherIncome(Index) = Value
End Property

'##ModelId=3E99A4700267
Public Property Get CommNumber() As String
   Let CommNumber = mCommNumber
End Property

'##ModelId=3E99A47001EF
Public Property Let CommNumber(ByVal Value As String)
    Let mCommNumber = Value
End Property

'##ModelId=3E99A47001A9
Public Property Get DayBalanced() As Boolean
   Let DayBalanced = mDayBalanced
End Property

'##ModelId=3E99A470013B
Public Property Let DayBalanced(ByVal Value As Boolean)
    Let mDayBalanced = Value
End Property

'##ModelId=3E99A47000F4
Public Property Get ControlDate() As Date
   Let ControlDate = mControlDate
End Property


'##ModelId=3E99A4700068
Public Property Let ControlDate(ByVal Value As Date)
    Let mControlDate = Value
End Property

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_CASHBALANCECONTROL * &H10000) + 1 To FID_CASHBALANCECONTROL_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter


'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()
    
    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cCashBalanceControl

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_CASHBALANCECONTROL, FID_CASHBALANCECONTROL_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_CASHBALANCECONTROL_ControlDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mControlDate
        Case (FID_CASHBALANCECONTROL_DayBalanced):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mDayBalanced
        Case (FID_CASHBALANCECONTROL_CommNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCommNumber
        Case (FID_CASHBALANCECONTROL_OtherIncome): Set GetField = SaveDoubleArray(mOtherIncome, m_oSession)
        Case (FID_CASHBALANCECONTROL_MiscOutgoings): Set GetField = SaveDoubleArray(mMiscOutgoings, m_oSession)
        Case (FID_CASHBALANCECONTROL_TotalDeposits):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mTotalDeposits
        Case (FID_CASHBALANCECONTROL_TotalVouchers):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mTotalVouchers
        Case (FID_CASHBALANCECONTROL_TotalFloats):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mTotalFloats
        Case (FID_CASHBALANCECONTROL_BankingAmount): Set GetField = SaveDoubleArray(mBankingAmount, m_oSession)
        Case (FID_CASHBALANCECONTROL_BankSlipNumbers): Set GetField = SaveStringArray(mBankSlipNumbers, m_oSession)
        Case (FID_CASHBALANCECONTROL_Comment1):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mComment1
        Case (FID_CASHBALANCECONTROL_Comment2):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mComment2
        Case (FID_CASHBALANCECONTROL_Comment3):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mComment3
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cCashBalanceControl
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_CASHBALANCECONTROL

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Cash Balance Control " & mControlDate

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_CASHBALANCECONTROL_ControlDate):     mControlDate = oField.ValueAsVariant
            Case (FID_CASHBALANCECONTROL_DayBalanced):     mDayBalanced = oField.ValueAsVariant
            Case (FID_CASHBALANCECONTROL_CommNumber):      mCommNumber = oField.ValueAsVariant
            Case (FID_CASHBALANCECONTROL_OtherIncome):     Call LoadDoubleArray(mOtherIncome, oField, m_oSession)
            Case (FID_CASHBALANCECONTROL_MiscOutgoings):   Call LoadDoubleArray(mMiscOutgoings, oField, m_oSession)
            Case (FID_CASHBALANCECONTROL_TotalDeposits):   mTotalDeposits = oField.ValueAsVariant
            Case (FID_CASHBALANCECONTROL_TotalVouchers):   mTotalVouchers = oField.ValueAsVariant
            Case (FID_CASHBALANCECONTROL_TotalFloats):     mTotalFloats = oField.ValueAsVariant
            Case (FID_CASHBALANCECONTROL_BankingAmount):   Call LoadDoubleArray(mBankingAmount, oField, m_oSession)
            Case (FID_CASHBALANCECONTROL_BankSlipNumbers): Call LoadStringArray(mBankSlipNumbers, oField, m_oSession)
            Case (FID_CASHBALANCECONTROL_Comment1):        mComment1 = oField.ValueAsVariant
            Case (FID_CASHBALANCECONTROL_Comment2):        mComment2 = oField.ValueAsVariant
            Case (FID_CASHBALANCECONTROL_Comment3):        mComment3 = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_CASHBALANCECONTROL_END_OF_STATIC

End Function

Public Function Interface(Optional eInterfaceType As Long) As cCashBalanceControl

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function


