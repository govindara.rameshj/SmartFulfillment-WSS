VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "fpSPR70.OCX"
Object = "{0F4DB82B-A487-11D6-B06E-0020AFC9A0C8}#1.0#0"; "CTSProgBar.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{905F077A-73D3-48CB-BCBB-2EF90EBA28A1}#1.0#0"; "EditDateCtl.ocx"
Begin VB.Form frmSOViewer 
   BackColor       =   &H00E0E0E0&
   Caption         =   "Customer Sales Order Viewer"
   ClientHeight    =   6870
   ClientLeft      =   330
   ClientTop       =   1620
   ClientWidth     =   11220
   Icon            =   "frmSOViewer.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   6870
   ScaleWidth      =   11220
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdShowDetails 
      Caption         =   "F4-Show Details"
      Height          =   405
      Left            =   6480
      TabIndex        =   24
      ToolTipText     =   "Display details of all orders"
      Top             =   5280
      Visible         =   0   'False
      Width           =   1200
   End
   Begin VB.CommandButton cmdDetails 
      Caption         =   "F11-Enquiry"
      Height          =   375
      Left            =   4320
      TabIndex        =   23
      ToolTipText     =   "Details of current order"
      Top             =   5280
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Frame frCriteria 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Selection criteria"
      Height          =   1095
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   11655
      Begin VB.TextBox txtOrderNo 
         Height          =   285
         Left            =   7920
         MaxLength       =   8
         TabIndex        =   9
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cmdReset 
         Caption         =   "F3-Reset"
         Height          =   375
         Left            =   10440
         TabIndex        =   15
         Top             =   660
         Width           =   1095
      End
      Begin VB.TextBox txtPCode 
         Height          =   285
         Left            =   5640
         MaxLength       =   8
         TabIndex        =   13
         Top             =   600
         Width           =   1215
      End
      Begin VB.ComboBox cmbStatus 
         Height          =   315
         ItemData        =   "frmSOViewer.frx":058A
         Left            =   5640
         List            =   "frmSOViewer.frx":059A
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   240
         Width           =   1215
      End
      Begin VB.TextBox txtName 
         Height          =   285
         Left            =   960
         MaxLength       =   30
         TabIndex        =   11
         ToolTipText     =   "Enter name  -  or part name e.g. %Smith%"
         Top             =   600
         Width           =   2055
      End
      Begin VB.ComboBox cmbOrdDateCrit 
         Height          =   315
         ItemData        =   "frmSOViewer.frx":05C6
         Left            =   960
         List            =   "frmSOViewer.frx":05D9
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cmdApply 
         Caption         =   "F7-Search"
         Height          =   375
         Left            =   10440
         TabIndex        =   14
         Top             =   180
         Width           =   1095
      End
      Begin ucEditDate.ucDateText dtxtOrdEndDate 
         Height          =   285
         Left            =   3360
         TabIndex        =   5
         Top             =   240
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   503
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DateFormat      =   "DD/MM/YY"
         Text            =   ""
      End
      Begin ucEditDate.ucDateText dtxtOrdStartDate 
         Height          =   285
         Left            =   1920
         TabIndex        =   3
         Top             =   240
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   503
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DateFormat      =   "DD/MM/YY"
         Text            =   ""
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Order No"
         Height          =   255
         Left            =   7200
         TabIndex        =   8
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "Postcode"
         Height          =   255
         Left            =   4920
         TabIndex        =   12
         Top             =   600
         Width           =   735
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "Name"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   600
         Width           =   495
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Status"
         Height          =   255
         Left            =   4920
         TabIndex        =   6
         Top             =   240
         Width           =   615
      End
      Begin VB.Label lblTo 
         BackStyle       =   0  'Transparent
         Caption         =   "to"
         Height          =   255
         Left            =   3160
         TabIndex        =   4
         Top             =   290
         Width           =   255
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Order Date"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   285
         Width           =   780
      End
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10-Exit"
      Height          =   375
      Left            =   120
      TabIndex        =   19
      Top             =   5280
      Width           =   975
   End
   Begin CTSProgBar.ucpbProgressBar ucProgress 
      Height          =   1815
      Left            =   1200
      TabIndex        =   17
      Top             =   2280
      Width           =   5055
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      FillColor       =   16744703
      Object.Width           =   5055
      Object.Height          =   1815
   End
   Begin FPSpreadADO.fpSpread sprdEdit 
      Height          =   3855
      Left            =   120
      TabIndex        =   16
      Top             =   1320
      Width           =   10335
      _Version        =   393216
      _ExtentX        =   18230
      _ExtentY        =   6800
      _StockProps     =   64
      ColsFrozen      =   2
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   39
      MaxRows         =   1
      RowHeaderDisplay=   0
      SpreadDesigner  =   "frmSOViewer.frx":05F3
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   18
      Top             =   6495
      Width           =   11220
      _ExtentX        =   19791
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmSOViewer.frx":3005
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11986
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "10:48"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F9-Print"
      Height          =   375
      Left            =   9480
      TabIndex        =   20
      Top             =   5280
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblNoLines 
      Alignment       =   1  'Right Justify
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   2400
      TabIndex        =   22
      Top             =   5280
      Width           =   975
   End
   Begin VB.Label lblNoLineslbl 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "No Matches"
      Height          =   255
      Left            =   1320
      TabIndex        =   21
      Top             =   5280
      Width           =   975
   End
End
Attribute VB_Name = "frmSOViewer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module :
'* Date   : 19/09/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Sales Order Viewer/frmSOViewer.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Damians $ $Date: 7/06/04 10:18 $ $Revision: 17 $
'* Versions:
'* 19/09/02    mauricem
'*             Header added.
'* 16/06/03    KeithB
'*         (A) Remove Quote Date - cmbQtDateCrit & dtxtQtStartDate & dtxtQtEndDate
'*         (B) Remove 'Not Printed' selection - chkNotPrinted
'*         (C) cmbOrdDate - replace >= with 'After' etc
'*         (D) Add frmSoDetails & routines to load sprdDetails on that form
'* 02/07/03    Add selection of 'Current' using cmbStatus  (STATUS_OUTSTANDING)
'*             - this assumes outstanding to mean :-
'*                      -   the complete  flag is set to false
'*                  and -   the cancelled flag is set to false
'*
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*      TS  Start Date followed by one of the following =  >  <
'*                           e.g. (/P='TS=20030326,  or  (/P>'TS>20030429,
'*      TD  End date         e.g. TD20030329
'*      DC  Destination Code DCP Printer or DCS Preview
'**********************************************************************************************
'*
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME                As String = "prjSalesOrderViewer"
Const BORDER_WIDTH               As Long = 60

Private Const CRIT_EQUALS        As Long = 0
Private Const CRIT_GREATERTHAN   As Long = 1
Private Const CRIT_LESSTHAN      As Long = 2
Private Const CRIT_ALL           As Long = 3
Private Const CRIT_FROM          As Long = 4
Private Const CRIT_WEEKENDING    As Long = 5

Private Const STATUS_ALL         As Long = 0
Private Const STATUS_EXPIRED     As Long = 1
Private Const STATUS_COMPLETE    As Long = 2
Private Const STATUS_CANCELLED   As Long = 3
Private Const STATUS_PROCESSED   As Long = 4
Private Const STATUS_OUTSTANDING     As Long = 5

Dim mstrDestCode                 As String 'hold passed in reports final destination code
Dim mblnDateFromParam            As Boolean
Dim mlngPayAmtDecPlaces          As Long
Dim mstrValueFormat              As String

Dim mstrPrintStore As String

Dim mlngAllDates    As Long
Dim mlngAllStatus   As Long

Private Const F4_SHOW_DETAILS    As String = "F4-Details for All Orders"
Private Const F4_HIDE_DETAILS    As String = "F4-Hide Details"

'
'Private Sub cmbQtDateCrit_Click()
'
'    Select Case (cmbQtDateCrit.ItemData(cmbQtDateCrit.ListIndex))
'        Case (CRIT_LESSTHAN), (CRIT_GREATERTHAN), (CRIT_EQUALS):
'                    dtxtQtStartDate.Enabled = True
'                    dtxtQtEndDate.Enabled = False
'                    dtxtQtStartDate.SetFocus
'        Case (CRIT_ALL):
'                    dtxtQtStartDate.Enabled = False
'                    dtxtQtEndDate.Enabled = False
'        Case (CRIT_FROM):
'                    dtxtQtStartDate.Enabled = True
'                    dtxtQtEndDate.Enabled = True
'                    dtxtQtStartDate.SetFocus
'    End Select
'
'End Sub

Private Sub cmbOrdDateCrit_Click()

    On Error Resume Next
    
    Select Case (cmbOrdDateCrit.ItemData(cmbOrdDateCrit.ListIndex))
        Case CRIT_LESSTHAN, CRIT_GREATERTHAN, CRIT_EQUALS
            dtxtOrdStartDate.Enabled = True
            dtxtOrdStartDate.Visible = True
            lblTo.Visible = False
            dtxtOrdEndDate.Enabled = False
            dtxtOrdEndDate.Visible = False
        Case CRIT_ALL
            dtxtOrdStartDate.Enabled = False
            dtxtOrdStartDate.Visible = False
            lblTo.Visible = False
            dtxtOrdEndDate.Enabled = False
            dtxtOrdEndDate.Visible = False
        Case CRIT_FROM
            dtxtOrdStartDate.Enabled = True
            dtxtOrdStartDate.Visible = True
            lblTo.Visible = True
            dtxtOrdEndDate.Enabled = True
            dtxtOrdEndDate.Visible = True
    End Select

End Sub

Private Sub cmbOrdDateCrit_GotFocus()
    
    SendKeys (KEY_DROPDOWN)

End Sub

Private Sub cmbOrdDateCrit_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        Call SendKeys(vbTab)
        KeyAscii = 0
    End If
    If KeyAscii = vbKeyEscape Then
        Call SendKeys("+{tab}")
        KeyAscii = 0
    End If
    
End Sub

Private Sub cmbStatus_GotFocus()

    SendKeys (KEY_DROPDOWN)
    
End Sub

Private Sub cmbStatus_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        Call SendKeys("{tab}")
        KeyAscii = 0
    End If
    If KeyAscii = vbKeyEscape Then
        SendKeys ("+{tab}")
        KeyAscii = 0
    End If
    
End Sub

Private Sub cmdDetails_Click()

' This is F11 - Enquiry (One Order)
' =========== - ======= ===========

Dim lngSelectedRow As Long
Dim lngLastRow     As Long 'hold variable - not used but required by ByRef

    lngSelectedRow = sprdEdit.ActiveRow
    
    If lngSelectedRow = -1 Then
        Call MsgBox("Please select an order by clicking on the grid", vbInformation, "Unable to display details")
        Exit Sub
    End If
    
    sprdEdit.Row = lngSelectedRow
    sprdEdit.Col = COL_SO_SHOW
    
    ' the user may have clicked in the middle of an expanded order
    If sprdEdit.TypeButtonText = "" Then
        Do
            lngSelectedRow = lngSelectedRow - 1
            sprdEdit.Row = lngSelectedRow
        Loop Until sprdEdit.TypeButtonText <> "" Or lngSelectedRow = 1
    End If
    
    ' we are on a row that says 'Shrink' or 'Retrieve'
    If sprdEdit.TypeButtonText = BUTTONTXT_RETRIEVE Then
        ' if it said retrieve then get it !
        Call sprdEdit_ButtonClicked(sprdEdit.Col, sprdEdit.Row, 0)
    End If
    
    ' load frmSoDetails
    ' then initialise it & copy headings from frmSoViewer
    Call COLoadAndPrepareDetailsForm("Enquiry", mlngHideSdColumns(), sprdEdit, ucProgress)
    
    frmSoDetails.mblnShowFileCopyPrint = True
    
    Call frmSoDetails.DisplayOrderDetails(lngSelectedRow, sprdEdit)
            
    frmSoDetails.mstrPrintHeader = GetPrintHeader
    
    frmSoDetails.Show vbModal
    
    On Error Resume Next
    
    ucProgress.Visible = False
    
    Unload frmSoDetails
    Call HideLineItems(sprdEdit)
    
    sprdEdit.SetFocus
    
End Sub

Private Sub cmdReset_Click()

    dtxtOrdStartDate.Text = "--/--/--"
    dtxtOrdEndDate.Text = "--/--/--"
    txtName.Text = ""
    txtPCode.Text = ""
    txtOrderNo.Text = ""
    lblNoLines.Caption = ""
    cmdDetails.Visible = False
    cmdShowDetails.Visible = False
    cmbOrdDateCrit.ListIndex = mlngAllDates
    DoEvents
    cmbStatus.ListIndex = mlngAllStatus
    sprdEdit.MaxRows = 0
    sprdEdit.LeftCol = 1
    cmdPrint.Visible = False
    Call cmbOrdDateCrit.SetFocus

End Sub

Private Sub cmdShowDetails_Click()

' This is F4 - Enquiry (All Orders)
' ========== - ======= ============

Dim lngSelectedRow         As Long
Dim lngLastLineTransferred As Long

    Screen.MousePointer = vbHourglass
    cmdShowDetails.Enabled = False
    sprdEdit.ReDraw = False
    
    ucProgress.Value = 0
    If sprdEdit.MaxRows > 0 Then ucProgress.Max = sprdEdit.MaxRows
    ucProgress.Visible = True
    Call COLoadAndPrepareDetailsForm("Enquiry", mlngHideSdColumns(), sprdEdit, ucProgress)
    
    ' show details for all orders
    If sprdEdit.MaxRows > 0 Then
        ucProgress.Caption1 = "Retrieving Lines for Orders."
        lngSelectedRow = 0
        
        Do
            lngSelectedRow = lngSelectedRow + 1
            
            sprdEdit.Row = lngSelectedRow
            sprdEdit.Col = COL_SO_SHOW
            ucProgress.Value = lngSelectedRow
            
            If sprdEdit.TypeButtonText = BUTTONTXT_RETRIEVE Then
                sprdEdit.Col = COL_SO_ONO
                Call DisplayOrderLines(sprdEdit, sprdEdit.Text, Nothing, False, False)
            End If
        
            sprdEdit.Row = lngSelectedRow
                        
            ' show details is currently selected
            ' ==================================
            Call frmSoDetails.DisplayOrderDetails(lngSelectedRow, sprdEdit)
            Call HideLineItems(sprdEdit)
            
        Loop Until lngSelectedRow >= sprdEdit.MaxRows
        
        Call frmSoDetails.DisplayDetailTotal
    End If
    
    On Error Resume Next
    sprdEdit.ReDraw = True
    
    ucProgress.Visible = False
    Screen.MousePointer = vbDefault
    cmdShowDetails.Enabled = True
    frmSoDetails.mstrPrintHeader = GetPrintHeader
    
    frmSoDetails.Show vbModal
    
    sprdEdit.SetFocus

End Sub

'Private Sub dtxtQtEndDate_GotFocus()
'
'    dtxtQtEndDate.BackColor = RGBEdit_Colour
'
'End Sub
'
'Private Sub dtxtQtEndDate_LostFocus()
'
'    dtxtQtEndDate.BackColor = lblNoLines.BackColor
'
'End Sub

'Private Sub dtxtQtStartDate_GotFocus()
'
'    dtxtQtStartDate.BackColor = RGBEdit_Colour
'
'End Sub

'Private Sub dtxtQtStartDate_LostFocus()
'
'    dtxtQtStartDate.BackColor = lblNoLines.BackColor
'
'End Sub

Private Sub dtxtOrdEndDate_GotFocus()

    dtxtOrdEndDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtOrdEndDate_KeyPress(KeyAscii As Integer)
  
    If KeyAscii = vbKeyReturn Then
        Call SendKeys(vbTab)
        KeyAscii = 0
    End If
    If KeyAscii = vbKeyEscape Then
        Call SendKeys("+{tab}")
        KeyAscii = 0
    End If
    
End Sub

Private Sub dtxtOrdEndDate_LostFocus()
    
    dtxtOrdEndDate.BackColor = lblNoLines.BackColor

End Sub

Private Sub dtxtOrdStartDate_GotFocus()
    
    dtxtOrdStartDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtOrdStartDate_KeyPress(KeyAscii As Integer)
  
    If KeyAscii = vbKeyReturn Then
        Call SendKeys(vbTab)
        KeyAscii = 0
    End If
    If KeyAscii = vbKeyEscape Then
        Call SendKeys("+{tab}")
        KeyAscii = 0
    End If
    
End Sub

Private Sub dtxtOrdStartDate_LostFocus()
    
    dtxtOrdStartDate.BackColor = lblNoLines.BackColor

End Sub

Private Sub sprdEdit_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
        
    Select Case Col
        Case COL_SO_SHOW
            sprdEdit.Row = Row
            sprdEdit.Col = Col
            
            Select Case (sprdEdit.TypeButtonText)
                Case BUTTONTXT_RETRIEVE ' Retrieve Order Lines
                    sprdEdit.Col = COL_SO_ONO
                    Call DisplayOrderLines(sprdEdit, sprdEdit.Text, ucProgress)
                    
                    ' hide the order no
                    sprdEdit.Col = COL_SL_ONO
                    sprdEdit.ColHidden = True
                    
                Case BUTTONTXT_SHRINK
                    sprdEdit.Col = COL_SO_ONO
                    Call RemoveLineItems(sprdEdit.Text)
            End Select
    End Select
    
End Sub

Private Sub sprdEdit_Click(ByVal Col As Long, ByVal Row As Long)
    
    On Error GoTo sprdEdit_Click_Err
    sprdEdit.Row = Row
    sprdEdit.Col = COL_SO_ONO
    cmdDetails.ToolTipText = "Display details for order no " & sprdEdit.Text
    Exit Sub
    
sprdEdit_Click_Err:
    cmdDetails.ToolTipText = "Display details"
End Sub

Private Sub txtName_GotFocus()

    txtName.BackColor = RGBEdit_Colour

End Sub

Private Sub txtName_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        Call SendKeys(vbTab)
        KeyAscii = 0
    End If
    If KeyAscii = vbKeyEscape Then
        Call SendKeys("+{tab}")
        KeyAscii = 0
    End If
 
End Sub

Private Sub txtName_LostFocus()
    
    txtName.BackColor = lblNoLines.BackColor

End Sub

Private Sub txtOrderNo_GotFocus()

    txtOrderNo.BackColor = RGBEdit_Colour

End Sub

Private Sub txtOrderNo_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        Call SendKeys(vbTab)
        KeyAscii = 0
    End If
    If KeyAscii = vbKeyEscape Then
        Call SendKeys("+{tab}")
        KeyAscii = 0
    End If
 
End Sub

Private Sub txtOrderNo_LostFocus()
    
    txtOrderNo.BackColor = lblNoLines.BackColor

End Sub

Private Sub txtPCode_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        Call SendKeys("{tab}")
        KeyAscii = 0
    End If
    If KeyAscii = vbKeyEscape Then
        Call SendKeys("+{tab}")
        KeyAscii = 0
    End If
    
End Sub

Private Sub txtPCode_GotFocus()

    txtPCode.BackColor = RGBEdit_Colour

End Sub

Private Sub txtPCode_LostFocus()
    
    txtPCode.Text = UCase(txtPCode.Text)
    txtPCode.BackColor = lblNoLines.BackColor

End Sub
Private Sub sprdEdit_GotFocus()
    
    sprdEdit.SelBackColor = RGB_BLUE
    sprdEdit.SelForeColor = RGB_WHITE

End Sub

Private Sub sprdEdit_LostFocus()
    
    sprdEdit.SelBackColor = -1
    sprdEdit.SelForeColor = -1
    sbStatus.Panels(PANEL_INFO).Text = ""

End Sub

Private Sub cmdApply_Click()

Dim oEntry          As Object
Dim colEntries      As Collection
Dim lngListIndex    As Long

On Error GoTo DisplayError
    
    Screen.MousePointer = vbHourglass
    cmdDetails.Visible = False
    ucProgress.Visible = True
    ucProgress.Caption1 = "Loading List from Database"
    lblNoLines.Caption = "Searching"
    DoEvents
    
    'Create single element that List must be retrieved for
    Set oEntry = goDatabase.CreateBusinessObject(CLASSID_SOHEADER)
    
'    'Set Quote Date criteria if selected by user
'    Select Case (cmbQtDateCrit.ItemData(cmbQtDateCrit.ListIndex))
'        Case (CRIT_LESSTHAN):    Call oEntry.IBo_AddLoadFilter(CMP_LESSEQUALTHAN, FID_SOHEADER_QuoteDate, GetDate(dtxtQtStartDate.Text))
'        Case (CRIT_GREATERTHAN): Call oEntry.IBo_AddLoadFilter(CMP_GREATEREQUALTHAN, FID_SOHEADER_QuoteDate, GetDate(dtxtQtStartDate.Text))
'        Case (CRIT_EQUALS):      Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_SOHEADER_QuoteDate, GetDate(dtxtQtStartDate.Text))
'        Case (CRIT_FROM):        Call oEntry.IBo_AddLoadFilter(CMP_GREATEREQUALTHAN, FID_SOHEADER_QuoteDate, GetDate(dtxtQtStartDate.Text))
'                                 Call oEntry.IBo_AddLoadFilter(CMP_LESSEQUALTHAN, FID_SOHEADER_QuoteDate, GetDate(dtxtQtEndDate.Text))
'    End Select

    'Set Order Date criteria
    '=== ===================
    lngListIndex = cmbOrdDateCrit.ListIndex
    Select Case (cmbOrdDateCrit.ItemData(cmbOrdDateCrit.ListIndex))
        Case Is = CRIT_LESSTHAN
            Call oEntry.IBo_AddLoadFilter(CMP_LESSEQUALTHAN, FID_SOHEADER_OrderDate, GetDate(dtxtOrdStartDate.Text))
        Case Is = CRIT_GREATERTHAN
            Call oEntry.IBo_AddLoadFilter(CMP_GREATEREQUALTHAN, FID_SOHEADER_OrderDate, GetDate(dtxtOrdStartDate.Text))
        Case Is = CRIT_EQUALS
            Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_SOHEADER_OrderDate, GetDate(dtxtOrdStartDate.Text))
        Case CRIT_FROM
            Call oEntry.IBo_AddLoadFilter(CMP_GREATEREQUALTHAN, FID_SOHEADER_OrderDate, GetDate(dtxtOrdStartDate.Text))
            Call oEntry.IBo_AddLoadFilter(CMP_LESSEQUALTHAN, FID_SOHEADER_OrderDate, GetDate(dtxtOrdEndDate.Text))
    End Select
    
    If txtOrderNo.Text <> "" Then
        If Len(txtOrderNo.Text) < 6 Then txtOrderNo.Text = Format(Val(txtOrderNo.Text), "000000")
        Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_SOHEADER_OrderNo, txtOrderNo.Text)
    End If
    
    If txtName.Text <> "" Then
        Call oEntry.IBo_AddLoadFilter(CMP_LIKE, FID_SOHEADER_Name, "%" & txtName.Text & "%")
    End If
    
    If txtPCode.Text <> "" Then
        txtPCode.Text = UCase(txtPCode.Text)
        Call oEntry.IBo_AddLoadFilter(CMP_LIKE, FID_SOHEADER_PostCode, txtPCode.Text & "%")
    End If
    
    'If chkNotPrinted.Value = 1 Then Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_SOHEADER_Printed, False)
    
    'Set Sales Order Status criteria if selected by user
    Select Case cmbStatus.ItemData(cmbStatus.ListIndex)
        Case STATUS_EXPIRED
            Call oEntry.IBo_AddLoadFilter(CMP_NOTEQUAL, FID_SOHEADER_QuoteExpiry, Null)
            Call oEntry.IBo_AddLoadFilter(CMP_LESSTHAN, FID_SOHEADER_QuoteExpiry, Now())
            Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_SOHEADER_OrderDate, Null)
        Case STATUS_COMPLETE
           Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_SOHEADER_Complete, True)
           'Added DMS Store Review 7.2
           Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_SOHEADER_Cancelled, False)
        Case STATUS_CANCELLED
           Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_SOHEADER_Cancelled, True)
        Case STATUS_PROCESSED
            Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_SOHEADER_OrderProcessed, True)
        Case STATUS_OUTSTANDING
            ' current if-
            '     the complete  flag is set to false
            ' and the cancelled flag is set to false
            Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_SOHEADER_Complete, False)
            Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_SOHEADER_Cancelled, False)
    End Select
    
    'Make call on entries to return collection of all entries
    Set colEntries = oEntry.IBo_LoadMatches
    
    sprdEdit.MaxRows = 0
    If cmdShowDetails.Caption <> F4_SHOW_DETAILS Then cmdShowDetails_Click
    
    On Error Resume Next
    If colEntries.Count > 0 Then
        Call DisplayOrders(colEntries, sprdEdit, ucProgress)
        sprdEdit.Row = 1
        sprdEdit.Col = COL_SO_ONO
        Call sprdEdit_Click(sprdEdit.Col, sprdEdit.Row)
        cmdDetails.Visible = True
        cmdShowDetails.Visible = True
        cmdPrint.Visible = True
    Else
        sprdEdit.MaxRows = 0
        cmdDetails.Visible = False
        cmdShowDetails.Visible = False
        cmdPrint.Visible = False
    End If
    
    sprdEdit.SortKey(1) = COL_SO_ONO
    sprdEdit.SortKeyOrder(1) = SortKeyOrderAscending
    
    Call sprdEdit.Sort(-1, -1, -1, -1, SortByRow)
    
    ' hide any columns that are not required
    lblNoLines.Caption = sprdEdit.MaxRows
    
    ucProgress.Visible = False
    sbStatus.Panels(PANEL_INFO).Text = ""
    Call sprdEdit.SetFocus
    Set oEntry = Nothing
    Screen.MousePointer = vbNormal
    
    cmbOrdDateCrit.ListIndex = lngListIndex
    
    Exit Sub
    
DisplayError:

    Set oEntry = Nothing
    Call Err.Report(MODULE_NAME, "cmdApply_click", 1, True, "Error displaying data", "Error occurred when display selected criteria")
    Call Err.Clear
    ucProgress.Visible = False
    sbStatus.Panels(PANEL_INFO).Text = ""
    lblNoLines.Caption = "Error(" & Err.Number & ")"
    Screen.MousePointer = vbNormal
    Exit Sub
    
Resume Next

End Sub

Private Sub cmdApply_GotFocus()

    cmdApply.FontBold = True

End Sub

Private Sub cmdApply_LostFocus()
    
    cmdApply.FontBold = False

End Sub

Private Sub cmdExit_Click()

    Unload Me

End Sub

Private Sub cmdPrint_Click()

    Call PrintList(sprdEdit, GetPrintHeader)

    Call cmdReset_Click
        
End Sub

Private Function GetPrintHeader() As String

Dim oPrintStore  As Object
Dim strHeader    As String

    If mstrPrintStore = "" Then
        Set oPrintStore = goSession.Database.CreateBusinessObject(CLASSID_STORE)
        Call oPrintStore.IBo_AddLoadFilter(CMP_EQUAL, FID_STORE_StoreNumber, goSession.CurrentEnterprise.IEnterprise_StoreNumber)
        Call oPrintStore.IBo_Load
        mstrPrintStore = "Store No: " & Str$(goSession.CurrentEnterprise.IEnterprise_StoreNumber) & "  " & oPrintStore.AddressLine1
    End If
   
    strHeader = mstrPrintStore & "  -  " & "Customer Order Summary Report" & "/n/cStatus Report on "
    
    If txtOrderNo.Text <> "" Then strHeader = strHeader & " Order No : " & txtOrderNo.Text
    'Add supplier details
    strHeader = strHeader & "/n/cFor status - '" & cmbStatus.Text & "'  "
        
    'Add date criteria
    If cmbOrdDateCrit.Enabled = True Then
        strHeader = strHeader & " Dated "
        Select Case (cmbOrdDateCrit.ItemData(cmbOrdDateCrit.ListIndex))
            Case (CRIT_ALL): strHeader = strHeader & ": ALL"
            Case (CRIT_FROM): strHeader = strHeader & "From " & dtxtOrdStartDate.Text & " to " & dtxtOrdEndDate.Text
            Case (CRIT_EQUALS): strHeader = strHeader & ": " & dtxtOrdStartDate.Text
            Case Else: strHeader = strHeader & cmbOrdDateCrit.Text & ": " & dtxtOrdStartDate.Text
        End Select
    Else
    End If
    
    Set oPrintStore = Nothing
    
    GetPrintHeader = strHeader

End Function

Private Sub cmdPrint_GotFocus()
    
    cmdPrint.FontBold = True

End Sub

Private Sub cmdPrint_LostFocus()
    
    cmdPrint.FontBold = False

End Sub

Private Sub cmdExit_GotFocus()
    
    cmdExit.FontBold = True

End Sub

Private Sub cmdExit_LostFocus()
    
    cmdExit.FontBold = False

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case (Shift)
        Case (0):
            Select Case (KeyCode)
                Case Is = vbKeyF3 ' reset criteria
                    KeyCode = 0
                    Call cmdReset_Click
                Case Is = vbKeyF4 ' details
                    KeyCode = 0
                    If cmdShowDetails.Visible = True And cmdShowDetails.Enabled = True Then
                        Call cmdShowDetails_Click
                    End If
                Case Is = vbKeyF7 'if F7 then call search
                    KeyCode = 0
                    If cmdApply.Visible = True Then Call cmdApply_Click
                Case Is = vbKeyF9 'if F9 then call print
                    KeyCode = 0
                    If cmdPrint.Visible = True Then Call cmdPrint_Click
                Case Is = vbKeyF10 'if F10 then exit
                    KeyCode = 0
                    Call cmdExit_Click
                Case Is = vbKeyF11 ' if F11 then Details
                    KeyCode = 0
                    Call cmdDetails_Click
            End Select 'Key pressed with no Shift/Alt combination
    End Select

End Sub
Private Sub Form_Load()

Dim lEntryNo    As Long
Dim lngItem     As Long

    Me.Show
    
    sbStatus.Panels(PANEL_INFO).Text = "Accessing session"
    ' Set up the object and field that we wish to select on
    Set goRoot = GetRoot
    'Display initial values in Status Bar
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    ucProgress.FillColor = Me.BackColor
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    
    Call InitialiseStatusBar(sbStatus)
    
    sbStatus.Panels(PANEL_INFO).Text = "Loading Editor"
    ucProgress.Visible = True
    ucProgress.Caption1 = "Loading List from Database"
    DoEvents
    
    sbStatus.Panels(PANEL_INFO).Text = "Populating grid"
    DoEvents
    ' get display format for pounds & pence from param file
    mlngPayAmtDecPlaces = goSession.GetParameter(PRM_VALUE_DEC_PLACES)
    mstrValueFormat = Replace(Space(mlngPayAmtDecPlaces), " ", "0")
    If mstrValueFormat <> "" Then
        mstrValueFormat = "0." & mstrValueFormat
    End If

    
    'Only required if Date selection is used
    mstrDateFmt = UCase$(goSession.GetParameter(125))
    'dtxtQtStartDate.DateFormat = mstrDateFmt
    'dtxtQtStartDate.Text = Format(Now(), mstrDateFmt)
    'dtxtQtEndDate.DateFormat = mstrDateFmt
    'dtxtQtEndDate.Text = Format(Now(), mstrDateFmt)
    dtxtOrdStartDate.DateFormat = mstrDateFmt
    dtxtOrdStartDate.Text = Format(Now(), mstrDateFmt)
    dtxtOrdEndDate.DateFormat = mstrDateFmt
    dtxtOrdEndDate.Text = Format(Now(), mstrDateFmt)
    
    ' Order Date combo box
    cmbOrdDateCrit.Clear
    Call cmbOrdDateCrit.AddItem("After")
    cmbOrdDateCrit.ItemData(0) = CRIT_GREATERTHAN
    Call cmbOrdDateCrit.AddItem("Before")
    cmbOrdDateCrit.ItemData(1) = CRIT_LESSTHAN
    Call cmbOrdDateCrit.AddItem("Dated")
    cmbOrdDateCrit.ItemData(2) = CRIT_EQUALS
    Call cmbOrdDateCrit.AddItem("All")
    cmbOrdDateCrit.ItemData(3) = CRIT_ALL
    Call cmbOrdDateCrit.AddItem("From")
    cmbOrdDateCrit.ItemData(4) = CRIT_FROM
    'Call cmbOrdDateCrit.AddItem("Week Ending")
    'cmbOrdDateCrit.ItemData(5) = CRIT_WEEKENDING
    
    For lngItem = 0 To cmbOrdDateCrit.ListCount Step 1
        If cmbOrdDateCrit.ItemData(lngItem) = CRIT_ALL Then
            cmbOrdDateCrit.ListIndex = lngItem
            mlngAllDates = lngItem
            Exit For
        End If
    Next lngItem
'    For lngItem = 0 To cmbQtDateCrit.ListCount Step 1
'        If cmbQtDateCrit.ItemData(lngItem) = CRIT_ALL Then
'            cmbQtDateCrit.ListIndex = lngItem
'            Exit For
'        End If
'    Next lngItem


    For lngItem = 0 To cmbStatus.ListCount Step 1
        If cmbStatus.ItemData(lngItem) = STATUS_ALL Then
            cmbStatus.ListIndex = lngItem
            mlngAllStatus = lngItem
            Exit For
        End If
    Next lngItem
    
    
    Call HideSOColumns(sprdEdit)
    
    ' These are the columns we want to hide in extended display
    mlngHideSdColumns(1) = COL_SO_CUSTNO
    mlngHideSdColumns(2) = COL_SO_USERID
    mlngHideSdColumns(3) = COL_SO_TILLID
    mlngHideSdColumns(4) = COL_SO_PCODE
    mlngHideSdColumns(5) = COL_SO_FAXNO
    mlngHideSdColumns(6) = COL_SO_DEPNO
    mlngHideSdColumns(7) = COL_SO_PROC
    mlngHideSdColumns(8) = COL_SO_PRINTED
    mlngHideSdColumns(9) = COL_SO_STATUS
    mlngHideSdColumns(10) = COL_SO_QTEDATE
    mlngHideSdColumns(11) = COL_SO_QTEEXP
    mlngHideSdColumns(12) = COL_SO_NODEL
    mlngHideSdColumns(13) = COL_SO_CANC_DATE
    mlngHideSdColumns(14) = COL_SO_CANC_BY
    mlngHideSdColumns(15) = COL_SO_ADD2
    mlngHideSdColumns(16) = COL_SO_ADD3
    mlngHideSdColumns(17) = COL_SO_2NDTELNO
    
    mlngHideSummaryColumns(1) = COL_SO_CUSTNO
    mlngHideSummaryColumns(2) = COL_SO_USERID
    mlngHideSummaryColumns(3) = COL_SO_TILLID
    mlngHideSummaryColumns(4) = COL_SO_ADD1
    mlngHideSummaryColumns(5) = COL_SO_PCODE
    mlngHideSummaryColumns(6) = COL_SO_FAXNO
    mlngHideSummaryColumns(7) = COL_SO_DEPNO
    mlngHideSummaryColumns(8) = COL_SO_PROC
    mlngHideSummaryColumns(9) = COL_SO_PRINTED
    mlngHideSummaryColumns(10) = COL_SO_STATUS
    mlngHideSummaryColumns(11) = COL_SO_QTEDATE
    mlngHideSummaryColumns(12) = COL_SO_QTEEXP
    mlngHideSummaryColumns(13) = COL_SO_NODEL
    mlngHideSummaryColumns(14) = COL_SO_CANC_DATE
    mlngHideSummaryColumns(15) = COL_SO_CANC_BY
    mlngHideSummaryColumns(16) = COL_SO_ADD2
    mlngHideSummaryColumns(17) = COL_SO_ADD3
    mlngHideSummaryColumns(18) = COL_SO_2NDTELNO
    
    'Hide line detail columns
    sprdEdit.Col = COL_SL_LINENO
    sprdEdit.ColHidden = True
    sprdEdit.Col = COL_SL_ORDERQTY
    sprdEdit.ColHidden = True
    sprdEdit.Col = COL_SL_PARTCODE
    sprdEdit.ColHidden = True
    sprdEdit.Col = COL_SL_DESCRIPTION
    sprdEdit.ColHidden = True
    sprdEdit.Col = COL_SL_SIZE
    sprdEdit.ColHidden = True
    sprdEdit.Col = COL_SL_QUANTATHAND
    sprdEdit.ColHidden = True
    sprdEdit.Col = COL_SL_QUANTONORD
    sprdEdit.ColHidden = True
    sprdEdit.Col = COL_SL_SUPPNO
    sprdEdit.ColHidden = True
    sprdEdit.Col = COL_SL_CURRENT
    sprdEdit.ColHidden = True
    sprdEdit.Col = COL_SL_COMPLETE
    sprdEdit.ColHidden = True
    sprdEdit.Col = COL_SL_CANCELLED
    sprdEdit.ColHidden = True
    sprdEdit.Col = COL_SL_LINESTAT
    sprdEdit.ColHidden = True
    sprdEdit.Col = COL_SL_ONO
    sprdEdit.ColHidden = True
    sprdEdit.Col = COL_SL_POSTATUS
    sprdEdit.ColHidden = True
    
    cmdShowDetails.Caption = F4_SHOW_DETAILS
    
    ucProgress.Visible = False
    sbStatus.Panels(PANEL_INFO).Text = ""
        
    Call DecodeParameters(goSession.ProgramParams)
    
    If mstrDestCode = DEST_REPORT_CODE_PREVIEW Then Call cmdPrint_Click
    If mstrDestCode = DEST_REPORT_CODE_PRINTER Then Call cmdPrint_Click
   
    On Error Resume Next
    
    If (goSession.ProgramParams = "") Then Call cmdReset_Click
    
End Sub

Private Sub Form_Resize()

Dim lSpacing As Long

    If Me.WindowState = vbMinimized Then Exit Sub
    lSpacing = sprdEdit.Left
    
    'Check form is not below minimum width
    If Me.Width < cmdPrint.Width + cmdExit.Width + lblNoLines.Width + (lSpacing * 4) Then
        Me.Width = cmdPrint.Width + cmdExit.Width + lblNoLines.Width + (lSpacing * 4)
        Exit Sub
    End If
    'Check form is not below minimum height
    If Me.Height < sprdEdit.Top + cmdExit.Height * 3 + sbStatus.Height Then
        Me.Height = sprdEdit.Top + cmdExit.Height * 3 + sbStatus.Height
        Exit Sub
    End If
    
    'relocate Progress bar
    If (Me.Width - ucProgress.Width) > 0 Then
        ucProgress.Left = (Me.Width - ucProgress.Width) / 2
    Else
        ucProgress.Left = 0
    End If
    If (Me.Height - ucProgress.Height) > 0 Then
        ucProgress.Top = (Me.Height - ucProgress.Height) / 2
    Else
        ucProgress.Top = 0
    End If
    
    'start resizing
    sprdEdit.Width = Me.Width - sprdEdit.Left * 2 - BORDER_WIDTH * 2
    sprdEdit.Height = Me.Height - (cmdExit.Height + sprdEdit.Top + (lSpacing * 5) + (BORDER_WIDTH * 2) + sbStatus.Height)
    
    cmdExit.Top = sprdEdit.Top + sprdEdit.Height + lSpacing
    cmdPrint.Top = cmdExit.Top
    cmdDetails.Top = cmdExit.Top
    cmdShowDetails.Top = cmdExit.Top
    
    lblNoLineslbl.Top = cmdExit.Top + lSpacing / 2
    lblNoLines.Top = lblNoLineslbl.Top
    
    cmdPrint.Left = (sprdEdit.Width - cmdPrint.Width) + sprdEdit.Left

End Sub


Private Sub DecodeParameters(strCommand As String)

Const TRAN_START_DATE    As String = "TS"
Const TRAN_END_DATE      As String = "TD"
Const TRAN_ORDER_NO      As String = "ON"
Const TRAN_STATUS        As String = "OS"

Dim strTempDate  As String
Dim dteTempDate  As Date
Dim strSection   As String
Dim strSignFound As String
Dim vntSection   As Variant
Dim lngSectNo    As Long
Dim lngItem      As Long
Dim blnSetOpts   As Boolean

    vntSection = Split(strCommand, ",")
    
    blnSetOpts = False
    mblnDateFromParam = False
    
    For lngSectNo = 0 To UBound(vntSection) Step 1
        
        'Debug.Print lngSectNo & "-" & vntSection(lngSectNo)
        strSection = vntSection(lngSectNo)
        Select Case (Left$(strSection, 2))
            Case TRAN_START_DATE
                mblnDateFromParam = True
                If Len(strSection) > 5 Then dteTempDate = GetParamDate(Right$(strSection, 8))
                dtxtOrdStartDate.Text = Format(dteTempDate, mstrDateFmt)
                strTempDate = Mid$(strSection, 3) ' strip off the TS or the DB
                
                If Len(strTempDate) > 3 Then
                    strSignFound = Left$(strTempDate, Len(strTempDate) - 8)
                Else
                    strSignFound = strTempDate
                End If
                blnSetOpts = True
                
                cmbOrdDateCrit.ListIndex = SetDateCrit(strSignFound, cmbOrdDateCrit)
                
'            Case TRAN_WEEKENDING
'                mblnDateFromParam = True
'                dteTempDate = GetParamDate(Right$(strSection, 8))
'                dtxtOrdEndDate.Text = Format(dteTempDate, mstrDateFmt)
'                cmbOrdDateCrit.ListIndex = SetDateCrit(TRAN_WEEKENDING, cmbOrdDateCrit)
'                blnSetOpts = True
                        
            Case TRAN_END_DATE
                mblnDateFromParam = True
                dteTempDate = GetParamDate(Right$(strSection, 8))
                dtxtOrdEndDate.Text = Format(dteTempDate, mstrDateFmt)
                blnSetOpts = True
                
            Case TRAN_STATUS
                strSignFound = Mid$(strSection, 3)
                For lngItem = 0 To cmbStatus.ListCount - 1 Step 1
                    If Left(cmbStatus.List(lngItem), 1) = strSignFound Then
                        cmbStatus.ListIndex = lngItem
                        Exit For
                    End If
                Next lngItem
                blnSetOpts = True
            
            Case DEST_CODE
                strTempDate = Mid$(strSection, 3)
                mstrDestCode = strTempDate
                blnSetOpts = True
                
            Case TRAN_ORDER_NO
                txtOrderNo.Text = Mid$(strSection, 3)
                blnSetOpts = True
                
'            Case Is = SHOW_BUTTON
'                cmdShowCash.Visible = True
        End Select
    
    Next lngSectNo
    
    If blnSetOpts = True Then Call cmdApply_Click

End Sub
Private Function SetDateCrit(ByVal strSign As String, cmbDateRange As ComboBox) As Long

Dim intItemValue As Integer
Dim intItem      As Integer

    SetDateCrit = -1
    
    Select Case UCase(Trim(strSign))
    Case Is = ">"
      intItemValue = CRIT_GREATERTHAN
    Case Is = "<"
      intItemValue = CRIT_LESSTHAN
    Case Is = "="
      intItemValue = CRIT_EQUALS
    Case Is = "ALL" ' all
      intItemValue = CRIT_ALL
    Case Is = ">="
      intItemValue = CRIT_FROM
    Case Is = "WE"
      intItemValue = CRIT_WEEKENDING
    Case Else
      Call MsgBox("Invalid sign " & strSign & vbCrLf & _
                  "passed to date selection.", vbInformation, "Error decoding date parameter")
      Exit Function
    End Select
    
    ' look for the value intItemValue in cmbDateRange.ItemData(intItem)
    For intItem = 0 To cmbDateRange.ListCount - 1 Step 1
        If cmbDateRange.ItemData(intItem) = intItemValue Then
            ' found intItemValue set the pointer to the ListIndex
            SetDateCrit = intItem
            Exit For
        End If
    Next intItem
    
End Function

Private Sub RemoveLineItems(ByVal strOrderKey As String)

Dim lNoItems As Long
Dim lTopRow  As Long
Dim lLineNo  As Long

    lTopRow = sprdEdit.Row
    lNoItems = 0
    
    For lLineNo = lTopRow To sprdEdit.MaxRows Step 1
        sprdEdit.Row = lLineNo
        sprdEdit.Col = COL_SL_ONO
        If sprdEdit.Text <> strOrderKey Then
            Exit For
        Else
            lNoItems = lNoItems + 1
        End If
    Next lLineNo
    
    ' Remove any rows below the first one
    If lNoItems > 1 Then
        Call sprdEdit.DeleteRows(lTopRow + 1, lNoItems - 1)
        sprdEdit.MaxRows = sprdEdit.MaxRows - (lNoItems - 1)
    End If

    sprdEdit.BlockMode = True
    sprdEdit.Col = COL_SL_LINENO
    sprdEdit.Col2 = COL_SL_ONO
    sprdEdit.Row = lTopRow
    sprdEdit.Row2 = lTopRow
    sprdEdit.CellType = CellTypeStaticText
    sprdEdit.Text = ""
    sprdEdit.BackColor = RGB_GREY
    'sprdEdit.ForeColor = RGB_GREY
    sprdEdit.BlockMode = False

    sprdEdit.Row = lTopRow
    sprdEdit.Col = COL_SO_SHOW
    sprdEdit.TypeButtonText = BUTTONTXT_RETRIEVE

End Sub

