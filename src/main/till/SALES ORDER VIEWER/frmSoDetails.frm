VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Begin VB.Form frmSoDetails 
   Caption         =   "Customer Order Enquiry"
   ClientHeight    =   5130
   ClientLeft      =   1155
   ClientTop       =   2460
   ClientWidth     =   9735
   Icon            =   "frmSoDetails.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   5130
   ScaleWidth      =   9735
   StartUpPosition =   1  'CenterOwner
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdFileCopy 
      Caption         =   "Alt F9-File Copy"
      Height          =   375
      Left            =   7140
      TabIndex        =   2
      Top             =   4260
      Width           =   1335
   End
   Begin CTSProgBar.ucpbProgressBar ucProgress 
      Height          =   1815
      Left            =   2340
      TabIndex        =   5
      Top             =   1658
      Visible         =   0   'False
      Width           =   5055
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      FillColor       =   16744703
      Object.Width           =   5055
      Object.Height          =   1815
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F12-Close"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   4260
      Width           =   975
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F9-Print"
      Height          =   375
      Left            =   8640
      TabIndex        =   1
      Top             =   4260
      Width           =   975
   End
   Begin FPSpreadADO.fpSpread sprdDetails 
      Height          =   3975
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   9495
      _Version        =   458752
      _ExtentX        =   16748
      _ExtentY        =   7011
      _StockProps     =   64
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   5
      MaxRows         =   1
      RowHeaderDisplay=   0
      SpreadDesigner  =   "frmSoDetails.frx":058A
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   4
      Top             =   4755
      Width           =   9735
      _ExtentX        =   17171
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmSoDetails.frx":0839
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9340
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "13:11"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSoDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const BORDER_WIDTH              As Long = 60
Public mstrPrintHeader          As String
Public mblnShowFileCopyPrint    As Boolean

Private Sub cmdExit_Click()
    Unload Me

End Sub

Private Sub cmdFileCopy_Click()
Dim strEXEPath  As String
Dim lngRow      As Long

    Screen.MousePointer = vbHourglass

    For lngRow = 1 To sprdDetails.MaxRows
        sprdDetails.Col = COL_SO_ORDDATE
        sprdDetails.Row = lngRow
        
        If sprdDetails.Text <> "" Then
            sprdDetails.Col = COL_SO_ONO
            strEXEPath = App.Path & "\" & "CustOrderReprint.exe " & goSession.CreateCommandLine("PTIF" & sprdDetails.Text)
            Call ShellWait(strEXEPath, SW_MAX)
            
        End If
    Next
    
    Screen.MousePointer = vbDefault
       
End Sub

Private Sub cmdPrint_Click()
Dim strFooter As String

    Screen.MousePointer = vbHourglass
    cmdPrint.Enabled = False
    
    ' Call LockSoDetails(False)
    
    ' Can't get the grid to disappear - the following do NOT work!
    'sprdDetails.GridShowHoriz = False
    'sprdDetails.GridShowVert = False
    sprdDetails.GridColor = vbWhite
    sprdDetails.PrintGrid = False
    ' sprdDetails.BackColorStyle = BackColorStyleOverGrid ' BackColorStyleUnderGrid
    
    ' use PrintColor to ensure that horizontal lines (inverted color) are shown
    sprdDetails.PrintColor = True
    ' ensure the background color of the column and row headers is white
    sprdDetails.ShadowColor = vbWhite
    sprdDetails.PrintSmartPrint = True

    ' Set up Footer
    strFooter = GetFooter
    
    ' /l is left align
    sprdDetails.PrintHeader = "/c/fb1" & mstrPrintHeader
    sprdDetails.PrintFooter = GetFooter
    sprdDetails.PrintJobName = "Customer Order Enquiry (Details)"
    sprdDetails.Refresh
    DoEvents
            
    Call sprdDetails.PrintSheet

    'sprdDetails.GridShowHoriz = True
    'sprdDetails.GridShowVert = True

    cmdPrint.Enabled = True
    Screen.MousePointer = vbDefault
    cmdExit.Value = True
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case (Shift)
        Case (0):
            Select Case (KeyCode)
                Case Is = vbKeyF9 'if F9 then call print
                    KeyCode = 0
                    If cmdPrint.Visible Then cmdPrint.Value = True
                    
                Case Is = vbKeyF10 'if F10 then clear keypressed as Menu is started
                    KeyCode = 0
                    
                Case Is = vbKeyF12 'if F12 then exit
                    KeyCode = 0
                    cmdExit.Value = True
                    
            End Select 'Key pressed with no Shift/Alt combination
            
        Case (4):
            Select Case (KeyCode)
                Case vbKeyF9
                    KeyCode = 0
                    If cmdFileCopy.Visible Then cmdFileCopy.Value = True
                    
            End Select
    End Select

End Sub

Public Sub DisplayOrderDetails(ByRef lngSelectedRow As Long, ByRef sprdData As fpSpread)
Dim lngLastRow As Long

    Call TransferHeaderInfo(sprdData, lngSelectedRow)
    Call AddOrderLinesSubHeading
    Call TransferLinesInfo(sprdData, lngSelectedRow, False, lngLastRow)
    Call TransferRemarks(sprdData, lngSelectedRow)
    Call DrawLineAcross(sprdDetails)
    lngSelectedRow = lngLastRow
    Call LockSoDetails
    
End Sub

Public Sub DisplayDetailTotal()

    Call DrawLineAcross(sprdDetails)
    sprdDetails.MaxRows = sprdDetails.MaxRows + 1
    sprdDetails.Row = sprdDetails.MaxRows
    
    sprdDetails.Col = COL_SO_TELNO
    sprdDetails.Text = "Totals"
    
    sprdDetails.Col = COL_SO_DEPVAL
    sprdDetails.CellType = CellTypeCurrency
    sprdDetails.Text = mdblDepositValue
    
    sprdDetails.Col = COL_SO_VALUE
    sprdDetails.CellType = CellTypeCurrency
    sprdDetails.Text = mdblFullValue
    
    sprdDetails.Col = COL_SO_OSVALUE
    sprdDetails.CellType = CellTypeCurrency
    sprdDetails.Text = mdblBalanceValue
    
End Sub


Private Sub Form_Load()

    Call InitialiseStatusBar(sbStatus)
    If mblnShowFileCopyPrint Then
        cmdFileCopy.Visible = True
        
    End If
    
End Sub

Private Sub Form_Resize()
Dim lngSpacing As Long
Dim lngMin     As Long

    If Me.WindowState = vbMinimized Then Exit Sub
    lngSpacing = sprdDetails.Left
    
    'Check form is not below minimum width
    lngMin = cmdPrint.Width + cmdExit.Width + (lngSpacing * 4)
    If Me.Width < lngMin Then
        Me.Width = lngMin
        Exit Sub
        
    End If
    
    'Check form is not below minimum height
    lngMin = 3000
    If Me.Height < lngMin Then
        Me.Height = lngMin
        Exit Sub
        
    End If
    
    'relocate Progress bar
    If (Me.Width - ucProgress.Width) > 0 Then
        ucProgress.Left = (Me.Width - ucProgress.Width) / 2
    Else
        ucProgress.Left = 0
    End If
    
    If (Me.Height - ucProgress.Height) > 0 Then
        ucProgress.Top = (Me.Height - ucProgress.Height) / 2
    Else
        ucProgress.Top = 0
    End If
    
    'start resizing
    sprdDetails.Width = Me.Width - sprdDetails.Left * 2 - BORDER_WIDTH * 2
    sprdDetails.Height = Me.Height - (cmdExit.Height + sprdDetails.Top + (lngSpacing * 2) + (BORDER_WIDTH * 4) + sbStatus.Height)
    cmdExit.Top = sprdDetails.Top + sprdDetails.Height + lngSpacing
    cmdPrint.Top = cmdExit.Top
    cmdFileCopy.Top = cmdExit.Top
    cmdPrint.Left = (sprdDetails.Width - cmdPrint.Width) + sprdDetails.Left
    cmdFileCopy.Left = cmdPrint.Left - 120 - cmdFileCopy.Width

End Sub

Private Sub IncrementSprdDetailsRow(Optional intNoOfRows As Integer = 1)
    
    frmSoDetails.sprdDetails.MaxRows = frmSoDetails.sprdDetails.MaxRows + intNoOfRows
    frmSoDetails.sprdDetails.Row = frmSoDetails.sprdDetails.MaxRows

End Sub

Private Sub TransferToSprdDetail(ByRef sprdData As fpSpread, _
                                 ByVal intColFrom As Integer, _
                                 ByVal lngRowFrom As Long, _
                                 ByVal intColTo As Integer, _
                                 ByVal lngRowTo As Long, _
                                 Optional ByVal blnSetRowHeight As Boolean = False)

    sprdData.Col = intColFrom
    sprdData.Row = lngRowFrom
    
    sprdDetails.Col = intColTo
    sprdDetails.Row = lngRowTo
    
    sprdDetails.CellType = sprdData.CellType
    sprdDetails.ForeColor = sprdData.ForeColor
    sprdDetails.BackColor = sprdData.BackColor
    sprdDetails.TypeHAlign = sprdData.TypeHAlign
    sprdDetails.FontStrikethru = sprdData.FontStrikethru
'            .sprdDetails.TypeEditMultiLine = sprdData.TypeEditMultiLine
    '.sprdDetails.TypeMaxEditLen = sprdData.TypeEditMultiLine
    
    If blnSetRowHeight Then
        sprdDetails.RowHeight(lngRowTo) = sprdData.RowHeight(lngRowFrom)
        If sprdData.CellType = CellTypeEdit Then sprdDetails.TypeEditMultiLine = sprdData.TypeEditMultiLine
        
    End If
    
    sprdDetails.Value = sprdData.Value

End Sub

'This transfers main order header details to sprdDetails in frmSoDetails
'data transferred includes order date, order no, ... address, tel no etc
Private Sub TransferHeaderInfo(ByRef sprdData As fpSpread, ByVal lngSelectedRow As Long)
Dim intCol  As Integer
Dim strAdd2 As String
Dim strTel2 As String

    Call IncrementSprdDetailsRow
    For intCol = 1 To COL_SO_CANC_BY Step 1 ' was up to COL_SO_REMARK but remarks now new row
        Call TransferToSprdDetail(sprdData, intCol, lngSelectedRow, intCol, sprdDetails.MaxRows, True)
        
    Next intCol

    sprdData.Row = lngSelectedRow

End Sub

Private Sub AddOrderLinesSubHeading()

    ' heading for order lines
    Call IncrementSprdDetailsRow(1)
    
    'sprdDetails.Col = COL_SD_LINENO
    'sprdDetails.Text = "Line No"
    sprdDetails.Col = COL_SO_SD_ORDERQTY
    sprdDetails.Text = "Quantity"
    sprdDetails.Col = COL_SO_SD_PARTCODE
    sprdDetails.Text = "SKU"
    sprdDetails.Col = COL_SO_SD_DESCRIPTION
    sprdDetails.Text = "Description"
    sprdDetails.ColWidth(COL_SO_SD_DESCRIPTION) = 25
    sprdDetails.Col = COL_SO_SD_SIZE
    sprdDetails.Text = "Size"
    sprdDetails.Col = COL_SO_SD_QUANTATHAND
    sprdDetails.Text = "On Hand"
    sprdDetails.Col = COL_SO_SD_QUANTONORD
    sprdDetails.Text = "On Order"
    sprdDetails.Col = COL_SO_SD_CURRENT
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_SO_SD_COMPLETE
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_SO_SD_CANCELLED
    sprdDetails.ColHidden = True
    
End Sub

Public Sub TransferLinesInfo(ByRef sprdData As fpSpread, _
                             ByVal lngSelectedRow As Long, _
                             ByVal blnShowTotals As Boolean, _
                             ByRef lngLastLineTransferred As Long)
Dim strOrderKey As String
Dim lngLineNo   As Long

    sprdData.Col = COL_SO_ONO
    sprdData.Row = lngSelectedRow
    lngLastLineTransferred = lngSelectedRow
    strOrderKey = sprdData.Text
    
    For lngLineNo = lngSelectedRow To sprdData.MaxRows Step 1
        sprdData.Row = lngLineNo
        sprdData.Col = COL_SL_ONO
        If sprdData.Text <> strOrderKey Then
            Exit For
            lngLastLineTransferred = lngLineNo
            
        Else
            lngLastLineTransferred = lngLineNo
            'Debug.Print sprdData.Text
            Call IncrementSprdDetailsRow
            
            ' Call TransferToSprdDetail(COL_SL_LINENO, lngLineNo, COL_SD_LINENO, sprdDetails.MaxRows)
            Call TransferToSprdDetail(sprdData, COL_SL_ORDERQTY, lngLineNo, COL_SO_SD_ORDERQTY, sprdDetails.MaxRows)
            Call TransferToSprdDetail(sprdData, COL_SL_PARTCODE, lngLineNo, COL_SO_SD_PARTCODE, sprdDetails.MaxRows)
            Call TransferToSprdDetail(sprdData, COL_SL_DESCRIPTION, lngLineNo, COL_SO_SD_DESCRIPTION, sprdDetails.MaxRows)
            Call TransferToSprdDetail(sprdData, COL_SL_SIZE, lngLineNo, COL_SO_SD_SIZE, sprdDetails.MaxRows)
            Call TransferToSprdDetail(sprdData, COL_SL_QUANTATHAND, lngLineNo, COL_SO_SD_QUANTATHAND, sprdDetails.MaxRows)
            Call TransferToSprdDetail(sprdData, COL_SL_QUANTONORD, lngLineNo, COL_SO_SD_QUANTONORD, sprdDetails.MaxRows)
            Call TransferToSprdDetail(sprdData, COL_SL_CURRENT, lngLineNo, COL_SO_SD_CURRENT, sprdDetails.MaxRows)
            Call TransferToSprdDetail(sprdData, COL_SL_COMPLETE, lngLineNo, COL_SO_SD_COMPLETE, sprdDetails.MaxRows)
            Call TransferToSprdDetail(sprdData, COL_SL_CANCELLED, lngLineNo, COL_SO_SD_CANCELLED, sprdDetails.MaxRows)
            Call TransferToSprdDetail(sprdData, COL_SL_POSTATUS, lngLineNo, COL_SO_SD_POSTATUS, sprdDetails.MaxRows)
            
        End If
        
        sprdDetails.ColWidth(COL_SO_SD_POSTATUS) = sprdDetails.MaxTextColWidth(COL_SO_SD_POSTATUS)

    Next lngLineNo
            
End Sub

Public Sub DrawLineAcross(ByVal sprdSheet As fpSpread)

    sprdSheet.MaxRows = sprdSheet.MaxRows + 1
    sprdSheet.Row = sprdSheet.MaxRows
    
    sprdSheet.Col = -1
    ' set the height of the row specified to 1 with background colour of black
     sprdSheet.RowHeight(sprdSheet.Row) = 1
    ' sprdSheet.FontSize = 4
    sprdSheet.BackColor = vbBlack
    sprdSheet.ForeColor = vbWhite
    
    'Call sprdSheet.AddCellSpan(COL_TIME, sprdSheet.Row, COL_TDATE, 1)
        
End Sub

Public Sub DisplayOrderTotals(ByVal sprdSheet As fpSpread, _
                               ByVal intCol1 As Integer, _
                               ByVal dblTotal As Double, _
                               intCol2 As Integer, _
                               strText As String, _
                               ByVal strValueFormat As String)
    
    With sprdSheet
        .Col = intCol1
        .Text = Format$(dblTotal, strValueFormat)
        .TypeHAlign = TypeHAlignRight
        
        If intCol2 > 0 Then
            .Col = intCol2
            .Text = strText
            
        End If
    End With
    
End Sub

Public Sub LockSoDetails()

    'Lock block of cells
    sprdDetails.Col = -1
    sprdDetails.Row = -1
    sprdDetails.Lock = True    ' Lock cells

End Sub

Public Sub TransferRemarks(ByRef sprdData As fpSpread, ByVal lngLineNo As Long)
        
    sprdData.Row = lngLineNo
    sprdData.Col = COL_SO_REMARK
    
    If sprdData.Text <> "" Then
        Call IncrementSprdDetailsRow
        sprdDetails.Col = COL_SO_SD_PARTCODE
        sprdDetails.Row = sprdDetails.MaxRows
        sprdDetails.Text = "Comment:"
        Call sprdDetails.AddCellSpan(COL_SO_SD_DESCRIPTION, sprdDetails.MaxRows, _
                                                  COL_SO_CANC_BY - COL_SO_SD_DESCRIPTION, 1)
        sprdDetails.TypeEditMultiLine = True
        sprdDetails.TypeMaxEditLen = 300
        ' transfer the remarks, setting Row height if necessary
        Call TransferToSprdDetail(sprdData, COL_SO_REMARK, lngLineNo, COL_SO_SD_DESCRIPTION, sprdDetails.MaxRows)
        sprdDetails.RowHeight(sprdDetails.MaxRows) = sprdDetails.MaxTextRowHeight(sprdDetails.MaxRows)
'        lngLineNo = sprdDetails.Row

    End If

End Sub
