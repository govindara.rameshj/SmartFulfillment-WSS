Attribute VB_Name = "modFillSOViewer"
'<CAMH>****************************************************************************************
'* Module : modFillSOViewer
'* Date   : 09/02/03
'* Author : Unknown
'*$Archive: /Projects/OasysV2/VB/Sales Order Viewer/modFillSOViewer.bas $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Damians $ $Date: 7/06/04 10:18 $ $Revision: 11 $
'* Versions:
'* 09/02/03    Unknown
'*             Header added.
'* 09/06/03  KeithB  add columns 28 -
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "modFillSOViewer"

Public Const COL_SO_ORDDATE     As Long = 1
Public Const COL_SO_ONO         As Long = 2
Public Const COL_SO_CUSTNO      As Long = 3
Public Const COL_SO_USERID      As Long = 4
Public Const COL_SO_TILLID      As Long = 5
Public Const COL_SO_NAME        As Long = 6
Public Const COL_SO_ADD1        As Long = 7
Public Const COL_SO_ADD2        As Long = 8
Public Const COL_SO_ADD3        As Long = 9
Public Const COL_SO_PCODE       As Long = 10
Public Const COL_SO_TELNO       As Long = 11
Public Const COL_SO_2NDTELNO    As Long = 12
Public Const COL_SO_FAXNO       As Long = 13
Public Const COL_SO_DEPNO       As Long = 14
Public Const COL_SO_DEPVAL      As Long = 15
Public Const COL_SO_VALUE       As Long = 16
Public Const COL_SO_OSVALUE     As Long = 17
Public Const COL_SO_PROC        As Long = 18
Public Const COL_SO_PRINTED     As Long = 19
Public Const COL_SO_STATUS      As Long = 20
Public Const COL_SO_QTEDATE     As Long = 21
Public Const COL_SO_QTEEXP      As Long = 22
Public Const COL_SO_NODEL       As Long = 23
Public Const COL_SO_CURRENT     As Long = 24
Public Const COL_SO_COMP        As Long = 25
Public Const COL_SO_CANC        As Long = 26
Public Const COL_SO_CUSTPRES    As Long = 27
Public Const COL_SO_ETADATE     As Long = 28
Public Const COL_SO_CANC_DATE   As Long = 29
Public Const COL_SO_CANC_BY     As Long = 30
Public Const COL_SO_REMARK      As Long = 31

'the following columns are used for the extended "details" display
Public Const COL_SO_SHOW        As Long = 32
Public Const COL_SO_PRN_TELNO   As Long = 33

Public Const COL_SL_LINENO      As Long = 33
Public Const COL_SL_ORDERQTY    As Long = 34
Public Const COL_SL_PARTCODE    As Long = 35 ' SKU
Public Const COL_SL_DESCRIPTION As Long = 36
Public Const COL_SL_SIZE        As Long = 37
Public Const COL_SL_QUANTATHAND As Long = 38
Public Const COL_SL_QUANTONORD  As Long = 39
Public Const COL_SL_SUPPNO      As Long = 40
Public Const COL_SL_CURRENT     As Long = 41
Public Const COL_SL_COMPLETE    As Long = 42
Public Const COL_SL_CANCELLED   As Long = 43
Public Const COL_SL_LINESTAT    As Long = 44
Public Const COL_SL_ONO         As Long = 45
Public Const COL_SL_POSTATUS    As Long = 46

' columns for details form
Public Const COL_SO_SD_LINENO       As Long = 1
Public Const COL_SO_SD_ORDERQTY     As Long = 2
Public Const COL_SO_SD_PARTCODE     As Long = 6 ' SKU
Public Const COL_SO_SD_DESCRIPTION  As Long = 7
Public Const COL_SO_SD_SIZE         As Long = 11
Public Const COL_SO_SD_QUANTATHAND  As Long = 15
Public Const COL_SO_SD_QUANTONORD   As Long = 16
Public Const COL_SO_SD_CURRENT      As Long = 24
Public Const COL_SO_SD_COMPLETE     As Long = 25
Public Const COL_SO_SD_CANCELLED    As Long = 26
Public Const COL_SO_SD_POSTATUS     As Long = 28
'Public Const COL_SD_SUPPNO      As Long = 27
'Public Const COL_SD_ONO         As Long = 28

Private Const CUSTOMERNO_ZEROS  As String = "00000000"
'
Public Const BUTTONTXT_SHRINK   As String = "Shrink"
Public Const BUTTONTXT_RETRIEVE As String = "Retrieve"

Dim arDispCols()     As Long
Dim arDispColWidth() As Double

Public mlngHideSoColumns(20)        As Long
Public mlngHideSdColumns(20)        As Long
Public mlngHideSummaryColumns(20)   As Long

Public mdblDepositValue     As Double
Public mdblFullValue        As Double
Public mdblBalanceValue     As Double

Public Sub DisplayOrders(ByRef colData As Collection, ByRef sprdDisp As fpSpread, _
                         Optional ByRef ucProgressBar As ucpbProgressBar = Nothing)
    
Dim lngEntryNum    As Long
Dim strValue       As String
Dim blnProgressBar As Boolean

    If ucProgressBar Is Nothing Then
        blnProgressBar = False
    Else
        blnProgressBar = True
    End If
    
    If blnProgressBar = True Then
        ucProgressBar.Caption1 = "Populating grid"
        ucProgressBar.Min = 0
        ucProgressBar.Value = 1
    End If
    
    sprdDisp.MaxRows = 0
    
    If colData.Count > 0 Then
    
        If blnProgressBar = True Then
            ucProgressBar.Max = colData.Count
        End If
        
        DoEvents
        
'#RWC        sprdDisp.MaxCols = COL_SO_PRN_TELNO

        mdblDepositValue = 0
        mdblFullValue = 0
        mdblBalanceValue = 0
        
        For lngEntryNum = 1 To colData.Count Step 1
                        
            sprdDisp.MaxRows = sprdDisp.MaxRows + 1
            sprdDisp.Row = sprdDisp.MaxRows
            
            If blnProgressBar = True Then
                ucProgressBar.Value = sprdDisp.MaxRows
            End If
            
            sprdDisp.Col = COL_SO_ONO
            sprdDisp.Text = colData(CLng(lngEntryNum)).OrderNo
            
            If colData(lngEntryNum).Cancelled = True Or colData(lngEntryNum).Complete = True Then
                sprdDisp.FontStrikethru = True
            End If
            
            sprdDisp.Col = COL_SO_CUSTNO
            sprdDisp.Text = colData(CLng(lngEntryNum)).CustomerNo
            sprdDisp.Col = COL_SO_USERID
            sprdDisp.Text = colData(CLng(lngEntryNum)).Salesman
            sprdDisp.Col = COL_SO_TILLID
            sprdDisp.Text = colData(CLng(lngEntryNum)).TillID
            sprdDisp.Col = COL_SO_CUSTPRES
            sprdDisp.Value = Not (colData(CLng(lngEntryNum)).CustomerPresent)
            sprdDisp.Col = COL_SO_NAME
            sprdDisp.Text = colData(CLng(lngEntryNum)).Name
            If colData(lngEntryNum).Cancelled = True Or colData(lngEntryNum).Complete = True Then
                sprdDisp.FontStrikethru = True
            End If
            
            ' address line 1
            sprdDisp.Col = COL_SO_ADD1
            sprdDisp.TypeEditMultiLine = True
            sprdDisp.TypeMaxEditLen = 300
            
            If colData(CLng(lngEntryNum)).CustomerNo = CUSTOMERNO_ZEROS Then
                ' not a trader
                sprdDisp.Text = colData(CLng(lngEntryNum)).AddressLine1
            Else
                ' this is a trader
                ' put 'Trader nnnnnnnn' into address line 1
                sprdDisp.Text = "Trade " & colData(CLng(lngEntryNum)).CustomerNo & vbCrLf & colData(CLng(lngEntryNum)).AddressLine1
            End If
            
            sprdDisp.Text = sprdDisp.Text & vbCrLf & colData(CLng(lngEntryNum)).AddressLine2 & _
                              vbCrLf & colData(CLng(lngEntryNum)).AddressLine3
                              
            If colData(lngEntryNum).Cancelled = True Or colData(lngEntryNum).Complete = True Then
                sprdDisp.FontStrikethru = True
            End If
            
            sprdDisp.Col = COL_SO_PCODE
            sprdDisp.Text = colData(CLng(lngEntryNum)).PostCode
            If colData(lngEntryNum).Cancelled = True Or colData(lngEntryNum).Complete = True Then
                sprdDisp.FontStrikethru = True
            End If
            
            sprdDisp.Col = COL_SO_TELNO
            sprdDisp.TypeEditMultiLine = True
            sprdDisp.Text = colData(CLng(lngEntryNum)).TelephoneNo & vbCrLf & colData(CLng(lngEntryNum)).SecondTelNo
            If colData(lngEntryNum).Cancelled = True Or colData(lngEntryNum).Complete = True Then
                sprdDisp.FontStrikethru = True
            End If
            sprdDisp.Col = COL_SO_PRN_TELNO
            sprdDisp.Text = colData(CLng(lngEntryNum)).TelephoneNo
            If colData(CLng(lngEntryNum)).SecondTelNo <> "" Then sprdDisp.Text = sprdDisp.Text & "-" & colData(CLng(lngEntryNum)).SecondTelNo
            
            sprdDisp.Col = COL_SO_FAXNO
'            sprdDisp.Text = colData(CLng(lngEntryNum)).FaxN
            sprdDisp.Col = COL_SO_DEPNO
            sprdDisp.Text = colData(CLng(lngEntryNum)).DepositNumber
            sprdDisp.Col = COL_SO_DEPVAL
            sprdDisp.Text = colData(CLng(lngEntryNum)).DepositValue
            
            mdblDepositValue = mdblDepositValue + colData(CLng(lngEntryNum)).DepositValue
            
            sprdDisp.Col = COL_SO_PROC
            sprdDisp.Text = colData(CLng(lngEntryNum)).OrderProcessed
            sprdDisp.Col = COL_SO_PRINTED
            sprdDisp.Value = colData(CLng(lngEntryNum)).Printed
            sprdDisp.Col = COL_SO_STATUS
            sprdDisp.Text = colData(CLng(lngEntryNum)).Status
            sprdDisp.Col = COL_SO_QTEDATE
            sprdDisp.Text = DisplayDate(colData(CLng(lngEntryNum)).QuoteDate, False)
            sprdDisp.Col = COL_SO_QTEEXP
            If Year(colData(CLng(lngEntryNum)).QuoteDate) = 1899 Then
                sprdDisp.BackColor = RGB_GREY
            Else
                sprdDisp.Text = DisplayDate(colData(CLng(lngEntryNum)).QuoteExpiry, False)
            End If
            
            sprdDisp.Col = COL_SO_ORDDATE
            sprdDisp.Text = DisplayDate(colData(CLng(lngEntryNum)).OrderDate, False)
            
            If colData(lngEntryNum).Cancelled = True Or colData(lngEntryNum).Complete = True Then
                sprdDisp.FontStrikethru = True
            End If
            
            '    ETA date -
            ' OR date completed from LastDespatchDate
            ' OR  date cancelled
            sprdDisp.Col = COL_SO_ETADATE
            If colData(CLng(lngEntryNum)).Complete = True Then
                ' sprdDisp.Text = "Complete"
                sprdDisp.Text = DisplayDate(colData(CLng(lngEntryNum)).LastDespatchDate, False)
            End If
            If colData(CLng(lngEntryNum)).Cancelled = True Then
                sprdDisp.Text = DisplayDate(colData(CLng(lngEntryNum)).CancelledDate, False)
            End If
            If sprdDisp.Text = "" Then
                sprdDisp.Text = DisplayDate(colData(CLng(lngEntryNum)).ETADate, False)
            End If
            
            sprdDisp.Col = COL_SO_NODEL
            sprdDisp.Text = colData(CLng(lngEntryNum)).NoDeliveries
            
            ' completed ?
            sprdDisp.Col = COL_SO_COMP
            sprdDisp.Value = Abs(colData(CLng(lngEntryNum)).Complete)
            
            ' cancelled ?
            sprdDisp.Col = COL_SO_CANC
            sprdDisp.Value = Abs(colData(CLng(lngEntryNum)).Cancelled)
            
            ' an order is said to be current if-
            '     the complete  flag is set to false
            ' and the cancelled flag is set to false

            sprdDisp.Col = COL_SO_CURRENT
            If (colData(CLng(lngEntryNum)).Complete = False) And _
                (colData(CLng(lngEntryNum)).Cancelled = False) Then
                sprdDisp.Value = 1
            Else
                sprdDisp.Value = 0
            End If
            
            If (colData(CLng(lngEntryNum)).Cancelled = True) Then
                sprdDisp.Col = COL_SO_CANC_DATE
                sprdDisp.Text = DisplayDate(colData(CLng(lngEntryNum)).CancelledDate, False)
                sprdDisp.Col = COL_SO_CANC_BY
                sprdDisp.Text = colData(CLng(lngEntryNum)).CancelledCashierID
            Else
                sprdDisp.Col = COL_SO_CANC_DATE
                sprdDisp.BackColor = RGB_GREY
                sprdDisp.Col = COL_SO_CANC_BY
                sprdDisp.BackColor = RGB_GREY
            End If
            sprdDisp.Col = COL_SO_VALUE
            sprdDisp.Text = colData(CLng(lngEntryNum)).OrderValue
            
            mdblFullValue = mdblFullValue + colData(CLng(lngEntryNum)).OrderValue
        
            sprdDisp.Col = COL_SO_OSVALUE
            sprdDisp.Text = colData(CLng(lngEntryNum)).OutstandingValue
            
            mdblBalanceValue = mdblBalanceValue + colData(CLng(lngEntryNum)).OutstandingValue
            
            sprdDisp.Col = COL_SL_ONO
            sprdDisp.Text = colData(CLng(lngEntryNum)).OrderNo
            
            ' remarks
            sprdDisp.Col = COL_SO_REMARK
            sprdDisp.TypeEditMultiLine = True
            sprdDisp.TypeMaxEditLen = 300
            sprdDisp.Text = colData(CLng(lngEntryNum)).Remarks
            
            If Trim(sprdDisp.Text) <> "" Then
                sprdDisp.RowHeight(sprdDisp.Row) = sprdDisp.MaxTextRowHeight(sprdDisp.Row)
            End If
            
        Next lngEntryNum
        
    End If 'any matched records to display
    Exit Sub
    
DisplayError:

    Call Err.Bubble(MODULE_NAME, "DisplayOrders", 1)
    Exit Sub
    
Resume Next

End Sub


Public Sub DisplayOrderLines(ByRef sprdData As fpSpread, _
                              ByVal strOrderKey As String, _
                              ByRef pbProgressBar As ucpbProgressBar, _
                              Optional blnForceAddColHeader As Boolean = False, _
                              Optional blnSetSpreadRedraw As Boolean = True)

Dim oPart             As cInventory
Dim oPurline          As cPurchaseLine
Dim oPOHeaderBO       As cPurchaseOrder
Dim colPurLine        As Collection
Dim lngLineNo         As Long
Dim lngTopRow         As Long
Dim strOrderDate      As String
Dim strDueDate        As String
Dim strSuppNo         As String
Dim strLineStatus     As String
Dim strPONumber       As String
Dim blnCurrent        As Boolean
Dim blnNeedHeaderCols As Boolean
Dim blnDispProgress   As Boolean
Dim oPOLine           As Object

Const PROCEDURE_NAME As String = MODULE_NAME & ".DisplayOrderLines"

    On Error GoTo DisplayOrderLines_Err

    sprdData.SetFocus
    
    If ((pbProgressBar Is Nothing) = False) Then pbProgressBar.Visible = True
    
    ' extend the grid if necessary
    If sprdData.MaxCols < COL_SL_POSTATUS Then
        sprdData.MaxCols = COL_SL_POSTATUS
        blnNeedHeaderCols = True
    Else
        blnNeedHeaderCols = False
    End If
        
    sprdData.Col = COL_SO_SHOW
    sprdData.TypeButtonText = BUTTONTXT_SHRINK
        
    sprdData.Col = COL_SL_ONO
    sprdData.Text = strOrderKey
    
    ' add headers if we need to or it is requested
    If (blnNeedHeaderCols = True) Or (blnForceAddColHeader = True) Then
        
        sprdData.Row = 0
        
        sprdData.Col = COL_SL_LINENO
        sprdData.Text = "Line No"
        sprdData.Col = COL_SL_PARTCODE
        sprdData.Text = "SKU"
        sprdData.ColHidden = True
        
        sprdData.Col = COL_SL_DESCRIPTION
        sprdData.Text = "Description"
        sprdData.ColWidth(COL_SL_DESCRIPTION) = 25
        sprdData.ColHidden = True
        
        sprdData.Col = COL_SL_SIZE
        sprdData.Text = "Size"
        sprdData.ColHidden = True
        
        sprdData.Col = COL_SL_QUANTATHAND
        sprdData.Text = "On Hand"
        sprdData.ColHidden = True
        
        sprdData.Col = COL_SL_QUANTONORD
        sprdData.Text = "On Order"
        sprdData.ColHidden = True
        
        sprdData.Col = COL_SL_ORDERQTY
        sprdData.Text = "Order Qty"
        sprdData.ColHidden = True
        sprdData.Col = COL_SL_SUPPNO
        sprdData.Text = "Supplier"
        sprdData.ColHidden = True
        
        sprdData.Col = COL_SL_CURRENT
        sprdData.Text = "Line Current"
        sprdData.ColWidth(COL_SL_CURRENT) = 5
        sprdData.ColHidden = True
        
        sprdData.Col = COL_SL_COMPLETE
        sprdData.Text = "Line Complete"
        sprdData.ColWidth(COL_SL_COMPLETE) = 5
        sprdData.ColHidden = True
        
        sprdData.Col = COL_SL_CANCELLED
        sprdData.Text = "Line Cancelled"
        sprdData.ColWidth(COL_SL_CANCELLED) = 5
        sprdData.ColHidden = True
        
        sprdData.Col = COL_SL_LINESTAT
        sprdData.Text = "Status"
        sprdData.ColWidth(COL_SL_LINESTAT) = 14
        sprdData.ColHidden = True
        
        sprdData.Col = COL_SL_ONO
        sprdData.Text = "Order No"
        sprdData.ColHidden = True
    
        sprdData.Col = COL_SL_POSTATUS
        sprdData.Text = "PO Status"
        sprdData.ColHidden = True
    End If
    
DisplayOrderLines_Done:
    On Error Resume Next
    sprdData.BlockMode = False
    
    If blnSetSpreadRedraw = True Then
        sprdData.Redraw = True
        sprdData.Refresh
    End If
    
    Set oPart = Nothing
    
    If blnDispProgress = True Then
        pbProgressBar.Visible = False
    End If
    
    sprdData.Enabled = True
    sprdData.SetFocus
   
    Exit Sub


DisplayOrderLines_Err:
    Debug.Print Err.Number, Err.Description
    Call Err.Push 'Preserve Error Code
    Call Err.Pop  'Retrieve Error Code
    'Re-raise error to next handler up the chain.
    Call Err.Report("", PROCEDURE_NAME, Erl)
    Call Err.Clear
    Resume DisplayOrderLines_Done
    
    Resume Next
    
End Sub

Private Sub HidePrintColumn(ByRef sprdData As fpSpread, lngColumnNo As Long)

    sprdData.Col = lngColumnNo
    If sprdData.ColHidden = False Then
        ReDim Preserve arDispCols(UBound(arDispCols) + 1)
        arDispCols(UBound(arDispCols)) = lngColumnNo
        ReDim Preserve arDispColWidth(UBound(arDispColWidth) + 1)
        arDispColWidth(UBound(arDispColWidth)) = sprdData.ColWidth(lngColumnNo)
        sprdData.ColHidden = True
    End If

End Sub

Private Sub UnHidePrintColumns(ByRef sprdData As fpSpread)

Dim lngColumnNo As Long

    For lngColumnNo = 1 To UBound(arDispCols) Step 1
        sprdData.Col = arDispCols(lngColumnNo)
        sprdData.ColHidden = False
        sprdData.ColWidth(sprdData.Col) = arDispColWidth(lngColumnNo)
    Next lngColumnNo

End Sub
Public Sub HideSOColumns(ByRef sprdData As fpSpread)
    
    ' these are columns we want to hide in an Order
    mlngHideSoColumns(1) = COL_SO_CUSTNO
    mlngHideSoColumns(2) = COL_SO_USERID
    mlngHideSoColumns(3) = COL_SO_TILLID
    mlngHideSoColumns(4) = COL_SO_FAXNO
    mlngHideSoColumns(5) = COL_SO_DEPNO
    mlngHideSoColumns(6) = COL_SO_PROC
    mlngHideSoColumns(7) = COL_SO_PRINTED
    mlngHideSoColumns(8) = COL_SO_STATUS
    mlngHideSoColumns(9) = COL_SO_QTEDATE
    mlngHideSoColumns(10) = COL_SO_QTEEXP
    mlngHideSoColumns(11) = COL_SO_NODEL
    mlngHideSoColumns(12) = COL_SO_CANC_DATE
    mlngHideSoColumns(13) = COL_SO_CANC_BY
    mlngHideSoColumns(14) = COL_SO_ADD2
    mlngHideSoColumns(15) = COL_SO_ADD3
    mlngHideSoColumns(16) = COL_SO_2NDTELNO
    mlngHideSoColumns(16) = COL_SO_SHOW

    Call HideColumns(sprdData, mlngHideSoColumns())
    
End Sub
Public Sub HideColumns(ByRef sprdControl As fpSpread, ByRef lngColToHide() As Long)

Dim intPnt As Integer

    sprdControl.Row = -1
    
    For intPnt = 1 To UBound(lngColToHide) Step 1
        If lngColToHide(intPnt) > 0 Then
            sprdControl.Col = lngColToHide(intPnt)
            sprdControl.ColHidden = True
        End If
    Next intPnt
    
End Sub

Public Sub PrintList(ByRef sprdData As fpSpread, ByVal strPrintHeader As String)

Dim strHeader    As String
Dim strFooter    As String
Dim dblRowHeight As Double
Dim lngRowNo     As Long
    
    ReDim arDispCols(0)
    ReDim arDispColWidth(0)
    
    Call HidePrintColumn(sprdData, COL_SO_ADD1)
    Call HidePrintColumn(sprdData, COL_SO_PCODE)
    Call HidePrintColumn(sprdData, COL_SO_TELNO)
    Call HidePrintColumn(sprdData, COL_SO_CURRENT)
    Call HidePrintColumn(sprdData, COL_SO_COMP)
    Call HidePrintColumn(sprdData, COL_SO_CANC)
    Call HidePrintColumn(sprdData, COL_SO_CUSTPRES)
    Call HidePrintColumn(sprdData, COL_SO_ETADATE)
    Call HidePrintColumn(sprdData, COL_SO_REMARK)
        
    'Unhide printed telephone nos
    sprdData.Col = COL_SO_PRN_TELNO
    sprdData.ColHidden = False
    sprdData.ColWidth(COL_SO_PRN_TELNO) = sprdData.MaxTextColWidth(COL_SO_PRN_TELNO) + 3
    sprdData.ColWidth(COL_SO_NAME) = sprdData.MaxTextColWidth(COL_SO_NAME) + 3
    'Extract 1 line height
    sprdData.Row = 1
    sprdData.Col = COL_SO_ONO
    dblRowHeight = sprdData.MaxTextCellHeight
    ' get name and address for current store to put at top of document
    sprdData.PrintHeader = "/c/fb1" & strPrintHeader
    
    sprdData.PrintJobName = "Customer Order Enquiry (Summary)"
    
    sprdData.Redraw = False
    For lngRowNo = 1 To sprdData.MaxRows Step 1
        sprdData.RowHeight(lngRowNo) = dblRowHeight
    Next lngRowNo
    sprdData.Redraw = True
    
    sprdData.MaxRows = sprdData.MaxRows + 3
    sprdData.RowHeight(sprdData.MaxRows - 2) = 1
    sprdData.Row = sprdData.MaxRows - 1
    sprdData.Col = COL_SO_NAME
    sprdData.Text = "Totals"
    sprdData.TypeHAlign = TypeHAlignCenter
    sprdData.Col = COL_SO_DEPVAL
    sprdData.Formula = "SUM(" & ColToText(COL_SO_DEPVAL) & "1:" & ColToText(COL_SO_DEPVAL) & sprdData.MaxRows - 3 & ")"
    sprdData.Col = COL_SO_VALUE
    sprdData.Formula = "SUM(" & ColToText(COL_SO_VALUE) & "1:" & ColToText(COL_SO_VALUE) & sprdData.MaxRows - 3 & ")"
    sprdData.Col = COL_SO_OSVALUE
    sprdData.Formula = "SUM(" & ColToText(COL_SO_OSVALUE) & "1:" & ColToText(COL_SO_OSVALUE) & sprdData.MaxRows - 3 & ")"
    sprdData.RowHeight(sprdData.MaxRows) = 1
    ' Set up Footer - program version no date/time printed
    sprdData.PrintFooter = GetFooter
    sprdData.PrintOrientation = PrintOrientationPortrait
    Call sprdData.PrintSheet
    
    ' clean up
    On Error Resume Next
    Call UnHidePrintColumns(sprdData)
    sprdData.Col = COL_SO_PRN_TELNO
    sprdData.ColHidden = True
        
End Sub

Public Sub HideLineItems(ByRef sprdData As fpSpread)

Dim lngNoLines As Long
Dim lngTopRow  As Long
Dim strKey   As String
Dim lngNoItems As Long

    sprdData.Col = COL_SL_ONO
    strKey = sprdData.Text
    lngTopRow = sprdData.Row
    lngNoItems = 1
    For lngNoLines = sprdData.Row + 1 To sprdData.MaxRows Step 1
        Call DebugMsg(MODULE_NAME, "HideLineItems", endlDebug, "Checking-" & lngNoLines & " at " & lngNoItems)
        sprdData.Row = lngNoLines
        If sprdData.Text <> strKey Then
            Exit For
        Else
            lngNoItems = lngNoItems + 1
        End If
    Next lngNoLines
    sprdData.BlockMode = True
    sprdData.Col = COL_SL_ONO
    sprdData.Col2 = COL_SL_ONO
    sprdData.Row = lngTopRow + 1
    sprdData.Row2 = lngTopRow + lngNoItems - 1
    sprdData.RowHidden = True
    sprdData.Row = lngTopRow
'    sprdData.BackColor = RGB_GREY
'    sprdData.ForeColor = RGB_GREY
    sprdData.BlockMode = False
    sprdData.Col = COL_SO_SHOW
    sprdData.TypeButtonText = "Expand"

End Sub

