VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cRefundLine"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public LineNo As Long
Public PartCode As String
Public RefundQuantity As Long
Public Price As Double
Public RefundReasonCode As String

Public RefundPrintLabels As Boolean
Public PriceOverrideReason As String

Public WEEEUnitCharge As Currency
Public WEEESKU As String
Public RefundDeliveryQty As Long
Public RefundTakenQty As Long
Public QtyCancelled As Long
Public QtyReturned As Long
Public OrderNumber As String


