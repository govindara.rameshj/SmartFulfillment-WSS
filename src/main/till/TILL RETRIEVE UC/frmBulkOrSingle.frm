VERSION 5.00
Begin VB.Form frmBulkOrSingle 
   Caption         =   "Single or Bulk"
   ClientHeight    =   2790
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   3900
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   2790
   ScaleWidth      =   3900
   StartUpPosition =   2  'CenterScreen
   Begin VB.Label lblBulk 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Bulk"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   2160
      TabIndex        =   3
      Top             =   1980
      Width           =   1515
   End
   Begin VB.Label lblSingle 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Single"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   240
      TabIndex        =   2
      Top             =   1980
      Width           =   1515
   End
   Begin VB.Label lblAreYouSure 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Is the"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   1470
      TabIndex        =   1
      Top             =   60
      Width           =   960
   End
   Begin VB.Label lblQuestion 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "[S]ingle or [B]ulk item being returned? (Press <s> or <b>)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1305
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   3660
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmBulkOrSingle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public SingleItem As Boolean

Public Event Duress()
Private mintDuressKeyCode As Integer

Private Sub lblBulk_Click()

    Call Form_KeyDown(vbKeyB, 0)

End Sub

Private Sub lblSingle_Click()

    Call Form_KeyDown(vbKeyS, 0)

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyS Then
        SingleItem = True
        Me.Hide
    End If
    
    If KeyCode = vbKeyB Then
        SingleItem = False
        Me.Hide
    End If
    
End Sub

Private Sub Form_Load()
    Me.BackColor = RGBQuery_BackColour
End Sub

