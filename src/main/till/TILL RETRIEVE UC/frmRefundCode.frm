VERSION 5.00
Object = "{6514F5A0-641C-11D2-9FD0-0020AF131A57}#3.0#0"; "fpFlp30.ocx"
Begin VB.Form frmRefundCode 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Select Refund Reason Code"
   ClientHeight    =   4620
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8445
   Icon            =   "frmRefundCode.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4620
   ScaleWidth      =   8445
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin LpADOLib.fpListAdo lstReasonsList 
      Height          =   3510
      Left            =   1320
      TabIndex        =   0
      Top             =   240
      Width           =   5835
      _Version        =   196608
      _ExtentX        =   10292
      _ExtentY        =   6191
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Columns         =   2
      Sorted          =   0
      LineWidth       =   1
      SelDrawFocusRect=   -1  'True
      ColumnSeparatorChar=   9
      ColumnSearch    =   -1
      ColumnWidthScale=   2
      RowHeight       =   -1
      MultiSelect     =   0
      WrapList        =   0   'False
      WrapWidth       =   0
      SelMax          =   -1
      AutoSearch      =   2
      SearchMethod    =   0
      VirtualMode     =   0   'False
      VRowCount       =   0
      DataSync        =   3
      ThreeDInsideStyle=   0
      ThreeDInsideHighlightColor=   -2147483633
      ThreeDInsideShadowColor=   -2147483627
      ThreeDInsideWidth=   1
      ThreeDOutsideStyle=   0
      ThreeDOutsideHighlightColor=   -2147483628
      ThreeDOutsideShadowColor=   -2147483632
      ThreeDOutsideWidth=   1
      ThreeDFrameWidth=   0
      BorderStyle     =   1
      BorderColor     =   0
      BorderWidth     =   1
      ThreeDOnFocusInvert=   0   'False
      ThreeDFrameColor=   -2147483633
      Appearance      =   0
      BorderDropShadow=   0
      BorderDropShadowColor=   -2147483632
      BorderDropShadowWidth=   3
      ScrollHScale    =   2
      ScrollHInc      =   0
      ColsFrozen      =   0
      ScrollBarV      =   1
      NoIntegralHeight=   0   'False
      HighestPrecedence=   0
      AllowColResize  =   0
      AllowColDragDrop=   0
      ReadOnly        =   0   'False
      VScrollSpecial  =   0   'False
      VScrollSpecialType=   0
      EnableKeyEvents =   -1  'True
      EnableTopChangeEvent=   -1  'True
      DataAutoHeadings=   -1  'True
      DataAutoSizeCols=   2
      SearchIgnoreCase=   -1  'True
      ScrollBarH      =   1
      VirtualPageSize =   0
      VirtualPagesAhead=   0
      ExtendCol       =   0
      ColumnLevels    =   1
      ListGrayAreaColor=   -2147483637
      GroupHeaderHeight=   -1
      GroupHeaderShow =   0   'False
      AllowGrpResize  =   0
      AllowGrpDragDrop=   0
      MergeAdjustView =   0   'False
      ColumnHeaderShow=   0   'False
      ColumnHeaderHeight=   -1
      GrpsFrozen      =   0
      BorderGrayAreaColor=   -2147483637
      ExtendRow       =   0
      DataField       =   ""
      DataMember      =   ""
      OLEDragMode     =   0
      OLEDropMode     =   0
      Redraw          =   -1  'True
      ResizeRowToFont =   0   'False
      TextTipMultiLine=   0
      ColDesigner     =   "frmRefundCode.frx":058A
   End
   Begin VB.Frame fraButtons 
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   300
      TabIndex        =   4
      Top             =   3960
      Width           =   7935
      Begin VB.CommandButton cmdSelect 
         Caption         =   "Select"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   5940
         TabIndex        =   6
         Top             =   0
         Width           =   1995
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Cancel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   60
         TabIndex        =   5
         Top             =   0
         Width           =   1995
      End
   End
   Begin VB.Frame fraTextDesc 
      BorderStyle     =   0  'None
      Height          =   1155
      Left            =   240
      TabIndex        =   1
      Top             =   3780
      Visible         =   0   'False
      Width           =   7935
      Begin VB.TextBox txtReasonDescription 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   0
         MaxLength       =   40
         TabIndex        =   3
         Top             =   480
         Width           =   7935
      End
      Begin VB.Label lblReasonDesclbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Refund Reason Description"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   0
         TabIndex        =   2
         Top             =   60
         Width           =   7395
      End
   End
End
Attribute VB_Name = "frmRefundCode"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public CloseKeyCode    As Long
Public UseKeyCode      As Long

Private mstrRefundCode As String
Private mcolRefundCodes As Collection

Public Function GetRefundReasonCode(ByVal strCancelCaption As String, ByRef colRefundReasons As Collection) As String

    
    cmdCancel.Caption = strCancelCaption
    
    mstrRefundCode = ""
    
    Call LoadReasonCodes(colRefundReasons)

    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    fraButtons.BackColor = Me.BackColor
    fraTextDesc.BackColor = Me.BackColor
    Call CentreForm(Me)
    Set mcolRefundCodes = colRefundReasons
    Call Me.Show(vbModal)
    GetRefundReasonCode = mstrRefundCode

End Function

Private Sub cmdSelect_Click()

    Call lstReasonsList_KeyPress(vbKeyReturn)

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    Select Case (KeyCode)
        Case (CloseKeyCode):    KeyCode = 0
                                cmdCancel.Value = True
        Case (UseKeyCode):      KeyCode = 0
                                cmdSelect.Value = True
                                
    End Select
    
End Sub

Private Sub cmdCancel_Click()

    Me.Hide

End Sub

Private Sub ProcessReason()

Dim lngSelectPos    As Long
Dim oSelectedRF     As cRefundCode
    
    lngSelectPos = lstReasonsList.ItemData(lstReasonsList.ListIndex)
    Set oSelectedRF = mcolRefundCodes(lngSelectPos)
    mstrRefundCode = oSelectedRF.Code
    Me.Hide

End Sub

Private Sub LoadReasonCodes(colRefundCodes As Collection)

Dim lngCodeNo As Long
Dim oReasonBO As cRefundCode
    
    Call lstReasonsList.Clear
    txtReasonDescription.Text = vbNullString

    For lngCodeNo = 1 To colRefundCodes.Count Step 1
        Set oReasonBO = colRefundCodes(lngCodeNo)
        Call lstReasonsList.AddItem(oReasonBO.Code & vbTab & oReasonBO.Description)
        lstReasonsList.ItemData(lstReasonsList.NewIndex) = lngCodeNo
    Next lngCodeNo
                    
    lstReasonsList.Col = 0
    lstReasonsList.ColWidth = 6
    lstReasonsList.ColHide = False
    lstReasonsList.Col = 1
    lstReasonsList.ColWidth = 22
    
    If (lstReasonsList.ListCount > 0) Then lstReasonsList.ListIndex = 0
    
End Sub



Private Sub lstReasonsList_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call ProcessReason
    End If
    
    If (KeyAscii = vbKeyEscape) Then
        KeyAscii = 0
        cmdCancel.Value = True
    End If
    
End Sub
