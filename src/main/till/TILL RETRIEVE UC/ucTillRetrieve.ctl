VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{6514F5A0-641C-11D2-9FD0-0020AF131A57}#3.0#0"; "fpFlp30.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Object = "{9BA18739-054D-4172-8E42-118133CE2FC4}#1.0#0"; "EditDateCtl.ocx"
Begin VB.UserControl ucTillRetrieve 
   ClientHeight    =   7185
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11910
   KeyPreview      =   -1  'True
   ScaleHeight     =   7185
   ScaleWidth      =   11910
   Begin CTSProgBar.ucpbProgressBar ucpbProgress 
      Height          =   1860
      Left            =   3480
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   3000
      Visible         =   0   'False
      Width           =   5100
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      Object.Width           =   5100
      Object.Height          =   1860
   End
   Begin VB.PictureBox fraTran 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   6255
      Left            =   60
      ScaleHeight     =   6225
      ScaleWidth      =   11685
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   720
      Width           =   11715
      Begin VB.PictureBox picPictureBox2 
         BorderStyle     =   0  'None
         Height          =   615
         Left            =   60
         ScaleHeight     =   615
         ScaleWidth      =   11535
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   5460
         Width           =   11535
         Begin VB.CommandButton cmdList 
            Caption         =   "List"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   6660
            TabIndex        =   42
            Top             =   60
            Width           =   1515
         End
         Begin VB.CommandButton cmdOtherTran 
            Caption         =   "Tran at other Store"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   6660
            TabIndex        =   6
            Top             =   60
            Visible         =   0   'False
            Width           =   3015
         End
         Begin VB.CommandButton cmdReset 
            Caption         =   "Reset"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   0
            TabIndex        =   8
            Top             =   60
            Width           =   1455
         End
         Begin VB.CommandButton cmdCancel 
            Caption         =   "Cancel"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   9780
            TabIndex        =   7
            Top             =   60
            Width           =   1755
         End
         Begin VB.CommandButton cmdDetails 
            Caption         =   "OK"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   5160
            TabIndex        =   5
            Top             =   60
            Width           =   1395
         End
         Begin VB.CommandButton cmdNoProof 
            Caption         =   "No Proof of Purchase"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   1560
            TabIndex        =   9
            Top             =   60
            Visible         =   0   'False
            Width           =   3495
         End
      End
      Begin VB.Frame fraTranSearch 
         BorderStyle     =   0  'None
         Height          =   1215
         Left            =   120
         TabIndex        =   14
         Top             =   120
         Width           =   11115
         Begin VB.TextBox txtWebOrderNo 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   3660
            TabIndex        =   4
            Top             =   660
            Width           =   7260
         End
         Begin VB.TextBox txtTranNo 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   9960
            MaxLength       =   4
            TabIndex        =   3
            Text            =   "0000"
            Top             =   0
            Width           =   975
         End
         Begin VB.TextBox txtTillId 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   7320
            MaxLength       =   2
            TabIndex        =   2
            Text            =   "00"
            Top             =   0
            Width           =   615
         End
         Begin VB.TextBox txtStoreNo 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   1680
            MaxLength       =   3
            TabIndex        =   0
            Text            =   "000"
            Top             =   0
            Width           =   855
         End
         Begin ucEditDate.ucDateText dtxtTranDate 
            Height          =   555
            Left            =   3660
            TabIndex        =   1
            Top             =   0
            Width           =   1815
            _ExtentX        =   3201
            _ExtentY        =   979
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DateFormat      =   "DD/MM/YY"
            Text            =   "13/05/03"
         End
         Begin VB.Label lblWebOrderNo 
            Caption         =   "Web Order Number"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   90
            TabIndex        =   19
            Top             =   660
            Width           =   3525
         End
         Begin VB.Label Label2 
            Caption         =   "Store No"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   0
            TabIndex        =   15
            Top             =   0
            Width           =   1575
         End
         Begin VB.Label Label5 
            Caption         =   "Tran No"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   8400
            TabIndex        =   18
            Top             =   0
            Width           =   1455
         End
         Begin VB.Label Label4 
            Caption         =   "Till ID"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   6120
            TabIndex        =   17
            Top             =   0
            Width           =   1095
         End
         Begin VB.Label Label3 
            Caption         =   "Date"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   2760
            TabIndex        =   16
            Top             =   0
            Width           =   855
         End
      End
      Begin MSAdodcLib.Adodc adodcSearch 
         Height          =   330
         Left            =   8400
         Top             =   120
         Visible         =   0   'False
         Width           =   1200
         _ExtentX        =   2117
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "Adodc1"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.Frame fraTranName 
         Caption         =   "Frame1"
         Height          =   495
         Left            =   120
         TabIndex        =   20
         Top             =   120
         Visible         =   0   'False
         Width           =   7695
         Begin VB.TextBox txtName 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   1680
            MaxLength       =   40
            TabIndex        =   21
            Top             =   0
            Width           =   4935
         End
         Begin VB.Label Label6 
            Caption         =   "Name"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   0
            TabIndex        =   22
            Top             =   0
            Width           =   1215
         End
      End
      Begin VB.Frame fraTranID 
         Height          =   495
         Left            =   120
         TabIndex        =   23
         Top             =   120
         Visible         =   0   'False
         Width           =   4575
         Begin VB.Label Label1 
            Caption         =   "Tran ID"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   0
            TabIndex        =   24
            Top             =   0
            Width           =   1455
         End
      End
   End
   Begin VB.PictureBox fraDetails 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   6255
      Left            =   60
      ScaleHeight     =   6225
      ScaleWidth      =   11685
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      Width           =   11715
      Begin VB.PictureBox picPictureBox1 
         BorderStyle     =   0  'None
         Height          =   615
         Left            =   30
         ScaleHeight     =   615
         ScaleWidth      =   11595
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   5400
         Width           =   11595
         Begin VB.CommandButton cmdRetrieveAll 
            Caption         =   "Retrieve All"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   2460
            TabIndex        =   28
            Top             =   120
            Width           =   2235
         End
         Begin VB.CommandButton cmdDtlRetrieve 
            Caption         =   "Retrieve"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   0
            TabIndex        =   27
            Top             =   120
            Width           =   1995
         End
         Begin VB.CommandButton cmdDtlCancel 
            Caption         =   "Cancel"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   9660
            TabIndex        =   29
            Top             =   120
            Width           =   1935
         End
      End
      Begin FPSpreadADO.fpSpread sprdDetails 
         Height          =   4605
         Left            =   0
         TabIndex        =   43
         Top             =   780
         Width           =   11625
         _Version        =   458752
         _ExtentX        =   20505
         _ExtentY        =   8123
         _StockProps     =   64
         ColsFrozen      =   2
         DisplayRowHeaders=   0   'False
         EditModeReplace =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   25
         MaxRows         =   1
         OperationMode   =   2
         SpreadDesigner  =   "ucTillRetrieve.ctx":0000
         UserResize      =   0
      End
      Begin VB.Label Label12 
         Caption         =   "Store No"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   37
         Top             =   180
         Width           =   1575
      End
      Begin VB.Label Label11 
         Caption         =   "Date"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3000
         TabIndex        =   36
         Top             =   180
         Width           =   855
      End
      Begin VB.Label Label10 
         Caption         =   "Till ID"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6720
         TabIndex        =   35
         Top             =   180
         Width           =   1095
      End
      Begin VB.Label Label9 
         Caption         =   "Tran No"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9000
         TabIndex        =   34
         Top             =   180
         Width           =   1455
      End
      Begin VB.Label lblStoreNo 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1800
         TabIndex        =   33
         Top             =   180
         Width           =   855
      End
      Begin VB.Label lblTillID 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7920
         TabIndex        =   32
         Top             =   180
         Width           =   615
      End
      Begin VB.Label lblTranNo 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   10560
         TabIndex        =   31
         Top             =   180
         Width           =   1095
      End
      Begin VB.Label lblTranDate 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   3900
         TabIndex        =   30
         Top             =   180
         Width           =   2355
      End
   End
   Begin VB.PictureBox fraParked 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   6255
      Left            =   60
      ScaleHeight     =   6225
      ScaleWidth      =   11685
      TabIndex        =   38
      TabStop         =   0   'False
      Top             =   780
      Width           =   11715
      Begin LpADOLib.fpListAdo lstParked 
         Height          =   5130
         Left            =   240
         TabIndex        =   41
         Top             =   240
         Width           =   11235
         _Version        =   196608
         _ExtentX        =   19817
         _ExtentY        =   9049
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   0   'False
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Columns         =   5
         Sorted          =   1
         LineWidth       =   1
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         MultiSelect     =   0
         WrapList        =   0   'False
         WrapWidth       =   0
         SelMax          =   -1
         AutoSearch      =   1
         SearchMethod    =   0
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   1
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   2
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   2
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   -1  'True
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         DataField       =   ""
         DataMember      =   ""
         OLEDragMode     =   0
         OLEDropMode     =   0
         EnableClickEvent=   -1  'True
         Redraw          =   -1  'True
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         ColDesigner     =   "ucTillRetrieve.ctx":0EA2
      End
      Begin VB.CommandButton cmdParkRetrieve 
         Caption         =   "Select"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   240
         TabIndex        =   40
         Top             =   5520
         Width           =   1995
      End
      Begin VB.CommandButton cmdParkCancel 
         Caption         =   "Cancel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   9540
         TabIndex        =   39
         Top             =   5520
         Width           =   1935
      End
   End
   Begin VB.Label lblAction 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Retrieve original transaction (Enter details/Scan Barcode)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   60
      TabIndex        =   10
      Top             =   180
      Width           =   11715
   End
End
Attribute VB_Name = "ucTillRetrieve"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>**************************************************************************************
'* Module : ucTillRetrieve
'* Date   : 01/10/03
'* Author : mauricem
' $Archive: /Projects/OasysV2/VB/Common/ucTillRetrieve.ctl $
'********************************************************************************************
'* Summary: User control used by the till to retrieve existing transactions from the database
'********************************************************************************************
'* $Revision: 28 $ $ Date: 1/22/03 5:21p $ $Author: Mauricem $
'* Versions:
'* 01/10/03    mauricem
'*             Header added.
'* 08/03/10    Mike O'Cain - Wix1380 Hub deliveries
'</CAMH>*************************************************************************************
Option Explicit


Const MODULE_NAME As String = "ucTillRetrieve"

Const PRM_ALREADY_DESPATCHED_MSG1 As Long = 969010
Const PRM_ALREADY_DESPATCHED_MSG2 As Long = 969011
Const PRM_ALREADY_DESPATCHED_MSG3 As Long = 969012
Const PRM_ALREADY_DESPATCHED_MSG4 As Long = 969013

Const COL_DTL_SKU As Long = 1
Const COL_DTL_DESC As Long = 2
Const COL_DTL_QTY As Long = 3
Const COL_DTL_PRICE As Long = 4
Const COL_DTL_TOTAL As Long = 5
Const COL_DTL_LINENO As Long = 6
Const COL_DTL_WEEE_RATE As Long = 7
Const COL_DTL_QTY_TAKEN As Long = 8
Const COL_DTL_QTY_TO_DELIVER As Long = 9
Const COL_DTL_QTY_REFUNDED As Long = 10
Const COL_DTL_QTY_RFND As Long = 11
Const COL_DTL_QTY_REASON As Long = 12
Const COL_DTL_RFND_PRICE As Long = 13
Const COL_DTL_BULK_ITEM As Long = 14
Const COL_DTL_SINGLE_SKU As Long = 15
Const COL_DTL_SINGLE_BULK As Long = 16
Const COL_DTL_SINGLE_PACKSIZE As Long = 17
Const COL_DTL_WEEE_SKU  As Long = 18
Const COL_DTL_WEEE_SEQ  As Long = 19
Const COL_DTL_MARKDOWN  As Long = 20
Const COL_DTL_CANCELLED  As Long = 21
Const COL_DTL_RETURNED  As Long = 22
Const COL_DTL_STATUS As Long = 23
Const COL_DTL_IS_DELIVERY_CHARGE As Long = 24

'CORHDR4 Delivery Statuses used to check refund screen
Const DELIVERY_STATUS_PICKCONFIRM_CREATED = 600
Const DELIVERY_STATUS_UNDELIVERED_CREATED = 800
Const DELIVERY_STATUS_COMPLETED = 999

Dim mblnInit            As Boolean 'flag to ensure initialise can only be called once
Dim moParent            As Object
Dim mblnReload          As Boolean
Dim mblnParkedOnly      As Boolean
Dim mblnPreview         As Boolean
Dim mblnSelectLine      As Boolean
Dim mblnSalesOnly       As Boolean 'used to set Selection criteria
Dim mblnVerifySKU       As Boolean

Dim mstrPriceSKU        As String
Dim mlngPriceQty        As Long
Dim mdtePPValidFrom     As Date

Dim mstrActionMsg       As String

Dim mlngSearchKeyCode   As Long
Dim mlngResetKeyCode    As Long
Dim mlngCloseKeyCode    As Long
Dim mlngUseKeyCode      As Long
Dim mlngDetailKeyCode   As Long
Dim mlngOtherKeyCode    As Long
Dim mlngVoidKeyCode     As Long
Dim mlngTenderKeyCode   As Long
Dim mlngListKeyCode     As Long
Dim mlngRetAllKeyCode   As Long
Dim mlngNoProofKeyCode  As Long
Dim mlngRefunded        As Long         'WIX 1380 - Hub Deliveries
Dim mlngCancelled       As Long         'WIX 1380 - Hub Deliveries
Dim mblnDelivered       As Boolean      'WIX 1380 - Hub Deliveries
Dim mcolLines           As Collection
Dim mDeliveryStatusHeader As Integer    'Ref 419

Dim mstrRefundCodes As String
Dim mstrUseReason   As String

Dim mstrOrderNo     As String

Dim mstrMarkDownMsg As String

Dim mstrDeliveryStatus As String 'WIX1380 Hub Deliveries

Dim mblnVoidTransaction As Boolean 'P007 - CR010 - Till Change

Dim mblnShowQodRefundScreen As Boolean

Dim mblnDetailsLinesEditing As Boolean

Dim mblnRefundDeterminationScreenHasShown As Boolean


Public AllowScan As Boolean
Public Event Apply(StoreNumber As String, TranDate As Date, TillID As String, TranNumber As String)
Public Event Cancel()
Public Event NoOriginalTran(RefundReasonCode As String)
Public Event OtherStoreTran(StoreNumber As String, TranDate As Date, TillID As String, TranNumber As String, RefundReasonCode As String)
Public Event SelectTransactionLine(ByRef strPartCode As String, _
                                   ByRef dblQuantity As Double, _
                                   ByRef curPrice As Currency, _
                                   ByRef curWEEERate As Currency, _
                                   ByRef strTranID As String, _
                                   ByRef dteTrandate As Date, _
                                   ByRef strTillID As String, _
                                   ByRef lngLineNo As Long, _
                                   ByRef blnVoided As Boolean, _
                                   ByRef blnTendered As Boolean, _
                                   ByRef OrderNumber As String)

Public Event SelectTransactionLines(ByRef strTranID As String, _
                                   ByRef dteTrandate As Date, _
                                   ByRef strTillID As String, _
                                   ByRef colLines As Collection, _
                                   ByRef blnRetrieveAll As Boolean, _
                                   ByRef blnVoided As Boolean, _
                                   ByRef OrderNumber As String)
' Hubs 2.0
' Fire event for till to collect web order information to store as part of a Web Order refund refund
Public Event WebOrderTran(WebOrderNumber As String, StoreNumber As String, TranDate As Date, TillID As String, TranNumber As String, RefundReasonCode As String)
' Hubs 2.0

Public RefundReasons As Collection

Public RefundTrans As New Collection

Public CurrentRefundLines As New Collection

Public PriceOverrideCodes As Collection

Public mMinimumStatus As Integer 'Ref 509

Private mblnVoidRetrieveAll As Boolean

Private TillInTrainingMode As Boolean

Public Sub FillScreen(ByRef ucMe As Object)

    'Move user control to top-left corner of Form
    ucMe.Left = 0
    ucMe.Top = 0
    
    'Match control size to container form
    UserControl.Width = moParent.Width
    UserControl.Height = moParent.Height
    
    'Move frame to center of screen
    lblAction.Left = ((UserControl.Width - fraTran.Width) / 2) - 80
    lblAction.Top = ((UserControl.Height - (lblAction.Height + fraTran.Height)) / 2) - 80
    fraTran.Left = lblAction.Left
    fraTran.Top = ((UserControl.Height - (lblAction.Height + fraTran.Height)) / 2) + lblAction.Height
    fraDetails.Top = fraTran.Top
    fraDetails.Left = fraTran.Left
    fraParked.Top = fraTran.Top
    fraParked.Left = fraTran.Left
End Sub

Public Sub AllowVoid(ByVal blnShowVoid As Boolean)

'    cmdDtlVoid.Visible = blnShowVoid

End Sub

Public Sub AllowTender(ByVal blnShowTender As Boolean)

'    cmdDtlTender.Visible = blnShowTender

End Sub

Public Sub Reset()

    txtStoreNo.Text = goSession.CurrentEnterprise.IEnterprise_StoreNumber
    dtxtTranDate.Text = Format$(Now(), "DD/MM/YY")
    txtTillId.Text = vbNullString
    txtTranNo.Text = vbNullString
    txtWebOrderNo.Text = vbNullString
    fraDetails.Visible = False
    fraParked.Visible = False
    fraTran.Visible = True
    AllowScan = True
    cmdReset.Visible = False
    mblnSalesOnly = False
    mstrOrderNo = ""
    
End Sub

Public Sub Show()
    
    If Not moParent Is Nothing Then
        mblnPreview = moParent.KeyPreview
        moParent.KeyPreview = False
    Else
        Call DebugMsg(MODULE_NAME, "UserControl_Show", endlDebug, "No Parent to set KeyPreview")
    End If
    If (txtTranNo.Visible = True) And (txtTranNo.Visible = True) Then txtTranNo.SetFocus

End Sub

Public Property Let ParkedOnly(value As Boolean)
    
    If value = True Then
        mstrActionMsg = "Retrieve parked transaction (Enter details/Scan barcode)"
        dtxtTranDate.Text = Format$(Now(), "DD/MM/YY")
        lblStoreNo.Caption = txtStoreNo.Text
        dtxtTranDate.Enabled = False
        txtStoreNo.Enabled = False
        cmdOtherTran.Visible = False
        cmdNoProof.Visible = False
        cmdList.Visible = True
        mblnParkedOnly = True
    Else
        mstrActionMsg = "Retrieve original transaction (Enter details/Scan barcode)"
        dtxtTranDate.Text = Format$(Now(), "DD/MM/YY")
        txtStoreNo.Text = vbNullString
        dtxtTranDate.Enabled = True
        txtStoreNo.Enabled = True
        cmdNoProof.Visible = True
        cmdOtherTran.Visible = True
        cmdList.Visible = False
        mblnParkedOnly = False
    End If
    lblAction.Caption = mstrActionMsg

End Property

Public Property Let PricePromiseSKU(value As String)
    
    mstrPriceSKU = value
    If (value = "") Then
        mblnVerifySKU = False
    Else
        mblnVerifySKU = True
        mdtePPValidFrom = DateAdd("d", goSession.GetParameter(PRM_PRICEMATCHDAYS) * -1, Date)
    End If
    

End Property

Public Property Let PricePromiseQty(value As Long)
    
    mlngPriceQty = value

End Property

Public Property Get PricePromiseValidQty() As Long
    
    PricePromiseValidQty = mlngPriceQty

End Property

Public Property Get InTrainingMode() As Variant

    InTrainingMode = TillInTrainingMode
End Property

Public Property Let InTrainingMode(ByVal value As Variant)

    TillInTrainingMode = value
End Property

Public Sub Initialise(ByRef CurrentSession As Object, Optional ByRef ParentForm As Object = Nothing)

Dim oBOCol  As Collection
Dim lItemNo As Long
Dim strKey  As String

    If mblnInit Then Exit Sub
    ' Set up the object and field that we wish to select on
    
    Screen.MousePointer = vbHourglass
    Set moParent = ParentForm
    
    Set goSession = CurrentSession
    Set goDatabase = goSession.Database
    
    Call CreateErrorObject(SYSTEM_NAME & "{" & App.Title & "}", VBA.Err, Err)
    
    UserControl.BackColor = goSession.GetParameter(PRM_QUERY_BORDERCOLOUR)
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    RGBQuery_BackColour = goSession.GetParameter(PRM_QUERY_BACKCOLOUR)
    
    lstParked.BackColor = goSession.GetParameter(PRM_QUERY_GRIDEVENCOLOUR)
    sprdDetails.GrayAreaBackColor = lstParked.BackColor
    lstParked.ListApplyTo = ListApplyToEvenRows
    lstParked.BackColor = goSession.GetParameter(PRM_QUERY_GRIDEVENCOLOUR)
    lstParked.ListApplyTo = ListApplyToOddRows
    lstParked.BackColor = goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR)
    lstParked.Row = -1
    lstParked.RowHeight = goSession.GetParameter(PRM_QUERY_ROWHEIGHT)
    
    If (goSession.GetParameter(PRM_COUNTRY_CODE) <> "IE") Then
        sprdDetails.ColWidth(COL_DTL_DESC) = sprdDetails.ColWidth(COL_DTL_DESC) + sprdDetails.ColWidth(COL_DTL_WEEE_RATE)
        sprdDetails.Col = COL_DTL_WEEE_RATE
        sprdDetails.ColHidden = True
    End If
    
    Call sprdDetails.SetOddEvenRowColor(goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR), vbBlack, goSession.GetParameter(PRM_QUERY_GRIDEVENCOLOUR), vbBlack)
    
    mblnVerifySKU = goSession.GetParameter(PRM_REFUND_VERIFY_SKU)

    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
    RGBMSGBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
        
    strKey = goSession.GetParameter(PRM_KEY_RESET)
    cmdReset.Caption = strKey & "-" & cmdReset.Caption
    If Left$(strKey, 1) = "F" Then
        mlngResetKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngResetKeyCode = Asc(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_CLOSE)
    cmdCancel.Caption = strKey & "-" & cmdCancel.Caption
    cmdDtlCancel.Caption = strKey & "-" & cmdDtlCancel.Caption
    cmdParkCancel.Caption = strKey & "-" & cmdParkCancel.Caption
    If Left$(strKey, 1) = "F" Then
        mlngCloseKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngCloseKeyCode = Asc(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_DETAILS)
    cmdDetails.Caption = strKey & "-" & cmdDetails.Caption
    If Left$(strKey, 1) = "F" Then
        mlngDetailKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngDetailKeyCode = Asc(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_REVERSAL)
    cmdOtherTran.Caption = strKey & "-" & cmdOtherTran.Caption
    If Left$(strKey, 1) = "F" Then
        mlngOtherKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngOtherKeyCode = Asc(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_SAVE)
    cmdDtlRetrieve.Caption = strKey & "-" & cmdDtlRetrieve.Caption
    cmdParkRetrieve.Caption = strKey & "-" & cmdParkRetrieve.Caption
    If Left$(strKey, 1) = "F" Then
        mlngUseKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngUseKeyCode = Asc(UCase$(strKey))
    End If
    mstrUseReason = strKey & "-Process"

    strKey = goSession.GetParameter(PRM_KEY_LOOKUP)
    cmdList.Caption = strKey & "-" & cmdList.Caption
    cmdRetrieveAll.Caption = strKey & "-" & cmdRetrieveAll.Caption
    cmdNoProof.Caption = strKey & "-" & cmdNoProof.Caption
    If Left$(strKey, 1) = "F" Then
        mlngListKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngListKeyCode = Asc(UCase$(strKey))
    End If
    mlngRetAllKeyCode = mlngListKeyCode
    mlngNoProofKeyCode = mlngListKeyCode


    cmdReset.Visible = False
    cmdDetails.Visible = False
    mblnInit = True
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdDetails_Click()
    
Dim strLineData   As String
Dim strStoreNo    As String
Dim dteTrandate   As Date
Dim strTillID     As String
Dim strTranNumber As String
Dim strName       As String
Dim strDesc       As String
Dim lngNoLines    As Long
Dim lngLineNo     As Long
Dim lngXtraSpace  As Long
Dim lngNoCols     As Long
Dim colLines      As Collection
Dim oTranLine     As cPOSLine
Dim oTillTran     As cPOSHeader
Dim oStockItem    As cInventory
Dim strSKU        As String
Dim blnCancel     As Boolean
Dim lngBoughtQty  As Long
Dim oOrderHdr     As cCustOrderHeader
Dim oOrderInfoBO  As cCustOrderInfo
Dim oOrderLineBO  As cCustOrderLine
Dim colOrderLines As Collection
Dim intLineOn     As Integer
Dim Skun          As String
Dim blnFound      As Boolean
Dim intOffset     As Integer
Dim intOrderLineOn As Integer
Dim strMsg As String

    If (mstrRefundCodes = "") Then Call SplitRefundCodes
'    If (mstrOPriceCodes = "") Then Call SplitPriceCodes
    'WIX180 - MO'C Added Clear New Columns to preserve old functionality
    sprdDetails.Col = COL_DTL_QTY_TAKEN
    sprdDetails.Lock = True
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_DTL_QTY_TO_DELIVER
    sprdDetails.Lock = True
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_DTL_QTY_REFUNDED
    sprdDetails.Lock = True
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_DTL_CANCELLED
    sprdDetails.Lock = True
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_DTL_RETURNED
    sprdDetails.Lock = True
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_DTL_STATUS
    sprdDetails.Lock = True
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_DTL_IS_DELIVERY_CHARGE
    sprdDetails.Lock = True
    sprdDetails.ColHidden = True
    
    If (cmdDetails.Visible = True) And (cmdDetails.Enabled = True) Then cmdDetails.SetFocus
    DoEvents
    cmdDetails.Enabled = False
    mstrOrderNo = ""
    'Added 13/3/04 - extra security check to enter sku for refund against selected transaction
    If (mblnVerifySKU = True) And (mstrPriceSKU = "") Then
        strSKU = InputBox("Enter SKU for refunded item", "SKU Validation", vbNullString)
        If LenB(strSKU) = 0 Then Exit Sub
        If Len(strSKU) < PARTCODE_LEN Then strSKU = Left$(PARTCODE_PAD, PARTCODE_LEN - Len(strSKU)) & strSKU
    End If
    
    If (mstrPriceSKU <> "") Then
        strSKU = mstrPriceSKU
    End If
        
    If mblnParkedOnly Then
        If dtxtTranDate.Text <> Format$(Now(), "DD/MM/YY") Then
            AllowScan = False
            Call MsgBoxEx("Transaction must be dated today", vbExclamation, "Recall Parked Transaction", , , , , RGBMsgBox_WarnColour)
            AllowScan = True
            dtxtTranDate.Text = Format$(Now(), "DD/MM/YY")
            cmdDetails.Enabled = True
            Exit Sub
        End If
    End If
    
    'Extract unique look up details for transaction
    strTillID = txtTillId.Text
    dteTrandate = CDate(dtxtTranDate.Text)
    strTranNumber = txtTranNo.Text
    strStoreNo = txtStoreNo.Text
    
    
    If strStoreNo <> Right$("000" & goSession.CurrentEnterprise.IEnterprise_StoreNumber, 3) Then
        If mblnParkedOnly Then
            AllowScan = False
            Call MsgBoxEx("Transaction from another store", vbOKOnly, "Display Transaction Details", , , , , RGBMSGBox_PromptColour)
            AllowScan = True
            Exit Sub
        Else
            'if Refunding and Other Store Number entered, then inform User and Click on Other Store Tran
            AllowScan = False
            Call MsgBoxEx("Transaction from another store", vbOKOnly, "Display Transaction Details", , , , , RGBMSGBox_PromptColour)
            AllowScan = True
            cmdOtherTran.value = True
            cmdDetails.Enabled = True
            Exit Sub
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    
    'Retrieve Transaction Header
    Set oTillTran = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    Call oTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, strTillID)
    Call oTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, dteTrandate)
    Call oTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, strTranNumber)
    'Call oTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_StoreNumber, strStoreNo)
    If oTillTran.LoadMatches.Count > 0 Then
        If mblnParkedOnly Then
            If Not oTillTran.TranParked Then
                AllowScan = False
                Call MsgBoxEx("Specified transaction was not parked", vbOKOnly, "Display Transaction Details", , , , , RGBMsgBox_WarnColour)
                AllowScan = True
                If (txtTillId.Visible = True) And (txtTillId.Enabled = True) Then txtTillId.SetFocus
                
                Screen.MousePointer = vbNormal
                cmdDetails.Enabled = True
                Exit Sub
            End If
        Else
            If oTillTran.TranParked = True Then
                AllowScan = False
                Call MsgBoxEx("Specified transaction was a parked transaction", vbOKOnly, "Display Transaction Details", , , , , RGBMsgBox_WarnColour)
                AllowScan = True
                If (txtTillId.Visible = True) And (txtTillId.Enabled = True) Then txtTillId.SetFocus
                
                Screen.MousePointer = vbNormal
                cmdDetails.Enabled = True
                Exit Sub
            End If
            
            If oTillTran.Voided = True Then
                AllowScan = False
                Call MsgBoxEx("Specified transaction was voided", vbOKOnly, "Display Transaction Details", , , , , RGBMsgBox_WarnColour)
                AllowScan = True

                If (txtTillId.Visible = True) And (txtTillId.Enabled = True) Then txtTillId.SetFocus
                
                Screen.MousePointer = vbNormal
                cmdDetails.Enabled = True
                Exit Sub
            End If
            
            If oTillTran.TrainingMode = True Then
                AllowScan = False
                Call MsgBoxEx("Specified transaction was a training-mode transaction", vbOKOnly, "Display Transaction Details", , , , , RGBMsgBox_WarnColour)
                AllowScan = True

                If (txtTillId.Visible = True) And (txtTillId.Enabled = True) Then txtTillId.SetFocus
                
                Screen.MousePointer = vbNormal
                cmdDetails.Enabled = True
                Exit Sub
            End If
        End If
   
        If SupressOrderRefundBecauseInTrainingMode And Val(oTillTran.OrderNumber) <> 0 Then
            AllowScan = False
            Call MsgBoxEx("Specified transaction is for an Order and you cannot refund against an order whilst in Training Mode", vbOKOnly, "Display Transaction Details", , , , , RGBMsgBox_WarnColour)
            AllowScan = True
            If (txtTillId.Visible = True) And (txtTillId.Enabled = True) Then
                txtTillId.SetFocus
            End If
            Screen.MousePointer = vbNormal
            cmdDetails.Enabled = True
            Exit Sub
        End If
        
        'Added 14/8/08 - check if Refund is against Order that has been delivered
        If (Val(oTillTran.OrderNumber) <> 0) Then
            Set oOrderHdr = goDatabase.CreateBusinessObject(CLASSID_CUSTORDERHEADER)
            Call oOrderHdr.IBo_AddLoadFilter(CMP_EQUAL, FID_CUSTORDERHEADER_OrderNumber, oTillTran.OrderNumber)
            Call oOrderHdr.LoadMatches
'*********  10/12/2010 MO'C - Removed as per referral 591 *****************
'            If (oOrderHdr.DeliveryConfirmed = False) Then
'                Screen.MousePointer = vbNormal
'                Call MsgBoxEx("Order has not been despatched", vbOKOnly, "Refund despatched order", , , , , RGBMSGBox_PromptColour)
'            End If
'**************************************************************************
            sprdDetails.Col = COL_DTL_QTY_TO_DELIVER
            sprdDetails.Row = 0
            If oOrderHdr.IsForDelivery Then
                sprdDetails.Text = "Qty to Deliver"
            Else
                sprdDetails.Text = "Qty to Collect"
            End If

        End If
        mstrOrderNo = oTillTran.OrderNumber
        
    End If
    
    'WIX1380 - Hub Deliveries: See if it's a QOD Customer Order
     If (Val(oTillTran.OrderNumber) <> 0) Then
        'Need to get delivery status from CORHDR4
         If oOrderHdr Is Nothing Then
            Set oOrderHdr = goDatabase.CreateBusinessObject(CLASSID_CUSTORDERHEADER)
            Call oOrderHdr.IBo_AddLoadFilter(CMP_EQUAL, FID_CUSTORDERHEADER_OrderNumber, oTillTran.OrderNumber)
            Call oOrderHdr.LoadMatches
        End If

        mstrDeliveryStatus = ""
        Set oOrderInfoBO = goDatabase.CreateBusinessObject(CLASSID_CUSTORDERINFO)
        Call oOrderInfoBO.IBo_AddLoadFilter(CMP_EQUAL, FID_CUSTORDERINFO_OrderNumber, oTillTran.OrderNumber)
        If oOrderInfoBO.LoadMatches.Count > 0 Then
            
            'Ref 489
            mMinimumStatus = Val(oOrderInfoBO.DeliveryRequestStatus)
            If mMinimumStatus <> 9999 Then
                Set oOrderLineBO = goDatabase.CreateBusinessObject(CLASSID_CUSTORDERLINE)
                Call oOrderLineBO.IBo_AddLoadFilter(CMP_EQUAL, FID_CUSTORDERLINE_OrderNumber, oTillTran.OrderNumber)
                Set colOrderLines = oOrderLineBO.LoadMatches
                For lngLineNo = 1 To colOrderLines.Count
                    Set oOrderLineBO = colOrderLines(lngLineNo)
                    If (oOrderLineBO.DeliveryRequestStatus >= DELIVERY_STATUS_PICKCONFIRM_CREATED And oOrderLineBO.DeliveryRequestStatus < DELIVERY_STATUS_UNDELIVERED_CREATED) Then
                        mblnVoidRetrieveAll = True
                    End If
                    If oOrderLineBO.DeliveryRequestStatus < mMinimumStatus Then
                        mMinimumStatus = oOrderLineBO.DeliveryRequestStatus
                    End If
                Next lngLineNo
            End If
            'End of Ref 489
            
            mstrDeliveryStatus = CStr(mMinimumStatus)
            
            'MO'C - Don't show QOD Refund screen if status is at 999
            If mMinimumStatus >= DELIVERY_STATUS_COMPLETED Then
                mblnShowQodRefundScreen = False
                mstrOrderNo = ""
                Set oOrderHdr = Nothing
                Set oOrderLineBO = Nothing
            Else
                ' Referral 774 - Moved the colOrderLines retrieval to here so can check against tran lines
                ' for matches.
                'WIX 1380 HUB Deliveries - Get Order Lines to read the Quantities
                Set oOrderLineBO = goDatabase.CreateBusinessObject(CLASSID_CUSTORDERLINE)
                Call oOrderLineBO.IBo_AddLoadFilter(CMP_EQUAL, FID_CUSTORDERLINE_OrderNumber, oTillTran.OrderNumber)
                Set colOrderLines = oOrderLineBO.LoadMatches
                ' End of Referral 774
                
                ' Referral 774 - Only show Qod refund screen if there are no sale lines that are not for this order
                ' on the transaction, e.g. exchange done on a previous refund for this order, only want to show the
                ' exchange line not the original order lines
                If TransactionHasNonOrderLines(oTillTran.Lines, colOrderLines) Then
                    mblnShowQodRefundScreen = False
                    mstrOrderNo = ""
                    Set oOrderHdr = Nothing
                    Set oOrderLineBO = Nothing
                    Set colOrderLines = Nothing
                Else
                    mblnShowQodRefundScreen = True
                    mstrDeliveryStatus = CStr(mMinimumStatus) 'oOrderInfoBO.DeliveryRequestStatus
                    sprdDetails.Col = COL_DTL_QTY_TAKEN
                    sprdDetails.ColHidden = False
                    sprdDetails.Col = COL_DTL_QTY_TO_DELIVER
                    sprdDetails.ColHidden = False
                    sprdDetails.Col = COL_DTL_QTY_REFUNDED
                    sprdDetails.ColHidden = False
                    ' Referral 774 - Moved the colOrderLines retrieval to above so can use earlier
                    ' than before
                End If
            End If
            ' Referral 774 - Moved the colOrderLines retrieval to above so can use earlier
            ' than before.  Rearranged code to include new check for existence of non order lines
            ' in a transaction with an order number
        End If
    
    End If
    'Set oOrderInfoBO = Nothing
    'Set oOrderHdr = Nothing

    If (oTillTran.Lines.Count = 0) Or (oTillTran.TillID <> txtTillId.Text) Then
        AllowScan = False
        If (oTillTran.TillID <> txtTillId.Text) Then
            If (goSession.ISession_IsOnline = True) Then
                Call MsgBoxEx("Transaction could not be found - check entered details.", vbOKOnly, "Display Transaction Details", , , , , RGBMsgBox_WarnColour)
            Else
                If (MsgBoxEx("Transaction could not be found - check entered details." & vbNewLine & "Currently Offline - Confirm transaction details are correct", vbYesNo, "Invalid Transaction Details", , , , , RGBMsgBox_WarnColour) = vbYes) Then
                    cmdOtherTran.Tag = "SKIP"
                    cmdOtherTran.value = True
                    Exit Sub
                End If
            End If
        Else
            Call MsgBoxEx("No line items available for display", vbOKOnly, "Display Transaction Details", , , , , RGBMSGBox_PromptColour)
        End If
        AllowScan = True
        If txtStoreNo.Enabled = True Then
            If (txtStoreNo.Visible = True) And (txtStoreNo.Enabled = True) Then txtStoreNo.SetFocus
        ElseIf dtxtTranDate.Enabled Then
            If (dtxtTranDate.Visible = True) And (dtxtTranDate.Enabled = True) Then dtxtTranDate.SetFocus
        Else
            If (txtTillId.Visible = True) And (txtTillId.Enabled = True) Then txtTillId.SetFocus
        End If
        
        Screen.MousePointer = vbNormal
        cmdDetails.Enabled = True
        Exit Sub
    End If
    lblStoreNo.Caption = txtStoreNo.Text
    lblTranDate.Caption = DisplayDate(dteTrandate, False)
    lblTranNo.Caption = strTranNumber
    lblTillID.Caption = strTillID
    If (mblnSelectLine = True) Then lblAction.Caption = "Select line to refund"
    If ((mblnSelectLine = True) And (mstrPriceSKU <> "")) Or (mblnParkedOnly = True) Then
        If (mblnParkedOnly = False) Then lblAction.Caption = "Select line for Price Match/Promise"
        sprdDetails.Col = COL_DTL_QTY_RFND
        If (sprdDetails.ColHidden = False) Then
            lngXtraSpace = sprdDetails.ColWidth(COL_DTL_QTY_RFND) + sprdDetails.ColWidth(COL_DTL_QTY_REASON) + _
                 sprdDetails.ColWidth(COL_DTL_RFND_PRICE)
        End If
        sprdDetails.ColHidden = True
        sprdDetails.Col = COL_DTL_QTY_REASON
        sprdDetails.ColHidden = True
        sprdDetails.Col = COL_DTL_RFND_PRICE
        sprdDetails.ColHidden = True
        cmdRetrieveAll.Visible = False
        sprdDetails.OperationMode = OperationModeRow
        
        'If first time columns hidden then allocate space to other columns
        sprdDetails.Col = COL_DTL_QTY_TAKEN
        If (lngXtraSpace > 0 And sprdDetails.ColHidden = False) Then
            'get the number of visible columns to split the extra space up into
            For lngLineNo = 1 To sprdDetails.MaxCols Step 1
                sprdDetails.Col = lngLineNo
                If (sprdDetails.ColHidden = False) Then lngNoCols = lngNoCols + 1
            Next lngLineNo
            'split the extra space into the visible columns, except SKU which is added to Description
            'sprdDetails.ColWidth(COL_skuESC) = (lngXtraSpace / lngNoCols) + sprdDetails.ColWidth(COL_DESC)
            For lngLineNo = COL_PARTCODE To sprdDetails.MaxCols Step 1
                sprdDetails.Col = lngLineNo
                If (sprdDetails.ColHidden = False) Then sprdDetails.ColWidth(lngLineNo) = (lngXtraSpace / lngNoCols) + sprdDetails.ColWidth(lngLineNo)
            Next lngLineNo
        End If
            
    End If
    
    'Display lines on Receipt
    sprdDetails.MaxRows = 0
    sprdDetails.LeftCol = 1
    Set colLines = oTillTran.Lines
    lngNoLines = colLines.Count
    intLineOn = 1
    intOffset = 0
    For lngLineNo = 1 To lngNoLines Step 1
        Set oTranLine = colLines(lngLineNo)
        With oTranLine
            If ((mblnVerifySKU = True) And (LenB(strSKU) <> 0) And (strSKU = .PartCode)) Or _
                  (mblnVerifySKU = False) Then
                If ((.QuantitySold > 0) Or mblnParkedOnly) And (Not .LineReversed) And (.SaleType <> "W") Then
                    If Not colOrderLines Is Nothing Then
                        intLineOn = 1
                        For Each oOrderLineBO In colOrderLines
                            If oOrderLineBO.PartCode = oTranLine.PartCode And oTranLine.SequenceNo = (Val(oOrderLineBO.OrderLine) + intOffset) Then
                                blnFound = True
                                intOrderLineOn = Val(oOrderLineBO.OrderLine)
                                Exit For
                            End If
                            If intLineOn < colOrderLines.Count Then intLineOn = intLineOn + 1
                        Next
                        If blnFound = False Then
                            intOffset = intOffset + 1
                        End If
                        If intOrderLineOn > 0 Then Set oOrderLineBO = colOrderLines(intOrderLineOn)

                        intLineOn = intLineOn + 1
                    End If
                    Set oStockItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
                    Call oStockItem.IBo_AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, .PartCode)
                    Call oStockItem.IBo_AddLoadField(FID_INVENTORY_Description)
                    Call oStockItem.IBo_AddLoadField(FID_INVENTORY_RelatedNoItems)
                    Call oStockItem.IBo_AddLoadField(FID_INVENTORY_PRFSKU)
                    Call oStockItem.IBo_LoadMatches
                    strDesc = oStockItem.Description
                    sprdDetails.MaxRows = sprdDetails.MaxRows + 1
                    sprdDetails.Row = sprdDetails.MaxRows
                    sprdDetails.Col = COL_DTL_SKU
                    sprdDetails.Text = .PartCode
                    Skun = .PartCode
                    sprdDetails.Col = COL_DTL_DESC
                    sprdDetails.Text = strDesc
                    sprdDetails.Col = COL_DTL_QTY
                    sprdDetails.Text = .QuantitySold
                    If (lngBoughtQty = 0) Then lngBoughtQty = .QuantitySold
                    sprdDetails.Col = COL_DTL_PRICE
                    sprdDetails.Text = Format$((.ExtendedValue - .DealGroupMarginAmount - .MultiBuyMarginAmount - .HierarchyMarginAmount - .QtyBreakMarginAmount) / .QuantitySold, "0.00")
                    sprdDetails.Col = COL_DTL_TOTAL
                    sprdDetails.Text = Format$((.ExtendedValue - .DealGroupMarginAmount - .MultiBuyMarginAmount - .HierarchyMarginAmount - .QtyBreakMarginAmount), "0.00")
                    sprdDetails.Col = COL_DTL_WEEE_RATE
                    sprdDetails.Text = .WEEELineCharge
                    sprdDetails.Col = COL_DTL_LINENO
                    sprdDetails.Text = .SequenceNo
                    
                    If mstrDeliveryStatus <> "" And Not oOrderHdr Is Nothing Then
                        
                        'MO'C Ref 419 - Make sure line status is that the stock is not with the customer
                        sprdDetails.Col = COL_DTL_STATUS
                        
                        Set oOrderLineBO = colOrderLines(lngLineNo - intOffset)
                        sprdDetails.Text = oOrderLineBO.DeliveryRequestStatus
                        
                        'Set oOrderLineBO = colOrderLines(intLineOn)
                        If oTillTran.TranParked = True And .QuantitySold > 0 Then  ' If its a Parked Refund need to re do the selection process for refund
                            
                            sprdDetails.Col = COL_DTL_QTY_TAKEN
                            sprdDetails.Text = "0"
                            sprdDetails.Col = COL_DTL_QTY_TO_DELIVER
                            sprdDetails.Text = "0"
                        
                        Else
                        
                            sprdDetails.Col = COL_DTL_QTY_TAKEN
                            If oOrderHdr.DeliveryConfirmed = False Then
                                If (oOrderLineBO.DeliveryRequestStatus >= 700 And oOrderLineBO.DeliveryRequestStatus < 800) Or _
                               (oOrderLineBO.DeliveryRequestStatus >= 900 And oOrderLineBO.DeliveryRequestStatus < 1000) Then
                                    If oOrderLineBO.IsDeliveryChargeItem Then
                                        sprdDetails.Text = "0"
                                    Else
                                        sprdDetails.Text = oOrderLineBO.UnitsTaken 'oOrderLineBO.UnitsOrdered + oOrderLineBO.UnitsRefunded
                                    End If
                                Else
                                    sprdDetails.Text = oOrderLineBO.UnitsTaken
                                End If
                            Else
                                sprdDetails.Text = oOrderLineBO.UnitsTaken
                            End If
                            sprdDetails.Col = COL_DTL_QTY_TO_DELIVER
                            If (oOrderHdr.DeliveryConfirmed = True And oOrderLineBO.DeliveryRequestStatus > 699) Or (oOrderLineBO.UnitsTaken = oOrderLineBO.UnitsOrdered) Or oOrderLineBO.IsDeliveryChargeItem Then
                                sprdDetails.Text = "0"
                                mblnDelivered = True
                            Else
                                sprdDetails.Text = oOrderLineBO.UnitsOrdered - oOrderLineBO.UnitsTaken + oOrderLineBO.UnitsRefunded 'oOrderLineBO.QuantityToBeDelivered
                                If Val(sprdDetails.Text) < 0 Then sprdDetails.Text = "0"
                                mblnDelivered = False
                            End If
                            
                        End If
                        
                        If oTillTran.TranParked = True Then
                            sprdDetails.Col = COL_DTL_QTY_REFUNDED
                            sprdDetails.Text = "0"
                        Else
                            sprdDetails.Col = COL_DTL_QTY_REFUNDED
                            If oOrderLineBO.UnitsRefunded < 0 Then oOrderLineBO.UnitsRefunded = oOrderLineBO.UnitsRefunded * -1
                            sprdDetails.Text = oOrderLineBO.UnitsRefunded + AnyItemsRefundedInThisTransaction(Skun, CLng(intOrderLineOn))
                        End If
                                            
                        sprdDetails.Col = COL_DTL_IS_DELIVERY_CHARGE 'MO'C Ref 295
                        If oOrderLineBO.IsDeliveryChargeItem Then
                            sprdDetails.Text = "1"
                        Else
                            sprdDetails.Text = "0"
                        End If
                        
                    End If
                    sprdDetails.Col = COL_DTL_QTY_RFND
                    sprdDetails.Text = "0"
                    sprdDetails.Col = COL_DTL_QTY_REASON
                    sprdDetails.Text = ""
                    sprdDetails.Col = COL_DTL_BULK_ITEM
                    sprdDetails.Text = oStockItem.RelatedNoItems
                    sprdDetails.RowHeight(sprdDetails.MaxRows) = sprdDetails.MaxTextRowHeight(sprdDetails.MaxRows)
                    sprdDetails.Col = COL_DTL_WEEE_SKU
                    sprdDetails.Text = oStockItem.PRFSKU
                    sprdDetails.Col = COL_DTL_MARKDOWN
                    sprdDetails.Text = IIf(.MarkedDownStock, "True", "")
                    sprdDetails.Col = COL_DTL_STATUS
                Else
                    intOffset = intOffset + 1
                End If
            End If 'item must be displayed
        End With
           
Skip:
    Next lngLineNo
    If (mblnVerifySKU = True) And (LenB(strSKU) <> 0) And (mlngPriceQty > lngBoughtQty) And (lngBoughtQty <> 0) Then
        Call DebugMsg(MODULE_NAME, "cmdDetails_click", endlDebug, "Verify SKU" & strSKU & " Qty=" & lngBoughtQty)
        AllowScan = False
        Call MsgBoxEx("There was only " & lngBoughtQty & " of SKU - '" & strSKU & "' purchased on the selected receipt" & vbCrLf & "Quantity has been adjusted on Price Match to Purchased Quantity", vbOKOnly, "Price Match Quantity Exceeded", , , , , RGBMsgBox_WarnColour)
        AllowScan = True
        mlngPriceQty = lngBoughtQty
    End If
    If (lngNoLines > 0) And (sprdDetails.MaxRows = 0) Then
        If mblnVerifySKU Then
            AllowScan = False
            Call MsgBoxEx("SKU - '" & strSKU & "' was not present on selected receipt" & vbCrLf & "Reselect transaction and enter SKU", vbOKOnly, "Entered SKU not verified", , , , , RGBMsgBox_WarnColour)
            AllowScan = True
        Else
            AllowScan = False
            Call MsgBoxEx("No sale lines present in selected transaction" & vbCrLf & "Reselect transaction", vbOKOnly, "Invalid Transaction", , , , , RGBMsgBox_WarnColour)
            AllowScan = True
            Screen.MousePointer = vbNormal
            txtTillId.Text = vbNullString
            txtTranNo.Text = vbNullString
            If (txtTillId.Visible = True) And (txtTillId.Enabled = True) Then txtTillId.SetFocus
            cmdDetails.Enabled = True
            Exit Sub
        End If
    End If
    Screen.MousePointer = vbNormal
    If (sprdDetails.MaxRows > 0) Then
        fraDetails.Visible = True
        fraParked.Visible = False
        fraTran.Visible = False
        AllowScan = False
        'DoEvents
        sprdDetails.Col = COL_DTL_QTY_RFND
        If (sprdDetails.ColHidden = False) Then Call sprdDetails_LeaveCell(COL_DTL_QTY_RFND, 1, COL_DTL_QTY_RFND, 1, blnCancel)
        Call sprdDetails.SetActiveCell(COL_DTL_QTY_RFND, 1)
        If (sprdDetails.Visible = True) And (sprdDetails.Enabled = True) Then Call sprdDetails.SetFocus
    End If
    If (mblnVerifySKU = True) And (sprdDetails.MaxRows = 1) Then
        fraDetails.Visible = False
        fraParked.Visible = False
        fraTran.Visible = True
        AllowScan = True
        Call sprdDetails.SetActiveCell(1, 1)
        cmdDtlRetrieve.value = True
    End If
    
    If mblnParkedOnly Then
        If oTillTran.RecoveredFromParked Then
            AllowScan = False
            Call MsgBoxEx("Transaction has already been recovered" & vbNewLine & "Select another transaction", vbOKOnly, "Entered SKU not verified", , , , , RGBMsgBox_WarnColour)
            AllowScan = True
            cmdDtlCancel.value = True
            cmdDetails.Enabled = True
            Exit Sub
        End If
    End If
    
    cmdDetails.Enabled = True
    
    Set oOrderInfoBO = Nothing
    Set oOrderHdr = Nothing

End Sub

Private Sub cmdDtlCancel_Click()
    frmRefundPopup.isParentAlreadyHide = True
    mstrDeliveryStatus = ""
    lblAction.Caption = mstrActionMsg
    fraDetails.Visible = False
    fraParked.Visible = False
    fraTran.Visible = True
    AllowScan = True
    If (txtStoreNo.Enabled) And (txtStoreNo.Visible = True) Then
        txtStoreNo.SetFocus
    ElseIf (dtxtTranDate.Enabled = True) And (dtxtTranDate.Visible = True) Then
        dtxtTranDate.SetFocus
    Else
        If (txtTillId.Visible = True) And (txtTillId.Enabled = True) Then txtTillId.SetFocus
    End If
    
End Sub

Private Sub cmdDtlRetrieve_Click()

Dim strLineData   As String
Dim strStoreNo    As String
Dim strPartCode   As String
Dim dblQuantity   As Double
Dim curPrice      As Currency
Dim curWEEERate   As Currency
Dim dteTrandate   As Date
Dim strTillID     As String
Dim strTranNumber As String
Dim lngNoLines    As Long
Dim lngLineNo     As Long
Dim intItem       As Integer
    If (mblnDetailsLinesEditing = True) Then
        Exit Sub
    End If
    
    cmdDtlRetrieve.Enabled = False
    lblAction.Caption = mstrActionMsg
    If (mblnSelectLine = True) Then
        If (mstrPriceSKU <> "") Then
            sprdDetails.Row = sprdDetails.ActiveRow
            sprdDetails.Col = COL_DTL_SKU
            strPartCode = sprdDetails.Text
            sprdDetails.Col = COL_DTL_PRICE
            curPrice = Val(sprdDetails.value)
            sprdDetails.Col = COL_DTL_WEEE_RATE
            curWEEERate = Val(sprdDetails.value)
                        
            RaiseEvent SelectTransactionLine(strPartCode, 1, curPrice, curWEEERate, lblTranNo.Caption, GetDate(lblTranDate.Caption), lblTillID.Caption, 1, mblnVoidTransaction, False, GetOrderNumber)
        Else
            If (GetRefundItems = True) Then
                If mcolLines.Count = 0 Then
                    Call MsgBoxEx("No Refund quantity has been entered against any of the lines.", vbOKOnly, "No quantities entered", , , , , RGBMsgBox_WarnColour)
                    cmdDtlRetrieve.Enabled = True
                    Exit Sub
                End If
                'For intItem = 1 To mcol
                RaiseEvent SelectTransactionLines(lblTranNo.Caption, GetDate(lblTranDate.Caption), lblTillID.Caption, mcolLines, False, mblnVoidTransaction, GetOrderNumber)
            Else
                cmdDtlRetrieve.Enabled = True
                Exit Sub
            End If
        End If
    Else
        Set mcolLines = New Collection
        
        RaiseEvent Apply(lblStoreNo.Caption, GetDate(lblTranDate.Caption), lblTillID.Caption, lblTranNo.Caption)
    End If
    cmdDtlRetrieve.Enabled = True

End Sub

Private Sub cmdDtlTender_Click()
        
    RaiseEvent SelectTransactionLine(vbNullString, 0, 0, 0, vbNullString, Date, vbNullString, 0, False, True, GetOrderNumber)

End Sub

Private Sub cmdDtlVoid_Click()

    RaiseEvent SelectTransactionLine(vbNullString, 0, 0, 0, vbNullString, Date, vbNullString, 0, True, False, GetOrderNumber)

End Sub

Private Sub cmdList_Click()
    
Dim lngNoTrans As Long
Dim lngLineNo  As Long
Dim colTrans   As Collection
Dim oTillTran  As cPOSHeader
Dim oRetCust   As cReturnCust
Dim colCusts   As Collection
Dim strCusName As String
    
    If (cmdList.Visible = True) And (cmdList.Enabled = True) Then cmdList.SetFocus
    'DoEvents
    
    Screen.MousePointer = vbHourglass

    Set oTillTran = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    Call oTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, Date)
    Call oTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranParked, True)
    Call oTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_RecoveredFromParked, False)
    Call oTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_FromDCOrders, True)
    Call oTillTran.AddLoadField(FID_POSHEADER_TranDate)
    Call oTillTran.AddLoadField(FID_POSHEADER_TillID)
    Call oTillTran.AddLoadField(FID_POSHEADER_TransactionNo)
    Call oTillTran.AddLoadField(FID_POSHEADER_TransactionTime)
    Call oTillTran.AddLoadField(FID_POSHEADER_OrderNumber)
    Set colTrans = oTillTran.LoadMatches
    
    If colTrans.Count = 0 Then
        AllowScan = False
        Call MsgBoxEx("No parked transactions available", vbOKOnly, "Retrieve Parked Transaction", , , , , RGBMsgBox_WarnColour)
        AllowScan = True
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    fraDetails.Visible = False
    fraParked.Visible = True
    fraTran.Visible = False
    AllowScan = False
    
    Set oRetCust = goDatabase.CreateBusinessObject(CLASSID_RETURNCUST)
    Call lstParked.Clear
    For Each oTillTran In colTrans
        With oTillTran
            Call oRetCust.ClearLoadFilter
            Call oRetCust.ClearLoadField
            
            Call oRetCust.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionDate, .TranDate)
            Call oRetCust.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_PCTillID, .TillID)
            Call oRetCust.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionNumber, .TransactionNo)
            Call oRetCust.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_LineNumber, 0)
            
            Call oRetCust.IBo_AddLoadField(FID_RETURNCUST_CustomerName)
            
            'Added 26/4/05 - to handle if DLRCUS record missing then use 'No Name' instead
            Set colCusts = oRetCust.IBo_LoadMatches
            If (colCusts.Count > 0) Then
                Set oRetCust = oRetCust.IBo_LoadMatches.Item(1)
                strCusName = oRetCust.CustomerName
            Else
                strCusName = "No Name"
            End If
            
            Call lstParked.AddItem(strCusName & vbTab & .TillID & vbTab & _
                .TransactionNo & vbTab & Format$(Left$(.TransactionTime, 2) & _
                ":" & Mid$(.TransactionTime, 3, 2), "H:MM") & vbTab & .OrderNumber)
        End With
    Next oTillTran
    lstParked.ListIndex = 0
    If (lstParked.Visible = True) And (lstParked.Enabled = True) Then lstParked.SetFocus
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdNoProof_Click()

Dim strCode As String
        
    If ((mblnSelectLine = True) And (mstrPriceSKU <> "")) Then
        Call MsgBoxEx("Price Match/Promise is not allowed with no Proof of Purchase", vbOKOnly, _
            "No Proof of Purchase", , , , , RGBMSGBox_PromptColour)
        Exit Sub
    End If
    
    Load frmRefundCode
    frmRefundCode.CloseKeyCode = mlngCloseKeyCode
    frmRefundCode.UseKeyCode = mlngUseKeyCode
    strCode = frmRefundCode.GetRefundReasonCode(cmdDtlCancel.Caption, RefundReasons)
    Unload frmRefundCode
    If (strCode = "") Then Exit Sub
    
    RaiseEvent NoOriginalTran(strCode)

End Sub

Private Sub cmdOtherTran_Click()

Dim strCode  As String

Dim lngQty As Long
Dim curPrice    As Currency
Dim curWEEERate As Currency
    
    If (txtStoreNo.Text = Right$("000" & goSession.CurrentEnterprise.IEnterprise_StoreNumber, 3)) And (cmdOtherTran.Tag <> "SKIP") Then
        Call MsgBoxEx("Enter originating store number for Transaction", vbOKOnly, "Capture transaction details", , , , , RGBMsgBox_WarnColour)
        If (txtStoreNo.Visible = True) And (txtStoreNo.Enabled = True) Then txtStoreNo.SetFocus
        Exit Sub
    End If
    cmdOtherTran.Tag = ""
    
    'for Price Match/Promise - ensure Tran Details filled in and get Price
    If (mstrPriceSKU <> "") And (GetDate(dtxtTranDate.Text) < mdtePPValidFrom) Then
        Call MsgBoxEx("Transaction Date is before acceptable limit for Price Match/Promise", vbOKOnly, "Capture transaction details", , , , , RGBMsgBox_WarnColour)
        If (dtxtTranDate.Visible = True) And (dtxtTranDate.Enabled = True) Then dtxtTranDate.SetFocus
        Exit Sub
    End If
    
    If (txtTillId.Text = "") Then
        Call MsgBoxEx("Till number for originating transaction not entered.", vbOKOnly, "Capture transaction details", , , , , RGBMsgBox_WarnColour)
        If (txtTillId.Visible = True) And (txtTillId.Enabled = True) Then txtTillId.SetFocus
        Exit Sub
    End If
    If (txtTranNo.Text = "") Then
        Call MsgBoxEx("Transaction number for originating transaction not entered.", vbOKOnly, "Capture transaction details", , , , , RGBMsgBox_WarnColour)
        If (txtTranNo.Visible = True) And (txtTranNo.Enabled = True) Then txtTranNo.SetFocus
        Exit Sub
    End If
    
    If (mstrPriceSKU <> "") Then 'performing price promise validation
        'After check mades, prompt to get unit price
        Load frmEntry
'        curPrice = Val(InputBoxEx("Enter original unit price from receipt for item.", "Confirm price", "0.00", enifNumeric, False, vbYellow, -2, 7))
'        If (ucnumpad1.Visible = True) Then frmEntry.ShowNumPad
        curPrice = frmEntry.GetUnitPrice
        Unload frmEntry
        If (curPrice = 0) Then Exit Sub
        RaiseEvent SelectTransactionLine(mstrPriceSKU, 1, curPrice, curWEEERate, txtTranNo.Text, GetDate(dtxtTranDate.Text), txtTillId.Text, 1, False, False, GetOrderNumber)
    Else
        'for Other Store refund - get Refund Reason
        Load frmRefundCode
        frmRefundCode.CloseKeyCode = mlngCloseKeyCode
        strCode = frmRefundCode.GetRefundReasonCode(cmdDtlCancel.Caption, RefundReasons)
        Unload frmRefundCode
        If (strCode = "") Then Exit Sub
        RaiseEvent OtherStoreTran(txtStoreNo.Text, dtxtTranDate.Text, txtTillId.Text, txtTranNo.Text, strCode)
    End If

End Sub

Private Sub cmdParkCancel_Click()

    Call Reset

End Sub

Private Sub cmdParkRetrieve_Click()
    
Dim varLine As Variant

    varLine = Split(lstParked.List(lstParked.ListIndex), vbTab)
    
    dtxtTranDate.Text = Format$(Now(), "DD/MM/YY")
    txtTillId.Text = varLine(1)
    txtTranNo.Text = varLine(2)
    
    fraParked.Visible = False
    fraTran.Visible = True
    AllowScan = True
    cmdDetails.Visible = True
    cmdDetails.value = True

End Sub

Private Sub cmdReset_Click()

    Call Reset
    If (txtStoreNo.Enabled) And (txtStoreNo.Visible = True) Then
        txtStoreNo.SetFocus
    ElseIf dtxtTranDate.Enabled And (dtxtTranDate.Visible = True) Then
        dtxtTranDate.SetFocus
    Else
        If (txtTillId.Visible = True) And (txtTillId.Enabled = True) Then txtTillId.SetFocus
    End If

End Sub

Public Sub SelectTransactionLine(blnSelectLine As Boolean)

    txtStoreNo.Text = goSession.CurrentEnterprise.IEnterprise_StoreNumber
    mblnSelectLine = blnSelectLine
    mblnSalesOnly = True

End Sub

Private Sub cmdRetrieveAll_Click()

Dim lngRowNo            As Long
Dim strCode             As String
Dim lngQty              As Long
Dim curPrice            As Currency
Dim lngAlreadyRefunded  As Long
Dim Skun                As String
Dim intQtyToDeliver     As Integer
Dim intQty              As Integer
Dim intTaken            As Integer
Dim intRefund           As Integer
Dim strMsg              As String

    If (mblnDetailsLinesEditing = True) Then
        Exit Sub
    End If

    'CR0017 - You must void a refund transaction if there are lines that have been dispatched when retreiving all lines
    If mblnVoidRetrieveAll Then
        mblnVoidTransaction = True
        strMsg = goSession.GetParameter(PRM_ALREADY_DESPATCHED_MSG1) & goSession.GetParameter(PRM_ALREADY_DESPATCHED_MSG2) & goSession.GetParameter(PRM_ALREADY_DESPATCHED_MSG3) & goSession.GetParameter(PRM_ALREADY_DESPATCHED_MSG4)
        Call MsgBoxEx(strMsg, vbOKOnly, "A Line has been Dispatched", , , , , RGBMsgBox_WarnColour)
        Set mcolLines = New Collection
        RaiseEvent SelectTransactionLines(lblTranNo.Caption, GetDate(lblTranDate.Caption), lblTillID.Caption, mcolLines, True, mblnVoidTransaction, GetOrderNumber)
        Exit Sub
    End If
    
    Load frmRefundCode
    frmRefundCode.CloseKeyCode = mlngCloseKeyCode
    strCode = frmRefundCode.GetRefundReasonCode(cmdDtlCancel.Caption, RefundReasons)
    Unload frmRefundCode
    If (strCode = "") Then Exit Sub
    
    'Force Refund Code and Quantities into all lines displayed
    For lngRowNo = 1 To sprdDetails.MaxRows Step 1
        sprdDetails.Row = lngRowNo
        sprdDetails.Col = COL_DTL_QTY
        lngQty = Val(sprdDetails.value)
        sprdDetails.Col = COL_DTL_QTY_REFUNDED
        lngAlreadyRefunded = Val(sprdDetails.value)
        sprdDetails.Col = COL_DTL_SKU
        Skun = sprdDetails.Text
        sprdDetails.Col = COL_DTL_QTY_RFND
        sprdDetails.value = lngQty - GetAlreadyRefundedLines(lngRowNo, Skun) - AnyItemsRefundedInThisTransaction(Skun, lngRowNo)

        sprdDetails.Col = COL_DTL_QTY_REASON
        sprdDetails.CellType = CellTypeComboBox
        sprdDetails.TypeComboBoxList = strCode
        sprdDetails.Text = strCode
        'Added 2/5/07 - fix to move price to refund price if not already set
        sprdDetails.Col = COL_DTL_PRICE
        curPrice = Val(sprdDetails.Text)
        sprdDetails.Col = COL_DTL_RFND_PRICE
        sprdDetails.Text = curPrice
        
    Next
    
    Set mcolLines = New Collection
    Call GetRefundItems(True)
    RaiseEvent SelectTransactionLines(lblTranNo.Caption, GetDate(lblTranDate.Caption), lblTillID.Caption, mcolLines, True, mblnVoidTransaction, GetOrderNumber)

End Sub

Private Function GetRefundItems(Optional blnRetrieveAll As Boolean = False) As Boolean

Dim lngRowNo        As Long
Dim cLine           As cRefundLine
Dim oPriceCodeBO    As cPriceOverrideCode
Dim curUnitPrice    As Currency
Dim strMsg          As String
Dim intRefunded     As String
Dim intQty          As Integer
Dim intTaken        As Integer
Dim LineStatus      As Integer
    
    GetRefundItems = True
    If Not PriceOverrideCodes Is Nothing Then
        Set oPriceCodeBO = PriceOverrideCodes(1)
    End If
    mstrMarkDownMsg = ""
    
    Set mcolLines = New Collection
    For lngRowNo = 1 To sprdDetails.MaxRows Step 1
        sprdDetails.Row = lngRowNo
        sprdDetails.Col = COL_DTL_QTY_RFND
        If (Val(sprdDetails.value) > 0) Then
            Set cLine = New cRefundLine
            sprdDetails.Col = COL_DTL_SKU
            cLine.PartCode = sprdDetails.Text
            'check if Bulk Item and use this SKU instead
            sprdDetails.Col = COL_DTL_SINGLE_BULK
            If (sprdDetails.Text = "S") Then
                sprdDetails.Col = COL_DTL_SINGLE_SKU
                cLine.PartCode = sprdDetails.Text
                sprdDetails.Col = COL_DTL_PRICE
                curUnitPrice = Val(sprdDetails.Text)
                sprdDetails.Col = COL_DTL_SINGLE_PACKSIZE
                curUnitPrice = Round(curUnitPrice / Val(sprdDetails.Text), 2)
            Else
                sprdDetails.Col = COL_DTL_PRICE
                curUnitPrice = Val(sprdDetails.Text)
            End If
                
            sprdDetails.Col = COL_DTL_QTY_RFND
            cLine.RefundQuantity = sprdDetails.Text
            sprdDetails.Col = COL_DTL_LINENO
            cLine.LineNo = sprdDetails.Text
            sprdDetails.Col = COL_DTL_RFND_PRICE
            cLine.Price = Val(sprdDetails.Text)
            sprdDetails.Col = COL_DTL_QTY_REASON
            cLine.RefundReasonCode = Left$(sprdDetails.Text, 2)
            sprdDetails.Col = COL_DTL_WEEE_RATE
            cLine.WEEEUnitCharge = Val(sprdDetails.value)
            sprdDetails.Col = COL_DTL_WEEE_SKU
            cLine.WEEESKU = sprdDetails.value
              
            'added 5/10/06 - check that reason code entered else clear and exit
            sprdDetails.Col = COL_DTL_CANCELLED
            cLine.QtyCancelled = Val(sprdDetails.value)
            sprdDetails.Col = COL_DTL_QTY
            intQty = Val(sprdDetails.value)
            sprdDetails.Col = COL_DTL_QTY_TAKEN
            intTaken = Val(sprdDetails.value)
          
            sprdDetails.Col = COL_DTL_QTY_TO_DELIVER
            cLine.RefundDeliveryQty = Val(sprdDetails.value) - cLine.QtyCancelled
          
            If cLine.RefundDeliveryQty < 0 Then cLine.RefundDeliveryQty = 0
            
            sprdDetails.Col = COL_DTL_QTY_REFUNDED
            intRefunded = Val(sprdDetails.value)
            
            sprdDetails.Col = COL_DTL_RETURNED
            If blnRetrieveAll Then
                sprdDetails.Col = COL_DTL_QTY_TO_DELIVER
                cLine.QtyCancelled = Val(sprdDetails.value)
                sprdDetails.Col = COL_DTL_IS_DELIVERY_CHARGE
                If sprdDetails.Text = "1" Then
                    cLine.QtyCancelled = 0
                    cLine.QtyReturned = intQty
                Else
                    cLine.RefundDeliveryQty = 0
                    sprdDetails.Col = COL_DTL_QTY_TAKEN
                    cLine.RefundTakenQty = Val(sprdDetails.value)
                    'MO'C Ref 416 - Refund all Problem fix
                    If cLine.RefundTakenQty = 0 And cLine.QtyCancelled = 0 Then
                        cLine.QtyReturned = cLine.RefundQuantity
                    Else
                        cLine.QtyReturned = cLine.RefundTakenQty
                        'cLine.QtyCancelled = 0
                    End If
                End If
            Else
                cLine.QtyReturned = Val(sprdDetails.value)
                If (cLine.QtyCancelled + cLine.QtyReturned) <> cLine.RefundQuantity Then
                    cLine.QtyCancelled = cLine.RefundQuantity
                    If cLine.RefundQuantity > cLine.RefundDeliveryQty Then
                        cLine.QtyCancelled = cLine.RefundDeliveryQty
                        cLine.QtyReturned = cLine.RefundQuantity - cLine.QtyCancelled
                    End If
                End If
                
                sprdDetails.Col = COL_DTL_QTY_TAKEN
                If Val(sprdDetails.Text) > 0 Then
                    cLine.RefundTakenQty = Val(sprdDetails.value) - cLine.QtyReturned
                End If
            End If
            
            'sprdDetails.Col = COL_DTL_MARKDOWN
            If (cLine.RefundReasonCode = "") Then
                Set mcolLines = New Collection
                Call MsgBoxEx("No Refund Reason entered for line" & vbNewLine & "Select reason from list to process", vbInformation, "Missing information", , , , , RGBMsgBox_WarnColour)
                Call sprdDetails_EditMode(COL_DTL_QTY_RFND, sprdDetails.Row, 0, True)
                GetRefundItems = False
                Exit Function
            End If
            
            If (curUnitPrice <> cLine.Price) Then cLine.PriceOverrideReason = oPriceCodeBO.Code
            cLine.OrderNumber = mstrOrderNo
            
            'Fix to stop web service from making IBTs for DElivered stock
            sprdDetails.Col = COL_DTL_STATUS
            LineStatus = Val(sprdDetails.value)
            If cLine.QtyCancelled > 0 And LineStatus = DELIVERY_STATUS_COMPLETED Then
                cLine.QtyReturned = cLine.QtyReturned + cLine.QtyCancelled
                cLine.QtyCancelled = 0
            End If
                        
            sprdDetails.Col = COL_DTL_QTY
            If Val(sprdDetails.value) - intRefunded >= 0 Then 'Is there anything to refund
                Call mcolLines.Add(cLine)
            End If
        End If
    Next lngRowNo
    
End Function

Private Sub cmdCancel_Click()
    mstrDeliveryStatus = ""
    RaiseEvent Cancel

End Sub

Private Sub lstParked_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        cmdParkRetrieve.value = True
    End If 'Enter has been pressed to select entry
    If (KeyAscii = vbKeyEscape) Then
        cmdParkCancel.value = True
    End If

End Sub

Private Sub sprdDetails_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)

Dim lngQty      As Long
Dim lngAlreadyRefunded As Long
Dim curPrice    As Currency
Dim blnCancel   As Boolean
Dim strValue    As String
Dim oRelItemBO  As cRelatedItem
Dim oRetLine    As cRefundLine
Dim LineStatus  As Integer
Dim strMsg      As String
Dim strPartCode As String
Dim curWEEERate As Currency
Dim lngTotalOrdered As Long

    mblnDetailsLinesEditing = True
    mblnRefundDeterminationScreenHasShown = False
    
    If (Mode = 1) And (Col = COL_DTL_RFND_PRICE) Then
        sprdDetails.Row = Row
        sprdDetails.Col = Col
        sprdDetails.CellType = CellTypeEdit
        sprdDetails.TypeHAlign = TypeHAlignRight
        sprdDetails.TypeMaxEditLen = 10
        sprdDetails.TypeEditCharSet = TypeEditCharSetNumeric
    End If
    If (Mode = 0) And (ChangeMade = False) And (Col = COL_DTL_QTY_REASON) Then
        Call sprdDetails.SetActiveCell(COL_DTL_RFND_PRICE, Row)
        GoTo Sub_Exit
    End If
    If (Mode = 0) And (ChangeMade = True) Then
        sprdDetails.Row = Row
        Select Case (Col)
            Case (COL_DTL_QTY_RFND):
                'WIX 1380 Hubs Deliveries
                sprdDetails.Col = COL_DTL_QTY
                lngTotalOrdered = sprdDetails.value
                sprdDetails.Col = COL_DTL_QTY_RFND
                lngQty = sprdDetails.value
                sprdDetails.Col = COL_DTL_QTY_REFUNDED
                lngAlreadyRefunded = Val(sprdDetails.value)
                sprdDetails.Col = COL_DTL_STATUS
                LineStatus = Val(sprdDetails.Text)
                sprdDetails.Col = COL_DTL_QTY
                'Referral 784 - Live store issue - don't check against normal refund
                If mblnShowQodRefundScreen = True Then
                    If (lngQty + lngAlreadyRefunded) > sprdDetails.value And LineStatus < DELIVERY_STATUS_COMPLETED Then
                        Call MsgBoxEx("Cannot refund more than was purchased.", vbOKOnly, "Invalid Operation", , , , , RGBMsgBox_WarnColour)
                        sprdDetails.Col = COL_DTL_QTY_RFND
                        sprdDetails.value = 0
                        GoTo Sub_Exit
                    End If
                End If
                
                'Wix 1380 - Hub Deliveries
                sprdDetails.Col = COL_DTL_QTY_TO_DELIVER
                If sprdDetails.ColHidden = False Then
                    Load frmRefundPopup
                    sprdDetails.Col = COL_DTL_QTY_TO_DELIVER
                    If sprdDetails.value > 0 Then
                    
                        frmRefundPopup.mintCheckCancel = sprdDetails.value
                        sprdDetails.Col = COL_DTL_QTY_TAKEN
                        frmRefundPopup.mintCheckRefund = sprdDetails.value
                        sprdDetails.Col = COL_DTL_QTY_RFND
                        frmRefundPopup.mintCancellation = sprdDetails.value
                        
                        'Only show determination screen if there are both taken and to be delivered quantities
                        If frmRefundPopup.mintCheckRefund > 0 And frmRefundPopup.mintCheckCancel > 0 Then
                            frmRefundPopup.mintDeliveryStatus = LineStatus
                            frmRefundPopup.Show (vbModal)
                            mblnRefundDeterminationScreenHasShown = True
                            mlngRefunded = frmRefundPopup.mintRefunded
                            mlngCancelled = frmRefundPopup.mintCancellation
                            
                            If frmRefundPopup.mblnCancel = True Then
                                sprdDetails.value = 0
                                Unload frmRefundPopup
                                Set frmRefundPopup = Nothing
                                GoTo Sub_Exit
                            End If
                            
                            If mlngCancelled <= frmRefundPopup.mintCheckCancel And mlngRefunded <= frmRefundPopup.mintCheckRefund Then
                                sprdDetails.Col = COL_DTL_QTY_RFND
                                sprdDetails.value = mlngCancelled + mlngRefunded
                                sprdDetails.Col = COL_DTL_CANCELLED
                                sprdDetails.value = mlngCancelled
                                sprdDetails.Col = COL_DTL_RETURNED
                                sprdDetails.value = mlngRefunded
                            End If
                        Else
                            sprdDetails.Col = COL_DTL_QTY_RFND
                            mlngRefunded = sprdDetails.value
                            If frmRefundPopup.mintCheckCancel > 0 And mblnVoidTransaction = False Then
                                sprdDetails.Col = COL_DTL_CANCELLED
                                sprdDetails.value = mlngRefunded
                            Else
                                sprdDetails.Col = COL_DTL_RETURNED
                                sprdDetails.value = mlngRefunded
                            End If
                        End If
                        Unload frmRefundPopup
                        Set frmRefundPopup = Nothing
                    Else
                        sprdDetails.Col = COL_DTL_QTY_RFND
                        mlngRefunded = sprdDetails.value
                        sprdDetails.Col = COL_DTL_RETURNED
                        sprdDetails.value = mlngRefunded
                    End If
                End If
                
                'Ref 592 - Double Check that the value cancelled is not at a despatched state 700-799
                If mlngCancelled > 0 Then
                    sprdDetails.Col = COL_DTL_STATUS
                    LineStatus = Val(sprdDetails.Text)
                    'CR0017 - if the line is despatched then we must void the transaction
                    If (LineStatus >= DELIVERY_STATUS_PICKCONFIRM_CREATED And LineStatus < DELIVERY_STATUS_UNDELIVERED_CREATED) Then
                        strMsg = goSession.GetParameter(PRM_ALREADY_DESPATCHED_MSG1) & goSession.GetParameter(PRM_ALREADY_DESPATCHED_MSG2) & goSession.GetParameter(PRM_ALREADY_DESPATCHED_MSG3) & goSession.GetParameter(PRM_ALREADY_DESPATCHED_MSG4)
                        Call MsgBoxEx(strMsg, vbOKOnly, "Line Dispatched", , , , , RGBMsgBox_WarnColour)
                        mblnVoidTransaction = True
                        sprdDetails.Row = sprdDetails.ActiveRow
                        sprdDetails.Col = COL_DTL_SKU
                        strPartCode = sprdDetails.Text
                        sprdDetails.Col = COL_DTL_PRICE
                        curPrice = Val(sprdDetails.value)
                        sprdDetails.Col = COL_DTL_WEEE_RATE
                        curWEEERate = Val(sprdDetails.value)
                        RaiseEvent SelectTransactionLine(strPartCode, 1, curPrice, curWEEERate, lblTranNo.Caption, GetDate(lblTranDate.Caption), lblTillID.Caption, 1, mblnVoidTransaction, False, GetOrderNumber)
                        GoTo Sub_Exit
                    End If
                                
                End If
                
                'Phils Referral/Change - If line status is at 999 then move Cancelled quantity to Returned
                If (LineStatus = DELIVERY_STATUS_COMPLETED) Then
                    mlngCancelled = 0
                    sprdDetails.Col = COL_DTL_RETURNED
                    sprdDetails.value = mlngRefunded
                    sprdDetails.Col = COL_DTL_CANCELLED
                    sprdDetails.value = mlngCancelled

                End If
                
                'Check if item is a bulk item and if so get Single SKU and prompt if working with Single or Bulk Item
                sprdDetails.Col = COL_DTL_BULK_ITEM
                If (Val(sprdDetails.Text) > 0) Then
                    sprdDetails.Col = COL_DTL_SINGLE_SKU
                    If (sprdDetails.Text = "") Then
                        sprdDetails.Col = COL_DTL_SKU
                        Set oRelItemBO = goDatabase.CreateBusinessObject(CLASSID_RELATEDITEM)
                        Call oRelItemBO.IBo_AddLoadFilter(CMP_EQUAL, FID_RELATEDITEM_BulkItemNo, sprdDetails.Text)
                        Call oRelItemBO.LoadMatches
                        sprdDetails.Col = COL_DTL_SINGLE_SKU
                        sprdDetails.Text = oRelItemBO.ItemNo
                        sprdDetails.Col = COL_DTL_SINGLE_PACKSIZE
                        sprdDetails.Text = oRelItemBO.SinglesPerPack
                        Set oRelItemBO = Nothing
                    End If
                    
                    sprdDetails.Col = COL_DTL_QTY_TO_DELIVER
                    If Val(sprdDetails.Text) = 0 Then
                        sprdDetails.Col = COL_DTL_SINGLE_BULK
                        Load frmBulkOrSingle
                        Call frmBulkOrSingle.Show(vbModal)
                        If (frmBulkOrSingle.SingleItem = True) Then
                            sprdDetails.Text = "S"
                            sprdDetails.Col = COL_DTL_PRICE
                            curPrice = Val(sprdDetails.Text)
                            sprdDetails.Col = COL_DTL_SINGLE_PACKSIZE
                            curPrice = Round(curPrice / Val(sprdDetails.Text), 2)
                        Else
                            sprdDetails.Text = "B"
                            sprdDetails.Col = COL_DTL_PRICE
                            curPrice = Val(sprdDetails.Text)
                        End If
                        Unload frmBulkOrSingle
                    Else
                        sprdDetails.Col = COL_DTL_SINGLE_BULK
                        sprdDetails.Text = "B"
                        sprdDetails.Col = COL_DTL_PRICE
                        curPrice = Val(sprdDetails.Text)
                    End If
                    sprdDetails.Col = COL_DTL_RFND_PRICE
                    sprdDetails.Text = curPrice
                End If
                                

                sprdDetails.Col = COL_DTL_QTY
                lngQty = Val(sprdDetails.value)
                sprdDetails.Col = COL_DTL_SINGLE_BULK
                If (sprdDetails.Text = "S") Then
                    sprdDetails.Col = COL_DTL_SINGLE_PACKSIZE
                    lngQty = lngQty * Val(sprdDetails.Text)
                End If
                sprdDetails.Col = COL_DTL_QTY_RFND
                If (lngQty < Val(sprdDetails.value)) Then
                    Call MsgBoxEx("Refund quantity is higher than purchased quantity.  Refund quantity changed to purchased quantity.", vbOKOnly, "Refund Quantity Exceeded", , , , , RGBMsgBox_WarnColour)
                    sprdDetails.value = lngQty
                End If
                sprdDetails.Col = COL_DTL_QTY_RFND
                If (Val(sprdDetails.value) > 0) Then
                    sprdDetails.Col = COL_DTL_QTY_REASON
                    sprdDetails.Lock = False
                    If (sprdDetails.Text = "") Then
                        sprdDetails.CellType = CellTypeComboBox
                        sprdDetails.TypeComboBoxList = mstrRefundCodes
                        sprdDetails.Text = Left$(mstrRefundCodes, InStr(mstrRefundCodes, vbTab) - 1)
                    End If
                    sprdDetails.Col = COL_DTL_PRICE
                    curPrice = sprdDetails.value
                    sprdDetails.Col = COL_DTL_RFND_PRICE
                    If (sprdDetails.Text = "") Then sprdDetails.value = curPrice
                    sprdDetails.Lock = False
                    sprdDetails.Col = COL_DTL_QTY_REASON
                End If
                sprdDetails.Col = COL_DTL_QTY_RFND
                If (Val(sprdDetails.value) = 0) Then
                    sprdDetails.Col = COL_DTL_QTY_REASON
                    sprdDetails.Text = ""
                    sprdDetails.Lock = True
                    sprdDetails.Col = COL_DTL_RFND_PRICE
                    sprdDetails.Text = ""
                    sprdDetails.Lock = True
                Else
                    Call sprdDetails.SetActiveCell(COL_DTL_QTY_REASON, Row)
                    sprdDetails.EditMode = True
                    Call SendKeys("%{Down}")
                End If
            Case (COL_DTL_QTY_REASON):
                    Call sprdDetails.SetActiveCell(COL_DTL_RFND_PRICE, Row)
                    Call sprdDetails_LeaveCell(COL_DTL_QTY_RFND, 1, COL_DTL_RFND_PRICE, 1, blnCancel)
            Case (COL_DTL_RFND_PRICE):
                sprdDetails.Col = COL_DTL_RFND_PRICE
                strValue = sprdDetails.Text
                If (InStr(strValue, ".") = 0) Then sprdDetails.Text = Val(strValue) / 100
                sprdDetails.CellType = CellTypeNumber
                sprdDetails.value = Abs(sprdDetails.value)
                strValue = sprdDetails.value
                'if value has changed then check if not original price and ask for auth
                sprdDetails.Col = COL_DTL_PRICE
                If (strValue <> sprdDetails.Text) Then
                    Load frmVerifyPwd
                    If (frmVerifyPwd.VerifySupervisorPassword = False) Then
                        strValue = sprdDetails.Text
                        sprdDetails.Col = COL_DTL_RFND_PRICE
                        sprdDetails.Text = strValue
                    End If
                    Unload frmVerifyPwd
                End If

            End Select
    End If
Sub_Exit:
    mblnDetailsLinesEditing = False

End Sub

Private Sub sprdDetails_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) And (sprdDetails.ActiveCol < COL_DTL_QTY_RFND) And (sprdDetails.EditMode = False) Then
        sprdDetails.Col = COL_DTL_QTY_RFND
        sprdDetails.Row = sprdDetails.ActiveRow
        If (sprdDetails.Lock = False) Then
            Call sprdDetails.SetActiveCell(COL_DTL_QTY_RFND, sprdDetails.Row)
            sprdDetails.EditMode = True
        End If
    End If
    
    If (sprdDetails.ActiveCol = COL_DTL_QTY_REASON) And (sprdDetails.EditMode = False) Then
        sprdDetails.Col = sprdDetails.ActiveCol
        sprdDetails.Row = sprdDetails.ActiveRow
        If (sprdDetails.Lock = True) Then
            Call sprdDetails.SetActiveCell(COL_DTL_QTY_RFND, sprdDetails.Row)
            sprdDetails.EditMode = True
        End If
        
    End If

End Sub

Private Sub sprdDetails_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)

    If (NewCol < COL_DTL_QTY_RFND) And (mblnVerifySKU = False) Then
        Cancel = True
    End If
    
End Sub

Private Sub txtTranNo_Change()

    cmdReset.Visible = True

End Sub

Private Sub txtTranNo_GotFocus()

    txtTranNo.SelStart = 0
    txtTranNo.SelLength = Len(txtTranNo.Text)
    txtTranNo.BackColor = RGBEdit_Colour

End Sub

Private Sub txtTranNo_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        If LenB(txtTranNo.Text) <> 0 Then txtTranNo.Text = Format$(Val(txtTranNo.Text), "0000")
        If LenB(txtTillId.Text) <> 0 And LenB(txtTranNo.Text) <> 0 Then
            cmdDetails.Visible = True
            cmdDetails.value = True
            Exit Sub
        End If
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

    Call EnforceNumeric(KeyAscii)

End Sub

Private Sub txtTranNo_LostFocus()

    txtTranNo.BackColor = RGB_WHITE
    
End Sub

Private Sub txtStoreNo_Change()

    cmdReset.Visible = True

End Sub

Private Sub txtStoreNo_GotFocus()

    txtStoreNo.SelStart = 0
    txtStoreNo.SelLength = Len(txtStoreNo.Text)
    txtStoreNo.BackColor = RGBEdit_Colour

End Sub

Private Sub txtStoreNo_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If
    
    Call EnforceNumeric(KeyAscii)

End Sub

Private Sub txtStoreNo_LostFocus()

    txtStoreNo.BackColor = RGB_WHITE
    
End Sub

Private Sub dtxtTranDate_Change()

    cmdReset.Visible = True

End Sub

Private Sub dtxtTranDate_GotFocus()

    dtxtTranDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtTranDate_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub dtxtTranDate_LostFocus()

    dtxtTranDate.BackColor = RGB_WHITE
    
End Sub

Private Sub txtTillID_Change()

    cmdReset.Visible = True

End Sub

Private Sub txtTillID_GotFocus()

    txtTillId.SelStart = 0
    txtTillId.SelLength = Len(txtTillId.Text)
    txtTillId.BackColor = RGBEdit_Colour

End Sub

Private Sub txtTillID_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

    Call EnforceNumeric(KeyAscii)

End Sub

Private Sub txtTillID_LostFocus()

    txtTillId.BackColor = RGB_WHITE
    If LenB(txtTillId.Text) <> 0 Then txtTillId.Text = Format$(Val(txtTillId.Text), "00")
    
End Sub

' Hubs 2.0
' New Web Order Number entry box
Private Sub txtWebOrderNo_Change()

    If Len(txtWebOrderNo.Text) = 0 Then
        cmdNoProof.Visible = True
        cmdOtherTran.Visible = True
    Else
        cmdNoProof.Visible = False
        cmdOtherTran.Visible = False
    End If
    cmdReset.Visible = True
End Sub

Private Sub txtWebOrderNo_GotFocus()

    With txtWebOrderNo
        .SelStart = 0
        .SelLength = Len(.Text)
        .BackColor = RGBEdit_Colour
    End With
End Sub

Private Sub txtWebOrderNo_KeyPress(KeyAscii As Integer)
    Dim OrderManagerWebService As COMOrderManager.WebService
    Dim Store As String, Till As String, TranDate As String
    Dim TranNo As String, ErrorMess As String, Mess As String
    Dim OldStore As String, OldDate As String
    Dim StoreNo As String
    Dim DateTran As Date
    Dim Errors() As String
    Dim ErrCount As Integer

    Call DebugMsg(MODULE_NAME, "txtWebOrderNo_KeyPress", endlTraceIn)
    If (KeyAscii = vbKeyReturn) Then
        If LenB(txtWebOrderNo.Text) <> 0 Then
            ' Clear down any existing store etc data; Referral 770 - keeping any existing store and date to replace if there was an error
            OldStore = txtStoreNo.Text
            txtStoreNo.Text = ""
            OldDate = dtxtTranDate.Text
            dtxtTranDate.Text = ""
            txtTillId.Text = ""
            txtTranNo.Text = ""
            ' Find Venda transaction here
            Call DebugMsg(MODULE_NAME, "txtWebOrderNo_KeyPress", endlDebug, "Creating Order Manager Webservice caller")
            Set OrderManagerWebService = New COMOrderManager.WebService
        On Error GoTo Failed
            If Not OrderManagerWebService Is Nothing Then
                Call DebugMsg(MODULE_NAME, "txtWebOrderNo_KeyPress", endlDebug, "Created Order Manager Webservice caller")
                With OrderManagerWebService
                    If .WebServiceIsAvailable Then
                        Call DebugMsg(MODULE_NAME, "txtWebOrderNo_KeyPress", endlDebug, "Calling Order Manager Source Transaction Lookup")
                        If .OMSourceTransactionLookup(txtWebOrderNo.Text, Store, Till, TranDate, TranNo, ErrorMess) Then
                            Call DebugMsg(MODULE_NAME, "txtWebOrderNo_KeyPress", endlDebug, "Order Manager Source Transaction Lookup success: Store=" & Store & ", Till=" & Till & ", Tran Date=" & TranDate & ", Tran No=" & TranNo)
                            If IsOMStore(Store, StoreNo) Then
                                txtStoreNo.Text = StoreNo
                                If IsOMDate(TranDate, DateTran) Then
                                    dtxtTranDate.Text = Format$(DateTran, "dd/mm/yy")
                                    If IsNumeric(Till) Then
                                        txtTillId.Text = Format$(Val(Till), "00")
                                        If IsNumeric(TranNo) Then
                                            txtTranNo.Text = Format$(Val(TranNo), "0000")
                                            Call ManageWebOrderNumber
                                            Exit Sub
                                        Else
                                            Call MsgBoxEx("'Order Manager' returned an invalid Transaction Number - " & TranNo, vbExclamation, "Invalid Transaction Number", , , , , RGBMsgBox_WarnColour)
                                        End If
                                    Else
                                        Call MsgBoxEx("'Order Manager' returned an invalid Till ID - " & Till, vbExclamation, "Invalid Till ID", , , , , RGBMsgBox_WarnColour)
                                    End If
                                Else
                                    Call MsgBoxEx("'Order Manager' returned an invalid Transaction Date - " & TranDate, vbExclamation, "Invalid Transaction Date", , , , , RGBMsgBox_WarnColour)
                                End If
                            Else
                                Call MsgBoxEx("'Order Manager' returned an invalid Store Number - " & Store, vbExclamation, "Invalid Transaction Store Number", , , , , RGBMsgBox_WarnColour)
                            End If
                        Else
                            Call DebugMsg(MODULE_NAME, "txtWebOrderNo_KeyPress", endlDebug, "Failed calling Order Manager Source Transaction Lookup.  Error=" & ErrorMess)
                            Errors = Split(ErrorMess, OrderManagerWebService.StringDelimiter, Compare:=vbTextCompare)
                            Errors = TransalateOMErrorMessages(Errors)
                            Mess = "Failed to retrieve transaction details from 'Order Manager'."
                            For ErrCount = 0 To UBound(Errors)
                                Mess = Mess & vbNewLine & Errors(ErrCount)
                            Next
                            Call MsgBoxEx(Mess, vbExclamation, "Failed getting transaction details", , , , , RGBMsgBox_WarnColour)
                        End If
                    Else
                        Call MsgBoxEx("'Order Manager' web service is not currently available." & vbNewLine & "Cannot retrieve the transaction details.", vbExclamation, "'Order Manager' web service unavailable", , , , , RGBMsgBox_WarnColour)
                    End If
                End With
            End If
            ' Referral 770; restore values if errored on web order number tran details retrieval
            txtStoreNo.Text = OldStore
            dtxtTranDate.Text = OldDate
            KeyAscii = 0
            ' Referral 769; reset focus to web order number if errored on web order number tran details retrieval
            With txtWebOrderNo
                .SetFocus
                .SelStart = 0
                .SelLength = Len(.Text)
            End With
            Call DebugMsg(MODULE_NAME, "txtWebOrderNo_KeyPress", endlTraceOut)
            Exit Sub
        End If
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If
    Call DebugMsg(MODULE_NAME, "txtWebOrderNo_KeyPress", endlTraceOut)
     
    Exit Sub

Failed:
    Call MsgBoxEx(Err.Description, vbExclamation, "Failed uinsg COMOrderManager.dll", , , , , RGBMsgBox_WarnColour)
End Sub

Private Sub txtWebOrderNo_LostFocus()

    txtWebOrderNo.BackColor = RGB_WHITE
End Sub
' End of Hubs 2.0

Private Sub UserControl_Hide()
    
    Call DebugMsg(MODULE_NAME, "UC_Hide", endlDebug, "Resetting preview")
    If Not moParent Is Nothing Then moParent.KeyPreview = mblnPreview

End Sub

Private Sub UserControl_Initialize()

    Call DebugMsg(MODULE_NAME, "Initialize", endlDebug)

End Sub

Private Sub UserControl_KeyDown(KeyCode As Integer, Shift As Integer)

Dim Ctrl As Control

    Select Case (KeyCode)
        Case (mlngCloseKeyCode):
                Call DebugMsg(MODULE_NAME, "UC_KeyDown", endlDebug)
                If (mlngCloseKeyCode <> 0) And (cmdDtlCancel.Visible = True) Then
                    cmdDtlCancel.value = True
                ElseIf (mlngCloseKeyCode <> 0) And (cmdCancel.Visible = True) Then
                    cmdCancel.value = True
                ElseIf (mlngCloseKeyCode <> 0) And (cmdParkCancel.Visible = True) Then
                    cmdParkCancel.value = True
                End If
                KeyCode = 0
        Case (mlngResetKeyCode):
                If (mlngResetKeyCode <> 0) And (cmdReset.Visible = True) Then Call Reset
                KeyCode = 0
        Case (mlngOtherKeyCode):
                If (mlngOtherKeyCode <> 0) And (cmdOtherTran.Visible = True) Then
                    If (cmdOtherTran.Visible = True) And (cmdOtherTran.Enabled = True) Then Call cmdOtherTran.SetFocus
                    cmdOtherTran.value = True
                End If
                KeyCode = 0
        Case (mlngUseKeyCode):
                If (mlngUseKeyCode <> 0) And (cmdDtlRetrieve.Visible = True) Then
                    If IsAllowRetrieveAction = True Then
                        cmdDtlRetrieve.value = True
                    End If
                End If
                If (mlngUseKeyCode <> 0) And (cmdParkRetrieve.Visible = True) Then cmdParkRetrieve.value = True
        Case (mlngDetailKeyCode):
                If (mlngDetailKeyCode <> 0) And cmdDetails.Visible Then cmdDetails.value = True
        Case (mlngListKeyCode):
                If (mlngListKeyCode <> 0) And (cmdList.Visible = True) Then cmdList.value = True
                If (mlngListKeyCode <> 0) And (cmdRetrieveAll.Visible = True) Then
                    If IsAllowRetrieveAction = True Then
                        cmdRetrieveAll.value = True
                    End If
                End If
                If (mlngListKeyCode <> 0) And (cmdNoProof.Visible = True) Then cmdNoProof.value = True
        Case vbKeyTab
            If Shift = 0 Then
                If (FirstActiveControl.Visible = True) And (FirstActiveControl.Enabled = True) Then FirstActiveControl.SetFocus
            ElseIf Shift = 1 Then
                If (LastActiveControl.Visible = True) And (LastActiveControl.Enabled = True) Then LastActiveControl.SetFocus
            End If
    End Select

End Sub

Private Function IsAllowRetrieveAction() As Boolean
    FinishEditInDetails
    IsAllowRetrieveAction = Not mblnRefundDeterminationScreenHasShown
End Function

Private Sub FinishEditInDetails()
    Call sprdDetails.SetActiveCell(COL_DTL_QTY_RFND, 1)
End Sub

Private Sub UserControl_Resize()

    If moParent Is Nothing Then
        UserControl.Width = 11655
        UserControl.Height = 7950
    End If

End Sub

Public Property Let TillID(value As String)
    If (txtTillId.Visible = True) And (txtTillId.Enabled = True) Then txtTillId.SetFocus
    txtTillId.Text = value
End Property

Public Property Let TranID(value As String)
    If (txtTranNo.Visible = True) And (txtTranNo.Enabled = True) Then txtTranNo.SetFocus
    txtTranNo.Text = value
End Property

Public Property Let StoreNo(value As String)
    lblStoreNo.Caption = value
    txtStoreNo.Text = value
End Property

Public Property Get StoreNo() As String
    StoreNo = txtStoreNo.Text
End Property

Public Sub ShowDetails()
    cmdDetails.Visible = True
    cmdDetails.value = True
End Sub

Public Property Let TransactionDate(value As Date)
    If dtxtTranDate.Enabled And (dtxtTranDate.Visible = True) Then dtxtTranDate.SetFocus
    dtxtTranDate.Text = Format(value, "DD/MM/YY")
End Property

' Hubs 2.0
Public Property Get WebOrderNumber() As String

    WebOrderNumber = txtWebOrderNo.Text
End Property

Public Property Let WebOrderNumber(value As String)

    With txtWebOrderNo
        If .Visible And .Enabled Then
            .SetFocus
            .Text = value
        End If
    End With
End Property

'Referral 767 - Allow Web Order Number fields to be made in/visible
Public Property Let ShowWebOrderNumber(value As Boolean)

    lblWebOrderNo.Visible = value
    txtWebOrderNo.Visible = value
End Property
' End of Hubs 2.0

Private Sub EnforceNumeric(ByRef intKeyAscii As Integer)
    
    If (intKeyAscii >= vbKeySpace) And (Not Chr$(intKeyAscii) Like "[0-9]") Then
        intKeyAscii = 0
    End If

End Sub

Private Function FirstActiveControl() As Control

Dim lowTab  As Long
Dim Ctrl    As Control
Dim retCtrl As Control

    lowTab = -1
    
    On Error Resume Next
    For Each Ctrl In Controls
        If Ctrl.Visible = True And Ctrl.Enabled = True And Ctrl.TabStop = True Then
            If Err.Number = 0 Then
                If (Ctrl.TabIndex < lowTab) Or (lowTab = -1) Then
                    lowTab = Ctrl.TabIndex
                    Set retCtrl = Ctrl
                End If
            End If
            Err.Clear
        End If
    Next Ctrl
    
    On Error GoTo 0
    Set FirstActiveControl = retCtrl
    
End Function

Private Function LastActiveControl() As Control

Dim highTab  As Long
Dim Ctrl    As Control
Dim retCtrl As Control

    highTab = -1
    
    On Error Resume Next
    For Each Ctrl In Controls
        If Ctrl.Visible = True And Ctrl.Enabled = True And Ctrl.TabStop = True Then
            If Err.Number = 0 Then
                If (Ctrl.TabIndex > highTab) Or (highTab = -1) Then
                    highTab = Ctrl.TabIndex
                    Set retCtrl = Ctrl
                End If
            End If
            Err.Clear
        End If
    Next Ctrl
    
    On Error GoTo 0
    Set LastActiveControl = retCtrl
    
End Function

Private Sub SplitRefundCodes()

Dim oRefundCodeBO As cRefundCode

    
    mstrRefundCodes = ""
    If ((RefundReasons Is Nothing) = False) Then
        For Each oRefundCodeBO In RefundReasons
            mstrRefundCodes = mstrRefundCodes & oRefundCodeBO.Code & "-" & oRefundCodeBO.Description & vbTab
        Next
    End If
    
End Sub

Private Sub SplitPriceCodes()

Dim oPriceCodeBO As cPriceOverrideCode

    
    'mstrOPriceCodes = ""
    'If ((PriceOverrideCodes Is Nothing) = False) Then
    '    For Each oPriceCodeBO In PriceOverrideCodes
    '        mstrOPriceCodes = mstrOPriceCodes & oPriceCodeBO.Code & "-" & oPriceCodeBO.Description & vbTab
    '    Next
    'End If
    
End Sub

Private Function AnyItemsRefundedInThisTransaction(Skun As String, lngLineNo As Long) As Integer

Dim intIndex As Integer
    
    AnyItemsRefundedInThisTransaction = 0
    If mblnShowQodRefundScreen = False Then Exit Function
    For intIndex = 1 To RefundTrans.Count
       If Skun = RefundTrans(intIndex).PartCode And RefundTrans(intIndex).LineNo = lngLineNo Then
           AnyItemsRefundedInThisTransaction = AnyItemsRefundedInThisTransaction + RefundTrans(intIndex).RefundQuantity
       End If
    Next intIndex
End Function

Private Function GetAlreadyRefundedLines(lngRowNo As Long, Skun As String) As Integer

Dim lngLineNo     As Long
Dim oOrderHdr     As cCustOrderHeader
Dim oOrderInfoBO  As cCustOrderInfo
Dim oOrderLineBO  As cCustOrderLine
Dim colOrderLines As Collection

    GetAlreadyRefundedLines = 0
    
    If mstrOrderNo <> "" Then
        Set oOrderLineBO = goDatabase.CreateBusinessObject(CLASSID_CUSTORDERLINE)
        Call oOrderLineBO.IBo_AddLoadFilter(CMP_EQUAL, FID_CUSTORDERLINE_OrderNumber, mstrOrderNo)
        Set colOrderLines = oOrderLineBO.LoadMatches
        
        For lngLineNo = 1 To colOrderLines.Count
            Set oOrderLineBO = colOrderLines(lngLineNo)
            If oOrderLineBO.PartCode = Skun And Val(oOrderLineBO.OrderLine) = lngRowNo Then
                GetAlreadyRefundedLines = oOrderLineBO.UnitsRefunded * -1
                Exit Function
            End If
        Next lngLineNo
    End If
End Function

' Hubs 2.0
'
' Order manager date is in reverse order, hyphenated with a 'T' delimiter between date and time
' Check for this format and if it is return true as well as the value of the date part in byref param
Private Function IsOMDate(ByVal OMDate As String, ByRef RealDate As Date) As Boolean
    Dim TPos As Integer
    Dim OMDatePart() As String
    Dim OMDay As String
    Dim OMMonth As String
    Dim OMYear As String

    TPos = InStr(1, OMDate, "T", vbTextCompare)
    If TPos > 0 Then
        ReDim OMDatePart(0) As String
        OMDatePart = Split(Left(OMDate, TPos - 1), "-", Compare:=vbTextCompare)
        If UBound(OMDatePart) = 2 Then
            If IsNumeric(OMDatePart(0)) Then
                OMYear = Format$(CInt(OMDatePart(0)), "00")
                If IsNumeric(OMDatePart(1)) Then
                    OMMonth = Format$(CInt(OMDatePart(1)), "00")
                    If IsNumeric(OMDatePart(2)) Then
                        OMDay = Format$(CInt(OMDatePart(2)), "00")
                        If IsDate(OMDay & "/" & OMMonth & "/" & OMYear) Then
                            RealDate = CDate(OMDay & "/" & OMMonth & "/" & OMYear)
                            IsOMDate = True
                        End If
                    End If
                End If
            End If
        End If
    End If
End Function
'
' Order manager Store has an 8 preceding the Store number.
' Just want the store number part, if its valid
Private Function IsOMStore(ByVal Store As String, ByRef StoreNo As String) As Boolean

    If Len(Trim$(Store)) = 4 Then
        If Left$(Store, 1) = "8" Then
            Store = Mid$(Store, 2)
            If IsNumeric(Store) Then
                StoreNo = Format$(Store, "000")
                IsOMStore = True
            End If
        End If
    End If
End Function


' Send the store, till, tran date and number along with refund code for Web Order, back (to till)
Private Function ManageWebOrderNumber()
    Dim strCode As String
    Dim RefundCodeBO As cRefundCode

    If Not RefundReasons Is Nothing Then
        For Each RefundCodeBO In RefundReasons
            With RefundCodeBO
                If InStr(1, .Description, "Web", vbTextCompare) > 0 Then
                    strCode = .Code
                    Exit For
                End If
            End With
        Next
    End If
    If (strCode <> "") Then
        RaiseEvent WebOrderTran(txtWebOrderNo.Text, txtStoreNo.Text, dtxtTranDate.Text, txtTillId.Text, txtTranNo.Text, strCode)
    End If
End Function

Private Function TransalateOMErrorMessages(ByRef Messages() As String) As String()
    Dim MessCount As Integer

On Error GoTo Finish
    For MessCount = 0 To UBound(Messages)
        Select Case LCase$(Messages(MessCount))
            Case "unknown source order"
                Messages(MessCount) = "Order Manager could not find a Web Order for Web Order Number:" & txtWebOrderNo.Text & "."
            Case "om order not yet created for source transaction"
                Messages(MessCount) = "Order Manager has not yet created the order for Web Order Number:" & txtWebOrderNo.Text & "."
            Case "invalid om order for source transaction"
                Messages(MessCount) = "Order Manager's order for Web Order Number (" & txtWebOrderNo.Text & ")is not valid."
        End Select
    Next MessCount
    
    TransalateOMErrorMessages = Messages
    
    Exit Function
Finish:
End Function
' End of Hubs 2.0

Private Function TransactionHasNonOrderLines(ByRef colLines As Collection, ByRef colOrderLines As Collection) As Boolean
    Dim oTranLine As cPOSLine
    Dim oOrderLineBO As cCustOrderLine
    Dim blnFound As Boolean
    Dim intOffset As Integer
    Dim intFoundOrderLineCount As Integer
    Dim TranLineCount As Integer
    
    If Not colOrderLines Is Nothing And Not colLines Is Nothing Then
        If colLines.Count > 0 Then
            For Each oTranLine In colLines
                With oTranLine
                    If (.QuantitySold > 0) And (Not .LineReversed) And (.SaleType <> "W") Then
                        For Each oOrderLineBO In colOrderLines
                            If oOrderLineBO.PartCode = .PartCode And .SequenceNo = (Val(oOrderLineBO.OrderLine) + intOffset) Then
                                blnFound = True
                                Exit For
                            End If
                        Next oOrderLineBO
                        If blnFound Then
                            intFoundOrderLineCount = intFoundOrderLineCount + 1
                        Else
                            intOffset = intOffset + 1
                        End If
                        ' Only count lines
                        TranLineCount = TranLineCount + 1
                    Else
                        intOffset = intOffset + 1
                    End If
                End With
            Next oTranLine

            ' Referral 868
            TransactionHasNonOrderLines = CBool(intFoundOrderLineCount < TranLineCount)

        End If
    End If
End Function

Friend Function SupressOrderRefundBecauseInTrainingMode() As Boolean
    Dim TrainModeOrderImplementationFactory As New TillRetrieveFctry
    Dim TrainModeOrderImplementation As ITrainModeOrder

    Set TrainModeOrderImplementation = TrainModeOrderImplementationFactory.FactoryGetTrainModeOrder
    SupressOrderRefundBecauseInTrainingMode = TrainModeOrderImplementation.DisableTrainingModeOrders And TillInTrainingMode
End Function

Private Function GetOrderNumber() As String

    If Len(mstrOrderNo & "") = 0 Then
        GetOrderNumber = "000000"
    Else
        GetOrderNumber = mstrOrderNo
    End If
End Function

