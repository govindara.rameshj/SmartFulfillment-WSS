VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{FA666EFA-0D22-45D3-9849-6B0694037D1B}#2.1#0"; "EditNumberTill.ocx"
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Begin VB.Form frmEntry 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Confirm Price"
   ClientHeight    =   3105
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5220
   ControlBox      =   0   'False
   Icon            =   "frmEntry.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3105
   ScaleWidth      =   5220
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin EditNumberTill.ucTillNumberText ntxtPrice 
      Height          =   555
      Left            =   1260
      TabIndex        =   1
      Top             =   1260
      Width           =   2955
      _ExtentX        =   5212
      _ExtentY        =   979
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdProceed 
      Caption         =   "OK"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      HelpContextID   =   555
      Left            =   660
      TabIndex        =   2
      Top             =   1980
      Width           =   1935
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      HelpContextID   =   555
      Left            =   3120
      TabIndex        =   3
      Top             =   1980
      Width           =   1935
   End
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   2460
      Top             =   2640
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   4
      Top             =   2730
      Width           =   5220
      _ExtentX        =   9208
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmEntry.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   635
            MinWidth        =   635
            Picture         =   "frmEntry.frx":1644
            Key             =   "NumPad"
            Object.ToolTipText     =   "Show/Hide Num Pad"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "14:33"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblMessage 
      BackStyle       =   0  'Transparent
      Caption         =   "Enter original unit price from receipt for item."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Left            =   120
      TabIndex        =   0
      Top             =   180
      Width           =   5055
   End
End
Attribute VB_Name = "frmEntry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim mblnShowKeyPad  As Boolean
Dim mcurPrice       As Currency

Public Function GetUnitPrice() As Currency

    mcurPrice = 0
    Me.BackColor = RGBMSGBox_PromptColour
    Call Me.Show(vbModal)
    GetUnitPrice = mcurPrice

End Function

Private Sub cmdCancel_Click()

    mcurPrice = 0
    Me.Hide
    
End Sub

Private Sub cmdProceed_Click()
                
    mcurPrice = Val(ntxtPrice.Value)
    Me.Hide

End Sub

Private Sub Form_Activate()
    
    ucKeyPad1.ShowNumpad
    Call Form_Resize

End Sub

Private Sub Form_Load()

Dim oWorkStationBO As cEnterprise_Wickes.cWorkStation
                
    Call ucKeyPad1.ShowNumpad
    ucKeyPad1.Visible = False
    Call InitialiseStatusBar
    Me.BackColor = RGBQuery_BackColour
    
    'Retrieve Workstation control and check if Barcode Reader working and if Touch Screen
    Set oWorkStationBO = goDatabase.CreateBusinessObject(CLASSID_WORKSTATIONCONFIG)
    Call oWorkStationBO.AddLoadFilter(CMP_EQUAL, FID_WORKSTATIONCONFIG_id, goSession.CurrentEnterprise.IEnterprise_WorkstationID)
    Call oWorkStationBO.AddLoadField(FID_WORKSTATIONCONFIG_UseTouchScreen)
    Call oWorkStationBO.LoadMatches
    
    If (oWorkStationBO.UseTouchScreen = False) Then sbStatus.Panels("NumPad").Picture = Nothing
    Call CentreForm(Me)

End Sub

Private Sub Form_Resize()

    If (ucKeyPad1.Visible = True) Or ((Me.Visible = False) And (mblnShowKeyPad = True)) Then
        Me.Height = ucKeyPad1.Top + ucKeyPad1.Height + sbStatus.Height + 640
    Else
        Me.Height = 3520
    End If
    ucKeyPad1.Left = (Me.Width - ucKeyPad1.Width) / 2
    Call CentreForm(Me)

End Sub

Private Sub ucKeyPad1_Resize()
    
    ucKeyPad1.Width = ucKeyPad1.Width

End Sub

Public Sub InitialiseStatusBar()
    
    sbStatus.Panels(PANEL_VERNO).Text = "Ver. " & App.Major & "." & App.Minor & "." & App.Revision
    sbStatus.Panels(PANEL_VERNO).ToolTipText = "Program Version Number"
    sbStatus.Panels(PANEL_WSID + 1).Text = Right$("00" & goSession.CurrentEnterprise.IEnterprise_WorkstationID, 2) & " "
    sbStatus.Panels(PANEL_WSID + 1).ToolTipText = "Current Work-Station Number"
    sbStatus.Panels(PANEL_DATE + 1).Text = Format$(Date, "DD-MMM-YY")

End Sub

Private Sub sbStatus_PanelClick(ByVal Panel As MSComctlLib.Panel)

Dim blnShow As Boolean

    Select Case (Panel.Key)
        Case ("NumPad"):
                            If ((Panel.Picture Is Nothing) = False) Then
                                ucKeyPad1.Visible = Not ucKeyPad1.Visible
                                Call ucKeyPad1.ShowNumpad
                                Call Form_Resize
                            End If
    End Select

End Sub


