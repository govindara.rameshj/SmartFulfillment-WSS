VERSION 5.00
Begin VB.Form frmRefundPopup 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Refund Determination"
   ClientHeight    =   2460
   ClientLeft      =   6045
   ClientTop       =   4350
   ClientWidth     =   7305
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2460
   ScaleWidth      =   7305
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "F12 Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      HelpContextID   =   555
      Left            =   3735
      TabIndex        =   3
      Top             =   1800
      Width           =   2295
   End
   Begin VB.CommandButton cmdProceed 
      Caption         =   "F5 OK"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      HelpContextID   =   555
      Left            =   1275
      TabIndex        =   2
      Top             =   1800
      Width           =   2295
   End
   Begin VB.TextBox txtRefundReturned 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   5280
      MaxLength       =   5
      TabIndex        =   1
      Top             =   960
      Width           =   1815
   End
   Begin VB.TextBox txtCancelled 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   5280
      MaxLength       =   5
      TabIndex        =   0
      Top             =   360
      Width           =   1815
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Refund for stock being Returned:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   5
      Top             =   1080
      Width           =   5175
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Cancellation for stock not yet delivered:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   360
      Width           =   5175
   End
End
Attribute VB_Name = "frmRefundPopup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const DELIVERY_STATUS_PICKCONFIRM_CREATED = 600
Const DELIVERY_STATUS_UNDELIVERED_CREATED = 800

Public mintCancellation As Integer
Public mintRefunded As Integer
Public mblnCancel As Boolean
Public mintCheckRefund As Integer
Public mintCheckCancel As Integer
Public mintDeliveryStatus As Integer
Public isParentAlreadyHide As Boolean

Private Sub cmdCancel_Click()
    mblnCancel = True
    Me.Hide
End Sub

Private Sub cmdProceed_Click()
    If (IsValidFields()) Then
        mintRefunded = Val(txtRefundReturned.Text)
        mintCancellation = Val(txtCancelled.Text)
        mblnCancel = False
        Me.Hide
    End If
End Sub

Private Sub Form_Activate()
    If isParentAlreadyHide = True Then
        cmdCancel.value = True
        Exit Sub
     End If
    
    txtCancelled.Text = Format(mintCancellation)
    If Val(txtCancelled.Text) > mintCheckCancel Then
        txtCancelled.Text = Format(mintCheckCancel)
        txtRefundReturned = Format(mintCancellation - mintCheckCancel)
    End If
    txtCancelled.SetFocus
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then
        cmdProceed.Value = True
    End If
    If KeyCode = vbKeyEscape Or KeyCode = vbKeyF12 Then
        cmdCancel.Value = True
    End If
    
End Sub

Private Sub Form_Load()
    Me.BackColor = RGBQuery_BackColour
End Sub

Private Sub txtCancelled_GotFocus()
    txtCancelled.SelStart = 0
    txtCancelled.SelLength = Len(txtCancelled.Text)
    txtCancelled.BackColor = RGBEdit_Colour
End Sub

Private Sub txtCancelled_KeyPress(KeyAscii As Integer)
    If (KeyAscii = vbKeyReturn) Then
            KeyAscii = 0
            Call SendKeys(vbTab)
        End If
    If (KeyAscii = vbKeyEscape) Then
            Call SendKeys("+{tab}")
        End If
    If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0
End Sub


Private Sub txtRefundReturned_GotFocus()
        txtRefundReturned.SelStart = 0
        txtRefundReturned.SelLength = Len(txtRefundReturned.Text)
        txtRefundReturned.BackColor = RGBEdit_Colour
End Sub

Private Sub txtRefundReturned_KeyPress(KeyAscii As Integer)
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If
    
    If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0
End Sub

Private Function IsValidFields() As Boolean
    Dim errorMessage As String
    Dim cancelledErrorMessage As String
    Dim refundReturnErrorMessage As String
    Dim isValid As Boolean
    errorMessage = ""
    cancelledErrorMessage = ""
    refundReturnErrorMessage = ""
    
    'First field validation
    If (mintDeliveryStatus >= DELIVERY_STATUS_PICKCONFIRM_CREATED And _
        mintDeliveryStatus < DELIVERY_STATUS_UNDELIVERED_CREATED And _
        Val(txtCancelled.Text) > 0) Then
           cancelledErrorMessage = "Cancellation is not possible as the line have been despatched." & vbCrLf & _
                "Please contact fulfilling store to confirm delivery status and" & vbCrLf & _
                "update SOM before refunding the order."
    ElseIf Val(txtCancelled.Text) > mintCheckCancel Then
         cancelledErrorMessage = "Cannot cancel more than is due to be delivered."
    End If
    
    If Len(cancelledErrorMessage) = 0 Then
        txtCancelled.BackColor = RGB_WHITE
    Else
        txtCancelled.BackColor = RGBEdit_Colour
    End If
        
    
    'Second field validation
    If Val(txtRefundReturned) > mintCheckRefund Then
        refundReturnErrorMessage = "Cannot refund more than was taken."
    End If
    
    If Len(refundReturnErrorMessage) = 0 Then
        txtRefundReturned.BackColor = RGB_WHITE
    Else
        txtRefundReturned.BackColor = RGBEdit_Colour
    End If
    
    ' Set Focus
    If Len(cancelledErrorMessage) > 0 Then
        txtCancelled.SetFocus
    ElseIf Len(refundReturnErrorMessage) > 0 Then
        txtRefundReturned.SetFocus
    End If
    
    isValid = (Len(cancelledErrorMessage) = 0 And Len(refundReturnErrorMessage) = 0)
    
    ' Show error message
    If (isValid = False) Then
        If ((Len(cancelledErrorMessage) = 0 Or Len(refundReturnErrorMessage) = 0)) Then
            errorMessage = cancelledErrorMessage & refundReturnErrorMessage
        Else
            errorMessage = "Please correct the following errors:" & vbCrLf & _
                " - " & cancelledErrorMessage & vbCrLf & _
                " - " & refundReturnErrorMessage
        End If
        Call MsgBoxEx(errorMessage, vbOKOnly, "Invalid Quantity", , , , , RGB_RED)
    End If
    
    IsValidFields = isValid
End Function

Private Sub txtCancelled_LostFocus()
       txtCancelled.BackColor = RGB_WHITE
End Sub

Private Sub txtRefundReturned_LostFocus()
   txtRefundReturned.BackColor = RGB_WHITE
End Sub
