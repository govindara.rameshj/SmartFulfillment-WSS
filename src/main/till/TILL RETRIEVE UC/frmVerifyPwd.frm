VERSION 5.00
Begin VB.Form frmVerifyPwd 
   BackColor       =   &H0080FFFF&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Refund Price Override - Supervisor Authorisation"
   ClientHeight    =   3210
   ClientLeft      =   1680
   ClientTop       =   1755
   ClientWidth     =   7080
   ControlBox      =   0   'False
   Icon            =   "frmVerifyPwd.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3210
   ScaleWidth      =   7080
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraLogon 
      BorderStyle     =   0  'None
      Height          =   1935
      Left            =   180
      TabIndex        =   0
      Top             =   120
      Visible         =   0   'False
      Width           =   6735
      Begin VB.TextBox txtAuthID 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   2400
         MaxLength       =   3
         TabIndex        =   2
         Top             =   120
         Width           =   975
      End
      Begin VB.TextBox txtPassword 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         IMEMode         =   3  'DISABLE
         Left            =   2400
         MaxLength       =   5
         PasswordChar    =   "*"
         TabIndex        =   8
         Top             =   1320
         Width           =   1335
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Auth ID"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label lblPasswordlbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Password"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   120
         TabIndex        =   7
         Top             =   1320
         Width           =   1695
      End
      Begin VB.Label lblFullName 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   3480
         TabIndex        =   6
         Top             =   720
         Width           =   2655
      End
      Begin VB.Label lblInitials 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   2400
         TabIndex        =   5
         Top             =   720
         Width           =   975
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Initials"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   120
         TabIndex        =   4
         Top             =   720
         Width           =   1335
      End
      Begin VB.Label lblLoggedUserID 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   2400
         TabIndex        =   3
         Top             =   120
         Visible         =   0   'False
         Width           =   975
      End
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   1260
      TabIndex        =   9
      Top             =   2220
      Width           =   1695
   End
   Begin VB.CommandButton cmdContinue 
      Caption         =   "Continue"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   4140
      TabIndex        =   10
      Top             =   2220
      Width           =   1695
   End
End
Attribute VB_Name = "frmVerifyPwd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>************************************************************************************
'* Module : frmRefundPwd
'* Date   : 17/02/03
'* Author : Unknown
'*$Archive: /Projects/OasysV2/VB/Till/frmVerifyPwd.frm $
'******************************************************************************************
'* Summary: Used to enter the Refund password.  Used for refund authorisation
'******************************************************************************************
'* $Author: Mauricem $ $Date: 12/03/04 9:56 $ $Revision: 6 $
'* Versions:
'* 17/02/03    Unknown
'*             Header added.
'</CAMH>***********************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmVerifyPwd"

Const PWD_TYPE_EFT    As Byte = 1
Const PWD_TYPE_REFUND As Byte = 2
Const PWD_TYPE_DURESS As Byte = 3
Const PWD_TYPE_SUPER  As Byte = 4
Const PWD_TYPE_MNGER  As Byte = 5

Private mblnPasswordOk   As Boolean
Private mstrPasswordType As String

Private mstrOldUserID As String 'used to auto move to password on 3 key pressed
Private mstrOldPwd    As String 'used to auto logon on 5 key pressed

Public Event Duress()
Private mintDuressKeyCode As Integer

Public Function VerifySupervisorPassword(Optional strMessage = vbNullString) As Boolean
    
    Me.BackColor = &H7FFF7F
    SetColors
    If LenB(strMessage) <> 0 Then
        mstrPasswordType = strMessage
    Else
        mstrPasswordType = "Supervisor Password"
    End If
    
    txtAuthID.Text = vbNullString
    txtPassword.Text = vbNullString
    
    fraLogon.Visible = True
    Call Me.Show(vbModal)
    VerifySupervisorPassword = mblnPasswordOk
    
End Function

Private Sub cmdCancel_Click()

    If (cmdCancel.Visible = True) And (cmdCancel.Enabled = True) Then cmdCancel.SetFocus
    mblnPasswordOk = False
    Call Me.Hide

End Sub

Private Sub cmdContinue_Click()
    
Const PROCEDURE_NAME As String = "cmdContinue_Click"
    
    If (cmdContinue.Visible = True) And (cmdContinue.Enabled = True) Then cmdContinue.SetFocus
    'Password entered correctly so report to calling form
    mblnPasswordOk = VerifyAuthCode
    If mblnPasswordOk = True Then Call Me.Hide

End Sub

Private Function VerifyAuthCode() As Boolean

Const PROCEDURE_NAME As String = "VerifyAuthCode"

Const PRM_CHECK_EXPIRED As Long = 106

Dim strFailedMsg    As String
Dim strTranID       As String
Dim blnCheckExpired As Boolean
Dim strPassword     As String

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Verify Authorisation Code")
    VerifyAuthCode = False
    
    blnCheckExpired = goSession.GetParameter(PRM_CHECK_EXPIRED)
    If (Not moUser Is Nothing) And (LenB(lblFullName.Caption) <> 0) Then
        Screen.MousePointer = vbHourglass
        'pad password to 5 characters
        strPassword = Right$("00000" & txtPassword.Text, 5)
        If (strPassword = moUser.AuthorisationCode) Then
            If (moUser.AuthorisationValidTill < Now()) And (blnCheckExpired = True) Then
                Call MsgBoxEx("Authorisation Code has expired for user '" & moUser.EmployeeID & "-" & moUser.FullName & "'", vbInformation, "Authorisation Expired", , , , , RGBMsgBox_WarnColour)
                Screen.MousePointer = vbNormal
                If (txtAuthID.Visible = True) And (txtAuthID.Enabled = True) Then txtAuthID.SetFocus
                Exit Function
            End If 'check that password has not expired
        Else
            strFailedMsg = "Invalid user name or password entered"
            Call MsgBoxEx(strFailedMsg, vbExclamation, Me.Caption, , , , , RGBMsgBox_WarnColour)
            Screen.MousePointer = vbNormal
            If (txtAuthID.Visible = True) And (txtAuthID.Enabled = True) Then txtAuthID.SetFocus
            Exit Function
        End If
        'Password entered correctly so report to calling form
        Screen.MousePointer = vbNormal
        If moUser.Supervisor Or moUser.Manager Then
            VerifyAuthCode = True
        Else
            strFailedMsg = "Employee not a supervisor"
        End If
    
        If LenB(strFailedMsg) <> 0 Then
            Call MsgBoxEx(strFailedMsg, vbExclamation, Me.Caption, , , , , RGBMsgBox_WarnColour)
            Screen.MousePointer = vbNormal
            If (txtAuthID.Visible = True) And (txtAuthID.Enabled = True) Then txtAuthID.SetFocus
            Exit Function
        End If
    End If 'verify current user's password

End Function

Private Sub Form_Activate()

    If (txtAuthID.Visible = True) And (txtAuthID.Enabled = True) Then Call txtAuthID.SetFocus

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If mintDuressKeyCode <> 0 And KeyCode = mintDuressKeyCode Then
        KeyCode = 0
        RaiseEvent Duress
    End If

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyEscape Then cmdCancel.Value = True

End Sub

Private Sub Form_Load()

    mblnPasswordOk = False
    mintDuressKeyCode = goSession.GetParameter(PRM_KEY_DURESS)
    
End Sub

Private Sub Form_Resize()

    Call CentreForm(frmVerifyPwd)

End Sub

Private Sub txtAuthID_Change()

    lblFullName.Caption = vbNullString
    lblInitials.Caption = vbNullString
    txtPassword.Text = vbNullString
    txtPassword.Enabled = False
    
End Sub

Private Sub txtAuthID_GotFocus()

    txtAuthID.SelStart = 0
    txtAuthID.SelLength = Len(txtAuthID.Text)
    txtAuthID.BackColor = RGBEdit_Colour

End Sub

Private Sub txtAuthID_KeyPress(KeyAscii As Integer)

Const PROCEDURE_NAME As String = MODULE_NAME & ".txtAuthID_KeyPress"

Dim blnLoggedIn As Boolean
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        If Len(txtAuthID.Text) <> 3 Then txtAuthID.Text = Right$("000" & txtAuthID.Text, 3)
        Set moUser = goDatabase.CreateBusinessObject(CLASSID_USER)
        Call moUser.AddLoadFilter(CMP_EQUAL, FID_USER_EmployeeID, txtAuthID.Text)
        Call moUser.AddLoadFilter(CMP_EQUAL, FID_USER_Deleted, False)
        Call moUser.AddLoadField(FID_USER_Initials)
        Call moUser.AddLoadField(FID_USER_FullName)
        Call moUser.AddLoadField(FID_USER_AuthorisationCode)
        Call moUser.AddLoadField(FID_USER_AuthorisationValidTill)
        Call moUser.AddLoadField(FID_USER_Supervisor)
        Call moUser.AddLoadField(FID_USER_Manager)
        Call moUser.AddLoadField(FID_USER_Password)
        If LenB(moUser.EmployeeID) = 0 Then moUser.EmployeeID = "***" 'force invalid lookup
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "UserID Validated")
        
        Call moUser.LoadMatches
        If LenB(moUser.FullName) <> 0 Then
            lblInitials.Caption = moUser.Initials
            lblFullName.Caption = moUser.FullName
            txtPassword.Enabled = True
            txtPassword.Visible = True
            lblPasswordlbl.Visible = True
            If (txtPassword.Visible = True) And (txtPassword.Enabled = True) Then Call txtPassword.SetFocus
            If (Val(txtAuthID.Text) >= Val(GetLastValidUserID)) Then
                lblInitials.Caption = vbNullString
                lblFullName.Caption = "TRAINING ID"
                If (txtAuthID.Visible = True) And (txtAuthID.Enabled = True) Then Call txtAuthID.SetFocus
                Set moUser = Nothing
            End If
        Else
            txtPassword.Text = vbNullString
            txtPassword.Enabled = False
            lblInitials.Caption = vbNullString
            lblFullName.Caption = "INVALID ID"
            If (txtAuthID.Visible = True) And (txtAuthID.Enabled = True) Then Call txtAuthID.SetFocus
            txtAuthID.SelStart = 0
            txtAuthID.SelLength = Len(txtAuthID.Text)
            Set moUser = Nothing
        End If
        Screen.MousePointer = vbNormal
    End If 'enter pressed so check User ID is valid

End Sub

Private Sub txtAuthID_KeyUp(KeyCode As Integer, Shift As Integer)
    
    'Check if new keypressed makes up full User ID, so auto move to password
    If (Len(txtAuthID.Text) = 3) And (Len(mstrOldUserID) = 2) Then
        KeyCode = 0
        Call txtAuthID_KeyPress(vbKeyReturn)
    End If
    mstrOldUserID = txtAuthID.Text


End Sub

Private Sub txtAuthID_LostFocus()

    txtAuthID.BackColor = RGB_WHITE

End Sub

Private Sub txtPassword_GotFocus()
    
    txtPassword.SelStart = 0
    txtPassword.SelLength = Len(txtPassword.Text)
    txtPassword.BackColor = RGBEdit_Colour

End Sub

Private Sub txtPassword_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        cmdContinue.Value = True
    End If

End Sub


Private Sub txtPassword_KeyUp(KeyCode As Integer, Shift As Integer)
    
'    If (Len(txtPassword.Text) = 5) And (Len(mstrOldPwd) = 4) Then
'        KeyCode = 0
'        Call txtPassword_KeyPress(vbKeyReturn)
'    End If
'    mstrOldPwd = txtPassword.Text

End Sub

Private Sub txtPassword_LostFocus()
    
    txtPassword.BackColor = RGB_WHITE

End Sub

Private Sub SetColors()
    
    fraLogon.BackColor = Me.BackColor
    cmdCancel.BackColor = Me.BackColor
    cmdContinue.BackColor = Me.BackColor

End Sub
