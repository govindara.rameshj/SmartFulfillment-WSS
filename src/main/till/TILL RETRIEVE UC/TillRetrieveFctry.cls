VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TillRetrieveFctry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Const m_TrainModeOrderParameterID As Long = 980064
Private m_InitialisedUseTrainModeOrderImplementation As Boolean
Private m_UseTrainModeOrderImplementation As Boolean

Friend Property Get UseTrainModeOrderImplementation() As Boolean
    
    UseTrainModeOrderImplementation = m_UseTrainModeOrderImplementation
End Property

Public Function FactoryGetTrainModeOrder() As ITrainModeOrder
    
    If UseTrainModeOrderImplementation Then
        'using implementation with CR 64
        Set FactoryGetTrainModeOrder = New TrainModeOrder
    Else
        'using live implementation
        Set FactoryGetTrainModeOrder = New TillRetrieveLive
    End If
End Function

Private Sub Class_Initialize()

    GetUseTrainModeOrderParameterValue
End Sub

Friend Sub GetUseTrainModeOrderParameterValue()

    If Not m_InitialisedUseTrainModeOrderImplementation Then
On Error Resume Next
        m_UseTrainModeOrderImplementation = goSession.GetParameter(m_TrainModeOrderParameterID)
        m_InitialisedUseTrainModeOrderImplementation = True
On Error GoTo 0
    End If
End Sub

