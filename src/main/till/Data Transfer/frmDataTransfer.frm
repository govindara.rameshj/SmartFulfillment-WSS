VERSION 5.00
Begin VB.Form frmDataTransfer 
   Caption         =   "Data Transfer"
   ClientHeight    =   5910
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7485
   Icon            =   "frmDataTransfer.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5910
   ScaleWidth      =   7485
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstStatus 
      Height          =   5325
      Left            =   360
      TabIndex        =   0
      Top             =   240
      Width           =   6735
   End
End
Attribute VB_Name = "frmDataTransfer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const WIX_FOLDER        As String = "F:\WIX"
Const COPY_SOURCE       As Long = 0
Const COPY_DESTINATION  As Long = 1
Const PRM_SQL_SERVER_SYNC As Long = 192

Dim moFSO               As New FileSystemObject
Dim moFiles             As Files
Dim moFile              As File
Dim mstrTransferFile    As String
Dim mstrWSID            As String

Private Sub Form_Load()
    
    On Error GoTo Old_System
    
    Set goRoot = GetRoot

    If goSession.GetParameter(PRM_SQL_SERVER_SYNC) = True Then
         Call ShellWait(App.Path & "\FileSync.exe /P:cfc", SW_MAX)
         If CheckForGOFile = True Then
            Call UpdateSystem
         End If
    Else

Old_System:
        On Error GoTo 0
        Me.Show
        DoEvents
        Call CheckForFolders
        Call CheckForTransferFile
        Call ReadLinesInTransferFile
        Call lstStatus.AddItem("Process complete")
        Call lstStatus.Refresh
        If CheckForGOFile = True Then
            Call UpdateSystem
        End If
        lstStatus.Selected(lstStatus.NewIndex) = True
    End If
    Unload Me

End Sub 'Form_Load

Private Sub CheckForTransferFile()

    mstrWSID = GetRegSetting("software\CTS Retail\Default\Oasys\2.0\Wickes\Enterprise", "WorkstationID", "", HKEY_LOCAL_MACHINE)
    mstrTransferFile = "TRANSFER." & mstrWSID
    Call lstStatus.AddItem("Checking for transfer file " & mstrTransferFile)
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    DoEvents
    'Check for the exsistance of a Transfer file
    If moFSO.FileExists(WIX_FOLDER & "\TRANSFERS\" & mstrTransferFile) = False Then
        'If the file does not exsist then create it
        Call moFSO.CreateTextFile(WIX_FOLDER & "\TRANSFERS\" & mstrTransferFile, True, False)
    End If

End Sub 'CheckForTransferFile

Private Sub ReadLinesInTransferFile()

Dim tsTransfer      As TextStream
Dim vntTransfer     As Variant
Dim strTextHold     As String
Dim strPath         As String
Dim strDestination  As String
Dim strFileName     As String

    'Open Transfer file
    Call lstStatus.AddItem("Opening " & WIX_FOLDER & "\TRANSFERS\" & mstrTransferFile)
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    Set tsTransfer = moFSO.OpenTextFile(WIX_FOLDER & "\TRANSFERS\" & mstrTransferFile)
    'Read each line
    While Not tsTransfer.AtEndOfStream
        strTextHold = tsTransfer.ReadLine
        strTextHold = Replace(strTextHold, ";", ",")
        Call lstStatus.AddItem("Reading line " & strTextHold)
        Call lstStatus.Refresh
        lstStatus.Selected(lstStatus.NewIndex) = True
        vntTransfer = Split(strTextHold, ",")
        strDestination = vntTransfer(COPY_DESTINATION)
        If Right(strDestination, 1) <> "\" Then strDestination = strDestination & "\"
        strPath = vntTransfer(COPY_SOURCE)
        While (Right$(strPath, 1) <> "\")
            strPath = Left$(strPath, Len(strPath) - 1)
        Wend
        If (moFSO.FolderExists(strPath) = False) Then
            Call moFSO.CreateFolder(strPath)
        End If
        
        strFileName = Mid(vntTransfer(COPY_SOURCE), Len(strPath) + 1)
        Call lstStatus.AddItem("strFileName = " & strFileName)
        Call lstStatus.Refresh
        lstStatus.Selected(lstStatus.NewIndex) = True
        
        Call lstStatus.AddItem("Copying  = " & strFileName & " from " & _
            strPath & " to " & strDestination)
        Call lstStatus.Refresh
        lstStatus.Selected(lstStatus.NewIndex) = True
        
        Select Case strFileName
            Case "*.*"
                'strFileName = "*.*"
                Call CopyAllFiles(strPath, strDestination)
            Case InStr(strFileName, "*") = 0
                'strFileName <> "*"
                On Error Resume Next
                Call CopySpecificFile(strPath, strDestination, strFileName)
                Err.Clear
                On Error GoTo 0
            Case Else
                On Error Resume Next
                'strFileName = "*"
                Call CopyOtherFiles(strPath, strDestination, strFileName)
                Err.Clear
                On Error GoTo 0
        End Select
       
    Wend
    'Copy file

End Sub 'ReadLinesInTransferFile

Private Sub CheckForFolders()

    Call lstStatus.AddItem("Checking for folders")
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    DoEvents
    'Check for the exsistance of a Transfer file
    Call lstStatus.AddItem("Checking for " & WIX_FOLDER)
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    If moFSO.FolderExists(WIX_FOLDER) = False Then
        'If the file does not exsist then create it
        Call moFSO.CreateFolder(WIX_FOLDER)
    End If

    Call lstStatus.AddItem("Checking for " & WIX_FOLDER & "\Transfers")
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    'Check for the exsistance of a Transfer file
    If moFSO.FolderExists(WIX_FOLDER & "\Transfers") = False Then
        'If the file does not exsist then create it
        Call moFSO.CreateFolder(WIX_FOLDER & "\Transfers")
    End If

End Sub 'CheckForFolders

Private Sub CopySpecificFile(ByVal strSource As String, ByVal strDestination As String, _
                             ByVal strFileName As String)

Dim strTempSource           As String
Dim strTempDestination      As String

    strTempSource = strSource & strFileName
    strTempDestination = strDestination & strFileName
  
    Call moFSO.CopyFile(CStr(strTempSource), CStr(strTempDestination), True)
    
End Sub 'CopySpecificFile

Private Sub CopyAllFiles(ByVal strSource As String, ByVal strDestination As String)

Dim strTempSource           As String
Dim strTempDestination      As String

    Set moFiles = moFSO.GetFolder(strSource).Files
    For Each moFile In moFiles
        Call lstStatus.AddItem("Copying  = " & moFile.Path)
        Call lstStatus.Refresh
        lstStatus.Selected(lstStatus.NewIndex) = True
        On Error Resume Next
        strTempSource = strSource & moFile.Name
        strTempDestination = strDestination & moFile.Name
        Call FileCopy(moFile.Path, strTempDestination$)
        Call moFSO.CopyFile(UCase$(strTempSource), UCase$(strTempDestination)) ', True)
        If Err.Number <> 0 Then
            Call lstStatus.AddItem("Error copying  = " & moFile.Path & vbTab & _
                Err.Number & " - " & Err.Description)
            Call lstStatus.Refresh
            lstStatus.Selected(lstStatus.NewIndex) = True
            On Error GoTo 0
        End If
        
    Next
    Set moFile = Nothing
    Set moFiles = Nothing

End Sub 'CopyAllFiles

Private Sub CopyOtherFiles(ByVal strSource As String, ByVal strDestination As String, _
                           ByVal strFileName As String)

    Call moFSO.CopyFile(strSource & "\" & strFileName, strDestination, True)

End Sub 'CopyOtherFiles

Private Function CheckForGOFile() As Boolean

Dim strWSFolder As String

    strWSFolder = "c:\CTS\Update\Workstations\WS" & mstrWSID
    CheckForGOFile = False
    Call lstStatus.AddItem("Checking for GO file " & vbTab & _
        strWSFolder & "\" & mstrWSID & ".go")
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    If moFSO.FileExists(strWSFolder & "\" & mstrWSID & ".go") Then
        CheckForGOFile = True
    End If

End Function 'CheckForGOFile

Private Sub UpdateSystem()

    Call ShellWait(App.Path & "\WixWorkstationUpdate.exe", SW_MAX)

End Sub 'UpdateSystem
