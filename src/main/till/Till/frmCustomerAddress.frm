VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Object = "{D165F572-038D-4ED4-A4A8-EB3E27C39F74}#1.0#0"; "CaptureCustUC.ocx"
Begin VB.Form frmCustomerAddress 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Customer Address"
   ClientHeight    =   5625
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   12240
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5625
   ScaleWidth      =   12240
   StartUpPosition =   2  'CenterScreen
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   120
      Top             =   840
      Visible         =   0   'False
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   1
      Top             =   5250
      Width           =   12240
      _ExtentX        =   21590
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmCustomerAddress.frx":0000
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13864
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   970
            MinWidth        =   970
            Picture         =   "frmCustomerAddress.frx":09A6
            Key             =   "KeyBoard"
            Object.ToolTipText     =   "Show/Hide Keyboard"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "11:22"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin ucCaptureCustomer_Wickes.ucCaptureCust ucccCustomer 
      Height          =   4455
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9510
      _ExtentX        =   16775
      _ExtentY        =   8811
   End
End
Attribute VB_Name = "frmCustomerAddress"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const MODULE_NAME As String = "frmCustomerAddress"

Public Event Duress()
Private mintDuressKeyCode As Integer
Private mblnCompulsory As Boolean

Public Cancel       As Boolean
Public ShowKeyPad   As Boolean

Private mstrName        As String
Private mstrPostCode    As String
Private mstrAddress     As String
Private mstrTelNo       As String
Private mstrMobileNo    As String
Private mstrWorkTelNo   As String
Private mstrEmail       As String


'MO'C WIX1345 - Below added to hold data
Private mstrSalutation  As String
Private mstrForename  As String
Private mstrSurname As String
Private mstrCompany As String
Private mstrHouseNo As String
Private mstrAddress1 As String
Private mstrAddress2 As String
Private mstrTown As String
Private mstrCounty As String
Private mstrCountry As String
'MO'C WI1345 - Finished

'Error handler variables
Private errorNo As Long
Private errorDesc As String



Private Sub Form_Activate()
    
    If (ucKeyPad1.Visible = True) And (ucccCustomer.Top = 0) Then ucccCustomer.Top = -120

End Sub

Private Sub Form_Load()
    
    On Error GoTo HandleException
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    ucKeyPad1.Left = 0
    ucccCustomer.Top = 0
    If (ShowKeyPad = True) Then
        ucKeyPad1.Visible = True
    Else
        ucKeyPad1.Visible = False
    End If
    Call InitialiseStatusBar(sbStatus)
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("Form_Load", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub sbStatus_PanelClick(ByVal Panel As MSComctlLib.Panel)
    
    Select Case (Panel.Key)
        Case ("KeyBoard"):
                If ((Panel.Picture Is Nothing) = False) Then
                    ucKeyPad1.Visible = Not ucKeyPad1.Visible
                    If (ucKeyPad1.Visible) Then
                        ucccCustomer.Top = -120
                    Else
                        ucccCustomer.Top = 0
                    End If
                    Call ucccCustomer_Resize
                End If
    End Select

End Sub

Private Sub ucccCustomer_Cancel()

    Cancel = True
    Me.Hide
    
End Sub

Private Sub ucccCustomer_Resize()

    Me.Height = ucccCustomer.Height + 360 + sbStatus.Height
    If (ucKeyPad1.Visible = True) Or ((Me.Visible = False) And (ShowKeyPad = True)) Then
        Me.Height = Me.Height + ucKeyPad1.Height
        Me.Width = ucKeyPad1.Width + 120
    Else
        Me.Width = ucccCustomer.Width + 120
    End If
    ucccCustomer.Left = (Me.Width - ucccCustomer.Width) / 2
    ucKeyPad1.Top = ucccCustomer.Height - 120
    Call CentreForm(Me)

End Sub

Private Sub ucccCustomer_Save(Name As String, PostCode As String, Address As String, TelNo As String, MobileNo As String, WorkTelNo As String, Email As String)

Dim strUserMsg As String
        
    On Error GoTo HandleException
    
    Cancel = False
    
    Call DebugMsg(MODULE_NAME, "ucccCustomer_Save", endlDebug, "Comp=" & mblnCompulsory & "->Name=" & Name & ",PostCode=" & PostCode & ",Address=" & Replace(Address, vbCrLf, "~") & "'")
    If (goSession.GetParameter(PRM_COUNTRY_CODE) <> "IE") Then
        strUserMsg = ", Postcode and House Number"
    Else
        strUserMsg = " and Address"
    End If
    
    If (mblnCompulsory = True) And ((Name = "") Or ((PostCode = "") And (goSession.GetParameter(PRM_COUNTRY_CODE) <> "IE")) Or (Trim$(Replace(Address, vbCrLf, "")) = "")) Then
        Call MsgBoxEx("Name" & strUserMsg & " required", vbOKOnly, "Invalid Address entered", , , , , RGBMsgBox_WarnColour)
        Exit Sub
    End If
    mstrName = Name
    mstrPostCode = PostCode
    mstrAddress = Address
    mstrTelNo = TelNo
    mstrMobileNo = MobileNo
    mstrWorkTelNo = WorkTelNo
    mstrEmail = Email
    Me.Hide
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("ucccCustomer_Save", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub ucccCustomer_SaveCustRes(Salutaion As String, ForeName As String, Surname As String, _
                                     Company As String, PostCode As String, HouseNo As String, _
                                     Address1 As String, Address2 As String, Town As String, _
                                     County As String, Country As String)
Dim strUserMsg As String
        
    On Error GoTo HandleException
    
    Cancel = False
    
    Call DebugMsg(MODULE_NAME, "ucccCustomer_Save", endlDebug, "Comp=" & mblnCompulsory & "->Name=" & Name & ",PostCode=" & PostCode & ",Address=" & Replace(Address1, vbCrLf, "~") & "'")
    If (goSession.GetParameter(PRM_COUNTRY_CODE) <> "IE") Then
        strUserMsg = ", Postcode and House Number"
    Else
        strUserMsg = " and Address"
    End If
    
    If (mblnCompulsory = True) And ((Name = "") Or ((PostCode = "") And (goSession.GetParameter(PRM_COUNTRY_CODE) <> "IE")) Or (Trim$(Replace(Address1, vbCrLf, "")) = "")) Then
        Call MsgBoxEx("Name" & strUserMsg & " required", vbOKOnly, "Invalid Address entered", , , , , RGBMsgBox_WarnColour)
        Exit Sub
    End If
    
    mstrSalutation = Salutaion
    mstrForename = ForeName
    mstrSurname = Surname
    mstrCompany = Company
    mstrPostCode = PostCode
    mstrHouseNo = HouseNo
    mstrAddress1 = Address1
    mstrAddress2 = Address2
    mstrTown = Town
    mstrCounty = County
    mstrCountry = Country
    Me.Hide
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("ucccCustomer_SaveCustRes", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Public Sub GetRefundAddress(ByRef strName As String, ByRef strPostCode, _
                            ByRef strAddress, ByRef strTelNo As String, strMobileNo As String, strWorkTelNo As String, strEmail As String)

    On Error GoTo HandleException
    
    Call ucccCustomer.Initialise(goSession, Me)
    Call ucccCustomer.Reset
    ucccCustomer.Top = 0
    ucccCustomer.Left = 0
'    Me.Width = ucccCustomer.Width
    Me.Height = ucccCustomer.Height + 480 + sbStatus.Height
'    Me.Top = Screen.Height = Me.Height \ 2
'    Me.Left = Screen.Width = Me.Width \ 2
    ucccCustomer.NameOnly = False
    mblnCompulsory = True
    Call ucccCustomer.ShowRefundDetails(vbNullString, Date, vbNullString, vbNullString, False, True)
    Me.Show vbModal
    
    If Cancel = False Then
        strName = mstrName
        strPostCode = mstrPostCode
        strAddress = mstrAddress
        strTelNo = mstrTelNo
        strMobileNo = mstrMobileNo
        strWorkTelNo = mstrWorkTelNo
        strEmail = mstrEmail
    End If
    
    Me.Hide
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("GetRefundAddress", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Public Sub InitialiseStatusBar(ByRef sbInfo As StatusBar)
    
    sbInfo.Panels(PANEL_VERNO).Text = "Ver. " & App.Major & "." & App.Minor & "." & App.Revision
    sbInfo.Panels(PANEL_VERNO).ToolTipText = "Program Version Number"
    sbInfo.Panels(PANEL_WSID + 1).Text = goSession.CurrentEnterprise.IEnterprise_WorkstationID & " "
    sbInfo.Panels(PANEL_WSID + 1).ToolTipText = "Current Work-Station Number"
    sbInfo.Panels(PANEL_DATE + 1).Text = Format$(Date, "DD-MMM-YY")
    If (mblnShowKeypadIcon = False) Then sbStatus.Panels("KeyBoard").Picture = Nothing

End Sub

Public Sub GetOrderAddress(ByRef strName As String, ByRef strPostCode, _
                            ByRef strAddress, ByRef strTelNo As String, _
                            ByRef strMobileNo As String, _
                            ByRef strWorkTelNo As String, _
                            ByRef strEmail As String, _
                            ByVal blnOrder As Boolean, Optional blnShowAddress = False)
Dim vAddress

    On Error GoTo HandleException
    
    Call ucccCustomer.Initialise(goSession, Me)
    Call ucccCustomer.Reset
    ucKeyPad1.Visible = False
    ucccCustomer.Top = 0
    ucccCustomer.Left = 0
    Me.Width = ucccCustomer.Width
    Me.Height = ucccCustomer.Height
    Me.Top = Screen.Height = Me.Height \ 2
    Me.Left = Screen.Width = Me.Width \ 2
    ucccCustomer.NameOnly = False
    mblnCompulsory = False
    If strPostCode <> "" And blnShowAddress Then
        strAddress = strAddress + vbCrLf + vbCrLf + vbCrLf + vbCrLf
        vAddress = Split(strAddress, vbCrLf)
        Call ucccCustomer.LoadData(strName, vAddress(0), vAddress(1), vAddress(2), vAddress(3), strPostCode, strTelNo, strMobileNo, strWorkTelNo, strEmail)
    End If
    Call ucccCustomer.ShowRefundDetails(vbNullString, Date, vbNullString, vbNullString, False, True, blnOrder)
    
    Me.Show vbModal
    
    If Cancel = False Then
        strName = UCase(mstrName)
        strPostCode = mstrPostCode
        strAddress = mstrAddress
        strTelNo = mstrTelNo
        strMobileNo = mstrMobileNo
        strWorkTelNo = mstrWorkTelNo
        strEmail = mstrEmail
    End If

    Me.Hide
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("GetOrderAddress", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Public Function GetPricePromiseAddress(ByRef strName As String, ByRef strPostCode, _
                            ByRef strAddress, ByRef strTelNo As String) As Boolean

    Call ucccCustomer.Initialise(goSession, Me)
    Call ucccCustomer.Reset
    ucKeyPad1.Visible = False
    ucccCustomer.Top = 0
    ucccCustomer.Left = 0
    Me.Width = ucccCustomer.Width
    Me.Height = ucccCustomer.Height
    Me.Top = Screen.Height = Me.Height \ 2
    Me.Left = Screen.Width = Me.Width \ 2
    ucccCustomer.NameOnly = False
    mblnCompulsory = True
    Call ucccCustomer.ShowRefundDetails(vbNullString, Date, vbNullString, vbNullString, False, True)
    Me.Show vbModal
    
    If Cancel = False Then
        strName = mstrName
        strPostCode = mstrPostCode
        strAddress = mstrAddress
        strTelNo = mstrTelNo
        GetPricePromiseAddress = True
    Else
        GetPricePromiseAddress = False
    End If

    Me.Hide

End Function

Public Sub GetWarrantyAddress(ByRef strName As String, ByRef strPostCode, _
                            ByRef strAddress, ByRef strTelNo As String)

    Call ucccCustomer.Initialise(goSession, Me)
    Call ucccCustomer.Reset
    ucccCustomer.Top = 0
    ucccCustomer.Left = 0
    Me.Width = ucccCustomer.Width
    Me.Height = ucccCustomer.Height + 300
    Me.Top = Screen.Height = Me.Height \ 2
    Me.Left = Screen.Width = Me.Width \ 2
    ucccCustomer.NameOnly = False
    mblnCompulsory = False
    Call ucccCustomer.ShowRefundDetails(vbNullString, Date, vbNullString, vbNullString, False, True)
    Me.Show vbModal
    
    If Cancel = False Then
        strName = mstrName
        strPostCode = mstrPostCode
        strAddress = mstrAddress
        strTelNo = mstrTelNo
    End If

    Me.Hide

End Sub


Public Sub GetParkedName(ByRef strName As String, ByRef strPostCode, _
                            ByRef strAddress, ByRef strTelNo As String)

    Call ucccCustomer.Initialise(goSession, Me)
    Call ucccCustomer.Reset
    ucccCustomer.Top = 0
    ucccCustomer.Left = 0
    Me.Width = ucccCustomer.Width
    Me.Height = ucccCustomer.Height + 300
    Me.Top = Screen.Height = Me.Height \ 2
    Me.Left = Screen.Width = Me.Width \ 2
    ucccCustomer.NameOnly = True
    mblnCompulsory = False
    Call ucccCustomer.ShowRefundDetails(vbNullString, Date, vbNullString, vbNullString, False, True)
    Me.Show vbModal
    
    If Cancel = False Then
        strName = mstrName
        strPostCode = mstrPostCode
        strAddress = mstrAddress
        strTelNo = mstrTelNo
    End If

    Me.Hide

End Sub

