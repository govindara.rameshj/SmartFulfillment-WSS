VERSION 5.00
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Begin VB.Form frmProjectNo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Project Loan"
   ClientHeight    =   2880
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7095
   Icon            =   "frmProjectNo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2880
   ScaleWidth      =   7095
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtLoanNo 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   960
      MaxLength       =   9
      TabIndex        =   1
      Top             =   1020
      Width           =   4995
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      HelpContextID   =   555
      Left            =   4020
      TabIndex        =   3
      Top             =   1980
      Width           =   1935
   End
   Begin VB.CommandButton cmdProceed 
      Caption         =   "Proceed"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      HelpContextID   =   555
      Left            =   960
      TabIndex        =   2
      Top             =   1980
      Width           =   1935
   End
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   2580
      Top             =   2820
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Enter Project Loan Agreement Number"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   675
      Left            =   120
      TabIndex        =   0
      Top             =   180
      Width           =   6855
   End
End
Attribute VB_Name = "frmProjectNo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim mblnShowKeyPad  As Boolean
Dim mstrLoanNo      As String

Public Function GetLoanNumber(ShowNumPad As Boolean) As String

    mstrLoanNo = ""
'    If (ShowNumPad = True) Then Call ShowNumPad
    Call Me.Show(vbModal)
    GetLoanNumber = mstrLoanNo

End Function

Private Sub cmdCancel_Click()

    mstrLoanNo = "000000000"
    Me.Hide
    
End Sub

Private Sub cmdProceed_Click()
                
    mstrLoanNo = Right("000000000" & Trim$(txtLoanNo.Text), 9)
    Me.Hide

End Sub

Private Sub Form_Activate()
    
    ucKeyPad1.ShowNumPad
    Call Form_Resize

End Sub

Private Sub Form_Load()
                
    ucKeyPad1.Visible = False
    Me.BackColor = RGBQuery_BackColour
    Call CentreForm(Me)

End Sub

Public Sub ShowNumPad()
    
    ucKeyPad1.Visible = True
    mblnShowKeyPad = True
    ucKeyPad1.ShowNumPad

End Sub

Private Sub Form_Resize()

    If (ucKeyPad1.Visible = True) Or ((Me.Visible = False) And (mblnShowKeyPad = True)) Then
        Me.Height = ucKeyPad1.Top + ucKeyPad1.Height + 640
    Else
        Me.Height = 3360
    End If
    ucKeyPad1.Left = (Me.Width - ucKeyPad1.Width) / 2
    Call CentreForm(Me)

End Sub

Private Sub txtLoanNo_KeyPress(KeyAscii As Integer)

    If (Len(txtLoanNo.Text) > 0) And (KeyAscii = vbKeyReturn) Then cmdProceed.Value = True

End Sub

Private Sub ucKeyPad1_Resize()
    
    ucKeyPad1.Width = ucKeyPad1.Width

End Sub
