VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmDeliveryDetails 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   4575
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8715
   Icon            =   "frmDeliveryDetails.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4575
   ScaleWidth      =   8715
   ShowInTaskbar   =   0   'False
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   60
      Top             =   3660
      Visible         =   0   'False
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   25
      Top             =   4200
      Width           =   8715
      _ExtentX        =   15372
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmDeliveryDetails.frx":0A96
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7990
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   635
            MinWidth        =   635
            Picture         =   "frmDeliveryDetails.frx":143C
            Key             =   "NumPad"
            Object.ToolTipText     =   "Show/Hide Num Pad"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "11:15"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraEntry 
      BorderStyle     =   0  'None
      Height          =   3555
      Left            =   540
      TabIndex        =   0
      Top             =   60
      Width           =   7575
      Begin VB.Frame fraDate 
         Caption         =   "Date Required"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   0
         TabIndex        =   9
         Top             =   2640
         Width           =   7575
         Begin MSComCtl2.DTPicker dtpCollDate 
            Height          =   420
            Left            =   2760
            TabIndex        =   32
            Top             =   300
            Width           =   2295
            _ExtentX        =   4048
            _ExtentY        =   741
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   57475073
            CurrentDate     =   41690
         End
         Begin VB.CommandButton cmdProceed 
            Caption         =   "Proceed"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   5160
            TabIndex        =   11
            Top             =   240
            Width           =   1695
         End
         Begin VB.Label lblDatelbl 
            Caption         =   "Date"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   240
            TabIndex        =   10
            Top             =   300
            Width           =   3495
         End
      End
      Begin VB.Frame fraFulfillmentInstructions 
         Caption         =   "Fulfillment Channel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3000
         Left            =   0
         TabIndex        =   29
         Top             =   2640
         Visible         =   0   'False
         Width           =   7575
         Begin VB.CommandButton cmdFulfillmentProceed 
            Caption         =   "Proceed"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   5160
            TabIndex        =   31
            Top             =   2200
            Width           =   1695
         End
         Begin VB.Label lblFulfillmentInstruction 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1600
            Left            =   240
            TabIndex        =   30
            Top             =   300
            Width           =   7100
         End
      End
      Begin VB.Frame fraDeliveryAdd 
         Caption         =   "Enter Delivery Address"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2655
         Left            =   0
         TabIndex        =   12
         Top             =   0
         Visible         =   0   'False
         Width           =   7575
         Begin VB.TextBox txtDelAdd4 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1800
            MaxLength       =   30
            TabIndex        =   19
            Top             =   2100
            Width           =   3915
         End
         Begin VB.TextBox txtDelPostCode 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1800
            MaxLength       =   10
            TabIndex        =   14
            Top             =   360
            Width           =   1695
         End
         Begin VB.TextBox txtDelAdd3 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1800
            MaxLength       =   30
            TabIndex        =   18
            Top             =   1680
            Width           =   3915
         End
         Begin VB.TextBox txtDelAdd2 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1800
            MaxLength       =   30
            TabIndex        =   17
            Top             =   1260
            Width           =   3915
         End
         Begin VB.TextBox txtDelAdd1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1800
            MaxLength       =   30
            TabIndex        =   16
            Top             =   840
            Width           =   3915
         End
         Begin VB.CommandButton cmdCancelAdd 
            Caption         =   "Cancel"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   5880
            TabIndex        =   21
            Top             =   1980
            Width           =   1575
         End
         Begin VB.CommandButton cmdNewAdd 
            Caption         =   "Accept"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   5880
            TabIndex        =   20
            Top             =   1320
            Width           =   1575
         End
         Begin VB.Label lblDelAddresslbl4 
            BackStyle       =   0  'Transparent
            Caption         =   "Address"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   180
            TabIndex        =   28
            Top             =   2100
            Visible         =   0   'False
            Width           =   1635
         End
         Begin VB.Label lblDelAddresslbl3 
            BackStyle       =   0  'Transparent
            Caption         =   "Address"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   180
            TabIndex        =   27
            Top             =   1680
            Visible         =   0   'False
            Width           =   1635
         End
         Begin VB.Label lblDelAddresslbl2 
            BackStyle       =   0  'Transparent
            Caption         =   "Address"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   120
            TabIndex        =   26
            Top             =   1200
            Visible         =   0   'False
            Width           =   1635
         End
         Begin VB.Label lblDelPostcode 
            BackStyle       =   0  'Transparent
            Caption         =   "Postcode"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   180
            TabIndex        =   13
            Top             =   360
            Width           =   1635
         End
         Begin VB.Label lblDelAddresslbl1 
            BackStyle       =   0  'Transparent
            Caption         =   "Address"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   180
            TabIndex        =   15
            Top             =   840
            Width           =   1635
         End
      End
      Begin VB.Frame fraDeliveryNotes 
         Caption         =   "Delivery Instructions"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2715
         Left            =   480
         TabIndex        =   22
         Top             =   0
         Width           =   6975
         Begin VB.TextBox txtDeliveryNotes 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1635
            Left            =   180
            MultiLine       =   -1  'True
            TabIndex        =   23
            Top             =   300
            Width           =   6615
         End
         Begin VB.CommandButton cmdSaveNotes 
            Caption         =   "Proceed"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   5160
            TabIndex        =   24
            Top             =   2040
            Width           =   1695
         End
      End
      Begin VB.Frame fraAddress 
         Caption         =   "Customer Address"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2655
         Left            =   0
         TabIndex        =   1
         Top             =   0
         Width           =   7575
         Begin VB.CommandButton cmdAcceptAdd 
            Caption         =   "Accept"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   3720
            TabIndex        =   7
            Top             =   1980
            Width           =   1695
         End
         Begin VB.CommandButton cmdChangeAdd 
            Caption         =   "Change"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   5700
            TabIndex        =   8
            Top             =   1980
            Width           =   1695
         End
         Begin VB.Label Label1 
            Caption         =   "Address"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   180
            TabIndex        =   4
            Top             =   660
            Width           =   1215
         End
         Begin VB.Label lblCustName 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   375
            Left            =   1800
            TabIndex        =   3
            Top             =   240
            Width           =   5595
         End
         Begin VB.Label Label3 
            Caption         =   "Name"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   180
            TabIndex        =   2
            Top             =   240
            Width           =   1335
         End
         Begin VB.Label lblConfirmAddress 
            Caption         =   "Confirm delivery address correct"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   120
            TabIndex        =   6
            Top             =   2040
            Width           =   3675
         End
         Begin VB.Label lblCustAddress 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   1275
            Left            =   1800
            TabIndex        =   5
            Top             =   660
            Width           =   5595
         End
      End
   End
End
Attribute VB_Name = "frmDeliveryDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const MODULE_NAME As String = "frmDeliveryDetails"

Dim mlngCloseKeyCode As Long
Dim mlngUseKeyCode   As Long

Dim mstrDAddressLine1 As String
Dim mstrDAddressLine2 As String
Dim mstrDAddressLine3 As String
Dim mstrDAddressLine4 As String
Dim mstrDPostCode As String
'Dim mstrDHouseName As String

Dim WithEvents frmMatchCode As frmMatchCode
Attribute frmMatchCode.VB_VarHelpID = -1

Dim blnVolumeMovementRequirementSwitch As Boolean   'SC300e
Dim strOrderNumber As String                        'SC300e

'Error handler variables
Private errorNo As Long
Private errorDesc As String

Private Sub cmdAcceptAdd_Click()

    On Error GoTo HandleException

    fraDate.Visible = True
    Me.Height = fraDate.Top + fraDate.Height + sbStatus.Height + 480
    If (ucKeyPad1.Visible = True) Then
        Me.Height = Me.Height + ucKeyPad1.Height
        ucKeyPad1.Top = ucKeyPad1.Top + fraDate.Height
    End If

    cmdAcceptAdd.Enabled = False
    cmdChangeAdd.Enabled = False
    Call CentreForm(Me)
    If (dtpCollDate.Visible = True) And (dtpCollDate.Enabled = True) Then Call dtpCollDate.SetFocus

    'SC300e
    If blnVolumeMovementRequirementSwitch = True Then

        cmdProceed.Caption = goSession.GetParameter(PRM_KEY_SAVE) & "-Accept"

        cmdFulfillmentProceed.Caption = goSession.GetParameter(PRM_KEY_SAVE) & "-" & cmdFulfillmentProceed.Caption

    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("cmdAcceptAdd_Click", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub cmdProceed_Click()
    
    'SC300e
    If blnVolumeMovementRequirementSwitch = True Then
    
       If mblnForDelivery = True Then
    
            Me.Height = Me.Height + 3000

            fraEntry.Height = fraEntry.Height + 3000

            fraFulfillmentInstructions.Visible = True

            fraFulfillmentInstructions.Top = fraDate.Top + fraDate.Height

            lblFulfillmentInstruction.Caption = CheckFulfilment

            cmdProceed.Enabled = False

            Exit Sub

       End If

    End If

    CommonProceedProcess

End Sub

Private Sub cmdFulfillmentProceed_Click()

   'SC300e
   CommonProceedProcess

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'                                   SC300e - Start                                    '
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Private Sub CommonProceedProcess()
    
    On Error GoTo HandleException
        
    If (dtpCollDate.Value < Date) Then
        dtpCollDate.Value = Date
        Call MsgBoxEx("Invalid date entered." & vbCrLf & "Date changed to today", vbOKOnly, "Incorrect Date", , , , , RGBMsgBox_WarnColour)
        If (dtpCollDate.Visible = True) And (dtpCollDate.Enabled = True) Then dtpCollDate.SetFocus
        Exit Sub
    End If

    If (lblConfirmAddress.Visible = False) Then
        Call Me.Hide
    Else
        If (MsgBoxEx("Are there any delivery instructions to be entered", vbYesNo, "Enter Delivery Instructions", , , , , RGBMSGBox_PromptColour) = vbYes) Then
            fraAddress.Visible = False
            fraDeliveryNotes.Visible = True
            If (txtDeliveryNotes.Visible = True) And (txtDeliveryNotes.Enabled = True) Then txtDeliveryNotes.SetFocus
            fraDeliveryNotes.Top = fraAddress.Top
            Me.Height = fraDeliveryNotes.Top + fraDeliveryNotes.Height + sbStatus.Height + 480
            If (ucKeyPad1.Visible = True) Then
                Me.Height = Me.Height + ucKeyPad1.Height
                ucKeyPad1.Top = fraDeliveryNotes.Height + 120
            End If
            fraDate.Visible = False
            Call CentreForm(Me)
        Else
            Call Me.Hide
        End If
    End If

    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("CommonProceedProcess", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Function CheckFulfilment() As String

   Dim VolumeMovement As COMTPWickes_InterOp_Interface.IVolumeMovement
   Dim VolumeMovementFactory As New COMTPWickes_InterOp_Wrapper.VolumeMovements
   
   Dim OrderHeader As COMTPWickes_InterOp_Interface.IOrderHeader
   Dim OrderHeaderFactory As New COMTPWickes_InterOp_Wrapper.OrderHeader
   
   Dim LineNo As Long
   
   Dim SellingStoreLineNo As Integer
   Dim ProductCode As Long
   Dim ProductDescription As String
   Dim TotalOrderQuantity As Double
   Dim QuantityTaken As Integer
   Dim UnitOfMeasure As String
   Dim LineValue As Double
   Dim DeliveryChargeItem As Boolean
   Dim SellingPrice As Double
   
   Dim DeliveryChargeRunningTotal As Double
  
   On Error GoTo HandleException
   
   Set VolumeMovement = VolumeMovementFactory.FactoryGet
   Set OrderHeader = OrderHeaderFactory.FactoryGet

   DeliveryChargeRunningTotal = 0
   SellingStoreLineNo = 0

   For LineNo = 2 To frmTill.sprdLines.MaxRows Step 1
      
      frmTill.sprdLines.Row = LineNo
      
      frmTill.sprdLines.Col = COL_VOIDED
      If (Val(frmTill.sprdLines.Value) = 0) Then

         frmTill.sprdLines.Col = COL_LINETYPE
         If Val(frmTill.sprdLines.Value) = LT_ITEM Then

            SellingStoreLineNo = SellingStoreLineNo + 1

            frmTill.sprdLines.Col = COL_PARTCODE
            ProductCode = Val(frmTill.sprdLines.Value)

            frmTill.sprdLines.Col = COL_DESC
            ProductDescription = frmTill.sprdLines.Value

            frmTill.sprdLines.Col = COL_QTY
            TotalOrderQuantity = Val(frmTill.sprdLines.Value)
            
            frmTill.sprdLines.Col = COL_TAKE_NOW
            QuantityTaken = Val(frmTill.sprdLines.Value)
            
            frmTill.sprdLines.Col = COL_PARTCODE
            UnitOfMeasure = StockUnitOfMeasure(frmTill.sprdLines.Value)
            
            frmTill.sprdLines.Col = COL_INCTOTAL
            LineValue = Val(frmTill.sprdLines.Value)

            frmTill.sprdLines.Col = COL_SALETYPE
            If frmTill.sprdLines.Value = "D" Then
               DeliveryChargeItem = True
               DeliveryChargeRunningTotal = DeliveryChargeRunningTotal + LineValue
            Else
               DeliveryChargeItem = False
            End If
            
            frmTill.sprdLines.Col = COL_SELLPRICE
            SellingPrice = Val(frmTill.sprdLines.Value)

            OrderHeader.AddByValue SellingStoreLineNo, ProductCode, ProductDescription, TotalOrderQuantity, _
                                   QuantityTaken, UnitOfMeasure, LineValue, DeliveryChargeItem, SellingPrice

         End If
      
      End If
   Next LineNo
   
   With OrderHeader

      .SellingStoreCode = goSession.CurrentEnterprise.IEnterprise_StoreNumber
      .SellingStoreOrderNumber = Val(strOrderNumber)
      .RequiredDeliveryDate = dtpCollDate.Value
      .DeliveryCharge = DeliveryChargeRunningTotal

      frmTill.sprdLines.Row = 1
      frmTill.sprdLines.Col = COL_INCTOTAL
      .TotalOrderValue = Val(frmTill.sprdLines.Value)

      .SaleDate = Now
      If Len(txtDelPostCode.Text) <> 0 Then
         .DeliveryPostcode = txtDelPostCode.Text
      Else
         .DeliveryPostcode = mstrDPostCode
      End If
      .ToBeDelivered = True
   
   End With

   CheckFulfilment = VolumeMovement.OMCheckFulfilment(OrderHeader)

    Exit Function

HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("CheckFulfilment", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Function

Private Function StockUnitOfMeasure(ByVal StockCode As String) As String

   Dim rs As New ADODB.Recordset
   Dim Sql As String

   Sql = "select BUYU from STKMAS where SKUN = " & StockCode
   Call DebugMsg("frmDeliveryDetails", "Get unit of measure from STKMAS", endlTraceOut, Sql)
   Call rs.Open(Sql, goDatabase.Connection)

   If rs Is Nothing Then
   
      StockUnitOfMeasure = ""
   
   Else
   
      If rs.RecordCount = 1 Then
        
         StockUnitOfMeasure = rs.Fields("BUYU").Value
         
      Else
      
         StockUnitOfMeasure = ""
      
      End If
   
   End If

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'                                   SC300e - End                                      '
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Private Sub cmdCancelAdd_Click()

    fraDeliveryAdd.Visible = False
    fraAddress.Visible = True

End Sub

Private Sub cmdChangeAdd_Click()

    On Error GoTo HandleException

    txtDelAdd1.Text = mstrDAddressLine1
    txtDelAdd2.Text = mstrDAddressLine2
    txtDelAdd3.Text = mstrDAddressLine3
    txtDelPostCode.Text = mstrDPostCode
    If (goSession.GetParameter(PRM_COUNTRY_CODE) = "IE") Then
        txtDelAdd4.Text = mstrDPostCode
    End If
    fraAddress.Visible = False
    fraDeliveryAdd.Visible = True
    If (txtDelPostCode.Visible = True) And (txtDelPostCode.Enabled = True) Then
        txtDelPostCode.SetFocus
    Else
        If (txtDelAdd1.Visible = True) And (txtDelAdd1.Enabled = True) Then txtDelAdd1.SetFocus
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("cmdChangeAdd_Click", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub cmdNewAdd_Click()

    On Error GoTo HandleException
    
    ' Referral 788 - Add validation of postcode
    With txtDelPostCode
        ' Not visible if in Ireland (don't have postcodes!)
        If (.Visible = True) And (Not .Text Like "[A-Z][1-9A-Z]*[0-9 ][0-9][A-Z][A-Z]") Then
            Call MsgBoxEx("Invalid postcode", vbOKOnly + vbInformation, "Customer Information")
            .SetFocus
        Else
        ' End of Referral 788
            mstrDAddressLine1 = txtDelAdd1.Text
            mstrDAddressLine2 = txtDelAdd2.Text
            mstrDAddressLine3 = txtDelAdd3.Text
            mstrDAddressLine4 = txtDelAdd4.Text
            mstrDPostCode = .Text
        '    mstrdhouseName = txtdelHouseName.Text
            If (goSession.GetParameter(PRM_COUNTRY_CODE) = "IE") Then
                mstrDPostCode = txtDelAdd4.Text
                mstrDAddressLine4 = ""
            End If
            
            lblCustAddress.Caption = txtDelAdd1.Text & vbCrLf & txtDelAdd2.Text & vbCrLf & txtDelAdd3.Text & vbCrLf & mstrDAddressLine4
            If (goSession.GetParameter(PRM_COUNTRY_CODE) <> "IE" And mstrDAddressLine4 <> "") Then
                lblCustAddress.Caption = lblCustAddress.Caption & ", "
            End If
            lblCustAddress.Caption = lblCustAddress.Caption & mstrDPostCode
            fraDeliveryAdd.Visible = False
            fraAddress.Visible = True
        End If
    End With
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("cmdNewAdd_Click", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub cmdSaveNotes_Click()

Dim intIndex As Integer
Dim intSize As Integer
Dim intPos As Integer
Dim strTemp As String
Dim intLen As Integer
    
    On Error GoTo HandleException
    
    txtDeliveryNotes.Text = Replace(txtDeliveryNotes.Text, vbCrLf, "")
    If Len(txtDeliveryNotes.Text) > 40 Then
        While Len(txtDeliveryNotes.Text) > 40
            If Mid(txtDeliveryNotes.Text, 40) <> " " Then
                intPos = 40
                For intPos = 39 To 1 Step -1
                    If Mid(txtDeliveryNotes.Text, intPos, 1) = " " Or intPos = 1 Then
                        Exit For
                    End If
                Next intPos
                strTemp = strTemp + Mid(txtDeliveryNotes.Text, 1, intPos) + vbCrLf
                txtDeliveryNotes.Text = Mid(txtDeliveryNotes.Text, intPos + 1)
            End If
        Wend
        strTemp = strTemp + txtDeliveryNotes.Text 'MO'C Hubs V1.5 - Remove blank line at end Ref 417 (+ vbCrLf)
        intLen = Len(strTemp)
        txtDeliveryNotes.Text = Mid(strTemp, 1, intLen)
    End If
    
    Call Me.Hide
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("cmdSaveNotes_Click", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub dtxtCollDate_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then cmdProceed.Value = True

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

Dim lngKeyIn As Integer
Dim lngRowNo As Integer
    
    On Error GoTo HandleException
    
    Call DebugMsg(vbNullString, vbNullString, endlDebug, "KDn" & KeyCode & "/" & Shift)
    
    If (Shift = 0) Then
        Select Case (KeyCode)
            Case (mlngCloseKeyCode):
                If (fraAddress.Visible = True) And (cmdChangeAdd.Enabled = True) Then
                    cmdChangeAdd.Value = True
                    KeyCode = 0
                    Exit Sub
                End If
                If (fraDeliveryAdd.Visible = True) Then
                    cmdCancelAdd.Value = True
                    KeyCode = 0
                    Exit Sub
                End If
            Case (mlngUseKeyCode):
                If (fraAddress.Visible = True) And (cmdAcceptAdd.Enabled = True) Then
                    cmdAcceptAdd.Value = True
                    KeyCode = 0
                    Exit Sub
                End If
                If (fraDeliveryAdd.Visible = True) Then
                    cmdNewAdd.Value = True
                    KeyCode = 0
                    Exit Sub
                End If
                If (fraDate.Visible = True) And (cmdProceed.Enabled = True) Then
                    cmdProceed.Value = True
                    KeyCode = 0
                    Exit Sub
                End If
                If (fraDeliveryNotes.Visible = True) Then
                    cmdSaveNotes.Value = True
                    KeyCode = 0
                    Exit Sub
                End If

                'SC300e
                If blnVolumeMovementRequirementSwitch = True Then

                    If (fraFulfillmentInstructions.Visible = True) Then
                        cmdFulfillmentProceed.Value = True
                        KeyCode = 0
                        Exit Sub
                    End If

                End If

        End Select
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("Form_KeyDown", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub


Private Sub Form_Load()

Dim strKey As String
    
    On Error GoTo HandleException

    Call InitialiseStatusBar(sbStatus)
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    fraEntry.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)

    strKey = goSession.GetParameter(PRM_KEY_CLOSE)
    cmdChangeAdd.Caption = strKey & "-" & cmdChangeAdd.Caption
    cmdCancelAdd.Caption = strKey & "-" & cmdCancelAdd.Caption
    
    If Left$(strKey, 1) = "F" Then
        mlngCloseKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngCloseKeyCode = AscW(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_SAVE)
    cmdAcceptAdd.Caption = strKey & "-" & cmdAcceptAdd.Caption
    cmdNewAdd.Caption = strKey & "-" & cmdNewAdd.Caption
    cmdProceed.Caption = strKey & "-" & cmdProceed.Caption
    cmdSaveNotes.Caption = strKey & "-" & cmdSaveNotes.Caption
    If Left$(strKey, 1) = "F" Then
        mlngUseKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngUseKeyCode = AscW(UCase$(strKey))
    End If
    dtpCollDate.Value = Date
    fraEntry.Left = 60
    Call FormatDelAddress

    On Error Resume Next
    blnVolumeMovementRequirementSwitch = goSession.GetParameter(983000)   'SC300e
    On Error GoTo 0
    Err.Clear
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("Form_Load", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub FormatDelAddress()
    
    If (goSession.GetParameter(PRM_COUNTRY_CODE) = "IE") Then
        lblDelPostcode.Visible = False
        txtDelPostCode.Visible = False
        
        lblDelAddresslbl1.Caption = "House Name"
        lblDelAddresslbl2.Caption = "Street Address"
        lblDelAddresslbl3.Caption = "Town"
        lblDelAddresslbl4.Caption = "County"
        lblDelAddresslbl2.Visible = True
        lblDelAddresslbl3.Visible = True
        lblDelAddresslbl4.Visible = True
        lblDelAddresslbl1.Top = lblDelAddresslbl1.Top - 360
        lblDelAddresslbl2.Top = lblDelAddresslbl2.Top - 300
        lblDelAddresslbl3.Top = lblDelAddresslbl3.Top - 240
        lblDelAddresslbl4.Top = lblDelAddresslbl4.Top - 180
        txtDelAdd1.Top = txtDelAdd1.Top - 360
        txtDelAdd2.Top = txtDelAdd2.Top - 300
        txtDelAdd3.Top = txtDelAdd3.Top - 240
        txtDelAdd4.Top = txtDelAdd4.Top - 180
    End If

End Sub

Public Sub InitialiseStatusBar(ByRef sbInfo As StatusBar)
    
    sbInfo.Panels(PANEL_VERNO).Text = "Ver. " & App.Major & "." & App.Minor & "." & App.Revision
    sbInfo.Panels(PANEL_VERNO).ToolTipText = "Program Version Number"
    sbInfo.Panels(PANEL_WSID + 1).Text = goSession.CurrentEnterprise.IEnterprise_WorkstationID & " "
    sbInfo.Panels(PANEL_WSID + 1).ToolTipText = "Current Work-Station Number"
    sbInfo.Panels(PANEL_DATE + 1).Text = Format$(Date, "DD-MMM-YY")
    If (mblnShowKeypadIcon = False) Then sbStatus.Panels("NumPad").Picture = Nothing

End Sub

Private Sub lblCompPhoneNum_Click()
End Sub

Private Sub fraCaptCompDetails_DragDrop(Source As Control, X As Single, Y As Single)

End Sub

Private Sub sbStatus_PanelClick(ByVal Panel As MSComctlLib.Panel)
    
    Select Case (Panel.Key)
        Case ("NumPad"):
                If ((Panel.Picture Is Nothing) = False) Then
                    ucKeyPad1.Visible = Not ucKeyPad1.Visible
                    If (fraDate.Visible = True) Then
                        Me.Height = fraDate.Top + fraDate.Height + 480 + sbStatus.Height
                    Else
                        Me.Height = fraAddress.Top + fraAddress.Height + 480 + sbStatus.Height
                    End If
                    If (ucKeyPad1.Visible = True) Then
                        Me.Height = Me.Height + ucKeyPad1.Height
                        Me.Width = ucKeyPad1.Width + 120
                        If (fraDate.Visible = True) Then
                            ucKeyPad1.Top = fraDate.Top + fraDate.Height + 120
                        Else
                            ucKeyPad1.Top = fraAddress.Top + fraAddress.Height + 120
                        End If
                        fraEntry.Left = (Me.Width - fraEntry.Width) / 2
                    Else
                        Me.Width = fraDeliveryAdd.Width + 240
                        fraEntry.Left = 60
                    End If
                    Call CentreForm(Me)
                End If
    End Select

End Sub

Public Function GetCollectionDate(CustomerName As String, _
                                  AddressLine1 As String, AddressLine2 As String, _
                                  AddressLine3 As String, PostCode As String) As Date

    Me.Caption = "Confirm Collection Date"
    lblDatelbl.Caption = "Items to be collected on"
    'Hide Delivery Address buttons
    lblConfirmAddress.Visible = False
    cmdChangeAdd.Visible = False
    cmdAcceptAdd.Visible = False
    cmdChangeAdd.Enabled = False
    cmdAcceptAdd.Enabled = False
    'Display Details
    lblCustName.Caption = CustomerName
    lblCustAddress.Caption = AddressLine1 & vbCrLf & AddressLine2 & vbCrLf & AddressLine3 & vbCrLf & PostCode
    fraDate.Visible = True
    fraDeliveryNotes.Visible = False
    
    'Resize form
    fraAddress.Height = lblConfirmAddress.Top + 120
    fraDate.Top = fraAddress.Top + fraAddress.Height + 120
    Me.Height = fraDate.Top + fraDate.Height + sbStatus.Height + 480
    Me.Width = fraAddress.Width + 240
    Call CentreForm(Me)
    
    Me.Show (vbModal)
    
    'Pass out values
    GetCollectionDate = dtpCollDate.Value
    
End Function

Public Sub GetDeliveryAddressVolumeMovement(ByRef dteDeliveryDate As Date, _
                                            ByRef CustomerName As String, _
                                            ByRef AddressLine1 As String, ByRef AddressLine2 As String, _
                                            ByRef AddressLine3 As String, ByRef PostCode As String, _
                                            ByRef AddressLine4 As String, ByRef DelNotes As String, _
                                            ByVal OrderNumber As String)

   strOrderNumber = OrderNumber

   Call GetDeliveryAddress(dteDeliveryDate, CustomerName, AddressLine1, AddressLine2, AddressLine3, PostCode, AddressLine4, DelNotes)

End Sub

Public Sub GetDeliveryAddress(ByRef dteDeliveryDate As Date, CustomerName As String, _
                                  AddressLine1 As String, AddressLine2 As String, _
                                  AddressLine3 As String, PostCode As String, _
                                  AddressLine4 As String, DelNotes As String)
    
    On Error GoTo HandleException
    
    Me.Caption = "Confirm Delivery Details"
    'Hide Delivery Address buttons
    lblConfirmAddress.Visible = True
    cmdChangeAdd.Visible = True
    cmdAcceptAdd.Visible = True
    'Display Details
    lblCustName.Caption = CustomerName
    lblCustAddress.Caption = AddressLine1 & vbCrLf & AddressLine2 & vbCrLf & AddressLine3 & vbCrLf & AddressLine4 & vbCrLf & PostCode
    mstrDAddressLine1 = AddressLine1
    mstrDAddressLine2 = AddressLine2
    mstrDAddressLine3 = AddressLine3
    mstrDAddressLine4 = AddressLine4
    mstrDPostCode = PostCode
    
    lblDatelbl.Caption = "Items to be delivered on"
    fraDate.Visible = True
    fraDeliveryNotes.Visible = False
    
    'Resize form
    fraDate.Visible = False
    Me.Height = fraAddress.Top + fraAddress.Height + sbStatus.Height + 480
    Me.Width = fraAddress.Width + 240
    Call CentreForm(Me)
    
    Me.Show (vbModal)
    
    AddressLine1 = mstrDAddressLine1
    AddressLine2 = mstrDAddressLine2
    AddressLine3 = mstrDAddressLine3
    AddressLine4 = mstrDAddressLine4
    PostCode = mstrDPostCode
    
    dteDeliveryDate = dtpCollDate.Value
    DelNotes = txtDeliveryNotes.Text
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("GetDeliveryAddress", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub txtDelAdd1_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub txtDelAdd2_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub txtDelAdd3_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub txtDelAdd4_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub txtDelPostCode_GotFocus()

    txtDelPostCode.SelStart = 0
    txtDelPostCode.SelLength = Len(txtDelPostCode.Text)

End Sub

Private Sub txtDelPostCode_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If

End Sub

