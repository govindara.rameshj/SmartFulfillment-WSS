VERSION 5.00
Begin VB.Form frmParkTran 
   Caption         =   "Park Transaction"
   ClientHeight    =   2295
   ClientLeft      =   2580
   ClientTop       =   3615
   ClientWidth     =   6015
   LinkTopic       =   "Form1"
   ScaleHeight     =   2295
   ScaleWidth      =   6015
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "NO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   600
      TabIndex        =   1
      Top             =   1080
      Width           =   1695
   End
   Begin VB.CommandButton cmdPark 
      Caption         =   "YES"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   3720
      TabIndex        =   0
      Top             =   1080
      Width           =   1695
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "PARK TRANSACTION"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   0
      TabIndex        =   2
      Top             =   360
      Width           =   6015
   End
End
Attribute VB_Name = "frmParkTran"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public ParkTransaction As Boolean

Private Sub cmdPark_Click()
    
    ParkTransaction = True
    Call Me.Hide

End Sub

Private Sub cmdCancel_Click()

    ParkTransaction = False
    Call Me.Hide

End Sub

