VERSION 5.00
Begin VB.Form frmRefundPwd 
   BackColor       =   &H0080FFFF&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Enter Refund Password"
   ClientHeight    =   3210
   ClientLeft      =   1680
   ClientTop       =   1755
   ClientWidth     =   7080
   Icon            =   "frmRefundPwd.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3210
   ScaleWidth      =   7080
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraLogon 
      BorderStyle     =   0  'None
      Height          =   1935
      Left            =   120
      TabIndex        =   4
      Top             =   0
      Visible         =   0   'False
      Width           =   6735
      Begin VB.TextBox txtAuthID 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   2400
         MaxLength       =   3
         TabIndex        =   6
         Top             =   120
         Width           =   975
      End
      Begin VB.TextBox txtPassword 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         IMEMode         =   3  'DISABLE
         Left            =   2400
         MaxLength       =   5
         PasswordChar    =   "*"
         TabIndex        =   5
         Top             =   1320
         Width           =   1335
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Auth ID"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   120
         TabIndex        =   12
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label lblPasswordlbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Password"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   120
         TabIndex        =   11
         Top             =   1320
         Width           =   1695
      End
      Begin VB.Label lblFullName 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   3480
         TabIndex        =   10
         Top             =   720
         Width           =   2655
      End
      Begin VB.Label lblInitials 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   2400
         TabIndex        =   9
         Top             =   720
         Width           =   975
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Initials"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   120
         TabIndex        =   8
         Top             =   720
         Width           =   1335
      End
      Begin VB.Label lblLoggedUserID 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   2400
         TabIndex        =   7
         Top             =   120
         Visible         =   0   'False
         Width           =   975
      End
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   1200
      TabIndex        =   2
      Top             =   2040
      Width           =   1695
   End
   Begin VB.TextBox txtRefundPwd 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      IMEMode         =   3  'DISABLE
      Left            =   2520
      MaxLength       =   5
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   960
      Width           =   2055
   End
   Begin VB.CommandButton cmdContinue 
      Caption         =   "Continue"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   4080
      TabIndex        =   3
      Top             =   2040
      Width           =   1695
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "ENTER REFUND PASSWORD"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   6975
   End
End
Attribute VB_Name = "frmRefundPwd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>************************************************************************************
'* Module : frmRefundPwd
'* Date   : 17/02/03
'* Author : Unknown
'*$Archive: /Projects/OasysV2/VB/Till/frmRefundPwd.frm $
'******************************************************************************************
'* Summary: Used to enter the Refund password.  Used for refund authorisation
'******************************************************************************************
'* $Author: Mauricem $ $Date: 29/01/04 12:06 $ $Revision: 3 $
'* Versions:
'* 17/02/03    Unknown
'*             Header added.
'</CAMH>***********************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmRefundPwd"

Const PWD_TYPE_EFT As Byte = 1
Const PWD_TYPE_REFUND As Byte = 2

Private mblnPasswordOk   As Boolean
Private mstrPasswordType As String
Private mbytPasswordType As Byte

Public Function VerifyRefundPassword() As Boolean

    mbytPasswordType = PWD_TYPE_REFUND
    Me.Caption = "Enter Refund Password"
    mstrPasswordType = "Refund Password"
    Call Me.Show(vbModal)
    VerifyRefundPassword = mblnPasswordOk

End Function

Public Function VerifyEFTPassword() As Boolean

    mbytPasswordType = PWD_TYPE_EFT
    Me.Caption = "Enter Authorisation Code"
    mstrPasswordType = "Authorisation Code"
    fraLogon.Visible = True
    Call Me.Show(vbModal)
    VerifyEFTPassword = mblnPasswordOk

End Function

Private Sub cmdCancel_Click()

    mblnPasswordOk = False
    Call Me.Hide

End Sub

Private Sub cmdContinue_Click()
    
Const PROCEDURE_NAME As String = "cmdContinue_Click"
    
    'Password entered correctly so report to calling form
    Screen.MousePointer = vbNormal
    If mbytPasswordType = PWD_TYPE_EFT Then
        mblnPasswordOk = VerifyAuthCode
    Else
        mblnPasswordOk = VerifyRefundCode
    End If
    If mblnPasswordOk = True Then Call Me.Hide

End Sub

Private Function VerifyRefundCode() As Boolean

Dim oSysOpt As Object
Dim strPwd  As String 'used to hold password pre-padded with 0's to make 5 chars
    
    VerifyRefundCode = False
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Refund password verify")
    'check current refund password filled in
    If txtRefundPwd.Text = "" Then
        Call MsgBoxEx(mstrPasswordType & " not entered" & vbCrLf & "Enter " & mstrPasswordType & " and continue", vbInformation, "Missing refund password", , , , , RGBMsgBox_WarnColour)
        Call txtRefundPwd.SetFocus
        Exit Function
    End If
    
    'Get System Options for updating
    Screen.MousePointer = vbHourglass
    Set oSysOpt = goDatabase.CreateBusinessObject(CLASSID_SYSTEMOPTIONS)
    Call oSysOpt.IBo_AddLoadField(FID_SYSTEMOPTIONS_StoreManagerPassword)
    Call oSysOpt.IBo_LoadMatches
    'Pad existing password to make sure 5 characters long
    strPwd = txtRefundPwd.Text
    While Len(strPwd) < 5
        strPwd = "0" & strPwd
    Wend
    'check new current password is correct
    If (oSysOpt.StoreManagerPassword <> strPwd) Then
        Call MsgBoxEx("Incorrect refund password entered", vbInformation, "Invalid Password", , , , , RGBMsgBox_WarnColour)
        Call txtRefundPwd.SetFocus
        Screen.MousePointer = vbNormal
        Exit Function
    End If
    'Password entered correctly so report to calling form
    Screen.MousePointer = vbNormal
    VerifyRefundCode = True

End Function


Private Function VerifyAuthCode() As Boolean

Const PRM_CHECK_EXPIRED As Long = 106

Dim strFailedMsg    As String
Dim strTranID       As String
Dim blnCheckExpired As Boolean
Dim strPassword     As String
    
    VerifyAuthCode = False
    blnCheckExpired = goSession.GetParameter(PRM_CHECK_EXPIRED)
    If (Not moUser Is Nothing) And (lblFullName.Caption <> "") Then
        Screen.MousePointer = vbHourglass
        'pad password to 5 characters
        strPassword = txtPassword.Text
        If Len(strPassword) < 5 Then
            While (Len(strPassword) < 5)
                strPassword = "0" & strPassword
            Wend
        End If
        If strPassword = moUser.AuthorisationCode Then
            If (moUser.AuthorisationValidTill < Now()) And (blnCheckExpired = True) Then
                Call MsgBox("Authorisation Code has expired for user '" & moUser.EmployeeId & "-" & moUser.FullName & "'", vbInformation, "Authorisation Expired")
                Screen.MousePointer = vbNormal
                txtAuthID.SetFocus
                Exit Function
            End If 'check that password has not expired
        Else
            strFailedMsg = "Invalid user name or password entered"
            Call MsgBox(strFailedMsg, vbExclamation, Me.Caption)
            Screen.MousePointer = vbNormal
            txtAuthID.SetFocus
            Exit Function
        End If
        'Password entered correctly so report to calling form
        Screen.MousePointer = vbNormal
        VerifyAuthCode = True
    End If 'verify current user's password

End Function

Private Sub Form_Activate()

    If txtRefundPwd.Visible = True Then Call txtRefundPwd.SetFocus
    If txtAuthID.Visible = True Then Call txtAuthID.SetFocus

End Sub

Private Sub Form_Load()

    mblnPasswordOk = False
    fraLogon.BackColor = Me.BackColor

End Sub

Private Sub Form_Resize()

    Call CentreForm(frmRefundPwd)

End Sub

Private Sub Label1_Click()

End Sub

Private Sub txtAuthID_Change()

    lblFullName.Caption = ""
    lblInitials.Caption = ""
    txtPassword.Text = ""
    txtPassword.Enabled = False
    
End Sub

Private Sub txtAuthID_GotFocus()

    txtAuthID.SelStart = 0
    txtAuthID.SelLength = Len(txtAuthID.Text)
    txtAuthID.BackColor = RGBEdit_Colour

End Sub

Private Sub txtAuthID_KeyPress(KeyAscii As Integer)

Const PROCEDURE_NAME As String = MODULE_NAME & ".cmdLogon_Click"

Dim blnLoggedIn As Boolean
    
    If (KeyAscii = vbKeyReturn) Then
        If Len(txtAuthID.Text) <> 3 Then txtAuthID.Text = Left$("000", 3 - Len(txtAuthID.Text)) & txtAuthID.Text
        Set moUser = goDatabase.CreateBusinessObject(CLASSID_USER)
        Call moUser.IBo_AddLoadFilter(CMP_EQUAL, FID_USER_EmployeeID, txtAuthID.Text)
        Call moUser.IBo_AddLoadField(FID_USER_Initials)
        Call moUser.IBo_AddLoadField(FID_USER_FullName)
        Call moUser.IBo_AddLoadField(FID_USER_AuthorisationCode)
        Call moUser.IBo_AddLoadField(FID_USER_AuthorisationValidTill)
        If moUser.EmployeeId = "" Then moUser.EmployeeId = "***" 'force invalid lookup
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "UserID Validated")
        Call moUser.IBo_LoadMatches
        If moUser.FullName <> "" Then
            lblInitials.Caption = moUser.Initials
            lblFullName.Caption = moUser.FullName
            txtPassword.Enabled = True
            txtPassword.Visible = True
            lblPasswordlbl.Visible = True
            Call txtPassword.SetFocus
            If (Val(txtAuthID.Text) >= Val(mstrMaxValidCashier)) Then
                lblInitials.Caption = ""
                lblFullName.Caption = "TRAINING ID"
                Call txtAuthID.SetFocus
                Set moUser = Nothing
            End If
        Else
            txtPassword.Text = ""
            txtPassword.Enabled = False
            lblInitials.Caption = ""
            lblFullName.Caption = "INVALID ID"
            Call txtAuthID.SetFocus
            Set moUser = Nothing
        End If
        Screen.MousePointer = vbNormal
    End If 'enter pressed so check User ID is valid

End Sub

Private Sub txtAuthID_LostFocus()

    txtAuthID.BackColor = RGB_WHITE

End Sub

Private Sub txtPassword_GotFocus()
    
    txtPassword.SelStart = 0
    txtPassword.SelLength = Len(txtPassword.Text)
    txtPassword.BackColor = RGBEdit_Colour

End Sub

Private Sub txtPassword_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then Call cmdContinue_Click

End Sub


Private Sub txtPassword_LostFocus()
    
    txtPassword.BackColor = RGB_WHITE

End Sub

Private Sub txtRefundPwd_Change()

End Sub

Private Sub txtRefundPwd_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then Call cmdContinue_Click

End Sub
