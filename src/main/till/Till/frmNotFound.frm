VERSION 5.00
Begin VB.Form frmNotFound 
   BackColor       =   &H0000FFFF&
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   3300
   ClientLeft      =   1215
   ClientTop       =   450
   ClientWidth     =   6030
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmNotFound.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3300
   ScaleWidth      =   6030
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdContinue 
      Caption         =   "Continue"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   2280
      TabIndex        =   0
      Top             =   2160
      Width           =   1695
   End
   Begin VB.Label lblSKU 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   360
      TabIndex        =   2
      Top             =   960
      Width           =   5295
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "ITEM NOT FOUND"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   360
      TabIndex        =   1
      Top             =   120
      Width           =   5535
   End
End
Attribute VB_Name = "frmNotFound"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>************************************************************************************
'* Module : frmNotFound
'* Date   : 17/02/03
'* Author : Unknown
'*$Archive: /Projects/OasysV2/VB/Till/frmNotFound.frm $
'******************************************************************************************
'* Summary: Form to display when SKU is not recognised
'******************************************************************************************
'* $Author: Mauricem $ $Date: 29/01/04 12:07 $ $Revision: 2 $
'* Versions:
'* 17/02/03    Unknown
'*             Header added.
'</CAMH>***********************************************************************************

Option Explicit

Public Event Duress()
Private mintDuressKeyCode As Integer

Public Sub DisplayNotFound(ByVal strSKU As String)

    Me.Left = (Screen.Width - Me.Width) / 2
    Me.Top = (Screen.Height - Me.Height) / 2
    lblSKU.Caption = strSKU
    Call Me.Show(vbModal)

End Sub

Private Sub cmdContinue_Click()

    If (cmdContinue.Visible = True) And (cmdContinue.Enabled = True) Then cmdContinue.SetFocus
    Call Me.Hide
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If mintDuressKeyCode <> 0 And KeyCode = mintDuressKeyCode Then
        KeyCode = 0
        RaiseEvent Duress
    End If
    
End Sub

