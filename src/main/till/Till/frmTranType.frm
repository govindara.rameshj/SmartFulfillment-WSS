VERSION 5.00
Begin VB.Form frmTranType 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Select Transaction Type"
   ClientHeight    =   6810
   ClientLeft      =   780
   ClientTop       =   4650
   ClientWidth     =   11295
   Icon            =   "frmTranType.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6810
   ScaleWidth      =   11295
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdSignOff 
      Caption         =   "Sign Off"
      Height          =   495
      Left            =   1320
      TabIndex        =   1
      Top             =   6000
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10-Exit"
      Height          =   495
      Left            =   240
      TabIndex        =   0
      Top             =   6000
      Width           =   975
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      Height          =   495
      Left            =   2640
      TabIndex        =   2
      Top             =   6000
      Visible         =   0   'False
      Width           =   975
   End
End
Attribute VB_Name = "frmTranType"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
