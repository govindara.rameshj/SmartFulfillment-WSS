Attribute VB_Name = "modEvents"
Option Explicit

Public m_oSession As Session

Const QS_DESC As String = "Bulk Saving"
Const MS_DESC As String = "Multibuy Saving"
Const DG_DESC As String = "Project Saving"
Const HS_DESC As String = "Spend Level Saving"

Public mblnUseDesc As Boolean 'debug flag whether to use fixed desc or from EVTHDR
Public mlngDayNo   As Long

Public Function CheckHeaderValid(ByVal strEventNo As String, _
                                 ByVal strPriority As String, _
                                 ByVal blnCheckDays As Boolean, _
                                 ByRef strStartAt As String, _
                                 ByRef strEndAt As String, _
                                 ByRef strDesc As String) As Boolean

Static lstrEventNo      As String
Static lstrPriority     As String
Static lblnCheckDays    As Boolean
Static lstrStartAt      As String
Static lstrEndAt        As String
Static lstrDesc         As String
Static lblnLastResponse As Boolean

Dim oEventHeader    As cOPEvents_Wickes.cEventHeader

Dim strQuery       As String
Dim rsData         As Recordset
Static oConnection As Connection

    If oConnection Is Nothing Then
        Set oConnection = New Connection
        oConnection.CursorLocation = adUseClient
        oConnection.ConnectionString = m_oSession.Database.ConnectionString
        oConnection.Open
    End If

    If strEventNo = lstrEventNo And strPriority = lstrPriority And blnCheckDays = lblnCheckDays Then
        strStartAt = lstrStartAt
        strEndAt = lstrEndAt
        strDesc = lstrDesc
        CheckHeaderValid = lblnLastResponse
        Exit Function
    End If
    
    lstrEventNo = strEventNo
    lstrPriority = strPriority
    lblnCheckDays = blnCheckDays
    
    strStartAt = ""
    strEndAt = ""
    strDesc = ""
    
'    Set oEventHeader = m_oSession.Database.CreateBusinessObject(CLASSID_OPHEADER)
'    Call oEventHeader.AddLoadFilter(CMP_EQUAL, FID_OPHEADER_EventNumber, strEventNo)
'    Call oEventHeader.AddLoadFilter(CMP_EQUAL, FID_OPHEADER_Priority, strPriority)
'    Call oEventHeader.AddLoadFilter(CMP_EQUAL, FID_OPHEADER_Deleted, False)
    
'    Call oEventHeader.AddLoadField(FID_OPHEADER_StartDate)
'    Call oEventHeader.AddLoadField(FID_OPHEADER_StartTime)
'    Call oEventHeader.AddLoadField(FID_OPHEADER_EndDate)
'    Call oEventHeader.AddLoadField(FID_OPHEADER_ActiveDays)
'    Call oEventHeader.AddLoadField(FID_OPHEADER_Description)
'    Call oEventHeader.IBo_LoadMatches
    strQuery = "SELECT SDAT ""StartDate"", STIM ""StartTime"", EDAT ""EndDate"", DESCR ""Description"", DACT1 ""ActiveDay1"", DACT2 ""ActiveDay2"", DACT3 ""ActiveDay3"", DACT4 ""ActiveDay4"", DACT5 ""ActiveDay5"", DACT6 ""ActiveDay6"", DACT7 ""ActiveDay7"" from EVTHDR where NUMB='" & strEventNo & "' and PRIO='" & strPriority & "' and IDEL=0"
    Set rsData = oConnection.Execute(strQuery)
    If (rsData.RecordCount <> 0) Then
        strStartAt = Format(rsData!StartDate, "YYYYMMDD") & rsData!StartTime
        strEndAt = Format(rsData!EndDate, "YYYYMMDD")
        strEndAt = strEndAt & "2359"
'        strEndAt = strEndAt & rsdata!EndDate
        strDesc = rsData!Description
        CheckHeaderValid = True
        'If based on Time/Day then check if valid for current day
        If (blnCheckDays = True) And (rsData.Fields("ActiveDay" & CStr(mlngDayNo)) = False) Then
            CheckHeaderValid = False
        End If
    Else
        CheckHeaderValid = False
    End If
'    Set oEventHeader = Nothing

    lstrStartAt = strStartAt
    lstrEndAt = strEndAt
    lstrDesc = strDesc
    lblnLastResponse = CheckHeaderValid
    
End Function



