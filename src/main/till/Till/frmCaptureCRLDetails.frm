VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Object = "{D165F572-038D-4ED4-A4A8-EB3E27C39F74}#1.0#0"; "CaptureCustUC.ocx"
Begin VB.Form frmCaptureCRLDetails 
   Caption         =   "Capture Customer Details"
   ClientHeight    =   7485
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11145
   Icon            =   "frmCaptureCRLDetails.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7485
   ScaleWidth      =   11145
   StartUpPosition =   3  'Windows Default
   Begin ucCaptureCustomer_Wickes.ucCaptureCust ucccCustomer 
      Height          =   4395
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   9510
      _ExtentX        =   16775
      _ExtentY        =   8811
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   1
      Top             =   7110
      Width           =   11145
      _ExtentX        =   19659
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmCaptureCRLDetails.frx":0A96
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11483
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   970
            MinWidth        =   970
            Picture         =   "frmCaptureCRLDetails.frx":143C
            Key             =   "KeyBoard"
            Object.ToolTipText     =   "Show/Hide Keyboard"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "15:39"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   0
      Top             =   3000
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
End
Attribute VB_Name = "frmCaptureCRLDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Cancel           As Boolean
Public ShowKeyPad       As Boolean

Private mstrSalutation  As String
Private mstrForename    As String
Private mstrSurname     As String
Private mstrCompany     As String
Private mstrPostCode    As String
Private mstrAddress1    As String
Private mstrAddress2    As String
Private mstrTown        As String
Private mstrCounty      As String
Private mstrCountry     As String

Private Sub sbStatus_PanelClick(ByVal Panel As MSComctlLib.Panel)
    
    Select Case (Panel.Key)
        Case ("KeyBoard"):
                If ((Panel.Picture Is Nothing) = False) Then
                    ucKeyPad1.Visible = Not ucKeyPad1.Visible
                    If (ucKeyPad1.Visible) Then
                        ucccCustomer.Top = -120
                    Else
                        ucccCustomer.Top = 0
                    End If
                    Call ucccCustomer_Resize
                End If
    End Select

End Sub

Private Sub Form_Activate()
    
    If (ucKeyPad1.Visible = True) And (ucccCustomer.Top = 0) Then ucccCustomer.Top = -120

End Sub

Private Sub Form_Load()

    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    ucKeyPad1.Left = 0
    ucccCustomer.Top = 0
    If (ShowKeyPad = True) Then
        ucKeyPad1.Visible = True
    Else
        ucKeyPad1.Visible = False
    End If
    Call InitialiseStatusBar(sbStatus)

End Sub

Private Sub ucccCustomer_Resize()
    Me.Height = ucccCustomer.Height + 360 + sbStatus.Height
    If (ucKeyPad1.Visible = True) Or ((Me.Visible = False) And (ShowKeyPad = True)) Then
        Me.Height = Me.Height + ucKeyPad1.Height
        Me.Width = ucKeyPad1.Width + 120
    Else
        Me.Width = ucccCustomer.Width + 120
    End If
    ucccCustomer.Left = (Me.Width - ucccCustomer.Width) / 2
    ucKeyPad1.Top = ucccCustomer.Height - 120
    Call CentreForm(Me)
End Sub


Private Sub ucccCustomer_Cancel()

    Cancel = True
    Me.Hide
    
End Sub

Public Sub GetCRLAddress()

End Sub

Private Sub ucccCustomer_Save(Name As String, PostCode As String, Address As String, TelNo As String, MobileNo As String, WorkTelNo As String, Email As String)
'To Do
End Sub
