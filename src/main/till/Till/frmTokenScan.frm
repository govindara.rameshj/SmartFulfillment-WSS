VERSION 5.00
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Begin VB.Form frmTokenScan 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Gift Token"
   ClientHeight    =   5895
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7065
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5895
   ScaleWidth      =   7065
   StartUpPosition =   2  'CenterScreen
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   7080
      Top             =   60
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   1800
      TabIndex        =   7
      Top             =   5280
      Width           =   1515
   End
   Begin VB.CommandButton cmdEnter 
      Caption         =   "Enter"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   3780
      TabIndex        =   1
      Top             =   5280
      Width           =   1515
   End
   Begin VB.PictureBox fraTendering 
      BorderStyle     =   0  'None
      Height          =   4995
      Left            =   60
      ScaleHeight     =   4995
      ScaleWidth      =   6915
      TabIndex        =   3
      Top             =   120
      Width           =   6915
      Begin VB.TextBox txtBarCode 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   2130
         MaxLength       =   13
         TabIndex        =   6
         Top             =   4440
         Width           =   2655
      End
      Begin VB.Image imgBarcode 
         Height          =   3765
         Left            =   0
         Picture         =   "frmTokenScan.frx":0000
         Top             =   540
         Width           =   6870
      End
      Begin VB.Label lblPrompt 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Scan/Enter Gift Token Barcode"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Index           =   1
         Left            =   968
         TabIndex        =   4
         Top             =   0
         Width           =   4980
      End
   End
   Begin VB.PictureBox fraSelling 
      BorderStyle     =   0  'None
      Height          =   5115
      Left            =   0
      ScaleHeight     =   5115
      ScaleWidth      =   7035
      TabIndex        =   2
      Top             =   60
      Width           =   7035
      Begin VB.TextBox txtTokenSerialInput 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   2670
         MaxLength       =   8
         TabIndex        =   0
         Top             =   4500
         Width           =   1695
      End
      Begin VB.Image imgGiftToken 
         Height          =   3765
         Left            =   60
         Picture         =   "frmTokenScan.frx":757C
         Top             =   600
         Width           =   6870
      End
      Begin VB.Label lblPrompt 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Scan/Enter Gift Token Serial No."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Index           =   0
         Left            =   908
         TabIndex        =   5
         Top             =   60
         Width           =   5220
      End
   End
End
Attribute VB_Name = "frmTokenScan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Cancel   As Boolean

Public Event Duress()
Private mintDuressKeyCode As Integer

Private mScanData As String
Private mblnShowNumPad As Boolean

Public Property Let ScanData(Value As String)
    
    If fraSelling.Visible Then
        txtTokenSerialInput.Text = Value
        cmdEnter.Value = True
    Else
        txtBarCode.Text = Value
        cmdEnter.Value = True
    End If
    
End Property

Public Property Get ScanData() As String
    
    ScanData = mScanData
    
End Property

Private Sub cmdCancel_Click()

    Call Form_KeyPress(vbKeyEscape)

End Sub

Private Sub cmdEnter_Click()
    
    If (cmdEnter.Visible = True) And (cmdEnter.Enabled = True) Then cmdEnter.SetFocus
    DoEvents
    If fraSelling.Visible Then
        If txtTokenSerialInput.Text <> "00000000" Then
            If SerialOK(txtTokenSerialInput.Text) Then
                Cancel = False
                Me.Hide
                mScanData = txtTokenSerialInput.Text
                Exit Sub
            End If
        End If
        
        Call MsgBoxEx("Invalid serial number entered", vbOKOnly, "Gift Token", , , , , RGBMsgBox_WarnColour)
        If (txtTokenSerialInput.Visible = True) And (txtTokenSerialInput.Enabled = True) Then txtTokenSerialInput.SetFocus
    Else
        If txtBarCode.Text <> "0000000000000" Then
            If SerialOK(txtBarCode.Text) Then
                Cancel = False
                Me.Hide
                mScanData = txtBarCode.Text
                Exit Sub
            End If
        End If
        
        Call MsgBoxEx("Invalid barcode number entered", vbOKOnly, "Gift Token", , , , , RGBMsgBox_WarnColour)
        If (txtBarCode.Enabled = True) And (txtBarCode.Visible = True) Then txtBarCode.SetFocus
    End If
End Sub

Private Sub Form_Activate()

    Call ShowNumPad
    If fraSelling.Visible Then
        If (txtTokenSerialInput.Enabled = True) And (txtTokenSerialInput.Visible = True) Then txtTokenSerialInput.SetFocus
    Else
        If (txtBarCode.Enabled = True) And (txtBarCode.Visible = True) Then txtBarCode.SetFocus
    End If

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If mintDuressKeyCode <> 0 And KeyCode = mintDuressKeyCode Then
        KeyCode = 0
        RaiseEvent Duress
    End If

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyEscape Then
        Cancel = True
        Me.Hide
    End If
    
End Sub


Private Sub Form_Load()
    
    Me.BackColor = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
    fraSelling.BackColor = Me.BackColor
    fraTendering.BackColor = Me.BackColor
    mintDuressKeyCode = goSession.GetParameter(PRM_KEY_DURESS)

End Sub

Private Sub ShowNumPad()

    If (mblnShowNumPad = True) Then
        Call ucKeyPad1.ShowNumPad
        ucKeyPad1.Visible = True
        Me.Width = fraSelling.Width + 240 + ucKeyPad1.Width
        Call CentreForm(Me)
    End If
    
End Sub

Private Sub txtBarCode_GotFocus()

    With txtBarCode
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
    
End Sub

Private Sub txtBarCode_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii > 32) And (Not Chr$(KeyAscii) Like "[0-9]") Then
        KeyAscii = 0
    End If

End Sub

Private Sub txtBarCode_LostFocus()
    txtBarCode.Text = Right$("0000000000000" & txtBarCode.Text, 13)
End Sub

Private Sub txtTokenSerialInput_GotFocus()

    With txtTokenSerialInput
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
    
End Sub

Private Sub txtTokenSerialInput_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii > 32) And (Not Chr$(KeyAscii) Like "[0-9]") Then
        KeyAscii = 0
    End If

End Sub

Private Sub txtTokenSerialInput_LostFocus()
    txtTokenSerialInput.Text = Right$("00000000" & txtTokenSerialInput.Text, 8)
End Sub

Public Sub GetTokenToSell(blnShowNumPad As Boolean)
    
    fraSelling.Visible = True
    fraTendering.Visible = False
    txtTokenSerialInput.Text = vbNullString
    mblnShowNumPad = blnShowNumPad
    Me.Show vbModal

End Sub

Public Sub GetTokenForTender(blnShowNumPad As Boolean)

    fraSelling.Visible = False
    fraTendering.Visible = True
    txtBarCode.Text = vbNullString
    mblnShowNumPad = blnShowNumPad
    Me.Show vbModal

End Sub

Private Sub ucKeyPad1_Resize()

    ucKeyPad1.Width = ucKeyPad1.Width
    ucKeyPad1.Height = ucKeyPad1.Height

End Sub
