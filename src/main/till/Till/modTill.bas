Attribute VB_Name = "modTill"
'<CAMH>************************************************************************************
'* Module : modTill
'* Date   : 17/02/03
'* Author : Unknown
'*$Archive: /Projects/OasysV2/VB/Till/modTill.bas $
'******************************************************************************************
'* Summary: Definition of Constants used by the till
'******************************************************************************************
'* $Author: Mauricem $ $Date: 1/06/04 9:56 $ $Revision: 15 $
'* Versions:
'* 17/02/03    Unknown
'*             Header added.
'</CAMH>***********************************************************************************
Option Explicit

Const MODULE_NAME As String = "modTill"

Public moUser    As cUser
Public moCashier As cCashier
Public moRetopt  As cRetailOptions

' Tender Types
Public Const TEN_CASH     As Long = 1
Public Const TEN_CHEQUE    As Long = 2
Public Const TEN_CREDCARD  As Long = 3
Public Const TEN_OLDTOKEN  As Long = 4
Public Const TEN_PROJLOAN  As Long = 5
Public Const TEN_COUPON    As Long = 6
Public Const TEN_GIFTTOKEN As Long = 7
Public Const TEN_DEBITCARD As Long = 8
Public Const TEN_ACCOUNT   As Long = 9
Public Const TEN_GIFTCARD  As Long = 13
Public Const TEN_CHANGE    As Long = 99

' Types of Sale
Public Const TT_SALE       As String = "SA"
Public Const TT_REFUND     As String = "RF"
Public Const TT_SIGNON     As String = "CO"
Public Const TT_COLLECT    As String = "IC"
Public Const TT_SIGNOFF    As String = "CC"
Public Const TT_OPENDRAWER As String = "OD"
Public Const TT_LOGO       As String = "RL"
Public Const TT_XREAD      As String = "XR"
Public Const TT_ZREAD      As String = "ZR"
Public Const TT_MISCIN     As String = "M+"
Public Const TT_MISCOUT    As String = "M-"
Public Const TT_RECALL     As String = "RP"
Public Const TT_QUOTE      As String = "QD"
Public Const TT_EXT_SALE   As String = "ES"
Public Const TT_TELESALES  As String = "TS"
Public Const TT_TELEREFUND As String = "TR"
Public Const TT_REPRINTREC As String = "RR"
Public Const TT_LOOKUP     As String = "LU"
Public Const TT_CORRIN     As String = "C+"
Public Const TT_CORROUT    As String = "C-"
Public Const TT_EAN_CHECK  As String = "EC"

Public Const PRM_TILL_OFFLINE As Long = 132 'Colour to show as background when Off-Line

Public Const PRM_MAX_LINE_QTY      As Long = 200
Public Const PRM_MAX_LINE_VALUE      As Long = 205

Public Const PRM_MAX_LOAN_ITEMS      As Long = 811
Public Const PRM_PROMPT_ACC_SALE     As Long = 818
Public Const PRM_REFUND_AUTH_CONFIG  As Long = 820
Public Const PRM_REFUND_ADD_LINES    As Long = 821
Public Const PRM_REFUND_MULT_TRANS   As Long = 822
Public Const PRM_REFUND_VERIFY_SKU   As Long = 823  'Added 15/3/04 - on refund check if SKU checked
Public Const PRM_PRINT_LOGO_AT_START As Long = 825
Public Const PRM_USER_RELOG_IN       As Long = 826
Public Const PRM_USE_EFTPOS          As Long = 831
Public Const PRM_USE_CHQEFTPOS       As Long = 834
Public Const PRM_PRINT_VATRECEIPT    As Long = 837
Public Const PRM_DEFAULT_MSG_DEPTNO  As Long = 838

Public Const PRM_USE_COLL_WHITE_LIST As Long = 842
Public Const PRM_USE_MULTI_CURR      As Long = 845 'added 15/11/06 - check if Multi-Currency available
Public Const PRM_USE_WEEE_RATES      As Long = 846 'added 21/11/06 - check if WEEE Rates

Public Const PRM_KEY_SEARCH          As Long = 850
Public Const PRM_KEY_PARK            As Long = 851
Public Const PRM_KEY_VOID            As Long = 852
Public Const PRM_KEY_ORDER           As Long = 853
Public Const PRM_KEY_QUANTITY        As Long = 854
Public Const PRM_KEY_REVERSAL        As Long = 855
Public Const PRM_KEY_TENDER          As Long = 856
Public Const PRM_KEY_ENQUIRY         As Long = 857
Public Const PRM_KEY_OVERRIDE        As Long = 858
Public Const PRM_KEY_LOOKUP          As Long = 859
Public Const PRM_KEY_CLOSE           As Long = 860
Public Const PRM_KEY_RESET           As Long = 861
Public Const PRM_KEY_SAVE            As Long = 862
Public Const PRM_KEY_DEPTSALE        As Long = 863
Public Const PRM_KEY_TRANDISC        As Long = 864
Public Const PRM_KEY_SPECIAL         As Long = 866
Public Const PRM_KEY_DETAILS         As Long = 867
Public Const PRM_KEY_PIM_LOOKUP      As Long = 878

Public Const PRM_KEY_DURESS          As Long = 868
Public Const PRM_ODC_DURESS          As Long = 869

Public Const PRM_KEY_GIFTVOUCHER     As Long = 873
Public Const PRM_KEY_BACKDOORITEMS   As Long = 874

Public Const PRM_QUOTES_HOTKEY        As Long = 885
Public Const PRM_QUOTES_VALID_FOR     As Long = 886
Public Const PRM_RECALL_QUOTES_HOTKEY As Long = 887
Public Const PRM_CREATE_ORDER_HOTKEY  As Long = 888

Public Const PRM_TRANSNO      As Long = 911

Public Const PRM_MCD_ACTIVE   As Long = 920
Public Const PRM_MCD_DATAPATH As Long = 921

Public Const PRM_MRC_COMMS_FAILED As Long = 922
Public Const PRM_RC_COMMS_FAILED  As Long = 923
Public Const PRM_SHOW_EFT_DATA    As Long = 924
Public Const PRM_TRACE_EFT        As Long = 925
Public Const PRM_CARDNO_MASK      As Long = 926

Public Const PRM_POSTCODE_STARTDATE As Long = 927
Public Const PRM_POSTCODE_ENDDATE   As Long = 928

Public Const PRM_COLLCARDFILE As Long = 930
Public Const PRM_QODFILE      As Long = 931
Public Const PRM_QODACTIVE    As Long = 935
Public Const PRM_BDCACTIVE    As Long = 936
Public Const PRM_QODSTOCK_CHK As Long = 937 'WIX1243 - QOD level to flag stock as concerned
Public Const PRM_QODFOC_LEVEL As Long = 938 'Spend Level on QOD to get FOC Delivery


Public Const PRM_PRICEMATCHREFCODE  As Long = 940
Public Const PRM_PRICEMATCHDAYS     As Long = 941

Public Const PRM_COUPON_WS_URL      As Long = 946 'WIX1165 - WS Address - Blank is disabled

Public Const PRM_PRINTERNAME    As Long = 950
Public Const PRM_CASHDRAWERNAME As Long = 951
Public Const PRM_BARSCANNERNAME As Long = 952
Public Const PRM_BARCODE_CHECK_FREQ As Long = 953
Public Const PRM_PRINTERMODE    As Long = 954
Public Const PRM_PRINTERWAITTIMEOUT    As Long = 958
Public Const PRM_MARKDOWN_DISCOUNT As Long = 4101
Public Const PRM_EFT_METHOD     As Long = 974

Public Const PRM_EFT_SWIPE_TIMEOUT As Long = 978 'added WIX1373 to split timeout when reading discount card

Public Const PRM_SITE_ADDRESS As Long = 7000
Public Const PRM_MAX_RETRIES_NUMBER_FOR_EXTERNAL_REQUEST_MONITOR As Long = 7005
Public Const PRM_RETRY_PERIOD_MINUTES_FOR_EXTERNAL_REQUEST_MONITOR As Long = 7006

Public Const PRM_VAT_UPDATE As Long = 110000 'Added 26/11/08 - to handle VAT Updates
Public Const PRM_TAKE_NOW_MSG     As Long = 965008 'WIX1381
Public Const PRM_TAKE_NOW_MSG1    As Long = 965009 'WIX1381

Public Const PRM_PARKED_REFUND_MSG1 As Long = 969002
Public Const PRM_PARKED_REFUND_MSG2 As Long = 969003
' Hubs 2.0 - Referral 771; Message re not allowed to park a transaction with a web order refund in it.
Public Const PRM_PARKED_REFUND_MSG3 As Long = 969004
' End fof Hubs 2.0
' Change Request CR0034
Public Const PRM_USING_SQL As Long = 140
Public Const PRM_REQUIREMENTACTIVATED_CR0034 As Long = 971034
' End of Change Request CR0034

'parameters for houskeeping of receipt log files
Public Const PRM_RECEIPT_LOGGING_PATH As Long = 949
Public Const PRM_RECEIPT_HOUSEKEEPING_INTERVAL As Long = 947

Public mstrOrderQuit              As String 'WIX1381

Public Const PRINTMODE_TO_PRINTER = 1

Public Const WORKSTATIONCONFIG_PRNTTYPE_for_nontill As String = "A"

Public Const QTY_BEFORE_SKU       As Byte = 1
Public Const QTY_AFTER_SKU        As Byte = 2
Public Const QTY_ALWAYS_AFTER_SKU As Byte = 3
Public Const QTY_UOM_ENTRY        As Byte = 4

Public Const COL_ALL            As Long = -1
Public Const COL_ITEMNO         As Long = 1
Public Const COL_PARTCODE       As Long = 2
Public Const COL_DESC           As Long = 3
Public Const COL_SIZE           As Long = 4
Public Const COL_QTY            As Long = 5
Public Const COL_UNITS          As Long = 6
Public Const COL_SALETYPE       As Long = 7
Public Const COL_COST           As Long = 8
Public Const COL_SELLPRICE      As Long = 9
Public Const COL_CUSTORDER      As Long = 10
Public Const COL_DISC           As Long = 11
Public Const COL_EXCTOTAL       As Long = 12
Public Const COL_VATTOTAL       As Long = 13
Public Const COL_INCTOTAL       As Long = 14
Public Const COL_BACKCOLLECT    As Long = 15 'added WIX1161 - store if Item is collected at backdoor
Public Const COL_ONHAND         As Long = 16
Public Const COL_CUSTONO        As Long = 17
Public Const COL_VATRATE        As Long = 18
Public Const COL_QTYSELLIN      As Long = 19
Public Const COL_ITEMTAG        As Long = 20
Public Const COL_BARCODE        As Long = 21
Public Const COL_VATCODE        As Long = 22
Public Const COL_RELITEM        As Long = 23
Public Const COL_CATCHALL       As Long = 24
Public Const COL_LINETYPE       As Long = 25
Public Const COL_VOIDED         As Long = 26
Public Const COL_TENDERTYPE     As Long = 27
Public Const COL_CPN_STATUS     As Long = 28 'WIX1165 - Coupon Status - reused COL_BAND
Public Const COL_TOTCOST        As Long = 29
Public Const COL_LOOKEDUP       As Long = 30
Public Const COL_DEPTCODE       As Long = 31
Public Const COL_DEPTGRPCODE    As Long = 32
Public Const COL_ORDERFACTOR    As Long = 33
Public Const COL_ORDERVALUE     As Long = 34
Public Const COL_SUPPNO         As Long = 35
Public Const COL_DISCCODE       As Long = 36
Public Const COL_RFND_STORENO   As Long = 37
Public Const COL_RFND_TRANID    As Long = 38
Public Const COL_RFND_TILLNO    As Long = 39
Public Const COL_RFND_LINENO    As Long = 40
Public Const COL_RFND_DATE      As Long = 41
Public Const COL_APPLY_TO_STOCK As Long = 42
Public Const COL_DB_LINENO      As Long = 43
Public Const COL_OP_EVENTSEQNO  As Long = 44
Public Const COL_OP_SUBEVENTNO  As Long = 45
Public Const COL_PP_POS         As Long = 46 'flag to mark where Price Promise/Match is stored in collection
Public Const COL_OP_TYPE        As Long = 47
Public Const COL_CORD_SUPP      As Long = 48 'flag for Customer Order Items, if Purchase Order must be raised
' Event Columns
Public Const COL_HCATEGORY      As Long = 49
Public Const COL_HGROUP         As Long = 50
Public Const COL_HSUBGROUP      As Long = 51
Public Const COL_HSTYLE         As Long = 52
Public Const COL_QTYBRKAMOUNT   As Long = 53
Public Const COL_QTYBRKCODE     As Long = 54
Public Const COL_DGRPAMOUNT     As Long = 55
Public Const COL_DGRPCODE       As Long = 56
Public Const COL_MBUYAMOUNT     As Long = 57
Public Const COL_MBUYCODE       As Long = 58
Public Const COL_HIERAMOUNT     As Long = 59
Public Const COL_HIERCODE       As Long = 60
' Supervisor Columns
Public Const COL_QURSUP         As Long = 61

Public Const COL_TEMPAMOUNT     As Long = 62
Public Const COL_TEMPCODE       As Long = 63

Public Const COL_VOUCHER        As Long = 64
Public Const COL_MARKDOWN       As Long = 65
Public Const COL_REFREASON      As Long = 66
Public Const COL_VOUCHERSERIAL  As Long = 67
Public Const COL_SUPERVISOR     As Long = 68
Public Const COL_ORIGTRANVER    As Long = 69
Public Const COL_DLGIFTTYPE     As Long = 70
Public Const COL_DISCMARGIN     As Long = 71
Public Const COL_ORDERINFO      As Long = 72
Public Const COL_ALLOW_BDC      As Long = 73
Public Const COL_WEEE_RATE      As Long = 74
Public Const COL_SPARE          As Long = 75 'WIX1337 - replaced Rate by specific SKU
Public Const COL_WEEE_SKU       As Long = 76 'WIX1337 - replaced Rate by specific SKU
Public Const COL_PPLINE         As Long = 77 'Flag if line is a Price Promise Refund
Public Const COL_KEYED_RC       As Long = 78 'WIX1202 - reason why keyed SKU
Public Const COL_KEYED_SKU      As Long = 79 'WIX1202 - SKU entered by scanner/operator
Public Const COL_REJECT_ID      As Long = 80 'Saved DLREJECT ID, to be updated if line reversed
Public Const COL_LINE_REV_CODE  As Long = 81 'WIX1202 - Line Reversal Code
Public Const COL_VOLUME         As Long = 82 'WIX1309 - Quotes - Added in to calc Quote Volume and Weight
Public Const COL_WEIGHT         As Long = 83 'WIX1309 - Quotes - Added in to calc Quote Volume and Weight
Public Const COL_ORIGTOTAL      As Long = 84
Public Const COL_QTYONORDER     As Long = 85 'WIX1243 - On Order - show On Order Qty
Public Const COL_CFOREIGN_AMT   As Long = 86 'WIX1312 - hold foreign currency amount
Public Const COL_CFOREIGN_CODE  As Long = 87 'WIX1312 - hold foreign currency code
Public Const COL_CFOREIGN_EXCH  As Long = 88 'WIX1312 - hold foreign currency exchange rate
Public Const COL_CFOREIGN_POWER As Long = 89 'WIX1312 - hold foreign currency Exch Power
Public Const COL_TAKE_QTY       As Long = 90 'Used to record Quantity to take, so omit Vouchers and Delivery Charges
Public Const COL_MDSERIAL_NUMB  As Long = 91 'WIX1302 - Hold MarkDown Serial Number
Public Const COL_MAX_HIE_DISC   As Long = 92 'WIX1302 - Hold Maximum Hierachy Discount
Public Const COL_CPN_ID         As Long = 93 'WIX1165 - Coupon ID
Public Const COL_CPN_EXCLUSIVE  As Long = 94 'WIX1165 - Flag if coupon is Exclusive so no more can be added
Public Const COL_CPN_REUSE      As Long = 95 'WIX1165 - Flag if coupon should be returned to user
Public Const COL_CPN_SERIALNO   As Long = 96 'WIX1165 - Serial Number of Coupon
Public Const COL_CPN_MARKETREF  As Long = 97 'WIX1165 - Market Ref/Originating Store No
Public Const COL_PRETRAN_PRICE  As Long = 98 'WIX1165 - Store Pre Transaction Discount Price
Public Const COL_PRETRAN_CODE   As Long = 99 'WIX1165 - Store Pre Transaction Discount Code if Any
Public Const COL_DAY_PRICE      As Long = 100 'WIX1165 - Store day price in case of Price Override
Public Const COL_TAKE_NOW       As Long = 101 'WIX1381 - Take Now
Public Const COL_TOTALVOL       As Long = 102 'WIX1389 - holds Total Volume for line
Public Const COL_GRP_PROMPT_QTY As Long = 103 'WIX1391 - holds the total quatity for GroupType 4 of a Group Prompt
Public Const COL_RFND_WEB_ORDER_NO As Long = 104 ' HUBS 2.0 - holds the originating Web Order number

Public Const LT_ITEM      As Long = 1
Public Const LT_SPACER    As Long = 2
Public Const LT_PMT       As Long = 3
Public Const LT_TOTAL     As Long = 4
Public Const LT_EVENT     As Long = 5
Public Const LT_PRICEPROM As Long = 6
Public Const LT_TEMPPRICE As Long = 7
Public Const LT_COUPON    As Long = 8

' CR0034
' Additional panel identifiers for new information to go on status bar PED ID and PED offline image
Public Const PANEL_PED As Integer = 10
Public Const PANEL_PEDOFFLINE As Integer = 11
' End of CR0034

Public Enum enRefundAuthCode
    enracNone = 0
    enracRefundPwd = 1
    enracCurrentUserNoPwd = 2
    enracCurrentUserPwd = 3
    enracOtherUser = 4
End Enum

Public Enum enEntryMode
    enemNone = -1
    enemSKU = 0
    enemQty = 1
    enemPrice = 2
    enemPerc = 3
    enemDesc = 4
    enemValue = 5
    enemCostPrice = 6
End Enum

Public Enum enCurrentMode
    encmLogon = 0
    encmTranType = 1
    encmRecord = 2
    encmTender = 3
    encmReversal = 4
    encmComplete = 5
    encmAuthorise = 6
    encmOverrideCode = 7
    encmCustomer = 8
    encmRetrieveTran = 9
    encmPaidOut = 11
    encmPaidIn = 12
    encmTransactionDiscount = 13
    encmOrderAction = 14
    encmOpenDrawer = 16
    encmRefund = 17
    encmGiftVoucher = 18
    encmColleagueDiscount = 19
    encmTenderOverride = 20
    encmRefundPriceOverride = 21
    encmBarcodeAudit = 22
    encmEANCheck = 23
    encmLineReversalCode = 24
    encmDiscountCardScheme = 25 'MO'C added 07/06/07
End Enum


Public moTranHeader As cPOSHeader 'Holds the Transaction as it is being captured
Public moTOSType    As cTypesOfSaleOptions 'Holds the current Transaction Type selected
Public moTenderOpts As cTenderOptions 'Holds the current Tender Type being captured
Public moReturnCust As cReturnCust ' Holds DLRCUS information
Public moConResHeaderBO As cConResHeader 'Holds the Concern Resolution Header for M- transactions

Public mblnPrePrintLogo As Boolean 'flag from parameter 825 to check if Logo is printed up front
Public mblnReLogIn      As Boolean 'flag if user must log in before each transaction
Public mstrMaxValidCashier As String

Public Declare Function timeSetEvent Lib "winmm.dll" (ByVal uDelay As Long, ByVal uResolution As Long, ByVal lpFunction As Long, ByVal dwUser As Long, ByVal uFlags As Long) As Long
Public Declare Function timeGetDevCaps Lib "winmm.dll" (lpTimeCaps As TIMECAPS, ByVal uSize As Long) As Long
Public Declare Function timeKillEvent Lib "winmm.dll" (ByVal uID As Long) As Long
Public Declare Function timeBeginPeriod Lib "winmm.dll" (ByVal uPeriod As Long) As Long
Public Declare Function timeEndPeriod Lib "winmm.dll" (ByVal uPeriod As Long) As Long

Public mstrWorkstationType  As String
Public mblnAllowQuotes      As Boolean
Public mblnHasCashDrawer    As Boolean
Public mblnHasEFT           As Boolean
Public mstrPrinterType      As String

Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Const KEYEVENTF_KEYUP = &H2
Const INPUT_MOUSE = 0
Const INPUT_KEYBOARD = 1
Const INPUT_HARDWARE = 2

Private Type MOUSEINPUT
  dx As Long
  dy As Long
  mouseData As Long
  dwFlags As Long
  time As Long
  dwExtraInfo As Long
End Type
Private Type KEYBDINPUT
  wVk As Integer
  wScan As Integer
  dwFlags As Long
  time As Long
  dwExtraInfo As Long
End Type
Private Type HARDWAREINPUT
  uMsg As Long
  wParamL As Integer
  wParamH As Integer
End Type
Private Type GENERALINPUT
  dwType As Long
  xi(0 To 23) As Byte
End Type


Public Declare Function SendInput Lib "user32.dll" (ByVal nInputs As Long, pInputs As GENERALINPUT, ByVal cbSize As Long) As Long

Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (pDst As Any, pSrc As Any, ByVal ByteLen As Long)
    
Public Type TIMECAPS
        wPeriodMin As Long
        wPeriodMax As Long
End Type

Public Const TIME_ONESHOT = 0
Public Const TIME_PERIODIC = 1

Public Const TIMERR_BASE = 96
Public Const TIMERR_NOERROR = (0)
Public Const TIMERR_STRUCT = (TIMERR_BASE + 33)

Public mlngTimerResolution As Long

Public mstrGiftVoucherSKU          As String
Public mstrGiftCardSKU             As String

Public mstrHouseName               As String
Public mstrAddressLine1            As String
Public mstrAddressLine2            As String
Public mstrAddressLine3            As String
Public mstrAddressLine4            As String
Public mstrHomeTelNumber           As String
Public mstrPostCode                As String

Public mcolBarcodeFailedCodes  As Collection 'WIX1202
Public mcolLineReversalCodes   As Collection 'WIX1202

Public mstrMarkDownStyle       As String     'MO'C WIX 1302

Public mblnShowKeypadIcon   As Boolean 'added 8/8/07 to flag if KeyPad icon should be hidden

Public mlngBackColor As Long

Public mblnRetreiveAll As Boolean

Public mblnIsHotSpotImageRequired As Boolean

Function SerialOK(strSerial As String)
    
    If GetCheckDigit(strSerial) = 0 Then
        SerialOK = True
    Else
        SerialOK = False
    End If

End Function

Function GetCheckDigit(strSerial As String) As Long

Dim intCharCount As Integer
Dim intAccumulator As Integer
Dim strWeight As String

    intAccumulator = 0
    If Len(strSerial) < 9 Then
        strWeight = "31313131"
    ElseIf Len(strSerial) = 9 Then
        strWeight = "379:5842"
    Else
        strWeight = "131313131313131313"
    End If
    
    If Len(strSerial) = 9 Then
        For intCharCount = 1 To Len(strSerial) - 1
            intAccumulator = intAccumulator + (CInt(Mid(strSerial, intCharCount, 1)) * CInt(Asc(Mid(strWeight, intCharCount, 1)) - 48))
        Next
    
        GetCheckDigit = 11 - (intAccumulator Mod 11) - CInt(Right$(strSerial, 1))
    Else
        For intCharCount = 1 To Len(strSerial)
            intAccumulator = intAccumulator + (CInt(Mid(strSerial, intCharCount, 1)) * CInt(Mid(strWeight, intCharCount, 1)))
        Next
        
        GetCheckDigit = (10 - (intAccumulator Mod 10)) Mod 10
    End If

End Function

Function SkuIsForDelivery(strSKU As String) As Boolean

Static oStockItem As cStock_Wickes.cInventoryWickes

    If oStockItem Is Nothing Then
        Set oStockItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORYWICKES)
        Call oStockItem.AddLoadField(FID_INVENTORYWICKES_SaleTypeAttribute)
    Else
        Call oStockItem.ClearLoadFilter
    End If
    
    Call oStockItem.AddLoadFilter(CMP_EQUAL, FID_INVENTORYWICKES_PartCode, strSKU)
    
    If oStockItem.LoadMatches.Count = 0 Then
        SkuIsForDelivery = False
        Exit Function
    End If

    SkuIsForDelivery = (oStockItem.SaleTypeAttribute = "D")
    
End Function

Function SkuIsGiftCardOrVaucher(strSKU As String) As Boolean

Static oStockItem As cStock_Wickes.cInventoryWickes

    If oStockItem Is Nothing Then
        Set oStockItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORYWICKES)
        Call oStockItem.AddLoadField(FID_INVENTORYWICKES_SaleTypeAttribute)
    Else
        Call oStockItem.ClearLoadFilter
    End If
    
    Call oStockItem.AddLoadFilter(CMP_EQUAL, FID_INVENTORYWICKES_PartCode, strSKU)
    
    If oStockItem.LoadMatches.Count = 0 Then
        SkuIsGiftCardOrVaucher = False
        Exit Function
    End If

    SkuIsGiftCardOrVaucher = (oStockItem.SaleTypeAttribute = "V")
    
End Function

Public Sub SavePPDetails(oTranHeaderBO As cPOSHeader, strCustName As String, strAddress1 As String, strAddress2 As String, strAddress3 As String, _
                        strAddress4 As String, strPostCode As String, strPhone As String, lngLineNo As Long, strCompetitorName As String, _
                        strCompetitorAddress1 As String, strCompetitorAddress2 As String, strCompetitorAddress3 As String, strCompetitorAddress4 As String, _
                        strCompetitorPostcode As String, strCompetitorPhone As String, curEnteredPrice As Currency, _
                        blnIncludeVAT As Boolean, curCompPriceExc As Currency, strOrigStoreNumber As String, dteOrigTransDate As Date, strOrigTillNumber As String, _
                        strOrigTransactionNumber As String, curOrigSellingPrice As Currency)
    

Dim oPPCust As cPriceCusInfo
Dim oLine   As cPriceLineInfo
   
    'Save DLOCUS information
        
    Set oPPCust = oTranHeaderBO.AddPricePromiseCust(strCustName)
'    oPPCust.Title = strTitle
'    oPPCust.Initial = strInitials
    oPPCust.Address1 = strAddress1
    oPPCust.Address2 = strAddress2
    oPPCust.Address3 = strAddress3
    oPPCust.Address4 = strAddress4
    oPPCust.PostCode = strPostCode
    oPPCust.Phone = strPhone
    
    'Save DLOLIN information
    Set oLine = oTranHeaderBO.AddPricePromiseLine(lngLineNo)
    oLine.CompetitorName = strCompetitorName
    oLine.CompetitorAddress1 = strCompetitorAddress1
    oLine.CompetitorAddress2 = strCompetitorAddress2
    oLine.CompetitorAddress3 = strCompetitorAddress3
    oLine.CompetitorAddress4 = strCompetitorAddress4
    oLine.CompetitorPostcode = strCompetitorPostcode
    oLine.CompetitorPhone = strCompetitorPhone
    oLine.EnteredPrice = curEnteredPrice
    oLine.IncludeVAT = blnIncludeVAT
    If (blnIncludeVAT = False) Then oLine.ConvertedPrice = curCompPriceExc
    
    oLine.OrigStoreNumber = strOrigStoreNumber
    oLine.OrigTransDate = dteOrigTransDate
    If (Year(oLine.OrigTransDate) = 1899) Then oLine.OrigTransDate = Now()
    If (Val(strOrigTransactionNumber) > 0) Then oLine.Previous = True
    oLine.OrigTillNumber = strOrigTillNumber
    oLine.OrigTransactionNumber = strOrigTransactionNumber
    oLine.OrigSellingPrice = curOrigSellingPrice
              
End Sub 'SavePPDetails


Public Sub SendKey(bKey As Byte)
    Dim GInput(0 To 1) As GENERALINPUT
    Dim KInput As KEYBDINPUT
    KInput.wVk = bKey  'the key we're going to press
    KInput.dwFlags = 0 'press the key
    'copy the structure into the input array's buffer.
    GInput(0).dwType = INPUT_KEYBOARD   ' keyboard input
    CopyMemory GInput(0).xi(0), KInput, Len(KInput)
    'do the same as above, but for releasing the key
    KInput.wVk = bKey  ' the key we're going to realease
    KInput.dwFlags = KEYEVENTF_KEYUP  ' release the key
    GInput(1).dwType = INPUT_KEYBOARD  ' keyboard input
    CopyMemory GInput(1).xi(0), KInput, Len(KInput)
    'send the input now
    Call SendInput(2, GInput(0), Len(GInput(0)))
End Sub
 

Public Function CreateEANReject(blnScanned As Boolean, _
                                strTranNo As String, _
                                strReasonCode As String, _
                                strSKU As String, _
                                strItemType As String, _
                                blnEANFound As Boolean, _
                                strEANSKU As String, _
                                blnEANSKUFound As Boolean, _
                                strCorrectSKU As String, _
                                strUserId As String) As cEANCheck

Dim oEANRejectBO    As cEANCheck

    On Error GoTo Error_CreateEANReject
    
    Set oEANRejectBO = goDatabase.CreateBusinessObject(CLASSID_EAN_CHECK)
    oEANRejectBO.TranDate = Date
    oEANRejectBO.TranTillID = goSession.CurrentEnterprise.IEnterprise_WorkstationID
    oEANRejectBO.TranTime = Format(Now, "HHMM")
    oEANRejectBO.TranNo = strTranNo
    If (blnScanned = True) Then
        oEANRejectBO.EntryMethod = "Scanned"
    Else
        oEANRejectBO.EntryMethod = "Keyed"
    End If
    oEANRejectBO.ReasonCode = strReasonCode
    oEANRejectBO.EnteredValue = strSKU
    oEANRejectBO.ItemType = strItemType
    oEANRejectBO.EANSKU = strEANSKU
    oEANRejectBO.EANFound = blnEANFound
    oEANRejectBO.CorrectSKU = strCorrectSKU
    oEANRejectBO.EANSKUFound = blnEANSKUFound
    oEANRejectBO.UserID = strUserId
    If (oEANRejectBO.SaveIfNew = False) Then Call Err.Raise(1, MODULE_NAME & ":CreateEANReject", "Failed to save EAN reject")
    Set CreateEANReject = oEANRejectBO
    Debug.Print (strReasonCode)
    Exit Function
    
Error_CreateEANReject:


    Call Err.Report(MODULE_NAME, "CreateEANReject", 1, True, "Error Detected", "Error detected whilst saving EAN Rejection - System will continue")
    Call Err.Clear
    
End Function

Public Function UpdateEANReject(EANRejectID As Long, _
                                strCorrectSKU As String) As cEANCheck

Dim oEANRejectBO    As cEANCheck

    On Error GoTo Error_CreateEANReject
    
    Set oEANRejectBO = goDatabase.CreateBusinessObject(CLASSID_EAN_CHECK)
    Call oEANRejectBO.AddLoadFilter(CMP_EQUAL, FID_EAN_CHECK_EntryID, EANRejectID)
    Call oEANRejectBO.LoadMatches
    oEANRejectBO.TranTime = Format(Now, "HHMM")
    oEANRejectBO.CorrectSKU = strCorrectSKU
    If (oEANRejectBO.SaveIfExists = False) Then Call Err.Raise(1, MODULE_NAME & ":CreateEANReject", "Failed to Update EAN reject")
    Set UpdateEANReject = oEANRejectBO
    Call DebugMsg(MODULE_NAME, "UpdateEANReject", endlDebug, EANRejectID & " to " & strCorrectSKU)
    Exit Function
    
Error_CreateEANReject:

    Call Err.Report(MODULE_NAME, "UpdateEANReject", 1, True, "Error Detected", "Error detected whilst saving EAN Rejection - System will continue")
    Call Err.Clear
    
End Function

' CR0034
' Additional PTID to go into new panel on the end
Public Sub InitialiseTillStatusBar(ByRef sbInfo As StatusBar, TillNo As String, Optional ByVal PTID As String = "")
    
    sbInfo.Panels(PANEL_VERNO).Text = "Ver. " & App.Major & "." & App.Minor & "." & App.Revision
    sbInfo.Panels(PANEL_VERNO).ToolTipText = "Program Version Number"
    sbInfo.Panels(PANEL_WSID + 3).Text = TillNo & " "
    sbInfo.Panels(PANEL_WSID + 3).ToolTipText = "Current Work-Station Number"
    sbInfo.Panels(PANEL_DATE + 3).Text = Format$(Date, "DD-MMM-YY")
    ' Do not show ptid panel if no ptid id (including PTID Unkown etc) value sent.  i.e. CR0034 probably not active
    If PTID = "" Then
        sbInfo.Panels(PANEL_PED).Visible = False
    Else
        sbInfo.Panels(PANEL_PED).Text = PTID
    End If
End Sub
' End of CR0034

Public Function GetCardScheme(strCardNumber As String) As cDiscountCardScheme

Dim oCardScheme As cDiscountCardScheme
Dim colCardScheme As Collection
Dim blnFoundScheme As Boolean
     
    Set GetCardScheme = Nothing
    
    Set oCardScheme = goDatabase.CreateBusinessObject(CLASSID_DISCOUNTCARD_SCHEME_ID)
   
    'Todo: get first 4 digits of card, to find the card scheme
    'Call oCardScheme.AddLoadFilter(CMP_LIKE, FID_CARD_SCHEME_ID_SchemeID, "9%1%") 'This adds a where clause
    
    Set colCardScheme = oCardScheme.LoadMatches
    blnFoundScheme = False
    
    For Each oCardScheme In colCardScheme
        If oCardScheme.ValidateCard(strCardNumber) Then
            Debug.Print oCardScheme.SchemeID
            blnFoundScheme = True
            Set GetCardScheme = oCardScheme
            Exit For
            'Exit Function
        End If
    Next
    
End Function

Public Function MarkDownBarCodeCheck(strBarCode) As Boolean

        'calculate check digit Using a 313 weighted calculation,:
        'Consider the code          M24032300000435?
        'Giving a value of          13/ 2/ 4/ 0/ 3/ 2/ 3/ 0/ 0/ 0/ 0/ 0/ 4/ 3/ 5
        'Using 313 weighting         3/ 1/ 3/ 1/ 3/ 1/ 3/ 1/ 3/ 1/ 3/ 1/ 3/ 1/ 3
        'Multiplied out gives       33/ 3/12/ 0/ 9/ 2/ 9/ 0/ 0/ 0/ 0/ 0/12/ 3/15
        'Sum of products            33+03+12+00+09+02+09+00+00+00+00+00+12+03+15=105
        'Take Mod 26(i.e.range A to Y)  105 - 26 - 26 - 26 = 25
        'Convert to relative alpha      25 = Y     Barcode = M24032300000435Y

        Dim strAlphabet As String
        Dim intCheckDigit As Integer
        Dim Index As Integer
        Dim strCheckChar As String
        
        'Check barcode length
        If Len(strBarCode) <> (Len(mstrMarkDownStyle)) Then
            MarkDownBarCodeCheck = False
            Exit Function
        End If
        
        'Calculate check digit
        strAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        Index = 1
        intCheckDigit = (CInt(InStr(1, strAlphabet, Mid(strBarCode, Index, 1)) - 1) * (((Index Mod 2) * 2) + 1))
        For Index = 2 To Len(strBarCode) - 1
            intCheckDigit = intCheckDigit + (CInt(Mid(strBarCode, Index, 1)) * (((Index Mod 2) * 2) + 1))
        Next
        
        'Now see if ok ...
        strCheckChar = Mid(strAlphabet, ((intCheckDigit Mod 26) + 1), 1)
        If strCheckChar = Right(strBarCode, 1) Then
            MarkDownBarCodeCheck = True
        Else
            MarkDownBarCodeCheck = False
        End If

End Function

Public Function GetMarkDownPrice(ByRef oMarkDown As cMarkDownStock, ByRef oItem As cInventory, oHierachy As cInventoryWickes) As Currency

Dim vntHierachy As Variant
Dim vntDiscountPercentage
Dim rsStock As New ADODB.Recordset
Dim strSql As String
Dim strDefaultMD As String
Dim intLevel As Integer
Dim intIndex As Integer
Dim i As Integer
Dim dblPercentageOff As Double

    If (oMarkDown.Price = oItem.NormalSellPrice) Or (oItem.NormalSellPrice > oMarkDown.Price) Then
        
        dblPercentageOff = (oMarkDown.ReductionWeek(oMarkDown.WeekNumber) / 100)
        GetMarkDownPrice = Round(oMarkDown.Price - (oMarkDown.Price * dblPercentageOff), 2)
        Exit Function
        
    Else
    
        vntHierachy = Array(oHierachy.HierStyle, oHierachy.HierSubGroup, oHierachy.HierGroup, oHierachy.HierCategory)
        strDefaultMD = goSession.GetParameter(PRM_MARKDOWN_DISCOUNT)
        vntDiscountPercentage = Split(strDefaultMD, ",")
        intLevel = 2
        For intIndex = 0 To 3
            strSql = "SELECT * FROM HIEMAS WHERE LEVL = '" & CStr(intLevel) & "' AND NUMB ='" & vntHierachy(intIndex) & "' AND IDEL=0"
            Call rsStock.Open(strSql, goDatabase.Connection)
            If rsStock.BOF = False Then
                If Not IsNull(rsStock.Fields("ReductionWeek1").Value) Then
                      For i = 1 To 10
                          vntDiscountPercentage(i - 1) = rsStock.Fields("ReductionWeek" & CStr(i)).Value
                      Next i
                      Exit For
                End If
            End If
            
            intLevel = intLevel + 1
            rsStock.Close
        
        Next intIndex
        
        'The price has gone down so we need to calculate the new price
        GetMarkDownPrice = Round(oItem.NormalSellPrice - (oItem.NormalSellPrice * (vntDiscountPercentage(oMarkDown.WeekNumber - 1) / 100)), 2)
        
    End If

End Function

Public Function GetAuthorisationLimit(dblOverridePrice As Double, strSKU As String, strMarkDownSerial As String) As Boolean
    
Dim vntHierachy As Variant
Dim vntDiscountPercentage
Dim rsStock As New ADODB.Recordset
Dim strSql As String
Dim strDefaultMD As String
Dim intLevel As Integer
Dim intIndex As Integer
Dim i As Integer
Dim oHierachy As cInventoryWickes
Dim oItem As cInventory
Dim dblOverridePerc As Double
Dim dblCalcPercentage As Double

    If (mstrMarkDownStyle = "") Then Exit Function
    Set oHierachy = goSession.Database.CreateBusinessObject(CLASSID_INVENTORYWICKES)
    Call oHierachy.AddLoadFilter(CMP_EQUAL, FID_INVENTORYWICKES_PartCode, strSKU)
    Call oHierachy.LoadMatches
    
    Set oItem = goSession.Database.CreateBusinessObject(CLASSID_INVENTORY)
    Call oItem.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, strSKU)
    Call oItem.LoadMatches

    vntHierachy = Array(oHierachy.HierStyle, oHierachy.HierSubGroup, oHierachy.HierGroup, oHierachy.HierCategory)
    intLevel = 2
    dblOverridePerc = -1 'false to disabled first
    For intIndex = 0 To 3
        strSql = "SELECT * FROM HIEMAS WHERE LEVL = '" & CStr(intLevel) & "' AND NUMB ='" & vntHierachy(intIndex) & "' AND IDEL=0"
        Call DebugMsg("modTill", "GetAuthorisationLimit", endlTraceOut, strSql)
        Call rsStock.Open(strSql, goDatabase.Connection)
        If rsStock.BOF = False Then
            On Error Resume Next
            dblOverridePerc = rsStock.Fields("MaxOverride").Value
            If Err.Number = 3265 Then
                'Table does not have facility yet installed so just skip
                Err.Clear
                rsStock.Close
                On Error GoTo 0
                Exit For
            End If
            Err.Clear
            rsStock.Close
            Exit For
        End If
        
        intLevel = intLevel + 1
        rsStock.Close
    
    Next intIndex
    
    'The price has gone down so we need to calculate the new price
    dblCalcPercentage = (1 - (dblOverridePrice / oItem.NormalSellPrice)) * 100
    If (dblCalcPercentage > dblOverridePerc) And (dblOverridePerc <> -1) Then
        GetAuthorisationLimit = True
    Else
        GetAuthorisationLimit = False
    End If
        
End Function


Public Function GetBooleanParameter(ParameterId As Integer, oConnection As Connection) As Boolean
    
    Dim strSql As String
    Dim rs As ADODB.Recordset
    
    strSql = "EXEC    [dbo].[SystemGetParameter] @Id = " & CStr(ParameterId)
    Set rs = oConnection.Execute(strSql)
    If rs.BOF = False And rs.EOF = False Then
        GetBooleanParameter = rs.Fields("BooleanValue")
    End If
    Set rs = Nothing
End Function

Public Sub SavePPDetailsNew(oTranHeaderBO As cPOSHeader, lngLineNo As Long, strCustName As String, _
                        strAddress As String, strPostCode As String, strPhone As String, _
                        strCompetitorName As String, _
                        curEnteredPrice As Currency, blnIncludeVAT As Boolean, _
                        curCompPriceExc As Currency, strOrigStoreNumber As String, _
                        dteOrigTransDate As Date, strOrigTillNumber As String, _
                        strOrigTransactionNumber As String, curOrigSellingPrice As Currency)
    
Dim oLine   As cPriceLineInfo
Dim oPPCust As cPriceCusInfo
Dim vntAddress  As Variant

    If Len(strAddress) > 0 Then
        'Save DLOCUS information
        Set oPPCust = oTranHeaderBO.AddPricePromiseCust(strCustName)
    
        vntAddress = Split(strAddress, vbNewLine)
        oPPCust.Address1 = ""
        oPPCust.Address2 = vntAddress(0)
        oPPCust.Address3 = vntAddress(1)
        oPPCust.Address4 = vntAddress(2)
        oPPCust.PostCode = strPostCode
        oPPCust.Phone = strPhone
    End If
    
    'Save DLOLIN information
    Set oLine = oTranHeaderBO.AddPricePromiseLine(lngLineNo)
    oLine.CompetitorName = strCompetitorName
    oLine.CompetitorAddress1 = ""
    oLine.CompetitorAddress2 = ""
    oLine.CompetitorAddress3 = ""
    oLine.CompetitorAddress4 = ""
    oLine.CompetitorPostcode = ""
    oLine.CompetitorPhone = ""
    oLine.EnteredPrice = curEnteredPrice
    oLine.IncludeVAT = blnIncludeVAT
    If (blnIncludeVAT = False) Then oLine.ConvertedPrice = curCompPriceExc
    
    oLine.OrigStoreNumber = strOrigStoreNumber
    oLine.OrigTransDate = dteOrigTransDate
    If (Year(oLine.OrigTransDate) = 1899) Then oLine.OrigTransDate = Now()
    If (Val(strOrigTransactionNumber) > 0) Then oLine.Previous = True
    oLine.OrigTillNumber = strOrigTillNumber
    oLine.OrigTransactionNumber = strOrigTransactionNumber
    oLine.OrigSellingPrice = curOrigSellingPrice
              
End Sub 'SavePPDetailsNew

Public Function GenerateWSSUniqueTransactionTenderReference(tenderType As Long, storeNo As String, oTranHeader As cPOSHeader, ByVal doTopupGiftCard As Boolean) As String

    If tenderType = TEN_GIFTCARD Then
        On Error GoTo FailedBoth
    
        GenerateWSSUniqueTransactionTenderReference = GenerateWSSUniqueTransactionGiftCardReference( _
            storeNo, oTranHeader.TillID, oTranHeader.TransactionNo, oTranHeader.TranDate, GiftCardPaymentCount(oTranHeader) + 1, doTopupGiftCard)
    
        Exit Function
    
    Else
    ' If off line, will not be able to get the Payments collection as requires connection to retrieve initially if not all ready set up
    ' Use error trapping, as tried moTranHeader.Offline (above) but seems to be set to true by default


    ' If can generate Seq No BEFORE save the transaction tender data
    ' (and pass it to the save process), then can use Seq No rather than datetime stamp
    ' currently it is generated by the save process
    On Error GoTo TryDateVersion
        ' Use payments collection count to identify the assigned sequence number (its what TranHeader does when adding a new payment item)
        GenerateWSSUniqueTransactionTenderReference = GenerateWSSUniqueTransactionTenderReferenceWithSequenceNumber( _
            storeNo, oTranHeader.TillID, oTranHeader.TransactionNo, oTranHeader.TranDate, oTranHeader.Payments.Count + 1)
            
        Exit Function
        
TryDateVersion:
    On Error GoTo FailedBoth
        ' No payments collection available, just use the date style
        GenerateWSSUniqueTransactionTenderReference = GenerateWSSUniqueTransactionTenderReferenceWithoutSequenceNumber( _
            storeNo, oTranHeader.TillID, oTranHeader.TransactionNo, oTranHeader.TranDate, oTranHeader.TransactionDateTime)
        Exit Function

FailedBoth:
        GenerateWSSUniqueTransactionTenderReference = "Failed generating reference"

    End If
    On Error GoTo 0
    
End Function

Public Function GenerateWSSUniqueTransactionTenderReferenceWithSequenceNumber(storeNo As String, TillID As String, TransactionNo As String, TransactionDate As Date, SequenceNumber As Integer) As String
    GenerateWSSUniqueTransactionTenderReferenceWithSequenceNumber = _
        "S" & Right("0000" & storeNo, 4) _
        & "T" & Right("00" & TillID, 2) _
        & "X" & Right("0000" & TransactionNo, 4) _
        & "D" & Format(TransactionDate, "ddMMyyyy") _
        & "N" & Right("0000" & Trim(CStr(SequenceNumber)), 4)
End Function

Public Function GenerateWSSUniqueTransactionTenderReferenceWithoutSequenceNumber(storeNo As String, TillID As String, TransactionNo As String, TransactionDate As Date, TransactionDateTime As Date) As String
    GenerateWSSUniqueTransactionTenderReferenceWithoutSequenceNumber = _
        "S" & Right("0000" & storeNo, 4) _
        & "T" & Right("00" & TillID, 2) _
        & "X" & Right("0000" & TransactionNo, 4) _
        & "D" & Format(TransactionDate, "ddMMyyyy" & Format(TimeValue(TransactionDateTime), "hhmmss"))
End Function

Private Function GenerateWSSUniqueTransactionGiftCardReference(storeNo As String, TillID As String, TransactionNo As String, TransactionDate As Date, SequenceNumber As Integer, ByVal isTopUp As Boolean) As String
    GenerateWSSUniqueTransactionGiftCardReference = _
        Right("000" & storeNo, 3) _
        & Right("00" & TillID, 2) _
        & Right("0000" & TransactionNo, 4) _
        & Format(TransactionDate, "ddMMyy") _
        & IIf(isTopUp, "T", CStr(SequenceNumber)) ' There is only one topup of GC per transaction possible, so we mark it simply with "T"
End Function

Public Function GiftCardPaymentCount(oTranHeader As cPOSHeader) As Integer
    Dim cnt As Integer
    cnt = 0
    
    Dim Pmt As cSale_Wickes.cPOSPayment
    For Each Pmt In oTranHeader.Payments
        If Pmt.tenderType = TEN_GIFTCARD Then
            cnt = cnt + 1
        End If
    Next Pmt
    
    GiftCardPaymentCount = cnt
End Function

Public Sub ErrorHandler(ByRef procedureName As String, ByRef errorMessage As String)
    Call DebugMsg(MODULE_NAME, procedureName, endlDebug, "UNHANDLED EXCEPTION: " & errorMessage & ". The application gonna crash! ")

    On Error Resume Next
        Call App.StartLogging("", vbLogToNT)
        Call App.LogEvent("UNHANDLED EXCEPTION: " & errorMessage & ". The application gonna crash! ", vbLogEventTypeError)
    Exit Sub
    
End Sub

