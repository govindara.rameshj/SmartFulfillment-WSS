VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "mswinsck.ocx"
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Begin VB.Form frmTender 
   Caption         =   "Record tender details"
   ClientHeight    =   8850
   ClientLeft      =   2715
   ClientTop       =   1275
   ClientWidth     =   7320
   ControlBox      =   0   'False
   Icon            =   "frmTender.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8850
   ScaleWidth      =   7320
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   45
      Top             =   8475
      Width           =   7320
      _ExtentX        =   12912
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmTender.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   5080
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   635
            MinWidth        =   635
            Picture         =   "frmTender.frx":0F30
            Key             =   "NumPad"
            Object.ToolTipText     =   "Show/Hide Num Pad"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "10:19"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   780
      Top             =   6360
      Visible         =   0   'False
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin VB.Frame fraEntry 
      Caption         =   "Frame1"
      Height          =   6255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7095
      Begin VB.PictureBox fraBorder 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   4455
         Left            =   0
         ScaleHeight     =   4425
         ScaleWidth      =   7065
         TabIndex        =   9
         Top             =   720
         Width           =   7095
         Begin VB.PictureBox fraChequeCard 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   3675
            Left            =   120
            ScaleHeight     =   3675
            ScaleWidth      =   6855
            TabIndex        =   27
            Top             =   0
            Visible         =   0   'False
            Width           =   6855
            Begin VB.TextBox txtEndYear 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   4320
               MaxLength       =   2
               TabIndex        =   18
               Top             =   1680
               Width           =   735
            End
            Begin VB.TextBox txtEndMonth 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   3240
               MaxLength       =   2
               TabIndex        =   17
               Top             =   1680
               Width           =   735
            End
            Begin VB.TextBox txtStartYear 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   4320
               MaxLength       =   2
               TabIndex        =   15
               Top             =   960
               Width           =   735
            End
            Begin VB.TextBox txtCardNo 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   2280
               MaxLength       =   19
               TabIndex        =   12
               Top             =   240
               Width           =   4575
            End
            Begin VB.TextBox txtStartMonth 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   3240
               MaxLength       =   2
               TabIndex        =   14
               Top             =   960
               Width           =   735
            End
            Begin VB.TextBox txtIssueNo 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   555
               Left            =   2280
               MaxLength       =   3
               TabIndex        =   20
               Top             =   2400
               Width           =   975
            End
            Begin VB.TextBox txtCustPresent 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   555
               Left            =   5760
               MaxLength       =   2
               TabIndex        =   22
               Text            =   "Y"
               Top             =   2400
               Width           =   495
            End
            Begin VB.TextBox txtCVV 
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   555
               Left            =   2280
               MaxLength       =   3
               TabIndex        =   24
               Top             =   3120
               Width           =   975
            End
            Begin VB.Label Label8 
               BackStyle       =   0  'Transparent
               Caption         =   "End Month / Year"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   16
               Top             =   1680
               Width           =   2895
            End
            Begin VB.Label Label7 
               BackStyle       =   0  'Transparent
               Caption         =   "Card No"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   11
               Top             =   240
               Width           =   1935
            End
            Begin VB.Label Label6 
               BackStyle       =   0  'Transparent
               Caption         =   "Start Month / Year"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   13
               Top             =   960
               Width           =   3015
            End
            Begin VB.Label Label5 
               BackStyle       =   0  'Transparent
               Caption         =   "Issue No"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   19
               Top             =   2400
               Width           =   1935
            End
            Begin VB.Label Label4 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Cust Present"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   435
               Left            =   3540
               TabIndex        =   21
               Top             =   2400
               Width           =   2055
            End
            Begin VB.Label Label9 
               BackStyle       =   0  'Transparent
               Caption         =   "CVV"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   23
               Top             =   3120
               Width           =   1935
            End
         End
         Begin VB.PictureBox fraAuthNum 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   735
            Left            =   120
            ScaleHeight     =   735
            ScaleWidth      =   6855
            TabIndex        =   10
            Top             =   3600
            Width           =   6855
            Begin VB.TextBox txtAuthNum 
               BackColor       =   &H00C0FFFF&
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   3240
               MaxLength       =   6
               TabIndex        =   26
               Top             =   180
               Width           =   1815
            End
            Begin VB.Label lblAuthNum 
               BackStyle       =   0  'Transparent
               Caption         =   "Authorisation No"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   240
               TabIndex        =   25
               Top             =   180
               Width           =   3015
            End
         End
      End
      Begin VB.PictureBox fraInsertCard 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   2535
         Left            =   0
         ScaleHeight     =   2505
         ScaleWidth      =   7065
         TabIndex        =   38
         Top             =   1680
         Visible         =   0   'False
         Width           =   7095
         Begin VB.Timer tmrESocket 
            Left            =   0
            Top             =   0
         End
         Begin VB.Label lblInsert 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1695
            Left            =   240
            TabIndex        =   39
            Top             =   120
            Width           =   6615
         End
      End
      Begin VB.PictureBox fraAccountNo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   2415
         Left            =   0
         ScaleHeight     =   2385
         ScaleWidth      =   7065
         TabIndex        =   28
         Top             =   1680
         Visible         =   0   'False
         Width           =   7095
         Begin VB.TextBox txtChequeNo 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   2280
            MaxLength       =   6
            TabIndex        =   1
            Top             =   240
            Width           =   1455
         End
         Begin VB.TextBox txtSortCode 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   2280
            MaxLength       =   6
            TabIndex        =   2
            Top             =   960
            Width           =   1455
         End
         Begin VB.TextBox txtAccountNo 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   2280
            MaxLength       =   10
            TabIndex        =   3
            Top             =   1680
            Width           =   3015
         End
         Begin VB.Label Label1 
            BackStyle       =   0  'Transparent
            Caption         =   "Cheque No"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   240
            TabIndex        =   29
            Top             =   240
            Width           =   1935
         End
         Begin VB.Label Label2 
            BackStyle       =   0  'Transparent
            Caption         =   "Sort Code"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   240
            TabIndex        =   30
            Top             =   960
            Width           =   1695
         End
         Begin VB.Label Label3 
            BackStyle       =   0  'Transparent
            Caption         =   "Account No"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   240
            TabIndex        =   31
            Top             =   1680
            Width           =   1935
         End
      End
      Begin VB.PictureBox fraChequeType 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   1215
         Left            =   0
         ScaleHeight     =   1215
         ScaleWidth      =   7095
         TabIndex        =   4
         Top             =   1920
         Visible         =   0   'False
         Width           =   7095
         Begin VB.CommandButton cmdNoTransaxChq 
            Caption         =   "N-No Transax"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   204
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   4800
            TabIndex        =   7
            Top             =   0
            Width           =   2300
         End
         Begin VB.CommandButton cmdCompanyChq 
            Caption         =   "C-Company"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   204
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   2400
            TabIndex        =   6
            Top             =   0
            Width           =   2300
         End
         Begin VB.CommandButton cmdPersonalChq 
            Caption         =   "P-Personal"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   204
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   0
            TabIndex        =   5
            Top             =   0
            Width           =   2300
         End
         Begin VB.CommandButton cmdEscCheque 
            Caption         =   "Esc-Cancel"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   204
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   5040
            TabIndex        =   8
            Top             =   600
            Width           =   2055
         End
      End
      Begin VB.PictureBox fraComplete 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   2295
         Left            =   -120
         ScaleHeight     =   2265
         ScaleWidth      =   7065
         TabIndex        =   32
         Top             =   5640
         Visible         =   0   'False
         Width           =   7095
         Begin VB.CommandButton cmdConfirm 
            Caption         =   "&Yes"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   204
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   120
            TabIndex        =   35
            Top             =   1320
            Width           =   1815
         End
         Begin VB.CommandButton cmdCancel 
            Caption         =   "&No"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   204
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   5160
            TabIndex        =   34
            Top             =   1320
            Width           =   1815
         End
         Begin VB.CommandButton cmdExit 
            Caption         =   "EXIT"
            Height          =   495
            Left            =   2760
            TabIndex        =   33
            Top             =   1200
            Width           =   975
         End
         Begin VB.Label lblPrintPrompt 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Insert Cheque - Esc Cancel"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   120
            TabIndex        =   36
            Top             =   240
            Visible         =   0   'False
            Width           =   6855
         End
         Begin VB.Label lblPrintChq 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Print front of cheque?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   120
            TabIndex        =   37
            Top             =   2160
            Visible         =   0   'False
            Width           =   6975
         End
      End
      Begin MSWinsockLib.Winsock Winsock1 
         Left            =   7200
         Top             =   5160
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   393216
      End
      Begin VB.Label lblStatus 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H0000FFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   0
         TabIndex        =   44
         Top             =   5280
         Visible         =   0   'False
         Width           =   7095
      End
      Begin VB.Label lblActionRequired 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Type of Cheque"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   0
         TabIndex        =   43
         Top             =   0
         Visible         =   0   'False
         Width           =   7095
      End
      Begin VB.Label lblChequeType 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   0
         TabIndex        =   42
         Top             =   600
         Visible         =   0   'False
         Width           =   7095
      End
      Begin VB.Label lblAuthTranMod 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Authorisation Server Mode: "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   0
         TabIndex        =   41
         Top             =   720
         Visible         =   0   'False
         Width           =   7095
      End
      Begin VB.Label lblAction 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Enter Credit Card"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   615
         Left            =   0
         TabIndex        =   40
         Top             =   0
         Visible         =   0   'False
         Width           =   7095
      End
   End
End
Attribute VB_Name = "frmTender"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const MODULE_NAME As String = "frmTender"

Enum enCaptureMode
    encmCheque = 1
    encmCCard = 2
    encmAuthCard = 3
    encmKeyed = 4
End Enum

Enum enConfirmMode
    encmReprintSlip = 1
    encmSignatureCon = 2
    encmNon = 3
End Enum

Const VERSION_NUM                   As String = "1.0.36"

'Constants to hold credit card entry modes
Const ENTRY_ICCS                    As String = "ICC Signed"
Const ENTRY_ICCP                    As String = "ICC"
Const ENTRY_ICC_PIN_BYPASS          As String = "ICC Pin Bypass"
Const ENTRY_KEY                     As String = "Keyed"
Const ENTRY_SWIPE                   As String = "Swipe"
'Responces returned from modPayware.InitiliseTransaction
Const READ_ICC                      As Integer = 0
Const READ_EMV                      As Integer = 30
Const MANUAL_ENTRY                  As String = "Manual Entry"
'Cheque types
Const CHEQUE_PERSONAL               As String = "Personal"
Const CHEQUE_COMP                   As String = "Company"
Const CHEQUE_NOTRANSAX              As String = "No Transax"

'Constants refering to the authorisation server
Const AUTH_ONLINE                   As String = "Online"
Const AUTH_OFFLINE                  As String = "Offline"
Const AUTHORISED_BY_PED             As String = "PED"
Const AUTH_SERVER_FOUND             As String = "0"
Const AUTH_SERVER_NOT_FOUND         As String = "1"
Const PRM_AUTHTMODE                 As Long = "827"
Const PRM_CARREG                    As Long = "808"

Const SELECT_CHEQUE_MSG             As String = "Press P for personal cheque" & vbCrLf & _
        "Press C for company cheque" & vbCrLf & _
        "Press N for No Transax" & vbCrLf & "Or press escape to cancel"

Private enEntryMode                 As enCaptureMode
Private enConfirmationMode          As enConfirmMode
Private moOPOSPrinter               As OPOSPOSPrinter
Dim madoConn                        As ADODB.Connection
Dim madors                          As ADODB.Recordset

Dim mblnRefund                      As Boolean
Dim mblnCNPTill                     As Boolean

Dim mlngUseEFTPOS                   As Long
Dim mblnAuthEntered                 As Boolean
Dim mstrTransactionAmount           As String
Dim mdteTransactionDate             As Date
Dim mstrTransactionNo               As String
Dim mstrCreditCardNum               As String
Dim mstrExpiryDate                  As String
Dim mstrStartDate                   As String
Dim mstrIssueNum                    As String
Dim mstrAuthNum                     As String
Dim mstrTrack2                      As String
Dim mstrTmode                       As String
Dim mstrAuthResponse                As String
Dim mstrAuthType                    As String
Dim mstrTerminalVerificationResult  As String
Dim mstrTransactionStatusInfo       As String
Dim mstrAuthResponceCode            As String
Dim mstrUnpredictableNumber         As String
Dim mstrApplicationIdentifier       As String
Dim mstrCashierName                 As String
Dim mstrEntryMethod                 As String
Dim mstrVerification                As String
Dim mstrAuthSuccess                 As String
Dim mstrAuthServerFound             As String
Dim mstrTransactionType             As String
Dim mstrCaptureCarReg               As String
Dim mstrCarReg                      As String
Dim mstrChequeGuaranteeValue        As String
Dim mstrChequeNo                    As String
Dim mstrSortCode                    As String
Dim mstrAccountNo                   As String
Dim mstrChequeType                  As String
Dim mstrEFTTransID                  As String
Dim mlngAttemptNum                  As Long
Dim mstrFallBackType                As String
Dim mstrEFTTranID                   As String

Dim mblnInitiateCheque              As Boolean
Dim mblnEscapePressed               As Boolean

' eSocket variables
Const ERR_CONNECTION As Long = 10061
 
'Action Code responses from eSocket software
Const RESP_ACTIONCODE                   As String = "ActionCode"
Const RESP_AUTHNUM                      As String = "AuthorizationNumber"
Const RESP_CARDNUMBER                   As String = "CardNumber"
Const RESP_CARDPRODUCTNAME              As String = "CardProductName"
Const RESP_DATETIME                     As String = "DateTime"
Const RESP_EXPIRYDATE                   As String = "ExpiryDate"
Const RESP_PANENTRYMODE                 As String = "PanEntryMode"
Const RESP_POSCONDITION                 As String = "PosCondition"
Const RESP_RESPONSECODE                 As String = " ResponseCode"
Const RESP_MESSAGEREASONCODE            As String = " MessageReasonCode"
Const RESP_SERVICERESTRICTIONCODE       As String = "ServiceRestrictionCode"
Const RESP_TRACK2                       As String = "Track2"
Const RESP_TYPE                         As String = "<Esp:"
Const RESP_EVENTID                      As String = "EventId"

Const RESP_EMVAMOUNT                        As String = "EmvAmount"
Const RESP_EMVAMOUNTOTHER                   As String = "EmvAmountOther"
Const RESP_EMVAPPLICATIONIDENTIFIER         As String = "EmvApplicationIdentifier"
Const RESP_EMVAPPLICATIONINTERCHANGEPROFILE As String = "EmvApplicationInterchangeProfile"
Const RESP_EMVAPPLICATIONLABEL              As String = "EmvApplicationLabel"
Const RESP_EMVAPPLICATIONTRANSACTIONCOUNTER As String = "EmvApplicationTransactionCounter"
Const RESP_EMVAPPLICATIONUSAGECONTROL       As String = "EmvApplicationUsageControl"
Const RESP_EMVAPPLICATIONVERSIONNUMBER      As String = "EmvApplicationVersionNumber"
Const RESP_EMVAUTHORIZATIONRESPONSECODE     As String = "EmvAuthorizationResponseCode"
Const RESP_EMVCRYPTOGRAM                    As String = "EmvCryptogram"
Const RESP_EMVCRYPTOGRAMINFORMATIONDATA     As String = "EmvCryptogramInformationData"
Const RESP_EMVCVMRESULTS                    As String = "EmvCvmResults"
Const RESP_EMVISSUERAPPLICATIONDATA         As String = "EmvIssuerApplicationData"
Const RESP_EMVTERMINALCAPABILITIES          As String = "EmvTerminalCapabilities"
Const RESP_EMVTERMINALCOUNTRYCODE           As String = "EmvTerminalCountryCode"
Const RESP_EMVTERMINALTYPE                  As String = "EmvTerminalType"
Const RESP_EMVTERMINALVERIFICATIONRESULT    As String = "EmvTerminalVerificationResult"
Const RESP_EMVTRANSACTIONCURRENCYCODE       As String = "EmvTransactionCurrencyCode"
Const RESP_EMVTRANSACTIONDATE               As String = "EmvTransactionDate"
Const RESP_EMVTRANSACTIONSTATUSINFORMATION  As String = "EmvTransactionStatusInformation"
Const RESP_EMVTRANSACTIONTYPE               As String = "EmvTransactionType"
Const RESP_EMVUNPREDICTABLENUMBER           As String = "EmvUnpredictableNumber"
Const RESP_SIGNATUREREQUIRED                As String = "SignatureRequired"
Const RESP_IACDEFAULT                       As String = "IACDefault"
Const RESP_IACDENIAL                        As String = "IACDenial"
Const RESP_IACONLINE                        As String = "IACOnline"
Const RESP_CARDSEQUENCENUMBER               As String = "CardSequenceNumber"
Const RESP_FALLBACKTYPE                     As String = "FallbackType"
Const RESP_MERCHANTID                       As String = "MerchantId"
Const RESP_CERTEGYAUTHCODE                  As String = "PrivateData"


Const RESP_ACTIONCODE_APPROVE           As String = "APPROVE"
Const RESP_ACTIONCODE_DECLINE           As String = "DECLINE"
Const RESP_ACTIONCODE_AUTH              As String = "AUTH"
Const RESP_TYPE_ERROR                   As String = "Error"
Const RESP_TYPE_TRANSACTION             As String = "Transaction"
Const RESP_TYPE_INQUIRY                 As String = "Inquiry"
Const RESP_TYPE_EVENT                   As String = "Event"
Const RESP_TYPE_CALLBACK                As String = "Callback"
Const RESP_TYPE_ADMIN                   As String = "Admin"

Dim mstrResponseType                        As String
Dim mblnSendComplete                        As Boolean
Dim mblnInitiated                           As Boolean
Dim mblnCardDetailsEntered                  As Boolean
Dim mstrData                                As String
Dim mstrCardProductName                     As String
Dim mstrPanEntryMode                        As String
Dim mstrEmvAmount                           As String
Dim mstrEmvAmountOther                      As String
Dim mstrEmvApplicationIdentifier            As String
Dim mstrEmvApplicationInterchangeProfile    As String
Dim mstrEmvApplicationLabel                 As String
Dim mstrEmvApplicationTransactionCounter    As String
Dim mstrEmvApplicationUsageControl          As String
Dim mstrEmvApplicationVersionNumber         As String
Dim mstrEmvAuthorizationResponseCode        As String
Dim mstrEmvCryptogram                       As String
Dim mstrEmvCryptogramInformationData        As String
Dim mstrEmvCvmResults                       As String
Dim mstrEmvIssuerApplicationData            As String
Dim mstrEmvTerminalCapabilities             As String
Dim mstrEmvTerminalCountryCode              As String
Dim mstrEmvTerminalType                     As String
Dim mstrEmvTerminalVerificationResult       As String
Dim mstrEmvTransactionCurrencyCode          As String
Dim mstrEmvTransactionDate                  As String
Dim mstrEmvTransactionStatusInformation     As String
Dim mstrEmvTransactionType                  As String
Dim mstrEmvUnpredictableNumber              As String
Dim mstrSignatureRequired                   As String
Dim mstrIACDefault                          As String
Dim mstrCardSequenceNum                     As String
Dim mstrIACDenial                           As String
Dim mstrIACOnline                           As String
Dim mstrTID                                 As String
Dim mstrMerchantID                          As String
Dim mblnCustPresent                         As Boolean

Dim mblnShowNumPad   As Boolean
Dim mblnUsingNumPad  As Boolean 'flag if using Number pad for screen resizing
Dim mblnResizedKeyPad As Boolean
Dim mblnProcessCheque As Boolean

Private mintDuressKeyCode As Integer
Public Event Duress()

'Error handler variables
Private errorNo As Long
Private errorDesc As String


Public Function CaptureCheque(ByRef oPrinter As OPOSPOSPrinter, _
                              ByVal dblAmount As Double, _
                              ByRef lngLimit As Long, _
                              ByRef strCardNumber As String, _
                              ByRef strStartDate As String, _
                              ByRef strEndDate As String, _
                              ByRef strIssueNo As String, _
                              ByRef strAccountNo As String, _
                              ByRef strChequeNo As String, _
                              ByRef strSortCode As String, _
                              ByRef strCashierName As String, _
                              ByRef strCarRegNum As String, _
                              ByRef strMerchantID As String, _
                              ByRef strEntryMethod As String, _
                              ByRef strAuthNum As String, _
                              ByRef strEFTTranID As String, _
                              ByRef blnUseNumPad As Boolean)


    mblnResizedKeyPad = False
    mblnProcessCheque = True
    Call SetUpKeyPad(True)
    sbStatus.Visible = False
    mblnShowNumPad = Not blnUseNumPad
    txtChequeNo.Text = vbNullString
    txtSortCode.Text = vbNullString
    txtAccountNo.Text = vbNullString
    txtAuthNum.Text = vbNullString
    txtEndMonth.Text = vbNullString
    txtStartMonth.Text = vbNullString
    txtStartYear.Text = vbNullString
    txtEndYear.Text = vbNullString
    txtCardNo.Text = vbNullString
    txtIssueNo.Text = vbNullString
    mblnCustPresent = False
    txtCustPresent.Text = "Y"
    strCardNumber = vbNullString
    strStartDate = vbNullString
    strEndDate = vbNullString
    strIssueNo = vbNullString
    strAuthNum = vbNullString
    mstrTrack2 = vbNullString
    mstrEFTTranID = vbNullString

    mstrAuthNum = vbNullString
    mstrCreditCardNum = vbNullString
    mstrStartDate = vbNullString
    mstrExpiryDate = vbNullString
    mstrIssueNum = vbNullString
    mstrTerminalVerificationResult = vbNullString
    mstrTransactionStatusInfo = vbNullString
    mstrAuthResponceCode = vbNullString
    mstrUnpredictableNumber = vbNullString
    mstrApplicationIdentifier = vbNullString
    mstrVerification = vbNullString
    mstrResponseType = vbNullString
    mstrCardSequenceNum = vbNullString

    Set moOPOSPrinter = oPrinter
    mstrCashierName = strCashierName
    enEntryMode = encmCheque
    mblnRefund = False
    If Left$(dblAmount, 1) <> "-" Then mblnRefund = True
    dblAmount = Abs(dblAmount)
    mstrTransactionAmount = dblAmount * 100
    Me.Caption = "Record cheque payment (v" & _
        VERSION_NUM & ")"
    
'    If mstrChequeType <> CHEQUE_COMP Then
    fraInsertCard.Visible = True
    lblInsert.Alignment = 0
    If (mblnShowKeypadIcon = False) Then
        cmdCompanyChq.Visible = False
        cmdPersonalChq.Visible = False
        cmdNoTransaxChq.Visible = False
        fraChequeType.Height = 0
    End If
    fraInsertCard.BackColor = RGBMSGBox_PromptColour
    lblInsert.Caption = SELECT_CHEQUE_MSG
    fraInsertCard.Top = lblActionRequired.Top + lblActionRequired.Height + 120
    fraChequeType.Visible = True
    fraChequeType.Top = fraInsertCard.Top + fraInsertCard.Height + 120
    
    Me.Height = fraInsertCard.Top + 120 + fraInsertCard.Height + 480 + fraChequeType.Height + 120
    lblAction.Caption = "Select cheque type"
    lblAction.Visible = True
    
    strChequeNo = "000000"
    strEntryMethod = mstrEntryMethod
    mstrChequeType = CHEQUE_NOTRANSAX

    cmdNoTransaxChq_Click
    
    If mstrAuthNum = "999" Then
        strAuthNum = mstrAuthNum
    End If

End Function 'CaptureCheque

Private Sub ConfirmPrintedNotOK()

Dim dblTransactionAmount        As Double
Dim strTransaction              As String
Dim intLen                      As Integer
Dim strActionCode               As String
Dim strAuthorisationNumber      As String
Dim strDateTime                 As String
Dim mstrPosCondition            As String
Dim strResponseCode             As String
Dim strMessageReasonCode        As String
Dim strServiceRestrictionCode   As String
Dim strError                    As String
Dim strEventType                As String
Dim strMsgBoxResponse           As String

    dblTransactionAmount = mstrTransactionAmount
    Select Case enConfirmationMode
        
        'Signature slip did not print correctly
        Case encmReprintSlip
            cmdCancel.Visible = False
            cmdConfirm.Visible = False
            lblPrintChq.Visible = False
            lblPrintPrompt.Caption = "Reprinting Receipt"
            DoEvents
            Call PrintSignatureSlip
        
        'Signature is not ok
        Case encmSignatureCon
            Select Case enEntryMode
           
                Case encmCheque
                    mstrAuthNum = vbNullString
                    txtAccountNo.Text = vbNullString
                    txtChequeNo.Text = vbNullString
                    txtSortCode.Text = vbNullString
                    Call Me.Hide
                    Exit Sub
                Case encmAuthCard
                    Select Case mstrAuthServerFound
                        Case AUTH_SERVER_FOUND
                            'Call modCCAuthorisation.CancelAuthorisation(mstrEFTTransID)
                        Case AUTH_SERVER_NOT_FOUND
                    End Select

                    'Send reversal
                    strTransaction = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & _
                    " encoding=" & Chr(34) & "UTF-8" & Chr(34) & "?>"
            
                    strTransaction = strTransaction & "<Esp:Interface Version=" & Chr(34) & "1.0" & Chr(34) & _
                    " xmlns:Esp=" & Chr(34) & "http://www.mosaicsoftware.com/Postilion/eSocket.POS/" & Chr(34) & _
                    "><Esp:Transaction TerminalId=" & Chr(34) & mstrTID & Chr(34) & " TransactionAmount=" & Chr(34) & _
                    dblTransactionAmount & Chr(34) & " TransactionId=" & Chr(34) & mstrTransactionNo & Chr(34) & " Type=" & Chr(34) & _
                    mstrTransactionType & Chr(34) & " AuthorizationNumber= " & Chr(34) & mstrAuthNum & Chr(34) & _
                    " MessageType=" & Chr(34) & "CONFIRM" & Chr(34) & " Reversal= " & Chr(34) & "TRUE" & Chr(34) & "/></Esp:Interface>"
                    
                    DoEvents
                    intLen = Len(strTransaction)
                    strTransaction = Chr(intLen \ 256) & Chr(intLen Mod 256) & strTransaction
                    RecordDataSent (strTransaction)
                    mstrData = vbNullString
                    mblnSendComplete = False
                    lblAction.Caption = "Sending Reversal"
                    lblStatus.Caption = "Sending Reversal"
                    While mblnSendComplete = False
                        DoEvents
                    Wend
                     'Loop until there is a response from the eSocket software
                    Call WaitForData
                    Call RecordDataLog("CONFIRM REVERESE(AUTHCARD)")

                    mstrAuthNum = vbNullString
                    Call Me.Hide
                    Exit Sub
                Case encmKeyed
                    Select Case mstrAuthServerFound
                        Case AUTH_SERVER_FOUND
                            'Call modCCAuthorisation.CancelAuthorisation(mstrEFTTransID)
                        Case AUTH_SERVER_NOT_FOUND
                    End Select

                    'Send reversal
                    If mstrTransactionType <> "REFUND" Then

                        strTransaction = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & _
                        " encoding=" & Chr(34) & "UTF-8" & Chr(34) & "?>"
                
                        strTransaction = strTransaction & "<Esp:Interface Version=" & Chr(34) & "1.0" & Chr(34) & _
                        " xmlns:Esp=" & Chr(34) & "http://www.mosaicsoftware.com/Postilion/eSocket.POS/" & Chr(34) & _
                        "><Esp:Transaction TerminalId=" & Chr(34) & "12345678" & Chr(34) & " TransactionAmount=" & Chr(34) & _
                        dblTransactionAmount & Chr(34) & " TransactionId=" & Chr(34) & mstrTransactionNo & Chr(34) & " Type=" & Chr(34) & _
                        mstrTransactionType & Chr(34) & _
                        " MessageType=" & Chr(34) & "AUTH" & Chr(34) & " Reversal=" & Chr(34) & "TRUE" & Chr(34) & " /></Esp:Interface>"
 
                        DoEvents
                        intLen = Len(strTransaction)
                        strTransaction = Chr(intLen \ 256) & Chr(intLen Mod 256) & strTransaction
                        RecordDataSent (strTransaction)
                        mstrData = vbNullString
                        mblnSendComplete = False
                        lblAction.Caption = "Sending Reversal"
                        lblStatus.Caption = "Sending Reversal"
                        While mblnSendComplete = False
                            DoEvents
                        Wend
                        
                         'Loop until there is a response from the eSocket software
                        Call WaitForData
                        Call RecordDataLog("CONFIRM REVERESE(KEYED)")
                    End If

                    mstrAuthNum = vbNullString
                    Call Me.Hide
                    Exit Sub
                Case Else
            End Select
        Case Else
    End Select
    
End Sub 'ConfirmPrintedNotOk

Private Sub cmdCompanyChq_Click()
    
    mstrChequeType = CHEQUE_COMP
    cmdPersonalChq.Value = True

End Sub 'cmdCompanyChq_Click

Private Sub ConfirmPrintedOk()
        
Dim dblTransactionAmount        As Double
Dim strTransaction              As String
Dim intLen                      As Integer
Dim strActionCode               As String
Dim strAuthorisationNumber      As String
Dim strDateTime                 As String
Dim mstrPosCondition            As String
Dim strResponseCode             As String
Dim strMessageReasonCode        As String
Dim strServiceRestrictionCode   As String
Dim strError                    As String
Dim strEventType                As String
Dim strMsgBoxResponse           As String

    Call DebugMsg(MODULE_NAME, "ConfirmPrintedOK", endlTraceIn, "ConfMode=" & enConfirmationMode & ",EntryMode=" & enEntryMode)
    dblTransactionAmount = mstrTransactionAmount

    Select Case enConfirmationMode
        
        'Confirming that the signature slip has printed ok
        Case encmReprintSlip
            enConfirmationMode = encmSignatureCon
            If Val(mstrTransactionAmount) > (50 * 100) Then
                If (mstrEntryMethod = ENTRY_KEY) And (txtCustPresent.Text = "Y") And (mblnRefund = False) Then
                    Call MsgBoxEx("An imprint of the card must be obtained on a verification voucher for this tender." & _
                        vbCrLf & "Confirm verification completed and signed by customer", vbOKOnly, _
                        "Manual Imprint Required", "Confirm", , , , RGBMsgBox_WarnColour)
                End If
            End If
            'For Customer not present do not confirm Signature, just complete
            If (mblnCNPTill = True) Then
                Call ConfirmPrintedOk
                Exit Sub
            End If
            
            If MsgBoxEx("Is the Signature ok?", vbYesNo, "Check Signature", , , , , RGBMSGBox_PromptColour) = vbYes Then
                Call ConfirmPrintedOk
                Exit Sub
            Else
                Call ConfirmPrintedNotOK
            End If
        
        'Confirming that the signature is ok
        Case encmSignatureCon
            
            Select Case enEntryMode
            
                Case encmCheque
                    Dim o As New clsReceiptPrinting
                    Set o.Printer = moOPOSPrinter
                    o.TillNo = moTranHeader.TillID
                    o.TranNo = moTranHeader.TransactionNo
                    Set o.goRoot = goRoot
                    Call o.Init(goSession, goDatabase)
                    
                    Call o.PrintChequeFranking(moTranHeader.TranDate, moTranHeader.TransactionTime, goSession.CurrentEnterprise.IEnterprise_StoreNumber, moTranHeader.TillID, moTranHeader.TransactionNo, moTranHeader.CashierID, moTranHeader.TransactionCode, mstrTransactionAmount / 100, mstrCreditCardNum, mstrExpiryDate, , mstrAuthNum, mstrMerchantID)
                    
                    'Check the signature on the cheque
                    'If MsgBoxEx("Is the Signature on the cheque ok?", vbYesNo, "Check Signature", , , , , RGBMSGBox_PromptColour) = vbYes Then 'WIX1375-Switch Off No-Transax
                    If mstrAuthNum <> "" Then
                        If MsgBoxEx("Check the signature on the Cheque." & vbCrLf & "If OK, then obtain authorisation and confirm Cheque Authorised.", vbYesNo, "Check Signature", , , , , RGBMSGBox_PromptColour) = vbYes Then
                            'mstrEntryMethod = "Cheque"
                            mstrEntryMethod = vbNullString
                            Call Me.Hide
                            Exit Sub
                        Else
                            'If failed then hide Auth - WIX1376 - No Transax
                            'Load frmVerifyPwd
                            'Do
                            'Loop Until frmVerifyPwd.VerifySupervisorPassword(ucKeyPad1.Visible) = True
                            'Unload frmVerifyPwd
                            mstrAuthNum = vbNullString
                            mstrChequeType = vbNullString
                            txtChequeNo.Text = vbNullString
                            txtSortCode.Text = vbNullString
                            txtAccountNo.Text = vbNullString
                            Call MsgBoxEx("Not Authorised." & vbCrLf & "Please ask customer for alternative Payment Type.", vbOKOnly _
                                , "Declination Card", , , , , RGBMsgBox_WarnColour)
                            Call Me.Hide
                            Exit Sub
                        End If
                    Else
                    End If
                    
                Case encmAuthCard
                    DoEvents
                    fraInsertCard.Visible = True
                    lblStatus.Visible = True
                    fraChequeCard.Visible = True
                    fraBorder.Visible = True
                    fraAuthNum.Visible = True
                    txtAuthNum.BackColor = RGB_LTYELLOW
                    fraComplete.Visible = False
                    lblPrintChq.Visible = True
                    lblPrintPrompt.Visible = False
                    'Completes the transaction - online
                    lblStatus.Caption = "Completing Transaction Please Wait"
                    DoEvents
                    Call DebugMsg("frmTender", "cmdConfirm_Click", endlDebug, mstrAuthServerFound)

                    'Send Confirmation
                     strTransaction = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & _
                    " encoding=" & Chr(34) & "UTF-8" & Chr(34) & "?>"
            
                    strTransaction = strTransaction & "<Esp:Interface Version=" & Chr(34) & "1.0" & Chr(34) & _
                    " xmlns:Esp=" & Chr(34) & "http://www.mosaicsoftware.com/Postilion/eSocket.POS/" & Chr(34) & _
                    "><Esp:Transaction TerminalId=" & Chr(34) & mstrTID & Chr(34) & " TransactionAmount=" & Chr(34) & _
                    dblTransactionAmount & Chr(34) & " TransactionId=" & Chr(34) & mstrTransactionNo & Chr(34) & " Type=" & Chr(34) & _
                    mstrTransactionType & Chr(34) & " AuthorizationNumber= " & Chr(34) & mstrAuthNum & Chr(34) & _
                    " MessageType=" & Chr(34) & "CONFIRM" & Chr(34) & " /></Esp:Interface>"
                        
                       
                    DoEvents
                    intLen = Len(strTransaction)
                    strTransaction = Chr(intLen \ 256) & Chr(intLen Mod 256) & strTransaction
                    RecordDataSent (strTransaction)
                    mstrData = vbNullString
                    mblnSendComplete = False
                    lblAction.Caption = "Sending Confirmation"
                    lblStatus.Caption = "Sending Confirmation"
                    While mblnSendComplete = False
                        DoEvents
                    Wend
                    Wait (1)
                     'Loop until there is a response from the eSocket software
                    Call WaitForData(, "Confirmation Sent")
                    
                    Call RecordDataLog("CONFIRMED(AUTHCARD)")
                    mstrData = "BYPASS"
                    lblAction.Caption = "Printing Receipt"
                    lblStatus.Caption = "Printing Receipt"
                    DoEvents
                    'Call PrintCreditCardreceipt - taken out for WIX1180
                    lblStatus.Caption = "Please remove any credit cards from the Pin Pad"
                    DoEvents
                    Call Me.Hide
                    Exit Sub
                    
                Case encmKeyed
                    fraInsertCard.Visible = False
                    lblStatus.Visible = False 'Was True
                    fraChequeCard.Visible = True
                    fraBorder.Visible = True
                    fraAuthNum.Visible = True
                    fraAuthNum.Visible = False
                    txtAuthNum.BackColor = RGB_LTYELLOW
                    fraComplete.Visible = False
                    lblPrintChq.Visible = True
                    lblPrintPrompt.Visible = False
                    DoEvents
                    'Completes the transaction - online
                    lblStatus.Caption = "Completing Transaction Please Wait"
                    DoEvents
                    'Call PrintCreditCardreceipt - taken out for WIX1180

                    'Send Confirmation
                    If mstrTransactionType <> "REFUND" Then

                        strTransaction = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & _
                        " encoding=" & Chr(34) & "UTF-8" & Chr(34) & "?>"
                
                        strTransaction = strTransaction & "<Esp:Interface Version=" & Chr(34) & "1.0" & Chr(34) & _
                        " xmlns:Esp=" & Chr(34) & "http://www.mosaicsoftware.com/Postilion/eSocket.POS/" & Chr(34) & _
                        "><Esp:Transaction TerminalId=" & Chr(34) & "12345678" & Chr(34) & " TransactionAmount=" & Chr(34) & _
                        dblTransactionAmount & Chr(34) & " TransactionId=" & Chr(34) & mstrTransactionNo & Chr(34) & " Type=" & Chr(34) & _
                        mstrTransactionType & Chr(34) & _
                        " MessageType=" & Chr(34) & "CONFIRM" & Chr(34) & " /></Esp:Interface>"
                        DoEvents
                        intLen = Len(strTransaction)
                        strTransaction = Chr(intLen \ 256) & Chr(intLen Mod 256) & strTransaction
                        RecordDataSent (strTransaction)
                        mstrData = vbNullString
                        mblnSendComplete = False
                        lblAction.Caption = "Sending Confirmation"
                        lblStatus.Caption = "Sending Confirmation"
                        While mblnSendComplete = False
                            DoEvents
                        Wend

                         'Loop until there is a response from the eSocket software
                        Call WaitForData
                        Call RecordDataLog("CONFIRMED(KEYED)")
                    End If

                    lblStatus.Caption = "Transaction Complete"
                    Call Me.Hide
                    Exit Sub
                
            End Select
            
        Case Else
    
    End Select 'enConfirmationMode
    
End Sub 'ConfirmPrintedOk

Private Sub cmdEscCheque_Click()

    Call Form_KeyPress(vbKeyEscape)

End Sub

Private Sub cmdExit_Click()

    If (cmdExit.Visible = True) And (cmdExit.Enabled = True) Then cmdExit.SetFocus
    Call Me.Hide

End Sub 'cmdExit_Click

Private Sub cmdNoTransaxChq_Click()
    
    mblnEscapePressed = False
    mstrChequeType = CHEQUE_NOTRANSAX
    If goSession.GetParameter(PRM_USE_CHQEFTPOS) = 3 Then
        
        fraChequeType.Visible = False
        fraInsertCard.Visible = False
        lblActionRequired.Visible = False
        lblAction.Visible = True
        fraBorder.Visible = True
        fraComplete.Top = fraChequeCard.Top
        lblAction.Caption = "Enter Cheque Details"
        fraBorder.Visible = False
        fraAccountNo.Visible = True
        fraAccountNo.Top = 800
        
        sbStatus.Visible = True
        
        Me.Height = fraAccountNo.Top + fraAccountNo.Height '+ 600
        'txtChequeNo.SetFocus
        Call ShowKeyPad
        Call CentreForm(Me)
        lblStatus.Visible = False
        Me.Show (vbModal)
    End If
    
    If mblnEscapePressed = False Then
        fraInsertCard.Visible = False
        lblChequeType.Caption = "Cheque Type - No Transax"
        lblChequeType.Visible = True
        Call ChequeNoTransax
    End If

End Sub 'cmdNoTransaxChq_Click

Private Sub cmdPersonalChq_Click()

    If (mstrChequeType = CHEQUE_COMP) Then
        fraInsertCard.Visible = False
    Else
        mstrChequeType = CHEQUE_PERSONAL
    End If
    
'    cmdNoTransaxChq_Click
    
'    If goSession.GetParameter(PRM_USE_CHQEFTPOS) = 3 Then
'
'        fraChequeType.Visible = False
'        lblActionRequired.Visible = False
'        lblAction.Visible = True
'        fraBorder.Visible = True
'        fraComplete.Top = fraChequeCard.Top
'        lblAction.Caption = "Enter Cheque Details"
'        fraBorder.Visible = False
'        fraAccountNo.Visible = True
'        fraAccountNo.Top = lblAction.Height + 320
'        sbStatus.Visible = True
'        Me.Height = fraAccountNo.Top + fraAccountNo.Height + 600
'        txtChequeNo.SetFocus
'
'        Call ShowKeyPad
'        Call CentreForm(Me)
'    Else
'        cmdNoTransaxChq_Click
'    End If
End Sub 'cmdPersonalChq_Click

Private Sub SetUpKeyPad(blnShowNumPad As Boolean)

    mblnUsingNumPad = blnShowNumPad
    If (Me.Visible = False) Or (ucKeyPad1.Visible = False) Then Exit Sub
    If (blnShowNumPad = True) Then
        Call ucKeyPad1.ShowNumPad
        If (mblnProcessCheque = True) Then
            ucKeyPad1.Left = fraInsertCard.Left + fraInsertCard.Width + 120
        Else
            ucKeyPad1.Left = fraEntry.Left + fraEntry.Width + 120
        End If
        ucKeyPad1.Top = 120
    Else
        Call ucKeyPad1.ShowKeyboard
        ucKeyPad1.Left = 0
        ucKeyPad1.Top = fraEntry.Top + fraEntry.Height + sbStatus.Height + 120
    End If
    mblnResizedKeyPad = True
    
End Sub

Private Sub ShowKeyPad()
    
    mblnShowNumPad = Not mblnShowNumPad
    If (mblnShowNumPad = False) Then
        ucKeyPad1.Visible = False
        If (mblnProcessCheque = True) Then
            Me.Height = 5000
            Me.Width = fraInsertCard.Width + 480
        Else
            Me.Height = fraEntry.Top + lblStatus.Top + lblStatus.Height + 720 + sbStatus.Height
            Me.Width = fraEntry.Width + 480
        End If
    Else
        ucKeyPad1.Visible = True
        If (mblnResizedKeyPad = False) Then Call SetUpKeyPad(mblnUsingNumPad)
        If (mblnUsingNumPad = True) Then
            If (mblnProcessCheque = True) Then
                Me.Width = fraInsertCard.Width + 480 + ucKeyPad1.Width
                Me.Height = ucKeyPad1.Height + sbStatus.Height + 720
            Else
                Me.Width = fraEntry.Width + 600 + ucKeyPad1.Width
                Me.Height = fraEntry.Top + lblStatus.Top + lblStatus.Height + 720 + sbStatus.Height
            End If
        Else
            Me.Width = ucKeyPad1.Width + 60
            Me.Height = ucKeyPad1.Height + sbStatus.Height + fraEntry.Top + fraEntry.Height + 480
        End If
    End If
    Call CentreForm(Me)
    
End Sub

Private Sub CaptureCardDetails()

    fraChequeType.Visible = False
    fraChequeCard.Visible = True
    fraBorder.Visible = True
    fraAuthNum.Visible = True
        
    If (enEntryMode = encmCCard) Or enEntryMode = encmAuthCard Then
        lblAction.Caption = "Card Details"
    Else
        lblAction.Caption = "Enter Cheque Guarantee Card Details"
        lblAction.Font.Size = "18"
    End If
    
    If mstrEntryMethod = ENTRY_KEY Then
        lblAction.Caption = "Enter Card Details"
    End If

End Sub 'CaptureCardDetails

Private Sub Form_Activate()

    If (ucKeyPad1.Visible = True) And (mblnResizedKeyPad = False) Then Call SetUpKeyPad(mblnUsingNumPad)
    If (txtCardNo.Visible = True) And (txtCardNo.Enabled = True) Then txtCardNo.SetFocus

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Call DebugMsg(MODULE_NAME, "KeyDown", endlTraceIn, "Key=" & KeyCode)
    
    If mintDuressKeyCode <> 0 And KeyCode = mintDuressKeyCode Then
        KeyCode = 0
        RaiseEvent Duress
    End If

End Sub 'Form_Keydown

Public Sub InitialiseStatusBar()
    
    sbStatus.Panels(PANEL_VERNO).Text = "Ver. " & App.Major & "." & App.Minor & "." & App.Revision
    sbStatus.Panels(PANEL_VERNO).ToolTipText = "Program Version Number"
    sbStatus.Panels(PANEL_WSID + 1).Text = Right$("00" & goSession.CurrentEnterprise.IEnterprise_WorkstationID, 2) & " "
    sbStatus.Panels(PANEL_WSID + 1).ToolTipText = "Current Work-Station Number"
    sbStatus.Panels(PANEL_DATE + 1).Text = Format$(Date, "DD-MMM-YY")
    If (mblnShowKeypadIcon = False) Then sbStatus.Panels("NumPad").Picture = Nothing

End Sub

Private Sub Form_KeyPress(KeyCode As Integer)

    KeyCode = Asc(UCase$(Chr$(KeyCode)))
    
    If (lblInsert.Caption = SELECT_CHEQUE_MSG) And (lblInsert.Visible = True) And (fraAuthNum.Visible = False) Then
        If KeyCode = vbKeyP Then cmdPersonalChq.Value = True
        If KeyCode = vbKeyC Then cmdCompanyChq.Value = True
        If KeyCode = vbKeyN Then cmdNoTransaxChq.Value = True
    End If

    If KeyCode = vbKeyK Then
        mstrEntryMethod = ENTRY_KEY
        tmrESocket.Interval = 0
'        modPayware.mblnExitCardWait = True
    End If
   
    If KeyCode = vbKeyEscape Then
        If ((enEntryMode = encmCheque) And (mblnInitiateCheque = False)) Or (fraAuthNum.Enabled = True) Then
            KeyCode = 0
            mstrEntryMethod = vbNullString
            If (Me.Visible = True) Then
                mlngAttemptNum = mlngAttemptNum + 1
                mstrAuthNum = vbNullString
                txtChequeNo.Text = vbNullString
                txtSortCode.Text = vbNullString
                txtAccountNo.Text = vbNullString
                txtAuthNum.Text = vbNullString
                txtEndMonth.Text = vbNullString
                txtStartMonth.Text = vbNullString
                txtStartYear.Text = vbNullString
                txtEndYear.Text = vbNullString
                txtCardNo.Text = vbNullString
                txtIssueNo.Text = vbNullString
                mblnCustPresent = False
                txtCustPresent.Text = "Y"
                mstrAuthNum = vbNullString
                mstrTrack2 = vbNullString
                mstrTransactionAmount = 0
                mstrChequeType = vbNullString
                Call Me.Hide
                mblnCardDetailsEntered = True
                mblnAuthEntered = True
                mblnEscapePressed = True
                Debug.Print ("frmTender - Escape Key Pressed")
            End If
        End If
        
    End If
    
End Sub 'form_keypress

Private Sub Form_Load()

    Call DebugMsg(MODULE_NAME, "frmTender_Load", endlTraceIn, "Loading frmTender")
    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
    RGBMSGBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
    mlngUseEFTPOS = goSession.GetParameter(PRM_USE_EFTPOS)
    mstrCaptureCarReg = goSession.GetParameter(PRM_CARREG)
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    fraEntry.BackColor = Me.BackColor
    mintDuressKeyCode = goSession.GetParameter(PRM_KEY_DURESS)
    fraBorder.Visible = False
    fraChequeType.BackColor = Me.BackColor
    Call InitialiseStatusBar
    mblnInitiated = False
    lblStatus.Visible = False
End Sub 'Form_Load



Private Sub Form_Unload(Cancel As Integer)
    
    Call DebugMsg(MODULE_NAME, "frmTender_Load", endlTraceOut, "Unloading frmTender")

End Sub

Private Sub lblStatus_Change()

    Call DebugMsg(MODULE_NAME, "Status Change", endlDebug, "Now-" & lblStatus.Caption)

End Sub

Private Sub sbStatus_PanelClick(ByVal Panel As MSComctlLib.Panel)

Dim blnShow As Boolean

    Select Case (Panel.Key)
        Case ("NumPad"):
                    If ((Panel.Picture Is Nothing) = False) Then Call ShowKeyPad
    End Select

End Sub

Private Sub tmrESocket_Timer()
'A timer that fires after one second of showing the "please
'insert your card screen", which starts the credit card processing method

Dim blnSucessful            As Boolean
Dim strInitiateResponce     As String
Dim blnCardFound            As Boolean

    Call DebugMsg(Me.Name, "tmrESocket", endlTraceIn)
    tmrESocket.Interval = 0
    blnSucessful = False
    mlngAttemptNum = 1
    Call CentreForm(Me)
    Call Connect
    If (Winsock1.State = sckConnected) Then
        Call Initiate(True)
    Else
        txtAuthNum.Text = vbNullString
        txtEndMonth.Text = vbNullString
        txtStartMonth.Text = vbNullString
        txtStartYear.Text = vbNullString
        txtEndYear.Text = vbNullString
        txtCardNo.Text = vbNullString
        txtIssueNo.Text = vbNullString
        mblnCustPresent = False
        txtCustPresent.Text = "Y"
        mstrAuthNum = vbNullString
        mstrTrack2 = vbNullString
        mstrTerminalVerificationResult = vbNullString
        mstrTransactionStatusInfo = vbNullString
        mstrAuthResponceCode = vbNullString
        mstrUnpredictableNumber = vbNullString
        mstrApplicationIdentifier = vbNullString
        mstrSignatureRequired = vbNullString
        mstrVerification = vbNullString
        mstrIACDefault = vbNullString
        mstrIACDenial = vbNullString
        mstrIACOnline = vbNullString
    End If
    
  
    Call DebugMsg(MODULE_NAME, "tmrESocket", endlDebug, "Transaction Finished so Me.Hide")
    Call Me.Hide

End Sub 'tmrESocket_Timer

Private Sub txtCVV_GotFocus()
    
    txtCVV.SelStart = 0
    txtCVV.SelLength = Len(txtCVV.Text)
    txtCVV.BackColor = RGBEdit_Colour

End Sub

Private Sub txtCVV_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        Call txtCustPresent_KeyPress(KeyAscii)
    End If
    
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

    If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0

End Sub

Private Sub txtCVV_LostFocus()
    
    txtCVV.BackColor = RGB_WHITE
    If (Trim$(txtCVV.Text) <> "") Then txtCVV.Text = Format$(Val(txtCVV.Text), "000")

End Sub

Private Sub ucKeyPad1_Resize()

    ucKeyPad1.Width = ucKeyPad1.Width

End Sub

Private Sub Winsock1_DataArrival(ByVal bytesTotal As Long)

Dim arByte() As Byte
Dim lngMessagesLength As Long

    mstrData = vbNullString
    ReDim arByte(bytesTotal)
    Call Winsock1.GetData(mstrData, , bytesTotal)
    lngMessagesLength = (Asc(Mid$(mstrData, 1, 1)) * 256) + Asc(Mid$(mstrData, 2, 1))
    Call RecordDataIn
    
End Sub 'Winsock1_DataArrival

Private Sub Winsock1_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)

Dim blnRetry As Boolean
            
    Select Case Number
        Case ERR_CONNECTION
            Winsock1.Close
            'Load frmEsocketReload
            'frmEsocketReload.Show vbModal
            If MsgBoxEx("There was a problem connecting to the eSocket program" & vbCrLf & _
                "Do you wish to retry?", vbYesNo, "Connection Error", , , , , RGBMsgBox_WarnColour) = vbYes Then
                Call Connect
            Else
            End If
'            If (frmEsocketReload.Reconnected = True) Then blnRetry = True
'            Unload frmEsocketReload
            If (blnRetry = False) Then
                Call Me.Hide
            Else
                Call Connect
                Exit Sub
            End If
        Case Else
            Call MsgBoxEx("WARNING: " & Number & " " & Description, vbOKOnly, "WARNING", , , , , RGBMsgBox_WarnColour)
    End Select
    
End Sub 'Winsock1_Error

Private Sub Winsock1_SendComplete()

    mblnSendComplete = True

End Sub 'Winsock1_SendComplete

Private Sub Connect()

    If Winsock1.State <> sckConnected Then
        'Connect to the IP address of the machine with the eSocket software on it
        Call DebugMsg("Connect", vbNullString, endlDebug, "Connecting to WinSck")
        Call Winsock1.Connect("127.0.0.1", "25000")

        While Winsock1.State = sckConnecting
            DoEvents
        Wend

        Select Case Winsock1.State
            Case sckConnected
            Case sckError
                Call Me.Hide
            Case Else
        End Select
    End If

End Sub 'Connect

Private Sub Initiate(ByVal blnStartTransaction As Boolean)

Dim strInt                  As String
Dim strTransaction          As String
Dim lngCounter              As Long
Dim intLen                  As Integer
Dim strResponseCode         As String
Dim strMessageReasonCode    As String
Dim strError                As String
Dim strEventType            As String
Dim objFileSystem           As New FileSystemObject
Dim tsMessage               As TextStream
Dim strRead                 As String
Dim strTillID               As String

    fraInsertCard.Visible = False
    If enEntryMode = encmCheque Then
        lblStatus.Visible = False
    Else
        lblStatus.Visible = True
    End If
    fraChequeCard.Visible = True
    fraAuthNum.Visible = True
    fraBorder.Visible = True
    fraBorder.Enabled = False
    lblAction.Visible = True
    If (txtCVV.Text = "") Then mstrCreditCardNum = vbNullString
    Call DisableKeyedEntry
    Call ResizeFormLiveMode
    DoEvents
    
    Call DebugMsg("Initiate", vbNullString, endlDebug, "Opening text file c:\eftauth.par")
    strTillID = goSession.CurrentEnterprise.IEnterprise_WorkstationID
    'Get terminal ids from eftauth.par
    Set tsMessage = objFileSystem.OpenTextFile("c:\eftauth.par", ForReading, False, TristateFalse)
    strRead = tsMessage.ReadAll
    
    mstrTID = ExtractTID(strRead, "TILL " & strTillID & "=")
    Call DebugMsg("Initiate", vbNullString, endlDebug, "Till Number = " & mstrTID)
    
    If mblnInitiated <> True Then
        lblStatus.Caption = "Initiating system"
        lblAction.Caption = "Initiating system"
        Call DebugMsg(Me.Name, "Initiate", endlDebug, "Initiating system")
        'Clear gobal variable holding the data
        
        mstrData = vbNullString
        strInt = vbNullString
        strInt = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & _
            " encoding=" & Chr(34) & "UTF-8" & Chr(34) & "?>"
              
        strInt = strInt & "<Esp:Interface Version=" & Chr(34) & "1.0" & Chr(34) & _
            " xmlns:Esp=" & Chr(34) & "http://www.mosaicsoftware.com/Postilion/eSocket.POS/" & Chr(34) & _
            "><Esp:Admin TerminalId=" & Chr(34) & mstrTID & Chr(34) & " Action=" & Chr(34) & _
            "INIT" & Chr(34) & "><Esp:Register Type= " & Chr(34) & "EVENT" & Chr(34) & _
            " EventId=" & Chr(34) & "DEBUG_ALL" & Chr(34) & _
            " /></Esp:Admin></Esp:Interface>"
            
        intLen = Len(strInt)
        strInt = Chr(intLen \ 256) & Chr(intLen Mod 256) & strInt
        mblnSendComplete = False
        
        Call DebugMsg(Me.Name, "Initiate", endlDebug, "Sending Initiate command: " & _
            strInt)
        
        RecordDataSent (strInt)
        
        'Loop until WinSck has send all of the message
        While mblnSendComplete = False
            DoEvents
        Wend
        
        'Loop until there is a response from the eSocket software
        Call WaitForData
        Call RecordDataLog("PROCESS INITIATE RESPONSE1")
        
        While mstrResponseType <> RESP_TYPE_ADMIN
            Call WaitForData
            Call RecordDataLog("PROCESS INITIATE RESPONSE2")
            Call DebugMsg(Me.Name, "Initiate", endlDebug, "Data received: " & EraseCardNos(mstrData))
            mstrResponseType = ExtractResponseType(mstrData, RESP_TYPE)
            Select Case mstrResponseType
                Case RESP_TYPE_ERROR
                    strResponseCode = ExtractAttribute(mstrData, RESP_RESPONSECODE)
                    strMessageReasonCode = ExtractAttribute(mstrData, RESP_MESSAGEREASONCODE)
                    strError = ExtractAttribute(mstrData, "Description")
                    
                    If MsgBoxEx("WARNING: There was an error whilst trying to initiate the transaction." & vbCrLf & _
                        strError & vbCrLf & _
                        "Response code: " & strResponseCode & " - " & ResponseMessage(strResponseCode) & _
                        vbCrLf & "Message Reason Code: " & strMessageReasonCode & " - " & MessageReasonCode(strMessageReasonCode) & _
                        vbCrLf & "Do you wish to retry?", vbYesNo, "Warning transaction failed", , , , , RGBMsgBox_WarnColour) = vbYes Then
                        Call Initiate(True)
                    Else
                        Call Me.Hide
                        Exit Sub
                    End If
                    mstrData = vbNullString
                Case Else
            End Select
        Wend
        
        'Determine the response from eSocket Software
        Select Case ExtractAttribute(mstrData, RESP_ACTIONCODE)
            Case RESP_ACTIONCODE_APPROVE
                lblStatus.Caption = "Initiate Successful"
                Wait (1)
                mblnInitiated = True
                
                fraInsertCard.Visible = True
                fraInsertCard.BackColor = RGBMSGBox_PromptColour
                If (txtCVV.Text = "") Then
                    lblInsert.Caption = ("Please insert \ swipe the card in Pin Pad")
                    lblAction.Caption = "Insert Credit Card"
                Else
                    lblInsert.Caption = ("Processing Authorisation")
                    lblAction.Caption = "Processing Authorisation"
                End If
                
                fraBorder.Visible = False
                DoEvents
                
                
                If (blnStartTransaction = True) Then Call Transaction
            Case RESP_ACTIONCODE_DECLINE
                strResponseCode = ExtractAttribute(mstrData, RESP_RESPONSECODE)
                strMessageReasonCode = ExtractAttribute(mstrData, RESP_MESSAGEREASONCODE)
                
                If MsgBoxEx("WARNING: The Initiate has been declined" & vbCrLf & _
                "Response code: " & strResponseCode & " - " & ResponseMessage(strResponseCode) & _
                vbCrLf & "Message Reason Code: " & strMessageReasonCode & " - " & MessageReasonCode(strMessageReasonCode) & _
                    vbCrLf & "Do you wish to retry?", vbYesNo, "Warning Initiate failed", , , , , RGBMsgBox_WarnColour) = vbYes Then
                    Call Initiate(True)
                Else
                    Call Me.Hide
                End If
            Case Else
    
        End Select
    Else
        
        If enEntryMode = encmCheque Then
            lblStatus.Visible = False
        Else
            lblStatus.Visible = True
        End If
        
        fraChequeCard.Visible = True
        fraBorder.Enabled = False
        lblAction.Visible = True
        Call DisableKeyedEntry
        Call ResizeFormLiveMode
        
        fraInsertCard.Visible = True
        fraInsertCard.BackColor = RGBMSGBox_PromptColour
        lblInsert.Caption = ("Please insert \ swipe the card in Pin Pad")
        lblAction.Caption = "Insert Credit Card"
        fraBorder.Visible = False
        DoEvents
        
        If (blnStartTransaction = True) Then Call Transaction
    End If
    
End Sub 'Initiate

Private Function ExtractAttribute(ByVal strSearchString As String, _
                                  ByVal strAttribute As String) As String
Dim lngStartPos As Long
Dim lngEndPos   As Long

    lngStartPos = InStr(1, strSearchString, strAttribute)
    If (lngStartPos = 0) Then
        ExtractAttribute = "NOT FOUND"
    Else
        lngStartPos = lngStartPos + Len(strAttribute & "=""")
        lngEndPos = InStr(lngStartPos, strSearchString, """")
        ExtractAttribute = Mid$(strSearchString, lngStartPos, lngEndPos - lngStartPos)
    End If

End Function 'ExtractAttribute

Private Sub Transaction()

Dim dblTransactionAmount        As Double
Dim strTransaction              As String
Dim intLen                      As Integer
Dim strActionCode               As String
Dim strAuthorisationNumber      As String
Dim strDateTime                 As String
Dim mstrPosCondition            As String
Dim strResponseCode             As String
Dim strMessageReasonCode        As String
Dim strServiceRestrictionCode   As String
Dim strError                    As String
Dim strEventType                As String
Dim strMsgBoxResponse           As String
Dim strStartDate                As String
Dim strPrivateData              As String
Dim blnPasswordVerified         As Boolean
Dim strCommFailedMsg            As String 'WIX1193 - Used to check if comms failed

    On Error GoTo HandleException
    
    Call DebugMsg(Me.Name, "Transaction", endlTraceIn)
    'Clear global variable holding the data
    mstrFallBackType = vbNullString
    mstrEntryMethod = vbNullString
    lblStatus.Caption = vbNullString
    dblTransactionAmount = mstrTransactionAmount
    mstrTransactionType = moTOSType.Description
    Select Case UCase(moTOSType.Code)
        Case TT_SALE, TT_MISCIN, TT_CORROUT
            mstrTransactionType = "PURCHASE"
        Case TT_REFUND, TT_MISCOUT, TT_CORRIN
            mstrTransactionType = "REFUND"
        Case Else
            Call MsgBoxEx("SYSTEM ERROR - " & moTOSType.Code & " not catered for", vbExclamation, "Payment Error")
    End Select

    If (Val(txtStartMonth.Text) > 0) Then
        strStartDate = " StartDate=" & Chr(34) & txtStartYear.Text & txtStartMonth.Text & Chr(34)
    End If

    mstrTransactionNo = Format(Now, "DDHHMMSS")
    mstrTransactionNo = Mid(mstrTransactionNo, 2, 6)
    If Left(mstrTransactionNo, 1) = "0" Then
        mstrTransactionNo = Replace(Left(mstrTransactionNo, 1), "0", "9") & _
        Mid(mstrTransactionNo, 2, 5)
    End If
    mstrEFTTranID = mstrTransactionNo
    
    strTransaction = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & _
        " encoding=" & Chr(34) & "UTF-8" & Chr(34) & "?>"

    If LenB(mstrCreditCardNum) <> 0 Then
        strTransaction = strTransaction & "<Esp:Interface Version=" & Chr(34) & "1.0" & Chr(34) & _
            " xmlns:Esp=" & Chr(34) & "http://www.mosaicsoftware.com/Postilion/eSocket.POS/" & Chr(34) & _
            "><Esp:Transaction TerminalId=" & Chr(34) & mstrTID & Chr(34) & " TransactionAmount=" & Chr(34) & _
            dblTransactionAmount & Chr(34) & " TransactionId=" & Chr(34) & mstrTransactionNo & Chr(34) & " CardNumber=" & _
            Chr(34) & mstrCreditCardNum & Chr(34) & strStartDate & " ExpiryDate=" & Chr(34) & mstrExpiryDate & Chr(34) & _
            IIf(mstrCardSequenceNum = "", "", " CardSequenceNumber=" & Chr(34) & mstrCardSequenceNum & Chr(34)) & _
            " Type=" & Chr(34) & mstrTransactionType & Chr(34)
        If (Val(txtCVV.Text) <> 0) Then strTransaction = strTransaction & " Cvv2=" & Chr(34) & txtCVV.Text & Chr(34)
        If mblnCustPresent = False Then
            strTransaction = strTransaction & " PosCondition =" & Chr(34) & "01" & Chr(34)
        Else
            mblnCustPresent = True
        End If
        strTransaction = strTransaction & " MessageType=" & Chr(34) & "AUTH" & Chr(34) & "/></Esp:Interface>"
    Else
        strTransaction = strTransaction & "<Esp:Interface Version=" & Chr(34) & "1.0" & Chr(34) & _
            " xmlns:Esp=" & Chr(34) & "http://www.mosaicsoftware.com/Postilion/eSocket.POS/" & Chr(34) & _
            "><Esp:Transaction TerminalId=" & Chr(34) & mstrTID & Chr(34) & " TransactionAmount=" & Chr(34) & _
            dblTransactionAmount & Chr(34) & " TransactionId=" & Chr(34) & mstrTransactionNo & Chr(34) & " Type=" & Chr(34) & _
            mstrTransactionType & Chr(34) & " MessageType=" & Chr(34) & "AUTH" & Chr(34) & " /></Esp:Interface>"

    End If
    
    intLen = Len(strTransaction)
    strTransaction = Chr(intLen \ 256) & Chr(intLen Mod 256) & strTransaction
    mblnSendComplete = False
    'Sending Transaction message to eSocket Software
    lblStatus.Caption = "Sending Transaction"
    mstrData = vbNullString
    mstrResponseType = vbNullString
    RecordDataSent (strTransaction)
    Call DebugMsg(Me.Name, "Transaction", endlDebug, "Sending Transaction command: " & _
            EraseCardNos(strTransaction))

    lblStatus.Caption = "Waiting"
    lblAction.Caption = "Waiting"
    While mblnSendComplete = False
        DoEvents
    Wend

     'Loop until there is a response from the eSocket software
    Call WaitForData
    Call RecordDataLog("PROCESS TRANSACTION RESPONSE1")
    Call DebugMsg(Me.Name, "Transaction", endlDebug, "Data received: " & _
        EraseCardNos(mstrData))
    lblAction.Caption = "Processing"
    fraBorder.Visible = True
    fraChequeCard.Visible = True
    fraAuthNum.Visible = True
    fraInsertCard.Visible = False
    Me.Refresh

    While mstrResponseType <> RESP_TYPE_TRANSACTION
        Call WaitForData
        Call RecordDataLog("PROCESS TRANSACTION RESPONSE2")
        Call DebugMsg(Me.Name, "Transaction", endlDebug, "Data received: " & _
            EraseCardNos(mstrData))
        mstrResponseType = ExtractResponseType(mstrData, RESP_TYPE)
        Select Case mstrResponseType
            Case RESP_TYPE_EVENT
                strEventType = ExtractAttribute(mstrData, RESP_EVENTID)
                lblStatus.Caption = Replace(strEventType, "_", " ")
                Call DebugMsg(Me.Name, "Event received", endlDebug, _
                    "Event received: " & strEventType)
                mstrData = vbNullString
                DoEvents
                Wait (1)

            Case RESP_TYPE_CALLBACK
            Case RESP_TYPE_ERROR
                strResponseCode = ExtractAttribute(mstrData, RESP_RESPONSECODE)
                strError = ExtractAttribute(mstrData, "Description")

                If MsgBoxEx("WARNING: There was an error whilst trying to process the transaction." & vbCrLf & _
                    strError & vbCrLf & _
                    "Response code: " & strResponseCode & " - " & ResponseMessage(strResponseCode) & _
                    vbCrLf & "Message Reason Code: " & strMessageReasonCode & " - " & MessageReasonCode(strMessageReasonCode) & _
                    vbCrLf & "Do you wish to retry?", vbYesNo, "Warning transaction failed", , , , , RGBMsgBox_WarnColour) = vbYes Then
                    mlngAttemptNum = mlngAttemptNum + 1
                    Call Transaction
                    Call DebugMsg(Me.Name, "Transaction", endlTraceOut, "Error-Retry")
                    Exit Sub
                Else
                    Call DebugMsg(Me.Name, "Transaction", endlTraceOut, "Error-No Retry")
                    Call Me.Hide
                    Exit Sub
                End If

        End Select
    Wend

    strCommFailedMsg = ""
    'Determine the response from eSocket Software
    Call RecordDataLog("PROCESS TRANSACTION RESP")

TryAgain:
    Select Case ExtractAttribute(mstrData, RESP_ACTIONCODE)

        Case RESP_ACTIONCODE_DECLINE 'Transaction has been declined
            lblStatus.Caption = "Transaction Declined"
            lblAction.Caption = "Declined"
            strResponseCode = ExtractAttribute(mstrData, RESP_RESPONSECODE)
            strMessageReasonCode = ExtractAttribute(mstrData, RESP_MESSAGEREASONCODE)

            strMsgBoxResponse = MsgBoxEx("WARNING: The Transaction has been declined" & vbCrLf & _
                "Response code : " & strResponseCode & " - " & ResponseMessage(strResponseCode) & _
                vbCrLf & "Message Reason Code: " & strMessageReasonCode & " - " & MessageReasonCode(strMessageReasonCode) _
                , vbYesNoCancel, _
                "DECLINED", "Retry", "Alternate Tender", "Key", , 16416882)
                mlngAttemptNum = mlngAttemptNum + 1

            Select Case strMsgBoxResponse
                Case vbYes
                    Call Transaction
                    Exit Sub
                Case vbNo
                    mstrAuthNum = vbNullString
                    
                    'Need to get Authorisation if doing a refund - as per ref No. 3 2010-016
                    If mblnRefund And (Not frmTill.mcolRefundTrans Is Nothing) Then
                        Call Load(frmVerifyPwd) 'MO'C Ref 487 missed the show numpad option
                        blnPasswordVerified = frmVerifyPwd.VerifyManagerPassword(False, "This change of tender requires management Athorisation.")
                        Call Unload(frmVerifyPwd)
                        If (blnPasswordVerified = False) Then
                            GoTo TryAgain 'Need to try again as we need authorisation
                        End If
                    End If
                    Call Me.Hide
                    
                    Exit Sub
                Case Else
                    Call EnableKeyedEntry
                    fraAuthNum.Enabled = False
                    mblnCardDetailsEntered = False
                    lblAction.Caption = "Enter Card Details"
                    lblStatus.Caption = vbNullString
                    If (txtCardNo.Visible = True) And (txtCardNo.Enabled = True) Then txtCardNo.SetFocus
                    Do
                        DoEvents
                    Loop Until mblnCardDetailsEntered = True
                    mstrEntryMethod = ENTRY_KEY
                    If (txtCardNo.Text <> vbNullString) Then
                        Call Transaction
                        Exit Sub
                    End If

            End Select

        Case RESP_ACTIONCODE_APPROVE 'Transaction has been Approved

        'Extract information from responce
            strActionCode = ExtractAttribute(mstrData, RESP_ACTIONCODE)
            strAuthorisationNumber = ExtractAttribute(mstrData, RESP_AUTHNUM)
            mstrCreditCardNum = ExtractAttribute(mstrData, RESP_CARDNUMBER)
            mstrCardProductName = ExtractAttribute(mstrData, RESP_CARDPRODUCTNAME)
            strDateTime = ExtractAttribute(mstrData, RESP_DATETIME)
            mstrExpiryDate = ExtractAttribute(mstrData, RESP_EXPIRYDATE)
            mstrPanEntryMode = ExtractAttribute(mstrData, RESP_PANENTRYMODE)
            mstrPosCondition = ExtractAttribute(mstrData, RESP_POSCONDITION)
            strResponseCode = ExtractAttribute(mstrData, RESP_RESPONSECODE)
            strMessageReasonCode = ExtractAttribute(mstrData, RESP_MESSAGEREASONCODE)
            strServiceRestrictionCode = ExtractAttribute(mstrData, RESP_SERVICERESTRICTIONCODE)
            mstrTrack2 = ExtractAttribute(mstrData, RESP_TRACK2)
            mstrEmvAmount = ExtractAttribute(mstrData, RESP_EMVAMOUNT)
            mstrEmvAmountOther = ExtractAttribute(mstrData, RESP_EMVAMOUNTOTHER)
            mstrEmvApplicationIdentifier = ExtractAttribute(mstrData, RESP_EMVAPPLICATIONIDENTIFIER)
            mstrEmvApplicationLabel = ExtractAttribute(mstrData, RESP_EMVAPPLICATIONLABEL)
            mstrEmvApplicationInterchangeProfile = ExtractAttribute(mstrData, RESP_EMVAPPLICATIONINTERCHANGEPROFILE)
            mstrEmvApplicationTransactionCounter = ExtractAttribute(mstrData, RESP_EMVAPPLICATIONTRANSACTIONCOUNTER)
            mstrEmvApplicationUsageControl = ExtractAttribute(mstrData, RESP_EMVAPPLICATIONUSAGECONTROL)
            mstrEmvApplicationVersionNumber = ExtractAttribute(mstrData, RESP_EMVAPPLICATIONVERSIONNUMBER)
            'mstrEmvAuthorizationResponseCode = ExtractAttribute(mstrData, RESP_EMVAUTHORIZATIONRESPONSECODE)
            mstrEmvCryptogram = ExtractAttribute(mstrData, RESP_EMVCRYPTOGRAM)
            mstrEmvCryptogramInformationData = ExtractAttribute(mstrData, RESP_EMVCRYPTOGRAMINFORMATIONDATA)
            mstrEmvCvmResults = ExtractAttribute(mstrData, RESP_EMVCVMRESULTS)
            mstrEmvIssuerApplicationData = ExtractAttribute(mstrData, RESP_EMVISSUERAPPLICATIONDATA)
            mstrEmvTerminalCapabilities = ExtractAttribute(mstrData, RESP_EMVTERMINALCAPABILITIES)
            mstrEmvTerminalCountryCode = ExtractAttribute(mstrData, RESP_EMVTERMINALCOUNTRYCODE)
            mstrEmvTerminalType = ExtractAttribute(mstrData, RESP_EMVTERMINALTYPE)
            mstrFallBackType = ExtractAttribute(mstrData, RESP_FALLBACKTYPE)
            mstrEmvTerminalVerificationResult = ExtractAttribute(mstrData, RESP_EMVTERMINALVERIFICATIONRESULT)
            If Val(mstrFallBackType) = 0 Then
                If Val(Mid(mstrEmvTerminalVerificationResult, 6, 1)) > 0 Then
                    mstrEntryMethod = ENTRY_ICC_PIN_BYPASS
                End If
            End If
            mstrEmvTransactionCurrencyCode = ExtractAttribute(mstrData, RESP_EMVTRANSACTIONCURRENCYCODE)
            mstrEmvTransactionDate = ExtractAttribute(mstrData, RESP_EMVTRANSACTIONDATE)
            mstrEmvTransactionStatusInformation = ExtractAttribute(mstrData, RESP_EMVTRANSACTIONSTATUSINFORMATION)
            mstrEmvTransactionType = ExtractAttribute(mstrData, RESP_EMVTRANSACTIONTYPE)
            mstrEmvUnpredictableNumber = ExtractAttribute(mstrData, RESP_EMVUNPREDICTABLENUMBER)
            mstrSignatureRequired = ExtractAttribute(mstrData, RESP_SIGNATUREREQUIRED)
            mstrCardSequenceNum = ExtractAttribute(mstrData, RESP_CARDSEQUENCENUMBER)
            mstrIACDenial = ExtractIACAttribute(mstrData, RESP_IACDENIAL)
            mstrIACDefault = ExtractIACAttribute(mstrData, RESP_IACDEFAULT)
            mstrIACOnline = ExtractIACAttribute(mstrData, RESP_IACONLINE)
            mstrMerchantID = ExtractAttribute(mstrData, RESP_MERCHANTID)
            mstrAuthNum = strAuthorisationNumber
            If (mstrEmvApplicationLabel <> vbNullString) And (mstrEmvApplicationLabel <> "NOT FOUND") Then
                mstrCardProductName = mstrEmvApplicationLabel
            End If
            
            'if Customer Not Preset and CVV number not valid then report and exit
            If (Val(txtCVV.Text) <> 0) Then
                strPrivateData = Trim(ExtractAttribute(mstrData, RESP_CERTEGYAUTHCODE))
                strPrivateData = Replace(strPrivateData, ",", vbNullString)
                strPrivateData = Replace(strPrivateData, "CV2AVResult=", vbNullString)
                If (Left$(strPrivateData, 1) <> "2") Then
                    Call MsgBoxEx("Entered CVV number is invalid" & vbCrLf & "Authorisation has been declined", vbOKOnly, "Invalid CVV Number")
                    enConfirmationMode = encmSignatureCon
                    enEntryMode = encmAuthCard
                    Call ConfirmPrintedNotOK
                    Exit Sub
                End If
            End If
            
            'Populate Text Boxes on the screen
            Call DisableKeyedEntry
            lblAction.Caption = "Card Details"
            txtCardNo.Text = String$(Len(mstrCreditCardNum) - 4, "X") & Right$(mstrCreditCardNum, 4)
            txtEndMonth.Text = Mid$(mstrExpiryDate, 3, 2)
            txtEndYear.Text = Left$(mstrExpiryDate, 2)
            txtIssueNo.Text = mstrCardSequenceNum
            If strAuthorisationNumber = vbNullString Then
            
                mblnAuthEntered = False
                lblAction.Caption = "Enter Authorisation Number"
                lblStatus.Caption = "Please enter a manual authorisation number"
                fraChequeCard.Enabled = False
                fraChequeType.Enabled = False
                fraBorder.Enabled = True
                fraAuthNum.Enabled = True
                DoEvents
                Call MsgBoxEx("Please phone the card issuer for a manual authorisation number", vbOKOnly, "Authorisation Required", _
                    , , , , RGBMsgBox_WarnColour)
                If (txtAuthNum.Visible = True) And (txtAuthNum.Enabled = True) Then txtAuthNum.SetFocus

                Do
                    DoEvents
                Loop Until mblnAuthEntered = True
                Call DebugMsg(MODULE_NAME, "Transaction", endlDebug, "Auth Num Entered:" & txtAuthNum.Text)
                While Len(txtAuthNum.Text) < 6
                    txtAuthNum.Text = "0" & txtAuthNum.Text
                Wend
                DoEvents
                mstrAuthNum = txtAuthNum.Text
                
            Else
                txtAuthNum.Text = strAuthorisationNumber
            End If

            Wait (1)

            Call DebugMsg(MODULE_NAME, "Transaction", endlDebug, "Successful Transaction - Printing Slip")
'            If (mstrAuthNum <> "000000") Then
                lblAction.Caption = "Printing"
                lblStatus.Caption = "Printing"
                Call PrintSignatureSlip

                lblStatus.Caption = "Transaction Successful"
'            Else
'                lblStatus.Caption = "Transaction Cancelled"
'            End If
            'Added 12/8/05 - to catch 000000 auth numbs and pass them as OK
            If (mstrAuthNum = "000000") Then
                Call DebugMsg(MODULE_NAME, "Transaction", endlDebug, "Approved with a ZERO AuthNumber - adding a . to by-pass Cancel method")
                mstrAuthNum = mstrAuthNum & "."
            End If
            Call DebugMsg(Me.Name, "Transaction", endlDebug, "Completed OK")

        Case RESP_ACTIONCODE_AUTH 'Request requires manual AUTHorisation code

            'Extract information from response
            strActionCode = ExtractAttribute(mstrData, RESP_ACTIONCODE)
            strAuthorisationNumber = ExtractAttribute(mstrData, RESP_AUTHNUM)
            mstrCreditCardNum = ExtractAttribute(mstrData, RESP_CARDNUMBER)
            mstrCardProductName = ExtractAttribute(mstrData, RESP_CARDPRODUCTNAME)
            strDateTime = ExtractAttribute(mstrData, RESP_DATETIME)
            mstrExpiryDate = ExtractAttribute(mstrData, RESP_EXPIRYDATE)
            mstrPanEntryMode = ExtractAttribute(mstrData, RESP_PANENTRYMODE)
            mstrPosCondition = ExtractAttribute(mstrData, RESP_POSCONDITION)
            strResponseCode = ExtractAttribute(mstrData, RESP_RESPONSECODE)
            strMessageReasonCode = ExtractAttribute(mstrData, RESP_MESSAGEREASONCODE)
            'WIX1193-check if comms down
            If (CheckForCommsFailure(strResponseCode, strMessageReasonCode) = True) Then
                strCommFailedMsg = "C n P Server Unavailable" & vbNewLine
            End If
            strServiceRestrictionCode = ExtractAttribute(mstrData, RESP_SERVICERESTRICTIONCODE)
            mstrTrack2 = ExtractAttribute(mstrData, RESP_TRACK2)
            mstrEmvAmount = ExtractAttribute(mstrData, RESP_EMVAMOUNT)
            mstrEmvAmountOther = ExtractAttribute(mstrData, RESP_EMVAMOUNTOTHER)
            mstrEmvApplicationIdentifier = ExtractAttribute(mstrData, RESP_EMVAPPLICATIONIDENTIFIER)
            mstrEmvApplicationLabel = ExtractAttribute(mstrData, RESP_EMVAPPLICATIONLABEL)
            mstrEmvApplicationInterchangeProfile = ExtractAttribute(mstrData, RESP_EMVAPPLICATIONINTERCHANGEPROFILE)
            mstrEmvApplicationTransactionCounter = ExtractAttribute(mstrData, RESP_EMVAPPLICATIONTRANSACTIONCOUNTER)
            mstrEmvApplicationUsageControl = ExtractAttribute(mstrData, RESP_EMVAPPLICATIONUSAGECONTROL)
            mstrEmvApplicationVersionNumber = ExtractAttribute(mstrData, RESP_EMVAPPLICATIONVERSIONNUMBER)
            mstrEmvAuthorizationResponseCode = ExtractAttribute(mstrData, RESP_EMVAUTHORIZATIONRESPONSECODE)
            mstrEmvCryptogram = ExtractAttribute(mstrData, RESP_EMVCRYPTOGRAM)
            mstrEmvCryptogramInformationData = ExtractAttribute(mstrData, RESP_EMVCRYPTOGRAMINFORMATIONDATA)
            mstrEmvCvmResults = ExtractAttribute(mstrData, RESP_EMVCVMRESULTS)
            mstrEmvIssuerApplicationData = ExtractAttribute(mstrData, RESP_EMVISSUERAPPLICATIONDATA)
            mstrEmvTerminalCapabilities = ExtractAttribute(mstrData, RESP_EMVTERMINALCAPABILITIES)
            mstrEmvTerminalCountryCode = ExtractAttribute(mstrData, RESP_EMVTERMINALCOUNTRYCODE)
            mstrEmvTerminalType = ExtractAttribute(mstrData, RESP_EMVTERMINALTYPE)
            mstrEmvTerminalVerificationResult = ExtractAttribute(mstrData, RESP_EMVTERMINALVERIFICATIONRESULT)
            If Val(Mid(mstrEmvTerminalVerificationResult, 6, 1)) > 0 Then mstrEntryMethod = ENTRY_ICC_PIN_BYPASS
            mstrEmvTransactionCurrencyCode = ExtractAttribute(mstrData, RESP_EMVTRANSACTIONCURRENCYCODE)
            mstrEmvTransactionDate = ExtractAttribute(mstrData, RESP_EMVTRANSACTIONDATE)
            mstrEmvTransactionStatusInformation = ExtractAttribute(mstrData, RESP_EMVTRANSACTIONSTATUSINFORMATION)
            mstrEmvTransactionType = ExtractAttribute(mstrData, RESP_EMVTRANSACTIONTYPE)
            mstrEmvUnpredictableNumber = ExtractAttribute(mstrData, RESP_EMVUNPREDICTABLENUMBER)
            mstrSignatureRequired = ExtractAttribute(mstrData, RESP_SIGNATUREREQUIRED)
            mstrFallBackType = ExtractIACAttribute(mstrData, RESP_FALLBACKTYPE)
            mstrCardSequenceNum = ExtractAttribute(mstrData, RESP_CARDSEQUENCENUMBER)
            mstrIACDenial = ExtractIACAttribute(mstrData, RESP_IACDENIAL)
            mstrIACDefault = ExtractIACAttribute(mstrData, RESP_IACDEFAULT)
            mstrIACOnline = ExtractIACAttribute(mstrData, RESP_IACONLINE)
            mstrAuthNum = strAuthorisationNumber
            If mstrEmvApplicationLabel <> vbNullString Then
                mstrCardProductName = mstrEmvApplicationLabel
            End If
            'Populate Text Boxes on the screen
            Call DisableKeyedEntry
            lblAction.Caption = "Card Details"
            'txtCardNo.Text = mstrCreditCardNum
            txtCardNo.Text = String$(Len(mstrCreditCardNum) - 4, "X") & Right$(mstrCreditCardNum, 4)
            txtEndMonth.Text = Mid$(mstrExpiryDate, 3, 2)
            txtEndYear.Text = Left$(mstrExpiryDate, 2)
            txtIssueNo.Text = mstrCardSequenceNum


            mblnAuthEntered = False
            lblAction.Caption = "Enter Authorisation Number"
            lblStatus.Caption = "Please enter a manual authorisation number"
            fraChequeCard.Enabled = False
            fraBorder.Enabled = True
            fraAuthNum.Enabled = True
            DoEvents
            Call MsgBoxEx(strCommFailedMsg & "Please phone the card issuer for a manual authorisation number", vbOKOnly, "Authorisation Required", _
                , , , , RGBMsgBox_WarnColour)
            If (txtAuthNum.Visible = True) And (txtAuthNum.Enabled = True) Then txtAuthNum.SetFocus

            Do
                DoEvents
            Loop Until mblnAuthEntered = True

            Wait (1)
            
            While Len(txtAuthNum.Text) < 6
                txtAuthNum.Text = "0" & txtAuthNum.Text
            Wend
            DoEvents
            mstrAuthNum = txtAuthNum.Text

            If (mstrAuthNum <> "000000") Then
                lblAction.Caption = "Printing"
                lblStatus.Caption = "Printing"
                Call PrintSignatureSlip
    
                lblStatus.Caption = "Transaction Successful"
            Else
                lblStatus.Caption = "Transaction Cancelled"
            End If


        Case Else
            strResponseCode = ExtractAttribute(mstrData, RESP_RESPONSECODE)
            strMessageReasonCode = ExtractAttribute(mstrData, RESP_MESSAGEREASONCODE)
            'WIX1193-check if comms down
            If (CheckForCommsFailure(strResponseCode, strMessageReasonCode) = True) Then
                strCommFailedMsg = "C n P Server Unavailable-Please phone the card issuer for a manual authorisation number"
            End If

            Call MsgBoxEx("WARNING: The Transaction has been declined" & vbCrLf & strCommFailedMsg & _
                "Response code : " & strResponseCode & " - " & ResponseMessage(strResponseCode) & _
                vbCrLf & "Message Reason Code: " & strMessageReasonCode & " - " & MessageReasonCode(strMessageReasonCode), _
                 vbOKOnly, "Warning Transaction failed", , , , , RGBMsgBox_WarnColour)
            mlngAttemptNum = mlngAttemptNum + 1
            mstrAuthNum = vbNullString
            Call Me.Hide

        End Select
        Call DebugMsg(MODULE_NAME, "Transaction", endlDebug, mstrEmvApplicationLabel & "#" & mstrCardProductName)

        Call DebugMsg(Me.Name, "Transaction", endlTraceOut)
        
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("Transaction", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub 'Transaction

Private Function CheckForCommsFailure(ByVal strReasonCode As String, ByVal strMessageReasonCode As String) As Boolean

Dim strReasonCodes As String
Dim strMessageReasonCodes As String

    'Get list of Failed codes from Parameter Table
    strReasonCodes = "," & goSession.GetParameter(PRM_RC_COMMS_FAILED) & ","
    strMessageReasonCodes = "," & goSession.GetParameter(PRM_MRC_COMMS_FAILED) & ","
    
    'check if any Reason Codes set up and if not then exit
    If (strReasonCodes = ",,") And (strMessageReasonCodes = ",,") Then Exit Function

    'prefix and suffix comma to split up values
    strReasonCode = "," & strReasonCode & ","
    strMessageReasonCode = "," & strMessageReasonCode & ","
    If (InStr(strReasonCodes, strReasonCode) > 0) Then
        If (InStr(strMessageReasonCodes, strMessageReasonCode) > 0) Then
            CheckForCommsFailure = True
        End If
    End If
        
End Function

Private Sub txtAccountNo_GotFocus()
    
    txtAccountNo.SelStart = 0
    txtAccountNo.SelLength = Len(txtAccountNo.Text)
    txtAccountNo.BackColor = RGBEdit_Colour

End Sub 'txtAccountNo_GotFocus

Private Sub txtAccountNo_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        If Len(txtAccountNo.Text) < 7 Then
            Call MsgBoxEx("Account number must be at least 7 digits" & vbCrLf & "Enter valid account number", vbOKOnly, "Invalid Details Entered", , , , , RGBMsgBox_WarnColour)
            Exit Sub
        End If
        If Len(txtChequeNo.Text) < 6 Then
            Call MsgBoxEx("Cheque number must be at least 6 digits" & vbCrLf & "Enter valid account number", vbOKOnly, "Invalid Details Entered", , , , , RGBMsgBox_WarnColour)
            Exit Sub
        End If
        If Len(txtSortCode.Text) < 6 Then
            Call MsgBoxEx("Sortcode number must be at least 6 digits" & vbCrLf & "Enter valid account number", vbOKOnly, "Invalid Details Entered", , , , , RGBMsgBox_WarnColour)
            Exit Sub
        End If
        If txtAccountNo.Text <> vbNullString Then
            If mstrChequeType = CHEQUE_COMP Then
               enConfirmationMode = encmSignatureCon
               mstrChequeNo = txtChequeNo.Text
               mstrSortCode = txtSortCode.Text
               mstrAccountNo = txtAccountNo.Text
               enEntryMode = encmCheque
               Call AuthoriseCheque
               
            Else
               Me.Hide
                
'                Call CaptureCardDetails
'                fraAuthNum.Enabled = False
'                txtAuthNum.BackColor = RGB_LTYELLOW
'                fraAuthNum.Visible = False
'                DoEvents
'                Call ResizeFormChequeMode
'
'                '** Changed 11/7/05 - force Cheque Gaurentee to 1p to force online
'                '** Authorisation of all cheques
'
'                'Request chegue guarantee amount
'                'mstrChequeGuaranteeValue = InputBoxEx("Please enter the cheque guarantee amount", _
'                        "Cheque Guarantee Amount", "50", enifNumbers, , RGBMSGBox_PromptColour, , 4)
'                mstrChequeGuaranteeValue = "0.01"
'
'                If Val(mstrChequeGuaranteeValue) > 0 Then
'
'                    mstrChequeNo = txtChequeNo.Text
'                    mstrSortCode = txtSortCode.Text
'                    mstrAccountNo = txtAccountNo.Text
'                    enEntryMode = encmCheque
'                    lblStatus.Visible = True
'                    lblStatus.Caption = "Please enter the cheque guarantee card details"
'                    txtCardNo.SetFocus
'                    'Call Cheque routine
'                    If Val(mstrTransactionAmount) > (Val(mstrChequeGuaranteeValue) * 100) Then
'                        Call AuthoriseCheque
'                    Else
'                        Call ChequeWithOutAuth
'                    End If
'                Else
'                    Call Me.Hide
'                End If
            End If
        Else
            Call MsgBoxEx("Unable to continue a cheque transaction without an account number", vbOKOnly, _
                "Unable to contiune", , , , , RGBMsgBox_WarnColour)
            If (txtAccountNo.Visible = True) And (txtAccountNo.Enabled = True) Then txtAccountNo.SetFocus
        End If
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")
    If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0

End Sub 'txtAccountNo_KeyPress

Private Sub txtAccountNo_LostFocus()

    txtAccountNo.BackColor = RGB_WHITE

End Sub 'txtAccountNo_LostFocus

Private Sub txtAuthNum_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        'This is what happens if a manual authorisation is accepted
        If Len(txtAuthNum.Text) > 0 Then
            If mstrChequeType <> vbNullString Then
                If Val(txtAuthNum.Text) = 4 Then
                    Call MsgBoxEx("WARNING: Invalid authorisation number", vbOKOnly, "Invalid Auth Number", _
                        , , , , RGBMsgBox_WarnColour)
                    mblnAuthEntered = False
                    Exit Sub
                End If
            End If
            If (mstrChequeType <> vbNullString) And (txtCVV.Enabled = True) And (Val(Trim(txtCVV.Text)) = 0) Then
                Call MsgBoxEx("WARNING: Invalid CVV number (must be the 3 Digit Number from the back of the Card)", vbOKOnly, "Invalid CVV Number", _
                        , , , , RGBMsgBox_WarnColour)
                mblnAuthEntered = False
                Exit Sub
            End If
            If (txtCVV.Enabled = True) And (Trim(txtCVV.Text) <> "") Then
                Call txtCustPresent_KeyPress(vbKeyReturn)
                If (fraBorder.Enabled = True) Then Exit Sub
            End If
            
            mblnAuthEntered = True
            mstrAuthNum = txtAuthNum.Text
            txtAuthNum.BackColor = RGB_LTYELLOW
            fraAuthNum.Enabled = False
            fraBorder.Enabled = False
            lblStatus.Caption = "Sending Confirmation"
            
            'Release control back to loop in tmrESocket
            DoEvents
'            If (mblnCNPTill = True) Then Call Me.Hide
        End If
    End If
    
    If (KeyAscii = vbKeyEscape) Then
        'This is what happens if a manual authorisation is declined
        mlngAttemptNum = mlngAttemptNum + 1
        mstrAuthNum = vbNullString
        txtChequeNo.Text = vbNullString
        txtSortCode.Text = vbNullString
        txtAccountNo.Text = vbNullString
        txtAuthNum.Text = vbNullString
        txtEndMonth.Text = vbNullString
        txtStartMonth.Text = vbNullString
        txtStartYear.Text = vbNullString
        txtEndYear.Text = vbNullString
        txtCardNo.Text = vbNullString
        txtIssueNo.Text = vbNullString
        mblnCustPresent = False
        txtCustPresent.Text = "Y"
        mstrAuthNum = vbNullString
        mstrTrack2 = vbNullString
        Call Me.Hide
    End If

End Sub 'txtAuthNum_KeyPress
Private Sub txtAuthNum_GotFocus()

    txtAuthNum.BackColor = RGBEdit_Colour

End Sub 'txtAuthNum_GotFocus

Private Sub txtCardNo_GotFocus()

    txtCardNo.SelStart = 0
    txtCardNo.SelLength = Len(txtCardNo.Text)
    txtCardNo.BackColor = RGBEdit_Colour

End Sub 'txtCardNo_GotFocus

Private Sub txtCardNo_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        mblnCardDetailsEntered = True
        mstrAuthNum = vbNullString
        txtAccountNo.Text = vbNullString
        txtChequeNo.Text = vbNullString
        txtSortCode.Text = vbNullString
        Unload Me
'        Call Me.Hide
    End If
    Debug.Print time$, KeyAscii, Chr$(KeyAscii)
    
    If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0
    
End Sub 'txtCardNo_KeyPress

Private Sub txtCardNo_LostFocus()

        txtCardNo.BackColor = RGB_WHITE
    
End Sub 'txtCardNo_LostFocus

Private Sub txtChequeNo_GotFocus()
    
    txtChequeNo.SelStart = 0
    txtChequeNo.SelLength = Len(txtChequeNo.Text)
    txtChequeNo.BackColor = RGBEdit_Colour

End Sub 'txtChequeNo_GotFocus

Private Sub txtChequeNo_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        If Val(txtChequeNo.Text) > 0 Then
            While Len(txtChequeNo.Text) < 6
                txtChequeNo.Text = "0" & txtChequeNo.Text
            Wend
            Call SendKeys(vbTab)
        End If
    End If
    If (KeyAscii = vbKeyEscape) Then
        mstrAuthNum = vbNullString
        txtAccountNo.Text = vbNullString
        txtChequeNo.Text = vbNullString
        txtSortCode.Text = vbNullString
        Call Me.Hide
        Me.Height = Me.Height - fraAccountNo.Height
    End If
    If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0
    
End Sub 'txtChequeNo_KeyPress

Private Sub txtChequeNo_LostFocus()

    txtChequeNo.BackColor = RGB_WHITE

End Sub 'txtChequeNo_LostFocus

Private Sub txtEndMonth_GotFocus()
    
    txtEndMonth.SelStart = 0
    txtEndMonth.SelLength = Len(txtEndMonth.Text)
    txtEndMonth.BackColor = RGBEdit_Colour

End Sub 'txtEndMonth_GotFocus

Private Sub txtEndMonth_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub 'txtEndMonth_KeyPress

Private Sub txtEndMonth_LostFocus()

    txtEndMonth.BackColor = RGB_WHITE
    txtEndMonth.Text = Format$(Val(txtEndMonth.Text), "00")
    If (Val(txtEndMonth.Text) <= 0) Or (Val(txtEndMonth.Text) > 12) Then txtEndMonth.Text = "--"

End Sub 'txtEndMonth_LostFocus

Private Sub txtEndYear_GotFocus()
    
    txtEndYear.SelStart = 0
    txtEndYear.SelLength = Len(txtEndYear.Text)
    txtEndYear.BackColor = RGBEdit_Colour

End Sub 'txtEndYear_GotFocus

Private Sub txtEndYear_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub 'txtEndYear_KeyPress

Private Sub txtEndYear_LostFocus()

    txtEndYear.BackColor = RGB_WHITE
    txtEndYear.Text = Format$(Val(txtEndYear.Text), "00")
    If (Val(txtEndYear.Text) <= 0) Or (Val(txtEndYear.Text) > 99) Then txtEndYear.Text = "--"

End Sub 'txtEndYear_LostFocus

Private Sub txtCustPresent_GotFocus()

    txtCustPresent.SelStart = 0
    txtCustPresent.SelLength = Len(txtCustPresent.Text)
    txtCustPresent.BackColor = RGBEdit_Colour
 
End Sub 'txtCustPresent_GotFocus

Private Sub txtIssueNo_GotFocus()
    
    txtIssueNo.SelStart = 0
    txtIssueNo.SelLength = Len(txtIssueNo.Text)
    txtIssueNo.BackColor = RGBEdit_Colour

End Sub

Private Sub txtIssueNo_LostFocus()

    If LenB(txtIssueNo.Text) = 0 Then txtIssueNo.Text = "000"
    txtIssueNo.Text = Format$(txtIssueNo.Text, "000")

    txtIssueNo.BackColor = RGB_WHITE

End Sub

Private Sub txtCustPresent_KeyPress(KeyAscii As Integer)
    
Dim strSql          As String
Dim strDSN          As String

    strDSN = goDatabase.ConnectionString

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        mblnCardDetailsEntered = False
        DoEvents
        mstrExpiryDate = txtEndYear.Text & txtEndMonth.Text
        If ValidateExpiryDate(mstrExpiryDate) = False Then
            Call MsgBoxEx("Please enter a valid expiry date", vbOKOnly, "Invalid Expiry Date", , , , , RGBMsgBox_WarnColour)
            If (txtCardNo.Visible = True) And (txtCardNo.Enabled = True) Then txtCardNo.SetFocus
            Exit Sub
        End If
        If (Trim$(txtCVV.Text) = "") And (txtCVV.Enabled = True) Then
            Call MsgBoxEx("Please enter a valid CVV number from the Card", vbOKOnly, "Invalid CVV", , , , , RGBMsgBox_WarnColour)
            If (txtCVV.Visible = True) And (txtCVV.Enabled = True) Then txtCVV.SetFocus
            Exit Sub
        End If

        KeyAscii = 0
        mblnCardDetailsEntered = True
        fraBorder.Enabled = False
        
        mstrStartDate = txtStartYear.Text & txtStartMonth.Text
        mstrExpiryDate = txtEndYear.Text & txtEndMonth.Text
        mstrCreditCardNum = txtCardNo.Text
        mstrCardSequenceNum = IIf(Val(txtIssueNo.Text) = 0, "", txtIssueNo.Text)
        If txtCustPresent = "Y" Then
            mblnCustPresent = True
        Else
            mblnCustPresent = False
        End If

        If (Trim$(txtCVV.Text) <> "") Then
            mstrEntryMethod = ENTRY_KEY
            tmrESocket.Interval = 1 'if CNP Till then switch on Authorisation
            tmrESocket.Enabled = True
        End If

        'Release control back to loop in tmrESocket
        DoEvents
    End If

    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub txtIssueNo_KeyPress(KeyAscii As Integer)

    If (mblnCNPTill = False) Then fraAuthNum.Enabled = False

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub


Private Sub txtSortCode_GotFocus()
    
    txtSortCode.SelStart = 0
    txtSortCode.SelLength = Len(txtSortCode.Text)
    txtSortCode.BackColor = RGBEdit_Colour

End Sub

Private Sub txtSortCode_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        If Len(txtSortCode.Text) < 6 Then
            Call MsgBoxEx("The Sort Code must be six characters long.", vbOKOnly, "Incorrect SortCode", , , , , RGBMsgBox_WarnColour)
        Else
            Call SendKeys(vbTab)
        End If
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")
    If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0

End Sub

Private Sub txtSortCode_LostFocus()

    txtSortCode.BackColor = RGB_WHITE

End Sub

Private Sub txtStartMonth_GotFocus()
    
    txtStartMonth.SelStart = 0
    txtStartMonth.SelLength = Len(txtStartMonth.Text)
    txtStartMonth.BackColor = RGBEdit_Colour

End Sub

Private Sub txtStartMonth_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")
If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0

End Sub

Private Sub txtStartMonth_LostFocus()

    txtStartMonth.BackColor = RGB_WHITE
    txtStartMonth.Text = Format$(Val(txtStartMonth.Text), "00")
    If (Val(txtStartMonth.Text) <= 0) Or (Val(txtStartMonth.Text) > 12) Then txtStartMonth.Text = "--"

End Sub

Private Sub txtStartYear_GotFocus()

    txtStartYear.SelStart = 0
    txtStartYear.SelLength = Len(txtStartYear.Text)
    txtStartYear.BackColor = RGBEdit_Colour

End Sub

Private Sub txtStartYear_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub txtStartYear_LostFocus()

    txtStartYear.BackColor = RGB_WHITE
    txtStartYear.Text = Format$(Val(txtStartYear.Text), "00")
    If (Val(txtStartYear.Text) <= 0) Or (Val(txtStartYear.Text) > 99) Then txtStartYear.Text = "--"

End Sub

Public Sub PrintSignatureSlip()

Dim dblAmount   As Double
Dim strVerified As String
    
    Call DebugMsg(Me.Name, "PrintSignatureSlip", endlTraceIn)
    dblAmount = mstrTransactionAmount / 100
    
    If mstrSignatureRequired = "TRUE" Then
        If Val(mstrFallBackType) = "1" Then
            mstrEntryMethod = "Swipe Fallback"
        Else
            If mstrEntryMethod <> ENTRY_ICC_PIN_BYPASS Then
                Select Case mstrPanEntryMode
                    Case "01"
                        mstrEntryMethod = ENTRY_KEY
                    Case "02"
                        mstrEntryMethod = ENTRY_SWIPE
                    Case "05"
                        mstrEntryMethod = ENTRY_ICCP
                    Case "90"
                        mstrEntryMethod = ENTRY_SWIPE
                    Case "95"
                        mstrEntryMethod = ENTRY_ICCP
                End Select
                'Extract the Verification method
                Select Case Right(mstrEmvCvmResults, 1)
                    Case 0: mstrEntryMethod = mstrEntryMethod & " Signature Verified"
                    Case 1: mstrEntryMethod = mstrEntryMethod & " PIN Verified"
                    Case 2: mstrEntryMethod = mstrEntryMethod & " PIN Verified"
                    Case 3: mstrEntryMethod = mstrEntryMethod & " PIN and Signature Verified"
                    Case 4: mstrEntryMethod = mstrEntryMethod & " PIN Verified"
                    Case 5: mstrEntryMethod = mstrEntryMethod & " PIN and Signature Verified"
                End Select
                
            End If
        End If

        Call modReceipt.PrintTransactionHeader(moOPOSPrinter, _
        moTranHeader.TranDate, moTranHeader.TransactionTime, moTranHeader.TillID, _
        moTranHeader.TransactionNo, moTOSType.Description, _
        moTranHeader.CashierID, mstrCashierName, _
        vbNullString)

        Call PrintCreditCardDetails(moOPOSPrinter, mstrCardProductName, mstrCreditCardNum, _
            mstrStartDate, mstrExpiryDate, dblAmount, mstrEntryMethod, mstrAuthNum, mstrCardSequenceNum, False, _
            mstrEmvTerminalVerificationResult, mstrEmvTransactionStatusInformation, mstrEmvAuthorizationResponseCode, _
            mstrEmvUnpredictableNumber, mstrEmvApplicationIdentifier, mstrTID, mstrMerchantID, mstrEmvCryptogram, _
            mstrChequeType, mstrIACDefault, mstrIACDenial, mstrIACOnline, mstrEmvCvmResults)
        
        Call modReceipt.PrintCreditCardSlip(moOPOSPrinter, True, mstrAuthNum, _
            mstrCardProductName, mstrCreditCardNum, mstrExpiryDate, dblAmount, mblnCNPTill)
    Else

        Select Case mstrPanEntryMode
            Case "05"

                If mstrSignatureRequired <> "TRUE" Then 'Signature slip not required
                    If Val(mstrFallBackType) = "1" Then
                        mstrEntryMethod = "Swipe Fallback"
                    Else
                    
                        If mstrEntryMethod <> ENTRY_ICC_PIN_BYPASS Then
                           
                            Select Case mstrPanEntryMode
                                Case "01"
                                    mstrEntryMethod = ENTRY_KEY
                                Case "02"
                                    mstrEntryMethod = ENTRY_SWIPE
                                Case "05"
                                    mstrEntryMethod = ENTRY_ICCP
                                Case "90"
                                    mstrEntryMethod = ENTRY_SWIPE
                                Case "95"
                                    mstrEntryMethod = ENTRY_ICCP
                            End Select
                            'Extract the Verification method
                            Select Case Right(mstrEmvCvmResults, 1)
                                Case 0: mstrEntryMethod = mstrEntryMethod & " Signature Verified"
                                Case 1: mstrEntryMethod = mstrEntryMethod & " PIN Verified"
                                Case 2: mstrEntryMethod = mstrEntryMethod & " PIN Verified"
                                Case 3: mstrEntryMethod = mstrEntryMethod & " PIN and Signature Verified"
                                Case 4: mstrEntryMethod = mstrEntryMethod & " PIN Verified"
                                Case 5: mstrEntryMethod = mstrEntryMethod & " PIN and Signature Verified"
                            End Select
                        End If
                    End If
                    If (goSession.GetParameter(PRM_SHOW_EFT_DATA) = True) Then
                        Call modReceipt.PrintTransactionHeader(moOPOSPrinter, _
                        moTranHeader.TranDate, moTranHeader.TransactionTime, moTranHeader.TillID, _
                        moTranHeader.TransactionNo, moTOSType.Description, _
                        moTranHeader.CashierID, mstrCashierName, _
                        vbNullString)
    
                        Call PrintCreditCardDetails(moOPOSPrinter, mstrCardProductName, mstrCreditCardNum, _
                            mstrStartDate, mstrExpiryDate, dblAmount, mstrEntryMethod, mstrAuthNum, mstrCardSequenceNum, False, _
                            mstrEmvTerminalVerificationResult, mstrEmvTransactionStatusInformation, mstrEmvAuthorizationResponseCode, _
                            mstrEmvUnpredictableNumber, mstrEmvApplicationIdentifier, mstrTID, mstrMerchantID, mstrEmvCryptogram, _
                            mstrChequeType, mstrIACDefault, mstrIACDenial, mstrIACOnline, mstrEmvCvmResults)
    
                        Call modReceipt.PrintCreditCardSlip(moOPOSPrinter, False, mstrAuthNum, _
                            mstrCardProductName, mstrCreditCardNum, mstrExpiryDate, dblAmount, False)
                    End If 'print extra slip
        
                    enEntryMode = encmAuthCard
                    enConfirmationMode = encmSignatureCon
                    Call ConfirmPrintedOk
                    DoEvents
                    Call DebugMsg(MODULE_NAME, "PrintSignatureSlip", endlDebug, "Exiting Auth")
                    Exit Sub
                End If 'Signature slip not required

            Case Else

                    If Val(mstrFallBackType) = "1" Then
                        mstrEntryMethod = "Swipe Fallback"
                    Else
                    
                        If mstrEntryMethod <> ENTRY_ICC_PIN_BYPASS Then
                           
                            Select Case mstrPanEntryMode
                                Case "01"
                                    mstrEntryMethod = ENTRY_KEY
                                Case "02"
                                    mstrEntryMethod = ENTRY_SWIPE
                                Case "05"
                                    mstrEntryMethod = ENTRY_ICCP
                                Case "90"
                                    mstrEntryMethod = ENTRY_SWIPE
                                Case "95"
                                    mstrEntryMethod = ENTRY_ICCP
                            End Select
                            'Extract the Verification method
                            Select Case Right(mstrEmvCvmResults, 1)
                                Case 0: mstrEntryMethod = mstrEntryMethod & " Signature Verified"
                                Case 1: mstrEntryMethod = mstrEntryMethod & " PIN Verified"
                                Case 2: mstrEntryMethod = mstrEntryMethod & " PIN Verified"
                                Case 3: mstrEntryMethod = mstrEntryMethod & " PIN and Signature Verified"
                                Case 4: mstrEntryMethod = mstrEntryMethod & " PIN Verified"
                                Case 5: mstrEntryMethod = mstrEntryMethod & " PIN and Signature Verified"
                            End Select
                        End If
                    End If
                    Call modReceipt.PrintTransactionHeader(moOPOSPrinter, _
                            moTranHeader.TranDate, moTranHeader.TransactionTime, moTranHeader.TillID, _
                            moTranHeader.TransactionNo, moTOSType.Description, _
                            moTranHeader.CashierID, mstrCashierName, _
                            vbNullString)

                    Call PrintCreditCardDetails(moOPOSPrinter, mstrCardProductName, mstrCreditCardNum, _
                        mstrStartDate, mstrExpiryDate, dblAmount, mstrEntryMethod, mstrAuthNum, mstrCardSequenceNum, False, _
                        vbNullString, vbNullString, vbNullString, vbNullString, vbNullString, mstrTID, mstrMerchantID, vbNullString, _
                        mstrChequeType, mstrIACDefault, mstrIACDenial, mstrIACOnline, mstrEmvCvmResults)

                    Call modReceipt.PrintCreditCardSlip(moOPOSPrinter, True, mstrAuthNum, _
                        mstrCardProductName, mstrCreditCardNum, mstrExpiryDate, dblAmount, mblnCNPTill)

        End Select
    End If

    enConfirmationMode = encmReprintSlip
    If (mblnCNPTill = True) Then
        If MsgBoxEx("Did the Customer Not Present Slip print ok?", vbYesNo, "Customer Not Present Slip", , , , , RGBMSGBox_PromptColour) = vbYes Then
            Call ConfirmPrintedOk
        Else
            Call ConfirmPrintedNotOK
        End If
    Else
        If MsgBoxEx("Did the Signature Slip print ok?", vbYesNo, "Signature Slip", , , , , RGBMSGBox_PromptColour) = vbYes Then
            Call ConfirmPrintedOk
        Else
            Call ConfirmPrintedNotOK
        End If
    End If
    
End Sub 'PrintSignatureSlip

Private Sub PrintCreditCardreceipt()

Dim dblAmount As Double
Dim strStage  As String
    
    On Error GoTo Err_Print_CCReceipt
    
    strStage = "Starting"
    dblAmount = Val(mstrTransactionAmount) / 100
    
    strStage = "PrintHeader-" & mstrCashierName
    Call modReceipt.PrintTransactionHeader(moOPOSPrinter, _
        moTranHeader.TranDate, moTranHeader.TransactionTime, moTranHeader.TillID, _
        moTranHeader.TransactionNo, moTOSType.Description, _
        moTranHeader.CashierID, mstrCashierName, _
        vbNullString)

    lblStatus.Caption = "Printing Receipt-Details"
    DoEvents
    strStage = "PrintDetails-" & mstrCardProductName
    Call PrintCreditCardDetails(moOPOSPrinter, mstrCardProductName, mstrCreditCardNum, _
            mstrStartDate, mstrExpiryDate, dblAmount, mstrEntryMethod, mstrAuthNum, mstrCardSequenceNum, True, _
            mstrTerminalVerificationResult, mstrTransactionStatusInfo, mstrAuthResponceCode, mstrUnpredictableNumber, _
            mstrApplicationIdentifier, mstrTID, mstrMerchantID, mstrEmvCryptogram, mstrChequeType, mstrIACDefault, _
            mstrIACDenial, mstrIACOnline, mstrEmvCvmResults)
    
    lblStatus.Caption = "Printing Receipt-Slip"
    DoEvents
    strStage = "PrintingSlip-" & mstrCardProductName
    Call modReceipt.PrintCreditCardSlip(moOPOSPrinter, False, mstrAuthNum, _
        mstrCardProductName, mstrCreditCardNum, mstrExpiryDate, dblAmount, False)
        
    Exit Sub

Err_Print_CCReceipt:

    Call MsgBoxEx("WARNING : An error has occurred when printing Credit Card Receipt - system will attempt to continue to complete transaction", vbOKOnly, "Print Error")
    Call Err.Report(MODULE_NAME, "PrintCreditCardReceipt-" & strStage, 1, False)
    Call Err.Clear

End Sub 'PrintCreditCardreceipt

Private Sub DisableKeyedEntry()

    fraBorder.Enabled = False
    DoEvents
    txtEndMonth.BackColor = RGB_LTYELLOW
    txtEndYear.BackColor = RGB_LTYELLOW
    txtStartMonth.BackColor = RGB_LTYELLOW
    txtStartYear.BackColor = RGB_LTYELLOW
    txtCardNo.BackColor = RGB_LTYELLOW
    txtIssueNo.BackColor = RGB_LTYELLOW
    txtCustPresent.BackColor = RGB_LTYELLOW

End Sub 'DisableKeyedEntry

Private Sub EnableKeyedEntry()

    txtEndMonth.BackColor = RGB_WHITE
    txtEndYear.BackColor = RGB_WHITE
    txtStartMonth.BackColor = RGB_WHITE
    txtStartYear.BackColor = RGB_WHITE
    txtCardNo.BackColor = RGB_WHITE
    txtIssueNo.BackColor = RGB_WHITE
    txtCustPresent.BackColor = RGB_WHITE
    fraBorder.Enabled = True
    DoEvents

End Sub 'EnableKeyedEntry

Public Function ConfirmCardRead() As Boolean

    txtEndMonth.Text = Format$(Val(txtEndMonth.Text), "00")
    If Val(txtStartMonth.Text) < 0 Then
        txtStartMonth.Text = Format$(Val(txtStartMonth.Text), "00")
        Exit Function
    End If
        
    If LenB(txtCardNo.Text) = 0 Then
        Call MsgBoxEx("Invalid card detected: No Credit card number" & vbCrLf & vbCrLf & "Please select an alternative tender type ", vbOKOnly, "Warning: No Card number", , , , , RGBMsgBox_WarnColour)
        Exit Function
    End If
    
    If IsNumeric(txtCardNo.Text) = False Then
        Call MsgBoxEx("Invalid card detected: Invalid Credit card number" & vbCrLf & vbCrLf & "Please select an alternative tender type ", vbOKOnly, "Warning: Invalid Card number", , , , , RGBMsgBox_WarnColour)
        Exit Function
    End If
    
    If Val(txtEndYear.Text & txtEndMonth.Text) > 0 Then
        
        If Val(txtEndYear.Text & txtEndMonth.Text) < Format$(Date, "YYMM") Then
            Call MsgBoxEx("Invalid card detected: Card has expired" & vbCrLf & vbCrLf & "Please select an alternative tender type ", vbOKOnly, "Warning: Card Expired", , , , , RGBMsgBox_WarnColour)
            Exit Function
        End If
        
        If (txtStartYear.Text & txtStartMonth.Text) > (txtEndYear & txtEndMonth) Then
            Call MsgBoxEx("Invalid card detected: Start date is after expiry date" & vbCrLf & vbCrLf & "Please select an alternative tender type ", vbOKOnly, "Warning: Bad start date", , , , , RGBMsgBox_WarnColour)
            Exit Function
        End If
    End If
    
    ConfirmCardRead = True
    
End Function 'ConfirmCardRead

Private Sub ResizeFormTestMode()

'Resizes the form accordingly when the authorisation server is set to test mode

    fraBorder.Height = fraChequeCard.Height + fraAuthNum.Height + 12
    fraBorder.Top = lblAction.Top + lblAction.Height + lblAuthTranMod.Height + 220
    lblStatus.Top = fraBorder.Top + fraBorder.Height + 120
    Me.Height = lblAction.Top + lblAction.Height + lblAuthTranMod.Height + lblStatus.Height + fraBorder.Height + 850 + sbStatus.Height
    DoEvents

End Sub 'ResizeFormTestMode

Private Sub ResizeFormLiveMode()

'Resizes the form accordingly when the authorisation server is set to live mode

    fraBorder.Height = fraChequeCard.Height + fraAuthNum.Height + 12
    fraBorder.Top = lblAction.Top + lblAction.Height + 120
    lblStatus.Top = fraBorder.Top + fraBorder.Height + 120
    Me.Height = lblAction.Top + lblAction.Height + lblStatus.Height + fraBorder.Height + 850 + sbStatus.Height
    DoEvents

End Sub 'ResizeFormLiveMode

Private Sub ResizeFormChequeMode()

'Resizes the form accordingly when the authorisation server is set to live mode

    fraBorder.Height = fraChequeCard.Height + fraAuthNum.Height + 12
    fraBorder.Top = lblAction.Top + lblAction.Height + 120
    fraChequeCard.Top = 120
    fraAuthNum.Top = fraChequeCard.Height - 12
    fraAuthNum.Visible = False
    Me.Height = lblAction.Top + lblAction.Height + lblStatus.Height + fraBorder.Height + 850 + sbStatus.Height
    DoEvents

End Sub 'ResizeFormLiveMode

Private Sub PrintChequeReceipt()

Dim dblAmount As Double
    
    dblAmount = mstrTransactionAmount / 100
    
    Call modReceipt.PrintTransactionHeader(moOPOSPrinter, _
        moTranHeader.TranDate, moTranHeader.TransactionTime, moTranHeader.TillID, _
        moTranHeader.TransactionNo, moTOSType.Description, _
        moTranHeader.CashierID, mstrCashierName, _
        vbNullString)

    Call PrintCreditCardDetails(moOPOSPrinter, mstrCardProductName, mstrCreditCardNum, _
        mstrStartDate, mstrExpiryDate, dblAmount, mstrEntryMethod, mstrAuthNum, mstrCardSequenceNum, True, _
        mstrTerminalVerificationResult, mstrTransactionStatusInfo, mstrAuthResponceCode, mstrUnpredictableNumber, _
        mstrApplicationIdentifier, mstrTID, mstrMerchantID, mstrEmvCryptogram, mstrChequeType, mstrIACDefault, _
        mstrIACDenial, mstrIACOnline, mstrEmvCvmResults)

    Call modReceipt.PrintCreditCardSlip(moOPOSPrinter, False, mstrAuthNum, _
        mstrCardProductName, mstrCreditCardNum, mstrExpiryDate, dblAmount, False)

End Sub 'PrintChequeReceipt

Private Sub WaitForData(Optional lngTimeAllowed As Long = 0, Optional ByVal strDebugMsg As String)

Dim lngTimer    As Long

    lngTimer = 0
    While (LenB(mstrData) = 0) And (lngTimer <= lngTimeAllowed)
        DoEvents
        Wait (1)
        Call DebugMsg(Me.Name, "WaitForData", endlDebug, "Waiting for data-" & strDebugMsg)
        If (lngTimeAllowed > 0) Then lngTimer = lngTimer + 1
    Wend

End Sub 'WaitForData

Private Function ResponseMessage(ByVal strResponseCode As String) As String

Const VAR_RESPONSECODE_NUMB             As Long = 0
Const VAR_RESPONSECODE_MESSAGE          As Long = 1
Const RESPONSE_MESSAGE_FILE             As String = "c:\wix\response codes.txt"

Dim objFileSystem       As Scripting.FileSystemObject
Dim tsMessage           As TextStream
Dim vntResponse         As Variant
Dim strTexthold         As String

    Call DebugMsg(MODULE_NAME, "ResponseMessage", endlDebug, "Processing - '" & strResponseCode & "'")
    If LenB(strResponseCode) <> 0 Then
        ResponseMessage = vbNullString

        Set objFileSystem = New Scripting.FileSystemObject
        Set tsMessage = objFileSystem.OpenTextFile(RESPONSE_MESSAGE_FILE, ForReading, False, TristateFalse)

        While Not tsMessage.AtEndOfStream And (LenB(ResponseMessage) = 0)
            strTexthold = tsMessage.ReadLine
            vntResponse = Split(strTexthold, ",")
            On Error Resume Next
            If vntResponse(VAR_RESPONSECODE_NUMB) = strResponseCode Then
                ResponseMessage = vntResponse(VAR_RESPONSECODE_MESSAGE)
            End If
            On Error GoTo 0
        Wend
        Call Err.Clear
        If (ResponseMessage = "") Then ResponseMessage = "'" & strResponseCode & "' Not Recognised"
    End If

End Function 'ResponseMessage

Private Function MessageReasonCode(ByVal strMessageReasonCode As String) As String

Const VAR_MESSAGEREASONCODE_NUMB            As Long = 0
Const VAR_MESSAGEREASONCODE_MESSAGE         As Long = 1
Const MESSAGEREASONCODE_FILE                As String = "c:\wix\Message Reason Code.txt"

Dim objFileSystem       As Scripting.FileSystemObject
Dim tsMessage           As TextStream
Dim vntResponse         As Variant
Dim strTexthold         As String

    Call DebugMsg(MODULE_NAME, "MessageReasonCode", endlDebug, "Processing - '" & strMessageReasonCode & "'")
    If LenB(strMessageReasonCode) <> 0 Then
        MessageReasonCode = vbNullString

        Set objFileSystem = New FileSystemObject
        Set tsMessage = objFileSystem.OpenTextFile(MESSAGEREASONCODE_FILE, ForReading, False, TristateFalse)

        While (Not tsMessage.AtEndOfStream) And (LenB(MessageReasonCode) = 0)
            strTexthold = tsMessage.ReadLine
            vntResponse = Split(strTexthold, ",")
            On Error Resume Next
            If vntResponse(VAR_MESSAGEREASONCODE_NUMB) = strMessageReasonCode Then
                MessageReasonCode = vntResponse(VAR_MESSAGEREASONCODE_MESSAGE)
            End If
            On Error GoTo 0
        Wend
    End If
    Call Err.Clear
    If (MessageReasonCode = "") Then MessageReasonCode = "'" & strMessageReasonCode & "' Not Recognised"

End Function 'MessageReasonCode

Private Function ExtractResponseType(ByVal strSearchString As String, _
                                  ByVal strAttribute As String) As String

Dim lngStartPos As Long
Dim lngEndPos   As Long

    strSearchString = Mid$(strSearchString, 45)
    lngStartPos = InStr(1, strSearchString, strAttribute)
    lngStartPos = lngStartPos + Len(strAttribute)
    lngEndPos = InStr(lngStartPos, strSearchString, " ")
    ExtractResponseType = Mid$(strSearchString, lngStartPos, lngEndPos - lngStartPos)

End Function 'ExtractResponseType

Private Function ExtractIACAttribute(ByVal strSearchString As String, _
                                  ByVal strAttribute As String) As String

Dim lngStartPos As Long
Dim lngEndPos   As Long

    lngStartPos = InStr(1, strSearchString, strAttribute)
    lngStartPos = lngStartPos + Len(strAttribute & "=")
    ExtractIACAttribute = Mid$(strSearchString, lngStartPos, 10)

End Function 'ExtractIACAttribute

Private Function ExtractTID(ByVal strSearchString As String, _
                                  ByVal strAttribute As String) As String

Dim lngStartPos As Long
Dim lngEndPos   As Long

    lngStartPos = InStr(1, strSearchString, strAttribute)
    lngStartPos = lngStartPos + Len(strAttribute)
    lngEndPos = InStr(lngStartPos, strSearchString, vbCrLf)
    ExtractTID = Mid$(strSearchString, lngStartPos, lngEndPos - lngStartPos)

End Function 'ExtractAttribute

Private Sub AuthoriseCheque()

    mstrTransactionNo = Format(Now, "DDHHMMSS")
    mstrTransactionNo = Mid(mstrTransactionNo, 2, 6)
    If Left(mstrTransactionNo, 1) = "0" Then
        mstrTransactionNo = Replace(Left(mstrTransactionNo, 1), "0", "9") & _
        Mid(mstrTransactionNo, 2, 5)
    End If
    
    Call CentreForm(Me)
    Call Connect
    If Winsock1.State = sckConnected Then
        Call InitiateCheque
    Else
        txtChequeNo.Text = vbNullString
        txtSortCode.Text = vbNullString
        txtAccountNo.Text = vbNullString
        txtAuthNum.Text = vbNullString
        txtEndMonth.Text = vbNullString
        txtStartMonth.Text = vbNullString
        txtStartYear.Text = vbNullString
        txtEndYear.Text = vbNullString
        txtCardNo.Text = vbNullString
        txtIssueNo.Text = vbNullString
        mblnCustPresent = False
        txtCustPresent.Text = "Y"
        mstrAuthNum = vbNullString
        mstrTrack2 = vbNullString
    End If
    'Unload Me
    
End Sub 'AuthoriseCheque

Private Sub InitiateCheque()

Dim strInt                  As String
Dim strTransaction          As String
Dim lngCounter              As Long
Dim intLen                  As Integer
Dim strResponseCode         As String
Dim strMessageReasonCode    As String
Dim strError                As String
Dim strEventType            As String
Dim objFileSystem           As New FileSystemObject
Dim tsMessage               As TextStream
Dim strRead                 As String
Dim strTillID               As String

    fraInsertCard.Visible = False
    lblStatus.Visible = False 'Was True
    fraChequeCard.Visible = True
    fraAuthNum.Visible = True
    fraBorder.Visible = True
    fraBorder.Enabled = False
    fraAccountNo.Visible = False
    lblAction.Visible = True
    mstrCreditCardNum = vbNullString
    Call DisableKeyedEntry
    Call ResizeFormLiveMode
    DoEvents
    
    Call DebugMsg("Initiate", vbNullString, endlDebug, "Opening text file c:\eftauth.par")
    strTillID = goSession.CurrentEnterprise.IEnterprise_WorkstationID
    'Get terminal ids from eftauth.par
    Set tsMessage = objFileSystem.OpenTextFile("c:\eftauth.par", ForReading, False, TristateFalse)
    strRead = tsMessage.ReadAll
    
    mstrTID = ExtractTID(strRead, "TILL " & strTillID & "=")
    Call DebugMsg("Initiate", vbNullString, endlDebug, "Till Number = " & mstrTID)
    
    If mblnInitiated <> True Then
        lblStatus.Caption = "Initiating system"
        lblAction.Caption = "Initiating system"
        Call DebugMsg(Me.Name, "Initiate", endlDebug, "Initiating system")
        'Clear gobal variable holding the data
        
        mstrData = vbNullString
        strInt = vbNullString
        strInt = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & _
            " encoding=" & Chr(34) & "UTF-8" & Chr(34) & "?>"
              
        strInt = strInt & "<Esp:Interface Version=" & Chr(34) & "1.0" & Chr(34) & _
            " xmlns:Esp=" & Chr(34) & "http://www.mosaicsoftware.com/Postilion/eSocket.POS/" & Chr(34) & _
            "><Esp:Admin TerminalId=" & Chr(34) & mstrTID & Chr(34) & " Action=" & Chr(34) & _
            "INIT" & Chr(34) & "><Esp:Register Type= " & Chr(34) & "EVENT" & Chr(34) & _
            " EventId=" & Chr(34) & "DEBUG_ALL" & Chr(34) & _
            " /></Esp:Admin></Esp:Interface>"
            
        intLen = Len(strInt)
        strInt = Chr(intLen \ 256) & Chr(intLen Mod 256) & strInt
        mblnSendComplete = False
        
        Call DebugMsg(Me.Name, "Initiate", endlDebug, "Sending Initiate Cheque command: " & _
            strInt)
        
        RecordDataSent (strInt)
        
        'Loop until WinSck has send all of the message
        While mblnSendComplete = False
            DoEvents
        Wend
        
        'Loop until there is a response from the eSocket software
        mblnInitiateCheque = True
        Call WaitForData
        Call RecordDataLog("PROCESS INITIATE RESPONSE CHQ1")
        mblnInitiateCheque = False
        
        While mstrResponseType <> RESP_TYPE_ADMIN
            Call WaitForData
            Call RecordDataLog("PROCESS INITIATE RESPONSE CHQ2")
            Call DebugMsg(Me.Name, "Initiate Cheque", endlDebug, "Data received: " & _
                EraseCardNos(mstrData))
            mstrResponseType = ExtractResponseType(mstrData, RESP_TYPE)
            Select Case mstrResponseType
                Case RESP_TYPE_ERROR
                    strResponseCode = ExtractAttribute(mstrData, RESP_RESPONSECODE)
                    strMessageReasonCode = ExtractAttribute(mstrData, RESP_MESSAGEREASONCODE)
                    strError = ExtractAttribute(mstrData, "Description")
                    
                    If MsgBoxEx("WARNING: There was an error whilst trying to initiate the transaction." & vbCrLf & _
                        strError & vbCrLf & _
                        "Response code: " & strResponseCode & " - " & ResponseMessage(strResponseCode) & _
                        vbCrLf & "Message Reason Code: " & strMessageReasonCode & " - " & MessageReasonCode(strMessageReasonCode) & _
                        vbCrLf & "Do you wish to retry?", vbYesNo, "Warning transaction failed", , , , , RGBMsgBox_WarnColour) = vbYes Then
                        Call InitiateCheque
                    Else
                        If Me.Visible = True Then Me.Hide
                        Exit Sub
                    End If
                    mstrData = vbNullString
                Case Else
            End Select
        Wend
        
        'Determine the response from eSocket Software
        Select Case ExtractAttribute(mstrData, RESP_ACTIONCODE)
            Case RESP_ACTIONCODE_APPROVE
                lblStatus.Caption = "Initiate Cheque Successful"
                Wait (1)
                mblnInitiated = True
                If mstrChequeType <> CHEQUE_COMP Then fraInsertCard.Visible = True
                fraInsertCard.BackColor = RGBMSGBox_PromptColour
                lblInsert.Caption = ("Please insert \ swipe the Cheque Guarantee card in Pin Pad")
                lblAction.Caption = "Insert Cheque Guarantee Card"
                
                fraBorder.Visible = False
                DoEvents
                
                Call ChequeInquiry
                
            Case RESP_ACTIONCODE_DECLINE
                strResponseCode = ExtractAttribute(mstrData, RESP_RESPONSECODE)
                strMessageReasonCode = ExtractAttribute(mstrData, RESP_MESSAGEREASONCODE)
                
                If MsgBoxEx("WARNING: The Initiate Cheque has been declined" & vbCrLf & _
                "Response code: " & strResponseCode & " - " & ResponseMessage(strResponseCode) & _
                vbCrLf & "Message Reason Code: " & strMessageReasonCode & " - " & MessageReasonCode(strMessageReasonCode) & _
                    vbCrLf & "Do you wish to retry?", vbYesNo, "Warning Initiate Cheque failed", , , , , RGBMsgBox_WarnColour) = vbYes Then
                    Call InitiateCheque
                Else
                    Call Me.Hide
                End If
            Case Else
    
        End Select
    Else
        lblStatus.Visible = False 'Was True
        fraChequeCard.Visible = True
        fraBorder.Enabled = False
        lblAction.Visible = True
        Call DisableKeyedEntry
        Call ResizeFormLiveMode
        If mstrChequeType <> CHEQUE_COMP Then fraInsertCard.Visible = True
        fraInsertCard.BackColor = RGBMSGBox_PromptColour
        lblInsert.Caption = ("Please insert \ swipe the Cheque Guarantee card in Pin Pad")
        lblAction.Caption = "Insert Cheque Guarantee Card"
        fraBorder.Visible = False
        DoEvents
        
        Call ChequeInquiry
    End If
    
End Sub 'InitiateCheque

Private Sub ChequeInquiry()

Dim dblTransactionAmount        As Double
Dim strTransaction              As String
Dim intLen                      As Integer
Dim strActionCode               As String
Dim strAuthorisationNumber      As String
Dim strDateTime                 As String
Dim mstrPosCondition            As String
Dim strResponseCode             As String
Dim strMessageReasonCode        As String
Dim strServiceRestrictionCode   As String
Dim strError                    As String
Dim strEventType                As String
Dim strMsgBoxResponse           As String
Dim strStartDate                As String

    'Clear gobal variable holding the data
    mstrFallBackType = vbNullString
    mstrEntryMethod = vbNullString
    lblStatus.Caption = vbNullString
    dblTransactionAmount = mstrTransactionAmount
    mstrTransactionType = moTOSType.Description
    While Len(mstrAccountNo) < 16
        mstrAccountNo = mstrAccountNo & " "
    Wend
    While Len(mstrSortCode) < 8
        mstrSortCode = mstrSortCode & " "
    Wend

    If (Val(txtStartMonth.Text) > 0) Then
        strStartDate = " StartDate=" & Chr(34) & txtStartYear.Text & txtStartMonth.Text & Chr(34)
    End If
    mstrTransactionNo = Format(Now, "DDHHMMSS")
    mstrTransactionNo = Mid(mstrTransactionNo, 2, 6)
    If Left(mstrTransactionNo, 1) = "0" Then
        mstrTransactionNo = Replace(Left(mstrTransactionNo, 1), "0", "9") & _
        Mid(mstrTransactionNo, 2, 5)
    End If
    
    strTransaction = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & _
        " encoding=" & Chr(34) & "UTF-8" & Chr(34) & "?>"

    If mstrChequeType = CHEQUE_COMP Then
        strTransaction = strTransaction & "<Esp:Interface Version=" & Chr(34) & "1.0" & Chr(34) & _
            " xmlns:Esp=" & Chr(34) & "http://www.mosaicsoftware.com/Postilion/eSocket.POS/" & Chr(34) & _
            "><Esp:Inquiry TerminalId=" & Chr(34) & mstrTID & Chr(34) & _
            " TransactionId=" & Chr(34) & mstrTransactionNo & Chr(34) & " Type=" & Chr(34) & _
            " CHEQUE_GUARANTEE" & Chr(34) & _
            " CardNumber=" & Chr(34) & "0000000000000000" & Chr(34) & _
            " ExpiryDate=" & Chr(34) & "0000" & Chr(34) & _
            " ChequeAccountNumber=" & Chr(34) & mstrAccountNo & Chr(34) & _
            " ChequeInstitutionCode=" & Chr(34) & mstrSortCode & Chr(34) & _
            " ChequeNumber=" & Chr(34) & mstrChequeNo & Chr(34) & _
            " DateTime=" & Chr(34) & Format(Now, "MMDDHHNNSS") & Chr(34) & _
            " TransactionAmount=" & Chr(34) & dblTransactionAmount & Chr(34) & _
            " /></Esp:Interface>"
            
    Else
    
        If txtCardNo.Text = vbNullString Then
            strTransaction = strTransaction & "<Esp:Interface Version=" & Chr(34) & "1.0" & Chr(34) & _
                " xmlns:Esp=" & Chr(34) & "http://www.mosaicsoftware.com/Postilion/eSocket.POS/" & Chr(34) & _
                "><Esp:Inquiry TerminalId=" & Chr(34) & mstrTID & Chr(34) & _
                " TransactionId=" & Chr(34) & mstrTransactionNo & Chr(34) & " Type=" & Chr(34) & _
                " CHEQUE_GUARANTEE" & Chr(34) & _
                " ChequeAccountNumber=" & Chr(34) & mstrAccountNo & Chr(34) & _
                " ChequeInstitutionCode=" & Chr(34) & mstrSortCode & Chr(34) & _
                " ChequeNumber=" & Chr(34) & mstrChequeNo & Chr(34) & _
                " DateTime=" & Chr(34) & Format(Now, "MMDDHHNNSS") & Chr(34) & _
                " TransactionAmount=" & Chr(34) & dblTransactionAmount & Chr(34) & _
                " /></Esp:Interface>"
        Else
        
        'Keyed entry
            mstrExpiryDate = txtEndYear.Text & txtEndMonth.Text
            If ValidateExpiryDate(mstrExpiryDate) = False Then
                Call MsgBoxEx("Please enter a valid expiry date", vbOKOnly, "Invalid Expiry Date", , , , , RGBMsgBox_WarnColour)
                If (txtCardNo.Visible = True) And (txtCardNo.Enabled = True) Then txtCardNo.SetFocus
                Exit Sub
            End If
            mblnCardDetailsEntered = True
            mstrStartDate = txtStartYear.Text & txtStartMonth.Text
            mstrExpiryDate = txtEndYear.Text & txtEndMonth.Text
            mstrCreditCardNum = txtCardNo.Text
            While Len(mstrCreditCardNum) < 16
                mstrCreditCardNum = "0" & mstrCreditCardNum
            Wend
            mstrCardSequenceNum = IIf(Val(txtIssueNo.Text) = 0, "", txtIssueNo.Text)
        
            strTransaction = strTransaction & "<Esp:Interface Version=" & Chr(34) & "1.0" & Chr(34) & _
                " xmlns:Esp=" & Chr(34) & "http://www.mosaicsoftware.com/Postilion/eSocket.POS/" & Chr(34) & _
                "><Esp:Inquiry TerminalId=" & Chr(34) & mstrTID & Chr(34) & _
                " TransactionId=" & Chr(34) & mstrTransactionNo & Chr(34) & " Type=" & Chr(34) & _
                " CHEQUE_GUARANTEE" & Chr(34) & _
                " ChequeAccountNumber=" & Chr(34) & mstrAccountNo & Chr(34) & _
                " ChequeInstitutionCode=" & Chr(34) & mstrSortCode & Chr(34) & _
                " CardNumber=" & Chr(34) & mstrCreditCardNum & Chr(34) & _
                " ExpiryDate=" & Chr(34) & mstrExpiryDate & Chr(34) & _
                strStartDate & _
                IIf(mstrCardSequenceNum = "", "", " CardSequenceNumber=" & Chr(34) & mstrCardSequenceNum & Chr(34)) & _
                " ChequeNumber=" & Chr(34) & mstrChequeNo & Chr(34) & _
                " DateTime=" & Chr(34) & Format(Now, "MMDDHHNNSS") & Chr(34) & _
                " TransactionAmount=" & Chr(34) & dblTransactionAmount & Chr(34) & _
                " /></Esp:Interface>"
       
        End If

    End If
        
    intLen = Len(strTransaction)
    strTransaction = Chr(intLen \ 256) & Chr(intLen Mod 256) & strTransaction
    mblnSendComplete = False
    'Sending Transaction message to eSocket Software
    lblStatus.Caption = "Sending Transaction"
    mstrData = vbNullString
    mstrResponseType = vbNullString
    RecordDataSent (strTransaction)
    Call DebugMsg(Me.Name, "Transaction", endlDebug, "Sending Transaction command: " & _
            strTransaction)

    lblStatus.Caption = "Waiting"
    lblAction.Caption = "Waiting"
    While mblnSendComplete = False
        DoEvents
    Wend

     'Loop until there is a response from the eSocket software
    mblnInitiateCheque = True
    Call WaitForData
    Call RecordDataLog("PROCESS INITIATE RESPONSE INQ1")
    mblnInitiateCheque = False
    Call DebugMsg(Me.Name, "Cheque Inquiry", endlDebug, "Data received: " & _
        EraseCardNos(mstrData))
    lblAction.Caption = "Processing"
    fraBorder.Visible = True
    fraChequeCard.Visible = True
    fraAuthNum.Visible = False
    fraAccountNo.Visible = False
    Me.Refresh

    While mstrResponseType <> RESP_TYPE_INQUIRY
        Call WaitForData
        Call RecordDataLog("PROCESS INITIATE RESPONSE INQ2")
        Call DebugMsg(Me.Name, "Cheque", endlDebug, "Data received: " & _
            EraseCardNos(mstrData))
        mstrResponseType = ExtractResponseType(mstrData, RESP_TYPE)
        Select Case mstrResponseType
            Case RESP_TYPE_EVENT
                strEventType = ExtractAttribute(mstrData, RESP_EVENTID)
                lblStatus.Caption = Replace(strEventType, "_", " ")
                Call DebugMsg(Me.Name, "Event received", endlDebug, _
                    "Event received: " & strEventType)
                mstrData = vbNullString
                DoEvents
                Wait (1)

            Case RESP_TYPE_CALLBACK
            Case RESP_TYPE_ERROR
                strResponseCode = ExtractAttribute(mstrData, RESP_RESPONSECODE)
                strError = ExtractAttribute(mstrData, "Description")

                If MsgBoxEx("WARNING: There was an error whilst trying to process the transaction." & vbCrLf & _
                    strError & vbCrLf & _
                    "Response code: " & strResponseCode & " - " & ResponseMessage(strResponseCode) & _
                    vbCrLf & "Message Reason Code: " & strMessageReasonCode & " - " & MessageReasonCode(strMessageReasonCode) & _
                    vbCrLf & "Do you wish to retry?", vbYesNo, "Warning transaction failed", , , , , RGBMsgBox_WarnColour) = vbYes Then
                    mlngAttemptNum = mlngAttemptNum + 1
                    Call ChequeInquiry
                    Exit Sub
                Else
                    txtChequeNo.Text = vbNullString
                    txtSortCode.Text = vbNullString
                    txtAccountNo.Text = vbNullString
                    txtAuthNum.Text = vbNullString
                    txtEndMonth.Text = vbNullString
                    txtStartMonth.Text = vbNullString
                    txtStartYear.Text = vbNullString
                    txtEndYear.Text = vbNullString
                    txtCardNo.Text = vbNullString
                    txtIssueNo.Text = vbNullString
                    mblnCustPresent = False
                    txtCustPresent.Text = "Y"
                    mstrAuthNum = vbNullString
                    mstrTrack2 = vbNullString
                    Call Me.Hide
                    Exit Sub
                End If

        End Select
    Wend

    'Determine the response from eSocket Software
    Select Case ExtractAttribute(mstrData, RESP_ACTIONCODE)

        Case RESP_ACTIONCODE_DECLINE 'Transaction has been declined
            lblStatus.Caption = "Cheque Inquiry Declined"
            lblAction.Caption = "Declined"
            strResponseCode = ExtractAttribute(mstrData, RESP_RESPONSECODE)
            strMessageReasonCode = ExtractAttribute(mstrData, RESP_MESSAGEREASONCODE)

            If mstrChequeType = CHEQUE_COMP Then
                strMsgBoxResponse = MsgBoxEx("WARNING: The Cheque Inquiry has been declined" & vbCrLf & _
                    "Response code : " & strResponseCode & " - " & ResponseMessage(strResponseCode) & _
                    vbCrLf & "Message Reason Code: " & strMessageReasonCode & " - " & MessageReasonCode(strMessageReasonCode) _
                    , vbYesNo, _
                    "DECLINED", "Retry", "Alternate Tender", , , RGBMsgBox_WarnColour)
                    mlngAttemptNum = mlngAttemptNum + 1
                
                Select Case strMsgBoxResponse
                    Case vbYes 'Retry
                        Call ChequeInquiry
                        Exit Sub
                    Case vbNo 'Alternative Tender
                        txtChequeNo.Text = vbNullString
                        txtSortCode.Text = vbNullString
                        txtAccountNo.Text = vbNullString
                        txtAuthNum.Text = vbNullString
                        txtEndMonth.Text = vbNullString
                        txtStartMonth.Text = vbNullString
                        txtStartYear.Text = vbNullString
                        txtEndYear.Text = vbNullString
                        txtCardNo.Text = vbNullString
                        txtIssueNo.Text = vbNullString
                        mblnCustPresent = False
                        txtCustPresent.Text = "Y"
                        mstrAuthNum = vbNullString
                        mstrTrack2 = vbNullString
                        mstrAuthNum = vbNullString
                        Call MsgBoxEx("Not Authorised. Present customer with declination card", vbOKOnly _
                            , "Declination Card", , , , , RGBMsgBox_WarnColour)
                        Call Me.Hide
                End Select
                
            Else
                strMsgBoxResponse = MsgBoxEx("WARNING: The Cheque Inquiry has been declined" & vbCrLf & _
                    "Response code : " & strResponseCode & " - " & ResponseMessage(strResponseCode) & _
                    vbCrLf & "Message Reason Code: " & strMessageReasonCode & " - " & MessageReasonCode(strMessageReasonCode) _
                    , vbYesNoCancel, _
                    "DECLINED", "Retry", "Key", "Alternate Tender", , RGBMsgBox_WarnColour)
                    mlngAttemptNum = mlngAttemptNum + 1
                    
                Select Case strMsgBoxResponse
                    Case vbYes 'Retry
                        Call ChequeInquiry
                        Exit Sub
                    Case vbNo 'Key
                        Call EnableKeyedEntry
                        mblnCardDetailsEntered = False
                        lblAction.Caption = "Enter Card Details"
                        lblStatus.Caption = vbNullString
                        If (txtCardNo.Visible = True) And (txtCardNo.Enabled = True) Then txtCardNo.SetFocus
                        Do
                            DoEvents
                        Loop Until mblnCardDetailsEntered = True
                        If (Me.Visible = False) Then Exit Sub
                        mstrEntryMethod = ENTRY_KEY
                        Call ChequeInquiry
                        Exit Sub
                    Case vbCancel 'Alternative Tender
                        txtChequeNo.Text = vbNullString
                        txtSortCode.Text = vbNullString
                        txtAccountNo.Text = vbNullString
                        txtAuthNum.Text = vbNullString
                        txtEndMonth.Text = vbNullString
                        txtStartMonth.Text = vbNullString
                        txtStartYear.Text = vbNullString
                        txtEndYear.Text = vbNullString
                        txtCardNo.Text = vbNullString
                        txtIssueNo.Text = vbNullString
                        mblnCustPresent = False
                        txtCustPresent.Text = "Y"
                        mstrAuthNum = vbNullString
                        mstrTrack2 = vbNullString
                        mstrAuthNum = vbNullString
                        Call MsgBoxEx("Not Authorised. Present customer with declination card", vbOKOnly _
                            , "Declination Card", , , , , RGBMsgBox_WarnColour)
                        Call Me.Hide
                End Select
            End If

        Case RESP_ACTIONCODE_APPROVE 'Transaction has been refered

        'Extract information from responce
            strActionCode = ExtractAttribute(mstrData, RESP_ACTIONCODE)
            mstrCreditCardNum = ExtractAttribute(mstrData, RESP_CARDNUMBER)
            mstrCardProductName = ExtractAttribute(mstrData, RESP_CARDPRODUCTNAME)
            strDateTime = ExtractAttribute(mstrData, RESP_DATETIME)
            mstrExpiryDate = ExtractAttribute(mstrData, RESP_EXPIRYDATE)
            mstrPanEntryMode = ExtractAttribute(mstrData, RESP_PANENTRYMODE)
            mstrPosCondition = ExtractAttribute(mstrData, RESP_POSCONDITION)
            strResponseCode = ExtractAttribute(mstrData, RESP_RESPONSECODE)
            strMessageReasonCode = ExtractAttribute(mstrData, RESP_MESSAGEREASONCODE)
            strServiceRestrictionCode = ExtractAttribute(mstrData, RESP_SERVICERESTRICTIONCODE)
            
            mstrAuthNum = Trim(ExtractAttribute(mstrData, RESP_CERTEGYAUTHCODE))
            mstrAuthNum = Replace(mstrAuthNum, ",", vbNullString)
            mstrAuthNum = Replace(mstrAuthNum, "CertegyAuthCode=", vbNullString)
            Call MsgBox(mstrAuthNum)
            
            mstrMerchantID = Trim(ExtractAttribute(mstrData, RESP_MERCHANTID))
            mstrTrack2 = ExtractAttribute(mstrData, RESP_TRACK2)
            Call DisableKeyedEntry
            lblAction.Caption = "Card Details"
            'txtCardNo.Text = mstrCreditCardNum
            txtCardNo.Text = String$(Len(mstrCreditCardNum) - 4, "X") & Right$(mstrCreditCardNum, 4)
            txtEndMonth.Text = Mid$(mstrExpiryDate, 3, 2)
            txtEndYear.Text = Left$(mstrExpiryDate, 2)
            txtIssueNo.Text = mstrCardSequenceNum

            Wait (1)

            lblAction.Caption = "Printing"
            lblStatus.Caption = "Printing"
            enEntryMode = encmCheque
            enConfirmationMode = encmSignatureCon
            
            Call ConfirmPrintedOk
            lblStatus.Caption = "Cheque Inquiry Successful"
            If mstrChequeType <> vbNullString Then
                If MsgBoxEx("Has the customer recorded their address on back of cheque?", vbYesNo + vbQuestion, _
                    "Record customers address", , , , , RGBMSGBox_PromptColour) = vbNo Then
                    mlngAttemptNum = mlngAttemptNum + 1
                    mstrAuthNum = vbNullString
                    txtChequeNo.Text = vbNullString
                    txtSortCode.Text = vbNullString
                    txtAccountNo.Text = vbNullString
                    txtAuthNum.Text = vbNullString
                    txtEndMonth.Text = vbNullString
                    txtStartMonth.Text = vbNullString
                    txtStartYear.Text = vbNullString
                    txtEndYear.Text = vbNullString
                    txtCardNo.Text = vbNullString
                    txtIssueNo.Text = vbNullString
                    mblnCustPresent = False
                    txtCustPresent.Text = "Y"
                    mstrAuthNum = vbNullString
                    mstrTrack2 = vbNullString
                    Call MsgBoxEx("Not Authorised. Present customer with declination card", vbOKOnly _
                        , "Declination Card", , , , , RGBMsgBox_WarnColour)
                    Call Me.Hide
                End If
            End If

        Case Else
            strResponseCode = ExtractAttribute(mstrData, RESP_RESPONSECODE)
            strMessageReasonCode = ExtractAttribute(mstrData, RESP_MESSAGEREASONCODE)

            If MsgBoxEx("WARNING: The Cheque Inquiry has been declined" & vbCrLf & _
                "Response code : " & strResponseCode & " - " & ResponseMessage(strResponseCode) & _
                vbCrLf & "Message Reason Code: " & strMessageReasonCode & " - " & MessageReasonCode(strMessageReasonCode) & _
                vbCrLf & "Do you wish to phone through for manual authorisation?", _
                 vbYesNo, "Warning Transaction failed", , , , , RGBMsgBox_WarnColour) = vbYes Then
                 mstrTrack2 = ExtractAttribute(mstrData, RESP_TRACK2)
                 
                If mstrChequeType = CHEQUE_COMP Then
                    txtCardNo.Text = "0000000000000000"
                    txtEndYear.Text = "00"
                    txtEndMonth.Text = "00"
                Else
                    txtCardNo.Text = Left(mstrTrack2, InStr(mstrTrack2, "=") - 1)
                    txtEndYear.Text = Mid(mstrTrack2, InStr(mstrTrack2, "=") + 1, 2)
                    txtEndMonth.Text = Mid(mstrTrack2, InStr(mstrTrack2, "=") + 3, 2)
                End If
                mstrCreditCardNum = txtCardNo.Text
                mstrExpiryDate = txtEndMonth.Text & txtEndYear.Text
                'Manual Cheque Authorisation
                mblnAuthEntered = False
                lblAction.Caption = "Enter Authorisation Number"
                lblStatus.Caption = "Please enter a manual authorisation number"
                fraChequeCard.Enabled = False
                fraChequeType.Enabled = False
                fraBorder.Enabled = True
                fraAuthNum.Enabled = True
                DoEvents
                Call MsgBoxEx("Please phone for a manual authorisation number", vbOKOnly, "Authorisation Required", _
                    , , , , RGBMsgBox_WarnColour)
                fraAuthNum.Visible = True
                DoEvents
                If (txtAuthNum.Visible = True) And (txtAuthNum.Enabled = True) Then txtAuthNum.SetFocus
    
                Do
                    DoEvents
                Loop Until mblnAuthEntered = True
                If Me.Visible = False Then Exit Sub
                While Len(txtAuthNum.Text) < 6
                    txtAuthNum.Text = "0" & txtAuthNum.Text
                Wend
                DoEvents
                mstrAuthNum = txtAuthNum.Text
                
                If (mstrAuthNum <> "000000") Then
                    lblAction.Caption = "Printing"
                    lblStatus.Caption = "Printing"
                    enEntryMode = encmCheque
                    enConfirmationMode = encmSignatureCon
                    Call ConfirmPrintedOk
                    lblStatus.Caption = "Cheque Inquiry Successful"
                    If MsgBoxEx("Has the customer recorded their address on back of cheque", vbYesNo, _
                        "Record customers address", , , , , RGBMSGBox_PromptColour) = vbNo Then
                        mlngAttemptNum = mlngAttemptNum + 1
                        mstrAuthNum = vbNullString
                        txtChequeNo.Text = vbNullString
                        txtSortCode.Text = vbNullString
                        txtAccountNo.Text = vbNullString
                        txtAuthNum.Text = vbNullString
                        txtEndMonth.Text = vbNullString
                        txtStartMonth.Text = vbNullString
                        txtStartYear.Text = vbNullString
                        txtEndYear.Text = vbNullString
                        txtCardNo.Text = vbNullString
                        txtIssueNo.Text = vbNullString
                        mblnCustPresent = False
                        txtCustPresent.Text = "Y"
                        mstrAuthNum = vbNullString
                        mstrTrack2 = vbNullString
                        Call MsgBoxEx("Not Authorised. Present customer with declination card", vbOKOnly _
                            , "Declination Card", , , , , RGBMsgBox_WarnColour)
                        Call Me.Hide
                    End If
                End If
            Else
                mlngAttemptNum = mlngAttemptNum + 1
                mstrAuthNum = vbNullString
                txtChequeNo.Text = vbNullString
                txtSortCode.Text = vbNullString
                txtAccountNo.Text = vbNullString
                txtAuthNum.Text = vbNullString
                txtEndMonth.Text = vbNullString
                txtStartMonth.Text = vbNullString
                txtStartYear.Text = vbNullString
                txtEndYear.Text = vbNullString
                txtCardNo.Text = vbNullString
                txtIssueNo.Text = vbNullString
                mblnCustPresent = False
                txtCustPresent.Text = "Y"
                mstrAuthNum = vbNullString
                mstrTrack2 = vbNullString
                Call MsgBoxEx("Not Authorised. Present customer with declination card", vbOKOnly _
                    , "Declination Card", , , , , RGBMsgBox_WarnColour)
                Call Me.Hide
            End If

        End Select

End Sub 'ChequeInquiry

Private Sub ChequeWithOutAuth()

Dim strTrack1   As String
Dim strTrack2   As String
Dim strTrack3   As String

    mstrTransactionNo = Format(Now, "DDHHMMSS")
    mstrTransactionNo = Mid(mstrTransactionNo, 2, 6)
    If Left(mstrTransactionNo, 1) = "0" Then
        mstrTransactionNo = Replace(Left(mstrTransactionNo, 1), "0", "9") & _
        Mid(mstrTransactionNo, 2, 5)
    End If
    
    Call DisableKeyedEntry
    Call CentreForm(Me)
    fraInsertCard.Visible = True
    fraChequeType.Visible = False
    fraAccountNo.Visible = False
    DoEvents
    fraInsertCard.BackColor = RGBMSGBox_PromptColour
    fraBorder.Visible = False
    lblInsert.Caption = ("Please insert \ swipe the Cheque Guarantee card in Pin Pad")
    lblAction.Caption = "Insert \ Swipe Cheque Guarantee Card"
    lblStatus.Caption = "Insert \ Swipe Cheque Guarantee Card"
    DoEvents
    Call frmReadCard.RetrieveTracks(strTrack1, strTrack2, strTrack3)
    
    If strTrack2 <> vbNullString Then
    
        fraBorder.Visible = True
        fraInsertCard.Visible = False
        fraChequeType.Visible = True
        lblAction.Caption = "Card Details Received"
        lblStatus.Caption = "Card Details Received"
        DoEvents
        
        txtAuthNum.Text = mstrTransactionNo
        mstrAuthNum = txtAuthNum.Text
        
        txtCardNo.Text = Left(strTrack2, InStr(strTrack2, "=") - 1)
        mstrCreditCardNum = txtCardNo.Text
        txtEndYear.Text = Mid(strTrack2, InStr(strTrack2, "=") + 1, 2)
        txtEndMonth.Text = Mid(strTrack2, InStr(strTrack2, "=") + 3, 2)
        mstrExpiryDate = txtEndMonth.Text & txtEndYear.Text
        
        Wait (1)
        lblAction.Caption = "Printing"
        lblStatus.Caption = "Printing"
        enEntryMode = encmCheque
        enConfirmationMode = encmSignatureCon
        
        Call ConfirmPrintedOk
        lblStatus.Caption = "Cheque Inquiry Successful"
    Else
        mlngAttemptNum = mlngAttemptNum + 1
        mstrAuthNum = vbNullString
        txtChequeNo.Text = vbNullString
        txtSortCode.Text = vbNullString
        txtAccountNo.Text = vbNullString
        txtAuthNum.Text = vbNullString
        txtEndMonth.Text = vbNullString
        txtStartMonth.Text = vbNullString
        txtStartYear.Text = vbNullString
        txtEndYear.Text = vbNullString
        txtCardNo.Text = vbNullString
        txtIssueNo.Text = vbNullString
        mblnCustPresent = False
        txtCustPresent.Text = "Y"
        mstrAuthNum = vbNullString
        mstrTrack2 = vbNullString
        mstrTransactionAmount = 0
        Call Me.Hide
    End If

End Sub 'ChequeWithOutAuth

Private Sub txtCustPresent_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyTab Then Call txtCustPresent_KeyDown(vbKeyReturn, 0)

End Sub

'Private Sub txtCustPresent_LostFocus()
'
'Dim strSQL          As String
'Dim strDSN          As String
'
'    strDSN = goDatabase.ConnectionString
'
' If (KeyAscii = vbKeyReturn) Then
'        KeyAscii = 0
'        mblnCardDetailsEntered = False
'        DoEvents
'        If (mstrExpiryDate = vbNullString) Or (mstrExpiryDate = "----") Then
'            Call MsgBoxEx("Please enter a valid expiry date", vbOKOnly, "Invalid Expiry Date", , , , , RGBMsgBox_WarnColour)
'            txtCardNo.SetFocus
'            Exit Sub
'        End If
'        KeyAscii = 0
'        mblnCardDetailsEntered = True
'        fraBorder.Enabled = False
'
'        mstrStartDate = txtStartYear.Text & txtStartMonth.Text
'        mstrExpiryDate = txtEndYear.Text & txtEndMonth.Text
'        mstrCreditCardNum = txtCardNo.Text
'        mstrCardSequenceNum = txtIssueNo.Text
'        If txtCustPresent = "Y" Then
'            mblnCustPresent = True
'        Else
'            mblnCustPresent = False
'        End If
'
'        'Release control back to loop in tmrESocket
'        DoEvents
'    End If
'
'
'    'Release control back to loop in tmrESocket
'    DoEvents
'
'End Sub 'txtCustPresent_LostFocus

Private Function ValidateExpiryDate(ByVal strExpiryDate) As Boolean

    ValidateExpiryDate = True
    strExpiryDate = Replace(strExpiryDate, "-", "")
    If strExpiryDate = vbNullString Then ValidateExpiryDate = False
    If Len(strExpiryDate) <> 4 Then ValidateExpiryDate = False
    
End Function 'ValidateExpiryDate

Private Sub ChequeNoTransax()

'    Call MsgBoxEx("Warning transaction amount exceeds the account credit limit" & _
'        vbCrLf & "Please enter a supervisor password", vbOKOnly, "Transaction Exceeds Account Credit Limit", _
'        , , , , RGBMsgBox_WarnColour)
'    If frmVerifyPwd.VerifyEFTPassword(ucKeyPad1.Visible) = True Then  'WIX 1376 - Switch off no Transax
     If True Then
        Call Me.Hide
        lblAction.Caption = "Printing"
        lblStatus.Caption = "Printing"
        enEntryMode = encmCheque
        enConfirmationMode = encmSignatureCon
        
        Call ConfirmPrintedOk
        lblStatus.Caption = "Cheque Complete Successful"
    Else
        fraInsertCard.Visible = True
        lblChequeType.Visible = False
    End If

End Sub 'ChequeNoTransax

Private Sub RecordDataSent(strCommand As String)

Dim rsCheck As New ADODB.Recordset
Dim strMessage As String

    Winsock1.SendData (strCommand)
    If (goSession.GetParameter(PRM_TRACE_EFT) = False) Then Exit Sub
    strMessage = EraseCardNos(strCommand)
    Call CheckEFTLogExists
    If (InStr(strMessage, "Cvv2=") > 0) Then
        strMessage = Left$(strMessage, InStr(strMessage, "Cvv2=") + 5) & "XX" & Mid$(strMessage, InStr(strMessage, "Cvv2=") + 8)
    End If
    If (InStr(strMessage, "CardNumber=") > 0) Then
        strMessage = Left$(strMessage, InStr(strMessage, "CardNumber=") + 11) & "XXXXXXXXXXXX" & Mid$(strMessage, InStr(strMessage, "CardNumber=") + 24)
    End If
    Call goDatabase.ExecuteCommand("INSERT INTO EFTLog (LogDate, LogTime, TillID, EFTTranID, OutData, XMLData) " & _
        " VALUES ({ts '" & Format(Now(), "YYYY-MM-DD 00:00:00") & "'}, '" & Format(Now(), "HHNNSS") & "', '" & _
        goSession.CurrentEnterprise.IEnterprise_WorkstationID & "','" & mstrTransactionNo & "',1,'" & Mid$(strMessage, 3) & "')")
    
End Sub

Private Function EraseCardNos(ByVal strInData As String) As String

Dim strBlank As String

    EraseCardNos = strInData
    If (InStr(EraseCardNos, "Cvv2=") > 0) Then
        EraseCardNos = Left$(EraseCardNos, InStr(EraseCardNos, "Cvv2=") + 5) & "XX" & Mid$(EraseCardNos, InStr(EraseCardNos, "Cvv2=") + 8)
    End If
    If (InStr(EraseCardNos, "CardNumber=") > 0) Then
        EraseCardNos = Left$(EraseCardNos, InStr(EraseCardNos, "CardNumber=") + 11) & "XXXXXXXXXXXX" & Mid$(EraseCardNos, InStr(EraseCardNos, "CardNumber=") + 24)
    End If
    If (InStr(EraseCardNos, "Track1=") > 0) Then
        EraseCardNos = Left$(EraseCardNos, InStr(EraseCardNos, "Track1=") + 7)
        strBlank = Mid$(EraseCardNos, InStr(EraseCardNos, "Track1=") + 8)
        strBlank = "XXXTRACK1XXX" & Mid$(strBlank, InStr(strBlank, """") + 1)
    End If
    If (InStr(EraseCardNos, "Track2=") > 0) Then
        EraseCardNos = Left$(EraseCardNos, InStr(EraseCardNos, "Track2=") + 7)
        strBlank = Mid$(EraseCardNos, InStr(EraseCardNos, "Track2=") + 8)
        strBlank = "XXXTRACK2XXX" & Mid$(strBlank, InStr(strBlank, """") + 1)
    End If
    If (InStr(EraseCardNos, "Track3=") > 0) Then
        EraseCardNos = Left$(EraseCardNos, InStr(EraseCardNos, "Track3=") + 7)
        strBlank = Mid$(EraseCardNos, InStr(EraseCardNos, "Track3=") + 8)
        strBlank = "XXXTRACK3XXX" & Mid$(strBlank, InStr(strBlank, """") + 1)
    End If

End Function

Private Sub RecordDataIn()

Dim strMessage As String

    If (goSession.GetParameter(PRM_TRACE_EFT) = False) Then Exit Sub
    Call CheckEFTLogExists
    strMessage = EraseCardNos(mstrData)
    If (InStr(strMessage, "Cvv2=") > 0) Then
        strMessage = Left$(strMessage, InStr(strMessage, "Cvv2=") + 5) & "XX" & Mid$(strMessage, InStr(strMessage, "Cvv2=") + 8)
    End If
    If (InStr(strMessage, "CardNumber=") > 0) Then
        strMessage = Left$(strMessage, InStr(strMessage, "CardNumber=") + 11) & "XXXXXXXXXXXX" & Mid$(strMessage, InStr(strMessage, "CardNumber=") + 24)
    End If
    Call goDatabase.ExecuteCommand("INSERT INTO EFTLog (LogDate, LogTime, TillID, EFTTranID, OutData, XMLData) " & _
        " VALUES ({ts '" & Format(Now(), "YYYY-MM-DD 00:00:00") & "'}, '" & Format(Now(), "HHNNSS") & "', '" & _
        goSession.CurrentEnterprise.IEnterprise_WorkstationID & "','" & mstrTransactionNo & "',0,'" & Mid$(Replace(strMessage, "'", "`"), 3) & "')")

End Sub

Private Sub RecordDataLog(ByVal strMessage As String)

    If (goSession.GetParameter(PRM_TRACE_EFT) = False) Then Exit Sub
    Call CheckEFTLogExists
    strMessage = EraseCardNos(strMessage)

    Call goDatabase.ExecuteCommand("INSERT INTO EFTLog (LogDate, LogTime, TillID, EFTTranID, OutData, XMLData) " & _
        " VALUES ({ts '" & Format(Now(), "YYYY-MM-DD 00:00:00") & "'}, '" & Format(Now(), "HHNNSS") & "', '" & _
        goSession.CurrentEnterprise.IEnterprise_WorkstationID & "','" & mstrTransactionNo & "',0,'" & strMessage & ":" & Mid$(Replace(mstrData, "'", "`"), 3) & "')")

End Sub

Private Sub CheckEFTLogExists()

Dim rsCheck As New ADODB.Recordset

    On Error Resume Next
    Call rsCheck.Open("SELECT TOP 1 * FROM EFTLog", goDatabase.Connection)
    If Err.Number = -2147217865 Then
        Call CreateEFTTable
    End If
    Call rsCheck.Close
    Call Err.Clear
    On Error GoTo 0

End Sub

Private Sub CreateEFTTable()

    Call goDatabase.ExecuteCommand("CREATE TABLE EFTLog ( Tkey Identity NOT NULL,LogDate Date NOT NULL,LogTime CHAR(6) NOT NULL," & _
        " TillID Char(2) NOT NULL , EFTTranID char(10) NOT NULL, OutData Bit, XMLData longvarchar)")

End Sub
