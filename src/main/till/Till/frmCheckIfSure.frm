VERSION 5.00
Begin VB.Form frmCheckIfSure 
   Caption         =   "Verify Age"
   ClientHeight    =   3015
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4110
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   3015
   ScaleWidth      =   4110
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAccept 
      Caption         =   "Accept"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   240
      TabIndex        =   3
      Top             =   2100
      Width           =   1575
   End
   Begin VB.CommandButton cmdReject 
      Caption         =   "Reject"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   2220
      TabIndex        =   2
      Top             =   2100
      Width           =   1515
   End
   Begin VB.Label lblQuestion 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   " Press <F4> to Accept, <Esc> to Reject Item"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   870
      Left            =   75
      TabIndex        =   1
      Top             =   1020
      Width           =   3960
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblAreYouSure 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Are you sure?"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   855
      TabIndex        =   0
      Top             =   180
      Width           =   2400
   End
End
Attribute VB_Name = "frmCheckIfSure"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Passed As Boolean

Public Event Duress()
Private mintDuressKeyCode As Integer

Private Sub cmdAccept_Click()

    Call Form_KeyDown(vbKeyF4, 0)

End Sub

Private Sub cmdReject_Click()

    Call Form_KeyDown(vbKeyEscape, 0)

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyEscape Then
        Passed = False
        Me.Hide
    End If
    
    If KeyCode = vbKeyF4 Then
        Passed = True
        Me.Hide
    End If
    
End Sub

Private Sub Form_Load()
    Me.BackColor = RGBQuery_BackColour
End Sub
