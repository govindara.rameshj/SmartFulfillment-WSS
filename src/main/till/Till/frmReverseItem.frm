VERSION 5.00
Begin VB.Form frmReverseItem 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Confirm item reversal"
   ClientHeight    =   3945
   ClientLeft      =   945
   ClientTop       =   1890
   ClientWidth     =   9660
   ControlBox      =   0   'False
   Icon            =   "frmReverseItem.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3945
   ScaleWidth      =   9660
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   1320
      TabIndex        =   2
      Top             =   2760
      Width           =   1695
   End
   Begin VB.CommandButton cmdContinue 
      Caption         =   "Continue"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   6000
      TabIndex        =   0
      Top             =   2760
      Width           =   1695
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "SKU"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   6
      Top             =   1080
      Width           =   975
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Desc"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   120
      TabIndex        =   5
      Top             =   1800
      Width           =   1095
   End
   Begin VB.Label lblVoidSKU 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   1320
      TabIndex        =   4
      Top             =   1080
      Width           =   1935
   End
   Begin VB.Label lblVoidDesc 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   1320
      TabIndex        =   3
      Top             =   1800
      Width           =   8055
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "REVERSE ITEM"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   0
      TabIndex        =   1
      Top             =   120
      Width           =   9615
   End
End
Attribute VB_Name = "frmReverseItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmReverseItem
'* Date   : 17/02/03
'* Author : Unknown
'*$Archive: /Projects/OasysV2/VB/Till/frmReverseItem.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 1/06/04 9:58 $ $Revision: 3 $
'* Versions:
'* 17/02/03    Unknown
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmRevereseItem"

Private mDeleteItem As Boolean

Public Event Duress()
Private mintDuressKeyCode As Integer

Public Function ConfirmReversal(ByVal strSKU As String, ByVal strDesc As String) As Boolean

    mDeleteItem = False
    lblVoidSKU.Caption = strSKU
    lblVoidDesc.Caption = strDesc
    Call DebugMsg(MODULE_NAME, "ConfirmReversal", endlDebug, "SKU=" & strSKU & "-" & strDesc)
    Call Me.Show(vbModal)
    ConfirmReversal = mDeleteItem

End Function

Private Sub cmdCancel_Click()

    If (cmdCancel.Visible = True) And (cmdCancel.Enabled = True) Then cmdCancel.SetFocus
    mDeleteItem = False
    Me.Hide
    
End Sub

Private Sub cmdContinue_Click()

    If (cmdCancel.Visible = True) And (cmdContinue.Enabled = True) Then cmdContinue.SetFocus
    mDeleteItem = True
    Me.Hide

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If mintDuressKeyCode <> 0 And KeyCode = mintDuressKeyCode Then
        KeyCode = 0
        RaiseEvent Duress
    End If

End Sub

Private Sub Form_Load()
    
    mintDuressKeyCode = goSession.GetParameter(PRM_KEY_DURESS)
    Me.BackColor = RGBMSGBox_PromptColour
    
End Sub
