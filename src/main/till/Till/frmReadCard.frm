VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.Form frmReadCard 
   Caption         =   "Read Card"
   ClientHeight    =   1680
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   1680
   ScaleWidth      =   4680
   StartUpPosition =   2  'CenterScreen
   Begin MSWinsockLib.Winsock Winsock1 
      Left            =   2940
      Top             =   180
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.Label lblInsert 
      Caption         =   "Label1"
      Height          =   315
      Left            =   60
      TabIndex        =   2
      Top             =   1080
      Width           =   2535
   End
   Begin VB.Label lblAction 
      Caption         =   "Label2"
      Height          =   315
      Left            =   60
      TabIndex        =   1
      Top             =   600
      Width           =   2535
   End
   Begin VB.Label lblStatus 
      Caption         =   "Label1"
      Height          =   315
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   2535
   End
End
Attribute VB_Name = "frmReadCard"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const MODULE_NAME As String = "frmReadCard"

Dim mstrTrack1  As String
Dim mstrTrack2  As String
Dim mstrTrack3  As String

'Constants to hold credit card entry modes
Const ENTRY_ICCS                    As String = "ICC Signed"
Const ENTRY_ICCP                    As String = "ICC"
Const ENTRY_ICC_PIN_BYPASS          As String = "ICC Pin Bypass"
Const ENTRY_KEY                     As String = "Keyed"
Const ENTRY_SWIPE                   As String = "Swipe"
'Verification methods returned from modPayware.ContinueTransaction
Const VERI_NON                      As Integer = 0
Const VERI_SIGNREQ                  As Integer = 1
Const VERI_PINOKSIGNREQ             As Integer = 2
Const VERI_PINSUCESS                As Integer = 3
Const VERI_PINFAILED                As Integer = 4
Const VERI_PINFAILEDSIGNREQ         As Integer = 5
Const VERI_PINBYPASSPOS             As Integer = 6
Const VERI_PINBYPASSCH              As Integer = 7
'Responces returned from modPayware.InitiliseTransaction
Const READ_ICC                      As Integer = 0
Const READ_EMV                      As Integer = 30
Const MANUAL_ENTRY                  As String = "Manual Entry"

Dim madoConn                        As ADODB.Connection
Dim madors                          As ADODB.Recordset

Dim mblnAllowKeyed  As Boolean

Dim mlngUseEFTPOS                   As Long
Dim mblnAuthEntered                 As Boolean
Dim mstrTransactionAmount           As String
Dim mdteTransactionDate             As Date
Dim mstrTransactionNo               As String
Dim mstrCreditCardNum               As String
Dim mstrExpiryDate                  As String
Dim mstrStartDate                   As String
Dim mstrIssueNum                    As String
Dim mstrAuthNum                     As String
Dim mstrTmode                       As String
Dim mstrAuthResponse                As String
Dim mstrAuthType                    As String
Dim mstrTerminalVerificationResult  As String
Dim mstrTransactionStatusInfo       As String
Dim mstrAuthResponceCode            As String
Dim mstrUnpredictableNumber         As String
Dim mstrApplicationIdentifier       As String
Dim mstrCashierName                 As String
Dim mstrEntryMethod                 As String
Dim mstrVerification                As String
Dim mstrAuthSuccess                 As String
Dim mstrAuthServerFound             As String
Dim mstrTransactionType             As String
Dim mstrChequeGuaranteeValue        As String
Dim mstrChequeNo                    As String
Dim mstrSortCode                    As String
Dim mstrAccountNo                   As String
Dim mstrChequeType                  As String
Dim mstrEFTTransID                  As String
Dim mlngAttemptNum                  As Long
Dim mstrFallBackType                As String

' eSocket variables
Const ERR_CONNECTION As Long = 10061

Private mExitWait As Boolean
 
'Action Code responses from eSocket software
Const RESP_TRACK1                       As String = "Track1"
Const RESP_TRACK2                       As String = "Track2"
Const RESP_TRACK3                       As String = "Track3"
Const RESP_TYPE                         As String = "<Esp:"
Const RESP_EVENTID                      As String = "EventId"

Const RESP_RESPONSECODE                 As String = " ResponseCode"
Const RESP_MESSAGEREASONCODE            As String = " MessageReasonCode"
Const RESP_SERVICERESTRICTIONCODE       As String = "ServiceRestrictionCode"
Const RESP_ACTIONCODE                   As String = "ActionCode"

Const RESP_ACTIONCODE_APPROVE           As String = "APPROVE"
Const RESP_ACTIONCODE_DECLINE           As String = "DECLINE"
Const RESP_ACTIONCODE_AUTH              As String = "AUTH"
Const RESP_TYPE_ERROR                   As String = "Error"
Const RESP_TYPE_TRANSACTION             As String = "Transaction"
Const RESP_TYPE_INQUIRY                 As String = "Inquiry"
Const RESP_TYPE_EVENT                   As String = "Event"
Const RESP_TYPE_CALLBACK                As String = "Callback"
Const RESP_TYPE_ADMIN                   As String = "Admin"

Dim mstrResponseType                        As String
Dim mblnSendComplete                        As Boolean
Dim mblnInitiated                           As Boolean
Dim mblnCardDetailsEntered                  As Boolean
Dim mstrData                                As String
Dim mstrCardProductName                     As String
Dim mstrPanEntryMode                        As String
Dim mlngTransactionNumb                     As Long
Dim mstrTID                                 As String

Dim mblnTimedOut    As Boolean
Public moParentForm     As frmDiscountCardScheme
Public IgnoreTimeout    As Boolean

Public Sub ExitWaitNow()

    Call DebugMsg(Me.Name, "ExitWaitNow", endlDebug)
    mExitWait = True
    On Error Resume Next
    If (Winsock1.State = 0) Then Call Winsock1.Close

End Sub


Private Sub WaitForData()

Dim intCounter As Integer

    intCounter = 0

    While (LenB(mstrData) = 0) And (mExitWait = False)
        DoEvents
        intCounter = intCounter + 1
        If intCounter = 30 Then
            Call DebugMsg(Me.Name, "WaitForData", endlDebug, "Waiting for data" & mExitWait)
            intCounter = 0
        End If
    Wend

End Sub 'WaitForData

Private Function ResponseMessage(ByVal strResponseCode As String) As String

Const VAR_RESPONSECODE_NUMB             As Long = 0
Const VAR_RESPONSECODE_MESSAGE          As Long = 1
Const RESPONSE_MESSAGE_FILE             As String = "c:\wix\response codes.txt"

Dim objFileSystem       As Scripting.FileSystemObject
Dim tsMessage           As TextStream
Dim vntResponse         As Variant
Dim strTexthold         As String

    If LenB(strResponseCode) <> 0 Then
        ResponseMessage = vbNullString

        Set objFileSystem = New Scripting.FileSystemObject
        Set tsMessage = objFileSystem.OpenTextFile(RESPONSE_MESSAGE_FILE, ForReading, False, TristateFalse)

        While Not tsMessage.AtEndOfStream And (LenB(ResponseMessage) = 0)
            strTexthold = tsMessage.ReadLine
            vntResponse = Split(strTexthold, ",")
            If vntResponse(VAR_RESPONSECODE_NUMB) = strResponseCode Then
                ResponseMessage = vntResponse(VAR_RESPONSECODE_MESSAGE)
            End If
        Wend
    End If

End Function 'ResponseMessage

Private Function MessageReasonCode(ByVal strMessageReasonCode As String) As String

Const VAR_MESSAGEREASONCODE_NUMB            As Long = 0
Const VAR_MESSAGEREASONCODE_MESSAGE         As Long = 1
Const MESSAGEREASONCODE_FILE                As String = "c:\wix\Message Reason Code.txt"

Dim objFileSystem       As Scripting.FileSystemObject
Dim tsMessage           As TextStream
Dim vntResponse         As Variant
Dim strTexthold         As String

    If LenB(strMessageReasonCode) <> 0 Then
        MessageReasonCode = vbNullString

        Set objFileSystem = New FileSystemObject
        Set tsMessage = objFileSystem.OpenTextFile(MESSAGEREASONCODE_FILE, ForReading, False, TristateFalse)

        While Not tsMessage.AtEndOfStream And (LenB(MessageReasonCode) = 0)
            strTexthold = tsMessage.ReadLine
            vntResponse = Split(strTexthold, ",")
            If vntResponse(VAR_MESSAGEREASONCODE_NUMB) = strMessageReasonCode Then
                MessageReasonCode = vntResponse(VAR_MESSAGEREASONCODE_MESSAGE)
            End If
        Wend
    End If

End Function 'MessageReasonCode

Private Function ExtractResponseType(ByVal strSearchString As String, _
                                  ByVal strAttribute As String) As String

Dim lngStartPos As Long
Dim lngEndPos   As Long

    strSearchString = Mid$(strSearchString, 45)
    lngStartPos = InStr(1, strSearchString, strAttribute)
    lngStartPos = lngStartPos + Len(strAttribute)
    lngEndPos = InStr(lngStartPos, strSearchString, " ")
    ExtractResponseType = Mid$(strSearchString, lngStartPos, lngEndPos - lngStartPos)

End Function 'ExtractResponseType

Private Function ExtractIACAttribute(ByVal strSearchString As String, _
                                  ByVal strAttribute As String) As String

Dim lngStartPos As Long
Dim lngEndPos   As Long

    lngStartPos = InStr(1, strSearchString, strAttribute)
    lngStartPos = lngStartPos + Len(strAttribute & "=")
    ExtractIACAttribute = Mid$(strSearchString, lngStartPos, 10)

End Function 'ExtractIACAttribute

Private Function ExtractTID(ByVal strSearchString As String, _
                                  ByVal strAttribute As String) As String

Dim lngStartPos As Long
Dim lngEndPos   As Long

    lngStartPos = InStr(1, strSearchString, strAttribute)
    lngStartPos = lngStartPos + Len(strAttribute)
    lngEndPos = InStr(lngStartPos, strSearchString, vbCrLf)
    ExtractTID = Mid$(strSearchString, lngStartPos, lngEndPos - lngStartPos)

End Function 'ExtractAttribute

Public Function RetrieveTracks(ByRef strTrack1 As String, _
                          ByRef strTrack2 As String, _
                          ByRef strTrack3 As String, _
                          Optional ByVal AllowKeyedIn As Boolean = False) As Boolean
    
    Call DebugMsg(MODULE_NAME, "RetrieveTracks", endlTraceIn)
    mExitWait = False
    IgnoreTimeout = False
    mstrTrack1 = vbNullString
    mstrTrack2 = vbNullString
    mstrTrack3 = vbNullString
    mblnTimedOut = False
    
    RetrieveTracks = False
    If LenB(mstrTransactionNo) = 0 Then mstrTransactionNo = "1456"
    
    mblnAllowKeyed = AllowKeyedIn
'    mstrTransactionNo = moTranHeader.TransactionNo
'    Call CentreForm(Me)
    Call Connect
    If (Winsock1.State = sckConnected) Then
        RetrieveTracks = True
        Call InitiateCardRead
    End If
    
    strTrack1 = mstrTrack1
    strTrack2 = mstrTrack2
    strTrack3 = mstrTrack3
    
    If (strTrack1 = "") And (strTrack2 = "") And (strTrack3 = "") Then RetrieveTracks = False
    
    If (Me.Visible = True) Then Call Me.Hide
    Call DebugMsg(MODULE_NAME, "RetrieveTracks", endlTraceOut)

End Function 'AuthoriseCheque

Private Sub InitiateCardRead()

Dim strInt                  As String
Dim strTransaction          As String
Dim lngCounter              As Long
Dim intLen                  As Integer
Dim strResponseCode         As String
Dim strMessageReasonCode    As String
Dim strError                As String
Dim strEventType            As String
Dim objFileSystem           As New FileSystemObject
Dim tsMessage               As TextStream
Dim strRead                 As String
Dim strTillID               As String

    
    Call DebugMsg("Initiate", vbNullString, endlDebug, "Opening text file c:\eftauth.par")
    strTillID = goSession.CurrentEnterprise.IEnterprise_WorkstationID
'    strTillID = "11"
    'Get terminal ids from eftauth.par
    Set tsMessage = objFileSystem.OpenTextFile("c:\eftauth.par", ForReading, False, TristateFalse)
    strRead = tsMessage.ReadAll
    
    mstrTID = ExtractTID(strRead, "TILL " & strTillID & "=")
    Call DebugMsg("Initiate", vbNullString, endlDebug, "Till Number = " & mstrTID)
    
    
        lblStatus.Caption = "Initiating system"
        lblAction.Caption = "Initiating system"
        Call DebugMsg(Me.Name, "Initiate", endlDebug, "Initiating system")
        'Clear gobal variable holding the data
        
        mstrData = vbNullString
        strInt = vbNullString
        strInt = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & _
            " encoding=" & Chr(34) & "UTF-8" & Chr(34) & "?>"
              
        strInt = strInt & "<Esp:Interface Version=" & Chr(34) & "1.0" & Chr(34) & _
            " xmlns:Esp=" & Chr(34) & "http://www.mosaicsoftware.com/Postilion/eSocket.POS/" & Chr(34) & _
            "><Esp:Admin TerminalId=" & Chr(34) & mstrTID & Chr(34) & " Action=" & Chr(34) & _
            "INIT" & Chr(34) & "><Esp:Register Type= " & Chr(34) & "EVENT" & Chr(34) & _
            " EventId=" & Chr(34) & "DEBUG_ALL" & Chr(34) & _
            " /></Esp:Admin></Esp:Interface>"
            
        intLen = Len(strInt)
        strInt = Chr(intLen \ 256) & Chr(intLen Mod 256) & strInt
        mblnSendComplete = False
        
        Call DebugMsg(Me.Name, "Initiate", endlDebug, "Sending Initiate Cheque command: " & _
            strInt)
        
        Winsock1.SendData (strInt)
        
        'Loop until WinSck has send all of the message
        While mblnSendComplete = False
            DoEvents
        Wend
        
        'Loop until there is a response from the eSocket software
        Call WaitForData
        
        If (mExitWait = True) Then Exit Sub
        
        While mstrResponseType <> RESP_TYPE_ADMIN
            Call WaitForData
            If (mExitWait = True) Then Exit Sub
            Call DebugMsg(Me.Name, "Initiate Cheque", endlDebug, "Data received: " & _
                mstrData)
            mstrResponseType = ExtractResponseType(mstrData, RESP_TYPE)
            Select Case mstrResponseType
                Case RESP_TYPE_ERROR
                    strResponseCode = ExtractAttribute(mstrData, RESP_RESPONSECODE)
                    strMessageReasonCode = ExtractAttribute(mstrData, RESP_MESSAGEREASONCODE)
                    strError = ExtractAttribute(mstrData, "Description")
                    
                    If MsgBoxEx("WARNING: There was an error whilst trying to initiate the transaction." & vbCrLf & _
                        strError & vbCrLf & _
                        "Response code: " & strResponseCode & " - " & ResponseMessage(strResponseCode) & _
                        vbCrLf & "Message Reason Code: " & strMessageReasonCode & " - " & MessageReasonCode(strMessageReasonCode) & _
                        vbCrLf & "Do you wish to retry" & IIf(mblnAllowKeyed, " or Key In", "") & "?", vbYesNo, "Warning transaction failed", , IIf(mblnAllowKeyed, " or Key In", ""), , , RGBMsgBox_WarnColour) = vbYes Then
                        If (moParentForm Is Nothing = False) Then Call moParentForm.ResetTimer
                        Call InitiateCardRead
                    Else
                        If (Me.Visible = True) Then Me.Hide
                        Exit Sub
                    End If
                    mstrData = vbNullString
                Case Else
            End Select
        Wend
        
        'Determine the response from eSocket Software
        Select Case ExtractAttribute(mstrData, RESP_ACTIONCODE)
            Case RESP_ACTIONCODE_APPROVE
                lblStatus.Caption = "Initiate Card Read Successful"
                Wait (1)
                lblInsert.Caption = ("Please swipe the Colleague Discount Card in Pin Pad")
                lblAction.Caption = "Insert Colleague Discount Card"
                DoEvents
                Call CardReadInquiry
                
            Case RESP_ACTIONCODE_DECLINE
                strResponseCode = ExtractAttribute(mstrData, RESP_RESPONSECODE)
                strMessageReasonCode = ExtractAttribute(mstrData, RESP_MESSAGEREASONCODE)
                
                If MsgBoxEx("WARNING: The Initiate Card Read has been declined" & vbCrLf & _
                "Response code: " & strResponseCode & " - " & ResponseMessage(strResponseCode) & _
                vbCrLf & "Message Reason Code: " & strMessageReasonCode & " - " & MessageReasonCode(strMessageReasonCode) & _
                    vbCrLf & "Do you wish to retry?", vbYesNo, "Warning Initiate Cheque failed", , , , , RGBMsgBox_WarnColour) = vbYes Then
                    If (moParentForm Is Nothing) = False Then Call moParentForm.ResetTimer
                    Call InitiateCardRead
                Else
                    If (Me.Visible) Then Call Me.Hide
                End If
            Case Else
    
        End Select

    
End Sub 'InitiateCardRead

Private Sub CardReadInquiry()

Dim dblTransactionAmount        As Double
Dim strTransaction              As String
Dim intLen                      As Integer
Dim strActionCode               As String
Dim strAuthorisationNumber      As String
Dim strDateTime                 As String
Dim mstrPosCondition            As String
Dim strResponseCode             As String
Dim strMessageReasonCode        As String
Dim strServiceRestrictionCode   As String
Dim strError                    As String
Dim strEventType                As String
Dim strMsgBoxResponse           As String

    Call DebugMsg(MODULE_NAME, "CardReadInquiry", endlTraceIn)
    'Clear gobal variable holding the data
    mstrEntryMethod = vbNullString
    lblStatus.Caption = vbNullString
    
    
    mlngTransactionNumb = mstrTransactionNo & Format(mlngAttemptNum, "00")
    strTransaction = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & _
        " encoding=" & Chr(34) & "UTF-8" & Chr(34) & "?>"

    strTransaction = strTransaction & "<Esp:Interface Version=" & Chr(34) & "1.0" & Chr(34) & _
    " xmlns:Esp=" & Chr(34) & "http://www.mosaicsoftware.com/Postilion/eSocket.POS/" & Chr(34) & _
    "><Esp:Inquiry TerminalId=" & Chr(34) & mstrTID & Chr(34) & _
    " TransactionId=" & Chr(34) & mlngTransactionNumb & Chr(34) & " Type=" & Chr(34) & _
    " CARD_READ" & Chr(34) & _
    " /></Esp:Interface>"
        
    intLen = Len(strTransaction)
    strTransaction = Chr(intLen \ 256) & Chr(intLen Mod 256) & strTransaction
    mblnSendComplete = False
    'Sending Transaction message to eSocket Software
    lblStatus.Caption = "Sending Transaction"
    mstrData = vbNullString
    mstrResponseType = vbNullString
    If (Winsock1.State = 0) Then Exit Sub
    Winsock1.SendData (strTransaction)
    Call DebugMsg(Me.Name, "Transaction", endlDebug, "State:" & Winsock1.State & " Sending Transaction command: " & _
            strTransaction)

    lblStatus.Caption = "Waiting"
    lblAction.Caption = "Waiting"
    While mblnSendComplete = False
        DoEvents
    Wend

     'Loop until there is a response from the eSocket software
    Call WaitForData
    If (mExitWait = True) Then Exit Sub
    Call DebugMsg(Me.Name, "CardReadInquiry", endlDebug, "Data received: " & _
        mstrData)
    lblAction.Caption = "Processing"
    Me.Refresh

    While mstrResponseType <> RESP_TYPE_INQUIRY
        Call WaitForData
        If (mExitWait = True) Then Exit Sub
        Call DebugMsg(Me.Name, "Card Read", endlDebug, "Data received: " & _
            mstrData)
        mstrResponseType = ExtractResponseType(mstrData, RESP_TYPE)
        Select Case mstrResponseType
            Case RESP_TYPE_EVENT
                strEventType = ExtractAttribute(mstrData, RESP_EVENTID)
                lblStatus.Caption = Replace(strEventType, "_", " ")
                Call DebugMsg(Me.Name, "Event received", endlDebug, _
                    "Event received: " & strEventType)
                mstrData = vbNullString
                DoEvents
                Wait (1)

            Case RESP_TYPE_CALLBACK
            Case RESP_TYPE_ERROR
                strResponseCode = ExtractAttribute(mstrData, RESP_RESPONSECODE)
                strError = ExtractAttribute(mstrData, "Description")

                If MsgBoxEx("WARNING: There was an error whilst trying to process the transaction." & vbCrLf & _
                    strError & vbCrLf & _
                    "Response code: " & strResponseCode & " - " & ResponseMessage(strResponseCode) & _
                    vbCrLf & "Message Reason Code: " & strMessageReasonCode & " - " & MessageReasonCode(strMessageReasonCode) & _
                    vbCrLf & "Do you wish to retry" & IIf(mblnAllowKeyed, " or Key In", "") & "?", vbYesNo, "Warning transaction failed", , IIf(mblnAllowKeyed, " or Key In", ""), , , RGBMsgBox_WarnColour) = vbYes Then
                    If (moParentForm Is Nothing) = False Then Call moParentForm.ResetTimer
                    mlngAttemptNum = mlngAttemptNum + 1
                    Call CardReadInquiry
                Else
                    If (Me.Visible) Then Call Me.Hide
                    Exit Sub
                End If

        End Select
    Wend

    'Determine the response from eSocket Software
    Select Case ExtractAttribute(mstrData, RESP_ACTIONCODE)

        Case RESP_ACTIONCODE_DECLINE 'Transaction has been declined
            lblStatus.Caption = "Card Read Inquiry Declined"
            lblAction.Caption = "Declined"
            strResponseCode = ExtractAttribute(mstrData, RESP_RESPONSECODE)
            strMessageReasonCode = ExtractAttribute(mstrData, RESP_MESSAGEREASONCODE)

            If (strMessageReasonCode <> "9635") Or (IgnoreTimeout = False) Then
                strMsgBoxResponse = MsgBoxEx("WARNING: The Card Read Inquiry has been declined" & vbCrLf & _
                    "Response code : " & strResponseCode & " - " & ResponseMessage(strResponseCode) & _
                    vbCrLf & "Message Reason Code: " & strMessageReasonCode & " - " & MessageReasonCode(strMessageReasonCode) _
                    , vbYesNo, "DECLINED", "Retry", IIf(mblnAllowKeyed, "Key", "Cancel"), , , RGBMsgBox_WarnColour)
                    If (strMsgBoxResponse = vbYes) And (moParentForm Is Nothing) = False Then Call moParentForm.ResetTimer
                    mlngAttemptNum = mlngAttemptNum + 1
            End If

            Select Case strMsgBoxResponse
                Case vbYes
                    Call CardReadInquiry
                Case Else
                    mstrTrack1 = vbNullString
                    mstrTrack2 = vbNullString
                    mstrTrack3 = vbNullString
                    mstrAuthNum = vbNullString
                    If (Me.Visible) Then Call Me.Hide
            End Select

        Case RESP_ACTIONCODE_APPROVE 'Transaction has been refered

            'Extract information from responce
            mstrTrack1 = ExtractAttribute(mstrData, RESP_TRACK1)
            mstrTrack2 = ExtractAttribute(mstrData, RESP_TRACK2)
            mstrTrack3 = ExtractAttribute(mstrData, RESP_TRACK3)
            
        Case Else
            strResponseCode = ExtractAttribute(mstrData, RESP_RESPONSECODE)
            strMessageReasonCode = ExtractAttribute(mstrData, RESP_MESSAGEREASONCODE)

            Call MsgBoxEx("WARNING: The Cheque Inquiry has been declined" & vbCrLf & _
                "Response code : " & strResponseCode & " - " & ResponseMessage(strResponseCode) & _
                vbCrLf & "Message Reason Code: " & strMessageReasonCode & " - " & MessageReasonCode(strMessageReasonCode), _
                 vbOKOnly, "Warning Transaction failed", , , , , RGBMsgBox_WarnColour)
            mlngAttemptNum = mlngAttemptNum + 1
            mstrAuthNum = vbNullString
            If (Me.Visible) Then Call Me.Hide

        End Select

End Sub 'CardReadInquiry

Private Sub Connect()

    Winsock1.Close
    If Winsock1.State <> sckConnected Then
        'Connect to the IP address of the machine with the eSocket software on it
        Call DebugMsg("Connect", vbNullString, endlDebug, "Connecting to WinSck")
        If (Winsock1.State <> 0) Then Winsock1.Close
        Call Winsock1.Connect("127.0.0.1", "25000")

        While Winsock1.State = sckConnecting
            DoEvents
        Wend

        Select Case Winsock1.State
            Case sckConnected
            Case sckError
                Call DebugMsg("Connect", vbNullString, endlDebug, "Error-Unloading")
                If (Me.Visible) Then Call Me.Hide
            Case Else
        End Select
    End If

End Sub 'Connect

Private Function ExtractAttribute(ByVal strSearchString As String, _
                                  ByVal strAttribute As String) As String
Dim lngStartPos As Long
Dim lngEndPos   As Long

    lngStartPos = InStr(1, strSearchString, strAttribute)
    lngStartPos = lngStartPos + Len(strAttribute & "=""")
    lngEndPos = InStr(lngStartPos, strSearchString, """")
    ExtractAttribute = Mid$(strSearchString, lngStartPos, lngEndPos - lngStartPos)

End Function 'ExtractAttribute

Private Sub Form_Initialize()
    
    Call DebugMsg(MODULE_NAME, "Form_Initialize", endlDebug)

End Sub

Private Sub Form_Load()

    Call DebugMsg(MODULE_NAME, "Form_Load", endlDebug)
'Dim strTrack1 As String
'Dim strTrack2 As String
'Dim strtrack3 As String

'    Call RetrieveTracks(strTrack1, strTrack2, strtrack3)
    
End Sub 'Form_Load

Private Sub Winsock1_DataArrival(ByVal bytesTotal As Long)

Dim arByte() As Byte
Dim lngMessagesLength As Long

    mstrData = vbNullString
    ReDim arByte(bytesTotal)
    Call Winsock1.GetData(mstrData, , bytesTotal)

    lngMessagesLength = (Asc(Mid$(mstrData, 1, 1)) * 256) + Asc(Mid$(mstrData, 2, 1))

End Sub 'Winsock1_DataArrival

Private Sub Winsock1_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
            
Dim blnRetry As Boolean
    
    Select Case Number
        Case ERR_CONNECTION
            Winsock1.Close
'            Load frmEsocketReload
'            frmEsocketReload.Show vbModal
'            If (frmEsocketReload.Reconnected = True) Then blnRetry = True
'            Unload frmEsocketReload
            If (blnRetry = False) Then
                If (Me.Visible = True) Then Call Me.Hide
            Else
                Call Connect
                Exit Sub
            End If
        Case Else
            Call MsgBoxEx("WARNING: " & Number & " " & Description, vbOKOnly, "WARNING", , , , , RGBMsgBox_WarnColour)
    End Select

End Sub 'Winsock1_Error

Private Sub Winsock1_SendComplete()

    mblnSendComplete = True

End Sub 'Winsock1_SendComplete

Private Function EraseCardNos(ByVal strInData As String) As String

Dim strBlank As String

    EraseCardNos = strInData
    If (InStr(EraseCardNos, "Cvv2=") > 0) Then
        EraseCardNos = Left$(EraseCardNos, InStr(EraseCardNos, "Cvv2=") + 5) & "XX" & Mid$(EraseCardNos, InStr(EraseCardNos, "Cvv2=") + 8)
    End If
    If (InStr(EraseCardNos, "CardNumber=") > 0) Then
        EraseCardNos = Left$(EraseCardNos, InStr(EraseCardNos, "CardNumber=") + 11) & "XXXXXXXXXXXX" & Mid$(EraseCardNos, InStr(EraseCardNos, "CardNumber=") + 24)
    End If
    If (InStr(EraseCardNos, "Track1=") > 0) Then
        EraseCardNos = Left$(EraseCardNos, InStr(EraseCardNos, "Track1=") + 7)
        strBlank = Mid$(EraseCardNos, InStr(EraseCardNos, "Track1=") + 8)
        strBlank = "XXXTRACK1XXX" & Mid$(strBlank, InStr(strBlank, """") + 1)
    End If
    If (InStr(EraseCardNos, "Track2=") > 0) Then
        EraseCardNos = Left$(EraseCardNos, InStr(EraseCardNos, "Track2=") + 7)
        strBlank = Mid$(EraseCardNos, InStr(EraseCardNos, "Track2=") + 8)
        strBlank = "XXXTRACK2XXX" & Mid$(strBlank, InStr(strBlank, """") + 1)
    End If
    If (InStr(EraseCardNos, "Track3=") > 0) Then
        EraseCardNos = Left$(EraseCardNos, InStr(EraseCardNos, "Track3=") + 7)
        strBlank = Mid$(EraseCardNos, InStr(EraseCardNos, "Track3=") + 8)
        strBlank = "XXXTRACK3XXX" & Mid$(strBlank, InStr(strBlank, """") + 1)
    End If

End Function




