VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TillFormLive"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Implements IProcessingPayment
Implements ITrainingModeOrders
Implements IBuyCoupon
Implements IGetCoupon
Implements IDotnetOfflineMode
Implements IRefundTenderType
Implements IPICRefundCountProcess
Implements IRepeatPayment

' IProcessingPayment Implementation
Private Sub IProcessingPayment_SetPaymentAuthorisationType(ByRef Payment As cSale_Wickes.cPOSPayment, ByVal EntryMethod As String)

    If Left$(EntryMethod, 5) <> "Keyed" Then
        Payment.CCNumberKeyed = False
        Payment.AuthorisationType = "O"
    Else
        Payment.CCNumberKeyed = True
        Payment.AuthorisationType = "R"
    End If
End Sub

Private Function IProcessingPayment_InProcessPaymentTypeMethod() As Boolean

    IProcessingPayment_InProcessPaymentTypeMethod = False
End Function

Private Sub IProcessingPayment_SetForInProcessPaymentType()

End Sub

Private Sub IProcessingPayment_SetForOutOfProcessPaymentType()

End Sub
' End of IProcessingPayment Implementation

' IRepeatPayment Implementation
Private Function IRepeatPayment_ShouldCancelRepeatPayment() As Boolean

    Call DebugMsg("TillFormLive", "ShouldCancelRepeatPayment", endlTraceIn)
    Call DebugMsg("TillFormLive", "ShouldCancelRepeatPayment", endlDebug, "Returning FAlse")
    IRepeatPayment_ShouldCancelRepeatPayment = False
    Call DebugMsg("TillFormLive", "ShouldCancelRepeatPayment", endlTraceOut)
End Function
' End of IRepeatPayment Implementation

' ITrainingModeOrders Implementation
Private Function ITrainingModeOrders_DisableTrainingModeOrders() As Boolean

    ITrainingModeOrders_DisableTrainingModeOrders = False
End Function
' End of ITrainingModeOrders Implementation

' IBuyCoupon Implementation
Private Function IBuyCoupon_EnableBuyCoupon() As Boolean

    IBuyCoupon_EnableBuyCoupon = False
End Function
' End of IBuyCoupon Implementation

' IGetCoupon Implementation
Private Function IGetCoupon_EnableGetCoupon() As Boolean

    IGetCoupon_EnableGetCoupon = False
End Function
' End of IGetCoupon Implementation

' IDotnetOfflineMode Implementation
Private Function IDotnetOfflineMode_EnableOfflineModeSetting() As Boolean

    IDotnetOfflineMode_EnableOfflineModeSetting = False
End Function
' End of IDotnetOfflineMode Implementation

' IRefundTenderType Implementation
Private Function IRefundTenderType_EnableRefundTenderType() As Boolean

    IRefundTenderType_EnableRefundTenderType = False
End Function
' End of IRefundTenderType Implementation

' IPICRefundCountProcess Implementation
Private Function IPICRefundCountProcess_EnableNewProcess() As Boolean

    IPICRefundCountProcess_EnableNewProcess = False
End Function
' End of IPICRefundCountProcess Implementation

