VERSION 5.00
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Object = "{FA666EFA-0D22-45D3-9849-6B0694037D1B}#2.1#0"; "EditNumberTill.ocx"
Begin VB.Form frmTranDisc 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Transaction Discount"
   ClientHeight    =   3870
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10425
   Icon            =   "frmTranDisc.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3870
   ScaleWidth      =   10425
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   0
      Top             =   3780
      Visible         =   0   'False
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   7020
      TabIndex        =   9
      Top             =   3120
      Width           =   2415
   End
   Begin VB.CommandButton cmdSelectItems 
      Caption         =   "Select Items"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4140
      TabIndex        =   8
      Top             =   3120
      Width           =   2715
   End
   Begin VB.CommandButton cmdApply 
      Caption         =   "Apply Discount"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   840
      TabIndex        =   7
      Top             =   3120
      Width           =   3135
   End
   Begin EditNumberTill.ucTillNumberText ntxtTotal 
      Height          =   435
      Left            =   8700
      TabIndex        =   4
      Top             =   1200
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   767
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinimumValue    =   0
   End
   Begin EditNumberTill.ucTillNumberText ntxtDiscValue 
      Height          =   435
      Left            =   8700
      TabIndex        =   5
      Top             =   1800
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   767
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinimumValue    =   0
   End
   Begin EditNumberTill.ucTillNumberText ntxtDiscRate 
      Height          =   435
      Left            =   9180
      TabIndex        =   6
      Top             =   2400
      Width           =   1035
      _ExtentX        =   1826
      _ExtentY        =   767
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinimumValue    =   0
      MaximumValue    =   100
   End
   Begin VB.Label lblOrigAmount 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Original Transaction Amount : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   60
      TabIndex        =   10
      Top             =   600
      Width           =   10335
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Specifiy a percentage discount to remove from the current total"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   180
      TabIndex        =   3
      Top             =   2460
      Width           =   8955
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Specify a value discount to remove from the current total."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   180
      TabIndex        =   2
      Top             =   1860
      Width           =   8355
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Amend the transaction total to a lesser value"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   180
      TabIndex        =   1
      Top             =   1260
      Width           =   7935
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Enter transaction discount"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   2820
      TabIndex        =   0
      Top             =   120
      Width           =   4815
   End
End
Attribute VB_Name = "frmTranDisc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mblnCancelled As Boolean
Private mcurOrigAmount As Currency
Public mlngTranDiscSupLevel As Long 'from PRM879 - passed in, level at which Supervisor must be given for Transaction Discount
Public mlngTranDiscMgrLevel As Long 'from PRM879 - Passed in, level at which Manager must be given for Transaction Discount


Dim mlngCloseKeyCode As Long
Dim mlngUseKeyCode   As Long
Dim mlngItemsKeyCode As Long
Dim mstrAllocItems() As String


Public Function RecordDiscount(ByVal TranTotal As Currency, strAllocItems() As String, dblDiscRate As Double) As Boolean

    ntxtTotal.AllowInvert = False
    ntxtTotal.MaximumValue = TranTotal
    ntxtTotal.Value = TranTotal
    
    ntxtDiscRate.AllowInvert = False
    ntxtDiscRate.MaximumValue = 100
    ntxtDiscRate.Value = 0
    
    ntxtDiscValue.AllowInvert = False
    ntxtDiscValue.MaximumValue = TranTotal
    ntxtDiscValue.Value = 0
    
    ReDim mstrAllocItems(0)
    mcurOrigAmount = TranTotal
    lblOrigAmount.Caption = lblOrigAmount.Caption & Format(TranTotal, "0.00")
    Call Me.Show(vbModal)
    strAllocItems = mstrAllocItems
    dblDiscRate = ntxtDiscRate.Value
    RecordDiscount = Not mblnCancelled

End Function

Private Sub cmdApply_Click()
    
    If (ntxtDiscValue.Value = 0) Then
        Call MsgBoxEx("Invalid discount entered - unable to proceed.", vbExclamation, "Apply Discount Cancelled", , , , , RGBMsgBox_WarnColour)
        Exit Sub
    End If
    If CheckAuthorisation(ntxtDiscValue.Value) = False Then
        Call MsgBoxEx("Unable to get authorisation for discount level" & vbNewLine & "Apply Discount Cancelled.", vbOKOnly, "Apply Discount Cancelled", , , , , RGBMsgBox_WarnColour)
        Exit Sub
    End If
    mblnCancelled = False
    Call Me.Hide

End Sub

Private Sub cmdApply_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyEscape) Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub cmdCancel_Click()
        
    mblnCancelled = True
    Call Me.Hide

End Sub

Private Sub cmdSelectItems_Click()

Dim lngRowNo    As Long
Dim strDesc     As String
Dim lngQuantity As Long
Dim curSelectedValue As Currency
Dim curNewTotal     As Currency

    If (ntxtDiscValue.Value = 0) Then
        Call MsgBoxEx("Invalid discount entered - unable to select items.", vbExclamation, "Apply Discount Cancelled", , , , , RGBMsgBox_WarnColour)
        Exit Sub
    End If
    If CheckAuthorisation(ntxtDiscValue.Value) = False Then
        Call MsgBoxEx("Unable to get authorisation for discount level" & vbNewLine & "Item selection cancelled.", vbOKOnly, "Apply Discount Cancelled", , , , , RGBMsgBox_WarnColour)
        Exit Sub
    End If
        
    Load frmBackDoorCollect
    For lngRowNo = 2 To frmTill.sprdLines.MaxRows Step 1
        frmTill.sprdLines.Row = lngRowNo
        frmTill.sprdLines.Col = COL_LINETYPE
        If (frmTill.sprdLines.Value = LT_ITEM) Then
            frmTill.sprdLines.Col = COL_VOIDED
            If (Val(frmTill.sprdLines.Value) = 0) Then
                frmTill.sprdLines.Col = COL_QTY
                If (Val(frmTill.sprdLines.Value) > 0) Then
                    lngQuantity = Val(frmTill.sprdLines.Value)
                    frmTill.sprdLines.Col = COL_DESC
                    strDesc = frmTill.sprdLines.Text
                    frmTill.sprdLines.Col = COL_INCTOTAL
                    curNewTotal = frmTill.sprdLines.Text
                    frmTill.sprdLines.Col = COL_PARTCODE
                    Call frmBackDoorCollect.AddItem(lngRowNo, frmTill.sprdLines.Text, strDesc, "" & lngQuantity, "", "", "", "", curNewTotal)
                End If
            End If
        End If
    Next lngRowNo
    
    If (frmBackDoorCollect.AllocateDiscount(ntxtDiscValue.Value, mstrAllocItems, curSelectedValue) = True) Then
        Call frmBackDoorCollect.PopulateTakeNow
        Unload frmBackDoorCollect
        curNewTotal = ntxtDiscValue.Value
        ntxtDiscRate.Value = Format(curNewTotal / curSelectedValue * 100, "0.00")
        Me.Hide
    Else
        Unload frmBackDoorCollect
    End If

End Sub

Private Sub Form_Load()

Dim strKey As String

    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    
    strKey = goSession.GetParameter(PRM_KEY_CLOSE)
    cmdCancel.Caption = strKey & "-" & cmdCancel.Caption
    
    If Left$(strKey, 1) = "F" Then
        mlngCloseKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngCloseKeyCode = AscW(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_SAVE)
    cmdApply.Caption = strKey & "-" & cmdApply.Caption
    
    If Left$(strKey, 1) = "F" Then
        mlngUseKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngUseKeyCode = AscW(UCase$(strKey))
    End If
    
    strKey = "F8"
    cmdSelectItems.Caption = strKey & "-" & cmdSelectItems.Caption
    
    If Left$(strKey, 1) = "F" Then
        mlngItemsKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngItemsKeyCode = AscW(UCase$(strKey))
    End If
    
    Call CentreForm(Me)

End Sub

Private Sub ntxtDiscRate_Change()

    If (ActiveControl Is Nothing = False) Then
        If (ActiveControl.Name = "ntxtDiscRate") Then Call CalculateDiscounts(2)
    End If

End Sub

Private Sub ntxtDiscValue_Change()
    
    If (ActiveControl Is Nothing = False) Then
        If (ActiveControl.Name = "ntxtDiscValue") Then Call CalculateDiscounts(1)
    End If

End Sub

Private Sub ntxtTotal_Change()
    
    If (ActiveControl Is Nothing = False) Then
        If (ActiveControl.Name = "ntxtTotal") Then Call CalculateDiscounts(0)
    End If

End Sub

Private Sub ntxtTotal_GotFocus()

    ntxtTotal.SelectAllValue
    ntxtTotal.BackColor = RGBEdit_Colour

End Sub 'ntxtTotal_GotFocus

Private Sub ntxtTotal_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        mblnCancelled = True
        Call Me.Hide
    End If

End Sub 'ntxtTotal_KeyPress

Private Sub ntxtTotal_LostFocus()

    ntxtTotal.BackColor = RGB_WHITE
    
End Sub 'ntxtTotal_LostFocus

Private Sub ntxtDiscValue_GotFocus()

    ntxtDiscValue.SelectAllValue
    ntxtDiscValue.BackColor = RGBEdit_Colour

End Sub 'ntxtDiscValue_GotFocus

Private Sub ntxtDiscValue_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub 'ntxtDiscValue_KeyPress

Private Sub ntxtDiscValue_LostFocus()

    ntxtDiscValue.BackColor = RGB_WHITE
    
End Sub 'ntxtDiscValue_LostFocus

Private Sub ntxtDiscRate_GotFocus()

    ntxtDiscRate.SelectAllValue
    ntxtDiscRate.BackColor = RGBEdit_Colour

End Sub 'ntxtDiscRate_GotFocus

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

Dim lngKeyIn As Integer
Dim lngRowNo As Integer
    
    Call DebugMsg(vbNullString, vbNullString, endlDebug, "KDn" & KeyCode & "/" & Shift)
    
    If (Shift = 0) Then
        Select Case (KeyCode)
            Case (mlngCloseKeyCode):
                    cmdCancel.Value = True
                    KeyCode = 0
                    Exit Sub
            Case (mlngUseKeyCode):
                    cmdApply.Value = True
                    KeyCode = 0
                    Exit Sub
            Case (mlngItemsKeyCode):
                    cmdSelectItems.Value = True
                    KeyCode = 0
                    Exit Sub
        End Select
    End If

End Sub

Private Sub ntxtDiscRate_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub 'ntxtDiscRate_KeyPress

Private Sub ntxtDiscRate_LostFocus()

    ntxtDiscRate.BackColor = RGB_WHITE
    
End Sub 'ntxtDiscRate_LostFocus


Private Sub CalculateDiscounts(EditingField As Integer)


Dim curOrigTotal As Currency
Dim curNewTotal As Currency
Dim curDiscValue As Currency
Dim curDiscRate As Currency

    If (frmTranDisc.Visible = False) Then Exit Sub

    curDiscRate = ntxtDiscRate.Value
    curDiscValue = ntxtDiscValue.Value
    curNewTotal = ntxtTotal.Value
    curOrigTotal = mcurOrigAmount
    Select Case (EditingField)
        Case (0): ntxtDiscValue.Value = curOrigTotal - curNewTotal
                  curDiscValue = ntxtDiscValue.Value
                  ntxtDiscRate.Value = Format(((curOrigTotal - curNewTotal) / curOrigTotal) * 100, "0.00")
        Case (1): ntxtTotal.Value = curOrigTotal - curDiscValue
                  curNewTotal = ntxtTotal.Value
                  ntxtDiscRate.Value = Format((curOrigTotal - curNewTotal) / curOrigTotal * 100, "0.00")
        Case (2): ntxtDiscValue.Value = curOrigTotal * (curDiscRate / 100)
                  curDiscValue = ntxtDiscValue.Value
                  ntxtTotal.Value = curOrigTotal - curDiscValue
    End Select

End Sub

Private Function CheckAuthorisation(ByVal curDiscountTotal As Currency)

    CheckAuthorisation = True
    
    If (mlngTranDiscMgrLevel = 0) And (mlngTranDiscSupLevel = 0) Then Exit Function
    If (curDiscountTotal >= mlngTranDiscMgrLevel) Then
        If frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) = False Then
            CheckAuthorisation = False
        Else
            moTranHeader.SupervisorUsed = True
            moTranHeader.RefundManager = frmVerifyPwd.txtAuthID.Text
        End If
        Unload frmVerifyPwd
        
    ElseIf (curDiscountTotal >= mlngTranDiscSupLevel) Then
        'Refund amount is above Supervisor Level or a non-validated receipt so get Authorisation
        If frmVerifyPwd.VerifySupervisorPassword(ucKeyPad1.Visible) = False Then
            CheckAuthorisation = False
        Else
            moTranHeader.SupervisorUsed = True
            moTranHeader.RefundSupervisor = frmVerifyPwd.txtAuthID.Text
        End If
        Unload frmVerifyPwd
    End If 'else don't need auth for this level, or are no refund lines
    
End Function



