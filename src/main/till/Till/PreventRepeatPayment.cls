VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PreventRepeatPayment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements IRepeatPayment

Private Function IRepeatPayment_ShouldCancelRepeatPayment() As Boolean

    Call DebugMsg("PreventRepeatPayment", "ShouldCancelRepeatPayment", endlTraceIn)
    Call DebugMsg("PreventRepeatPayment", "ShouldCancelRepeatPayment", endlDebug, "Returning True")
    IRepeatPayment_ShouldCancelRepeatPayment = True
    Call DebugMsg("PreventRepeatPayment", "ShouldCancelRepeatPayment", endlTraceOut)
End Function
