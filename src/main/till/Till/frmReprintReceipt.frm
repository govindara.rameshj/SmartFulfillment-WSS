VERSION 5.00
Object = "{CCB90150-B81E-11D2-AB74-0040054C3719}#1.0#0"; "OPOSPOSPrinter.ocx"
Object = "{6514F5A0-641C-11D2-9FD0-0020AF131A57}#3.0#0"; "fpFlp30.ocx"
Object = "{9BA18739-054D-4172-8E42-118133CE2FC4}#1.0#0"; "EditDateCtl.ocx"
Begin VB.Form frmReprintReceipt 
   Caption         =   "Reprint Receipt"
   ClientHeight    =   6825
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6810
   ControlBox      =   0   'False
   Icon            =   "frmReprintReceipt.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6825
   ScaleWidth      =   6810
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox fraTranDetails 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   6615
      Left            =   120
      ScaleHeight     =   6615
      ScaleWidth      =   6615
      TabIndex        =   3
      Top             =   120
      Width           =   6615
      Begin VB.TextBox txtTranNo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   3600
         MaxLength       =   4
         TabIndex        =   9
         Text            =   "111"
         Top             =   2100
         Width           =   1695
      End
      Begin VB.TextBox txtTill 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   3600
         MaxLength       =   2
         TabIndex        =   8
         Top             =   1140
         Width           =   1695
      End
      Begin VB.CommandButton cmdRetrieve 
         Caption         =   "Retrieve"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   2400
         TabIndex        =   7
         Top             =   5700
         Width           =   1455
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Cancel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   720
         TabIndex        =   6
         Top             =   5700
         Width           =   1455
      End
      Begin VB.CommandButton cmdRetrieveAll 
         Caption         =   "Retrieve All"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   4080
         TabIndex        =   5
         Top             =   5700
         Width           =   1695
      End
      Begin ucEditDate.ucDateText txtDate 
         Height          =   615
         Left            =   3600
         TabIndex        =   4
         Top             =   240
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   1085
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DateFormat      =   "DD/MM/YY"
         Text            =   ""
      End
      Begin OposPOSPrinter_1_9_LibCtl.OPOSPOSPrinter OPOSPOSPrinter1 
         Left            =   720
         OleObjectBlob   =   "frmReprintReceipt.frx":058A
         Top             =   3480
      End
      Begin VB.Label lblDate 
         BackStyle       =   0  'Transparent
         Caption         =   "Date"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   480
         TabIndex        =   12
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label lblTill 
         BackStyle       =   0  'Transparent
         Caption         =   "Till Num"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   480
         TabIndex        =   11
         Top             =   1200
         Width           =   2775
      End
      Begin VB.Label lblTran 
         BackStyle       =   0  'Transparent
         Caption         =   "Transaction Num"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   480
         TabIndex        =   10
         Top             =   2160
         Width           =   3015
      End
   End
   Begin VB.PictureBox fraDisplayTran 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   6615
      Left            =   120
      ScaleHeight     =   6615
      ScaleWidth      =   6615
      TabIndex        =   0
      Top             =   120
      Width           =   6615
      Begin LpADOLib.fpListAdo lstMatches 
         Height          =   4815
         Left            =   180
         TabIndex        =   1
         Top             =   300
         Width           =   6135
         _Version        =   196608
         _ExtentX        =   10821
         _ExtentY        =   8493
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   0   'False
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Columns         =   4
         Sorted          =   0
         LineWidth       =   1
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         MultiSelect     =   0
         WrapList        =   0   'False
         WrapWidth       =   0
         SelMax          =   -1
         AutoSearch      =   1
         SearchMethod    =   0
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   1
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   2
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   -1  'True
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         DataField       =   ""
         DataMember      =   ""
         OLEDragMode     =   0
         OLEDropMode     =   0
         EnableClickEvent=   -1  'True
         Redraw          =   -1  'True
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         ColDesigner     =   "frmReprintReceipt.frx":05AE
      End
      Begin VB.CommandButton cmdReprintReceipt 
         Caption         =   "Reprint Receipt"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   180
         TabIndex        =   2
         Top             =   5700
         Width           =   2295
      End
   End
   Begin VB.PictureBox OPOSPrinter1 
      Height          =   480
      Left            =   1980
      ScaleHeight     =   420
      ScaleWidth      =   1140
      TabIndex        =   13
      Top             =   1980
      Width           =   1200
   End
End
Attribute VB_Name = "frmReprintReceipt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim moTillTran      As cPOSHeader
Dim moSysopt        As cSystemOptions
Dim mstrDateFmt     As String
Dim mdteDate        As String
Dim mstrTill        As String
Dim mstrTranNo      As String

'Head Office Address
Private mstrHeadofficename          As String
Private mstrHeadOfficeAddress       As String
Private mstrHeadOfficePhoneNo       As String
Private mstrHeadOfficeFaxNumber     As String
Private mstrVATNumber               As String

'Account Details
Private mstrAccountNum              As String
Private mstrAccountName             As String
Private mstrActive                  As String
Private mstrCardExpiry              As String
Private mstrAddressLine1            As String
Private mstrAddressLine2            As String
Private mstrAddressLine3            As String
Private mstrPostCode                As String
Private mstrHomeTelNumber           As String
Private mstrDeliveryName            As String
Private mstrDeliveryAddressLine1    As String
Private mstrDeliveryAddressLine2    As String
Private mstrDeliveryAddressLine3    As String
Private mstrDeliveryPostCode        As String
Private mstrDeliveryWorkTelNumber   As String
Private mstrDeliveryHomeTelNumber   As String
Private mstrDateAccountOpened       As String
Private mstrStopped                 As String
Private mstrCreditLimit             As String
Private mstrAccountBalance          As String
Private mstrSoldToday               As String
Private mstrTend                    As String
Private mstrInvoiceNumber           As String

Public Event Duress()
Private mintDuressKeyCode As Integer

Private Sub cmdCancel_Click()

    If (cmdCancel.Visible = True) And (cmdCancel.Enabled = True) Then cmdCancel.SetFocus
    Me.Hide

End Sub 'cmdCancel_click

Private Sub cmdReprintReceipt_Click()
          
Dim astrData()  As String
Dim strTranType As String
Dim strName     As String
Dim colLines    As Collection
Dim lngLineNo   As Long
    
    If (cmdReprintReceipt.Visible = True) And (cmdReprintReceipt.Enabled = True) Then cmdReprintReceipt.SetFocus
    astrData = Split(lstMatches.List(lstMatches.ListIndex), vbTab)
    mdteDate = astrData(0)
    mstrTill = astrData(1)
    mstrTranNo = astrData(2)
    
    'Retrieve DLTOTS record for printing header
    Call moTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, mdteDate)
    Call moTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, mstrTill)
    Call moTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, mstrTranNo)
    Call moTillTran.AddLoadField(FID_POSHEADER_TransactionTime)
    Call moTillTran.AddLoadField(FID_POSHEADER_TransactionCode)
    Call moTillTran.AddLoadField(FID_POSHEADER_CashierID)
    Call moTillTran.AddLoadField(FID_POSHEADER_CustomerAcctNo)
    Call moTillTran.AddLoadField(FID_POSHEADER_TotalSaleAmount)
    Call moTillTran.LoadMatches
          
    'Retrieve Cashier Name
    strName = GetFullName(moTillTran.CashierID)
    
    'Retrieve Customer Number
    mstrAccountNum = moTillTran.CustomerAcctNo
    
    'Determine the type of transaction
    If mstrAccountNum <> "00000000" Then
        Select Case (moTillTran.TransactionCode)
            Case TT_SALE
                strTranType = "INVOICE - DUPLICATE RECEIPT"
            Case TT_REFUND
                strTranType = "CREDIT NOTE - DUPLICATE RECEIPT"
        End Select
    Else
        Select Case (moTillTran.TransactionCode)
            Case TT_SALE
                strTranType = "SALE - DUPLICATE RECEIPT"
            Case TT_REFUND
                strTranType = "REFUND - DUPLICATE RECEIPT"
        End Select
    End If
    
    Call modReceipt.PrintTransactionHeader(frmTill.OPOSPrinter1, moTillTran.TranDate, _
                moTillTran.TransactionTime, moTillTran.TillID, moTillTran.TransactionNo, _
                strTranType, moTillTran.CashierID, strName, vbNullString)
    
    If mstrAccountNum <> "00000000" Then
        Call PrintInvoiceDetails
    End If
    
    'Print the Transaction Lines
    Set colLines = moTillTran.Lines
    For lngLineNo = 1 To colLines.Count Step 1
        With colLines(CLng(lngLineNo))
            Call PrintLine(frmTill.OPOSPrinter1, .PartCode, .Description, vbNullString, .QuantitySold, _
                .ExtendedValue / .QuantitySold, .ExtendedValue, .VATCode, False)

        End With
    Next lngLineNo
    
'    If lenb(moTillTran.DiscountReasonCode) <> 0 Then
'        For lngPPPos = 1 To moTillTran.GetPricePromiseLine.Count Step 1
'            If (moTranHeader.GetPricePromiseLine(CLng(lngPPPos)).SequenceNo = .SequenceNo) Then
'                Call PrintDiscountLine(frmTill.OPOSPrinter1, "Price Promise", moTranHeader.GetPricePromiseLine(CLng(lngPPPos)).DiscTotal, vbNullString)
'                Exit For
'            End If
'        Next lngPPPos
'    End If

    Call PrintTranTotal(frmTill.OPOSPrinter1, vbNullString, moTillTran.TotalSaleAmount, True)
    
    If mstrAccountNum <> "00000000" Then
        Select Case moTillTran.TransactionCode
            Case TT_SALE
                Call PrintInvoiceTrailer1(frmTill.OPOSPrinter1)
            Case TT_REFUND
                Call PrintInvoiceTrailer1Refund(frmTill.OPOSPrinter1)
        End Select
        'Call PrintInvoiceTrailer2(frmTill.OPOSPrinter1)
        Call PrintInvoiceTrailer3(frmTill.OPOSPrinter1)
    End If
    
    'Print Footer
    'Call PrintDivideLine(frmTill.OPOSPrinter1)
    Call PrintFooter(frmTill.OPOSPrinter1)
    
    Unload Me


End Sub 'CmdReprintReceipt

Private Sub cmdRetrieve_Click()

    If (cmdRetrieve.Visible = True) And (cmdRetrieve.Enabled = True) Then cmdRetrieve.SetFocus
    Call lstMatches.Clear
    
    mdteDate = GetDate(txtDate.Text)
    mstrTill = txtTill.Text
    mstrTranNo = txtTranNo.Text

    Set moTillTran = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)

    Call moTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, mdteDate)
    Call moTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, mstrTill)
    Call moTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, mstrTranNo)
    Call moTillTran.AddLoadField(FID_POSHEADER_TransactionCode)
    Call moTillTran.LoadMatches
    
    fraTranDetails.Visible = False
    fraDisplayTran.Visible = True
    
    Call lstMatches.AddItem(moTillTran.TranDate & vbTab & moTillTran.TillID & vbTab & _
                moTillTran.TransactionNo & vbTab & moTillTran.TransactionCode)
    If (lstMatches.Visible = True) And (lstMatches.Enabled = True) Then lstMatches.SetFocus
    lstMatches.ListIndex = 0

End Sub 'cmdRetrieve_Click

Private Sub cmdRetrieveAll_Click()

Dim lngRecordCount  As Long
Dim oRow            As IRowSelector
Dim oField          As IField

    If (cmdRetrieveAll.Visible = True) And (cmdRetrieveAll.Enabled = True) Then cmdRetrieveAll.SetFocus
    Set moTillTran = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    Set oRow = goSession.Root.CreateUtilityObject("CRowSelector")
    
    'Create field for selection criteria Key into
    Set oField = moTillTran.IBo_GetField(FID_POSHEADER_TransactionCode)
    If oField Is Nothing Then Exit Sub
    oField.ValueAsVariant = TT_SIGNOFF
    Call oRow.AddSelection(CMP_NOTEQUAL, oField)
    'Create field for selection criteria Key into
    Set oField = moTillTran.GetField(FID_POSHEADER_TransactionCode)
    If oField Is Nothing Then Exit Sub
    oField.ValueAsVariant = TT_SIGNON
    Call oRow.AddSelection(CMP_NOTEQUAL, oField)
        'Create field for selection criteria Key into
    Set oField = moTillTran.GetField(FID_POSHEADER_Voided)
    If oField Is Nothing Then Exit Sub
    oField.ValueAsVariant = False
    Call oRow.AddSelection(CMP_EQUAL, oField)
    
    lngRecordCount = goSession.Database.GetAggregateValue(AGG_COUNT, moTillTran.GetField(FID_POSHEADER_TranDate), oRow)
    
    If MsgBoxEx("WARNING: If you perform this function it will retrieve " & lngRecordCount & _
        " records from the database. Are you sure?", vbYesNo, "Warning", , , , , RGBMsgBox_WarnColour) = vbYes Then
        Call moTillTran.AddLoadField(FID_POSHEADER_TransactionCode)
        Call moTillTran.LoadMatches
        fraTranDetails.Visible = False
        lstMatches.Visible = True
    End If

End Sub 'cmdRetrieveAll_Click

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If mintDuressKeyCode <> 0 And KeyCode = mintDuressKeyCode Then
        KeyCode = 0
        RaiseEvent Duress
    End If
    
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

    If fraTranDetails.Visible = True Then
        'If (KeyAscii = vbKeyReturn) Then Call cmdRetrieve_Click
        If (KeyAscii = vbKeyEscape) Then Me.Hide
    Else
        If (KeyAscii = vbKeyReturn) Then cmdReprintReceipt.Value = True
        If (KeyAscii = vbKeyEscape) Then
            
            txtTill = vbNullString
            txtTranNo = vbNullString
            fraDisplayTran.Visible = False
            fraTranDetails.Visible = True
        End If
    
    End If
    

End Sub

Private Sub Form_Load()

    mstrDateFmt = UCase$(goSession.GetParameter(PRM_DATE_FORMAT))
    txtDate.DateFormat = mstrDateFmt
    txtDate.Text = Format$(Date, mstrDateFmt)
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    fraTranDetails.BackColor = Me.BackColor
    fraDisplayTran.BackColor = Me.BackColor
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
    RGBMSGBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
    'Call GetPrinterHeaderTrailer(frmTill.OPOSPrinter1)

End Sub 'From_Load


Private Sub txtTranNo_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then cmdRetrieve.Value = True
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub txtDate_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then Call SendKeys(vbTab)
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub txtTill_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then Call SendKeys(vbTab)
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub txtTill_GotFocus()

    txtTill.BackColor = RGBEdit_Colour
    
End Sub

Private Sub txtTill_LostFocus()

    txtTill.BackColor = RGB_WHITE

End Sub

Private Sub txtTranNo_GotFocus()

    txtTranNo.BackColor = RGBEdit_Colour
    
End Sub

Private Sub txtTranNo_LostFocus()

    txtTranNo.BackColor = RGB_WHITE

End Sub

Private Sub txtDate_GotFocus()

    txtDate.BackColor = RGBEdit_Colour
    
End Sub

Private Sub txtDate_LostFocus()

    txtDate.BackColor = RGB_WHITE

End Sub

Public Sub RetrieveTranDetails()

    fraTranDetails.Visible = True
    fraDisplayTran.Visible = False

    Call Initialise
    Call Me.Show(vbModal)
    
End Sub 'RetrieveTranDetails

Private Sub Initialise()

    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    RGBQuery_BackColour = goSession.GetParameter(PRM_QUERY_BACKCOLOUR)
    lstMatches.BackColor = goSession.GetParameter(PRM_QUERY_GRIDEVENCOLOUR)
    lstMatches.ListApplyTo = ListApplyToEvenRows
    lstMatches.BackColor = goSession.GetParameter(PRM_QUERY_GRIDEVENCOLOUR)
    lstMatches.ListApplyTo = ListApplyToOddRows
    lstMatches.BackColor = goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR)
    lstMatches.Row = -1
    lstMatches.RowHeight = goSession.GetParameter(PRM_QUERY_ROWHEIGHT)

End Sub 'Initialise

Private Sub PrintInvoiceDetails()

    'Print Information for an account sale invoice
    Call RetrieveHeadOfficeAddress
    Call PrintAccountDepartmentAddress(frmTill.OPOSPrinter1, mstrHeadofficename, _
        mstrHeadOfficeAddress, mstrHeadOfficePhoneNo, mstrHeadOfficeFaxNumber, _
        mstrVATNumber)
    Call PrintInvoiceHeader(frmTill.OPOSPrinter1, mstrAccountNum, mstrAccountName, _
        mstrActive, mstrCardExpiry, mstrAddressLine1, mstrAddressLine2, mstrAddressLine3, _
        mstrPostCode, mstrHomeTelNumber, mstrInvoiceNumber, True)


End Sub 'PrintInvoiceDetails

Private Sub RetrieveHeadOfficeAddress()


    Set moSysopt = goDatabase.CreateBusinessObject(CLASSID_SYSTEMOPTIONS)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeName)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeAddress1)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeAddress2)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeAddress3)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeAddress4)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeAddress5)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_HeadOfficePostCode)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_HeadOfficePhoneNo)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeFaxNumber)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_VATNumber)
    Call moSysopt.LoadMatches
    
    mstrHeadofficename = moSysopt.HeadOfficeName
    'Add Address 1 to HeadOfficeAddress
    If Len(moSysopt.HeadOfficeAddress1) > 0 Then
        mstrHeadOfficeAddress = Space$((40 - Len(moSysopt.HeadOfficeAddress1)) / 2) & moSysopt.HeadOfficeAddress1 & vbCrLf
    End If
    'Add Address 2 to HeadOfficeAddress
    If Len(moSysopt.HeadOfficeAddress2) > 0 Then
        mstrHeadOfficeAddress = mstrHeadOfficeAddress & Space$((40 - Len(moSysopt.HeadOfficeAddress2)) / 2) & moSysopt.HeadOfficeAddress2 & vbCrLf
    End If
    'Add Address 3 to HeadOfficeAddress
    If Len(moSysopt.HeadOfficeAddress3) > 0 Then
        mstrHeadOfficeAddress = mstrHeadOfficeAddress & Space$((40 - Len(moSysopt.HeadOfficeAddress3)) / 2) & moSysopt.HeadOfficeAddress3 & vbCrLf
    End If
    'Add Address 4 to HeadOfficeAddress
    If Len(moSysopt.HeadOfficeAddress4) > 0 Then
        mstrHeadOfficeAddress = mstrHeadOfficeAddress & Space$((40 - Len(moSysopt.HeadOfficeAddress4)) / 2) & moSysopt.HeadOfficeAddress4 & vbCrLf
    End If
    'Add Address 5 to HeadOfficeAddress
    If Len(moSysopt.HeadOfficeAddress5) > 0 Then
        mstrHeadOfficeAddress = mstrHeadOfficeAddress & Space$((40 - Len(moSysopt.HeadOfficeAddress5)) / 2) & moSysopt.HeadOfficeAddress5 & vbCrLf
    End If
    'Add Postcode to HeadOfficeAddress
    If Len(moSysopt.HeadOfficePostCode) > 0 Then
        mstrHeadOfficeAddress = mstrHeadOfficeAddress & Space$((40 - Len(moSysopt.HeadOfficePostCode)) / 2) & moSysopt.HeadOfficePostCode & vbCrLf
    End If
    mstrHeadOfficePhoneNo = moSysopt.HeadOfficePhoneNo
    mstrHeadOfficeFaxNumber = moSysopt.HeadOfficeFaxNumber
    mstrVATNumber = moSysopt.VATNumber

End Sub 'RetrieveHeadOfficeAddress

