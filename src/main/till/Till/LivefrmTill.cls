VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "LivefrmTill"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Implements IfrmTill870
Implements IfrmTill876

' IfrmTill870 Implementation
Private Sub IfrmTill870_SetPaymentAuthorisationType(ByRef Payment As cSale_Wickes.cPOSPayment, ByVal EntryMethod As String)

    If Left$(EntryMethod, 5) <> "Keyed" Then
        Payment.CCNumberKeyed = False
        Payment.AuthorisationType = "O"
    Else
        Payment.CCNumberKeyed = True
        Payment.AuthorisationType = "R"
    End If
End Sub
' End of IfrmTill870 Implementation

' IfrmTill876 Implementation
Private Function IfrmTill876_InProcessPaymentTypeMethod() As Boolean

    IfrmTill876_InProcessPaymentTypeMethod = False
End Function

Private Sub IfrmTill876_SetForInProcessPaymentType()

End Sub

Private Sub IfrmTill876_SetForOutOfProcessPaymentType()

End Sub
' End of IfrmTill876 Implementation

