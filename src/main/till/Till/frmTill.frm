VERSION 5.00
Object = "{CCB90040-B81E-11D2-AB74-0040054C3719}#1.0#0"; "OPOSCashDrawer.ocx"
Object = "{CCB90150-B81E-11D2-AB74-0040054C3719}#1.0#0"; "OPOSPOSPrinter.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{8DDE6232-1BB0-11D0-81C3-0080C7A2EF7D}#3.0#0"; "Flp32a30.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "mscomm32.ocx"
Object = "{FA666EFA-0D22-45D3-9849-6B0694037D1B}#2.1#0"; "EditNumberTill.ocx"
Begin VB.Form frmTill 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Till Function"
   ClientHeight    =   11190
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15360
   ControlBox      =   0   'False
   Icon            =   "frmTill.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11190
   ScaleWidth      =   15360
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   240
      Top             =   7800
      Visible         =   0   'False
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   8
      Top             =   10815
      Width           =   15360
      _ExtentX        =   27093
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   11
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmTill.frx":0A96
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   14049
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   970
            MinWidth        =   970
            Picture         =   "frmTill.frx":143C
            Key             =   "KeyBoard"
            Object.ToolTipText     =   "Show/Hide Keyboard"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   573
            MinWidth        =   573
            Picture         =   "frmTill.frx":1AEE
            Key             =   "NumPad"
            Object.ToolTipText     =   "Show/Hide numeric keypad"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel8 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel9 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1058
            MinWidth        =   1058
            TextSave        =   "17:47"
         EndProperty
         BeginProperty Panel10 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   2646
            MinWidth        =   2646
            Key             =   "PED"
         EndProperty
         BeginProperty Panel11 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Object.Width           =   882
            MinWidth        =   882
            Picture         =   "frmTill.frx":1F3C
            Key             =   "Offline"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox fraLogon 
      BorderStyle     =   0  'None
      Height          =   2175
      Left            =   0
      ScaleHeight     =   2175
      ScaleWidth      =   8040
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   0
      Width           =   8040
      Begin VB.CommandButton cmdScannerEnable 
         Caption         =   "Scanner Enable"
         Height          =   495
         Left            =   5700
         TabIndex        =   27
         Top             =   1440
         Visible         =   0   'False
         Width           =   915
      End
      Begin VB.CommandButton cmdSleep 
         Caption         =   "Scanner Sleep"
         Height          =   495
         Left            =   5700
         TabIndex        =   53
         Top             =   900
         Visible         =   0   'False
         Width           =   915
      End
      Begin VB.TextBox txtPassword 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   525
         IMEMode         =   3  'DISABLE
         Left            =   1740
         MaxLength       =   5
         PasswordChar    =   "*"
         TabIndex        =   13
         Top             =   1440
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.TextBox txtUserID 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   525
         Left            =   1740
         MaxLength       =   3
         TabIndex        =   12
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cmdExitTill 
         Caption         =   "EXIT"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   2820
         TabIndex        =   67
         Top             =   180
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.Label lblTrainingMode 
         BackStyle       =   0  'Transparent
         Caption         =   "***TRAINING MODE***"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   3720
         TabIndex        =   43
         Top             =   240
         Visible         =   0   'False
         Width           =   4335
      End
      Begin VB.Label lblLoggedUserID 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   1740
         TabIndex        =   39
         Top             =   240
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Initials"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   0
         TabIndex        =   18
         Top             =   840
         Width           =   1335
      End
      Begin VB.Label lblInitials 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   1740
         TabIndex        =   17
         Top             =   840
         Width           =   975
      End
      Begin VB.Label lblFullName 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   2820
         TabIndex        =   16
         Top             =   840
         Width           =   2655
      End
      Begin VB.Label lblPasswordlbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Password"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   0
         TabIndex        =   15
         Top             =   1440
         Visible         =   0   'False
         Width           =   1695
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "User ID"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   0
         TabIndex        =   14
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.PictureBox fraEntry 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   735
      Left            =   0
      ScaleHeight     =   735
      ScaleWidth      =   11895
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   6480
      Visible         =   0   'False
      Width           =   11895
      Begin EditNumberTill.ucTillNumberText uctntPrice 
         Height          =   675
         Left            =   2100
         TabIndex        =   25
         Top             =   0
         Visible         =   0   'False
         Width           =   3855
         _ExtentX        =   6800
         _ExtentY        =   1191
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.PictureBox fraQty 
         BorderStyle     =   0  'None
         Height          =   720
         Left            =   7620
         ScaleHeight     =   720
         ScaleWidth      =   2055
         TabIndex        =   61
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         Width           =   2055
         Begin VB.Label lblQty 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   24
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   0
            TabIndex        =   62
            Top             =   0
            Width           =   135
         End
      End
      Begin VB.CommandButton cmdViewLines 
         Caption         =   "Vi&ew Lines"
         BeginProperty Font 
            Name            =   "Microsoft Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6000
         TabIndex        =   28
         Top             =   0
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.PictureBox fraMaxValue 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   735
         Left            =   7740
         ScaleHeight     =   735
         ScaleWidth      =   4035
         TabIndex        =   48
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         Width           =   4035
         Begin VB.Label lblMaxPrompt 
            BackStyle       =   0  'Transparent
            Caption         =   "Max quantity is"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   0
            TabIndex        =   50
            Top             =   120
            Width           =   2655
         End
         Begin VB.Label lblMaxValue 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H0000FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   " "
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   495
            Left            =   2580
            TabIndex        =   49
            Top             =   120
            Width           =   1455
         End
      End
      Begin VB.TextBox txtSKU 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   2100
         TabIndex        =   26
         Top             =   0
         Visible         =   0   'False
         Width           =   3860
      End
      Begin VB.CommandButton cmdCurrency 
         Caption         =   "F5-List Currency"
         Height          =   315
         Left            =   6000
         TabIndex        =   71
         Top             =   0
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.CommandButton cmdProcess 
         Caption         =   "ENTER"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   6000
         TabIndex        =   29
         Top             =   300
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.Label lblType 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "SKU"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Left            =   0
         TabIndex        =   30
         Top             =   60
         Visible         =   0   'False
         Width           =   2055
      End
   End
   Begin CTSProgBar.ucpbProgressBar ucpbStatus 
      Height          =   1860
      Left            =   4200
      TabIndex        =   70
      Top             =   3120
      Visible         =   0   'False
      Width           =   5100
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      Object.Width           =   5100
      Object.Height          =   1860
   End
   Begin MSCommLib.MSComm comBarScanner 
      Left            =   360
      Top             =   8280
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DTREnable       =   -1  'True
   End
   Begin VB.PictureBox fraVoidLine 
      BorderStyle     =   0  'None
      Height          =   1395
      Left            =   0
      ScaleHeight     =   1395
      ScaleWidth      =   12015
      TabIndex        =   34
      TabStop         =   0   'False
      Top             =   6720
      Visible         =   0   'False
      Width           =   12015
      Begin VB.CommandButton cmdVoidLineEsc 
         Caption         =   "Cancel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   3360
         TabIndex        =   65
         Top             =   720
         Width           =   1875
      End
      Begin VB.CommandButton cmdVoidLineOK 
         Caption         =   "Void Line"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   5640
         TabIndex        =   64
         Top             =   720
         Width           =   1935
      End
      Begin VB.Label lblVoidDesc 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   3780
         TabIndex        =   38
         Top             =   120
         Width           =   7995
      End
      Begin VB.Label lblVoidSKU 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   960
         TabIndex        =   37
         Top             =   120
         Width           =   1575
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Desc"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   2640
         TabIndex        =   36
         Top             =   120
         Width           =   1095
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "SKU"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   -60
         TabIndex        =   35
         Top             =   120
         Width           =   975
      End
   End
   Begin VB.PictureBox fraTranType 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   1455
      Left            =   0
      ScaleHeight     =   1455
      ScaleWidth      =   11895
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   6720
      Visible         =   0   'False
      Width           =   11895
      Begin VB.CommandButton cmdPrevTranType 
         Caption         =   "F11-Previous"
         Height          =   405
         Left            =   10560
         TabIndex        =   23
         Top             =   510
         Width           =   1215
      End
      Begin VB.CommandButton cmdNextTranType 
         Caption         =   "F12-Next"
         Height          =   405
         Left            =   10560
         TabIndex        =   22
         Top             =   1020
         Width           =   1215
      End
      Begin VB.CommandButton cmdTranType 
         Caption         =   "TranType"
         Height          =   495
         Index           =   0
         Left            =   120
         TabIndex        =   20
         Top             =   720
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.Label lblfraTranType 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Select Transaction Type"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Index           =   0
         Left            =   15
         TabIndex        =   59
         Top             =   15
         Width           =   2100
      End
      Begin VB.Label lblTranFKey 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   0
         Left            =   420
         TabIndex        =   21
         Top             =   360
         Visible         =   0   'False
         Width           =   495
      End
   End
   Begin VB.PictureBox fraReasonsList 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   5295
      Left            =   0
      ScaleHeight     =   5295
      ScaleWidth      =   8415
      TabIndex        =   40
      TabStop         =   0   'False
      Top             =   1080
      Visible         =   0   'False
      Width           =   8415
      Begin LpLib.fpList lstReasonsList 
         Height          =   4350
         Left            =   1920
         TabIndex        =   42
         Top             =   720
         Width           =   4575
         _Version        =   196608
         _ExtentX        =   8070
         _ExtentY        =   7673
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Columns         =   2
         Sorted          =   0
         LineWidth       =   1
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         MultiSelect     =   0
         WrapList        =   0   'False
         WrapWidth       =   0
         SelMax          =   -1
         AutoSearch      =   2
         SearchMethod    =   0
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   0
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   0
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   0
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   0
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   0   'False
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         DataField       =   ""
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         ColDesigner     =   "frmTill.frx":2B8E
      End
      Begin VB.CommandButton cmdListOk 
         Caption         =   "Select"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   6540
         TabIndex        =   67
         Top             =   4500
         Width           =   1875
      End
      Begin VB.CommandButton cmdListCancel 
         Caption         =   "Cancel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   6540
         TabIndex        =   66
         Top             =   3780
         Width           =   1875
      End
      Begin VB.TextBox txtReasonDescription 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   240
         MaxLength       =   40
         TabIndex        =   45
         Top             =   5580
         Width           =   7935
      End
      Begin OposPOSPrinter_1_9_LibCtl.OPOSPOSPrinter OPOSPrinter1 
         Left            =   720
         OleObjectBlob   =   "frmTill.frx":2E4D
         Top             =   2640
      End
      Begin OposCashDrawer_1_9_LibCtl.OPOSCashDrawer OPOSCashDrawer 
         Left            =   240
         OleObjectBlob   =   "frmTill.frx":2E71
         Top             =   2160
      End
      Begin VB.Label lblReasonDesclbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Description"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   44
         Top             =   5220
         Width           =   7815
      End
      Begin VB.Label lblReasonDescription 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Select Price Override Reason"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   0
         TabIndex        =   41
         Top             =   180
         Width           =   8415
      End
   End
   Begin VB.PictureBox fraActions 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      Height          =   915
      Left            =   0
      ScaleHeight     =   915
      ScaleWidth      =   11895
      TabIndex        =   54
      TabStop         =   0   'False
      Top             =   7260
      Visible         =   0   'False
      Width           =   11895
      Begin VB.CommandButton cmdAction 
         Caption         =   "Colleague Discount"
         Height          =   495
         Index           =   0
         Left            =   120
         TabIndex        =   57
         Top             =   360
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton cmdPrevAction 
         Caption         =   "F11-Previous"
         BeginProperty Font 
            Name            =   "Microsoft Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   10560
         TabIndex        =   56
         Top             =   60
         Width           =   1215
      End
      Begin VB.CommandButton cmdNextAction 
         Caption         =   "F12-Next"
         Height          =   315
         Left            =   10560
         TabIndex        =   55
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label lblActionFKey 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   0
         Left            =   420
         TabIndex        =   58
         Top             =   60
         Visible         =   0   'False
         Width           =   600
      End
   End
   Begin VB.Timer tmrLogout 
      Left            =   0
      Top             =   5760
   End
   Begin FPSpreadADO.fpSpread sprdLines 
      Height          =   4575
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1800
      Visible         =   0   'False
      Width           =   12015
      _Version        =   458752
      _ExtentX        =   21193
      _ExtentY        =   8070
      _StockProps     =   64
      BorderStyle     =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   204
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   104
      MaxRows         =   2
      OperationMode   =   2
      RowHeaderDisplay=   2
      ScrollBars      =   2
      SelectBlockOptions=   0
      SpreadDesigner  =   "frmTill.frx":2E95
      UserResize      =   2
      VisibleCols     =   39
   End
   Begin VB.PictureBox fraTender 
      Appearance      =   0  'Flat
      BackColor       =   &H0080FF80&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1455
      Left            =   0
      ScaleHeight     =   1455
      ScaleWidth      =   11895
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   6480
      Visible         =   0   'False
      Width           =   11895
      Begin VB.CommandButton cmdNextTenderType 
         Caption         =   "F12-Next"
         Height          =   315
         Left            =   10560
         TabIndex        =   33
         Top             =   1020
         Width           =   1215
      End
      Begin VB.CommandButton cmdPrevTenderType 
         Caption         =   "F11-Previous"
         Height          =   315
         Left            =   10560
         TabIndex        =   32
         Top             =   600
         Width           =   1215
      End
      Begin VB.CommandButton cmdTenderType 
         Caption         =   "Tender1"
         Height          =   495
         Index           =   0
         Left            =   180
         TabIndex        =   10
         Top             =   660
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label lblfraTender 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Select Tender Type"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Index           =   0
         Left            =   15
         TabIndex        =   60
         Top             =   15
         Width           =   1695
      End
      Begin VB.Label lblTenderFKey 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   0
         Left            =   540
         TabIndex        =   31
         Top             =   300
         Visible         =   0   'False
         Width           =   495
      End
   End
   Begin FPSpreadADO.fpSpread sprdTotal 
      Height          =   1185
      Left            =   0
      TabIndex        =   72
      Top             =   600
      Visible         =   0   'False
      Width           =   12075
      _Version        =   458752
      _ExtentX        =   21299
      _ExtentY        =   2090
      _StockProps     =   64
      Enabled         =   0   'False
      BorderStyle     =   0
      ColHeaderDisplay=   0
      DisplayColHeaders=   0   'False
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   204
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   13
      MaxRows         =   4
      MoveActiveOnFocus=   0   'False
      OperationMode   =   1
      RetainSelBlock  =   0   'False
      RowHeaderDisplay=   0
      ScrollBars      =   0
      SelectBlockOptions=   0
      SpreadDesigner  =   "frmTill.frx":45DB
   End
   Begin VB.PictureBox fraTrader 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   0
      ScaleHeight     =   615
      ScaleWidth      =   11955
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   -120
      Visible         =   0   'False
      Width           =   11955
      Begin VB.Label lblTranNo 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   840
         TabIndex        =   52
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label8 
         Caption         =   "Tran No"
         Height          =   255
         Left            =   120
         TabIndex        =   51
         Top             =   240
         Width           =   615
      End
      Begin VB.Label lblOrderNo 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   9240
         TabIndex        =   47
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label2 
         Caption         =   "Order No"
         Height          =   255
         Left            =   8520
         TabIndex        =   46
         Top             =   240
         Width           =   735
      End
      Begin VB.Label lblTraderNo 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2640
         TabIndex        =   4
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label lblOrderDate 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   10800
         TabIndex        =   3
         Top             =   240
         Width           =   975
      End
      Begin VB.Label lblTraderName 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   5040
         TabIndex        =   2
         Top             =   240
         Width           =   3375
      End
      Begin VB.Label Label27 
         Caption         =   "Customer"
         Height          =   255
         Left            =   4320
         TabIndex        =   6
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label28 
         Caption         =   "Card Number"
         Height          =   255
         Left            =   1680
         TabIndex        =   7
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label26 
         Caption         =   "Date"
         Height          =   255
         Left            =   10320
         TabIndex        =   5
         Top             =   240
         Width           =   495
      End
   End
   Begin VB.Image imgCustomerCommitments 
      Height          =   10125
      Left            =   12480
      Picture         =   "frmTill.frx":54DC
      Top             =   5400
      Width           =   13500
   End
   Begin VB.Image ImgSplash 
      Height          =   4695
      Left            =   0
      Picture         =   "frmTill.frx":38116
      Top             =   0
      Visible         =   0   'False
      Width           =   7215
   End
   Begin VB.Image Image1 
      Height          =   735
      Left            =   0
      Picture         =   "frmTill.frx":A66DC
      Stretch         =   -1  'True
      Top             =   0
      Visible         =   0   'False
      Width           =   3855
   End
End
Attribute VB_Name = "frmTill"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>************************************************************************************
'* Module : frmTill
'* Date   : 17/02/03
'* Author : Unknown
'*$Archive: /Projects/OasysV2/VB/Till/frmTill.frm $
'******************************************************************************************
'* Summary:
'******************************************************************************************
'* $Author: Richardc $ $Date: 10/09/04 9:20 $ $Revision: 84 $
'* Versions:
'* 17/02/03    Unknown
'*             Header added.
'</CAMH>***********************************************************************************
Option Explicit

Const APP_NAME          As String = "TILL"
Const MODULE_NAME       As String = "frmTill"

Const PARTCODE_LEN      As Long = 6
Const PARTCODE_PAD      As String = "000000"

Const TRAN_BTN_PER_ROW  As Long = 7
Const BTN_PER_ROW       As Long = 8
Const PROMPT_FONT_SIZE  As Long = 24

Const DEFAULT_LOGON_CONTROLLS_POSITION As Integer = 3960 'Looks like magical numbers but it's their position before new branding imsge with girl in a tub was adderd

Public mblnStopScanning As Boolean
Private mblnOrderBtnPressed As Boolean
Private mblnShowAddress As Boolean

Private Const COMMAND_BUTTON_SKU_ENQUIRY As Long = 0
Private Const COMMAND_BUTTON_SKU_ENQUIRY_PIM As Long = 1
Private Const COMMAND_BUTTON_FIRST_TRUE_BUTTON_ID As Long = 2
Private Const OPTION_NUMBER_SKU_ENQUIRY As Long = 8
Private Const OPTION_NUMBER_SKU_ENQUIRY_PIM As Long = 9
Private Const SKU_ENQUIRY_PIM_F_KEY As Long = 8

Const ROW_TOTALS As Long = 1

Const ROW_TOTTOTALS As Long = 3
Const COL_TOTQTY    As Long = 3
Const COL_TOTORD    As Long = 5
Const COL_TOTTYPE   As Long = 7
Const COL_TOTVAL    As Long = 11
Const COL_TOT_GRP   As Long = 12

Const SOT_COL_VALUES      As Long = 4
Const SOT_ROW_TOTAL       As Long = 2
Const SOT_ROW_DEP_USED    As Long = 4
Const SOT_ROW_BAL_SALE    As Long = 6
Const SOT_ROW_ORDER_VALUE As Long = 10
Const SOT_ROW_DEP_SUGG    As Long = 12
Const SOT_ROW_DEP_TAKEN   As Long = 14
Const SOT_ROW_CUST_PRSNT  As Long = 16
Const SOT_ROW_BALANCE     As Long = 20

Const PRM_CASH_DRAWER               As Long = 833
Const PRM_OVERRIDE_DEPT_DESC        As Long = 870
Const PRM_CAPTURE_COST_PRICE        As Long = 871
Const PRM_TRANSACTION_DISCOUNT      As Long = 872
Const PRM_TRANSACTION_DISCOUNT_AUTH As Long = 810
Const PRM_USE_TRADERS               As Long = 819
Const PRM_REFUND_PRINT_DECLARATION  As Long = 835
Const PRM_ASKPOSTCODE               As Long = 839
Const PRM_GIFTVOUCHERSKU            As Long = 840
Const PRM_SHOW_STOCK_ON_QUOTE_SAVE  As Long = 884
Const PRM_CRL_PAIDOUT_CODE          As Long = 942 'WIX1345 - Paid Out Code that will invoke the CRL system
Const PRM_WHICH_DISCOUNT_SCHEME     As Long = 956
Const PRM_MARKDOWN_PREFIX           As Long = 965 'MO'C WIX1302 - ParameterID to Hold Prefix Character on a Markdown Barcode
Const PRM_TRAN_DISC_HOTKEY          As Long = 879 'MM WIX1165 - ParameterID to Hold Transaction Hotkey from Action Button
Const PRM_AMEND_PRICE_HOTKEY        As Long = 889 'MM WIX1165 - ParameterID to Hold Amend Price Hotkey from Action Button
Const PRM_ALLOW_COUPONS             As Long = 905 'WIX1165 - Allow capture coupons
Const PRM_ACTIONS_LIST1             As Long = 882 'MO'C WIX1376 - Allows the user to define the order of the Action buttons
Const PRM_ACTIONS_LIST2             As Long = 883 'MO'C WIX1376 - Allows the user to define the order of the Action buttons
Const PRM_STOP_GIFT_VOUCHER_PRINT   As Long = 270

' Commidea Into WSS
' Yet more constants for Tender Types, these used where Tender Type 9 = AmEx is relevant
Const TENTYPE_CASH As Long = 1
Const TENTYPE_CREDIT As Long = 3
Const TENTYPE_GIFTTOKEN As Long = 7
Const TENTYPE_DEBIT  As Long = 8
Const TENTYPE_AMEX   As Long = 9
' End of Commidea Into WSS

Const DEFAULT_MSG   As String = "F1-Help"
Const POSTCODE_NAME As String = "PostCode Survey"

'Const LINE_ORDER_FLAG As String = "C" 'used to mark items on a Sale, as Customer Orders

'Constants for the attribute field of STKMAS
Const ATTR_SPECIAL_ORDER As String = "0"
Const ATTR_TELESALES     As String = "T"
Const ATTR_STORE_USE     As String = "U"
Const ATTR_SERVICES      As String = "S"

Const PRINT_NONE   As Long = 0
Const PRINT_PROMPT As Long = 1
Const PRINT_ALWAYS As Long = 2

Dim mlngClickX As Long
Dim mlngClickY As Long

Dim mdteEventEndDate As Date

'WIX1202 - barcode check
Private mlngBCodeCheckCnt   As Long 'count out the number of Items keyed in
Private mblnBCodeCheck      As Boolean
Private mstrBCodeBrokenCode As String
Private mstrBCodeAuditCode  As String

'WIX1218 - Prompts Reject log
Private mcolPromptRejects   As Collection

'WIX1389 - EON Promotion
Private mcolVolumeSKUs   As Collection
Private mcolEOTPrompts   As Collection 'all groups that contain Type 4 prompts

'WIX1165 - Todays Price Amendments - keep in Transaction until saved at end of Tran
Private mcolPriceAmends     As Collection
Private mstrTranDiscCode    As String 'from PRM879 - to hold where discount is stored against
Private mstrTranDiscAuth    As String 'from PRM879 - 'S'upervisor/ 'M'anager / None
Private mlngTranDiscSupLevel As Long 'from PRM879 - level at which Supervisor must be given for Transaction Discount
Private mlngTranDiscMgrLevel As Long 'from PRM879 - level at which Manager must be given for Transaction Discount

'WIX1345 - Concern Resolution - Paid In Reason Code
Private mstrCRLPaidOutReasonCode As String

Private mblnAllowCoupons As Boolean 'WIX1165 - Switch on Coupons using parameter 905
Private mcolGetCoupons   As Collection 'List of Coupons that must be saved and printed
Private mcolHSGetCoupons As Collection 'List of HS Coupons that must be saved and printed
Private mblnCouponWSActive  As Boolean 'WIX1165 - indicates if Coupon Validation WebService is active
Private mstrCouponWSURL As String 'WIX1165 -URL of web service to validate against
Private mcolUsedCoupons As Collection ' P014-01 - List of coupons used up in a transaction

'Added 26/11/08 - collection of previous VAT Rates for refund items
Private mcolPreviousVATRates As New Collection

Private mcolTTypes   As Collection
Private moVATRates   As cVATRates
Private moSysopt     As cSystemOptions
Private moRetopt     As cRetailOptions

Private moCashierTotals As cCashierTotals
Private moTillTotals    As cTillTotals
Private moSaleTotals    As cSaleTotals

Private mcolTOSOpt    As Collection 'holds list of Transaction types -once user logged in
Private mcolOPSummary As Collection 'holds a list of events summary - to be printed
Private mcolHSSummary As Collection 'holds a list of Hierachy Spends summary - to be printed

Private mcolWEEESKUs    As Collection 'holds a list of WEEE Rates that are applied to the list at Tender

Private mcolCurrencies  As Collection 'holds a list of Currency Codes
Private mcDefaultCurr   As cCurrencies 'holds the default Currency Code
Private mblnUseMultiCur As Boolean 'flag to switch on/off Multi-Currency from PRM 845
Private mblnSaveNewOrder As Boolean 'flag to indicate when fully tendered, whether to call SaveOrder WIX1309

Private moOrigTend    As cPOSPayment 'holds first tender used on original transaction of a refund
' Commidea Into WSS
Private moOrigTendComm  As cPOSPaymentComm 'holds extended Commidea info on first tender used on original transaction of a refund
' End of Commidea Into WSS

Private mlngTranTypeRowNo   As Long
Private mlngTenderTypeRowNo As Long
Private mlngActionRowNo     As Long

Private mdblTranTotal As Double 'store Transaction Total, when Tenders are started
Private mdblTotalBeforeDis As Double 'store Transaction Total before discount
Private mdblPaidTotal As Double 'stores amount tendered

Public mdblLineQuantity As Double 'used to store quantity if entered before SKU

Private mblnSavedVersions As Boolean
Private mblnEnterDecPoint As Boolean
Private mblnQtyBeforeSKU  As Boolean
Private mblnItemPrePriced As Boolean

Private mcolLinePrints    As Collection
Private mcolTrailerPrints As Collection
Private mcolValues        As Collection

Private mblnNonValidated          As Boolean 'added to track if refund has any non-validated transactions
Private mblnRefundOverride        As Boolean 'added to force supervisor if Refund Price Override
Private mblnTokensOnly            As Boolean
Private mblnAllowAllTenders       As Boolean
Private mblnGiftTokenUsedInTender As Boolean
Private mblnCreditCardUsedInTender As Boolean
Private mblnRefundingWithAGiftToken As Boolean 'Added to stop Signature for gift token printing twice, MO'C 31/03/2008
Private mblnNonTokenUsedInTender  As Boolean

Private mblnCustomerOrder As Boolean 'flag to process Customer Orders as lines are entered

Private mlngPrinterCode As Long
Private mstrTillNo    As String
Private mstrStoreNo   As String
Private mstrTranId    As String 'used to hold current Transaction No
Private mstrOldUserID As String 'used to auto move to password on 3 key pressed
Private mstrOldPwd    As String 'used to auto logon on 5 key pressed

Private mbytQtyEntry   As Byte 'holds flag for work station to indicate QTY/SKU Order
Private mblnTraining   As Boolean
Private mblnSOCustDone As Boolean 'used to ensure Customer details entered before entering tender
Private mblnCustDone   As Boolean 'used to ensure Customer details entered before entering tender

Private mblnParkingTran As Boolean 'used to indicate if capturing customer for Parked Transaction
Private mblnValidItem   As Boolean

Private mcurCompetitorsPrice As Currency
Private mdblRefundPrice     As Double 'temp var for refund item's price, entered after quantity
Private mblnRefundAddLines  As Boolean 'from parameter 821: manual refund allowed
Private mblnAllowMixRefund  As Boolean 'from parameter 822: multiple receipts can be refunded
Private mblnRefundPrintDec  As Boolean 'from parameter 835: declarations are printed on receipt
Private mblnExchangeRefunds As Boolean 'when doing Exchange, flags if on Refund Items
Private mblnFirstPmtDone    As Boolean 'used to ensure after first payment, can not go back
Private mblnCapturePostCode As Boolean 'Capture customers postcode
Private mblnPricePromise    As Boolean
Private mdblOrigSellingPrice As Double
Private mblnApplyCollDisc   As Boolean 'flag if Colleague discount must be stored
Private mblnColleagueDiscount As Boolean
Private mblnDiscountColleague As Boolean
Private mblnUseCashDrawer   As Boolean
Private mblnNeedDrawerOpen  As Boolean
Private mblnProcessPaymentFailed As Boolean

Private mblnBarcoded        As Boolean 'flag if SKU was barcoded so it can be saved into Db
'Account Details
Private mstrAccountNum              As String
Private mstrAccountName             As String
Private mstrActive                  As String
Private mstrCardExpiry              As String
Private mstrDeliveryName            As String
Private mstrDeliveryAddressLine1    As String
Private mstrDeliveryAddressLine2    As String
Private mstrDeliveryAddressLine3    As String
Private mstrDeliveryPostCode        As String
Private mstrDeliveryWorkTelNumber   As String
Private mstrDeliveryHomeTelNumber   As String
Private mstrDateAccountOpened       As String
Private mstrStopped                 As String
Private mstrCreditLimit             As String
Private mstrAccountBalance          As String
Private mstrSoldToday               As String
Private mstrTend                    As String
Private mstrInvoiceNumber           As String
Private mcurMaxGiftVoucherValue     As Currency
Private mstrRefundReason            As String
Private mstrTranSuper               As String
Private mstrRefundCashier           As String
Private mstrTenderType              As String

Private mstrEFTDebugInfo            As String

Private mlngHighestAgePassed        As Long
Private mlngLowestAgeFailed         As Long

Private mstrCashDrawer  As String
Private mstrBarCodeName As String

Private mstrKeyedCode As String
Private mstrKeyedSKU  As String

Private mintDuressKeyCode        As Integer
Private mlngDuressOpenDrawerCode As Long

Private mlngTranSign As Long

Private mblnHaveRefPrice As Boolean
Private mcurRefundPrice  As Currency

Private dictCreditCardNames        As New Dictionary 'holds list of CreditCard Names to Tender Types
Private dictCreditCardNamePatterns As New Dictionary

Private moRefundOrigTran    As cPOSHeader
Private moParkTranHdr       As cPOSHeader 'used to pass Price Promise info from Parked to actual sale
Private mstrOrders          As Collection 'used to hold any orders that need updating

Private oRetrieveTran       As frmRetrieveTran 'used in Refunds/Exchanges to hold transaction details

Private frmItemFilter       As frmItemFilter

Private mdteQuoteTranDate   As Date
Private mstrQuoteTillID     As String
Private mstrQuoteTranNo     As String
Private mlngQuoteQtyTaken   As Long

Private mstrCardNoMask      As String 'required to record PRM926 to mask Credit Card No

'MO'C - Added below variables for WIX1345
Private mstrConResDesc      As String
Private mblnManager         As Boolean
Private mstrAttribute       As String
Private mstrConResCode      As String
'MO'C - finished WIX1345

Private WithEvents frmCapturePricePromDetails As frmCapturePricePromDetails
Attribute frmCapturePricePromDetails.VB_VarHelpID = -1
Private WithEvents frmNotFound                As frmNotFound
Attribute frmNotFound.VB_VarHelpID = -1
Private WithEvents frmPostcode                As frmPostcode
Attribute frmPostcode.VB_VarHelpID = -1
Private WithEvents frmReprintReceipt          As frmReprintReceipt
Attribute frmReprintReceipt.VB_VarHelpID = -1
Private WithEvents frmRetrieveTran            As frmRetrieveTran
Attribute frmRetrieveTran.VB_VarHelpID = -1
Private WithEvents frmReverseItem             As frmReverseItem
Attribute frmReverseItem.VB_VarHelpID = -1
Private WithEvents frmTender                  As frmTender
Attribute frmTender.VB_VarHelpID = -1
Private WithEvents frmVerifyPwd               As frmVerifyPwd
Attribute frmVerifyPwd.VB_VarHelpID = -1
Private WithEvents frmTokenScan               As frmTokenScan
Attribute frmTokenScan.VB_VarHelpID = -1
Private WithEvents frmCustomerAddress         As frmCustomerAddress
Attribute frmCustomerAddress.VB_VarHelpID = -1
Private WithEvents frmComplete                As frmComplete
Attribute frmComplete.VB_VarHelpID = -1
Private WithEvents frmCoupon                  As frmCoupon
Attribute frmCoupon.VB_VarHelpID = -1
Private WithEvents frmEANCheck                As frmEANCheck
Attribute frmEANCheck.VB_VarHelpID = -1
Private WithEvents frmDiscountCardScheme      As frmDiscountCardScheme  'MO'C Added 07/06/07
Attribute frmDiscountCardScheme.VB_VarHelpID = -1
Private WithEvents frmConcernRes              As frmConcernRes          'MO'C Added 07/01/09 WIX1345 Concern resolution
Attribute frmConcernRes.VB_VarHelpID = -1

Private moEvents As cTillEvents_Wickes.cOPEvents

Public Enum enReasonMode
    enrmPriceOverride = 0
    enrmPaidOut = 1
    enrmPaidIn = 2
    enrmOpenDrawer = 3
    enrmTransactionDiscount = 4
    enrmCancelOrder = 5
    enrmMarkDown = 6
    enrmRefund = 7
    enrmTenderOverride = 8
    enrmBarcodeFailed = 9
    enrmLineReversed = 10
End Enum

Private Enum enFlashTotalCategory
    enftSales = 1
    enftSaleCorrections
    enftRefunds
    enftRefundCorrections
    enftSignon
    enftSignOff
    enftMiscPaidIn
    enftMiscPaidOut
    enftMiscOpenDrawer
    enftRepritLogo
    enftVoidedTransactions
    enftPriceOverrides
    enftSpare
End Enum

Private Enum enWhichDiscountScheme
    enVerifyDiscountCardScheme = 1
    enVerifyColleagueCardScheme
    enVerifyBoth
End Enum

Private mblnPrintVAT  As Boolean 'Flag to indicate if VAT breakdown must be printed
Private mstrName      As String
Private mstrAddress   As String
Private mstrTelNo     As String
Private mstrMobileNo  As String
Private mstrWorkTelNo   As String
Private mstrEmail       As String
Private mstrRemarks   As String

Public mlngEntryMode      As enEntryMode
Public mlngCurrentMode    As enCurrentMode
Private mcolPriceOverrides As Collection
Private moRefPriceOverride As cPriceOverrideCode
Private mcolRefundCodes    As Collection

Private mblnRefundLineMode As Boolean 'used to switch between selecting/entering lines on refunds
Private mblnIsRefundLine As Boolean
Private mblnRecallingParked As Boolean

Private mblnRecalledTran    As Boolean 'Flag if Parked/Recalled so that no Refund Items can be added

Private mcolPaidOutCodes            As Collection
Private mcolPaidInCodes             As Collection
Private mcolOpenDrawerCodes         As Collection
Private mcolTransactionDiscount     As Collection
Private mcolMarkDownCodes           As Collection
Private mcolTenderOverrideCodes     As Collection
Private mcolOrderCancel             As Collection
Private mcolSpecialDept             As Collection
Private mcolSpecialMsgDept          As Collection
Private mcolCouponsInTransaction    As Collection
Private mblnQuoteRecalledOrderCreated As Boolean
Private mblnRecalledQuote           As Boolean

Private mdblPreviousQty   As Double 'temp to hold previously entered quantity-on refunded line
Private mdblPreviousPrice As Double 'temp to hold previously entered price-on refunded line
Private mblnEditingLine   As Boolean 'flag: current line is being edited, update instead of adding
Private mdblMaxQtyLoanItems As Double 'for Loan A Tile sales - max number of items allowed
Private mstrDeptSkuNum    As String
Private mstrDeptSalePrice As String

Private mblnByPassCashDrawer            As Boolean
Private mblnOverrideDeptDesc            As Boolean
Private mblnCaptureCostPrice            As Boolean
Private mlngWaitForCashDraw             As Long
'Modules for Transaction Discount
Private mdblPercentageValue             As Double
Private mstrTransDiscountCode           As String
Private mstrTransDiscountDesc           As String
Private mlngTransactionDiscountMethod   As Long
Private mblnTransactionDiscountApplied  As Boolean
'Variables to hold head office address
Private mstrHeadofficename          As String
Private mstrHeadOfficeAddress       As String
Private mstrHeadOfficePhoneNo       As String
Private mstrHeadOfficeFaxNumber     As String
Private mstrVATNumber               As String

Private mstrTeleOrderNum        As String
Public mlngTransDiscountAmnt    As Long
Private mdblMaxValue            As Double
Private mdblMaxQty              As Double
Private mdblDepositValue        As Double
Private mstrMsgDeptNo           As String

Private mdblSQMDiscLevel    As Double
Private mdblSQMDiscRate     As Double

Private mcolActionBtns  As New Collection

Private mlngSaleIndex   As Long
Private mlngRefundIndex As Long
Private mlngReasonCode  As Long
Private mstrReasonDesc  As String

Private mstrRefOrigStoreNo  As String
Private mdteRefOrigTranDate As Date
Private mstrRefOrigTillID   As String
Private mstrRefOrigTranNo   As String
Private mlngRefOrigLineNo   As Long
Private mstrRefOrigOrderNo  As String
Private mblnRefLineValid    As Boolean

Private mstrRefundTrans As String 'added to print onto Refund Auth Slip

Private mcolEFTPayments As Collection 'added 20/10/05 to hold EFT Trans to Collect upon completion
Private mblnCNPTill     As Boolean 'WIX1192 - used to switch of EFT trans and go to CVV Number entry
Private mblnRefCNPTill  As Boolean 'WIX1378 - used to switch of Refund EFT trans and go to CVV Number entry
Private mblnUseWERates  As Boolean ' used to switch on/off WEEE Rates

'Discount Scheme Variables
Private moDiscountCardScheme    As cDiscountCardScheme
Private mblnApplyDiscountScheme As Boolean
Private mstrLocation    As String
Private mstrDiscCardNo  As String
Private mintWhichDiscountCard   As Integer    '24/08/07 MO'C Added for Wix1311 referal #35

Private mstrAmendPriceReason    As String 'WIX1165 - 26/1/09 to hold Amend Reason Discount Code
Private mintGroupPromptQty      As Integer 'WIX1391 - Value used to total up the Prompt Quantities

Private Type PROCESS_INFORMATION
    hProcess As Long
    hThread As Long
    dwProcessId As Long
    dwThreadId As Long
End Type

Private Type STARTUPINFO
    cb As Long
    lpReserved As String
    lpDesktop As String
    lpTitle As String
    dwX As Long
    dwY As Long
    dwXSize As Long
    dwYSize As Long
    dwXCountChars As Long
    dwYCountChars As Long
    dwFillAttribute As Long
    dwFlags As Long
    wShowWindow As Integer
    cbReserved2 As Integer
    lpReserved2 As Long
    hStdInput As Long
    hStdOutput As Long
    hStdError As Long
End Type

Private Declare Function CreateProcess Lib "kernel32" _
    Alias "CreateProcessA" _
    (ByVal lpApplicationName As String, _
    ByVal lpCommandLine As String, _
    lpProcessAttributes As Any, _
    lpThreadAttributes As Any, _
    ByVal bInheritHandles As Long, _
    ByVal dwCreationFlags As Long, _
    lpEnvironment As Any, _
    ByVal lpCurrentDriectory As String, _
    lpStartupInfo As STARTUPINFO, _
    lpProcessInformation As PROCESS_INFORMATION) As Long

Private Declare Function OpenProcess Lib "kernel32.dll" _
    (ByVal dwAccess As Long, _
    ByVal fInherit As Integer, _
    ByVal hObject As Long) As Long

Private Declare Function TerminateProcess Lib "kernel32" _
    (ByVal hProcess As Long, _
    ByVal uExitCode As Long) As Long

Private Declare Function CloseHandle Lib "kernel32" _
    (ByVal hObject As Long) As Long

Const SYNCHRONIZE = 1048576
Const NORMAL_PRIORITY_CLASS = &H20&

'Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, _
                                                    ByVal dwProcessId As Long) As Long
Const PROCESS_TERMINATE = &H1

'Commidea Into WSS
' If using Ocius Sentinel PED then flag up whether all ready launched and logged in
Private mblnOciusSoftwareReady As Boolean
Private mblnOciusPEDLoggedIn As Boolean
Private mstrCommideaTills As String
Private mstrPrm_Use_EFTPOS As String
Private mstrOciusLaunchParams As String
Private mstrCommideaLoginDetails As String
Private mstrCommideaUserID As String
Private mstrCommideaUserPIN As String
Private mstrOciusSentinelPath As String
Private mstrCommideaIntegrationPort As String
Private mstrCommideaProgressPort As String
Private mintOciusLoginTimeout As Integer
Private mintConnectionTimeout As Integer
Private mstrCommideaCustRecPath As String
Private mstrCommideaCustRecFilename As String
Private mstrCommideaMerRecPath As String
Private mstrCommideaMerRecFilename As String
Private mblnCNPTend As Boolean
Private mstrCurrChar As String
Private mblnUseCommideaTokenIDs As Boolean
Private mblnGot_Prm_Use_Commidea_TokenIDs As Boolean
Private mblnInvalidTokenID As Boolean
Private mblnCommideaStoreTerminalID As Boolean
Private mblnCommideaSeparateReceipt As Boolean
Private mstrUnknownCreditCardNames As String
Private mstrCommideaReceiptHeaderLastLine As String
Private mstrCommideaReceiptFooterFirstLine As String
Private mstrCommideaLicenceName As String    ' Referral 818.  When PED install, PED might ask to accept a licence. This stores the name on that licence to allow till to accept it.
Private mblnCommideaModalAuthorisation As Boolean   ' Referral 1093
Private mblnGot_Prm_Modal_Authorisation As Boolean  ' Referral 1093
Private mintOciusPrintConfirmTimeout As Integer
Private mPartialGCPaymentTimeout As Integer

' CR0034
' Flag up whether code changes for CR0034 are currently active
Private mblnChangeRequestCR0034Active As Boolean
Private mblnGotParamReqActive As Boolean
' Store the Commidea PED PTID for support
Private mstrPTID As String
' End of CR0034
Private mstrOciusSentinelVersion As String

Private mavReceipts() As Variant                   ' Collect all the receipt arrays
Private mblnGot_Prm_Separate_Receipts As Boolean
Private mblnReturningFromCancelledEFTTender As Boolean   ' Referral 482
' New parameters
Private Const PRM_COMMIDEA_TILLS As Long = 970001
Private Const PRM_COMMIDEA_OCIUS_LAUNCH_PARAMS As Long = 970002
Private Const PRM_COMMIDEA_OCIUS_LOGIN As Long = 970003
Private Const PRM_COMMIDEA_OCIUS_PATH As Long = 970004
Private Const PRM_COMMIDEA_INTEGRATION_PORT As Long = 970005
Private Const PRM_COMMIDEA_PROGRESS_PORT As Long = 970006
Private Const PRM_COMMIDEA_LOGIN_TIMEOUT As Long = 970007
Private Const PRM_COMMIDEA_CONN_TIMEOUT As Long = 970008
Private Const PRM_COMMIDEA_CUSTOMER_RECEIPTPATH As Long = 970009
Private Const PRM_COMMIDEA_CUSTOMER_RECEIPTFILE As Long = 970010
Private Const PRM_COMMIDEA_MERCHANT_RECEIPTPATH As Long = 970011
Private Const PRM_COMMIDEA_MERCHANT_RECEIPTFILE As Long = 970012
Private Const PRM_COMMIDEA_STORE_TERMINALID As Long = 970013
Private Const PRM_COMMIDEA_USE_TOKEN_IDS As Long = 970014
Private Const PRM_COMMIDEA_UNKNOWN_CCNAMES_FILE As Long = 970015
Private Const PRM_COMMIDEA_SEPARATE_RECEIPT As Long = 970016
Private Const PRM_COMMIDEA_REC_HEAD_LAST_LINE As Long = 970017
Private Const PRM_COMMIDEA_REC_FOOT_FIRST_LINE As Long = 970018
Private Const PRM_COMMIDEA_LICENCE_NAME As Long = 970019    ' Referral 818.  When PED install, PED might ask to accept a licence. This param specifies the name on that licence to allow till to accept it.
Private Const PRM_COMMIDEA_PRINTCONFIRM_TIMEOUT As Long = 970020
Private Const PRM_COMMIDEA_PARTIALGCPAYMENT_TIMEOUT As Long = 970022
'Store AccountId
Private Const PRM_ACCOUNT_ID As Long = 970021
'End
Private Const PRM_PRINTER_CURR_CODE As Long = 149
Private Const PRM_GEN_CONFIG_FILE As Long = 910
Private Const PRM_COMMIDEA_MODAL_AUTHORISATION As Long = 981093 ' Referral 1093
' New CNP button
Private Const TENDNO_CNP       As String = "66"
Private Const TENDNO_CRDR      As String = "06"
' For handling Duress event
Private WithEvents moCommidea As WSSCommidea.cCommidea
Attribute moCommidea.VB_VarHelpID = -1
' So can get WSSUniqueTransactionTenderRef from a Commidea object without Receipt printing needing to have a WSSCommidea reference
Private WithEvents moReceiptPrinting As cReceiptPrint_Wickes.clsReceiptPrinting
Attribute moReceiptPrinting.VB_VarHelpID = -1
'End of Commidea Into WSS

Public mcolRefundTrans As Collection

' Hubs 2.0.
Private mstrWebOrderNumber As String
Private mblnTransactionHasWebOrderNoRefund As Boolean
' End of Hubs 2.0.

' Referral 876
Private m_ProcessingPaymentImplementation As IProcessingPayment
' End of Referral 876

' P014-01
Private WithEvents mRetrieveCoupon As COMTPWickes_InterOp_Implementation.RetrieveCoupon
Attribute mRetrieveCoupon.VB_VarHelpID = -1
' End of P014-01

Private RepeatPaymentImplementation As IRepeatPayment
Private PreviousWSSUniqueTTRef  As String
Private RepeatPaymentAttempted As Boolean
Private Const OldTokenTenderIndex As Long = 5
Private Const CouponTenderIndex As Integer = 8

' US17601 B2B PPC Wickes Till Sell gift cards (purchase a gift card)
Const PRM_GIFTCARDSKU As Long = 979
Const PRM_GIFTCARDMAXVALUE As Long = 980
Const PRM_GIFTCARDMINVALUE As Long = 981
Const PRM_GIFTCARDREFMAXVALUE As Long = 982
Const PRM_GIFTCARDREFMINVALUE As Long = 983
Const PRM_GIFTCARDREDMAXVALUE As Long = 984
Private mcurMaxGiftCardValue As Currency
Private mcurMinGiftCardValue As Currency
Private flSaleGiftCard As Boolean
Private mstrSKUGiftVoucher As String
Private mcurMaxRefundGiftCardValue As Currency
Private mcurMinRefundGiftCardValue As Currency
Private mcurMaxRedeemGiftCardValue As Currency

'Duplicate refunds issue
Private commideaTransactionInProgress As Boolean

'Error handler variables
Private errorNo As Long
Private errorDesc As String

' Check process variables
'Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
'Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Private Declare Function EnumProcesses Lib "PSAPI.DLL" (lpidProcess As Long, ByVal cb As Long, cbNeeded As Long) As Long
Private Declare Function EnumProcessModules Lib "PSAPI.DLL" (ByVal hProcess As Long, lphModule As Long, ByVal cb As Long, lpcbNeeded As Long) As Long
Private Declare Function GetModuleBaseName Lib "PSAPI.DLL" Alias "GetModuleBaseNameA" (ByVal hProcess As Long, ByVal hModule As Long, ByVal lpFileName As String, ByVal nSize As Long) As Long
 
Private Const PROCESS_VM_READ = &H10
Private Const PROCESS_QUERY_INFORMATION = &H400

Private Function SetUpActions()

Dim lngEntryNo      As Long
Dim lngBtnNo        As Long
Dim lngLeftPos      As Long
Dim cActionCode     As clsActionCodes
Dim oFSO            As New FileSystemObject
Dim colTempActions  As New Collection     'MO'C WIX1376 - Store the TmpAction in Sequence as before, then Cherry pick them into the order the user chose in the parameters table in mcolActionBtns
Dim varActionOrder  As Variant
    
    On Error GoTo HandleException
    
    Call DebugMsg(MODULE_NAME, "SetUpActions", endlTraceIn)
    
    mintDuressKeyCode = goSession.GetParameter(PRM_KEY_DURESS)
    mlngDuressOpenDrawerCode = goSession.GetParameter(PRM_ODC_DURESS)

    If (mblnHasEFT = True) And (mblnHasCashDrawer = True) Then
        Set cActionCode = New clsActionCodes
        cActionCode.ButtonDesc = "Park"
        cActionCode.ActionCode = enacPark
        cActionCode.HotKeyCode = goSession.GetParameter(PRM_KEY_PARK)
        cActionCode.ActionId = PRM_KEY_PARK
        Call colTempActions.Add(cActionCode, Str$(cActionCode.ActionCode))
    End If

    Set cActionCode = New clsActionCodes
    cActionCode.ButtonDesc = "Void"
    cActionCode.ActionCode = enacVoid
    cActionCode.HotKeyCode = goSession.GetParameter(PRM_KEY_VOID)
    cActionCode.ActionId = PRM_KEY_VOID
    Call colTempActions.Add(cActionCode, Str$(cActionCode.ActionCode))

'    Set cActionCode = New clsActionCodes
'    cActionCode.ButtonDesc = "Special"
'    cActionCode.ActionCode = enacSpecialItem
'    cActionCode.HotKeyCode = goSession.GetParameter(PRM_KEY_SPECIAL)
'    cActionCode.ActionId = PRM_KEY_SPECIAL
'    Call colTempActions.Add(cActionCode, Str$(cActionCode.ActionCode))
    
    Set cActionCode = New clsActionCodes
    cActionCode.ButtonDesc = "Quantity"
    cActionCode.ActionCode = enacQuantity
    cActionCode.HotKeyCode = goSession.GetParameter(PRM_KEY_QUANTITY)
    cActionCode.ActionId = PRM_KEY_QUANTITY
    Call colTempActions.Add(cActionCode, Str$(cActionCode.ActionCode))

    Set cActionCode = New clsActionCodes
    cActionCode.ButtonDesc = "Reversal"
    cActionCode.ActionCode = enacReversal
    cActionCode.HotKeyCode = goSession.GetParameter(PRM_KEY_REVERSAL)
    cActionCode.ActionId = PRM_KEY_REVERSAL
    Call colTempActions.Add(cActionCode, Str$(cActionCode.ActionCode))

    If (mblnHasCashDrawer = True) And (mblnHasEFT = True) Then
        Set cActionCode = New clsActionCodes
        cActionCode.ButtonDesc = "Tender"
        cActionCode.ActionCode = enacTender
        cActionCode.HotKeyCode = goSession.GetParameter(PRM_KEY_TENDER)
        cActionCode.ActionId = PRM_KEY_TENDER
        Call colTempActions.Add(cActionCode, Str$(cActionCode.ActionCode))
    End If

    Set cActionCode = New clsActionCodes
    cActionCode.ButtonDesc = "SKU Enquiry"
    cActionCode.ActionCode = enacEnquiry
    cActionCode.HotKeyCode = goSession.GetParameter(PRM_KEY_LOOKUP)
    cActionCode.ActionId = PRM_KEY_LOOKUP
    Call colTempActions.Add(cActionCode, Str$(cActionCode.ActionCode))
    
    If (oFSO.FolderExists(goSession.GetParameter(PRM_PIM_ONLY_IMAGE_PATH)) = True) Then
        Set cActionCode = New clsActionCodes
        cActionCode.ButtonDesc = "PIM SKU Enquiry"
        cActionCode.ActionCode = enacPIMEnquiry
        cActionCode.HotKeyCode = goSession.GetParameter(PRM_KEY_PIM_LOOKUP)
        cActionCode.ActionId = PRM_KEY_PIM_LOOKUP
        cActionCode.Visible = False
        Call colTempActions.Add(cActionCode, Str$(cActionCode.ActionCode))
    End If

    Set cActionCode = New clsActionCodes
    cActionCode.ButtonDesc = "Override"
    cActionCode.ActionCode = enacOverride
    cActionCode.HotKeyCode = goSession.GetParameter(PRM_KEY_OVERRIDE)
    cActionCode.ActionId = PRM_KEY_OVERRIDE
    Call colTempActions.Add(cActionCode, Str$(cActionCode.ActionCode))

    mintWhichDiscountCard = goSession.GetParameter(PRM_WHICH_DISCOUNT_SCHEME)
    If (mblnHasEFT = True) Then
        If mintWhichDiscountCard = enWhichDiscountScheme.enVerifyColleagueCardScheme Then
        
            Set cActionCode = New clsActionCodes
            cActionCode.ButtonDesc = "Colleague Disc"
            cActionCode.ActionCode = enacColleagueDiscount
            cActionCode.HotKeyCode = goSession.GetParameter(PRM_KEY_TRANDISC)
            cActionCode.ActionId = PRM_KEY_TRANDISC
            Call colTempActions.Add(cActionCode, Str$(cActionCode.ActionCode))
            
        ElseIf mintWhichDiscountCard = enWhichDiscountScheme.enVerifyDiscountCardScheme Then
            
            Set cActionCode = New clsActionCodes
            cActionCode.ButtonDesc = "Card Discount"      'MO'C Added 07/06/07
            cActionCode.ActionCode = enacDiscountScheme
            cActionCode.HotKeyCode = goSession.GetParameter(PRM_KEY_TRANDISC)
            cActionCode.ActionId = PRM_KEY_TRANDISC
            Call colTempActions.Add(cActionCode, Str$(cActionCode.ActionCode))
        
        End If
    End If
    
    If (mblnHasCashDrawer = True) And (mstrPrinterType = "P" Or mstrPrinterType = "R") Then
        Set cActionCode = New clsActionCodes
        cActionCode.ButtonDesc = "Gift Card" ' US17601 B2B PPC Wickes Till Sell gift cards (purchase a gift card)
        cActionCode.ActionCode = enacGiftVoucher
        cActionCode.HotKeyCode = goSession.GetParameter(PRM_KEY_GIFTVOUCHER)
        cActionCode.ActionId = PRM_KEY_GIFTVOUCHER
        Call colTempActions.Add(cActionCode, Str$(cActionCode.ActionCode))
    End If

    Call DebugMsg(MODULE_NAME, "SetUpActions", endlDebug, "Checking if QOD enabled - " & goSession.GetParameter(PRM_QODACTIVE))
    If (goSession.GetParameter(PRM_QODACTIVE) = True) Then
        If (goSession.GetParameter(PRM_KEY_ORDER) <> "") Then
            Set cActionCode = New clsActionCodes
            cActionCode.ButtonDesc = "Customer Order"
            cActionCode.ActionCode = enacOrder
            cActionCode.HotKeyCode = goSession.GetParameter(PRM_KEY_ORDER)
            cActionCode.ActionId = PRM_KEY_ORDER
            Call colTempActions.Add(cActionCode, Str$(cActionCode.ActionCode))
        End If 'Customer Order Button hotkey set PRM853
    End If 'QOD parameter is set to True/Active

    Call DebugMsg(MODULE_NAME, "SetUpActions", endlDebug, "Checking if BDC enabled - " & goSession.GetParameter(PRM_BDCACTIVE))
    If (goSession.GetParameter(PRM_BDCACTIVE) = True) And (mblnHasEFT = True) Then
        Set cActionCode = New clsActionCodes
        cActionCode.ButtonDesc = "Backdoor Collection"
        cActionCode.ActionCode = enacBackdoorItems
        cActionCode.HotKeyCode = goSession.GetParameter(PRM_KEY_BACKDOORITEMS)
        cActionCode.ActionId = PRM_KEY_BACKDOORITEMS
        Call colTempActions.Add(cActionCode, Str$(cActionCode.ActionCode))
    End If 'BDC Flag is set to Active

    Call DebugMsg(MODULE_NAME, "SetUpActions", endlDebug, "Checking if Quotes enabled - " & goSession.GetParameter(PRM_QUOTES_HOTKEY))
    If (goSession.GetParameter(PRM_QUOTES_HOTKEY) <> "") And (mblnAllowQuotes = True) Then
        Set cActionCode = New clsActionCodes
        cActionCode.ButtonDesc = "Create Quote"
        cActionCode.ActionCode = enacSaveQuote
        cActionCode.HotKeyCode = goSession.GetParameter(PRM_QUOTES_HOTKEY)
        cActionCode.ActionId = PRM_QUOTES_HOTKEY
        Call colTempActions.Add(cActionCode, Str$(cActionCode.ActionCode))
        Set cActionCode = New clsActionCodes
        cActionCode.ButtonDesc = "Recall Quote"
        cActionCode.ActionCode = enacRecallQuote
        cActionCode.HotKeyCode = goSession.GetParameter(PRM_RECALL_QUOTES_HOTKEY)
        cActionCode.ActionId = PRM_RECALL_QUOTES_HOTKEY 'PRM_KEY_LOOKUP
        Call colTempActions.Add(cActionCode, Str$(cActionCode.ActionCode))
        
        If (mblnHasEFT = True) Then
            Set cActionCode = New clsActionCodes
            cActionCode.ButtonDesc = "Create Order"
            cActionCode.ActionCode = enacCreateOrder
            cActionCode.HotKeyCode = goSession.GetParameter(PRM_CREATE_ORDER_HOTKEY)
            cActionCode.ActionId = PRM_CREATE_ORDER_HOTKEY
            Call colTempActions.Add(cActionCode, Str$(cActionCode.ActionCode))
        End If
        
    End If 'Quotes Hotkey is set to Active
    
    If (goSession.GetParameter(PRM_AMEND_PRICE_HOTKEY) <> "") Then
        Set cActionCode = New clsActionCodes
        cActionCode.ButtonDesc = "Amend Price"
        cActionCode.ActionCode = enacAmendPrice
        mstrAmendPriceReason = goSession.GetParameter(PRM_AMEND_PRICE_HOTKEY)
        If (InStr(mstrAmendPriceReason, ":") = 0) Then
            Call MsgBoxEx("Amend Price configuration does not have the Reason Code defined." & vbCrLf & "Amend Price has been disabled.", vbOKOnly, "Invalid Amend Price Set-Up", , , , , RGBMsgBox_WarnColour)
        Else
            cActionCode.HotKeyCode = Left(mstrAmendPriceReason, InStr(mstrAmendPriceReason, ":") - 1)
            cActionCode.ActionId = PRM_AMEND_PRICE_HOTKEY
            mstrAmendPriceReason = Mid$(mstrAmendPriceReason, InStr(mstrAmendPriceReason, ":") + 1)
            Call colTempActions.Add(cActionCode, Str$(cActionCode.ActionCode))
        End If
    Else
        Set mcolPriceAmends = New Collection
    End If
    
    '*******************************************************************************
    'MO'C WIX1376 - We are going to read the parametrs 882 and 883 to give us the
    'order we want for the Action buttons.
    '*******************************************************************************
    varActionOrder = GetKeysFromParameters()
    For lngEntryNo = 0 To UBound(varActionOrder) - 1
        Call FindAction(colTempActions, varActionOrder(lngEntryNo), lngEntryNo)
    Next lngEntryNo
    
    lngBtnNo = 1
    For lngEntryNo = 1 To mcolActionBtns.Count Step 1
        Load cmdAction(lngBtnNo)
        lngLeftPos = ((lngBtnNo - 1) Mod BTN_PER_ROW) + 1
        cmdAction(lngBtnNo).Left = ((cmdAction(0).Width + 120) * (lngLeftPos - 1)) + 120
        cmdAction(lngBtnNo).Visible = False
        cmdAction(lngBtnNo).Caption = mcolActionBtns(lngEntryNo).ButtonDesc
        cmdAction(lngBtnNo).Tag = mcolActionBtns(lngEntryNo).ActionCode
        cmdAction(lngBtnNo).BackColor = fraActions.BackColor
        'for the Action buttons - load extra captions as they vary from the F1 to Fxx standard
        Load lblActionFKey(lngBtnNo)
        With lblActionFKey(lngBtnNo)
                .Left = cmdAction(lngBtnNo).Left + ((cmdAction(lngBtnNo).Width - .Width) / 2)
                .Visible = False
                .Caption = mcolActionBtns(lngBtnNo).HotKeyCode
        End With
        With mcolActionBtns(lngBtnNo)
            If (Left$(.HotKeyCode, 1) = "F") And (Len(.HotKeyCode) > 1) Then
                lblActionFKey(lngBtnNo).Tag = Val(Mid$(.HotKeyCode, 2)) + vbKeyF1 - 1
            Else
                If (Left$(.HotKeyCode, 4) = "Ctrl") And (Len(.HotKeyCode) > 5) Then
                    lblActionFKey(lngBtnNo).Tag = (Val(Mid$(.HotKeyCode, 7)) + vbKeyF1 - 1) * -1
                Else
                    If .HotKeyCode = "" Then
                        lblActionFKey(lngBtnNo).Tag = 32
                        lblActionFKey(lngBtnNo).Visible = False
                    Else
                        lblActionFKey(lngBtnNo).Tag = Asc(.HotKeyCode)
                    End If
                End If
            End If
        End With
        lngBtnNo = lngBtnNo + 1
    Next lngEntryNo
    
    cmdNextAction.BackColor = fraActions.BackColor
    cmdPrevAction.BackColor = fraActions.BackColor
    
    Call ShowActionRow

    Exit Function

HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("SetUpActions", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Function

Private Sub EnableAction(enacButton As enActionCode, blnEnabled As Boolean)
    
Dim lngBtnNo As Long

    Call DebugMsg(MODULE_NAME, "EnableAction", endlTraceIn)
    For lngBtnNo = 1 To mcolActionBtns.Count Step 1
        If cmdAction(lngBtnNo).Tag = enacButton Then
            cmdAction(lngBtnNo).Enabled = blnEnabled
            Exit For
        End If
    Next lngBtnNo
    
End Sub

Private Function ActionEnabled(enacButton As enActionCode) As Boolean

Dim lngBtnNo As Long

    Call DebugMsg(MODULE_NAME, "ActionEnabled", endlTraceIn)
    For lngBtnNo = 1 To mcolActionBtns.Count Step 1
        If cmdAction(lngBtnNo).Tag = enacButton Then
            ActionEnabled = cmdAction(lngBtnNo).Enabled
            Exit For
        End If
    Next lngBtnNo
    
End Function

Private Sub cmdAction_Click(index As Integer)

Dim strTraderNumber As String
Dim strTraderName   As String
Dim lngLineNo     As Long
Dim strProcedureName As String

    strProcedureName = "cmdAction_Click"
    
    On Error GoTo HandleException
    
    Call DebugMsg(MODULE_NAME, strProcedureName, endlTraceIn)
    
    If mlngActionRowNo > 0 And index <= BTN_PER_ROW Then
        index = index + (mlngActionRowNo * BTN_PER_ROW)
    End If
    
    If cmdAction(index).Visible = False Or cmdAction(index).Enabled = False Then GoTo DebugMsgExit
  
    If (cmdAction(index).Enabled = True) And (cmdAction(index).Visible = True) Then cmdAction(index).SetFocus
    Select Case (cmdAction(index).Tag)
        Case (enacPark):            Call ParkTransaction
        Case (enacVoid):            Call VoidTransaction
        Case (enacQuantity):        Call SwitchToQuantity
        Case (enacReversal):        Call ReverseLine
        Case (enacTender):
                                    If mblnRecalledQuote Then
                                        Call RecordTender(False, False, True)
                                    Else
                                        Call RecordTender((mdblTotalBeforeDis <> 0), False, True)
                                    End If
                                    'mblnCreatingOrder = False
                                    'mblnOrderBtnPressed = False
                                    'mblnSaveNewOrder = False
        Case (enacEnquiry):         Call frmItemFilter.ItemEnquiry(etFullItems)
        Case (enacPIMEnquiry):      Call frmItemFilter.ItemEnquiry(etPIMOnly)
        Case (enacOverride):        Call ItemOverride
        Case (enacLookUp):          Call TransactionLookUp
        Case (enacColleagueDiscount): Call ColleagueDiscount
        Case (enacDiscountScheme):  Call DiscountScheme
        Case (enacGiftVoucher):
                                    If (mstrGiftCardSKU <> "") Then
                                    
                                        sprdLines.Col = COL_PARTCODE
                                        For lngLineNo = 2 To sprdLines.MaxRows Step 1
                                            If (sprdLines.Text <> "" And sprdLines.Text <> mstrGiftCardSKU) Then
                                                
                                                Call MsgBoxEx("Unable to include Gift Card Sell/Top Up within this transaction." & vbNewLine & _
                                                                               "Please complete a separate transaction.", vbOKOnly + vbExclamation, "Warning", , , , , RGBMsgBox_WarnColour)
                                                txtSKU.Text = ""
                                                GoTo DebugMsgExit
                                            End If
                                            If (sprdLines.Text <> "" And sprdLines.Text = mstrGiftCardSKU) Then
                                                
                                                Call MsgBoxEx("Only one Gift Card Sell/Top Up allowed per transaction." & vbNewLine & _
                                                                               "Please complete a separate transaction.", vbOKOnly + vbExclamation, "Warning", , , , , RGBMsgBox_WarnColour)
                                                txtSKU.Text = ""
                                                GoTo DebugMsgExit
                                            End If
                                        Next lngLineNo
                                        
                                        flSaleGiftCard = True
                                        mstrGiftVoucherSKU = mstrGiftCardSKU
                                        Call RecordVouchers
                                    Else
                                        Call MsgBoxEx("SKU Number for Gift Card not found.", _
                                        vbOKOnly, "Gift Card", , , , , RGBMsgBox_WarnColour)
                                        GoTo DebugMsgExit
                                    End If
        Case (enacOrder):           Call CustomerOrder
        Case (enacBackdoorItems):   Call BackDoorCollections
        Case (enacSaveQuote):       mblnCreatingOrder = False
                                    mblnOrderBtnPressed = False
                                    Call SaveQuote(True, False, False, False)
        Case (enacRecallQuote):     Call RecallQuote
        Case (enacCreateOrder):     Call CreateOrder(False, False, False)
        Case (enacTransactionDisc): Call EnterTransactionDisc
        Case (enacAmendPrice):      Call AmendPrice
    End Select
    mlngActionRowNo = 0
    Call ShowActionRow
    
DebugMsgExit:
    Call DebugMsg(MODULE_NAME, strProcedureName, endlTraceOut)
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler(strProcedureName, Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub AmendPrice()

Dim oPriceBO As cTillPrompts_Wickes.cDayPrice

Dim strSKU  As String
Dim strDesc As String
Dim curPrice As Currency
Dim strMgrID As String
Dim lngRowNo As Long

    On Error GoTo HandleException
    
    Call DebugMsg(MODULE_NAME, "AmendPrice", endlTraceIn)
    sprdLines.Col = COL_LINETYPE
    If (Val(sprdLines.Value) <> LT_ITEM) Then
        Call MsgBoxEx("Last entry is not an Item.  Unable to amend price", vbOKOnly, "Apply Price Amendment", , , , , RGBMsgBox_WarnColour)
        mlngCurrentMode = encmRecord
        Call SetEntryMode(enemSKU)
        Exit Sub
    End If
    
    sprdLines.Col = COL_VOIDED
    If (Val(sprdLines.Value) <> 0) Then
        Call MsgBoxEx("Last entry was voided.  Unable to amend price", vbOKOnly, "Apply Price Amendment", , , , , RGBMsgBox_WarnColour)
        mlngCurrentMode = encmRecord
        Call SetEntryMode(enemSKU)
        Exit Sub
    End If
    
    
    sprdLines.Col = COL_PARTCODE
    strSKU = sprdLines.Text
    
    'Check if item has Price amendment
    On Error Resume Next
    Set oPriceBO = mcolPriceAmends(strSKU)
    If (oPriceBO Is Nothing = False) Then
        Err.Clear
        Call MsgBoxEx("Item has already had Price Amended in this transaction" & vbCrLf & "Unable to apply new amendment", vbOKOnly, "Apply Price Amendment", , , , , RGBMsgBox_WarnColour)
        mlngCurrentMode = encmRecord
        Call SetEntryMode(enemSKU)
        Exit Sub
    End If
    Err.Clear
    On Error GoTo 0
    
    'Check if item already exists so will impact other items
    For lngRowNo = 2 To sprdLines.MaxRows - 1 Step 1
        sprdLines.Row = lngRowNo
        sprdLines.Col = COL_PARTCODE
        If (sprdLines.Text = strSKU) Then
            sprdLines.Col = COL_VOIDED
            If (Val(sprdLines.Value) = 0) Then
                If MsgBoxEx("Item exists multiple times in this transaction." & vbCrLf & "Price amendment will apply to other items", vbOKCancel, "Apply Price Amendment", , , , , RGBMSGBox_PromptColour) = vbCancel Then
                    mlngCurrentMode = encmRecord
                    Call SetEntryMode(enemSKU)
                    Exit Sub
                Else
                    Exit For
                End If
            End If
        End If
    Next lngRowNo
    
    sprdLines.Row = sprdLines.MaxRows
    Load frmVerifyPwd
    If frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) = True Then
        strMgrID = frmVerifyPwd.txtAuthID.Text
        Unload frmVerifyPwd
        Call SetEntryMode(enemNone)
        sprdLines.Col = COL_PARTCODE
        strSKU = sprdLines.Text
        
        sprdLines.Col = COL_LOOKEDUP
        curPrice = Val(sprdLines.Value)
        sprdLines.Col = COL_DESC
        strDesc = sprdLines.Text
        Load frmAmendPrice
        If (frmAmendPrice.ConfirmAmendPrice(strSKU, strDesc, curPrice) = True) Then
            For lngRowNo = 2 To sprdLines.MaxRows Step 1
                sprdLines.Row = lngRowNo
                sprdLines.Col = COL_PARTCODE
                If (sprdLines.Value = strSKU) Then
                    sprdLines.Col = COL_VOIDED
                    If (Val(sprdLines.Value) = 0) Then
                        sprdLines.Col = COL_SELLPRICE
                        sprdLines.Value = frmAmendPrice.NewPrice
                        sprdLines.Col = COL_SUPERVISOR
                        sprdLines.Text = strMgrID
                        sprdLines.Col = COL_DISCCODE
                        sprdLines.Text = mstrAmendPriceReason
                        sprdLines.Col = COL_DAY_PRICE
                        sprdLines.Value = frmAmendPrice.NewPrice
                    End If
                End If
            Next lngRowNo 'step through lines and update all items with same sku
            Call UpdateTotals
            Set oPriceBO = goSession.Database.CreateBusinessObject(CLASSID_DAYPRICE)
            oPriceBO.PartCode = strSKU
            oPriceBO.AuthId = strMgrID
            oPriceBO.Comment = frmAmendPrice.Reason
            oPriceBO.DateCreated = Date
            oPriceBO.TimeCreated = "" 'flag as not saved yet
            oPriceBO.NewPrice = frmAmendPrice.NewPrice
            Call mcolPriceAmends.Add(oPriceBO, strSKU)
        End If
    Else 'invalid authorisation code, or escape pressed
        Unload frmVerifyPwd
    End If
    Unload frmAmendPrice
    mlngCurrentMode = encmRecord
    Call SetEntryMode(enemSKU)
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("AmendPrice", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub EnterTransactionDisc()

Dim curTotal As Currency
Dim curDiscount() As Currency
Dim strAllocItems() As String
Dim dblDiscRate As Double
Dim lngRowNo    As Long
Dim lngItemNo   As Long
Dim curPayAmount As Currency
Dim strPreCode   As String
    
    On Error GoTo HandleException
    
    Call DebugMsg(MODULE_NAME, "EnterTransactionDisc", endlTraceIn)
    'Check if Authorisation required before starting
    If (mstrTranDiscAuth <> "") Then
        Load frmVerifyPwd
        If (mstrTranDiscAuth = "S") Then
            If frmVerifyPwd.VerifySupervisorPassword(ucKeyPad1.Visible) = True Then
                moTranHeader.RefundSupervisor = frmVerifyPwd.txtAuthID.Text
                Unload frmVerifyPwd
            Else
                Unload frmVerifyPwd
                Exit Sub
            End If
        Else
            If frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) = True Then
                moTranHeader.RefundManager = frmVerifyPwd.txtAuthID.Text
                Unload frmVerifyPwd
            Else
                Unload frmVerifyPwd
                Exit Sub
            End If
        End If
    End If 'Auth required before Tran Discount
    'Added 17/4/09 -Restore Item Price to pre-Transaction Discount Levels, if present
    Call DebugMsg(MODULE_NAME, "EnterTransactionDisc", endlDebug, "Rollback Transaction Discount")
    For lngItemNo = 2 To sprdLines.MaxRows Step 1
        sprdLines.Row = lngItemNo
        sprdLines.Col = COL_LINETYPE
        If (Val(sprdLines.Value) = LT_ITEM) Then
            sprdLines.Col = COL_PRETRAN_PRICE
            If (sprdLines.Text <> "") Then
                curPayAmount = Val(sprdLines.Value)
                sprdLines.Col = COL_SELLPRICE
                sprdLines.Value = curPayAmount
                sprdLines.Col = COL_PRETRAN_CODE
                strPreCode = sprdLines.Value
                sprdLines.Col = COL_DISCCODE
                sprdLines.Value = strPreCode
            End If 'Rollback Price found
        End If 'Line Item
    Next lngItemNo
    Call UpdateTotals
    'Display transaction total
    sprdTotal.Row = ROW_TOTTOTALS
    sprdTotal.Col = COL_TOTVAL
    sprdLines.Row = sprdLines.MaxRows - 1
    sprdLines.Col = COL_INCTOTAL
    sprdLines.Text = sprdTotal.Text
    mdblTranTotal = sprdTotal.Text
    sprdTotal.Text = mdblTranTotal
    
    sprdTotal.Col = COL_TOTVAL
    sprdTotal.Row = ROW_TOTTOTALS
    curTotal = sprdTotal.Value
        
    'Calculate value of Actual counted items
    curPayAmount = 0
    For lngRowNo = 2 To sprdLines.MaxRows Step 1
        sprdLines.Row = lngRowNo
        sprdLines.Col = COL_LINETYPE
        If (sprdLines.Value = LT_ITEM) Then
            sprdLines.Col = COL_VOIDED
            If (Val(sprdLines.Value) = 0) Then
                sprdLines.Col = COL_INCTOTAL
                curPayAmount = curPayAmount + sprdLines.Value
            End If
        End If
    Next lngRowNo
    
    Load frmTranDisc
    frmTranDisc.mlngTranDiscSupLevel = mlngTranDiscSupLevel
    frmTranDisc.mlngTranDiscMgrLevel = mlngTranDiscMgrLevel
    fraTender.Visible = False
    If (frmTranDisc.RecordDiscount(curTotal, strAllocItems, dblDiscRate) = True) Then
        'Adjust Discount rate to allow for given discounts
        If UBound(strAllocItems) = 0 Then
            dblDiscRate = dblDiscRate * (curTotal / curPayAmount)
            For lngRowNo = 2 To sprdLines.MaxRows Step 1
                sprdLines.Row = lngRowNo
                sprdLines.Col = COL_LINETYPE
                If (sprdLines.Value = LT_ITEM) Then
                    sprdLines.Col = COL_VOIDED
                    If (Val(sprdLines.Value) = 0) Then
                        sprdLines.Col = COL_SELLPRICE
                        sprdLines.Value = Format(sprdLines.Value * (1 - (dblDiscRate / 100)), "0.00")
                        sprdLines.Col = COL_DISCCODE
                        sprdLines.Text = mstrTranDiscCode
                    End If
                End If
            Next lngRowNo
        Else 'apply discount to specific items
            For lngItemNo = 1 To UBound(strAllocItems) Step 1
                For lngRowNo = 2 To sprdLines.MaxRows Step 1
                    sprdLines.Row = lngRowNo
                    sprdLines.Col = COL_ITEMNO
                    If (Val(sprdLines.Value) = strAllocItems(lngItemNo)) Then
                        sprdLines.Col = COL_SELLPRICE
                        sprdLines.Value = Format(sprdLines.Value * (1 - (dblDiscRate / 100)), "0.00")
                        sprdLines.Col = COL_DISCCODE
                        sprdLines.Text = mstrTranDiscCode
                    End If
                Next lngRowNo
            Next lngItemNo
        End If
        Call UpdateTotals
        'Display transaction total
        sprdLines.Row = sprdLines.MaxRows - 1
        sprdTotal.Row = ROW_TOTTOTALS
        sprdTotal.Col = COL_TOTVAL
        sprdLines.Row = sprdLines.MaxRows - 1
        sprdLines.Col = COL_INCTOTAL
        sprdLines.Text = sprdTotal.Text
        mdblTranTotal = sprdTotal.Text
        sprdTotal.Text = mdblTranTotal
    End If
    fraTender.Visible = True
    Unload frmTranDisc
    'if curtotal
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("EnterTransactionDisc", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub BackDoorCollections()

Dim lngItemNo   As Long
Dim strSKU      As String
Dim strDesc     As String
Dim strQuantity As String
Dim strCollect  As String
Dim colItems    As Collection

    Call DebugMsg(MODULE_NAME, "BackDoorCollections", endlTraceIn)
    Load frmBackDoorCollect
    
    'copy Line items onto the Back Door Collection screen
    For lngItemNo = 2 To sprdLines.MaxRows Step 1
        sprdLines.Row = lngItemNo
        sprdLines.Col = COL_VOIDED
        If (Val(sprdLines.Value) = 0) Then
            sprdLines.Col = COL_QTY
            If (Val(sprdLines.Value) > 0) Then 'ensure sold item
                sprdLines.Col = COL_PARTCODE
                strSKU = sprdLines.Text
                sprdLines.Col = COL_DESC
                strDesc = sprdLines.Text
                sprdLines.Col = COL_QTY
                strQuantity = sprdLines.Text
                sprdLines.Col = COL_BACKCOLLECT
                strCollect = sprdLines.Text
                Call frmBackDoorCollect.AddItem(lngItemNo, strSKU, strDesc, strQuantity, strCollect, "", "", "")
            End If 'item being sold - ignore refund lines
        End If 'item not voided
    Next lngItemNo
    'Get list of items to Collect or not
    Set colItems = frmBackDoorCollect.ConfirmCollect
    Call frmBackDoorCollect.PopulateTakeNow
    Unload frmBackDoorCollect
    
    'Transfer customer requirements onto front end
'    If ((colItems Is Nothing) = False) Then
'        'Reset collection flags
'        For lngItemNo = 2 To sprdLines.MaxRows Step 1
'            sprdLines.Row = lngItemNo
'            sprdLines.Col = COL_LINETYPE
'            If (Val(sprdLines.Value) = LT_ITEM) Then
'                sprdLines.Col = COL_BACKCOLLECT
'                sprdLines.Text = "N"
'            End If
'        Next lngItemNo
'        'Now copy responses onto front end
'        For lngItemNo = 1 To colItems.Count Step 1
'            sprdLines.Row = colItems(lngItemNo)
'            sprdLines.Col = COL_BACKCOLLECT
'            sprdLines.Text = "Y"
'        Next lngItemNo
'    End If
    If (txtSKU.Enabled = True) And (txtSKU.Visible = True) Then Call txtSKU.SetFocus

End Sub

Private Function ConfirmStockLevels() As Boolean

Dim lngItemNo   As Long
Dim strSKU      As String
Dim strDesc     As String
Dim strQuantity As String
Dim strCollect  As String
Dim strOnHand   As String
Dim strOnOrder  As String
Dim strSuppNo   As String
Dim strProcedureName As String
    
    On Error GoTo HandleException
    
    strProcedureName = "ConfirmStockLevels"
    
    Call DebugMsg(MODULE_NAME, strProcedureName, endlTraceIn)
    Load frmBackDoorCollect
    
    Call DebugMsgSprdLinesValue(strProcedureName)
    'copy Line items onto the Back Door Collection screen
    For lngItemNo = 2 To sprdLines.MaxRows Step 1
        sprdLines.Row = lngItemNo
        sprdLines.Col = COL_VOIDED
        If (Val(sprdLines.Value) = 0) Then
            sprdLines.Col = COL_SALETYPE
            If (sprdLines.Value <> "D") Then
                sprdLines.Col = COL_QTY
                If (Val(sprdLines.Value) > 0) Then 'ensure sold item
                    sprdLines.Col = COL_PARTCODE
                    strSKU = sprdLines.Text
                    sprdLines.Col = COL_DESC
                    strDesc = sprdLines.Text
                    sprdLines.Col = COL_QTY
                    strQuantity = sprdLines.Text
                    sprdLines.Col = COL_ONHAND
                    strOnHand = Val(sprdLines.Text)
                    sprdLines.Col = COL_QTYONORDER
                    strOnOrder = sprdLines.Text
                    sprdLines.Col = COL_BACKCOLLECT
                    strCollect = sprdLines.Text
                    sprdLines.Col = COL_SUPPNO
                    strSuppNo = sprdLines.Text
                    Call frmBackDoorCollect.AddItem(-1, strSKU, strDesc, strQuantity, strCollect, strOnHand, strOnOrder, strSuppNo)
                    Call DebugMsgSprdLineValue(lngItemNo, strProcedureName & "(BackDoorCollect)")
                End If 'item being sold - ignore refund lines
            End If 'Ignore any delivery charges
        End If 'item not voided
    Next lngItemNo
    'Get list of items to Collect or not
    ConfirmStockLevels = frmBackDoorCollect.ConfirmCreateOrder
    If ConfirmStockLevels = True Then
        Call frmBackDoorCollect.PopulateTakeNow
        mlngQuoteQtyTaken = frmBackDoorCollect.mlngTakeNow
    End If
    Unload frmBackDoorCollect
    
    If (txtSKU.Enabled = True) And (txtSKU.Visible = True) Then Call txtSKU.SetFocus

    Exit Function

HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler(strProcedureName, Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Function

Private Sub RecordVouchers()

Dim strTestSerial As String
Dim strTokenSerial As String
Dim curTokenVal As Currency

    On Error GoTo HandleException
    
    Call DebugMsg(MODULE_NAME, "RecordVouchers", endlTraceIn)
    SetEntryMode (enemNone)
    mlngCurrentMode = encmGiftVoucher
    If mdblLineQuantity < 0 Then
        mdblLineQuantity = -1
        
        frmTokenScan.GetTokenToSell (ucKeyPad1.Visible)
        If frmTokenScan.Cancel Then
            mdblLineQuantity = 1
            mlngCurrentMode = encmRecord
            Call SetEntryMode(enemSKU)
            Exit Sub
        End If
        strTestSerial = frmTokenScan.ScanData
        
        frmTokenScan.GetTokenForTender (ucKeyPad1.Visible)
        If frmTokenScan.Cancel Then
            mdblLineQuantity = 1
            mlngCurrentMode = encmRecord
            Call SetEntryMode(enemSKU)
            Exit Sub
        End If
        
        Call DecodeGiftTokenBarcode(frmTokenScan.ScanData, strTokenSerial, curTokenVal)
        
        If strTestSerial <> strTokenSerial Then
            Call MsgBoxEx("Serial numbers do NOT match", vbOKOnly & vbExclamation, _
                          "Invalid Gift Token", , , , , RGBMsgBox_WarnColour)
            mdblLineQuantity = 1
            mlngCurrentMode = encmRecord
            Call SetEntryMode(enemSKU)
            Exit Sub
        End If
        
        If GiftVoucherValid(strTokenSerial, vbNullString) = False Then
            mdblLineQuantity = 1
            mlngCurrentMode = encmRecord
            Call SetEntryMode(enemSKU)
            Exit Sub
        End If
        
        If GiftVoucherNotUsed(strTokenSerial, True) = False Then
            mdblLineQuantity = 1
            mlngCurrentMode = encmRecord
            Call SetEntryMode(enemSKU)
            Exit Sub
        End If
        
        Call AddVoucherSKUs(curTokenVal)
        sprdLines.Col = COL_VOUCHERSERIAL
        sprdLines.Text = strTokenSerial
        
        Call SetEntryMode(enemSKU)
        Call EnableAction(enacVoid, True)
        Call EnableAction(enacPark, False)
        Call EnableAction(enacReversal, True)
        mlngCurrentMode = encmRecord
    Else
        SetEntryMode (enemValue)
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("RecordVouchers", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub TransactionLookUp()

Dim lngLineNo   As Long
Dim lngItemNo   As Long
Dim oTillTran   As cPOSHeader
Dim oTranLine   As cPOSLine
Dim oPOCode     As cPriceOverrideCode
Dim colRefundLines As Collection

Dim strStoreNo    As String
Dim strTillID     As String
Dim dteTrandate   As Date
Dim strTranNumber As String
Dim lngSaveMode   As Long
Dim strLocation   As String
Dim strRefundCode As String ' for No Validated Trans - get a Refund Reason Code
Dim strSelectedSKU As String
Dim dblOrigQuan    As Double
Dim dblOrigPrice   As Double
Dim blnVoided      As Boolean
Dim blnTendered    As Boolean
Dim blnOtherStore  As Boolean
Dim blnCancelled   As Boolean
Dim colPOCodes     As Collection
Dim colRefundTrans As Collection

Dim oMarkDown      As cMarkDownStock
Dim colList        As Collection
Dim intItem        As Integer
Dim intLineNo      As Integer
Dim cLines         As cRefundLine
Dim NextPayComm    As Integer   ' Commidea Into WSS
Dim oConn          As Connection
Dim tmpLine        As cRefundLine

Dim RefundOrderNumber As String

    On Error GoTo HandleException
    
    Set oConn = goDatabase.Connection

    Call DebugMsg(MODULE_NAME, "TransactionLookUp", endlTraceIn)
    Load frmRetrieveTran
    With frmRetrieveTran.uctrRefund
        .ParkedOnly = mblnRecallingParked
        .InTrainingMode = mblnTraining
    End With
    lngSaveMode = mlngCurrentMode
    If mlngCurrentMode <> encmRefund Then
        mlngCurrentMode = encmRetrieveTran
    End If
    
    mblnHaveRefPrice = False
    If (mblnRecallingParked = False) Then 'performing refund
        If ((mcolRefundCodes Is Nothing) = True) Then Call LoadRefundReasons
        Set colPOCodes = New Collection
        Call colPOCodes.Add(GetRefundPriceOverrideCode)
        Set frmRetrieveTran.mcolRefundTrans = mcolRefundTrans
        Call frmRetrieveTran.SelectTranLines(strStoreNo, strTranNumber, dteTrandate, strTillID, _
                                             mcolRefundCodes, colPOCodes, colRefundLines, strRefundCode, blnCancelled, blnVoided, RefundOrderNumber)

        If blnVoided Then    'CR0017 - Void Refund transaction with any line at Delivery Status at 700-799
            VoidTransaction (False) 'This was decided with in the retrieve transaction ocx.
            Exit Sub
        End If
        
        If (blnCancelled = True) Then
            Unload frmRetrieveTran
            mdblLineQuantity = 1
            mlngCurrentMode = encmRecord
            Call SetEntryMode(enemSKU)
            Exit Sub
        Else
            If mcolRefundTrans Is Nothing Then
                Set mcolRefundTrans = frmRetrieveTran.mcolRefundLines
            Else
                If Not frmRetrieveTran.mcolRefundLines Is Nothing Then
                    For intLineNo = 1 To frmRetrieveTran.mcolRefundLines.Count
                        Set cLines = frmRetrieveTran.mcolRefundLines(intLineNo)
                        'Changes for referral 904 Icorrect Delivery value and Cancelled value for refunds
                        If GetBooleanParameter(-904, oConn) Then
                            Dim AlreadyAddedCount As Integer
                            Dim AlreadyAdded As Boolean
                            
                            AlreadyAdded = False
                            For AlreadyAddedCount = 1 To mcolRefundTrans.Count
                                Set tmpLine = mcolRefundTrans(AlreadyAddedCount)
                                If tmpLine.PartCode = cLines.PartCode Then
                                    tmpLine.QtyCancelled = tmpLine.QtyCancelled + cLines.QtyCancelled
                                    tmpLine.QtyReturned = tmpLine.QtyReturned + cLines.QtyReturned
                                    tmpLine.RefundDeliveryQty = tmpLine.RefundDeliveryQty - cLines.QtyCancelled
                                    AlreadyAdded = True
                                End If
                            Next
                            If Not AlreadyAdded Then
                                Call mcolRefundTrans.Add(cLines)
                            End If
                        Else
                            Call mcolRefundTrans.Add(cLines)
                        End If
                    Next intLineNo
                End If
            End If
        End If
        
        Set moRefundOrigTran = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
        ' no proof of purchase so only refund in tokens
        If (LenB(Trim$(strTillID)) = 0) Then
         If EnableRefundTenderType Then
            mblnNonValidated = False
        Else
            mblnNonValidated = True
        End If
            mblnRefLineValid = False
            mblnTokensOnly = True
        ' Hubs 2.0 - Add refund to a WebOrder to be processed like No Proof of Purchase, but
        ' with originating store details!
        ElseIf frmRetrieveTran.IsWebOrder Then
            mblnNonValidated = True
            mblnRefLineValid = False
            If (mstrRefundTrans = "") Then 'save first transaction to print on Refund Auth Slip
                mstrRefundTrans = Format(strStoreNo, "000") & Format(strTillID, "00") & Format(strTranNumber, "0000") & Format(dteTrandate, "DD/MMM/YYYY")
            End If
            mstrRefOrigStoreNo = strStoreNo
            mdteRefOrigTranDate = dteTrandate
            mstrRefOrigTillID = strTillID
            mstrRefOrigTranNo = strTranNumber
            mlngRefOrigLineNo = lngLineNo
            mstrWebOrderNumber = frmRetrieveTran.WebOrderNumber
            ' Referral 771; Flag up a web order refund in this transaction, so can refuse to park it if attempted later
            mblnTransactionHasWebOrderNoRefund = True
            mstrRefOrigOrderNo = "000000"
            mblnAllowAllTenders = True
            mblnTokensOnly = False
        ' End of Hubs 2.0
        Else
            'if transaction from other store then save refund details for line save
            If (strStoreNo <> goSession.CurrentEnterprise.IEnterprise_StoreNumber) And (strStoreNo <> "") Then
                If (mstrRefundTrans = "") Then 'save first transaction to print on Refund Auth Slip
                    mstrRefundTrans = Format(strStoreNo, "000") & Format(strTillID, "00") & Format(strTranNumber, "0000") & Format(dteTrandate, "DD/MMM/YYYY")
                End If
                mstrRefOrigStoreNo = strStoreNo
                mdteRefOrigTranDate = dteTrandate
                mstrRefOrigTillID = strTillID
                mstrRefOrigTranNo = strTranNumber
                mlngRefOrigLineNo = lngLineNo
                mstrRefOrigOrderNo = "000000"
                mblnAllowAllTenders = True
                mblnTokensOnly = False
            Else
                'from this store so get Transactions to store details against
                With moRefundOrigTran
    '                .TranDate = CStr(dteTranDate)
    '                .TillID = strTillID
    '                .TransactionNo = strTranNumber
                    Call .AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, dteTrandate)
                    Call .AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, strTillID)
                    Call .AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, strTranNumber)
                End With
                Set colRefundTrans = moRefundOrigTran.LoadMatches
                If (colRefundTrans.Count > 0) Then
                    Set moRefundOrigTran = colRefundTrans.Item(1)
                    mstrRefOrigStoreNo = goSession.CurrentEnterprise.IEnterprise_StoreNumber
                    mdteRefOrigTranDate = dteTrandate
                    mstrRefOrigTillID = strTillID
                    mstrRefOrigTranNo = strTranNumber
                    mlngRefOrigLineNo = lngLineNo
                    mblnRefLineValid = True
                    If EnableNewPICRefundCountProcess Then
                        moTranHeader.OrderNumber = RefundOrderNumber
                    Else
                        moTranHeader.OrderNumber = moRefundOrigTran.OrderNumber
                    End If
                    If moRefundOrigTran.Payments.Count > 0 Then
                        Set moOrigTend = moRefundOrigTran.Payments(1)
                    End If
                    ' Commidea Into WSS
                    ' Get matching PaymentComm for Payment
                    If moRefundOrigTran.PaymentComms.Count > 0 Then
                        For NextPayComm = 1 To moRefundOrigTran.PaymentComms.Count
                            Set moOrigTendComm = moRefundOrigTran.PaymentComms.Item(NextPayComm)
                            If moOrigTend.TransactionDate = moOrigTendComm.TransactionDate _
                            And moOrigTend.TillID = moOrigTendComm.TillID _
                            And moOrigTend.TransactionNumber = moOrigTendComm.TransactionNumber _
                            And moOrigTend.SequenceNumber = moOrigTendComm.SequenceNumber Then
                                Exit For
                            Else
                                Set moOrigTendComm = Nothing
                            End If
                        Next NextPayComm
                    End If
                    ' End of Commidea Into WSS
                    If dblOrigPrice <> 0 Then
                        mblnHaveRefPrice = True
                        mcurRefundPrice = dblOrigPrice
                    End If
                    'Check to see if its a Markdown Item
                    If (mstrMarkDownStyle <> vbNullString) Then
                        Set oMarkDown = goDatabase.CreateBusinessObject(CLASSID_MARKDOWNSTOCK)
                        Call oMarkDown.AddLoadFilter(CMP_EQUAL, FID_MARKDOWNSTOCK_SoldTransaction, mstrRefOrigTranNo)
                        Call oMarkDown.AddLoadFilter(CMP_EQUAL, FID_MARKDOWNSTOCK_SoldTill, mstrRefOrigTillID)
                        Call oMarkDown.AddLoadFilter(CMP_EQUAL, FID_MARKDOWNSTOCK_SoldDate, Format(mdteRefOrigTranDate, "yyyy-mm-dd"))
                        Set colList = oMarkDown.LoadMatches
                        If colList.Count > 0 Then
                            'Found one
                            For intItem = 1 To colList.Count
                                Set oMarkDown = colList(intItem)
                                Debug.Print oMarkDown.SKUNumber & " : " & oMarkDown.Serial
                            Next intItem
                        End If
                    End If
                Else
                    Set moRefundOrigTran = Nothing
                End If
                
            End If 'transaction from this store and is validated
        End If 'processing validated transaction
    Else 'retrieve parked transaction
        strLocation = mstrStoreNo
        Call frmRetrieveTran.RetrieveTransaction(strLocation, dteTrandate, strTillID, _
                                                 strTranNumber, blnOtherStore)
    End If
    
    Unload frmRetrieveTran
    mlngCurrentMode = lngSaveMode
'    If lstMatches.ListIndex = -1 Then Exit Sub
    
    
   If (mblnRecallingParked = False) Then   'See if this fixes dave s Refund issue
        'for passed out lines, added each one to Transaction
        If ((colRefundLines Is Nothing) = False) Then
            For lngItemNo = 1 To colRefundLines.Count Step 1
                
                mstrRefOrigStoreNo = goSession.CurrentEnterprise.IEnterprise_StoreNumber
                mdteRefOrigTranDate = dteTrandate
                mstrRefOrigTillID = strTillID
                mstrRefOrigTranNo = strTranNumber
                mblnRefLineValid = True
                mblnHaveRefPrice = False
                
                strSelectedSKU = colRefundLines(lngItemNo).PartCode
                dblOrigQuan = colRefundLines(lngItemNo).RefundQuantity
                mdblLineQuantity = dblOrigQuan * -1
                dblOrigPrice = colRefundLines(lngItemNo).Price
                lngLineNo = colRefundLines(lngItemNo).LineNo
                mlngRefOrigLineNo = lngLineNo
                mstrRefundReason = colRefundLines(lngItemNo).RefundReasonCode
                mlngCurrentMode = encmRecord
                Call SetEntryMode(enemSKU)
                If LenB(strSelectedSKU) <> 0 Then
                    If strSelectedSKU = mstrGiftVoucherSKU Then
                        RecordVouchers
                    Else
'                        If (dblOrigPrice <> 0) Then 'use validated price instead of system price
                            mblnHaveRefPrice = True
                            mcurRefundPrice = dblOrigPrice + colRefundLines(lngItemNo).WEEEUnitCharge
'                        End If
                        mlngBCodeCheckCnt = mlngBCodeCheckCnt - 1 'effectively reverse out SKU count added in processing SKU
                        txtSKU.Text = strSelectedSKU
                        cmdProcess.Value = True
                        
                        sprdLines.Col = COL_WEEE_RATE
                        sprdLines.Text = colRefundLines(lngItemNo).WEEEUnitCharge
                        sprdLines.Col = COL_WEEE_SKU
                        sprdLines.Text = colRefundLines(lngItemNo).WEEESKU 'This is the WEEE SKU from the Stock Master
                        
                        Set oTranLine = goDatabase.CreateBusinessObject(CLASSID_POSLINE)
                        Call oTranLine.AddLoadField(FID_POSLINE_PriceOverrideReason)
                        Call oTranLine.AddLoadField(FID_POSLINE_OverrideMarginCode)
                        Call oTranLine.AddLoadFilter(CMP_EQUAL, FID_POSLINE_TranDate, dteTrandate)
                        Call oTranLine.AddLoadFilter(CMP_EQUAL, FID_POSLINE_TillID, strTillID)
                        Call oTranLine.AddLoadFilter(CMP_EQUAL, FID_POSLINE_TransactionNumber, strTranNumber)
                        Call oTranLine.AddLoadFilter(CMP_EQUAL, FID_POSLINE_SequenceNo, lngLineNo)
                        Set oTranLine = oTranLine.LoadMatches.Item(1)
                        
                        If oTranLine.PriceOverrideAmount <> 0 Then
                            sprdLines.Col = COL_DISCCODE
                            sprdLines.Text = moRefPriceOverride.Code
                            sprdLines.Col = COL_DISCMARGIN
                            sprdLines.Text = Right$("000000" & moRefPriceOverride.Code, 6)
                        End If
                        
                        If (Val(colRefundLines(lngItemNo).PriceOverrideReason) <> 0) Then
                            sprdLines.Col = COL_DISCCODE
                            sprdLines.Text = colRefundLines(lngItemNo).PriceOverrideReason
                            sprdLines.Col = COL_DISCMARGIN
                            sprdLines.Text = Right$("000000" & colRefundLines(lngItemNo).PriceOverrideReason, 6)
                        End If
                        sprdLines.Col = COL_CUSTONO
                        sprdLines.Text = colRefundLines(lngItemNo).OrderNumber
                        
                        sprdLines.Col = COL_KEYED_RC
                        sprdLines.Text = ""
                        
                    End If
                End If
            Next lngItemNo
        Else
            If (mstrRefOrigStoreNo = "") Then mstrRefOrigStoreNo = goSession.CurrentEnterprise.IEnterprise_StoreNumber
            mdteRefOrigTranDate = dteTrandate
            mstrRefOrigTillID = strTillID
            mstrRefOrigTranNo = strTranNumber
            mblnRefLineValid = True
            
            strSelectedSKU = ""
            mlngRefOrigLineNo = 0
            mstrRefundReason = strRefundCode
            mlngCurrentMode = encmRecord
            Call SetEntryMode(enemSKU)
            If LenB(strSelectedSKU) <> 0 Then
                If strSelectedSKU = mstrGiftVoucherSKU Then
                    RecordVouchers
                Else
                    txtSKU.Text = strSelectedSKU
                    mlngBCodeCheckCnt = mlngBCodeCheckCnt - 1 'effectively reverse out SKU count added in processing SKU
                    cmdProcess.Value = True
                    
                    Set oTranLine = goDatabase.CreateBusinessObject(CLASSID_POSLINE)
                    Call oTranLine.AddLoadField(FID_POSLINE_PriceOverrideReason)
                    Call oTranLine.AddLoadField(FID_POSLINE_OverrideMarginCode)
                    Call oTranLine.AddLoadFilter(CMP_EQUAL, FID_POSLINE_TranDate, dteTrandate)
                    Call oTranLine.AddLoadFilter(CMP_EQUAL, FID_POSLINE_TillID, strTillID)
                    Call oTranLine.AddLoadFilter(CMP_EQUAL, FID_POSLINE_TransactionNumber, strTranNumber)
                    Call oTranLine.AddLoadFilter(CMP_EQUAL, FID_POSLINE_SequenceNo, lngLineNo)
                    Set oTranLine = oTranLine.LoadMatches.Item(1)
                    
                    If oTranLine.PriceOverrideAmount <> 0 Then
                        sprdLines.Col = COL_DISCCODE
                        sprdLines.Text = moRefPriceOverride.Code
                        sprdLines.Col = COL_DISCMARGIN
                        sprdLines.Text = Right$("000000" & moRefPriceOverride.Code, 6)
                    End If
                    sprdLines.Col = COL_KEYED_RC
                    sprdLines.Text = ""
                End If
            Else
                mlngBCodeCheckCnt = mlngBCodeCheckCnt - 1
            End If
        End If
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        
    End If

    ' Retrieving a parked transaction, so load header and lines and display
    If LenB(Trim(strTranNumber)) = 0 Then
        mblnRecallingParked = False
        Exit Sub
    End If
    
    Set oTillTran = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    Call oTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, dteTrandate)
    Call oTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, strTillID)
    Call oTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, strTranNumber)
    
    Call oTillTran.LoadMatches
    
    Call DisplayParkedTran(oTillTran, False)
    Call SetEntryMode(enemSKU)
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("TransactionLookUp", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub CheckVATHistory(ByVal OrigTranDate As Date)
                        
Dim oParamBO       As cEnterprise_Wickes.cParameter

Dim dteStart    As Date
Dim dteEnd      As Date
Dim strVATCode  As String
Dim strRates()  As String
Dim lngValuePos As Long
    
    If (OrigTranDate >= Date) Then Exit Sub
    
    'Added 26/11/08 - overwrite VAT Rates
    For Each oParamBO In mcolPreviousVATRates
        dteStart = CDate(Left$(oParamBO.Value, 10))
        dteEnd = CDate(Mid$(oParamBO.Value, 12, 10))
        'Check if Transaction date falls within VAT Rate History
        If (OrigTranDate >= dteStart) And (OrigTranDate <= dteEnd) Then
            strRates = Split(Mid$(oParamBO.Value, InStr(oParamBO.Value, ";") + 1), ",")
            For lngValuePos = LBound(strRates) To UBound(strRates) Step 1
                If (strRates(lngValuePos) <> "") Then
                    strVATCode = Left$(strRates(lngValuePos), 1)
                    sprdLines.Col = COL_VATCODE
                    'If otrher VAT Rate then extract and save in Grid for saving to DB
                    If (Val(sprdLines.Value) = Val(strVATCode)) Then
                        sprdLines.Text = Mid$(strRates(lngValuePos), 2, 1)
                        sprdLines.Col = COL_VATRATE
                        sprdLines.Value = Val(Mid$(strRates(lngValuePos), 3))
                        'Update Tran Header to show historical VAT Rate
                        moTranHeader.VATRates(Asc(Mid$(strRates(lngValuePos), 2, 1)) - 96) = Val(Mid$(strRates(lngValuePos), 3))
                        Exit Sub
                    End If
                End If 'valiud VAT Rate to process
            Next lngValuePos
        End If 'date range matches
    Next

End Sub

Public Sub DisplayParkedTran(ByRef oTillTranBO As cPOSHeader, blnFromQOD As Boolean)

Dim lngNoLines  As Long
Dim colRetCusts As Collection
Dim oReturnCust As cReturnCust
Dim colLines    As Collection
Dim oTranLine   As cPOSLine
Dim oLine   As cPriceLineInfo
Dim lngPPLineNo As Long

'    lblTranID.Caption = "NA"
'    lblStoreNo.Caption = "NA"
'    lblTranDate.Caption = DisplayDate(TranDate, False)
'    lblTranNo.Caption = TranNumber
'    lblTillID.Caption = TillID
    Call DebugMsg(MODULE_NAME, "DisplayParkedTran", endlTraceIn)
    lblOrderNo.Caption = oTillTranBO.OrderNumber
    
    mstrRefundCashier = oTillTranBO.RefundCashier
    
    sprdLines.MaxRows = 1
    Call SetGridTotals(sprdLines.MaxRows)
    Call UpdateTotals
    
    Set colLines = oTillTranBO.Lines
    lngNoLines = colLines.Count
    If GetBooleanParameter(-8, goDatabase.Connection) Then
        If (oTillTranBO.LoadPricePromiseLineInfo = True) Then Set moParkTranHdr = oTillTranBO
    Else
        If (oTillTranBO.LoadPricePromiseInfo = True) Then Set moParkTranHdr = oTillTranBO
    End If
    
    For Each oTranLine In colLines
        If (oTranLine.LineReversed = False) And (oTranLine.SaleType <> "W") Then
            Call AddOriginalLine(oTillTranBO, oTranLine, lngPPLineNo)
        End If
    Next oTranLine
    
    If sprdLines.MaxRows > 1 Then
        Call EnableAction(enacVoid, True)
        Call EnableAction(enacPark, True)
        Call EnableAction(enacReversal, True)
    End If
    
    Set oReturnCust = goDatabase.CreateBusinessObject(CLASSID_RETURNCUST)
    With oReturnCust
        Call .AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionDate, oTillTranBO.TranDate)
        Call .AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_PCTillID, oTillTranBO.TillID)
        Call .AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionNumber, oTillTranBO.TransactionNo)
        Call .AddLoadFilter(CMP_NOTEQUAL, FID_RETURNCUST_LineNumber, 0)
        Call .AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_OrigTranValidated, False)
    
        If .LoadMatches.Count > 0 Then
            mblnTokensOnly = True
        End If
        
        Call .ClearLoadFilter
        Call .AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionDate, oTillTranBO.TranDate)
        Call .AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_PCTillID, oTillTranBO.TillID)
        Call .AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionNumber, oTillTranBO.TransactionNo)
        Call .AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_LineNumber, 0)
        Set colRetCusts = .LoadMatches
    End With
    
    If colRetCusts.Count > 0 Then
        Set oReturnCust = colRetCusts(1)
        With oReturnCust
            mstrAddress = .AddressLine1 & vbNewLine & .AddressLine2 & _
                          vbNewLine & .AddressLine3
            mstrName = .CustomerName
            mstrPostCode = .PostCode
        End With
    End If
    
    With oReturnCust
        Call .AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionDate, oTillTranBO.TranDate)
        Call .AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_PCTillID, oTillTranBO.TillID)
        Call .AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionNumber, oTillTranBO.TransactionNo)
        Call .AddLoadFilter(CMP_NOTEQUAL, FID_RETURNCUST_LineNumber, 0)
        Call .AddLoadFilter(CMP_NOTEQUAL, FID_RETURNCUST_OrigTranValidated, False)
        Set colRetCusts = .LoadMatches
    End With
    
    If colRetCusts.Count > 0 Then
        Set oReturnCust = colRetCusts(1)
        With oReturnCust
            If .OrigTranTill <> "00" Then
                Set moRefundOrigTran = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
                Call moRefundOrigTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, .OrigTranDate)
                Call moRefundOrigTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, .OrigTranTill)
                Call moRefundOrigTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, .OrigTranNumb)
                'Set moRefundOrigTran = moRefundOrigTran.LoadMatches.Item(1)
                Call moRefundOrigTran.LoadMatches
                moRefundOrigTran.StoreNumber = .OrigTranStore
            Else
                mblnAllowAllTenders = True
            End If
        End With
    End If
    

    'Added 29/06/2007 MO'C - WIX1311 Referal reapply discount to parked transaction if a discount was swiped when parked
    If Len(oTillTranBO.DiscountCardNumber) > 0 Then
        Set moDiscountCardScheme = goDatabase.CreateBusinessObject(CLASSID_DISCOUNTCARD_SCHEME_ID)
        Call moDiscountCardScheme.AddLoadFilter(CMP_GREATEREQUALTHAN, FID_DISCOUNTCARD_SCHEME_ID_EndCardNumberRange, oTillTranBO.DiscountCardNumber)
        Call moDiscountCardScheme.AddLoadFilter(CMP_LESSEQUALTHAN, FID_DISCOUNTCARD_SCHEME_ID_StartCardNumberRange, oTillTranBO.DiscountCardNumber)
        Call moDiscountCardScheme.LoadMatches
        If moDiscountCardScheme.SchemeName = "" Then
            'Card scheme could not be found they will need to re swipe the discount card.
            Call MsgBoxEx("Unable to recall the discount scheme. You will need to apply the discount again.", vbOKOnly, "Discount Scheme Not Found", , , , , RGBMsgBox_WarnColour)
        End If
    End If
    
    'Flag Transactions as recalled so it is no longer displayed
    oTillTranBO.TranParked = False
    oTillTranBO.RecoveredFromParked = True
    oTillTranBO.SaveIfExists
    mblnRecalledTran = True 'flag so that no Refund Items can be added
    
    mdblLineQuantity = 1
    
    Set oReturnCust = Nothing
    Set colRetCusts = Nothing

End Sub

Private Sub cmdCurrency_Click()

Dim strCurrCode As String
Dim curExchRate As Currency
Dim lngDecPos   As Long
Dim curAmountTendered As Currency
Dim curForeignAmt     As Currency

Dim curTotal    As Currency

    On Error GoTo HandleException
    
    Call DebugMsg(MODULE_NAME, "cmdCurrency_Click", endlTraceIn)
    Load frmCurrExch
    
    sprdTotal.Row = 3
    sprdTotal.Col = COL_TOTVAL
    curTotal = Val(sprdTotal.Value)
    
    If (frmCurrExch.GetCurrency(mcolCurrencies, curTotal, strCurrCode, curExchRate, lngDecPos, curAmountTendered, curForeignAmt) = True) Then
        sprdLines.Row = sprdLines.MaxRows
        sprdLines.Col = COL_CFOREIGN_AMT
        sprdLines.Text = curForeignAmt
        sprdLines.Col = COL_CFOREIGN_CODE
        sprdLines.Text = strCurrCode
        sprdLines.Col = COL_CFOREIGN_EXCH
        sprdLines.Text = curExchRate
        sprdLines.Col = COL_CFOREIGN_POWER
        sprdLines.Text = lngDecPos
        uctntPrice.Value = curAmountTendered
        DoEvents
        Call uctntPrice_KeyPress(vbKeyReturn)
    Else
        If (uctntPrice.Visible = True) And (uctntPrice.Enabled = True) Then Call uctntPrice.SetFocus
        'Call SendKeys(vbKeyEscape)
    End If
    Unload frmCurrExch

    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("cmdCurrency_Click", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub cmdExitTill_Click()

    On Error GoTo HandleException

    Call DebugMsg(MODULE_NAME, "cmdExitTill_Click", endlTraceIn)
    Screen.MousePointer = vbHourglass
    Select Case (mlngCurrentMode)
        Case (encmLogon):
            If (txtUserID.Visible = True) And (txtUserID.Enabled = True) Then Call txtUserID.SetFocus
            Call Form_KeyPress(vbKeyEscape)
        Case (encmTranType):
            Call Form_KeyPress(vbKeyEscape)
            If (txtUserID.Visible = True) And (txtUserID.Enabled = True) Then Call txtUserID.SetFocus
            Call Form_KeyPress(vbKeyEscape)
    End Select
    Screen.MousePointer = vbNormal
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("cmdExitTill_Click", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub cmdListCancel_Click()

    Call lstReasonsList_KeyPress(vbKeyEscape)

End Sub

Private Sub cmdListOk_Click()

    Call lstReasonsList_KeyPress(vbKeyReturn)

End Sub

Private Sub cmdNextAction_Click()
    
    On Error GoTo HandleException
        
    Call DebugMsg(MODULE_NAME, "cmdNextAction_Click", endlTraceIn)
    If (cmdNextAction.Visible = True) And (cmdNextAction.Enabled = True) Then cmdNextAction.SetFocus
    mlngActionRowNo = mlngActionRowNo + 1
    Call ShowActionRow
    
    If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then
        txtSKU.SetFocus
    ElseIf (uctntPrice.Visible = True) And (uctntPrice.Enabled = True) Then
        uctntPrice.SetFocus
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("cmdNextAction_Click", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub cmdNextTranType_Click()

    On Error GoTo HandleException

    If (cmdNextTranType.Visible) = True And (cmdNextTranType.Enabled = True) Then cmdNextTranType.SetFocus
    mlngTranTypeRowNo = mlngTranTypeRowNo + 1
    Call ShowTranTypeRow
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("cmdNextTranType_Click", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub LoadReasonCodes(ByVal enrmDisplayList As enReasonMode)

Dim oPOCodes  As cPriceOverrideCode
Dim oMPCodes  As cPaidInCode
Dim oMMCodes  As cPaidOutCode
Dim oTDCodes  As cTransactionDiscount
Dim oODCodes  As cOpenDrawerCode
Dim oSACodes  As cStockAdjustmentCode
Dim oTOCodes  As cTenderOverride
Dim oBCCodes  As cBarcodeBroken
Dim oLRCodes  As cLineReversalCode
Dim lngCodeNo As Long
Dim blnAddMsg As Boolean
Dim colList   As Collection

    On Error GoTo HandleException

    Call DebugMsg(MODULE_NAME, "LoadReasonCodes", endlTraceIn)
    fraReasonsList.Height = sprdLines.Height
    fraReasonsList.Top = sprdLines.Top
    fraReasonsList.Left = sprdLines.Left
    fraReasonsList.Width = sprdLines.Width
    lblReasonDescription.Left = 120
    lblReasonDescription.Width = fraReasonsList.Width - 240
    
    lstReasonsList.Col = 0
    lstReasonsList.ColHide = False
    If (mblnShowKeypadIcon = True) Then cmdListCancel.Visible = True
    
    Call SetEntryMode(enemNone)
    fraActions.Visible = False
    fraReasonsList.Visible = True
    Call lstReasonsList.Clear
    txtReasonDescription.Text = vbNullString
    fraReasonsList.Height = lblReasonDesclbl.Top

    Select Case (enrmDisplayList)
         Case (enrmPriceOverride):
            lblReasonDescription.Caption = "Select Price Override Reason"

            If mcolPriceOverrides Is Nothing Then
               Set oPOCodes = goDatabase.CreateBusinessObject(CLASSID_PRICEOVERRIDE)
               Call oPOCodes.AddLoadFilter(CMP_EQUAL, FID_PRICEOVERRIDE_IsActive, True)
               Set mcolPriceOverrides = oPOCodes.LoadMatches
               
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ' Author   : Partha Dutta
                ' Date     : 05/12/2011
                ' Project  : PO14-03
                ' Notes    : Single SKU Override Price
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            
               lstReasonsList.FontSize = "13.5"
               lstReasonsList.Height = lstReasonsList.Height + 100
            
                ''''''''''''''''''''''''''''''''''''''''''' End : PO14-03 ''''''''''''''''''''''''''''''''''''''''''
            
            End If
            Set colList = mcolPriceOverrides

            'Populate selection list from
            For lngCodeNo = 1 To colList.Count Step 1
                Set oPOCodes = colList(lngCodeNo)
                
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ' Author   : Partha Dutta
                ' Date     : 05/12/2011
                ' Project  : PO14-03
                ' Notes    : Single SKU Override Price
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Dim blnRequirementSwitch As Boolean
                On Error Resume Next
                blnRequirementSwitch = goSession.GetParameter(9801403)
                On Error GoTo 0
                If Not (blnRequirementSwitch = False And oPOCodes.Code = "11") Then

                   Call lstReasonsList.AddItem(oPOCodes.Code & vbTab & oPOCodes.Description)
                   lstReasonsList.ItemData(lstReasonsList.NewIndex) = lngCodeNo

                End If

                'old code
                'Call lstReasonsList.AddItem(oPOCodes.Code & vbTab & oPOCodes.Description)
                'lstReasonsList.ItemData(lstReasonsList.NewIndex) = lngCodeNo
            
                
                ''''''''''''''''''''''''''''''''''''''''''' End : PO14-03 ''''''''''''''''''''''''''''''''''''''''''

            Next lngCodeNo
            
        Case (enrmPaidOut):
            lblReasonDescription.Caption = "Select Paid Out Reason"
            If mcolPaidOutCodes Is Nothing Then
                Set oMMCodes = goDatabase.CreateBusinessObject(CLASSID_PAIDOUTCODE)
                Call oMMCodes.AddLoadFilter(CMP_EQUAL, FID_PAIDOUTCODE_IsActive, True)
                Set mcolPaidOutCodes = oMMCodes.LoadMatches
            End If
            Set colList = mcolPaidOutCodes
        
        Case (enrmPaidIn):
            lblReasonDescription.Caption = "Select Misc Inc Reason"
            If mcolPaidInCodes Is Nothing Then
                Set oMPCodes = goDatabase.CreateBusinessObject(CLASSID_PAIDINCODE)
                Call oMPCodes.AddLoadFilter(CMP_EQUAL, FID_PAIDINCODE_IsActive, True)
                Set mcolPaidInCodes = oMPCodes.LoadMatches
            End If
            Set colList = mcolPaidInCodes
        
        Case (enrmOpenDrawer):
            lblReasonDescription.Caption = "Select Open Drawer Reason"
            If mcolOpenDrawerCodes Is Nothing Then
                Set oODCodes = goDatabase.CreateBusinessObject(CLASSID_OPENDRAWERCODE)
                Set mcolOpenDrawerCodes = oODCodes.LoadMatches
            End If
            Set colList = mcolOpenDrawerCodes
        
        Case (enrmTransactionDiscount):
            lblReasonDescription.Caption = "Select Transaction Discount Reason"
            If mcolTransactionDiscount Is Nothing Then
                Set oTDCodes = goDatabase.CreateBusinessObject(CLASSID_TRANSACTION_DISCOUNT)
                Call oTDCodes.AddLoadField(FID_TRANSACTION_DISCOUNT_Code)
                Call oTDCodes.AddLoadField(FID_TRANSACTION_DISCOUNT_CodeType)
                Call oTDCodes.AddLoadField(FID_TRANSACTION_DISCOUNT_Description)
                Set mcolTransactionDiscount = oTDCodes.LoadMatches
            End If
            Set colList = mcolTransactionDiscount
            
            'Populate selection list from
            For lngCodeNo = 1 To colList.Count Step 1
                Set oTDCodes = colList(lngCodeNo)
                Call lstReasonsList.AddItem(oTDCodes.Description)
'                Call lstReasonsList.AddItem((oTDCodes.Percentage) & " % " & vbTab & (oTDCodes.Description))
                lstReasonsList.ItemData(lstReasonsList.NewIndex) = lngCodeNo
            Next lngCodeNo
        
        Case (enrmMarkDown):
            lblReasonDescription.Caption = "Select Mark Down Reason"
            If mcolMarkDownCodes Is Nothing Then
                Set oSACodes = goDatabase.CreateBusinessObject(CLASSID_STOCKADJCODE)
                Call oSACodes.AddLoadFilter(CMP_EQUAL, FID_STOCKADJCODE_IsMarkDown, True)
                Set mcolMarkDownCodes = oSACodes.LoadMatches
            End If
            Set colList = mcolMarkDownCodes
        
            'Populate selection list from
            For lngCodeNo = 1 To colList.Count Step 1
                Set oSACodes = colList(lngCodeNo)
                Call lstReasonsList.AddItem(oSACodes.AdjustmentNo & vbTab & oSACodes.Description)
                lstReasonsList.ItemData(lstReasonsList.NewIndex) = lngCodeNo
            Next lngCodeNo
    
        Case (enrmRefund):
            lblReasonDescription.Caption = "Select Refund Reason"
            If (mcolRefundCodes Is Nothing) Then Call LoadRefundReasons
            Set colList = mcolRefundCodes
            
'            For lngCodeNo = 1 To colList.Count Step 1
'                Set oRFCodes = colList(lngCodeNo)
'                Call lstReasonsList.AddItem(oRFCodes.Code & vbTab & oRFCodes.Description)
'                lstReasonsList.ItemData(lstReasonsList.NewIndex) = lngCodeNo
'            Next lngCodeNo
            
        Case (enrmTenderOverride):
            lblReasonDescription.Caption = "Select Tender Override Reason"
            If mcolTenderOverrideCodes Is Nothing Then
                Set oTOCodes = goDatabase.CreateBusinessObject(CLASSID_TENDEROVERRIDE)
                Set mcolTenderOverrideCodes = oTOCodes.LoadMatches
            End If
            Set colList = mcolTenderOverrideCodes
            
'        Case (enrmCancelOrder):
'            lblReasonDescription.Caption = "Select Order Cancel Reason"
'            If ((mcolOrderCancel Is Nothing) = True) Then
'                'Set oCodes = goDatabase.CreateBusinessObject(CLASSID_ORDERCANCELCODE)
'                Set mcolOrderCancel = oCodes.LoadMatches
'            End If
'            Set colList = mcolOrderCancel
        
        Case (enrmBarcodeFailed):
            lblReasonDescription.Caption = "Select SKU Keyed In Reason"
            If mcolBarcodeFailedCodes Is Nothing Then
                Set oBCCodes = goDatabase.CreateBusinessObject(CLASSID_BARCODEFAILED)
                Set mcolBarcodeFailedCodes = oBCCodes.LoadMatches
            End If
            Set colList = mcolBarcodeFailedCodes
            mlngCurrentMode = encmBarcodeAudit 'inform Till SKU entry to wait for response
            cmdListCancel.Visible = False
        
        Case (enrmLineReversed): 'WIX1202 - display reason why line has been reversed
            mlngCurrentMode = encmLineReversalCode
            lblReasonDescription.Caption = "Select Line Reversal Reason"
            If mcolLineReversalCodes Is Nothing Then
                Set oLRCodes = goDatabase.CreateBusinessObject(CLASSID_LINEREVERSAL)
                Set mcolLineReversalCodes = oLRCodes.LoadMatches
            End If
            Set colList = mcolLineReversalCodes
            cmdListCancel.Visible = False
        
    End Select
        
    If (enrmDisplayList <> enrmTransactionDiscount) And _
        (enrmDisplayList <> enrmPriceOverride) And _
        (enrmDisplayList <> enrmRefund) And _
        (enrmDisplayList <> enrmMarkDown) Then
        'Populate selection list from
        For lngCodeNo = 1 To colList.Count Step 1
            lstReasonsList.Col = 1
            Select Case enrmDisplayList
                Case enrmPaidIn:
                    Set oMPCodes = colList(lngCodeNo)
                    Call lstReasonsList.AddItem(oMPCodes.Description)
                Case enrmPaidOut:
                    Set oMMCodes = colList(lngCodeNo)
                    Call lstReasonsList.AddItem(oMMCodes.Description)
                Case enrmOpenDrawer:
                    Set oODCodes = colList(lngCodeNo)
                    Call lstReasonsList.AddItem(oODCodes.Description)
                Case enrmMarkDown:
                    Set oSACodes = colList(lngCodeNo)
                    Call lstReasonsList.AddItem(oSACodes.Description)
                Case enrmTransactionDiscount:
                    Set oTDCodes = colList(lngCodeNo)
                    Call lstReasonsList.AddItem(oTDCodes.Description)
                Case enrmTenderOverride:
                    Set oTOCodes = colList(lngCodeNo)
                    Call lstReasonsList.AddItem(oTOCodes.Description)
                Case enrmBarcodeFailed:
                    Set oBCCodes = colList(lngCodeNo)
                    Call lstReasonsList.AddItem(oBCCodes.Description)
                Case enrmLineReversed:
                    Set oLRCodes = colList(lngCodeNo)
                    Call lstReasonsList.AddItem(oLRCodes.Description)
            End Select
            lstReasonsList.ItemData(lstReasonsList.NewIndex) = lngCodeNo
        Next lngCodeNo
        lstReasonsList.Col = 1
        lstReasonsList.ColHide = True
        lstReasonsList.Col = 0
        lstReasonsList.ColWidth = 20
        lstReasonsList.Width = lstReasonsList.ColWidth * 280
    Else
        lstReasonsList.Col = 1
        lstReasonsList.ColWidth = 35
        lstReasonsList.ColHide = False
        lstReasonsList.Col = 0
        lstReasonsList.ColWidth = 8
        lstReasonsList.Width = 48 * 280
    End If
    
    lstReasonsList.Left = (fraReasonsList.Width - lstReasonsList.Width) / 2
    cmdListOk.Left = lstReasonsList.Width + lstReasonsList.Left + 220
    cmdListCancel.Left = cmdListOk.Left
    If (enrmDisplayList <> enrmBarcodeFailed) Then lstReasonsList.ListIndex = 0
    'check if only 1 entry then auto select
    If lstReasonsList.ListCount = 1 Then
        Call lstReasonsList_KeyPress(vbKeyReturn)
    Else 'display list to select from
        If (lstReasonsList.Visible = True) And (cmdNextTranType.Enabled = True) Then Call lstReasonsList.SetFocus
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("LoadReasonCodes", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub LoadRefundReasons()

Dim oRFCodes  As cRefundCode

    If mcolRefundCodes Is Nothing Then
        Set oRFCodes = goDatabase.CreateBusinessObject(CLASSID_REFUNDCODE)
        Call oRFCodes.AddLoadFilter(CMP_EQUAL, FID_REFUNDCODE_IsActive, True)
        Set mcolRefundCodes = oRFCodes.LoadMatches
    End If

End Sub

Private Sub ItemOverride()

    On Error GoTo HandleException
    
    mblnPricePromise = False
                        
    mblnOverrideDeptDesc = goSession.GetParameter(PRM_OVERRIDE_DEPT_DESC)
    mblnCaptureCostPrice = goSession.GetParameter(PRM_CAPTURE_COST_PRICE)
    
    sprdLines.Row = sprdLines.MaxRows
    sprdLines.Col = COL_MARKDOWN
    If sprdLines.Value = "True" Then 'display Mark Down Reasons List
        mlngCurrentMode = encmOverrideCode
        Call LoadReasonCodes(enrmMarkDown)
    Else
        sprdLines.Col = COL_VOUCHER
        If (sprdLines.Value = "V") Then
            Call MsgBoxEx("Unable to perform price change override on Vouchers", vbOKOnly, "Invalid Action", , , , , RGBMsgBox_WarnColour)
            Call SetEntryMode(enemSKU)
            Exit Sub
        End If
        sprdLines.Col = COL_LINETYPE
        If (Val(sprdLines.Value) = LT_COUPON) Then
            Call MsgBoxEx("Unable to perform price change override on Coupons", vbOKOnly, "Invalid Action", , , , , RGBMsgBox_WarnColour)
            Call SetEntryMode(enemSKU)
            Exit Sub
        End If
        'Normal SKU so check that it is already Price Overrided
        sprdLines.Col = COL_LINETYPE
        If (sprdLines.Value = LT_PRICEPROM) Then sprdLines.Row = sprdLines.Row - 1
        'Normal SKU so check that it is not voided
        sprdLines.Col = COL_VOIDED
        If Val(sprdLines.Value) = 0 Then
            sprdLines.Col = COL_QTY
            If (sprdLines.Value < 0) Then
                'Amend Refund Item Price - Need Password
                Call SetEntryMode(enemNone) 'disable barcode
                Load frmVerifyPwd
                If frmVerifyPwd.VerifySupervisorPassword(ucKeyPad1.Visible) = True Then
                    moTranHeader.SupervisorUsed = True
                    Call SetEntryMode(enemNone)
                    sprdLines.Col = COL_SUPERVISOR
                    sprdLines.Text = frmVerifyPwd.txtAuthID.Text
                    Unload frmVerifyPwd
                    mlngCurrentMode = encmRefundPriceOverride
                    sprdLines.Col = COL_DISCCODE
                    sprdLines.Text = moRefPriceOverride.Code
                    sprdLines.Col = COL_DISCMARGIN
                    sprdLines.Text = Right$("000000" & moRefPriceOverride.Code, 6)
                    sprdLines.Col = COL_RFND_STORENO
                    If (sprdLines.Text <> "") Then mblnRefundOverride = True
                    mblnIsRefundLine = True
                    Call SetEntryMode(enemPrice)
                    
                    'VT fix def1077
                    DoEvents
                    uctntPrice.SelectAllValue
                    
                    mlngCurrentMode = encmOverrideCode
                    Exit Sub
                Else 'invalid authorisation code, or escape pressed
                    Unload frmVerifyPwd
                    mlngCurrentMode = encmRecord
                    Call SetEntryMode(enemSKU)
                    Exit Sub
                End If
            Else
                Call SetEntryMode(enemNone)
                mlngCurrentMode = encmOverrideCode
                Call LoadReasonCodes(enrmPriceOverride)
            End If 'Sold Item
        Else
            'Line is voided so just enter next SKU
            mlngCurrentMode = encmRecord
            Call SetEntryMode(enemSKU)
        End If
    End If

    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("ItemOverride", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub ParkTransaction()

Dim vntAddress      As Variant
Dim oReturnCust     As cReturnCust
Dim curRefTotal     As Currency
Dim oRecPrinting    As clsReceiptPrinting
Dim blnSignatureOK  As Boolean
Dim strOrigStoreNo  As String
Dim strOrigTillID   As String
Dim strOrigTranNo   As String
Dim dteOrigTranDate As Date
Dim strMsg          As String
Dim DeliveryStatus  As Integer
    
    On Error GoTo HandleException
    
    If Not mcolRefundTrans Is Nothing Then
        If mcolRefundTrans.Count > 0 Then
            If Val(mcolRefundTrans(1).OrderNumber) > 0 Then
                'This is a QOD Order therefore we cannot park it, let the user know
                DeliveryStatus = GetDeliveryStatus(mcolRefundTrans(1).OrderNumber)
                If (DeliveryStatus >= 700 And DeliveryStatus < 800) Or DeliveryStatus > 899 Then
                    'Its Ok to Park this transaction as it is not recording customer details
                Else
                    strMsg = goSession.GetParameter(PRM_PARKED_REFUND_MSG1) + vbCrLf
                    strMsg = strMsg + goSession.GetParameter(PRM_PARKED_REFUND_MSG2)
                    Call MsgBoxEx(strMsg, vbOKOnly, "Unable to Park Transaction", , , , , RGBMsgBox_WarnColour)
                    Exit Sub
                End If
            End If
        End If
    Else
        ' Hubs 2.0 - Referral 771; Cannot park Web Order refunds, same as QOD orders because cannot
        ' save the customer details which includes the web order number
        If mblnTransactionHasWebOrderNoRefund Then
            strMsg = goSession.GetParameter(PRM_PARKED_REFUND_MSG3) + vbCrLf
            strMsg = strMsg + goSession.GetParameter(PRM_PARKED_REFUND_MSG2)
            Call MsgBoxEx(strMsg, vbOKOnly, "Unable to Park Transaction", , , , , RGBMsgBox_WarnColour)
            Exit Sub
        End If
        ' End of Hubs 2.0
    End If
    
    curRefTotal = Abs(RefundTotal)
    If Not RefundAuthOK(curRefTotal) Then
        Exit Sub
    End If
    
    'Added 5/9/06 - ensure that parked does not have any Colleague Discount info
    mblnApplyCollDisc = False
    mblnColleagueDiscount = False
    moTranHeader.ColleagueCardNo = "000000000"
    
    Set moDiscountCardScheme = Nothing 'Added 29/06/07 - Make sure parked transactions have no discount
    
    If LenB(mstrName) = 0 Then
        Call SetEntryMode(enemNone)
        Call frmCustomerAddress.GetParkedName(mstrName, mstrPostCode, mstrAddress, mstrTelNo)
        If frmCustomerAddress.Cancel Then
            If (moTranHeader.TransactionCode = TT_SALE) And (mblnCustDone = False) Then
                mdblLineQuantity = 1
                mlngCurrentMode = encmRecord
                Call SetEntryMode(enemSKU)
            End If
            Exit Sub
        Else
            If LenB(mstrAddress) <> 0 Then
                vntAddress = Split(mstrAddress, vbNewLine)
                mstrAddressLine1 = vntAddress(0)
                mstrAddressLine2 = vntAddress(1)
                mstrAddressLine3 = vntAddress(2)
            End If
            
            Set oReturnCust = goDatabase.CreateBusinessObject(CLASSID_RETURNCUST)
            
            With oReturnCust
                .TransactionDate = moTranHeader.TranDate
                .PCTillID = moTranHeader.TillID
                .TransactionNumber = moTranHeader.TransactionNo
                .LineNumber = 0
                
                .CustomerName = mstrName
                .AddressLine1 = mstrAddressLine1
                .AddressLine2 = mstrAddressLine2
                .AddressLine3 = mstrAddressLine3
                .PostCode = mstrPostCode
                .PhoneNo = mstrTelNo
                .HouseName = ""
                .LabelsRequired = True
                .RefundReason = "00"
                .SaveIfNew
            End With
        End If
    End If

    mblnParkingTran = True
    mlngCurrentMode = encmCustomer
    Call SetEntryMode(enemNone)
    
    If (curRefTotal <> 0) Then
    
        Set oRecPrinting = New clsReceiptPrinting
        Set oRecPrinting.Printer = OPOSPrinter1
        Set oRecPrinting.TranHeader = moTranHeader
        oRecPrinting.TillNo = mstrTillNo
        oRecPrinting.TranNo = mstrTranId
        Set oRecPrinting.goRoot = goRoot
        Call oRecPrinting.Init(goSession, goDatabase)
        
        vntAddress = Split(mstrAddress, vbNewLine)

        strOrigStoreNo = Left(mstrRefundTrans, 3)
        strOrigTillID = Mid(mstrRefundTrans, 4, 2)
        strOrigTranNo = Mid(mstrRefundTrans, 6, 4)
        If IsDate(Mid(mstrRefundTrans, 10)) Then dteOrigTranDate = CDate(Mid(mstrRefundTrans, 10))
        
        If Not moRefundOrigTran Is Nothing Then
            With moRefundOrigTran
                If (moRefundOrigTran.TransactionDateTime <> 0) Then
                    strOrigStoreNo = .StoreNumber
                    strOrigTillID = .TillID
                    strOrigTranNo = .TransactionNo
                    dteOrigTranDate = .TranDate
                End If
                blnSignatureOK = oRecPrinting.SignatureVerified(sprdLines, vntAddress(0), mstrName, vntAddress(1), _
                                                                vntAddress(2), vntAddress(3), mstrPostCode, _
                                                                strOrigTillID, strOrigTranNo, dteOrigTranDate, strOrigStoreNo)
            End With
        Else
            blnSignatureOK = oRecPrinting.SignatureVerified(sprdLines, vntAddress(0), mstrName, vntAddress(1), _
                                                            vntAddress(2), vntAddress(3), mstrPostCode, vbNullString, _
                                                            vbNullString, vbNull, vbNullString)
        End If
        Set oRecPrinting = Nothing
        
        If blnSignatureOK = False Then
            mdblLineQuantity = 1
            mlngCurrentMode = encmRecord
            Call SetEntryMode(enemSKU)
            Exit Sub
        End If
    End If
    
    
    sprdTotal.Row = ROW_TOTTOTALS
    sprdTotal.Col = COL_TOTVAL
    mdblTotalBeforeDis = Val(sprdTotal.Value)
    
    Call SaveSale(True, False, False, False)
    fraTender.Visible = False
    mlngCurrentMode = encmComplete
    Call Form_KeyPress(vbKeyReturn)
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("ParkTransaction", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub cmdPrevAction_Click()
        
    On Error GoTo HandleException
    
    If (cmdPrevAction.Visible = True) And (cmdPrevAction.Enabled = True) Then cmdPrevAction.SetFocus
    mlngActionRowNo = mlngActionRowNo - 1
    Call ShowActionRow

    If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then
        txtSKU.SetFocus
    ElseIf (uctntPrice.Visible = True) And (uctntPrice.Enabled = True) Then
        uctntPrice.SetFocus
    End If

    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("cmdPrevAction_Click", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub cmdPrevTranType_Click()
    
    On Error GoTo HandleException
    
    If (cmdPrevTranType.Visible = True) And (cmdPrevTranType.Enabled = True) Then cmdPrevTranType.SetFocus
    mlngTranTypeRowNo = mlngTranTypeRowNo - 1
    Call ShowTranTypeRow

    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("cmdPrevTranType_Click", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub cmdNextTenderType_Click()
    
    On Error GoTo HandleException

    If (cmdNextTenderType.Visible = True) And (cmdNextTenderType.Enabled = True) Then cmdNextTenderType.SetFocus
    mlngTenderTypeRowNo = mlngTenderTypeRowNo + 1
    Call ShowTenderTypeRow
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("cmdNextTenderType_Click", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub cmdPrevTenderType_Click()
    
    On Error GoTo HandleException
    
    If (cmdPrevTenderType.Visible = True) And (cmdPrevTenderType.Enabled = True) Then cmdPrevTenderType.SetFocus
    mlngTenderTypeRowNo = mlngTenderTypeRowNo - 1
    Call ShowTenderTypeRow

    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("cmdPrevTenderType_Click", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub cmdProcess_Click()

    On Error GoTo HandleException
    
    If (cmdProcess.Visible = True) And (cmdProcess.Enabled = True) Then cmdProcess.SetFocus
    If txtSKU.Visible Then
        Call txtSKU_KeyPress(vbKeyReturn)
    Else
        Call uctntPrice_KeyPress(vbKeyReturn)
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("cmdProcess_Click", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub SwitchToQuantity()

    If fraReasonsList.Visible = False Then
        Call SetEntryMode(enemQty)
    End If

End Sub

Private Sub SetEntryMode(enemNewMode As enEntryMode)

Dim dblArea As Double
    
    On Error GoTo HandleException
    
    lblType.Visible = True
    txtSKU.Visible = True
    fraEntry.Visible = True
    fraActions.Visible = False
    uctntPrice.Visible = False
    Call EnableAction(enacEnquiry, False)
    
    Select Case (enemNewMode)
        Case (enemQty):
            mlngEntryMode = enemQty
            lblType.Font.Size = 24
            lblType.Caption = "Quantity"
            txtSKU.Text = "1"
            If (mblnEditingLine = True) Then txtSKU.Text = mdblPreviousQty
            sbStatus.Panels(PANEL_INFO).Text = "Enter Quantity"
        Case (enemSKU):
            mblnStopScanning = False
            mlngEntryMode = enemSKU
            cmdProcess.Visible = True
            cmdViewLines.Visible = True
            fraActions.Visible = True
            Call EnableAction(enacEnquiry, True)
            mblnBarcoded = False
            lblType.Font.Size = 24
            lblType.Caption = "SKU"
            If Abs(mdblLineQuantity) > 1 Then
                fraQty.Visible = True
                lblQty.Caption = "x " & CStr(mdblLineQuantity)
            Else
                fraQty.Visible = False
            End If
            If (mdblLineQuantity < 0) Then
                lblType.Font.Size = 14
                lblType.Caption = "Refund" & vbNewLine & "SKU"
            End If
            txtSKU.Text = vbNullString
            fraMaxValue.Visible = False
            sbStatus.Panels(PANEL_INFO).Text = "Enter SKU"
        Case (enemPrice):
            Call EnableAction(enacTender, True)
            Call EnableAction(enacQuantity, True)
            Call EnableAction(enacOverride, True)
            cmdProcess.Visible = True
            cmdViewLines.Enabled = True
            'Call EnableAction(enacLookUp, True)
            Call EnableAction(enacVoid, True)
            cmdProcess.Visible = True
            cmdViewLines.Visible = True
            mlngEntryMode = enemPrice
            lblType.Font.Size = 24
            lblType.Caption = "Price"
            txtSKU.Text = vbNullString
            txtSKU.Visible = False
            uctntPrice.Visible = True
            If mlngCurrentMode = encmOverrideCode Then
                sprdLines.Row = sprdLines.MaxRows
                sprdLines.Col = COL_SELLPRICE
                uctntPrice.Value = sprdLines.Value
                If (mblnUseWERates = True) Then
                    sprdLines.Col = COL_WEEE_RATE
                    uctntPrice.Value = uctntPrice.Value - sprdLines.Value
                End If
            ElseIf (mblnIsRefundLine = True) Then
                sprdLines.Row = sprdLines.MaxRows
                sprdLines.Col = COL_LOOKEDUP
                sprdLines.Col = COL_SELLPRICE
                uctntPrice.Value = Val(sprdLines.Value)
            End If
            If (uctntPrice.Visible = True) And (uctntPrice.Enabled = True) Then Call uctntPrice.SetFocus
'            txtSKU.Text = Format$(Val(sprdTotal.Text), "0.00")
'            If (mblnEditingLine = True) Then txtSKU.Text = Format$(Val(mdblPreviousPrice), "0.00")
            sbStatus.Panels(PANEL_INFO).Text = "Enter Price"
       
        Case (enemCostPrice):
            Call EnableAction(enacTender, True)
            Call EnableAction(enacQuantity, False)
            Call EnableAction(enacOverride, True)
            cmdViewLines.Enabled = True
            'Call EnableAction(enacLookUp, True)
            Call EnableAction(enacVoid, True)
            cmdProcess.Visible = True
            cmdViewLines.Visible = True
            mlngEntryMode = enemCostPrice
            lblType.Font.Size = 24
            lblType.Caption = "Cost Price"
            txtSKU.Text = vbNullString
'            txtSKU.Text = Format$(Val(sprdTotal.Text), "0.00")
'            If (mblnEditingLine = True) Then txtSKU.Text = Format$(Val(mdblPreviousPrice), "0.00")
            txtSKU.Visible = False
            uctntPrice.Visible = True
            uctntPrice.Value = 0
            If (uctntPrice.Visible = True) And (uctntPrice.Enabled = True) Then Call uctntPrice.SetFocus
            sbStatus.Panels(PANEL_INFO).Text = "Enter Cost Price"
        Case (enemValue):
            mlngEntryMode = enemValue
            lblType.Font.Size = 24
            lblType.Caption = "Value"
            sprdTotal.Col = COL_TOTVAL
            txtSKU.Text = vbNullString
            txtSKU.Visible = False
            uctntPrice.Visible = True
            uctntPrice.Value = 0
            If (uctntPrice.Visible = True) And (uctntPrice.Enabled = True) Then Call uctntPrice.SetFocus
            fraMaxValue.Visible = False
            sbStatus.Panels(PANEL_INFO).Text = "Enter Value"
        Case (enemNone):
            fraQty.Visible = False
            mlngEntryMode = enemNone
            fraEntry.Visible = False
            fraMaxValue.Visible = False
            sbStatus.Panels(PANEL_INFO).Text = vbNullString
            fraMaxValue.Visible = False
            cmdCurrency.Visible = False
    End Select
    'DoEvents
    If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then
        txtSKU.SetFocus
        txtSKU.SelStart = 0
        txtSKU.SelLength = Len(txtSKU.Text)
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("SetEntryMode", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
        
End Sub

Private Sub ReverseLine()

Dim lngRowNo    As Long
    
    If (Left(lblOrderNo.Caption, 1) = "Q") Then
        lblOrderNo.Caption = ""
        Call ClearEvents
        mdblTotalBeforeDis = 0
    End If
    
    lngRowNo = LastUnvoidedLine
    
    If lngRowNo <> 0 Then
        fraActions.Visible = False
        mlngCurrentMode = encmReversal
        Call SetEntryMode(enemNone)
        If (sprdLines.Visible = True) And (sprdLines.Enabled = True) Then sprdLines.SetFocus
        fraVoidLine.Visible = True
        Call StretchScrollBars(True)
        Call DisplayVoidLine
    Else
        Call SetEntryMode(enemNone)
        Call MsgBoxEx("No lines found to Reverse", vbOKOnly, "Reverse Line Item", , , , , _
                      RGBMSGBox_PromptColour)
        Call SetEntryMode(enemSKU)
    End If

End Sub

Private Sub DisplayVoidLine()

    sprdLines.Col = COL_PARTCODE
    lblVoidSKU.Caption = sprdLines.Text
    sprdLines.Col = COL_BARCODE
    lblVoidSKU.Tag = sprdLines.Text
    sprdLines.Col = COL_DESC
    lblVoidDesc.Caption = sprdLines.Text

End Sub

Private Sub cmdScannerEnable_Click()
    
    On Error GoTo HandleException
    
    comBarScanner.Output = Chr(6)

    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("cmdScannerEnable_Click", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub cmdSleep_Click()
    
    On Error GoTo HandleException
    
    comBarScanner.Output = Chr(7)
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("cmdSleep_Click", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub cmdTranType_Click(index As Integer)

Dim strTranType   As String
Dim lngTypePos    As Long
Dim colReturnCust As Collection
Dim blnKeyPad     As Boolean

'Parked Transaction Price Promise details
Dim oPPCust       As cPriceCusInfo
Dim oPPLineBO     As cPriceLineInfo
Dim oNewPPLineBO  As cPriceLineInfo
Dim colPPLines    As Collection
    
    On Error GoTo HandleException
    
    Set mcolRefundTrans = Nothing
    mblnCreatingOrder = False
    mblnOrderBtnPressed = False
    mblnQuoteRecalledOrderCreated = False
    mblnRecalledQuote = False
    mblnSaveNewOrder = False
    Call DebugMsg(MODULE_NAME, "cmdTranType_Click", endlTraceIn, "Pressed:" & index)
    Call DebugMsg(MODULE_NAME, "cmdTranType_Click", endlTraceIn, "Pressed:" & cmdTranType(index).Caption)
    If (index = COMMAND_BUTTON_SKU_ENQUIRY) Then
        fraTranType.Visible = False
        Call frmItemFilter.ItemEnquiry(etFullItems, True)
        Exit Sub
    ElseIf (index = COMMAND_BUTTON_SKU_ENQUIRY_PIM) Then
        fraTranType.Visible = False
        Call frmItemFilter.ItemEnquiry(etPIMOnly, True)
        Exit Sub
    End If
    
    If mlngTranTypeRowNo > 0 And index <= TRAN_BTN_PER_ROW Then
        index = index + (mlngTranTypeRowNo * TRAN_BTN_PER_ROW)
    End If
    
    'catch all keys pressed that exceed displayed list
    If cmdTranType(index).Visible = False Then Exit Sub
    
    If (cmdTranType(index).Visible = True) And (cmdTranType(index).Enabled = True) Then cmdTranType(index).SetFocus
    
    If Not NextTransactionOK Then
        Load frmVerifyPwd
        Do
            Call MsgBoxEx("Duplicate transaction detected" & vbNewLine & vbNewLine & _
                          "Contact CTS Support Desk", vbOKOnly + vbExclamation, _
                          "Duplicate Transaction", , , , , RGBMsgBox_WarnColour)
        Loop While frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) = False
        Unload frmVerifyPwd
        Unload Me
        End
    End If
   
    mstrTranSuper = vbNullString
    Call DebugMsg(MODULE_NAME, "cmdTranType_Click", endlDebug, "Switch to Tran Type")
    Set moConResHeaderBO = goDatabase.CreateBusinessObject(CLASSID_CONRESHEADER)
    Set moTOSType = mcolTOSOpt(CLng(cmdTranType(index).Tag))
    mdteEventEndDate = DateAdd("y", 100, Date) 'force when events will expire to 100 years ahead
'    If moTOSType.SupervisorRequired = True And _
'       moUser.Supervisor = False And _
'       moUser.Manager = False Then
    If moTOSType.SupervisorRequired Then
        Load frmVerifyPwd
        If frmVerifyPwd.VerifySupervisorPassword(ucKeyPad1.Visible, "Transaction Authorisation") = False Then
            Exit Sub
        End If
        mstrTranSuper = frmVerifyPwd.txtAuthID
        Unload frmVerifyPwd
    End If
    
    mlngTranTypeRowNo = 0
    Call ShowTranTypeRow
    
    mblnEditingLine = False 'reset whether currently editing existing line
    mblnIsRefundLine = False
    
    Set mcolPromptRejects = New Collection  'WIX1218 - list of Rejected Item Prompts
    Set mcolGetCoupons = New Collection     'WIX1165 - list of coupons received as reward
    Set mcolUsedCoupons = New Collection    ' P014-01 - List of coupons used up in a transaction
    
    mblnNonValidated = False
    mblnRefundOverride = False
    mblnAllowAllTenders = False
    mblnTokensOnly = False
    mblnGiftTokenUsedInTender = False
    mblnNonTokenUsedInTender = False
    mblnParkingTran = False
    mblnRecallingParked = False
    mblnNeedDrawerOpen = False
    mblnSaveNewOrder = False
    mblnShowAddress = True

    mstrName = vbNullString
    mstrPostCode = vbNullString
    mstrAddress = vbNullString
    mstrTelNo = vbNullString
    mstrMobileNo = vbNullString
    mstrEmail = vbNullString
    mstrWorkTelNo = vbNullString
    mstrRemarks = vbNullString
    mstrTeleOrderNum = vbNullString
    
    mstrAccountNum = vbNullString
    mstrAccountName = vbNullString
    
    mstrRefundCashier = vbNullString
    
    Set mcolEFTPayments = New Collection

    mlngHighestAgePassed = 0
    mlngLowestAgeFailed = 0

    mstrDiscCardNo = vbNullString
    mstrLocation = vbNullString
    Set moDiscountCardScheme = Nothing
    
    'reset transaction header displays
    lblTranNo.Caption = "TBA"
    lblTraderNo.Caption = "00000000"
    lblTraderName.Caption = vbNullString
    lblOrderNo.Caption = vbNullString
    lblOrderDate.Caption = vbNullString
    mdblDepositValue = 0
    mstrRefundTrans = ""
    mstrLocation = ""

    Set mcolOPSummary = New Collection
    Set mcolHSSummary = New Collection
    
    Set moOrigTend = Nothing
    Set moOrigTendComm = Nothing    ' Commidea Into WSS
    
    mstrEFTDebugInfo = vbNullString
    
    mblnCustomerOrder = False
    mblnFirstPmtDone = False
    mblnExchangeRefunds = False
    mblnPrintVAT = False
    mblnApplyCollDisc = False
    mblnOrderBtnPressed = False

    mblnColleagueDiscount = False
    mblnDiscountColleague = False
    mblnRecalledTran = False
    
    mdblTotalBeforeDis = 0
    
    '** Reset the Quote/Sales Orders Transaction details - so they are flagged as processed **
    mstrQuoteTillID = ""
    mstrQuoteTranNo = ""
    
    Set mcolLinePrints = New Collection
    Set mcolTrailerPrints = New Collection
    Set mcolValues = New Collection
    
    Set moRefundOrigTran = Nothing
    
    Set moTOSType = mcolTOSOpt(CLng(cmdTranType(index).Tag))
    Set moTenderOpts = Nothing
    strTranType = moTOSType.Code
    
    sprdLines.ColWidth(COL_BACKCOLLECT) = 0
    Call DebugMsg(MODULE_NAME, "cmdTranType_Click", endlDebug, "Enabling Actions")
    'Configure options buttons on screen for access
    Call EnableAction(enacLookUp, False)
    Call EnableAction(enacOrder, False)
    Call EnableAction(enacBackdoorItems, False)
    Call EnableAction(enacCreateOrder, False)
    Call EnableAction(enacRecallQuote, False)
    Call EnableAction(enacSaveQuote, False)
    
    If moVATRates Is Nothing Then
        Set moVATRates = goDatabase.CreateBusinessObject(CLASSID_VATRATES)
        Call moVATRates.Load
    End If
    
    If Not moEvents Is Nothing Then
        Call moEvents.ClearSKUS
    End If

    sprdTotal.Row = 1
    sprdTotal.Col = COL_TOTVAL - 1
    sprdTotal.Text = "Balance"
    
    Set moTranHeader = Nothing
    
    'Process selected option
    Select Case strTranType
        Case TT_SALE:
                        sprdLines.ColWidth(COL_BACKCOLLECT) = 4.75
                        Call EnableAction(enacBackdoorItems, True)
                        mlngTranSign = 1
                        Call EnableAction(enacCreateOrder, Not DisableTrainingModeOrders)
                        Call EnableAction(enacRecallQuote, Not DisableTrainingModeOrders)
                        Call EnableAction(enacSaveQuote, True)
                        Call SetCurrentMode(encmRecord)
                        Call SetEntryMode(enemSKU)
        Case TT_REFUND:
'                        'set up screen to retrieve original transaction to refund
                        mlngTranSign = -1
                        Call SetCurrentMode(encmRecord) 'force to configure screen only
                        Call SetEntryMode(enemNone)
                        Call SetCurrentMode(encmRetrieveTran)
'                        Call SetCurrentMode(encmTranType)
'                        Exit Sub
        Case TT_LOGO:
                        Call RePrintLogo(OPOSPrinter1)
                        Exit Sub
        Case TT_SIGNON: 'This is a place holder
        Case TT_SIGNOFF:
                        Call SignOff
                        Exit Sub
                        
        Case TT_COLLECT:
'                        'set up screen to retrieve original transaction to refund
                        Call SetCurrentMode(encmRecord) 'force to configure screen only
                        Call SetEntryMode(enemNone)
        Case TT_OPENDRAWER:
                        Call SetCurrentMode(encmOpenDrawer)
                        sprdLines.MaxRows = sprdLines.MaxRows + 1
                        sprdLines.Row = sprdLines.MaxRows
                        sprdLines.Col = 0
                        sprdLines.Text = " " 'override auto column lettering
                        sprdLines.Col = COL_DESC
                        sprdLines.Text = moTOSType.Description
                        sprdLines.Col = COL_LINETYPE
                        sprdLines.Text = LT_ITEM
                        Call LoadReasonCodes(enrmOpenDrawer)
        
                        Call SetGridTotals(sprdLines.MaxRows)
        Case TT_XREAD:
                        Call XZRead(False)
                        Exit Sub
        Case TT_ZREAD:
                        Call XZRead(True)
                        Exit Sub
        Case TT_MISCIN, TT_CORRIN:
                        mlngTranSign = IIf(strTranType = TT_MISCIN, 1, -1)
                        Call SetCurrentMode(encmPaidIn)
                        sprdLines.MaxRows = sprdLines.MaxRows + 1
                        sprdLines.Row = sprdLines.MaxRows
                        sprdLines.Col = 0
                        sprdLines.Text = " " 'override auto column lettering
                        sprdLines.Col = COL_DESC
                        sprdLines.Text = moTOSType.Description
                        sprdLines.Col = COL_LINETYPE
                        sprdLines.Text = LT_ITEM
                        Call LoadReasonCodes(enrmPaidIn)
                        
                        Call SetGridTotals(sprdLines.MaxRows)
        Case TT_MISCOUT, TT_CORROUT:
                        mlngTranSign = IIf(strTranType = TT_MISCOUT, -1, 1)
                        Call SetCurrentMode(encmPaidOut)
                        sprdLines.MaxRows = sprdLines.MaxRows + 1
                        sprdLines.Row = sprdLines.MaxRows
                        sprdLines.Col = 0
                        sprdLines.Text = " " 'override auto column lettering
                        sprdLines.Col = COL_DESC
                        sprdLines.Text = moTOSType.Description
                        sprdLines.Col = COL_LINETYPE
                        sprdLines.Text = LT_ITEM
                        Call LoadReasonCodes(enrmPaidOut)

                        Call SetGridTotals(sprdLines.MaxRows)
        Case TT_RECALL:
                        mblnRecallingParked = True
                        mlngTranSign = 1
                        Call SetCurrentMode(encmRecord)
                        mlngCurrentMode = encmTranType
                        Set moTranHeader = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
                        ReDim mavReceipts(0) As Variant  ' Commidea Into WSS, record all receipt files from Commidea for the tran
                        mblnTransactionHasWebOrderNoRefund = False  ' Hubs 2.0 - Referral 771 - Reset flag used to identify whether tran involves a web order no refund as cannot park these
                        Call TransactionLookUp
                        mlngCurrentMode = encmRecord
                        Call EnableAction(enacBackdoorItems, True)
                        sprdLines.ColWidth(COL_BACKCOLLECT) = 4.75

                        ' Change to "Sale" type
                        Set moTOSType = mcolTOSOpt(CLng(cmdTranType(2).Tag))
                        strTranType = moTOSType.Code
                        
                        Call SetEntryMode(enemSKU)
                        If AreThereAnyRefundLines() = False Then
                            Call EnableAction(enacCreateOrder, Not DisableTrainingModeOrders)
                            Call EnableAction(enacRecallQuote, Not DisableTrainingModeOrders)
                            Call EnableAction(enacSaveQuote, True)
                        End If
                        mblnRecallingParked = False
                        
        Case TT_QUOTE:
        Case TT_EXT_SALE:
        Case TT_TELESALES:
                        sprdLines.ColWidth(COL_BACKCOLLECT) = 4.75
                        Call SetCurrentMode(encmRecord)
                        Call SetEntryMode(enemSKU)
        Case TT_TELEREFUND:
                        Load frmEntry
                        If (ucKeyPad1.Visible = True) Then frmEntry.ShowNumPad
                        blnKeyPad = ucKeyPad1.Visible
                        ucKeyPad1.Visible = False
                        mstrTeleOrderNum = frmEntry.GetEntry("Please enter a Telesales Order Number", _
                                "Enter Number", RGBMSGBox_PromptColour, 7, False)
                        Unload frmEntry
                        ucKeyPad1.Visible = blnKeyPad
                        Call SetCurrentMode(encmRecord) 'force to configure screen only
                        Call SetEntryMode(enemNone)
                        Call SetCurrentMode(encmRetrieveTran)
        
        Case TT_REPRINTREC:
                        Call frmReprintReceipt.RetrieveTranDetails
        Case TT_EAN_CHECK:
                    Set frmEANCheck = New frmEANCheck
                    Load frmEANCheck
                    frmEANCheck.UserID = txtUserID.Text
                    mlngCurrentMode = encmEANCheck
            
                    'Added 29/10/07 - create Tran Header to save Lines against if OffLine only, into HST File
                    If (moTranHeader Is Nothing) And (goSession.ISession_IsOnline = False) Then
                        Set moTranHeader = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
                        moTranHeader.TillID = mstrTillNo
                        moTranHeader.CashierID = txtUserID.Text
                        moTranHeader.TransactionCode = moTOSType.Code
                        moTranHeader.TrainingMode = mblnTraining
                    End If

                    Call frmEANCheck.Show(vbModal)
                    Unload frmEANCheck
                    mlngCurrentMode = encmTranType
                    If ((moTranHeader Is Nothing) = False) And (goSession.ISession_IsOnline = False) Then
                        Call moTranHeader.CreateOfflineFile
                    End If
                    Set moTranHeader = Nothing
                    Exit Sub
    End Select
    
    If (Not mblnRecallingParked) Or (sprdLines.MaxRows < 2) Then
        Call EnableAction(enacVoid, True)
        Call EnableAction(enacPark, False)
        Call EnableAction(enacReversal, False)
        Call EnableAction(enacOverride, False)
    End If
    cmdViewLines.Enabled = False
    
    If moEvents Is Nothing Then
        Set moEvents = New cTillEvents_Wickes.cOPEvents
        Call moEvents.Initialise(goSession)
    End If
    
    sprdTotal.Row = 3
    sprdTotal.Col = COL_TOTTYPE
    sprdTotal.Text = moTOSType.Description
    
    sprdTotal.Row = 1
    sprdTotal.Col = COL_TOTTYPE - 1
    If (mblnTraining = True) Then
        sprdTotal.Text = "*** TRAINING ***"
        Call EnableAction(enacGiftVoucher, False)
    Else
        sprdTotal.Text = "TYPE"
        Call EnableAction(enacGiftVoucher, True)
    End If
    
    'Create new Transaction Header in database and display Transaction ID
    If (moTranHeader Is Nothing) Then
        Set moTranHeader = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
        ReDim mavReceipts(0) As Variant  ' Commidea Into WSS, record all receipt files from Commidea for the tran
        mblnTransactionHasWebOrderNoRefund = False  ' Hubs 2.0 - Referral 771 - Reset flag used to identify whether tran involves a web order no refund as cannot park these
    End If
    moTranHeader.TillID = mstrTillNo
    moTranHeader.CashierID = txtUserID.Text
    moTranHeader.TransactionCode = moTOSType.Code
    moTranHeader.TrainingMode = mblnTraining
    moTranHeader.RefundCashier = mstrRefundCashier
    moTranHeader.CollectPostCode = mblnCapturePostCode
    
    If LenB(mstrTranSuper) <> 0 Then
        moTranHeader.SupervisorUsed = True
        moTranHeader.SupervisorNo = mstrTranSuper
    End If
    
    moTranHeader.Voided = True
    Call SetUpSaleVATRates(moTranHeader)
    'added 16/9/05 - ensure Saved OK else report error and exit - Error Type is shown in DebugView
    If (moTranHeader.IBo_SaveIfNew = False) Then
        Call Err.Clear
        Call MsgBoxEx("ERROR : A critical error has occurred when completing Transaction to Database." & _
            "System is unable to proceed and will exit." & vbCrLf & vbCrLf & "Desc :" & moTranHeader.SaveFailedStatus & _
            vbCrLf & "Contact IT Support immediattely", vbExclamation, "System Failure", , , , , RGBMsgBox_WarnColour)
        On Error Resume Next
        Call Err.Raise(1, "cmdTranType_Click", "Unable to save to Database")
        Call Err.Report(MODULE_NAME, "cmdTranType_Click", 0, False)
        End
    End If
    
    On Error Resume Next
    Call goDatabase.ExecuteCommand("UPDATE DLTOTS SET RTIHOLD=1 WHERE DATE1='" & Format(Date, "YYYY-MM-DD") & "' AND TILL='" & moTranHeader.TillID & "' AND ""Tran""='" & moTranHeader.TransactionNo & "'")
    Call Err.Clear
    On Error GoTo 0
        
    If ((moParkTranHdr Is Nothing) = False) Then
        'Copy Price Promise details across
        Set oPPCust = moTranHeader.AddPricePromiseCust(moParkTranHdr.GetPricePromiseCust.Name)
        oPPCust.Address1 = moParkTranHdr.GetPricePromiseCust.Address1
        oPPCust.Address2 = moParkTranHdr.GetPricePromiseCust.Address2
        oPPCust.Address3 = moParkTranHdr.GetPricePromiseCust.Address3
        oPPCust.PostCode = moParkTranHdr.GetPricePromiseCust.PostCode
        oPPCust.Phone = moParkTranHdr.GetPricePromiseCust.Phone
        'Copy Price Promise Lines details across
        Set colPPLines = moParkTranHdr.GetPricePromiseLine
        For Each oPPLineBO In colPPLines
            Set oNewPPLineBO = moTranHeader.AddPricePromiseLine(oPPLineBO.SequenceNo)
            oNewPPLineBO.CompetitorName = oPPLineBO.CompetitorName
            oNewPPLineBO.CompetitorAddress1 = oPPLineBO.CompetitorAddress1
            oNewPPLineBO.CompetitorAddress2 = oPPLineBO.CompetitorAddress2
            oNewPPLineBO.CompetitorAddress3 = oPPLineBO.CompetitorAddress3
            oNewPPLineBO.CompetitorPostcode = oPPLineBO.CompetitorPostcode
            oNewPPLineBO.CompetitorPhone = oPPLineBO.CompetitorPhone
            oNewPPLineBO.EnteredPrice = oPPLineBO.EnteredPrice
            oNewPPLineBO.IncludeVAT = oPPLineBO.IncludeVAT
            
            oNewPPLineBO.OrigStoreNumber = oPPLineBO.OrigStoreNumber
            oNewPPLineBO.OrigTransDate = oPPLineBO.OrigTransDate
            oNewPPLineBO.OrigTillNumber = oPPLineBO.OrigTillNumber
            oNewPPLineBO.OrigTransactionNumber = oPPLineBO.OrigTransactionNumber
            oNewPPLineBO.OrigSellingPrice = oPPLineBO.OrigSellingPrice
        Next
    End If
    
    Set moParkTranHdr = Nothing
    mstrTranId = moTranHeader.TransactionNo
    lblTranNo.Caption = mstrTranId
    
    'Reset transaction totals
    mdblTranTotal = 0
    mdblPaidTotal = 0
    If (txtSKU.Enabled = True) And (txtSKU.Visible = True) Then Call txtSKU.SetFocus

    ' Capture Customer PostCode Information
    If strTranType = TT_SALE Then
        If GetPostCode Then
            Set moReturnCust = goDatabase.CreateBusinessObject(CLASSID_RETURNCUST)
            With moReturnCust
                Call .AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionDate, _
                                    moTranHeader.TranDate)
                Call .AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_PCTillID, mstrTillNo)
                Call .AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionNumber, _
                                    moTranHeader.TransactionNo)
                Call .AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_LineNumber, 0)
                Set colReturnCust = .LoadMatches
                If .LoadMatches.Count = 0 Then
                    .TransactionDate = moTranHeader.TranDate
                    .PCTillID = mstrTillNo
                    .TransactionNumber = moTranHeader.TransactionNo
                    .SaveIfNew
                    Set colReturnCust = .LoadMatches
                End If
            End With
            Set moReturnCust = colReturnCust.Item(1)
            With moReturnCust
                If (LenB(Trim$(.PostCode)) = 0) And (mstrPostCode <> vbNullString) Then
                    .PostCode = mstrPostCode
                    .SaveIfExists
                End If
                If (LenB(Trim$(.AddressLine1)) = 0) And (mstrAddressLine1 <> vbNullString) Then
                    .AddressLine1 = mstrAddressLine1
                    .SaveIfExists
                End If
            End With
            moTranHeader.CollectPostCode = True
        Else
            Set moReturnCust = Nothing
        End If
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("cmdTranType_Click", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub SetUpSaleVATRates(oTranHeader As cPOSHeader)

Dim lngVATNo As Long

    For lngVATNo = 1 To 9 Step 1
        If lngVATNo <= moVATRates.VATRateCount Then
                oTranHeader.VATRates(lngVATNo) = moVATRates.VATRate(lngVATNo)
        End If
        oTranHeader.VATSymbol(lngVATNo) = Chr$(lngVATNo + 96)
    Next lngVATNo

End Sub

Private Sub SetGridTotals(ByVal lngRowNo As Long)
            
    sprdLines.Row = lngRowNo
    sprdLines.Col = COL_QTY
    'Force quantity to at least 1 unit if blank
    If Val(sprdLines.Value) = 0 Then
        sprdLines.Col = COL_PARTCODE
        If LenB(Trim$(sprdLines.Text)) <> 0 Then
            sprdLines.Col = COL_LINETYPE
            If (Val(sprdLines.Text) <> LT_COUPON) Then
                sprdLines.Col = COL_QTY
                sprdLines.Text = 1
            End If
        End If
    End If
    sprdLines.Row = ROW_TOTALS
    sprdLines.Col = COL_QTY
    sprdLines.Formula = "SUM(" & ColToText(COL_QTY) & "2:" & ColToText(COL_QTY) & sprdLines.MaxRows & ")"
    sprdLines.Col = COL_INCTOTAL
    sprdLines.Formula = "SUM(" & ColToText(COL_INCTOTAL) & "2:" & ColToText(COL_INCTOTAL) & sprdLines.MaxRows & ")"
    sprdLines.Col = COL_ORDERVALUE
    sprdLines.Formula = "SUM(" & ColToText(COL_ORDERVALUE) & "2:" & ColToText(COL_ORDERVALUE) & sprdLines.MaxRows & ")"
    sprdLines.Col = COL_VATTOTAL
    sprdLines.Formula = "SUM(" & ColToText(COL_VATTOTAL) & "2:" & ColToText(COL_VATTOTAL) & sprdLines.MaxRows & ")"
    sprdLines.Col = COL_TOTALVOL
    sprdLines.Formula = "SUM(" & ColToText(COL_TOTALVOL) & "2:" & ColToText(COL_TOTALVOL) & sprdLines.MaxRows & ")"
    sprdLines.Col = COL_GRP_PROMPT_QTY
    sprdLines.Formula = "SUM(" & ColToText(COL_GRP_PROMPT_QTY) & "2:" & ColToText(COL_GRP_PROMPT_QTY) & sprdLines.MaxRows & ")"
    sprdLines.Row = lngRowNo

End Sub

Private Sub SetCurrentMode(encmNewMode As enCurrentMode)

Dim lngSMan     As Long
   
    On Error GoTo HandleException
    
    Call DebugMsg(MODULE_NAME, "SetCurrentMode", endlTraceIn, "Setting to:" & encmNewMode)
    
    Select Case (encmNewMode)
        Case (encmRecord):
            Call EnableAction(enacTender, True)
            Call EnableAction(enacOrder, Not mblnCustomerOrder Or Not DisableTrainingModeOrders)
            Call EnableAction(enacOverride, True)
            cmdViewLines.Enabled = True
            'Call EnableAction(enacLookUp, True)
            fraTrader.Visible = True
            sprdLines.Visible = True
            sprdTotal.Visible = True
            
            If (goSession.ISession_IsOnline) Then
                Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
            End If
            
            fraTranType.Visible = False
            fraLogon.Visible = False
            
            UpdateBrandingImageVisibility (False)
            
            fraActions.Visible = True
            sprdLines.MaxRows = 1
            sprdLines.Row = 1
            sprdLines.RowHidden = True
            Call SetGridTotals(sprdLines.MaxRows)
            Call UpdateTotals
            mdblLineQuantity = 1
            mstrRefundReason = vbNullString
            mlngCurrentMode = encmNewMode
        Case (encmPaidOut):
            fraTrader.Visible = True
            sprdLines.Visible = True
            sprdTotal.Visible = True
            
            If (goSession.ISession_IsOnline) Then
                Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
            End If
            
            fraTranType.Visible = False
            fraLogon.Visible = False
            
            UpdateBrandingImageVisibility (False)
            
            sprdLines.MaxRows = 1
            sprdLines.Row = 1
            sprdLines.RowHidden = True
            Call SetGridTotals(sprdLines.MaxRows)
            Call UpdateTotals
            mlngCurrentMode = encmPaidOut
        Case (encmPaidIn):
            fraTrader.Visible = True
            sprdLines.Visible = True
            sprdTotal.Visible = True
            
            If (goSession.ISession_IsOnline) Then
                Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
            End If
            
            fraTranType.Visible = False
            fraLogon.Visible = False
            
            UpdateBrandingImageVisibility (False)
            
            sprdLines.MaxRows = 1
            sprdLines.Row = 1
            sprdLines.RowHidden = True
            Call SetGridTotals(sprdLines.MaxRows)
            Call UpdateTotals
            mlngCurrentMode = encmPaidIn
        Case (encmOpenDrawer):
            fraTrader.Visible = True
            sprdLines.Visible = True
            sprdTotal.Visible = True
            
            If (goSession.ISession_IsOnline) Then
                Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
            End If
            
            fraTranType.Visible = False
            fraLogon.Visible = False
            
            UpdateBrandingImageVisibility (False)
            
            sprdLines.MaxRows = 1
            sprdLines.Row = 1
            sprdLines.RowHidden = True
            Call SetGridTotals(sprdLines.MaxRows)
            Call UpdateTotals
            mlngCurrentMode = encmOpenDrawer
        Case (encmTranType):
            Dim Y As Integer
            For Y = 0 To Forms.Count - 1 Step 1
                Call DebugMsg(MODULE_NAME, "SetCurrentMode:TranType", endlDebug, "Form" & Y & ":" & Forms(Y).Name)
            Next Y
            mblnRefundLineMode = False
            lblOrderNo.Caption = vbNullString
            lblTraderNo.Caption = "00000000"
            lblTraderName.Caption = vbNullString
            lblOrderDate.Caption = vbNullString
            mblnSOCustDone = False
            mblnCustDone = False
            sprdTotal.Visible = False
            
            If (goSession.ISession_IsOnline) Then
                Me.BackColor = goSession.GetParameter(PRM_BRANDING_IMAGE_BACKCOLOUR)
            End If
            
            fraTrader.Visible = False
            fraActions.Visible = False
            fraLogon.Visible = True
            
            UpdateBrandingImageVisibility (True)
            
            sprdLines.Visible = False
            fraTranType.Visible = True
            mlngCurrentMode = encmTranType
            Call SetEntryMode(enemNone)
        Case (encmRetrieveTran)
            mblnRefundLineMode = True 'force system to select next line
            Call RetrieveSaleLine
        Case (encmTenderOverride)
            fraActions.Visible = False
            Call SetEntryMode(enemNone)
            sbStatus.Panels(PANEL_INFO).Text = "Select tender override reason"
            mlngCurrentMode = encmTenderOverride

    End Select
    Call DebugMsg(MODULE_NAME, "SetCurrentMode", endlTraceOut)

    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("SetCurrentMode", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub cmdViewLines_Click()
    
    On Error GoTo HandleException
    
    If cmdViewLines.Caption = "R&eturn" Or ActiveControl.Name = "sprdLines" Then
        cmdViewLines.Caption = "Vi&ew Lines"
        If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then
            txtSKU.SetFocus
        ElseIf (uctntPrice.Visible = True) And (uctntPrice.Enabled = True) Then
            uctntPrice.SetFocus
        End If
    Else
        cmdViewLines.Caption = "R&eturn"
        If (sprdLines.Visible = True) And (sprdLines.Enabled = True) Then sprdLines.SetFocus
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("cmdViewLines_Click", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub cmdVoidLineEsc_Click()
    
    Call Form_KeyPress(vbKeyEscape)

End Sub

Private Sub cmdVoidLineOK_Click()

    Call Form_KeyPress(vbKeyReturn)

End Sub

Private Sub comBarScanner_OnComm()
    
Static strInBuffer As String
Dim oWorkStationBO  As cWorkStation

    On Error Resume Next
    
    Call DebugMsg(MODULE_NAME, "comBarScanner_OnComm", endlDebug, "Code-" & comBarScanner.CommEvent & " Input=" & strInBuffer)
    Select Case comBarScanner.CommEvent
   ' Handle each event or error by placing
   ' code below each case statement

   ' Errors
        Case comEventBreak   ' A Break was received.
            Exit Sub
        Case comEventFrame   ' Framing Error
            Exit Sub
        Case comEventOverrun   ' Data Lost.
            Exit Sub
        Case comEventRxOver   ' Receive buffer overflow.
            Exit Sub
        Case comEventRxParity   ' Parity Error.
            Exit Sub
        Case comEventTxFull   ' Transmit buffer full.
            Exit Sub
        Case comEventDCB   ' Unexpected error retrieving DCB]
            Exit Sub
    
       ' Events
        Case comEvCD   ' Change in the CD line.
            Exit Sub
        Case comEvCTS   ' Change in the CTS line.
            Exit Sub
        Case comEvDSR   ' Change in the DSR line.
            Exit Sub
        Case comEvRing   ' Change in the Ring Indicator.
            Exit Sub
        Case comEvReceive   ' Received RThreshold # of
                            ' chars.
        Case comEvSend   ' There are SThreshold number of
                         ' characters in the transmit
                         ' buffer.
            Exit Sub
        Case comEvEOF   ' An EOF charater was found in
                         ' the input stream
            Exit Sub
    End Select
    
    strInBuffer = strInBuffer & comBarScanner.Input
    If InStr(1, strInBuffer, vbCr) = 0 Then
        Exit Sub
    End If
    
    strInBuffer = Replace(strInBuffer, vbCr, vbNullString)
    strInBuffer = Replace(strInBuffer, vbLf, vbNullString)
    
    If (mstrBCodeBrokenCode <> "") Then 'system thinks barcode reader broken, so re-activate
        Set oWorkStationBO = goDatabase.CreateBusinessObject(CLASSID_WORKSTATIONCONFIG)
        Call oWorkStationBO.RecordBarcodeBroken(goSession.CurrentEnterprise.IEnterprise_WorkstationID, False) 'working so update status on W/S record
        mstrBCodeBrokenCode = ""
        Set oWorkStationBO = Nothing
    End If
    
    If mlngCurrentMode = encmRetrieveTran Then
        If frmRetrieveTran.uctrRefund.AllowScan Then
            If Len(strInBuffer) = 12 Then
                frmRetrieveTran.uctrRefund.TransactionDate = CDate(Mid$(strInBuffer, 7, 2) & "/" & _
                                                                   Mid$(strInBuffer, 9, 2) & "/" & _
                                                                   Mid$(strInBuffer, 11, 2))
                frmRetrieveTran.uctrRefund.TillID = Left$(strInBuffer, 2)
                frmRetrieveTran.uctrRefund.TranID = Mid$(strInBuffer, 3, 4)
                frmRetrieveTran.uctrRefund.ShowDetails
            Else
                comBarScanner.PortOpen = False
                Call MsgBoxEx("Scanned barcode is not for a parked transaction", vbOKOnly + vbExclamation, "Retrieve Parked Transaction", , , , , RGBMsgBox_WarnColour)
                comBarScanner.PortOpen = True
            End If
        End If
    ElseIf mlngCurrentMode = encmRefund Then
        If fraReasonsList.Visible = False Then
            If frmRetrieveTran.uctrRefund.AllowScan Then
                If Len(strInBuffer) = 15 Then
                    ' Hubs 2.0 - Assume 15 chars ending in date is not a WebOrderNumber
                    ' and must therefore be a normal sale receipt
                    If IsDate(Mid$(strInBuffer, 10, 2) & "/" & _
                              Mid$(strInBuffer, 12, 2) & "/" & _
                              Mid$(strInBuffer, 14, 2)) Then
                        frmRetrieveTran.uctrRefund.storeNo = Left$(strInBuffer, 3)
                        frmRetrieveTran.uctrRefund.TillID = Mid$(strInBuffer, 4, 2)
                        frmRetrieveTran.uctrRefund.TranID = Mid$(strInBuffer, 6, 4)
                        frmRetrieveTran.uctrRefund.TransactionDate = CDate(Mid$(strInBuffer, 10, 2) & "/" & _
                                                                           Mid$(strInBuffer, 12, 2) & "/" & _
                                                                           Mid$(strInBuffer, 14, 2))
                    Else
                        ' Hubs 2.0
                        ' Anything else must be a Web Order number
                        frmRetrieveTran.uctrRefund.WebOrderNumber = strInBuffer
                    End If
                Else
                    ' Hubs 2.0
                    ' Anything else must be a Web Order number
                    frmRetrieveTran.uctrRefund.WebOrderNumber = strInBuffer
                End If
                frmRetrieveTran.uctrRefund.ShowDetails
            End If
        End If
    
    ElseIf mlngCurrentMode = encmColleagueDiscount Then
    ElseIf mlngCurrentMode = encmDiscountCardScheme Then    'MO'C Added 07/06/07
        If frmDiscountCardScheme.AllowScan Then
            frmDiscountCardScheme.CardNumber = strInBuffer
        End If
    ElseIf (mlngCurrentMode = encmEANCheck) Then
        frmEANCheck.BarcodedValue = strInBuffer
    ElseIf (frmItemFilter.ItemFilterVisible = True) Then
        frmItemFilter.ucItemFind.LookupSKU = strInBuffer
        
    ElseIf mlngEntryMode = enemSKU Then
        'If frmNotFound.Visible = False Then
        If mblnStopScanning = False Then 'MO'C 7/4/2008 - 2008 - 021
            mblnBarcoded = True
'            sprdLines.Col = COL_BARCODE
'            sprdLines.Text = strInBuffer
            Call DebugMsg(MODULE_NAME, "comBarScanner", endlDebug, "Processing Barcode-" & strInBuffer & "Count=" & mlngBCodeCheckCnt)
            txtSKU.Text = strInBuffer 'sprdLines.Text
            mlngBCodeCheckCnt = mlngBCodeCheckCnt - 1 'effectively reverse out SKU count added in processing SKU
            mstrBCodeBrokenCode = "" 'clear out if barcode was flagged as broken
            Call DebugMsg(MODULE_NAME, "comBarScanner", endlDebug, "Process Barcode-" & strInBuffer & "Count=" & mlngBCodeCheckCnt)
            Call txtSKU_KeyPress(vbKeyReturn)
        End If
    ElseIf mlngEntryMode = enemSKU Then
        'If frmNotFound.Visible = False Then
        If mblnStopScanning = False Then 'MO'C 7/4/2008 - 2008 - 021
            mblnBarcoded = True
'            sprdLines.Col = COL_BARCODE
'            sprdLines.Text = strInBuffer
            Call DebugMsg(MODULE_NAME, "comBarScanner", endlDebug, "Processing SKU Barcode-" & strInBuffer)
            txtSKU.Text = strInBuffer 'sprdLines.Text
            mlngBCodeCheckCnt = mlngBCodeCheckCnt - 1 'effectively reverse out SKU count added in processing SKU
            mstrBCodeBrokenCode = "" 'clear out if barcode was flagged as broken
            Call txtSKU_KeyPress(vbKeyReturn)
        End If
    ElseIf frmTokenScan.Visible Then
        frmTokenScan.ScanData = strInBuffer
    ElseIf frmCustomerOrder.Visible Then
        If frmCustomerOrder.AllowScan Then
            frmCustomerOrder.OrderNumber = strInBuffer
        End If
    End If
    
    
    strInBuffer = vbNullString
    
    Err.Clear
    
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

Dim lngKeyNo  As Long
Dim lngLineNo As Long
Dim blnCoupon As Boolean
Dim lngRevLineNo As Long
Dim curPayAmount As Currency
Dim strPreCode   As String
Dim strSKU  As String
Dim blnAmendFound As Boolean
Dim strMsg As String
Dim lngAnotherRevLineNo As Long ' Referral 669. 2 diff variables used to store rev line no in 2 diff situations.  Need one for all
Dim strProcedureName As String

   On Error GoTo HandleException
   
   strProcedureName = "Form_KeyPress"
    
    Call DebugMsg(MODULE_NAME, strProcedureName, endlTraceIn)
    
    Select Case (mlngCurrentMode)
        Case (encmLogon):
            Select Case (KeyAscii)
                Case (vbKeyEscape):
                    If Me.ActiveControl.Name = "txtUserID" Then
                        If MsgBoxEx("Are you sure you want to log out of the till?", vbYesNo, _
                            "Logout Confirmation", , , , , RGBMSGBox_PromptColour) = vbYes Then
                            Unload Me
                            End
                        Else
                            GoTo DebugMsgExit
                        End If
                    End If
                    txtPassword.Text = vbNullString
                    If (txtUserID.Visible = True) And (txtUserID.Enabled = True) Then txtUserID.SetFocus
                    Debug.Print ("frmTill (encmLogon) - Escape Key Pressed")

            End Select
        
        Case (encmTranType):
            Select Case (KeyAscii)
                Case (vbKeyEscape):
                    Call SignOff
                    Debug.Print ("frmTill (encmTranType) - Escape Key Pressed")

            End Select
        
        Case (encmRecord):
            Select Case (KeyAscii)
                Case (vbKeyEscape):
                    If mblnCreatingOrder = True Then
                        If mstrOrderQuit = "Y" Then
                            mlngCurrentMode = encmTender
                            Call SetEntryMode(enemNone)
                            Call Form_KeyPress(vbKeyEscape)
                            GoTo DebugMsgExit
                        End If
' MO'C Refs 2010-012 Removed as per ref no.1
'                        strMsg = goSession.GetParameter(PRM_TAKE_NOW_MSG)
'                        If mstrOrderQuit = "" Then
'                            If MsgBoxEx(strMsg, vbYesNo + vbDefaultButton2, "Order", , , , , RGBMsgBox_WarnColour) = vbYes Then
'                                mstrOrderQuit = "Y"
'                                mlngCurrentMode = encmTender
'                                Call SetEntryMode(enemNone)
'                                Call Form_KeyPress(vbKeyEscape)
'                                Exit Sub
'                            End If
'                        ElseIf mstrOrderQuit = "Y" Then
                            mstrOrderQuit = "Y"
                            mlngCurrentMode = encmTender
                            Call SetEntryMode(enemNone)
                            Call Form_KeyPress(vbKeyEscape)
                            GoTo DebugMsgExit
'                        End If
                    End If
            End Select
            
        Case (encmReversal):
            Select Case (KeyAscii)
                Case (vbKeyEscape):
                    fraVoidLine.Visible = False
                    Call StretchScrollBars(False)
                    mlngCurrentMode = encmRecord
                    Call SetEntryMode(enemSKU)
                    Debug.Print ("frmTill (encmreversal) - Escape Key Pressed")
                Case (vbKeyReturn):
                    KeyAscii = 0
                    Load frmReverseItem
                    If frmReverseItem.ConfirmReversal(lblVoidSKU.Caption, lblVoidDesc.Caption) Then
                        lngAnotherRevLineNo = sprdLines.Row ' Referral 669. lngLineNo or lngRevLineNo used to store rev line no.  Need one for all
                        sprdLines.Col = COL_LINETYPE
                        If (sprdLines.Value = LT_COUPON) Then
                            lngLineNo = sprdLines.Row
                            sprdLines.Col = COL_VOIDED
                            sprdLines.Text = 1 'flag original item as voided
                            sprdLines.Col = -1
                            sprdLines.FontStrikethru = True
                            sprdLines.Col = COL_CPN_STATUS
                            sprdLines.Text = "7"
                            sprdLines.Col = 0
                            sprdLines.Text = "R"
                            sprdLines.FontStrikethru = False
                            sprdLines.Col = COL_CPN_ID
                            Call moEvents.DeleteCoupon(sprdLines.Text, lngLineNo)
                            blnCoupon = True ' so skip Reason Code
                        Else
                            sprdLines.MaxRows = sprdLines.MaxRows + 1
                            sprdLines.Col = COL_VOLUME
                            sprdLines.Text = 0 'clear out volume so that it is omitted from totals
                            Call sprdLines.CopyRowRange(sprdLines.Row, sprdLines.Row, sprdLines.MaxRows)
                            lngRevLineNo = sprdLines.Row
                            sprdLines.Col = COL_VOIDED
                            sprdLines.Text = 1 'flag original item as voided
                            sprdLines.Col = COL_PARTCODE
                            strSKU = sprdLines.Text 'flag original item as voided
                            sprdLines.Col = COL_DISCCODE
                            strPreCode = sprdLines.Text 'flag original item as voided
                            sprdLines.Col = -1
                            sprdLines.FontStrikethru = True
                            sprdLines.Col = 0
                            sprdLines.Text = "R"
                            sprdLines.FontStrikethru = False
                            'Check if Line reverse had a price promise, if so then delete line
                            sprdLines.Row = sprdLines.Row + 1
                            sprdLines.Col = COL_LINETYPE
                            If (sprdLines.Text = LT_PRICEPROM) Then
                                sprdLines.Col = COL_PP_POS
                                Call moTranHeader.GetPricePromiseLine.Remove(CLng(sprdLines.Value))
                                Call sprdLines.DeleteRows(sprdLines.Row, 1)
                                sprdLines.MaxRows = sprdLines.MaxRows - 1
                                'Check if prior Line is the Refund Line for the Price Promise
                                sprdLines.Row = sprdLines.Row - 2
                                
                                sprdLines.Col = COL_PPLINE
                                If (sprdLines.Text = "1") Then
                                    Call sprdLines.DeleteRows(sprdLines.Row, 1)
                                    sprdLines.MaxRows = sprdLines.MaxRows - 1
                                End If
                                'step through rest of lines and resync any price promise positions
                                If GetBooleanParameter(-8, goDatabase.Connection) Then
                                    'Discoverred
                                    Dim PriceMatchRow As Integer
                                    PriceMatchRow = sprdLines.Row + 1
                                    
                                    For lngLineNo = sprdLines.MaxRows To PriceMatchRow Step -1
                                        sprdLines.Row = lngLineNo
                                        sprdLines.Col = COL_PP_POS
                                        If (Val(sprdLines.Value) > 0) Then sprdLines.Value = sprdLines.Value - 1
                                    Next lngLineNo
                                    
                                Else
                                    For lngLineNo = sprdLines.Row - 1 To sprdLines.MaxRows Step 1
                                        sprdLines.Row = lngLineNo
                                        sprdLines.Col = COL_PP_POS
                                        If (Val(sprdLines.Value) > 0) Then sprdLines.Value = sprdLines.Value - 1
                                    Next lngLineNo
                                End If
                            End If
                            sprdLines.Row = sprdLines.MaxRows
                            sprdLines.Col = COL_QTY
                            sprdLines.Text = -Val(sprdLines.Value)
                            sprdLines.Col = COL_VOIDED
                            sprdLines.Text = 2 'mark void line as a Void line
                            sprdLines.RowHeight(sprdLines.MaxRows) = 0
                            
                            'Check if Item is a Day Price and if so, reverse Temp Price if no more SKUs match
                            If (strPreCode = mstrAmendPriceReason) Then
                                blnAmendFound = False
                                For lngLineNo = 2 To sprdLines.MaxRows Step 1
                                    sprdLines.Row = lngLineNo
                                    sprdLines.Col = COL_PARTCODE
                                    If (sprdLines.Value = strSKU) Then
                                        sprdLines.Col = COL_VOIDED
                                        If (Val(sprdLines.Value) = 0) Then
                                            sprdLines.Col = COL_DISCCODE
                                            If (sprdLines.Value = mstrAmendPriceReason) Then
                                                blnAmendFound = True
                                                Exit For
                                            End If
                                        End If
                                    End If
                                Next lngLineNo 'step through lines and update all items with same sku
                                If (blnAmendFound = False) Then
                                    For lngLineNo = 1 To mcolPriceAmends.Count Step 1
                                        If (mcolPriceAmends(lngLineNo).TimeCreated = "") And (mcolPriceAmends(lngLineNo).PartCode = strSKU) Then
                                            mcolPriceAmends.Remove (lngLineNo)
                                            Exit For
                                        End If
                                    Next
                                End If 'other Day Prices checked to see if can be deleted
                            End If 'check if must reverse Price Amendment
                            
                            Call SetGridTotals(sprdLines.MaxRows)
                            Call UpdateTotals
                            Call moEvents.DeleteSKU(lblVoidSKU.Caption, lngRevLineNo)
                            ' Referral 669
                            ' Remove the line from refund collection if is a reversal on a refund.
                            Call RemoveReversedRefund(lngAnotherRevLineNo)
                            ' End of Referral 669
                            blnCoupon = False ' so select Reversal Reason Code
                        End If 'line Item to Reverse
                        Unload frmReverseItem
                        sprdLines.Row = sprdLines.MaxRows
                        Call sprdLines.SetActiveCell(1, sprdLines.MaxRows)
                        fraVoidLine.Visible = False
                        Call StretchScrollBars(False)
                        sprdLines.Row = lngLineNo
                        If (blnCoupon = True) Then
                            mlngCurrentMode = encmRecord
                            Call SetEntryMode(enemSKU)
                            If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then txtSKU.SetFocus
                        Else
                            Call LoadReasonCodes(enrmLineReversed)
                        End If
                    Else
                        Unload frmReverseItem
                        sprdLines.Row = sprdLines.MaxRows
                        Call sprdLines.SetActiveCell(1, sprdLines.MaxRows)
                        fraVoidLine.Visible = False
                        Call StretchScrollBars(False)
                        mlngCurrentMode = encmRecord
                        Call SetEntryMode(enemSKU)
                        If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then txtSKU.SetFocus
                    End If
            End Select 'KeyAscii
        Case (encmComplete)
            Select Case (KeyAscii)
                Case (vbKeyReturn):
                    KeyAscii = 0
                    sprdLines.MaxRows = 1
                    Call SetGridTotals(sprdLines.MaxRows)
                    Call UpdateTotals
                    fraTrader.Visible = False
                    sprdLines.Visible = False
                    sprdTotal.Visible = False
                    
                    If (goSession.ISession_IsOnline) Then
                        Me.BackColor = goSession.GetParameter(PRM_BRANDING_IMAGE_BACKCOLOUR)
                    End If
                    
                    fraActions.Visible = False
                    fraLogon.Visible = True
                    
                    UpdateBrandingImageVisibility (True)
                    
                    If goSession.GetParameter(PRM_USER_RELOG_IN) = False Then
                        Call SetCurrentMode(enCurrentMode.encmTranType)
                    Else
                        Call SignOff
                        Call UserLogon
                    End If
            End Select 'KeyAscii
        Case (encmTender)
            Select Case (KeyAscii)
                Case (vbKeyEscape):
                    ' Commidea Into WSS
                    ' Referral 482
                    ' If just come back from a cancelled/failed eft then need to
                    ' just go back to selecting an alternative tender type
                    If Not mblnReturningFromCancelledEFTTender Then
                        If mblnCreatingOrder = True Then
                            If mstrOrderQuit = "N" Then
                                GoTo DebugMsgExit
                            End If
                            'MO'C Refs 2010-012 Removed as per Ref No.1
'                            strMsg = goSession.GetParameter(PRM_TAKE_NOW_MSG)
'                            If MsgBoxEx(strMsg, vbYesNo + vbDefaultButton2, "Order", , , , , RGBMsgBox_WarnColour) = vbNo Then
'                                mstrOrderQuit = "N"
'                                'mlngCurrentMode = encmRecord
'                                'Call SetEntryMode(enemNone)
'                                'Call Form_KeyPress(vbKeyEscape)
'                                Exit Sub
'                            Else
                                mstrOrderQuit = "Y"
                                mblnCreatingOrder = False
'                            End If
                        End If
                    Else
                        mblnReturningFromCancelledEFTTender = False
                    End If
                    ' End of Commidea Into WSS

                    If (mblnFirstPmtDone = False) And (mlngEntryMode = enemNone) Then
                        fraTender.Visible = False
                        fraTranType.Visible = False
                        'Take of Total from Grid
                        If mblnTransactionDiscountApplied = True Then
'                            sprdLines.MaxRows = sprdLines.MaxRows - 4
'                            sprdLines.Row = sprdLines.MaxRows
'                            Call SetGridTotals(sprdLines.Row)
'                            Call UpdateTotals
                            Call EnableAction(enacTender, True)
                            Call EnableAction(enacOrder, Not Not mblnCustomerOrder Or Not DisableTrainingModeOrders)
                        Else
'                            sprdLines.MaxRows = sprdLines.MaxRows - 3
'                            If mlngEntryMode = enemValue Then
'                                sprdLines.MaxRows = sprdLines.MaxRows - 1
'                            End If
                        End If
                        'Remove any OP Event lines
                        Call ClearEvents
                        
                        'Added 17/4/09 -Restore Item Price to pre-Transaction Discount Levels, if present
                        Call DebugMsg(MODULE_NAME, strProcedureName, endlDebug, "Rollback Transaction Discount")
                        For lngLineNo = 2 To sprdLines.MaxRows Step 1
                            sprdLines.Row = lngLineNo
                            sprdLines.Col = COL_LINETYPE
                            If (Val(sprdLines.Value) = LT_ITEM) Then
                                sprdLines.Col = COL_PRETRAN_PRICE
                                If (sprdLines.Text <> "") Then
                                    curPayAmount = Val(sprdLines.Value)
                                    sprdLines.Text = ""
                                    sprdLines.Col = COL_SELLPRICE
                                    sprdLines.Value = curPayAmount
                                    sprdLines.Col = COL_PRETRAN_CODE
                                    strPreCode = sprdLines.Value
                                    sprdLines.Text = ""
                                    sprdLines.Col = COL_DISCCODE
                                    sprdLines.Value = strPreCode
                                End If 'Rollback Price found
                            End If 'Line Item
                        Next lngLineNo
                        Call UpdateTotals
                        'If (Left(lblOrderNo.Caption, 1) = "Q") Then lblOrderNo.Caption = ""
                        If (Left(lblOrderNo.Caption, 1) <> "Q") Then
                            mdblTotalBeforeDis = 0
                            mblnTransactionDiscountApplied = False
                        End If
                        'Move screen back to previous state before Tender Total was called
                        Select Case (moTOSType.Code)
                            Case TT_MISCOUT, TT_CORROUT:
                                Call SetEntryMode(enemNone)
                                Call LoadReasonCodes(enrmPaidOut)
                                mlngCurrentMode = encmPaidOut
                                Debug.Print ("frmTill (encmTender) - Escape Key Pressed (OUT: enemNone).")
                            Case TT_MISCIN, TT_CORRIN:
                                Call SetEntryMode(enemNone)
                                Call LoadReasonCodes(enrmPaidIn)
                                mlngCurrentMode = encmPaidIn
                                Debug.Print ("frmTill (encmTender) - Escape Key Pressed (IN: enemNone).")
                            Case Else
                                Call SetEntryMode(enemSKU)
                                mlngCurrentMode = encmRecord
                                Debug.Print ("frmTill (encmTender) - Escape Key Pressed (enemSKU).")
                        End Select
                    Else
                        'past first payment so unable to cancel out
                        If mlngEntryMode = enemValue Then
                            sprdLines.MaxRows = sprdLines.MaxRows - 1
                        End If
                        
                        Call SetEntryMode(enemNone)
                        
                        'Ref: 1065 - Check to see if cmdTenderType is loaded, if not load it.
                        If GetBooleanParameter(-1065, goDatabase.Connection) Then
                            If cmdTenderType.UBound = 0 Then
                                Call LoadTenderBar
                            End If
                        End If
                        
                        fraTender.Visible = True
                        
                        Debug.Print ("frmTill (encmTender) - Escape Key Pressed (Past First payment).")
                   
                    End If
                    Debug.Print ("frmTill (encmTender) - Escape Key Pressed")

            End Select 'KeyAscii
        Case (encmPaidOut):
            Select Case (KeyAscii)
                Case (vbKeyEscape):
                    If mlngEntryMode = enemValue Then
                        VoidTransaction
'                        mlngCurrentMode = encmComplete
'                        Call Form_KeyPress(vbKeyReturn)
'                    ElseIf fraReasonsList.Visible Then
'                        fraReasonsList.Visible = False
'                        Call SetEntryMode(enemNone)
                    Else
                        VoidTransaction
                    End If
                    Debug.Print ("frmTill (encmPaidOut) - Escape Key Pressed")
            End Select
        Case (encmPaidIn):
            Select Case (KeyAscii)
                Case (vbKeyEscape):
                    If mlngEntryMode = enemValue Then
                        VoidTransaction
'                        mlngCurrentMode = encmComplete
'                        Call Form_KeyPress(vbKeyReturn)
'                    ElseIf fraReasonsList.Visible Then
'                        fraReasonsList.Visible = False
'                        Call SetEntryMode(enemNone)
                    End If
            End Select
        Case (encmOpenDrawer):
            Select Case (KeyAscii)
                Case (vbKeyEscape):
'                    If fraReasonsList.Visible Then
'                        fraReasonsList.Visible = False
'                        Call SetEntryMode(enemNone)
'                    End If
            End Select
    
        Case (encmRefund):
            Select Case (KeyAscii)
                Case (vbKeyEscape):
                    If fraReasonsList.Visible Then
                        fraReasonsList.Visible = False
                        mdblLineQuantity = 1
                        mlngCurrentMode = encmRecord
                        Call SetEntryMode(enemSKU)
                    End If
                    Debug.Print ("frmTill (encmRefund) - Escape Key Pressed")

            End Select
            
    End Select 'Current Mode
            
    If KeyAscii = vbKeyEscape Then
'        If fraComplete.Visible = True Then
'            mblnByPassCashDrawer = True
'        End If
    End If
    
DebugMsgExit:
    Call DebugMsg(MODULE_NAME, strProcedureName, endlTraceOut)
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler(strProcedureName, Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub SignOff()

    fraTranType.Visible = False
    If (moUser Is Nothing) = False Then Call UserSignedOff(moUser, mstrTillNo, mstrTranId)
    Set moUser = Nothing
    mlngCurrentMode = encmLogon
    fraLogon.Visible = True
    
    UpdateBrandingImageVisibility (True)
    
    txtUserID.Visible = True
    lblLoggedUserID.Visible = False
    txtUserID.Text = vbNullString
    If (txtUserID.Visible = True) And (txtUserID.Enabled = True) Then txtUserID.SetFocus

End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

Debug.Print "form down" & ActiveControl.Name & Now()

End Sub

Private Sub Form_Resize()

Dim curStretchPerc As Double
Dim lblResize   As Label
Dim objLabels   As Object
Dim lngColNo    As Long

    On Error GoTo HandleException
    
    sbStatus.Top = 0
    
    curStretchPerc = 1 + Round((Me.Width - fraTrader.Width) / (fraTrader.Width), 4)
    fraTrader.Width = fraTrader.Width * curStretchPerc
    fraActions.Width = fraTrader.Width
    fraTranType.Width = fraTrader.Width
    fraEntry.Width = fraTrader.Width
    fraLogon.Width = lblTrainingMode.Left + lblTrainingMode.Width + 500
    fraTender.Width = fraTrader.Width
    fraVoidLine.Width = fraTrader.Width
    sprdLines.Width = fraTrader.Width
    sprdTotal.Width = fraTrader.Width
    
    UpdateBrandingImageAndLogonConrollsPosition
    
    For lngColNo = 1 To sprdTotal.MaxCols Step 1
        sprdTotal.ColWidth(lngColNo) = sprdTotal.ColWidth(lngColNo) * curStretchPerc
    Next lngColNo
    
    'Stretch the First Command button before it is duplicated
    cmdTranType(0).Width = cmdTranType(0).Width * curStretchPerc
    cmdAction(0).Width = cmdAction(0).Width * curStretchPerc
    cmdTenderType(0).Width = cmdTenderType(0).Width * curStretchPerc
    
    'Move the Next and Previous buttons over
    cmdNextAction.Left = cmdNextAction.Left * curStretchPerc
    cmdNextAction.Width = cmdNextAction.Width * curStretchPerc
    cmdPrevAction.Left = cmdPrevAction.Left * curStretchPerc
    cmdPrevAction.Width = cmdPrevAction.Width * curStretchPerc

    cmdNextTenderType.Left = cmdNextTenderType.Left * curStretchPerc
    cmdNextTenderType.Width = cmdNextTenderType.Width * curStretchPerc
    cmdPrevTenderType.Left = cmdPrevTenderType.Left * curStretchPerc
    cmdPrevTenderType.Width = cmdPrevTenderType.Width * curStretchPerc
    
    cmdNextTranType.Left = cmdNextTranType.Left * curStretchPerc
    cmdNextTranType.Width = cmdNextTranType.Width * curStretchPerc
    cmdPrevTranType.Left = cmdPrevTranType.Left * curStretchPerc
    cmdPrevTranType.Width = cmdPrevTranType.Width * curStretchPerc
    
    fraTender.Top = sbStatus.Top - fraTender.Height
    fraActions.Top = sbStatus.Top - fraActions.Height
    fraTranType.Top = sbStatus.Top - fraTranType.Height
        
    For Each objLabels In Controls
        If TypeOf objLabels Is Label Then
            Set lblResize = objLabels
            If ((lblResize.Container.Name = fraVoidLine.Name) Or (lblResize.Container.Name = fraTrader.Name)) Then
                lblResize.Left = lblResize.Left * curStretchPerc
                If lblResize.BorderStyle <> 0 Then lblResize.Width = lblResize.Width * curStretchPerc
            End If
        End If
    Next
    cmdVoidLineEsc.Left = (fraVoidLine.Width / 2) - cmdVoidLineEsc.Width - 240
    cmdVoidLineOK.Left = (fraVoidLine.Width / 2) + 240

    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("Form_Resize", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub Form_Unload(Cancel As Integer)

    On Error GoTo HandleException

    Call ClosePrinter(OPOSPrinter1)
    ' Commidea Into WSS
    Call ClosePED(mstrCommideaUserID, mstrCommideaUserPIN)
    ' End of Commidea Into WSS
    
    Set frmCapturePricePromDetails = Nothing
    Set frmNotFound = Nothing
    Set frmPostcode = Nothing
    Set frmReprintReceipt = Nothing
    Set frmReverseItem = Nothing
    Set frmRetrieveTran = Nothing
    Set frmTender = Nothing
    Set frmVerifyPwd = Nothing
    Set frmTokenScan = Nothing
    Set frmCustomerAddress = Nothing
    Set frmComplete = Nothing
    Set frmCoupon = Nothing
    Set frmDiscountCardScheme = Nothing
    Set frmConcernRes = Nothing
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("Form_Unload", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub frmCapturePricePromDetails_Duress()
    OpenDrawerUnderDuress
End Sub

Private Sub frmNotFound_Duress()
    OpenDrawerUnderDuress
End Sub

Private Sub moCommidea_Duress()
    
    OpenDrawerUnderDuress
End Sub

Private Sub moCommidea_InvalidTokenID()

    mblnInvalidTokenID = True
End Sub

Private Sub moReceiptPrinting_CommideaReferenceRequired(CommideaRef As String, ByVal StoreNumber As String, ByVal TillID As String, ByVal TransactionNo As String, ByVal TranDate As Date, ByVal TranTime As Date, ByVal SequenceNumber As Integer)
    
    On Error GoTo HandleException
    
    If SequenceNumber >= 0 Then
        CommideaRef = GenerateWSSUniqueTransactionTenderReferenceWithSequenceNumber(StoreNumber, _
                                                                               TillID, _
                                                                               TransactionNo, _
                                                                               TranDate, _
                                                                    SequenceNumber)
    Else
        CommideaRef = GenerateWSSUniqueTransactionTenderReferenceWithoutSequenceNumber(StoreNumber, _
                                                                    TillID, _
                                                                    TransactionNo, _
                                                                    TranDate, _
                                                                    TranTime)
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("moReceiptPrinting_CommideaReferenceRequired", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub


Private Sub sbStatus_PanelClick(ByVal Panel As MSComctlLib.Panel)

Dim blnShow As Boolean

    On Error GoTo HandleException

    blnShow = ucKeyPad1.Visible
    Select Case (Panel.Key)
        Case ("KeyBoard"):
                            If ((Panel.Picture Is Nothing) = False) Then
                                If (ucKeyPad1.KeyboardShown = True) Then blnShow = Not blnShow
                                Call ucKeyPad1.ShowKeyboard
                                ucKeyPad1.Visible = blnShow
                            End If
        Case ("NumPad"):
                            If ((Panel.Picture Is Nothing) = False) Then
                                If (blnShow = False) Then ucKeyPad1.Visible = True
                                If (ucKeyPad1.KeyboardShown = False) Then blnShow = Not blnShow
                                Call ucKeyPad1.ShowNumPad
                                ucKeyPad1.Visible = blnShow
                            End If
    End Select
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("sbStatus_PanelClick", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub txtUserID_KeyDown(KeyCode As Integer, Shift As Integer)

If 1 = 1 Then
End If

End Sub

Private Sub ucKeyPad1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

    ucKeyPad1.BackColor = vbGrayText
    mlngClickX = X
    mlngClickY = Y

End Sub

Private Sub ucKeyPad1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    If Button = 1 And Shift = 0 Then
        ucKeyPad1.Left = ucKeyPad1.Left + X - mlngClickX
        ucKeyPad1.Top = ucKeyPad1.Top + Y - mlngClickY
    End If

End Sub

Private Sub ucKeyPad1_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  
  ucKeyPad1.BackColor = RGB(0, 64, 0)
    
End Sub



Private Sub lblTranNo_Change()

    If Val(lblTranNo.Caption) = 0 Then 'error allocating Transaction ID, so give indication
        lblTranNo.BackColor = RGB_RED
    Else
        lblTranNo.BackColor = lblOrderNo.BackColor
    End If

End Sub

Private Sub lstReasonsList_KeyPress(KeyAscii As Integer)

Dim dblTranTotal As Double
Dim strDesc      As String
Dim lngKeyIn     As Long
Dim lngSelectPos As Long
Dim lngCurrMode  As Long
Dim curNewPrice  As Currency
Dim curWEEERate  As Currency
Dim curPPPrice   As Currency
Dim curVATRate   As Currency
Dim strSKU       As String

Dim curCompPrice    As Currency
Dim blnVATInc       As Boolean
Dim curCompPriceExc As Currency
Dim lngReqQty       As Long
Dim lngValidQty     As Long

Dim strContact1Salutation As String
Dim strContact1FirstName As String
Dim strContact1SurName As String
Dim strCompanyName As String
Dim strPostCode As String
Dim strHouseNumberName As String
Dim strAddressLine1 As String
Dim strAddressLine2 As String
Dim strTown As String
Dim strCounty As String
Dim strCountry As String

Dim oSelectedMP  As cPaidInCode
Dim oSelectedMM  As cPaidOutCode
Dim oSelectedOC  As cPriceOverrideCode
Dim oSelectedTD  As cTransactionDiscount
Dim oSelectedOD  As cOpenDrawerCode
Dim oSelectedSA  As cStockAdjustmentCode
Dim oSelectedRF  As cRefundCode
Dim oSelectedTO  As cTenderOverride
Dim oSelectedBC  As cBarcodeBroken
Dim oWStationBO  As cWorkStation
Dim oSelectedLR  As cLineReversalCode
Dim MarkDownSkuOverride As Boolean

    On Error GoTo HandleException
    
    If (KeyAscii = vbKeyReturn) And (lstReasonsList.ListIndex <> -1) Then
        KeyAscii = 0
            lngSelectPos = lstReasonsList.ItemData(lstReasonsList.ListIndex)
            Select Case (mlngCurrentMode)
            Case (encmPaidOut)
                cmdProcess.Visible = True
                cmdViewLines.Visible = True
                Set oSelectedMM = mcolPaidOutCodes(lngSelectPos)
                sprdLines.Col = COL_DESC
                sprdLines.Row = sprdLines.MaxRows
                sprdLines.Text = oSelectedMM.Description
                mstrReasonDesc = oSelectedMM.Description
                mlngReasonCode = oSelectedMM.Code
                If LenB(Trim$(oSelectedMM.Description)) = 0 Then
                    lblReasonDesclbl.Caption = "Enter Reason for Paying Out"
                    fraReasonsList.Height = txtReasonDescription.Top + txtReasonDescription.Height + 120
                    Call SetEntryMode(enemDesc)
                    If (txtReasonDescription.Visible = True) And (txtReasonDescription.Enabled = True) Then Call txtReasonDescription.SetFocus
                Else
                    fraReasonsList.Visible = False
                    fraReasonsList.Height = lstReasonsList.Top + lstReasonsList.Height + 120
                    Call SetEntryMode(enemValue)
                    End If
            Case (encmPaidIn)
                cmdProcess.Visible = True
                cmdViewLines.Visible = True
                Set oSelectedMP = mcolPaidInCodes(lngSelectPos)
                sprdLines.Col = COL_DESC
                sprdLines.Row = sprdLines.MaxRows
                sprdLines.Text = oSelectedMP.Description
                mstrReasonDesc = oSelectedMP.Description
                mlngReasonCode = oSelectedMP.Code
                If LenB(Trim$(oSelectedMP.Description)) = 0 Then
                    lblReasonDesclbl.Caption = lstReasonsList.Text & "-Description"
                    fraReasonsList.Height = txtReasonDescription.Top + txtReasonDescription.Height + 120
                    Call SetEntryMode(enemDesc)
                    If (txtReasonDescription.Visible = True) And (txtReasonDescription.Enabled = True) Then Call txtReasonDescription.SetFocus
                Else
                    fraReasonsList.Visible = False
                    fraReasonsList.Height = lstReasonsList.Top + lstReasonsList.Height + 120
                    Call SetEntryMode(enemValue)
                    'Call RecordTender
                End If
            Case (encmOverrideCode)
                fraReasonsList.Visible = False
                cmdProcess.Visible = True
                cmdViewLines.Visible = True
                sprdLines.Col = COL_MARKDOWN
                If sprdLines.Value = "True" Then
                    Set oSelectedSA = mcolMarkDownCodes(lngSelectPos)
                    sprdLines.Col = COL_DISCCODE
                    sprdLines.Text = oSelectedSA.AdjustmentNo
                    MarkDownSkuOverride = True
                 Else
                    MarkDownSkuOverride = False
                    Set oSelectedOC = mcolPriceOverrides(lngSelectPos)
                    'Check if selected Reason code, requires Manager or Supervisor authorisation
                    If (oSelectedOC.SupervisorAuthorisation = True) Or (oSelectedOC.ManagerAuthorisation = True) Then
                        Call SetEntryMode(enemNone) 'disable barcode
                        Load frmVerifyPwd
                        If (oSelectedOC.SupervisorAuthorisation = True) And (oSelectedOC.ManagerAuthorisation = False) Then
                            If frmVerifyPwd.VerifySupervisorPassword(ucKeyPad1.Visible) = True Then
                                moTranHeader.SupervisorUsed = True
                                Call SetEntryMode(enemNone)
                                sprdLines.Col = COL_SUPERVISOR
                                sprdLines.Text = frmVerifyPwd.txtAuthID.Text
                            Else 'invalid authorisation code, or escape pressed
                                Unload frmVerifyPwd
                                mlngCurrentMode = encmRecord
                                Call SetEntryMode(enemSKU)
                                Exit Sub
                            End If
                        End If ' Supervisor required
                        If (oSelectedOC.ManagerAuthorisation = True) Then
                            If frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) = True Then
                                moTranHeader.SupervisorUsed = True
                                sprdLines.Col = COL_SUPERVISOR
                                sprdLines.Text = frmVerifyPwd.txtAuthID.Text
                            Else 'invalid authorisation code, or escape pressed
                                Unload frmVerifyPwd
                                mlngCurrentMode = encmRecord
                                Call SetEntryMode(enemSKU)
                                Exit Sub
                            End If
                        End If ' Supervisor required
                        Unload frmVerifyPwd
                    End If
                                        
                    If oSelectedOC.Code <> "12" Then 'Do not put price override data into DLLINEs for Multi Sku Override
                        sprdLines.Col = COL_DISCCODE
                        sprdLines.Text = oSelectedOC.Code
                        sprdLines.Col = COL_DISCMARGIN
                        sprdLines.Text = Right$("000000" & oSelectedOC.Code, 6)
                        If (sprdLines.Row < sprdLines.MaxRows) Then 'check if price match has been added, and delete if so
                            sprdLines.Row = sprdLines.Row + 1
                            sprdLines.Col = COL_LINETYPE
                            If (sprdLines.Value = LT_PRICEPROM) Then
                                sprdLines.Col = COL_PP_POS
                                Call moTranHeader.GetPricePromiseLine.Remove(CLng(sprdLines.Value))
                                Call sprdLines.DeleteRows(sprdLines.Row + 1, 1)
                                sprdLines.MaxRows = sprdLines.MaxRows - 1
                            End If
                            'Check if Price Promise, if so remove Refund Line
                            sprdLines.Row = sprdLines.MaxRows - 1
                            sprdLines.Col = COL_PPLINE
                            If (sprdLines.Text = "1") Then
                                Call sprdLines.DeleteRows(sprdLines.Row, 1)
                                sprdLines.MaxRows = sprdLines.MaxRows - 1
                            End If
                            sprdLines.Row = sprdLines.MaxRows
                        End If
                           
                        If (oSelectedOC.PriceMatch = True) Then
                            If GetBooleanParameter(-8, goDatabase.Connection) = False Then
                                If (mstrName = "") Then
                                    If (frmCustomerAddress.GetPricePromiseAddress(mstrName, mstrPostCode, mstrAddress, mstrTelNo) = False) Then
                                        'cancel pressed so exit back to Reason List
                                        Call LoadReasonCodes(enrmPriceOverride)
                                        Exit Sub
                                    End If
                                End If
                            End If
                            sprdLines.Col = COL_PARTCODE
                            strSKU = sprdLines.Text
                            sprdLines.Col = COL_DESC
                            strDesc = sprdLines.Text
                            sprdLines.Col = COL_QTY
                            lngReqQty = sprdLines.Value
                            sprdLines.Col = COL_SELLPRICE
                            curNewPrice = Val(sprdLines.Value)
                            sprdLines.Col = COL_VATRATE
                            curVATRate = sprdLines.Value
                            sprdLines.Col = COL_WEEE_RATE
                            curWEEERate = Val(sprdLines.Value)
                            Load frmCapturePricePromDetails
                            lngCurrMode = mlngCurrentMode
                            mlngCurrentMode = encmRefund
                            If (frmCapturePricePromDetails.CapturePricePromise(moTranHeader, strSKU, strDesc, sprdLines.Row, _
                                   curVATRate, curNewPrice, curWEEERate, mstrName, mstrAddress, mstrTelNo, mstrPostCode, curNewPrice, _
                                   curPPPrice, curCompPrice, blnVATInc, curCompPriceExc, oSelectedOC.PriceMultiplier, oSelectedOC.PriceMaximum, _
                                   oSelectedOC.ManagerFlexibility, lngReqQty, lngValidQty, frmRetrieveTran) = True) Then
                                    
                                
                                If (lngValidQty > 0) And (lngReqQty <> lngValidQty) Then
                                    sprdLines.Col = COL_QTY
                                    sprdLines.Value = lngValidQty
                                End If
                                mblnPricePromise = True
                            Else
                                Unload frmCapturePricePromDetails
                                mlngCurrentMode = encmRecord
                                Call SetEntryMode(enemSKU)
                                Exit Sub
                            End If
                            mlngCurrentMode = lngCurrMode
                            Unload frmCapturePricePromDetails
                        End If
                    End If
                
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ' Author   : Partha Dutta
                    ' Date     : 06/12/2011
                    ' Project  : PO14-03
                    ' Notes    : Single SKU Override Price
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    If (oSelectedOC.Code = "11") Then
                       Call SetEntryMode(enemNone)

                       Dim X As New frmSkuTemporaryPriceOverride
                       Dim CancelPressed As Boolean

                       Load X

                       With X
                          'sku :code
                          sprdLines.Col = COL_PARTCODE
                          .txtSkuCode.Text = sprdLines.Text

                          'sku : description
                          sprdLines.Col = COL_DESC
                          .txtSkuDescription.Text = sprdLines.Text

                          'sku : existing selling price
                          sprdLines.Col = COL_SELLPRICE
                          .txtExistingPrice.Text = sprdLines.Text

                          'user one id : till user
                          .UserOneID (txtUserID.Text)

                          'user two id : supervisor/manager user
                          sprdLines.Col = COL_SUPERVISOR
                          .UserTwoID (sprdLines.Text)

                          .Show vbModal
                          CancelPressed = X.CancelPressed

                          If X.CancelPressed = False Then
                             sprdLines.Col = COL_SELLPRICE

                             curNewPrice = X.NewPrice
                             sprdLines.Text = curNewPrice

                          End If
                       End With
                        
                       Call UpdateTotals

                       Unload X
                       Set X = Nothing

                       If CancelPressed = True Then
                          mlngCurrentMode = encmRecord
                          Call SetEntryMode(enemSKU)
                          Exit Sub
                       End If
                   End If
                
                    ''''''''''''''''''''''''''''''''''''''''''' End : PO14-03 ''''''''''''''''''''''''''''''''''''''''''
                    Call SetEntryMode(enemPrice)
                    txtSKU.Text = curNewPrice '+ curWEEERate
                    mlngCurrentMode = encmOverrideCode
                    Call txtSKU_KeyPress(vbKeyReturn)
                    
                End If
                
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ' Author   : Michael O'Cain
                ' Date     : 20/12/2011
                ' Project  : PO14-04
                ' Notes    : Multiple SKU Override Price
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                If MarkDownSkuOverride = True Then
                    
                    Call SetEntryMode(enemPrice)
                    txtSKU.Text = curNewPrice '+ curWEEERate
                    mlngCurrentMode = encmOverrideCode
                    Call txtSKU_KeyPress(vbKeyReturn)

                Else
                    If (oSelectedOC.Code = "12") Then
                       'requirement switch
                        On Error GoTo ErrHandled
                        If goSession.GetParameter(981404) = True Then
                            Call SetEntryMode(enemNone)
                            
                            CancelPressed = False
                            Dim Discount As New frmBackDoorCollect
                            Load Discount
                            
                            Dim curDiscountValue As Currency
                            Dim curSelectedValue As Currency
                            Dim curPrice As Currency
                            Dim curTotal As Currency
                            Dim strAllocItems() As String
                            
                            sprdTotal.Col = COL_TOTVAL
                            sprdTotal.Row = ROW_TOTTOTALS
                            curTotal = sprdTotal.Value
            
                            With Discount
                            
                                Dim lngRowNo As Long
                                Dim lngQuantity As Long
                                 
                                'Load Discount Grid
                                For lngRowNo = 2 To frmTill.sprdLines.MaxRows Step 1
                                    frmTill.sprdLines.Row = lngRowNo
                                    frmTill.sprdLines.Col = COL_LINETYPE
                                    If (frmTill.sprdLines.Value = LT_ITEM) Then
                                        frmTill.sprdLines.Col = COL_VOIDED
                                        If (Val(frmTill.sprdLines.Value) = 0) Then
                                            frmTill.sprdLines.Col = COL_SALETYPE
                                            If (frmTill.sprdLines.Value <> "D" And frmTill.sprdLines.Value <> "V") Then
                                                frmTill.sprdLines.Col = COL_QTY
                                                If (Val(frmTill.sprdLines.Value) > 0) Then
                                                    lngQuantity = Val(frmTill.sprdLines.Value)
                                                    frmTill.sprdLines.Col = COL_DESC
                                                    strDesc = frmTill.sprdLines.Text
                                                    frmTill.sprdLines.Col = COL_INCTOTAL
                                                    curPrice = frmTill.sprdLines.Text
                                                    frmTill.sprdLines.Col = COL_PARTCODE
                                                    Call Discount.AddItem(lngRowNo, frmTill.sprdLines.Text, strDesc, "" & lngQuantity, "", "", "", "", curPrice)
                                                End If
                                            End If
                                        End If
                                    End If
                                Next lngRowNo
                            
                                curDiscountValue = 0
                                curSelectedValue = 0
                                
                                Dim AuthId As String
                                sprdLines.Col = COL_SUPERVISOR
                                AuthId = sprdLines.Text
    
                                Call Discount.AllocateDiscount(curDiscountValue, strAllocItems, curSelectedValue, curTotal, CLng(AuthId))
                            
                            End With
                            
                            Unload Discount
                            Set Discount = Nothing
                        
                        End If
                        
ErrHandled:
                        Err.Clear
                        On Error GoTo 0
                        
                        mlngCurrentMode = encmRecord
                        Call SetEntryMode(enemSKU)
                        Exit Sub
                    
                    End If
                                
                End If
                ''''''''''''''''''''''''''''''''''''''''''' End : PO14-04 ''''''''''''''''''''''''''''''''''''''''''
            
            Case (encmRefundPriceOverride)
                fraReasonsList.Visible = False
                cmdProcess.Visible = True
                cmdViewLines.Visible = True
                Set oSelectedOC = mcolPriceOverrides(lngSelectPos)
                sprdLines.Col = COL_DISCCODE
                sprdLines.Text = oSelectedOC.Code
                sprdLines.Col = COL_DISCMARGIN
                sprdLines.Text = Right$("000000" & oSelectedOC.Code, 6)
                mlngCurrentMode = encmRecord
                Call SetEntryMode(enemSKU)
            
            Case (encmTransactionDiscount)
                Set oSelectedTD = mcolTransactionDiscount(lngSelectPos)
                mstrTransDiscountCode = oSelectedTD.Code
                sprdLines.MaxRows = sprdLines.MaxRows + 1
                sprdLines.Row = sprdLines.MaxRows
                sprdLines.Col = 0
                sprdLines.Text = " " 'override auto column lettering
                sprdLines.Col = COL_DESC
                mstrTransDiscountDesc = oSelectedTD.Description
                sprdLines.Text = mstrTransDiscountDesc
                sprdLines.Col = COL_SIZE
                If mlngTransactionDiscountMethod = 2 Then
                    lblReasonDesclbl.Caption = "Transaction Discount Percentage"
                    fraReasonsList.Height = txtReasonDescription.Top + txtReasonDescription.Height + 120
                    txtReasonDescription.Text = vbNullString 'oSelectedTD.Percentage
                    txtReasonDescription.SelStart = 0
                    txtReasonDescription.SelLength = Len(txtReasonDescription.Text)
                    If (txtReasonDescription.Visible = True) And (txtReasonDescription.Enabled = True) Then Call txtReasonDescription.SetFocus
                Else
                    sprdLines.Text = "0.00%" 'oSelectedTD.Percentage & "%"
                    sprdTotal.Row = ROW_TOTTOTALS
                    sprdTotal.Col = COL_TOTVAL
                    dblTranTotal = sprdTotal.Value
                    mdblTotalBeforeDis = sprdTotal.Value
                    sprdLines.Col = COL_INCTOTAL
                    mdblPercentageValue = dblTranTotal * 0 'oSelectedTD.Percentage / 100
                    sprdLines.Text = "-" & mdblPercentageValue
                    fraReasonsList.Visible = False
                    DoEvents
                    Call SetGridTotals(sprdLines.Row)
                    sprdLines.Col = COL_QTY
                    sprdLines.Text = vbNullString
                    mblnTransactionDiscountApplied = True
                    Call UpdateTotals
                    Call SetEntryMode(enemNone)
                    Call RecordTender(False, False, True)
                End If
                
            Case (encmBarcodeAudit)
                fraReasonsList.Visible = False
                cmdProcess.Visible = True
                cmdViewLines.Visible = True
                Set oSelectedBC = mcolBarcodeFailedCodes(lngSelectPos)
                If (oSelectedBC.ReaderBroken = True) Then
                    On Error Resume Next
                    Set oWStationBO = goDatabase.CreateBusinessObject(CLASSID_WORKSTATIONCONFIG)
                    Call oWStationBO.RecordBarcodeBroken(goSession.CurrentEnterprise.IEnterprise_WorkstationID, True)
                    Set oWStationBO = Nothing
                    Call Err.Clear
                    mstrBCodeBrokenCode = GetBrokenBarcodeCode 'flag any further audits as broken barcode reader
                End If
                mstrKeyedCode = oSelectedBC.Code
                mstrKeyedSKU = txtSKU.Text
                mlngCurrentMode = encmRecord
            
            Case (encmOpenDrawer)
                cmdProcess.Visible = True
                cmdViewLines.Visible = True
                Set oSelectedOD = mcolOpenDrawerCodes(lngSelectPos)
                sprdLines.Col = COL_DESC
                sprdLines.Row = sprdLines.MaxRows
                sprdLines.Text = oSelectedOD.Description
                mstrReasonDesc = oSelectedOD.Description
                mlngReasonCode = oSelectedOD.Code
                If LenB(Trim$(oSelectedOD.Description)) = 0 Then
                    lblReasonDesclbl.Caption = lstReasonsList.Text & "-Description"
                    fraReasonsList.Height = Me.txtReasonDescription.Top + txtReasonDescription.Height + 120
                    Call SetEntryMode(enemDesc)
                    If (txtReasonDescription.Visible = True) And (txtReasonDescription.Enabled = True) Then Call txtReasonDescription.SetFocus
                Else
                    fraReasonsList.Visible = False
                    fraReasonsList.Height = lstReasonsList.Top + lstReasonsList.Height + 120
                    Call SaveOpenDrawer(False)
                    Call SetCurrentMode(encmTranType)
                End If
            
            Case (encmTenderOverride)
                fraReasonsList.Visible = False
                Set oSelectedTO = mcolTenderOverrideCodes(lngSelectPos)
                moTranHeader.TenderOverrideCode = oSelectedTO.Code
                mlngCurrentMode = encmTender
            
            Case (encmRefund):
                fraReasonsList.Visible = False
                Set oSelectedRF = mcolRefundCodes(lngSelectPos)
                mstrRefundReason = oSelectedRF.Code
                If LenB(mstrName) = 0 Then
                    mlngCurrentMode = encmCustomer
                    Call frmCustomerAddress.GetRefundAddress(mstrName, mstrPostCode, mstrAddress, mstrTelNo, mstrMobileNo, mstrWorkTelNo, mstrEmail)
                    If frmCustomerAddress.Cancel Then
                        If (moTranHeader.TransactionCode = TT_SALE) And (mblnCustDone = False) Then
                            mdblLineQuantity = 1
                            mlngCurrentMode = encmRecord
                            If txtSKU.Visible Then
                                SetEntryMode enemSKU
                            ElseIf uctntPrice.Visible Then
                                SetEntryMode enemPrice
                            Else
                                Call SetEntryMode(enemSKU)
                            End If
                        End If
                        Exit Sub
                    End If
                End If
                mlngCurrentMode = encmRefund
                Call TransactionLookUp
            Case (encmLineReversalCode)
                strSKU = ""
                Set oSelectedLR = mcolLineReversalCodes(lngSelectPos)
                If (oSelectedLR.SKUMismatch = True) Then
                    Set frmEANCheck = New frmEANCheck
                    Load frmEANCheck
                    frmEANCheck.UserID = txtUserID.Text
                    frmEANCheck.ucKeyPad1.Visible = ucKeyPad1.Visible
                    frmEANCheck.txtSKU.Text = lblVoidSKU.Caption
                    sprdLines.Col = COL_REJECT_ID
                    strSKU = frmEANCheck.GetCorrectSKU(lblVoidSKU.Tag <> "", True, Val(sprdLines.Text))
                    Unload frmEANCheck
                    If (strSKU = "") Then Exit Sub
                End If
                fraReasonsList.Visible = False
                cmdProcess.Visible = True
                cmdViewLines.Visible = True
                sprdLines.Col = COL_LINE_REV_CODE
                sprdLines.Text = oSelectedLR.Code
                
                
                mlngCurrentMode = encmRecord
                Call SetEntryMode(enemSKU)
                If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then txtSKU.SetFocus
                If (strSKU <> "") Then
                    txtSKU.Text = strSKU
                    Call txtSKU_KeyPress(vbKeyReturn)
                    sprdLines.Col = COL_KEYED_RC
                    sprdLines.Text = ""
                End If
        End Select
    End If
    If (KeyAscii = vbKeyEscape) Then
        Select Case (mlngCurrentMode)
            Case encmPaidOut
                VoidTransaction
            Case encmPaidIn
                VoidTransaction
            Case encmOpenDrawer
                VoidTransaction
'                fraReasonsList.Visible = False
'                Call SaveOpenDrawer(True)
'                Call SetCurrentMode(encmTranType)
                'Call uctntPrice.SetFocus
            Case (encmOverrideCode)
                fraReasonsList.Visible = False
                cmdProcess.Visible = True
                cmdViewLines.Visible = True
                sprdLines.Col = COL_MARKDOWN
                If sprdLines.Value = "True" Then
                    sprdLines.MaxRows = sprdLines.MaxRows - 1
                    Call SetGridTotals(sprdLines.MaxRows - 1)
                    Call UpdateTotals
                End If
                mlngCurrentMode = encmRecord
                Call SetEntryMode(enemSKU)
            Case (encmRecord)
                Call EnableAction(enacTender, True)
                Call EnableAction(enacOrder, Not mblnCustomerOrder Or Not DisableTrainingModeOrders)
                Call EnableAction(enacOverride, True)
                cmdViewLines.Enabled = True
                'Call EnableAction(enacLookUp, True)
                Call EnableAction(enacVoid, True)
                lstReasonsList.Col = 0
                lstReasonsList.ColHide = True
                fraReasonsList.Visible = False
                Call lstReasonsList.Clear
                txtReasonDescription.Text = vbNullString
                Call SetEntryMode(enemSKU)
                If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then Call txtSKU.SetFocus
            Case (encmTransactionDiscount)
                Call EnableAction(enacTender, True)
                Call EnableAction(enacOrder, Not mblnCustomerOrder Or Not DisableTrainingModeOrders)
                Call EnableAction(enacQuantity, True)
                Call EnableAction(enacOverride, True)
                cmdViewLines.Enabled = True
                'Call EnableAction(enacLookUp, True)
                Call EnableAction(enacVoid, True)
                lstReasonsList.Col = 0
                lstReasonsList.ColHide = True
                fraReasonsList.Visible = False
                Call lstReasonsList.Clear
                txtReasonDescription.Text = vbNullString
                mlngCurrentMode = encmRecord
                Call SetEntryMode(enemSKU)
                If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then Call txtSKU.SetFocus
            Case (encmRefundPriceOverride)
                mlngCurrentMode = encmOverrideCode
                fraReasonsList.Visible = False
                Call SetEntryMode(enemPrice)
                
        End Select
    End If

    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("lstReasonsList_KeyPress", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub PerformRefund()
                
    On Error GoTo HandleException
    
    If LenB(mstrName) = 0 Then
        mlngCurrentMode = encmCustomer
        frmCustomerAddress.ShowKeyPad = Not (sbStatus.Panels("KeyBoard").Picture Is Nothing)
        Call frmCustomerAddress.GetRefundAddress(mstrName, mstrPostCode, mstrAddress, mstrTelNo, mstrMobileNo, mstrWorkTelNo, mstrEmail)
        If frmCustomerAddress.Cancel Then
            If (moTranHeader.TransactionCode = TT_SALE) And (mblnCustDone = False) Then
                mdblLineQuantity = 1
                mlngCurrentMode = encmRecord
                If txtSKU.Visible Then
                    SetEntryMode enemSKU
                ElseIf uctntPrice.Visible Then
                    SetEntryMode enemPrice
                Else
                    Call SetEntryMode(enemSKU)
                End If
            End If
            Exit Sub
        End If
    End If
    mlngCurrentMode = encmRefund
    Call TransactionLookUp
    Call EnableAction(enacCreateOrder, False)
    Call EnableAction(enacRecallQuote, False)
    Call EnableAction(enacSaveQuote, False)
'    Set oSelectedRF = mcolRefundCodes(lngSelectPos)
'    mstrRefundReason = oSelectedRF.Code

    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("PerformRefund", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub ConfirmCancelOrder(strDesc As String)

Dim lngKeyIn    As Long
Dim oDepositBO  As cDeposit

    On Error GoTo HandleException
    
    lngKeyIn = MsgBoxEx("Confirm Reason - " & strDesc, vbYesNo, "Order Cancellation", , , , , _
                        RGBMsgBox_WarnColour)
    If lngKeyIn = vbNo Then
    Else
        Set oDepositBO = goDatabase.CreateBusinessObject(CLASSID_DEPOSIT)
        Call oDepositBO.AddLoadFilter(CMP_EQUAL, FID_DEPOSIT_OrderNo, lblOrderNo.Caption)
        Call oDepositBO.AddLoadFilter(CMP_EQUAL, FID_DEPOSIT_TransactionMoveCode, "T")
        Call oDepositBO.AddLoadFilter(CMP_EQUAL, FID_DEPOSIT_Refunded, False)
        Call oDepositBO.LoadMatches
        If LenB(oDepositBO.DepositNo) <> 0 Then
                With oDepositBO
                lngKeyIn = MsgBoxEx("Do you want to refund outstanding deposit of " & _
                                    Format$(.DepositAmount - .DepositUsedAmount, "0.00") & "?", _
                                    vbYesNo, "Confirm Order Cancellation", , , , , _
                                    RGBMsgBox_WarnColour)
                    If lngKeyIn = vbNo Then
                    Else
                        lngKeyIn = MsgBoxEx("Do you want to refund :" & vbCrLf & vbTab & _
                                            "Deposit No: " & .DepositNo & vbCrLf & vbTab & _
                                            "Cust Name : " & .CustomerName & vbTab & _
                                            Format$(.DepositAmount - .DepositUsedAmount, "0.00") & _
                                            vbCrLf, vbOKOnly, _
                                            "Confirm Order Cancellation-Refund Deposit", , , , , _
                                            RGBMsgBox_WarnColour)
                    End If
            End With
            sprdTotal.Row = 3
            sprdTotal.Col = COL_TOTQTY
            sprdTotal.Text = vbNullString
            sprdTotal.Col = COL_TOTTYPE
            sprdTotal.Text = "CANCEL ORDER"
            sprdTotal.Col = COL_TOTVAL
            sprdTotal.Text = oDepositBO.DepositAmount - oDepositBO.DepositUsedAmount
            sprdTotal.Col = COL_TOTVAL - 1
            sprdTotal.Row = 1
            sprdTotal.Text = "Deposit Refund"
            For lngKeyIn = 1 To mcolTOSOpt.Count Step 1
                If mcolTOSOpt(CLng(lngKeyIn)).TypeOfSaleCode = TT_MISCOUT Then
                    Set moTOSType = mcolTOSOpt(CLng(lngKeyIn))
                    moTranHeader.TransactionCode = moTOSType.Code
                    Exit For
                End If
            Next lngKeyIn
            Call RecordTender(False, False, False)
        End If
        
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("ConfirmCancelOrder", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)


End Sub

Private Sub frmTender_Duress()
    OpenDrawerUnderDuress
End Sub

Private Sub Picture1_Click()

Debug.Print ActiveControl.Name

Call modTill.SendKey(49)
Call modTill.SendKey(49)
Call modTill.SendKey(48)
Call modTill.SendKey(48)
Call modTill.SendKey(49)
Call modTill.SendKey(50)
Call modTill.SendKey(13)

End Sub

Private Sub sprdLines_GotFocus()
    cmdViewLines.Caption = "R&eturn"
    sprdLines.SelBackColor = vbYellow
    sprdLines.SelForeColor = vbBlack
End Sub

Private Sub sprdLines_LostFocus()
    sprdLines.SelBackColor = vbBlack
    sprdLines.SelForeColor = vbWhite
End Sub

Private Sub txtPassword_GotFocus()
    
    txtPassword.SelStart = 0
    txtPassword.SelLength = Len(txtPassword.Text)
    
    If (goSession.ISession_IsOnline) Then
        txtPassword.BackColor = Me.BackColor
    Else
        txtPassword.BackColor = RGBEdit_Colour
    End If

End Sub

Private Function CheckPickupProcess(UserID As String, procName As String, paramName As String) As Boolean
    Dim oConnection As Connection
    Dim objCommand As ADODB.Command
    
    Set oConnection = goDatabase.Connection
    Set objCommand = New ADODB.Command
    
    With objCommand
        .ActiveConnection = oConnection
        .CommandType = adCmdStoredProc
        .CommandText = procName
        .Prepared = True
        .Parameters.Append .CreateParameter("ownerID", adVarChar, adParamInput, 3, UserID)
        .Parameters.Append .CreateParameter(paramName, adBoolean, adParamOutput)
        .Execute , , adExecuteNoRecords
   End With
    
   CheckPickupProcess = objCommand.Parameters(paramName).Value
    
End Function

Private Sub txtPassword_KeyPress(KeyAscii As Integer)

Const PRM_CHECK_EXPIRED As Long = 106

Dim strFailedMsg    As String
Dim blnCheckExpired As Boolean
Dim strPassword     As String
Dim blnOKToProceed  As Boolean
Dim blnCheckPickupProcessStart As Boolean
Dim blnCheckPickupProcessEnd As Boolean
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Screen.MousePointer = vbHourglass
        blnCheckExpired = goSession.GetParameter(PRM_CHECK_EXPIRED)
        If (Not moUser Is Nothing) And (LenB(lblFullName.Caption) <> 0) Then
            'pad password to 5 characters
            strPassword = txtPassword.Text
            If Len(strPassword) < 5 Then
                strPassword = Right$("00000" & strPassword, 5)
            End If
            
            If mcolTOSOpt Is Nothing Then
                If LoadTypesOfSales = False Then
                    Call MsgBoxEx("Sign On/Off NOT available", vbExclamation, _
                                  "System Configuration", , , , , RGBMsgBox_WarnColour)
                    Screen.MousePointer = vbNormal
                    If (txtUserID.Visible = True) And (txtUserID.Enabled = True) Then Call txtUserID.SetFocus
                    Exit Sub
                End If
            End If
            
            If mcolTOSOpt.Count = 0 Then
                Call MsgBoxEx("No valid Transaction Types", vbExclamation, _
                              "System Configuration", , , , , RGBMsgBox_WarnColour)
                Screen.MousePointer = vbNormal
                If (txtUserID.Visible = True) And (txtUserID.Enabled = True) Then Call txtUserID.SetFocus
                Exit Sub
            End If
            
            If GetSaleRefundIndexes(mcolTOSOpt) = False Then
                Call MsgBoxEx("Either ""sale"" or ""refund"" Transaction Type is missing", _
                              vbExclamation, "System Configuration", , , , , RGBMsgBox_WarnColour)
                Screen.MousePointer = vbNormal
                If (txtUserID.Visible = True) And (txtUserID.Enabled = True) Then Call txtUserID.SetFocus
                Exit Sub
            End If
            
            Set moRefPriceOverride = GetRefundPriceOverrideCode
            If moRefPriceOverride Is Nothing Then
                Call MsgBoxEx("Refund Price-Override code is missing", vbExclamation, _
                              "System Configuration", , , , , RGBMsgBox_WarnColour)
                Screen.MousePointer = vbNormal
                If (txtUserID.Visible = True) And (txtUserID.Enabled = True) Then Call txtUserID.SetFocus
                Exit Sub
            End If
            
            If Not EnsureStockAdjustmentCode08 Then
                Call MsgBoxEx("Stock Adjustment code for Parked Transactions(08) missing", _
                              vbExclamation, "System Configuration", , , , , RGBMsgBox_WarnColour)
                Screen.MousePointer = vbNormal
                If (txtUserID.Visible = True) And (txtUserID.Enabled = True) Then Call txtUserID.SetFocus
                Exit Sub
            End If
            
            If Not EnsureRefundCode05 Then
                Call MsgBoxEx("Refund code for Faulty items missing", vbExclamation, _
                              "System Configuration", , , , , RGBMsgBox_WarnColour)
                Screen.MousePointer = vbNormal
                If (txtUserID.Visible = True) And (txtUserID.Enabled = True) Then Call txtUserID.SetFocus
                Exit Sub
            End If
    
            If Not EnsureTenderOverrides Then
                Call MsgBoxEx("No valid tender overrides", vbExclamation, _
                              "System Configuration", , , , , RGBMsgBox_WarnColour)
                Screen.MousePointer = vbNormal
                If (txtUserID.Visible = True) And (txtUserID.Enabled = True) Then Call txtUserID.SetFocus
                Exit Sub
            End If
    
            
            If strPassword = moUser.Password Then
                If (moUser.PasswordValidTill < Now()) And (blnCheckExpired = True) Then
                    txtPassword.Text = vbNullString
                    Call MsgBoxEx("Password has expired for user '" & moUser.EmployeeID & "-" & _
                                  moUser.FullName & "'", vbInformation, "Password Expired", , , , , _
                                  RGBMsgBox_WarnColour)
                    Screen.MousePointer = vbNormal
                    If (txtUserID.Visible = True) And (txtUserID.Enabled = True) Then Call txtUserID.SetFocus
                    Exit Sub
                End If 'check that password has not expired
                
                ' Checking starting the Pickup process
                blnCheckPickupProcessStart = CheckPickupProcess(txtUserID.Text, "dbo.CheckPickupProcessStart", "pickupProcessStart")
                
                If blnCheckPickupProcessStart = True Then
                    strFailedMsg = "The Pickup process is running. You can't login to the Till."
                    
                    Call MsgBoxEx(strFailedMsg, vbExclamation, Me.Caption, , , , , RGBMsgBox_WarnColour)
                    Screen.MousePointer = vbNormal
                    If (txtUserID.Visible = True) And (txtUserID.Enabled = True) Then Call txtUserID.SetFocus
                    txtUserID.Text = ""
                    txtPassword.Text = ""
                    Exit Sub
                End If
                
                ' Checking completing the Pickup process
                blnCheckPickupProcessEnd = CheckPickupProcess(txtUserID.Text, "dbo.CheckPickupProcessEnd", "pickupProcessEnd")
                
                If blnCheckPickupProcessEnd = True Then
                    strFailedMsg = "End of Day Pick Up has been completed for this user." & vbCrLf & _
                    "If you continue to log into the Till it will corrupt your banking figures."
                    
                    If MsgBoxEx(strFailedMsg, vbYesNo, Me.Caption, , , , , RGBMsgBox_WarnColour) = vbNo Then
                        Call txtUserID.SetFocus
                        txtUserID.Text = ""
                        txtPassword.Text = ""
                        Exit Sub
                    End If
                End If
                
                ' Initialise Cashier Totals
                Set moCashierTotals = goDatabase.CreateBusinessObject(CLASSID_CASHIERTOTALS)
                Call moCashierTotals.AddLoadFilter(CMP_EQUAL, FID_CASHIERTOTALS_CashierNumber, _
                                                   moUser.EmployeeID)
                If moCashierTotals.LoadMatches.Count < 1 Then
                    
                    ' First Time Running
                    If strPassword <> moUser.Password Then
                        txtPassword.Text = vbNullString
                        strFailedMsg = "Invalid user name or password entered"
                        Call MsgBoxEx(strFailedMsg, vbExclamation, Me.Caption, , , , , _
                                      RGBMsgBox_WarnColour)
                    End If
                    
                    moCashierTotals.CashierNumber = moUser.EmployeeID
                    If moUser.EmployeeID <= moRetopt.MaxValidCashierNumber Then
                        moCashierTotals.CashierName = moUser.FullName
                    Else
                        moCashierTotals.CashierName = "* * Training Cashier"
                    End If
                    moCashierTotals.SecurityCode = moUser.Password
                    moCashierTotals.CurrentTillNumber = mstrTillNo

                    If moRetopt.AccountabilityType = "C" Then
                        Set moSaleTotals = goDatabase.CreateBusinessObject(CLASSID_SALETOTALS)
                        moSaleTotals.FloatAmount = moUser.FloatAmount
                        moSaleTotals.SaveIfNew
                        
                        moCashierTotals.SaleTotalsKey = moSaleTotals.SaleTotalKey
                    End If
                    moCashierTotals.SaveIfNew
                Else
                    If Val(moCashierTotals.CurrentTillNumber) <> Val(mstrTillNo) And _
                       Val(moCashierTotals.CurrentTillNumber) <> 99 Then
                        Call MsgBoxEx("Please Sign Off Till " & moCashierTotals.CurrentTillNumber, _
                                      vbOKOnly, Me.Caption, , , , , RGBMsgBox_WarnColour)
                        Screen.MousePointer = vbNormal
                        If (txtUserID.Visible = True) And (txtUserID.Enabled = True) Then Call txtUserID.SetFocus
                        Exit Sub
                    Else
                        If strPassword <> moCashierTotals.SecurityCode Then
                            strFailedMsg = "Invalid user name or password entered"
                            Call MsgBoxEx(strFailedMsg, vbExclamation, Me.Caption, , , , , _
                                          RGBMsgBox_WarnColour)
                            Screen.MousePointer = vbNormal
                            If (txtUserID.Visible = True) And (txtUserID.Enabled = True) Then Call txtUserID.SetFocus
                            Exit Sub
                        End If
                                           
                        moCashierTotals.CurrentTillNumber = mstrTillNo
                        moCashierTotals.IBo_SaveIfExists

                        If moRetopt.AccountabilityType = "C" Then
                            Set moSaleTotals = goDatabase.CreateBusinessObject(CLASSID_SALETOTALS)
                            Call moSaleTotals.AddLoadFilter(CMP_EQUAL, FID_SALETOTALS_SaleTotalKey, _
                                                            moCashierTotals.SaleTotalsKey)
                            moSaleTotals.LoadMatches
                        End If
                    End If
                End If
                
                If mblnTraining Then
                    Load frmVerifyPwd
                    blnOKToProceed = frmVerifyPwd.VerifySupervisorPassword(ucKeyPad1.Visible)
                    Unload frmVerifyPwd
                    If Not blnOKToProceed Then
                        strFailedMsg = "Authorisation not verified"
                        Call MsgBoxEx(strFailedMsg, vbExclamation, Me.Caption, , , , , _
                                      RGBMsgBox_WarnColour)
                        Screen.MousePointer = vbNormal
                        If (txtUserID.Visible = True) And (txtUserID.Enabled = True) Then Call txtUserID.SetFocus
                        Exit Sub
                    End If
                End If

                If mblnPrePrintLogo = True Then Call PrintHeader(OPOSPrinter1)
                lblPasswordlbl.Visible = False
                txtPassword.Visible = False
                txtUserID.Visible = False
                lblLoggedUserID.Visible = True
                lblLoggedUserID.Caption = txtUserID.Text
                sbStatus.Panels(PANEL_INFO + 3).Text = txtUserID.Text & "-" & lblFullName.Caption
                Call UserSignedOn(moUser, mstrTillNo, mstrTranId)
'                Call moUser.SignedOn(goSession.CurrentEnterprise.IEnterprise_WorkstationID, strTranID)
                
                If Val(mstrTranId) = 0 Then
                    Call MsgBoxEx("Invalid Transaction ID supplied by Transaction Audit Log" & _
                                  vbCrLf & "(Ensure Transaction Number file available)" & _
                                  vbCrLf & vbCrLf & "EXITING SYSTEM", vbExclamation + vbOKOnly, _
                                  "Invalid Transaction ID", , , , , RGBMsgBox_WarnColour)
                    Unload Me
                    End
                End If
                mlngTranTypeRowNo = 0
                Call ShowTranTypeRow
                Call SetCurrentMode(enCurrentMode.encmTranType)
            Else
                txtPassword.Text = vbNullString
                strFailedMsg = "Invalid user name or password entered"
                Call MsgBoxEx(strFailedMsg, vbExclamation, Me.Caption, , , , , _
                              RGBMsgBox_WarnColour)
            End If
        End If 'verify current user's password
    End If 'enter pressed so validate password
    Screen.MousePointer = vbNormal

End Sub
Private Function UserSignedOn(ByVal oUser As cUser, ByVal strTillID As String, _
                              ByRef strTransactionNo As String) As Boolean

Dim oZread    As cZReads
Dim colZreads As Collection
Dim strVers   As String

    If Not NextTransactionOK Then
        Load frmVerifyPwd
        Do
            Call MsgBoxEx("Duplicate transaction detected" & vbNewLine & vbNewLine & _
                          "Contact CTS Support Desk", vbOKOnly + vbExclamation, _
                          "Duplicate Transaction", , , , , RGBMsgBox_WarnColour)
        Loop While frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) = False
        Unload frmVerifyPwd
        Unload Me
        End
    End If
   
    'Check if Till Version has been saved yet and if not - extract version number
    If (mblnSavedVersions = False) Then
        mblnSavedVersions = True
        strVers = App.Major & "." & App.Minor & "." & Format(App.Revision, "000")
    End If
    Set moTranHeader = goSession.Database.CreateBusinessObject(CLASSID_POSHEADER)
    Call moTranHeader.SaveSignOn(strTillID, oUser.EmployeeID, strVers)
    
    On Error Resume Next
    ' Tran is reserved sql word, use "Tran" to identify as a column
    Call goDatabase.ExecuteCommand("UPDATE DLTOTS SET RTIHOLD=1 WHERE DATE1='" & Format(Date, "YYYY-MM-DD") & "' AND TILL='" & moTranHeader.TillID & "' AND ""Tran""='" & moTranHeader.TransactionNo & "'")
    Call Err.Clear
    On Error GoTo 0
    
    strTransactionNo = moTranHeader.TransactionNo
    lblTranNo.Caption = strTransactionNo
    Call BackUpTransaction
    
    With moTillTotals
        .CashierNumber = moUser.EmployeeID
        .CurrentTransactionNumber = moTranHeader.TransactionNo
        .TillZReadDone = False
        .TillCheckingEvents = False
        .SaveIfExists
    End With
    
    With moSaleTotals
        .FloatAmount = moCashier.DefaultFloatAmount
        .SaveIfExists
    End With
    
    For Each moTOSType In mcolTOSOpt
        If moTOSType.Code = TT_SIGNON Then
            If mblnUseCashDrawer And Not mblnTraining Then
                If moTOSType.OpenOnZeroTran Then
                    InitialiseCashDrawer
                    OPOSCashDrawer.OpenDrawer
                    Call Wait(2)
                    CloseCashDrawer
                End If
            End If
            Exit For
        End If
    Next
    
    Set oZread = goDatabase.CreateBusinessObject(CLASSID_ZREADS)
    oZread.ClearLoadFilter
    
    Call oZread.AddLoadFilter(CMP_EQUAL, FID_ZREADS_TillID, mstrTillNo)
    Set colZreads = oZread.LoadMatches
    
    If Not ZReadRecordExists(colZreads, "CA", txtUserID.Text) Then
        Call ZReadAdd("CA", txtUserID.Text)
    End If
    Call ZReadUpdate("TR", TT_SIGNON)
    Call UpdateFlashTotals(False, enftSignon, 0)
    
    Set moTranHeader = Nothing
    UserSignedOn = True
    
End Function

Private Function UserSignedOff(ByVal oUser As cUser, ByVal strTillID As String, _
                               ByRef strTransactionNo As String) As Boolean
                               
Dim oUserBO As cUser

    If Not NextTransactionOK Then
        Load frmVerifyPwd
        Do
            Call MsgBoxEx("Duplicate transaction detected" & vbNewLine & vbNewLine & _
                          "Contact CTS Support Desk", vbOKOnly + vbExclamation, _
                          "Duplicate Transaction", , , , , RGBMsgBox_WarnColour)
        Loop While frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) = False
        Unload frmVerifyPwd
        Unload Me
        End
    End If
   
    Set moTranHeader = goSession.Database.CreateBusinessObject(CLASSID_POSHEADER)
    Call moTranHeader.SaveSignOff(strTillID, oUser.EmployeeID)
    
    On Error Resume Next
   ' Tran is reserved sql word, use "Tran" to identify as a column
    Call goDatabase.ExecuteCommand("UPDATE DLTOTS SET RTIHOLD=1 WHERE DATE1='" & Format(Date, "YYYY-MM-DD") & "' AND TILL='" & moTranHeader.TillID & "' AND ""Tran""='" & moTranHeader.TransactionNo & "'")
    Call Err.Clear
    On Error GoTo 0
    
    strTransactionNo = moTranHeader.TransactionNo
    lblTranNo.Caption = strTransactionNo
    Call BackUpTransaction
    
    With moCashierTotals
        'Added 20/9/07 to refresh user password to avoid changes by other system being missed
        Set oUser = goDatabase.CreateBusinessObject(CLASSID_USER)
        Call oUser.IBo_AddLoadFilter(CMP_EQUAL, FID_USER_EmployeeID, moUser.EmployeeID)
        Call oUser.LoadMatches
        .SecurityCode = oUser.Password
        .CurrentTillNumber = "99"
        .SaveIfExists
    End With
    
    With moTillTotals
        .PreviousCashierNumber = .CashierNumber
        .CashierNumber = "999"
        .CurrentTransactionNumber = strTransactionNo
        .TillCheckingEvents = False
        .TimeLastSignedOff = Format$(Now, "HHMM")
        .SaveIfExists
    End With
    
    For Each moTOSType In mcolTOSOpt
        If moTOSType.Code = TT_SIGNOFF Then
            If mblnUseCashDrawer And Not mblnTraining Then
                If moTOSType.OpenOnZeroTran Then
                    InitialiseCashDrawer
                    OPOSCashDrawer.OpenDrawer
                    Call Wait(2)
                    CloseCashDrawer
                End If
            End If
            Exit For
        End If
    Next
    
    Call ZReadUpdate("TR", TT_SIGNOFF)
    Call UpdateFlashTotals(False, enftSignOff, 0)

    Set moTranHeader = Nothing
    UserSignedOff = True

End Function

Private Sub txtPassword_KeyUp(KeyCode As Integer, Shift As Integer)
    
    'Check if new keypressed makes up full password, so auto logon
'    If (Len(txtPassword.Text) = 5) And (Len(mstrOldPwd) = 4) Then
'        KeyCode = 0
'        Call txtPassword_KeyPress(vbKeyReturn)
'    End If
'    mstrOldPwd = txtPassword.Text

End Sub

Private Sub txtPassword_LostFocus()
    
    txtPassword.BackColor = RGB_WHITE

End Sub

Private Sub txtReasonDescription_KeyPress(KeyAscii As Integer)

Dim strTraderNumber As String
Dim strTraderName   As String
Dim dblTranTotal        As Double
    
    On Error GoTo HandleException
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        fraReasonsList.Visible = False
        Call SetEntryMode(enemNone)
        
        Select Case (mlngCurrentMode)
            
           'Case (encmtransactiondiscount): Call RecordTender
            Case (encmPaidOut): moTranHeader.Description = txtReasonDescription.Text
                                Call RecordTender(False, False, False)
            Case (encmPaidIn):  moTranHeader.Description = txtReasonDescription.Text
            Case (encmTransactionDiscount)
                sprdLines.Text = txtReasonDescription.Text
                sprdTotal.Row = ROW_TOTTOTALS
                sprdTotal.Col = COL_TOTVAL
                dblTranTotal = sprdTotal.Value
                mdblTotalBeforeDis = sprdTotal.Value
                sprdLines.Col = COL_INCTOTAL
                mdblPercentageValue = dblTranTotal * txtReasonDescription.Text / 100
                sprdLines.Text = "-" & mdblPercentageValue
                fraReasonsList.Visible = False
                DoEvents
                Call SetGridTotals(sprdLines.Row)
                sprdLines.Col = COL_QTY
                sprdLines.Text = vbNullString
                mblnTransactionDiscountApplied = True
                Call UpdateTotals
                Call SetEntryMode(enemNone)
                Call RecordTender(False, False, True)
            Case (encmOrderAction):
                Call ConfirmCancelOrder(txtReasonDescription.Text)
        End Select
    End If
    
    If (KeyAscii = vbKeyEscape) Then
        fraReasonsList.Height = lstReasonsList.Top + lstReasonsList.Height + 240
        If (lstReasonsList.Visible = True) And (lstReasonsList.Enabled = True) Then lstReasonsList.SetFocus
    End If

    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("txtReasonDescription_KeyPress", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub txtUserID_Change()

    lblFullName.Caption = vbNullString
    lblInitials.Caption = vbNullString
    txtPassword.Text = vbNullString
    txtPassword.Enabled = False
    fraTranType.Visible = False
    lblTrainingMode.Visible = False
    mblnTraining = False
    
End Sub

Private Sub txtUserID_GotFocus()

    txtUserID.SelStart = 0
    txtUserID.SelLength = Len(txtUserID.Text)
    
    If (goSession.ISession_IsOnline) Then
        txtUserID.BackColor = Me.BackColor
    Else
        txtUserID.BackColor = RGBEdit_Colour
    End If

End Sub

Private Sub txtUserID_KeyPress(KeyAscii As Integer)

Const PROCEDURE_NAME As String = MODULE_NAME & ".txtUserID_KeyPress"

Dim blnLoggedIn As Boolean

On Error GoTo HandleException

Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, KeyAscii)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        If Len(txtUserID.Text) <> 3 Then
            txtUserID.Text = Left$("000", 3 - Len(txtUserID.Text)) & txtUserID.Text
        End If
        Set moUser = goDatabase.CreateBusinessObject(CLASSID_USER)
        Call moUser.AddLoadFilter(CMP_EQUAL, FID_USER_EmployeeID, txtUserID.Text)
        Call moUser.AddLoadFilter(CMP_EQUAL, FID_USER_Deleted, False)
        Call moUser.AddLoadField(FID_USER_Initials)
        Call moUser.AddLoadField(FID_USER_FullName)
        Call moUser.AddLoadField(FID_USER_Password)
        Call moUser.AddLoadField(FID_USER_PasswordValidTill)
        Call moUser.AddLoadField(FID_USER_AuthorisationCode)
        Call moUser.AddLoadField(FID_USER_AuthorisationValidTill)
        Call moUser.AddLoadField(FID_USER_Manager)
        Call moUser.AddLoadField(FID_USER_Supervisor)
        If LenB(moUser.EmployeeID) = 0 Then moUser.EmployeeID = "***" 'force invalid lookup
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "UserID Validated")
        Call moUser.LoadMatches
        If LenB(moUser.FullName) <> 0 Then
            
            Set moCashier = goDatabase.CreateBusinessObject(CLASSID_CASHIER)
            Call moCashier.AddLoadFilter(CMP_EQUAL, FID_CASHIER_CashierNumber, moUser.EmployeeID)
            Call moCashier.LoadMatches
            
            lblInitials.Caption = moUser.Initials
            lblFullName.Caption = moCashier.Name
            txtPassword.Enabled = True
            txtPassword.Visible = True
            lblPasswordlbl.Visible = True
            If (txtPassword.Visible = True) And (txtPassword.Enabled = True) Then Call txtPassword.SetFocus
            ' Commidea Into WSS.
            ' Commidea loading was taking so long, user could type in before retrieved last cashier value
            ' related to referral 474
            If (Val(txtUserID.Text) >= Val(mstrMaxValidCashier) And mstrMaxValidCashier <> "") Then
                lblTrainingMode.Visible = True
                mblnTraining = True
            End If
        Else
            txtPassword.Text = vbNullString
            txtPassword.Enabled = False
            lblInitials.Caption = vbNullString
            lblFullName.Caption = "INVALID ID"
            If (txtUserID.Visible = True) And (txtUserID.Enabled = True) Then Call txtUserID.SetFocus
            txtUserID.SelStart = 0
            txtUserID.SelLength = Len(txtUserID.Text)
            Set moUser = Nothing
        End If
        Screen.MousePointer = vbNormal
    End If 'enter pressed so check User ID is valid

    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("txtUserID_KeyPress", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub DebugMsgSprdLinesValue(procedureName As String)
    Dim lngLineNo       As Long
    
    For lngLineNo = 2 To sprdLines.MaxRows Step 1
        Call DebugMsgSprdLineValue(lngLineNo, procedureName)
    Next lngLineNo
End Sub

Private Sub DebugMsgSprdLineValue(lngLineNo As Long, procedureName As String)
    Dim strMessage As String
    
    strMessage = "SKU - " & GetTextsprdLine(COL_PARTCODE, lngLineNo)
    strMessage = strMessage & " Description - " & GetTextsprdLine(COL_DESC, lngLineNo)
    strMessage = strMessage & " Quantity - " & GetTextsprdLine(COL_QTY, lngLineNo)
    strMessage = strMessage & " Price - " & GetTextsprdLine(COL_SELLPRICE, lngLineNo)
    strMessage = strMessage & " Total - " & GetTextsprdLine(COL_INCTOTAL, lngLineNo)
    
    Call DebugMsg(MODULE_NAME, procedureName, endlDebug, strMessage)
End Sub

Private Function GetTextsprdLine(Col As Long, Row As Long) As Variant
Dim res As Boolean
Dim varColValue As Variant

    res = sprdLines.GetText(Col, Row, varColValue)
    GetTextsprdLine = varColValue
End Function

Private Sub RecordTender(blnDiscountsApplied As Boolean, blnDiscountCardApplied As Boolean, blnDoPrompts As Boolean, Optional ByVal flSignatureVerification As Boolean = True)

Dim oTenderOpts     As cTenderOptions
Dim oTenderCtl      As cTenderControl
Dim oTOSTenders     As cTypesOfSaleTenders
Dim oPOSPayment     As cPOSPayment
Dim lngEntryNo      As Long
Dim lngBtnNo        As Long
Dim lngLeftPos      As Long
Dim colTenders      As Collection
Dim colEvents       As Collection
Dim colHSEvents     As Collection
Dim colTOSTends     As Collection
Dim colGetCoupons   As Collection

Dim rsTOSTends      As Recordset
Dim rsTenders       As Recordset

Dim strTranCode     As String
Dim strCardNo       As String
Dim strLocation     As String
Dim lngSaveMode     As Long
Dim curRefTotal     As Currency
Dim curCoDiscTot    As Currency
Dim curCollTot      As Currency
Dim curDiscTot      As Currency
Dim curTranTot      As Currency
Dim curVoucherTot   As Currency 'used to total up vouchers to exclude from Coll Disc
Dim curRefundChange As Currency 'WIX2008-027
Dim intTokenCount   As Integer
Dim oRecPrinting    As clsReceiptPrinting
Dim blnSignatureOK  As Boolean
Dim vntAddress      As Variant
Dim curPayAmount    As Currency
Dim strPreCode      As String 'Wix1165 to save PO Code
Dim colReturnCoupon As Collection 'WIX1165 - coupons not used and to be returned
Dim colSKUs         As Collection 'List of SKUs for checking against Missing SKU prompt
Dim blnFoundItem    As Boolean

Dim strOrigStoreNo  As String
Dim strOrigTillID   As String
Dim strOrigTranNo   As String
Dim dteOrigTranDate As Date
   
Dim oPrintOut      As clsReceiptPrinting
Dim lngKeyIn       As Long
    
Dim intQty          As Integer
Dim strPartCode     As String
Dim lngLineNo       As Long
Dim strMsg          As String
Dim CheckForCNP As Boolean  ' Commidea Into WSS - Referral 475
Dim strAnotherTenderTypeNew As String
Dim strAnotherTenderTypeOld As String
Dim strProcedureName As String

strProcedureName = "RecordTender"

mblnCreditCardUsedInTender = False

    On Error GoTo HandleException
    
    'WIX1389 - check if any Item Prompts and if the cancel transaction
    If (blnDoPrompts = True) Then
        If (ProcessEndOfTranPrompts(False) = False) Then
            mstrOrderQuit = "Y"
            mlngCurrentMode = encmTender
            Call SetEntryMode(enemNone)
            Call Form_KeyPress(vbKeyEscape)
            Exit Sub
        End If
    End If
    
    If mblnOrderBtnPressed = True And mblnCreatingOrder = False Then
        If Not Abs(RefundTotal) > 0 Then
            strMsg = goSession.GetParameter(PRM_TAKE_NOW_MSG1)
            mstrOrderQuit = "N"
            If MsgBoxEx(strMsg, vbYesNo + vbDefaultButton1, "Tender Transaction", , , , , RGBMsgBox_WarnColour) = vbYes Then
                mstrOrderQuit = "Y"
                mlngCurrentMode = encmTender
                Call SetEntryMode(enemNone)
                Call Form_KeyPress(vbKeyEscape)
                Call CreateOrder(False, False, False)
                Exit Sub
            End If
        End If
                ' Referral TBA - Live store issue, cancelling order not clearing variable meaning that totals were calculated incorrectly
                blnDiscountsApplied = False
                mblnSaveNewOrder = False
            End If

    Set colReturnCoupon = New Collection
    
    sprdTotal.Row = ROW_TOTTOTALS
    sprdTotal.Col = COL_TOTVAL
    
    If Val(sprdTotal.Text) = 0 Then
        'MO'C WIX1165 - Check if zero value then assume coupon so check at least 1 item exists
        blnFoundItem = False
        For lngLineNo = 2 To sprdLines.MaxRows Step 1
            Call DebugMsg(MODULE_NAME, strProcedureName, endlDebug, "Checking Line" & lngLineNo)
            Call DebugMsgSprdLineValue(lngLineNo, strProcedureName)
            sprdLines.Row = lngLineNo
            'Ensure only lines that are SKU's
            sprdLines.Col = COL_LINETYPE
            If (Val(sprdLines.Value) = LT_ITEM) Then
                sprdLines.Col = COL_VOIDED
                If (Val(sprdLines.Value) = 0) Then
                    blnFoundItem = True
                    Exit For
                End If 'voided
            End If 'line item to check
        Next lngLineNo
        If (blnFoundItem = False) Then
            Call MsgBoxEx("No Line items have been entered into Transaction" & vbCrLf & "Tender has been cancelled.", vbOKOnly, "Tender Transaction", , , , , RGBMsgBox_WarnColour)
            Call SetEntryMode(enemSKU)
            If (txtSKU.Visible = True) Then
                txtSKU.Text = ""
                If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then txtSKU.SetFocus
            End If
            Exit Sub
        End If
    End If
    'Check for any combinations that when the Total is requested, must be completed first
    'Now go and process the amount to tender
    'only show tender if an amount needs tendering
    sprdTotal.Row = ROW_TOTTOTALS
    sprdTotal.Col = COL_TOTVAL
    If (Abs(Val(sprdTotal.Value)) > 9999999.99) Then
        Call MsgBoxEx("Warning - Total Value exceeds system limits (above 9,999,999.99)" & _
            vbNewLine & "Unable to proceed and to Tender." & vbNewLine & "Split transaction into separate lines if required.", vbOKOnly + vbExclamation, _
            "Value Limit Exceeded", , , , , RGBMsgBox_WarnColour)
        Call SetEntryMode(enemSKU)
        If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then txtSKU.SetFocus
        Exit Sub
    End If
    If (mdblTotalBeforeDis = 0) Then mdblTotalBeforeDis = Val(sprdTotal.Value)
    moTranHeader.ColleagueCardNo = "000000000"
    moTranHeader.DiscountCardNumber = vbNullString
    
    mblnApplyCollDisc = False
    Set mcolEFTPayments = New Collection 'reset all EFT Authorised payments for collection

    If (Val(sprdTotal.Text) = 0) Then
        If sprdLines.MaxRows = 1 Then
            If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then
                txtSKU.SetFocus
            ElseIf (uctntPrice.Visible = True) And (uctntPrice.Enabled = True) Then
                uctntPrice.SetFocus
            End If
            Exit Sub
        End If
        
        If LastUnvoidedLine = 0 Then
            Call SetEntryMode(enemNone)
            If MsgBoxEx("All lines reversed" & vbNewLine & "Void transaction?", _
                        vbYesNo + vbQuestion, "Empty transaction", , , , , _
                        RGBQuery_BackColour) = vbYes Then
                mstrAccountName = vbNullString
                mstrAccountNum = vbNullString
                mstrName = ""
                mstrAddress = ""
                mstrPostCode = ""
                mstrTelNo = ""
                mstrMobileNo = ""
                mstrWorkTelNo = ""
                mstrEmail = ""
                Call SaveSale(False, True, False, False)
                mlngCurrentMode = encmComplete
                Call Form_KeyPress(vbKeyReturn)
            Else
                Call SetEntryMode(enemSKU)
                If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then txtSKU.SetFocus
            End If
            Exit Sub
        End If
        
    End If
    
    fraActions.Visible = False
    ucpbStatus.Visible = False
    Call SetEntryMode(enemNone)
    
    If (blnDiscountsApplied = False) Then
        'Added 16/4/09 -preserve Item Price incase Transaction Discount applied, which is reversed out
        Call DebugMsg(MODULE_NAME, strProcedureName, endlDebug, "Preserving Unit price")
        Call DebugMsgSprdLinesValue(strProcedureName)
        For lngLineNo = 2 To sprdLines.MaxRows Step 1
            sprdLines.Row = lngLineNo
            sprdLines.Col = COL_LINETYPE
            If (Val(sprdLines.Value) = LT_ITEM) Then
                sprdLines.Col = COL_SELLPRICE
                curPayAmount = Val(sprdLines.Value)
                sprdLines.Col = COL_PRETRAN_PRICE
                sprdLines.Value = curPayAmount
                sprdLines.Col = COL_DISCCODE
                strPreCode = sprdLines.Value
                sprdLines.Col = COL_PRETRAN_CODE
                sprdLines.Value = strPreCode
            End If
            sprdLines.Col = COL_LINETYPE
            If (Val(sprdLines.Value) = LT_COUPON) Then 'reset any returned coupons
                sprdLines.Col = COL_CPN_STATUS
                If (sprdLines.Value = "") Or (sprdLines.Value = "8") Then sprdLines.Value = "0"
            End If
        Next lngLineNo
        Call DebugMsg(MODULE_NAME, strProcedureName, endlDebug, "Saving Line " & lngLineNo)
        
        'Process any Offers and Promotions events
        sbStatus.Panels(PANEL_INFO).Text = "Processing Offers and Promotions"
        Call sbStatus.Refresh
        
        Call DebugMsg(MODULE_NAME, strProcedureName, endlTraceIn, "GetOPEvents " & mstrTranId)
        Set colEvents = moEvents.GetOPEvents(mcolOPSummary, mcolGetCoupons, colReturnCoupon)
        Call DebugMsg(MODULE_NAME, strProcedureName, endlTraceOut, "GetOPEvents " & mstrTranId)

        'Process any Hierachical Spend offers/Department Sales
        sbStatus.Panels(PANEL_INFO).Text = "Processing Discounts(HS)"
        Call sbStatus.Refresh
    
        Call DebugMsg(MODULE_NAME, strProcedureName, endlTraceIn, "CheckForHSDiscounts " & mstrTranId)
        If EnableGetCoupons Then
            Set colHSEvents = moEvents.CheckForHSDiscounts(mcolHSSummary, mcolHSGetCoupons, colReturnCoupon)
        Else
            Set colHSEvents = moEvents.CheckForHSDiscounts(mcolHSSummary, mcolGetCoupons, colReturnCoupon)
        End If
        If EnableBuyCoupons Then
            Set mcolUsedCoupons = moEvents.UsedCoupons
        End If
        
        sbStatus.Panels(PANEL_INFO).Text = "Processing Discounts(HS Processed)"
        If Not colHSEvents Is Nothing Then
            If colHSEvents.Count > 0 Then Call InsertEvents(colHSEvents, mcolHSSummary, sprdLines)
        End If
        Call DebugMsg(MODULE_NAME, strProcedureName, endlTraceOut, "CheckForHSDiscounts " & mstrTranId)
        
        If Not colEvents Is Nothing Then
            If colEvents.Count > 0 Then Call InsertEvents(colEvents, mcolOPSummary, sprdLines)
        End If
        
        'add any coupons onto Transaction to save
        For lngEntryNo = 1 To mcolGetCoupons.Count
            Call RetrieveCoupon(mcolGetCoupons(lngEntryNo), True)
        Next lngEntryNo
        
        If EnableGetCoupons Then
            'Needed to Print HS Get Coupons onto Transaction to save
            For lngEntryNo = 1 To mcolHSGetCoupons.Count
                Call RetrieveCoupon(mcolHSGetCoupons(lngEntryNo), True)
            Next lngEntryNo
        End If
        
        If Not EnableBuyCoupons Then
            'Flag all unused coupons as returned to customer
            For lngEntryNo = 1 To colReturnCoupon.Count
                Call MarkReturnCoupon(colReturnCoupon(lngEntryNo))
            Next lngEntryNo
        End If
       
        If EnableBuyCoupons Then
            Set mcolCouponsInTransaction = moEvents.CouponsUsedInTransaction 'PO14 Used in transaction Information
        End If
       
    End If 'Discount must be applied
    
    sbStatus.Panels(PANEL_INFO).Text = "Processing Colleague Discount"
    curRefTotal = Abs(RefundTotal)
    If (mlngCurrentMode = encmColleagueDiscount) And (blnDiscountCardApplied = False) Then
        sprdTotal.Col = COL_TOTVAL
        
        'Extract total value of vouchers
        curVoucherTot = VouchersTotal
        
        'Add the Colleague discount onto the end of the list
        sprdLines.MaxRows = sprdLines.MaxRows + 1
        sprdLines.Row = sprdLines.MaxRows + 1
        sprdLines.Col = 0
        sprdLines.Text = " " 'override auto column lettering
        sprdLines.Col = COL_DESC
        sprdLines.Text = "Colleague Discount"
        sprdLines.Col = COL_QTY
        sprdLines.CellType = CellTypeStaticText
        sprdLines.Text = "Less " & moRetopt.EmployeeDiscount & "%"
        sprdLines.Col = COL_LINETYPE
        sprdLines.Text = LT_SPACER
        sprdLines.Col = COL_INCTOTAL
        'curCollTot = Format((Val(sprdTotal.Value) + curRefTotal - curVoucherTot) * -moRetopt.EmployeeDiscount / 100, "0.00")
        curCollTot = ColleagueDiscTotal
        sprdLines.Col = COL_INCTOTAL
        sprdLines.Text = curCollTot
        SetGridTotals (sprdLines.MaxRows)
        UpdateTotals
        sprdTotal.Col = COL_TOTVAL
        
        'Now validate the user card
        If (LenB(strCardNo) = 0) Then
            'get out of tender mode and clear up any added lines
            mblnApplyCollDisc = False
            mlngCurrentMode = encmTender
            Call SetEntryMode(enemNone)
            Call Form_KeyPress(vbKeyEscape)
            Exit Sub
        Else
            mblnApplyCollDisc = True
            moTranHeader.ColleagueCardNo = strCardNo
            moTranHeader.StoreNumber = strLocation
        End If
    End If
    
    sbStatus.Panels(PANEL_INFO).Text = "Processing Discount Card Scheme"  'MO'C Added 07/06/07
    If (Not moDiscountCardScheme Is Nothing) And (blnDiscountCardApplied = False) Then  'MO'C Added 07/06/07 if we have a Dicount card scheme in place, has a discount card been swiped?
        sprdTotal.Col = COL_TOTVAL
        
        curTranTot = Val(sprdTotal.Value)
        moTranHeader.DiscountCardNumber = mstrDiscCardNo
        
        sprdTotal.Col = COL_TOTVAL
        
        'Extract total value of vouchers
        curVoucherTot = VouchersTotal
        
        'Add the discount onto the end of the list
        sprdLines.MaxRows = sprdLines.MaxRows + 1
        sprdLines.Row = sprdLines.MaxRows + 1
        sprdLines.Col = 0
        sprdLines.Text = " " 'override auto column lettering
        sprdLines.Col = COL_DESC
        sprdLines.Text = moDiscountCardScheme.SchemeName '"Discount Scheme"
        sprdLines.Col = COL_QTY
        sprdLines.CellType = CellTypeStaticText
        sprdLines.Text = "Less " & Format(moDiscountCardScheme.DiscountRate) & "%"
        sprdLines.Col = COL_LINETYPE
        sprdLines.Text = LT_SPACER
        sprdLines.Col = COL_INCTOTAL
        
        curDiscTot = DiscountTotal(moDiscountCardScheme.DiscountRate)
        sprdLines.Col = COL_INCTOTAL
        sprdLines.Text = curDiscTot
        SetGridTotals (sprdLines.MaxRows)
        UpdateTotals
        sprdTotal.Col = COL_TOTVAL
        
        moTranHeader.StoreNumber = mstrLocation
        
        If Left(mstrDiscCardNo, 1) = "9" Then
            mblnApplyCollDisc = True
        Else
            mblnApplyCollDisc = False
        End If
        
        If ((curTranTot + curDiscTot) >= moDiscountCardScheme.ManagerAuthLevel And _
            moDiscountCardScheme.ManagerAuthLevel <> 0) Then
        
            Load frmVerifyPwd
            If frmVerifyPwd.VerifyManagerPassword(False, "Confirm Discount") = False Then
                Unload frmVerifyPwd
                'get out of tender mode and clear up any added lines
                Set moDiscountCardScheme = Nothing
                mblnApplyDiscountScheme = False
                mstrDiscCardNo = vbNullString
                mlngCurrentMode = encmTender
                Call SetEntryMode(enemNone)
                Call Form_KeyPress(vbKeyEscape)
                Exit Sub
            End If
            moTranHeader.SupervisorUsed = True
            moTranHeader.RefundManager = frmVerifyPwd.txtAuthID.Text
            Unload frmVerifyPwd
        
        Else
       
            'Need to see if we require authorisation?
             If ((curTranTot + curDiscTot) >= moDiscountCardScheme.SupervisorAuthLevel And _
                 moDiscountCardScheme.SupervisorAuthLevel <> 0) Then
                 
                 Load frmVerifyPwd
                 If frmVerifyPwd.VerifySupervisorPassword(False, "Confirm Discount") = False Then
                     Unload frmVerifyPwd
                     'get out of tender mode and clear up any added lines
                     Set moDiscountCardScheme = Nothing
                     mblnApplyDiscountScheme = False
                     mlngCurrentMode = encmTender
                     Call SetEntryMode(enemNone)
                     Call Form_KeyPress(vbKeyEscape)
                     Exit Sub
                 End If
                 moTranHeader.SupervisorUsed = True
                 moTranHeader.DiscountSupervisor = frmVerifyPwd.txtAuthID.Text
                 Unload frmVerifyPwd
                 
             End If
        
        End If
        
        If moDiscountCardScheme.PrintSignatureSlip Then
            Set oPrintOut = New clsReceiptPrinting
            Set oPrintOut.Printer = frmTill.OPOSPrinter1
            Set oPrintOut.TranHeader = moTranHeader
            oPrintOut.TillNo = mstrTillNo
            oPrintOut.TranNo = mstrTranId
            
            Call oPrintOut.Init(goSession, goDatabase)
            
            curTranTot = Val(sprdTotal.Value)
            Call DebugMsg(MODULE_NAME, strProcedureName, endlDebug, "Print Siganture Slip for Discount Card: " & curTranTot)
            Call oPrintOut.PrintColleagueSignatureSlip(curTranTot, moTranHeader.TranDate, moTranHeader.TransactionTime, moTranHeader.TillID, moTranHeader.TransactionNo, frmTill.txtUserID, frmTill.lblFullName, moRetopt.StoreName, moDiscountCardScheme)
            lngKeyIn = MsgBoxEx("Confirm signature matches Card", vbYesNo, "Verify Signature", , , , , RGBMSGBox_PromptColour)
            
            If (lngKeyIn <> vbYes) Then
                If (Left(lblOrderNo.Caption, 1) = "Q") Then
                    lblOrderNo.Caption = ""
                    Call ClearEvents
                    mdblTotalBeforeDis = 0
                End If
                Call MsgBoxEx("Signature validation cancelled/stopped" & vbCrLf & "Cancelling Discount", vbOKOnly, "Card Validation Error", , , , , RGBMsgBox_WarnColour)
                Set moDiscountCardScheme = Nothing
                mblnApplyDiscountScheme = False
                mstrDiscCardNo = vbNullString
                mlngCurrentMode = encmTender
                Call SetEntryMode(enemNone)
                Call Form_KeyPress(vbKeyEscape)
                Exit Sub
            End If
        End If
    End If
    
    sbStatus.Panels(PANEL_INFO).Text = "Setting Transaction Code"
    sprdTotal.Col = COL_TOTVAL
    If moTranHeader.TransactionCode = TT_SALE Then
        If sprdTotal.Text < 0 Then
            mlngTranSign = -1
            moTranHeader.TransactionCode = TT_REFUND
            For Each moTOSType In mcolTOSOpt
                If moTOSType.Code = TT_REFUND Then Exit For
            Next moTOSType
        End If
    ElseIf moTranHeader.TransactionCode = TT_REFUND Then
        If sprdTotal.Text >= 0 Then
            mlngTranSign = 1
            moTranHeader.TransactionCode = TT_SALE
            For Each moTOSType In mcolTOSOpt
                If moTOSType.Code = TT_SALE Then Exit For
            Next moTOSType
        End If
    End If
    
    sbStatus.Panels(PANEL_INFO).Text = "Processing Refund Authorisation"
    curRefTotal = Abs(RefundTotal)
    If (mblnRecalledTran = False) Then
        If Not RefundAuthOK(curRefTotal) Then
            mlngCurrentMode = encmTender
            Call SetEntryMode(enemNone)
            Call Form_KeyPress(vbKeyEscape)
            Exit Sub
        End If
    End If
    
    If LenB(Trim$(moTranHeader.ColleagueCardNo)) > 0 Then
        sbStatus.Panels(PANEL_INFO).Text = "Authorising Colleague Discount"
        If Not CollDiscAuthOK(GetGridData(sprdTotal, COL_TOTVAL, 3)) Then
            moTranHeader.ColleagueCardNo = "000000000"
            mlngCurrentMode = encmTender
            Call SetEntryMode(enemNone)
            Call Form_KeyPress(vbKeyEscape)
            Exit Sub
        End If
    End If
    
    If flSignatureVerification And (curRefTotal <> 0) And (mblnRecalledTran = False) And (mblnRefundingWithAGiftToken = False) Then
        If moTranHeader.RefundCashier = "000" Then
            moTranHeader.RefundCashier = moTranHeader.CashierID
        End If
        
        Set oRecPrinting = New clsReceiptPrinting
        Set oRecPrinting.Printer = OPOSPrinter1
        Set oRecPrinting.TranHeader = moTranHeader
        oRecPrinting.TillNo = mstrTillNo
        oRecPrinting.TranNo = mstrTranId
        Set oRecPrinting.goRoot = goRoot
        Call oRecPrinting.Init(goSession, goDatabase)
        
        vntAddress = Split(mstrAddress, vbNewLine)

        strOrigStoreNo = Left(mstrRefundTrans, 3)
        strOrigTillID = Mid(mstrRefundTrans, 4, 2)
        strOrigTranNo = Mid(mstrRefundTrans, 6, 4)
        If IsDate(Mid(mstrRefundTrans, 10)) Then dteOrigTranDate = CDate(Mid(mstrRefundTrans, 10))
        
        If Not moRefundOrigTran Is Nothing Then
            With moRefundOrigTran
                If (moRefundOrigTran.TransactionDateTime <> 0) Then
                    strOrigStoreNo = .StoreNumber
                    strOrigTillID = .TillID
                    strOrigTranNo = .TransactionNo
                    dteOrigTranDate = .TranDate
                End If
                blnSignatureOK = oRecPrinting.SignatureVerified(sprdLines, "", mstrName, vntAddress(0), _
                                                                vntAddress(1), vntAddress(2), mstrPostCode, _
                                                                strOrigTillID, strOrigTranNo, dteOrigTranDate, strOrigStoreNo)
            End With
        Else
            blnSignatureOK = oRecPrinting.SignatureVerified(sprdLines, "", mstrName, vntAddress(0), _
                                                            vntAddress(1), vntAddress(2), mstrPostCode, vbNullString, _
                                                            vbNullString, vbNull, vbNullString)
        End If
        Set oRecPrinting = Nothing
        mblnRefundingWithAGiftToken = False
        
        If blnSignatureOK = False Then
            'Call SetEntryMode(enemSku)
            moTranHeader.TransactionCode = TT_SALE
            mlngTranSign = 1
            For Each moTOSType In mcolTOSOpt
                If moTOSType.Code = TT_SALE Then Exit For
            Next moTOSType
            Call SetEntryMode(enemNone)
            mlngCurrentMode = encmTender
            Call Form_KeyPress(vbKeyEscape)
            Exit Sub
        End If
    End If
    
    If moTranHeader.TransactionCode = TT_REFUND Then
        Call DebugMsg(MODULE_NAME, strProcedureName, endlTraceIn, "Processing Refund - " & mblnTokensOnly & "/" & mblnTraining)
        If mblnTokensOnly And mblnTraining = False Then
            If CheckRefundTender(lngSaveMode) Then
               Exit Sub
            End If 'CheckRefundTender(lngSaveMode)
        ElseIf (Not moOrigTend Is Nothing) And _
               (mblnTraining = False) And _
               (mblnAllowAllTenders = False) Then
                ' Use Default Tender from previous transaction
                ' Commidea Into WSS - change constants so 9 is represented by an AMEX named one
                            
                strAnotherTenderTypeNew = "[No]-Select Another Tender Type"
                strAnotherTenderTypeOld = "[No]-Get Manager Auth"
                Select Case moOrigTend.tenderType
                    Case TENTYPE_CREDIT, TENTYPE_DEBIT, TENTYPE_AMEX:
                        moOrigTend.tenderType = TENTYPE_CREDIT
                    Case TENTYPE_GIFTTOKEN, TEN_GIFTCARD:
                        moOrigTend.tenderType = TEN_GIFTCARD
                    Case Else:
                        moOrigTend.tenderType = TENTYPE_CASH
                End Select
                
                Set oTenderCtl = goDatabase.CreateBusinessObject(CLASSID_TENDERCONTROL)
                Call oTenderCtl.AddLoadField(FID_TENDERCONTROL_Description)
                Call oTenderCtl.AddLoadFilter(CMP_EQUAL, FID_TENDERCONTROL_TendType, moOrigTend.tenderType)
                Call oTenderCtl.LoadMatches
            Select Case MsgBoxEx("Refund of " & Format(Abs(Val(sprdTotal.Text)), "0.00") & _
                                 " will be issued as " & IIf(oTenderCtl.Description = "Gift Token", "Gift Card", oTenderCtl.Description) & vbNewLine & _
                                 vbNewLine & "[Yes]-Continue / " & IIf(EnableRefundTenderType, strAnotherTenderTypeNew, strAnotherTenderTypeOld), _
                                 vbYesNo, "Refund", , , , , RGBMSGBox_PromptColour)
                Case vbNo, vbCancel:
                    
                If EnableRefundTenderType = False Then
                   If frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) = False Then
                        ' Commidea Into WSS - change constants (so 9 is represented by an AMEX named one -see above)
                        Call SetupDisplayForTender
                        fraTender.Visible = False
                        sprdLines.MaxRows = sprdLines.MaxRows + 1
                        sprdLines.Row = sprdLines.MaxRows
                        sprdLines.Col = 0
                        sprdLines.Text = " "
                        sprdLines.Col = COL_DESC
                        sprdLines.Text = oTenderCtl.Description
                        sprdLines.Col = COL_TENDERTYPE
                        sprdLines.Text = oTenderCtl.TendType
                        curPayAmount = Abs(Val(sprdTotal.Text))
                        mlngCurrentMode = encmTender
                        mstrTenderType = oTenderCtl.TendType
                        Call SetEntryMode(enemValue)
                        uctntPrice.Value = curPayAmount
                        cmdProcess.Value = True
                        Exit Sub
                    Else
                        Call DebugMsg(MODULE_NAME, strProcedureName, endlTraceIn, "Processing Refund - " & mblnTokensOnly & "/" & mblnTraining)
                        Call LoadReasonCodes(enrmTenderOverride)
                        Call SetEntryMode(enemNone)
                        Call SetCurrentMode(encmTenderOverride)
                        While mlngCurrentMode = encmTenderOverride
                            DoEvents
                        Wend
                        moTranHeader.SupervisorUsed = True
                    End If
                Else
                     Call DebugMsg(MODULE_NAME, strProcedureName, endlTraceIn, "Processing Refund - " & mblnTokensOnly & "/" & mblnTraining)
                        Call SetEntryMode(enemNone)
                        Call SetCurrentMode(encmComplete)
                        While mlngCurrentMode = encmComplete
                            DoEvents
                        Wend
                        moTranHeader.SupervisorUsed = True
                End If
                Case Else:
                    Call DebugMsg(MODULE_NAME, strProcedureName, endlTraceIn, "Type is " & oTenderCtl.TendType)
                    ' Commidea Into WSS
                    ' Change constants so 9 is represented by an AMEX named one
                    ' Add check for CNP so ProcessPaymentType can pick this up
                    ' and send the right modifier to the Commidea PED
                    If oTenderCtl.TendType = TENTYPE_CREDIT Then
                        ' If got a Commidea tokenid (and using them) attempt 'cardholder not present' refund first.  This
                        ' will refund to original card, or if fails (card or tokenid expired) will allow retry
                        ' with card details being entered, keyed or inserted, or some other tender type
                        If TillUsingCommideaPED Then
                            If UsingCommideaTokenIDs Then
                                If Not moOrigTendComm Is Nothing Then
                                    If moOrigTendComm.TokenID <> "" Then
                                        If Not mblnInvalidTokenID Then
                                            ' Default to refund to same card - have to be CNP modifier for this to work
                                            mblnCNPTend = True
                                        Else
                                            mblnCNPTend = False
                                        End If
                                    Else
                                        ' Commidea Into WSS - Referral 475 - allow operator chance of CNP
                                        CheckForCNP = True
                                    End If
                                Else
                                    ' Commidea Into WSS - Referral 475 - allow operator chance of CNP
                                    CheckForCNP = True
                                End If
                            Else
                                ' Commidea Into WSS - Referral 475 - allow operator chance of CNP
                                CheckForCNP = True
                            End If
                            ' Commidea Into WSS - Referral 475
                            ' Using Commidea till, but no tokenid or not using them, then ask if want
                            ' a cnp transaction (without token id)
                            If CheckForCNP Then
                                If MsgBoxEx("Is the card holder present for the refund?", vbYesNo, "Refund card holder present", , , , , RGBMSGBox_PromptColour) = vbYes Then
                                    mblnCNPTend = False
                                Else
                                    mblnCNPTend = True
                                End If
                            End If
                        Else
                            mblnCNPTend = False
                        End If
                    End If
                    ' End of Commidea Into WSS
                    Call SetupDisplayForTender
                    fraTender.Visible = False
                    sprdLines.MaxRows = sprdLines.MaxRows + 1
                    sprdLines.Row = sprdLines.MaxRows
                    sprdLines.Col = 0
                    sprdLines.Text = " "
                    sprdLines.Col = COL_DESC
                    sprdLines.Text = oTenderCtl.Description
                    sprdLines.Col = COL_TENDERTYPE
                    sprdLines.Text = oTenderCtl.TendType
                    curPayAmount = Abs(Val(sprdTotal.Text))
                    mlngCurrentMode = encmTender
                    mstrTenderType = oTenderCtl.TendType
                    Call SetEntryMode(enemValue)
                    uctntPrice.Value = curPayAmount
                    cmdProcess.Value = True
                    Call DebugMsg(MODULE_NAME, strProcedureName, endlTraceIn, "Showing Price to confirm")
                    Exit Sub
            End Select
        End If
    End If
    
    If moTranHeader.TransactionCode = TT_SALE Then
        If Abs(Val(sprdTotal.Text)) >= moRetopt.MaximumUnauthorisedSale Then
            If Not frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) Then
                Call SetEntryMode(enemNone)
                mlngCurrentMode = encmTender
                Call Form_KeyPress(vbKeyEscape)
                Exit Sub
            End If
            moTranHeader.SupervisorUsed = True
        End If
    End If
    
    If Val(sprdTotal.Text) = 0 Then
        ' autotender this stuff
        'MO'C WIX1345 - If a paidout product concern resolution theres no need to record as a sale make a stock Adjustment 54
        If mlngCurrentMode = encmPaidOut Then
            'To Do MO'C WIX1345 - Requires a Code 54 stock adjustment ?
             'step through each line and save
             For lngLineNo = 2 To sprdLines.MaxRows Step 1
                Call DebugMsg(MODULE_NAME, strProcedureName, endlDebug, "Saving Line " & lngLineNo)
                Call DebugMsgSprdLineValue(lngLineNo, strProcedureName)
                ucpbStatus.Value = lngLineNo - 1
                sprdLines.Row = lngLineNo
                'Ensure only lines that are SKU's
                sprdLines.Col = COL_LINETYPE
                Select Case (Val(sprdLines.Value))
                    Case (LT_ITEM):
                        sprdLines.Col = COL_PARTCODE
                        strPartCode = sprdLines.Text
                        sprdLines.Col = COL_QTY
                        intQty = Val(sprdLines.Value)
                        If strPartCode <> "" Then
                            Call StockAdjustMent54(strPartCode, intQty)
                        End If
                End Select
            Next lngLineNo
        End If
        Call FinaliseTransaction(False)
        sprdTotal.Refresh
        txtSKU.Text = vbNullString
        txtSKU.Visible = False
        lblType.Visible = False
        Exit Sub
    End If
    
    Set mcolTTypes = Nothing
        
    Call DebugMsg(MODULE_NAME, strProcedureName, endlDebug, "Get Tenders: " & mstrTranId)
    
    strTranCode = moTranHeader.TransactionCode
' Ensure Type 8 exists.  Put it away if not
    If strTranCode = TT_REFUND Then
        Set oTOSTenders = goDatabase.CreateBusinessObject(CLASSID_TYPESOFSALETENDERS)
        Call oTOSTenders.AddLoadFilter(CMP_EQUAL, FID_TYPESOFSALETENDERS_TOSDisplaySeq, _
                                       moTOSType.DisplaySeq)
        Call oTOSTenders.AddLoadFilter(CMP_EQUAL, FID_TYPESOFSALETENDERS_TenderNo, 8)
        Set colTOSTends = oTOSTenders.LoadMatches
    
        If colTOSTends.Count = 0 Then
            oTOSTenders.TOSDisplaySeq = moTOSType.DisplaySeq
            oTOSTenders.TenderNo = 8
            oTOSTenders.SaveIfNew
        End If
    End If
    
    If GetBooleanParameter(-1065, goDatabase.Connection) Then
        Call LoadTenderBar
    Else
        Set rsTenders = TendersRecordset(moTranHeader.TransactionCode)
        
        Call DebugMsg(MODULE_NAME, strProcedureName, endlDebug, "Got Tenders: " & mstrTranId)
        
        'Unload any previous Tender Types that where valid for previous transaction types
        For lngBtnNo = cmdTenderType.UBound To 1 Step -1
            Call Unload(cmdTenderType(lngBtnNo))
        Next lngBtnNo
        For lngBtnNo = lblTenderFKey.UBound To 1 Step -1
            Call Unload(lblTenderFKey(lngBtnNo))
        Next lngBtnNo
            
        lngBtnNo = 1
        
    '    For lngEntryNo = 1 To mcolTTypes.Count Step 1
        Do While Not rsTenders.EOF
            'Set oTenderOpts = mcolTTypes(lngEntryNo)
    '        If (lenb(mcolTTypes(lngEntryNo).Description) <> 0) And (moTOSType(lngEntryNo).TenderTypesAllowed = True) Then
            If (LenB(rsTenders!Description) <> 0) Then
                Load cmdTenderType(lngBtnNo)
                lngLeftPos = ((lngBtnNo - 1) Mod BTN_PER_ROW) + 1
                cmdTenderType(lngBtnNo).Left = ((cmdTenderType(0).Width + 120) * (lngLeftPos - 1)) + 180
                cmdTenderType(lngBtnNo).Visible = False
                cmdTenderType(lngBtnNo).Caption = Trim(rsTenders!Description)
                cmdTenderType(lngBtnNo).Tag = rsTenders!TenderNo
                cmdTenderType(lngBtnNo).Enabled = (IsNull(rsTenders!Active) = False)
                cmdTenderType(lngBtnNo).BackColor = fraTender.BackColor
                
                If mblnTraining Then
                    If rsTenders!TendType = TEN_CHEQUE Or rsTenders!TendType = TEN_CREDCARD Or rsTenders!TendType = TEN_DEBITCARD Then
                        cmdTenderType(lngBtnNo).Enabled = False
                    Else
                    End If
                End If
                Select Case (rsTenders!TendType)
                    Case (TEN_CASH), (TEN_GIFTTOKEN), (TEN_GIFTCARD), (TEN_PROJLOAN), (TEN_COUPON):
                        If (mblnHasCashDrawer = False) Then cmdTenderType(lngBtnNo).Enabled = False
                    Case (TEN_CHEQUE):
                        If (mblnHasCashDrawer = False) Or (mblnHasEFT = False) Then cmdTenderType(lngBtnNo).Enabled = False
                    Case (TEN_CREDCARD), (TEN_DEBITCARD):
                        If (mblnHasEFT = False) Then cmdTenderType(lngBtnNo).Enabled = False
                End Select
                
                'Commidea Into WSS
                ' New Cnp button
                If rsTenders!TenderNo = TENDNO_CNP Then
                    ' Only enabled for QOD (provided have Commidea till)
                    ' Referral 476 - allow Misc Income to have Card Not Present
                    ' Start assuming no CNP allowed...
                    cmdTenderType(lngBtnNo).Enabled = False
                    '...then switch on in desired circumstances
                    If TillUsingCommideaPED Then
                        ' Referral 476, 524 (again) - allow Misc Income (M+ MISCIN) to have Card Not Present
                        ' Referral 523 - allow Paid Out (M- MISCOUT) to have Card Not Present
                        ' Referral 527 - allow Misc Income Correct (C+ CORRIN) to have Card Not Present
                        ' Referral 527 - allow Paid Out Correct (C- CORROUT) to have Card Not Present
                        If mblnOrderBtnPressed And mblnCreatingOrder _
                        Or moTOSType.Code = TT_MISCIN _
                        Or moTOSType.Code = TT_MISCOUT _
                        Or moTOSType.Code = TT_CORRIN _
                        Or moTOSType.Code = TT_CORROUT Then
                            cmdTenderType(lngBtnNo).Enabled = True
                        End If
                    End If
                End If
                'End of Commidea Into WSS
                
                If lngBtnNo <= BTN_PER_ROW Then
                    Load lblTenderFKey(lngBtnNo)
                    lblTenderFKey(lngBtnNo).Left = cmdTenderType(lngBtnNo).Left + _
                                ((cmdTenderType(lngBtnNo).Width - lblTenderFKey(lngBtnNo).Width) / 2)
                    lblTenderFKey(lngBtnNo).Visible = True
                    lblTenderFKey(lngBtnNo).Caption = "F" & lngBtnNo
                End If
                lngBtnNo = lngBtnNo + 1
            End If
            rsTenders.MoveNext
        Loop
        
        If (goSession.GetParameter(PRM_TRAN_DISC_HOTKEY) <> "") Then
            mstrTranDiscCode = goSession.GetParameter(PRM_TRAN_DISC_HOTKEY)
            If (mstrTranDiscCode = "") Then
                Call MsgBoxEx("Transaction Discount configuration does not have the Reason Code defined." & vbCrLf & "Transaction Discount has been disabled.", vbOKOnly, "Invalid Transaction Discount Set-Up", , , , , RGBMsgBox_WarnColour)
            Else
                mstrTranDiscAuth = Mid$(mstrTranDiscCode, InStr(mstrTranDiscCode, ",") + 1)
                mstrTranDiscCode = Left$(mstrTranDiscCode, InStr(mstrTranDiscCode, ",") - 1)
                strPartCode = Mid$(mstrTranDiscAuth, InStr(mstrTranDiscAuth, ",") + 1)
                mstrTranDiscAuth = Left$(mstrTranDiscAuth, InStr(mstrTranDiscAuth, ",") - 1)
                mlngTranDiscMgrLevel = Val(Mid$(strPartCode, InStr(strPartCode, ",") + 1))
                mlngTranDiscSupLevel = Val(Left$(strPartCode, InStr(strPartCode, ",") - 1))
                Load cmdTenderType(lngBtnNo)
                lngLeftPos = ((lngBtnNo - 1) Mod BTN_PER_ROW) + 1
                cmdTenderType(lngBtnNo).Left = ((cmdTenderType(0).Width + 120) * (lngLeftPos - 1)) + 180
                cmdTenderType(lngBtnNo).Visible = False
                cmdTenderType(lngBtnNo).Caption = "Tran Disc"
                cmdTenderType(lngBtnNo).Tag = "TranDisc"
                cmdTenderType(lngBtnNo).Enabled = True
                cmdTenderType(lngBtnNo).BackColor = fraTender.BackColor
        '        cActionCode.ActionCode = enacTransactionDisc
            End If
        End If
        
        Call DebugMsg(MODULE_NAME, strProcedureName, endlDebug, "Buttons Built: " & mstrTranId)
    
        mlngTenderTypeRowNo = 0
        Call ShowTenderTypeRow
        
        Call SetupDisplayForTender
    
    End If
    
    Call DebugMsg(MODULE_NAME, strProcedureName, endlDebug, "Tender set-up complete " & mstrTranId)
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler(strProcedureName, Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub cmdTenderType_Click(index As Integer)

Dim strTokenSerial As String
Dim strTestSerial  As String
Dim curTokenValue  As Currency
Dim oRecPrinting   As clsReceiptPrinting
Dim cCurrBO        As cCurrencies
Dim blnStopPrint   As Boolean

    On Error GoTo HandleException
    
    Call SetPreviousWSSUniqueTTRef("")

    Call SetEntryMode(enemNone)
    If mlngTenderTypeRowNo > 0 And index <= BTN_PER_ROW Then
        index = index + (mlngTenderTypeRowNo * BTN_PER_ROW)
    End If
    
    If (cmdTenderType(index).Visible = False) Or (cmdTenderType(index).Enabled = False) Then Exit Sub
    
    cmdTenderType(index).SetFocus
    'When Tender type selected, extract options into moTenderOptions and enter amount of Tender
    
    If (cmdTenderType(index).Tag = "TranDisc") Then
        If (mblnFirstPmtDone = False) Then
            Call EnterTransactionDisc
            mlngTenderTypeRowNo = 0
            Call ShowTenderTypeRow
            Exit Sub
        Else
            Call MsgBoxEx("Unable to perform discount once actual tender has been started", vbOKOnly, "Discount Cancelled", , , , , RGBMsgBox_WarnColour)
            mlngTenderTypeRowNo = 0
            Call ShowTenderTypeRow
            Exit Sub
        End If
    End If
        
    Set moTenderOpts = goDatabase.CreateBusinessObject(CLASSID_TENDEROPTIONS)
    Call moTenderOpts.AddLoadFilter(CMP_EQUAL, FID_TENDEROPTIONS_TenderNo, cmdTenderType(index).Tag)
    Set moTenderOpts = moTenderOpts.LoadMatches.Item(1)
    
    If moTenderOpts.TendType = TEN_GIFTCARD Then
        If (GiftCardPaymentCount(moTranHeader) >= 9) Then
            Call MsgBoxEx("Maximum number of Gift Card payments is reached." & vbNewLine _
                        & "Please use other tender type.", vbExclamation, "Tender Transaction", , , , , RGBMsgBox_WarnColour)
            Exit Sub
        End If
        
        If moTOSType.Code = TT_REFUND Then
            If (Abs(Val(GetGridData(sprdTotal, COL_TOTVAL, ROW_TOTTOTALS))) > mcurMaxRefundGiftCardValue) Then
                Call MsgBoxEx("You have exceeded the maximum allowance that can be refunded to a Gift Card" & vbNewLine & _
                " for this transaction.", vbOKOnly + vbExclamation, "Warning", , , , , RGBMsgBox_WarnColour)
                Exit Sub
            End If 'If (Abs(Val(GetGridData(sprdTotal, COL_TOTVAL, ROW_TOTTOTALS))) > mcurMaxRefundGiftCardValue)
            If (Abs(Val(GetGridData(sprdTotal, COL_TOTVAL, ROW_TOTTOTALS))) < mcurMinRefundGiftCardValue) Then
                Call MsgBoxEx("You have specified the value less than the minimum allowance that can be refunded to a Gift Card" & vbNewLine & _
                " for this transaction.", vbOKOnly + vbExclamation, "Warning", , , , , RGBMsgBox_WarnColour)
                Exit Sub
            End If 'If (Abs(Val(GetGridData(sprdTotal, COL_TOTVAL, ROW_TOTTOTALS))) < mcurMinRefundGiftCardValue)
        End If
    End If
        
    If (moTenderOpts.TendType = TEN_GIFTTOKEN) And moTOSType.Code = TT_REFUND Then
            
        mlngCurrentMode = encmGiftVoucher
        mdblTotalBeforeDis = mdblTotalBeforeDis + Abs(Val(GetGridData(sprdTotal, COL_TOTVAL, ROW_TOTTOTALS)))
        Call AddVoucherSKUs(Abs(Val(GetGridData(sprdTotal, COL_TOTVAL, ROW_TOTTOTALS))), True, "TR")
        
        If (Val(GetGridData(sprdTotal, COL_TOTVAL, ROW_TOTTOTALS)) = 0) Then
            If EnableRefundTenderType Then
                Call DebugMsg(MODULE_NAME, "cmdTenderType_Click", endlDebug, "Calling RecordTender(False, False, False)")
                Call RecordTender(False, False, False, False)
            Else
                Call DebugMsg(MODULE_NAME, "cmdTenderType_Click", endlDebug, "Calling FinaliseTransaction(True)")
                Call FinaliseTransaction(True)
            End If
        Else
            SetEntryMode (enemNone)
            mlngCurrentMode = encmTender
        End If
        sprdTotal.Refresh
        txtSKU.Text = vbNullString
        txtSKU.Visible = False
        lblType.Visible = False
        Exit Sub
    End If
    
    ' Commidea Into WSS
    mblnCNPTend = CBool(cmdTenderType(index).Tag = TENDNO_CNP) And TillUsingCommideaPED
    
    mstrTenderType = moTenderOpts.TendType
    'Add line to display to show Tendering
    sprdLines.MaxRows = sprdLines.MaxRows + 1
    If ((sprdLines.MaxRows - sprdLines.TopRow) > 14) Then sprdLines.TopRow = sprdLines.MaxRows - 13
    sprdLines.Row = sprdLines.MaxRows
    sprdLines.Col = 0
    sprdLines.Text = " " 'override auto column lettering
    sprdLines.Col = COL_DESC
    sprdLines.Text = Split(moTenderOpts.Description, "(")(0)
    sprdLines.Col = COL_TENDERTYPE
    sprdLines.Text = moTenderOpts.TendType
    'Hide Tender Types and enter amount
    fraTender.Visible = False
    cmdCurrency.Visible = False
    
    If (mblnUseMultiCur = True) And (moTOSType.Code <> TT_REFUND) Then
        For Each cCurrBO In mcolCurrencies
            If (cCurrBO.tenderType(moTenderOpts.TendType) = True) Then
                cmdCurrency.Visible = True
                Exit For
            End If
        Next
    End If
   
    If moTenderOpts.TendType <> TEN_GIFTTOKEN Or moTOSType.Code = TT_REFUND Then
        Call SetEntryMode(enemValue)
        
        'Call uctntPrice.SetFocus
        sprdTotal.Row = 3
        If moTenderOpts.DefaultRemainingAmount Then
            If IsNumeric(sprdTotal.Text) Then
                uctntPrice.Value = CCur(sprdTotal.Text) * mlngTranSign
            End If
        ElseIf Right$(cmdTenderType(index).Caption, 8) = "(Next " & ChrW(163) & ")" Then
            uctntPrice.Value = Int(CCur(sprdTotal.Text) * mlngTranSign) + 1
        ElseIf Right$(cmdTenderType(index).Caption, 10) = "(Next " & ChrW(163) & "10)" Then
            uctntPrice.Value = (Int(CCur(sprdTotal.Text) * mlngTranSign) \ 10) * 10 + 10
        End If
        
        DoEvents
        uctntPrice.SelectAllValue
        If (uctntPrice.Enabled = True) And (uctntPrice.Visible = True) Then uctntPrice.SetFocus
'        uctntPrice.SelectAllValue
    Else
        frmTokenScan.GetTokenToSell (ucKeyPad1.Visible)
        If frmTokenScan.Cancel Then
            sprdLines.MaxRows = sprdLines.MaxRows - 1
            fraTender.Visible = True
            Exit Sub
        End If
        strTestSerial = frmTokenScan.ScanData
        
        frmTokenScan.GetTokenForTender (ucKeyPad1.Visible)
        If frmTokenScan.Cancel Then
            sprdLines.MaxRows = sprdLines.MaxRows - 1
            fraTender.Visible = True
            Exit Sub
        End If
        
        Call DecodeGiftTokenBarcode(frmTokenScan.ScanData, strTokenSerial, curTokenValue)
        
        If strTestSerial <> strTokenSerial Then
            Call MsgBoxEx("Serial numbers do NOT match", vbOKOnly & vbExclamation, _
                          "Invalid Gift Token", , , , , RGBMsgBox_WarnColour)
            sprdLines.MaxRows = sprdLines.MaxRows - 1
            fraTender.Visible = True
            Exit Sub
        End If
        
        If GiftVoucherValid(strTokenSerial, vbNullString) = False Then
            fraTender.Visible = True
            Exit Sub
        End If
        
        If GiftVoucherNotUsed(strTokenSerial, True) = False Then
            sprdLines.MaxRows = sprdLines.MaxRows - 1
            fraTender.Visible = True
            Exit Sub
        End If
        
        mblnGiftTokenUsedInTender = True
        
        Call moTranHeader.AddGiftVoucher(strTokenSerial, curTokenValue, "TS")
        
        'CR0014 - Stop Gift Voucher Print if Parameter 270 is set to true
        On Error Resume Next
        blnStopPrint = goSession.GetParameter(PRM_STOP_GIFT_VOUCHER_PRINT)
        On Error GoTo 0
        If blnStopPrint = False Then
            Set oRecPrinting = New clsReceiptPrinting
            Set oRecPrinting.Printer = OPOSPrinter1
            oRecPrinting.TillNo = mstrTillNo
            oRecPrinting.TranNo = mstrTranId
            Set oRecPrinting.goRoot = goRoot
            
            Call oRecPrinting.Init(goSession, goDatabase)
            
            Call oRecPrinting.PrintGiftTokenFranking(eftRedeem, moTranHeader.TillID, _
                            moTranHeader.TransactionNo, moTranHeader.TranDate, strTokenSerial, 0)
        End If
        'CR0014 End
        
        Call OutputRedeemedGiftVoucher(strTokenSerial)
        
        Call SetEntryMode(enemValue)
        sprdTotal.Row = 3
        sprdLines.Col = COL_VOUCHER
        
        uctntPrice.Value = curTokenValue
        Call uctntPrice_KeyPress(vbKeyReturn)
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("cmdTenderType_Click", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub DisplayPayment(dblValue As Double)
                        
    sprdLines.Col = COL_INCTOTAL
    sprdLines.Text = txtSKU.Text
    mdblPaidTotal = mdblPaidTotal + Val(txtSKU.Text)
    sprdTotal.Row = 3
    sprdTotal.Col = COL_TOTVAL
    sprdTotal.Text = Format$(mdblTranTotal - mdblPaidTotal, "0.00")
    sprdLines.Col = COL_LINETYPE
    sprdLines.Text = LT_PMT
    If ((sprdLines.MaxRows - sprdLines.TopRow) > sprdLines.VisibleRows) Then sprdLines.TopRow = sprdLines.MaxRows - (sprdLines.VisibleRows + 3)
    If Val(sprdTotal.Text) <= 0 Then
        sprdTotal.Row = 1
        sprdTotal.Col = COL_TOTVAL - 1
        sprdTotal.Text = "Change"
        
        If (mblnSaveNewOrder = True) Then
            Call CreateOrder(False, True, False)
            Exit Sub
        Else
            Call SaveSale(False, False, False, False)
            fraTender.Visible = False
            fraActions.Visible = False
            mlngCurrentMode = encmComplete
            Call Form_KeyPress(vbKeyReturn)
        End If
    Else
        sprdTotal.Row = 1
        sprdTotal.Col = COL_TOTVAL - 1
        sprdTotal.Text = "Balance"
        SetEntryMode enemNone
        fraTender.Visible = True
    End If
    sprdTotal.Refresh
    txtSKU.Text = vbNullString
    txtSKU.Visible = False
    lblType.Visible = False

End Sub

Private Sub MarkReturnCoupon(strCouponID As String)

Dim lngRowNo As Long

    strCouponID = Left$(strCouponID, InStr(strCouponID, ":") - 1)
    For lngRowNo = 2 To sprdLines.MaxRows Step 1
        sprdLines.Row = lngRowNo
        sprdLines.Col = COL_LINETYPE
        If (Val(sprdLines.Value) = LT_COUPON) Then
            sprdLines.Col = COL_VOIDED
            If (Val(sprdLines.Value) = 0) Then
                sprdLines.Col = COL_CPN_STATUS
                If (sprdLines.Value = "0") Then
                    sprdLines.Col = COL_CPN_ID
                    If (sprdLines.Value = strCouponID) Then
                        sprdLines.Col = COL_CPN_STATUS
                        sprdLines.Text = "8"
                        Exit For
                    End If
                End If
            End If
        End If
    Next lngRowNo

End Sub

Private Sub VoidTransaction(Optional blnShowConfirmMessage As Boolean = True)
        
    On Error GoTo HandleException

    If Not blnShowConfirmMessage Then
        If Not frmRetrieveTran Is Nothing Then
            Unload frmRetrieveTran
        End If
        
        If frmVerifyPwd.VerifySupervisorPassword(ucKeyPad1.Visible, , True) = False Then
            If mlngCurrentMode <> encmPaidOut And mlngCurrentMode <> encmPaidIn And _
               mlngCurrentMode <> encmOpenDrawer Then
                Call SetEntryMode(enemSKU)
            Else
                If (uctntPrice.Visible = True) And (uctntPrice.Enabled = True) Then uctntPrice.SetFocus
            End If
            Exit Sub
        End If
        
        moTranHeader.SupervisorUsed = True
        moTranHeader.VoidSupervisor = frmVerifyPwd.txtAuthID
    
        mstrAccountName = vbNullString
        mstrAccountNum = vbNullString
        mblnCreatingOrder = False
        mblnOrderBtnPressed = False
        If moTranHeader.TransactionCode <> TT_MISCOUT And _
            moTranHeader.TransactionCode <> TT_MISCIN And moTranHeader.TransactionCode <> TT_CORRIN And moTranHeader.TransactionCode <> TT_CORROUT And _
            moTranHeader.TransactionCode <> TT_OPENDRAWER Then
            If Val(moTranHeader.OrderNumber) <> 0 Then
                BackOutOrder
            End If
            Call SaveSale(False, True, False, False)
            mlngCurrentMode = encmComplete
            Call Form_KeyPress(vbKeyReturn)
        Else
            Select Case mlngCurrentMode
                Case encmPaidOut
                    Call SavePaidOut(True)
                Case encmPaidIn
                    Call SavePaidIn(True)
                Case encmOpenDrawer
                    Call SaveOpenDrawer(True)
            End Select
            
            Call SetCurrentMode(encmTranType)
        End If
        Exit Sub
    End If
    
    If fraReasonsList.Visible = False Then
        If (mlngEntryMode = enemSKU) Then mlngEntryMode = enemNone
        If MsgBoxEx("Void transaction", vbYesNo, "Confirm Void", , , , PROMPT_FONT_SIZE, _
                    RGBMsgBox_WarnColour) = vbYes Then
            If frmVerifyPwd.VerifySupervisorPassword(ucKeyPad1.Visible) = False Then
                If mlngCurrentMode <> encmPaidOut And mlngCurrentMode <> encmPaidIn And _
                   mlngCurrentMode <> encmOpenDrawer Then
                    Call SetEntryMode(enemSKU)
                Else
                    If (uctntPrice.Visible = True) And (uctntPrice.Enabled = True) Then uctntPrice.SetFocus
                End If
                Exit Sub
            End If
            
            moTranHeader.SupervisorUsed = True
            moTranHeader.VoidSupervisor = frmVerifyPwd.txtAuthID

            mstrAccountName = vbNullString
            mstrAccountNum = vbNullString
            mblnCreatingOrder = False
            mblnOrderBtnPressed = False
            If moTranHeader.TransactionCode <> TT_MISCOUT And _
                moTranHeader.TransactionCode <> TT_MISCIN And moTranHeader.TransactionCode <> TT_CORRIN And moTranHeader.TransactionCode <> TT_CORROUT And _
                moTranHeader.TransactionCode <> TT_OPENDRAWER Then
                If Val(moTranHeader.OrderNumber) <> 0 Then
                    BackOutOrder
                End If
                Call SaveSale(False, True, False, False)
                mlngCurrentMode = encmComplete
                Call Form_KeyPress(vbKeyReturn)
            Else
                Select Case mlngCurrentMode
                    Case encmPaidOut
                        Call SavePaidOut(True)
                    Case encmPaidIn
                        Call SavePaidIn(True)
                    Case encmOpenDrawer
                        Call SaveOpenDrawer(True)
                End Select
                
                Call SetCurrentMode(encmTranType)
            End If
        
        Else
            If mlngCurrentMode <> encmPaidOut And _
               mlngCurrentMode <> encmPaidIn And _
               mlngCurrentMode <> encmOpenDrawer Then
                Call SetEntryMode(enemSKU)
            Else
                If (uctntPrice.Enabled = True) And (uctntPrice.Visible = True) Then uctntPrice.SetFocus
            End If
        End If
    Else
        If MsgBoxEx("Void transaction", vbYesNo, "Confirm Void", , , , PROMPT_FONT_SIZE, _
                    RGBMsgBox_WarnColour) = vbYes Then
        
            If frmVerifyPwd.VerifySupervisorPassword(ucKeyPad1.Visible) = False Then
                Exit Sub
            End If
            
            moTranHeader.SupervisorUsed = True
            moTranHeader.VoidSupervisor = frmVerifyPwd.txtAuthID
            
            fraReasonsList.Visible = False
            If (moTranHeader.TotalSaleAmount > 9999999) Then moTranHeader.TotalSaleAmount = 0
            Select Case mlngCurrentMode
                Case encmPaidOut
                    Call SavePaidOut(True)
                Case encmPaidIn
                    Call SavePaidIn(True)
                Case encmOpenDrawer
                    Call SaveOpenDrawer(True)
            End Select
            
            Call SetCurrentMode(encmTranType)
        Else
            If (lstReasonsList.Enabled = True) And (lstReasonsList.Visible = True) Then lstReasonsList.SetFocus
        End If
    End If
    
    If Not moTranHeader Is Nothing Then
        moTranHeader.Offline = True
        moTranHeader.SaveIfExists
        Call BackUpTransaction
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("VoidTransaction", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim lngKeyIn As Integer
    Dim lngRowNo As Integer
    Dim strKey   As String
    Dim strProcedureName As String
    
    On Error GoTo HandleException
    
    strProcedureName = "Form_KeyDown"
    
    Call DebugMsg(MODULE_NAME, strProcedureName, endlTraceIn)
    
    If (KeyCode >= 112) And (KeyCode <= 123) Then strKey = "(F" & KeyCode - 111 & "-Pressed)"
    If (KeyCode >= 48) And (KeyCode <= 57) Then strKey = "(" & Chr(KeyCode) & "-Pressed)"
    If (KeyCode >= 65) And (KeyCode <= 90) Then strKey = "(" & Chr(KeyCode) & "-Pressed)"
    If (KeyCode >= 97) And (KeyCode <= 122) Then strKey = "(" & Chr(KeyCode) & "-Pressed)"
    Call DebugMsg(vbNullString, vbNullString, endlDebug, "KDn" & KeyCode & "/" & Shift & strKey & " EM=" & mlngEntryMode & ":" & mlngCurrentMode)
    
    If Not moTOSType Is Nothing Then
        Select Case moTOSType.Code
        Case TT_MISCIN, TT_CORRIN, TT_MISCOUT, TT_CORROUT:
            If (KeyCode = 109 Or KeyCode = 189) Then
                KeyCode = 0
                uctntPrice.SetFocus
            End If
        End Select
    End If
    
    If mlngEntryMode = enemSKU Then     'MO'C WIX1302
        If mstrMarkDownStyle <> "" And Len(txtSKU.Text) = (Len(mstrMarkDownStyle) - 1) Then GoTo DebugMsgExit
    End If
    
    If mintDuressKeyCode <> 0 And KeyCode = mintDuressKeyCode Then
        KeyCode = 0
        OpenDrawerUnderDuress
        UpdateTotals
        If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then
            txtSKU.SetFocus
        ElseIf (uctntPrice.Visible = True) And (uctntPrice.Enabled = True) Then
            uctntPrice.SetFocus
        End If
        GoTo DebugMsgExit
    End If
    
    ' Referral 876
    ' If enter key press occurs after card payment processing but before
    ' results are processed can get a 2nd call to ProcessPaymentType method
    ' that causes many problems.  So check if in ProcessPaymentType via
    ' this call 1st, and if so, kill off the keypress and quit
    If KeyCode = vbKeyReturn Then
        If Not m_ProcessingPaymentImplementation Is Nothing Then
            If m_ProcessingPaymentImplementation.InProcessPaymentTypeMethod Then
                KeyCode = 0
                GoTo DebugMsgExit
            End If
        End If
    End If
    ' End of Referral 876
    
    
    If (Shift = 4) And (KeyCode = vbKeyF4) Then KeyCode = 0 'disbale the alt-f4 (close window)
    
    If fraActions.Visible = True Then
        If (Shift = 2) Then KeyCode = KeyCode * -1
        For lngRowNo = 1 To lblActionFKey.UBound Step 1
            If (lblActionFKey(lngRowNo).Visible = True) And (lblActionFKey(lngRowNo).Enabled = True) Then
                If (KeyCode = lblActionFKey(lngRowNo).Tag) Then
                    cmdAction(lngRowNo).Value = True
                    KeyCode = 0
                    GoTo DebugMsgExit
                End If
            End If
        Next lngRowNo
    End If
    
    Select Case (mlngCurrentMode)
        Case (encmTranType):
            If (Shift = 0) And (KeyCode >= vbKeyF1) And (KeyCode <= vbKeyF10) Then
                If KeyCode = vbKeyF8 Then
                    cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY).Value = True
                Else
                    lngKeyIn = (KeyCode - vbKeyF1) + 2
                    If lngKeyIn + (mlngTranTypeRowNo * TRAN_BTN_PER_ROW) <= cmdTranType.UBound Then
                        If cmdTranType(lngKeyIn + (mlngTranTypeRowNo * TRAN_BTN_PER_ROW)).Visible Then
                            cmdTranType(lngKeyIn + (mlngTranTypeRowNo * TRAN_BTN_PER_ROW)).Value = True
                        End If
                    End If
                End If
                KeyCode = 0
            End If
            If (Shift = 2) And (KeyCode = vbKeyF8) And (cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY_PIM).Visible = True) Then
                cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY_PIM).Value = True
            End If
            If (Shift = 0) And (KeyCode = vbKeyF11) And (cmdPrevTranType.Visible = True) Then
                cmdPrevTranType.Value = True
                KeyCode = 0
            End If
            If (Shift = 0) And (KeyCode = vbKeyF12) And (cmdNextTranType.Visible = True) Then
                cmdNextTranType.Value = True
                KeyCode = 0
            End If
            
        Case (encmTender):
            If fraTender.Visible Then
                If (Shift = 0) And (KeyCode >= vbKeyF1) And (KeyCode <= vbKeyF10) Then
                    lngKeyIn = (KeyCode - vbKeyF1) + 1
                    If lngKeyIn + (mlngTenderTypeRowNo * BTN_PER_ROW) <= cmdTenderType.UBound Then
                        If cmdTenderType(lngKeyIn + (mlngTenderTypeRowNo * BTN_PER_ROW)).Visible Then
                                cmdTenderType(lngKeyIn).Value = True
                        End If
                    End If
                    KeyCode = 0
                End If
                If (Shift = 0) And (KeyCode = vbKeyF11) And (cmdPrevTenderType.Visible = True) Then
                    cmdPrevTenderType.Value = True
                    KeyCode = 0
                End If
                If (Shift = 0) And (KeyCode = vbKeyF12) And (cmdNextTenderType.Visible = True) Then
                    cmdNextTenderType.Value = True
                    KeyCode = 0
                End If
                
                If (KeyCode = vbKeyEscape) Then
                    'Set moDiscountCardScheme = Nothing
                    'KeyCode = 0
                End If
                

            Else
                If (cmdCurrency.Visible = True) And (KeyCode = vbKeyF5) Then
                    KeyCode = 0
                    cmdCurrency.Value = True
                End If
                
                If KeyCode = vbKeySubtract Or KeyCode = 189 Then 'Ref 551 remove negative value
                    KeyCode = 0
                End If
            End If
        Case (encmRecord):
            If ActiveControl Is Nothing Then
                Debug.Print "Dodging Bullet"
            Else
                If ActiveControl.Name = uctntPrice.Name Or mlngEntryMode = enemQty Then
                    If KeyCode = vbKeyEscape Then
                        KeyCode = 0
                        Call SetEntryMode(enemSKU)
                    End If
                End If
            End If
            
            If Shift = 0 Then
                If (KeyCode = vbKeyF11) And (cmdPrevAction.Visible = True) Then
                    cmdPrevAction.Value = True
                    KeyCode = 0
                End If
                If (KeyCode = vbKeyF12) And (cmdNextAction.Visible = True) Then
                    cmdNextAction.Value = True
                    KeyCode = 0
                End If
            End If
        Case (encmGiftVoucher):
            If ActiveControl Is Nothing Then
                Debug.Print "Dodging Bullet"
            Else
                If ActiveControl.Name = uctntPrice.Name Then
                    If KeyCode = vbKeyEscape Then
                        KeyCode = 0
                        mlngCurrentMode = encmRecord
                        Call SetEntryMode(enemSKU)
                    End If
                End If
            End If
        Case (encmOverrideCode) 'Ref 590 remove negative value
            If KeyCode = vbKeySubtract Or KeyCode = 189 Then
                KeyCode = 0
            End If
    End Select
    
DebugMsgExit:
    Call DebugMsg(MODULE_NAME, strProcedureName, endlTraceOut)
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler(strProcedureName, Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Function IsProcessRunning(ByVal sProcess As String) As Long
    Const MAX_PATH As Long = 260
    Dim lProcesses() As Long, lModules() As Long, N As Long, lRet As Long, hProcess As Long
    Dim sName As String
    Dim res As Long
    res = 0
   
    sProcess = UCase$(sProcess)
   
    ReDim lProcesses(1023) As Long
    If EnumProcesses(lProcesses(0), 1024 * 4, lRet) Then
        For N = 0 To (lRet \ 4) - 1
            hProcess = OpenProcess(PROCESS_QUERY_INFORMATION Or PROCESS_VM_READ, 0, lProcesses(N))
            If hProcess Then
                ReDim lModules(1023)
                If EnumProcessModules(hProcess, lModules(0), 1024 * 4, lRet) Then
                    sName = String$(MAX_PATH, vbNullChar)
                    GetModuleBaseName hProcess, lModules(0), sName, MAX_PATH
                    sName = Left$(sName, InStr(sName, vbNullChar) - 1)
                    If Len(sName) = Len(sProcess) Then
                        If sProcess = UCase$(sName) Then res = res + 1
                    End If
                End If
            End If
            CloseHandle hProcess
        Next N
    End If
    
    IsProcessRunning = res
End Function

Private Sub ReceiptLogsHousekeeping()
        
Dim oFs As New FileSystemObject
Dim oFolder As Folder
Dim oFile As File
Dim folderPath As String
Dim housekeepingInterval As Integer
Dim reg As New RegExp
    
    On Error GoTo Err
    Call DebugMsg(MODULE_NAME, "ReceiptLogsHousekeeping", endlDebug, "Start housekeeping" & mstrTranId)
    folderPath = goSession.GetParameter(PRM_RECEIPT_LOGGING_PATH)
    housekeepingInterval = goSession.GetParameter(PRM_RECEIPT_HOUSEKEEPING_INTERVAL)
    
    reg.Pattern = "\d{8}_\d{2}_\d{4}.RCP"
    reg.Global = True
    If oFs.FolderExists(folderPath) Then
        Set oFolder = oFs.GetFolder(folderPath)
        For Each oFile In oFolder.Files
            If (reg.test(oFile.Name) = True) And DateDiff("d", oFile.DateCreated, DateValue(Now)) > housekeepingInterval Then
                On Error Resume Next
                oFile.Delete True
                On Error GoTo Err
            End If
        Next
    End If
    Call DebugMsg(MODULE_NAME, "ReceiptLogsHousekeeping", endlDebug, "End housekeeping" & mstrTranId)

Exit Sub
Err:
    Call DebugMsg(MODULE_NAME, "ReceiptLogsHousekeeping", endlDebug, "Error has occured during housekeeping process" & mstrTranId)
End Sub

Private Sub Form_Load()

Dim oFSO        As FileSystemObject
Dim tsTranFile  As TextStream
Dim strLine     As String
Dim strFileName As String
Dim strCNPTills As String
Dim lngTranID   As Long

Dim pInfo As PROCESS_INFORMATION
Dim sInfo As STARTUPINFO
Dim sNull As String
Dim lSuccess As Long
Dim lRetValue As Long

Dim oTranID As cSale_Wickes.cPOSHeader
Dim lngTran As Byte
Dim oRow    As Object
Dim oField  As Object

Dim oWorkStationBO As cEnterprise_Wickes.cWorkStation
Dim oParameterBO   As cEnterprise_Wickes.cParameter

Dim tsWEEESKU As TextStream

Dim lngRowNo    As Long
Dim lngColNo    As Long
    
    On Error GoTo HandleException
    
    Call DebugMsg(MODULE_NAME, "Form_Load", endlDebug, "Enter" & mstrTranId)

    If IsProcessRunning("TillFunction.exe") > 1 Then
        Dim res As Long
        res = MsgBox("You can only run 1 instance of the Till Application at a time.", vbOKOnly + vbExclamation)
        End
    End If
    
    frmSplash.Show vbModeless
    frmSplash.Refresh
    Call frmSplash.DisplayStatus("Connecting to Server..")
    
    Call GetRoot
    
    Call DebugMsg(MODULE_NAME, "Form_Load", endlDebug, "Got Root" & mstrTranId)
    
    ' Must come after GetRoot as need initalised goSession
    If EnableDotnetOfflineModeSetting Then
        Call SetDotnetOfflineMode
    End If
    
    Call DebugMsg(MODULE_NAME, "Form_Load", endlDebug, "PRM910='" & goSession.GetParameter(910) & "'")
    Call DebugMsg(MODULE_NAME, "Form_Load", endlDebug, "PRM911='" & goSession.GetParameter(911) & "'")
  
    
    Call DebugMsg(MODULE_NAME, "Form_Load", endlDebug, "PRM110000='" & goSession.GetParameter(PRM_VAT_UPDATE) & "'")
    
    Call ReceiptLogsHousekeeping
    
    flSaleGiftCard = False
    
    'System uses parameter 110000 to get either determine if new VAT Rate.  System checks if DATE then knows it not applied yet
    'if valid date then retrieve parameter that has date at start of Stringvalue column adn then step through vat rates and apply
    'in format 01/01/2001-01-12-2040;ax17,bz15
    
    'Added 26/11/08 - if VAT Rate update has passed then apply
    If (IsDate(goSession.GetParameter(PRM_VAT_UPDATE)) = True) Then
        If (Date >= CDate(goSession.GetParameter(PRM_VAT_UPDATE))) Then 'apply new VAT Rate
                        
            'VAT Rates update due, so apply and mark as processed
            Call ApplyNewVATRates
            
            'Get Parameter so that it can be flagged as processed
            Set oParameterBO = goDatabase.CreateBusinessObject(CLASSID_PARAMETER)
            Call oParameterBO.AddLoadFilter(CMP_EQUAL, FID_PARAMETER_String, goSession.GetParameter(PRM_VAT_UPDATE))
            Call oParameterBO.LoadMatches
            oParameterBO.Value = "APPLIED(" & Format(Now(), "HH:NN DD/MM/YY") & ")" & oParameterBO.Value
            Call oParameterBO.IBo_SaveIfExists
            
        End If
    End If
    
    mblnIsHotSpotImageRequired = GetBooleanParameter(-105, goDatabase.Connection)
        
    If (goSession.ISession_IsOnline = True) Then
        ImgSplash.Visible = mblnIsHotSpotImageRequired
    Else
        imgCustomerCommitments.Visible = mblnIsHotSpotImageRequired
    End If
    
    mlngEntryMode = True
    mlngCurrentMode = True
    mblnHasEFT = True
    mblnHasCashDrawer = True
  
    'Get VAT Rates History
    Set oParameterBO = goDatabase.CreateBusinessObject(CLASSID_PARAMETER)
    Call oParameterBO.AddLoadFilter(CMP_GREATERTHAN, FID_PARAMETER_ID, PRM_VAT_UPDATE)
    Call oParameterBO.AddLoadFilter(CMP_LESSEQUALTHAN, FID_PARAMETER_ID, PRM_VAT_UPDATE + 999)
    Set mcolPreviousVATRates = oParameterBO.LoadMatches
    
    ' Commidea Into WSS
    ' Moved this to earlier point so can be done before Ocius Sentinel is loaded
    ' as need the values to initalise the Commidea object with.
    mstrTillNo = Right$("00" & goSession.CurrentEnterprise.IEnterprise_WorkstationID, 2)
    mstrStoreNo = Right$("000" & goSession.CurrentEnterprise.IEnterprise_StoreNumber, 3)
    ' ...and this one is because Commidea loading takes so long that if user starts typing
    ' in the user id label too early will go into training mode - related to referral 474
    mstrMaxValidCashier = GetLastValidUserID
    
    ' Launch Ocius Sentinel software if using their PEDs.
    ' (always a delay in PED handshaking that can throw off
    ' the PED login process, so if can launch now and log
    ' in at end of start up that'll all be done with by the
    ' time we get to 1st transaction)
    If TillUsingCommideaPED Then
        Call frmSplash.DisplayStatus("Loading Ocius Sentinel..")
        Call GetCommideaParameters
        Call FireUpCommideaSoftwareAndPED(True)
    End If
    ' End ofCommidea Into WSS
    
    'Carry on loading Till
    Call frmSplash.DisplayStatus("Loading Parameters..")
    On Error Resume Next
    strCNPTills = goSession.GetParameter(100000)
    If (Err.Number <> 0) Then
        Call MsgBoxEx("Unable to locate 'Customer Not Present Till List' parameter", vbInformation, "Parameter Not Found", , , , , RGBMsgBox_WarnColour)
    Else
        'compare Till ID against specific Tills List
        On Error Resume Next
        If (InStr(strCNPTills, goSession.CurrentEnterprise.IEnterprise_WorkstationID) > 0) Then mblnCNPTill = True
        Call Err.Clear
        'Check if store specific NCP tills have been configured
        strCNPTills = ""
        On Error GoTo 0
        On Error Resume Next
        strCNPTills = goSession.GetParameter(100000 + Val(goSession.CurrentEnterprise.IEnterprise_StoreNumber))
        If (Err.Number = 0) Then
            If (InStr(strCNPTills, goSession.CurrentEnterprise.IEnterprise_WorkstationID) > 0) Then
                mblnCNPTill = True
            Else
                mblnCNPTill = False
            End If
        End If
        Call Err.Clear
    End If

    'WIX1389-get Volume SKUs
    Call GetVolumeSKUs

    'WIX1378 - added Refund Customer Not present parameter
    strCNPTills = goSession.GetParameter(100000)
    If (Err.Number <> 0) Then
        Call MsgBoxEx("Unable to locate 'Refund Customer Not Present Till List' parameter", vbInformation, "Parameter Not Found", , , , , RGBMsgBox_WarnColour)
    Else
        'compare Till ID against specific Tills List
        If (InStr(strCNPTills, goSession.CurrentEnterprise.IEnterprise_WorkstationID) > 0) Then mblnCNPTill = True
        Call Err.Clear
        'Check if store specific CNP tills have been configured
        strCNPTills = ""
        On Error Resume Next
        strCNPTills = goSession.GetParameter(100000 + Val(goSession.CurrentEnterprise.IEnterprise_StoreNumber))
        If (Err.Number = 0) Then
            If (InStr(strCNPTills, goSession.CurrentEnterprise.IEnterprise_WorkstationID) > 0) Then
                mblnRefCNPTill = True
            Else
                mblnRefCNPTill = False
            End If
        End If
        Call Err.Clear
    End If

    mblnUseWERates = goSession.GetParameter(PRM_USE_WEEE_RATES)
    
   'WIX1165 - for Coupons check if Web Service is available and system is online to access it
    mblnCouponWSActive = goSession.ISession_IsOnline
    If (mblnCouponWSActive = True) Then
        On Error Resume Next
        mstrCouponWSURL = goSession.GetParameter(PRM_COUPON_WS_URL)
        Err.Clear
        On Error GoTo Load_failed
        If (mstrCouponWSURL = "") Then mblnCouponWSActive = False
    Else
        mstrCouponWSURL = ""
    End If
    
    Call Err.Clear
    On Error GoTo Load_failed
    
    mstrCashDrawer = goSession.GetParameter(PRM_CASHDRAWERNAME)
    
    mstrMarkDownStyle = ""
    On Error Resume Next
    mstrMarkDownStyle = Replace(goSession.GetParameter(PRM_MARKDOWN_PREFIX), "$", "") 'MO'C WIX 1302
    Call Err.Clear
    On Error GoTo Load_failed
    
    mstrCRLPaidOutReasonCode = ""
    On Error Resume Next
    mstrCRLPaidOutReasonCode = goSession.GetParameter(PRM_CRL_PAIDOUT_CODE) 'MO'C WIX 1345
    Call Err.Clear
    On Error GoTo Load_failed
    
    mblnAllowCoupons = False
    On Error Resume Next
    mblnAllowCoupons = goSession.GetParameter(PRM_ALLOW_COUPONS) 'MO'C WIX 1165
    Call moEvents.CheckSKUPrice("100803", 1, "000000", False, 0)   'Dummy Call to .Net to speed up loading of first sku
    Call Err.Clear
    On Error GoTo Load_failed
    
    Call DebugMsg(MODULE_NAME, "Form_Load", endlDebug, "Got First Parameter" & mstrTranId)
    
    mstrBarCodeName = goSession.GetParameter(PRM_BARSCANNERNAME)
        
    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
    RGBMSGBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
    
'    ucKeyPad1.Left = 240
    
    Set mcolWEEESKUs = New Collection
    
    Call GetCreditCardNames
    
    Call frmSplash.DisplayStatus("Loading Printer Files..")
    
    Set oWorkStationBO = goDatabase.CreateBusinessObject(CLASSID_WORKSTATIONCONFIG)
    Call oWorkStationBO.AddLoadFilter(CMP_EQUAL, FID_WORKSTATIONCONFIG_id, goSession.CurrentEnterprise.IEnterprise_WorkstationID)
    Call oWorkStationBO.AddLoadField(FID_WORKSTATIONCONFIG_PrinterType)
    Call oWorkStationBO.LoadMatches
    
    mstrPrinterType = oWorkStationBO.PrinterType
    
    If mstrPrinterType <> WORKSTATIONCONFIG_PRNTTYPE_for_nontill Then
    
        Dim mbytPrintMode As Byte
        Dim mblnPrinterInitialized As Boolean
        
        'mbytPrintMode = goSession.GetParameter(PRM_PRINTERMODE)
        mbytPrintMode = goSession.GetParameter(PRM_PRINTERMODE)
        
        If mbytPrintMode = PRINTMODE_TO_PRINTER Then
        
            mblnPrinterInitialized = GetPrinterHeaderTrailer(OPOSPrinter1)
            
            If Not mblnPrinterInitialized Then
                Call MsgBoxEx("Printer failed to Initialize." & vbCrLf & "Please reboot and retry Till." & _
                vbCrLf & "If you are seeing this on a second attempt, please contact the IT Service Desk.", _
                vbExclamation, "System configuration", , , , , RGBMsgBox_WarnColour)
                End
            End If
        
        End If
    
    End If
    
    If InitialiseCashDrawer = False Then
        Call MsgBoxEx("Cash drawer initialisation failed", vbExclamation, _
                      "System configuration", , , , , RGBMsgBox_WarnColour)
'        End
    End If
        
    On Error Resume Next
    mblnUseMultiCur = goSession.GetParameter(PRM_USE_MULTI_CURR)
    If (mblnUseMultiCur = True) Then Call LoadCurrencies
    On Error GoTo Load_failed
    
    Call frmSplash.DisplayStatus("Loading Option File..")
    Set moRetopt = goDatabase.CreateBusinessObject(CLASSID_RETAILEROPTIONS)
    If moRetopt.LoadMatches.Count < 1 Then
        Call MsgBoxEx("Retailer Options record missing", vbExclamation, _
                      "System Configuration", , , , , RGBMsgBox_WarnColour)
        End
    End If
    Call frmSplash.DisplayStatus("Initialising additional forms..")
    Set frmCapturePricePromDetails = New frmCapturePricePromDetails
    Set frmNotFound = New frmNotFound
    Set frmPostcode = New frmPostcode
    Set frmReprintReceipt = New frmReprintReceipt
    Set frmReverseItem = New frmReverseItem
    Set frmRetrieveTran = New frmRetrieveTran
    Set frmTender = New frmTender
    Set frmVerifyPwd = New frmVerifyPwd
    Set frmTokenScan = New frmTokenScan
    Set frmCustomerAddress = New frmCustomerAddress
    Set frmDiscountCardScheme = New frmDiscountCardScheme   'MO'C Added 07/06/07
    Set frmComplete = New frmComplete
    Set frmCoupon = New frmCoupon
    Set frmConcernRes = New frmConcernRes
    
    mdblLineQuantity = 1
    mstrRefundReason = vbNullString
    mblnQtyBeforeSKU = True
    mbytQtyEntry = 1  'dummy test code
    mstrCardNoMask = goSession.GetParameter(PRM_CARDNO_MASK)
    
    sprdLines.Col = COL_CUSTORDER
    sprdLines.ColHidden = True
    sprdLines.Col = COL_UNITS
    sprdLines.ColHidden = True
    sprdLines.Col = COL_QTYSELLIN
    sprdLines.ColHidden = True
    sprdLines.Col = COL_SALETYPE
    sprdLines.ColHidden = True
    sprdLines.Col = COL_COST
    sprdLines.ColHidden = True
    sprdLines.Col = COL_DISC
    sprdLines.ColHidden = True
    sprdLines.Col = COL_EXCTOTAL
    sprdLines.ColHidden = True
    sprdLines.Col = COL_VATTOTAL
    sprdLines.ColHidden = True
    sprdLines.Col = COL_ONHAND
    sprdLines.ColHidden = True
    sprdLines.Col = COL_CUSTONO
    sprdLines.ColHidden = True
    sprdLines.Col = COL_VATRATE
    sprdLines.ColHidden = True
    sprdLines.Col = COL_ITEMTAG
    sprdLines.ColHidden = True
    sprdLines.Col = COL_VATCODE
    sprdLines.ColHidden = True
    sprdLines.Col = COL_TENDERTYPE
    sprdLines.ColHidden = True
    sprdLines.Col = COL_RELITEM
    sprdLines.ColHidden = True
    sprdLines.Col = COL_CATCHALL
    sprdLines.ColHidden = True
    If EnableBuyCoupons Then
        sprdLines.Col = COL_CPN_STATUS
        sprdLines.ColHidden = True
    End If
    sprdLines.Col = COL_TOTCOST
    sprdLines.ColHidden = True
    sprdLines.Col = COL_LOOKEDUP
    sprdLines.ColHidden = True
    sprdLines.Col = COL_DEPTCODE
    sprdLines.ColHidden = True
    sprdLines.Col = COL_ORDERFACTOR
    sprdLines.ColHidden = True
    sprdLines.Col = COL_ORDERVALUE
    sprdLines.ColHidden = True
    sprdLines.Col = COL_LINETYPE
    sprdLines.ColHidden = True
    sprdLines.Col = COL_VOIDED
    sprdLines.ColHidden = True
    sprdLines.Col = COL_BARCODE
    sprdLines.ColHidden = True
    sprdLines.Col = COL_SUPPNO
    sprdLines.ColHidden = True
    sprdLines.Col = COL_DISCCODE
    sprdLines.ColHidden = True
    sprdLines.Col = COL_RFND_TRANID
    sprdLines.ColHidden = True
    sprdLines.Col = COL_RFND_DATE
    sprdLines.ColHidden = True
    sprdLines.Col = COL_RFND_TILLNO
    sprdLines.ColHidden = True
    sprdLines.Col = COL_RFND_LINENO
    sprdLines.ColHidden = True
    sprdLines.Col = COL_HCATEGORY
    sprdLines.ColHidden = True
    sprdLines.Col = COL_HGROUP
    sprdLines.ColHidden = True
    sprdLines.Col = COL_HSUBGROUP
    sprdLines.ColHidden = True
    sprdLines.Col = COL_HSTYLE
    sprdLines.ColHidden = True
    sprdLines.Col = COL_QTYBRKAMOUNT
    sprdLines.ColHidden = True
    sprdLines.Col = COL_QTYBRKCODE
    sprdLines.ColHidden = True
    sprdLines.Col = COL_DGRPAMOUNT
    sprdLines.ColHidden = True
    sprdLines.Col = COL_DGRPCODE
    sprdLines.ColHidden = True
    sprdLines.Col = COL_MBUYAMOUNT
    sprdLines.ColHidden = True
    sprdLines.Col = COL_MBUYCODE
    sprdLines.ColHidden = True
    sprdLines.Col = COL_HIERAMOUNT
    sprdLines.ColHidden = True
    sprdLines.Col = COL_HIERCODE
    sprdLines.ColHidden = True
    sprdLines.Col = COL_QURSUP
    sprdLines.ColHidden = True
    sprdLines.Col = COL_ORDERINFO
    sprdLines.ColHidden = True
    sprdLines.Col = COL_SIZE
    sprdLines.ColHidden = True

    sprdLines.Col = COL_TEMPAMOUNT
    sprdLines.ColHidden = True
    sprdLines.Col = COL_TEMPCODE
    sprdLines.ColHidden = True

    sprdLines.Col = COL_VOUCHER
    sprdLines.ColHidden = True

    sprdLines.Col = COL_MARKDOWN
    sprdLines.ColHidden = True
    
    sprdLines.Col = COL_REFREASON
    sprdLines.ColHidden = True
    sprdLines.Col = COL_VOUCHERSERIAL
    sprdLines.ColHidden = True
    sprdLines.Col = COL_SUPERVISOR
    sprdLines.ColHidden = True
    sprdLines.Col = COL_ORIGTRANVER
    sprdLines.ColHidden = True
    sprdLines.Col = COL_DLGIFTTYPE
    sprdLines.ColHidden = True
    sprdLines.Col = COL_DISCMARGIN
    sprdLines.ColHidden = True
    sprdLines.Col = COL_MDSERIAL_NUMB   'MO'C WIX1302
    sprdLines.ColHidden = True          'MO'C WIX1302
    sprdLines.Col = COL_MAX_HIE_DISC    'MO'C WIX1302
    sprdLines.ColHidden = True          'MO'C WIX1302
    
    sprdLines.Row = 1
    sprdLines.RowHidden = True
        
    fraTender.Left = 0
    fraActions.Left = 0
    fraTranType.Left = 0
    
    Call frmSplash.DisplayStatus("Initialising Fuzzy Matching..")
    Set frmItemFilter = New frmItemFilter
    Load frmItemFilter
    'Call ucItemFind.Initialise(goSession, Me)
    
    ' CR0034
    ' Move this to after starting up Commidea PED so can add PTID to new panel on the end
    ' Call InitialiseTillStatusBar(sbStatus, mstrTillNo, mstrPTID)
    ' End of CR0034
    
    Call Me.Show
    
    If (goSession.ISession_IsOnline = True) Then
        Me.BackColor = goSession.GetParameter(PRM_BRANDING_IMAGE_BACKCOLOUR)
        mlngBackColor = goSession.GetParameter(PRM_BACKCOLOUR)
        ImgSplash.Visible = False
        imgCustomerCommitments.Visible = True
        
    Else
        Me.BackColor = goSession.GetParameter(PRM_TILL_OFFLINE)
        mlngBackColor = Me.BackColor
        ImgSplash.Visible = True
        imgCustomerCommitments.Visible = False
        For lngRowNo = 1 To sprdTotal.MaxRows Step 1
            For lngColNo = 1 To sprdTotal.MaxCols Step 1
                sprdTotal.Col = lngColNo
                sprdTotal.Row = lngRowNo
                If (sprdTotal.BackColor = vbBlue) Then sprdTotal.BackColor = vbRed
            Next lngColNo
        Next lngRowNo
    End If
    
    
    fraLogon.BackColor = Me.BackColor
    fraEntry.BackColor = mlngBackColor
    fraMaxValue.BackColor = mlngBackColor
    cmdProcess.BackColor = mlngBackColor
    cmdViewLines.BackColor = mlngBackColor
    RGBQuery_BackColour = goSession.GetParameter(PRM_QUERY_BACKCOLOUR)
    fraQty.BackColor = mlngBackColor
    
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    mblnPrePrintLogo = goSession.GetParameter(PRM_PRINT_LOGO_AT_START)
    mblnRefundAddLines = goSession.GetParameter(PRM_REFUND_ADD_LINES)
    mblnAllowMixRefund = goSession.GetParameter(PRM_REFUND_MULT_TRANS)
    mdblMaxQty = goSession.GetParameter(PRM_MAX_LINE_QTY)
    mdblMaxValue = goSession.GetParameter(PRM_MAX_LINE_VALUE)
    mdblMaxQtyLoanItems = goSession.GetParameter(PRM_MAX_LOAN_ITEMS)
    mblnUseCashDrawer = goSession.GetParameter(PRM_CASH_DRAWER)
    
    ' Commidea Into WSS
    ' Launch Ocius Sentinel software if using their PEDs.
    ' (always a delay in PED handshaking that can throw off
    ' the PED login process, so launched at start of Form_Load
    ' so now perform a log in to force any updates to be initiated
    ' prior to actual transactions taking place.  Updates might involve
    ' a restart of the Ocius software so need to be able to
    ' get operatot to do that/automate it?
    If TillUsingCommideaPED Then
        ' Inital launch at start of Form_Load might not have worked
        ' Check for that and try again if not worked previously
        If Not mblnOciusSoftwareReady Then
            Call frmSplash.DisplayStatus("Attempting to load Ocius Sentinel again..")
            Call FireUpCommideaSoftwareAndPED(True)
            ' Need a delay before attempting a login
            Call Wait(4)
        End If
        Call frmSplash.DisplayStatus("Logging on to Pin Entry Device..")
        If Not FireUpCommideaSoftwareAndPED Then
            Call DebugMsg(MODULE_NAME, "Form_Load", endlDebug, "Restart required?")
            Call Unload(frmSplash)
            Call Unload(Me)
            End
        End If
    End If
    ' End of Commidea Into WSS
    
    ' CR0034
    ' Allow new code not to be active yet
    If ChangeRequestCR0034Active Then
        ' Moved this from above to here so after starting up Commidea PED therefore can add PTID to new panel on the end
        Call InitialiseTillStatusBar(sbStatus, mstrTillNo, mstrPTID)
    Else
        ' Don't send any PT ID info, so not get PTID panel showing at all
        Call InitialiseTillStatusBar(sbStatus, mstrTillNo)
    End If
    ' End of CR0034
    
    If CheckForFiles = False Then
        Unload Me
        Exit Sub
    End If
    Call Me.Show
    
    sprdTotal.Row = 1
    sprdTotal.Col = 1
    sprdTotal.BackColor = mlngBackColor
    sprdTotal.Col = 5
    sprdTotal.BackColor = mlngBackColor
    sprdTotal.Col = 9
    sprdTotal.BackColor = mlngBackColor
    sprdTotal.Col = 13
    sprdTotal.BackColor = mlngBackColor
    
    sprdLines.Col = COL_COST
'    sprdLines.ColHidden = False
    Me.Show
    
    sprdLines.MaxRows = 0
    
    Set moTillTotals = goDatabase.CreateBusinessObject(CLASSID_TILLTOTALS)
    Call moTillTotals.AddLoadFilter(CMP_EQUAL, FID_TILLTOTALS_TillNumber, mstrTillNo)
    
    Call frmSplash.DisplayStatus("Setting up transaction numbers..")
    Set oFSO = New FileSystemObject
    strFileName = goSession.GetParameter(PRM_TRANSNO)
    strFileName = strFileName & "transno." & goSession.CurrentEnterprise.IEnterprise_WorkstationID
    Call DebugMsg(MODULE_NAME, "Next Transaction number", endlDebug, "Looking for file " & strFileName)
    If oFSO.FileExists(strFileName) Then
        Set tsTranFile = oFSO.OpenTextFile(strFileName, ForReading, False)
        strLine = tsTranFile.ReadLine
        Call tsTranFile.Close
'        lngTranID = (Val(strLine) + 1) Mod 10000
'        If lngTranID = 0 Then lngTranID = 1
'        strLine = Format$(lngTranID, "0000")
    Else
                
        strLine = "0001"
        Set oTranID = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
        Dim strLastTranID As String
        Dim vntValue      As Variant
    
        Set oRow = goSession.Root.CreateUtilityObject("CRowSelector")
    
        'Create field for selection criteria Key into
        Set oField = oTranID.IBo_GetField(FID_POSHEADER_TranDate)
        oField.ValueAsVariant = Date
        Call oRow.AddSelection(CMP_EQUAL, oField)
        Set oField = oTranID.IBo_GetField(FID_POSHEADER_TillID)
        oField.ValueAsVariant = goSession.CurrentEnterprise.IEnterprise_WorkstationID
        Call oRow.AddSelection(CMP_EQUAL, oField)
        vntValue = goDatabase.GetAggregateValue(AGG_MAX, oTranID.GetField(FID_POSHEADER_TransactionNo), oRow)
        If (IsNull(vntValue) = True) Then
            strLastTranID = "0000"
        Else
            strLastTranID = vntValue
        End If
        If strLastTranID = "4999" Then
            'system has rolled round from 4999, so will have to step from bottom to find last entry
            For lngTran = 0 To 4 Step 1
                'Get Last Tran ID for each Thousand ie 1??? then 2??? until <> ?999
                Set oRow = goSession.Root.CreateUtilityObject("CRowSelector")
                'Create field for selection criteria Key into
                Set oField = oTranID.IBo_GetField(FID_POSHEADER_TranDate)
                oField.ValueAsVariant = Date
                Call oRow.AddSelection(CMP_EQUAL, oField)
                Set oField = oTranID.IBo_GetField(FID_POSHEADER_TillID)
                oField.ValueAsVariant = goSession.CurrentEnterprise.IEnterprise_WorkstationID
                Call oRow.AddSelection(CMP_EQUAL, oField)
                Set oField = oTranID.IBo_GetField(FID_POSHEADER_TransactionNo)
                oField.ValueAsVariant = lngTran & "%"
                Call oRow.AddSelection(CMP_LIKE, oField)
                vntValue = goDatabase.GetAggregateValue(AGG_MAX, oTranID.GetField(FID_POSHEADER_TransactionNo), oRow)
                'Check if NULL as this is okay to start from
                If (IsNull(vntValue) = True) Then
                    If lngTran > 0 Then
                        strLastTranID = lngTran - 1 & "999"
                    Else
                        strLastTranID = "0000"
                    End If
                    Exit For
                Else
                    strLastTranID = vntValue
                    If (Right$(strLastTranID, 3) <> "999") Then
                        Exit For
                    End If
                End If
            Next lngTran
        End If
        strLine = Format$(Val(strLastTranID) + 1, "0000")
        Set tsTranFile = oFSO.OpenTextFile(strFileName, ForWriting, True)
        Call tsTranFile.WriteLine(strLine)
        Call tsTranFile.Close
    End If
    mstrTranId = strLine
    
    Set oFSO = Nothing
    
    Call frmSplash.DisplayStatus("Loading Till Totals..")
    
    If moTillTotals.LoadMatches.Count <> 0 Then
        If moRetopt.AccountabilityType <> "C" Then
        
            If moTillTotals.SaleTotalKey = 0 Then
                Set moSaleTotals = goDatabase.CreateBusinessObject(CLASSID_SALETOTALS)
                moSaleTotals.FloatAmount = moRetopt.DefaultFloatAmount
                moSaleTotals.SaveIfNew
                
                moTillTotals.SaleTotalKey = moSaleTotals.SaleTotalKey
                moTillTotals.SaveIfExists
            Else
                Set moSaleTotals = goDatabase.CreateBusinessObject(CLASSID_SALETOTALS)
                Call moSaleTotals.AddLoadFilter(CMP_EQUAL, FID_SALETOTALS_SaleTotalKey, _
                                                moTillTotals.SaleTotalKey)
                If moSaleTotals.LoadMatches.Count = 0 Then
                    Call MsgBoxEx("Sale Totals record missing." & vbNewLine & "Creating initial values record", vbExclamation, _
                                  "System Configuration", , , , , RGBMsgBox_WarnColour)
                    moSaleTotals.SaleTotalKey = moTillTotals.SaleTotalKey
                    End
                End If
            End If
        End If
    Else
        With moTillTotals
            .TillNumber = mstrTillNo
            .CashierNumber = "999"
            .CurrentTransactionNumber = mstrTranId
            
            If moRetopt.AccountabilityType <> "C" Then
                Set moSaleTotals = goDatabase.CreateBusinessObject(CLASSID_SALETOTALS)
                moSaleTotals.FloatAmount = moRetopt.DefaultFloatAmount
                moSaleTotals.SaveIfNew
                
                .SaleTotalKey = moSaleTotals.SaleTotalKey
            End If
            
            .SaveIfNew
        End With
    End If
    
    Call frmSplash.DisplayStatus("Initialising Gift Vouchers..")
    Call SetupGiftVoucherConstants
    
    lRetValue = TerminateProcess(pInfo.hProcess, 0&)
    lRetValue = CloseHandle(pInfo.hThread)
    lRetValue = CloseHandle(pInfo.hProcess)
    
    Call frmSplash.DisplayStatus("Retrieving Workstation Configuration..")
    'Retrieve Workstation control and check if Barcode Reader working and if Touch Screen
    Set oWorkStationBO = goDatabase.CreateBusinessObject(CLASSID_WORKSTATIONCONFIG)
    Call oWorkStationBO.AddLoadFilter(CMP_EQUAL, FID_WORKSTATIONCONFIG_id, goSession.CurrentEnterprise.IEnterprise_WorkstationID)
    Call oWorkStationBO.AddLoadField(FID_WORKSTATIONCONFIG_BarcodeBroken)
    Call oWorkStationBO.AddLoadField(FID_WORKSTATIONCONFIG_UseTouchScreen)
    Call oWorkStationBO.AddLoadField(FID_WORKSTATIONCONFIG_QuoteTill)
    Call oWorkStationBO.AddLoadField(FID_WORKSTATIONCONFIG_UseCashDrawer)
    Call oWorkStationBO.AddLoadField(FID_WORKSTATIONCONFIG_OutletFunction)
    Call oWorkStationBO.AddLoadField(FID_WORKSTATIONCONFIG_UseEFT)
    Call oWorkStationBO.AddLoadField(FID_WORKSTATIONCONFIG_PrinterType)
    Call oWorkStationBO.LoadMatches
    
    mstrWorkstationType = oWorkStationBO.OutletFunction
    mblnAllowQuotes = oWorkStationBO.QuoteTill
    mblnHasCashDrawer = oWorkStationBO.UseCashDrawer
    mblnHasEFT = oWorkStationBO.UseEFT
    mstrPrinterType = oWorkStationBO.PrinterType
    If (oWorkStationBO.UseTouchScreen = False) Then
        sbStatus.Panels("KeyBoard").Picture = Nothing
        sbStatus.Panels("NumPad").Picture = Nothing
        cmdExitTill.Visible = False
        mblnShowKeypadIcon = False
        cmdVoidLineEsc.Visible = False
        cmdVoidLineOK.Visible = False
        cmdListOk.Visible = False
        cmdListCancel.Visible = False
        fraVoidLine.Height = fraVoidLine.Height - cmdVoidLineEsc.Height
    Else
        mblnShowKeypadIcon = True
    End If
        
    Call frmSplash.DisplayStatus("Initialising Barcode Scanner..")
    If InitialiseBarScanner = False Then
    End If
    Call Err.Clear 'just in case Barcode reader init failed
    
    Unload frmSplash
    
    Call SetUpActions
    
    txtUserID.Enabled = True    ' Commidea Into WSS.  Referral 474.  This textbox defaults to disabled to prevent logging in too soon.
    Call UserLogon
    
    If (Format(Date, "YYYYMMDD") >= goSession.GetParameter(PRM_POSTCODE_STARTDATE)) And (Format(Date, "YYYYMMDD") <= goSession.GetParameter(PRM_POSTCODE_ENDDATE)) Then
        mblnCapturePostCode = (MsgBoxEx("Capture " & IIf(goSession.GetParameter(PRM_COUNTRY_CODE) = "IE", "Regions", "Postcodes") & "?", vbQuestion + vbYesNo + vbDefaultButton1, _
                                        IIf(goSession.GetParameter(PRM_COUNTRY_CODE) = "IE", "Region", "Postcode") & " Survey", , , , , RGBQuery_BackColour) = vbYes)
    End If
    
    'WIX1201 - check if Barcode Reader working or not, if flagged as broken
    If (goSession.GetParameter(PRM_BARCODE_CHECK_FREQ) > 0) Then
        mblnBCodeCheck = True
        mstrBCodeAuditCode = "99"
        'WIX1201 - check if Barcode Reader working or not, if flagged as broken
        If (oWorkStationBO.BarcodeBroken = True) Then
            
            lRetValue = 0
            While (lRetValue = 0)
                lRetValue = MsgBoxEx("Barcode Reader recorded as broken on this Till" & vbNewLine & "Confirm Barcode Reader is Fixed or Broken.", vbYesNo, "Confirm Barcode Reader Broken", "Fixed", "Broken", , , RGBMSGBox_PromptColour)
            Wend
            If (lRetValue = vbNo) Then
                mstrBCodeBrokenCode = GetBrokenBarcodeCode
            Else
                Call oWorkStationBO.RecordBarcodeBroken(goSession.CurrentEnterprise.IEnterprise_WorkstationID, False) 'working so update status on W/S record
                mstrBCodeBrokenCode = ""
            End If
        Else
            mstrBCodeBrokenCode = ""
        End If
    End If
    Set oWorkStationBO = Nothing
        
'    'mintWhichDiscountCard = goSession.GetParameter(PRM_WHICH_DISCOUNT_SCHEME) '24/08/07 MO'C Added for Wix1311 referal#35
'    Set oPo = New cRequestMearsPO.clsGetMearsPO
'    mblnUseWebService = oPo.IsWebServiceAvailable("http://mcmweb.mearsgroup.co.uk/mcmwebservice/purchaseorder.asmx", "Topp5til@s")
'    If mblnUseWebService = False Then
'        Call MsgBoxEx("Mears trader contract purchase order number web service is off Line, manual authorisation will be required.", vbOKOnly, "Web Service Off Line", , , , , RGBMsgBox_WarnColour)
'    End If
'
'    If oPo.RequestPO("http://mcmweb.mearsgroup.co.uk/mcmwebservice/purchaseorder.asmx", "Topp5til@s", "001", "Test Store", _
'                  "01249832230", "Counter Staff", "0000001", "00000002", "0009999", "123456", "9999999") = False Then
'        Call DebugMsg(MODULE_NAME, "cmdSave_Click", endlDebug, "Calling WebService")
'        Call MsgBoxEx("Trader contract sale has been declined." & vbCrLf & "00000999", vbOKOnly, "Request Declined", , , , , RGBMsgBox_WarnColour)
'        'mstrContractAuthNum = ""
'        'cmdCancel.Value = True
'        Set oPo = Nothing
'        Exit Sub
'    End If

    
    Exit Sub
    
Load_failed:

    'Call Err.Report("Till", "FormLoad", 1, True, "Till Loading Error", "An error has occured when loading the till as detailed below : ")
    'End
    Resume Next

HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("Form_Load", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub ApplyNewVATRates()

    Dim oVATRates As cLookUps_Wickes.cVATRates
    Dim oParameterBO As cEnterprise_Wickes.cParameter
    Dim strValues As String
    Dim strRates() As String
    Dim lngVATPos As Long
    Dim lngValuePos As Long
    Dim curVATRate As Currency
    
    Set oVATRates = goDatabase.CreateBusinessObject(CLASSID_VATRATES)
    Call oVATRates.LoadMatches
    Set oParameterBO = goDatabase.CreateBusinessObject(CLASSID_PARAMETER)
    Call oParameterBO.AddLoadFilter(CMP_LIKE, FID_PARAMETER_String, goSession.GetParameter(PRM_VAT_UPDATE) & "-%")
    Call oParameterBO.LoadMatches
    
    strValues = Mid$(oParameterBO.Value, InStr(oParameterBO.Value, ";") + 1)
    strRates = Split(strValues, ",")
    
    For lngValuePos = LBound(strRates) To UBound(strRates) Step 1
        If (strRates(lngValuePos) <> "") Then
            strValues = strRates(lngValuePos)
            lngVATPos = Asc(Left$(strValues, 1)) - 96
            curVATRate = Val(Mid$(strValues, 3))
            oVATRates.VATRate(lngVATPos) = curVATRate
        End If
    Next
    
    oVATRates.IBo_SaveIfExists

End Sub 'ApplyNewVATRates

Private Sub UserLogon()

    sbStatus.Panels(PANEL_INFO).Text = "Cashier sign-on"
    fraLogon.Visible = True
    
    UpdateBrandingImageVisibility (True)
    
    fraTender.Visible = False
    fraTranType.Visible = False
    fraActions.Visible = False
    Call SetEntryMode(enemNone)
    mlngCurrentMode = encmLogon
    fraTrader.Visible = False
    sprdLines.Visible = False
    sprdTotal.Visible = False
    
    If (goSession.ISession_IsOnline) Then
        Me.BackColor = goSession.GetParameter(PRM_BRANDING_IMAGE_BACKCOLOUR)
    End If
    
    txtUserID.Visible = True
    lblLoggedUserID.Visible = False
    txtUserID.Refresh
    If (txtUserID.Enabled = True) And (txtUserID.Visible = True) Then Call txtUserID.SetFocus
    
End Sub
'<CACH>****************************************************************************************
'* Function:  Boolean GetItem()
'**********************************************************************************************
'* Description: Used to retrieve an Item given the SKU and display in spread sheet.  Also used
'*              to initialise line values to 0 as each item is selected/ displayed.
'**********************************************************************************************
'* Parameters:
'*In/Out:strPartCode  String.-Part Code used to retrieve item - must be ready formatted
'*In/Out:blnCheckItem Boolean.-As item is retrieved the status flag is checked to ignore obselete items
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 15/11/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Function GetItem(ByVal strPartCode As String, ByVal blnCheckItem As Boolean) As Boolean

Dim oItem       As cInventory
Dim oItemWickes As cInventoryWickes
Dim lRow        As Long
Dim sErrMsg     As String
Dim blnContinue As Boolean
Dim oEANBO      As cEAN
Dim strEANNo    As String
Dim oProdMsg    As clsProductMessaging
Dim oSKUGroups  As cItemGrpPrompts
Dim oGrpPrompts As cGroupPrompts

Dim strTempCode  As String
Dim dblTempPrice As Double

Dim strTokenSerial As String
Dim strTestSerial As String
Dim curTokenValue As Currency
Dim dteBlankDate  As Date
Dim curWEEECharge As Currency
Dim blnEANFound   As Boolean
Dim blnEANEntry   As Boolean
Dim blnInHouseEAN As Boolean
Dim blnInHouseErr As Boolean
Dim blnPriceAmend As Boolean
Dim strEANSKU     As String

Dim strSKU        As String
Dim strSerial     As String

Dim oEANCheckBO   As cEANCheck
Dim oMarkDown     As cMarkDownStock
Dim oAmendPrice   As cDayPrice
Dim strMsg        As String
Dim intIndex      As Integer
Dim intPosSKU     As Integer
Dim intPosSerial  As Integer
Dim intQty        As Long    'WIX1391 Total so far for the Eon scheme

Dim colPrices     As Collection 'WIX1165 to hold Prices to Amend

Const PROCEDURE_NAME As String = MODULE_NAME & ".GetItem"

Const EAN_MISMATCH_CODE As String = 93
Const EAN_MISS_CHECK_CODE As String = 98
Const PROMPT_GRP_QTY As Long = 6

    On Error GoTo Catch
Try:
    'Main procedure code goes here
                   
    If (strPartCode = mstrGiftVoucherSKU) And _
       (mlngCurrentMode <> encmGiftVoucher) And _
       (mdblLineQuantity > 0) Then
        mlngEntryMode = enemNone
        Call MsgBoxEx("This SKU is reserved for Gift Vouchers only", vbExclamation, _
                      "Reserved SKU Entered", , , , , RGBMsgBox_WarnColour)
        sprdLines.Text = vbNullString
        'Clear out Line Quantity for Gift vouchers - ref 2010-016 1.
        mdblLineQuantity = 1
        lblQty.Caption = CStr(mdblLineQuantity)
        fraQty.Visible = False
        mlngEntryMode = enemSKU
        txtSKU.Text = vbNullString
        If (txtSKU.Enabled = True) And (txtSKU.Visible = True) Then txtSKU.SetFocus
        mblnBarcoded = False
        Exit Function
    End If
    
'    If (strPartCode = mstrGiftVoucherSKU) And (mdblLineQuantity < 0) And mblnHaveRefPrice Then
'        frmTokenScan.GetTokenToSell
'        If frmTokenScan.Cancel Then
'            mdblLineQuantity = 1
'            mlngCurrentMode = encmRecord
'            Call SetEntryMode(enemSku)
'            Exit Function
'        End If
'        strTestSerial = frmTokenScan.ScanData
'
'        frmTokenScan.GetTokenForTender
'        If frmTokenScan.Cancel Then
'            mdblLineQuantity = 1
'            mlngCurrentMode = encmRecord
'            Call SetEntryMode(enemSku)
'            Exit Function
'        End If
'
'        Call DecodeGiftTokenBarcode(frmTokenScan.ScanData, strTokenSerial, curTokenValue)
'
'        If strTestSerial <> strTokenSerial Then
'            Call MsgBoxEx("Serial numbers do NOT match", vbOKOnly & vbExclamation, _
'                          "Invalid Gift Token", , , , , RGBMsgBox_WarnColour)
'            mdblLineQuantity = 1
'            mlngCurrentMode = encmRecord
'            Call SetEntryMode(enemSku)
'            Exit Function
'        End If
'
'        If GiftVoucherValid(strTokenSerial, vbnullstring) = False Then
'            mdblLineQuantity = 1
'            mlngCurrentMode = encmRecord
'            Call SetEntryMode(enemSku)
'            Exit Function
'        End If
'
'        If GiftVoucherNotUsed(strTokenSerial, True) = False Then
'            mdblLineQuantity = 1
'            mlngCurrentMode = encmRecord
'            Call SetEntryMode(enemSku)
'            Exit Function
'        End If
'
'        mdblRefundPrice = curTokenValue
'        sprdLines.Col = COL_VOUCHERSERIAL
'        sprdLines.Text = strTokenSerial
'
'    End If

    If Left(txtSKU.Text, 1) = Left(mstrMarkDownStyle, 1) And Len(txtSKU.Text) > 6 Then 'MO'C WIX 1302
        
        If MarkDownBarCodeCheck(txtSKU.Text) Then
            'Valid Bar Code
            intPosSKU = InStr(mstrMarkDownStyle, "S")
            intPosSerial = InStr(mstrMarkDownStyle, "N")
            strSerial = Mid(strPartCode, intPosSerial, 6)
            strPartCode = Mid(strPartCode, intPosSKU, 6)
            Set oMarkDown = goDatabase.CreateBusinessObject(CLASSID_MARKDOWNSTOCK)
            Call oMarkDown.AddLoadFilter(CMP_EQUAL, FID_MARKDOWNSTOCK_SKUNumber, strPartCode)
            Call oMarkDown.AddLoadFilter(CMP_EQUAL, FID_MARKDOWNSTOCK_Serial, strSerial)
            Call oMarkDown.LoadMatches
            
            'No Such Markdown found
            If Len(oMarkDown.SKUNumber) = 0 Then
                mlngEntryMode = enemNone
                strMsg = goSession.GetParameter(965001)
                Call MsgBoxEx(strMsg, vbExclamation + vbOKOnly, "Markdown Stock", , , , , _
                     RGBMsgBox_WarnColour)
                mlngEntryMode = enemSKU
                txtSKU.Text = vbNullString
                If (txtSKU.Enabled = True) And (txtSKU.Visible = True) Then txtSKU.SetFocus
                GetItem = False
                Exit Function
            End If
            
            'This item has been sold already raise message
            If Val(oMarkDown.SoldTransaction) > 0 Then
                mlngEntryMode = enemNone
                Call DisplayMarkdownMessage(oMarkDown)
                mlngEntryMode = enemSKU
                txtSKU.Text = vbNullString
                If (txtSKU.Enabled = True) And (txtSKU.Visible = True) Then txtSKU.SetFocus
                GetItem = False
                Exit Function
            End If
            
            'This item has been written off
            If oMarkDown.DateWrittenOff <> "00:00:00" Then
                mlngEntryMode = enemNone
                Call DisplayMarkdownMessage(oMarkDown)
                mlngEntryMode = enemSKU
                txtSKU.Text = vbNullString
                If (txtSKU.Enabled = True) And (txtSKU.Visible = True) Then txtSKU.SetFocus
                GetItem = False
                Exit Function
            End If
            
            'Store the serial nmber for when saving the line
            sprdLines.Col = COL_MDSERIAL_NUMB
            sprdLines.Text = oMarkDown.Serial

        Else
            'Invalid Barcode entered reject
            mlngEntryMode = enemNone
            Call MsgBoxEx("Invalid markdown barcode entered.", vbExclamation + vbOKOnly, "Markdown Barcode", , , , , _
                 RGBMsgBox_WarnColour)
            mlngEntryMode = enemSKU
            txtSKU.Text = vbNullString
            If (txtSKU.Enabled = True) And (txtSKU.Visible = True) Then txtSKU.SetFocus
            GetItem = False
            Exit Function
        
        End If
    
    ElseIf Len(strPartCode) > 6 Then
        'check if Part Code is actually an EAN so look up as EAN
        strEANNo = Right$("0000000000000000" & strPartCode, 16)
        blnEANEntry = True
        Set oEANBO = goDatabase.CreateBusinessObject(CLASSID_EAN)
        Call oEANBO.AddLoadFilter(CMP_EQUAL, FID_EAN_EANNumber, strEANNo)
        Call oEANBO.LoadMatches
        If LenB(oEANBO.PartCode) = 0 Then  'EAN not found then down grade to In House EAN
            'Chop off all pre-leading Zero
            While (Left$(strEANNo, 1) = "0") And (Len(strEANNo) > 6)
                strEANNo = Mid$(strEANNo, 2)
            Wend
            'Check if First Character is a 2 and there are 8 characters
            'This is to notate a Client based EAN
            If (Left$(strEANNo, 1) = "2") And (Len(strEANNo) = 8) Then
                strPartCode = Mid$(strEANNo, 2, 6)
                blnInHouseErr = True
            Else
                mblnStopScanning = True
                Load frmNotFound
                Call frmNotFound.DisplayNotFound(strEANNo)
                Unload frmNotFound
                'Added 28/1/08 - Referral WIX2007-056 to capture correct SKU
                strSKU = ""
                Set frmEANCheck = New frmEANCheck
                Load frmEANCheck
                frmEANCheck.UserID = txtUserID.Text
                frmEANCheck.ucKeyPad1.Visible = ucKeyPad1.Visible
                frmEANCheck.txtSKU.Text = strPartCode
                strSKU = frmEANCheck.GetCorrectSKU(mblnBarcoded, False)
                Unload frmEANCheck
                
                Set oEANCheckBO = CreateEANReject(mblnBarcoded, moTranHeader.TransactionNo, EAN_MISS_CHECK_CODE, txtSKU.Text, "EAN", False, "000000", False, strSKU, txtUserID.Text)
                If (goSession.ISession_IsOnline = False) Then Call moTranHeader.AddOfflineEANCheck(oEANCheckBO)
                mblnBarcoded = False
                txtSKU.Text = vbNullString
                If (strSKU <> "") Then 'if new SKU then try and process
                    GetItem = GetItem(strSKU, True)
                    If (GetItem = True) Then
                        sprdLines.Col = COL_REJECT_ID
                        sprdLines.Text = oEANCheckBO.EntryID
                    End If
                End If
                If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then txtSKU.SetFocus
                Exit Function
            End If
        Else
            'for barcode logging check if EAN is an In-House EAN
            'Chop off all pre-leading Zero
            While (Left$(strEANNo, 1) = "0") And (Len(strEANNo) > 6)
                strEANNo = Mid$(strEANNo, 2)
            Wend
            'Check if First Character is a 2 and there are 8 characters
            'This is to notate a Client based EAN
            If (Left$(strEANNo, 1) = "2") And (Len(strEANNo) = 8) Then
                blnInHouseEAN = True
            End If

            blnEANFound = True
            strPartCode = oEANBO.PartCode
            strEANSKU = strPartCode 'save for later saving into EANReject
        End If
    End If
    
    lRow = sprdLines.Row
    Set oItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
    Call oItem.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, strPartCode)
    Call oItem.AddLoadField(FID_INVENTORY_PartCode)
    Call oItem.AddLoadField(FID_INVENTORY_Description)
    Call oItem.AddLoadField(FID_INVENTORY_SupplierNo)
    Call oItem.AddLoadField(FID_INVENTORY_ItemObsolete)
    Call oItem.AddLoadField(FID_INVENTORY_SupplierPackSize)
    Call oItem.AddLoadField(FID_INVENTORY_CostPrice)
    Call oItem.AddLoadField(FID_INVENTORY_QuantityAtHand)
    Call oItem.AddLoadField(FID_INVENTORY_LastSold)
    Call oItem.AddLoadField(FID_INVENTORY_VATRate)
    Call oItem.AddLoadField(FID_INVENTORY_LastOrdered)
    Call oItem.AddLoadField(FID_INVENTORY_NormalSellPrice)
    
    Call oItem.AddLoadField(FID_INVENTORY_CatchAll)
    Call oItem.AddLoadField(FID_INVENTORY_RelatedItemSingle)
    Call oItem.AddLoadField(FID_INVENTORY_RelatedNoItems)
    Call oItem.AddLoadField(FID_INVENTORY_SupplierNo)
    Call oItem.AddLoadField(FID_INVENTORY_ItemTagged)
    Call oItem.AddLoadField(FID_INVENTORY_ItemTagType)
    Call oItem.AddLoadField(FID_INVENTORY_Warranty)
    Call oItem.AddLoadField(FID_INVENTORY_Weight)
    Call oItem.AddLoadField(FID_INVENTORY_ItemVolume)
    Call oItem.AddLoadField(FID_INVENTORY_QuantityOnOrder) 'WIX1243 - show Qty on Purchase Orders
    
    If (mblnUseWERates = True) Then
        Call oItem.AddLoadField(FID_INVENTORY_PRFSKU)
    End If
    
    'call oitem.AddLoadField(FID_INVENTORY_
    Call oItem.LoadMatches
    
    If LenB(oItem.Description) = 0 Then  'Item not found
        mblnStopScanning = True
        Load frmNotFound
        Call frmNotFound.DisplayNotFound(strPartCode)
        Unload frmNotFound
        'Only add if not a sku, and
'        If Len(txtSKU.Text) > 6 And mblnBarcoded = False Then
            'Added 28/1/08 - Referral WIX2007-056 to capture correct SKU
        strSKU = ""
        If (blnEANEntry = True) Or (blnInHouseErr = True) Then
            Set frmEANCheck = New frmEANCheck
            Load frmEANCheck
            frmEANCheck.UserID = txtUserID.Text
            frmEANCheck.ucKeyPad1.Visible = ucKeyPad1.Visible
            frmEANCheck.txtSKU.Text = strPartCode
            strSKU = frmEANCheck.GetCorrectSKU(mblnBarcoded, False)
            Unload frmEANCheck
        End If
            
        Set oEANCheckBO = CreateEANReject(mblnBarcoded, moTranHeader.TransactionNo, IIf(blnEANEntry, IIf(blnInHouseEAN, "94", IIf(blnInHouseErr, "96", "97")), IIf(blnInHouseErr, "96", "95")), txtSKU.Text, IIf(blnEANEntry, IIf(blnInHouseEAN, "IN-HOUSE", IIf(blnInHouseErr, "IN-HOUSE", "EAN")), "SKU"), blnEANFound, strEANSKU, False, strSKU, txtUserID.Text)
        If (goSession.ISession_IsOnline = False) Then Call moTranHeader.AddOfflineEANCheck(oEANCheckBO)
        mblnBarcoded = False
        txtSKU.Text = vbNullString
        If (strSKU <> "") Then 'if new SKU then try and process
            GetItem = GetItem(strSKU, True)
            If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then txtSKU.SetFocus
        End If
 '       End If
        
        Exit Function
    End If
    
    'Added 18/11/08 - disable Quote recal after 1st valid item
    Call EnableAction(enacRecallQuote, False)
    
    sprdLines.LeftCol = sprdLines.ColsFrozen  'ensure moved to start of grid
    
    Set oItemWickes = goDatabase.CreateBusinessObject(CLASSID_INVENTORYWICKES)
    Call oItemWickes.AddLoadFilter(CMP_EQUAL, FID_INVENTORYWICKES_PartCode, strPartCode)
    Call oItemWickes.LoadMatches
    
        'For refunded items - need Manager Auth for Refund Delivery Changes
    If (oItemWickes.SaleTypeAttribute = "W") Then
        Call MsgBoxEx("Entered SKU is reserved for Product Recycling Fund charges.", vbOKOnly, "PRF Item Entered", _
                                , , , , RGBMsgBox_WarnColour)
        mlngEntryMode = enemSKU
        sprdLines.Text = vbNullString
        txtSKU.Text = vbNullString
        If (txtSKU.Enabled = True) And (txtSKU.Visible = True) Then txtSKU.SetFocus
        mblnBarcoded = False
        Exit Function
    End If
    
    If Not oMarkDown Is Nothing Then
        'MO'C WIX1302 - Work out Price
        If oMarkDown.Serial <> "" Then
            oItem.NormalSellPrice = GetMarkDownPrice(oMarkDown, oItem, oItemWickes)
            'Check here if the price is ok, if not get manager auth
            'This item has been sold already raise message
            If oItem.NormalSellPrice = 0 Then
                mlngEntryMode = enemNone
                Call DisplayMarkdownMessage(oMarkDown)
                mlngEntryMode = enemSKU
                txtSKU.Text = vbNullString
                If (txtSKU.Enabled = True) And (txtSKU.Visible = True) Then txtSKU.SetFocus
                mblnBarcoded = False
                GetItem = False
                Exit Function
            End If
        End If
    End If
    
    If (mdblLineQuantity > 0) Then 'selling item out, so perform checks
        'Item Message
        If (mblnRecallingParked = False) Then
            If oItem.ItemObsolete And oItem.QuantityAtHand <= 0 Then
                'WIX1201- check if loading HOSTU file using RETOPTS Flags
                Dim oRetOptBO As cRetailOptions
                Set oRetOptBO = goSession.Database.CreateBusinessObject(CLASSID_RETAILEROPTIONS)
                Call oRetOptBO.AddLoadField(FID_RETAILEROPTIONS_SystemUpload)
                Call oRetOptBO.LoadMatches
                If (oRetOptBO.SystemUpload = False) Then
                    mlngEntryMode = enemNone
                    If MsgBoxEx("Item Set - Obsolete" & vbNewLine & "Accept?", vbYesNo, "Obsolete Item", _
                                , , , , RGBMSGBox_PromptColour) = vbNo Then
                        mlngEntryMode = enemSKU
                        sprdLines.Text = vbNullString
                        txtSKU.Text = vbNullString
                        If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then
                            txtSKU.SetFocus
                        Else
                            Call MsgBoxEx("Item has been removed - please re-quote.", vbOKOnly, "Obsolete Item Removed", , , , , RGBMSGBox_PromptColour)
                        End If
                        mblnBarcoded = False
                        Exit Function
                    End If
                    
                    If frmVerifyPwd.VerifySupervisorPassword(ucKeyPad1.Visible) = False Then
                        sprdLines.Text = vbNullString
                        txtSKU.Text = vbNullString
                        If (txtSKU.Enabled = True) And (txtSKU.Visible = True) Then txtSKU.SetFocus
                        mblnBarcoded = False
                        Exit Function
                    End If
                    moTranHeader.SupervisorUsed = True
                End If
            End If 'item is obselete and requires supervisor authorisation
        End If 'recalling parked
                    
       'WIX1391 - EON Light Bulbs
        Dim intRow As Integer
        intRow = sprdLines.Row
        sprdLines.Row = ROW_TOTALS
        sprdLines.Col = COL_GRP_PROMPT_QTY
        intQty = mdblLineQuantity + GetPromptGroupQty() 'Val(sprdLines.Text)
        sprdLines.Row = intRow
                    
        Set oSKUGroups = goDatabase.CreateBusinessObject(CLASSID_ITEM_GRPPROMPT)
        If (oSKUGroups.GetGroupsForSKU(strPartCode) = True) Then
            Set oGrpPrompts = goDatabase.CreateBusinessObject(CLASSID_GROUP_PROMPT)
            If (oGrpPrompts.GetPromptsForGroupID(oSKUGroups.PromptGroupID) = True) Then
                mlngEntryMode = enemNone
                frmItemPrompts.strUserId = txtUserID.Text
                If (frmItemPrompts.ProcessPrompts(oItem, oItemWickes, oGrpPrompts, _
                        mcolLinePrints, mcolTrailerPrints, mcolValues, mlngHighestAgePassed, mlngLowestAgeFailed, mcolPromptRejects, ucKeyPad1.Visible, 0, intQty, New Collection, False) = False) Then
                    mlngEntryMode = enemSKU
                    Set oGrpPrompts = Nothing
                    Set oSKUGroups = Nothing
                    sprdLines.Text = vbNullString
                    txtSKU.Text = vbNullString
                    If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then txtSKU.SetFocus
                    mblnBarcoded = False
                    Unload frmItemPrompts
                    Exit Function
                End If
                If Val(oSKUGroups.PromptGroupID) = PROMPT_GRP_QTY Then
                    'sprdLines.Row = ROW_TOTALS
                    sprdLines.Col = COL_GRP_PROMPT_QTY  'WIX1391 - Add to the Group Total for line checking
                    sprdLines.Text = CStr(mdblLineQuantity)
                End If
                mlngEntryMode = enemSKU
                Unload frmItemPrompts
            End If 'no prompts so just carry on
            Set oGrpPrompts = Nothing
        End If
        Set oSKUGroups = Nothing
        
        ' Solvent/Weapon/Quarantine Messages
        Set oProdMsg = New clsProductMessaging
        
        oProdMsg.HighestAgePassed = mlngHighestAgePassed
        oProdMsg.LowestAgeFailed = mlngLowestAgeFailed
        mlngEntryMode = enemNone
'        Set oProdMsg.Product = oItem
        
        If oProdMsg.AbortItemSale Then
            If (oProdMsg.AgeInQuestion < mlngLowestAgeFailed) Or (mlngLowestAgeFailed = 0) Then
                mlngLowestAgeFailed = oProdMsg.AgeInQuestion
            End If
            
            sprdLines.Text = vbNullString
            txtSKU.Text = vbNullString
            If (txtSKU.Enabled = True) And (txtSKU.Visible = True) Then txtSKU.SetFocus
            mlngEntryMode = enemSKU
            mblnBarcoded = False
            Exit Function
        End If
        
        If oProdMsg.AgeInQuestion > mlngHighestAgePassed Then
            mlngHighestAgePassed = oProdMsg.AgeInQuestion
        End If
                
        If (oProdMsg.HasWarranty) And (mblnRecallingParked = False) Then
            Do
                Call frmCustomerAddress.GetWarrantyAddress(mstrName, mstrPostCode, mstrAddress, mstrTelNo)
                If frmCustomerAddress.Cancel Then
                    sprdLines.MaxRows = sprdLines.MaxRows - 1
                    Call SetEntryMode(enemSKU)
                    Exit Function
                End If
            Loop While frmCustomerAddress.Cancel = True
        End If 'warranty requires address and not recalling parked tran
                        
        Set oProdMsg = Nothing
    
        'Item requires a warranty document
        If oItem.Warranty = True Then
            mlngEntryMode = enemNone
            Call MsgBoxEx(oItem.PartCode & "-" & oItem.Description & vbNewLine & "Please issue a warranty document with this product", vbOKOnly, _
                          "WARNING: Warranty document required", , , , , RGBMsgBox_WarnColour)
        End If
    End If 'item is being sold

    'For refunded items - need Manager Auth for Refund Delivery Changes
    If (mdblLineQuantity < 0) And (oItemWickes.SaleTypeAttribute = "D") Then
        Load frmVerifyPwd
        frmVerifyPwd.Caption = "Verify-Refunding Delivery Only Item"
        If frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) = False Then
            Unload frmVerifyPwd
            mdblLineQuantity = 1
            SetEntryMode (enemSKU)
            Exit Function
        End If
        Unload frmVerifyPwd
    End If
    
    If LenB(sErrMsg) <> 0 Then
        sprdLines.Col = COL_DESC
        sprdLines.Text = sErrMsg
        sbStatus.Panels(PANEL_INFO).Text = sprdLines.Text
        sprdLines.Col = COL_ITEMNO
        sprdLines.Col2 = COL_SIZE
        sprdLines.Row2 = sprdLines.Row
        sprdLines.BlockMode = True
        sprdLines.BackColor = RGB_RED
        sprdLines.BlockMode = False
        Screen.MousePointer = vbNormal
        If sprdLines.Redraw = True Then
            Call sprdLines.SetActiveCell(COL_PARTCODE, lRow)
            If (sprdLines.Enabled = True) And (sprdLines.Visible = True) Then Call sprdLines.SetFocus
            sprdLines.EditMode = True
        End If
        GetItem = False
        mlngEntryMode = enemSKU
        mblnBarcoded = False
        Exit Function
    End If

    sprdLines.Col = COL_ITEMNO
    sprdLines.Col2 = COL_SIZE
    sprdLines.Row2 = sprdLines.Row
    sprdLines.BlockMode = True
    sprdLines.BackColor = RGB_WHITE
    sprdLines.BlockMode = False
    sprdLines.Col = COL_ITEMNO
    sprdLines.Text = sprdLines.Row
    sprdLines.Col = COL_PARTCODE
    sprdLines.Text = oItem.PartCode
    sprdLines.Col = COL_DESC
    sprdLines.Text = oItem.Description
    sbStatus.Panels(PANEL_INFO).Text = strPartCode & "-" & sprdLines.Text
    sprdLines.Col = COL_SIZE
    'sprdLines.Text = oItem.SizeDescription
    sprdLines.Col = COL_UNITS
    'sprdLines.Text = oItem.SellInPacksOf
    sprdLines.Col = COL_QTY
    sprdLines.Text = 0
    sprdLines.Col = COL_EXCTOTAL
    sprdLines.Text = 0
    sprdLines.Col = COL_COST
    sprdLines.Text = oItem.CostPrice
    sprdLines.Col = COL_ONHAND
    sprdLines.Text = oItem.QuantityAtHand
    sprdLines.Col = COL_QTYONORDER
    sprdLines.Text = oItem.QuantityOnOrder
    sprdLines.Col = COL_VATCODE
    sprdLines.Text = Chr$(oItem.VATRate + 96)
    sprdLines.Col = COL_SALETYPE
    sprdLines.Text = oItemWickes.SaleTypeAttribute
    sprdLines.Col = COL_ITEMTAG
    sprdLines.Text = Trim$(oItem.ItemTagType)
    sprdLines.Col = COL_WEEE_RATE
    sprdLines.Value = 0
    If (Val(oItem.PRFSKU) > 0) Then curWEEECharge = GetWEEERate(oItem.PRFSKU)
    sprdLines.Value = curWEEECharge
    sprdLines.Col = COL_WEEE_SKU
    sprdLines.Value = oItem.PRFSKU
    'WIX1202 - record keyed in reason code
    sprdLines.Col = COL_KEYED_RC
    sprdLines.Text = mstrKeyedCode
    sprdLines.Col = COL_KEYED_SKU
    sprdLines.Text = mstrKeyedSKU
    
    sprdLines.Col = COL_BARCODE
    If (mblnBarcoded = True) Then sprdLines.Text = "1"
    If moEvents Is Nothing Then
        Set moEvents = New cTillEvents_Wickes.cOPEvents
        Call moEvents.Initialise(goSession)
    End If

    Dim InCorrectDayPriceOverrideEvent As Boolean
    Dim ExistingTemporaryPriceSkuEventPrice As Double

    blnPriceAmend = False
    'Call moEvents.ClearSKUS
    If mdblLineQuantity > 0 Then
        
        dblTempPrice = moEvents.CheckSKUPrice(strPartCode, mdblLineQuantity, strTempCode, InCorrectDayPriceOverrideEvent, ExistingTemporaryPriceSkuEventPrice)
        
        If (mstrAmendPriceReason <> "") Then
            If (mcolPriceAmends Is Nothing) Then 'not loaded yet, so populate
                Set oAmendPrice = goSession.Database.CreateBusinessObject(CLASSID_DAYPRICE)
                Call oAmendPrice.IBo_AddLoadFilter(CMP_EQUAL, FID_DAYPRICE_DateCreated, Date)
                Set colPrices = oAmendPrice.LoadMatches
                Set oAmendPrice = Nothing
                Set mcolPriceAmends = New Collection
                For Each oAmendPrice In colPrices
                    Call mcolPriceAmends.Add(oAmendPrice, "" & oAmendPrice.PartCode)
                Next
            End If
                
            On Error Resume Next
            Set oAmendPrice = mcolPriceAmends(strPartCode)
            On Error GoTo 0
            Call Err.Clear
            If (oAmendPrice Is Nothing = False) Then
                dblTempPrice = oAmendPrice.NewPrice
                strTempCode = mstrAmendPriceReason
                blnPriceAmend = True
            End If
        End If
    End If
    If (dblTempPrice = 0) Or (dblTempPrice > oItem.NormalSellPrice) Then 'no temporary price - so use normal price
        If mdblLineQuantity < 0 And mblnHaveRefPrice Then
            sprdLines.Col = COL_SELLPRICE
            sprdLines.Value = mcurRefundPrice
        Else
            sprdLines.Col = COL_SELLPRICE
            sprdLines.Value = IIf(mlngCurrentMode = encmPaidOut, 0, oItem.NormalSellPrice + curWEEECharge) 'MO'C WIX 1345 - If Concerned Products there free
        End If
        sprdLines.Col = COL_TEMPCODE
        sprdLines.Text = vbNullString
        sprdLines.Col = COL_TEMPAMOUNT
        sprdLines.Value = 0
    Else
        sprdLines.Col = COL_SELLPRICE
        sprdLines.Value = dblTempPrice + curWEEECharge
        If (blnPriceAmend = False) Then
            '******************************************************
            'Added to show in Price Violations report  ????????????
            sprdLines.Col = COL_DISCCODE
            sprdLines.Text = "11"
            sprdLines.Col = COL_DISCMARGIN
            sprdLines.Text = "000011"
            '******************************************************
            
            sprdLines.Col = COL_TEMPCODE
            sprdLines.Text = strTempCode
            sprdLines.Col = COL_TEMPAMOUNT
            sprdLines.Value = IIf(mlngCurrentMode = encmPaidOut, 0, oItem.NormalSellPrice - dblTempPrice) 'MO'C WIX 1345 - If Concerned Products there free
        Else 'Price Amendment so record Discount Reason Code
            sprdLines.Col = COL_DISCCODE
            sprdLines.Text = strTempCode
            sprdLines.Col = COL_DAY_PRICE
            sprdLines.Text = dblTempPrice
        End If
    End If

    'if "incorrect day price" event then classed as a "price override" rather than a "TS" event
    If InCorrectDayPriceOverrideEvent = True Then
        'dlline: porc
        sprdLines.Col = COL_DISCCODE
        sprdLines.Text = "11"
        
        'dlline:tppd field from being populated
        sprdLines.Col = COL_TEMPAMOUNT
        If ExistingTemporaryPriceSkuEventPrice = 0 Then
            sprdLines.Value = 0
        Else
            sprdLines.Value = oItem.NormalSellPrice - ExistingTemporaryPriceSkuEventPrice
        End If
        'dlline:pome
        sprdLines.Col = COL_DISCMARGIN
        sprdLines.Text = "000011"

    End If

    sprdLines.Col = COL_SELLPRICE
    If (mdblLineQuantity > 0) Then
        Call moEvents.AddSKU(strPartCode, mdblLineQuantity, sprdLines.Value - curWEEECharge, sprdLines.Row, _
                             oItemWickes.HierCategory, oItemWickes.HierGroup, _
                             oItemWickes.HierSubGroup, oItemWickes.HierStyle)
    End If
    sprdLines.Col = COL_LOOKEDUP
    sprdLines.Text = IIf(mlngCurrentMode = encmPaidOut, 0, oItem.NormalSellPrice) 'MO'C WIX 1345 - If Concerned Products there free
    sprdLines.Col = COL_QTYSELLIN
    'sprdLines.Text = oItem.QuantityToUOM(oItem.SellInUnitOccNo)
    sprdLines.Col = COL_VATRATE
    sprdLines.Text = moVATRates.VATRate(oItem.VATRate)
    
    If (mdblLineQuantity < 0) Then
        Call CheckVATHistory(mdteRefOrigTranDate)
    End If

    sprdLines.Col = COL_INCTOTAL
    sprdLines.Formula = ColToText(COL_QTY) & "# * " & ColToText(COL_SELLPRICE) & "#" ' / " & ColToText(COL_QTYSELLIN) & "#"
    sprdLines.Col = COL_TOTCOST
    sprdLines.Formula = ColToText(COL_QTY) & "# * " & ColToText(COL_COST) & "#" ' / " & ColToText(COL_QTYSELLIN) & "#"
    sprdLines.Col = COL_EXCTOTAL
    sprdLines.Formula = ColToText(COL_INCTOTAL) & "# / (1 + " & ColToText(COL_VATRATE) & "# / 100)"
    sprdLines.Col = COL_VATTOTAL
    sprdLines.Formula = ColToText(COL_INCTOTAL) & "# - " & ColToText(COL_EXCTOTAL) & "#"
    sprdLines.Col = COL_DEPTCODE
    'sprdLines.Text = oItem.DepartmentCode
    sprdLines.Col = COL_DEPTGRPCODE
    'sprdLines.Text = oItem.Group
    sprdLines.Col = COL_ORDERFACTOR
    sprdLines.Text = 0
    sprdLines.Col = COL_ORDERVALUE
    sprdLines.Text = 0
    sprdLines.Col = COL_BACKCOLLECT
    sprdLines.Text = "N"
    sprdLines.Formula = ColToText(COL_QTY) & "# * " & ColToText(COL_SELLPRICE) & "#"
    sprdLines.Col = COL_SUPPNO
    sprdLines.Text = oItem.SupplierNo
    sprdLines.Col = COL_CORD_SUPP
    sprdLines.Text = 0 'flag item as not requiring ordering for Supplier
    'Determine if any stock movements should be applied
    sprdLines.Col = COL_APPLY_TO_STOCK
    If moTOSType.Code = TT_SALE Or moTOSType.Code = TT_REFUND Or moTOSType.Code = TT_RECALL Then
        sprdLines.Value = 1
    Else
        sprdLines.Value = 0
    End If
    sprdLines.ColHidden = True
    
    sprdLines.Col = COL_HCATEGORY
    sprdLines.Text = oItemWickes.HierCategory
    sprdLines.Col = COL_HGROUP
    sprdLines.Text = oItemWickes.HierGroup
    sprdLines.Col = COL_HSUBGROUP
    sprdLines.Text = oItemWickes.HierSubGroup
    sprdLines.Col = COL_HSTYLE
    sprdLines.Text = oItemWickes.HierStyle
    
    'Initialise Events values - set to 0
    sprdLines.Col = COL_QTYBRKAMOUNT
    sprdLines.Value = 0
    sprdLines.Col = COL_DGRPAMOUNT
    sprdLines.Value = 0
    sprdLines.Col = COL_MBUYAMOUNT
    sprdLines.Value = 0
    sprdLines.Col = COL_HIERAMOUNT
    sprdLines.Value = 0
    
    sprdLines.Col = COL_RELITEM
    sprdLines.Value = IIf(oItem.RelatedItemSingle, "True", vbNullString)
    
    If mdblLineQuantity < 0 Then
        sprdLines.Col = COL_ORIGTRANVER
        sprdLines.Text = IIf(mblnTokensOnly, "False", "True")
    End If
    
    sprdLines.Col = COL_WEIGHT
    sprdLines.Value = oItem.Weight
    sprdLines.Col = COL_VOLUME
    'WIX1389 -check if Volume SKU and use Text value if set
    Dim strVolume As String
    strVolume = oItem.ItemVolume
    If oItem.ItemVolume = 0 Then
        On Error Resume Next
        strVolume = mcolVolumeSKUs(strPartCode)
        On Error GoTo Catch
    End If
    sprdLines.Value = Val(strVolume) 'oItem.ItemVolume
    sprdLines.Col = COL_TOTALVOL
    sprdLines.Value = 0
    sprdLines.Formula = "IF(" & ColToText(COL_QTY) & "#>0," & ColToText(COL_QTY) & "#,0) * " & ColToText(COL_VOLUME) & "#"
    
    Set oItem = Nothing
    GetItem = True

    mlngEntryMode = enemSKU
    'Tidy up
    GoSub Finally

    mblnBarcoded = False
    
    If mblnRecallingParked Then
        Call EnableAction(enacRecallQuote, False)
        Call EnableAction(enacCreateOrder, False)   'To Do
        Call EnableAction(enacOrder, False)
    End If
    Exit Function

    '<CAEH>
Catch:
    Call Err.Push 'Preserve Error Code
    'Tidy up.
    GoSub Finally

    Call Err.Pop 'Retrieve Error Code
    'Re-raise error to next handler up the chain.
    Call Err.Bubble(APP_NAME, PROCEDURE_NAME, CLng(Erl))
    mblnBarcoded = False
    Exit Function
Resume Next
Finally:
    'Perform tidy up here

    Return
    '</CAEH>

End Function      'GetItem

Private Sub sprdLines_EditMode(ByVal Col As Long, ByVal Row As Long, _
                               ByVal Mode As Integer, ByVal ChangeMade As Boolean)

    On Error GoTo HandleException

    If Mode = 0 Then
        sprdLines.Col = Col
        sprdLines.Row = Row
        If Col = COL_PARTCODE Then
            If GetItem(sprdLines.Text, False) = False Then
                sprdLines.MaxRows = sprdLines.MaxRows - 1
                mblnValidItem = False
                Exit Sub
            End If
            mblnValidItem = True
            Call SetGridTotals(Row)
            Call UpdateTotals
        Else
            Exit Sub
        End If
        Call sprdLines.SetActiveCell(Col, Row)
        sprdLines.EditMode = True
    End If

    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("sprdLines_EditMode", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub sprdLines_EnterRow(ByVal Row As Long, ByVal RowIsLast As Long)

'    lblPartCode.Caption = "Part Code " & Row
'    lblOnHand.Caption = 2 * Row

End Sub

Private Sub sprdLines_LeaveRow(ByVal Row As Long, ByVal RowWasLast As Boolean, _
                               ByVal RowChanged As Boolean, ByVal AllCellsHaveData As Boolean, _
                               ByVal NewRow As Long, ByVal NewRowIsLast As Long, Cancel As Boolean)

Dim blnMoved As Boolean
Dim blnPPLine   As Boolean
Dim lngRowNo As Long

    On Error GoTo HandleException
    
    sprdLines.Row = NewRow
    
    Select Case (mlngCurrentMode)
        Case (encmReversal):
            blnMoved = False
            If (Row > NewRow) Then 'Moving Up
                If NewRow = 2 Then
                     NewRow = NewRow
                End If
                For lngRowNo = NewRow To 2 Step -1
                    sprdLines.Row = lngRowNo
                    sprdLines.Col = COL_VOIDED
                    If (Val(sprdLines.Text) <> 1) And (Val(sprdLines.Text) <> 2) Then
                        sprdLines.Col = COL_PPLINE
                        blnPPLine = (sprdLines.Text = "1")
                        sprdLines.Col = COL_LINETYPE
                        If ((sprdLines.Text <> LT_PRICEPROM) And (blnPPLine = False)) And (sprdLines.Text <> LT_EVENT) Then
                            Call sprdLines.SetSelection(-1, sprdLines.Row, -1, sprdLines.Row)
                            blnMoved = True
                            Exit For
                        End If
                    End If
                Next
                If blnMoved = False Then
                    sprdLines.Row = Row
                    Cancel = True
                Else
'                    KeyCode = 0
                    Call DisplayVoidLine
                End If
            Else 'Moving Down
                For lngRowNo = NewRow To sprdLines.MaxRows Step 1
                    sprdLines.Row = lngRowNo
                    sprdLines.Col = COL_VOIDED
                    If (Val(sprdLines.Text) <> 1) And (Val(sprdLines.Text) <> 2) Then
                        sprdLines.Col = COL_PPLINE
                        blnPPLine = (sprdLines.Text = "1")
                        sprdLines.Col = COL_LINETYPE
                        If (sprdLines.Text <> LT_PRICEPROM) And (blnPPLine = False) And (sprdLines.Text <> LT_EVENT) Then
                            Call sprdLines.SetSelection(-1, sprdLines.Row, -1, sprdLines.Row)
                            blnMoved = True
                            Exit For
                        End If
                    End If
                Next
                If blnMoved = False Then
                    sprdLines.Row = Row
                    Cancel = True
                Else
'                    KeyCode = 0
                    Call DisplayVoidLine
                End If
            End If
    End Select
    'lblPartCode.Caption = "Part Code " & NewRow
    'lblOnHand.Caption = 2 * NewRow

    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("sprdLines_LeaveRow", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Function SaveSale(blnParked As Boolean, _
                          blnVoided As Boolean, _
                          blnQuote As Boolean, _
                          blnSaveOrder As Boolean) As Boolean

Dim oPOSLine      As cPOSLine
Dim oPOSPmt       As cPOSPayment
Dim oQuoteTran    As cPOSHeader
Dim oOrderHdrBO   As cCustOrderHeader
Dim dblAmount     As Double
Dim dblCCPmt      As Double
Dim dblNonCCPmt   As Double
Dim strTranCode   As String
Dim lngLineNo     As Long
Dim lngTotalPos   As Long
Dim lngFirstPmt   As Long
Dim lngSeqNo      As Long ' used to hold Line Item DB Line No
Dim lngPPPos      As Long
Dim lngOPDescPos  As Long 'used to locate description of OP Events
Dim blnCustDone   As Boolean 'used to ensure only 1 DLRCUS written for DLTOTS
Dim colLines      As Collection
Dim colOPLines    As Collection
Dim arlngEvent()  As Long
Dim blnOPEvents   As Boolean
Dim blnHSEvents   As Boolean
Dim curTotalOP    As Currency
Dim curItemDisc   As Currency
Dim vntAddress    As Variant
Dim strReasonCode As String
Dim strKeyedValue As String
Dim strSKU        As String
Dim curLineTotals As Currency
Dim blnVouchers   As Boolean
Dim ReprintCount  As Integer    ' Commidea Into WSS - Referral 517 - Add receipt re-print number to receipt
Dim moTranHeaderCheck As cPOSHeader
Dim colCouponsUsed As Collection
Dim index As Integer

    On Error GoTo HandleException
    
    Call SetEntryMode(enemNone)
    fraActions.Visible = False
    
    ucpbStatus.Visible = True
    
     'reset totals for VAT Discount calculations
    dblCCPmt = 0
    dblNonCCPmt = 0
    moTranHeader.Voided = blnVoided
    moTranHeader.TranParked = blnParked
    moTranHeader.FromDCOrders = (blnParked And (blnQuote = False)) 'double flag required as Customer Orders causing conflict
    moTranHeader.EmployeeDiscountOnly = mblnApplyCollDisc
    moTranHeader.StoreNumber = mstrLocation
    moTranHeader.DiscountCardNumber = mstrDiscCardNo
    If mblnTransactionDiscountApplied = True Then
        moTranHeader.DiscountAmount = mdblPercentageValue
        moTranHeader.Description = vbNullString
    End If
    
    If LenB(mstrAddress) <> 0 Then
        vntAddress = Split(mstrAddress, vbNewLine)
        mstrAddressLine1 = vntAddress(0)
        mstrAddressLine2 = vntAddress(1)
        mstrAddressLine3 = vntAddress(2)
    End If
    
    strTranCode = mstrStoreNo & mstrTillNo & mstrTranId & Format$(moTranHeader.TranDate, "DDMMYY")
    
    ucpbStatus.Caption1 = "Printing Receipt Header"
    
    If ((moTranHeader.GetPricePromiseLine Is Nothing) = False) Then
        If (moTranHeader.GetPricePromiseLine.Count = 0) Then
            Call moTranHeader.ClearPricePromiseCust
        End If
    End If
    
    'Print Information for an account sale invoice
    If LenB(mstrAccountNum) <> 0 Then
        Call RetrieveHeadOfficeAddress
'        Call PrintAccountDepartmentAddress(OPOSPrinter1, mstrHeadofficename, _
            mstrHeadOfficeAddress, mstrHeadOfficePhoneNo, mstrHeadOfficeFaxNumber, _
            mstrVATNumber)
'        Call PrintInvoiceHeader(OPOSPrinter1, mstrAccountNum, mstrAccountName, _
            mstrActive, mstrCardExpiry, mstrAddressLine1, mstrAddressLine2, mstrAddressLine3, _
            mstrPostCode, mstrHomeTelNumber, mstrInvoiceNumber, False)
    End If
    
    'Print TeleOrderNum on receipt if required
'    If LenB(mstrTeleOrderNum) <> 0 Then Call PrintTeleOrderNum(OPOSPrinter1, mstrTeleOrderNum)
    
    ucpbStatus.Caption1 = "Storing Line Items"
    ucpbStatus.Value = 0
    If sprdLines.MaxRows > 1 Then ucpbStatus.Max = sprdLines.MaxRows - 1
    
    If mcolOPSummary.Count > 0 Then
        blnOPEvents = True
    Else
        blnOPEvents = False
    End If
    
    If mcolHSSummary.Count > 0 Then
        blnHSEvents = True
    Else
        blnHSEvents = False
    End If
     'Extract transaction total and put into header
    sprdLines.Row = ROW_TOTALS
    sprdLines.Col = COL_INCTOTAL
    moTranHeader.TotalSaleAmount = Val(sprdLines.Value)
    If (Abs(moTranHeader.TotalSaleAmount) > 9999999) Then moTranHeader.TotalSaleAmount = 0
   
    '********************************************************************************************
    'Due to legistation - this has been deemed illegal - so until resolved has been commented out
    '********************************************************************************************
    'Add up total value of Credit Card and Non Credit Card payments
    'dblCCPmt = 0
    'dblNonCCPmt = 0
    'If ((moTranHeader.Payments Is Nothing) = False) Then
    '    For Each oPOSPmt In moTranHeader.Payments
    '        If (oPOSPmt.TenderType = TEN_CREDCARD) Or (oPOSPmt.TenderType = TEN_DEBITCARD) Then
    '            dblCCPmt = dblCCPmt + Abs(oPOSPmt.TenderAmount)
    '        Else
    '            dblNonCCPmt = dblNonCCPmt + Abs(oPOSPmt.TenderAmount)
    '        End If
    '    Next
    'End If
    'Call DebugMsg(MODULE_NAME, "SaveSale", endlDebug, "CCAmt=" & dblCCPmt & " Non=" & dblNonCCPmt)
    'If (dblCCPmt > 0) Then dblCCPmt = dblCCPmt / (dblNonCCPmt + dblCCPmt) * 100
    'Call DebugMsg(MODULE_NAME, "SaveSale", endlDebug, "CC Perc=" & dblCCPmt & "%")
    '********************************************************************************************
   
   Call DebugMsg(MODULE_NAME, "SaveSale", endlDebug, "Preprocessing Price Matches")
   'step through each line locate any price promise/match to apply to proceeding line
    For lngLineNo = 2 To sprdLines.MaxRows Step 1
        ucpbStatus.Value = lngLineNo - 1
        sprdLines.Row = lngLineNo
        'Process line if a Price Promise - reduce Item price by Discount amount
        sprdLines.Col = COL_LINETYPE
        If (Val(sprdLines.Value) = LT_PRICEPROM) Then
            'if line is a Price Promise/Match - then update price on item
            sprdLines.Col = COL_SELLPRICE
            curItemDisc = Abs(sprdLines.Value)
            'step up to previous line item and change price to force recalc
            sprdLines.Col = COL_LINETYPE
            While (Val(sprdLines.Value) <> LT_ITEM) And (sprdLines.Row > 1)
                sprdLines.Row = sprdLines.Row - 1
            Wend
            sprdLines.Col = COL_SELLPRICE
            sprdLines.Value = Val(sprdLines.Value) - curItemDisc
'            sprdLines.Col = col_
        End If
    Next lngLineNo
    
    If EnableBuyCoupons Then
        Set colCouponsUsed = New Collection
        If Not mcolCouponsInTransaction Is Nothing Then
            For index = 1 To mcolCouponsInTransaction.Count
                colCouponsUsed.Add (mcolCouponsInTransaction.Item(index)) 'PO14 - ref 928
            Next index
        End If
    End If
   
    ' 13/08/14 Replace each GC tender with GC sku for Refund
    If moTOSType.Code = TT_REFUND Then
        Dim oPmt As cPOSPayment
        Dim intPmtIndex As Integer
        intPmtIndex = 1
        
        For lngLineNo = 2 To sprdLines.MaxRows Step 1
            sprdLines.Row = lngLineNo
            sprdLines.Col = COL_LINETYPE
            Select Case (Val(sprdLines.Value))
                Case (LT_PMT):
                    sprdLines.Col = COL_TENDERTYPE
                    If Val(sprdLines.Value) = TEN_GIFTCARD Then
                        sprdLines.Col = COL_INCTOTAL
                        Call AddVoucherSKUs(Val(sprdLines.Value), , , lngLineNo)
                        Set oPmt = moTranHeader.Payments(intPmtIndex)
                        moTranHeader.TotalSaleAmount = moTranHeader.TotalSaleAmount + oPmt.TenderAmount
                        mdblTotalBeforeDis = mdblTotalBeforeDis + oPmt.TenderAmount
                        oPmt.TenderAmount = 0
                        blnVouchers = True
                    End If 'TEN_GIFTCARD
                    intPmtIndex = intPmtIndex + 1
            End Select
        Next lngLineNo
    End If
    
    'step through each line and save
    For lngLineNo = 2 To sprdLines.MaxRows Step 1
        Call DebugMsg(MODULE_NAME, "SaveSale", endlDebug, "Saving Line" & lngLineNo)
        ucpbStatus.Value = lngLineNo - 1
        sprdLines.Row = lngLineNo
        'Ensure only lines that are SKU's
        sprdLines.Col = COL_LINETYPE
        Select Case (Val(sprdLines.Value))
            Case (LT_TOTAL):    lngTotalPos = lngLineNo
            Case (LT_PMT):
                                If lngFirstPmt = 0 Then lngFirstPmt = lngLineNo
            Case (LT_ITEM):
                                lngSeqNo = 0
                                'Check line is not part of Customer Sale
'                                sprdLines.Col = COL_VOIDED
'                                If (Val(sprdLines.Value) <= 2) Then
                                Call SaveTranLine(oPOSLine, sprdLines.Row, dblCCPmt, blnCustDone, blnVoided)
                                sprdLines.Col = COL_DB_LINENO
                                lngSeqNo = Val(sprdLines.Text)
'                                End If
                                
                                sprdLines.Col = COL_KEYED_RC
                                If (sprdLines.Text <> "") Then
                                    sprdLines.Col = COL_VOIDED
                                    If (Val(sprdLines.Value) < 1) Then
                                        sprdLines.Col = COL_VOUCHER
                                        If sprdLines.Text <> "V" Then
                                            sprdLines.Col = COL_KEYED_RC
                                            strReasonCode = sprdLines.Text
                                            sprdLines.Col = COL_KEYED_SKU
                                            strKeyedValue = sprdLines.Text
                                            sprdLines.Col = COL_PARTCODE
                                            strSKU = sprdLines.Text
                                            Call moTranHeader.AddKeyedAudit(strReasonCode, strKeyedValue, strSKU)
                                        End If 'Not a voucher so save
                                    End If 'Not a Reversal Line
                                End If 'Keyed Audit Code so save
                                sprdLines.Col = COL_PARTCODE
                                If (sprdLines.Text = mstrGiftCardSKU) Then blnVouchers = True

            Case (LT_EVENT):
                                'Add the Event into the DLEVNT table
'                                If EnableBuyCoupons Then
'                                    If blnQuote = False Then 'PO14-01 Don't Save Event Info If saving a quote
'                                        Call SaveEventLine(lngSeqNo, sprdLines.Row)
'                                        'Check if Event occured is last event for line, else update line
'                                        sprdLines.Col = COL_OP_EVENTSEQNO
'                                        If (Val(oPOSLine.LastEventSequenceNo) < Val(sprdLines.Value)) Then
'                                            oPOSLine.LastEventSequenceNo = sprdLines.Text
'                                        End If
'                                    End If
'                                Else
                                    Call SaveEventLine(lngSeqNo, sprdLines.Row)
                                    'Check if Event occured is last event for line, else update line
                                    sprdLines.Col = COL_OP_EVENTSEQNO
                                    If (Val(oPOSLine.LastEventSequenceNo) < Val(sprdLines.Value)) Then
                                        oPOSLine.LastEventSequenceNo = sprdLines.Text
                                    End If
'                                End If
            Case (LT_PRICEPROM):
                                'if line is a Price Promise/Match - then update
                                'Sequence number to it matches Item It applies to
                                'Check line has not been voided
                                sprdLines.Col = COL_DB_LINENO
                                sprdLines.Text = lngSeqNo
                                sprdLines.Col = COL_PP_POS
                                moTranHeader.GetPricePromiseLine(CLng(sprdLines.Text)).SequenceNo = lngSeqNo
 '                               sprdLines.Col = COL_SELLPRICE
 '                               oPosLine.PriceOverrideAmount = Abs(sprdLines.Value)
 '                               'step up to previous line and change price to force recalc
 '                               sprdLines.Row = sprdLines.Row - 1
 '                               sprdLines.Col = COL_SELLPRICE
 '                               sprdLines.oPosLine.PriceOverrideAmount = Abs(sprdLines.Value)
 '                               oPosLine.ActualSellPrice = oPosLine.ActualSellPrice + sprdLines.Value
            
            Case (LT_COUPON):
                                If EnableBuyCoupons Then
                                    If blnVoided = False And blnQuote = False Then
                                        Call SaveCouponNew(sprdLines.Row, blnQuote, colCouponsUsed)
                                    End If
                                Else
                                    Call SaveCoupon(sprdLines.Row, blnQuote)
                                End If
        End Select
    Next lngLineNo
    
    ucpbStatus.Caption1 = "Calculating Offers and Promotion Totals"
    'Print the Discount Summary if there are any discounts
    If (mblnTransactionDiscountApplied = True) Or (blnOPEvents = True) Or (blnHSEvents = True) Then
        curTotalOP = 0
        If blnOPEvents = True Then
            For lngLineNo = 1 To mcolOPSummary.Count Step 1
                curTotalOP = curTotalOP + mcolOPSummary(lngLineNo).dblDiscTotal
            Next lngLineNo
        End If
        If blnHSEvents = True Then
            For lngLineNo = 1 To mcolHSSummary.Count Step 1
                curTotalOP = curTotalOP + mcolHSSummary(lngLineNo).dblDiscTotal
            Next lngLineNo
        End If
    End If
    
    
    'Extract transaction total and put into header
'    moTranHeader.MerchandiseAmount = Val(sprdLines.Value)
    moTranHeader.MerchandiseAmount = mdblTotalBeforeDis
    sprdLines.Col = COL_VATTOTAL
    
    moTranHeader.TaxAmount = 0
    For lngLineNo = 1 To 9
        If blnParked = False Then
            moTranHeader.TaxAmount = moTranHeader.TaxAmount + moTranHeader.VATValue(lngLineNo)
        Else
            moTranHeader.ExVATValue(lngLineNo) = 0
            moTranHeader.VATValue(lngLineNo) = 0
        End If
    Next lngLineNo
    
    If (Abs(moTranHeader.TaxAmount) > 9999999) Then moTranHeader.TaxAmount = 0
    
'    If (blnParked = False) And (blnVoided = False) Then Call PrintDivideLine(OPOSPrinter1)
'    Call goDatabase.StartTransaction
    Call DebugMsg(MODULE_NAME, "SaveSale", endlDebug, "Started saving tran-" & mstrTranId)
    'Write Customer Number to TranHeader
    If LenB(mstrAccountNum) <> 0 Then
        moTranHeader.CustomerAcctNo = mstrAccountNum
    End If
    
    Call DebugMsg(MODULE_NAME, "SaveSale", endlDebug, "Submitting " & moTranHeader.Payments.Count & " EFT Tran(s)-Online(" & goSession.ISession_IsOnline & ")Voided=(" & blnVoided & ")")
    
    moTranHeader.TransactionComplete = True
    If goSession.ISession_IsOnline Then
        moTranHeader.Offline = False
    Else
        moTranHeader.Offline = True
    End If
    
    'Added 9/11/07 - pass in Item Prompts list to add to history file
    Set moTranHeader.PromptRejects = mcolPromptRejects
    If mblnDiscountColleague Then
        moTranHeader.EmployeeDiscountOnly = True
    End If
    
    
    'Added 30/12/2011 - Ensure Recall Quote Process works fine when Coupon is typed in
    If (moTranHeader.IBo_SaveIfExists = False) Then
        Call Err.Clear
        Call MsgBoxEx("ERROR : A critical error has occurred when completing Transaction to Database." & _
            "System is unable to proceed and will exit." & vbCrLf & vbCrLf & "Desc :" & moTranHeader.SaveFailedStatus & _
            vbCrLf & "Contact IT Support immediately", vbExclamation, "System Failure", , , , , RGBMsgBox_WarnColour)
        On Error Resume Next
        Call Err.Raise(1, "SaveSale_Click", "Unable to save to Database")
        Call Err.Report(MODULE_NAME, "SaveSale", 0, False)
        End
    End If

      
    If (blnVoided = False) And (blnParked = False) Then
        Call ZReadUpdate("TR", moTOSType.Code)
        Call UpdateFlashTotals(False, IIf(moTOSType.Code = TT_SALE, enftSales, enftRefunds), _
                               moTranHeader.TotalSaleAmount)
    Else
        Call UpdateFlashTotals(False, enftVoidedTransactions, moTranHeader.TotalSaleAmount)
    End If

    Call CheckTranTotals(moTranHeader)

    
    Dim strReturnCoupons As String
    Dim CouponsTendered As Boolean
    Dim strDescription As String
    Dim noUnusableCoupons As Integer
    
    If EnableBuyCoupons Then
        'Step through List looking for any coupons that might have been used in the transaction
        For lngLineNo = 2 To sprdLines.MaxRows Step 1
            Call DebugMsg(MODULE_NAME, "SaveSale", endlDebug, "Checking for Coupons" & lngLineNo)
            sprdLines.Row = lngLineNo
            'Ensure only lines that are SKU's
            sprdLines.Col = COL_LINETYPE
            Select Case Val(sprdLines.Value)
                Case (LT_COUPON):
                    sprdLines.Col = COL_CPN_STATUS
                    ' Only want buy coupons!
                    If Val(sprdLines.Text) <> 9 Then
                        CouponsTendered = True
                        Exit For
                    End If
            End Select
        Next lngLineNo
        
        If CouponsTendered And ((Not mcolCouponsInTransaction Is Nothing) Or blnParked Or blnVoided) Then
            If (blnQuote = True And mblnCreatingOrder = False) Or (blnVoided = True Or blnParked = True) Then
                If mblnCreatingOrder = False Then
                    Call MsgBoxEx("Return coupon(s) to customer." & vbCrLf & vbCrLf & vbCrLf & "Please return all the coupon(s) to the Customer.", vbOKOnly, "Return Coupons", , , , , RGBMSGBox_PromptColour)
                End If
            Else
                
                For lngLineNo = 1 To mcolCouponsInTransaction.Count
                    If CouponReuseable(mcolCouponsInTransaction.Item(lngLineNo), strDescription) = False Then
                        strReturnCoupons = strReturnCoupons & Space(15) & strDescription & vbCrLf
                        noUnusableCoupons = noUnusableCoupons + 1
                    End If
                Next lngLineNo
                If strReturnCoupons <> "" Then
                    If noUnusableCoupons < mcolCouponsInTransaction.Count Then
                        Call MsgBoxEx("The following coupon(s) cannot be used again..." & vbCrLf & vbCrLf & vbCrLf & Replace(strReturnCoupons, " & ", " && ", Compare:=vbTextCompare) & vbCrLf & vbCrLf & "Please return the rest to the Customer.", vbOKOnly, "Return Coupons", , , , , RGBMSGBox_PromptColour)
                    Else
                        Dim NoUnusedCoupons As Integer
                        
                        NoUnusedCoupons = GetNumberOfCouponsScannedIntransaction()
                        If NoUnusedCoupons > 0 Then
                            Call MsgBoxEx("The following coupon(s) cannot be used again..." & vbCrLf & vbCrLf & vbCrLf & Replace(strReturnCoupons, " & ", " && ", Compare:=vbTextCompare) & vbCrLf & vbCrLf & "Please return the " & CStr(NoUnusedCoupons) & " unused coupons to the Customer.", vbOKOnly, "Return Coupons", , , , , RGBMSGBox_PromptColour)
                        Else
                            Call MsgBoxEx("The following coupon(s) cannot be used again..." & vbCrLf & vbCrLf & vbCrLf & Replace(strReturnCoupons, " & ", " && ", Compare:=vbTextCompare) & vbCrLf & vbCrLf & "Please keep the coupons used.", vbOKOnly, "Return Coupons", , , , , RGBMSGBox_PromptColour)
                        End If
                    End If
                Else
                    If mcolCouponsInTransaction.Count = 0 Then
                        Call MsgBoxEx("No coupons were used." & vbCrLf & vbCrLf & vbCrLf & "Please return all the coupons to the Customer.", vbOKOnly, "Coupons", , , , , RGBMSGBox_PromptColour)
                    Else
                        Call MsgBoxEx("Return coupon(s) to customer." & vbCrLf & vbCrLf & vbCrLf & "Please return all the coupons to the Customer.", vbOKOnly, "Return Coupons", , , , , RGBMSGBox_PromptColour)
                    End If
                End If
            End If
        Else
            'Step through List and Return any coupons that are not used or are re-usable
            For lngLineNo = 2 To sprdLines.MaxRows Step 1
                Call DebugMsg(MODULE_NAME, "SaveSale", endlDebug, "Checking return Coupons" & lngLineNo)
                sprdLines.Row = lngLineNo
                'Ensure only lines that are SKU's
                sprdLines.Col = COL_LINETYPE
                Select Case (Val(sprdLines.Value))
                    Case (LT_COUPON):
                            sprdLines.Col = COL_CPN_STATUS
                            If (Val(sprdLines.Text) <> 9) Then 'Not Customer Reward Coupons
                                sprdLines.Col = COL_PARTCODE
                                If (sprdLines.Text <> "") Then 'Not already returned
                                    sprdLines.Col = COL_CPN_REUSE
                                    If (sprdLines.Value = 1) Then
                                        sprdLines.Col = COL_DESC
                                        strReturnCoupons = strReturnCoupons & Space(15) & "-" & sprdLines.Text & vbCrLf
                                    Else
                                        sprdLines.Col = COL_CPN_STATUS
                                        If (Val(sprdLines.Text) = 1) Or (Val(sprdLines.Text) = 2) Or (Val(sprdLines.Text) = 8) Then
                                            sprdLines.Col = COL_DESC
                                            strReturnCoupons = strReturnCoupons & Space(15) & "-" & sprdLines.Text & vbCrLf
                                        End If
                                        If (Val(sprdLines.Text) = 7) Or (Val(sprdLines.Text) = 5) Then
                                            sprdLines.Col = COL_DESC
                                            strReturnCoupons = strReturnCoupons & Space(15) & "-" & sprdLines.Text & " (Reversed)" & vbCrLf
                                        End If
                                    End If
                                    sprdLines.Col = COL_PARTCODE
                                    sprdLines.Text = "" 'clear Coupon in Part Code so that will not be prompted again
                                End If
                            End If
                End Select
            Next lngLineNo
            If (strReturnCoupons <> "") Then
                Call MsgBoxEx("The following Unused/Re-Usable coupons must be returned to Customer." & vbCrLf & Replace(strReturnCoupons, " & ", " && ", Compare:=vbTextCompare) & vbCrLf & vbCrLf & "Confirm coupons returned.", vbOKOnly, "Return Coupons", , , , , RGBMSGBox_PromptColour)
            End If
        End If
    End If
    
    Call DebugMsg(MODULE_NAME, "SaveSale", endlDebug, "Saved tran-" & mstrTranId)
'    Call goDatabase.CommitTransaction
'    If oPOSCust Is Nothing = False Then Call oPOSCust.SaveIfNew

    ucpbStatus.Caption1 = "Printing Receipt Footer"
    Dim oRecPrinting As New clsReceiptPrinting
    Dim blnOK As Boolean
    
    Call SetEntryMode(enemNone)
    fraTender.Visible = False

    If (blnQuote = False) Then
        If (mstrPrinterType = "P" Or mstrPrinterType = "R") Then
        
            Call DebugMsg(MODULE_NAME, "SaveSale", endlDebug, "Preparing to print tran-" & mstrTranId)
            Set oRecPrinting.Printer = OPOSPrinter1
            Set oRecPrinting.TranHeader = moTranHeader
            oRecPrinting.TillNo = mstrTillNo
            oRecPrinting.TranNo = mstrTranId
            Set oRecPrinting.goRoot = goRoot
            
            Call oRecPrinting.Init(goSession, goDatabase)
            oRecPrinting.EFTDebugInfo = mstrEFTDebugInfo
            ' Commidea Into WSS
            Dim PrintOK As Boolean
            
            ' Referral 522
            ' If merging Commidea receipt, need to reprint - unless it a void receipt with no commidea content
            If MergingCommideaAndWickesReceipts And Not (blnVoided And UBound(mavReceipts) = 0) Then
                Set moReceiptPrinting = oRecPrinting
                oRecPrinting.TranHeader.StoreNumber = mstrStoreNo
                oRecPrinting.UsingCommidea = True
                oRecPrinting.CommideaCCReceipts = mavReceipts
                Do
                    ' Commidea Into WSS - Referral 517 - Add receipt re-print number to receipt
                    PrintOK = oRecPrinting.printReceipt(False, moDiscountCardScheme, ReprintCount)
                    ReprintCount = ReprintCount + 1
                Loop While MsgBoxEx("Did the receipt print OK?" & vbNewLine & "[Yes] to continue, [No] to re-print.", _
                                  vbQuestion + vbYesNo, _
                                  "Receipt printed", , , , , RGBMSGBox_PromptColour) = vbNo
            Else
                PrintOK = oRecPrinting.printReceipt(False, moDiscountCardScheme)
            End If
            If Not PrintOK Then             ' End of Commidea Into WSS
                Call MsgBoxEx("Transaction will be voided", vbExclamation + vbOKOnly, _
                              "Authorisation Failure", , , , , RGBMsgBox_WarnColour)
                mstrAccountName = vbNullString
                mstrAccountNum = vbNullString
                moTranHeader.Voided = True
                Call moTranHeader.SaveIfExists
        '        Call SaveSale(False, True, vbNullString, vbNullString, vbNullString, vbNullString)
            ElseIf moTranHeader.Voided = False And mblnParkingTran = False Then
                ' Check to see if there are any Gift Tokens to be printed
                If (moTranHeader.Offline = True) Then Call oRecPrinting.printReceipt(True, moDiscountCardScheme)
                ' 13/08/14 For SALE only
                If moTOSType.Code = TT_SALE Then
                    For lngLineNo = 2 To sprdLines.MaxRows
                        sprdLines.Row = lngLineNo
                        sprdLines.Col = COL_VOIDED
                        If (Val(sprdLines.Value) = 0) Then
                            sprdLines.Col = COL_VOUCHER
                            If sprdLines.Value = "V" Then
                                sprdLines.Col = COL_VOUCHER
                                
                                Dim Amount As Currency
                                Amount = GetGridData(sprdLines, COL_SELLPRICE, sprdLines.Row)
                                
                                'Applicable as for Sale/TopUp of GC, so for Change to GC from Gift Token
                                Call TopupGiftCard(Amount, moTranHeader.CashierID, flSaleGiftCard)
                            End If
                        End If
                    Next lngLineNo
                    
                    flSaleGiftCard = False
                    'Taken out 6/2/08 - Wix2008-007 Gift Vouchers wil be saved once printed instead of all at once
                    'Added back in 21/4/08 - Wix2008-008 - save gift vouchers not already saved i.e. Taken as payment
                    moTranHeader.SaveGiftVouchers
                End If
            End If 'Printer not attached
        End If 'Non-Quote so print out receipt
        
        Call DebugMsg(MODULE_NAME, "SaveSale", endlDebug, "Printing Completed")
        'WIX1311 Referral 15. Added by MO'C 28/06/2007 code below to reprint receipt if copyreceipt option set to true
        If Not moDiscountCardScheme Is Nothing Then
            If moDiscountCardScheme.CopyReceipt Then
                Call oRecPrinting.printReceipt(False, moDiscountCardScheme)
            End If
        End If
        
        'If Transaction was for a Quote/Order, flag the transaction as processed so that it is not duplicated
        If (mstrQuoteTillID <> "") And (blnVoided = False) And (blnParked = False) Then
            Set oQuoteTran = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
            Call oQuoteTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, mdteQuoteTranDate)
            Call oQuoteTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, mstrQuoteTillID)
            Call oQuoteTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, mstrQuoteTranNo)
            Call oQuoteTran.LoadMatches
            oQuoteTran.TranParked = False
            oQuoteTran.RecoveredFromParked = True
            oQuoteTran.SaveIfExists
            Set oQuoteTran = Nothing
            
            'Update the quantity taken and the current transaction details
            Set oOrderHdrBO = goDatabase.CreateBusinessObject(CLASSID_CUSTORDERHEADER)
            With oOrderHdrBO
                Call .AddLoadFilter(CMP_EQUAL, FID_CUSTORDERHEADER_OrderNumber, lblOrderNo.Caption)
                Call .LoadMatches
                
                If (mlngQuoteQtyTaken > 0) Then .UnitsTaken = mlngQuoteQtyTaken
                
                If .UnitsRefunded = 0 Then
                    .TranDate = Date
                    .TillID = mstrTillNo
                    .TransactionNumber = mstrTranId
                Else
                    .RefundTranDate = Date
                    .RefundTillID = mstrTillNo
                    .RefundTransactionNumber = mstrTranId
                End If
            
                Call .SaveIfExists
            End With
        End If 'Order/Quote tran must be flagged as processed
    
        Call DebugMsg(MODULE_NAME, "SaveSale", endlDebug, "Saving any Day Prices")
        Dim oDayPrice As cDayPrice
        'If voided then remove any Price Amendments
        If (mcolPriceAmends Is Nothing = False) Then
            If (blnVoided = True) And (blnQuote = False) Then
                For lngLineNo = mcolPriceAmends.Count To 1 Step -1
                    Set oDayPrice = mcolPriceAmends(lngLineNo)
                    If oDayPrice.TimeCreated = "" Then mcolPriceAmends.Remove (lngLineNo)
                Next
            Else
                'Valid Transaction so save any new Price Amendments
                For Each oDayPrice In mcolPriceAmends
                    If oDayPrice.TimeCreated = "" Then
                        oDayPrice.TimeCreated = Format(Now, "HHNN")
                        Call oDayPrice.IBo_SaveIfNew
                    End If
                Next
            End If
        End If
'        moTranHeader.SaveIfExists
    End If
    
    If (moTranHeader.TranParked = True) Then moTranHeader.RecordAsVoided
    
    Call SaveItemPromptRejects
    mstrTranId = moTranHeader.TransactionNo
    'Added 3/9/08 to call CashBalUpdate after each Transaction
    If (blnParked = False) And (goSession.ISession_IsOnline = True) Then
        Call Shell(App.Path & "\CashierBalancingUpdate.exe T" & Format(Date, "YYYYMMDD") & moTranHeader.TillID & moTranHeader.TransactionNo, vbHide)
    End If

''##SET ICOM=1
   If (blnVoided = False) And (blnVouchers = True) And (mblnTraining = False) And (blnQuote = False) And (goSession.GetParameter(2160) = True) Then Call CreateVoucherAdjustment

'    If (moTranHeader.Voided = False) And (moTranHeader.Offline = False) Then Call moTranHeader.SendToHeadOffice
    If (LenB(mstrAccountNum) = 0) And (blnQuote = False) And (blnSaveOrder = False) Then
        Set moTranHeader = Nothing
        Set moDiscountCardScheme = Nothing
        ucpbStatus.Visible = False
    Else
        For lngLineNo = 2 To sprdLines.MaxRows Step 1
            sprdLines.Row = lngLineNo
            'Process line if a Price Promise - reduce Item price by Discount amount
            sprdLines.Col = COL_VOIDED
            If (Val(sprdLines.Value) = 0) Then
                sprdLines.Col = COL_ORIGTOTAL
                If (Val(sprdLines.Value) <> 0) Then
                    'if line is a Price Promise/Match - then update price on item
                    curItemDisc = Val(sprdLines.Value)
                    'step up to previous line item and change price to force recalc
                    sprdLines.Col = COL_INCTOTAL
                    sprdLines.Value = curItemDisc
                End If
            End If
        Next lngLineNo
    End If
        
    Call BackUpTransaction

   Call DebugMsg(MODULE_NAME, "SaveSale", endlDebug, "Reversing any Price Promise Adjustments")
   'step through each line locate any price promise/match to apply to proceeding line
    For lngLineNo = 2 To sprdLines.MaxRows Step 1
        ucpbStatus.Value = lngLineNo - 1
        sprdLines.Row = lngLineNo
        'Process line if a Price Promise - reduce Item price by Discount amount
        sprdLines.Col = COL_LINETYPE
        If (Val(sprdLines.Value) = LT_PRICEPROM) Then
            'if line is a Price Promise/Match - then update price on item
            sprdLines.Col = COL_SELLPRICE
            curItemDisc = Abs(Val(sprdLines.Value))
            'step up to previous line item and change price to force recalc
            sprdLines.Col = COL_LINETYPE
            While (Val(sprdLines.Value) <> LT_ITEM) And (sprdLines.Row > 1)
                sprdLines.Row = sprdLines.Row - 1
            Wend
            sprdLines.Col = COL_SELLPRICE
            sprdLines.Value = Val(sprdLines.Value) + curItemDisc
'            sprdLines.Col = col_
        End If
    Next lngLineNo
    Call DebugMsg(MODULE_NAME, "SaveSale", endlTraceOut, "Reversed Price Promise Adjustments-Completed")
    
    If (mstrGiftVoucherSKU <> mstrSKUGiftVoucher) Then
        mstrGiftVoucherSKU = mstrSKUGiftVoucher
    End If
    
    'mblnCreatingOrder = False
    
    Exit Function

HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("SaveSale", Err.Description)
        
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Function

'Added 26/2/08 - after saving - copy history file to back-up folder
Private Sub BackUpTransaction()
    
    Dim oFSO        As New FileSystemObject
    Dim strFileName As String
    Dim oSysOpt     As cSystemOptions
    Dim strTillID   As String
    
    strTillID = goSession.CurrentEnterprise.IEnterprise_WorkstationID
    On Error Resume Next
    
    Set oSysOpt = goDatabase.CreateBusinessObject(CLASSID_SYSTEMOPTIONS)
    Call oSysOpt.LoadMatches
    
    If goSession.ISession_IsOnline Then
        strFileName = oSysOpt.TillRemoteDrive & ":\" & Trim$(oSysOpt.TillPathName) & "\UPDATED\" & strTillID & lblTranNo.Caption & ".DTU"
    Else
        strFileName = oSysOpt.TillLocalDrive & ":\" & Trim$(oSysOpt.TillPathName) & "\HISTORY\" & strTillID & lblTranNo.Caption & ".DTA"
    End If
    
    If (oFSO.FolderExists("C:\wix\Trans") = False) Then oFSO.CreateFolder ("c:\wix\trans")
    If (oFSO.FolderExists("C:\wix\Trans\Tran" & Format(Date, "MMYY")) = False) Then oFSO.CreateFolder ("c:\wix\trans\Tran" & Format(Date, "MMYY"))
    Call oFSO.CopyFile(strFileName, "C:\wix\Trans\Tran" & Format(Date, "MMYY") & "\" & strTillID & lblTranNo.Caption & ".DTA", True)
    
    Call Err.Clear
    
    ' Tran is reserved sql word, use "Tran" to identify as a column
    Call DebugMsg(MODULE_NAME, "BackUpTransaction", endlDebug, "Setting RTI Flag:SET RTI='S' WHERE DATE1='" & Format(Date, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND " & Chr(34) & "Tran" & Chr(34) & "='" & lblTranNo.Caption & "'")
    Call goDatabase.ExecuteCommand("UPDATE DLLINE SET RTI='S' WHERE DATE1='" & Format(Date, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND " & Chr(34) & "Tran" & Chr(34) & "='" & lblTranNo.Caption & "'")
    Call goDatabase.ExecuteCommand("UPDATE DLPAID SET RTI='S' WHERE DATE1='" & Format(Date, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND " & Chr(34) & "Tran" & Chr(34) & "='" & lblTranNo.Caption & "'")
    ' Commidea Into WSS - DLCOMM is an extension of DLPAID
    Call goDatabase.ExecuteCommand("UPDATE DLCOMM SET RTI='S' WHERE DATE1='" & Format(Date, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND " & Chr(34) & "Tran" & Chr(34) & "='" & lblTranNo.Caption & "'")
    Call goDatabase.ExecuteCommand("UPDATE DLRCUS SET RTI='S' WHERE DATE1='" & Format(Date, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND " & Chr(34) & "Tran" & Chr(34) & "='" & lblTranNo.Caption & "'")
    Call goDatabase.ExecuteCommand("UPDATE DLEVNT SET RTI='S' WHERE DATE1='" & Format(Date, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND " & Chr(34) & "Tran" & Chr(34) & "='" & lblTranNo.Caption & "'")
    Call goDatabase.ExecuteCommand("UPDATE DLANAS SET RTI='S' WHERE DATE1='" & Format(Date, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND " & Chr(34) & "Tran" & Chr(34) & "='" & lblTranNo.Caption & "'")
    Call goDatabase.ExecuteCommand("UPDATE DLGIFT SET RTI='S' WHERE DATE1='" & Format(Date, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND " & Chr(34) & "Tran" & Chr(34) & "='" & lblTranNo.Caption & "'")
    Call goDatabase.ExecuteCommand("UPDATE DLOCUS SET RTI='S' WHERE DATE1='" & Format(Date, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND " & Chr(34) & "Tran" & Chr(34) & "='" & lblTranNo.Caption & "'")
    Call goDatabase.ExecuteCommand("UPDATE DLOLIN SET RTI='S' WHERE DATE1='" & Format(Date, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND " & Chr(34) & "Tran" & Chr(34) & "='" & lblTranNo.Caption & "'")
    Call goDatabase.ExecuteCommand("UPDATE DLEANCHK SET RTI='S' WHERE DATE1='" & Format(Date, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND " & Chr(34) & "Tran" & Chr(34) & "='" & lblTranNo.Caption & "'")
    Call goDatabase.ExecuteCommand("UPDATE DLREJECT SET RTI='S' WHERE DATE1='" & Format(Date, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND " & Chr(34) & "Tran" & Chr(34) & "='" & lblTranNo.Caption & "'")
    Call goDatabase.ExecuteCommand("UPDATE DLTOTS SET RTI='S' WHERE DATE1='" & Format(Date, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND " & Chr(34) & "Tran" & Chr(34) & "='" & lblTranNo.Caption & "'")
    If Err.Number <> 0 Then Call DebugMsg(MODULE_NAME, "BackUpTransaction", endlDebug, "Error when setting RTI Flag:" & Err.Description)
    Call Err.Clear

End Sub

'Added 26/2/08 - after saving - copy history file to back-up folder
Private Sub CheckTranTotals(ByRef TranHeader As cPOSHeader)
        
Dim curVATTotal As Currency
Dim lngVATCode  As Long
Dim oFSO        As New FileSystemObject

    On Error Resume Next

    For lngVATCode = 1 To 9 Step 1
        curVATTotal = curVATTotal + TranHeader.VATValue(lngVATCode)
    Next lngVATCode
    
    If (TranHeader.TaxAmount <> curVATTotal) Then
        If (oFSO.FolderExists("C:\wix\Trans") = False) Then oFSO.CreateFolder ("c:\wix\trans")
        If (oFSO.FolderExists("C:\wix\Trans\Faults") = False) Then oFSO.CreateFolder ("c:\wix\trans\Faults")
        Call sprdLines.SaveTabFile("c:\wix\trans\faults\Tran" & goSession.CurrentEnterprise.IEnterprise_WorkstationID & lblTranNo.Caption & ".csv")
    End If

End Sub

Private Function SaveRefund(blnParked As Boolean, _
                          blnVoided As Boolean, _
                          strName As String, _
                          strPostCode As String, _
                          strAddress As String, _
                          strTelNo As String) As Boolean

Dim oPOSLine     As cPOSLine
Dim oPOSPmt      As cPOSPayment
Dim oRecPrinting As New clsReceiptPrinting
Dim oRefundTran   As cPOSHeader 'used to mark items that have been refunded
Dim strRFTranNo   As String 'if refund was against original transaction then used to update
Dim strRFTillID   As String
Dim dteRFTranDate As Date
Dim lngRFLineNo   As Long
Dim dblRFQuantity As Double
Dim lngLineNo    As Long
Dim dblAmount    As Double
Dim strTranCode  As String
Dim lngTotalPos  As Long
Dim lngFirstPmt  As Long
Dim blnCustDone  As Boolean 'used to ensure only 1 DLRCUS written for DLTOTS
Dim ReprintCount As Integer ' Commidea Into WSS - Referral 517 - Add receipt re-print number to receipt

    On Error GoTo HandleException
    
    moTranHeader.Voided = blnVoided
    moTranHeader.TranParked = blnParked
    moTranHeader.FromDCOrders = blnParked 'double flag required as Customer Orders causing conflict
    
    strTranCode = mstrStoreNo & mstrTillNo & mstrTranId & Format$(moTranHeader.TranDate, "DDMMYY")
    
    Set oRefundTran = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    
    oRecPrinting.TillNo = mstrTillNo
    oRecPrinting.TranNo = mstrTranId
   
    Call PrintTransactionHeader(OPOSPrinter1, moTranHeader.TranDate, moTranHeader.TransactionTime, _
                                mstrTillNo, moTranHeader.TransactionNo, "Store Copy", _
                                txtUserID.Text, lblFullName.Caption, vbNullString)
    
    If LenB(mstrAccountNum) <> 0 Then
        Call RetrieveHeadOfficeAddress
        Call PrintAccountDepartmentAddress(OPOSPrinter1, mstrHeadofficename, _
            mstrHeadOfficeAddress, mstrHeadOfficePhoneNo, mstrHeadOfficeFaxNumber, _
            mstrVATNumber)
        Call PrintCreditNoteHeader(OPOSPrinter1, mstrAccountNum, mstrAccountName, _
            mstrActive, mstrCardExpiry, mstrAddressLine1, mstrAddressLine2, mstrAddressLine3, _
            mstrPostCode, mstrHomeTelNumber, mstrInvoiceNumber, False)
    End If
    
    'Print TeleOrderNum on receipt if required
    If LenB(mstrTeleOrderNum) <> 0 Then Call PrintTeleOrderNum(OPOSPrinter1, mstrTeleOrderNum)
    
    'step through each line and save
    For lngLineNo = 2 To sprdLines.MaxRows Step 1
        sprdLines.Row = lngLineNo
        'Ensure only lines that are SKU's
        sprdLines.Col = COL_LINETYPE
        If Val(sprdLines.Value) = LT_TOTAL Then lngTotalPos = lngLineNo
        If Val(sprdLines.Value) = LT_PMT And lngFirstPmt = 0 Then lngFirstPmt = lngLineNo
        If Val(sprdLines.Value) = LT_ITEM Then
            'Check line has not been voided
            sprdLines.Col = COL_VOIDED
            If (Val(sprdLines.Value) = 0) Then
                Call SaveTranLine(oPOSLine, sprdLines.Row, 0, blnCustDone, blnVoided)
                'Check if line was from another Receipt, if so flag as refunded
                sprdLines.Col = COL_RFND_TRANID
                If LenB(sprdLines.Text) <> 0 Then
                    strRFTranNo = sprdLines.Text
                    sprdLines.Col = COL_RFND_TILLNO
                    strRFTillID = sprdLines.Text
                    sprdLines.Col = COL_RFND_DATE
                    dteRFTranDate = sprdLines.Text
                    sprdLines.Col = COL_RFND_LINENO
                    lngRFLineNo = Val(sprdLines.Value)
                    sprdLines.Col = COL_QTY
                    dblRFQuantity = Val(Abs(sprdLines.Text))
                End If
                'If transaction completed okay, then print line item
                If (blnParked = False) And (blnVoided = False) Then
                    Call PrintLine(OPOSPrinter1, oPOSLine.PartCode, sprdLines.Text, vbNullString, _
                                oPOSLine.QuantitySold, oPOSLine.ExtendedValue / oPOSLine.QuantitySold, _
                                oPOSLine.ExtendedValue, oPOSLine.VATCode, False)
                End If
            End If 'line not voided
        End If 'saving line item
    Next lngLineNo
    
    'Extract transaction total and put into header
    sprdLines.Col = COL_INCTOTAL
    sprdLines.Row = lngTotalPos
    moTranHeader.TotalSaleAmount = Val(sprdLines.Value)
    
    If (blnParked = False) And (blnVoided = False) Then
        Call PrintTranTotal(OPOSPrinter1, moTOSType.Description, moTranHeader.TotalSaleAmount, True)
    End If
    'step through each line and save any payment lines
    For lngLineNo = lngFirstPmt To sprdLines.MaxRows Step 1
        sprdLines.Row = lngLineNo
        'Ensure only Payment lines are saved
        sprdLines.Col = COL_LINETYPE
        If Val(sprdLines.Value) = LT_PMT Then
            'add payment line to Till header for saving
            sprdLines.Col = COL_INCTOTAL
            dblAmount = Val(sprdLines.Value)
            sprdLines.Col = COL_TENDERTYPE
            'Set oPOSPmt = moTranHeader.AddPayment(sprdLines.Text, dblAmount)
            'If transaction completed okay, then print line item
            If (blnParked = False) And (blnVoided = False) Then
                sprdLines.Col = COL_DESC
                Call PrintPayment(OPOSPrinter1, dblAmount, sprdLines.Text)
            End If
        End If 'saving payment item
    Next lngLineNo
    
    'Write Customer Number to TranHeader
    If LenB(mstrAccountNum) <> 0 Then
        moTranHeader.CustomerAcctNo = mstrAccountNum
    End If
    
    If (blnParked = False) And (blnVoided = False) Then Call PrintDivideLine(OPOSPrinter1)
'    Call goDatabase.StartTransaction
    Call DebugMsg(MODULE_NAME, "SaveSale", endlDebug, "Started saving tran-" & mstrTranId)
    moTranHeader.TransactionComplete = True
    If goSession.ISession_IsOnline Then
        moTranHeader.Offline = False
    Else
        moTranHeader.Offline = True
    End If
    Call moTranHeader.SaveIfExists
    
    If blnVoided = False Then
        Call ZReadUpdate("TR", moTOSType.Code)
        Call UpdateFlashTotals(False, enftRefunds, moTranHeader.TotalSaleAmount)
    End If
    
    Call DebugMsg(MODULE_NAME, "SaveSale", endlDebug, "Saved tran-" & mstrTranId)
'    Call goDatabase.CommitTransaction
'    If oPOSCust Is Nothing = False Then Call oPOSCust.SaveIfNew
    
    If goSession.GetParameter(PRM_REFUND_PRINT_DECLARATION) = True Then
        Call PrintRefundSlip(OPOSPrinter1, True, "STR", "PAID", Date, "TRAN ID", "RSN", "USER")
    Else
        Call PrintInvoiceTrailer1Refund(OPOSPrinter1)
        Call PrintInvoiceTrailer2(OPOSPrinter1)
        Call PrintInvoiceTrailer3(OPOSPrinter1)
    End If

    'Finish print of Till Receipt footer
    If (blnParked = True) Then
        Call PrintParked(OPOSPrinter1, mstrTillNo & moTranHeader.TransactionNo & _
                         Format$(moTranHeader.TranDate, "DDMMYY"))
    End If
    If (blnParked = False) And (blnVoided = True) Then
        Call PrintVoided(OPOSPrinter1)
    Else
        If (blnParked = False) Then Call PrintBarCode(OPOSPrinter1, strTranCode)
        If LenB(strName) <> 0 Then
            Call PrintAddress(OPOSPrinter1, strName, strAddress, strPostCode, strTelNo)
            Call PrintDivideLine(OPOSPrinter1)
        End If
        Call PrintFooter(OPOSPrinter1)
        Set oRecPrinting.goRoot = goRoot
        Call oRecPrinting.Init(goSession, goDatabase)
        oRecPrinting.EFTDebugInfo = mstrEFTDebugInfo
        ' Commidea Into WSS
        ' Referral 522
        ' If merging Commidea receipt, need to reprint - unless it a void receipt with no commidea content
        If MergingCommideaAndWickesReceipts And Not (blnVoided And UBound(mavReceipts) = 0) Then
            Set moReceiptPrinting = oRecPrinting
            oRecPrinting.TranHeader.StoreNumber = mstrStoreNo
            oRecPrinting.UsingCommidea = True
            oRecPrinting.CommideaCCReceipts = mavReceipts
            Do
                ' Commidea Into WSS - Referral 517 - Add receipt re-print number to receipt
                Call oRecPrinting.printReceipt(False, moDiscountCardScheme, ReprintCount)
                ReprintCount = ReprintCount + 1
            Loop While MsgBoxEx("Did the receipt print OK?" & vbNewLine & "[Yes] to continue, [No] to re-print.", _
                              vbQuestion + vbYesNo, _
                              "Receipt printed", , , , , RGBMSGBox_PromptColour) = vbNo
        Else
            ' Only print if offline. Is that true?????
            If (moTranHeader.Offline = True) Then
                Call oRecPrinting.printReceipt(True, moDiscountCardScheme)
            End If
        End If
        ' End of Commidea Into WSS
    End If
    
    If (blnParked = False) And (goSession.ISession_IsOnline) Then
        Call Shell(App.Path & "\CashierBalancingUpdate.exe T" & Format(Date, "YYYYMMDD") & moTranHeader.TillID & moTranHeader.TransactionNo, vbHide)
    End If
    
    If LenB(mstrAccountNum) = 0 Then Set moTranHeader = Nothing
    Call BackUpTransaction
    
    Exit Function

HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("SaveRefund", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
        
End Function

Private Function SaveQuote(blnPrintQuote As Boolean, blnQuoteForOrder As Boolean, blnSkipDelQuestion As Boolean, blnForDelivery As Boolean) As Boolean

Dim oPOSLine      As cPOSLine
Dim oQuoteTran    As cPOSHeader
Dim oOrderHdrBO   As cCustOrderHeader
Dim oQuoteLineBO  As cQuoteLine
Dim oReturnCust   As cReturnCust
Dim strTranCode   As String
Dim lngLineNo     As Long
Dim lngTotalPos   As Long
Dim lngSeqNo      As Long ' used to hold Line Item DB Line No
Dim colLines      As Collection
Dim arlngEvent()  As Long
Dim curItemDisc   As Currency
Dim vntAddress    As Variant

Dim oQuoteHdr     As cQuoteHeader
Dim oQuoteLine    As cQuoteLine

Dim oPrintQuote   As cPrintQuote_Wickes.clsPrintQuote

Dim strSKU        As String
Dim blnDelCharge  As Boolean
Dim curDelCharge  As Currency
Dim curOrderTotal As Currency
Dim blnToDeliver  As Boolean
Dim lngKeyResp    As Long

Dim strSchemeName As String
Dim strExpiryMsg  As String  'used to inform user that due to events quote will expire early
Dim strFOCDelSup  As String 'WIX1243 User who gave FOC Delivery
Dim dblDiscountRate As Double
Dim blnQuotePrnOK   As Boolean
Dim strMsg          As String
Dim strProcedureName As String
    
    On Error GoTo HandleException
    
    strProcedureName = "SaveQuote"
    
    If (Left(lblOrderNo.Caption, 1) = "Q") Then
        lblOrderNo.Caption = ""
        Call ClearEvents
        mdblTotalBeforeDis = 0
    End If
    
    blnToDeliver = blnForDelivery
    strFOCDelSup = "000"
    If (blnQuoteForOrder = False) Then mdblTotalBeforeDis = 0
    
    If (goSession.ISession_IsOnline = False) Then
        Call MsgBoxEx("System is currently working Off-Line" & vbCrLf & vbCrLf & "System is unable to create quotes whilst Off-Line", vbOKOnly, "Create Quotes Disabled")
        Exit Function
    End If
    
    sprdTotal.Row = ROW_TOTTOTALS
    sprdTotal.Col = COL_TOTVAL
    If (Val(sprdTotal.Text) = 0) And (txtSKU.Visible = True) Then
        Call MsgBoxEx("Warning - Quote has no value." & _
            vbNewLine & "Unable to proceed.", vbOKOnly + vbExclamation, _
            "No Quote Value", , , , , RGBMsgBox_WarnColour)
        Call SetEntryMode(enemSKU)
        If (txtSKU.Enabled = True) And (txtSKU.Visible = True) Then txtSKU.SetFocus
        Exit Function
    End If
    curOrderTotal = sprdTotal.Value
    
    'Check if any delivery charges have been added
    blnDelCharge = False
    Call DebugMsgSprdLinesValue(strProcedureName)
    For lngLineNo = 2 To sprdLines.MaxRows Step 1
        sprdLines.Col = COL_SALETYPE
        sprdLines.Row = lngLineNo
        If (sprdLines.Value = "D") Then
            sprdLines.Col = COL_VOIDED
            If (Val(sprdLines.Value) = 0) Then
                blnDelCharge = True
                sprdLines.Col = COL_INCTOTAL
                curDelCharge = curDelCharge + Val(sprdLines.Value)
            End If
        End If
        blnToDeliver = (blnDelCharge Or blnToDeliver)
    Next lngLineNo
    
    If (blnSkipDelQuestion = False) Then
        'WIX1389 - check if any Item Prompts and if the cancel transaction
        If (ProcessEndOfTranPrompts(True) = False) Then
            mblnCreatingOrder = False
            mstrOrderQuit = "Y"
            Call Form_KeyPress(vbKeyEscape)
            Call Form_KeyPress(vbKeyEscape)
            Call SetEntryMode(enemSKU)
            Exit Function
        End If
        If (blnDelCharge = False) Then
            If (MsgBoxEx("Are these goods to be delivered?", vbYesNo, "Create Quote - Confirm Delivery", , , , , RGBMSGBox_PromptColour) = vbYes) Then
                blnToDeliver = True
                If (MsgBoxEx("Is this delivery Free of Charge?" & vbCrLf & IIf(curOrderTotal < goSession.GetParameter(PRM_QODFOC_LEVEL), "Total value is below Free Of Charge Delivery Level.", ""), vbYesNo + vbDefaultButton3, "Create Quote - Confirm Delivery", , , , , RGBMSGBox_PromptColour) = vbNo) Then
                    Call MsgBoxEx("This transaction has no specified Delivery Charges." & vbCrLf & "Add delivery charge SKU's before saving", vbInformation, "Delivery Charges Missing", , , , , RGBMsgBox_WarnColour)
                    Call SetEntryMode(enemSKU)
                    Exit Function
                Else 'WIX1243 - Free Of Charge Delivery
                    If (curOrderTotal < goSession.GetParameter(PRM_QODFOC_LEVEL)) Then
                        Load frmVerifyPwd
                        If (frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) = False) Then
                            Unload frmVerifyPwd
                            Call SetEntryMode(enemSKU)
                            Exit Function
                        End If
                        strFOCDelSup = frmVerifyPwd.txtAuthID.Text
                        Unload frmVerifyPwd
                    End If
                End If 'for Free Delivery check if below spend level
            End If
        End If
    End If
    
    mblnForDelivery = blnToDeliver 'WIX 1381
    If EnableBuyCoupons Then
        Call RecordTender(blnSkipDelQuestion, blnSkipDelQuestion, False)  'get system to calculate events
        mblnQuoteRecalledOrderCreated = True
    Else
        Call RecordTender(blnSkipDelQuestion, blnSkipDelQuestion, False) 'get system to calculate events
    End If
    
    If (blnQuoteForOrder = False) Then
        
        fraTender.Visible = False
'        If Not moDiscountCardScheme Is Nothing Then 'Added 28/06/2007 MO'C WIX1311 Referral 16
            strExpiryMsg = ""
            If (DateAdd("d", goSession.GetParameter(PRM_QUOTES_VALID_FOR), Date) > mdteEventEndDate) Then
                strExpiryMsg = vbNewLine & vbNewLine & "NOTE : Due to promotional items, this quote is only valid till " & Format(mdteEventEndDate, "DD/MM/YY")
            End If
            If (mblnHasEFT = True) And Not DisableTrainingModeOrders Then
                lngKeyResp = MsgBoxEx("Total Quote for entered goods is " & vbTab & " : " & vbTab & Format(mdblTranTotal, "0.00") & vbCrLf & "Includes delivery charge of " & vbTab & vbTab & " : " & vbTab & Format(curDelCharge, "0.00") & strExpiryMsg & vbCrLf & vbCrLf & "Select Action", vbYesNoCancel, "Quote Saved", "Save Quote", "Create Order", "Amend Items", , RGBMSGBox_PromptColour)
            Else
                lngKeyResp = MsgBoxEx("Total Quote for entered goods is " & vbTab & " : " & vbTab & Format(mdblTranTotal, "0.00") & vbCrLf & "Includes delivery charge of " & vbTab & vbTab & " : " & vbTab & Format(curDelCharge, "0.00") & strExpiryMsg & vbCrLf & vbCrLf & "Select Action", vbOKCancel, "Quote Saved", "Save Quote", "Amend Items", , , RGBMSGBox_PromptColour)
            End If
            If (DateAdd("d", goSession.GetParameter(PRM_QUOTES_VALID_FOR), Date) > mdteEventEndDate) Then
            End If
'        Else
'            lngKeyResp = MsgBoxEx("Total Quote for entered goods is " & vbTab & " : " & vbTab & Format(mdblTotalBeforeDis, "currency") & vbCrLf & "Includes delivery charge of " & vbTab & vbTab & " : " & vbTab & Format(curDelCharge, "currency") & vbCrLf & vbCrLf & "Select Action or Esc to Amend Lines", vbYesNoCancel, "Quote Saved", "Save Quote", "Create Order", "Amend Items", , RGBMSGBox_PromptColour)
'        End If
        
        Select Case (lngKeyResp)
            Case (vbNo): Call CreateOrder(False, True, blnToDeliver)
                         Exit Function
            Case (vbCancel):    Call Form_KeyPress(vbKeyEscape)
                                Exit Function
        End Select
        If (goSession.GetParameter(PRM_SHOW_STOCK_ON_QUOTE_SAVE) = True) Then
            If (ConfirmStockLevels = False) Then
                Call Form_KeyPress(vbKeyEscape)
                Exit Function
            End If
        End If
    End If
    
    If LenB(mstrName) = 0 Then
        Call frmCustomerAddress.GetOrderAddress(mstrName, mstrPostCode, mstrAddress, mstrTelNo, mstrMobileNo, mstrWorkTelNo, mstrEmail, False)
        If frmCustomerAddress.Cancel Then
            If mblnCreatingOrder = True Then
                'MO'C Refs 2010-012 removed as per ref no.1
                'strMsg = goSession.GetParameter(PRM_TAKE_NOW_MSG)
                'If MsgBoxEx(strMsg, vbYesNo + vbDefaultButton2, "Order", , , , , RGBMsgBox_WarnColour) = vbNo Then
                '    mstrOrderQuit = "N"
                '    GoTo TryAgain
                'Else
                    mstrOrderQuit = "Y"
                    mblnCreatingOrder = False
                'End If
            End If

            Call Form_KeyPress(vbKeyEscape)
            Exit Function
        End If
    End If
    
    Call SetEntryMode(enemNone)
    fraActions.Visible = False
    
    ucpbStatus.Visible = True
    
    'moTranHeader.DiscountCardNumber = ""
    Call SaveSale(True, True, True, False)
    ucpbStatus.Caption1 = "Saving Quote Details"

     'reset totals for VAT Discount calculations
    Set oQuoteHdr = goDatabase.CreateBusinessObject(CLASSID_QUOTEHEADER)
    oQuoteHdr.QuoteDate = Date
    oQuoteHdr.TranDate = moTranHeader.TranDate
    oQuoteHdr.TransactionNumber = moTranHeader.TransactionNo
    oQuoteHdr.TillID = moTranHeader.TillID
    oQuoteHdr.ExpiryDate = DateAdd("d", goSession.GetParameter(PRM_QUOTES_VALID_FOR), Date)
    If (oQuoteHdr.ExpiryDate > mdteEventEndDate) Then oQuoteHdr.ExpiryDate = mdteEventEndDate
    oQuoteHdr.DeliveryCharge = curDelCharge
    oQuoteHdr.IsForDelivery = blnToDeliver
    oQuoteHdr.FOCDelSupervisor = strFOCDelSup
    If Not moDiscountCardScheme Is Nothing Then
        oQuoteHdr.DiscountCardNo = mstrDiscCardNo
    End If

    'Flag Quote Transaction so that if converted to Order, will be flagged as accepted
    mdteQuoteTranDate = Date
    mstrQuoteTillID = moTranHeader.TillID
    mstrQuoteTranNo = moTranHeader.TransactionNo
        
    If LenB(mstrAddress) <> 0 Then
    
        vntAddress = Split(mstrAddress, vbNewLine)
        mstrAddressLine1 = vntAddress(0)
        mstrAddressLine2 = vntAddress(1)
        mstrAddressLine3 = vntAddress(2)
        Set oReturnCust = goDatabase.CreateBusinessObject(CLASSID_RETURNCUST)
        
        With oReturnCust
        
            .TransactionDate = moTranHeader.TranDate
            .PCTillID = moTranHeader.TillID
            .TransactionNumber = moTranHeader.TransactionNo
            .LineNumber = 0
            
            .CustomerName = mstrName
            .AddressLine1 = mstrAddressLine1
            .AddressLine2 = mstrAddressLine2
            .AddressLine3 = mstrAddressLine3
            .PostCode = mstrPostCode
            .PhoneNo = mstrTelNo
            .HouseName = ""
            .LabelsRequired = False
            .RefundReason = "00"
            .MobileNo = mstrMobileNo
            .WorkTelNumber = Mid(mstrWorkTelNo, 1, 1)
            .Email = Mid(mstrEmail, 1, 1)
            .SaveIfNew
            
        End With
        
    End If
    
    strTranCode = mstrStoreNo & mstrTillNo & mstrTranId & Format$(oQuoteHdr.TranDate, "DDMMYY")
    
    ucpbStatus.Caption1 = "Storing Items"
    ucpbStatus.Value = 0
    If sprdLines.MaxRows > 1 Then ucpbStatus.Max = sprdLines.MaxRows - 1
        
     'Extract transaction total and put into header
    sprdLines.Row = ROW_TOTALS
    sprdLines.Col = COL_INCTOTAL
    oQuoteHdr.MerchandiseValue = moTranHeader.MerchandiseAmount + moTranHeader.DiscountAmount
    
    If (Abs(oQuoteHdr.MerchandiseValue) > 9999999) Then oQuoteHdr.MerchandiseValue = 0
 
   'step through each line and save
    For lngLineNo = 2 To sprdLines.MaxRows Step 1
        Call DebugMsg(MODULE_NAME, strProcedureName, endlDebug, "Saving Quote Line " & lngLineNo)
        Call DebugMsgSprdLineValue(lngLineNo, strProcedureName & "(Save)")
        ucpbStatus.Value = lngLineNo - 1
        sprdLines.Row = lngLineNo
        'Ensure only lines that are SKU's
        sprdLines.Col = COL_LINETYPE
        Select Case (Val(sprdLines.Value))
            Case (LT_ITEM):
                sprdLines.Col = COL_VOIDED
                If (Val(sprdLines.Value) = 0) Then
                    sprdLines.Col = COL_PARTCODE
                    strSKU = sprdLines.Value
                    sprdLines.Col = COL_QTY
                    Set oQuoteLineBO = oQuoteHdr.AddLine(strSKU, sprdLines.Value)
                    sprdLines.Col = COL_VOLUME
                    oQuoteLineBO.LineVolume = Round(sprdLines.Value * oQuoteLineBO.UnitsQuoted, 2)
                    sprdLines.Col = COL_WEIGHT
                    'oQuoteLineBO.LineWeight = Round(sprdLines.Value * oQuoteLineBO.UnitsQuoted, 2)
                    oQuoteLineBO.LineWeight = 0
                    oQuoteHdr.QuoteVolume = oQuoteHdr.QuoteVolume + oQuoteLineBO.LineVolume
                    oQuoteHdr.QuoteWeight = oQuoteHdr.QuoteWeight + oQuoteLineBO.LineWeight
                End If
        End Select
    Next lngLineNo
        
    ucpbStatus.Caption1 = "Storing Quote Header"
    ucpbStatus.Value = 0

    Call DebugMsg(MODULE_NAME, strProcedureName, endlDebug, "Started saving tran-" & mstrTranId)
    If (oQuoteHdr.IBo_SaveIfNew = False) Then
        Call Err.Clear
        Call MsgBoxEx("ERROR : A critical error has occurred when completing Transaction to Database." & _
            "System is unable to proceed and will exit." & vbCrLf & vbCrLf & "Desc :" & oQuoteHdr.SaveFailedStatus & _
            vbCrLf & "Contact IT Support immediattely", vbExclamation, "System Failure", , , , , RGBMsgBox_WarnColour)
        On Error Resume Next
        Call Err.Raise(1, "SaveQuote_Click", "Unable to save to Database")
        Call Err.Report(MODULE_NAME, "SaveQuote", 0, False)
        End
    End If
    
    Call moTranHeader.UpdateQuoteNumber(oQuoteHdr.QuoteNumber)
    lblOrderNo.Caption = "Q" & oQuoteHdr.QuoteNumber

    Call DebugMsg(MODULE_NAME, strProcedureName, endlDebug, "Saved tran-" & mstrTranId)
'    Call goDatabase.CommitTransaction
    
    ucpbStatus.Caption1 = "Printing Quotation"
    
    Call SetEntryMode(enemNone)
    fraTender.Visible = False

    If (blnQuoteForOrder = False) Then 'ask if print quote
        
        lngKeyResp = vbCancel
        While (lngKeyResp = vbCancel)
            lngKeyResp = MsgBoxEx("Quote Saved. Print Quote now?" & vbNewLine & vbNewLine & "Printer:" & Printer.DeviceName, vbYesNoCancel, "Quote Saved", , , "Select Printer", , RGBMSGBox_PromptColour)
            If (lngKeyResp = vbCancel) Then
                Load frmPrinter
                Call frmPrinter.Show(vbModal)
                Unload frmPrinter
            End If
        Wend
        If (lngKeyResp = vbYes) Then
        
            If (Not moDiscountCardScheme Is Nothing) Then
                strSchemeName = moDiscountCardScheme.SchemeName
                dblDiscountRate = moDiscountCardScheme.DiscountRate
            Else
                strSchemeName = ""
                dblDiscountRate = 0
            End If
            
            Call DebugMsg(MODULE_NAME, strProcedureName, endlDebug, "Preparing to print quote-" & mstrTranId)
            Set oPrintQuote = New clsPrintQuote

            While (blnQuotePrnOK = False)
                If oPrintQuote.PrintQuote(oQuoteHdr, goSession, goRoot, goDatabase, strSchemeName, dblDiscountRate, mstrDiscCardNo) = False Then
                    Call MsgBoxEx("An error has occurred when printing Quote.  Quote has been successfully saved.", vbExclamation + vbOKOnly, _
                                  "Print Quote Failure", , , , , RGBMsgBox_WarnColour)
                Else
                    lngKeyResp = vbCancel
                    While (lngKeyResp = vbCancel)
                        lngKeyResp = MsgBoxEx("Check if Quote has printed successfully or is a re-print required?" & vbNewLine & vbNewLine & "Printer:" & Printer.DeviceName, vbYesNoCancel, "Confirm Quote Printed", "OK", "Reprint", "Select Printer", , RGBMSGBox_PromptColour)
                        If (lngKeyResp = vbYes) Then blnQuotePrnOK = True
                        If (lngKeyResp = vbCancel) Then
                            Load frmPrinter
                            Call frmPrinter.Show(vbModal)
                            Unload frmPrinter
                        End If
                    Wend
                End If
            Wend
            
        End If
        
    End If

    'If Transaction was for a Quote/Order, flag the transaction as processed so that it is not duplicated
    If (mstrQuoteTillID <> "") Then
        ucpbStatus.Caption1 = "Recording retrieved quote as processed"
        Set oQuoteTran = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
        Call oQuoteTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, mdteQuoteTranDate)
        Call oQuoteTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, mstrQuoteTillID)
        Call oQuoteTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, mstrQuoteTranNo)
        Call oQuoteTran.LoadMatches
        oQuoteTran.TranParked = False
        oQuoteTran.RecoveredFromParked = True
        oQuoteTran.SaveIfExists
        Set oQuoteTran = Nothing
        
    End If 'Order/Quote tran must be flagged as processed

    Set oQuoteHdr = Nothing
    ucpbStatus.Visible = False
    
    If (blnQuoteForOrder = False) Then 'finished saving quote so wait for next transaction
        Set moDiscountCardScheme = Nothing
        fraTender.Visible = False
        mlngCurrentMode = encmComplete
        Call Form_KeyPress(vbKeyReturn)
    End If
    
    Exit Function

HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler(strProcedureName, Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Function


Private Function CreateOrder(blnRecordedTender As Boolean, blnSkipDelQuestion As Boolean, blnForDelivery As Boolean) As Boolean

Dim oPOSLine      As cPOSLine
Dim oQuoteTran    As cPOSHeader
Dim oOrderHdrBO   As cCustOrderHeader
Dim oOrderInfoBO  As cCustOrderInfo
Dim oOrderLineBO  As cCustOrderLine
Dim oQuoteHdrBO   As cQuoteHeader
Dim oReturnCust   As cReturnCust
Dim strTranCode   As String
Dim lngLineNo     As Long
Dim lngTotalPos   As Long
Dim lngSeqNo      As Long ' used to hold Line Item DB Line No
Dim colLines      As Collection
Dim arlngEvent()  As Long
Dim curItemDisc   As Currency
Dim vntAddress    As Variant
Dim oOrigQuoteHdr As cQuoteHeader

Dim oPrintOrder   As cPrintQuote_Wickes.clsPrintQuote

Dim strSKU        As String
Dim blnDelCharge  As Boolean
Dim curDelCharge  As Currency
Dim curTotalValue As Currency
Dim strSchemeName As String
Dim dblDiscountRate As Double

Dim strDelAddressLine1 As String
Dim strDelAddressLine2 As String
Dim strDelAddressLine3 As String
Dim strDelAddressLine4 As String
Dim strDelPostCode     As String

Dim dteDelDate  As Date
Dim lngDelQty   As Long 'take out Order Charges from Total Quantities
Dim strDelNotes As String
Dim blnOrderPrnOK   As Boolean
Dim lngRowNo    As Long

Dim blnCreateQuote As Boolean

Dim cQuotePPCust    As cPriceCusInfo
Dim colQuotePPLines As Collection
Dim oNewQtePPLineBO As cPriceLineInfo

Dim oPPCust      As cPriceCusInfo
Dim oNewPPLineBO As cPriceLineInfo

Dim lngKeyResp      As Long
Dim strMsg          As String
Dim lngQtyTakeNow   As Long
Dim lngToDeliver    As Long
Dim lngQtyOrdered   As Long

Dim moTranHeaderCheck As cPOSHeader

Dim strProcedureName As String

    On Error GoTo HandleException
    
    strProcedureName = "CreateOrder"
    
    mstrOrderQuit = ""
    If (goSession.ISession_IsOnline = False) Then
        Call MsgBoxEx("System is currently working Off-Line" & vbCrLf & vbCrLf & "System is unable to create quotes/orders whilst Off-Line", vbOKOnly, "Create Quotes Disabled")
        Exit Function
    End If
        
    mblnCreatingOrder = True 'Wix1381
    mblnOrderBtnPressed = True
    
    sprdTotal.Row = ROW_TOTTOTALS
    sprdTotal.Col = COL_TOTVAL
    If (Val(sprdTotal.Text) = 0) And (txtSKU.Visible = True) Then
        Call MsgBoxEx("Warning - Order has no value." & _
            vbNewLine & "Unable to proceed.", vbOKOnly + vbExclamation, _
            "No Order Value", , , , , RGBMsgBox_WarnColour)
        Call SetEntryMode(enemSKU)
        If (txtSKU.Enabled = True) And (txtSKU.Visible = True) Then txtSKU.SetFocus
        Exit Function
    End If
    
    
    If (LenB(mstrTelNo) = 0) And (LenB(mstrMobileNo) = 0) And (LenB(mstrWorkTelNo) = 0) Then mblnShowAddress = True
    If mblnShowAddress = True Then
TryAgain:
        Call frmCustomerAddress.GetOrderAddress(mstrName, mstrPostCode, mstrAddress, mstrTelNo, mstrMobileNo, mstrWorkTelNo, mstrEmail, True, mblnShowAddress)
        mblnShowAddress = False
        If frmCustomerAddress.Cancel Then
            If mblnCreatingOrder = True Then
                 ' MO'C Refs 2010-012 Removed as per ref no.1
'                strMsg = goSession.GetParameter(PRM_TAKE_NOW_MSG)
'                If MsgBoxEx(strMsg, vbYesNo + vbDefaultButton2, "Order", , , , , RGBMsgBox_WarnColour) = vbNo Then
'                    GoTo TryAgain
'                    mstrOrderQuit = ""
'                Else
                    mstrOrderQuit = ""
                    mblnCreatingOrder = False
'                End If
            End If
            fraTender.Visible = True
            Call Form_KeyPress(vbKeyEscape)
            fraTender.Visible = False
            Call SetEntryMode(enemSKU)
            Exit Function
        End If
    End If
    
    Call SetEntryMode(enemNone)
    fraActions.Visible = False
    
    ucpbStatus.Visible = True
    ucpbStatus.Caption1 = "Creating Order"
    
    'Set moDiscountCardScheme = Nothing 'Added 29/06/07 - Make sure parked transactions have no discount
    
    If (Left$(lblOrderNo.Caption, 1) <> "Q") Then
        
        blnCreateQuote = True
        Set cQuotePPCust = moTranHeader.GetPricePromiseCust
        Set colQuotePPLines = moTranHeader.GetPricePromiseLine
        Call SaveQuote(False, True, blnSkipDelQuestion, blnForDelivery)
        'check if save successful and if not then do not continue with save Sales Order
        If (Left$(lblOrderNo.Caption, 1) <> "Q") Then
            SetEntryMode (enemSKU)
            ucpbStatus.Visible = False
            mstrOrderQuit = "Y"
            mblnCreatingOrder = False
            Call Form_KeyPress(vbKeyEscape) 'cancel SKU entry
            Call Form_KeyPress(vbKeyEscape) 'cancel Tendering
            fraTender.Visible = False
            Exit Function
        End If
        
        'Clear down any Key Audits as they have already been saved
        sprdLines.Row = -1
        sprdLines.Col = COL_KEYED_RC
        sprdLines.Text = ""
        
        Set moTranHeader = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
        ReDim mavReceipts(0) As Variant  ' Commidea Into WSS, record all receipt files from Commidea for the tran
        mblnTransactionHasWebOrderNoRefund = False  ' Hubs 2.0 - Referral 771 - Reset flag used to identify whether tran involves a web order no refund as cannot park these
        moTranHeader.TillID = mstrTillNo
        moTranHeader.CashierID = txtUserID.Text
        moTranHeader.TransactionCode = moTOSType.Code
        moTranHeader.TrainingMode = mblnTraining
        moTranHeader.RefundCashier = mstrRefundCashier
        moTranHeader.CollectPostCode = mblnCapturePostCode
        moTranHeader.DiscountCardNumber = mstrDiscCardNo

        If LenB(mstrTranSuper) <> 0 Then
            moTranHeader.SupervisorUsed = True
            moTranHeader.SupervisorNo = mstrTranSuper
        End If
        
        moTranHeader.Voided = True
        Call SetUpSaleVATRates(moTranHeader)
        
  'Added 30/12/2011 - Ensure Recall Quote Process works fine when Coupon is typed in
     moTranHeader.IBo_SaveIfNew
     Set moTranHeaderCheck = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
        If (moTranHeaderCheck.IBo_SaveIfExists = False) Then
            Call Err.Clear
            Call MsgBoxEx("ERROR : A critical error has occurred when completing Transaction to Database." & _
                "System is unable to proceed and will exit." & vbCrLf & vbCrLf & "Desc :" & moTranHeaderCheck.SaveFailedStatus & _
                vbCrLf & "Contact IT Support immediately", vbExclamation, "System Failure", , , , , RGBMsgBox_WarnColour)
            On Error Resume Next
            Call Err.Raise(1, "SaveSale_Click", "Unable to save to Database")
            Call Err.Report(MODULE_NAME, "SaveSale", 0, False)
            End
        End If
    
        'Added 16/9/05 - ensure Saved OK else report error and exit - Error Type is shown in DebugView
'        If (moTranHeader.IBo_SaveIfNew = False) Then
'            Call Err.Clear
'            Call MsgBoxEx("ERROR : A critical error has occurred when completing Transaction to Database." & _
'                "System is unable to proceed and will exit." & vbCrLf & vbCrLf & "Desc :" & moTranHeader.SaveFailedStatus & _
'                vbCrLf & "Contact IT Support immediattely", vbExclamation, "System Failure", , , , , RGBMsgBox_WarnColour)
'            On Error Resume Next
'            Call Err.Raise(1, "cmdTranType_Click", "Unable to save to Database")
'            Call Err.Report(MODULE_NAME, "cmdTranType_Click", 0, False)
'            End
'        End If
        lblTranNo.Caption = moTranHeader.TransactionNo
                        
        If ((cQuotePPCust Is Nothing) = False) Then
            Set oPPCust = moTranHeader.AddPricePromiseCust(cQuotePPCust.Name)
            oPPCust.Address1 = cQuotePPCust.Address1
            oPPCust.Address2 = cQuotePPCust.Address2
            oPPCust.Address3 = cQuotePPCust.Address3
            oPPCust.PostCode = cQuotePPCust.PostCode
            oPPCust.Phone = cQuotePPCust.Phone
        End If
        
        'Copy Price Promise Lines details across
        If ((colQuotePPLines Is Nothing) = False) Then
            For Each oNewQtePPLineBO In colQuotePPLines
                Set oNewPPLineBO = moTranHeader.AddPricePromiseLine(oNewQtePPLineBO.SequenceNo)
                oNewPPLineBO.CompetitorName = oNewQtePPLineBO.CompetitorName
                oNewPPLineBO.CompetitorAddress1 = oNewQtePPLineBO.CompetitorAddress1
                oNewPPLineBO.CompetitorAddress2 = oNewQtePPLineBO.CompetitorAddress2
                oNewPPLineBO.CompetitorAddress3 = oNewQtePPLineBO.CompetitorAddress3
                oNewPPLineBO.CompetitorPostcode = oNewQtePPLineBO.CompetitorPostcode
                oNewPPLineBO.CompetitorPhone = oNewQtePPLineBO.CompetitorPhone
                oNewPPLineBO.EnteredPrice = oNewQtePPLineBO.EnteredPrice
                oNewPPLineBO.IncludeVAT = oNewQtePPLineBO.IncludeVAT
                
                oNewPPLineBO.OrigStoreNumber = oNewQtePPLineBO.OrigStoreNumber
                oNewPPLineBO.OrigTransDate = oNewQtePPLineBO.OrigTransDate
                oNewPPLineBO.OrigTillNumber = oNewQtePPLineBO.OrigTillNumber
                oNewPPLineBO.OrigTransactionNumber = oNewQtePPLineBO.OrigTransactionNumber
                oNewPPLineBO.OrigSellingPrice = oNewQtePPLineBO.OrigSellingPrice
            Next
        End If
    End If
    
    
    'Check if Order has been fully paid, else go and get payment
    If blnRecordedTender = False Then
        
        If (ConfirmStockLevels = False) Then
            SetEntryMode (enemSKU)
            ucpbStatus.Visible = False
            Call Form_KeyPress(vbKeyEscape) 'cancel SKU entry
            Call Form_KeyPress(vbKeyEscape) 'cancel Tendering
            Exit Function
        End If
        
        sprdTotal.Row = 3
        sprdTotal.Col = COL_TOTVAL
        'If Val(sprdTotal.Text) <= 0 Then
        If Val(sprdTotal.Text) > 0 Then
            sprdTotal.Row = 1
            sprdTotal.Col = COL_TOTVAL - 1
            mblnSaveNewOrder = True
            If EnableBuyCoupons Then
                Call RecordTender(mblnQuoteRecalledOrderCreated, blnCreateQuote, Left$(lblOrderNo.Caption, 1) <> "Q")
                mblnQuoteRecalledOrderCreated = True
            Else
                Call RecordTender(Left$(lblOrderNo.Caption, 1) = "Q", blnCreateQuote, Left$(lblOrderNo.Caption, 1) <> "Q")
            End If
            If (curTotalValue > 0) Then mdblTotalBeforeDis = curTotalValue
            Exit Function
        End If
    End If
    
    If (Left$(lblOrderNo.Caption, 1) = "Q") Then
        lblOrderNo.Caption = Mid$(lblOrderNo.Caption, 2)
        Set oQuoteHdrBO = goDatabase.CreateBusinessObject(CLASSID_QUOTEHEADER)
        Call oQuoteHdrBO.RecordAsSold(lblOrderNo.Caption)
    End If
    
    moTranHeader.OrderNumber = lblOrderNo.Caption
    
    Set oOrderHdrBO = goDatabase.CreateBusinessObject(CLASSID_CUSTORDERHEADER)
    oOrderHdrBO.OrderNumber = lblOrderNo.Caption
    oOrderHdrBO.OrderDate = Date
    oOrderHdrBO.TranDate = moTranHeader.TranDate
    oOrderHdrBO.TransactionNumber = moTranHeader.TransactionNo
    mstrTranId = moTranHeader.TransactionNo
    oOrderHdrBO.TillID = moTranHeader.TillID
    
    'Added 21/8/07 - Other info added to separate table
    Set oOrderInfoBO = goDatabase.CreateBusinessObject(CLASSID_CUSTORDERINFO)
    oOrderInfoBO.OrderNumber = lblOrderNo.Caption
    oOrderInfoBO.OrderDate = Date
    oOrderInfoBO.FreeDelID = "000"

    'MO'C Added 6/08/07 - WIX 1240 Get the original order taker (could still be the current user?)
    Set oOrigQuoteHdr = goDatabase.CreateBusinessObject(CLASSID_QUOTEHEADER)    'MO'C Added 6/08/07
    Call oOrigQuoteHdr.IBo_AddLoadFilter(CMP_EQUAL, FID_QUOTEHEADER_QuoteNumber, oOrderHdrBO.OrderNumber) 'MO'C Added 6/08/07
    oOrigQuoteHdr.LoadMatches 'MO'C Added 6/08/07
    oOrigQuoteHdr.IsSold = True
    oOrigQuoteHdr.SaveIfExists

    Set oQuoteTran = goDatabase.CreateBusinessObject(CLASSID_POSHEADER) 'MO'C Added 6/08/07
    Call oQuoteTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, oOrigQuoteHdr.TranDate) 'MO'C Added 6/08/07
    Call oQuoteTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, oOrigQuoteHdr.TillID) 'MO'C Added 6/08/07
    Call oQuoteTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, oOrigQuoteHdr.TransactionNumber) 'MO'C Added 6/08/07
    Call oQuoteTran.LoadMatches 'MO'C Added 6/08/07
    oOrderInfoBO.UserID = oQuoteTran.CashierID 'MO'C Added 6/08/07 - WIX 1240
    If (oOrderInfoBO.UserID = "") Then oOrderInfoBO.UserID = "000"
    
    'Check if any delivery charges have been added
    blnDelCharge = False
    Call DebugMsgSprdLinesValue(strProcedureName)
    For lngLineNo = 2 To sprdLines.MaxRows Step 1
        sprdLines.Col = COL_SALETYPE
        sprdLines.Row = lngLineNo
        If (sprdLines.Value = "D") Then
            sprdLines.Col = COL_VOIDED
            If (Val(sprdLines.Value) = 0) Then
                blnDelCharge = True
                sprdLines.Col = COL_INCTOTAL
                curDelCharge = curDelCharge + Val(sprdLines.Value)
                sprdLines.Col = COL_QTY
                lngDelQty = lngDelQty + Val(sprdLines.Value)
            End If
        End If
    Next lngLineNo
    oOrderHdrBO.DeliveryCharge = curDelCharge
    
    'get Quote to determine if being delivered
    Set oQuoteHdrBO = goDatabase.CreateBusinessObject(CLASSID_QUOTEHEADER)
    Call oQuoteHdrBO.AddLoadFilter(CMP_EQUAL, FID_QUOTEHEADER_QuoteNumber, lblOrderNo.Caption)
    Call oQuoteHdrBO.AddLoadField(FID_QUOTEHEADER_IsForDelivery)
    Call oQuoteHdrBO.LoadMatches

    oOrderHdrBO.IsForDelivery = oQuoteHdrBO.IsForDelivery
    mstrAddress = mstrAddress + vbCrLf
    vntAddress = Split(mstrAddress, vbNewLine)
    
    mstrAddressLine1 = vntAddress(0)
    mstrAddressLine2 = vntAddress(1)
    mstrAddressLine3 = vntAddress(2)
    mstrAddressLine4 = vntAddress(3)
        
    If (goSession.GetParameter(PRM_COUNTRY_CODE) = "IE") Then
        strDelAddressLine1 = ""
        strDelAddressLine2 = mstrAddressLine1
        strDelAddressLine3 = mstrAddressLine2
        strDelAddressLine4 = "" 'this is ignore here and handled as a Postcode
        strDelPostCode = mstrAddressLine3
    Else
        strDelAddressLine1 = mstrAddressLine1
        strDelAddressLine2 = mstrAddressLine2
        strDelAddressLine3 = mstrAddressLine3
'        If strDelAddressLine4 = "" Then
'            strDelAddressLine4 = mstrPostCode
'        End If
        strDelAddressLine4 = mstrAddressLine4
        strDelPostCode = mstrPostCode
    End If
    
    Load frmDeliveryDetails
    If (oOrderHdrBO.IsForDelivery = True) Then

        'SC300e
        On Error GoTo Skip983000Check
        If goSession.GetParameter(983000) = True Then
           On Error GoTo 0
           Err.Clear

           Call frmDeliveryDetails.GetDeliveryAddressVolumeMovement(dteDelDate, mstrName, strDelAddressLine1, strDelAddressLine2, _
                                                                     strDelAddressLine3, strDelPostCode, strDelAddressLine4, _
                                                                     strDelNotes, oOrderHdrBO.OrderNumber)
           oOrderHdrBO.DateRequired = dteDelDate
    
        Else
Skip983000Check:
            On Error GoTo 0
            Err.Clear
            Call frmDeliveryDetails.GetDeliveryAddress(dteDelDate, mstrName, strDelAddressLine1, strDelAddressLine2, _
                                                       strDelAddressLine3, strDelPostCode, strDelAddressLine4, strDelNotes)
            oOrderHdrBO.DateRequired = dteDelDate
    
        End If

    Else
        oOrderHdrBO.DateRequired = frmDeliveryDetails.GetCollectionDate(mstrName, _
            strDelAddressLine1, strDelAddressLine2, _
            strDelAddressLine3, mstrPostCode)
            'strDelAddressLine4 = mstrPostCode
    End If
    Unload frmDeliveryDetails
    
    If LenB(mstrAddress) <> 0 Then
        Set oReturnCust = goDatabase.CreateBusinessObject(CLASSID_RETURNCUST)
        
        With oReturnCust
            .TransactionDate = moTranHeader.TranDate
            .PCTillID = moTranHeader.TillID
            .TransactionNumber = moTranHeader.TransactionNo
            .LineNumber = 0
            
            .CustomerName = mstrName
            .AddressLine1 = mstrAddressLine1
            .AddressLine2 = mstrAddressLine2
            .AddressLine3 = mstrAddressLine3
            .PostCode = mstrPostCode
            .PhoneNo = mstrTelNo
            .HouseName = ""
            .LabelsRequired = False
            .RefundReason = "00"
            .MobileNo = mstrMobileNo
            .WorkTelNumber = Mid(mstrWorkTelNo, 1, 1)
            .Email = Mid(mstrEmail, 1, 1)
            .SaveIfNew
        End With
        
        oOrderHdrBO.CustomerName = mstrName
        oOrderHdrBO.DeliveryAddress1 = strDelAddressLine1
        oOrderHdrBO.DeliveryAddress2 = strDelAddressLine2
        oOrderHdrBO.DeliveryAddress3 = strDelAddressLine3
        If (goSession.GetParameter(PRM_COUNTRY_CODE) = "IE") Then
            strDelAddressLine4 = strDelPostCode 'For non-postcodes swap into Line 4
            strDelPostCode = ""
        End If
        oOrderHdrBO.DeliveryAddress4 = strDelAddressLine4
        oOrderHdrBO.PostCode = strDelPostCode
        oOrderHdrBO.PhoneNumber = mstrTelNo
        oOrderHdrBO.MobilePhoneNo = mstrMobileNo
        
        oOrderHdrBO.Notes = strDelNotes
    
        oOrderInfoBO.CustomerName = mstrName
        oOrderInfoBO.CustomerNumber = ""
        oOrderInfoBO.DateRequired = oOrderHdrBO.DateRequired
        
        'If (oOrderHdrBO.IsForDelivery = True) Then
        If (oOrderHdrBO.UnitsOrdered - oOrderHdrBO.UnitsTaken > 0) Then
            oOrderInfoBO.DeliveryRequestStatus = 100
        Else
            oOrderInfoBO.DeliveryRequestStatus = 100 '999
        End If
        oOrderInfoBO.SellingStoreId = IIf(Len(mstrStoreNo) < 4, Val(mstrStoreNo) + 8000, Val(mstrStoreNo))
        oOrderInfoBO.SellingStoreOrderId = Val(lblOrderNo.Caption)
        oOrderInfoBO.OmOrderNumber = 0
        oOrderInfoBO.WorkTelNumber = mstrWorkTelNo
        oOrderInfoBO.Email = mstrEmail
        oOrderInfoBO.CustomerAddress1 = Left$(mstrAddressLine1, 30)
        oOrderInfoBO.CustomerAddress2 = mstrAddressLine2
        oOrderInfoBO.CustomerAddress3 = mstrAddressLine3
        oOrderInfoBO.CustomerAddress4 = mstrAddressLine4
        oOrderInfoBO.CustomerPostCode = mstrPostCode
    End If
    
    strTranCode = mstrStoreNo & mstrTillNo & mstrTranId & Format$(oOrderHdrBO.TranDate, "DDMMYY")
    
    ucpbStatus.Caption1 = "Storing Line Items"
    ucpbStatus.Value = 0
    If sprdLines.MaxRows > 1 Then ucpbStatus.Max = sprdLines.MaxRows - 1
        
     'Extract transaction total and put into header
    sprdLines.Row = ROW_TOTALS
    sprdLines.Col = COL_INCTOTAL
    oOrderHdrBO.MerchandiseValue = Val(sprdLines.Value)
    If (Abs(oOrderHdrBO.MerchandiseValue) > 9999999) Then oOrderHdrBO.MerchandiseValue = 0
   
   
   Call DebugMsg(MODULE_NAME, "CreateOrder", endlDebug, "Preprocessing Price Matches")
    
   'step through each line and save
    For lngLineNo = 2 To sprdLines.MaxRows Step 1
        Call DebugMsg(MODULE_NAME, strProcedureName, endlDebug, "Saving Line" & lngLineNo)
        Call DebugMsgSprdLineValue(lngLineNo, strProcedureName & "(Save)")
        ucpbStatus.Value = lngLineNo - 1
        sprdLines.Row = lngLineNo
        'Ensure only lines that are SKU's
        sprdLines.Col = COL_VOIDED
        If (Val(sprdLines.Value) = 0) Then
            sprdLines.Col = COL_LINETYPE
            Select Case (Val(sprdLines.Value))
                Case (LT_ITEM):
                    sprdLines.Col = COL_PARTCODE
                    strSKU = sprdLines.Value
                    sprdLines.Col = COL_TAKE_NOW
                    lngQtyTakeNow = Val(sprdLines.Value)
                    sprdLines.Col = COL_QTY
                    Set oOrderLineBO = oOrderHdrBO.AddLine(strSKU, sprdLines.Value, lngQtyTakeNow)
                    oOrderHdrBO.UnitsOrdered = oOrderHdrBO.UnitsOrdered + Val(sprdLines.Value)
                    lngQtyOrdered = Val(sprdLines.Value)
                    sprdLines.Col = COL_VOLUME
                    oOrderLineBO.LineVolume = Round(sprdLines.Value * oOrderLineBO.UnitsOrdered, 2)
                    sprdLines.Col = COL_WEIGHT
                    oOrderLineBO.LineWeight = 0 'Round(sprdLines.Value * oOrderLineBO.UnitsOrdered, 2)
                    oOrderHdrBO.OrderVolume = oOrderHdrBO.OrderVolume + oOrderLineBO.LineVolume
                    oOrderHdrBO.OrderWeight = oOrderHdrBO.OrderWeight + oOrderLineBO.LineWeight
                    sprdLines.Col = COL_TAKE_NOW
                    oOrderLineBO.UnitsTaken = Val(sprdLines.Text)
                    lngToDeliver = lngQtyOrdered - lngQtyTakeNow
                    If (lngToDeliver > 0) Then
                        oOrderLineBO.DeliveryRequestStatus = 100
                        oOrderLineBO.QuantityToBeDelivered = lngToDeliver
                        If SkuIsForDelivery(strSKU) Then
                            oOrderLineBO.IsDeliveryChargeItem = True
                            oOrderLineBO.DeliveryRequestStatus = 999
                        Else
                            oOrderLineBO.IsDeliveryChargeItem = False
                        End If
                    Else
                        oOrderLineBO.DeliveryRequestStatus = 0
                        oOrderLineBO.QuantityToBeDelivered = 0
                        oOrderLineBO.IsDeliveryChargeItem = False
                        oOrderLineBO.DeliveryRequestStatus = 999
                    End If
                    
                    'Ref 470 - Gift Voucher must be set to 999
                    'DE1778 - Exclude Gift Cards from QOD orders
                    If SkuIsGiftCardOrVaucher(strSKU) Then
                        oOrderLineBO.UnitsTaken = 0
                        oOrderLineBO.DeliveryRequestStatus = 999
                        oOrderLineBO.QuantityToBeDelivered = 0
                    End If
                  
                    oOrderLineBO.SellingStoreId = IIf(Len(mstrStoreNo) < 4, Val(mstrStoreNo) + 8000, Val(mstrStoreNo))
                    oOrderLineBO.SellingStoreOrderId = Val(lblOrderNo.Caption)
                    sprdLines.Col = COL_PRETRAN_PRICE
                    sprdLines.Col = COL_QTYBRKAMOUNT
                    oOrderLineBO.Price = GetExtendedPriceFromSpreadSheet(lngLineNo)
                    sprdLines.Col = COL_DISCMARGIN
                    If Val(sprdLines.Text) > 0 Then 'MO'C Refs 2010-012 No.5
                        oOrderLineBO.PriceOverideReason = Val(sprdLines.Text)
                        lstReasonsList.ListIndex = -1
                    End If
            End Select
        End If
    Next lngLineNo
    oOrderHdrBO.UnitsOrdered = oOrderHdrBO.UnitsOrdered - lngDelQty 'take deliveries way from total items orders
        
    ucpbStatus.Caption1 = "Storing Payment Lines"
    ucpbStatus.Value = 0

'    If (blnParked = False) And (blnVoided = False) Then Call PrintDivideLine(OPOSPrinter1)
'    Call goDatabase.StartTransaction
    Call DebugMsg(MODULE_NAME, strProcedureName, endlDebug, "Started saving tran-" & mstrTranId)
    'Write Customer Number to TranHeader
    
    If (oOrderHdrBO.IBo_SaveIfNew = False) Then
        Call Err.Clear
        Call MsgBoxEx("ERROR : A critical error has occurred when saving Order to Database." & _
            "System is unable to proceed and will exit." & vbCrLf & vbCrLf & "Desc :" & oOrderHdrBO.SaveFailedStatus & _
            vbCrLf & "Contact IT Support immediattely", vbExclamation, "System Failure", , , , , RGBMsgBox_WarnColour)
        On Error Resume Next
        Call Err.Raise(1, "CreateOrder_Click", "Unable to save to Database")
        Call Err.Report(MODULE_NAME, "CreateOrder", 0, False)
        End
    End If
    
    If (oOrderHdrBO.IsForDelivery = True) Or (Err.Number <> 0) Then
        Call FillDeliverySlot(oOrderHdrBO.OrderNumber, oOrderHdrBO.DateRequired)
    End If
    
    If (oOrderInfoBO.IBo_SaveIfNew = False) Then
        
        Call MsgBoxEx("ERROR : An error has occurred when saving Order Info to Database." & _
            "System will continue may have problems with Order." & vbCrLf & vbCrLf & _
            vbCrLf & "Contact IT Support immediately." & vbCrLf & Err.Description, vbExclamation, "System Failure", , , , , RGBMsgBox_WarnColour)
        Call Err.Clear
        On Error Resume Next
        Call Err.Raise(1, "CreateOrder_Click", "Unable to save Order Info to Database")
        Call Err.Report(MODULE_NAME, "CreateOrder", 0, False)
    End If
    
    Call DebugMsg(MODULE_NAME, strProcedureName, endlDebug, "Saved tran-" & mstrTranId)
'    Call goDatabase.CommitTransaction
'    If oPOSCust Is Nothing = False Then Call oPOSCust.SaveIfNew

    ucpbStatus.Caption1 = "Printing Sales Order"
    
    
    If (Not moDiscountCardScheme Is Nothing) Then
        strSchemeName = moDiscountCardScheme.SchemeName
        dblDiscountRate = moDiscountCardScheme.DiscountRate
    Else
        strSchemeName = ""
        dblDiscountRate = 0
    End If
    
    Call SaveSale(False, False, False, blnCreateQuote)
    
    Call DebugMsg(MODULE_NAME, strProcedureName, endlDebug, "Preparing to print Order-" & mstrTranId)
    Set oPrintOrder = New clsPrintQuote
    Call SetEntryMode(enemNone)
    fraTender.Visible = False

    lngKeyResp = vbCancel
    
    While (lngKeyResp = vbCancel)
        lngKeyResp = MsgBoxEx("Order Saved. Printing Order - Confirm Printer Ready?" & vbNewLine & vbNewLine & "Printer:" & Printer.DeviceName, vbOKCancel, "Order Saved", , "Select Printer", , , RGBMSGBox_PromptColour)
        If (lngKeyResp = vbCancel) Then
            Load frmPrinter
            Call frmPrinter.Show(vbModal)
            Unload frmPrinter
        End If
    Wend
    
    While (blnOrderPrnOK = False)
        If oPrintOrder.PrintOrder(oOrderHdrBO, goSession, goRoot, goDatabase, strSchemeName, dblDiscountRate, mstrDiscCardNo) = False Then
            If (MsgBoxEx("An error has occurred when printing Order.  Order has been saved successfully." & vbNewLine & "Do you wish to retry or skip printing?", vbExclamation + vbYesNo, _
                          "Printing Failure", "Skip", "Reprint", , , RGBMsgBox_WarnColour) = vbNo) Then blnOrderPrnOK = True
        Else
            lngKeyResp = vbCancel
            While (lngKeyResp = vbCancel)
                lngKeyResp = MsgBoxEx("Check if Order has printed successfully or is a re-print required?" & vbNewLine & vbNewLine & "Printer:" & Printer.DeviceName, vbYesNoCancel, "Confirm Order Printed", "OK", "Reprint", "Select Printer", , RGBMSGBox_PromptColour)
                If (lngKeyResp = vbYes) Then blnOrderPrnOK = True
                If (lngKeyResp = vbCancel) Then
                    Load frmPrinter
                    Call frmPrinter.Show(vbModal)
                    Unload frmPrinter
                End If
            Wend
        End If
    Wend

'        oOrderHdrBO.Voided = True
'    Call oOrderHdrBO.SaveIfExists


    'If Transaction was for a Quote/Order, flag the transaction as processed so that it is not duplicated
    If (mstrQuoteTillID <> "") Then
        Set oQuoteTran = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
        Call oQuoteTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, mdteQuoteTranDate)
        Call oQuoteTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, mstrQuoteTillID)
        Call oQuoteTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, mstrQuoteTranNo)
        Call oQuoteTran.LoadMatches
        
        oQuoteTran.TranParked = False
        oQuoteTran.RecoveredFromParked = True
        oQuoteTran.SaveIfExists
        Set oQuoteTran = Nothing
        
    End If 'Order/Quote tran must be flagged as processed

    Set oOrderHdrBO = Nothing
    ucpbStatus.Visible = False

    fraTender.Visible = False
    mlngCurrentMode = encmComplete
    'fraTender.Visible = True        'Added 6/7/07
    'mlngCurrentMode = encmTender    'Added 6/7/07
    Call Form_KeyPress(vbKeyReturn)
    
    mblnCreatingOrder = False
    mblnOrderBtnPressed = False
    Set moDiscountCardScheme = Nothing
    
    Exit Function

HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("CreateOrder", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Function

Private Sub FillDeliverySlot(ByVal OrderNumber As String, ByVal DateRequired As Date)
    Dim oConnection As Connection
    Dim objCommand As ADODB.Command
    
    Set oConnection = goDatabase.Connection
    Set objCommand = New ADODB.Command
    
    With objCommand
        .ActiveConnection = oConnection
        .CommandType = adCmdStoredProc
        .CommandText = "dbo.usp_FillSlotInDeliveryOrder"
        .Prepared = True
        .Parameters.Append .CreateParameter("OrderNumber", adVarChar, adParamInput, 6, OrderNumber)
        .Parameters.Append .CreateParameter("DeliveryDate", adDate, adParamInput, , DateRequired)
        .Execute , , adExecuteNoRecords
    End With
    
End Sub

Private Sub SaveCouponNew(ByVal lngRowNo As Long, ByVal Quoting As Boolean, ByRef colCouponsUsed As Collection)

Dim strCouponID    As String
Dim strStatus      As String
Dim blnExcCoupon   As Boolean
Dim strMarketRef   As String
Dim strIssuedStore As String
Dim strSerialNo    As String
Dim oSoldCoupon    As cSoldCoupon

    sprdLines.Col = COL_CPN_ID
    strCouponID = sprdLines.Text
    If CheckCouponTriggeredAnEvent(strCouponID, colCouponsUsed) Then
        Call DebugMsg(MODULE_NAME, "SaveCoupon", endlDebug, "Row=" & lngRowNo)
        'go to the row that must be saved
        sprdLines.Row = lngRowNo
        'add line to Till header for saving
        sprdLines.Col = COL_CPN_STATUS
        strStatus = sprdLines.Text
        sprdLines.Col = COL_CPN_EXCLUSIVE
        blnExcCoupon = sprdLines.Text
        sprdLines.Col = COL_CPN_SERIALNO
        strSerialNo = sprdLines.Text
        sprdLines.Col = COL_CPN_MARKETREF
        strMarketRef = sprdLines.Text
        Set oSoldCoupon = moTranHeader.AddCoupon(strCouponID, strStatus, blnExcCoupon, strMarketRef, strSerialNo)
        sprdLines.Col = COL_CPN_REUSE
        oSoldCoupon.ReUsable = (sprdLines.Text = "1")
        sprdLines.Col = COL_SUPERVISOR
        oSoldCoupon.managerId = sprdLines.Text
        'End If
    
        If (strStatus = "") Then oSoldCoupon.Status = "0"
        If (strStatus = "6") And (Quoting = False) Then oSoldCoupon.Status = "0"
        If (Val(strStatus) = 0) And (Quoting = True) Then oSoldCoupon.Status = "6"
        If (strStatus = "9") Then oSoldCoupon.IssueStoreNo = goSession.CurrentEnterprise.IEnterprise_StoreNumber
    End If
End Sub

Private Sub SaveCoupon(ByVal lngRowNo As Long, ByVal Quoting As Boolean)

Dim strCouponID    As String
Dim strStatus      As String
Dim blnExcCoupon   As Boolean
Dim strMarketRef   As String
Dim strIssuedStore As String
Dim strSerialNo    As String
Dim oSoldCoupon    As cSoldCoupon

    Call DebugMsg(MODULE_NAME, "SaveCoupon", endlDebug, "Row=" & lngRowNo)
    'go to the row that must be saved
    sprdLines.Row = lngRowNo
    'add line to Till header for saving
    sprdLines.Col = COL_CPN_ID
    strCouponID = sprdLines.Text
    sprdLines.Col = COL_CPN_STATUS
    strStatus = sprdLines.Text
    sprdLines.Col = COL_CPN_EXCLUSIVE
    blnExcCoupon = sprdLines.Text
    sprdLines.Col = COL_CPN_SERIALNO
    strSerialNo = sprdLines.Text
    sprdLines.Col = COL_CPN_MARKETREF
    strMarketRef = sprdLines.Text
    Set oSoldCoupon = moTranHeader.AddCoupon(strCouponID, strStatus, blnExcCoupon, strMarketRef, strSerialNo)
    sprdLines.Col = COL_CPN_REUSE
    oSoldCoupon.ReUsable = (sprdLines.Text = "1")
    sprdLines.Col = COL_SUPERVISOR
    oSoldCoupon.managerId = sprdLines.Text
    If (strStatus = "") Then oSoldCoupon.Status = "0"
    If (strStatus = "6") And (Quoting = False) Then oSoldCoupon.Status = "0"
    If (Val(strStatus) = 0) And (Quoting = True) Then oSoldCoupon.Status = "6"
    If (strStatus = "9") Then oSoldCoupon.IssueStoreNo = goSession.CurrentEnterprise.IEnterprise_StoreNumber
End Sub


Private Sub SaveTranLine(ByRef oPOSLine As cPOSLine, ByVal lngRowNo As Long, ByVal dblCCAllocPerc As Double, ByRef blnCustDone As Boolean, TranVoided As Boolean)

Dim oReturnCust As cReturnCust
Dim curExcTotal As Currency
Dim curLineTotal As Currency
Dim curVATExcl  As Currency
Dim curVATDisc  As Currency
Dim curQty      As Currency
Dim lngQuantity As Long
Dim strSKU      As String
Dim strPRFSKU   As String
Dim strOrderNo  As String
Dim strStoreNo  As String

Dim oOrderHeader As cCustOrderHeader
Dim oOrderInfoBO  As cCustOrderInfo
Dim oOrderLineBO As cCustOrderLine
Dim colOrderLines As Collection
Dim lngLineNo As Long
Dim mMinimumStatus As Integer

    Call DebugMsg(MODULE_NAME, "SaveTranLine", endlDebug, "Row=" & lngRowNo)
    'go to the row that must be saved
    sprdLines.Row = lngRowNo
    'add line to Till header for saving
    sprdLines.Col = COL_PARTCODE
    strSKU = sprdLines.Text
    Set oPOSLine = moTranHeader.AddItem(strSKU)
    sprdLines.Col = COL_DB_LINENO
    sprdLines.Value = oPOSLine.SequenceNo
    sprdLines.Col = COL_QTY
    oPOSLine.QuantitySold = Val(sprdLines.Value)
    curQty = oPOSLine.QuantitySold
    lngQuantity = oPOSLine.QuantitySold
    sprdLines.Col = COL_WEEE_RATE
    oPOSLine.WEEELineCharge = sprdLines.Value
    sprdLines.Col = COL_SELLPRICE
    sprdLines.Value = sprdLines.Value - oPOSLine.WEEELineCharge
    oPOSLine.ActualSellPrice = Val(sprdLines.Value)
    sprdLines.Col = COL_LOOKEDUP
    oPOSLine.LookUpPrice = Val(sprdLines.Value)
    sprdLines.Col = COL_QTYSELLIN
    'oPOSLine.QuantityPerUnit = Val(sprdLines.Value)
    sprdLines.Col = COL_INCTOTAL
    oPOSLine.ExtendedValue = Val(sprdLines.Value)
    sprdLines.Col = COL_TOTCOST
    oPOSLine.ExtendedCost = Val(sprdLines.Value)
    'sprdLines.Col = COL_BAND
    'oPOSLine.PriceBand = Val(sprdLines.Value)
    sprdLines.Col = COL_DEPTCODE
    oPOSLine.DepartmentNumber = sprdLines.Text
    sprdLines.Col = COL_VATCODE
    oPOSLine.VATCode = sprdLines.Text
    sprdLines.Col = COL_DESC
    'oPOSLine.Description = sprdLines.Text
    sprdLines.Col = COL_TEMPAMOUNT
    oPOSLine.TempPriceMarginAmount = Val(sprdLines.Value)
    sprdLines.Col = COL_TEMPCODE
    oPOSLine.TempPriceMarginCode = sprdLines.Text
    sprdLines.Col = COL_DISCCODE
    oPOSLine.PriceOverrideReason = Val(sprdLines.Text)
    oPOSLine.PriceOverrideAmount = (oPOSLine.LookUpPrice - oPOSLine.ActualSellPrice) - _
                                   oPOSLine.TempPriceMarginAmount
    sprdLines.Col = COL_DISCMARGIN
    oPOSLine.OverrideMarginCode = sprdLines.Text
    
    If oPOSLine.PriceOverrideAmount <> 0 And Val(oPOSLine.PriceOverrideReason) = 0 Then
        oPOSLine.PriceOverrideReason = moRefPriceOverride.Code
        oPOSLine.OverrideMarginCode = Right$("000000" & moRefPriceOverride.Code, 6)
    End If
    
    sprdLines.Col = COL_SUPERVISOR
    oPOSLine.SupervisorNumber = sprdLines.Value
    If oPOSLine.PriceOverrideAmount <> 0 Then
        Call UpdateFlashTotals(False, enftPriceOverrides, oPOSLine.PriceOverrideAmount)
    End If
    sprdLines.Col = COL_APPLY_TO_STOCK
    oPOSLine.ApplyToStock = sprdLines.Text

    sprdLines.Col = COL_WEEE_RATE
    oPOSLine.WEEELineCharge = sprdLines.Value
    sprdLines.Col = COL_WEEE_SKU
    strPRFSKU = sprdLines.Text
    oPOSLine.WEEESequence = IIf(oPOSLine.WEEELineCharge <> 0, "001", "000")
    
    sprdLines.Col = COL_HGROUP
    oPOSLine.HierGroup = sprdLines.Text
    sprdLines.Col = COL_HSUBGROUP
    oPOSLine.HierSubGroup = sprdLines.Text
    sprdLines.Col = COL_HCATEGORY
    oPOSLine.HierCategory = sprdLines.Text
    sprdLines.Col = COL_HSTYLE
    oPOSLine.HierStyle = sprdLines.Text
    sprdLines.Col = COL_QURSUP
    oPOSLine.QuarantineSupervisor = sprdLines.Text
    
    sprdLines.Col = COL_BACKCOLLECT 'added 22/9/05-WIX1161
    Call DebugMsg(MODULE_NAME, "SaveTranLine", endlDebug, "Row=" & lngRowNo & " BDC=" & sprdLines.Text)
    oPOSLine.BackDoorCollect = (sprdLines.Text = "Y")
    
    sprdLines.Col = COL_BARCODE
    If LenB(Trim$(sprdLines.Text)) > 0 Then
        oPOSLine.BarcodeEntry = True
    Else
        oPOSLine.BarcodeEntry = False
    End If
    
    'Save any Events details against the Line
    sprdLines.Col = COL_OP_EVENTSEQNO
    oPOSLine.LastEventSequenceNo = sprdLines.Text
    sprdLines.Col = COL_QTYBRKAMOUNT
    oPOSLine.QtyBreakMarginAmount = Val(sprdLines.Value)
    sprdLines.Col = COL_QTYBRKCODE
    oPOSLine.QtyBreakMarginCode = sprdLines.Text
    sprdLines.Col = COL_DGRPAMOUNT
    oPOSLine.DealGroupMarginAmount = Val(sprdLines.Value)
    sprdLines.Col = COL_DGRPCODE
    oPOSLine.DealGroupMarginCode = sprdLines.Text
    sprdLines.Col = COL_MBUYAMOUNT
    oPOSLine.MultiBuyMarginAmount = Val(sprdLines.Value)
    sprdLines.Col = COL_MBUYCODE
    oPOSLine.MultiBuyMarginCode = sprdLines.Text
    sprdLines.Col = COL_HIERAMOUNT
    oPOSLine.HierarchyMarginAmount = Val(sprdLines.Value)
    sprdLines.Col = COL_HIERCODE
    oPOSLine.HierarchyMarginCode = sprdLines.Text
    
   
    moTranHeader.DiscountAmount = moTranHeader.DiscountAmount - (oPOSLine.QtyBreakMarginAmount + _
        oPOSLine.DealGroupMarginAmount + oPOSLine.MultiBuyMarginAmount + oPOSLine.HierarchyMarginAmount)
    
    sprdLines.Col = COL_MARKDOWN
    oPOSLine.MarkedDownStock = IIf(sprdLines.Value = "True", True, False)
    
    'Put Markdown serial number in - MO'C WIX1302
    sprdLines.Col = COL_MDSERIAL_NUMB
    oPOSLine.MarkDownSerialNumber = sprdLines.Text
    If oPOSLine.MarkDownSerialNumber <> "" Then
        oPOSLine.MarkedDownStock = True
        'oPOSLine.PriceOverrideReason
    End If
    
    sprdLines.Col = COL_RELITEM
    oPOSLine.RelatedItems = IIf(sprdLines.Value = "True", True, False)
    
    'Recalculate total including VAT but less events
    sprdLines.Col = COL_INCTOTAL
    curLineTotal = sprdLines.Value
    sprdLines.Value = sprdLines.Value - (oPOSLine.QtyBreakMarginAmount + _
        oPOSLine.DealGroupMarginAmount + oPOSLine.MultiBuyMarginAmount + oPOSLine.HierarchyMarginAmount)
    sprdLines.Formula = vbNullString 'disable recalc of QTY * Price
    Call sprdLines.ReCalc 'force recalc vat
    
    sprdLines.Col = COL_ORIGTOTAL
    sprdLines.Value = curLineTotal
    
    ' Line-level VAT Tracking
    sprdLines.Col = COL_VATTOTAL
    oPOSLine.VATAmount = sprdLines.Value
    
    sprdLines.Col = COL_VOUCHER
    If LenB(sprdLines.Value) <> 0 Then
        oPOSLine.SaleType = sprdLines.Value
    Else
        sprdLines.Col = COL_SALETYPE
        oPOSLine.SaleType = sprdLines.Value
    End If
    sprdLines.Col = COL_VATCODE
    oPOSLine.VATCodeNumber = Asc(sprdLines.Value) - Asc("`")
    
    sprdLines.Col = COL_ITEMTAG
    oPOSLine.ItemTagged = LenB(sprdLines.Value) <> 0
    
    sprdLines.Col = COL_VOIDED
    oPOSLine.LineReversed = Val(sprdLines.Value) > 0
    
    sprdLines.Col = COL_LINE_REV_CODE
    If (oPOSLine.LineReversed = True) Then oPOSLine.ReversalCode = sprdLines.Value
    
    'For Refunded items check if validated i.e. Line number from previous transaction
    sprdLines.Col = COL_RFND_LINENO
    If (Val(sprdLines.Value) = 0) Then curQty = Abs(curQty) 'switch to positive qty to include
    
    If mintWhichDiscountCard = enVerifyColleagueCardScheme Then '24/08/2007 MO'C Added - Wix1311 referral #35
        If (mblnApplyCollDisc = True) And ((oPOSLine.QuantitySold > 0) Or (curQty > 0)) And _
            (oPOSLine.LineReversed = False) And (oPOSLine.SaleType <> "D") And (oPOSLine.SaleType <> "V") And (oPOSLine.SaleType <> "C") Then
            oPOSLine.EmpSalePrimaryMarginAmount = Round((oPOSLine.ActualSellPrice - (oPOSLine.QtyBreakMarginAmount + _
                    oPOSLine.DealGroupMarginAmount + oPOSLine.MultiBuyMarginAmount + oPOSLine.HierarchyMarginAmount) / _
                    oPOSLine.QuantitySold) * moRetopt.EmployeeDiscount / 100 * oPOSLine.QuantitySold, 2)
            'Apply employee discount to line when saving
            oPOSLine.ActualSellPrice = Round(oPOSLine.ActualSellPrice * (100 - moRetopt.EmployeeDiscount) / 100, 2)
            oPOSLine.ExtendedValue = oPOSLine.ExtendedValue - oPOSLine.EmpSalePrimaryMarginAmount
            oPOSLine.VATAmount = Round(oPOSLine.VATAmount * (100 - moRetopt.EmployeeDiscount) / 100, 2)
            moTranHeader.DiscountAmount = moTranHeader.DiscountAmount - oPOSLine.EmpSalePrimaryMarginAmount
            oPOSLine.EmpSalePrimaryMarginAmount = oPOSLine.EmpSalePrimaryMarginAmount / oPOSLine.QuantitySold
            mblnColleagueDiscount = True
            mblnDiscountColleague = True
        End If
    Else
        If (Not moDiscountCardScheme Is Nothing) And ((oPOSLine.QuantitySold > 0) Or (curQty > 0)) And _
                (oPOSLine.LineReversed = False) And (oPOSLine.SaleType <> "D") And (oPOSLine.SaleType <> "V" And (oPOSLine.SaleType <> "C")) Then
            oPOSLine.EmpSalePrimaryMarginAmount = Round((oPOSLine.ActualSellPrice - (oPOSLine.QtyBreakMarginAmount + _
                    oPOSLine.DealGroupMarginAmount + oPOSLine.MultiBuyMarginAmount + oPOSLine.HierarchyMarginAmount) / _
                    oPOSLine.QuantitySold) * moDiscountCardScheme.DiscountRate / 100, 2) * oPOSLine.QuantitySold
            'Apply employee discount to line when saving
            oPOSLine.ActualSellPrice = Round(oPOSLine.ActualSellPrice * (100 - moDiscountCardScheme.DiscountRate) / 100, 2)
            oPOSLine.ExtendedValue = oPOSLine.ExtendedValue - oPOSLine.EmpSalePrimaryMarginAmount
            oPOSLine.VATAmount = Round(oPOSLine.VATAmount * (100 - moDiscountCardScheme.DiscountRate) / 100, 2)
            moTranHeader.DiscountAmount = moTranHeader.DiscountAmount - oPOSLine.EmpSalePrimaryMarginAmount
            oPOSLine.EmpSalePrimaryMarginAmount = oPOSLine.EmpSalePrimaryMarginAmount / oPOSLine.QuantitySold
            oPOSLine.EmpSalePrimaryMarginCode = moDiscountCardScheme.ErosionCode 'Added 11/06/07 M'OC
            mblnColleagueDiscount = True
            If InStr(UCase(moDiscountCardScheme.SchemeName), "COLLEAGUE") > 0 Then
                mblnDiscountColleague = True
            Else
                mblnDiscountColleague = False
            End If
        End If
    End If
    
    Call DebugMsg(MODULE_NAME, "SaveTranLine", endlDebug, "Row=" & lngRowNo & " Ext Totals")
    sprdLines.Col = COL_EXCTOTAL
    curExcTotal = (oPOSLine.ExtendedValue - oPOSLine.VATAmount - (oPOSLine.QtyBreakMarginAmount + _
        oPOSLine.DealGroupMarginAmount + oPOSLine.MultiBuyMarginAmount + oPOSLine.HierarchyMarginAmount))

    If (moRetopt.VATDiscountsActive = True) Then
        curVATDisc = moRetopt.VATDiscountPercentage / 100
    Else
        curVATDisc = 0
    End If

    curVATExcl = Round(oPOSLine.VATAmount * dblCCAllocPerc / 100 * curVATDisc, 2)
    oPOSLine.VATAmount = oPOSLine.VATAmount - curVATExcl
    
    If (oPOSLine.LineReversed = False) Then
        Call DebugMsg(MODULE_NAME, "SaveTranLine", endlDebug, "Row=" & lngRowNo & " Not Reversed")
        curExcTotal = curExcTotal - Round(curExcTotal * (dblCCAllocPerc / 100) * curVATDisc, 2)
    
        moTranHeader.ExVATValue(Asc(oPOSLine.VATCode) - 96) = _
                            moTranHeader.ExVATValue(Asc(oPOSLine.VATCode) - 96) + curExcTotal
        
        sprdLines.Col = COL_VATTOTAL
        moTranHeader.VATValue(Asc(oPOSLine.VATCode) - 96) = _
                            moTranHeader.VATValue(Asc(oPOSLine.VATCode) - 96) + oPOSLine.VATAmount
    
        'If WEEERate for line then add WEEE SKU to Transaction
        If (oPOSLine.WEEELineCharge <> 0) Then Call GetWEEELine(oPOSLine, oPOSLine.WEEELineCharge, oPOSLine.QuantitySold, strPRFSKU)
        
        'If transaction Line has negative quantity then treat as a refund.
        If (mblnParkingTran = False) Then
            If (oPOSLine.QuantitySold < 0) Or (Val(moTranHeader.OrderNumber) <> 0) Then
                Set oReturnCust = goDatabase.CreateBusinessObject(CLASSID_RETURNCUST)
                
                With oReturnCust
                    .TransactionDate = moTranHeader.TranDate
                    .PCTillID = moTranHeader.TillID
                    .TransactionNumber = moTranHeader.TransactionNo
                    .LineNumber = 0
                    
                    .CustomerName = mstrName
                    .AddressLine1 = mstrAddressLine1
                    .AddressLine2 = mstrAddressLine2
                    .AddressLine3 = mstrAddressLine3
                    .PostCode = mstrPostCode
                    .PhoneNo = mstrTelNo
                    .HouseName = ""
                    .MobileNo = mstrMobileNo
                    .WorkTelNumber = Mid(mstrWorkTelNo, 1, 1)
                    .Email = Mid(mstrEmail, 1, 1)
                    sprdLines.Col = COL_REFREASON
                    .LabelsRequired = ReturnLabelRequired(sprdLines.Value)
                    .RefundReason = "00"
                    If (blnCustDone = False) Then .SaveIfNew
                    blnCustDone = True
                    
                    .LineNumber = oPOSLine.SequenceNo
        
                    .CustomerName = vbNullString
                    .AddressLine1 = vbNullString
                    .AddressLine2 = vbNullString
                    .AddressLine3 = vbNullString
                    .PostCode = vbNullString
                    .PhoneNo = vbNullString
        
                    sprdLines.Col = COL_REFREASON
                    .RefundReason = sprdLines.Value
        
                    If Not moRefundOrigTran Is Nothing Then
                        If Val(moRefundOrigTran.StoreNumber) <> 0 Then
                            .OrigTranStore = moRefundOrigTran.StoreNumber
                        Else
                            .OrigTranStore = goSession.CurrentEnterprise.IEnterprise_StoreNumber
                        End If
                    End If
                    
                    sprdLines.Col = COL_RFND_DATE
                    If LenB(sprdLines.Value) <> 0 Then
                        .OrigTranDate = CDate(sprdLines.Value)
                        sprdLines.Col = COL_RFND_TILLNO
                        .OrigTranTill = sprdLines.Value
                        sprdLines.Col = COL_RFND_TRANID
                        .OrigTranNumb = sprdLines.Value
                        sprdLines.Col = COL_RFND_STORENO
                        .OrigTranStore = sprdLines.Value
                        sprdLines.Col = COL_RFND_LINENO
                        .OrigLineNumber = sprdLines.Value
                        ' Hubs 2.0 - Save the Web Order number for Head Office to cross reference
                        sprdLines.Col = COL_RFND_WEB_ORDER_NO
                        .WebOrderNumber = Trim$(sprdLines.Value)
                        ' NB Temporary fudge whilst still using Pervasive system AND only using Venda for web ordering system
                        ' WebOrderNumber field in pervasive limited to 11 chars, so if longer AND has a known Source prefix
                        ' then remove the prefix (the source prefix will be the irrelevant whilst there is only 1 source i.e. Venda)
                        ' NB Currently Source is WO, but might change to VE.  Hard code as should only
                        ' be a very TEMPORARY issue.
                        ' Been assured that the non prefix part will not exceed 9 characters, but still...
                        If Len(.WebOrderNumber) > 11 And Len(.WebOrderNumber) < 14 Then
                            If LCase(Left$(.WebOrderNumber, 2)) = "wo" _
                            Or LCase(Left$(.WebOrderNumber, 2)) = "ve" Then
                                .WebOrderNumber = Mid$(.WebOrderNumber, 3)
                            Else
                                ' what to do here?  Just hope it doesn't happen before
                                ' sql Back Offic rolled out
                            End If
                        End If
                        ' End of Hubs 2.0
                    End If
                    sprdLines.Col = COL_ORIGTRANVER
                    .OrigTranValidated = IIf(sprdLines.Value = "True", True, False)
                    '.OrigTenderType =
                    '.LabelsRequired = True
                    If (oPOSLine.QuantitySold < 0) Then .SaveIfNew
                End With
                
                'Added 14/08/08 - for Refund Items, check if against Order and if so update Order with Refunded Qty
                sprdLines.Col = COL_CUSTONO
                If (Val(sprdLines.Value) <> 0) And (TranVoided = False) And (mblnParkingTran = False) Then
                    'MO'C - Fix for Ref 418 don't save COR???? data if delivery satus is >699 and <800 and > 899
                    Set oOrderInfoBO = goDatabase.CreateBusinessObject(CLASSID_CUSTORDERINFO)
                    Call oOrderInfoBO.IBo_AddLoadFilter(CMP_EQUAL, FID_CUSTORDERINFO_OrderNumber, sprdLines.Value)
                    If oOrderInfoBO.LoadMatches.Count > 0 Then
                        'Minimum Line Status
                        'Ref 489
                        strOrderNo = Format(sprdLines.Text, "000000")
                        mMinimumStatus = Val(oOrderInfoBO.DeliveryRequestStatus)
                        If mMinimumStatus <> 9999 Then
                            Set oOrderLineBO = goDatabase.CreateBusinessObject(CLASSID_CUSTORDERLINE)
                            Call oOrderLineBO.IBo_AddLoadFilter(CMP_EQUAL, FID_CUSTORDERLINE_OrderNumber, strOrderNo)
                            Set colOrderLines = oOrderLineBO.LoadMatches
                            For lngLineNo = 1 To colOrderLines.Count
                                Set oOrderLineBO = colOrderLines(lngLineNo)
                                If oOrderLineBO.DeliveryRequestStatus < mMinimumStatus Then
                                    mMinimumStatus = oOrderLineBO.DeliveryRequestStatus
                                End If
                            Next lngLineNo
                        End If
                        'End of Ref 489

                        If Val(mMinimumStatus) > 699 And Val(mMinimumStatus) < 800 Or Val(mMinimumStatus) > 899 Then
                            'Dont save the refund details as this has not been
                            Set oOrderHeader = Nothing
                        Else
                            Set oOrderHeader = goDatabase.CreateBusinessObject(CLASSID_CUSTORDERHEADER)
                            strOrderNo = sprdLines.Text
                            'Record Order Number if not delivery SKU
                            sprdLines.Col = COL_SALETYPE
                            If Val(mstrStoreNo) < 8000 Then
                                strStoreNo = Format((Val(mstrStoreNo) + 8000), "0000")
                            Else
                                strStoreNo = mstrStoreNo
                            End If

                            ' Referral 868 - pass original tender object so can extract transaction keys filter
                            ' which will allow code to take into account any transaction lines not in the order
                            ' (because reversed etc) when comparing the sequence number against the order line
                            ' number to find corresponding lines to refund
                            
                                                        Dim TranTender As cPOSPayment
                            Set TranTender = New cPOSPayment
                            
                            sprdLines.Col = COL_RFND_TRANID
                            TranTender.TransactionNumber = sprdLines.Value
                            
                            sprdLines.Col = COL_RFND_DATE
                            TranTender.TransactionDate = sprdLines.Value
                            
                            sprdLines.Col = COL_RFND_TILLNO
                            TranTender.TillID = sprdLines.Value
                            
                            Call oOrderHeader.RecordItemRefund(strOrderNo, moTranHeader.TranDate, moTranHeader.TillID, moTranHeader.TransactionNo, strStoreNo, strSKU, sprdLines.Value <> "D", mcolRefundTrans, oPOSLine.SequenceNo, CInt(lngRowNo), TranTender)
                            
                            Set oOrderHeader = Nothing
                        End If
                    End If
                    
                    Set oOrderInfoBO = Nothing
                
                End If
            
            End If 'the Qty >0 and is a sale item
        
        End If
'    Else
'        mLineSkipped = mLineSkipped + 1
    End If 'a valid non-reversed line item
    
    sprdLines.Row = lngRowNo
    sprdLines.Col = COL_SELLPRICE
    sprdLines.Value = sprdLines.Value + oPOSLine.WEEELineCharge
    sprdLines.Col = COL_ORIGTOTAL
    sprdLines.Value = sprdLines.Value + (oPOSLine.WEEELineCharge * oPOSLine.QuantitySold)
    sprdLines.Col = COL_INCTOTAL
    sprdLines.Formula = ColToText(COL_QTY) & "# * " & ColToText(COL_SELLPRICE) & "#" ' / " & ColToText(COL_QTYSELLIN) & "#"
    DoEvents
    
End Sub

Private Sub GetWEEELine(cLine As cPOSLine, WEEERate As Currency, Quantity As Long, strPRFSKU As String)

Dim curExcTotal As Currency
Dim curLineTotal As Currency
Dim curVATExcl  As Currency

Dim oPOSLine    As cPOSLine
Dim oPRFSKU     As cInventory
Dim oPRFSKUW    As cInventoryWickes

    Call DebugMsg(MODULE_NAME, "GetWEEELine", endlDebug, "WEEERate=" & WEEERate & " for " & Quantity & " units")
    'add line to Till header for saving
    Set oPOSLine = moTranHeader.AddItem(strPRFSKU)
    oPOSLine.QuantitySold = Quantity
    oPOSLine.ActualSellPrice = WEEERate
    oPOSLine.LookUpPrice = WEEERate
    oPOSLine.ExtendedValue = WEEERate * Quantity
    oPOSLine.ExtendedCost = 0
    oPOSLine.DepartmentNumber = ""
    oPOSLine.VATCode = cLine.VATCode 'use same vat code as item line
    oPOSLine.SupervisorNumber = cLine.SupervisorNumber
    oPOSLine.ApplyToStock = False
    
    On Error Resume Next
    Set oPRFSKU = mcolWEEESKUs(strPRFSKU)
    Set oPRFSKUW = mcolWEEESKUs(strPRFSKU & "W")
    On Error GoTo 0
    
    sprdLines.Col = COL_HGROUP
    oPOSLine.HierGroup = oPRFSKUW.HierGroup
    sprdLines.Col = COL_HSUBGROUP
    oPOSLine.HierSubGroup = oPRFSKUW.HierSubGroup
    sprdLines.Col = COL_HCATEGORY
    oPOSLine.HierCategory = oPRFSKUW.HierCategory
    sprdLines.Col = COL_HSTYLE
    oPOSLine.HierStyle = oPRFSKUW.HierStyle
    
    oPOSLine.BackDoorCollect = False
    oPOSLine.BarcodeEntry = False
    
    'Recalculate total including VAT but less events
    ' Line-level VAT Tracking
    oPOSLine.VATAmount = 0
    
    oPOSLine.SaleType = "W"
    oPOSLine.VATCodeNumber = oPRFSKU.VATRate
    oPOSLine.ItemTagged = False
    oPOSLine.LineReversed = False
    
    oPOSLine.VATAmount = oPOSLine.ExtendedValue - Round(oPOSLine.ExtendedValue / (1 + (moVATRates.VATRate(oPOSLine.VATCodeNumber) / 100)), 2)
 
    moTranHeader.ExVATValue(oPOSLine.VATCodeNumber) = _
                        moTranHeader.ExVATValue(oPOSLine.VATCodeNumber) + oPOSLine.ExtendedValue - oPOSLine.VATAmount
    
    moTranHeader.VATValue(oPOSLine.VATCodeNumber) = _
                        moTranHeader.VATValue(oPOSLine.VATCodeNumber) + oPOSLine.VATAmount
    
End Sub

Private Function ReturnLabelRequired(strRefundCode As String) As Boolean

Dim oRefundCode As cRefundCode

    If (Trim$(strRefundCode) = "") Then Exit Function
    If (mcolRefundCodes Is Nothing) Then Call LoadRefundReasons

    For Each oRefundCode In mcolRefundCodes
        If (strRefundCode = oRefundCode.Code) Then ReturnLabelRequired = oRefundCode.PrintReturnLabel
    Next
    
End Function

Private Sub SaveEventLine(ByVal lngItemLineNo As Long, ByVal lngRowNo As Long)

Dim oEvent As cEventTranLine

    Call DebugMsg(MODULE_NAME, "SaveEventLine", endlDebug, "Saving Line " & lngItemLineNo)
    'go to the row that must be saved
    sprdLines.Row = lngRowNo
    'add line to Till header for saving
    Set oEvent = moTranHeader.AddEventItem(lngItemLineNo)
    sprdLines.Col = COL_OP_EVENTSEQNO
    sprdLines.Text = oEvent.EventSeqNo 'update line with Event Sequence No allocated
    sprdLines.Col = COL_INCTOTAL
    oEvent.DiscountAmount = Abs(Val(sprdLines.Value))
    sprdLines.Col = COL_OP_TYPE
    oEvent.EventType = sprdLines.Text

End Sub

Private Function SavePaidOut(blnVoided As Boolean) As Boolean

Dim oPOSPmt      As cPOSPayment
Dim lngLineNo    As Long
Dim dblAmount    As Double
Dim strTranCode  As String
Dim lngTotalPos  As Long
Dim lngFirstPmt  As Long
Dim strOStatus   As String
Dim intSequence  As Integer
Dim strPartCode     As String
Dim strDesc         As String
Dim strDesc2        As String
Dim dblQty          As Double
Dim dblPrice        As Double
Dim oConResHeaderBO As cConResHeader
Dim oConResDetail   As cConResDetail
Dim ReprintCount  As Integer    ' Commidea Into WSS - Referral 517 - Add receipt re-print number to receipt

    On Error GoTo HandleException
    
    strTranCode = mstrStoreNo & mstrTillNo & mstrTranId & Format$(moTranHeader.TranDate, "DDMMYY")
        
    If LenB(lblOrderNo.Caption) <> 0 Then
        strOStatus = "ORDER CANCELLATION"
    End If
    
    'MO'C WIX1345 - Save Concern Resolution Data
    If Not moConResHeaderBO Is Nothing Then
        With moConResHeaderBO
            .StoreID = mstrStoreNo
            .TransactionDate = Format(moTranHeader.TranDate, "yyyy-mm-dd")
            .TransactionTill = moTranHeader.TillID
            .TransactionNumber = moTranHeader.TransactionNo
            .ReasonCode = Format(mstrConResCode, "00")
            If Len(mstrConResDesc) <= 250 Then
                .ConcernDesc(1) = Mid(mstrConResDesc, 1)
            Else
                If Len(mstrConResDesc) <= 500 Then
                    .ConcernDesc(1) = Mid(mstrConResDesc, 1, 250)
                    .ConcernDesc(2) = Mid(mstrConResDesc, 251)
                End If
                If Len(mstrConResDesc) > 500 And Len(mstrConResDesc) <= 750 Then
                    .ConcernDesc(1) = Mid(mstrConResDesc, 1, 250)
                    .ConcernDesc(2) = Mid(mstrConResDesc, 251, 250)
                    .ConcernDesc(3) = Mid(mstrConResDesc, 501)
                End If
                If Len(mstrConResDesc) > 750 Then
                    .ConcernDesc(1) = Mid(mstrConResDesc, 1, 250)
                    .ConcernDesc(2) = Mid(mstrConResDesc, 251, 250)
                    .ConcernDesc(3) = Mid(mstrConResDesc, 501, 250)
                    .ConcernDesc(4) = Mid(mstrConResDesc, 751)
                End If
            End If
            .EmployeeID = moTranHeader.CashierID
            .RTI = "S"
            .IBo_SaveIfNew
        End With
    End If 'moConResHeaderBO
    '***************************************************
    
    Set oConResDetail = goDatabase.CreateBusinessObject(CLASSID_CONRESDETAIL)
    intSequence = 1
    'step through each line and save
    For lngLineNo = 2 To sprdLines.MaxRows Step 1
        sprdLines.Row = lngLineNo
        'Locate TOTAL line and where payments start
        sprdLines.Col = COL_LINETYPE
        If Val(sprdLines.Value) = LT_TOTAL Then lngTotalPos = lngLineNo
        If Val(sprdLines.Value) = LT_PMT And lngFirstPmt = 0 Then lngFirstPmt = lngLineNo
        'If LenB(lblOrderNo.Caption) <> 0 Then
        sprdLines.Col = COL_PARTCODE
        strPartCode = sprdLines.Text
        sprdLines.Col = COL_DESC
        strDesc = sprdLines.Text
        sprdLines.Col = COL_SIZE
        strDesc2 = sprdLines.Text
        sprdLines.Col = COL_QTY
        dblQty = Val(sprdLines.Value)
        sprdLines.Col = COL_SELLPRICE
        dblPrice = Val(sprdLines.Value)
        
        If strPartCode <> "" Then
            oConResDetail.PartCode = strPartCode
            oConResDetail.Quantity = dblQty
            oConResDetail.StoreID = mstrStoreNo
            oConResDetail.TransactionDate = Format(moTranHeader.TranDate, "yyyy-mm-dd")
            oConResDetail.TransactionNumber = moTranHeader.TransactionNo
            oConResDetail.TransactionTill = moTranHeader.TillID
            oConResDetail.RTI = "S"
            oConResDetail.ItemSequence = Format(intSequence, "00")
            oConResDetail.IBo_SaveIfNew
            intSequence = intSequence + 1
        End If
        'End If
    Next lngLineNo
    sprdLines.Col = COL_INCTOTAL
    sprdLines.Row = lngTotalPos
    moTranHeader.TotalSaleAmount = Val(sprdLines.Value)
    
    moTranHeader.NonMerchandiseAmount = Val(sprdLines.Value)
    
    If (Abs(moTranHeader.TotalSaleAmount) > 9999999) Then moTranHeader.TotalSaleAmount = 0
    If (Abs(moTranHeader.NonMerchandiseAmount) > 9999999) Then moTranHeader.NonMerchandiseAmount = 0
    
    moTranHeader.ReasonCode = mlngReasonCode
    moTranHeader.Description = mstrReasonDesc
    
    'step through each line and save and payment lines
    For lngLineNo = lngFirstPmt To sprdLines.MaxRows Step 1
        sprdLines.Row = lngLineNo
        'Ensure only lines that are SKU's
        sprdLines.Col = COL_LINETYPE
        If Val(sprdLines.Value) = LT_PMT Then
            'add payment line to Till header for saving
            sprdLines.Col = COL_INCTOTAL
            dblAmount = Val(sprdLines.Value)
            sprdLines.Col = COL_TENDERTYPE
            'Set oPOSPmt = moTranHeader.AddPayment(sprdLines.Text, dblAmount)
            'If transaction completed okay, then print payment line
            sprdLines.Col = COL_DESC
        End If 'saving line item
    Next lngLineNo
    
'    Call goDatabase.StartTransaction
    Call DebugMsg(MODULE_NAME, "SavePaidOut", endlDebug, "Started saving tran-" & mstrTranId)
    moTranHeader.Voided = blnVoided
    mstrTranId = moTranHeader.TransactionNo
    moTranHeader.TransactionComplete = True
    If goSession.ISession_IsOnline Then
        moTranHeader.Offline = False
    Else
        moTranHeader.Offline = True
    End If
    Call moTranHeader.SaveIfExists
    
    Call ZReadUpdate("TR", moTOSType.Code)
    Call UpdateFlashTotals(False, enftMiscPaidOut, moTranHeader.TotalSaleAmount)
    
    Dim oRecPrinting As New clsReceiptPrinting
    
    Set oRecPrinting.Printer = OPOSPrinter1
    Set oRecPrinting.TranHeader = moTranHeader
    oRecPrinting.TillNo = mstrTillNo
    oRecPrinting.TranNo = mstrTranId
    Set oRecPrinting.goRoot = goRoot
    Call oRecPrinting.Init(goSession, goDatabase)
    oRecPrinting.EFTDebugInfo = mstrEFTDebugInfo
    ' Commidea Into WSS
    ' Referral 522
    ' If merging Commidea receipt, need to reprint - unless it a void receipt with no commidea content
    If MergingCommideaAndWickesReceipts And Not (blnVoided And UBound(mavReceipts) = 0) Then
        Set moReceiptPrinting = oRecPrinting
        oRecPrinting.TranHeader.StoreNumber = mstrStoreNo
        oRecPrinting.UsingCommidea = True
        oRecPrinting.CommideaCCReceipts = mavReceipts
        ' Referral 525, 527 - Move loop condition to AFTER the print is done
        Do
            ' Commidea Into WSS - Referral 517 - Add receipt re-print number to receipt
            Call oRecPrinting.printReceipt(False, moDiscountCardScheme, ReprintCount)
            ReprintCount = ReprintCount + 1
        Loop While MsgBoxEx("Did the receipt print OK?" & vbNewLine & "[Yes] to continue, [No] to re-print.", _
                          vbQuestion + vbYesNo, _
                          "Receipt printed", , , , , RGBMSGBox_PromptColour) = vbNo
    Else
        Call oRecPrinting.printReceipt(False, moDiscountCardScheme)
    End If
    ' End of Commidea Into WSS
    
    If (moTranHeader.Offline = True) Then Call oRecPrinting.printReceipt(True, moDiscountCardScheme)
    Call DebugMsg(MODULE_NAME, "SavePaidOut", endlDebug, "Saved tran-" & mstrTranId)
'    Call goDatabase.CommitTransaction
    
    If (goSession.ISession_IsOnline) Then Call Shell(App.Path & "\CashierBalancingUpdate.exe T" & Format(Date, "YYYYMMDD") & moTranHeader.TillID & moTranHeader.TransactionNo, vbHide)
    
    Set moTranHeader = Nothing
    Call BackUpTransaction
        
    Exit Function

HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("SavePaidOut", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
        
End Function

Private Function SavePaidIn(blnVoided As Boolean) As Boolean

Dim oPOSPmt      As cPOSPayment
Dim lngLineNo    As Long
Dim dblAmount    As Double
Dim strTranCode  As String
Dim lngTotalPos  As Long
Dim lngFirstPmt  As Long
Dim ReprintCount  As Integer    ' Commidea Into WSS - Referral 517 - Add receipt re-print number to receipt

    On Error GoTo HandleException
    
    strTranCode = mstrStoreNo & mstrTillNo & mstrTranId & Format$(moTranHeader.TranDate, "DDMMYY")
        
    'step through each line and save
    For lngLineNo = 2 To sprdLines.MaxRows Step 1
        sprdLines.Row = lngLineNo
        'Locate TOTAL line and where payments start
        sprdLines.Col = COL_LINETYPE
        If Val(sprdLines.Value) = LT_TOTAL Then lngTotalPos = lngLineNo
        If Val(sprdLines.Value) = LT_PMT And lngFirstPmt = 0 Then lngFirstPmt = lngLineNo
    Next lngLineNo
    sprdLines.Col = COL_INCTOTAL
    sprdLines.Row = lngTotalPos
    moTranHeader.TotalSaleAmount = Val(sprdLines.Value)
    moTranHeader.NonMerchandiseAmount = Val(sprdLines.Value)
    If (Abs(moTranHeader.TotalSaleAmount) > 9999999) Then moTranHeader.TotalSaleAmount = 0
    If (Abs(moTranHeader.NonMerchandiseAmount) > 9999999) Then moTranHeader.NonMerchandiseAmount = 0
    
    moTranHeader.ReasonCode = mlngReasonCode
    moTranHeader.Description = mstrReasonDesc
    moTranHeader.Voided = blnVoided ' Referral 519 - set Void flag B4 printing
    
    'step through each line and save and payment lines
    For lngLineNo = lngFirstPmt To sprdLines.MaxRows Step 1
        sprdLines.Row = lngLineNo
        'Ensure only lines that are SKU's
        sprdLines.Col = COL_LINETYPE
        If Val(sprdLines.Value) = LT_PMT Then
            'add payment line to Till header for saving
            sprdLines.Col = COL_INCTOTAL
            dblAmount = Val(sprdLines.Value)
            sprdLines.Col = COL_TENDERTYPE
            'Set oPOSPmt = moTranHeader.AddPayment(sprdLines.Text, dblAmount)
            'If transaction completed okay, then print line item
            sprdLines.Col = COL_DESC
        End If 'saving line item
    Next lngLineNo
       
    Dim oRecPrinting As New clsReceiptPrinting
    
    Set oRecPrinting.Printer = OPOSPrinter1
    Set oRecPrinting.TranHeader = moTranHeader
    oRecPrinting.TillNo = mstrTillNo
    oRecPrinting.TranNo = mstrTranId
    Set oRecPrinting.goRoot = goRoot
    Call oRecPrinting.Init(goSession, goDatabase)
    oRecPrinting.EFTDebugInfo = mstrEFTDebugInfo
    ' Commidea Into WSS
    ' Referral 522
    ' If merging Commidea receipt, need to reprint - unless it a void receipt with no commidea content
    If MergingCommideaAndWickesReceipts And Not (blnVoided And UBound(mavReceipts) = 0) Then
        Set moReceiptPrinting = oRecPrinting
        oRecPrinting.TranHeader.StoreNumber = mstrStoreNo
        oRecPrinting.UsingCommidea = True
        oRecPrinting.CommideaCCReceipts = mavReceipts
        ' Referral 519, 527 - Move loop condition to AFTER the print is done
        Do
            ' Commidea Into WSS - Referral 517 - Add receipt re-print number to receipt
            Call oRecPrinting.printReceipt(False, moDiscountCardScheme, ReprintCount)
            ReprintCount = ReprintCount + 1
        Loop While MsgBoxEx("Did the receipt print OK?" & vbNewLine & "[Yes] to continue, [No] to re-print.", _
                          vbQuestion + vbYesNo, _
                          "Receipt printed", , , , , RGBMSGBox_PromptColour) = vbNo
    Else
        Call oRecPrinting.printReceipt(False, moDiscountCardScheme)
    End If
    ' End of Commidea Into WSS
    
    Call DebugMsg(MODULE_NAME, "SavePaidIn", endlDebug, "Started saving tran-" & mstrTranId)
    moTranHeader.Voided = blnVoided
    mstrTranId = moTranHeader.TransactionNo
    moTranHeader.TransactionComplete = True
    If goSession.ISession_IsOnline Then
        moTranHeader.Offline = False
    Else
        moTranHeader.Offline = True
    End If
    Call moTranHeader.SaveIfExists
    
    Call ZReadUpdate("TR", moTOSType.Code)
    Call UpdateFlashTotals(False, enftMiscPaidIn, moTranHeader.TotalSaleAmount)
    
    If (moTranHeader.Offline = True) Then Call oRecPrinting.printReceipt(True, moDiscountCardScheme)
    
    Call DebugMsg(MODULE_NAME, "SavepaidIn", endlDebug, "Saved tran-" & mstrTranId)
'    Call goDatabase.CommitTransaction
    
    If (goSession.ISession_IsOnline) Then Call Shell(App.Path & "\CashierBalancingUpdate.exe T" & Format(Date, "YYYYMMDD") & moTranHeader.TillID & moTranHeader.TransactionNo, vbHide)
    
    Set moTranHeader = Nothing
    
    Call BackUpTransaction
    
    Exit Function

HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("SavePaidIn", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
        
End Function

Private Function SaveOpenDrawer(blnVoided As Boolean) As Boolean

Dim strTranCode  As String
Dim ReprintCount  As Integer    ' Commidea Into WSS - Referral 517 - Add receipt re-print number to receipt

    strTranCode = mstrStoreNo & mstrTillNo & mstrTranId & Format$(moTranHeader.TranDate, "DDMMYY")
        
    moTranHeader.OpenDrawerCode = mlngReasonCode
    moTranHeader.Description = mstrReasonDesc
    
    If mblnUseCashDrawer And Not mblnTraining Then
        If Not blnVoided Then
            InitialiseCashDrawer
            OPOSCashDrawer.OpenDrawer
        End If
    End If
    'Call PrintOpenDrawerReasons(OPOSPrinter1, txtReasonDescription.Text)

'    Call goDatabase.StartTransaction
    Call DebugMsg(MODULE_NAME, "SaveOpenDrawer", endlDebug, "Started saving tran-" & mstrTranId)
    moTranHeader.Voided = blnVoided
    mstrTranId = moTranHeader.TransactionNo
    moTranHeader.TransactionComplete = True
    moTranHeader.Offline = True
    If goSession.ISession_IsOnline Then
        moTranHeader.Offline = False
    Else
        moTranHeader.Offline = True
    End If
    Call moTranHeader.SaveIfExists
    lblTranNo.Caption = mstrTranId
    Call BackUpTransaction
    
    If Not blnVoided Then
        Call ZReadUpdate("TR", moTOSType.Code)
        Call UpdateFlashTotals(False, enftMiscOpenDrawer, moTranHeader.TotalSaleAmount)
    End If
    
    Dim oRecPrinting As New clsReceiptPrinting
    Set oRecPrinting.Printer = OPOSPrinter1
    Set oRecPrinting.TranHeader = moTranHeader
    oRecPrinting.TillNo = mstrTillNo
    oRecPrinting.TranNo = mstrTranId
    Set oRecPrinting.goRoot = goRoot
    Call oRecPrinting.Init(goSession, goDatabase)
    ' Commidea Into WSS
    ' Referral 522
    ' If merging Commidea receipt, need to reprint - unless it a void receipt with no commidea content
    If MergingCommideaAndWickesReceipts And Not (blnVoided And UBound(mavReceipts) = 0) Then
        Set moReceiptPrinting = oRecPrinting
        oRecPrinting.TranHeader.StoreNumber = mstrStoreNo
        oRecPrinting.UsingCommidea = True
        oRecPrinting.CommideaCCReceipts = mavReceipts
        Do
            ' Commidea Into WSS - Referral 517 - Add receipt re-print number to receipt
            Call oRecPrinting.printReceipt(False, moDiscountCardScheme, ReprintCount)
            ReprintCount = ReprintCount + 1
        Loop While MsgBoxEx("Did the receipt print OK?" & vbNewLine & "[Yes] to continue, [No] to re-print.", _
                          vbQuestion + vbYesNo, _
                          "Receipt printed", , , , , RGBMSGBox_PromptColour) = vbNo
    Else
        Call oRecPrinting.printReceipt(False, moDiscountCardScheme)
    End If
    ' End of Commidea Into WSS
    
    Call DebugMsg(MODULE_NAME, "SaveOpenDrawer", endlDebug, "Saved tran-" & mstrTranId)
'    Call goDatabase.CommitTransaction
    
    If (moTranHeader.Offline = True) Then Call oRecPrinting.printReceipt(True, moDiscountCardScheme)
    
    If mblnUseCashDrawer And Not mblnTraining Then
        If moTranHeader.Voided = False Then
            CloseCashDrawer
        End If
    End If
    
    If (goSession.ISession_IsOnline) Then Call Shell(App.Path & "\CashierBalancingUpdate.exe T" & Format(Date, "YYYYMMDD") & moTranHeader.TillID & moTranHeader.TransactionNo, vbHide)
    
    Set moTranHeader = Nothing

End Function

Private Sub txtSKU_GotFocus()

    txtSKU.SelStart = 0
    txtSKU.SelLength = Len(txtSKU.Text)
    cmdViewLines.Caption = "Vi&ew Lines"

End Sub

Public Sub txtSKU_KeyPress(KeyAscii As Integer)

Dim dblOSAmount     As Double
Dim dblQuantity     As Double
Dim dblUnits        As Double
Dim dblUnitPrice    As Double
Dim blnAddItem      As Boolean
Dim blnOrderNow     As Boolean
Dim lngKeyIn        As Long
Dim lngLineNo       As Long
Dim strSKU          As String
Dim strValueHigh    As String
Dim strQtyHigh      As String
Dim strDeptNo       As String
Dim strDesc         As String
Dim curPayAmount    As Currency
Dim curListPrice    As Currency
Dim oTranLine       As cPOSLine
Dim strOvrdSKU      As String
Dim blnCheckRefund  As Boolean
Dim lngTType        As Long
Dim cPriceLineBO    As cPriceLineInfo
Dim curWEEERate     As Currency
Dim blnKeyPad       As Boolean
Dim strMarkDownSerial As String
    
    Call DebugMsg(MODULE_NAME, "txtSKU_KeyPress", endlTraceIn, "txtSKU=" & txtSKU.Text)
    'ensure only 1 decimal point allowed
    If (KeyAscii = 46) Then
        If mlngEntryMode = enemQty Then
            KeyAscii = 0
        ElseIf (InStr(txtSKU.Text, ".") > 0) Then
            KeyAscii = 0
        End If
    End If
    
'    If mlngEntryMode = enemSKU Then     'MO'C WIX1302
'        If mstrMarkDownStyle <> "" And Len(txtSKU.Text) = (Len(mstrMarkDownStyle) - 1) Then Exit Sub
'    End If
    
    'Process 0-9, . and -/+ sign
    If ((KeyAscii < 48) Or (KeyAscii > 57)) And (KeyAscii <> 43) And (KeyAscii <> 46) And _
        (KeyAscii <> 45) And (KeyAscii <> vbKeyReturn) And (KeyAscii <> 8) Then
        If mstrMarkDownStyle <> "" Then 'MO'C WIX 1302
            'Test for Markdown Barcode key entry
            If (KeyAscii = 109 Or KeyAscii = 77) And Len(txtSKU.Text) = 0 Then 'MO'C WIX 1302
                KeyAscii = 77
                Exit Sub
            End If
            
            If Len(txtSKU.Text) = (Len(mstrMarkDownStyle) - 1) Then 'MO'C WIX 1302
                If KeyAscii > 96 And KeyAscii < 123 Then
                    KeyAscii = KeyAscii And Not 32
                    Exit Sub
                End If
            Else
                KeyAscii = 0
                Exit Sub
            End If
        Else
            KeyAscii = 0
            Exit Sub
        End If
        
    End If
    
    'Check if user has pressed Enter to process entry
    If (KeyAscii = vbKeyReturn) Then
    
        If commideaTransactionInProgress Then
            Exit Sub
        End If
        
        
        sprdLines.Col = COL_PARTCODE
        For lngLineNo = 2 To sprdLines.MaxRows Step 1
            If (sprdLines.Text <> "" And sprdLines.Text = mstrGiftCardSKU And flSaleGiftCard = True) Then
                txtSKU.Text = ""
                Call MsgBoxEx("This transaction can only include the Sell/Top Up of a Gift Card." & vbNewLine & _
                                               "Please complete a separate transaction.", vbOKOnly + vbExclamation, "Warning", , , , , RGBMsgBox_WarnColour)
                Exit Sub
            End If
        Next lngLineNo

        Call DebugMsg(MODULE_NAME, "txtSKU_KeyPress", endlTraceIn, "CurrentMode=" & mlngCurrentMode & ", EntryMode=" & mlngEntryMode)
        KeyAscii = 0
                
        Select Case (mlngCurrentMode)
            Case (encmRecord):
                Select Case (mlngEntryMode)
                    Case (enemSKU):
                        ProcessSKU
                    
                    Case (enemQty):
                        ProcessQty
                        
                    Case (enemValue):
                        sprdLines.Col = COL_INCTOTAL
                        sprdLines.Row = sprdLines.MaxRows
                        sprdLines.Text = txtSKU.Text
                        Call SetEntryMode(enemDesc)
                        
                    Case (enemCostPrice):
                        'Check if limit is imposed and has not been exceeded, else force to use max value
                        If (Val(lblMaxValue.Caption) < Val(txtSKU.Text)) And (fraMaxValue.Visible = True) Then
                            Call MsgBoxEx("Price entered for return exceeds original price sold", _
                                          vbOKOnly, "High price entered", , , , , RGBMsgBox_WarnColour)
                            Exit Sub
                        End If
                        
                        sprdLines.Col = COL_COST
                        sprdLines.Text = Format$(txtSKU.Text, "0.00")
                        
                        txtSKU.SelStart = 0
                        txtSKU.SelLength = Len(txtSKU.Text)
                        If (txtSKU.Enabled = True) And (txtSKU.Visible = True) Then txtSKU.SetFocus
                                                
                        If (moTranHeader.TransactionCode = TT_REFUND) And (mblnEditingLine = True) Then
                            mblnEditingLine = False
                            Call sprdLines.SetSelection(-1, sprdLines.MaxRows, -1, sprdLines.MaxRows)
                            sprdLines.SelBackColor = -1
                        End If
                        
                        'Check if Refund and next line item must be entered or selected from Transaction
                        If ((moTranHeader.TransactionCode = TT_REFUND) And (mblnRefundLineMode = True)) Then
                            Call RetrieveSaleLine
                        Else
                            Call SetEntryMode(enemSKU)
                        End If
                        
                        sprdLines.Col = COL_PARTCODE
                        If (sprdLines.Text = "000000") Then
                            uctntPrice.Value = 0
                            Call SetEntryMode(enemPrice)
                        End If
                        
                    Case (enemPrice):
                        
                        'Check if limit is imposed and has not been exceeded, else force to use max value
                        If (Val(lblMaxValue.Caption) < Val(txtSKU.Text)) And (fraMaxValue.Visible = True) Then
                            Call MsgBoxEx("Price entered for return exceeds original price sold", _
                                          vbOKOnly, "High price entered", , , , , RGBMsgBox_WarnColour)
                            Exit Sub
                        End If
                        
                        sprdLines.Col = COL_PARTCODE
                        strSKU = sprdLines.Text
                        
                        sprdLines.Col = COL_ITEMNO
                        lngLineNo = CCur(sprdLines.Value)
                        
                        sprdLines.Col = COL_LOOKEDUP
                        curListPrice = CCur(sprdLines.Value)
                        
                        sprdLines.Col = COL_SELLPRICE
                        sprdLines.Text = Format$(txtSKU.Text, "0.00")
                        mstrDeptSalePrice = sprdLines.Text
                        
                        If (uctntPrice.Visible = True) And (uctntPrice.Enabled = True) Then uctntPrice.SetFocus
                        
                        If (Val(sprdLines.Value)) <> curListPrice Then
                            Call LoadReasonCodes(enrmPriceOverride)
                            Exit Sub
                        End If
                        
                        Call UpdateTotals
                        If (moTranHeader.TransactionCode = TT_REFUND) And (mblnEditingLine = True) Then
                            mblnEditingLine = False
                            Call sprdLines.SetSelection(-1, sprdLines.MaxRows, -1, sprdLines.MaxRows)
                            sprdLines.SelBackColor = -1
                        End If
                        'Check if Refund and next line item must be entered or selected from Transaction
                        If ((moTranHeader.TransactionCode = TT_REFUND) And (mblnRefundLineMode = True)) Then
                            Call RetrieveSaleLine
                        Else
                            Call SetEntryMode(enemSKU)
                        End If
                        
                        sprdLines.Col = COL_PARTCODE
                        If (sprdLines.Text = "000000") And (mbytQtyEntry <> QTY_ALWAYS_AFTER_SKU) Then
                            sprdLines.Col = COL_LOOKEDUP
                            sprdLines.Text = mstrDeptSalePrice
                            Call SetEntryMode(enemQty)
                        End If
                        
                End Select 'if entering an item to add to line
            Case (encmTender):
                Call DebugMsg(MODULE_NAME, "txtSKU_KeyPress", endlTraceIn, "CurrentMode=" & (moTenderOpts Is Nothing) & "," & (moOrigTend Is Nothing))
     
                Select Case (mlngEntryMode)
                    Case (enemValue):
                        'Process any other information for selected Tender Type
                        sprdTotal.Row = 3
                        sprdTotal.Col = COL_TOTVAL
                        
                        If Val(txtSKU.Text) = 0 Then ' And sprdLines.MaxRows = 1 Then
                            Exit Sub
                        End If
                        
                        Call DebugMsg(MODULE_NAME, "txtSKU_KeyPress", endlTraceIn, "CurrentMode=" & (moTenderOpts Is Nothing) & "," & (moOrigTend Is Nothing))
                        Dim oTenderOpts   As cTenderOptions
                        If ((moTenderOpts Is Nothing) = True) And ((moOrigTend Is Nothing) = False) Then
                            Call DebugMsg(MODULE_NAME, "txtSKU_KeyPress", endlTraceIn, "Type was blank so set type")
                            If (mcolTTypes Is Nothing) Then
                                Set mcolTTypes = New Collection
                                Set oTenderOpts = goSession.Database.CreateBusinessObject(CLASSID_TENDEROPTIONS)
                                Call oTenderOpts.AddLoadFilter(CMP_EQUAL, FID_TENDEROPTIONS_TendType, _
                                                               moOrigTend.tenderType)
                                Set mcolTTypes = oTenderOpts.IBo_LoadMatches
                            End If

                            Call DebugMsg(MODULE_NAME, "txtSKU_KeyPress", endlTraceIn, "Count is " & mcolTTypes.Count)
                            For lngTType = 1 To mcolTTypes.Count Step 1
                                Set oTenderOpts = mcolTTypes(lngTType)
                                Call DebugMsg(MODULE_NAME, "txtSKU_KeyPress", endlDebug, "Orig is " & moOrigTend.tenderType)
                                Call DebugMsg(MODULE_NAME, "txtSKU_KeyPress", endlDebug, "Col is " & oTenderOpts.TendType)
                                If (moOrigTend.tenderType = oTenderOpts.TendType) Then
                                    Call DebugMsg(MODULE_NAME, "txtSKU_KeyPress", endlDebug, "Matched at " & lngTType)
                                    Set moTenderOpts = mcolTTypes(lngTType)
                                    mstrTenderType = moTenderOpts.TendType
                                    Exit For
                                End If
                            Next lngTType
                        End If
                        
                        If ((mcolTTypes Is Nothing) = False) Then Call DebugMsg(MODULE_NAME, "txtSKU_KeyPress", endlDebug, "After Match Count is " & mcolTTypes.Count)
                        If moTenderOpts Is Nothing Then Set moTenderOpts = mcolTTypes(1)
                        If Abs(Val(txtSKU.Text)) > Abs(Val(sprdTotal.Value)) Then
                            If moTenderOpts.OverTenderAllowed = False Then
                                Call MsgBoxEx("Tender type does not allow over-tendering", vbOKOnly, "Invalid Tender Amount", , , , , RGBMsgBox_WarnColour)
                                txtSKU.Text = Format(sprdTotal.Value, "0.00")
                                uctntPrice.Value = Val(sprdTotal.Value)
                                uctntPrice.SelectAllValue
                                Exit Sub
                            End If
                            ' Refunding
                            If (Val(sprdTotal.Value) < 0) Then
                                Call MsgBoxEx("Over-tendering is not allowed for refund", vbOKOnly, "Invalid Tender Amount", , , , , RGBMsgBox_WarnColour)
                                txtSKU.Text = Format(Abs(Val(sprdTotal.Value)), "0.00")
                                uctntPrice.Value = Abs(Val(sprdTotal.Value))
                                uctntPrice.SelectAllValue
                                Exit Sub
                            End If
                            If (Abs(Val(txtSKU.Text)) - Abs(Val(sprdTotal.Value))) > moRetopt.MaximumCashGiven Then
                                Call MsgBoxEx("Tender amount would exceed maximum change allowed", _
                                              vbOKOnly, "Invalid Tender Amount", , , , , RGBMsgBox_WarnColour)
                                uctntPrice.Value = 0
                                Exit Sub
                            End If
                        End If
                                           
                        If (mstrTenderType = TEN_GIFTCARD) Then
                            If (sprdTotal.Value < 0) Then
                                If (Val(uctntPrice.Value) > mcurMaxRefundGiftCardValue) Then
                                    txtSKU.Visible = False
                                    Call MsgBoxEx("You have exceeded the maximum allowance that can be refunded to a Gift Card" & vbNewLine & _
                                    " for this transaction.", vbOKOnly + vbExclamation, "Warning", , , , , RGBMsgBox_WarnColour)
                                    uctntPrice.Value = Format(mcurMaxRefundGiftCardValue, "0.00")
                                    uctntPrice.SelectAllValue
                                    Exit Sub
                                End If '(Val(uctntPrice.Value) > mcurMaxRefundGiftCardValue)
                                If (Val(uctntPrice.Value) < mcurMinRefundGiftCardValue) Then
                                    txtSKU.Visible = False
                                    Call MsgBoxEx("You have specified the value less than the minimum allowance that can be refunded to a Gift Card" & vbNewLine & _
                                    " for this transaction.", vbOKOnly + vbExclamation, "Warning", , , , , RGBMsgBox_WarnColour)
                                    uctntPrice.Value = Format(mcurMinRefundGiftCardValue, "0.00")
                                    uctntPrice.SelectAllValue
                                    Exit Sub
                                End If '(Val(uctntPrice.Value) < mcurMinRefundGiftCardValue)
                            End If '(sprdTotal.Value < 0)
                            If (sprdTotal.Value > 0 And Val(uctntPrice.Value) > mcurMaxRedeemGiftCardValue) Then
                                txtSKU.Visible = False
                                Call MsgBoxEx("You have exceeded the maximum allowance that can be redeemed from a Gift Card" & vbNewLine & _
                                " for this transaction.", vbOKOnly + vbExclamation, "Warning", , , , , RGBMsgBox_WarnColour)
                                uctntPrice.Value = Format(mcurMaxRedeemGiftCardValue, "0.00")
                                uctntPrice.SelectAllValue
                                Exit Sub
                            End If '(sprdTotal.Value > 0 And Val(uctntPrice.Value) > mcurMaxRedeemGiftCardValue)
                            If moTranHeader.TransactionCode = TT_REFUND Then
                                InitialiseCashDrawer
                                If mblnUseCashDrawer And Not mblnTraining Then
                                    OPOSCashDrawer.OpenDrawer
                                End If
                                Call MsgBoxEx("Get a gift card", vbOKOnly, _
                                "Gift Card", , , , , RGBMSGBox_PromptColour)
                                CloseCashDrawer
                            End If 'If moTranHeader.TransactionCode = TT_REFUND
                        End If '(mstrTenderType = TEN_GIFTCARD)
                        
'                        If moTranHeader.TransactionCode = TT_SALE Then
'                            If sprdTotal.Text < 0 Then
'                                mlngTranSign = -1
'                                moTranHeader.TransactionCode = TT_REFUND
'                                For Each moTOSType In mcolTOSOpt
'                                    If Val(moTOSType.DisplaySeq) = 5 Then Exit For
'                                Next moTOSType
'                            End If
'                        ElseIf moTranHeader.TransactionCode = TT_REFUND Then
'                            If sprdTotal.Text >= 0 Then
'                                mlngTranSign = 1
'                                moTranHeader.TransactionCode = TT_SALE
'                                Set moTOSType = mcolTOSOpt(1)
'                            End If
'                        End If
'
                        sprdLines.Col = COL_LINETYPE
                        sprdLines.Text = LT_PMT
    
                        sprdLines.Row = ROW_TOTALS
                        sprdLines.Col = COL_INCTOTAL
                        curPayAmount = Val(txtSKU.Text) * -mlngTranSign
                        Call ProcessPaymentType(mstrTenderType, curPayAmount)
                        If curPayAmount = 0 Then 'WIX1376 Changed to help with Dropped Line Bug
                            Unload frmTender
                            Call Form_KeyPress(vbKeyEscape)
                            Exit Sub
                        End If
                        Call UpdatePayments(curPayAmount)
                        
                End Select 'if in Tender Mode
            
            Case (encmPaidOut):
                Select Case (mlngEntryMode)
                    Case (enemValue)
                        sprdLines.Col = COL_INCTOTAL
                        sprdLines.Row = sprdLines.MaxRows
                        sprdLines.Text = Val(txtSKU.Text) * mlngTranSign
                        Call SetGridTotals(sprdLines.MaxRows)
                        Call UpdateTotals
                        Call SetEntryMode(enemNone)
                        Call RecordTender(False, False, False)
                        
                    Case (enemDesc)
                        sprdLines.Col = COL_INCTOTAL
                        sprdLines.Row = sprdLines.MaxRows
                        sprdLines.Text = txtSKU.Text
                        Call SetEntryMode(enemNone)
                    
                    Case (enemSKU)
                        Call ProcessSKU(True)
                                            
                    Case (enemQty)
                        Call ProcessQty(True)
                
                End Select
                
            Case (encmPaidIn):
                Select Case (mlngEntryMode)
                    Case (enemValue)
                        sprdLines.Col = COL_INCTOTAL
                        sprdLines.Row = sprdLines.MaxRows
                        sprdLines.Text = Val(txtSKU.Text) * mlngTranSign
                        Call SetGridTotals(sprdLines.MaxRows)
                        Call UpdateTotals
                        Call SetEntryMode(enemNone)
                        Call RecordTender(False, False, False)
                    Case (enemDesc)
                        sprdLines.Col = COL_INCTOTAL
                        sprdLines.Row = sprdLines.MaxRows
                        sprdLines.Text = txtSKU.Text
                        Call SetEntryMode(enemNone)
                End Select
            Case (encmOverrideCode):
                Select Case (mlngEntryMode)
                    Case (enemSKU):
                        sprdLines.MaxRows = sprdLines.MaxRows + 1
                        sprdLines.Row = sprdLines.MaxRows
                        sprdLines.Col = 0
                        sprdLines.Text = " " 'override auto column lettering
                        sprdLines.Col = COL_PARTCODE
                        If Len(txtSKU.Text) < PARTCODE_LEN Then
                                txtSKU.Text = Left$(PARTCODE_PAD, PARTCODE_LEN - Len(txtSKU.Text)) & txtSKU.Text
                        End If
                        sprdLines.Text = txtSKU.Text
                        Call sprdLines_EditMode(COL_PARTCODE, sprdLines.Row, 0, True)
                        sprdLines.Col = COL_PARTCODE
                        If LenB(sprdLines.Text) = 0 Then Exit Sub
                        sprdLines.Col = COL_QTY
                        Call SetEntryMode(enemQty)
                    Case (enemQty):
                        mdblLineQuantity = Val(txtSKU.Text)
                        sprdLines.Col = COL_QTY
                        sprdLines.Text = Val(txtSKU.Text)
                        sprdLines.Col = COL_LINETYPE
                        sprdLines.Text = LT_ITEM
                        Call UpdateTotals
                        uctntPrice.Value = 0
                        Call SetEntryMode(enemPrice)
                    Case (enemPrice):
                        
                        If (mblnUseWERates = True) Then
                            sprdLines.Col = COL_WEEE_RATE
                            curWEEERate = Val(sprdLines.Value)
                        End If
                        sprdLines.Col = COL_LOOKEDUP
                        
                        If Val(txtSKU.Text) = 0 Then
                            Call SetEntryMode(enemPrice)
                            DoEvents
                            uctntPrice.SelectAllValue
                            Exit Sub
                        End If
                        
                        'MO'C WIX 1302 - Check for Manager  Authorisation here ..
                        sprdLines.Col = COL_MDSERIAL_NUMB
                        strMarkDownSerial = sprdLines.Text
                        sprdLines.Col = COL_PARTCODE
                        strSKU = sprdLines.Text
                        If strMarkDownSerial <> "" Or GetAuthorisationLimit(Val(txtSKU.Text), strSKU, strMarkDownSerial) Then
                           If frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) = False Then
                                mlngCurrentMode = encmRecord
                                Call SetEntryMode(enemSKU)
'                                Call SetGridTotals(sprdLines.MaxRows)
'                                Call UpdateTotals
                                Unload frmVerifyPwd
                                Exit Sub
                           End If
                        End If
                        
                        'WIX1165 - for Price Overrides, check if Price Amended so can only override from there downwards
                        sprdLines.Col = COL_DAY_PRICE
                        If (Val(sprdLines.Value) > 0) Then
                            If Val(txtSKU.Text) > CCur(Val(sprdLines.Text) - Val(GetGridData(sprdLines, COL_TEMPAMOUNT, sprdLines.Row))) Then
                                sprdLines.Col = COL_QTY
                                If Val(sprdLines.Text) > 0 Then
                                    ' Invalid price.  Greater than looked up
                                    Call MsgBoxEx("Price HIGHER than amended retail", vbOKOnly, _
                                                  "Invalid price entered", , , , , RGBMsgBox_WarnColour)
                                    sprdLines.Col = COL_DAY_PRICE
                                    uctntPrice.Value = Val(sprdLines.Text)
                                    Call SetEntryMode(enemPrice)
                                    Exit Sub
                                End If
                                sprdLines.Col = COL_LOOKEDUP
                            End If
                        End If
                        
                        sprdLines.Col = COL_LOOKEDUP
                        
                       'existing validation code
                       On Error GoTo 0
                       If Val(txtSKU.Text) > CCur(Val(sprdLines.Text) - Val(GetGridData(sprdLines, COL_TEMPAMOUNT, sprdLines.Row))) Then
                           sprdLines.Col = COL_QTY
                           If Val(sprdLines.Text) > 0 Then
                               ' Invalid price.  Greater than looked up
                               Call MsgBoxEx("Price HIGHER than retail", vbOKOnly, _
                                             "Invalid price entered", , , , , RGBMsgBox_WarnColour)
                               uctntPrice.Value = Val(sprdLines.Text)
                               Call SetEntryMode(enemPrice)
                               Exit Sub
                           End If
                           sprdLines.Col = COL_LOOKEDUP
                       End If

                        If (Val(sprdLines.Text) - Val(txtSKU.Text)) > 999.99 Then
                            If MsgBoxEx("Price entered adjusts prices by more than 999.99" & _
                                        vbNewLine & vbNewLine & "Continue?", vbYesNo, _
                                        "Suspicious price entered", , , , , RGBMsgBox_WarnColour) = vbNo Then
                                uctntPrice.Value = Val(sprdLines.Text)
                                Call SetEntryMode(enemPrice)
                                Exit Sub
                            End If
                        End If
                        
                        sprdLines.Col = COL_MARKDOWN
                        If sprdLines.Value = "True" Then
                            Load frmVerifyPwd
                            If frmVerifyPwd.VerifySupervisorPassword(ucKeyPad1.Visible) = False Then
                                sprdLines.MaxRows = sprdLines.MaxRows - 1
                                sprdLines.Row = sprdLines.MaxRows
                                mlngCurrentMode = encmRecord
                                Call SetEntryMode(enemSKU)
                                Call SetGridTotals(sprdLines.MaxRows)
                                Call UpdateTotals
                                Unload frmVerifyPwd
                                Exit Sub
                            End If
                            moTranHeader.SupervisorUsed = True
                            moTranHeader.SupervisorNo = frmVerifyPwd.txtAuthID.Text
                            Unload frmVerifyPwd
                        End If
                        
                        sprdLines.Col = COL_PARTCODE
                        strSKU = sprdLines.Value
                        sprdLines.Col = COL_SELLPRICE
                        If mblnPricePromise = True Then
                            mdblOrigSellingPrice = sprdLines.Text
                            mcurCompetitorsPrice = Val(txtSKU.Text)
                            sprdLines.Col = COL_QTY
                            mdblLineQuantity = Abs(sprdLines.Value)
                            Set cPriceLineBO = moTranHeader.GetPricePromiseLine(CLng(moTranHeader.GetPricePromiseLine.Count))
                            If (cPriceLineBO.OrigTransactionNumber <> "") Then
                                Call moEvents.DeleteSKU(strSKU, sprdLines.MaxRows) 'delete original line from events as only refunding money
                                mlngCurrentMode = encmTranType
                                sprdLines.MaxRows = sprdLines.MaxRows + 1
                                Call sprdLines.InsertRows(sprdLines.MaxRows - 1, 1)
                                sprdLines.Row = sprdLines.MaxRows - 1
                                sprdLines.Col = 0
                                sprdLines.Text = " " 'override auto column lettering
                                sprdLines.Col = COL_LINETYPE
                                sprdLines.Text = LT_ITEM
                                sprdLines.Col = COL_PARTCODE
                                sprdLines.Value = strSKU
                                Call GetItem(strSKU, False)
                                Call moEvents.DeleteSKU(strSKU, sprdLines.Row) 'delete original line from events as only refunding money
                                sprdLines.Col = COL_QTY
                                sprdLines.Text = mdblLineQuantity * -1
                                sprdLines.Col = COL_SELLPRICE
                                sprdLines.Text = cPriceLineBO.OrigSellingPrice + curWEEERate
    
                                sprdLines.Col = COL_REFREASON
                                sprdLines.Text = goSession.GetParameter(PRM_PRICEMATCHREFCODE)

                                sprdLines.Col = COL_RFND_STORENO
                                sprdLines.Text = cPriceLineBO.OrigStoreNumber
                                sprdLines.Col = COL_RFND_DATE
                                sprdLines.Text = cPriceLineBO.OrigTransDate
                                sprdLines.Col = COL_RFND_TILLNO
                                sprdLines.Text = cPriceLineBO.OrigTillNumber
                                sprdLines.Col = COL_RFND_TRANID
                                sprdLines.Text = cPriceLineBO.OrigTransactionNumber
                                sprdLines.Col = COL_PPLINE
                                sprdLines.Value = 1
                            Else
                                'for Price match update event line so it uses the reduced price
                                Call moEvents.UpdateSKUPrice(strSKU, sprdLines.Row, mcurCompetitorsPrice)
                                sprdLines.Col = COL_WEEE_RATE
                            End If
                            sprdLines.Row = sprdLines.MaxRows
                            sprdLines.Col = COL_DESC
                            strDesc = sprdLines.Text
                            sprdLines.Col = COL_ITEMNO
                            Call InsertPricePromise(sprdLines.Row, (mdblOrigSellingPrice - mcurCompetitorsPrice - curWEEERate) * -1, (mdblOrigSellingPrice - mcurCompetitorsPrice - curWEEERate) * mdblLineQuantity * -1, moTranHeader.GetPricePromiseLine.Count)
                            mdblLineQuantity = 1
                        Else
                            sprdLines.Text = txtSKU.Text
                        End If
                        sprdLines.Col = COL_PARTCODE
                        strSKU = sprdLines.Text
                        
                        sprdLines.Col = COL_ITEMNO
                        lngLineNo = Val(sprdLines.Value)
                        
                        sprdLines.Col = COL_QTY
                        If (Val(sprdLines.Value) < 0) Then blnCheckRefund = True
                        
                        Call moEvents.UpdateSKUPrice(strSKU, lngLineNo, Val(txtSKU.Text))
                        If (mblnUseWERates = True) Then
                            sprdLines.Col = COL_WEEE_RATE
                            curWEEERate = Val(sprdLines.Value)
                            sprdLines.Col = COL_SELLPRICE
                            sprdLines.Value = Val(sprdLines.Value) + curWEEERate
                        End If
                        Call UpdateTotals
'                        If blnCheckRefund And mblnValidItem Then
'                            sprdLines.Col = COL_LOOKEDUP
'                            If Val(txtSKU.Text) <> Val(sprdLines.Text) Then
'                                mlngCurrentMode = encmRefundPriceOverride
'                                Call LoadReasonCodes(enrmPriceOverride)
'                            Else
'                                mlngCurrentMode = encmRecord
'                                Call SetEntryMode(enemSKU)
'                            End If
'                        Else
                            mlngCurrentMode = encmRecord
                            Call SetEntryMode(enemSKU)
'                        End If

                    
                    Case (enemPerc):
                        sprdLines.Col = COL_SELLPRICE
                        sprdLines.Text = txtSKU.Text
                        mlngCurrentMode = encmRecord
                        Call SetEntryMode(enemSKU)
                        Call UpdateTotals
                    
                 End Select
            Case (encmGiftVoucher):
                If (Val(uctntPrice.Value) < mcurMinGiftCardValue) Then
                    txtSKU.Visible = False
                    Call MsgBoxEx("You have specified the value less than the minimum allowance that can be added to a Gift Card" & vbNewLine & _
                    " for this transaction.", vbOKOnly + vbExclamation, "Warning", , , , , RGBMsgBox_WarnColour)
                    uctntPrice.Value = Format(mcurMinGiftCardValue, "0.00")
                    uctntPrice.SelectAllValue
                    Exit Sub '(Val(uctntPrice.Value) < mcurMinGiftCardValue)
                End If
                
                If (Val(uctntPrice.Value) <= mcurMaxGiftCardValue) Then
                    Call SetEntryMode(enemNone)
                    Call AddVoucherSKUs(Val(txtSKU.Text))
                    Call SetEntryMode(enemSKU)
                    Call EnableAction(enacVoid, True)
                    Call EnableAction(enacPark, True)
                    Call EnableAction(enacReversal, True)
                    mlngCurrentMode = encmRecord
                Else
                    txtSKU.Visible = False
                    Call MsgBoxEx("You have exceeded the maximum allowance that can be added to a Gift Card" & vbNewLine & _
                    " for this transaction.", vbOKOnly + vbExclamation, "Warning", , , , , RGBMsgBox_WarnColour)
                    uctntPrice.Value = Format(mcurMaxGiftCardValue, "0.00")
                    uctntPrice.SelectAllValue
                End If '(Val(uctntPrice.Value) <= mcurMaxGiftCardValue)
            End Select 'if in Price Override entry
    End If

End Sub

Private Sub ProcessSKU(Optional blnPaidOut As Boolean = False)
    
Dim blnKeyPad       As Boolean
Dim blnCheckRefund  As Boolean
Dim strOvrdSKU      As String
Dim oTranLine       As cPOSLine

    On Error GoTo HandleException
    
    mstrKeyedCode = ""
    mstrKeyedSKU = ""
  
    If (Left$(lblOrderNo.Caption, 1) = "Q") Then
        lblOrderNo.Caption = ""
        Call ClearEvents
        mdblTotalBeforeDis = 0
    End If
    'WIX1202 - Check if keyed item exceeds limit and if so then prompt for reason why keyed
    If (txtSKU.Text <> mstrGiftVoucherSKU) And (txtSKU.Text <> "000000") And _
        (mdblLineQuantity > 0) And (IsDeliveryChargeCharitySKU(txtSKU.Text) = False) Then mlngBCodeCheckCnt = mlngBCodeCheckCnt + 1
    'Check if Barcode Reader is broken but logging still required
    If (mblnBCodeCheck = True) Then
        If (mstrBCodeBrokenCode <> "") Then
            mlngBCodeCheckCnt = 0
            mstrKeyedCode = mstrBCodeBrokenCode
            mstrKeyedSKU = txtSKU.Text
        Else
            If (mblnBarcoded = False) Then
                If (mlngBCodeCheckCnt >= goSession.GetParameter(PRM_BARCODE_CHECK_FREQ)) And (Len(txtSKU.Text) < 17) Then
                    mlngBCodeCheckCnt = 0
                    Call LoadReasonCodes(enrmBarcodeFailed)
                    While (mlngCurrentMode = encmBarcodeAudit)
                        DoEvents
                    Wend
                Else
                    mstrKeyedCode = mstrBCodeAuditCode
                    mstrKeyedSKU = txtSKU.Text
                End If
            Else
                mstrKeyedCode = ""
                mstrKeyedSKU = ""
            End If
        End If
    End If 'Keyed entry Auditing
    'Added 12/9/07 - for Specific SKUs do not Audit if keyed
    If (txtSKU.Text = mstrGiftVoucherSKU) Or (txtSKU.Text = "000000") Or _
            (mdblLineQuantity < 0) Or (IsDeliveryChargeCharitySKU(txtSKU.Text) = True) Then
        mstrKeyedCode = ""
        mstrKeyedSKU = ""
    End If
    
    If txtSKU = "000000" Then
        txtSKU.Text = "20000004"
    End If
    
    If txtSKU.Text Like "*[!0-9]*" And (Left(txtSKU.Text, 1) <> "M") And (Left(txtSKU.Text, 1) <> "*") Then 'MO'C WIX 1302/1165
        mlngEntryMode = enemNone 'disable barcode processing until Msg OK'ed
        Call MsgBoxEx("Invalid SKU", vbExclamation + vbOKOnly, "SKU", , , , , _
                      RGBMsgBox_WarnColour)
        mlngEntryMode = enemSKU
    
        txtSKU.Text = vbNullString
        If (txtSKU.Enabled = True) And (txtSKU.Visible = True) Then txtSKU.SetFocus
        Exit Sub
    End If
    
    If Len(txtSKU.Text) = 8 Then
        ' Wickes EAN
        If Left$(txtSKU.Text, 1) = "2" Then
            If txtSKU.Text = "20000004" Then
                If mblnIsRefundLine Then
                    mlngEntryMode = enemNone
                    Call MsgBoxEx("Only allowed in Sale", vbInformation, , , , , , _
                                  RGBMsgBox_WarnColour)
                    mlngEntryMode = enemSKU
                    txtSKU.Text = vbNullString
                    If (txtSKU.Enabled = True) And (txtSKU.Visible = True) Then txtSKU.SetFocus
                    Exit Sub
                End If
                
                Call SetEntryMode(enemNone)
                fraActions.Visible = False
                Load frmEntry
                If (ucKeyPad1.Visible = True) Then frmEntry.ShowNumPad
                blnKeyPad = ucKeyPad1.Visible
                ucKeyPad1.Visible = False
                strOvrdSKU = Right("000000" & frmEntry.GetEntry("Enter Actual SKU", _
                                   "Mark-Down Item", RGBQuery_BackColour, 6, True), 6)
                ucKeyPad1.Visible = blnKeyPad
                Unload frmEntry
                Call SetEntryMode(enemSKU)
                txtSKU.Text = strOvrdSKU
                
                If txtSKU = "000000" Then
                    txtSKU.Text = vbNullString
                    If (txtSKU.Enabled = True) And (txtSKU.Visible = True) Then txtSKU.SetFocus
                    Exit Sub
                End If
                
                If AddItemToLines Then
                    sprdLines.Row = sprdLines.MaxRows
                    sprdLines.Col = COL_MARKDOWN
                    sprdLines.Value = "True"
                    Call ItemOverride
                End If
                
                Exit Sub
            End If
        End If
    End If
    If (Len(txtSKU.Text) = 17) And (mblnAllowCoupons = True) Then
        Call RetrieveCoupon(txtSKU.Text, False)
        mdblLineQuantity = 1
        Call SetEntryMode(enemSKU)
        Exit Sub
    End If
    
    If mdblLineQuantity < 0 Then
        blnCheckRefund = True
    Else
        blnCheckRefund = False
    End If
    Call AddItemToLines(blnPaidOut)
    Call SetEntryMode(enemSKU)
    If blnCheckRefund And mblnValidItem And (1 = 0) Then
        sprdLines.Col = COL_RFND_STORENO
        If (Val(sprdLines.Value) <> goSession.CurrentEnterprise.IEnterprise_StoreNumber) Then
            mblnIsRefundLine = True
            Call SetEntryMode(enemPrice)
            mlngCurrentMode = encmOverrideCode
            mblnIsRefundLine = False
        End If
    End If
    
    If mblnValidItem Then
        If Not moRefundOrigTran Is Nothing Then
            If mdblLineQuantity < 0 Then
                If Val(moRefundOrigTran.StoreNumber) <> 0 Then
                    
                    sprdLines.Col = COL_PARTCODE
                    sprdLines.Row = sprdLines.MaxRows
                
                    For Each oTranLine In moRefundOrigTran.Lines
                        If oTranLine.PartCode = sprdLines.Text Then Exit For
                    Next
                
                    If oTranLine Is Nothing Then
                        mlngEntryMode = enemNone
                        Call MsgBoxEx("Item not found in original transaction", _
                                      vbOKOnly, "Invalid Item", , , , , _
                                      RGBMsgBox_WarnColour)
                        mlngEntryMode = enemSKU
                        sprdLines.MaxRows = sprdLines.MaxRows - 1
                        Call SetGridTotals(sprdLines.MaxRows)
                        Call UpdateTotals
                        Exit Sub
                    End If
                    
                    sprdLines.Col = COL_LOOKEDUP
                    sprdLines.Text = oTranLine.ActualSellPrice
                    Call SetEntryMode(enemPrice)
                End If
            End If
        End If
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("ProcessSKU", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Function RetrieveCoupon(strCouponNo As String, GiveCoupon As Boolean) As Boolean

Dim oCouponMaster As cOPEvents_Wickes.cCouponMaster
Dim strCouponID   As String 'taken from positions 1-7
Dim strMarketRef  As String 'taken from positions 8-11
Dim strSerialNo   As String 'taken from positions 12-17
Dim lngRowNo      As Long 'for checking if other exclusive coupon exists
Dim blnOtherFnd   As Boolean
Dim blnExclFnd    As Boolean
Dim lngCouponResp As Long
Dim vntAddress    As Variant
Dim strMgrID      As String
Dim strProcedureName As String
'Dim RetrieveCouponFactory As New COMTPWickes_InterOp_Wrapper.RetrieveCoupon

    On Error GoTo HandleException
    
    strProcedureName = "RetrieveCoupon"
    
    If EnableBuyCoupons = True Then
        Set mRetrieveCoupon = New COMTPWickes_InterOp_Implementation.RetrieveCoupon
        'Set mRetrieveCoupon = RetrieveCouponFactory.FactoryGet
        If GiveCoupon = False Then
            mRetrieveCoupon.RetrieveBuyCoupon strCouponNo
        Else
            mRetrieveCoupon.RetrieveGetCoupon strCouponNo
        End If
    Else
        Set oCouponMaster = goDatabase.CreateBusinessObject(CLASSID_COUPONFMT)
        strCouponID = Left$(strCouponNo, 7)
        Call oCouponMaster.AddLoadFilter(CMP_EQUAL, FID_COUPONFMT_CouponID, strCouponID)
        If (oCouponMaster.LoadMatches.Count = 0) Then
            Call MsgBoxEx("Invalid Coupon", vbOKOnly, "Invalid Coupon Entered", , , , , RGBMsgBox_WarnColour)
            RetrieveCoupon = False
            Exit Function
        End If
        If (oCouponMaster.IsDeleted = True) Then
            If EnableGetCoupons And Not GiveCoupon Then
                Call MsgBoxEx("Entered coupon has been marked as deleted.  Unable to accept coupon.", vbOKOnly, "Invalid Coupon Entered", , , , , RGBMsgBox_WarnColour)
                RetrieveCoupon = False
                Exit Function
            End If
        End If
        If (GiveCoupon = False) Then
            blnOtherFnd = False
            blnExclFnd = False
            'Step through items and check if other coupons and if any are Exclusive
            For lngRowNo = 2 To sprdLines.MaxRows Step 1
                sprdLines.Row = lngRowNo
                Call DebugMsgSprdLineValue(lngRowNo, strProcedureName)
                sprdLines.Col = COL_VOIDED
                If (Val(sprdLines.Text) <> 1) Then 'active
                    sprdLines.Col = COL_LINETYPE
                    If (Val(sprdLines.Text) = LT_COUPON) Then
                        blnOtherFnd = True
                        sprdLines.Col = COL_CPN_EXCLUSIVE
                        If (Val(sprdLines.Text) = 1) Then
                            blnExclFnd = True
                            Exit For
                        End If 'an Exclusive Coupon
                    End If 'Line is a coupon
                End If 'Active
            Next lngRowNo
            
            'handle if Exclusive Coupon exists
            If (oCouponMaster.exclusiveCoupon = True) Then
                If (blnOtherFnd = True) Then
                    lngCouponResp = MsgBoxEx("This is an Exclusive Coupon." & vbNewLine & "No other Coupon discounts will apply during this transaction." & _
                        vbNewLine & vbNewLine & "You already have other Coupon(s) in the transaction.", vbOKCancel, "Exclusive Coupon Added", "Override", "Reject", , , RGBMsgBox_WarnColour)
                    If (lngCouponResp = vbOK) Then blnExclFnd = True 'flag to delete all coupons
                Else
                    lngCouponResp = MsgBoxEx("This is an Exclusive Coupon.  No other Coupon discounts will apply during this transaction.", vbOKCancel, _
                        "Exclusive Coupon Added", "Accept", "Reject", , , RGBMSGBox_PromptColour)
                    If (lngCouponResp = vbOK) Then blnExclFnd = True 'flag to delete all coupons
                End If
            Else 'non-exclusive so check if Exclusive exists and accept/reject coupon
                If (blnExclFnd = True) Then
                    lngCouponResp = MsgBoxEx("You already have an Exclusive Coupon in the Transaction." & vbNewLine & "This coupon will remove the Exclusive Coupon.", _
                         vbOKCancel, "Exclusive Coupon Exists", "Override", "Reject", , , RGBMsgBox_WarnColour)
                    If (lngCouponResp = vbOK) Then blnExclFnd = True 'flag to delete all coupons
                End If
            End If
            
            If (lngCouponResp = vbCancel) Then 'Reject/Cancel entry
                RetrieveCoupon = False
                Exit Function
            End If
                    
            If (lngCouponResp = vbOK) And (blnExclFnd = True) Then 'A
                'Step through items and check if other coupons and if any are Exclusive
                Call DebugMsgSprdLinesValue(strProcedureName)
                For lngRowNo = 2 To sprdLines.MaxRows Step 1
                    sprdLines.Row = lngRowNo
                    sprdLines.Col = COL_VOIDED
                    If (Val(sprdLines.Text) <> 1) Then 'active
                        sprdLines.Col = COL_LINETYPE
                        If (Val(sprdLines.Text) = LT_COUPON) Then
                            sprdLines.Col = COL_VOIDED
                            sprdLines.Text = 1 'flag original item as voided
                            sprdLines.Col = -1
                            sprdLines.FontStrikethru = True
                            sprdLines.Col = COL_CPN_STATUS
                            sprdLines.Text = "5"
                            sprdLines.Col = 0
                            sprdLines.Text = "R"
                            sprdLines.FontStrikethru = False
                            sprdLines.Col = COL_CPN_ID
                            Call moEvents.DeleteCoupon(sprdLines.Text, lngRowNo)
                        End If 'Line is a coupon
                    End If 'Active
                Next lngRowNo
            End If
            
        Else
            'printing Coupon so set Market Ref and Serial No
            strCouponNo = Left$(strCouponNo, 7) & "0000" & "000000"
        End If
    
        strMarketRef = Mid$(strCouponNo, 8, 4)
        strSerialNo = Mid$(strCouponNo, 12, 6)
        
        If (GiveCoupon = False) Then
            If (oCouponMaster.SerialNumbered = True) Then
                'Check if Webservice must be called to verify Coupon has not been used
                If (mblnCouponWSActive = True) Then
                    Dim wsCoupon As New SoapClient30
                    On Error Resume Next
                    Call wsCoupon.MSSoapInit(mstrCouponWSURL)
                    If Err.Number <> 0 Then
                        On Error GoTo 0
                        If MsgBoxEx("Unable to Validate Coupon." & vbNewLine & vbNewLine & "Error detected:" & wsCoupon.FaultString, vbOKCancel + vbInformation, "Unable to Validate Coupon", "Manager Auth", , , , RGBMsgBox_WarnColour) = vbCancel Then
                            RetrieveCoupon = False
                            Exit Function
                        Else
                            Load frmVerifyPwd
                            If frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) = True Then
                                strMgrID = frmVerifyPwd.txtAuthID.Text
                                sprdLines.Col = COL_SUPERVISOR
                                sprdLines.Text = strMgrID
                                Unload frmVerifyPwd
                            Else
                                Unload frmVerifyPwd
                                RetrieveCoupon = False
                                Exit Function
                            End If
                        End If
                        Err.Clear
                    Else 'Webservice available so Validate coupon
                        If (wsCoupon.CouponRedeemed(strCouponID, strSerialNo) = True) Then
                            Call MsgBoxEx("Coupon has already been redeemed", vbOKOnly, "Coupon Validation Failed", , , , , RGBMsgBox_WarnColour)
                            RetrieveCoupon = False
                            Exit Function
                        End If
                    End If
                    Set wsCoupon = Nothing
                Else 'No web service so just get manager authorisation
                    Load frmVerifyPwd
                    If frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) = True Then
                        strMgrID = frmVerifyPwd.txtAuthID.Text
                        Unload frmVerifyPwd
                    Else
                        Unload frmVerifyPwd
                        RetrieveCoupon = False
                        Exit Function
                    End If
                End If
            End If 'Coupon is Serial Numbered, so validate at Head Office
            
            If (oCouponMaster.CollectInfo = True) And (mstrName = "") Then
                Call MsgBoxEx("Coupon requires entry of Customer Details", vbOKOnly, "Capture Customer Details", , , , , RGBMSGBox_PromptColour)
                Call SetEntryMode(enemNone)
                Call frmCustomerAddress.GetParkedName(mstrName, mstrPostCode, mstrAddress, mstrTelNo)
                If frmCustomerAddress.Cancel = False Then
                    If LenB(mstrAddress) <> 0 Then
                        vntAddress = Split(mstrAddress, vbNewLine)
                        mstrAddressLine1 = vntAddress(0)
                        mstrAddressLine2 = vntAddress(1)
                        mstrAddressLine3 = vntAddress(2)
                        Dim oReturnCust As cSale_Wickes.cReturnCust
                        Set oReturnCust = goDatabase.CreateBusinessObject(CLASSID_RETURNCUST)
                        With oReturnCust
                            .TransactionDate = moTranHeader.TranDate
                            .PCTillID = moTranHeader.TillID
                            .TransactionNumber = moTranHeader.TransactionNo
                            .LineNumber = 0
                            .CustomerName = mstrName
                            .AddressLine1 = mstrAddressLine1
                            .AddressLine2 = mstrAddressLine2
                            .AddressLine3 = mstrAddressLine3
                            .PostCode = mstrPostCode
                            .PhoneNo = mstrTelNo
                            .HouseName = ""
                            .LabelsRequired = False
                            .RefundReason = "00"
                            .MobileNo = mstrMobileNo
                            .WorkTelNumber = Mid(mstrWorkTelNo, 1, 1)
                            .Email = Mid(mstrEmail, 1, 1)
                            .SaveIfNew
                        End With
                    End If 'address entered so save
                End If 'address not cancelled
            End If 'address required
        End If 'capturing Coupon and address must be captured
            
        'If accepted then add to Transaction and set columns with values
        sprdLines.MaxRows = sprdLines.MaxRows + 1
        sprdLines.Row = sprdLines.MaxRows
        sprdLines.Col = 0
        sprdLines.Text = " " 'override auto column lettering
        sprdLines.Col = COL_PARTCODE
        sprdLines.Text = "Coupon"
        sprdLines.Col = COL_DESC
        sprdLines.Text = oCouponMaster.Description
         
        sprdLines.Col = COL_LINETYPE
        sprdLines.Text = LT_COUPON
        sprdLines.Col = COL_CPN_EXCLUSIVE
        sprdLines.Text = Abs(oCouponMaster.exclusiveCoupon)
        sprdLines.Col = COL_CPN_ID
        sprdLines.Text = oCouponMaster.CouponId
        sprdLines.Col = COL_CPN_REUSE
        sprdLines.Text = Abs(oCouponMaster.ReUsable)
        sprdLines.Col = COL_CPN_SERIALNO
        sprdLines.Text = strSerialNo
        sprdLines.Col = COL_SUPERVISOR
        sprdLines.Text = strMgrID
        sprdLines.Col = COL_CPN_MARKETREF
        sprdLines.Text = strMarketRef
        If (GiveCoupon = False) Then
            If EnableBuyCoupons Then
                Call moEvents.AddCouponWithDescription(oCouponMaster.CouponId, sprdLines.Row, IIf(CBool(oCouponMaster.ReUsable), "", oCouponMaster.Description))
            Else
                Call moEvents.AddCoupon(oCouponMaster.CouponId, sprdLines.Row)
            End If
        Else
            sprdLines.Col = COL_CPN_STATUS
            sprdLines.Value = "9"
        End If
    End If
    
    Exit Function

HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler(strProcedureName, Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Function



Private Sub ProcessQty(Optional blnPaidOut As Boolean = False)

Dim strQtyHigh As String
Dim strValueHigh As String
Dim strDeptNo As String
Dim strSKU As String

    On Error GoTo HandleException
    
    If (Val(txtSKU.Text) > 9999999) Then
        Call MsgBoxEx("Warning - Quantity entered system limits (above 9,999,999 items)" & _
            vbNewLine & "Unable to accept quantity", vbOKOnly + vbExclamation, _
            "Quantity Limit Exceeded", , , , , RGBMsgBox_WarnColour)
        mlngEntryMode = enemQty
        txtSKU.Text = 1
        Exit Sub
    End If
    
    If (Val(txtSKU.Text) < 0) And (mblnHasCashDrawer = False) Then
        Call MsgBoxEx("Warning - Unable to Refund Items onto a Quote/Order." & _
            vbNewLine & "Unable to accept quantity", vbOKOnly + vbExclamation, _
            "Unable to add Refund Lines", , , , , RGBMsgBox_WarnColour)
        mlngEntryMode = enemQty
        txtSKU.Text = 1
        Exit Sub
    End If
    
    'Do Not allow refunds on a Customer Order
    If (Val(txtSKU.Text) < 0) And (mblnOrderBtnPressed = True) And (Not Abs(RefundTotal) > 0) Then
        If MsgBoxEx("Warning - Unable to add Refund Items onto a Customer Order." & _
            vbNewLine & "Do you wish to continue with Order?", vbYesNo + vbDefaultButton1, _
            "Unable to add Refund Lines", , , , , RGBMsgBox_WarnColour) = vbYes Then
            mlngEntryMode = enemQty
            txtSKU.Text = 1
            Exit Sub
        End If
    End If
    
    If (Val(txtSKU.Text) < 0) And (mblnRecalledTran = True) Then
        Call MsgBoxEx("Warning - Unable to Refund Items onto a recalled transaction." & _
            vbNewLine & "Unable to accept quantity", vbOKOnly + vbExclamation, _
            "Unable to add Refund Lines", , , , , RGBMsgBox_WarnColour)
        mlngEntryMode = enemQty
        txtSKU.Text = 1
        Exit Sub
    End If
    'Perform simple check to ensure SKU not entered as quantity
    If (Val(txtSKU.Text) > mdblMaxQty) And (mdblMaxQty > 0) Then
            strQtyHigh = "Warning - Quantity entered seems high (above " & _
                            mdblMaxQty & " items)" & vbCrLf
    End If
    'display message to cancel or accept values
    If (LenB(strQtyHigh) <> 0) Or (LenB(strValueHigh) <> 0) Then  'Ref CR-0009 default to Cancel
        mlngEntryMode = enemNone
        If MsgBoxEx(strQtyHigh & strValueHigh, vbOKCancel + vbExclamation + vbDefaultButton2, _
                    "Quantity high", , , , , RGBMSGBox_PromptColour) = vbCancel Then
            mlngEntryMode = enemQty
            txtSKU.Text = 1
            Exit Sub
        End If
    End If
    
    mlngEntryMode = enemNone
    'Check if limit is imposed and has not been exceeded, else force to use max value
    If (Val(lblMaxValue.Caption) < Val(txtSKU.Text)) And (fraMaxValue.Visible = True) Then
        Call MsgBoxEx("Quantity entered for return exceeds original quantity sold", _
                      vbOKOnly, "High quantity entered", , , , , RGBMsgBox_WarnColour)
        mlngEntryMode = enemQty
        Exit Sub
    End If
    
    If Val(txtSKU.Text) > 9999999 Then
        Call MsgBoxEx("Quantity entered exceeds maximum allowable quantity" & _
                      vbCrLf & "Maximum value must be less than 9,999,999", _
                      vbOKOnly, "Invalid high quantity entered", , , , , _
                      RGBMsgBox_WarnColour)
        mlngEntryMode = enemQty
        Exit Sub
    End If
    
    If (Val(txtSKU.Text) = 0) Then
        mlngEntryMode = enemQty
        Exit Sub
    End If
    
    If Val(txtSKU.Text) < 0 Then
        
        mdblLineQuantity = Val(txtSKU.Text)
        mblnIsRefundLine = True
        If Val(moTranHeader.RefundCashier) = 0 Then
            Dim oRefCashier As cCashier
            Set oRefCashier = goDatabase.CreateBusinessObject(CLASSID_CASHIER)
            
            Load frmVerifyPwd
            If frmVerifyPwd.VerifyRefundPassword(ucKeyPad1.Visible) = False Then
                mdblLineQuantity = 1
                mstrRefundReason = vbNullString
                mlngCurrentMode = encmRecord
                Call SetEntryMode(enemSKU)
                Unload frmVerifyPwd
                Exit Sub
            End If
            Call oRefCashier.AddLoadFilter(CMP_EQUAL, FID_CASHIER_CashierNumber, _
                                           frmVerifyPwd.txtAuthID.Text)
            Set oRefCashier = oRefCashier.LoadMatches.Item(1)
            Unload frmVerifyPwd
            moTranHeader.RefundCashier = oRefCashier.CashierNumber
            Set oRefCashier = Nothing
        End If
        Call PerformRefund
    '                            Call LoadReasonCodes(enrmRefund)
    
    '                            mlngCurrentMode = encmRefund
        Exit Sub
    End If
        
    sprdLines.Col = COL_DEPTCODE
    strDeptNo = sprdLines.Text
    If mblnQtyBeforeSKU = False Then
        sprdLines.Row = sprdLines.MaxRows
        If mblnEditingLine Then sprdLines.Row = sprdLines.SelBlockRow
        sprdLines.Col = COL_QTY
        sprdLines.Text = txtSKU.Text
        Call UpdateTotals
        sprdLines.Row = sprdLines.MaxRows
        sprdLines.Col = COL_PARTCODE
        strSKU = sprdLines.Text
    Else
        mdblLineQuantity = Val(txtSKU.Text)
    End If
    
    If (moTranHeader.TransactionCode = TT_REFUND) Then
        If fraMaxValue.Visible Then
            lblMaxValue.Caption = Format$(mdblRefundPrice, "0.00")
            lblMaxPrompt.Caption = "Max price is"
            uctntPrice.Value = Val(lblMaxValue.Caption)
        Else
            sprdLines.Row = sprdLines.MaxRows
            sprdLines.Col = COL_SELLPRICE
            uctntPrice.Value = sprdLines.Value
        End If
        Call SetEntryMode(enemSKU)
        
    'need max price + display price
    Else
        If mblnItemPrePriced = True Then
            sprdLines.Col = COL_SELLPRICE
            uctntPrice.Value = Val(sprdLines.Value)
            Call SetEntryMode(enemPrice)
        Else
            Call SetEntryMode(enemSKU)
        End If
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("ProcessQty", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub InsertPricePromise(Row As Long, ItemDiscount As Currency, TotalDiscount As Currency, PPPos As Long)
    
    sprdLines.MaxRows = sprdLines.MaxRows + 1
    Call sprdLines.InsertRows(Row + 1, 1)
    sprdLines.Row = Row + 1
    sprdLines.Col = 0
    sprdLines.Text = " " 'override auto column lettering
    sprdLines.Col = COL_DESC
    sprdLines.Text = "Price Match/Promise"
    sprdLines.Col = COL_LINETYPE
    sprdLines.Text = LT_PRICEPROM
    sprdLines.Col = COL_SELLPRICE
    sprdLines.Text = ItemDiscount
    sprdLines.Col = COL_INCTOTAL
    sprdLines.Text = TotalDiscount
    Call SetGridTotals(sprdLines.MaxRows)
    sprdLines.Col = COL_QTY
    sprdLines.Text = vbNullString
    sprdLines.Col = COL_GRP_PROMPT_QTY  'WIX1391 Allow for Totaling Qty
    sprdLines.Text = vbNullString
    sprdLines.Col = COL_PP_POS
    sprdLines.Text = PPPos
    Call UpdateTotals

End Sub

Public Sub GetCreditCardNames()

Dim oFSO          As FileSystemObject
Dim tsFile        As TextStream
Dim strFilePath   As String
Dim strFileLine   As String
Dim openBracket   As String
Dim closeBracket  As String
Dim bracketsStart As Integer
Dim strCardName   As String
Dim strTenderType As String

Dim regex   As RegExp
Dim matches As MatchCollection

    If TillUsingCommideaPED Then
        openBracket = "["
        closeBracket = "]"
    Else
        openBracket = "("
        closeBracket = ")"
    End If

    On Error GoTo Bad_CC_File
    
    Set oFSO = New FileSystemObject
    
    strFilePath = goSession.GetParameter(PRM_TRANSNO)
    
    Call DebugMsg(MODULE_NAME, "GetCreditCardName", endlDebug, "Open Path-" & strFilePath)
    Set tsFile = oFSO.OpenTextFile(strFilePath & "CCNAMES.LST", ForReading, False, TristateUseDefault)
    If Not tsFile Is Nothing Then
        Set regex = New RegExp
        regex.IgnoreCase = True
        regex.Global = True
        regex.pattern = "\" & openBracket & "(\d+)\" & closeBracket & "\s*$"

        While Not tsFile.AtEndOfStream
            strFileLine = UCase(tsFile.ReadLine)

            Set matches = regex.Execute(strFileLine)
            If matches.Count > 0 Then
                strTenderType = matches(0).SubMatches(0)
                bracketsStart = matches(0).FirstIndex

                If Mid$(strFileLine, 1, 1) = "%" Then
                    strCardName = Trim(Mid$(strFileLine, 2, bracketsStart - 2))
                    If Not dictCreditCardNamePatterns.Exists(strCardName) Then
                        dictCreditCardNamePatterns.Add strCardName, strTenderType
                    End If
                Else
                    strCardName = Trim(Mid$(strFileLine, 1, bracketsStart - 1))
                    If Not dictCreditCardNames.Exists(strCardName) Then
                        dictCreditCardNames.Add strCardName, strTenderType
                    End If
                End If
            End If
        Wend
        Call tsFile.Close
    End If
    
    Exit Sub
    
Bad_CC_File:

    Call MsgBoxEx("Credit Card Names file initialisation failed" & vbCrLf & "Error :" & Err.Number & "-" & Err.Description, vbExclamation, _
                  "System configuration Error", , , , , RGBMsgBox_WarnColour)
    End
    
End Sub

Public Sub GetVolumeSKUs()

Dim oFSO        As FileSystemObject
Dim tsFile      As TextStream
Dim strFilePath As String
Dim strSKULine  As String

    On Error GoTo Bad_Vol_SKUs_File
    
    Set oFSO = New FileSystemObject
    
    strFilePath = goSession.GetParameter(PRM_TRANSNO)
    Set mcolVolumeSKUs = New Collection
    
    Call DebugMsg(MODULE_NAME, "GetVolumeSKUs", endlDebug, "Open Path-" & strFilePath & "PROMO_COL.TXT")
    Set tsFile = oFSO.OpenTextFile(strFilePath & "PROMO_COL.TXT", ForReading, False, TristateUseDefault)
    If ((tsFile Is Nothing) = False) Then
        While tsFile.AtEndOfStream = False
            strSKULine = tsFile.ReadLine
            If (InStr(strSKULine, ",") > 0) Then Call mcolVolumeSKUs.Add(Val(Mid$(strSKULine, InStr(strSKULine, ",") + 1)), Left$(strSKULine, InStr(strSKULine, ",") - 1))
        Wend
        Call tsFile.Close
    End If
    
    Exit Sub
    
Bad_Vol_SKUs_File:

    Call DebugMsg(MODULE_NAME, "GetVolumeSKUs", endlDebug, "Error accessing file")
    Err.Clear
    
End Sub

Private Function GetCardTTID(ByVal strCardName As String, _
                             Optional ByVal StoreUnknownSchemeNames As Boolean = False) As Long

Dim pattern As Variant
Dim regex   As RegExp
Dim matches As MatchCollection

On Error GoTo Errored
    
    GetCardTTID = 3
    strCardName = UCase$(Trim(strCardName)) 'ensure letter case does not cause problems

    If dictCreditCardNames.Exists(strCardName) Then
        GetCardTTID = Val(dictCreditCardNames.Item(strCardName))
        Exit Function
    End If

    Set regex = New RegExp
    regex.IgnoreCase = True
    regex.Global = True
    
    For Each pattern in dictCreditCardNamePatterns
        regex.Pattern = CStr(pattern)
        Set matches = regex.Execute(strCardName)
        If matches.Count> 0 Then
            GetCardTTID = Val(dictCreditCardNamePatterns.Item(pattern))
            Exit Function
        End If
    Next

    ' Collect unknown name, to aid with banking should txns be recorded incorrectly
    If TillUsingCommideaPED And StoreUnknownSchemeNames Then
        ' Use Payments.Count +1 as not added the payment object for this payemt yet, but just about to once sent back this TTID
        Call SaveUnknownCreditCardName(strCardName, moTranHeader.TransactionDateTime, moTranHeader.TransactionNo, moTranHeader.Payments.Count + 1)
    End If

    Exit Function
    
Errored:
    If TillUsingCommideaPED And StoreUnknownSchemeNames Then
        Call SaveUnknownCreditCardName(strCardName, moTranHeader.TransactionDateTime, moTranHeader.TransactionNo, moTranHeader.Payments.Count)
    End If
    Call DebugMsg(MODULE_NAME, "GetCardTTID", endlDebug, "Error:-" & Err.Number & Err.Description & "/" & strCardName)
    Call Err.Clear
End Function

Private Sub AddVoucherSKUs(ByVal curVoucherTotal As Currency, _
                           Optional ByVal blnTendering As Boolean = False, _
                           Optional ByVal strDLGIFTType As String = vbNullString, _
                           Optional ByVal lngLineNo As Long = vbNull)

        On Error GoTo HandleException
        
        If (curVoucherTotal > 0) And (curVoucherTotal >= mcurMinGiftCardValue) Then
            If lngLineNo = vbNull Then
                sprdLines.MaxRows = sprdLines.MaxRows + 1
                sprdLines.Row = sprdLines.MaxRows
            Else
                sprdLines.Row = lngLineNo
            End If
            sprdLines.Col = 0
            sprdLines.Text = " " 'override auto column lettering
            sprdLines.Col = COL_PARTCODE
            sprdLines.Text = mstrGiftCardSKU
            Call GetItem(mstrGiftCardSKU, False)
            sprdLines.Col = COL_QTY
            sprdLines.Text = Sgn(mdblLineQuantity)
            sprdLines.Col = COL_LOOKEDUP
            sprdLines.Text = curVoucherTotal
            sprdLines.Col = COL_SELLPRICE
            sprdLines.Text = curVoucherTotal
            sprdLines.Col = COL_LINETYPE
            sprdLines.Text = LT_ITEM
            sprdLines.Col = COL_VOUCHER
            sprdLines.Value = "V"
            If LenB(strDLGIFTType) <> 0 Then
                sprdLines.Col = COL_DLGIFTTYPE
                sprdLines.Text = strDLGIFTType
            End If
            If (blnTendering = True) Then mdblPaidTotal = mdblPaidTotal + curVoucherTotal
        End If
        
    If Not blnTendering Then
        Call SetGridTotals(sprdLines.MaxRows)
        Call UpdateTotals
    Else
        sprdTotal.Row = 3
        sprdTotal.Col = COL_TOTVAL
        sprdTotal.Text = Val(Format(sprdTotal.Text, "#######.##")) + curVoucherTotal
    
        sprdLines.Row = ROW_TOTALS
        sprdLines.Col = COL_INCTOTAL
        sprdLines.Text = Val(Format(sprdLines.Text, "#######.##")) + curVoucherTotal
    End If

    mdblLineQuantity = 1 'reset quantity counter
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("AddVoucherSKUs", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub RollbackPaymentToTenderScreen()
    sprdLines.Col = COL_LINETYPE
    sprdLines.Row = sprdLines.MaxRows
    sprdLines.Text = 0
    txtSKU.Text = "0"
    If Not m_ProcessingPaymentImplementation Is Nothing Then
        m_ProcessingPaymentImplementation.SetForOutOfProcessPaymentType
    End If
End Sub

Private Sub ProcessPaymentType(ByVal lngTenderType As Long, ByRef dblTenderAmount As Currency)
    
    Const MethodName = "ProcessPaymentType"

    Dim lngLimit        As Long
    Dim strCardNumber   As String
    Dim strStartDate    As String
    Dim strEndDate      As String
    Dim strIssueNo      As String
    Dim strAccountNo    As String
    Dim strChequeNo     As String
    Dim strSortCode     As String
    Dim strAuthNum      As String
    Dim strCashierName  As String
    Dim strCarRegNum    As String
    Dim strTrack2       As String
    Dim oPayment        As cPOSPayment
    Dim strCoupon       As String
    Dim strCardName     As String
    Dim strMerchantID   As String
    Dim strAppID        As String
    Dim strEFTTranID    As String
    Dim strTerminalID   As String
    Dim strPANSeq       As String
    Dim strCVVNo        As String
    Dim strEntryMethod  As String
    Dim lngSaveMode     As String
    Dim blnKeyPad       As Boolean
    Dim strCouponPostCode As String
    Dim strMessageNumber As String
    Dim dblTransactionId As Double
    
    On Error GoTo HandleException
    
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Processing Payment Type #" & lngTenderType & " for Amount=" & dblTenderAmount)
    strCashierName = lblFullName.Caption

    ' Referral 876
    ' Switch on flag to indicate in middle of processing a payment type,
    ' so as to be able to prevent a second call to same method before this one finishes
    ' e.g. when a DoEvents is called
    Dim myFactory As New TillFormFactory

    Set m_ProcessingPaymentImplementation = myFactory.FactoryGetProcessingPayment
    m_ProcessingPaymentImplementation.SetForInProcessPaymentType
    ' End of Referral 876

    Select Case (lngTenderType)
    
        Case TEN_CASH, TEN_CHANGE:
                Set oPayment = moTranHeader.AddPayment(lngTenderType, CDbl(dblTenderAmount))
                mblnNonTokenUsedInTender = True
                mblnFirstPmtDone = True
                
        Case TEN_CHEQUE:
                
                Load frmTender
                Call frmTender.CaptureCheque(OPOSPrinter1, dblTenderAmount, lngLimit, _
                                             strCardNumber, strStartDate, strEndDate, strIssueNo, _
                                             strAccountNo, strChequeNo, strSortCode, _
                                             strCashierName, strCarRegNum, strMerchantID, strEntryMethod, _
                                             strAuthNum, strEFTTranID, ucKeyPad1.Visible)
                DoEvents
                Unload frmTender

                If LenB(strChequeNo) <> 0 And strAuthNum <> "" Then
                    Set oPayment = moTranHeader.AddPayment(lngTenderType, CDbl(dblTenderAmount))
                    oPayment.CreditCardNumber = MaskCreditCardNo(strCardNumber)
                    oPayment.CreditCardExpiryDate = strEndDate
                    oPayment.IssuerNumber = strIssueNo
                    oPayment.ChequeAccountNumber = Right$(String$(10, "0") & strAccountNo, 10)
                    oPayment.AuthorisationCode = strAuthNum
                    oPayment.ChequeSortCode = strSortCode
                    oPayment.ChequeNumber = strChequeNo
                    oPayment.EFTPOSMerchantNo = strMerchantID
                    oPayment.AuthorisationMethod = strEntryMethod
                    If strEntryMethod <> "Keyed" Then
                        oPayment.CCNumberKeyed = False
                        oPayment.AuthorisationType = "O"
                    Else
                        oPayment.CCNumberKeyed = True
                        oPayment.AuthorisationType = "R"
                    End If
                    oPayment.IssuerNumber = strIssueNo
                    oPayment.EFTConfirmID = strEFTTranID
                    oPayment.TerminalID = strTerminalID
                    oPayment.PANSeq = strPANSeq
                    oPayment.CardAppID = strAppID
                    mblnNonTokenUsedInTender = True
                    mblnFirstPmtDone = True
                    Call mcolEFTPayments.Add(oPayment)
                    'oPayment.CardLimit = lngLimit - not available
                Else
                    sprdLines.Col = COL_LINETYPE
                    sprdLines.Row = sprdLines.MaxRows
                    If Val(sprdLines.Value) = LT_PMT Then 'MO'C 1376 - Fix for Referral 2009-043 - Dropped Line Bug
                                                
                        sprdLines.Text = 0
                        txtSKU.Text = "0"
                        dblTenderAmount = 0
                    End If
                   
                End If
                    
                mblnTransactionDiscountApplied = False
                mstrAccountNum = vbNullString
                mstrAccountName = vbNullString
        
        Case TEN_CREDCARD, TEN_DEBITCARD, TEN_GIFTCARD:
                
                Dim strTokenID As String ' Commidea 'Account on File' registration identifier
                Dim strCardNumberHash As String ' non reversible PAN (+ merchant) identifier
                
                If Not CheckCommideaIsReady() Then
                    GoTo ResetTenderAmountProcessPaymentFlagAndExit
                End If
                 
                If lngTenderType = TEN_GIFTCARD Then
                    If Not CheckStoreSettings Then
                        mblnReturningFromCancelledEFTTender = True
                        Call Form_KeyPress(vbKeyEscape)
                        GoTo ResetTenderAmountProcessPaymentFlagAndExit
                    End If
                End If
                         
                Dim oTender As cCommidea
                Set oTender = CreateAndInitializeCommideaFacade(MethodName)
                
                Dim oAuthorizationRequest As New AuthorizationRequest
                Set oAuthorizationRequest = CreateAuthorizationRequest(lngTenderType, dblTenderAmount)
                
                If ShouldCancelForRepeatPayment(oAuthorizationRequest.WSSUniqueTransTenderRef) Then
                    DebugMsg MODULE_NAME, MethodName, endlDebug, "Repeat payment spotted aborting additional payment."
                    Exit Sub
                End If
                Call SetPreviousWSSUniqueTTRef(oAuthorizationRequest.WSSUniqueTransTenderRef)
                
                Dim oAuthorizationResult As AuthorizationResult
                Set oAuthorizationResult = oTender.Authorise(oAuthorizationRequest)

                Call ResetRepeatPayment
                
                Call StoreCommideaAuthorizationReceipts(oAuthorizationResult)
                    
                If oAuthorizationResult.Completed Then
                    If Not oAuthorizationResult.Details Is Nothing Then
                        With oAuthorizationResult.Details
                        
                            If oAuthorizationRequest.RequestType = CommideaRequestType.BarclaycardGift Then
                                dblTenderAmount = .TransactionAmount * -mlngTranSign
                                txtSKU.Text = .TransactionAmount
                                
                                Dim oBGiftResponse As BGiftResponseREcord
                                Set oBGiftResponse = oAuthorizationResult.Details
                                strMessageNumber = oBGiftResponse.RecordMessageNumber
                                dblTransactionId = oBGiftResponse.RecordTransactionId
                            End If
                        
                            strCardNumber = .PAN ' This is all ready masked
                            strStartDate = .StartDate
                            'added to conform with existing Cnp solution - code switches it back again later
                            strStartDate = Right$(strStartDate, 2) & Left$(strStartDate, 2)
                            strEndDate = .ExpiryDate
                            'added to conform with existing Cnp solution - code switches it back again later
                            strEndDate = Right$(strEndDate, 2) & Left$(strEndDate, 2)
                            strIssueNo = .IssueNumber
                            strAuthNum = .AuthorisationCode
                            ' Dont get an auth code for card holder not present refund,
                            ' so set this to the Customer Verification message so
                            ' as to not trip up at auth code length check further on
                            ' that will prevent the payment from being processed
                            If (oAuthorizationRequest.TRecordModifier = etrmCNPMailOrder _
                                Or oAuthorizationRequest.TRecordModifier = etrmCNPAccountOnFile _
                                Or oAuthorizationRequest.TRecordModifier = etrmCNPTelephoneOrder) _
                                And .IsAuthorized() _
                                And .CustomerVerification = "CONFIRMED" _
                                And strAuthNum = "" Then
                                
                                strAuthNum = .CustomerVerification
                            End If
                            strEntryMethod = .CaptureMethod
                            'strTrack2 = .Track2Details
                            ' Referral 857 - quick fix, restrict length of scheme name to
                            ' match current max length of database field that stores it
                            ' namely DLPAID.CTYP
                            strCardName = Trim$(Left$(.SchemeName, 30))
                            ' End of Referral 857
                            strMerchantID = .MerchantNumber
                            strEFTTranID = .eftSequenceNumber
                            ' Previously, Terminal ID has been used to store Cheque Account No - strange but true.
                            ' so not switch this on yet as will get stored in CKAC on DLPAID
                            ' but might want to in future, so parameterised to make easy switch on
                            If mblnCommideaStoreTerminalID Then
                                strTerminalID = .TerminalID
                            End If
                            ' Don't seem to have these for Commidea
                            'strPANSeq = .seqNum
                            'strAppID = .applicationid (SchemeID?)
                            'strCVVNo = .CV2Data
                            ' New Commidea fields
                            strTokenID = .TokenID
                            strCardNumberHash = .CardNumberHash
                            sbStatus.Panels(PANEL_PEDOFFLINE).Visible = PedOffline(strEFTTranID)
                        End With
                    Else 'Not result Is Nothing
                        ' error - failure to return transaction result
                        ' all ready had messages from wsscommidea.dll
                        ' Referral 482
                        ' Flag return, so processes like QOD can return to tender selection process
                        mblnReturningFromCancelledEFTTender = True
                        ' Have to call this twice to get back to tender type selection screen.  So once here and
                        ' once when return to call process
                        Call Form_KeyPress(vbKeyEscape)
                        
                        GoTo ResetWSSRefTenderAmountProcessPaymentFlagAndExit
                     End If
                    
                Else 'oAuthorizationResult.Completed
                
                    ' error - transaction failed
                    Call ShowFailedCommideaTransactionMsgBox(oAuthorizationResult)

                    ' Referral 482
                    ' Flag return, so processes like QOD can return to tender selection process
                    mblnReturningFromCancelledEFTTender = True
                    ' Have to call this twice to get back to tender type selection screen.  So once here and
                    ' once when return to call process
                    Call Form_KeyPress(vbKeyEscape)
                    GoTo ResetWSSRefTenderAmountProcessPaymentFlagAndExit
                End If 'oAuthorizationResult.Completed
                            
                ' Try to return focus to till
                Call oTender.ResetFocus(Me.Name)
                    
                commideaTransactionInProgress = True
                
                If Not RepeatPaymentImplementation.ShouldCancelRepeatPayment Then
                    DoEvents
                End If
                    
                Set moCommidea = Nothing
                    
                If Not RepeatPaymentImplementation.ShouldCancelRepeatPayment Then
                    DoEvents
                End If

                commideaTransactionInProgress = False

                Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Amt=" & CDbl(dblTenderAmount) & ",AuthNum='" & strAuthNum & "',Type=" & moTranHeader.TransactionCode)
                
                If LenB(strAuthNum) <> 0 Then
                    If (lngTenderType <> TEN_GIFTCARD) Then
                        lngTenderType = GetCardTTID(strCardName, TillUsingCommideaPED)
                    End If
                    Set oPayment = moTranHeader.AddPayment(lngTenderType, CDbl(dblTenderAmount))
                    oPayment.CreditCardNumber = MaskCreditCardNo(strCardNumber)
                    oPayment.CreditCardExpiryDate = Right$(strEndDate, 2) & Left$(strEndDate, 2)
                    oPayment.CreditCardStartDate = Right$(strStartDate, 2) & Left$(strStartDate, 2)
                    oPayment.IssuerNumber = strIssueNo
                    oPayment.AuthorisationCode = strAuthNum
                    oPayment.EFTPOSMerchantNo = strMerchantID
                    oPayment.EFTConfirmID = strEFTTranID
                    oPayment.TerminalID = strTerminalID
                    oPayment.CardAppID = strAppID
                    oPayment.PANSeq = strPANSeq
                    oPayment.AuthorisationMethod = strEntryMethod
                    oPayment.MessageNumber = strMessageNumber
                    oPayment.TransactionId = dblTransactionId
                    
                    If Left$(strEntryMethod, 5) <> "Keyed" Then
                        oPayment.CCNumberKeyed = False
                        oPayment.AuthorisationType = "O"
                    Else
                        oPayment.CCNumberKeyed = True
                        oPayment.AuthorisationType = "R"
                    End If
                    oPayment.IssuerNumber = strIssueNo
                    oPayment.CreditCardName = strCardName
                    
                    'oPayment.CardLimit = lngLimit - not available
                    mblnNonTokenUsedInTender = True
                    mblnFirstPmtDone = Not mblnReturningFromCancelledEFTTender ' True
                    Call mcolEFTPayments.Add(oPayment)
                    
                    ' Add the new Commidea fields too
                    Dim oPaymentComm As cPOSPaymentComm    ' extension of POSPayment for Commidea fields - TokenID and Card Number Hash
                        Set oPaymentComm = moTranHeader.AddPaymentComm(oPayment.SequenceNumber)
                        oPaymentComm.TokenID = strTokenID
                        oPaymentComm.CardNumberHash = strCardNumberHash
                        Call mcolEFTPayments.Add(oPayment)
                    
                Else 'LenB(strAuthNum) <> 0
                    sprdLines.Col = COL_LINETYPE
                    sprdLines.Row = sprdLines.MaxRows
                    If Val(sprdLines.Value) = LT_PMT Then 'MO'C 1376 - Fix for Referral 2009-043 - Dropped Line Bug
                        sprdLines.Text = 0
                        txtSKU.Text = "0"
                        dblTenderAmount = 0
                    End If
                    ' Referral 786
                    ' Issue identified during offline PED testing in live store 17/3/2011
                    ' Not quite a 'cancelled', more an unauthorised, e.g. if a card scheme not accepted.
                    ' Still need to return to tender selection as not had a valid payment yet, so
                    ' flag up that point
                    mblnReturningFromCancelledEFTTender = True
                End If ' LenB(strAuthNum) <> 0
                
                mblnTransactionDiscountApplied = False
                mstrAccountNum = vbNullString
                mstrAccountName = vbNullString
                
            Case TEN_PROJLOAN:
                Load frmProjectNo
                If (ucKeyPad1.Visible = True) Then frmProjectNo.ShowNumPad
                blnKeyPad = ucKeyPad1.Visible
                ucKeyPad1.Visible = False
                strCardNumber = frmProjectNo.GetLoanNumber(blnKeyPad)
                Unload frmProjectNo
                fraEntry.Visible = True
                ucKeyPad1.Visible = blnKeyPad
                If (strCardNumber = "000000000") Or Not SerialOK(strCardNumber) Then
                    Call MsgBoxEx("Invalid Project Loan Agreement Number", vbOKOnly + vbExclamation, _
                                  "Project Loan", , , , , RGBMsgBox_WarnColour)
                    
                    sprdLines.Col = COL_LINETYPE
                    sprdLines.Row = sprdLines.MaxRows
                    sprdLines.Text = 0
                    txtSKU.Text = "0"
                    
                    GoTo ResetTenderAmountProcessPaymentFlagAndExit
                End If
                Set oPayment = moTranHeader.AddPayment(lngTenderType, CDbl(dblTenderAmount))
                oPayment.CreditCardNumber = Right$(String$(19, "0") & strCardNumber, 19)

                mblnNonTokenUsedInTender = True
                mblnFirstPmtDone = True
                
            Case TEN_COUPON: ' Coupon
                blnKeyPad = ucKeyPad1.Visible
                ucKeyPad1.Visible = False
                strCouponPostCode = mstrPostCode
                Call frmCoupon.GetCouponInfo(blnKeyPad, strCoupon, strCouponPostCode)
                ucKeyPad1.Visible = blnKeyPad
                If frmCoupon.Cancel Then
                    sprdLines.Col = COL_LINETYPE
                    sprdLines.Row = sprdLines.MaxRows
                    If Val(sprdLines.Value) = LT_PMT Then 'MO'C 1376 - Fix for Referral 2009-043 - Dropped Line Bug
                        sprdLines.Text = 0
                        txtSKU.Text = "0"
                        dblTenderAmount = 0
                    End If
                    GoTo ResetProcessPaymentFlagAndExit
                End If
                Set oPayment = moTranHeader.AddPayment(lngTenderType, CDbl(dblTenderAmount))
                oPayment.CouponNumber = strCoupon
                oPayment.CouponPostCode = strCouponPostCode 'mstrPostCode
                mblnNonTokenUsedInTender = True
                mblnFirstPmtDone = True
            
            Case TEN_ACCOUNT: 'Account Card
                
                If Val(mstrAccountBalance) + dblTenderAmount > Val(mstrCreditLimit) Then
                    Call MsgBoxEx("Warning transaction amount exceeds the account credit limit" & _
                        vbCrLf & "Please enter a supervisor password", vbOKOnly, _
                        "Transaction Exceeds Account Credit Limit", , , , , RGBMsgBox_WarnColour)
                    If frmVerifyPwd.VerifyEFTPassword(ucKeyPad1.Visible) = False Then
                        sprdLines.Col = COL_LINETYPE
                        sprdLines.Row = sprdLines.MaxRows
                        sprdLines.Text = 0
                        txtSKU.Text = "0"
                        dblTenderAmount = 0
                        Unload frmTender
                        mblnTransactionDiscountApplied = False
                        mstrAccountNum = vbNullString
                        mstrAccountName = vbNullString
                        
                        GoTo ResetProcessPaymentFlagAndExit
                    End If
                End If
                Unload frmVerifyPwd
                
                fraEntry.Visible = False
                Set oPayment = moTranHeader.AddPayment(lngTenderType, CDbl(dblTenderAmount))
                mblnNonTokenUsedInTender = True
                mblnFirstPmtDone = True
                'Write Cusmas record away
            
            Case (TEN_GIFTTOKEN):
                If moTOSType.Code = TT_REFUND Then
                    lngSaveMode = mlngCurrentMode
                    mlngCurrentMode = encmGiftVoucher
                    sprdLines.MaxRows = sprdLines.MaxRows - 1
                    Call AddVoucherSKUs(dblTenderAmount, , "TR")
                    mlngCurrentMode = lngSaveMode
                Else
                    Set oPayment = moTranHeader.AddPayment(lngTenderType, CDbl(dblTenderAmount))
                    mblnFirstPmtDone = True
                End If

    End Select
    
    'For valid payment method check if Foreign currency and set Currency Codes
    If ((oPayment Is Nothing) = False) Then
        sprdLines.Row = sprdLines.MaxRows
        sprdLines.Col = COL_CFOREIGN_AMT
        oPayment.ForeignTender = Val(sprdLines.Text)
        sprdLines.Col = COL_CFOREIGN_CODE
        oPayment.CurrencyCode = sprdLines.Text
        sprdLines.Col = COL_CFOREIGN_EXCH
        oPayment.ExchangeRate = Val(sprdLines.Text)
        sprdLines.Col = COL_CFOREIGN_POWER
        oPayment.ExchangeFactor = Val(sprdLines.Text)
    End If
    
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Processed " & (moTenderOpts Is Nothing))
    If mblnUseCashDrawer And Not mblnTraining = True Then
        If mblnTraining = False Then
            If moTenderOpts.OpenCashDrawer = True Then
                mblnNeedDrawerOpen = True
            End If
        End If
    End If
    
    If Not oPayment Is Nothing Then
        If lngTenderType <> TEN_CHANGE Then
            Call ZReadUpdate("TE", Format$(lngTenderType, "00 "), CCur(dblTenderAmount))
        End If
        Call UpdateFlashTotals(True, lngTenderType, CCur(dblTenderAmount))
    End If
    
    GoTo ResetProcessPaymentFlagAndExit
    
ResetWSSRefTenderAmountProcessPaymentFlagAndExit:
    ' If legimately not been able to pay then unset previous payment ref value so
    ' not treated as a repeat payment for next payment
    Call SetPreviousWSSUniqueTTRef("")
    
ResetTenderAmountProcessPaymentFlagAndExit:
    dblTenderAmount = 0
   
ResetProcessPaymentFlagAndExit:
    ' Referral 876
    ' Switch off flag that indicates in middle of processing a payment type
    If Not m_ProcessingPaymentImplementation Is Nothing Then
        m_ProcessingPaymentImplementation.SetForOutOfProcessPaymentType
    End If
     ' End of Referral 876
     
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("ProcessPaymentType", Err.Description)
    commideaTransactionInProgress = False
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Function MaskCreditCardNo(ByVal strCardNo As String) As String

Dim lngCharPos  As Long
Dim strChar     As String
Dim lngFirstPos As Long
Dim lngLastPos  As Long

    If (mstrCardNoMask <> "") Then
        If (InStr(mstrCardNoMask, ":") = 0) Then
            'Apply mask directly
            For lngCharPos = 1 To Len(mstrCardNoMask) Step 1
                strChar = Mid$(mstrCardNoMask, lngCharPos, 1)
                If (strChar <> "0") Then strCardNo = Left$(strCardNo, lngCharPos - 1) & strChar & Mid$(strCardNo, lngCharPos + 1)
            Next lngCharPos
        Else
            'apply mask only to first and last characters only
            For lngCharPos = 1 To Len(mstrCardNoMask) Step 1
                strChar = Mid$(mstrCardNoMask, lngCharPos, 1)
                If (strChar = "0") And (lngFirstPos > 0) And (lngLastPos = 0) Then lngLastPos = lngCharPos
                If (strChar <> "0") And (lngFirstPos = 0) And (lngLastPos = 0) Then lngFirstPos = lngCharPos - 1
            Next lngCharPos
            lngLastPos = Len(mstrCardNoMask) - lngLastPos + 1
            If (Len(mstrCardNoMask) - 3 < Len(strCardNo)) Then
                strCardNo = Left$(strCardNo, lngFirstPos) & String(Len(strCardNo) - (Len(mstrCardNoMask) - 3), Mid$(mstrCardNoMask, lngFirstPos + 1, 1)) & Right$(strCardNo, lngLastPos)
            End If
        End If
    End If
    MaskCreditCardNo = Right$(String$(19, "0") & strCardNo, 19)

End Function

Private Function MaskGiftCardNo(ByVal cardNo As String) As String

    If (Len(cardNo) = 16) Then
        MaskGiftCardNo = Right$(String$(16, "x") & Mid$(cardNo, 12, 16), 16)
    ElseIf (Len(cardNo) = 19) Then
        MaskGiftCardNo = Right$(String$(19, "x") & Mid$(cardNo, 16, 19), 19)
    End If

End Function

Private Sub UpdateTotals()

Dim lngLastRow As Long
Dim lngRowNo   As Long
Dim lngNoItems As Long

    For lngRowNo = 2 To sprdLines.MaxRows Step 1
        sprdLines.Row = lngRowNo
        sprdLines.Col = COL_VOIDED
        If (Val(sprdLines.Text) = 0) Then 'not voided
            sprdLines.Col = COL_SALETYPE
            If (sprdLines.Text <> "D") And (sprdLines.Text <> "W") And (sprdLines.Text <> "V") And (sprdLines.Text <> "C") Then
                sprdLines.Col = COL_QTY
                If (Val(sprdLines.Value) > 0) Then lngNoItems = lngNoItems + Val(sprdLines.Value)
            End If
        End If
    Next lngRowNo
    lngLastRow = sprdLines.Row
    sprdLines.Row = ROW_TOTALS
    sprdTotal.Row = ROW_TOTTOTALS
    sprdLines.Col = COL_QTY
    sprdTotal.Col = COL_TOTQTY
'    sprdTotal.Text = sprdLines.Text
    sprdTotal.Text = lngNoItems
    sprdLines.Col = COL_INCTOTAL
    sprdTotal.Col = COL_TOTVAL
    sprdTotal.Text = sprdLines.Text

    If Not moTranHeader Is Nothing Then
        If (moTranHeader.TransactionCode = TT_SALE) Then
            sprdTotal.Col = COL_TOTTYPE
            If (Left(sprdTotal.Text, 5) <> "QUOTE") Then
                sprdTotal.Text = IIf(Val(sprdLines.Value) < 0, "Refund", "Sale")
            End If
        End If
    End If
    
    sprdLines.Row = lngLastRow
    
End Sub

Private Function LoadTypesOfSales() As Boolean

Dim lngOptNo   As Long
Dim lngTypeNo  As Long
Dim lngRowNo   As Long
Dim lngLeftPos As Long
Dim oTOSOpt    As cTypesOfSaleOptions
Dim oTOSCtl    As cTypesOfSaleControl
Dim blnHaveCC  As Boolean
Dim blnHaveCO  As Boolean
Dim blnUseTOS  As Boolean
Dim oFSO    As New FileSystemObject

    Set oTOSOpt = goDatabase.CreateBusinessObject(CLASSID_TYPESOFSALEOPTIONS)
    Call oTOSOpt.AddLoadFilter(CMP_GREATERTHAN, FID_TYPESOFSALEOPTIONS_DisplaySeq, vbNullString)
    ' load entire table
    Set mcolTOSOpt = oTOSOpt.LoadMatches
    
    Set oTOSCtl = goDatabase.CreateBusinessObject(CLASSID_TYPESOFSALECONTROL)
    Call oTOSCtl.AddLoadField(FID_TYPESOFSALECONTROL_Code)
    Call oTOSCtl.AddLoadField(FID_TYPESOFSALECONTROL_Description)
    Call oTOSCtl.AddLoadField(FID_TYPESOFSALECONTROL_ValidForUse)
    
    lngTypeNo = 1
    'Create command button for each Type of Sales
    For lngOptNo = 1 To mcolTOSOpt.Count Step 1
        Set oTOSOpt = mcolTOSOpt(lngOptNo)
        Call oTOSCtl.ClearLoadFilter
        Call oTOSCtl.AddLoadFilter(CMP_EQUAL, FID_TYPESOFSALECONTROL_Code, oTOSOpt.Code)
        If Not oTOSCtl.LoadMatches.Count = 0 Then
            If oTOSCtl.ValidForUse Then
                If (oTOSOpt.Code <> TT_SIGNON) And (oTOSOpt.Code <> TT_SIGNOFF) Then
                    blnUseTOS = False
                    Select Case (oTOSOpt.Code)
                        Case (TT_SALE):          ' "SA"
                            If (mblnHasCashDrawer = False) And (mblnHasEFT = False) Then
                                oTOSOpt.Description = "Quote"
                            End If
                            If (mblnHasCashDrawer = False) And (mblnHasEFT = True) Then
                                oTOSOpt.Description = "Quote/Order"
                            End If
                            blnUseTOS = True
                        Case (TT_OPENDRAWER), (TT_EAN_CHECK):   ' "OD"
                            If (mblnHasCashDrawer = True) And (mblnUseCashDrawer = True) Then blnUseTOS = True
                        Case (TT_MISCIN), (TT_MISCOUT), _
                            (TT_CORRIN), (TT_CORROUT), _
                            (TT_RECALL):
                            If (mblnHasCashDrawer = True) And (mblnHasEFT = True) Then blnUseTOS = True
                        Case (TT_LOGO), (TT_XREAD), (TT_ZREAD):
                            If (mstrPrinterType = "P" Or mstrPrinterType = "R") Then blnUseTOS = True
                        Case Else
                            blnUseTOS = True
                    End Select
                    If (blnUseTOS = True) Then
                        lngTypeNo = lngTypeNo + 1
                        Load cmdTranType(lngTypeNo)
                        lngLeftPos = ((lngTypeNo - 2) Mod TRAN_BTN_PER_ROW) + 1
                        cmdTranType(lngTypeNo).Left = ((cmdTranType(lngTypeNo).Width + 120) * (lngLeftPos - 1)) + 180
                        If LenB(oTOSOpt.Description) <> 0 Then
                            cmdTranType(lngTypeNo).Caption = oTOSOpt.Description
                        Else
                            cmdTranType(lngTypeNo).Caption = oTOSOpt.Code
                        End If
                        cmdTranType(lngTypeNo).Tag = lngOptNo
                        cmdTranType(lngTypeNo).BackColor = fraTranType.BackColor
                    End If
                Else
                    If oTOSOpt.Code = TT_SIGNOFF Then
                        blnHaveCC = True
                    Else
                        blnHaveCO = True
                    End If
                End If
            End If
        End If
    Next lngOptNo
    
    lngLeftPos = TRAN_BTN_PER_ROW + 1
    cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY).Left = ((cmdTranType(lngTypeNo).Width + 120) * (lngLeftPos - 1)) + 180
    cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY).Caption = "SKU Enquiry"
    cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY).Tag = 0
    cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY).BackColor = fraTranType.BackColor
    cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY).Visible = True
    
    Load cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY_PIM)
    lngLeftPos = TRAN_BTN_PER_ROW + 2
    cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY_PIM).Left = ((cmdTranType(lngTypeNo).Width + 120) * (lngLeftPos - 1)) + 180
    cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY_PIM).Caption = "PIM SKU Enquiry"
    cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY_PIM).Tag = 0
    cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY_PIM).BackColor = fraTranType.BackColor
    If (oFSO.FolderExists(goSession.GetParameter(PRM_PIM_ONLY_IMAGE_PATH)) = True) Then
        cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY_PIM).Visible = True
    Else
        cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY_PIM).Visible = False
    End If
    cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY_PIM).TabIndex = cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY).TabIndex + 1


    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author   : Partha Dutta
    ' Date     : 07/06/2011
    ' Referral : 26
    ' Notes    : Disable existing PIM
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    'separate pim screen no longer required; enquiry contains required functionality
    cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY_PIM).Visible = False

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    If Not (blnHaveCC And blnHaveCO) Then
        LoadTypesOfSales = False
        Exit Function
    End If
    
    cmdNextTranType.BackColor = fraTranType.BackColor
    cmdPrevTranType.BackColor = fraTranType.BackColor
    
    'Display Fnn function hotkey label above buttons
    For lngOptNo = 1 To TRAN_BTN_PER_ROW Step 1
        If lngOptNo < lngTypeNo Then
            Load lblTranFKey(lngOptNo)
            lngLeftPos = ((lngOptNo - 1) Mod TRAN_BTN_PER_ROW) + 1
            lngLeftPos = (cmdTranType(1).Width + 120) * (lngLeftPos - 1) + 180
            lblTranFKey(lngOptNo).Left = lngLeftPos + _
                        ((cmdTranType(1).Width - lblTranFKey(lngOptNo).Width) / 2)
            lblTranFKey(lngOptNo).Visible = True
            lblTranFKey(lngOptNo).Caption = "F" & lngOptNo
        End If
    Next lngOptNo
    
    ' Show the F8 SKU Enquiry button.
    lngOptNo = OPTION_NUMBER_SKU_ENQUIRY
    Load lblTranFKey(lngOptNo)
    lblTranFKey(lngOptNo).Left = cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY).Left + _
                ((cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY).Width - lblTranFKey(lngOptNo).Width) / 2)
    lblTranFKey(lngOptNo).Visible = True
    lblTranFKey(lngOptNo).Caption = "F" & lngOptNo

    ' Show the Ctrl F8 PIM SKU Enquiry button.
    lngOptNo = OPTION_NUMBER_SKU_ENQUIRY_PIM
    Load lblTranFKey(lngOptNo)
    lblTranFKey(lngOptNo).Width = 800
    lblTranFKey(lngOptNo).Left = cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY_PIM).Left + _
                ((cmdTranType(COMMAND_BUTTON_SKU_ENQUIRY_PIM).Width - lblTranFKey(lngOptNo).Width) / 2)
    If (oFSO.FolderExists(goSession.GetParameter(PRM_PIM_ONLY_IMAGE_PATH)) = True) Then
        lblTranFKey(lngOptNo).Visible = True
        lblTranFKey(lngOptNo).Enabled = True
    Else
        lblTranFKey(lngOptNo).Visible = False
        lblTranFKey(lngOptNo).Enabled = False
    End If
    lblTranFKey(lngOptNo).Caption = "Ctrl F" & SKU_ENQUIRY_PIM_F_KEY


    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Author   : Partha Dutta
    ' Date     : 07/06/2011
    ' Referral : 26
    ' Notes    : Disable existing PIM
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    'separate pim screen no longer required; enquiry contains required functionality
    lblTranFKey(lngOptNo).Visible = False
    lblTranFKey(lngOptNo).Enabled = False

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


    LoadTypesOfSales = True
    
End Function

Private Sub ShowTranTypeRow()

Dim lngOptNo As Long
Dim lngBtnNo As Long

    'Hide all options and function key labels first
    For lngOptNo = COMMAND_BUTTON_FIRST_TRUE_BUTTON_ID To cmdTranType.UBound Step 1
        cmdTranType(lngOptNo).Visible = False
        If lngOptNo - 1 <= TRAN_BTN_PER_ROW Then
            lblTranFKey(lngOptNo - 1).Visible = False
        End If
    Next lngOptNo
            
    For lngOptNo = 1 To TRAN_BTN_PER_ROW Step 1
        lngBtnNo = mlngTranTypeRowNo * TRAN_BTN_PER_ROW + lngOptNo
        If lngBtnNo + 1 > cmdTranType.UBound Then Exit For
'        Set moTOSType = mcolTOSOpt(CLng(cmdTranType(lngOptNo).Tag))
'        If moTOSType.SupervisorRequired = False Or _
'           moUser.Supervisor = True Or _
'           moUser.Manager = True Then
'            If (mlngTranTypeRowNo = 0) Then
                lblTranFKey(lngOptNo).Visible = True
 '           Else
  '              lblTranFKey(lngOptNo - 1).Visible = True
   '         End If
            cmdTranType(lngBtnNo + 1).Visible = True
'        End If
    Next lngOptNo
    
    cmdPrevTranType.Visible = True
    cmdNextTranType.Visible = True
    If mlngTranTypeRowNo = 0 Then cmdPrevTranType.Visible = False
    If (mlngTranTypeRowNo + 1) * TRAN_BTN_PER_ROW > cmdTranType.UBound Then cmdNextTranType.Visible = False

End Sub

Private Sub ShowTenderTypeRow()

Dim lngOptNo As Long
Dim lngBtnNo As Long

    'Hide all options and function key labels first
    For lngOptNo = 1 To cmdTenderType.UBound Step 1
        cmdTenderType(lngOptNo).Visible = False
        If lngOptNo <= lblTenderFKey.UBound Then
            lblTenderFKey(lngOptNo).Visible = False
        End If
    Next lngOptNo
            
    For lngOptNo = 1 To BTN_PER_ROW Step 1
        lngBtnNo = mlngTenderTypeRowNo * BTN_PER_ROW + lngOptNo
        If lngBtnNo > cmdTenderType.UBound Then Exit For
        lblTenderFKey(lngOptNo).Visible = True
        cmdTenderType(lngBtnNo).Visible = True
    Next lngOptNo
    
    cmdPrevTenderType.Visible = True
    cmdNextTenderType.Visible = True
    If mlngTenderTypeRowNo = 0 Then cmdPrevTenderType.Visible = False
    If (mlngTenderTypeRowNo + 1) * BTN_PER_ROW >= cmdTenderType.UBound Then
        cmdNextTenderType.Visible = False
    End If

End Sub

Private Sub ShowActionRow()

Dim lngOptNo As Long
Dim lngBtnNo As Long

    'Hide all options and key labels first
    For lngOptNo = 1 To cmdAction.UBound Step 1
        cmdAction(lngOptNo).Visible = False
        lblActionFKey(lngOptNo).Visible = False
    Next lngOptNo
            
    'Show active options buttons and key labels
    For lngOptNo = 1 To BTN_PER_ROW Step 1
        lngBtnNo = mlngActionRowNo * BTN_PER_ROW + lngOptNo
        If lngBtnNo > cmdAction.UBound Then Exit For
        lblActionFKey(lngBtnNo).Visible = True
        cmdAction(lngBtnNo).Visible = True
        If cmdAction(lngBtnNo).Caption = "Blank" Then 'MO'C WIX1376 - Make blank Buttons invisible
            lblActionFKey(lngBtnNo).Visible = False
            cmdAction(lngBtnNo).Visible = False
        End If
    Next lngOptNo
    
    cmdPrevAction.Visible = True
    cmdNextAction.Visible = True
    If mlngActionRowNo = 0 Then cmdPrevAction.Visible = False
    If (mlngActionRowNo + 1) * BTN_PER_ROW >= cmdAction.UBound Then
        cmdNextAction.Visible = False
    End If

End Sub

Private Sub txtUserID_KeyUp(KeyCode As Integer, Shift As Integer)
    
    'Check if new keypressed makes up full User ID, so auto move to password
    If (Len(txtUserID.Text) = 3) And (Len(mstrOldUserID) = 2) Then
        KeyCode = 0
        Call txtUserID_KeyPress(vbKeyReturn)
    End If
    mstrOldUserID = txtUserID.Text

End Sub

Private Sub txtUserID_LostFocus()

    txtUserID.BackColor = RGB_WHITE

End Sub

Public Sub ucItemFind_Apply(PartCode As String)

'    ucItemFind.Visible = False
    If frmItemFilter.ucItemFind.ViewOnly Then
        fraTranType.Visible = True
        Exit Sub
    End If
    
    txtSKU.Text = PartCode
    Call txtSKU_KeyPress(vbKeyReturn)

End Sub

Public Sub ucItemFind_Cancel()

    'ucItemFind.Visible = False
    If frmItemFilter.ucItemFind.ViewOnly Then
        fraTranType.Visible = True
        Exit Sub
    End If
    
    If (txtSKU.Enabled = True) And (txtSKU.Enabled = True) Then
        txtSKU.SetFocus
    ElseIf (uctntPrice.Enabled = True) And (uctntPrice.Visible = True) Then
        uctntPrice.SetFocus
    End If

End Sub

Private Sub RetrieveSaleLine()

Const PRM_PROMPT_ACC_REFUND As Long = 824

Dim strRPartCode   As String
Dim dblRQuantity   As Double
Dim curRPrice      As Currency
Dim strRStoreNo    As String
Dim strRTranID     As String
Dim dteRTranDate   As Date
Dim strRTillID     As String
Dim strRPriceCode  As String
Dim lngRLineNo     As Long
Dim blnSwitchMode  As Boolean
Dim blnVoided      As Boolean
Dim blnTendered    As Boolean

Dim lngLinePos    As Long
Dim strPartCode   As String
Dim blnNewItem    As Boolean

Dim OrderNumber   As String

    If ((oRetrieveTran Is Nothing) = True) Then
        Set oRetrieveTran = New frmRetrieveTran
        Load oRetrieveTran
    End If
    
    If goSession.GetParameter(PRM_PROMPT_ACC_REFUND) = True Then
        If MsgBoxEx("Is this an account refund?", vbYesNo, "Account Refund", , , , , _
                    RGBMSGBox_PromptColour) = vbYes Then
            mblnCustDone = True
        End If
    End If
    
    mdblPreviousPrice = 0
    mdblPreviousQty = 0
    
    Call oRetrieveTran.SelectTranLine(strRPartCode, dblRQuantity, curRPrice, strRStoreNo, strRTranID, _
                                      dteRTranDate, strRTillID, lngRLineNo, strRPriceCode, blnVoided, blnTendered, _
                                      mcolRefundCodes, mcolPriceOverrides, OrderNumber)

    If LenB(strRPartCode) <> 0 Then
        lblMaxValue.Caption = Abs(dblRQuantity)
        lblMaxPrompt.Caption = "Max quantity is"
        
        'Preserve maximum refund price for after quantity entered
        mdblRefundPrice = curRPrice
        
        'before entering displaying line, ensure not already refunded
        For lngLinePos = 1 To sprdLines.MaxRows Step 1
            sprdLines.Row = lngLinePos
            sprdLines.Col = COL_PARTCODE
            If sprdLines.Text = strRPartCode Then
                'Matched part code so then check other fields
                sprdLines.Col = COL_RFND_TRANID
                If sprdLines.Text = strRTranID Then
                    sprdLines.Col = COL_RFND_TILLNO
                    If sprdLines.Text = strRTillID Then
                        sprdLines.Col = COL_RFND_DATE
                        If sprdLines.Text = dteRTranDate Then
                            sprdLines.Col = COL_RFND_LINENO
                            If sprdLines.Text = lngRLineNo Then
                                mlngEntryMode = enemNone
                                Call MsgBoxEx("Line already refunded for SKU-'" & strRPartCode & "'" & _
                                              vbCrLf & "Amending refunded line", vbInformation, _
                                              "Refund line already used", , , , , RGBMSGBox_PromptColour)
                                mblnEditingLine = True
                                blnNewItem = False
                                sprdLines.Col = COL_SELLPRICE
                                mdblPreviousPrice = Abs(Val(sprdLines.Value))
                                sprdLines.Col = COL_QTY
                                mdblPreviousQty = Abs(Val(sprdLines.Value))
                                txtSKU.Text = mdblPreviousQty
                                sprdLines.SelBackColor = RGB_RED
                                Call sprdLines.SetSelection(-1, sprdLines.Row, -1, sprdLines.Row)
                            End If 'Match found on Line number
                        End If 'Transaction date matched
                    End If 'Till Number matched
                End If 'Transaction ID matched
            End If 'Part Code matched
        Next lngLinePos
        
        'If not refunded then add and wait for refund quantity
        If mblnEditingLine = False Then
            sprdLines.MaxRows = sprdLines.MaxRows + 1
            sprdLines.Row = sprdLines.MaxRows
            sprdLines.Col = 0
            sprdLines.Text = " " 'override auto column lettering
            sprdLines.Col = COL_PARTCODE
            sprdLines.Text = strRPartCode
            Call sprdLines_EditMode(COL_PARTCODE, sprdLines.Row, 0, True)
            sprdLines.Row = sprdLines.MaxRows
            sprdLines.Col = COL_LINETYPE
            sprdLines.Text = LT_ITEM
            sprdLines.Col = COL_RFND_TRANID
            sprdLines.Text = strRTranID
            sprdLines.Col = COL_RFND_DATE
            sprdLines.Text = dteRTranDate
            sprdLines.Col = COL_RFND_TILLNO
            sprdLines.Text = strRTillID
            sprdLines.Col = COL_RFND_LINENO
            sprdLines.Text = lngRLineNo
            sprdLines.Col = COL_PARTCODE
        End If 'new item must be added to list
        
        Call SetEntryMode(enemQty)
        fraMaxValue.Visible = True

    Else 'no part code so Close/Cancel called
        Unload oRetrieveTran
        Set oRetrieveTran = Nothing
        mblnRefundLineMode = False
 '       Call SetCurrentMode(encmRecord)
        Call SetEntryMode(enemSKU)
    End If

End Sub

Private Sub ColleagueDiscount()

    sprdTotal.Row = ROW_TOTTOTALS
    sprdTotal.Col = COL_TOTVAL
    
    If (sprdLines.MaxRows = 1) Or ((Val(sprdTotal.Value) - RefundTotal) = 0) Then
        If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then Call txtSKU.SetFocus
        If (uctntPrice.Visible = True) And (uctntPrice.Enabled = True) Then Call uctntPrice.SetFocus
        Exit Sub
    End If
    
    mlngCurrentMode = encmColleagueDiscount
    Call RecordTender(False, False, True)

End Sub 'Colleague Discount



Private Sub RetrieveHeadOfficeAddress()

    Set moSysopt = goDatabase.CreateBusinessObject(CLASSID_SYSTEMOPTIONS)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeName)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeAddress1)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeAddress2)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeAddress3)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeAddress4)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeAddress5)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_HeadOfficePostCode)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_HeadOfficePhoneNo)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeFaxNumber)
    Call moSysopt.AddLoadField(FID_SYSTEMOPTIONS_VATNumber)
    Call moSysopt.LoadMatches
    
    mstrHeadofficename = moSysopt.HeadOfficeName
    'Add Address 1 to HeadOfficeAddress
    If Len(moSysopt.HeadOfficeAddress1) > 0 Then
        mstrHeadOfficeAddress = _
            Space$((40 - Len(moSysopt.HeadOfficeAddress1)) / 2) & moSysopt.HeadOfficeAddress1 & vbCrLf
    End If
    'Add Address 2 to HeadOfficeAddress
    If Len(moSysopt.HeadOfficeAddress2) > 0 Then
        mstrHeadOfficeAddress = mstrHeadOfficeAddress & _
            Space$((40 - Len(moSysopt.HeadOfficeAddress2)) / 2) & moSysopt.HeadOfficeAddress2 & vbCrLf
    End If
    'Add Address 3 to HeadOfficeAddress
    If Len(moSysopt.HeadOfficeAddress3) > 0 Then
        mstrHeadOfficeAddress = mstrHeadOfficeAddress & _
            Space$((40 - Len(moSysopt.HeadOfficeAddress3)) / 2) & moSysopt.HeadOfficeAddress3 & vbCrLf
    End If
    'Add Address 4 to HeadOfficeAddress
    If Len(moSysopt.HeadOfficeAddress4) > 0 Then
        mstrHeadOfficeAddress = mstrHeadOfficeAddress & _
            Space$((40 - Len(moSysopt.HeadOfficeAddress4)) / 2) & moSysopt.HeadOfficeAddress4 & vbCrLf
    End If
    'Add Address 5 to HeadOfficeAddress
    If Len(moSysopt.HeadOfficeAddress5) > 0 Then
        mstrHeadOfficeAddress = mstrHeadOfficeAddress & _
            Space$((40 - Len(moSysopt.HeadOfficeAddress5)) / 2) & moSysopt.HeadOfficeAddress5 & vbCrLf
    End If
    'Add Postcode to HeadOfficeAddress
    If Len(moSysopt.HeadOfficePostCode) > 0 Then
        mstrHeadOfficeAddress = mstrHeadOfficeAddress & _
            Space$((40 - Len(moSysopt.HeadOfficePostCode)) / 2) & moSysopt.HeadOfficePostCode & vbCrLf
    End If
    mstrHeadOfficePhoneNo = moSysopt.HeadOfficePhoneNo
    mstrHeadOfficeFaxNumber = moSysopt.HeadOfficeFaxNumber
    mstrVATNumber = moSysopt.VATNumber

End Sub 'RetrieveHeadOfficeAddress

Private Sub PrintCustomerInvoice(blnParked As Boolean, _
                                blnVoided As Boolean, _
                                strName As String, _
                                strPostCode As String, _
                                strAddress As String, _
                                strTelNo As String)

Dim oPOSLine     As cPOSLine
Dim oPOSPmt      As cPOSPayment
Dim lngLineNo    As Long
Dim dblAmount    As Double
Dim strTranCode  As String
Dim lngTotalPos  As Long
Dim lngFirstPmt  As Long
Dim blnOrderItem As Boolean
Dim colLines     As Collection

    On Error GoTo HandleException
    
    ucpbStatus.Visible = True
    
    If mblnTransactionDiscountApplied = True Then
        moTranHeader.DiscountAmount = mdblPercentageValue
        moTranHeader.Description = vbNullString
    End If
    
    strTranCode = mstrStoreNo & mstrTillNo & mstrTranId & Format$(moTranHeader.TranDate, "DDMMYY")
    
    ucpbStatus.Caption1 = "Printing Receipt Header"
    
    Call PrintTransactionHeader(OPOSPrinter1, moTranHeader.TranDate, moTranHeader.TransactionTime, _
                                mstrTillNo, moTranHeader.TransactionNo, "SALE", txtUserID.Text, _
                                lblFullName.Caption, lblOrderNo.Caption)
    
    'Print Information for an account sale invoice
    If LenB(mstrAccountNum) <> 0 Then
        Call RetrieveHeadOfficeAddress
        Call PrintAccountDepartmentAddress(OPOSPrinter1, mstrHeadofficename, _
            mstrHeadOfficeAddress, mstrHeadOfficePhoneNo, mstrHeadOfficeFaxNumber, _
            mstrVATNumber)
        Call PrintInvoiceHeader(OPOSPrinter1, mstrAccountNum, mstrAccountName, _
            mstrActive, mstrCardExpiry, mstrAddressLine1, mstrAddressLine2, mstrAddressLine3, _
            mstrPostCode, mstrHomeTelNumber, mstrInvoiceNumber, True)
    End If
    
    'step through each line and print
    Set colLines = moTranHeader.Lines
    For lngLineNo = 1 To colLines.Count Step 1
        With colLines(CLng(lngLineNo))
            Call PrintLine(OPOSPrinter1, .PartCode, .Description, vbNullString, .QuantitySold, _
                .LookUpPrice, .ExtendedValue, .VATCode, False)
            
        End With
    Next lngLineNo
    
    'Print the Discount Summary if there are any discounts
    If mblnTransactionDiscountApplied = True Then
        Call modReceipt.PrintTotalBeforeDiscounts(OPOSPrinter1, _
            mdblTotalBeforeDis)
        Call modReceipt.PrintTransDiscountSummary(OPOSPrinter1, _
            mdblPercentageValue, mstrTransDiscountDesc)
        Call modReceipt.PrintTotalDiscounts(OPOSPrinter1, _
            mdblPercentageValue)
    End If

    If (blnParked = False) And (blnVoided = False) Then
        Call PrintTranTotal(OPOSPrinter1, moTOSType.Description, moTranHeader.TotalSaleAmount, True)
    End If
    
    ucpbStatus.Caption1 = "Storing Payment Lines"
    ucpbStatus.Value = 0
    If ((sprdLines.MaxRows - lngFirstPmt) > 1) Then ucpbStatus.Max = sprdLines.MaxRows - lngFirstPmt
    'step through each line and save any payment lines
    For lngLineNo = lngFirstPmt To sprdLines.MaxRows Step 1
        ucpbStatus.Value = lngLineNo - lngFirstPmt
        sprdLines.Row = lngLineNo
        'Ensure only Payment lines are saved
        sprdLines.Col = COL_LINETYPE
        If Val(sprdLines.Value) = LT_PMT Then
            'add payment line to Till header for saving
            sprdLines.Col = COL_INCTOTAL
            dblAmount = Val(sprdLines.Value)
            sprdLines.Col = COL_TENDERTYPE
'            Set oPOSPmt = moTranHeader.AddPayment(sprdLines.Text, dblAmount)
            'If transaction completed okay, then print line item
            sprdLines.Col = COL_DESC
            If (blnParked = False) And (blnVoided = False) Then
                Call PrintPayment(OPOSPrinter1, dblAmount, sprdLines.Text)
            End If
        End If 'saving payment item
    Next lngLineNo
    
    
    ucpbStatus.Caption1 = "Printing Receipt Footer"
    
    If LenB(mstrAccountNum) <> 0 Then
        Call PrintInvoiceTrailer1(OPOSPrinter1)
        'Call PrintInvoiceTrailer2(OPOSPrinter1)
        Call PrintInvoiceTrailer3(OPOSPrinter1)
    End If

    'Finish print of Till Receipt footer
    If (blnParked = True) Then
        Call PrintParked(OPOSPrinter1, mstrTillNo & moTranHeader.TransactionNo & _
                         Format$(moTranHeader.TranDate, "DDMMYY"))
    End If
    If (blnParked = False) And (blnVoided = True) Then
        Call PrintVoided(OPOSPrinter1)
    Else
        If (blnParked = False) Then Call PrintBarCode(OPOSPrinter1, strTranCode)
        If LenB(strName) <> 0 Then
            Call PrintAddress(OPOSPrinter1, strName, strAddress, strPostCode, strTelNo)
        End If
        Call PrintFooter(OPOSPrinter1)
    End If
    Set moTranHeader = Nothing
    ucpbStatus.Visible = False
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("PrintCustomerInvoice", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
        
End Sub

Private Sub PrintCreditNoteCustomerCopy(blnParked As Boolean, _
                            blnVoided As Boolean, _
                            strName As String, _
                            strPostCode As String, _
                            strAddress As String, _
                            strTelNo As String)

Dim oPOSLine     As cPOSLine
Dim oPOSPmt      As cPOSPayment

Dim oRefundTran   As cPOSHeader 'used to mark items that have been refunded
Dim strRFTranNo   As String 'if refund was against original transaction then used to update
Dim strRFTillID   As String
Dim dteRFTranDate As Date
Dim lngRFLineNo   As Long
Dim dblRFQuantity As Double

Dim lngLineNo    As Long
Dim dblAmount    As Double
Dim strTranCode  As String
Dim lngTotalPos  As Long
Dim lngFirstPmt  As Long
Dim blnCustDone  As Boolean 'used to ensure only 1 DLRCUS written for DLTOTS

    On Error GoTo HandleException
    
    moTranHeader.Voided = blnVoided
    moTranHeader.TranParked = blnParked
    moTranHeader.FromDCOrders = blnParked 'double flag required as Customer Orders causing conflict
    
    strTranCode = mstrStoreNo & mstrTillNo & mstrTranId & Format$(moTranHeader.TranDate, "DDMMYY")
    
    Set oRefundTran = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    
    Call PrintTransactionHeader(OPOSPrinter1, moTranHeader.TranDate, moTranHeader.TransactionTime, _
                                mstrTillNo, moTranHeader.TransactionNo, "Store Copy", _
                                txtUserID.Text, lblFullName.Caption, vbNullString)
    
    If LenB(mstrAccountNum) <> 0 Then
        Call RetrieveHeadOfficeAddress
        Call PrintAccountDepartmentAddress(OPOSPrinter1, mstrHeadofficename, _
            mstrHeadOfficeAddress, mstrHeadOfficePhoneNo, mstrHeadOfficeFaxNumber, _
            mstrVATNumber)
        Call PrintCreditNoteHeader(OPOSPrinter1, mstrAccountNum, mstrAccountName, _
            mstrActive, mstrCardExpiry, mstrAddressLine1, mstrAddressLine2, mstrAddressLine3, _
            mstrPostCode, mstrHomeTelNumber, mstrInvoiceNumber, True)
    End If
    
    'step through each line and save
    For lngLineNo = 2 To sprdLines.MaxRows Step 1
        sprdLines.Row = lngLineNo
        'Ensure only lines that are SKU's
        sprdLines.Col = COL_LINETYPE
        If Val(sprdLines.Value) = LT_TOTAL Then lngTotalPos = lngLineNo
        If Val(sprdLines.Value) = LT_PMT And lngFirstPmt = 0 Then lngFirstPmt = lngLineNo
        If Val(sprdLines.Value) = LT_ITEM Then
            'Check line has not been voided
            sprdLines.Col = COL_VOIDED
            If (Val(sprdLines.Value) = 0) Then
                Call SaveTranLine(oPOSLine, sprdLines.Row, 0, blnCustDone, blnVoided)
'                'Check if line was from another Receipt, if so flag as refunded
'                sprdLines.Col = COL_RFND_TRANID
'                If lenb(sprdLines.Text) <> 0 Then
'                    strRFTranNo = sprdLines.Text
'                    sprdLines.Col = COL_RFND_TILLNO
'                    strRFTillID = sprdLines.Text
'                    sprdLines.Col = COL_RFND_DATE
'                    dteRFTranDate = sprdLines.Text
'                    sprdLines.Col = COL_RFND_LINENO
'                    lngRFLineNo = Val(sprdLines.Value)
'                    sprdLines.Col = COL_QTY
'                    dblRFQuantity = Val(Abs(sprdLines.Text))
'                    Call oRefundTran.RecordLineRefunded(strRFTranNo, strRFTillID, dteRFTranDate, lngRFLineNo, dblRFQuantity)
'                End If
                'If transaction completed okay, then print line item
                If (blnParked = False) And (blnVoided = False) Then
                    Call PrintLine(OPOSPrinter1, oPOSLine.PartCode, sprdLines.Text, vbNullString, _
                                   oPOSLine.QuantitySold, oPOSLine.LookUpPrice, _
                                   oPOSLine.ExtendedValue, oPOSLine.VATCode, False)
                End If
            End If 'line not voided
        End If 'saving line item
    Next lngLineNo
    
    'Extract transaction total and put into header
    sprdLines.Col = COL_INCTOTAL
    sprdLines.Row = lngTotalPos
    moTranHeader.TotalSaleAmount = Val(sprdLines.Value)
    
    If (blnParked = False) And (blnVoided = False) Then
        Call PrintTranTotal(OPOSPrinter1, moTOSType.Description, moTranHeader.TotalSaleAmount, True)
    End If
    'step through each line and save any payment lines
    For lngLineNo = lngFirstPmt To sprdLines.MaxRows Step 1
        sprdLines.Row = lngLineNo
        'Ensure only Payment lines are saved
        sprdLines.Col = COL_LINETYPE
        If Val(sprdLines.Value) = LT_PMT Then
'            'add payment line to Till header for saving
'            sprdLines.Col = COL_INCTOTAL
'            dblAmount = Val(sprdLines.Value)
'            sprdLines.Col = COL_TENDERTYPE
            'Set oPOSPmt = moTranHeader.AddPayment(sprdLines.Text, dblAmount)
            'If transaction completed okay, then print line item
            If (blnParked = False) And (blnVoided = False) Then
                Call PrintPayment(OPOSPrinter1, dblAmount, sprdLines.Text)
            End If
        End If 'saving payment item
    Next lngLineNo
    
    
    If (blnParked = False) And (blnVoided = False) Then Call PrintDivideLine(OPOSPrinter1)
'    Call goDatabase.StartTransaction
    Call DebugMsg(MODULE_NAME, "SaveSale", endlDebug, "Started saving tran-" & mstrTranId)
    Call moTranHeader.SaveIfExists
    Call DebugMsg(MODULE_NAME, "SaveSale", endlDebug, "Saved tran-" & mstrTranId)
'    Call goDatabase.CommitTransaction
'    If oPOSCust Is Nothing = False Then Call oPOSCust.SaveIfNew
    
    If goSession.GetParameter(PRM_REFUND_PRINT_DECLARATION) = True Then
        Call PrintRefundSlip(OPOSPrinter1, True, "STR", "PAID", Date, "TRAN ID", "RSN", "USER")
    Else
        Call PrintInvoiceTrailer1Refund(OPOSPrinter1)
        'Call PrintInvoiceTrailer2(OPOSPrinter1)
        Call PrintInvoiceTrailer3(OPOSPrinter1)
    End If

    'Finish print of Till Receipt footer
    If (blnParked = True) Then
        Call PrintParked(OPOSPrinter1, mstrTillNo & moTranHeader.TransactionNo & _
                         Format$(moTranHeader.TranDate, "DDMMYY"))
    End If
    If (blnParked = False) And (blnVoided = True) Then
        Call PrintVoided(OPOSPrinter1)
    Else
        If (blnParked = False) Then Call PrintBarCode(OPOSPrinter1, strTranCode)
        If LenB(strName) <> 0 Then
            Call PrintAddress(OPOSPrinter1, strName, strAddress, strPostCode, strTelNo)
            Call PrintDivideLine(OPOSPrinter1)
        End If
        Call PrintFooter(OPOSPrinter1)
    End If
    Set moTranHeader = Nothing
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("PrintCreditNoteCustomerCopy", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
        
End Sub 'PrintCreditNoteCustomerCopy


Public Sub InsertEvents(ByRef colEvents As Collection, ByRef colEventTotals As Collection, _
                        ByRef sprdDest As fpSpread)

Dim lngEventNo   As Long
Dim lngLineNo    As Long
Dim lngEventLine As Long
Dim oEvent       As cTillEvents_Wickes.cLineEvent
Dim oEventTotal  As cTillEvents_Wickes.cEventTotal
Dim strProcedureName As String

strProcedureName = "InsertEvents"

    'step through events and locate line to place event details against
    For lngEventNo = 1 To colEvents.Count Step 1
        Set oEvent = colEvents(lngEventNo)
        For lngLineNo = 1 To sprdDest.MaxRows Step 1
            Call DebugMsgSprdLineValue(lngLineNo, strProcedureName)
            sprdDest.Row = lngLineNo
            sprdDest.Col = COL_ITEMNO
            If Val(sprdDest.Text) = oEvent.lngLineNo Then
                Select Case (oEvent.strType)
                    Case ("QS"):
                                sprdDest.Col = COL_QTYBRKAMOUNT
                                sprdDest.Value = sprdDest.Value + oEvent.dblDiscTotal
                                sprdDest.Col = COL_QTYBRKCODE
                                sprdDest.Text = oEvent.strDealGroupNo
                    Case ("DG"):
                                sprdDest.Col = COL_DGRPAMOUNT
                                sprdDest.Text = sprdDest.Value + oEvent.dblDiscTotal
                                sprdDest.Col = COL_DGRPCODE
                                sprdDest.Text = oEvent.strDealGroupNo
                    Case ("MS"):
                                sprdDest.Col = COL_MBUYAMOUNT
                                sprdDest.Text = sprdDest.Value + oEvent.dblDiscTotal
                                sprdDest.Col = COL_MBUYCODE
                                sprdDest.Text = oEvent.strDealGroupNo
                    Case ("HS"):
                                sprdDest.Col = COL_HIERAMOUNT
                                sprdDest.Text = sprdDest.Value + oEvent.dblDiscTotal
                                sprdDest.Col = COL_HIERCODE
                                sprdDest.Text = oEvent.strDealGroupNo
                End Select
                sprdDest.Col = COL_OP_EVENTSEQNO
                sprdDest.Text = oEvent.strEventNo
                Exit For
            End If
        Next lngLineNo
    Next lngEventNo
    
    'step through events that must be printed and insert after line
    For lngEventNo = colEventTotals.Count To 1 Step -1
        For lngLineNo = 2 To sprdDest.MaxRows Step 1
            Call DebugMsgSprdLineValue(lngLineNo, strProcedureName)
            sprdDest.Row = lngLineNo
            sprdDest.Col = COL_ITEMNO
            Set oEventTotal = colEventTotals(lngEventNo)
            If Val(sprdDest.Text) = oEventTotal.lngLineNo Then
                sprdDest.MaxRows = sprdDest.MaxRows + 1
                Call sprdDest.InsertRows(sprdDest.Row + 1, 1)
                sprdDest.Row = sprdDest.Row + 1
                sprdLines.Col = 0
                sprdLines.Text = " " 'override auto column lettering
                sprdDest.Col = COL_DESC
                sprdDest.Text = oEventTotal.strDesc
'                sprdDest.Col = COL_QTY
'                sprdDest.Text = oEventTotal.dblQuantity * -1
                sprdDest.Col = COL_LINETYPE
                sprdDest.Text = LT_EVENT
                sprdDest.Col = COL_OP_EVENTSEQNO
                sprdDest.Text = oEventTotal.strEventNo
                sprdDest.Col = COL_OP_SUBEVENTNO
                sprdDest.Text = oEventTotal.strDealGroupNo
                sprdDest.Col = COL_INCTOTAL
                sprdDest.Text = oEventTotal.dblDiscTotal * -1
                sprdDest.Col = COL_OP_TYPE
                sprdDest.Text = oEventTotal.strType
                If (mdteEventEndDate > oEventTotal.dteEndDate) Then mdteEventEndDate = oEventTotal.dteEndDate
                Exit For
            End If
        Next lngLineNo
    Next lngEventNo
    Call SetGridTotals(sprdLines.MaxRows)
    Call UpdateTotals
                
End Sub

Public Sub InsertLineDiscounts(ByRef colDiscounts As Collection, ByRef sprdDest As fpSpread)

Dim lngEventNo   As Long
Dim lngLineNo    As Long
Dim lngEventLine As Long
Dim strDeptKey   As String
Dim strDeptGrp   As String
Dim dblLineAmt   As Double
Dim oEvent       As cTillEvents_Wickes.cLineEvent
Dim oEventTotal  As cTillEvents_Wickes.cEventTotal

    'step through events and locate line to place after
    For lngLineNo = sprdDest.MaxRows To 2 Step -1
        sprdDest.Row = lngLineNo
        sprdDest.Col = COL_LINETYPE
        If Val(sprdDest.Text) = LT_ITEM Then
            sprdDest.Col = COL_DEPTCODE
            strDeptKey = sprdDest.Text & "000"
            strDeptGrp = sprdDest.Text
            sprdDest.Col = COL_DEPTGRPCODE
            strDeptGrp = strDeptGrp & sprdDest.Text
'            Set oEvent = Nothing
'            On Error Resume Next
'            Set oEvent = colDiscounts(strDeptGrp)
'            If oEvent Is Nothing Then Set oEvent = colDiscounts(strDeptKey)
'            Call Err.Clear
'            On Error GoTo 0
'            If ((oEvent Is Nothing) = False) Then
'                sprdDest.MaxRows = sprdDest.MaxRows + 1
'                Call sprdDest.InsertRows(sprdDest.Row + 1, 1)
'                sprdDest.Col = COL_INCTOTAL
'                dblLineAmt = Val(sprdDest.Text)
'                sprdDest.Row = sprdDest.Row + 1
'                sprdLines.Col = 0
'                sprdLines.Text = " " 'override auto column lettering
'                sprdDest.Col = COL_DESC
'                sprdDest.Text = oEvent.strDesc
'                sprdDest.Col = COL_QTY
'                sprdDest.Text = 0
'                sprdDest.Col = COL_LINETYPE
'                sprdDest.Text = LT_EVENT
'                sprdDest.Col = COL_OP_EVENTSEQNO
'                sprdDest.Text = oEvent.strEventNo
'                sprdDest.Col = COL_OP_SUBEVENTNO
'                sprdDest.Text = oEvent.strSubEventNo
'                sprdDest.Col = COL_INCTOTAL
'                sprdDest.Text = oEvent.dblDiscRate * -1 * dblLineAmt / 100
'                sprdDest.Col = COL_OP_TYPE
'                sprdDest.Text = "HS"
'            End If
        End If
    Next lngLineNo
    Call SetGridTotals(sprdLines.MaxRows)
    Call UpdateTotals
                
End Sub

Private Sub ucItemFind_Duress()
'    OpenDrawerUnderDuress
End Sub

Private Sub ucKeyPad1_Resize()

    ucKeyPad1.Width = ucKeyPad1.Width

End Sub

Private Sub uctntPrice_GotFocus()
    cmdViewLines.Caption = "Vi&ew Lines"
'    uctntPrice.SelStart = 0
'    uctntPrice.SelLength = Len(uctntPrice.Text)
End Sub

Private Sub uctntPrice_KeyDown(KeyCode As Integer, Shift As Integer)
'
End Sub

Private Sub uctntPrice_KeyPress(KeyAscii As Integer)
    
    Dim strNegativeValue As String
    
    On Error GoTo HandleException
    
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        
        If (mlngCurrentMode = encmOverrideCode) And (mlngEntryMode = enemPrice) And (Abs(uctntPrice.Value) > 9999.99) Then
          Call MsgBoxEx("Override Price entered is too high, please re-enter", _
                                              vbOKOnly, "Invalid Override price", , , , , RGBMsgBox_WarnColour)
                                                                                
          DoEvents
          uctntPrice.SelectAllValue
                                              
          Exit Sub
        End If

        If Not moTOSType Is Nothing Then
            strNegativeValue = CStr(uctntPrice.Value)
            Select Case moTOSType.Code
            Case TT_MISCIN, TT_CORRIN, TT_MISCOUT, TT_CORROUT:
                If (Mid(strNegativeValue, 1, 1) = "-") Then
                    Call MsgBoxEx("Negative values are not allowed.", vbOKOnly, "Invalid value", , , , , RGBMsgBox_WarnColour)
                    uctntPrice.SetFocus
                    Exit Sub
                End If
            End Select
        End If

        txtSKU.Text = uctntPrice.Value
        txtSKU.Visible = True
        Call txtSKU_KeyPress(vbKeyReturn)
    End If

    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("uctntPrice_KeyPress", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub OpenDrawerUnderDuress()

    
Dim strTranCode  As String
Dim oDuressTranHeader As cPOSHeader
Dim lngTranID As Long
Dim ReprintCount  As Integer    ' Commidea Into WSS - Referral 517 - Add receipt re-print number to receipt

    On Error GoTo HandleException
    
    If Not frmVerifyPwd.VerifyDuressPassword(ucKeyPad1.Visible) Then
        Exit Sub
    End If

    If mblnUseCashDrawer And Not mblnTraining Then
        InitialiseCashDrawer
        OPOSCashDrawer.OpenDrawer
    End If
    
    'Create new Transaction Header in database and display Transaction ID
    Set oDuressTranHeader = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    oDuressTranHeader.TillID = mstrTillNo
    oDuressTranHeader.CashierID = frmVerifyPwd.txtAuthID.Text
    oDuressTranHeader.TransactionCode = TT_OPENDRAWER
    oDuressTranHeader.TrainingMode = mblnTraining
    oDuressTranHeader.Voided = False
    If goSession.ISession_IsOnline Then
        oDuressTranHeader.Offline = False
    Else
        oDuressTranHeader.Offline = True
    End If
    
    strTranCode = mstrStoreNo & mstrTillNo & mstrTranId & Format$(oDuressTranHeader.TranDate, "DDMMYY")
        
    oDuressTranHeader.OpenDrawerCode = goSession.GetParameter(Format$(PRM_ODC_DURESS, "00"))
    
    Call DebugMsg(MODULE_NAME, "OpenDrawerUnderDuress", endlDebug, "Started saving tran-" & mstrTranId)
    Call oDuressTranHeader.SaveIfNew
    lblTranNo.Caption = oDuressTranHeader.TransactionNo
    Call BackUpTransaction
    
    Call ZReadUpdate("TR", TT_OPENDRAWER)
    Call UpdateFlashTotals(False, enftMiscOpenDrawer, oDuressTranHeader.TotalSaleAmount)
    
    Dim oRecPrinting As New clsReceiptPrinting
    Set oRecPrinting.Printer = OPOSPrinter1
    Set oRecPrinting.TranHeader = oDuressTranHeader
    oRecPrinting.TillNo = mstrTillNo
    oRecPrinting.TranNo = mstrTranId
    Set oRecPrinting.goRoot = goRoot
    Call oRecPrinting.Init(goSession, goDatabase)
    ' Commidea Into WSS
    ' Referral 522
    ' If merging Commidea receipt, need to reprint - unless it a void receipt with no commidea content
    If MergingCommideaAndWickesReceipts And Not (oDuressTranHeader.Voided And UBound(mavReceipts) = 0) Then
        Set moReceiptPrinting = oRecPrinting
        oRecPrinting.TranHeader.StoreNumber = mstrStoreNo
        oRecPrinting.UsingCommidea = True
        oRecPrinting.CommideaCCReceipts = mavReceipts
        Do
            ' Commidea Into WSS - Referral 517 - Add receipt re-print number to receipt
            Call oRecPrinting.printReceipt(False, moDiscountCardScheme, ReprintCount)
            ReprintCount = ReprintCount + 1
        Loop While MsgBoxEx("Did the receipt print OK?" & vbNewLine & "[Yes] to continue, [No] to re-print.", _
                          vbQuestion + vbYesNo, _
                          "Receipt printed", , , , , RGBMSGBox_PromptColour) = vbNo
    Else
        If (mstrPrinterType = "P" Or mstrPrinterType = "R") Then
            Call oRecPrinting.printReceipt(False, moDiscountCardScheme)
        End If
    End If
    ' End of Commidea Into WSS
    
    Call DebugMsg(MODULE_NAME, "OpenDrawerUnderDuress", endlDebug, "Saved tran-" & mstrTranId)

    Set oDuressTranHeader = Nothing
 
    If mstrTranId > 5000 Then 
        lngTranID = 1
    Else
        lngTranID = (Val(mstrTranId) + 1) Mod 5000
        If lngTranID = 0 Then lngTranID = 1
    End If
    mstrTranId = Format$(lngTranID, "0000")

    'Call OutputCCTVMessage("Under Duress")
    If mblnUseCashDrawer And Not mblnTraining Then
        CloseCashDrawer
    End If
    DoEvents

    fraTender.Visible = False
    fraActions.Visible = False
    
    mlngCurrentMode = encmComplete
    Call Form_KeyPress(vbKeyReturn)
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("OpenDrawerUnderDuress", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub frmPostcode_Duress()
    OpenDrawerUnderDuress
End Sub

Private Function GetPostCode() As Boolean

Dim blnKeyPad As Boolean

    If mblnCapturePostCode = False Then
        GetPostCode = False
        Exit Function
    End If
    
    mlngEntryMode = enemNone
    With frmPostcode
        'If TouchScreen then force Touchscreen for PostCode entry
        If ((sbStatus.Panels("KeyBoard").Picture Is Nothing) = False) Then .ShowNumPad
        blnKeyPad = ucKeyPad1.Visible
        ucKeyPad1.Visible = False
        .PostCode = vbNullString
        .RegionPath = goSession.GetParameter(PRM_TRANSNO)
        .CountryCode = goSession.GetParameter(PRM_COUNTRY_CODE)
        .Show vbModal
        GetPostCode = Not .Cancel
        If GetPostCode Then
            If (goSession.GetParameter(PRM_COUNTRY_CODE) <> "IE") Then
                mstrPostCode = .PostCode
            Else
                mstrAddressLine1 = .PostCode
            End If
        End If
        ucKeyPad1.Visible = blnKeyPad
    End With
    mlngEntryMode = enemSKU
    
End Function


Private Function GetSaleRefundIndexes(colTOS As Collection) As Boolean

Const NOT_SETUP As Long = 0

Dim oTOS As cTypesOfSaleOptions
Dim lngIndex As Long

    mlngSaleIndex = NOT_SETUP
    mlngRefundIndex = NOT_SETUP
    
    For lngIndex = 1 To colTOS.Count
        Set oTOS = colTOS(lngIndex)
        If oTOS.Code = TT_SALE And mlngSaleIndex = NOT_SETUP Then
            mlngSaleIndex = lngIndex
        ElseIf oTOS.Code = TT_REFUND And mlngRefundIndex = NOT_SETUP Then
            mlngRefundIndex = lngIndex
        End If
    Next

    If mlngSaleIndex <> NOT_SETUP And mlngRefundIndex <> NOT_SETUP Then
        GetSaleRefundIndexes = True
    End If
    
    Set oTOS = Nothing
    
End Function

Private Function GetRefundPriceOverrideCode() As cPriceOverrideCode

Dim colPriceOverrides As Collection
Dim oPriceOverrides   As cPriceOverrideCode
    
    Set oPriceOverrides = goDatabase.CreateBusinessObject(CLASSID_PRICEOVERRIDE)
    
    Call oPriceOverrides.AddLoadFilter(CMP_EQUAL, FID_PRICEOVERRIDE_IsRefundPriceOverride, True)
    Set colPriceOverrides = oPriceOverrides.LoadMatches
    
    If colPriceOverrides.Count <> 1 Then
        Exit Function
    End If
    
    Set GetRefundPriceOverrideCode = colPriceOverrides.Item(1)
    Set colPriceOverrides = Nothing
    Set oPriceOverrides = Nothing
        
End Function

Private Function EnsureStockAdjustmentCode08() As Boolean

Dim oStockAdjCode As cStockAdjustmentCode
'Dim colStockAdjCode As Collection

    Set oStockAdjCode = goDatabase.CreateBusinessObject(CLASSID_STOCKADJCODE)

    With oStockAdjCode
        Call .AddLoadFilter(CMP_EQUAL, FID_STOCKADJCODE_AdjustmentNo, "08")
        
        If .LoadMatches.Count > 0 Then
            EnsureStockAdjustmentCode08 = True
        End If
        
'        If colStockAdjCode.Count = 0 Then
'            .AdjustmentNo = "08"
'            .Description = "Parked Transaction Adj."
'        End If
'            .AdjustmentType = "N"
'            .Sign = "S"
'            .SecurityLevel = 9
'            .IsMarkDown = False
'            .IsWriteOff = False
'        If colStockAdjCode.Count = 0 Then
'            .SaveIfNew
'        Else
'            .SaveIfExists
'        End If
    End With
    
    Set oStockAdjCode = Nothing

End Function

Private Function EnsureRefundCode05() As Boolean

Dim oRefundCode    As cRefundCode
Dim colRefundCodes As Collection

    Set oRefundCode = goDatabase.CreateBusinessObject(CLASSID_REFUNDCODE)
    
    With oRefundCode
        Call .AddLoadFilter(CMP_EQUAL, FID_REFUNDCODE_Code, "05")
    
        Set colRefundCodes = .LoadMatches
        If colRefundCodes.Count > 0 Then
            EnsureRefundCode05 = True
        End If
'
'        If colRefundCodes.Count = 0 Then
'            .CodeType = "RF"
'            .Code = "05"
'        End If
'
'        .StockAttribute = " "
'        .PrintReturnLabel = False
'        .Description = "Faulty"
'        .SaveIfNew
'
'        If colRefundCodes.Count = 0 Then
'            .SaveIfNew
'        Else
'            .SaveIfExists
'        End If
    End With
    
    Set oRefundCode = Nothing
    
End Function

Private Function EnsureTenderOverrides() As Boolean

Dim oTenderOverride    As cTenderOverride
'Dim colTenderOverrides As Collection
'Dim oFile              As Scripting.FileSystemObject
'Dim otsTOData          As Scripting.TextStream
'Dim astrTendOverride() As String

    Set oTenderOverride = goDatabase.CreateBusinessObject(CLASSID_TENDEROVERRIDE)
    If oTenderOverride.LoadMatches.Count > 0 Then
        EnsureTenderOverrides = True
    End If

'    Set oFile = New Scripting.FileSystemObject
'    Set otsTOData = oFile.OpenTextFile(goSession.GetParameter(PRM_TENOVRRIDEFILE))
'
'    If Not otsTOData Is Nothing Then
'
'    End If
'
'    With oRefundCode
'        Call .AddLoadFilter(CMP_EQUAL, FID_REFUNDCODE_Code, "05")
'
'        Set colRefundCodes = .LoadMatches
'        If colRefundCodes.Count = 0 Then
'            .CodeType = "RF"
'            .Code = "05"
'        End If
'
'        .StockAttribute = " "
'        .PrintReturnLabel = False
'        .Description = "Faulty"
'        .SaveIfNew
'
'        If colRefundCodes.Count = 0 Then
'            .SaveIfNew
'        Else
'            .SaveIfExists
'        End If
'    End With
    
    Set oTenderOverride = Nothing

End Function

Private Sub XZRead(ByVal blnIsZRead As Boolean)
    
On Error GoTo ZRead_Error:

    Set moTranHeader = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    moTranHeader.TillID = mstrTillNo
    moTranHeader.CashierID = txtUserID.Text
    moTranHeader.TransactionCode = moTOSType.Code
    moTranHeader.TrainingMode = mblnTraining
    moTranHeader.Voided = False
    If goSession.ISession_IsOnline Then
        moTranHeader.Offline = False
    Else
        moTranHeader.Offline = True
    End If
    If LenB(mstrTranSuper) <> 0 Then
        moTranHeader.SupervisorUsed = True
        moTranHeader.SupervisorNo = mstrTranSuper
    End If
    Call moTranHeader.SaveIfNew
    
    On Error Resume Next
    ' Tran is reserved sql word, use "Tran" to identify as a column
    Call goDatabase.ExecuteCommand("UPDATE DLTOTS SET RTIHOLD=1 WHERE DATE1='" & Format(Date, "YYYY-MM-DD") & "' AND TILL='" & moTranHeader.TillID & "' AND ""Tran""='" & moTranHeader.TransactionNo & "'")
    Call Err.Clear
    On Error GoTo 0
    
    mstrTranId = moTranHeader.TransactionNo
    lblTranNo.Caption = mstrTranId
    
    
    Dim oRecPrinting As New clsReceiptPrinting
    
    Set oRecPrinting.Printer = OPOSPrinter1
    Set oRecPrinting.TranHeader = moTranHeader
    oRecPrinting.TillNo = mstrTillNo
    oRecPrinting.TranNo = mstrTranId
    Set oRecPrinting.goRoot = goRoot
    Call oRecPrinting.Init(goSession, goDatabase)
    Call oRecPrinting.PrintXZReads
    
    If blnIsZRead Then
        Call ZReadClear
    
        With moCashierTotals
            .CurrentTillNumber = "99"
            .SaveIfExists
        End With
        
        With moTillTotals
'            .PreviousCashierNumber = .CashierNumber
'            .CashierNumber = "999"
            .CurrentTransactionNumber = mstrTranId
            .TillCheckingEvents = False
            .TimeLastSignedOff = Format$(Now, "HHMM")
            .TillZReadDone = True
            .SaveIfExists
        End With
    Else
        Call ZReadUpdate("TR", moTOSType.Code)
    End If
    
    If mblnUseCashDrawer And Not mblnTraining Then
        If moTOSType.OpenOnZeroTran Then
            InitialiseCashDrawer
            OPOSCashDrawer.OpenDrawer
            CloseCashDrawer
        End If
    End If
    
    Call BackUpTransaction
    
    Exit Sub

ZRead_Error:
    Call Err.Bubble(MODULE_NAME, "ZRead", CLng(Erl))
    
End Sub

Private Sub EnsureZReadData()

Dim oTOSCtl   As cTypesOfSaleControl
Dim colTOS    As Collection

Dim oTenCtl   As cTenderControl
Dim colTender As Collection

Dim oZread    As cZReads
Dim colZreads As Collection

    Set oZread = goDatabase.CreateBusinessObject(CLASSID_ZREADS)
    
    Call oZread.AddLoadFilter(CMP_EQUAL, FID_ZREADS_TillID, mstrTillNo)
    Set colZreads = oZread.LoadMatches
    
    Set oTOSCtl = goDatabase.CreateBusinessObject(CLASSID_TYPESOFSALECONTROL)
    Call oTOSCtl.AddLoadFilter(CMP_EQUAL, FID_TYPESOFSALECONTROL_ValidForUse, True)
    Call oTOSCtl.AddLoadFilter(CMP_EQUAL, FID_TYPESOFSALECONTROL_UpdateZReads, True)
    Set colTOS = oTOSCtl.LoadMatches
    
    Set oTenCtl = goDatabase.CreateBusinessObject(CLASSID_TENDERCONTROL)
    Call oTenCtl.AddLoadFilter(CMP_EQUAL, FID_TENDERCONTROL_ValidForUse, True)
    Set colTender = oTenCtl.LoadMatches
    
    If Not ZReadRecordExists(colZreads, "ZR", "   ") Then Call ZReadAdd("ZR", "   ")
    If Not ZReadRecordExists(colZreads, "TC", "   ") Then Call ZReadAdd("TC", "   ")
    If Not ZReadRecordExists(colZreads, "RT", "DIS") Then Call ZReadAdd("RT", "DIS")
    If Not ZReadRecordExists(colZreads, "RT", "MER") Then Call ZReadAdd("RT", "MER")
    If Not ZReadRecordExists(colZreads, "RT", "NME") Then Call ZReadAdd("RT", "NME")
    
    For Each oTOSCtl In colTOS
        If Not ZReadRecordExists(colZreads, "TR", oTOSCtl.Code) Then Call ZReadAdd("TR", oTOSCtl.Code)
    Next oTOSCtl
    
    For Each oTenCtl In colTender
        If Not ZReadRecordExists(colZreads, "TE", Format$(oTenCtl.TendType, "00")) Then
                Call ZReadAdd("TE", Format$(oTenCtl.TendType, "00"))
        End If
    Next oTenCtl
    
End Sub

Private Function ZReadRecordExists(colZR As Collection, strTypeCode As String, strSubType As String) As Boolean

Dim oZread As cZReads
    
    ZReadRecordExists = False
    
    If colZR.Count = 0 Then
        Exit Function
    End If
    
    For Each oZread In colZR
        If oZread.TypeCode = strTypeCode And Trim$(oZread.SubType) = Trim$(strSubType) Then
            ZReadRecordExists = True
            Exit Function
        End If
    Next oZread
    
End Function

Private Sub ZReadAdd(strTypeCode As String, strSubType As String)

Dim oZread As cZReads

    Set oZread = goDatabase.CreateBusinessObject(CLASSID_ZREADS)
    
    oZread.TillID = mstrTillNo
    oZread.TypeCode = strTypeCode
    oZread.SubType = Left$(strSubType & "   ", 3)
    oZread.SaveIfNew
    
End Sub

Private Sub ZReadUpdate(strTypeCode As String, strSubType As String, Optional curTenderAmount As Currency = 0)
    
'Dim oZread   As cZReads
Dim strQuery As String

    On Error GoTo ZRU_Error
        
    ' Set oZread = goDatabase.CreateBusinessObject(CLASSID_ZREADS)
    
    ' Call oZread.AddLoadFilter(CMP_EQUAL, FID_ZREADS_TillID, mstrTillNo)
    ' Call oZread.AddLoadFilter(CMP_EQUAL, FID_ZREADS_TypeCode, strTypeCode)
    ' Call oZread.AddLoadFilter(CMP_EQUAL, FID_ZREADS_SubType, strSubType)
    
    ' If oZread.LoadMatches.Count > 0 Then
        If strTypeCode <> "TE" Then
            If strTypeCode = "TR" Then
                'If mblnTraining Or moTranHeader.Voided Then Exit Sub
                If mblnTraining Then Exit Sub
                
                If strSubType <> "OD" Then
                    ' Update Cash
                    Call goDatabase.ExecuteCommand("Update ZREADS SET COUN = 0 WHERE COUN + 1>99999 AND WSID = '" & mstrTillNo & "' and TYPE = 'TE' and STYP = '01 '")
                    Call goDatabase.ExecuteCommand("Update ZREADS set AMNT = 0 WHERE AMNT + " & moTranHeader.TotalSaleAmount & " > 999999.99 AND WSID = '" & mstrTillNo & "' and TYPE = 'TE' and STYP = '01 '")
                    strQuery = "Update ZREADS set COUN = COUN + 1, AMNT = AMNT + " & moTranHeader.TotalSaleAmount & " where WSID = '" & mstrTillNo & "' and TYPE = 'TE' and STYP = '01 '"
                    Call DebugMsg(MODULE_NAME, "ZReadUpdate", endlDebug, strQuery)
                    Call goDatabase.ExecuteCommand(strQuery)
                    
                    ' Update Discounts
                    Call goDatabase.ExecuteCommand("Update ZREADS SET COUN = 0 WHERE COUN + 1>99999 AND WSID = '" & mstrTillNo & "' and TYPE = 'RT' and STYP = 'DIS'")
                    Call goDatabase.ExecuteCommand("Update ZREADS set AMNT = 0 WHERE AMNT + " & moTranHeader.DiscountAmount & " > 999999.99 AND WSID = '" & mstrTillNo & "' and TYPE = 'RT' and STYP = 'DIS'")
                    strQuery = "Update ZREADS set COUN = COUN + 1, AMNT = AMNT + " & moTranHeader.DiscountAmount & " where WSID = '" & mstrTillNo & "' and TYPE = 'RT' and STYP = 'DIS'"
                    Call DebugMsg(MODULE_NAME, "ZReadUpdate", endlDebug, strQuery)
                    Call goDatabase.ExecuteCommand(strQuery)
                    
                    ' Update Merchandise
                    Call goDatabase.ExecuteCommand("Update ZREADS SET COUN = 0 WHERE COUN + 1>99999 AND WSID = '" & mstrTillNo & "' and TYPE = 'RT' and STYP = 'MER'")
                    Call goDatabase.ExecuteCommand("Update ZREADS set AMNT = 0 WHERE AMNT + " & moTranHeader.MerchandiseAmount & " > 999999.99 AND WSID = '" & mstrTillNo & "' and TYPE = 'RT' and STYP = 'MER'")
                    strQuery = "Update ZREADS set COUN = COUN + 1, AMNT = AMNT + " & moTranHeader.MerchandiseAmount & " where WSID = '" & mstrTillNo & "' and TYPE = 'RT' and STYP = 'MER'"
                    Call DebugMsg(MODULE_NAME, "ZReadUpdate", endlDebug, strQuery)
                    Call goDatabase.ExecuteCommand(strQuery)
                    
                    ' Update Non-Merchandise
                    Call goDatabase.ExecuteCommand("Update ZREADS SET COUN = 0 WHERE COUN + 1>99999 AND WSID = '" & mstrTillNo & "' and TYPE = 'RT' and STYP = 'NME'")
                    Call goDatabase.ExecuteCommand("Update ZREADS set AMNT = 0 WHERE AMNT + " & moTranHeader.MerchandiseAmount & " > 999999.99 AND WSID = '" & mstrTillNo & "' and TYPE = 'RT' and STYP = 'NME'")
                    strQuery = "Update ZREADS set COUN = COUN + 1, AMNT = AMNT + " & moTranHeader.NonMerchandiseAmount & " where WSID = '" & mstrTillNo & "' and TYPE = 'RT' and STYP = 'NME'"
                    Call DebugMsg(MODULE_NAME, "ZReadUpdate", endlDebug, strQuery)
                    Call goDatabase.ExecuteCommand(strQuery)
                End If
            
                With moTillTotals
                    .CurrentTransactionNumber = mstrTranId
                    .TillCheckingEvents = False
                    If strSubType <> "CO" And strSubType <> "CC" Then
                        .TransactionCount = .TransactionCount + 1
                        If strSubType <> "OD" Then
                            .TransactionValues = .TransactionValues + moTranHeader.TotalSaleAmount
                        End If
                    End If
                    .SaveIfExists
                End With
                
                If Not moCashierTotals Is Nothing Then
                With moCashierTotals
                    If strSubType <> "CC" And strSubType <> "CO" Then
                        .TransactionCount = .TransactionCount + 1
                    End If
                    If strSubType <> "OD" Then
                        .TransactionValue = .TransactionValue + moTranHeader.TotalSaleAmount
                    End If
                    .SaveIfExists
                End With
                End If
                
            End If
            
            If strSubType <> "OD" Then
                If strTypeCode <> "ZR" Then
                    'If moTranHeader.TotalSaleAmount <> 0 Then
                        Call goDatabase.ExecuteCommand("Update ZREADS SET COUN = 0 WHERE COUN + 1>99999 AND WSID = '" & mstrTillNo & "' and TYPE = '" & strTypeCode & "' and STYP = '   '")
                        Call goDatabase.ExecuteCommand("Update ZREADS set AMNT = 0 WHERE AMNT + " & moTranHeader.NonMerchandiseAmount & " > 999999.99 AND WSID = '" & mstrTillNo & "' and TYPE = '" & strTypeCode & "' and STYP = '   '")
                        strQuery = "Update ZREADS set COUN = COUN + 1, AMNT = AMNT + " & moTranHeader.NonMerchandiseAmount & " where WSID = '" & mstrTillNo & "' and TYPE = '" & strTypeCode & "' and STYP = '   '"
                        Call DebugMsg(MODULE_NAME, "ZReadUpdate", endlDebug, strQuery)
                        Call goDatabase.ExecuteCommand(strQuery)
'                       Call oZread.AddToTotals(mstrTillNo, strTypeCode, Left$(strSubType & "   ", 3), _
'                                               moTranHeader.TotalSaleAmount, True)
                    'End If
                End If
            
                Call goDatabase.ExecuteCommand("Update ZREADS SET COUN = 0 WHERE COUN + 1>99999 AND WSID = '" & mstrTillNo & "' and TYPE = 'TC' and STYP = '   '")
                Call goDatabase.ExecuteCommand("Update ZREADS set AMNT = 0 WHERE AMNT + " & moTranHeader.TotalSaleAmount & " > 999999.99 AND WSID = '" & mstrTillNo & "' and TYPE = 'TC' and STYP = '   '")
                strQuery = "Update ZREADS set COUN = COUN + 1, AMNT = AMNT + " & moTranHeader.TotalSaleAmount & " where WSID = '" & mstrTillNo & "' and TYPE = 'TC' and STYP = '   '"
                Call DebugMsg(MODULE_NAME, "ZReadUpdate", endlDebug, strQuery)
                Call goDatabase.ExecuteCommand(strQuery)
                'Call oZread.AddToTotals(mstrTillNo, "ZR", "   ", moTranHeader.TotalSaleAmount, True)
            
                Call goDatabase.ExecuteCommand("Update ZREADS SET COUN = 0 WHERE COUN + 1>99999 AND WSID = '" & mstrTillNo & "' and TYPE = 'CA' and STYP = '" & txtUserID.Text & "'")
                Call goDatabase.ExecuteCommand("Update ZREADS set AMNT = 0 WHERE AMNT + " & moTranHeader.TotalSaleAmount & " > 999999.99 AND WSID = '" & mstrTillNo & "' and TYPE = 'CA' and STYP = '" & txtUserID.Text & "'")
                strQuery = "Update ZREADS set COUN = COUN + 1, AMNT = AMNT + " & moTranHeader.TotalSaleAmount & " where WSID = '" & mstrTillNo & "' and TYPE = 'CA' and STYP = '" & txtUserID.Text & "'"
                Call DebugMsg(MODULE_NAME, "ZReadUpdate", endlDebug, strQuery)
                Call goDatabase.ExecuteCommand(strQuery)
                'Call oZread.AddToTotals(mstrTillNo, "CA", txtUserID.Text, moTranHeader.TotalSaleAmount, True)
            End If
        Else
            'If mblnTraining Or moTranHeader.Voided Then Exit Sub
            If mblnTraining Then Exit Sub
            
            Call goDatabase.ExecuteCommand("Update ZREADS set AMNT = 0 WHERE AMNT + " & curTenderAmount & " > 999999.99 AND WSID = '" & mstrTillNo & "' and STYP = '" & strTypeCode & "' and STYP = '01 '")
            strQuery = "Update ZREADS set AMNT = AMNT + " & curTenderAmount & " where WSID = '" & mstrTillNo & "' and TYPE = '" & strTypeCode & "' and STYP = '01 '"
            Call DebugMsg(MODULE_NAME, "ZReadUpdate", endlDebug, strQuery)
            Call goDatabase.ExecuteCommand(strQuery)
            'Call oZread.AddToTotals(mstrTillNo, strTypeCode, "01 ", curTenderAmount, False)
            Call goDatabase.ExecuteCommand("Update ZREADS SET COUN = 0 WHERE COUN + 1>99999 AND WSID = '" & mstrTillNo & "' and TYPE = '" & strTypeCode & "' and STYP = '" & Left$(strSubType & "   ", 3) & "'")
            Call goDatabase.ExecuteCommand("Update ZREADS set AMNT = 0 WHERE AMNT + " & curTenderAmount & " > 999999.99 AND WSID = '" & mstrTillNo & "' and TYPE = '" & strTypeCode & "' and STYP = '" & Left$(strSubType & "   ", 3) & "'")
            strQuery = "Update ZREADS set COUN = COUN + 1, AMNT = AMNT - " & curTenderAmount & " where WSID = '" & mstrTillNo & "' and TYPE = '" & strTypeCode & "' and STYP = '" & Left$(strSubType & "   ", 3) & "'"
            Call DebugMsg(MODULE_NAME, "ZReadUpdate", endlDebug, strQuery)
            Call goDatabase.ExecuteCommand(strQuery)
            'Call oZread.AddToTotals(mstrTillNo, strTypeCode, Left$(strSubType & "   ", 3), -curTenderAmount, True)
        End If
'    Else
'        Call MsgBoxEx("ZRead Record code: " & mstrTillNo & strTypeCode & strSubType & " missing.", _
'                      vbExclamation, Me.Caption, , , , 18, RGBMsgBox_WarnColour)
'        End
'    End If
    Exit Sub

ZRU_Error:
    Call Err.Bubble(MODULE_NAME, "ZReadUpdate", CLng(Erl))
    
End Sub

Private Sub ZReadClear()

Dim oZread    As cZReads
Dim oZRControl  As cZReads
Dim colZreads   As Collection

    'Get all ZRead records for this workstation
    Set oZread = goDatabase.CreateBusinessObject(CLASSID_ZREADS)
    Call oZread.AddLoadFilter(CMP_EQUAL, FID_ZREADS_TillID, mstrTillNo)
    Set colZreads = oZread.LoadMatches

    'Step through each record and if zero
    For Each oZread In colZreads
        
        Select Case (oZread.TypeCode)
            Case ("CA"): oZread.Delete 'Delete Cash Record
            
            Case ("TC"): 'Get ZRead Control record
                        Set oZRControl = goDatabase.CreateBusinessObject(CLASSID_ZREADS)
                        Call oZRControl.AddLoadFilter(CMP_EQUAL, FID_ZREADS_TillID, mstrTillNo)
                        Call oZRControl.AddLoadFilter(CMP_EQUAL, FID_ZREADS_TypeCode, "ZR")
                        Call oZRControl.LoadMatches
                        'Append Todays Count figures on ZReads Control
                        oZRControl.Count = oZRControl.Count + 1
                        oZRControl.Amount = oZRControl.Amount + oZread.Amount
                        'Catch any Database rollover counters
                        If (oZRControl.Count > 99999) Then oZRControl.Count = oZRControl.Count - 99999
                        If (oZRControl.Amount > 9999999.99) Then oZRControl.Amount = oZRControl.Amount - 9999999.99
                        oZRControl.SaveIfExists
                        'Reset Todays Count record
                        oZread.Count = 0
                        oZread.Amount = 0
                        oZread.SaveIfExists
            Case ("TR"), ("RT"), ("TE"):
                        oZread.Count = 0
                        oZread.Amount = 0
                        oZread.SaveIfExists
        End Select
        
    Next oZread
    
End Sub

Private Sub UpdateFlashTotals(blnIsTender As Boolean, lngOccurance As enFlashTotalCategory, curValue As Currency)

    If moSaleTotals Is Nothing Then Exit Sub
    With moSaleTotals
        
        If blnIsTender Then
            If lngOccurance = 99 Then lngOccurance = 1
            .TenderTypesCount(lngOccurance) = .TenderTypesCount(lngOccurance) + 1
            .TenderTypesValue(lngOccurance) = .TenderTypesValue(lngOccurance) - curValue
        Else
            .TransactionCount(lngOccurance) = .TransactionCount(lngOccurance) + 1
            .TranLastTime(lngOccurance) = Format$(Now, "HHNN")
            .TransactionValue(lngOccurance) = .TransactionValue(lngOccurance) + curValue
        End If
        
        .SaveIfExists
    End With
    
End Sub

Private Sub CloseCashDrawer()
    
    On Error GoTo HandleException
    
    If mblnUseCashDrawer And Not mblnTraining Then
        Set frmComplete.CashDrawer = OPOSCashDrawer
        frmComplete.DisplayMessage
        
        OPOSCashDrawer.ReleaseDevice
        OPOSCashDrawer.Close
    End If
    
'    sprdTotal.Text = "Balance"
    Call Form_KeyPress(vbKeyReturn)
    
    DoEvents
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("CloseCashDrawer", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Function InitialiseCashDrawer() As Boolean

Dim lngCashDrawerCode As Long
    
    If (mblnUseCashDrawer = False) Then
        InitialiseCashDrawer = True
        Exit Function
    End If
    
    lngCashDrawerCode = OPOSCashDrawer.Open(mstrCashDrawer)
    
    If lngCashDrawerCode = 0 Then
        lngCashDrawerCode = OPOSCashDrawer.ClaimDevice(10)
    End If
    
    If lngCashDrawerCode = 0 Then
        OPOSCashDrawer.DeviceEnabled = True
        lngCashDrawerCode = OPOSCashDrawer.ResultCode
    End If

    If lngCashDrawerCode = 0 Then
        InitialiseCashDrawer = True
    Else
        InitialiseCashDrawer = False
    End If
    
End Function

Private Function InitialiseBarScanner() As Boolean

    Dim vntParts As Variant '() As String

    vntParts = Split(mstrBarCodeName, ",")
    
    If (mblnHasCashDrawer = False) Or (mblnHasEFT = False) Then On Error Resume Next
    If UBound(vntParts) > 0 Then
        comBarScanner.CommPort = CInt(Right$(vntParts(0), Len(vntParts(0)) - 3))
        comBarScanner.Settings = vntParts(1) & "," & vntParts(2) & "," & vntParts(3) & "," & vntParts(4)
        comBarScanner.DTREnable = True
        comBarScanner.RTSEnable = True
        comBarScanner.RThreshold = 1
        comBarScanner.SThreshold = 1
        comBarScanner.PortOpen = True
        Call DebugMsg(MODULE_NAME, "InitialiseBarScanner", endlDebug, "Barcode reader initialised.")
    Else
        Call DebugMsg(MODULE_NAME, "InitialiseBarScanner", endlDebug, "Barcode reader initialise skipped as no parameter value")
    End If
    If (Err.Number = 0) Then InitialiseBarScanner = True
    
End Function

Private Function LastUnvoidedLine()

Dim lngRowNo    As Long
Dim blnFound    As Boolean
    
    blnFound = False
    For lngRowNo = sprdLines.MaxRows To 2 Step -1
        sprdLines.Row = lngRowNo
        sprdLines.Col = COL_VOIDED
        'check if line is not a reversal line first
        If Val(sprdLines.Text) = 0 Then
            sprdLines.Col = COL_LINETYPE
            If (sprdLines.Text <> LT_PRICEPROM) Then
                Call sprdLines.SetSelection(-1, lngRowNo, -1, lngRowNo)
                blnFound = True
                Exit For
            End If
        End If
    Next
    
    If blnFound = True Then
        LastUnvoidedLine = lngRowNo
    Else
        LastUnvoidedLine = 0
    End If

End Function

Private Function AddItemToLines(Optional blnPaidOut As Boolean = False) As Boolean

Dim strPartCode     As String
Dim lngLineNo       As Long
    
    On Error GoTo HandleException
    
    sprdLines.MaxRows = sprdLines.MaxRows + 1
    sprdLines.Row = sprdLines.MaxRows
    sprdLines.Col = 0
    sprdLines.Text = " " 'override auto column lettering
    sprdLines.Col = COL_PARTCODE
    If Len(txtSKU.Text) < PARTCODE_LEN Then txtSKU.Text = Right$(PARTCODE_PAD & txtSKU.Text, PARTCODE_LEN)
    sprdLines.Text = txtSKU.Text
    strPartCode = txtSKU.Text
    Call sprdLines_EditMode(COL_PARTCODE, sprdLines.Row, 0, True)
    sprdLines.Col = COL_PARTCODE
    sprdLines.Row = sprdLines.MaxRows
    If mblnValidItem Then 'Valid SKU so carry on processing
        If mblnQtyBeforeSKU = True Then
            sprdLines.Col = COL_REFREASON
            sprdLines.Text = mstrRefundReason
            sprdLines.Col = COL_RFND_DATE
            sprdLines.Text = mdteRefOrigTranDate
            sprdLines.Col = COL_RFND_STORENO
            sprdLines.Text = mstrRefOrigStoreNo
            sprdLines.Col = COL_RFND_TILLNO
            sprdLines.Text = mstrRefOrigTillID
            sprdLines.Col = COL_RFND_TRANID
            sprdLines.Text = mstrRefOrigTranNo
            sprdLines.Col = COL_RFND_LINENO
            sprdLines.Text = mlngRefOrigLineNo
            ' Hubs 2.0 - Store the web order number to save in refund cust info
            sprdLines.Col = COL_RFND_WEB_ORDER_NO
            sprdLines.Text = mstrWebOrderNumber
            ' End of Hubs 2.0
            
            sprdLines.Col = COL_QTY
            sprdLines.Text = mdblLineQuantity
            mblnIsRefundLine = False
            mdblLineQuantity = 1
            mstrRefundReason = vbNullString
            mdteRefOrigTranDate = 0
            mstrRefOrigTillID = vbNullString
            mstrRefOrigTranNo = vbNullString
            mstrRefOrigStoreNo = vbNullString
            ' Hubs 2.0 - Referral 766; Blank the web order number so not passed on to next line
            mstrWebOrderNumber = vbNullString
            ' End of Hubs 2.0
            mlngRefOrigLineNo = 0
            
            sprdLines.Row = sprdLines.MaxRows
            sprdLines.Col = COL_INCTOTAL
            If (Val(sprdLines.Value) > 9999999.99) Then
                If blnPaidOut Then
                    sprdLines.Value = 0
                End If
                Call MsgBoxEx("Warning - Total Line Value exceeds system limits (above 9,999,999.99)" & _
                    vbNewLine & "Unable to accept SKU at this quantity", vbOKOnly + vbExclamation, _
                    "Line Total Limit Exceeded", , , , , RGBMsgBox_WarnColour)
                Call moEvents.DeleteSKU(strPartCode, sprdLines.MaxRows)
                sprdLines.MaxRows = sprdLines.MaxRows - 1
                Call SetGridTotals(sprdLines.MaxRows)
                mblnValidItem = False
                Call UpdateTotals
                Exit Function
            End If
        
            Call UpdateTotals
            
        End If
        If (uctntPrice.Visible = True) And (uctntPrice.Enabled = True) Then uctntPrice.SetFocus
        txtSKU.Text = vbNullString
        If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then txtSKU.SetFocus
        If blnPaidOut = False Then
            Call EnableAction(enacVoid, True)
            Call EnableAction(enacPark, True)
            Call EnableAction(enacReversal, True)
            Call EnableAction(enacOverride, True)
        End If
        cmdViewLines.Enabled = True
     
        sprdLines.Col = COL_LINETYPE
        sprdLines.Text = LT_ITEM
        If mbytQtyEntry = QTY_ALWAYS_AFTER_SKU Then
            Call SetEntryMode(enemQty)
        End If
        Call EnableAction(enacLookUp, False)
        If moTOSType.Code = TT_SALE Then
            Call EnableAction(enacOrder, Not mblnCustomerOrder Or Not DisableTrainingModeOrders)
        End If
        AddItemToLines = True
    Else
        AddItemToLines = False
    End If
    
    Exit Function
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("AddItemToLines", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Function

Private Sub SetupGiftVoucherConstants()

    mstrGiftVoucherSKU = goSession.GetParameter(PRM_GIFTVOUCHERSKU)
    mcurMaxGiftVoucherValue = moRetopt.MaxGiftVoucherValue
    
    mstrGiftCardSKU = goSession.GetParameter(PRM_GIFTCARDSKU)
    mcurMaxGiftCardValue = goSession.GetParameter(PRM_GIFTCARDMAXVALUE)
    mcurMinGiftCardValue = goSession.GetParameter(PRM_GIFTCARDMINVALUE)
    mcurMaxRefundGiftCardValue = goSession.GetParameter(PRM_GIFTCARDREFMAXVALUE)
    mcurMinRefundGiftCardValue = goSession.GetParameter(PRM_GIFTCARDREFMINVALUE)
    mcurMaxRedeemGiftCardValue = goSession.GetParameter(PRM_GIFTCARDREDMAXVALUE)
   
    mstrSKUGiftVoucher = mstrGiftVoucherSKU
End Sub

Private Function GiftVoucherValid(strSerial As String, strLineType As String) As Boolean

Dim oGiftVoucherHotFile As cGiftVoucherHotFile

    Set oGiftVoucherHotFile = goDatabase.CreateBusinessObject(CLASSID_GIFTVOUCHERHOTFILE)
    
    Call oGiftVoucherHotFile.AddLoadFilter(CMP_EQUAL, FID_GIFTVOUCHERHOTFILE_SerialNumber, strSerial)
    
    If oGiftVoucherHotFile.LoadMatches.Count <> 0 Then
        
        Call MsgBoxEx("This voucher has already been redeemed" & vbNewLine & vbNewLine & _
                      oGiftVoucherHotFile.HeadOfficeInfo, vbExclamation, "Gift Voucher", , , , , _
                      RGBMsgBox_WarnColour)
        GiftVoucherValid = False
    Else
        If strLineType <> ChrW(178) Then
            GiftVoucherValid = True
            Exit Function
        End If
    End If

End Function
                
Private Function GiftVoucherNotUsed(strSerial As String, blnExcludeSale As Boolean) As Boolean

Dim oPOSGiftVoucher As cPOSGiftVoucher

    GiftVoucherNotUsed = True
    
    Set oPOSGiftVoucher = goDatabase.CreateBusinessObject(CLASSID_POSGIFTVOUCHERS)

    Call oPOSGiftVoucher.AddLoadFilter(CMP_EQUAL, FID_POSGIFTVOUCHERS_SerialNo, strSerial)
    If blnExcludeSale Then
        Call oPOSGiftVoucher.AddLoadFilter(CMP_NOTEQUAL, FID_POSGIFTVOUCHERS_TransactionCode, "SA")
        Call oPOSGiftVoucher.AddLoadFilter(CMP_NOTEQUAL, FID_POSGIFTVOUCHERS_TransactionCode, "TR")
        Call oPOSGiftVoucher.AddLoadFilter(CMP_NOTEQUAL, FID_POSGIFTVOUCHERS_TransactionCode, "CC")
    End If
    
    If oPOSGiftVoucher.LoadMatches.Count <> 0 Then
        Call MsgBoxEx("This Voucher Has Already Been Used", vbExclamation + vbOKOnly, _
                      "Gift Token", , , , , RGBMsgBox_WarnColour)
        GiftVoucherNotUsed = False
    ElseIf moTranHeader.GiftVoucherInTran(strSerial) Then
        Call MsgBoxEx("This Voucher Has Already Been Used", vbExclamation + vbOKOnly, _
                      "Gift Token", , , , , RGBMsgBox_WarnColour)
        GiftVoucherNotUsed = False
    End If
    Call DebugMsg(MODULE_NAME, "GiftVoucherNotused", endlTraceOut)
    
End Function

Private Function GenerateGiftVoucherBarcode(strSerial As String, curValue As Currency) As String
    
Dim strValue As String
Dim strWork  As String * 12
Dim c_seri   As String * 7
Dim cNos     As String

Dim lngCount As Long
Dim lngIndex As Long

    cNos = "6248317950"

'
    Mid$(c_seri, 1, 1) = Mid$(strSerial, 7, 1)
    Mid$(c_seri, 2, 1) = Mid$(strSerial, 6, 1)
    Mid$(c_seri, 3, 1) = Mid$(strSerial, 5, 1)
    Mid$(c_seri, 4, 1) = Mid$(strSerial, 4, 1)
    Mid$(c_seri, 5, 1) = Mid$(strSerial, 3, 1)
    Mid$(c_seri, 6, 1) = Mid$(strSerial, 2, 1)
    Mid$(c_seri, 7, 1) = Mid$(strSerial, 1, 1)
    
    For lngCount = 1 To 7
        lngIndex = Mid$(c_seri, lngCount, 1) + 1
        Mid$(c_seri, lngCount, 1) = Mid$(cNos, lngIndex, 1)
    Next lngCount

    strValue = Right$("00000" & CStr(curValue * 100), 5)
    
    Mid$(strWork, 1, 1) = Mid$(c_seri, 1, 1)
    Mid$(strWork, 3, 1) = Mid$(c_seri, 2, 1)
    Mid$(strWork, 5, 1) = Mid$(c_seri, 3, 1)
    Mid$(strWork, 7, 1) = Mid$(c_seri, 4, 1)
    Mid$(strWork, 9, 1) = Mid$(c_seri, 5, 1)
    Mid$(strWork, 11, 1) = Mid$(c_seri, 6, 1)
    Mid$(strWork, 12, 1) = Mid$(c_seri, 7, 1)
    
    Mid$(strWork, 2, 1) = Mid$(strValue, 1, 1)
    Mid$(strWork, 4, 1) = Mid$(strValue, 2, 1)
    Mid$(strWork, 6, 1) = Mid$(strValue, 3, 1)
    Mid$(strWork, 8, 1) = Mid$(strValue, 4, 1)
    Mid$(strWork, 10, 1) = Mid$(strValue, 5, 1)
    
    GenerateGiftVoucherBarcode = strWork

End Function

Private Function OutputCancelledGiftVoucher(strSerial As String) As String

Dim oHotFile As cGiftVoucherHotFile

    Set oHotFile = goDatabase.CreateBusinessObject(CLASSID_GIFTVOUCHERHOTFILE)
    
    With oHotFile
        .serialNumber = strSerial
        .RedemptionInfo = mstrStoreNo & Format$(moTranHeader.TranDate, "DD/MM/YY") & moTranHeader.TillID & _
                            moTranHeader.TransactionNo & "CS" & moTranHeader.CashierID
        .HeadOfficeInfo = "Cancelled"
        .Commed = False
        .SaveIfNew
    End With
    
End Function

Private Function OutputRedeemedGiftVoucher(strSerial As String) As String

Dim oHotFile As cGiftVoucherHotFile

    Set oHotFile = goDatabase.CreateBusinessObject(CLASSID_GIFTVOUCHERHOTFILE)
    
    With oHotFile
        .serialNumber = strSerial
        .RedemptionInfo = mstrStoreNo & moTranHeader.TranDate & moTranHeader.TillID & _
                            moTranHeader.TransactionNo & "TS" & moTranHeader.CashierID
        .HeadOfficeInfo = "Redeemed"
        .Commed = False
        .SaveIfNew
    End With
    
    
End Function

Private Function OutputRefundedGiftVoucher(strSerial As String)
Dim oHotFile As cGiftVoucherHotFile

    Set oHotFile = goDatabase.CreateBusinessObject(CLASSID_GIFTVOUCHERHOTFILE)
    
    With oHotFile
        .serialNumber = strSerial
        .RedemptionInfo = mstrStoreNo & moTranHeader.TranDate & moTranHeader.TillID & _
                            moTranHeader.TransactionNo & "RR" & moTranHeader.CashierID
        .HeadOfficeInfo = "Refunded"
        .Commed = False
        .SaveIfNew
    End With
    
    
End Function

Private Sub DecodeGiftTokenBarcode(ByVal strTokenBC As String, ByRef strTokenSerial As String, _
                                   ByRef curTokenValue As Currency)
    
Dim strValue As String
Dim strWork  As String
Dim c_seri   As String * 7
Dim cNos     As String

Dim lngCount As Long
Dim lngIndex As Long

    cNos = "6248317950"

    strValue = Mid$(strTokenBC, 2, 1) & Mid$(strTokenBC, 4, 1) & Mid$(strTokenBC, 6, 1) & _
               Mid$(strTokenBC, 8, 1) & Mid$(strTokenBC, 10, 1)
    curTokenValue = CCur(strValue) / 100
    
    strWork = (InStr(1, cNos, Mid$(strTokenBC, 12, 1)) - 1)
    strWork = strWork & (InStr(1, cNos, Mid$(strTokenBC, 11, 1)) - 1)
    strWork = strWork & (InStr(1, cNos, Mid$(strTokenBC, 9, 1)) - 1)
    strWork = strWork & (InStr(1, cNos, Mid$(strTokenBC, 7, 1)) - 1)
    strWork = strWork & (InStr(1, cNos, Mid$(strTokenBC, 5, 1)) - 1)
    strWork = strWork & (InStr(1, cNos, Mid$(strTokenBC, 3, 1)) - 1)
    strWork = strWork & (InStr(1, cNos, Mid$(strTokenBC, 1, 1)) - 1)
    
    strTokenSerial = strWork & GetCheckDigit(strWork)

End Sub

Private Sub FinaliseTransaction(blnNewSalesOrder As Boolean, Optional blnResetCreateOrder As Boolean = False)

Dim blnShowNumPad    As Boolean
Dim curCashAmount    As Currency
Dim oRecPrinting     As clsReceiptPrinting
Dim oPayment         As cSale_Wickes.cPOSPayment
Dim blnDontCreateOrder As Boolean

    On Error GoTo HandleException
    
    Call DebugMsg(MODULE_NAME, "FinaliseTransaction", endlTraceIn)
    sprdTotal.Row = 1
    sprdTotal.Col = COL_TOTVAL - 1
    
    If Val(sprdTotal.Text) * mlngTranSign < 0 Then
        sprdTotal.Text = "Due"
    Else
        If mlngTranSign = -1 Then
            sprdTotal.Text = "Balance"
        Else
            sprdTotal.Text = "Change"
        End If
    End If
    
    sprdTotal.Row = 3
    sprdTotal.Col = COL_TOTVAL
    
    If Val(sprdTotal.Text) < 0 Then ' Save type 99 change payment record
        For Each oPayment In moTranHeader.Payments
            If (oPayment.tenderType = TEN_CASH) Then
                curCashAmount = curCashAmount + oPayment.TenderAmount
            End If
        Next
            
            
      If (mblnCreditCardUsedInTender = False) Then
          
      
        If (mblnGiftTokenUsedInTender = True) And (Abs(Val(sprdTotal.Text)) >= mcurMinGiftCardValue) And (Abs(Val(sprdTotal.Text)) > Abs(curCashAmount)) Then
            mlngCurrentMode = encmGiftVoucher
            Call AddVoucherSKUs(Abs(Val(sprdTotal.Text)), True, "CC")
            Me.Refresh
            mlngCurrentMode = encmTender
        Else
            sprdLines.MaxRows = sprdLines.MaxRows + 1
            sprdLines.Row = sprdLines.MaxRows
            sprdLines.Col = 0
            sprdLines.Text = " " 'override auto column lettering
            sprdLines.Col = COL_DESC
            sprdLines.Text = "Change"
            sprdTotal.Row = sprdTotal.MaxRows - 1
            sprdTotal.Col = COL_TOTVAL
            
            Me.Refresh
            sprdLines.Col = COL_INCTOTAL
            sprdLines.Text = sprdTotal.Text
            
            sprdLines.Col = COL_LINETYPE
            sprdLines.Value = LT_PMT
            
            Call ProcessPaymentType(99, Val(sprdTotal.Text) * mlngTranSign * -1)
        End If
    End If
     End If
    SetEntryMode (enemNone)
    If moTOSType.RequestDocRef Then
        Do
            Load frmEntry
            blnShowNumPad = ucKeyPad1.Visible
            ucKeyPad1.Visible = False
            If (blnShowNumPad = True) Then frmEntry.ShowNumPad
            moTranHeader.ExternalDocumentNo = Right$("00000000" & _
                                              frmEntry.GetEntry("Enter Invoice number", moTOSType.Description, RGBMSGBox_PromptColour, _
                                              8, False), 8)
'            moTranHeader.ExternalDocumentNo = Right$("00000000" & _
                                              InputBoxEx("Enter Invoice number", moTOSType.Description, , _
                                              enifNumbers, True, RGB_YELLOW, 0, 8), 8)
            ucKeyPad1.Visible = blnShowNumPad
            Unload frmEntry
            If moTranHeader.ExternalDocumentNo = "00000000" Then
                Call MsgBoxEx("Valid Entry Required", vbExclamation, moTOSType.Description, , , , , _
                              RGBMsgBox_WarnColour)
            End If
        Loop While moTranHeader.ExternalDocumentNo = "00000000"
        
        Set oRecPrinting = New clsReceiptPrinting
        Set oRecPrinting.Printer = OPOSPrinter1
        oRecPrinting.TillNo = mstrTillNo
        oRecPrinting.TranNo = mstrTranId
        Set oRecPrinting.goRoot = goRoot
        Call oRecPrinting.Init(goSession, goDatabase)
        
        'Call MsgBoxEx("Insert Document", vbOKOnly, moTOSType.Description, , , , , RGBMSGBox_PromptColour)
        
        sprdLines.Row = ROW_TOTALS
        sprdLines.Col = COL_INCTOTAL
        Call oRecPrinting.PrintDocumentFranking(CDate(Now), Format$(Now, "HHMM"), mstrStoreNo, _
                                                mstrTillNo, mstrTranId, moTranHeader.CashierID, moCashier.Name, _
                                                moTOSType.Description, Val(sprdLines.Value), "Insert Document", _
                                                moTOSType.Description, , , moTranHeader.ExternalDocumentNo, , , _
                                                moConResHeaderBO.Contact1Salutation, moConResHeaderBO.Contact1FirstName, _
                                                moConResHeaderBO.Contact1SurName, moConResHeaderBO.CompanyName, moConResHeaderBO.PostCode, _
                                                moConResHeaderBO.HouseNumberName, moConResHeaderBO.AddressLine1, moConResHeaderBO.AddressLine2, _
                                                moConResHeaderBO.Town, moConResHeaderBO.County, moConResHeaderBO.Country, mstrConResCode, _
                                                mstrConResDesc)
    End If
    
    If mblnNeedDrawerOpen = True Then
        InitialiseCashDrawer
        OPOSCashDrawer.OpenDrawer
    End If
    
    If (blnNewSalesOrder = False) Then
        Call DebugMsg(MODULE_NAME, "FinaliseTransaction", endlDebug, "Save it Now")
        Call DebugMsg(MODULE_NAME, "FinaliseTransaction", endlDebug, "Saving-" & moTOSType.Code)
        Select Case (moTOSType.Code)
            Case TT_SALE:         Call SaveSale(False, False, False, False)
                                    If LenB(mstrAccountNum) <> 0 Then
                                        Call MsgBoxEx("Please confirm that the customer has signed the invoice", _
                                            vbOKOnly, "Confirm Signature", , , , , RGBMSGBox_PromptColour)
                                        Call PrintCustomerInvoice(False, False, vbNullString, vbNullString, _
                                            vbNullString, vbNullString)
                                    End If
                                    mstrAccountNum = vbNullString
            Case TT_REFUND:       Call SaveSale(False, False, False, False)
                                    If LenB(mstrAccountNum) <> 0 Then
                                        Call MsgBoxEx("Please confirm that the customer has signed the credit note", _
                                            vbOKOnly, "Confirm Signature", , , , , RGBMSGBox_PromptColour)
                                        Call PrintCreditNoteCustomerCopy(False, False, vbNullString, _
                                            vbNullString, vbNullString, vbNullString)
                                    End If
                                    mstrAccountNum = vbNullString
            Case TT_MISCOUT, TT_CORROUT: Call SavePaidOut(False)
            Case TT_MISCIN, TT_CORRIN:   Call SavePaidIn(False)
            Case TT_TELESALES:    Call SaveSale(False, False, False, False)
                                    If LenB(mstrAccountNum) <> 0 Then
                                        Call MsgBoxEx("Please confirm that the customer has signed the invoice", _
                                            vbOKOnly, "Confirm Signature", , , , , RGBMSGBox_PromptColour)
                                        Call PrintCustomerInvoice(False, False, vbNullString, _
                                            vbNullString, vbNullString, vbNullString)
                                    End If
                                    mstrAccountNum = vbNullString
            Case TT_TELEREFUND:   Call SaveRefund(False, False, mstrName, mstrPostCode, mstrAddress, mstrTelNo)
                                    If LenB(mstrAccountNum) <> 0 Then
                                        Call MsgBoxEx("Please confirm that the customer has signed the credit note", _
                                            vbOKOnly, "Confirm Signature", , , , , RGBMSGBox_PromptColour)
                                        Call PrintCreditNoteCustomerCopy(False, False, vbNullString, _
                                            vbNullString, vbNullString, vbNullString)
                                    End If
                                    mstrAccountNum = vbNullString
        End Select
        fraTender.Visible = False
        fraActions.Visible = False
    
    Else
        'MO'C 07-03-2008 - Ammended so a gift voucher gets printed for a refund WIX Ref Feb 08
        If moTenderOpts.TendType = TEN_GIFTTOKEN And moTOSType.Code = TT_REFUND Then
            mblnRefundingWithAGiftToken = True
            Call DebugMsg(MODULE_NAME, "FinaliseTransaction", endlDebug, "Get a gift voucher for this transaction.")
            Call RecordTender(False, False, False)
        Else
            Call DebugMsg(MODULE_NAME, "FinaliseTransaction", endlDebug, "Skip save as New Sales Order")
        End If
        If EnableRefundTenderType And blnResetCreateOrder Then
            Call DebugMsg(MODULE_NAME, "FinaliseTransaction", endlDebug, "Setting blnNewSalesOrder to False")
            blnDontCreateOrder = True
        End If
    End If
    
    If mblnUseCashDrawer And Not mblnTraining Then
        CloseCashDrawer
    End If
    
    If (blnNewSalesOrder = False Or blnDontCreateOrder) Then
        mlngCurrentMode = encmComplete
        Call Form_KeyPress(vbKeyReturn)
    Else
        mblnSaveNewOrder = False
        txtSKU.Visible = False 'override Order Value check
        Call CreateOrder(True, False, False)
    End If

    
    ' Referral 820
    ' Get invalid tokenid working
    ' Reset this flag for next transaction
    mblnInvalidTokenID = False
    ' End of Referral 820
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("FinaliseTransaction", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Function RefundTotal() As Currency

Dim lngLineNo As Long

    RefundTotal = 0
    For lngLineNo = 2 To sprdLines.MaxRows
        sprdLines.Row = lngLineNo
        sprdLines.Col = COL_VOIDED
        
        If Val(sprdLines.Value) = 0 Then ' exclude reversal lines
            sprdLines.Col = COL_QTY
            If Val(sprdLines.Value) < 0 Then
                sprdLines.Col = COL_INCTOTAL
                RefundTotal = RefundTotal + Val(sprdLines.Value)
            End If
        End If
    Next

End Function

Private Function ColleagueDiscTotal() As Currency

Dim lngLineNo       As Long
Dim curTotal        As Currency
Dim curLineTotal    As Currency
Dim curQty          As Currency

    curTotal = 0
    For lngLineNo = 2 To sprdLines.MaxRows
        sprdLines.Row = lngLineNo
        sprdLines.Col = COL_VOIDED
        
        If Val(sprdLines.Value) = 0 Then ' exclude reversal lines
            sprdLines.Col = COL_QTY
            curQty = Val(sprdLines.Value)
            sprdLines.Col = COL_RFND_LINENO
            If (curQty < 0) And (Val(sprdLines.Value) = 0) Then curQty = Abs(curQty) 'include Refunded Items if validated
            If (curQty > 0) Then 'Sales item or Non Validated Refund Item
                sprdLines.Col = COL_VOUCHER
                If (sprdLines.Value = "") Then 'exclude Vouchers
                    sprdLines.Col = COL_SALETYPE
                    If (sprdLines.Value <> "D") And (sprdLines.Value <> "C") Then 'exclude Delivery Charges and Charity SKU's
                        sprdLines.Col = COL_INCTOTAL
                        curLineTotal = Val(sprdLines.Value)
                        sprdLines.Col = COL_QTYBRKAMOUNT
                        curLineTotal = curLineTotal - Val(sprdLines.Value)
                        sprdLines.Col = COL_DGRPAMOUNT
                        curLineTotal = curLineTotal - Val(sprdLines.Value)
                        sprdLines.Col = COL_MBUYAMOUNT
                        curLineTotal = curLineTotal - Val(sprdLines.Value)
                        sprdLines.Col = COL_HIERAMOUNT
                        curLineTotal = curLineTotal - Val(sprdLines.Value)
                        'Added 13/3/08 - WIX1312 - for Coll discount - remove WEEE charge
                        sprdLines.Col = COL_WEEE_RATE
                        curLineTotal = curLineTotal - (Val(sprdLines.Value) * curQty)
                        sprdLines.Col = COL_DISCCODE
                        If (Val(sprdLines.Value) = 7) Then
                            sprdLines.Row = sprdLines.Row + 1
                            sprdLines.Col = COL_INCTOTAL
                            curLineTotal = curLineTotal + Val(sprdLines.Value)
                            lngLineNo = lngLineNo + 1
                        End If
                        curTotal = curTotal + Format(curLineTotal * moRetopt.EmployeeDiscount / 100, "0.00")
                    End If 'Delivery Charge
                End If 'Voucher
            End If 'Line not a refund
        End If 'Line not reversed
    Next lngLineNo
    'pass out total discount based per line total
    ColleagueDiscTotal = curTotal * -1

End Function

Private Function VouchersTotal() As Currency

Dim lngLineNo As Long

    VouchersTotal = 0
    For lngLineNo = 2 To sprdLines.MaxRows
        sprdLines.Row = lngLineNo
        sprdLines.Col = COL_VOIDED
        
        If Val(sprdLines.Value) = 0 Then ' exclude reversal lines
            sprdLines.Col = COL_QTY
            If Val(sprdLines.Value) > 0 Then
                sprdLines.Col = COL_VOUCHER
                If (sprdLines.Value <> "") Then
                    sprdLines.Col = COL_INCTOTAL
                    VouchersTotal = VouchersTotal + Val(sprdLines.Value)
                End If 'vouchers
            End If 'not a refund line
        End If 'not voided
    Next lngLineNo
    Call DebugMsg(MODULE_NAME, "VouchersTotal", endlDebug, "Total Vouchers is " & VouchersTotal)

End Function

Private Function GetGridData(sprGrid As fpSpread, lngCol As Long, lngRow As Long) As String

Dim lngSaveRow As Long
Dim lngSaveCol As Long

    With sprGrid
        lngSaveRow = .Row
        lngSaveCol = .Col
        .Row = lngRow
        .Col = lngCol
        GetGridData = .Value
        .Row = lngSaveRow
        .Col = lngSaveCol
    End With
    
End Function

Private Function NextTransactionOK() As Boolean

Dim lngTranID As Long
Dim strTranID As String
Dim oTranHead As cPOSHeader

    If mstrTranId > 5000 Then 
        lngTranID = 1
    Else
        lngTranID = (Val(mstrTranId) + 1) Mod 5000
        If lngTranID = 0 Then lngTranID = 1
    End If
    strTranID = Format$(lngTranID, "0000")

    Set oTranHead = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    Call oTranHead.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, Date)
    Call oTranHead.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, mstrTillNo)
    Call oTranHead.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, strTranID)
    
    If oTranHead.LoadMatches.Count = 0 Then
        NextTransactionOK = True
    Else
        NextTransactionOK = False
    End If
    
End Function


Private Sub UpdatePayments(ByVal curPayAmount As Currency)
                        
    'Display payment line on screen
    sprdLines.Row = sprdLines.MaxRows
    sprdLines.Col = COL_INCTOTAL
    sprdLines.Text = txtSKU.Text
    mdblPaidTotal = mdblPaidTotal + curPayAmount
    
    sprdTotal.Text = Format$(mdblTranTotal + mdblPaidTotal, "0.00")
    'Check if payment reduces balance to finish transaction
    If Val(sprdTotal.Text) * mlngTranSign <= 0 Then
        Call FinaliseTransaction(mblnSaveNewOrder)
    Else
        sprdTotal.Row = 1
        sprdTotal.Col = COL_TOTVAL - 1
        sprdTotal.Text = "Balance"
        SetEntryMode enemNone
        fraTender.Visible = True
    End If
    sprdTotal.Refresh
    txtSKU.Text = vbNullString
    txtSKU.Visible = False
    lblType.Visible = False

End Sub

Private Sub SetupDisplayForTender()

Dim blnShowTotals As Boolean
Dim lngRowNo      As Long
    
    sbStatus.Panels(PANEL_INFO).Text = "Select payment method"
    Call SetEntryMode(enemNone)
    mlngCurrentMode = encmTender
    fraActions.Visible = False
    blnShowTotals = True
    sprdLines.Col = COL_LINETYPE
    For lngRowNo = sprdLines.MaxRows To 2 Step -1
        sprdLines.Row = lngRowNo
        If (sprdLines.Text = LT_TOTAL) Then
            blnShowTotals = False
            Exit For
        End If
    Next lngRowNo

    sprdTotal.Col = 2
    sprdTotal.Row = 1
    sprdTotal.ForeColor = RGB_GREY
    sprdTotal.Col = 3
    sprdTotal.Row = 3
    sprdTotal.ForeColor = RGB_GREY
    SetEntryMode enemNone
    fraTender.Visible = True
    If (blnShowTotals = True) Then
        sprdLines.MaxRows = sprdLines.MaxRows + 3
        sprdLines.RowHeight(sprdLines.MaxRows - 2) = 2
        sprdLines.RowHeight(sprdLines.MaxRows) = 2
        sprdLines.Col = 0
        sprdLines.Row = sprdLines.MaxRows - 2
        sprdLines.Text = " "
        sprdLines.Row = sprdLines.MaxRows - 1
        sprdLines.Text = " "
        sprdLines.Row = sprdLines.MaxRows
        sprdLines.Text = " "
        'mark lines as spacers, so they are not printed
        sprdLines.Col = COL_LINETYPE
        sprdLines.Row = sprdLines.MaxRows - 2
        sprdLines.Text = LT_SPACER
        sprdLines.Row = sprdLines.MaxRows - 1
        sprdLines.Text = LT_TOTAL
        sprdLines.Row = sprdLines.MaxRows
        sprdLines.Text = LT_SPACER
        'Display transaction total
        sprdLines.Row = sprdLines.MaxRows - 1
        sprdLines.Col = COL_DESC
        sprdLines.Text = "Total"
        sprdTotal.Row = ROW_TOTTOTALS
        sprdTotal.Col = COL_TOTVAL
        sprdLines.Row = sprdLines.MaxRows - 1
        sprdLines.Col = COL_INCTOTAL
        sprdLines.Text = sprdTotal.Text
        mdblTranTotal = sprdTotal.Text
        sprdTotal.Text = mdblTranTotal
    End If
    Call sprdLines.SetSelection(-1, sprdLines.MaxRows - 1, -1, sprdLines.MaxRows - 1)
    
    lblType.Visible = False
    txtSKU.Visible = False
    'cmdProcess.Visible = False
    cmdViewLines.Visible = False

End Sub

Private Sub AddOriginalLine(ByVal oTillTran As cPOSHeader, ByVal oTranLine As cPOSLine, ByRef lngPPLineNo As Long)
    
Dim oReturnCust     As cReturnCust
Dim curWEEECharge   As Currency
    
    On Error GoTo HandleException
    
    mdblLineQuantity = oTranLine.QuantitySold
    If oTranLine.PartCode = mstrGiftCardSKU Then
        mlngCurrentMode = encmGiftVoucher
        Call AddVoucherSKUs(oTranLine.ActualSellPrice)
    Else
        mlngCurrentMode = encmTranType
        sprdLines.MaxRows = sprdLines.MaxRows + 1
        sprdLines.Row = sprdLines.MaxRows
        sprdLines.Col = 0
        sprdLines.Text = " " 'override auto column lettering
        sprdLines.Col = COL_LINETYPE
        sprdLines.Text = LT_ITEM
        mlngBCodeCheckCnt = mlngBCodeCheckCnt - 1 'effectively reverse out SKU count added in processing SKU
        Call GetItem(oTranLine.PartCode, False)
        sprdLines.Col = COL_PARTCODE
        If (sprdLines.Text <> oTranLine.PartCode) Then
            sprdLines.MaxRows = sprdLines.MaxRows - 1
            sprdLines.Row = sprdLines.MaxRows
            Exit Sub
        End If

        sprdLines.Col = COL_QTY
        sprdLines.Text = oTranLine.QuantitySold
        sprdLines.Col = COL_SELLPRICE
        sprdLines.Text = oTranLine.ActualSellPrice
        
        If GetBooleanParameter(-1046, goDatabase.Connection) Then
            Call moEvents.UpdateSKUPrice(oTranLine.PartCode, oTranLine.SequenceNo + 1, Val(sprdLines.Text))
        End If
'                sprdLines.Col = COL_INCTOTAL
'                sprdLines.Text = oTranline.ExtendedValue
        
        sprdLines.Col = COL_DISCCODE
        sprdLines.Text = oTranLine.PriceOverrideReason
        sprdLines.Col = COL_DISCMARGIN
        sprdLines.Text = oTranLine.OverrideMarginCode
        
        If mdblLineQuantity < 0 Then
            Set oReturnCust = goDatabase.CreateBusinessObject(CLASSID_RETURNCUST)
            With oReturnCust
                Call .AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionDate, oTillTran.TranDate)
                Call .AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_PCTillID, oTillTran.TillID)
                Call .AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionNumber, oTillTran.TransactionNo)
                Call .AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_LineNumber, oTranLine.SequenceNo)
            End With
            If oReturnCust.LoadMatches.Count > 0 Then
                Set oReturnCust = oReturnCust.LoadMatches.Item(1)
                sprdLines.Col = COL_REFREASON
                sprdLines.Text = oReturnCust.RefundReason
            
                sprdLines.Col = COL_RFND_STORENO
                sprdLines.Text = oReturnCust.OrigTranStore
                sprdLines.Col = COL_RFND_DATE
                sprdLines.Text = oReturnCust.OrigTranDate
                sprdLines.Col = COL_RFND_TILLNO
                sprdLines.Text = oReturnCust.OrigTranTill
                sprdLines.Col = COL_RFND_TRANID
                sprdLines.Text = oReturnCust.OrigTranNumb
            End If
        End If
        If oTranLine.MarkedDownStock Then
            sprdLines.Col = COL_MARKDOWN
            sprdLines.Text = "True"
        End If
        sprdLines.Col = COL_BACKCOLLECT
        If (oTranLine.BackDoorCollect = True) Then
            sprdLines.Text = "Y"
        Else
            sprdLines.Text = "N"
        End If
        
        sprdLines.Col = COL_WEEE_RATE
        sprdLines.Value = 0
        If (Val(oTranLine.WEEELineCharge) > 0) Then curWEEECharge = oTranLine.WEEELineCharge
        sprdLines.Value = curWEEECharge
        'sprdLines.Col = COL_WEEE_SEQN
        'sprdLines.Value = oTranLine.WEEESequence
        
        sprdLines.Col = COL_KEYED_RC
        sprdLines.Text = ""
        
        If (oTranLine.PriceOverrideReason = "07") Then  'MLM SORT THIS ****goSession.GetParameter(PRM_PRICEMATCHCODES)) Then
            sprdLines.Col = COL_SELLPRICE
            sprdLines.Value = sprdLines.Value + oTranLine.PriceOverrideAmount
            lngPPLineNo = lngPPLineNo + 1
            Call InsertPricePromise(sprdLines.Row, oTranLine.PriceOverrideAmount * -1, oTranLine.PriceOverrideAmount * oTranLine.QuantitySold * -1, lngPPLineNo)
            sprdLines.Row = sprdLines.MaxRows - 1
            'copy Price Override Line details from Parked to Current Transaction
        End If
        sprdLines.Col = COL_SELLPRICE
        sprdLines.Value = sprdLines.Value + curWEEECharge
        
        Call SetGridTotals(sprdLines.MaxRows)
        Call UpdateTotals
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("AddOriginalLine", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Function RefundAuthOK(ByVal curRefTotal As Currency)

    RefundAuthOK = True
    
    If mblnRecallingParked Then
        Exit Function
    End If
    
      
    If (curRefTotal >= moRetopt.RefundAuthLevelMgr) Then
        If frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) = False Then
            RefundAuthOK = False
        Else
            moTranHeader.SupervisorUsed = True
            moTranHeader.RefundManager = frmVerifyPwd.txtAuthID.Text
        End If
        Unload frmVerifyPwd
    
            
    ElseIf (curRefTotal >= moRetopt.RefundAuthLevelSupv) Or (mblnNonValidated = True) Or (mblnRefundOverride = True) Then
        'Refund amount is above Supervisor Level or a non-validated receipt so get Authorisation
        If frmVerifyPwd.VerifySupervisorPassword(ucKeyPad1.Visible) = False Then
            RefundAuthOK = False
        Else
            moTranHeader.SupervisorUsed = True
            moTranHeader.RefundSupervisor = frmVerifyPwd.txtAuthID.Text
        End If
        Unload frmVerifyPwd
    End If 'else don't need auth for this level, or are no refund lines
    
End Function

Private Function CollDiscAuthOK(ByVal curCollDiscTotal As Currency)

    CollDiscAuthOK = True
    Exit Function
    
    If curCollDiscTotal >= moRetopt.CollDiscLevelMgr Then
        If frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) = False Then
            CollDiscAuthOK = False
        Else
            moTranHeader.SupervisorUsed = True
            moTranHeader.SupervisorNo = frmVerifyPwd.txtAuthID.Text
        End If
        Unload frmVerifyPwd
    ElseIf curCollDiscTotal >= moRetopt.CollDiscLevelSupv Then
        If frmVerifyPwd.VerifySupervisorPassword(ucKeyPad1.Visible) = False Then
            CollDiscAuthOK = False
        Else
            moTranHeader.SupervisorUsed = True
            moTranHeader.SupervisorNo = frmVerifyPwd.txtAuthID.Text
        End If
        Unload frmVerifyPwd
    End If 'else don't need auth for this level, or are no refund lines
    
End Function

Private Function TendersRecordset(TypeOfSale As String) As Recordset

Dim strQuery       As String
Dim oConnection As Connection
Dim lngSaveCL   As Long

    Set oConnection = goDatabase.Connection
    lngSaveCL = oConnection.CursorLocation
    'oConnection.CursorLocation = adUseClient
    
'    strQuery = "select TENOPT.TTID TendType, TENOPT.TEND TenderNo, TENOPT.TTDE Description, " & _
                       "convert(if(tosten.tend is null, 0, -1), signed) Active " & _
               "From tenopt " & _
               "inner join TENCTL on TENCTL.TTID=TENOPT.TTID " & _
               "inner join tosctl on tosctl.tosc='" & TypeOfSale & "' " & _
               "inner join tosopt on tosopt.tosc=tosctl.tosc " & _
               "left outer join TOSTEN on tosten.tend=TENOPT.Tend and tosten.dseq=tosopt.dseq " & _
               "Where tenctl.IUSE = 1 " & _
               "order by tenopt.TEND"
    
    strQuery = "select TENOPT.TTID TendType, TENOPT.TEND TenderNo, TENOPT.TTDE Description, " & _
                       "tosten.tend Active " & _
               "From tenopt " & _
               "inner join TENCTL on TENCTL.TTID=TENOPT.TTID " & _
               "inner join tosctl on tosctl.tosc='" & TypeOfSale & "' " & _
               "inner join tosopt on tosopt.tosc=tosctl.tosc " & _
               "left outer join TOSTEN on tosten.tend=TENOPT.Tend and tosten.dseq=tosopt.dseq " & _
               "Where tenctl.IUSE = 1 " & _
               "order by tenopt.TEND"
    
    Set TendersRecordset = goDatabase.ExecuteCommand(strQuery)
    oConnection.CursorLocation = adUseClient
    
End Function


Private Function CheckForFiles() As Boolean

Dim oFSO        As New FileSystemObject

    CheckForFiles = False

    'Search for eftauth.par
    If oFSO.FileExists("c:\eftauth.par") = False Then
        Call MsgBoxEx("WARNING: Unable to find c:\eftauth.par" & vbCrLf & _
            "Please contact CTS Support", vbExclamation, "Unable to find file", , , , , RGBMsgBox_WarnColour)
        CheckForFiles = False
        Exit Function
    End If
    
    'Search for logo
    If oFSO.FileExists("c:\logo.bmp") = False Then
        Call MsgBoxEx("WARNING: Unable to find c:\logo.bmp" & vbCrLf & _
            "Please contact CTS Support", vbExclamation, "Unable to find file", , , , , RGBMsgBox_WarnColour)
        CheckForFiles = False
        Exit Function
    End If
    
    'Message Reason Code
    If oFSO.FileExists("c:\wix\Message Reason Code.txt") = False Then
        Call MsgBoxEx("WARNING: Unable to find c:\wix\Message Reason Code.txt" & vbCrLf & _
            "Please contact CTS Support", vbExclamation, "Unable to find file", , , , , RGBMsgBox_WarnColour)
        CheckForFiles = False
        Exit Function
    End If
    
    'Response Codes
    If oFSO.FileExists("c:\wix\response codes.txt") = False Then
        Call MsgBoxEx("WARNING: Unable to find c:\wix\response codes.txt" & vbCrLf & _
            "Please contact CTS Support", vbExclamation, "Unable to find file", , , , , RGBMsgBox_WarnColour)
        CheckForFiles = False
        Exit Function
    End If
    
    CheckForFiles = True

End Function 'CheckForFiles

Private Sub CustomerOrder()

Dim intQResponse   As Integer
Dim strOrderNumber As String
Dim blnAddResult   As Boolean
Dim oQuoteHead     As cQuoteHeader

    On Error GoTo HandleException
    
    If (goSession.ISession_IsOnline = False) Then
        Call MsgBoxEx("Customer Orders are not available in Offline mode", vbOKOnly, "Customer Orders", , , , , RGBMsgBox_WarnColour)
        txtSKU.Text = ""
        Exit Sub
    End If
    
    Call SetEntryMode(enemNone)
        
    intQResponse = MsgBoxEx("Have all other items been entered?", vbYesNo + vbQuestion, "Customer Order", _
                            , , , , RGBMSGBox_PromptColour)
    If intQResponse <> vbYes Then
        Call SetEntryMode(enemSKU)
        Exit Sub
    End If
    
    ' Get Order Number
    lblOrderNo.Caption = vbNullString 'reset Order No flag
    Load frmCustomerOrder
    strOrderNumber = frmCustomerOrder.GetCustomerOrderNo
    Unload frmCustomerOrder
    If (strOrderNumber = "") Then 'user has cancelled Order Retrieval
        Call SetEntryMode(enemSKU)
        Exit Sub
    End If
    
    'Check if Order No was set by the Recall Parked Transaction
    If (lblOrderNo.Caption = vbNullString) Then
        'with order number - process header and lines
        If LenB(strOrderNumber) > 0 Then
            blnAddResult = AddOrderToTransaction(strOrderNumber)
        Else
            blnAddResult = False
        End If
    Else
        'Get the details of the pending Transaction so it can be flagged as processed
        Set oQuoteHead = goDatabase.CreateBusinessObject(CLASSID_QUOTEHEADER)
        Call oQuoteHead.AddLoadFilter(CMP_EQUAL, FID_QUOTEHEADER_QuoteNumber, lblOrderNo.Caption)
        If oQuoteHead.LoadMatches.Count = 0 Then
            Call MsgBoxEx("Quote for Order NOT Found", vbExclamation, "Customer Orders", , , , , RGBMsgBox_WarnColour)
        End If
        'Store Pending Transaction Details so that it can be flagged as done, when saving
        mdteQuoteTranDate = oQuoteHead.TranDate
        mstrQuoteTillID = oQuoteHead.TillID
        mstrQuoteTranNo = oQuoteHead.TransactionNumber
        Set oQuoteHead = Nothing
        
        blnAddResult = True
    End If
    
    Call SetEntryMode(enemSKU)
    If blnAddResult Then
        lblOrderNo.Caption = strOrderNumber
        mblnCustomerOrder = True
        Call EnableAction(enacOrder, False)
        Call RecordTender(False, False, False)
    End If

    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("CustomerOrder", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub RecallQuote()

Dim intQResponse   As Integer
Dim strQuoteNumber As String
Dim blnAddResult   As Boolean
Dim blnExpired     As Boolean
Dim oQuoteHead     As cQuoteHeader
Dim lngKeyResp     As Long
Dim curTotalValue  As Currency

    On Error GoTo HandleException
    
    If (goSession.ISession_IsOnline = False) Then
        Call MsgBoxEx("Quotes are not available in Offline mode", vbOKOnly, "Recall Quotes", , , , , RGBMsgBox_WarnColour)
        txtSKU.Text = ""
        Exit Sub
    End If
    
    Call SetEntryMode(enemNone)
    
    
    ' Get Order Number
    lblOrderNo.Caption = vbNullString 'reset Order No flag
    strQuoteNumber = ""
    Load frmCustomerOrder
    strQuoteNumber = frmCustomerOrder.GetQuote
    Unload frmCustomerOrder
    If (strQuoteNumber = "") Then 'user has cancelled Order Retrieval
        Call SetEntryMode(enemSKU)
        Exit Sub
    End If
    
    'Check if Order No was set by the Recall Parked Transaction
    If (lblOrderNo.Caption = vbNullString) Then
        'with order number - process header and lines
        If LenB(strQuoteNumber) > 0 Then
            DoEvents
            blnAddResult = AddQuoteToTransaction(strQuoteNumber, blnExpired)
        Else
            blnAddResult = False
        End If
        If (blnExpired = False) And (blnAddResult = True) Then
            lblOrderNo.Caption = "Q" & strQuoteNumber
            DoEvents
            Call EnableAction(enacReversal, True)
            If (mblnHasEFT = True) Then
                lngKeyResp = MsgBoxEx("Quote retrieved - Select Action", vbYesNoCancel, "Quote Retrieved", "Take Now", "Create Order", "Amend Quote", , RGBMSGBox_PromptColour)
            Else
                Call MsgBoxEx("Quote retrieved", vbOKOnly, "Quote Retrieved", , , , , RGBMSGBox_PromptColour)
            End If
            
            mblnRecalledQuote = True
            Select Case (lngKeyResp)
                Case (vbYes): curTotalValue = mdblTotalBeforeDis
                            If EnableBuyCoupons Then
                                Call RecordTender(False, False, False)
                            Else
                                Call RecordTender(True, False, False)
                            End If
                            mdblTotalBeforeDis = curTotalValue
                            Exit Sub
                Case (vbNo): mblnShowAddress = True
                             mblnRecalledQuote = False
                             Call CreateOrder(False, True, True)   '##' blnToDeliver)
                             Exit Sub
            End Select
        End If ' if not expired then either Take Now or convert to Order
    Else
        'Get the details of the pending Transaction so it can be flagged as processed
        Set oQuoteHead = goDatabase.CreateBusinessObject(CLASSID_QUOTEHEADER)
        Call oQuoteHead.AddLoadFilter(CMP_EQUAL, FID_QUOTEHEADER_QuoteNumber, lblOrderNo.Caption)
        If oQuoteHead.LoadMatches.Count = 0 Then
            Call MsgBoxEx("Quote for Order NOT Found", vbExclamation, "Customer Orders", , , , , RGBMsgBox_WarnColour)
        End If
        'Store Pending Transaction Details so that it can be flagged as done, when saving
        mdteQuoteTranDate = oQuoteHead.TranDate
        mstrQuoteTillID = oQuoteHead.TillID
        mstrQuoteTranNo = oQuoteHead.TransactionNumber
        Set oQuoteHead = Nothing
        
        blnAddResult = True
    End If
    
    Call SetEntryMode(enemSKU)
    Call EnableAction(enacReversal, True)
    If (blnAddResult = True) And (blnExpired = False) Then
        lblOrderNo.Caption = "Q" & strQuoteNumber
        Call EnableAction(enacRecallQuote, False)
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("RecallQuote", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Function AddOrderToTransaction(strOrderNumber As String) As Boolean

Dim oOrderHead  As cCustOrderHeader
Dim oTranLine   As cPOSLine
Dim colTranLines As Collection
Dim colOrderLines As Collection
Dim oTranHead   As cPOSHeader
Dim oQuoteHead  As cQuoteHeader
Dim oOrderLine  As cCustOrderLine
Dim colCust     As Collection
Dim oCustomer   As cReturnCust
Dim oPOSLine    As cPOSLine

Dim lngLastRow  As Long
Dim lngRow      As Long
Dim lngOrderQty As Long
Dim lngLineQty  As Long
Dim intResponse As Integer
Dim lngTotTaken As Long
Dim blnLineAdded As Boolean

Dim strQuantity   As String
Dim strPartCode   As String
Dim arSaleItems() As String
Dim arOrderItems() As String
Dim lngItemNo     As Long
Dim oAllocLine    As cCustOrderLine
Dim oRefCashierBO As cCashier

Dim blnPayment    As Boolean

    On Error GoTo HandleException
    
    lngLastRow = sprdLines.MaxRows
    
    'Build up collection of Items on Sale/Collection with total quantities
    ReDim arSaleItems(0)
    For lngRow = 2 To lngLastRow
        sprdLines.Row = lngRow
        sprdLines.Col = COL_VOIDED
        If (Val(sprdLines.Value) = 0) Then
            sprdLines.Col = COL_QTY
            If (Val(sprdLines.Value) > 0) Then
                sprdLines.Col = COL_PARTCODE
                strPartCode = sprdLines.Text
                strQuantity = ""
                For lngItemNo = LBound(arSaleItems) To UBound(arSaleItems) Step 1
                    If Left$(arSaleItems(lngItemNo), 6) = strPartCode Then
                        strQuantity = Val(Mid$(arSaleItems(lngItemNo), 7))
                        Exit For
                    End If
                Next lngItemNo
                sprdLines.Col = COL_QTY
                If strQuantity = "" Then
                    strQuantity = strPartCode & sprdLines.Text
                    ReDim Preserve arSaleItems(UBound(arSaleItems) + 1)
                    lngItemNo = UBound(arSaleItems)
                Else
                    strQuantity = strPartCode & (Val(sprdLines.Text) + Val(strQuantity))
                End If
                arSaleItems(lngItemNo) = strQuantity
            End If 'non refund item
        End If 'line has not been reversed
    Next lngRow
    
    moTranHeader.OrderNumber = strOrderNumber

    Set oQuoteHead = goDatabase.CreateBusinessObject(CLASSID_QUOTEHEADER)
    Call oQuoteHead.AddLoadFilter(CMP_EQUAL, FID_QUOTEHEADER_QuoteNumber, strOrderNumber)
    If oQuoteHead.LoadMatches.Count = 0 Then
        Call MsgBoxEx("Quote for Order NOT Found", vbExclamation, "Customer Orders", , , , , RGBMsgBox_WarnColour)
        Exit Function
    End If
    
    'moved 10/10/05 - required to get items to process and prices as per quote, not current price
    Set oTranHead = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    Call oTranHead.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, oQuoteHead.TranDate)
    Call oTranHead.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, oQuoteHead.TillID)
    Call oTranHead.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, oQuoteHead.TransactionNumber)
    Call oTranHead.AddLoadField(FID_POSHEADER_TranParked)
    Call oTranHead.AddLoadField(FID_POSHEADER_RecoveredFromParked)
    Call oTranHead.AddLoadField(FID_POSHEADER_TransactionCode)
    Call oTranHead.LoadMatches
    If (oTranHead.Lines.Count > 0) Then
    End If
    'Remove all reversed lines to ensure that the DLTOTS sequence matches the CORLIN
    Set colTranLines = oTranHead.Lines
    For lngRow = colTranLines.Count To 1 Step -1
        Set oTranLine = colTranLines(lngRow)
        If (oTranLine.LineReversed = True) Then Call colTranLines.Remove(lngRow)
    Next lngRow
        
    If (oTranHead.TranParked = False) And (oTranHead.RecoveredFromParked = True) Then
        If (oTranHead.TransactionCode = TT_SALE) Then
            Call MsgBoxEx("Order '" & strOrderNumber & "' already paid for", vbOKOnly + vbInformation, "Invalid Order", , , , , RGBMsgBox_WarnColour)
            Exit Function
        End If
        If (oTranHead.TransactionCode = TT_REFUND) Then
            Call MsgBoxEx("Refund for Order '" & strOrderNumber & "' already processed", vbOKOnly + vbInformation, "Invalid Order", , , , , RGBMsgBox_WarnColour)
            Exit Function
        End If
    End If
    
    If (oTranHead.TransactionCode = TT_REFUND) And (Val(moTranHeader.RefundCashier) = 0) Then
        'for refund order, ensure refund Password verified
        Load frmVerifyPwd
        If (frmVerifyPwd.VerifyRefundPassword(ucKeyPad1.Visible) = False) Then
            Unload frmVerifyPwd
            Call MsgBoxEx("Order '" & strOrderNumber & "' contains Refund Lines.  Refund authorisation must be entered", vbOKOnly + vbInformation, "Refund Authorisation Invalid", , , , , RGBMsgBox_WarnColour)
            Exit Function
        End If
        Set oRefCashierBO = goDatabase.CreateBusinessObject(CLASSID_CASHIER)
        
        Call oRefCashierBO.AddLoadFilter(CMP_EQUAL, FID_CASHIER_CashierNumber, _
                                       frmVerifyPwd.txtAuthID.Text)
        Set oRefCashierBO = oRefCashierBO.LoadMatches.Item(1)
        Unload frmVerifyPwd
        moTranHeader.RefundCashier = oRefCashierBO.CashierNumber
        Set oRefCashierBO = Nothing
    End If


    If (oTranHead.TransactionCode = TT_SALE) Then blnPayment = True
        
    'get Order to compare items on sales receipt are not items being collected from order
    Set oOrderLine = goDatabase.CreateBusinessObject(CLASSID_CUSTORDERLINE)
    Call oOrderLine.IBo_AddLoadFilter(CMP_EQUAL, FID_CUSTORDERLINE_OrderNumber, oQuoteHead.QuoteNumber)
    Set colOrderLines = oOrderLine.IBo_LoadMatches
    'Build up collection of Items on Order with total quantities
    ReDim arOrderItems(0)
    For Each oOrderLine In colOrderLines
        strPartCode = oOrderLine.PartCode
        If Not SkuIsForDelivery(strPartCode) Then
            lngOrderQty = oOrderLine.UnitsOrdered
            strQuantity = ""
            For lngItemNo = LBound(arOrderItems) To UBound(arOrderItems) Step 1
                If Left$(arOrderItems(lngItemNo), 6) = strPartCode Then
                    strQuantity = Val(Mid$(arOrderItems(lngItemNo), 7))
                    Exit For
                End If
            Next lngItemNo
            If strQuantity = "" Then
                strQuantity = strPartCode & lngOrderQty
                ReDim Preserve arOrderItems(UBound(arOrderItems) + 1)
                lngItemNo = UBound(arOrderItems)
            Else
                strQuantity = strPartCode & (lngOrderQty + Val(strQuantity))
            End If
            arOrderItems(lngItemNo) = strQuantity
        End If
    Next

    'Flag each items as not prompted first
    For lngItemNo = LBound(arOrderItems) To UBound(arOrderItems) Step 1
        arOrderItems(lngItemNo) = arOrderItems(lngItemNo) & "N"
    Next lngItemNo
            
    'Step through Sale Lines and check if items have been added onto Sales Slip so they can be removed from the Order
    If (blnPayment = True) Then
        For Each oTranLine In colTranLines
            With oTranLine
                If Not SkuIsForDelivery(.PartCode) Then
                    strPartCode = .PartCode
                    lngOrderQty = .QuantitySold
                    strQuantity = ""
                        
                    'Locate if Order item has been manually entered as well
                    For lngRow = LBound(arSaleItems) To UBound(arSaleItems) Step 1
                        If Left$(arSaleItems(lngRow), 6) = strPartCode Then
                            strQuantity = Val(Mid$(arSaleItems(lngRow), 7))
                            Exit For
                        End If
                    Next lngRow
                    
                    'if SKU on Sale and Order and still items to collect
                    If (strQuantity <> "") Then
                        
                        lngLineQty = Val(strQuantity)
                        
                        'Locate total quantity on Sales Order and ensure not prompted already
                        For lngItemNo = LBound(arOrderItems) To UBound(arOrderItems) Step 1
                            If Left$(arOrderItems(lngItemNo), 6) = strPartCode Then
                                strQuantity = Mid$(arOrderItems(lngItemNo), 7)
                                Exit For
                            End If
                        Next lngItemNo
                        
                        If Right$(strQuantity, 1) = "N" Then 'not prompted before so check now
                            'Locate line on Sale to get item description
                            For lngRow = 2 To sprdLines.MaxRows Step 1
                                sprdLines.Col = COL_PARTCODE
                                sprdLines.Row = lngRow
                                If sprdLines.Text = strPartCode Then Exit For
                            Next lngRow
                            
                            lngOrderQty = Val(Left$(strQuantity, Len(strQuantity) - 1))
                        
                            intResponse = MsgBoxEx(strPartCode & " - " & GetGridData(sprdLines, COL_DESC, lngRow) & vbNewLine & vbNewLine & _
                                                    "Transaction contains " & lngLineQty & ", " & vbNewLine & _
                                                    "Customer Order contains " & lngOrderQty & ", " & vbNewLine & _
                                                    "Is the " & lngOrderQty & " in addition to those already sold?", _
                                                    vbYesNo + vbQuestion, "Customer Orders", , , , , _
                                                    RGBMSGBox_PromptColour)
                            If intResponse = vbNo Then
                                'increase quantity taken to remove from sales order
                                For Each oAllocLine In colOrderLines
                                    If (strPartCode = oAllocLine.PartCode) And (oAllocLine.UnitsOrdered > oAllocLine.UnitsTaken) Then
                                        'check if Line Qty to allocate > O/S qty of order line
                                        If (lngLineQty > (oAllocLine.UnitsOrdered - oAllocLine.UnitsTaken)) Then
                                            lngLineQty = lngLineQty - (oAllocLine.UnitsOrdered - oAllocLine.UnitsTaken)
                                            oAllocLine.UnitsTaken = oAllocLine.UnitsOrdered
                                        Else
                                            oAllocLine.UnitsTaken = oAllocLine.UnitsTaken + lngLineQty
                                            lngLineQty = 0
                                        End If
    '                                        If .QuantitySold > .UnitsOrdered Then .UnitsTaken = .UnitsOrdered
                                        oAllocLine.SaveIfExists
                                        blnLineAdded = True 'ensure Order is not treated as paid for/complete
                                        If (lngLineQty <= 0) Then Exit For
                                    End If 'Part Code matched
                                Next
                            End If
                            'Flag SKU as prompted so that it is not checked again
                            arOrderItems(lngItemNo) = Left$(arOrderItems(lngItemNo), Len(arOrderItems(lngItemNo)) - 1) & "Y"
                        End If 'not prompted already
                    End If 'item on Sale and on Order and still active
                End If 'Delivery SKU
            End With
        Next 'line to check if item is being collected
    End If 'Check lines already on Sale are part of Order being Collected
    
    lngTotTaken = 0
    lngRow = 0 'track Order Line to DLLINE to get unit price
    For Each oTranLine In colTranLines
        With oTranLine
            lngRow = lngRow + 1
            If (blnPayment = True) Then
                Set oOrderLine = colOrderLines(lngRow)
                lngTotTaken = lngTotTaken + oOrderLine.UnitsTaken 'to update Order Header
                lngOrderQty = .QuantitySold - oOrderLine.UnitsTaken
            Else
                lngOrderQty = .QuantitySold
            End If
            
            If lngOrderQty <> 0 Then
                mdblLineQuantity = lngOrderQty
                
                mlngCurrentMode = encmTranType
                sprdLines.MaxRows = sprdLines.MaxRows + 1
                sprdLines.Row = sprdLines.MaxRows
                sprdLines.Col = 0
                sprdLines.Text = " " 'override auto column lettering
                sprdLines.Col = COL_LINETYPE
                sprdLines.Text = LT_ITEM
                Call GetItem(.PartCode, False)
                
                'if item has been rejected for any reason then tidy up
                sprdLines.Row = sprdLines.MaxRows
                sprdLines.Col = COL_PARTCODE
                If sprdLines.Text = "" Then
                    sprdLines.MaxRows = sprdLines.MaxRows - 1
                Else
                    sprdLines.Col = COL_QTY
                    sprdLines.Text = lngOrderQty
                    sprdLines.Col = COL_SELLPRICE
                    If (blnPayment = True) Then
                        sprdLines.Text = .ActualSellPrice
                    Else
                        sprdLines.Text = .ActualSellPrice - .DealGroupMarginAmount - .QtyBreakMarginAmount - .HierarchyMarginAmount - .MultiBuyMarginAmount
                    End If
                    
                    'Added Refereral 2007-017 - flag Markdown Item from order onto Transaction
                    sprdLines.Col = COL_MARKDOWN
                    sprdLines.Text = IIf(.MarkedDownStock, "True", "False")
                    sprdLines.Col = COL_DISCCODE
                    sprdLines.Text = .PriceOverrideReason
                    sprdLines.Col = COL_DISCMARGIN
                    sprdLines.Text = .OverrideMarginCode
                                        
                    sprdLines.Col = COL_ORDERINFO
                    If (blnPayment = True) Then sprdLines.Text = oOrderLine.OrderNumber & vbTab & oOrderLine.OrderLine
                    
                    Call SetGridTotals(sprdLines.MaxRows)
                    Call UpdateTotals
                    blnLineAdded = True
                End If
                
            End If
        End With
    Next
    
    If (blnPayment = True) Then mlngQuoteQtyTaken = lngTotTaken

    If (Not blnLineAdded) Then
'        Call MsgBoxEx("No lines added from order", vbOKOnly, "Customer Orders", , , , , RGBMsgBox_WarnColour)
        Call MsgBoxEx("Order already Paid For", vbOKOnly, "Customer Orders", , , , , RGBMsgBox_WarnColour)
        Exit Function
    End If
    
    'Copy customer name and address details from Order to Sales Transactions
    Set oCustomer = goDatabase.CreateBusinessObject(CLASSID_RETURNCUST)
    Call oCustomer.AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionDate, oQuoteHead.TranDate)
    Call oCustomer.AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_PCTillID, oQuoteHead.TillID)
    Call oCustomer.AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionNumber, oQuoteHead.TransactionNumber)
    Set colCust = oCustomer.LoadMatches
    If colCust.Count > 0 Then
        Set oCustomer = colCust.Item(1)
        With oCustomer
            mstrAddress = .AddressLine1 & vbNewLine & .AddressLine2 & vbNewLine & .AddressLine3
            mstrName = .CustomerName
            mstrPostCode = .PostCode
            mstrTelNo = .PhoneNo
            mstrMobileNo = .MobileNo
            mstrWorkTelNo = .WorkTelNumber
            mstrEmail = .Email
        End With
    Else
        mstrAddress = vbNewLine & vbNewLine & vbNewLine & vbNewLine
        mstrName = vbNullString
        mstrPostCode = vbNullString
        mstrTelNo = vbNullString
        mstrMobileNo = vbNullString
        mstrWorkTelNo = vbNullString
        mstrEmail = vbNullString
    End If
    
    oQuoteHead.IsSold = True
    oQuoteHead.SaveIfExists
    
    Call EnableAction(enacPark, True)
    'Store Pending Transaction Details so that it can be flagged as done, when saving
    mdteQuoteTranDate = oQuoteHead.TranDate
    mstrQuoteTillID = oQuoteHead.TillID
    mstrQuoteTranNo = oQuoteHead.TransactionNumber
    
    mdblLineQuantity = 1
    
    AddOrderToTransaction = True

    Exit Function
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("AddOrderToTransaction", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Function

Private Function AddQuoteToTransaction(strQuoteNumber As String, ByRef blnExpired As Boolean) As Boolean

Dim colDiscountDesc As New Collection

Dim oOrderHead  As cCustOrderHeader
Dim oTranLine   As cPOSLine
Dim colTranLines As Collection
Dim colQuoteLines As Collection
Dim oTranHead   As cPOSHeader
Dim oQuoteHead  As cQuoteHeader
Dim oQuoteLine  As cCustOrderLine
Dim colCust     As Collection
Dim oCustomer   As cReturnCust
Dim oPOSLine    As cPOSLine
Dim oItemBO     As cInventory

Dim oEventBO    As cLineEvent

Dim lngLastRow  As Long
Dim lngRow      As Long
Dim lngOrderQty As Long
Dim lngLineQty  As Long
Dim intResponse As Integer
Dim lngTotTaken As Long
Dim blnLineAdded As Boolean

Dim strQuantity   As String
Dim strPartCode   As String
Dim lngItemNo     As Long
Dim oAllocLine    As cCustOrderLine
Dim oCouponMaster As cOPEvents_Wickes.cCouponMaster

Dim lngEventNo  As Long
Dim lngLineNo   As Long

Dim arSaleItems() As String
Dim arOrderItems() As String

    On Error GoTo HandleException
    
    Call colDiscountDesc.Add("* Project Saving    ", "DG")
    Call colDiscountDesc.Add("* Spend Level Saving", "HS")
    Call colDiscountDesc.Add("* Multibuy Saving   ", "MS")
    Call colDiscountDesc.Add("* Bulk Saving       ", "QS")
    Call colDiscountDesc.Add("* Temporary Markdown", "TM")
    Call colDiscountDesc.Add("* Promotional Saving", "TS")
    
    'Build up collection of Items on Sale/Collection with total quantities
    ReDim arSaleItems(0)
    For lngRow = 2 To lngLastRow
        sprdLines.Row = lngRow
        sprdLines.Col = COL_VOIDED
        If (Val(sprdLines.Value) = 0) Then
            sprdLines.Col = COL_QTY
            If (Val(sprdLines.Value) > 0) Then
                sprdLines.Col = COL_PARTCODE
                strPartCode = sprdLines.Text
                strQuantity = ""
                For lngItemNo = LBound(arSaleItems) To UBound(arSaleItems) Step 1
                    If Left$(arSaleItems(lngItemNo), 6) = strPartCode Then
                        strQuantity = Val(Mid$(arSaleItems(lngItemNo), 7))
                        Exit For
                    End If
                Next lngItemNo
                sprdLines.Col = COL_QTY
                If strQuantity = "" Then
                    strQuantity = strPartCode & sprdLines.Text
                    ReDim Preserve arSaleItems(UBound(arSaleItems) + 1)
                    lngItemNo = UBound(arSaleItems)
                Else
                    strQuantity = strPartCode & (Val(sprdLines.Text) + Val(strQuantity))
                End If
                arSaleItems(lngItemNo) = strQuantity
            End If 'non refund item
        End If 'line has not been reversed
    Next lngRow
    
    lngLastRow = sprdLines.MaxRows
    
    'Get Quote from DB, ready to process
    Set oQuoteHead = goDatabase.CreateBusinessObject(CLASSID_QUOTEHEADER)
    Call oQuoteHead.AddLoadFilter(CMP_EQUAL, FID_QUOTEHEADER_QuoteNumber, strQuoteNumber)
    If oQuoteHead.LoadMatches.Count = 0 Then
        Call MsgBoxEx("Quote NOT Found", vbExclamation, "Recall Quote", , , , , RGBMsgBox_WarnColour)
        Exit Function
    End If
    
    blnExpired = (Date > oQuoteHead.ExpiryDate)
    'moved 10/10/05 - required to get items to process and prices as per quote, not current price
    Set oTranHead = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    Call oTranHead.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, oQuoteHead.TranDate)
    Call oTranHead.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, oQuoteHead.TillID)
    Call oTranHead.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, oQuoteHead.TransactionNumber)
    Call oTranHead.AddLoadField(FID_POSHEADER_TranParked)
    Call oTranHead.AddLoadField(FID_POSHEADER_RecoveredFromParked)
    Call oTranHead.AddLoadField(FID_POSHEADER_TransactionCode)
    Call oTranHead.AddLoadField(FID_POSHEADER_DiscountCardNo)
    Call oTranHead.AddLoadField(FID_POSHEADER_StoreNumber)
    Call oTranHead.AddLoadField(FID_POSHEADER_SupervisorNo)
    Call oTranHead.AddLoadField(FID_POSHEADER_SupervisorUsed)
    Call oTranHead.LoadMatches
    
    mstrDiscCardNo = oTranHead.DiscountCardNumber
    mstrLocation = oTranHead.StoreNumber
    Set moDiscountCardScheme = GetCardScheme(mstrDiscCardNo)
    
    If (oTranHead.Lines.Count > 0) Then 'force Lines to be loaded
    End If
        
    moTranHeader.SupervisorNo = oTranHead.SupervisorNo
    moTranHeader.SupervisorUsed = oTranHead.SupervisorUsed
    
    'Build up collection of Items on Order with total quantities
    ReDim arOrderItems(0)
    For Each oTranLine In oTranHead.Lines
        strPartCode = oTranLine.PartCode
        If (Not SkuIsForDelivery(strPartCode)) And (oTranLine.SaleType <> "W") And (oTranLine.LineReversed = False) Then
            lngOrderQty = oTranLine.QuantitySold
            strQuantity = ""
            For lngItemNo = LBound(arOrderItems) To UBound(arOrderItems) Step 1
                If Left$(arOrderItems(lngItemNo), 6) = strPartCode Then
                    strQuantity = Val(Mid$(arOrderItems(lngItemNo), 7))
                    Exit For
                End If
            Next lngItemNo
            If strQuantity = "" Then
                strQuantity = strPartCode & lngOrderQty
                ReDim Preserve arOrderItems(UBound(arOrderItems) + 1)
                lngItemNo = UBound(arOrderItems)
            Else
                strQuantity = strPartCode & (lngOrderQty + Val(strQuantity))
            End If
            arOrderItems(lngItemNo) = strQuantity
        End If
    Next

    'Flag each items as not prompted first
    For lngItemNo = LBound(arOrderItems) To UBound(arOrderItems) Step 1
        arOrderItems(lngItemNo) = arOrderItems(lngItemNo) & "N"
    Next lngItemNo
            
    'Step through Sale Lines and check if items have been added onto Sales Slip so they can be removed from the Order
    For Each oTranLine In oTranHead.Lines
        With oTranLine
            If (.SaleType <> "W") And (.LineReversed = False) Then
                lngRow = lngRow + 1
                mdblLineQuantity = .QuantitySold
                
                mlngCurrentMode = encmRecord
                sprdLines.MaxRows = sprdLines.MaxRows + 1
                sprdLines.Row = sprdLines.MaxRows
                sprdLines.Col = 0
                sprdLines.Text = " " 'override auto column lettering
                sprdLines.Col = COL_LINETYPE
                sprdLines.Text = LT_ITEM
                Call GetItem(.PartCode, False)
                
                'if item has been rejected for any reason then tidy up
                sprdLines.Row = sprdLines.MaxRows
                sprdLines.Col = COL_PARTCODE
                If sprdLines.Text = "" Then
                    sprdLines.MaxRows = sprdLines.MaxRows - 1
                Else
                    sprdLines.Col = COL_DB_LINENO
                    sprdLines.Text = .SequenceNo
                    sprdLines.Col = COL_QTY
                    sprdLines.Text = .QuantitySold
                    sprdLines.Col = COL_SELLPRICE
    '                sprdLines.Text = .ActualSellPrice - ((.DealGroupMarginAmount + .QtyBreakMarginAmount + .HierarchyMarginAmount + .MultiBuyMarginAmount) / .QuantitySold)
    '                sprdLines.Text = .ActualSellPrice + .WEEELineCharge + .EmpSalePrimaryMarginAmount
                    sprdLines.Text = Round((.ExtendedValue + ((.WEEELineCharge + .EmpSalePrimaryMarginAmount) * .QuantitySold)) / .QuantitySold, 2)
                    
                    If GetBooleanParameter(-1046, goDatabase.Connection) Then
                        Call moEvents.UpdateSKUPrice(.PartCode, .SequenceNo + 1, Val(sprdLines.Text))
                    End If
                    
                    If Not EnableBuyCoupons Then
                        sprdLines.Col = COL_OP_EVENTSEQNO
                        sprdLines.Value = .LastEventSequenceNo
                        sprdLines.Col = COL_QTYBRKAMOUNT
                        sprdLines.Value = .QtyBreakMarginAmount
                        sprdLines.Col = COL_QTYBRKCODE
                        sprdLines.Value = .QtyBreakMarginCode
                        sprdLines.Col = COL_DGRPAMOUNT
                        sprdLines.Value = .DealGroupMarginAmount
                        sprdLines.Col = COL_DGRPCODE
                        sprdLines.Value = .DealGroupMarginCode
                        sprdLines.Col = COL_MBUYAMOUNT
                        sprdLines.Value = .MultiBuyMarginAmount
                        sprdLines.Col = COL_MBUYCODE
                        sprdLines.Value = .MultiBuyMarginCode
                        sprdLines.Col = COL_HIERAMOUNT
                        sprdLines.Value = .HierarchyMarginAmount
                        sprdLines.Col = COL_HIERCODE
                        sprdLines.Value = .HierarchyMarginCode
                    End If
                    sprdLines.Col = COL_WEEE_RATE
                    sprdLines.Value = .WEEELineCharge
                    sprdLines.Col = COL_DISCCODE
                    sprdLines.Text = .PriceOverrideReason
                    sprdLines.Col = COL_DISCMARGIN
                    sprdLines.Text = .OverrideMarginCode
                    sprdLines.Col = COL_TEMPAMOUNT
                    sprdLines.Value = .TempPriceMarginAmount
                    sprdLines.Col = COL_TEMPCODE
                    sprdLines.Text = .TempPriceMarginCode
                    sprdLines.Col = COL_SUPERVISOR
                    sprdLines.Value = .SupervisorNumber
                    Call SetGridTotals(sprdLines.MaxRows)
                    Call UpdateTotals
                    blnLineAdded = True
                End If
            End If
        End With
    Next
    
    sprdTotal.Row = ROW_TOTTOTALS
    sprdTotal.Col = COL_TOTVAL
    mdblTotalBeforeDis = Val(sprdTotal.Value)
    'Only re-add discounts if coupons is not require - PO14-01 requirement switch
    If EnableBuyCoupons = False Then
        'if expired then events must not be added, as they must be recalculated
        If (blnExpired = False) Then
            Dim oEvents As cEventTranLine
            Dim colEventsTotals As Collection
            
            Set oEvents = goDatabase.CreateBusinessObject(CLASSID_POSEVENTLINE)
            Call oEvents.AddLoadFilter(CMP_EQUAL, FID_POSEVENTLINE_TranDate, oQuoteHead.TranDate)
            Call oEvents.AddLoadFilter(CMP_EQUAL, FID_POSEVENTLINE_TillID, oQuoteHead.TillID)
            Call oEvents.AddLoadFilter(CMP_EQUAL, FID_POSEVENTLINE_TransactionNo, oQuoteHead.TransactionNumber)
            Set colEventsTotals = oEvents.LoadMatches
            
            For lngEventNo = colEventsTotals.Count To 1 Step -1
                For lngLineNo = 2 To sprdLines.MaxRows Step 1
                    sprdLines.Row = lngLineNo
                    sprdLines.Col = COL_DB_LINENO
                    Set oEvents = colEventsTotals(lngEventNo)
                    If (Val(sprdLines.Text) = oEvents.TransactionLineNo) Then
                        sprdLines.MaxRows = sprdLines.MaxRows + 1
                        Call sprdLines.InsertRows(sprdLines.Row + 1, 1)
                        sprdLines.Row = sprdLines.Row + 1
                        sprdLines.Col = 0
                        sprdLines.Text = " " 'override auto column lettering
                        sprdLines.Col = COL_DESC
                        sprdLines.Text = colDiscountDesc(oEvents.EventType)
                        sprdLines.Col = COL_LINETYPE
                        sprdLines.Text = LT_EVENT
                        sprdLines.Col = COL_OP_EVENTSEQNO
                        sprdLines.Text = oEvents.EventSeqNo
                        sprdLines.Col = COL_OP_SUBEVENTNO
                        sprdLines.Text = oEvents.EventSeqNo
                        sprdLines.Col = COL_INCTOTAL
                        sprdLines.Text = oEvents.DiscountAmount * -1
                        sprdLines.Col = COL_OP_TYPE
                        sprdLines.Text = oEvents.EventType
                        Exit For
                    End If
                Next lngLineNo
            Next lngEventNo
            Call SetGridTotals(sprdLines.MaxRows)
            Call UpdateTotals
        End If
    
        'get all coupons that were used and given as rewards
        If (blnExpired = False) Then
            Dim oSoldCoupon As cSoldCoupon
            Dim colSoldCoupons As Collection
            
            Set oSoldCoupon = goDatabase.CreateBusinessObject(CLASSID_SOLDCOUPON)
            Call oSoldCoupon.AddLoadFilter(CMP_EQUAL, FID_SOLDCOUPON_TransactionDate, oQuoteHead.TranDate)
            Call oSoldCoupon.AddLoadFilter(CMP_EQUAL, FID_SOLDCOUPON_TransactionTill, oQuoteHead.TillID)
            Call oSoldCoupon.AddLoadFilter(CMP_EQUAL, FID_SOLDCOUPON_TransactionNumber, oQuoteHead.TransactionNumber)
            Set colSoldCoupons = oSoldCoupon.LoadMatches
            
            For lngEventNo = colSoldCoupons.Count To 1 Step -1
                Set oSoldCoupon = colSoldCoupons(lngEventNo)
                If (oSoldCoupon.Status = "6") Or (oSoldCoupon.Status = "9") Then
                    Call RetrieveCoupon(oSoldCoupon.CouponId & oSoldCoupon.MarketRef & oSoldCoupon.SerialNo, oSoldCoupon.Status = "9")
                End If ' Coupon was used or a reward coupon
            Next lngEventNo
        End If
    End If
    
    'Copy customer name and address details from Order to Sales Transactions
    Set oCustomer = goDatabase.CreateBusinessObject(CLASSID_RETURNCUST)
    Call oCustomer.AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionDate, oQuoteHead.TranDate)
    Call oCustomer.AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_PCTillID, oQuoteHead.TillID)
    Call oCustomer.AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionNumber, oQuoteHead.TransactionNumber)
    Set colCust = oCustomer.LoadMatches
    If colCust.Count > 0 Then
        Set oCustomer = colCust.Item(1)
        With oCustomer
            mstrAddress = .AddressLine1 & vbNewLine & .AddressLine2 & vbNewLine & .AddressLine3
            mstrName = .CustomerName
            mstrPostCode = .PostCode
            mstrTelNo = .PhoneNo
            mstrMobileNo = .MobileNo
            mstrWorkTelNo = .WorkTelNumber
            mstrEmail = .Email
        End With
    Else
        mstrAddress = vbNewLine & vbNewLine & vbNewLine & vbNewLine
        mstrName = vbNullString
        mstrPostCode = vbNullString
        mstrTelNo = vbNullString
        mstrMobileNo = vbNullString
        mstrWorkTelNo = vbNullString
        mstrEmail = vbNullString
    End If
    
'    oQuoteHead.IsSold = True
'    oQuoteHead.SaveIfExists
    
    Call EnableAction(enacPark, True)
    'Store Pending Transaction Details so that it can be flagged as done, when saving
    mdteQuoteTranDate = oQuoteHead.TranDate
    mstrQuoteTillID = oQuoteHead.TillID
    mstrQuoteTranNo = oQuoteHead.TransactionNumber
    
    mdblLineQuantity = 1
    
    If (sprdLines.MaxRows > 1) Then AddQuoteToTransaction = True

    Exit Function
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("AddQuoteToTransaction", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Function

Private Sub BackOutOrder()
    
Dim oQuoteHead  As cQuoteHeader
Dim oOrderLine  As cCustOrderLine
Dim oOrderHead  As cCustOrderHeader
Dim colOrdLines As Collection

Dim lngLine     As Long
Dim lngLineQty  As Long
Dim lngOrderQty As Long
Dim vntOrdInfo  As Variant

    On Error GoTo HandleException
    
    'If Voiding a Sale/Payment - then reflag Quote as not sold yet
    Set oOrderHead = goDatabase.CreateBusinessObject(CLASSID_CUSTORDERHEADER)
    Call oOrderHead.AddLoadFilter(CMP_EQUAL, FID_CUSTORDERHEADER_OrderNumber, moTranHeader.OrderNumber)
    Call oOrderHead.LoadMatches
    If (Val(oOrderHead.RefundTransactionNumber) = 0) Then
        Set oQuoteHead = goDatabase.CreateBusinessObject(CLASSID_QUOTEHEADER)
        With oQuoteHead
            Call .AddLoadFilter(CMP_EQUAL, FID_QUOTEHEADER_QuoteNumber, moTranHeader.OrderNumber)
            Call .LoadMatches
            .IsSold = False
            .SaveIfExists
        End With
    
        'get Order Lines to rollback any taken quantities
        Set oOrderLine = goDatabase.CreateBusinessObject(CLASSID_CUSTORDERLINE)
        Call oOrderLine.AddLoadFilter(CMP_EQUAL, FID_CUSTORDERLINE_OrderNumber, lblOrderNo.Caption)
        Call oOrderLine.AddLoadFilter(CMP_GREATERTHAN, FID_CUSTORDERLINE_UnitsTaken, 0)
        Set colOrdLines = oOrderLine.LoadMatches
        For Each oOrderLine In colOrdLines
            If Not SkuIsForDelivery(oOrderLine.PartCode) Then
                oOrderLine.UnitsTaken = 0
                Call oOrderLine.SaveIfExists
            End If
        Next
        
        If colOrdLines.Count > 0 Then
            oOrderHead.UnitsTaken = GetWetherTakenNow(oOrderLine.PartCode)
            Call oOrderHead.SaveIfExists
        End If
    End If
    
    Set oOrderHead = Nothing

    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("BackOutOrder", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Sub

Private Sub LoadCurrencies()

Dim cCurrBO As cCurrencies

    Set cCurrBO = goDatabase.CreateBusinessObject(CLASSID_CURRENCY)
    Call cCurrBO.AddLoadFilter(CMP_LESSEQUALTHAN, FID_CURRENCY_CurrencyActiveDate, Date)
    Call cCurrBO.AddLoadFilter(CMP_GREATERTHAN, FID_CURRENCY_CurrencyInactiveDate, Date)
    Call cCurrBO.AddLoadFilter(CMP_EQUAL, FID_CURRENCY_Deleted, False)
    Set mcolCurrencies = cCurrBO.LoadMatches

    For Each cCurrBO In mcolCurrencies
        If (cCurrBO.SystemDefaultCurrency = True) Then
            Set mcDefaultCurr = cCurrBO
            Exit For
        End If
    Next
    If mcDefaultCurr Is Nothing Then Set mcDefaultCurr = goDatabase.CreateBusinessObject(CLASSID_CURRENCY)

End Sub

Private Function GetWEEERate(strPRFSKU As String) As Currency

Dim oWEEESKU As cInventory
Dim oWEEESKUW As cInventoryWickes

    GetWEEERate = 0
        
    If (Val(strPRFSKU) = 0) Then Exit Function
    
    If (goSession.GetParameter(PRM_USE_WEEE_RATES) = False) Then Exit Function

    On Error Resume Next
    Set oWEEESKU = mcolWEEESKUs(strPRFSKU)
    Set oWEEESKUW = mcolWEEESKUs(strPRFSKU & "W")
    On Error GoTo 0
    
    On Error GoTo BadWEEERate
    If ((oWEEESKU Is Nothing) = True) Then
        Set oWEEESKU = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
        Call oWEEESKU.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, strPRFSKU)
        Call oWEEESKU.AddLoadField(FID_INVENTORY_PartCode)
        Call oWEEESKU.AddLoadField(FID_INVENTORY_NormalSellPrice)
        Call oWEEESKU.AddLoadField(FID_INVENTORY_VATRate)
        If (oWEEESKU.LoadMatches.Count = 0) Then
            Call DebugMsg(MODULE_NAME, "GetWEEECost", endlDebug, "")
            Call MsgBoxEx("SKU has an invalid Product Recycling Rate set-up" & vbNewLine & "Contact Head Office", vbInformation, "Invalid Set-Up", , , , , RGBMsgBox_WarnColour)
            Exit Function
        End If
        Call mcolWEEESKUs.Add(oWEEESKU, strPRFSKU)
        Set oWEEESKUW = goDatabase.CreateBusinessObject(CLASSID_INVENTORYWICKES)
        Call oWEEESKUW.AddLoadFilter(CMP_EQUAL, FID_INVENTORYWICKES_PartCode, strPRFSKU)
        Call oWEEESKUW.LoadMatches
        Call mcolWEEESKUs.Add(oWEEESKUW, strPRFSKU & "W")
    End If
    
    GetWEEERate = oWEEESKU.NormalSellPrice
    Exit Function
    
BadWEEERate:
    
    Call Err.Raise(Err.Number, Err.Source, Err.Description)

End Function

Private Sub StretchScrollBars(ByVal blnStretched As Boolean)

    If (mblnShowKeypadIcon = False) Then Exit Sub
    If (blnStretched = True) Then
        sprdLines.ScrollBarVColor = vbRed
        sprdLines.ScrollBarWidth = 50
    Else
        sprdLines.ScrollBarVColor = -2147483637
        sprdLines.ScrollBarWidth = -1
    End If
    
End Sub

Private Function GetBrokenBarcodeCode() As String

Dim oBCCodes    As cBarcodeBroken

    If mcolBarcodeFailedCodes Is Nothing Then
        Set oBCCodes = goDatabase.CreateBusinessObject(CLASSID_BARCODEFAILED)
        Set mcolBarcodeFailedCodes = oBCCodes.LoadMatches
    End If
    For Each oBCCodes In mcolBarcodeFailedCodes
        If oBCCodes.ReaderBroken = True Then
            GetBrokenBarcodeCode = oBCCodes.Code
            Exit For
        End If
    Next

End Function


Private Sub SaveItemPromptRejects()

Dim oItemRejectBO   As cSale_Wickes.cSKUReject

    On Error GoTo Error_SaveReject
    
    For Each oItemRejectBO In mcolPromptRejects
        Call oItemRejectBO.SaveIfNew
    Next
    Set mcolPromptRejects = New Collection
    
    Exit Sub
    
Error_SaveReject:

    Call Err.Report(MODULE_NAME, "SaveItemPromptRejects", 1, True, "Save Rejects Error", "System has detected error when saving Item Prompt Rejects.  System will continue.")
    Call Err.Clear

End Sub

Private Sub DiscountScheme()

Dim strLocation As String
Dim strCardNo   As String
Dim lngPreviousMode As Long
Dim curWEEETotal    As Currency
Dim curWEEERate     As Currency
Dim lngRowNo        As Currency

'    sprdTotal.Row = ROW_TOTTOTALS
'    sprdTotal.Col = COL_TOTVAL
    
'    If (sprdLines.MaxRows = 1) Or ((Val(sprdTotal.Value) - RefundTotal) = 0) Then
'        If (txtSKU.Visible = True) Then Call txtSKU.SetFocus
'        If (uctntPrice.Visible = True) Then Call uctntPrice.SetFocus
'        Exit Sub
'    End If
    lngPreviousMode = mlngCurrentMode
    
    mlngCurrentMode = encmDiscountCardScheme
    
    If (Left(lblOrderNo.Caption, 1) = "Q") Then
        lblOrderNo.Caption = ""
        Call ClearEvents
        mdblTotalBeforeDis = 0
    End If
    
    Load frmDiscountCardScheme          'MO'C Added 07/06/07
    ' Commidea Into WSS
    ' Commidea PEDs cannot read cards, so switch off Swipe button
    If TillUsingCommideaPED Then
        Call frmDiscountCardScheme.SetForCommidea(InitializeCommideaConfiguration())
    End If
    ' End of Commidea Into WSS
    
    For lngRowNo = 1 To sprdLines.MaxRows Step 1
        sprdLines.Row = lngRowNo
        sprdLines.Col = COL_WEEE_RATE
        curWEEERate = Val(sprdLines.Value)
        sprdLines.Col = COL_QTY
        curWEEETotal = curWEEETotal + (Val(sprdLines.Value) * curWEEERate)
    Next lngRowNo
    
    sprdTotal.Col = COL_TOTVAL
    Set moDiscountCardScheme = frmDiscountCardScheme.ValidateDiscount(ucKeyPad1.Visible, moTranHeader, _
                                    Val(sprdTotal.Value) - curWEEETotal, moRetopt, strLocation, strCardNo)
    
    If moDiscountCardScheme Is Nothing Then
        mstrDiscCardNo = vbNullString
        mstrLocation = vbNullString
    Else
        mstrDiscCardNo = strCardNo
        mstrLocation = strLocation
    End If
    
    'Now validate the user card
    Unload frmDiscountCardScheme        'MO'C Added 07/06/07
    
    mlngCurrentMode = lngPreviousMode   'Put back to whatever mode it was at the start.
    
'    Call RecordTender
     If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then Call txtSKU.SetFocus
     If (uctntPrice.Visible = True) And (uctntPrice.Enabled = True) Then Call uctntPrice.SetFocus

End Sub

Private Function DiscountTotal(dblDiscountRate As Double) As Currency

Dim lngLineNo       As Long
Dim curTotal        As Currency
Dim curLineTotal    As Currency
Dim curQty          As Currency

    curTotal = 0
    For lngLineNo = 2 To sprdLines.MaxRows
        sprdLines.Row = lngLineNo
        sprdLines.Col = COL_VOIDED
        
        If Val(sprdLines.Value) = 0 Then ' exclude reversal lines
            sprdLines.Col = COL_QTY
            curQty = Val(sprdLines.Value)
            sprdLines.Col = COL_RFND_LINENO
            If (curQty < 0) And (Val(sprdLines.Value) = 0) Then curQty = Abs(curQty) 'include Refunded Items if validated
            If (curQty > 0) Then 'Sales item or Non Validated Refund Item
                sprdLines.Col = COL_VOUCHER
                If (sprdLines.Value = "") Then 'exclude Vouchers
                    sprdLines.Col = COL_SALETYPE
                    If (sprdLines.Value <> "D") And (sprdLines.Value <> "C") Then 'exclude Delivery Charges
                        sprdLines.Col = COL_INCTOTAL
                        curLineTotal = Val(sprdLines.Value)
                        sprdLines.Col = COL_QTYBRKAMOUNT
                        curLineTotal = curLineTotal - Val(sprdLines.Value)
                        sprdLines.Col = COL_DGRPAMOUNT
                        curLineTotal = curLineTotal - Val(sprdLines.Value)
                        sprdLines.Col = COL_MBUYAMOUNT
                        curLineTotal = curLineTotal - Val(sprdLines.Value)
                        sprdLines.Col = COL_HIERAMOUNT
                        curLineTotal = curLineTotal - Val(sprdLines.Value)
                        'Added 13/3/08 - WIX1312 - for Coll discount - remove WEEE charge
                        sprdLines.Col = COL_WEEE_RATE
                        curLineTotal = curLineTotal - (Val(sprdLines.Value) * curQty)
                        sprdLines.Col = COL_DISCCODE
                        If (Val(sprdLines.Value) = 7) Then
                            sprdLines.Row = sprdLines.Row + 1
                            sprdLines.Col = COL_INCTOTAL
                            curLineTotal = curLineTotal + Val(sprdLines.Value)
                            lngLineNo = lngLineNo + 1
                        End If
                        'For Unit rounding take of discount and round to unit price
                        curLineTotal = Format((curLineTotal * dblDiscountRate / 100) / curQty, "0.00")
                        curTotal = curTotal + Format(curLineTotal * curQty, "0.00")
                    End If 'Delivery Charge
                End If 'Voucher
            End If 'Line not a refund
        End If 'Line not reversed
    Next lngLineNo
    
    'pass out total discount based per line total
    DiscountTotal = curTotal * -1

End Function

Private Function IsDeliveryChargeCharitySKU(strSKU As String) As Boolean

Dim oItemWixBO As cInventoryWickes

    IsDeliveryChargeCharitySKU = False

    Set oItemWixBO = goDatabase.CreateBusinessObject(CLASSID_INVENTORYWICKES)
    Call oItemWixBO.AddLoadFilter(CMP_EQUAL, FID_INVENTORYWICKES_PartCode, strSKU)
    Call oItemWixBO.AddLoadField(FID_INVENTORYWICKES_SaleTypeAttribute)
    Call oItemWixBO.LoadMatches
    If (oItemWixBO.SaleTypeAttribute = "D") Or (oItemWixBO.SaleTypeAttribute = "C") Then
        IsDeliveryChargeCharitySKU = True
        Exit Function
    End If
    
    Set oItemWixBO = Nothing
    
End Function 'IsDeliveryCharge

Private Sub ClearEvents()

Dim lngLineNo As Long
Dim strProcedureName As String

strProcedureName = "ClearEvents"
    'Remove any OP Event lines
    Call DebugMsgSprdLinesValue(strProcedureName)
    For lngLineNo = sprdLines.MaxRows To 2 Step -1
        sprdLines.Row = lngLineNo
        sprdLines.Col = COL_LINETYPE
        If EnableBuyCoupons Then
            If (Val(sprdLines.Text) = LT_EVENT) Or (Val(sprdLines.Text) = LT_SPACER) _
                Or (Val(sprdLines.Text) = LT_TOTAL) Then
                Call DebugMsgSprdLineValue(lngLineNo, strProcedureName & "(Remove)")
                Call sprdLines.DeleteRows(lngLineNo, 1)
                sprdLines.MaxRows = sprdLines.MaxRows - 1
            End If
        Else
            If ((Val(sprdLines.Text) = LT_EVENT) And (Left$(lblOrderNo.Caption, 1) <> "Q")) Or (Val(sprdLines.Text) = LT_SPACER) _
                Or (Val(sprdLines.Text) = LT_TOTAL) Then
                Call DebugMsgSprdLineValue(lngLineNo, strProcedureName & "(Remove)")
                Call sprdLines.DeleteRows(lngLineNo, 1)
                sprdLines.MaxRows = sprdLines.MaxRows - 1
            End If
        End If
    Next lngLineNo
    
    If EnableBuyCoupons Then
        If mblnQuoteRecalledOrderCreated Then
            mblnQuoteRecalledOrderCreated = False
        End If
    End If
    
    'Remove any Store Generated Reward Coupons
    Call DebugMsgSprdLinesValue(strProcedureName)
    For lngLineNo = sprdLines.MaxRows To 2 Step -1
        sprdLines.Row = lngLineNo
        sprdLines.Col = COL_LINETYPE
        If Val(sprdLines.Text) = LT_COUPON Then
            sprdLines.Col = COL_CPN_STATUS
            If (sprdLines.Value) = "9" Then
                Call DebugMsgSprdLineValue(lngLineNo, strProcedureName & "(Remove)")
                Call sprdLines.DeleteRows(lngLineNo, 1)
                sprdLines.MaxRows = sprdLines.MaxRows - 1
            End If
        End If
    Next lngLineNo
    If EnableBuyCoupons Then
        sprdLines.Row = -1
        sprdLines.Col = COL_OP_EVENTSEQNO
        sprdLines.Text = vbNullString
        sprdLines.Col = COL_QTYBRKAMOUNT
        sprdLines.Text = 0
        sprdLines.Col = COL_QTYBRKCODE
        sprdLines.Text = vbNullString
        sprdLines.Col = COL_DGRPAMOUNT
        sprdLines.Text = 0
        sprdLines.Col = COL_DGRPCODE
        sprdLines.Text = vbNullString
        sprdLines.Col = COL_MBUYAMOUNT
        sprdLines.Text = 0
        sprdLines.Col = COL_MBUYCODE
        sprdLines.Text = vbNullString
        sprdLines.Col = COL_HIERAMOUNT
        sprdLines.Text = 0
        sprdLines.Col = COL_HIERCODE
        sprdLines.Text = vbNullString
    Else
        'Reset any events marked against the lines
        If (Left$(lblOrderNo.Caption, 1) <> "Q") Then
            sprdLines.Row = -1
            sprdLines.Col = COL_OP_EVENTSEQNO
            sprdLines.Text = vbNullString
            sprdLines.Col = COL_QTYBRKAMOUNT
            sprdLines.Text = 0
            sprdLines.Col = COL_QTYBRKCODE
            sprdLines.Text = vbNullString
            sprdLines.Col = COL_DGRPAMOUNT
            sprdLines.Text = 0
            sprdLines.Col = COL_DGRPCODE
            sprdLines.Text = vbNullString
            sprdLines.Col = COL_MBUYAMOUNT
            sprdLines.Text = 0
            sprdLines.Col = COL_MBUYCODE
            sprdLines.Text = vbNullString
            sprdLines.Col = COL_HIERAMOUNT
            sprdLines.Text = 0
            sprdLines.Col = COL_HIERCODE
            sprdLines.Text = vbNullString
        End If
    End If
    Call SetGridTotals(sprdLines.MaxRows)
    Call UpdateTotals

End Sub

Private Sub DisplayMarkdownMessage(oMarkDown As cMarkDownStock)

Dim strMsg As String
Dim intIndex As Integer

    strMsg = goSession.GetParameter(965002) + vbCrLf
    strMsg = strMsg + goSession.GetParameter(965006) + vbCrLf
    For intIndex = 0 To 9
        strMsg = Replace(strMsg, "{" & CStr(intIndex) & "}", oMarkDown.ReductionWeek(intIndex + 1) & "%")
    Next intIndex
    strMsg = strMsg + vbCrLf + goSession.GetParameter(965007) + vbCrLf
    
    Call MsgBoxEx(strMsg, vbExclamation + vbOKOnly, "Markdown Stock", , , , , _
         RGBMsgBox_WarnColour)

'    If frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) = False Then
'        sprdLines.Text = vbNullString
'        txtSKU.Text = vbNullString
'        txtSKU.SetFocus
'        GetItem = False
'        Exit Function
'    End If

End Sub

'MO'C WIX1345 - Added to do a code 54 Stock Adjustment
Private Sub StockAdjustMent54(ByVal strPartCode As String, ByVal intQtySold As Integer)

Dim oItem       As cInventory
Dim oLog        As cStockLog
Dim oAdjust     As cStockAdjustment
    
    'Get the latest information on the item
    Set oItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
    Call oItem.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, strPartCode)
    Call oItem.AddLoadField(FID_INVENTORY_PartCode)
    Call oItem.AddLoadField(FID_INVENTORY_Description)
    Call oItem.AddLoadField(FID_INVENTORY_SupplierNo)
    Call oItem.AddLoadField(FID_INVENTORY_ItemObsolete)
    Call oItem.AddLoadField(FID_INVENTORY_SupplierPackSize)
    Call oItem.AddLoadField(FID_INVENTORY_CostPrice)
    Call oItem.AddLoadField(FID_INVENTORY_QuantityAtHand)
    Call oItem.AddLoadField(FID_INVENTORY_LastOrdered)
    Call oItem.AddLoadField(FID_INVENTORY_NormalSellPrice)
    Call oItem.AddLoadField(FID_INVENTORY_WriteOffQuantity)
    Call oItem.AddLoadField(FID_INVENTORY_UnitsInOpenReturns)
    Call oItem.AddLoadField(FID_INVENTORY_MarkDownQuantity)
    Call oItem.LoadMatches

    'SAVE AWAY THE STOCK LOG VALUES
    Set oLog = goDatabase.CreateBusinessObject(CLASSID_STOCKLOG)
    oLog.DayNumber = DateDiff("d", CDate("01/01/1900"), Now()) + 1
    oLog.LogDate = Format(Now(), "yyyy-mm-dd")
    oLog.LogTime = Format(Now(), "HH:MM:SS")
    oLog.CommedToHO = False
    oLog.MovementType = "68" 'Positive Write Off
    oLog.TransactionKey = CStr(moTranHeader.TranDate) & " 54 " & oItem.PartCode & " 00"
    oLog.EmployeeID = Format(moTranHeader.CashierID, "000")
    oLog.PartCode = oItem.PartCode
    oLog.StartStockQty = oItem.QuantityAtHand
    oLog.EndStockQty = oItem.QuantityAtHand - intQtySold
    oLog.StartOpenReturnQty = oItem.UnitsInOpenReturns
    oLog.EndOpenReturnQty = oItem.UnitsInOpenReturns
    oLog.StartMarkDownSOH = oItem.MarkDownQuantity
    oLog.EndMarkDownSOH = oItem.MarkDownQuantity
    oLog.StartWriteOffSOH = oItem.WriteOffQuantity
    oLog.EndWriteOffSOH = oItem.WriteOffQuantity + intQtySold
    oLog.StartPrice = oItem.NormalSellPrice
    oLog.EndPrice = oItem.NormalSellPrice
    oLog.SaveCustomerComplaint

    'SAVE AWAY THE STOCK Adjustment VALUES
    Set oAdjust = goDatabase.CreateBusinessObject(CLASSID_STOCKADJUST)
    oAdjust.PartCode = oItem.PartCode
    oAdjust.AdjustmentDate = Format(Now(), "yyyy-mm-dd")
    oAdjust.AdjustmentCode = "54"
    oAdjust.SequenceNo = "00"
    oAdjust.CommNo = False
    oAdjust.AdjustmentBy = moTranHeader.CashierID
    oAdjust.OpeningQuantity = oItem.QuantityAtHand
    oAdjust.AdjustmentQuantity = intQtySold
    oAdjust.Price = oItem.NormalSellPrice
    oAdjust.Cost = oItem.CostPrice
    oAdjust.AdustmentType = "N"
    oAdjust.Comment = "Customer Complaint"
    Call oAdjust.SaveAdjustment


End Sub

Private Function FindAction(colTempActions As Collection, varActionOrder As Variant, lngButtonIndex As Long) As Boolean

Dim intLoop As Integer
Dim cActionCode As clsActionCodes

    For intLoop = 1 To colTempActions.Count
        If colTempActions.Item(intLoop).ActionId = varActionOrder Then
            mcolActionBtns.Add (colTempActions.Item(intLoop))
            colTempActions.Item(intLoop).ActionId = "Used" 'MO'C WIX1376 - Mark it as used so its not repeated
            Exit Function
        End If
    Next intLoop
    
    'MO'C WIX1376 - Not Found so assume blank key
    Set cActionCode = New clsActionCodes
    cActionCode.ButtonDesc = "Blank"
    cActionCode.ActionCode = enacBlank + lngButtonIndex
    cActionCode.HotKeyCode = ""
    cActionCode.ActionId = CStr(lngButtonIndex)
    Call mcolActionBtns.Add(cActionCode, Str$(cActionCode.ActionCode))
    
End Function

Private Function GetKeysFromParameters() As Variant

    Dim strList1 As String
    Dim strList2 As String
    Dim intNumberCommas As Integer
    Dim intPos As Integer
    
    'MO'C WIX1376 - Make sure we have enough / not too many buttons
    strList1 = goSession.GetParameter(PRM_ACTIONS_LIST1)
    strList2 = goSession.GetParameter(PRM_ACTIONS_LIST2)
    Call CountCommas(strList1)
    Call CountCommas(strList2)
    GetKeysFromParameters = Split((strList1 & strList2), ",")
  
  End Function

Private Function CountCommas(ByRef strParam As String) As Integer

Dim intPos As Integer
Dim intCount As Integer
Dim strTemp As String
    
    'MO'C WIX1376 - Needed to make sure that there are only the number of buttons per screen as set
    '               by the BTN_PER_ROW value
    strTemp = strParam
    intCount = 0
    For intPos = 1 To Len(strParam)
        If Mid(strParam, intPos, 1) = "," Then
            intCount = intCount + 1
        End If
    Next intPos
    
    'MO'C WIX1376 - Does it match our our rows per screen
    If intCount <> BTN_PER_ROW Then
        If intCount < BTN_PER_ROW Then
            'Add extra ,
            For intPos = intCount To BTN_PER_ROW - 1
                strParam = strParam & ","
            Next intPos
        Else
            'Only include BTNS_PER_ROW Maximum
            intCount = 0
            strParam = ""
            For intPos = 1 To Len(strParam)
                If intCount < BTN_PER_ROW Then
                    strParam = strParam & Mid(strTemp, intPos, 1)
                End If
                If Mid(strParam, intPos, 1) = "," Then
                    intCount = intCount + 1
                End If
            Next intPos
        End If
    End If
    
    CountCommas = intCount
    
End Function

Private Sub CreateVoucherAdjustment()

Dim oPOSPmt      As cPOSPayment
Dim oPOSLine     As cPOSLine
Dim oNewLine     As cPOSLine
Dim lngLineNo    As Long
Dim dblAmount    As Double
Dim strTranCode  As String
Dim lngTotalPos  As Long
Dim lngFirstPmt  As Long
Dim oTranHeader  As New cSale_Wickes.cPOSHeader
Dim oPaidIn      As New cSale_Wickes.cPOSHeader
    
    On Error GoTo HandleException
    
    If Not NextTransactionOK Then
        Load frmVerifyPwd
        Do
            Call MsgBoxEx("Duplicate transaction detected" & vbNewLine & vbNewLine & _
                          "Contact CTS Support Desk", vbOKOnly + vbExclamation, _
                          "Duplicate Transaction", , , , , RGBMsgBox_WarnColour)
        Loop While frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) = False
        Unload frmVerifyPwd
        Unload Me
        End
    End If
   
    Set oTranHeader = goSession.Database.CreateBusinessObject(CLASSID_POSHEADER)
    oTranHeader.TillID = moTranHeader.TillID
    oTranHeader.CashierID = moTranHeader.CashierID
    oTranHeader.CustomerAcctCardNo = moTranHeader.CustomerAcctCardNo
    oTranHeader.CustomerAcctNo = moTranHeader.CustomerAcctNo
    oTranHeader.ColleagueCardNo = moTranHeader.ColleagueCardNo
    oTranHeader.DiscountCardNumber = moTranHeader.DiscountCardNumber
    oTranHeader.Offline = moTranHeader.Offline
    oTranHeader.OrderNumber = moTranHeader.OrderNumber
    oTranHeader.StoreNumber = moTranHeader.StoreNumber
    oTranHeader.TranDate = moTranHeader.TranDate
    oTranHeader.TransactionCode = "RF"
    oTranHeader.TransactionTime = moTranHeader.TransactionTime
    For lngLineNo = 1 To 9 Step 1
        oTranHeader.VATRates(lngLineNo) = moTranHeader.VATRates(lngLineNo)
        oTranHeader.VATSymbol(lngLineNo) = moTranHeader.VATSymbol(lngLineNo)
    Next lngLineNo
        
    'step through each line and copy onto Duplicate Transaction
    For Each oPOSLine In moTranHeader.Lines
        If (oPOSLine.LineReversed = False) And (oPOSLine.PartCode = mstrGiftCardSKU) Then
            Set oNewLine = oTranHeader.AddItem(oPOSLine.PartCode)
            oNewLine.QuantitySold = oPOSLine.QuantitySold * -1
            oNewLine.ActualPriceExVAT = oPOSLine.ActualPriceExVAT * -1
            oNewLine.ActualSellPrice = oPOSLine.ActualSellPrice
            oNewLine.DealGroupMarginCode = oPOSLine.DealGroupMarginAmount
            oNewLine.DealGroupMarginCode = oPOSLine.DealGroupMarginCode
            oNewLine.DepartmentNumber = oPOSLine.DepartmentNumber
            oNewLine.EmpSalePrimaryMarginAmount = oPOSLine.EmpSalePrimaryMarginAmount
            oNewLine.EmpSalePrimaryMarginCode = oPOSLine.EmpSalePrimaryMarginCode
            oNewLine.EmpSaleSecondaryMarginAmount = oPOSLine.EmpSaleSecondaryMarginAmount
            oNewLine.ExtendedCost = oPOSLine.ExtendedCost * -1
            oNewLine.ExtendedValue = oPOSLine.ExtendedValue * -1
            oNewLine.HierarchyMarginAmount = oPOSLine.HierarchyMarginAmount
            oNewLine.HierarchyMarginCode = oPOSLine.HierarchyMarginCode
            oNewLine.HierGroup = oPOSLine.HierGroup
            oNewLine.HierCategory = oPOSLine.HierCategory
            oNewLine.HierStyle = oPOSLine.HierStyle
            oNewLine.HierSubGroup = oPOSLine.HierSubGroup
            oNewLine.LastEventSequenceNo = oPOSLine.LastEventSequenceNo
            oNewLine.LookUpPrice = oPOSLine.LookUpPrice
            oNewLine.MultiBuyMarginAmount = oPOSLine.MultiBuyMarginAmount
            oNewLine.MultiBuyMarginCode = oPOSLine.MultiBuyMarginCode
            oNewLine.OverrideMarginCode = oPOSLine.OverrideMarginCode
            oNewLine.PriceOverrideAmount = oPOSLine.PriceOverrideAmount
            oNewLine.PriceOverrideReason = oPOSLine.PriceOverrideReason
            oNewLine.QtyBreakMarginAmount = oPOSLine.QtyBreakMarginAmount
            oNewLine.QtyBreakMarginCode = oPOSLine.QtyBreakMarginCode
            oNewLine.SaleType = oPOSLine.SaleType
            oNewLine.SequenceNo = oPOSLine.SequenceNo
            oNewLine.VATAmount = oPOSLine.VATAmount * -1
            oNewLine.VATCode = oPOSLine.VATCode
            oNewLine.VATCodeNumber = oPOSLine.VATCodeNumber
            oTranHeader.TaxAmount = oTranHeader.TaxAmount + oNewLine.VATAmount
            oTranHeader.ExVATValue(oPOSLine.VATCodeNumber) = oTranHeader.ExVATValue(oPOSLine.VATCodeNumber) - (oPOSLine.ExtendedValue - oPOSLine.VATAmount)
            oTranHeader.VATValue(oPOSLine.VATCodeNumber) = oTranHeader.VATValue(oPOSLine.VATCodeNumber) - oPOSLine.VATAmount
            oTranHeader.TotalSaleAmount = oTranHeader.TotalSaleAmount + oNewLine.ExtendedValue
        End If
    Next
    oTranHeader.MerchandiseAmount = oTranHeader.TotalSaleAmount
    If (Abs(oTranHeader.TotalSaleAmount) > 9999999) Then oTranHeader.TotalSaleAmount = 0
    If (Abs(oTranHeader.NonMerchandiseAmount) > 9999999) Then oTranHeader.NonMerchandiseAmount = 0
    
    Set oPOSPmt = goSession.Database.CreateBusinessObject(CLASSID_POSPAYMENT)
    oPOSPmt.tenderType = TEN_CASH
    Call oTranHeader.AddPayment(oPOSPmt.tenderType, oTranHeader.TotalSaleAmount * -1)

    oTranHeader.TransactionComplete = True
    oTranHeader.SaveIfNew
    lblTranNo.Caption = oTranHeader.TransactionNo
    Call BackUpTransaction
    'Added 22/12/09 to call CashBalUpdate after each Transaction
    If (goSession.ISession_IsOnline = True) Then
        Call Shell(App.Path & "\CashierBalancingUpdate.exe T" & Format(Date, "YYYYMMDD") & oTranHeader.TillID & oTranHeader.TransactionNo, vbHide)
    End If

    Set oPaidIn = goSession.Database.CreateBusinessObject(CLASSID_POSHEADER)
    oPaidIn.TillID = oTranHeader.TillID
    oPaidIn.CashierID = oTranHeader.CashierID
    oPaidIn.CustomerAcctCardNo = oTranHeader.CustomerAcctCardNo
    oPaidIn.CustomerAcctNo = oTranHeader.CustomerAcctNo
    oPaidIn.Offline = oTranHeader.Offline
    oPaidIn.StoreNumber = oTranHeader.StoreNumber
    oPaidIn.TranDate = oTranHeader.TranDate
    oPaidIn.TransactionCode = IIf(oTranHeader.TransactionCode = "SA", "M-", "M+")
    oPaidIn.TransactionTime = oTranHeader.TransactionTime
    oPaidIn.ReasonCode = "12" 'New Reason Code for GiftCARD
    oPaidIn.Description = "GIFT CARD"
    For lngLineNo = 1 To 9 Step 1
        oPaidIn.VATRates(lngLineNo) = oTranHeader.VATRates(lngLineNo)
        oPaidIn.VATSymbol(lngLineNo) = oTranHeader.VATSymbol(lngLineNo)
    Next lngLineNo
    
    oPaidIn.NonMerchandiseAmount = oTranHeader.TotalSaleAmount * -1
    oPaidIn.TotalSaleAmount = oTranHeader.TotalSaleAmount * -1
    Call oPaidIn.AddPayment(oPOSPmt.tenderType, oTranHeader.TotalSaleAmount)
    
    oPaidIn.TransactionComplete = True
    oPaidIn.SaveIfNew
    lblTranNo.Caption = oPaidIn.TransactionNo
    Call BackUpTransaction
    If (goSession.ISession_IsOnline = True) Then
        Call Shell(App.Path & "\CashierBalancingUpdate.exe T" & Format(Date, "YYYYMMDD") & oPaidIn.TillID & oPaidIn.TransactionNo, vbHide)
    End If

    mstrTranId = oPaidIn.TransactionNo
    lblTranNo.Caption = moTranHeader.TransactionNo
          
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("CreateVoucherAdjustment", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
        
End Sub

Private Function GetWetherTakenNow(strPartCode As String) As Long

Dim lngItemNo As Long
Dim lngRow As Integer
    
    GetWetherTakenNow = 0
    For lngItemNo = 2 To sprdLines.MaxRows Step 1
         sprdLines.Row = lngItemNo
         sprdLines.Col = COL_VOIDED
         If (Val(sprdLines.Value) = 0) Then
             sprdLines.Col = COL_SALETYPE
             If (sprdLines.Value <> "D") Then
                 sprdLines.Col = COL_QTY
                 If (Val(sprdLines.Value) > 0) Then 'ensure sold item
                     'Find Line in the Take Now Column
                     sprdLines.Col = COL_PARTCODE
                     If sprdLines.Text = strPartCode Then 'Matching entry
                        sprdLines.Col = COL_TAKE_NOW
                        GetWetherTakenNow = Val(sprdLines.Text)
                        Exit For
                    End If
                 End If
             End If
         End If
    Next lngItemNo
End Function


Private Function GetExtendedPriceFromSpreadSheet(Row As Long) As String

Dim SPRI As Currency
Dim SellPrice As Currency
Dim TempAmount As Currency
Dim QUAN As Integer
Dim POPD As Currency
Dim TPPD As Currency
Dim DGPD As Currency
Dim MBPD As Currency
Dim QBPD As Currency
Dim HSPD As Currency
Dim ESPD As Currency

    'Get the values required from the till line
    sprdLines.Row = Row
    sprdLines.Col = COL_LOOKEDUP
    SPRI = Val(sprdLines.Value)
    sprdLines.Col = COL_QTY
    QUAN = Val(sprdLines.Value)
    sprdLines.Col = COL_SELLPRICE
    SellPrice = Val(sprdLines.Value)
    sprdLines.Col = COL_TEMPAMOUNT
    TPPD = Val(sprdLines.Value)
    POPD = (SPRI - SellPrice) - TPPD
    sprdLines.Col = COL_DGRPAMOUNT
    DGPD = Val(sprdLines.Value)
    sprdLines.Col = COL_MBUYAMOUNT
    MBPD = Val(sprdLines.Value)
    sprdLines.Col = COL_QTYBRKAMOUNT
    QBPD = Val(sprdLines.Value)
    sprdLines.Col = COL_HIERAMOUNT
    HSPD = Val(sprdLines.Value)

    'Calculate Colleague Discount - If Required
    If mblnColleagueDiscount Then
        ESPD = ((((SPRI - POPD - TPPD) * QUAN) - DGPD - MBPD - QBPD - HSPD) * moDiscountCardScheme.DiscountRate) / 100
    Else
        ESPD = 0
    End If
    
    'Calculate Extended Price
    GetExtendedPriceFromSpreadSheet = (((SPRI - POPD - TPPD) * QUAN - DGPD - MBPD - QBPD - HSPD) - ESPD) / QUAN
End Function



Private Function ProcessEndOfTranPrompts(FromQuote As Boolean) As Boolean
    
    Dim oEOTGroup As cGroupPrompts
    
    Dim dblTotalVolume As Double
    Dim intQty       As Integer
    Dim oSKUGroups   As cItemGrpPrompts
    Dim lngPromptNo  As Long
    Dim lngEntryMode As Long
    Dim lngLineNo    As Long
    Dim colSKUs      As New Collection
    
    sprdLines.Row = ROW_TOTALS
    sprdLines.Col = COL_TOTALVOL
    dblTotalVolume = Val(sprdLines.Value)
'    sprdLines.Col = COL_GRP_PROMPT_QTY
'    intQty = Val(sprdLines.Text)
    lngEntryMode = mlngEntryMode
    
    'Initialise any Generic Till Prompts if not done already
    If (mcolEOTPrompts Is Nothing) Then
        Set oSKUGroups = goDatabase.CreateBusinessObject(CLASSID_ITEM_GRPPROMPT)
        Set mcolEOTPrompts = oSKUGroups.GetGenericPromptGroups
        Set oSKUGroups = Nothing
    End If
    
    If (mcolEOTPrompts.Count > 0) Then
       'step through each line and save
        Call DebugMsg(MODULE_NAME, "ProcessEndOfTranPrompts", endlDebug, "Build Missing SKUs List")
        
        For lngLineNo = 2 To sprdLines.MaxRows Step 1
            sprdLines.Row = lngLineNo
            'Ensure only lines that are SKU's
            sprdLines.Col = COL_LINETYPE
            Select Case (Val(sprdLines.Value))
                Case (LT_ITEM):
                    sprdLines.Col = COL_VOIDED
                    If (Val(sprdLines.Value) = 0) Then
                        sprdLines.Col = COL_QTY
                        If (Val(sprdLines.Value) >= 0) Then
                            sprdLines.Col = COL_PARTCODE
                            colSKUs.Add ("" & sprdLines.Text)
                        Else
                            FromQuote = True 'flag as Quote/Refund to skip missing sku checks
                        End If 'non refund item
                    End If 'Not a reversed line
            End Select
        Next lngLineNo
    End If

    For Each oSKUGroups In mcolEOTPrompts
        Set oEOTGroup = goDatabase.CreateBusinessObject(CLASSID_GROUP_PROMPT)
        If (oEOTGroup.GetPromptsForGroupID(oSKUGroups.PromptGroupID) = True) Then
            mlngEntryMode = enemNone
            frmItemPrompts.strUserId = txtUserID.Text
            If (frmItemPrompts.ProcessPrompts(Nothing, Nothing, oEOTGroup, mcolLinePrints, _
                     mcolTrailerPrints, mcolValues, mlngHighestAgePassed, mlngLowestAgeFailed, mcolPromptRejects, ucKeyPad1.Visible, dblTotalVolume, intQty, colSKUs, FromQuote) = False) Then
                Set oEOTGroup = Nothing
                Set oSKUGroups = Nothing
                Unload frmItemPrompts
                ProcessEndOfTranPrompts = False
                mlngEntryMode = lngEntryMode
                Exit Function
            End If
            Unload frmItemPrompts
        End If 'no prompts so just carry on
        Set oEOTGroup = Nothing
    Next
    mlngEntryMode = lngEntryMode
    ProcessEndOfTranPrompts = True

End Function

Private Function GetPromptGroupQty() As Double
Dim lngItemNo As Long
Dim lngRow As Integer
    
    GetPromptGroupQty = 0
    For lngItemNo = 2 To sprdLines.MaxRows Step 1
         sprdLines.Row = lngItemNo
         sprdLines.Col = COL_VOIDED
         If (Val(sprdLines.Value) = 0) Then
             sprdLines.Col = COL_SALETYPE
             If (sprdLines.Value <> "D") Then
                 sprdLines.Col = COL_GRP_PROMPT_QTY
                 If (Val(sprdLines.Value) > 0) Then 'ensure sold item
                     'Find Line in the Take Now Column
                     GetPromptGroupQty = GetPromptGroupQty + Val(sprdLines.Value)
                 End If
             End If
         End If
    Next lngItemNo

End Function

Private Function AreThereAnyRefundLines() As Boolean
    
Dim lngRowNo As Long

    AreThereAnyRefundLines = False
    For lngRowNo = sprdLines.MaxRows To 2 Step -1
        sprdLines.Row = lngRowNo
        sprdLines.Col = COL_QTY
        If Val(sprdLines.Text) < 0 Then
            sprdLines.Col = COL_LINE_REV_CODE
            If sprdLines.Text = "" Then
                AreThereAnyRefundLines = True
                Exit Function
            End If
        End If
    Next lngRowNo

End Function


' Commidea Into WSS
' Switch off the Commidea PED.
' Need to log on to PED (if not logged on) as also
' fire an instruction to process any offline
' transactions before closing
Private Sub ClosePED(ByVal UserID As String, ByVal UserPIN As String)
    Dim strPrm_Use_EFTPOS As String
    Dim oCommidea As WSSCommidea.cCommidea
    Dim timedOut As Boolean
    Dim RestartRequired As Boolean  ' Closing the PED anyway, so can ignore this result

    On Error GoTo HandleException
    
    Call DebugMsg(MODULE_NAME, "ClosePED", endlTraceIn)
    ' If using Ocius Sentinel PED....
    If TillUsingCommideaPED Then
        Call DebugMsg(MODULE_NAME, "ClosePED", endlDebug, "Creating Commidea class")
        Set oCommidea = New WSSCommidea.cCommidea
            ' Use modular variable for event handling
            Set moCommidea = oCommidea
            Call DebugMsg(MODULE_NAME, "ClosePED", endlDebug, "Initialising Commidea class")
                If mblnOciusSoftwareReady Then
                    ' Use parameters used to initialise PED at start up
                    Dim commideaCfg As CommideaConfiguration
                    Set commideaCfg = InitializeCommideaConfiguration()
                    Call oCommidea.Initialise(commideaCfg)
            
                    Call DebugMsg(MODULE_NAME, "ClosePED", endlDebug, "Commidea PED " & IIf(mblnOciusPEDLoggedIn, " not ", " ") & "logged into already")
                    ' Check we are logged in - if not, probably not have done any eft transactions!
                    If mblnOciusPEDLoggedIn Then
                        ' First try and send any offline transactions
                        Call DebugMsg(MODULE_NAME, "ClosePED", endlDebug, "Requesting PED to submit any offline transactions")
                        ' Referral 818 - pass in 'software ready' byref parameter, so if licence problem can flag up the issue
                        Call oCommidea.ProcessOfflineTransactions(UserID, UserPIN, timedOut, RestartRequired, mblnOciusSoftwareReady)
                    End If
                End If
                ' Now close the Ocius Sentinel app
                Call DebugMsg(MODULE_NAME, "ClosePED", endlDebug, "Closing PED")
        Call oCommidea.OciusSentinelClose(timedOut, mblnOciusPEDLoggedIn)
                If timedOut Then
                    Call DebugMsg(MODULE_NAME, "ClosePED", endlDebug, "PED closing timed out")
                    Call MsgBoxEx("Problem logging off PED e.g. not logged in to start with, ran out of time waiting for log out." & vbNewLine & "Make sure the PED is switched on and connected." & vbNewLine & "Ensure there is access to port " & mstrCommideaIntegrationPort & ".", vbOKOnly, "Failed logging out of Pin Entry Device", , , , , RGBMsgBox_WarnColour)
                End If
                ' Try to return focus to till
        Call oCommidea.ResetFocus(Me.Name)
            Set moCommidea = Nothing
            Set oCommidea = Nothing
        End If
    Call DebugMsg(MODULE_NAME, "ClosePED", endlTraceOut)
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("ClosePED", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

' Retrieve and store all the parameters for initialising
' the Commidea dll objects.  Use them to launch (if not
' already launched) the Ocius software.
' If not JustLaunch, then login as well.  Use JustLaunch to
' separate the launch from the login in time, to give the
' Ocius Sentinel/PED time to initialise
Private Function FireUpCommideaSoftwareAndPED(Optional ByVal JustLaunch As Boolean = False) As Boolean
    Dim oCommidea As WSSCommidea.cCommidea
    Dim LoginDetails() As String
    Dim timedOut As Boolean
    Dim RestartRequired As Boolean
    
    On Error GoTo HandleException
    
    Call DebugMsg(MODULE_NAME, "FireUpCommideaSoftwareAndPED", endlTraceIn)
    FireUpCommideaSoftwareAndPED = True
    Call DebugMsg(MODULE_NAME, "FireUpCommideaSoftwareAndPED", endlDebug, "Checking Ocius and PED readiness")
    If Not mblnOciusSoftwareReady Or Not mblnOciusPEDLoggedIn Then
        Call DebugMsg(MODULE_NAME, "FireUpCommideaSoftwareAndPED", endlDebug, "Ocius and PED ready")
        Call DebugMsg(MODULE_NAME, "FireUpCommideaSoftwareAndPED", endlDebug, "Creating Commidea class")
        Set oCommidea = New WSSCommidea.cCommidea
            ' Use modular variable for event handling (duress, etc)
            Set moCommidea = oCommidea
            Call DebugMsg(MODULE_NAME, "FireUpCommideaSoftwareAndPED", endlDebug, "Initialising Commidea class")
        
        Dim commideaCfg As CommideaConfiguration
        Set commideaCfg = InitializeCommideaConfiguration()
        Call oCommidea.Initialise(commideaCfg)
        
                ' Launch and login.  As when first login the PED checks for updates
                ' TODO - Alanl - Manage Ocius relaunch required situation
                Call DebugMsg(MODULE_NAME, "FireUpCommideaSoftwareAndPED", endlDebug, "Preparing Commidea launch commands from parameter id=" & PRM_COMMIDEA_OCIUS_LAUNCH_PARAMS)
                mstrOciusLaunchParams = goSession.GetParameter(PRM_COMMIDEA_OCIUS_LAUNCH_PARAMS)
                LoginDetails = Split(mstrCommideaLoginDetails, ",")
                If UBound(LoginDetails) > 0 Then
                    mstrCommideaUserID = LoginDetails(0)
                    mstrCommideaUserPIN = LoginDetails(1)
                Else
                    Call DebugMsg(MODULE_NAME, "FireUpCommideaSoftwareAndPED", endlDebug, "Using default Commidea launch commands")
                    ' Try the default
                    mstrCommideaUserID = "1234"
                    mstrCommideaUserPIN = "1234"
                End If
                If JustLaunch Then
                    Call DebugMsg(MODULE_NAME, "FireUpCommideaSoftwareAndPED", endlDebug, "Attempting to just launch Ocius Sentinel")
                    If oCommidea.LaunchOciusSentinel(mstrOciusLaunchParams) Then
                        mblnOciusSoftwareReady = True
                        mblnOciusPEDLoggedIn = False
                    End If
                Else
                    Call DebugMsg(MODULE_NAME, "FireUpCommideaSoftwareAndPED", endlDebug, "Attempting to launch Ocius Sentinel and log in to PED")
                    ' Referral 818 - pass in 'software ready' byref parameter, so if licence problem can flag up the issue
                    If LaunchOciusSentinelAndLogin(oCommidea, timedOut, RestartRequired) Then
                        mblnOciusSoftwareReady = True
                        mblnOciusPEDLoggedIn = True
                    Else
                        Call DebugMsg(MODULE_NAME, "FireUpCommideaSoftwareAndPED", endlDebug, "Failed to launch Ocius Sentinel and log in to PED")
                        If timedOut Then
                            Call DebugMsg(MODULE_NAME, "FireUpCommideaSoftwareAndPED", endlDebug, "Log in to PED timed out")
                            Call MsgBoxEx("Timed out logging into PED." & vbNewLine & "Make sure the PED is switched on and connected." & vbNewLine & "Ensure there is access to port " & mstrCommideaIntegrationPort & ".", vbOKOnly, "Failed accessing Pin Entry Device", , , , , RGBMsgBox_WarnColour)
                            ' Identify that PED is not available
                            mstrPTID = "PED login timeout"
                        ' Referral 818 - If calls to oCommidea methods flag up that the software is not ready, then inform operator, but do not cause till to close as can use for cash transactions etc
                        ElseIf Not mblnOciusSoftwareReady Then
                            Call DebugMsg(MODULE_NAME, "FireUpCommideaSoftwareAndPED", endlDebug, "Log in to PED failed - software issue")
                            Call MsgBoxEx("PED software not properly installed." & vbNewLine & "Make sure the PED software install has been completed and initialised correctly." & vbNewLine & "PED will not be available until this has been resolved.", vbOKOnly, "Failed accessing Pin Entry Device", , , , , RGBMsgBox_WarnColour)
                            ' CR0034
                            ' Allow new code not to be active yet
                            If ChangeRequestCR0034Active Then
                                ' Identify that PED is not available
                                mstrPTID = "PED Unavailable"
                            End If
                            ' End of CR0034
                        End If
                    End If
                    Call DebugMsg(MODULE_NAME, "FireUpCommideaSoftwareAndPED", endlDebug, "Ocius Sentinel/PED requested a restart")
                    FireUpCommideaSoftwareAndPED = Not RestartRequired
                End If
                ' Try to return focus to till
        Call oCommidea.ResetFocus(Me.Name)
            Set moCommidea = Nothing
            Set oCommidea = Nothing
        End If
    Call DebugMsg(MODULE_NAME, "FireUpCommideaSoftwareAndPED", endlTraceOut)
    
    Exit Function

HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("FireUpCommideaSoftwareAndPED", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Function

Private Function LaunchOciusSentinelAndLogin(ByVal oCommidea As WSSCommidea.cCommidea, _
                                             ByRef LoginTimedOut As Boolean, _
                                             ByRef RestartRequired As Boolean) As Boolean
    If moSysopt Is Nothing Then
        Set moSysopt = goDatabase.CreateBusinessObject(CLASSID_SYSTEMOPTIONS)
        Call moSysopt.LoadMatches
    End If
    
    LaunchOciusSentinelAndLogin = oCommidea.LaunchOciusSentinelAndLogin(mstrCommideaUserID, _
                                                                        mstrCommideaUserPIN, _
                                                                        mstrOciusLaunchParams, _
                                                                        LoginTimedOut, _
                                                                        RestartRequired, _
                                                                        mblnOciusSoftwareReady)
    If LaunchOciusSentinelAndLogin Then
            ' Make call to get the PTID used in support
            ' Referral 818 - pass in 'software ready' byref parameter, so if licence problem can flag up the issue
            If Not oCommidea.GetPTID(mstrPTID, _
                                     mstrCommideaUserID, _
                                     mstrCommideaUserPIN, _
                                     LoginTimedOut, _
                                     mblnOciusSoftwareReady, _
                                     RestartRequired) Then
                ' Best identify that have not got the PTID
                mstrPTID = "PTID Unknown"
            End If

        If Not oCommidea.GetSoftwareVersion(mstrOciusSentinelVersion, _
                                            mstrCommideaUserID, _
                                            mstrCommideaUserPIN, _
                                            LoginTimedOut, _
                                            mblnOciusSoftwareReady, _
                                            RestartRequired) Then
            Call DebugMsg(MODULE_NAME, "FireUpCommideaSoftwareAndPED", endlDebug, "Failed to get the Ocius Sentinel Version")
        End If
        
        Dim fileInfoPath As String
        fileInfoPath = moSysopt.TillLocalDrive & ":\" & moSysopt.TillPathName & "\Data\PED.txt"
        Dim fso As New FileSystemObject
        Dim ts As TextStream
        Set ts = fso.CreateTextFile(fileInfoPath, True)
        ts.WriteLine ("Version=" & mstrOciusSentinelVersion)
        ts.WriteLine ("PTID=" & mstrPTID)
        ts.Close
    End If
    
End Function

Private Sub GetCommideaParameters()

On Error Resume Next

    ' Get params to initalise with
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlTraceIn)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Commidea Ocius Path'")
    mstrOciusSentinelPath = goSession.GetParameter(PRM_COMMIDEA_OCIUS_PATH)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Commidea Ocius Path' - value = " & mstrOciusSentinelPath)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Commidea Integration Port'")
    mstrCommideaIntegrationPort = goSession.GetParameter(PRM_COMMIDEA_INTEGRATION_PORT)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Commidea Integration Port' - value = " & mstrCommideaIntegrationPort)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Commidea Progress Port'")
    mstrCommideaProgressPort = goSession.GetParameter(PRM_COMMIDEA_PROGRESS_PORT)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Commidea Progress Port' - value = " & mstrCommideaProgressPort)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Commidea Login Timeout'")
    mintOciusLoginTimeout = goSession.GetParameter(PRM_COMMIDEA_LOGIN_TIMEOUT)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Commidea Login Timeout' - value = " & CStr(mintOciusLoginTimeout))
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Commidea Connection Timout'")
    mintConnectionTimeout = goSession.GetParameter(PRM_COMMIDEA_CONN_TIMEOUT)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Commidea Connection Timeout' - value = " & CStr(mintConnectionTimeout))
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Commidea Customer Receipt File'")
    mstrCommideaCustRecFilename = goSession.GetParameter(PRM_COMMIDEA_CUSTOMER_RECEIPTFILE)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Commidea Customer Receipt File' - value = " & mstrCommideaCustRecFilename)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Commidea Customer Receipt Path'")
    mstrCommideaCustRecPath = goSession.GetParameter(PRM_COMMIDEA_CUSTOMER_RECEIPTPATH)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Commidea Customer Receipt Path' - value = " & mstrCommideaCustRecPath)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Commidea Merchant Receipt File'")
    mstrCommideaMerRecFilename = goSession.GetParameter(PRM_COMMIDEA_MERCHANT_RECEIPTFILE)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Commidea Merchant Receipt File' - value = " & mstrCommideaMerRecFilename)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Commidea Merchant Receipt Path'")
    mstrCommideaMerRecPath = goSession.GetParameter(PRM_COMMIDEA_MERCHANT_RECEIPTPATH)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Commidea Merchant Receipt Path' - value = " & mstrCommideaMerRecPath)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Commidea Launch Params'")
    mstrOciusLaunchParams = goSession.GetParameter(PRM_COMMIDEA_OCIUS_LAUNCH_PARAMS)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Commidea Launch Params' - value = " & mstrOciusLaunchParams)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Commidea Ocius Login Details'")
    mstrCommideaLoginDetails = goSession.GetParameter(PRM_COMMIDEA_OCIUS_LOGIN)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Commidea Ocius Login Details' - value = " & mstrCommideaLoginDetails)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Commidea Store Terminal IDs'")
    mblnCommideaStoreTerminalID = goSession.GetParameter(PRM_COMMIDEA_STORE_TERMINALID)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Commidea Store Terminal IDs' - value = " & IIf(mblnCommideaStoreTerminalID, "True", "False"))
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Commidea Receipt Header Last Line'")
    mstrCommideaReceiptHeaderLastLine = goSession.GetParameter(PRM_COMMIDEA_REC_HEAD_LAST_LINE)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Commidea Receipt Header Last Line' - value = " & mstrCommideaReceiptHeaderLastLine)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Commidea Receipt Footer First Line'")
    mstrCommideaReceiptFooterFirstLine = goSession.GetParameter(PRM_COMMIDEA_REC_FOOT_FIRST_LINE)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Commidea Receipt Footer First Line' - value = " & mstrCommideaReceiptFooterFirstLine)
    ' Referral 818.  New licence name parameter used to auto verify licence on an incomplete ocius sentinel install.
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Licence Merchant Name'")
    mstrCommideaLicenceName = goSession.GetParameter(PRM_COMMIDEA_LICENCE_NAME)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Licence Merchant Name' - value = " & mstrCommideaLicenceName)
    
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Commidea Print Confirm Timeout'")
    mintOciusPrintConfirmTimeout = goSession.GetParameter(PRM_COMMIDEA_PRINTCONFIRM_TIMEOUT)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Commidea Print Confirm Timeout' - value = " & CStr(mintOciusPrintConfirmTimeout))
    
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Partial Gift Card payment timeout'")
    mPartialGCPaymentTimeout = goSession.GetParameter(PRM_COMMIDEA_PARTIALGCPAYMENT_TIMEOUT)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Partial Gift Card payment timeout' - value = " & CStr(mPartialGCPaymentTimeout))
    
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Commidea Separate Receipt'")
    
    ' If cannot get parameter here, need to flag the fact, so jump flag setting if there is a problem
On Error GoTo CommideaTills
    mblnCommideaSeparateReceipt = goSession.GetParameter(PRM_COMMIDEA_SEPARATE_RECEIPT)
    mblnGot_Prm_Separate_Receipts = True
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Commidea Separate Receipt' - value = " & IIf(mblnCommideaSeparateReceipt, "True", "False"))
CommideaTills:
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Commidea Tills List'")
    mstrCommideaTills = goSession.GetParameter(PRM_COMMIDEA_TILLS)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Commidea Tills List' - value = " & mstrCommideaTills)
    
    ' If cannot get parameter here, need to flag the fact, so jump flag setting if there is a problem
On Error GoTo UseTokenIDs
    mblnCommideaModalAuthorisation = goSession.GetParameter(PRM_COMMIDEA_MODAL_AUTHORISATION)
    mblnGot_Prm_Modal_Authorisation = True
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Commidea Modal Authoristion' - value = " & IIf(mblnCommideaModalAuthorisation, "True", "False"))
    
UseTokenIDs:
    ' If cannot get parameter here, need to flag the fact, so jump flag setting if there is a problem
On Error GoTo NonCommideaSpecific
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Commidea Use Token IDs'")
    mblnUseCommideaTokenIDs = goSession.GetParameter(PRM_COMMIDEA_USE_TOKEN_IDS)
    mblnGot_Prm_Use_Commidea_TokenIDs = True
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Commidea Use Token IDs' - value = " & IIf(mblnUseCommideaTokenIDs, "True", "False"))

NonCommideaSpecific:
    If Not mblnGot_Prm_Use_Commidea_TokenIDs Then
        Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Did NOT get parameter 'Commidea Use Token IDs'")
    End If

    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting non Commimdea-specific parameters, still used by Commidea related code")
On Error Resume Next
    ' Not specific to Commidea, but need them
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Use EFT POS'")
    mstrPrm_Use_EFTPOS = goSession.GetParameter(PRM_USE_EFTPOS)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Use EFT POS' - value = " & mstrPrm_Use_EFTPOS)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Duress Key'")
    mintDuressKeyCode = goSession.GetParameter(PRM_KEY_DURESS)
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Duress Key' - value = " & Chr$(mintDuressKeyCode))
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Getting parameter 'Printer Currency Code'")
    mstrCurrChar = Chr(goSession.GetParameter(PRM_PRINTER_CURR_CODE))
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlDebug, "Got parameter 'Printer Currency Code' - value = " & mstrCurrChar)

On Error GoTo 0
    Call DebugMsg(MODULE_NAME, "GetCommideaParameters", endlTraceOut)
End Sub

' As is a boolean parameter need another boolean to indicate whether have got the
' 1st value in the first place. Cannot rely on False meaning False it could mean
' that have not got the parameter yet
Private Function MergingCommideaAndWickesReceipts() As Boolean

    Call DebugMsg(MODULE_NAME, "MergingCommideaAndWickesReceipts", endlTraceIn)
    Call DebugMsg(MODULE_NAME, "MergingCommideaAndWickesReceipts", endlDebug, "Checking if have 'Commidea Separate Receipt' parameter already")
    If mblnGot_Prm_Separate_Receipts Then
        Call DebugMsg(MODULE_NAME, "MergingCommideaAndWickesReceipts", endlDebug, "Have 'Commidea Separate Receipt' parameter already - value = " & IIf(mblnCommideaSeparateReceipt, "'True'", "'False'"))
        MergingCommideaAndWickesReceipts = TillUsingCommideaPED And Not mblnCommideaSeparateReceipt
    Else

On Error GoTo SkipFlagSetting
        
        Call DebugMsg(MODULE_NAME, "MergingCommideaAndWickesReceipts", endlDebug, "Getting 'Commidea Separate Receipt' parameter")
        mblnCommideaSeparateReceipt = goSession.GetParameter(PRM_COMMIDEA_SEPARATE_RECEIPT)
        mblnGot_Prm_Separate_Receipts = True
        Call DebugMsg(MODULE_NAME, "MergingCommideaAndWickesReceipts", endlDebug, "Now have 'Commidea Separate Receipt' parameter - value = " & IIf(mblnCommideaSeparateReceipt, "'True'", "'False'"))

SkipFlagSetting:
        
        If mblnGot_Prm_Separate_Receipts Then
            MergingCommideaAndWickesReceipts = TillUsingCommideaPED And Not mblnCommideaSeparateReceipt
        End If
    End If
    Call DebugMsg(MODULE_NAME, "MergingCommideaAndWickesReceipts", endlDebug, "Returning " & IIf(MergingCommideaAndWickesReceipts, "'True'", "'False'"))
End Function

Private Sub SaveUnknownCreditCardName(ByVal UnknownName As String, ByVal TranDate As Date, ByVal TranNo As String, ByVal SeqNo As Integer)
    Dim fsoConfigFile As FileSystemObject
    Dim txtstrUnknownCCNames As TextStream
    Dim ConfigFilePath As String
    Dim UnknownCCNamesFile As String
    Dim UnknownCCName() As String
    Dim NameCount As Integer

    Call DebugMsg(MODULE_NAME, "SaveUnknownCreditCardName", endlTraceIn)
    Call DebugMsg(MODULE_NAME, "SaveUnknownCreditCardName", endlDebug, "Attempting to write " & Chr(34) & UnknownName & "," & Format(TranDate, "dd/MM/yyyy hh:mm:ss") & "," & mstrTillNo & "," & TranNo & "," & CStr(SeqNo) & Chr(34) & " to unknown credit card names list file")
    If UnknownName <> "" And TillUsingCommideaPED Then
        Call DebugMsg(MODULE_NAME, "SaveUnknownCreditCardName", endlDebug, "Unknown name is " & UnknownName)
        Set fsoConfigFile = New FileSystemObject
    On Error GoTo TryF
        ConfigFilePath = goSession.GetParameter(PRM_GEN_CONFIG_FILE)
        Call DebugMsg(MODULE_NAME, "SaveUnknownCreditCardName", endlDebug, "Config file path is " & ConfigFilePath)
    On Error GoTo 0
        With fsoConfigFile
            If Not .FolderExists(ConfigFilePath) Then
    On Error GoTo TryC
                ConfigFilePath = goSession.GetParameter(PRM_TRANSNO)
    On Error GoTo 0
                If Not .FolderExists(ConfigFilePath) Then
                    ConfigFilePath = ""
                End If
            End If
            If ConfigFilePath <> "" Then
                Call DebugMsg(MODULE_NAME, "SaveUnknownCreditCardName", endlDebug, "Config file path " & ConfigFilePath & " exists")
    On Error GoTo TryUnknownNames
                UnknownCCNamesFile = goSession.GetParameter(PRM_COMMIDEA_UNKNOWN_CCNAMES_FILE)
    On Error GoTo 0
                UnknownCCNamesFile = Replace(ConfigFilePath & "\" & UnknownCCNamesFile, "\\", "\")
                Set txtstrUnknownCCNames = .OpenTextFile(UnknownCCNamesFile, ForAppending, True)
                If Not txtstrUnknownCCNames Is Nothing Then
                    With txtstrUnknownCCNames
                        Call .WriteLine(UnknownName & "," & Format(TranDate, "dd/MM/yyyy hh:mm:ss") & "," & mstrTillNo & "," & TranNo & "," & CStr(SeqNo))
                        .Close
                        Call DebugMsg(MODULE_NAME, "SaveUnknownCreditCardName", endlDebug, "Successfully written " & Chr(34) & UnknownName & "," & Format(TranDate, "dd/MM/yyyy hh:mm:ss") & "," & mstrTillNo & "," & TranNo & "," & CStr(SeqNo) & Chr(34) & " to  " & UnknownCCNamesFile)
                    End With
                    Set txtstrUnknownCCNames = Nothing
                Else
                    Call DebugMsg(MODULE_NAME, "SaveUnknownCreditCardName", endlDebug, "Cannot open/create Unknown Credit Card Names List file: " & UnknownCCNamesFile)
                End If
            Else
                Call DebugMsg(MODULE_NAME, "SaveUnknownCreditCardName", endlDebug, "Config file path does not exist")
            End If
        End With
        Set fsoConfigFile = Nothing
    End If
    Call DebugMsg(MODULE_NAME, "SaveUnknownCreditCardName", endlTraceOut)
    
    Exit Sub

' Try default values and resume
TryF:
    ConfigFilePath = "F:\Wix\Data"
    Resume Next
TryC:
    ConfigFilePath = "C:\Wix\Data"
    Resume Next
TryUnknownNames:
    UnknownCCNamesFile = "UnknownCCNames.lst"
    Resume Next
End Sub


' Check parameters 831 to determine if using
' CommideaPEDs generally and 970001 to determine
' if this till is using a Commidea PED specifically.
' Store parameter values to save re-checking.
Private Function TillUsingCommideaPED() As Boolean
    Dim astrCommideaTills() As String
    Dim intTill As Integer

    ' Just skip anything that might set result to true
On Error GoTo NotUsingCommideaPED

    If mstrCommideaTills = "" Then
        mstrCommideaTills = goSession.GetParameter(PRM_COMMIDEA_TILLS)
    End If
    If mstrPrm_Use_EFTPOS = "" Then
        mstrPrm_Use_EFTPOS = goSession.GetParameter(PRM_USE_EFTPOS)
    End If
    If mstrCommideaTills <> "" And mstrPrm_Use_EFTPOS <> "" Then
        astrCommideaTills = Split(mstrCommideaTills, ",")
        For intTill = 0 To UBound(astrCommideaTills)
            If IsNumeric(astrCommideaTills(intTill)) And IsNumeric(mstrTillNo) And IsNumeric(mstrPrm_Use_EFTPOS) Then
                If CInt(astrCommideaTills(intTill)) = CInt(mstrTillNo) And CInt(mstrPrm_Use_EFTPOS) = 4 Then
                    TillUsingCommideaPED = True
                    Exit For
                End If
            End If
        Next intTill
    End If
    
    Exit Function
    
NotUsingCommideaPED:
    Err.Clear
End Function

' As is a boolean parameter need another boolean to indicate whether have got the
' 1st value in the first place. Cannot rely on False meaning False it could mean
' that have not got the parameter yet
Private Function UsingCommideaTokenIDs() As Boolean

    If mblnGot_Prm_Use_Commidea_TokenIDs Then
        UsingCommideaTokenIDs = TillUsingCommideaPED And mblnUseCommideaTokenIDs
    Else

On Error GoTo SkipFlagSetting
        
        mblnUseCommideaTokenIDs = goSession.GetParameter(PRM_COMMIDEA_USE_TOKEN_IDS)
        mblnGot_Prm_Use_Commidea_TokenIDs = True

SkipFlagSetting:
        
        If mblnGot_Prm_Use_Commidea_TokenIDs Then
            UsingCommideaTokenIDs = TillUsingCommideaPED And mblnUseCommideaTokenIDs
        End If
    End If
End Function
' End of Commidea Into WSS


Private Function GetDeliveryStatus(strOrderNumber As String) As Integer

Dim rs As New ADODB.Recordset
Dim strSql As String

        strSql = "SELECT DeliveryStatus FROM CORHDR4 WHERE NUMB = '" & strOrderNumber & "'"
        Call rs.Open(strSql, goDatabase.Connection)
        If rs.BOF = False Then
            GetDeliveryStatus = rs.Fields(0)
        Else
            GetDeliveryStatus = 0
        End If
        rs.Close

End Function

' Referral 669
' Remove the line from refund collection if is a reversal on a refund.
Private Sub RemoveReversedRefund(ByVal ReversedRefundLine As Long)
    Dim lngRefOrigLineNo As Long
    Dim oRefundLine As cRefundLine
    Dim lngLineIndex As Long

    With sprdLines
        .Row = ReversedRefundLine
        .Col = COL_QTY
        If .Value < 0 And Not mcolRefundTrans Is Nothing Then
            .Col = COL_RFND_LINENO
            lngRefOrigLineNo = Val(.Value)
            For lngLineIndex = 1 To mcolRefundTrans.Count
                Set oRefundLine = mcolRefundTrans(lngLineIndex)
                ' Match on original order line number
                If oRefundLine.LineNo = CLng(Val(.Value)) Then
                    Set oRefundLine = Nothing
                    Call mcolRefundTrans.Remove(lngLineIndex)
                    Exit For
                End If
            Next
        End If
    End With
End Sub
' End of Referral 669

' Change request CR0034
' Flag up whether functionality is switched on or not
Private Function ChangeRequestCR0034Active() As Boolean
    Dim clsStock As New StockFactory
    Dim blnSqlDatabase As Boolean

    Call DebugMsg(MODULE_NAME, "ChangeRequestCR0034Active", endlTraceIn)
    Call DebugMsg(MODULE_NAME, "ChangeRequestCR0034Active", endlDebug, "Checking if already got parameter value")
    If mblnGotParamReqActive Then
        Call DebugMsg(MODULE_NAME, "ChangeRequestCR0034Active", endlDebug, "Already got parameter value (" & CStr(mblnChangeRequestCR0034Active) & ")")
        ChangeRequestCR0034Active = mblnChangeRequestCR0034Active
    Else
        Call DebugMsg(MODULE_NAME, "ChangeRequestCR0034Active", endlDebug, "Reading parameter value")
    
        On Error Resume Next
        
            mblnChangeRequestCR0034Active = goSession.GetParameter(PRM_REQUIREMENTACTIVATED_CR0034)
        
        On Error GoTo 0
        
        Call DebugMsg(MODULE_NAME, "ChangeRequestCR0034Active", endlDebug, "Read parameter value (" & CStr(mblnChangeRequestCR0034Active) & ")")
        
        mblnGotParamReqActive = True
    End If
    ChangeRequestCR0034Active = mblnChangeRequestCR0034Active
    Call DebugMsg(MODULE_NAME, "ChangeRequestCR0034Active", endlTraceOut)
End Function
' End of Change request CR0034

' Change Request CR0064
Friend Function DisableTrainingModeOrders() As Boolean
    Dim TrainingModeOrdersImplementationFactory As New TillFormFactory
    Dim TrainingModeOrdersImplementation As ITrainingModeOrders

    Set TrainingModeOrdersImplementation = TrainingModeOrdersImplementationFactory.FactoryGetTrainingModeOrders
    DisableTrainingModeOrders = TrainingModeOrdersImplementation.DisableTrainingModeOrders And mblnTraining
End Function
' End of Change Request CR0064

' End of P014-01

Private Sub mRetrieveCoupon_InvalidCouponEvent(ByRef response As String)
          
    Call MsgBoxEx("Invalid Coupon", vbOKOnly, "Invalid Coupon Entered", , , , , RGBMsgBox_WarnColour)
End Sub

Private Sub mRetrieveCoupon_DeletedCouponEvent(ByRef response As String)
          
    Call MsgBoxEx("Entered coupon has been marked as deleted.  Unable to accept coupon.", vbOKOnly, "Invalid Coupon Entered", , , , , RGBMsgBox_WarnColour)
End Sub

Private Sub mRetrieveCoupon_ExclusiveCouponEvent(ByRef response As String)

    response = MsgBoxEx("This is an Exclusive Coupon.  No other Coupon discounts will apply during this transaction.", vbOKCancel, _
                        "Exclusive Coupon Added", "Accept", "Reject", , , RGBMSGBox_PromptColour)
End Sub

Private Sub mRetrieveCoupon_ExclusiveAlreadyExistsInTransactionEvent(ByRef response As String)
          
    response = MsgBoxEx("You already have an Exclusive Coupon in the Transaction." & vbNewLine & "This coupon will remove the Exclusive Coupon.", _
                        vbOKCancel, "Exclusive Coupon Exists", "Override", "Reject", , , RGBMsgBox_WarnColour)
End Sub

Private Sub mRetrieveCoupon_ExclusiveCouponOtherCouponsEvent(ByRef response As String)
          
    response = MsgBoxEx("This is an Exclusive Coupon." & vbNewLine & "No other Coupon discounts will apply during this transaction." & _
                        vbNewLine & vbNewLine & "You already have other Coupon(s) in the transaction.", vbOKCancel, "Exclusive Coupon Added", "Override", "Reject", , , RGBMsgBox_WarnColour)
End Sub

Private Sub mRetrieveCoupon_CustomerDetailsEvent()
    Dim vntAddress    As Variant
          
    On Error GoTo HandleException
          
    If mstrName = "" Then
        Call MsgBoxEx("Coupon requires entry of Customer Details", vbOKOnly, "Capture Customer Details", , , , , RGBMSGBox_PromptColour)
        Call SetEntryMode(enemNone)
        Call frmCustomerAddress.GetParkedName(mstrName, mstrPostCode, mstrAddress, mstrTelNo)
        If frmCustomerAddress.Cancel = False Then
            If LenB(mstrAddress) <> 0 Then
                vntAddress = Split(mstrAddress, vbNewLine)
                mstrAddressLine1 = vntAddress(0)
                mstrAddressLine2 = vntAddress(1)
                mstrAddressLine3 = vntAddress(2)
                
                Dim oReturnCust As cSale_Wickes.cReturnCust
                
                Set oReturnCust = goDatabase.CreateBusinessObject(CLASSID_RETURNCUST)
                With oReturnCust
                    .TransactionDate = moTranHeader.TranDate
                    .PCTillID = moTranHeader.TillID
                    .TransactionNumber = moTranHeader.TransactionNo
                    .LineNumber = 0
                    .CustomerName = mstrName
                    .AddressLine1 = mstrAddressLine1
                    .AddressLine2 = mstrAddressLine2
                    .AddressLine3 = mstrAddressLine3
                    .PostCode = mstrPostCode
                    .PhoneNo = mstrTelNo
                    .HouseName = ""
                    .LabelsRequired = False
                    .RefundReason = "00"
                    .MobileNo = mstrMobileNo
                    .WorkTelNumber = Mid(mstrWorkTelNo, 1, 1)
                    .Email = Mid(mstrEmail, 1, 1)
                    .SaveIfNew
                End With
            End If 'address entered so save
        End If 'address not cancelled
    End If 'address required
     'capturing Coupon and address must be captured
     
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("mRetrieveCoupon_CustomerDetailsEvent", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
     
End Sub

Private Sub mRetrieveCoupon_CheckOtherExclusiveCouponInSpreadSheetEvent(ByRef blnOtherFnd As Boolean, ByRef blnExclFnd As Boolean)
    Dim lngRowNo      As Long

    On Error GoTo HandleException

    blnOtherFnd = False
    blnExclFnd = False
    'Step through items and check if other coupons and if any are Exclusive
    For lngRowNo = 2 To sprdLines.MaxRows Step 1
        sprdLines.Row = lngRowNo
        sprdLines.Col = COL_VOIDED
        If (Val(sprdLines.Text) <> 1) Then 'active
            sprdLines.Col = COL_LINETYPE
            If (Val(sprdLines.Text) = LT_COUPON) Then
                blnOtherFnd = True
                sprdLines.Col = COL_CPN_EXCLUSIVE
                If (Val(sprdLines.Text) = 1) Then
                    blnExclFnd = True
                    Exit For
                End If 'an Exclusive Coupon
            End If 'Line is a coupon
        End If 'Active
    Next lngRowNo
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("mRetrieveCoupon_CheckOtherExclusiveCouponInSpreadSheetEvent", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub mRetrieveCoupon_DeleteExclusiveCouponsEvent()
    Dim lngRowNo      As Long

     'Step through items and check if other coupons and if any are Exclusive
    For lngRowNo = 2 To sprdLines.MaxRows Step 1
        sprdLines.Row = lngRowNo
        sprdLines.Col = COL_VOIDED
        If (Val(sprdLines.Text) <> 1) Then 'active
            sprdLines.Col = COL_LINETYPE
            If (Val(sprdLines.Text) = LT_COUPON) Then
                sprdLines.Col = COL_VOIDED
                sprdLines.Text = 1 'flag original item as voided
                sprdLines.Col = -1
                sprdLines.FontStrikethru = True
                sprdLines.Col = COL_CPN_STATUS
                sprdLines.Text = "5"
                sprdLines.Col = 0
                sprdLines.Text = "R"
                sprdLines.FontStrikethru = False
                sprdLines.Col = COL_CPN_ID
                Call moEvents.DeleteCoupon(sprdLines.Text, lngRowNo)
            End If 'Line is a coupon
        End If 'Active
    Next lngRowNo
End Sub

Private Sub mRetrieveCoupon_VerifyCouponUsageEvent(ByVal strCouponID As String, ByVal strSerialNo As String, ByRef strMgrID As String, ByRef response As Boolean)

    On Error GoTo HandleException

    response = True
 
    'Check if Webservice must be called to verify Coupon has not been used
    
    'The check to call webservice to validate a coupon is not valid for project and will be considered for next year
    
    ' If (mblnCouponWSActive = True) Then
       ' Dim wsCoupon As New SoapClient30
       
       ' On Error Resume Next
       ' Call wsCoupon.MSSoapInit(mstrCouponWSURL)
       ' If Err.Number <> 0 Then
           ' On Error GoTo 0
            'If MsgBoxEx("Unable to Validate Coupon." & vbNewLine & vbNewLine & "Error detected:" & wsCoupon.FaultString, vbOKCancel + vbInformation, "Unable to Validate Coupon", "Manager Auth", , , , RGBMsgBox_WarnColour) = vbCancel Then
               ' response = False
               ' Exit Sub
            'Else
               ' Load frmVerifyPwd
                'If frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) = True Then
                   ' strMgrID = frmVerifyPwd.txtAuthID.Text
                   ' sprdLines.Col = COL_SUPERVISOR
                    'sprdLines.Text = strMgrID
                    'Unload frmVerifyPwd
                'Else
                 '   Unload frmVerifyPwd
                  '  response = False
                   ' Exit Sub
                'End If
            'End If
           ' Err.Clear
        'Else 'Webservice available so Validate coupon
         '   If (wsCoupon.CouponRedeemed(strCouponID, strSerialNo) = True) Then
          '      Call MsgBoxEx("Coupon has already been redeemed", vbOKOnly, "Coupon Validation Failed", , , , , RGBMsgBox_WarnColour)
           '     response = False
            '    Exit Sub
           ' End If
        'End If
        'Set wsCoupon = Nothing
        
    'Else 'No web service so just get manager authorisation
        Load frmVerifyPwd
        If frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible) = True Then
            strMgrID = frmVerifyPwd.txtAuthID.Text
            Unload frmVerifyPwd
        Else
            Unload frmVerifyPwd
            response = False
            Exit Sub
        End If
    ' End If
    'Coupon is Serial Numbered, so validate at Head Office
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("mRetrieveCoupon_VerifyCouponUsageEvent", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub mRetrieveCoupon_AddtoTransactionEvent(ByVal BuyCoupon As Boolean, ByVal couponDescription As String, ByVal exclusiveCoupon As Boolean, ByVal CouponId As String, ByVal reusableCoupon As Boolean, ByVal serialNumber As String, ByVal managerId As String, ByVal marketReference As String)

    On Error GoTo HandleException

    sprdLines.MaxRows = sprdLines.MaxRows + 1
    sprdLines.Row = sprdLines.MaxRows
    sprdLines.Col = 0
    sprdLines.Text = " " 'override auto column lettering
    sprdLines.Col = COL_PARTCODE
    sprdLines.Text = "Coupon"
    sprdLines.Col = COL_DESC
    sprdLines.Text = couponDescription
     
    sprdLines.Col = COL_LINETYPE
    sprdLines.Text = LT_COUPON
    sprdLines.Col = COL_CPN_EXCLUSIVE
    sprdLines.Text = Abs(exclusiveCoupon)
    sprdLines.Col = COL_CPN_ID
    sprdLines.Text = CouponId
    sprdLines.Col = COL_CPN_REUSE
    sprdLines.Text = Abs(reusableCoupon)
    sprdLines.Col = COL_CPN_SERIALNO
    sprdLines.Text = serialNumber
    sprdLines.Col = COL_SUPERVISOR
    sprdLines.Text = managerId
    sprdLines.Col = COL_CPN_MARKETREF
    sprdLines.Text = marketReference
    If BuyCoupon Then
        Call moEvents.AddCouponWithDescription(CouponId, sprdLines.Row, IIf(reusableCoupon, "", couponDescription))
    Else
        sprdLines.Col = COL_CPN_STATUS
        sprdLines.Value = "9"
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("mRetrieveCoupon_AddtoTransactionEvent", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

' Buy Coupons P014-01
Friend Function EnableBuyCoupons() As Boolean
    Dim TillImplementationFactory As New TillFormFactory
    Dim BuyCouponsImplementation As IBuyCoupon

    Set BuyCouponsImplementation = TillImplementationFactory.FactoryGetBuyCoupon
    EnableBuyCoupons = BuyCouponsImplementation.EnableBuyCoupon
End Function
' End of Buy Coupons P014-01

' Get Coupons P014-02
Friend Function EnableGetCoupons() As Boolean
    Dim TillImplementationFactory As New TillFormFactory
    Dim GetCouponsImplementation As IGetCoupon

    Set GetCouponsImplementation = TillImplementationFactory.FactoryGetGetCoupon
    EnableGetCoupons = GetCouponsImplementation.EnableGetCoupon
End Function
' End of Get Coupons P014-02

Private Function CheckCouponTriggeredAnEvent(ByVal CouponId As String, ByRef colCouponsUsed As Collection) As Boolean
Dim index As Integer

    CheckCouponTriggeredAnEvent = False
    If Not colCouponsUsed Is Nothing Then
        For index = 1 To colCouponsUsed.Count
            If CouponId = colCouponsUsed.Item(index) Then
                CheckCouponTriggeredAnEvent = True
                colCouponsUsed.Remove (index) 'PO14 - 01 Ref 928
                Exit For
            End If
        Next index
    End If

    'Check a Get Coupon
    If CheckCouponTriggeredAnEvent = False Then
        For index = 1 To mcolGetCoupons.Count
            If CouponId = Mid(mcolGetCoupons.Item(index), 1, 7) Then
                CheckCouponTriggeredAnEvent = True
                Exit For
            End If
        Next index
    End If
    
    If CheckCouponTriggeredAnEvent = False Then
        If Not mcolHSGetCoupons Is Nothing Then
            For index = 1 To mcolHSGetCoupons.Count
                If CouponId = Mid(mcolHSGetCoupons.Item(index), 1, 7) Then
                    CheckCouponTriggeredAnEvent = True
                    Exit For
                End If
            Next index
        End If
    End If

End Function

Private Function CouponReuseable(ByVal CouponId, ByRef strDescription As String) As Boolean

Dim rs As New ADODB.Recordset
Dim strSql As String

    strSql = "SELECT REUSABLE, DESCRIPTION FROM COUPONMASTER WHERE COUPONID = '" & CouponId & "'"
    Call DebugMsg("frmTill", "Get Reusablity of Coupon", endlTraceOut, strSql)
    Call rs.Open(strSql, goDatabase.Connection)
    If rs.BOF = False Then
        On Error Resume Next
        strDescription = rs.Fields("DESCRIPTION").Value
        CouponReuseable = rs.Fields("REUSABLE").Value
        Err.Clear
    End If
    
    rs.Close

End Function

Private Function GetNumberOfCouponsScannedIntransaction() As Integer
    Dim lngRowNo    As Long
    Dim index       As Integer
    Dim Count       As Integer
    Dim Status      As String
    index = 1
    Count = 0
    'Step through items and check for unused coupons
    For lngRowNo = 2 To sprdLines.MaxRows Step 1
        sprdLines.Row = lngRowNo
        sprdLines.Col = COL_VOIDED
        If (Val(sprdLines.Text) <> 1) Then 'active
            sprdLines.Col = COL_LINETYPE
            If (Val(sprdLines.Text) = LT_COUPON) Then
                sprdLines.Col = COL_CPN_STATUS
                Status = sprdLines.Text
                sprdLines.Col = COL_CPN_ID
                If mcolCouponsInTransaction.Count >= index Then
                    If mcolCouponsInTransaction.Item(index) = sprdLines.Text Then
                        index = index + 1
                    End If
                Else
                    If Status = "0" Then Count = Count + 1
                End If
            End If 'Line is a coupon
        End If 'Active
    Next lngRowNo
    GetNumberOfCouponsScannedIntransaction = Count

End Function

' Dotnet Offline Mode - US 4151
Friend Function EnableDotnetOfflineModeSetting() As Boolean
    Dim TillImplementationFactory As New TillFormFactory
    Dim DotnetOfflineModeImplementation As IDotnetOfflineMode

    Set DotnetOfflineModeImplementation = TillImplementationFactory.FactoryGetDotnetOfflineMode
    EnableDotnetOfflineModeSetting = DotnetOfflineModeImplementation.EnableOfflineModeSetting
End Function

Friend Sub SetDotnetOfflineMode()
    Dim OfflineModeImplementation As COMTPWickes_InterOp_Interface.IOfflineMode
    Dim OfflineModeImplementationFactory As New COMTPWickes_InterOp_Wrapper.OfflineMode
    
    Set OfflineModeImplementation = OfflineModeImplementationFactory.FactoryGet()
    If Not OfflineModeImplementation Is Nothing Then
        Call OfflineModeImplementation.SetOfflineMode(Not goSession.ISession_IsOnline)
    End If
End Sub
' End of Dotnet Offline Mode - US 4151

' Refund Tender Type CR0082
Friend Function EnableRefundTenderType() As Boolean
    Dim TillImplementationFactory As New TillFormFactory
    Dim RefundTenderTypeImplementation As IRefundTenderType

    Set RefundTenderTypeImplementation = TillImplementationFactory.FactoryGetRefundTenderType
    EnableRefundTenderType = RefundTenderTypeImplementation.EnableRefundTenderType
    Call DebugMsg(MODULE_NAME, "EnableRefundTenderType", endlDebug, "Returning - " & IIf(EnableRefundTenderType, "True", "False"))
End Function

Private Function CheckRefundTender(ByRef lngSaveMode As Long) As Boolean

Dim curPayAmount As Currency
                
    On Error GoTo HandleException
                
    Select Case MsgBoxEx("Refund of " & Format(Abs(Val(sprdTotal.Text)), "0.00") & _
                         " will be given onto customer's card " & vbNewLine & _
                         vbNewLine & "[Yes]-Continue / [No]-Select Another Tender Type", _
                         vbYesNo, "Refund", , , , , RGBMSGBox_PromptColour)
                         
        Case vbNo, vbCancel:
                Call SetEntryMode(enemNone)
                moTranHeader.SupervisorUsed = True
       Case Else:
            lngSaveMode = mlngCurrentMode
            curPayAmount = Val(sprdTotal.Text) * mlngTranSign
            mlngCurrentMode = encmTender
            Set moTenderOpts = goDatabase.CreateBusinessObject(CLASSID_TENDEROPTIONS)
            Call moTenderOpts.AddLoadFilter(CMP_EQUAL, FID_TENDEROPTIONS_TenderNo, TENDNO_CRDR)
            Set moTenderOpts = moTenderOpts.LoadMatches.Item(1)
            Call ProcessPaymentType(TEN_CREDCARD, curPayAmount)
            If curPayAmount = 0 Then
                Unload frmTender
                Call Form_KeyPress(vbKeyEscape)
                CheckRefundTender = False
            Else
            mlngCurrentMode = encmComplete
            SetEntryMode (enemNone)
            mblnCreditCardUsedInTender = True
            Call FinaliseTransaction(False, True)
            mblnCreditCardUsedInTender = False
            CheckRefundTender = True
            End If
    End Select
    
    Exit Function

HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("CheckRefundTender", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Function

Public Sub DisableOrderButtons(Value As Boolean)
    Call EnableAction(enacCreateOrder, Value)
    Call EnableAction(enacRecallQuote, Value)
    Call EnableAction(enacSaveQuote, Value)
End Sub

Private Sub LoadTenderBar()

Dim rsTenders As ADODB.Recordset
Dim lngBtnNo As Long
Dim lngLeftPos As Long
Dim strPartCode As String

    Set rsTenders = TendersRecordset(moTranHeader.TransactionCode)
    
    Call DebugMsg(MODULE_NAME, "LoadTenderBar", endlDebug, "Got Tenders: " & mstrTranId)
    
    'Unload any previous Tender Types that where valid for previous transaction types
    For lngBtnNo = cmdTenderType.UBound To 1 Step -1
        Call Unload(cmdTenderType(lngBtnNo))
    Next lngBtnNo
    For lngBtnNo = lblTenderFKey.UBound To 1 Step -1
        Call Unload(lblTenderFKey(lngBtnNo))
    Next lngBtnNo
        
    lngBtnNo = 1
    
    Do While Not rsTenders.EOF
        If (LenB(rsTenders!Description) <> 0) Then
            Load cmdTenderType(lngBtnNo)
            lngLeftPos = ((lngBtnNo - 1) Mod BTN_PER_ROW) + 1
            cmdTenderType(lngBtnNo).Left = ((cmdTenderType(0).Width + 120) * (lngLeftPos - 1)) + 180
            cmdTenderType(lngBtnNo).Visible = False
            cmdTenderType(lngBtnNo).Caption = Trim(rsTenders!Description)
            cmdTenderType(lngBtnNo).Tag = rsTenders!TenderNo
            cmdTenderType(lngBtnNo).Enabled = (IsNull(rsTenders!Active) = False)
            cmdTenderType(lngBtnNo).BackColor = fraTender.BackColor
            
            If mblnTraining Then
                If rsTenders!TendType = TEN_CHEQUE Or rsTenders!TendType = TEN_CREDCARD Or rsTenders!TendType = TEN_DEBITCARD Then
                    cmdTenderType(lngBtnNo).Enabled = False
                Else
                End If
            End If
            Select Case (rsTenders!TendType)
                Case (TEN_CASH), (TEN_GIFTTOKEN), (TEN_GIFTCARD), (TEN_PROJLOAN), (TEN_COUPON):
                    If (mblnHasCashDrawer = False) Then cmdTenderType(lngBtnNo).Enabled = False
                Case (TEN_CHEQUE):
                    If (mblnHasCashDrawer = False) Or (mblnHasEFT = False) Then cmdTenderType(lngBtnNo).Enabled = False
                Case (TEN_CREDCARD), (TEN_DEBITCARD):
                    If (mblnHasEFT = False) Then cmdTenderType(lngBtnNo).Enabled = False
            End Select
            
            'Commidea Into WSS
            ' New Cnp button
            If rsTenders!TenderNo = TENDNO_CNP Then
                ' Only enabled for QOD (provided have Commidea till)
                ' Referral 476 - allow Misc Income to have Card Not Present
                ' Start assuming no CNP allowed...
                cmdTenderType(lngBtnNo).Enabled = False
                '...then switch on in desired circumstances
                If TillUsingCommideaPED Then
                    ' Referral 476, 524 (again) - allow Misc Income (M+ MISCIN) to have Card Not Present
                    ' Referral 523 - allow Paid Out (M- MISCOUT) to have Card Not Present
                    ' Referral 527 - allow Misc Income Correct (C+ CORRIN) to have Card Not Present
                    ' Referral 527 - allow Paid Out Correct (C- CORROUT) to have Card Not Present
                    If mblnOrderBtnPressed And mblnCreatingOrder _
                    Or moTOSType.Code = TT_MISCIN _
                    Or moTOSType.Code = TT_MISCOUT _
                    Or moTOSType.Code = TT_CORRIN _
                    Or moTOSType.Code = TT_CORROUT Then
                        cmdTenderType(lngBtnNo).Enabled = True
                    End If
                End If
            End If
            'End of Commidea Into WSS
            
            If lngBtnNo <= BTN_PER_ROW Then
                Load lblTenderFKey(lngBtnNo)
                lblTenderFKey(lngBtnNo).Left = cmdTenderType(lngBtnNo).Left + _
                            ((cmdTenderType(lngBtnNo).Width - lblTenderFKey(lngBtnNo).Width) / 2)
                lblTenderFKey(lngBtnNo).Visible = True
                lblTenderFKey(lngBtnNo).Caption = "F" & lngBtnNo
            End If
            lngBtnNo = lngBtnNo + 1
        End If
        rsTenders.MoveNext
    Loop
    If (goSession.GetParameter(PRM_TRAN_DISC_HOTKEY) <> "") Then
        mstrTranDiscCode = goSession.GetParameter(PRM_TRAN_DISC_HOTKEY)
        If (mstrTranDiscCode = "") Then
            Call MsgBoxEx("Transaction Discount configuration does not have the Reason Code defined." & vbCrLf & "Transaction Discount has been disabled.", vbOKOnly, "Invalid Transaction Discount Set-Up", , , , , RGBMsgBox_WarnColour)
        Else
            mstrTranDiscAuth = Mid$(mstrTranDiscCode, InStr(mstrTranDiscCode, ",") + 1)
            mstrTranDiscCode = Left$(mstrTranDiscCode, InStr(mstrTranDiscCode, ",") - 1)
            strPartCode = Mid$(mstrTranDiscAuth, InStr(mstrTranDiscAuth, ",") + 1)
            mstrTranDiscAuth = Left$(mstrTranDiscAuth, InStr(mstrTranDiscAuth, ",") - 1)
            mlngTranDiscMgrLevel = Val(Mid$(strPartCode, InStr(strPartCode, ",") + 1))
            mlngTranDiscSupLevel = Val(Left$(strPartCode, InStr(strPartCode, ",") - 1))
            Load cmdTenderType(lngBtnNo)
            lngLeftPos = ((lngBtnNo - 1) Mod BTN_PER_ROW) + 1
            cmdTenderType(lngBtnNo).Left = ((cmdTenderType(0).Width + 120) * (lngLeftPos - 1)) + 180
            cmdTenderType(lngBtnNo).Visible = False
            cmdTenderType(lngBtnNo).Caption = "Tran Disc"
            cmdTenderType(lngBtnNo).Tag = "TranDisc"
            cmdTenderType(lngBtnNo).Enabled = True
            cmdTenderType(lngBtnNo).BackColor = fraTender.BackColor
    '        cActionCode.ActionCode = enacTransactionDisc
        End If
    End If
    
    Call DebugMsg(MODULE_NAME, "RecordTender", endlDebug, "Buttons Built: " & mstrTranId)

    mlngTenderTypeRowNo = 0
    Call ShowTenderTypeRow
    
    Call SetupDisplayForTender

End Sub

'End of Refund Tender Type CR0082

' Pic Refund Count CR0087.  RF 1070 - Do not include Order refunds in count unless completed
' This requires that the ORDN is no  longer stored in DLTOTS for this type of refund
Private Function EnableNewPICRefundCountProcess() As Boolean
    Dim TillImplementationFactory As New TillFormFactory
    Dim PICRefundCountProcessImplementation As IPICRefundCountProcess

    Set PICRefundCountProcessImplementation = TillImplementationFactory.FactoryGetPICRefundCountProcess
    EnableNewPICRefundCountProcess = PICRefundCountProcessImplementation.EnableNewProcess
    Call DebugMsg(MODULE_NAME, "EnableNewPICRefundCountProcess", endlDebug, "Returning - " & IIf(EnableNewPICRefundCountProcess, "True", "False"))
End Function
'End of Pic Refund Count CR0087.  RF 1070 - Do not include Order refunds in count unless completed

' RF1094 Repeated refunds if hold down enter key
Private Function ShouldCancelForRepeatPayment(ByVal WSSUniqueTTRef As String) As Boolean
    
    Call DebugMsg(MODULE_NAME, "ShouldCancelForRepeatPayment", endlTraceIn)
    Call SetRepeatPayment(WSSUniqueTTRef)
    Call SetRepeatPaymentImplementation
    If RepeatPaymentAttempted Then
        ShouldCancelForRepeatPayment = RepeatPaymentImplementation.ShouldCancelRepeatPayment()
    Else
        ShouldCancelForRepeatPayment = False
    End If
    Call DebugMsg(MODULE_NAME, "ShouldCancelForRepeatPayment", endlDebug, "Returning " & IIf(ShouldCancelForRepeatPayment, "True", "False"))
    Call DebugMsg(MODULE_NAME, "ShouldCancelForRepeatPayment", endlTraceOut)
End Function

Private Sub SetRepeatPayment(ByVal WSSUniqueTTRef As String)

    Call DebugMsg(MODULE_NAME, "SetRepeatPayment", endlTraceIn)
    Call DebugMsg(MODULE_NAME, "SetRepeatPayment", endlDebug, "WSSUniqueTTRef = " & WSSUniqueTTRef & "; PreviousWSSUniqueTTRef = " & PreviousWSSUniqueTTRef)
    RepeatPaymentAttempted = CBool(WSSUniqueTTRef = PreviousWSSUniqueTTRef)
    Call DebugMsg(MODULE_NAME, "SetRepeatPayment", endlDebug, "Set RepeatPaymentAttempted to " & IIf(RepeatPaymentAttempted, "True", "False"))
    Call DebugMsg(MODULE_NAME, "SetRepeatPayment", endlTraceOut)
End Sub

Private Sub ResetRepeatPayment()

    Call DebugMsg(MODULE_NAME, "ResetRepeatPayment", endlTraceIn)
    RepeatPaymentAttempted = False
    Call DebugMsg(MODULE_NAME, "ResetRepeatPayment", endlTraceOut)
End Sub

Private Sub SetPreviousWSSUniqueTTRef(ByVal WSSUniqueTTRef As String)

    Call DebugMsg(MODULE_NAME, "SetPreviousWSSUniqueTTRef", endlTraceIn)
    PreviousWSSUniqueTTRef = WSSUniqueTTRef
    Call DebugMsg(MODULE_NAME, "SetPreviousWSSUniqueTTRef", endlTraceOut)
End Sub

Private Function SetRepeatPaymentImplementation()

    Call DebugMsg(MODULE_NAME, "SetRepeatPaymentImplementation", endlTraceIn)
    If RepeatPaymentImplementation Is Nothing Then
        Dim TillFactory As New TillFormFactory
        
        Call DebugMsg(MODULE_NAME, "SetRepeatPaymentImplementation", endlDebug, "Getting RepeatPaymentImplementation")
        Set RepeatPaymentImplementation = TillFactory.FactoryGetRepeatPayment
    End If
    Call DebugMsg(MODULE_NAME, "SetRepeatPaymentImplementation", endlTraceOut)
End Function
' End of RF1094 Repeated refunds if hold down enter key

Private Function InitializeCommideaConfiguration() As CommideaConfiguration
        
    'Setup PrinterInfo fields
    'Printing for Commidea
    Dim oRecPrinting As New clsReceiptPrinting
    Set oRecPrinting.Printer = OPOSPrinter1
    Set oRecPrinting.goRoot = goRoot
    oRecPrinting.TillNo = mstrTillNo
    oRecPrinting.TranNo = mstrTranId
    Call oRecPrinting.Init(goSession, goDatabase)
    
    ' Always use AccountOnFile Registration
    ' GenerateWSSUniqueTransactionTenderReference - use AssignedSequenceNumber:=moTranHeader.Payments.Count + 1 as
    ' moTranHeader (cSales.cPosHeader type) uses the Payments Count as the sequence number AFTER a new payment
    ' has been added and the moTranHeader.Seq is used as the AssignedSequence number for a tender
    
    Dim result As New CommideaConfiguration
    result.SoftwareVersion = mstrOciusSentinelVersion
    result.TillID = mstrTillNo
    result.PathToOciusSentinel = mstrOciusSentinelPath
    result.IntegrationPort = mstrCommideaIntegrationPort
    result.ProgressPort = mstrCommideaProgressPort
    result.LoginTimeout = mintOciusLoginTimeout
    result.ConnectionTimeout = mintConnectionTimeout
    result.ShowKeypadIcon = False
    result.BackgroundColour = mlngBackColor
    result.EditColour = RGBEdit_Colour
    result.MsgBoxPromptColour = RGBMSGBox_PromptColour
    result.MsgBoxWarningColour = RGBMsgBox_WarnColour
    result.AlreadyLoggedIn = mblnOciusPEDLoggedIn
    result.CustRecPath = mstrCommideaCustRecPath
    result.CustRecFilename = mstrCommideaCustRecFilename
    result.MerRecPath = mstrCommideaMerRecPath
    result.MerRecFilename = mstrCommideaMerRecFilename
    result.UnderDuressKeyCode = mintDuressKeyCode
    result.CurrencyCharacter = mstrCurrChar
    result.UseSeparateReceipt = Not MergingCommideaAndWickesReceipts
    result.ReceiptHeaderLastLine = mstrCommideaReceiptHeaderLastLine
    result.ReceiptFooterFirstLine = mstrCommideaReceiptFooterFirstLine
    result.LicenceMerchantName = mstrCommideaLicenceName
    result.UseModalAuthorisation = mblnCommideaModalAuthorisation
    result.PrintConfirmTimeout = mintOciusPrintConfirmTimeout
    result.PartialGCPaymentTimeout = mPartialGCPaymentTimeout
    
    Set result.PrinterInfo = oRecPrinting
    
    Set InitializeCommideaConfiguration = result
End Function

Private Sub DebugMsgCommideaConfiguration(ByVal cfg As CommideaConfiguration, ByVal MethodName As String)

    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Till ID= " & cfg.TillID)
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Ocius Sentinel Path= " & cfg.PathToOciusSentinel)
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Integration Port= " & cfg.IntegrationPort)
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Progress Port= " & cfg.ProgressPort)
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Login Timeout= " & CStr(cfg.LoginTimeout))
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Connection Timeout= " & CStr(cfg.ConnectionTimeout))
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Back Colour= " & CStr(cfg.BackgroundColour))
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Edit Colour= " & CStr(cfg.EditColour))
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Prompt Colour= " & CStr(cfg.MsgBoxPromptColour))
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Warning Colour= " & CStr(cfg.MsgBoxWarningColour))
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Logged In = " & IIf(cfg.AlreadyLoggedIn, "Yes", "No"))
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Customer Receipt Path = " & cfg.CustRecPath)
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Customer Receipt File = " & cfg.CustRecFilename)
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Merchant Receipt Path = " & cfg.MerRecPath)
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Merchant Receipt File = " & cfg.MerRecFilename)
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Under Duress KeyCode = " & CStr(cfg.UnderDuressKeyCode))
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Currency Character = " & mstrCurrChar & "(" & CInt(Asc(cfg.CurrencyCharacter)) & ")")
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Use Separate Receipt = " & IIf(cfg.UseSeparateReceipt, "Yes", "No"))
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Receipt Header Last Line = " & cfg.ReceiptHeaderLastLine)
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Receipt Footer 1st Line = " & cfg.ReceiptFooterFirstLine)
    Call DebugMsg(MODULE_NAME, MethodName, endlDebug, "Initialising Commidea - Modal Authorisation = " & IIf(cfg.UseModalAuthorisation, "Yes", "No"))
    
End Sub

 Private Function PedOffline(eftSequenceNumber As String) As Boolean
    If Len(eftSequenceNumber) > 0 And UCase(Left(eftSequenceNumber, 2)) = "OL" Then
        PedOffline = True
    Else
        PedOffline = False
    End If
End Function

Private Function CheckCommideaIsReady() As Boolean
    
    Dim lngUseEFTPOS As Long
    
    On Error GoTo HandleException
        
    lngUseEFTPOS = goSession.GetParameter(PRM_USE_EFTPOS)
    
    ' Commidea Into WSS
    ' lngUseEFTPOS = 4 means Commidea PED, but can be introduced on a till by till basis,
    ' so extra check for this till using Commidea
    If lngUseEFTPOS = 0 Or lngUseEFTPOS = 1 Or Not TillUsingCommideaPED Then
        Call MsgBoxEx("Unable to process the transaction." & vbCrLf & "This Till is not set up to work with Commidea PED.", vbExclamation, "Error", , , , , RGBMsgBox_WarnColour)
        GoTo CheckFailed
    End If

    Dim oTender As New WSSCommidea.cCommidea ' dll to talk with commidea
    ' Use the modular variable for event handling (duress, etc)
    Set moCommidea = oTender
    Call oTender.Initialise(InitializeCommideaConfiguration())
                         
    Dim timedOut As Boolean ' In log in fails because run out of time (set by param 970007)
    
    ' Check to make sure PED is ready and attempt to start it if not
    If Not mblnOciusSoftwareReady Or Not mblnOciusPEDLoggedIn Then
        ' Referral 818 - pass in software ready byref parameter, so if licence problem can flag up the issue
        Dim RestartRequired As Boolean ' When log in, can get a software/config/permissions update that might require a restart of ped
        
        If LaunchOciusSentinelAndLogin(oTender, timedOut, RestartRequired) Then
            
            If Not RestartRequired Then
                mblnOciusSoftwareReady = True
                mblnOciusPEDLoggedIn = True
            Else 'Not restartRequired
                ' In unlikely event of a PED software update happening now,
                ' need to restart PED - when finished update, so get operator
                ' to restart till
                Call MsgBoxEx("Unable to process the transaction." & vbCrLf _
                            & "PED is updating.  When it is finished, please log out, close down and restart the Till." & vbCrLf _
                            & "Thank you.", vbExclamation, "Pin Entry Device Updating", , , , , RGBMsgBox_WarnColour)
                
                GoTo CheckFailed
            End If 'Not restartRequired
            
        Else 'oTender.LaunchOciusSentinelAndLogin
        
            mblnOciusPEDLoggedIn = False
            mblnOciusSoftwareReady = False

            If timedOut Then
                If Not flSaleGiftCard Then
                    ' Error - cannot connect to Ocius Sentnel/PED etc
                    Call MsgBoxEx("Failed to connect with the PED." _
                                & vbNewLine & "Please try another payment type." _
                                & vbNewLine & "Time out on login to the Pin Entry Device; check access to port:" & mstrCommideaIntegrationPort & ".", vbExclamation, "Error has occurred connecting with the Pin Entry Device.", , , , , RGBMsgBox_WarnColour)
                Else
                    ' Error - cannot connect to Ocius Sentnel/PED etc
                    Call MsgBoxEx("Failed to connect with the PED." _
                                & vbNewLine & "Time out on login to the Pin Entry Device; check access to port:" & mstrCommideaIntegrationPort & ".", vbExclamation, "Error has occurred connecting with the Pin Entry Device.", , , , , RGBMsgBox_WarnColour)
                End If 'Not flSaleGiftCard
            Else 'timedOut
                If Not flSaleGiftCard Then
                    ' Error - cannot connect to Ocius Sentnel/PED etc
                    Call MsgBoxEx("Failed to connect with the PED." & vbNewLine & "Please try another payment type.", vbExclamation, "Error has occurred connecting with the Pin Entry Device.", , , , , RGBMsgBox_WarnColour)
                Else
                    ' Error - cannot connect to Ocius Sentnel/PED etc
                    Call MsgBoxEx("Failed to connect with the PED.", vbExclamation, "Error has occurred connecting with the Pin Entry Device.", , , , , RGBMsgBox_WarnColour)
                End If 'Not flSaleGiftCard
            End If 'timedOut
            
            GoTo CheckFailed
        End If 'oTender.LaunchOciusSentinelAndLogin
        
    End If 'Not mblnOciusSoftwareReady Or Not mblnOciusPEDLoggedIn
    
    CheckCommideaIsReady = True
    GoTo CleanUp

CheckFailed:
    CheckCommideaIsReady = False

CleanUp:
    Set moCommidea = Nothing
    
    Exit Function
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("CheckCommideaIsReady", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Function


Private Sub ShowFailedCommideaTransactionMsgBox(oAuthorizationResult As AuthorizationResult)

    Dim errorMessageToShow As String ' messages from
    Dim errorTitleToShow As String ' commidea for display
    
    If InStr(1, oAuthorizationResult.FailureReason, ";") > 0 Then
        errorTitleToShow = Split(oAuthorizationResult.FailureReason, ";")(0)
        errorMessageToShow = Split(oAuthorizationResult.FailureReason, ";")(1)
    Else
        errorTitleToShow = "Error has occurred in processing the transaction"
        errorMessageToShow = "The transaction authorisation failed to complete." & vbNewLine & "Please try again."
    End If
    
    Call MsgBoxEx(errorMessageToShow, vbExclamation, errorTitleToShow, , , , , RGBMsgBox_WarnColour)
End Sub

Private Function CreateAuthorizationRequest(ByVal lngTenderType As Long, ByVal dblTenderAmount As Currency, Optional ByVal doTopupGiftCard = False) As AuthorizationRequest

    Dim oAuthorizationRequest As New AuthorizationRequest
    
    On Error GoTo HandleException
    
    With oAuthorizationRequest
    
        Dim RefundTokenID As String ' Commidea's 'Account of File' registration identifier - read from PaymentComm
    
        If lngTenderType = TEN_GIFTCARD Then
            .RequestType = BarclaycardGift
            
            .BgiftTransactionType = IIf(doTopupGiftCard, BGiftRequestTransactionType.NewCardOrTopUp, IIf(dblTenderAmount > 0, BGiftRequestTransactionType.Refund, BGiftRequestTransactionType.Sale))
        Else
            .RequestType = Transaction
            
            If dblTenderAmount > 0 Then
                .TransactionType = TRecordTransactionType.etrttRefund
                If Not moOrigTendComm Is Nothing Then
                    ' Might not be using token ids yet, and even if are, the
                    ' token might have just been rejected so need to resend
                    ' without token id
                    If UsingCommideaTokenIDs And Not mblnInvalidTokenID Then
                        .AOFTokenID = moOrigTendComm.TokenID
                    Else
                        ' Invalid token, then retry refund without tokenid to force
                        ' entry of card details
                        .AOFTokenID = ""
                        ' Referral 820
                        ' Getting the invalid token code working
                        ' blank this too, so if another attempt is made by accident (e.g. timeout doing a 'card present')
                        ' the fact that the mblnInvalidTokenID flag has been reset wont kick off another failed
                        ' refund to original card' attempt
                        moOrigTendComm.TokenID = ""
                        ' Need this further on
                        'mblnInvalidTokenID = False
                        ' End of Referral 820.
                    End If
                End If
            Else
                .TransactionType = TRecordTransactionType.etrttPurchase
            End If
            
            ' Set TRecordModifier
            .TRecordModifier = TRecordModifier.etrmCardholderPresent ' Default setting
            ' Check for 'Cardholder not present' flag
            If mblnCNPTend Then
                If .AOFTokenID = "" Then
                    ' Referral 820
                    ' Getting the invalid token code working
                    ' If invalid token then was a refund, which means customer present
                    ' just attempted to do refund to orignal card, so now do
                    ' refund to another card that the customer has, so will revert to
                    ' use the dtandard account on file
                    If mblnInvalidTokenID Then
                        mblnInvalidTokenID = False
                        'eTxnModifier = TRecordModifier.etrmCNPAccountOnFile
                    Else
                        .TRecordModifier = TRecordModifier.etrmCNPTelephoneOrder
                        'eTxnModifier = TRecordModifier.etrmCNPMailOrder
                    End If
                    ' End of Referral 820
                Else
                    .TRecordModifier = TRecordModifier.etrmCNPAccountOnFile
                End If
            End If 'mblnCNPTend
            
            
        End If 'lngTenderType = TEN_GIFTCARD
    
        .TxnValue = dblTenderAmount
        .WSSUniqueTransTenderRef = GenerateWSSUniqueTransactionTenderReference(lngTenderType, mstrStoreNo, moTranHeader, doTopupGiftCard)
        .RegisterForAOF = TRecordAccountOnFileRegistration.etraofrRegister
        .accountId = CStr(goSession.GetParameter(PRM_ACCOUNT_ID))
        .IsScreenless = True
    
    End With 'oAuthorizationRequest
    
    Set CreateAuthorizationRequest = oAuthorizationRequest
    
    Exit Function

HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("CreateAuthorizationRequest", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Function

Private Function TopupGiftCard(ByVal Amount As Currency, ByVal CashierID As String, ByVal flSaleGiftCard As Boolean) As Boolean
    
    On Error GoTo HandleException
    
    Const MethodName = "TopupGiftCard"
    
    If Not CheckCommideaIsReady() Then
        GoTo AlreadyHandledFailure
    End If
            
    If Not CheckStoreSettings Then
        GoTo AlreadyHandledFailure
    End If

    Dim oCommidea As WSSCommidea.cCommidea
    Set oCommidea = CreateAndInitializeCommideaFacade(MethodName)
    
    Dim oAuthorizationRequest As New AuthorizationRequest
    Set oAuthorizationRequest = CreateAuthorizationRequest(TEN_GIFTCARD, Amount, doTopupGiftCard:=IIf(flSaleGiftCard, True, False))
        
    Do
        Dim oAuthorizationResult As AuthorizationResult
        Set oAuthorizationResult = oCommidea.Authorise(oAuthorizationRequest)
    
        Call StoreCommideaAuthorizationReceipts(oAuthorizationResult)
      
        If oAuthorizationResult.Completed Then
        
            If Not oAuthorizationResult.Details Is Nothing Then
                Dim oResponse As BGiftResponseREcord
                Set oResponse = oAuthorizationResult.Details
                Dim oPayment As cPOSPayment
                Set oPayment = moTranHeader.AddPayment(TEN_GIFTCARD, CDbl(Amount))
                With oResponse
                    oPayment.CreditCardNumber = MaskCreditCardNo(.IAuthorizationDetails_PAN)
                    oPayment.AuthorisationCode = .IAuthorizationDetails_AuthorisationCode
                    oPayment.TransactionId = .RecordTransactionId
                    oPayment.MessageNumber = .RecordMessageNumber
                End With
                oPayment.SaveGiftCard (True)
                Dim oPaymentComm As cPOSPaymentComm
                Set oPaymentComm = moTranHeader.AddPaymentComm(oPayment.SequenceNumber)
                With oResponse
                    oPaymentComm.TokenID = .IAuthorizationDetails_TokenID
                    oPaymentComm.CardNumberHash = .IAuthorizationDetails_CardNumberHash
                End With
                oPaymentComm.IBo_Save
                Exit Do
            End If 'Not oAuthorizationResult.Details Is Nothing
            
        Else 'oAuthorizationResult.Completed
        
            ' error - transaction failed
            Call ShowFailedCommideaTransactionMsgBox(oAuthorizationResult)
            
        End If 'oAuthorizationResult.Completed
                
    Loop While TryAnotherCard = True
              
    ' Try to return focus to till
    Call oCommidea.ResetFocus(Me.Name)

    GoTo CleanUp
    
AlreadyHandledFailure:
    Call MsgBoxEx("Payment will be taken with NO funds added to Gift Card.", vbOKOnly, _
                    "Error has occurred in processing the transaction.", , , , , RGBMsgBox_WarnColour)
    
CleanUp:
    Call DisposeCommideaFacade
    
    Exit Function

HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("TopupGiftCard", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Function

Private Function TryAnotherCard() As Boolean
    TryAnotherCard = MsgBoxEx("Unable to add funds to Card." & _
                            vbNewLine & vbNewLine & "Would you like to try another card?", vbYesNo, _
                            "Card Failure error has occurred processing transaction.", , , , , RGBMsgBox_WarnColour) = vbYes
    If Not TryAnotherCard Then
        Call MsgBoxEx("Payment will be taken with NO funds added to Gift Card.", vbOKOnly, _
                        "Card Failure error has occurred processing transaction.", , , , , RGBMsgBox_WarnColour)
    End If
End Function

Private Function CreateAndInitializeCommideaFacade(ByVal CallerMethodName As String) As cCommidea
    Dim oCommidea As New WSSCommidea.cCommidea
    
    ' Use modular variable for event handling (duress, etc)
    Set moCommidea = oCommidea
    
    Call DebugMsg(MODULE_NAME, CallerMethodName, endlDebug, "Initialising Commidea class")
    
    Dim commideaCfg As CommideaConfiguration
    Set commideaCfg = InitializeCommideaConfiguration()
    Call oCommidea.Initialise(commideaCfg)
    
    Set CreateAndInitializeCommideaFacade = oCommidea

End Function

Private Sub DisposeCommideaFacade()
    Set moCommidea = Nothing
End Sub

Private Sub StoreCommideaAuthorizationReceipts(ByVal oAuthorizationResult As AuthorizationResult)
    
    ' Collect the receipts regardless of success or not - need Voids etc
    If UBound(oAuthorizationResult.Receipts) > 0 Then
        ReDim Preserve mavReceipts(UBound(mavReceipts) + 1) As Variant
        mavReceipts(UBound(mavReceipts)) = oAuthorizationResult.Receipts
    End If

End Sub

Private Sub UpdateBrandingImageVisibility(ByVal isVisible As Boolean)
    If mblnIsHotSpotImageRequired Then
        If (goSession.ISession_IsOnline) Then
            imgCustomerCommitments.Visible = isVisible
        Else
            ImgSplash.Visible = isVisible
        End If
    End If
End Sub

Private Sub UpdateBrandingImageAndLogonConrollsPosition()
    If mblnIsHotSpotImageRequired Then
        If (goSession.ISession_IsOnline = True) Then
            imgCustomerCommitments.Top = (Me.Height \ 2) - (imgCustomerCommitments.Height \ 2)
            imgCustomerCommitments.Left = (Me.Width \ 2) - (imgCustomerCommitments.Width \ 2)
            fraLogon.Left = imgCustomerCommitments.Left + 625
            fraLogon.Top = imgCustomerCommitments.Top
        Else
            ImgSplash.Top = (Me.Height \ 2) - (ImgSplash.Height \ 2)
            ImgSplash.Left = (Me.Width \ 2) - (ImgSplash.Width \ 2)
            fraLogon.Left = DEFAULT_LOGON_CONTROLLS_POSITION
        End If
    End If

End Sub

Private Function CheckStoreSettings() As Boolean
    CheckStoreSettings = True
    'Check if AccountId is in DB
    Dim accountId As String
    accountId = goSession.GetParameter(PRM_ACCOUNT_ID)
    If accountId = "" Then
        Call MsgBoxEx("Account Id is not set in DataBase for this Store", vbOKOnly, "Error has occurred in processing the transaction", , , , , RGBMsgBox_WarnColour)
        CheckStoreSettings = False
    End If
End Function

