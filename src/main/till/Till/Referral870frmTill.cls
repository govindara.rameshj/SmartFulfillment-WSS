VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Referral870frmTill"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Implements IfrmTill870

Private Sub IfrmTill870_SetPaymentAuthorisationType(ByRef Payment As cSale_Wickes.cPOSPayment, ByVal EntryMethod As String)

    If Len(EntryMethod & "") > 0 Then
        If StrComp(UCase(EntryMethod & ""), "KEYED", vbTextCompare) = 0 Then
            Payment.CCNumberKeyed = False
            Payment.AuthorisationType = "O"
        Else
            Payment.CCNumberKeyed = True
            Payment.AuthorisationType = "R"
        End If
    Else
        Payment.CCNumberKeyed = True
        Payment.AuthorisationType = "R"
    End If
End Sub
