VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Object = "{D3BFAA51-028A-459C-BF16-A546601288D5}#1.0#0"; "TillTranRetrieve.ocx"
Begin VB.Form frmRetrieveTran 
   Caption         =   "Select Transaction to Retrieve"
   ClientHeight    =   8490
   ClientLeft      =   3975
   ClientTop       =   1680
   ClientWidth     =   11670
   ControlBox      =   0   'False
   Icon            =   "frmRetrieveTran.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   ScaleHeight     =   8490
   ScaleWidth      =   11670
   WindowState     =   2  'Maximized
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   4800
      Top             =   2460
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   1
      Top             =   8115
      Width           =   11670
      _ExtentX        =   20585
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmRetrieveTran.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12753
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   635
            MinWidth        =   635
            Picture         =   "frmRetrieveTran.frx":0F30
            Key             =   "NumPad"
            Object.ToolTipText     =   "Show/Hide Num Pad"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "15:05"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin ucTillTranRtrv_Wickes.ucTillRetrieve uctrRefund 
      Height          =   7950
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   60
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   14023
   End
End
Attribute VB_Name = "frmRetrieveTran"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'<CAMH>************************************************************************************
'* Module : frmRetrieveTran
'* Date   : 26/02/04
'* Author : Unknown
'*$Archive: /Projects/OasysV2/VB/Till/frmRetrieveTran.frm $
'******************************************************************************************
'* Summary:
'******************************************************************************************
'* $Author: Mauricem $ $Date: 10/03/04 9:54 $ $Revision: 2 $
'* Versions:
'* 23/02/04    Unknown
'*             Header added.
'</CAMH>***********************************************************************************
Option Explicit

Dim mstrStoreNumber As String
Dim mdteTranDate    As Date
Dim mstrTillID      As String
Dim mstrTranNumber  As String
Dim mstrRefundCode  As String
Dim mstrPartCode    As String
Dim mdblQuantity    As Double
Dim mcurPrice       As Currency
Dim mcurWEEERate    As Currency
Dim mlngLineNo      As Long
Dim mstrPriceCode   As String
Dim mblnSwitchEntry As Boolean
Dim mblnOtherStore  As Boolean
Dim mblnCancelled   As Boolean
Dim mblnVoided      As Boolean
Dim mOrderNumber    As String
Public mcolRefundLines As Collection

Private mlngRefundAuthMode As enRefundAuthCode

Private mlngClickX As Long
Private mlngClickY As Long

Public Event Duress()

Private mintDuressKeyCode As Integer

Private mcolTrans As Collection

Public mcolRefundTrans As Collection

' Hubs 2.0
Private mblnWebOrder        As Boolean
Private mstrWebOrderNumber  As String
' End of Hubs 2.0

Private Sub Form_Activate()
    
    Call uctrRefund.FillScreen(uctrRefund)
    
    Call ucKeyPad1.ShowNumPad

End Sub

Private Sub ucKeyPad1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

    ucKeyPad1.BackColor = vbGrayText
    mlngClickX = X
    mlngClickY = Y

End Sub
Private Sub ucKeyPad1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    If Button = 1 And Shift = 0 Then
        ucKeyPad1.Left = ucKeyPad1.Left + X - mlngClickX
        ucKeyPad1.Top = ucKeyPad1.Top + Y - mlngClickY
    End If

End Sub

Private Sub ucKeyPad1_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  
  ucKeyPad1.BackColor = RGB(0, 64, 0)
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If mintDuressKeyCode <> 0 And KeyCode = mintDuressKeyCode Then
        KeyCode = 0
        RaiseEvent Duress
    End If

End Sub

Private Sub Form_Load()

Dim oWorkStationBO As cWorkStation
    
    Me.WindowState = frmTill.WindowState
    Me.Top = frmTill.Top
    Me.Left = frmTill.Left
    Me.Height = frmTill.Height
    Me.Width = frmTill.Width
    Call InitialiseStatusBar
    
    'Retrieve Workstation control and check if Barcode Reader working and if Touch Screen
    Set oWorkStationBO = goDatabase.CreateBusinessObject(CLASSID_WORKSTATIONCONFIG)
    Call oWorkStationBO.AddLoadFilter(CMP_EQUAL, FID_WORKSTATIONCONFIG_id, goSession.CurrentEnterprise.IEnterprise_WorkstationID)
    Call oWorkStationBO.AddLoadField(FID_WORKSTATIONCONFIG_UseTouchScreen)
    Call oWorkStationBO.LoadMatches
    
    If (oWorkStationBO.UseTouchScreen = False) Then
        sbStatus.Panels("NumPad").Picture = Nothing
        ucKeyPad1.Visible = False
    End If
    Set oWorkStationBO = Nothing
    
    mlngRefundAuthMode = goSession.GetParameter(PRM_REFUND_AUTH_CONFIG)
    mintDuressKeyCode = goSession.GetParameter(PRM_KEY_DURESS)
    Call uctrRefund.Initialise(goSession, Me)
    Call uctrRefund.FillScreen(uctrRefund)
    uctrRefund.ParkedOnly = False
    Call uctrRefund.Reset
    Call CentreForm(Me)

    mblnWebOrder = False ' Hubs 2.0
End Sub

Public Sub InitialiseStatusBar()
    
    sbStatus.Panels(PANEL_VERNO).Text = "Ver. " & App.Major & "." & App.Minor & "." & App.Revision
    sbStatus.Panels(PANEL_VERNO).ToolTipText = "Program Version Number"
    sbStatus.Panels(PANEL_WSID + 1).Text = Right$("00" & goSession.CurrentEnterprise.IEnterprise_WorkstationID, 2) & " "
    sbStatus.Panels(PANEL_WSID + 1).ToolTipText = "Current Work-Station Number"
    sbStatus.Panels(PANEL_DATE + 1).Text = Format$(Date, "DD-MMM-YY")
    If (mblnShowKeypadIcon = False) Then sbStatus.Panels("NumPad").Picture = Nothing

End Sub

Private Sub sbStatus_PanelClick(ByVal Panel As MSComctlLib.Panel)

Dim blnShow As Boolean

    Select Case (Panel.Key)
        Case ("NumPad"):
                            If ((Panel.Picture Is Nothing) = False) Then
                                ucKeyPad1.Visible = Not ucKeyPad1.Visible
                            End If
    End Select

End Sub

Public Function SelectTranLine(strPartCode As String, _
                               dblQuantity As Double, _
                               curPrice As Currency, _
                               strStoreNo As String, _
                               strTranID As String, _
                               dteTrandate As Date, _
                               strTillID As String, _
                               lngLineNo As Long, _
                               strPriceCode As String, _
                               blnVoided As Boolean, _
                               blnTendered As Boolean, _
                               colRefundReasons As Collection, _
                               colPriceOverrides As Collection, _
                               OrderNumber As String)
            
    'Reset transaction line properties to avoid preset values being passed out
    mstrPartCode = vbNullString
    mdblQuantity = 0
    mcurPrice = 0
    mstrTranNumber = vbNullString
    mdteTranDate = Date
    mstrTillID = vbNullString
    mstrStoreNumber = vbNullString
    ' Hubs 2.0
    ' Referral 765; include web order values in reset process
    mblnWebOrder = False
    mstrWebOrderNumber = vbNullString
    ' End of Hubs 2.0
    mlngLineNo = 0
    Set mcolRefundLines = Nothing
    If EnableResetVoidedFlag Then
        mblnVoided = False
    End If
    mOrderNumber = vbNullString
    
    'Go and get transaction line
    Set uctrRefund.RefundReasons = colRefundReasons
    Set uctrRefund.PriceOverrideCodes = colPriceOverrides
    ' Hubs 2.0
    ' Might be switched off by price promise (referral 768) or recall transaction (referral 767)
    ' so reset it here
    uctrRefund.ShowWebOrderNumber = True
    ' End of Hubs 2.0
    Call uctrRefund.SelectTransactionLine(True)  'force using Transaction Line and to access Sales Only
    Call uctrRefund.FillScreen(uctrRefund)
    Call Me.Show(vbModal)
    
    'pass out retrieved line details
    strPartCode = mstrPartCode
    dblQuantity = mdblQuantity
    curPrice = mcurPrice
    strStoreNo = mstrStoreNumber
    strTranID = mstrTranNumber
    dteTrandate = mdteTranDate
    strTillID = mstrTillID
    lngLineNo = mlngLineNo
    strPriceCode = mstrPriceCode

    OrderNumber = mOrderNumber
End Function 'SelectTranLine

Public Function SelectPricePromiseLine(ByVal strPartCode As String, _
                               strStoreNo As String, _
                               dteTrandate As Date, _
                               strTillID As String, _
                               strTranID As String, _
                               curOrigPrice As Currency, _
                               curWEEERate As Currency, _
                               lngRequiredQty As Long, _
                               lngValidatedQty As Long)

    'Reset transaction line properties to avoid preset values being passed out
    mstrPartCode = strPartCode
    mdblQuantity = 0
    mcurPrice = 0
    mstrTranNumber = vbNullString
    mdteTranDate = Date
    mstrTillID = vbNullString
    mstrStoreNumber = vbNullString
    ' Hubs 2.0
    ' Referral 765; include web order values in reset process
    mblnWebOrder = False
    mstrWebOrderNumber = vbNullString
    ' End of Hubs 2.0
    mlngLineNo = 0
    Set mcolRefundLines = Nothing
    
    
    mcurPrice = 0
    mstrTranNumber = ""
    mdteTranDate = Date
    mstrTillID = ""
    mstrStoreNumber = "999"
    
    mOrderNumber = vbNullString
    
    uctrRefund.PricePromiseSKU = strPartCode
    uctrRefund.PricePromiseQty = lngRequiredQty
    ' Hubs 2.0
    ' Referral 768; Web Order Number not relevant to price promise
    uctrRefund.ShowWebOrderNumber = False
    ' End of Hubs 2.0
    'Go and get transaction line
    Call uctrRefund.SelectTransactionLine(True)  'force using Transaction Line and to access Sales Only
    Call uctrRefund.FillScreen(uctrRefund)
    Call Me.Show(vbModal)
    
    'pass out retrieved line details
    curOrigPrice = mcurPrice
    curWEEERate = mcurWEEERate
    strStoreNo = uctrRefund.StoreNo
    dteTrandate = mdteTranDate
    strTranID = mstrTranNumber
    strTillID = mstrTillID
    lngValidatedQty = uctrRefund.PricePromiseValidQty

End Function 'SelectPricePromiseLine

Public Function SelectTranLines(strStoreNo As String, _
                               strTranID As String, _
                               dteTrandate As Date, _
                               strTillID As String, _
                               colRefundReasons As Collection, _
                               colPriceOverrides As Collection, _
                               colRefundLines As Collection, _
                               strRefundReason As String, _
                               blnCancelled As Boolean, _
                               blnVoided As Boolean, _
                               OrderNumber As String)
            
    'Reset transaction line properties to avoid preset values being passed out
    mstrPartCode = vbNullString
    mdblQuantity = 0
    mcurPrice = 0
    mstrTranNumber = vbNullString
    mdteTranDate = Date
    mstrTillID = vbNullString
    mstrStoreNumber = vbNullString
    ' Hubs 2.0
    ' Referral 765; include web order values in reset process
    mblnWebOrder = False
    mstrWebOrderNumber = vbNullString
    ' End of Hubs 2.0
    mstrPriceCode = vbNullString
    mlngLineNo = 0
    mblnCancelled = False
    Set mcolRefundLines = Nothing
    mstrRefundCode = ""
    If EnableResetVoidedFlag Then
        mblnVoided = False
    End If
    mOrderNumber = vbNullString
    
    'Go and get transaction line
    Set uctrRefund.RefundReasons = colRefundReasons
    Set uctrRefund.PriceOverrideCodes = colPriceOverrides
    Call LoadRefundTrans
    Set uctrRefund.RefundTrans = mcolRefundTrans
    Set uctrRefund.CurrentRefundLines = mcolTrans
    ' Hubs 2.0
    ' Might be switched off by price promise (referral 768) or recall transaction (referral 767)
    uctrRefund.ShowWebOrderNumber = True
    ' End of Hubs 2.0
    uctrRefund.SelectTransactionLine (True) 'force using Transaction Line and to access Sales Only
    Call uctrRefund.FillScreen(uctrRefund)
    Call Me.Show(vbModal)
    
    'pass out retrieved line details
    strStoreNo = mstrStoreNumber
    strTranID = mstrTranNumber
    dteTrandate = mdteTranDate
    strTillID = mstrTillID
    Set colRefundLines = mcolRefundLines
    If Not colRefundLines Is Nothing Then
        If colRefundLines.Count = 0 Then
            mblnCancelled = True
        End If
    End If
    blnCancelled = mblnCancelled
    blnVoided = mblnVoided
    strRefundReason = mstrRefundCode

    OrderNumber = mOrderNumber
End Function 'SelectTranLines

Public Function RetrieveTransaction(StoreNumber As String, TranDate As Date, TillID As String, TranNumber As String, ByRef blnOtherStore As Boolean)

'    Call uctrRefund.Initialise(goSession, Me)
'    uctrRefund.ParkedOnly = True
'    Call uctrRefund.Reset
    uctrRefund.StoreNo = StoreNumber
    'Hubs 2.0
    ' Referral 767; Web Order Number is not relevant to recalling a transaction
    uctrRefund.ShowWebOrderNumber = False
    'End of Hubs 2.0
    mblnOtherStore = False
    
    Call Me.Show(vbModal)
    StoreNumber = mstrStoreNumber
    TranDate = mdteTranDate
    TillID = mstrTillID
    TranNumber = mstrTranNumber
    blnOtherStore = mblnOtherStore
End Function 'RetrieveTransaction

' Hubs 2.0
Public Property Get IsWebOrder() As Boolean

    IsWebOrder = mblnWebOrder
End Property

Public Property Get WebOrderNumber() As String

    WebOrderNumber = mstrWebOrderNumber
End Property
' End of Hubs 2.0

Private Sub uctrRefund_Apply(StoreNumber As String, TranDate As Date, TillID As String, TranNumber As String)
    
    mstrStoreNumber = StoreNumber
    mstrTranNumber = TranNumber
    mdteTranDate = TranDate
    mstrTillID = TillID
    Call Me.Hide

End Sub

Private Sub uctrRefund_Cancel()

    Call Me.Hide
    mstrTillID = vbNullString
    mstrTranNumber = vbNullString
    mblnCancelled = True
    
End Sub


Private Sub uctrRefund_NoOriginalTran(RefundReasonCode As String)

    mblnSwitchEntry = False
    Select Case (mlngRefundAuthMode)
        Case (enracNone):
                mblnSwitchEntry = True
        Case (enracRefundPwd):
                mblnSwitchEntry = True
'                Load frmVerifyPwd
'                If frmVerifyPwd.VerifyRefundPassword = True Then mblnSwitchEntry = True
        Case (enracCurrentUserNoPwd):
                mblnSwitchEntry = True
        Case (enracCurrentUserPwd):
                mblnSwitchEntry = True
        Case (enracOtherUser):
                mblnSwitchEntry = True
    End Select
                    
    Unload frmVerifyPwd
    mstrRefundCode = RefundReasonCode
    
    If mblnSwitchEntry = True Then Me.Hide

End Sub

Private Sub uctrRefund_OtherStoreTran(StoreNumber As String, TranDate As Date, TillID As String, TranNumber As String, RefundReasonCode As String)

    mstrTranNumber = TranNumber
    mdteTranDate = TranDate
    mstrTillID = TillID
    mstrStoreNumber = StoreNumber
    mblnOtherStore = True
    mstrRefundCode = RefundReasonCode
    Call Me.Hide

End Sub

Private Sub uctrRefund_SelectTransactionLine(strPartCode As String, _
                                             dblQuantity As Double, _
                                             curPrice As Currency, _
                                             curWEEERate As Currency, _
                                             strTranID As String, _
                                             dteTrandate As Date, _
                                             strTillID As String, _
                                             lngLineNo As Long, _
                                             blnVoided As Boolean, _
                                             blnTendered As Boolean, _
                                             OrderNumber As String)

'    mstrStoreNumber = StoreNumber
    mstrPartCode = strPartCode
    mdblQuantity = dblQuantity
    mcurPrice = curPrice
    mcurWEEERate = curWEEERate
    mstrTranNumber = strTranID
    mdteTranDate = dteTrandate
    mstrTillID = strTillID
    mlngLineNo = lngLineNo
    mblnVoided = blnVoided
    mOrderNumber = OrderNumber
    Call Me.Hide

End Sub

Private Sub uctrRefund_SelectTransactionLines(strTranID As String, dteTrandate As Date, strTillID As String, colLines As Collection, blnRetrieveAll As Boolean, blnVoided As Boolean, OrderNumber As String)

 
'    mstrPartCode = strPartCode
'    mdblQuantity = dblQuantity
'    mcurPrice = dblPrice
    mstrTranNumber = strTranID
    mdteTranDate = dteTrandate
    mstrTillID = strTillID
    Set mcolRefundLines = colLines
    mblnRetreiveAll = blnRetrieveAll
    mblnVoided = blnVoided
'    mlngLineNo = lngLineNo
    mOrderNumber = OrderNumber
    Call Me.Hide

End Sub

' Hubs 2.0

' Indicates that have used a Web Order Number to retrieve transaction details from Order Manager
Private Sub uctrRefund_WebOrderTran(WebOrderNumber As String, StoreNumber As String, TranDate As Date, TillID As String, TranNumber As String, RefundReasonCode As String)

    ' Save web order transaction details
    mstrTranNumber = TranNumber
    mdteTranDate = TranDate
    mstrTillID = TillID
    mstrStoreNumber = StoreNumber
    mstrRefundCode = RefundReasonCode
    ' Flag up is a web order and provide the number
    mblnWebOrder = True
    mstrWebOrderNumber = WebOrderNumber
    ' At some point in future, SKUs will also be brought back from Order Manager (as well as the tran details)
    ' but not yet, so shutdown to allow manual SKU entry
    Call Me.Hide
End Sub
' End of Hubs 2.0

Private Sub LoadRefundTrans()
'Dim cTrans As uctr clsTranRefund
Dim lngItemNo As Long
Dim intLineNo As Integer
Dim cTrans As clsTranRefund

    Set mcolTrans = New Collection
    intLineNo = 1
    With frmTill
        For lngItemNo = 2 To .sprdLines.MaxRows Step 1
            .sprdLines.Row = lngItemNo
            .sprdLines.Col = COL_VOIDED
            If (Val(.sprdLines.Value) = 0) Then
                .sprdLines.Col = COL_SALETYPE
                If (.sprdLines.Value <> "D") Then
                    Set cTrans = New clsTranRefund
                    cTrans.LineNo = intLineNo
                    .sprdLines.Col = COL_PARTCODE
                    cTrans.PartCode = .sprdLines.Text
                    .sprdLines.Col = COL_QTY
                    cTrans.Quantity = Val(.sprdLines.Text) * -1
                    .sprdLines.Col = COL_SELLPRICE
                    cTrans.Price = Val(.sprdLines.Text)
                    intLineNo = intLineNo + 1
                    Call mcolTrans.Add(cTrans)
                End If
            End If
        Next lngItemNo
    End With
End Sub

Private Function EnableResetVoidedFlag() As Boolean
    Dim ImplementationFactory As New RetrieveTranFormFactory
    Dim ResetVoidedFlagImplementation As ITxnLookupResetVoidFlag
    
    Set ResetVoidedFlagImplementation = ImplementationFactory.FactoryGetTransactionLookupResetVoidedFlag
    
    EnableResetVoidedFlag = ResetVoidedFlagImplementation.ResetVoidedFlag
End Function
