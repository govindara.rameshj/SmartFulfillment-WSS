VERSION 5.00
Object = "{9A526B11-3010-11D5-99E4-000102897E9C}#2.2#0"; "NumTextControl.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{8DDE6232-1BB0-11D0-81C3-0080C7A2EF7D}#3.0#0"; "Flp32a30.ocx"
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Begin VB.Form frmCapturePricePromDetails 
   BackColor       =   &H80000000&
   Caption         =   "Capture Customer Details"
   ClientHeight    =   8250
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11250
   ControlBox      =   0   'False
   Icon            =   "frmCapturePricePromDet.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   ScaleHeight     =   8250
   ScaleWidth      =   11250
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraOrigTran 
      Caption         =   "Original Transaction Details"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2355
      Left            =   6600
      TabIndex        =   29
      Top             =   540
      Width           =   4575
      Begin VB.Label lblWEEERate 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   420
         Left            =   3480
         TabIndex        =   43
         Top             =   1800
         Width           =   975
      End
      Begin VB.Label lblWEEElbl 
         BackStyle       =   0  'Transparent
         Caption         =   "PRF"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2760
         TabIndex        =   42
         Top             =   1860
         Width           =   735
      End
      Begin VB.Label Label13 
         BackStyle       =   0  'Transparent
         Caption         =   "Original Price"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   120
         TabIndex        =   38
         Top             =   1860
         Width           =   1635
      End
      Begin VB.Label lblOrigStore 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   420
         Left            =   780
         TabIndex        =   32
         Top             =   360
         Width           =   3435
      End
      Begin VB.Label Label11 
         BackStyle       =   0  'Transparent
         Caption         =   "Till - Tran No"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   120
         TabIndex        =   35
         Top             =   1380
         Width           =   1635
      End
      Begin VB.Label lblOrigPrice 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   420
         Left            =   1620
         TabIndex        =   39
         Top             =   1800
         Width           =   1095
      End
      Begin VB.Label lblOrigTillNo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   420
         Left            =   1620
         TabIndex        =   36
         Top             =   1320
         Width           =   495
      End
      Begin VB.Label lblOrigDate 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   420
         Left            =   1620
         TabIndex        =   34
         Top             =   840
         Width           =   1455
      End
      Begin VB.Label lblOrigTranNo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   420
         Left            =   2280
         TabIndex        =   37
         Top             =   1320
         Width           =   795
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Date"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   120
         TabIndex        =   33
         Top             =   900
         Width           =   1155
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Store"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   120
         TabIndex        =   31
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.Frame fraCaptCompDetails 
      Caption         =   "Competitor Details"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3495
      Left            =   60
      TabIndex        =   5
      Top             =   480
      Width           =   6495
      Begin LpLib.fpCombo cmbCompName 
         Height          =   360
         Left            =   1260
         TabIndex        =   7
         Top             =   300
         Width           =   3915
         _Version        =   196608
         _ExtentX        =   6906
         _ExtentY        =   635
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   0   'False
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Text            =   ""
         Columns         =   0
         Sorted          =   1
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         WrapList        =   0   'False
         WrapWidth       =   0
         AutoSearch      =   1
         SearchMethod    =   0
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   1
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   2
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         DataFieldList   =   ""
         ColumnEdit      =   -1
         ColumnBound     =   -1
         Style           =   0
         MaxDrop         =   8
         ListWidth       =   -1
         EditHeight      =   -1
         GrayAreaColor   =   -2147483633
         ListLeftOffset  =   0
         ComboGap        =   -2
         MaxEditLen      =   150
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   0   'False
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         ListPosition    =   0
         ButtonThreeDAppearance=   0
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         AutoSearchFill  =   0   'False
         AutoSearchFillDelay=   500
         EditMarginLeft  =   1
         EditMarginTop   =   1
         EditMarginRight =   0
         EditMarginBottom=   3
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         AutoMenu        =   -1  'True
         EditAlignH      =   0
         EditAlignV      =   0
         ColDesigner     =   "frmCapturePricePromDet.frx":058A
      End
      Begin VB.Frame fraCompPriceWEEE 
         BorderStyle     =   0  'None
         Height          =   1815
         Left            =   60
         TabIndex        =   17
         Top             =   1140
         Visible         =   0   'False
         Width           =   6375
         Begin VB.Label lblCompPriceWEEE 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Confirm competitor price includes PRF charge"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   435
            Left            =   180
            TabIndex        =   18
            Top             =   1320
            Width           =   6075
         End
      End
      Begin VB.CommandButton cmdGetCompetitor 
         Caption         =   "Retrieve"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   5220
         TabIndex        =   41
         Top             =   240
         Width           =   1215
      End
      Begin NumTextControl.NumText ntxtCompPrice 
         Height          =   315
         Left            =   1260
         TabIndex        =   20
         Top             =   3060
         Width           =   1155
         _ExtentX        =   2037
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   12
         SelStart        =   4
         Text            =   "0.00"
      End
      Begin VB.TextBox txtCompAdd4 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1260
         MaxLength       =   30
         TabIndex        =   14
         Top             =   2220
         Width           =   3915
      End
      Begin VB.TextBox txtVATInc 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   6060
         MaxLength       =   1
         TabIndex        =   24
         Text            =   "Y"
         Top             =   3060
         Width           =   315
      End
      Begin VB.TextBox txtCompTelNo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1260
         TabIndex        =   16
         Top             =   2580
         Width           =   3255
      End
      Begin VB.TextBox txtCompPostCode 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1260
         TabIndex        =   9
         Top             =   720
         Width           =   1695
      End
      Begin VB.TextBox txtCompAdd3 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1260
         MaxLength       =   30
         TabIndex        =   13
         Top             =   1860
         Width           =   3915
      End
      Begin VB.TextBox txtCompAdd2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1260
         MaxLength       =   30
         TabIndex        =   12
         Top             =   1500
         Width           =   3915
      End
      Begin VB.TextBox txtCompAdd1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1260
         MaxLength       =   30
         TabIndex        =   11
         Top             =   1140
         Width           =   3915
      End
      Begin NumTextControl.NumText ntxtCompWEEE 
         Height          =   315
         Left            =   3480
         TabIndex        =   22
         Top             =   3060
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   12
         SelStart        =   4
         Text            =   "0.00"
      End
      Begin VB.Label lblCompWEEE 
         BackStyle       =   0  'Transparent
         Caption         =   "PRF"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2880
         TabIndex        =   21
         Top             =   3060
         Width           =   615
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "VAT Inclusive"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4560
         TabIndex        =   23
         Top             =   3060
         Width           =   1575
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Price"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   120
         TabIndex        =   19
         Top             =   3060
         Width           =   1035
      End
      Begin VB.Label lblCompPhoneNum 
         BackStyle       =   0  'Transparent
         Caption         =   "Telephone"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   15
         Top             =   2640
         Width           =   1695
      End
      Begin VB.Label lblCompPostcode 
         BackStyle       =   0  'Transparent
         Caption         =   "Postcode"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         TabIndex        =   8
         Top             =   720
         Width           =   1155
      End
      Begin VB.Label lblCompAddress 
         BackStyle       =   0  'Transparent
         Caption         =   "Address"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   10
         Top             =   1140
         Width           =   1395
      End
      Begin VB.Label lblCompName 
         BackStyle       =   0  'Transparent
         Caption         =   "Name"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   6
         Top             =   300
         Width           =   1215
      End
   End
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   60
      Top             =   4020
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin NumTextControl.NumText ntxtNewPrice 
      Height          =   435
      Left            =   7980
      TabIndex        =   25
      Top             =   2940
      Width           =   1395
      _ExtentX        =   2461
      _ExtentY        =   767
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxLength       =   12
      SelStart        =   4
      Text            =   "0.00"
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6600
      TabIndex        =   30
      Top             =   3480
      Width           =   1335
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   9540
      TabIndex        =   28
      Top             =   3480
      Width           =   1635
   End
   Begin VB.CommandButton cmdAccept 
      Caption         =   "Accept"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   7980
      TabIndex        =   27
      Top             =   3480
      Width           =   1515
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   40
      Top             =   7875
      Width           =   11250
      _ExtentX        =   19844
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmCapturePricePromDet.frx":08C8
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12012
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   635
            MinWidth        =   635
            Picture         =   "frmCapturePricePromDet.frx":126E
            Key             =   "NumPad"
            Object.ToolTipText     =   "Show/Hide Num Pad"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "15:39"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblNewWEEE 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   10200
      TabIndex        =   45
      Top             =   2940
      Width           =   975
   End
   Begin VB.Label lblNewWEEElbl 
      BackStyle       =   0  'Transparent
      Caption         =   "PRF"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   9420
      TabIndex        =   44
      Top             =   3000
      Width           =   795
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Current Price"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7980
      TabIndex        =   3
      Top             =   60
      Width           =   1935
   End
   Begin VB.Label lblCurrentPrice 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H0080FFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   9900
      TabIndex        =   4
      Top             =   60
      Width           =   1275
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Override to"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   6600
      TabIndex        =   26
      Top             =   3000
      Width           =   1575
   End
   Begin VB.Label lblDescr 
      Appearance      =   0  'Flat
      BackColor       =   &H0080FFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1860
      TabIndex        =   2
      Top             =   60
      Width           =   6015
   End
   Begin VB.Label lblSKU 
      Appearance      =   0  'Flat
      BackColor       =   &H0080FFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "000000"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   720
      TabIndex        =   1
      Top             =   60
      Width           =   1095
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "Item"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   1455
   End
End
Attribute VB_Name = "frmCapturePricePromDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const MODULE_NAME As String = "frmCapturePricePromDetails"

Dim moTranHdr         As cPOSHeader
Dim mlngLineNo        As Long
Dim mcurTranPrice     As Double
Dim mblnCompPCEdited  As Boolean
Dim mblnFirstFocus    As Boolean
Dim mlngResetKeyCode  As Long
Dim mlngCloseKeyCode  As Long
Dim mlngUseKeyCode    As Long
Dim mlngFindKeyCode   As Long

Dim mstrCompName      As String

Dim mstrCustName As String
Dim mstrAddress  As String
Dim mstrTelNo    As String
Dim mstrPostCode As String

Dim mdblPMMultiplier As Double
Dim mcurPMMaximum    As Currency
Dim mblnFlexPrice    As Boolean

Dim mcurVATRate      As Currency
Dim mcurWEEERate     As Currency

Public Event Duress()
Private mintDuressKeyCode As Integer
Private mfrmRetrieveTran As frmRetrieveTran

Dim mlngClickX  As Long
Dim mlngClickY  As Long


Public Function CapturePricePromise(ByRef oTranHeader As cPOSHeader, _
                                    ByRef strSKU As String, _
                                    ByRef strDesc As String, _
                                    ByRef lngLineNo As Long, _
                                    ByRef curVATRate As Currency, _
                                    ByRef curTranPrice As Currency, _
                                    ByVal curWEEERate As Currency, _
                                    ByRef strCustName As String, _
                                    ByRef strAddress As String, _
                                    ByRef strTelNo As String, _
                                    ByRef strPostCode As String, _
                                    ByRef curNewPrice As Currency, _
                                    ByRef curOrigPrice As Currency, _
                                    ByRef curCompPrice As Currency, _
                                    ByRef blnVATInc As Boolean, _
                                    ByRef curCompPriceExc As Currency, _
                                    ByVal dblPMMultiplier As Double, _
                                    ByVal curPMMaximum As Currency, _
                                    ByVal blnManagerFlex As Boolean, _
                                    ByVal lngQuantityReq As Long, _
                                    ByRef lngValidQuantity As Long, _
                                    ByRef frmRetrieveTran As frmRetrieveTran) As Boolean
                                    
Dim Address
Dim lngAddLine  As Long
Dim SmartPriceMatch As COMTPWickes_InterOp_Interface.IPriceMatch
Dim Factory As New COMTPWickes_InterOp_Wrapper.PriceMatch
Set SmartPriceMatch = Factory.FactoryGet
        
    lngValidQuantity = lngQuantityReq 'in validated transactions - check that Price Match Qty does not exceed orig qty
    Set moTranHdr = oTranHeader
    Set mfrmRetrieveTran = frmRetrieveTran
    mlngLineNo = lngLineNo
    mcurTranPrice = curTranPrice
    lblCurrentPrice.Caption = Format(curTranPrice, "0.00") & " "
    lblSKU.Caption = strSKU
    lblDescr.Caption = strDesc
    ntxtCompPrice.Value = 0
    ntxtNewPrice.Value = 0
    mcurWEEERate = curWEEERate
    If (curWEEERate <> 0) Then
        lblNewWEEElbl.Visible = True
        lblNewWEEE.Visible = True
    Else
        lblNewWEEElbl.Visible = False
        lblNewWEEE.Visible = False
        lblCompPriceWEEE.Visible = False
        lblCompWEEE.Visible = False
        ntxtCompWEEE.Visible = False
        lblWEEElbl.Visible = False
        lblWEEERate.Visible = False
    End If
    lblNewWEEE.Caption = Format(curWEEERate, "0.00")

    mdblPMMultiplier = dblPMMultiplier
    mcurPMMaximum = curPMMaximum
    mblnFlexPrice = blnManagerFlex
    
    mcurVATRate = (curVATRate / 100) + 1
    
    cmbCompName.Text = ""
    mblnCompPCEdited = False
    
    mstrCustName = strCustName
    mstrAddress = strAddress
    mstrTelNo = strTelNo
    mstrPostCode = strPostCode
    
   If (mblnHasEFT = True) And (mblnHasCashDrawer = True) Then
        If (MsgBoxEx("Is this a Price Promise for previously purchased items?" & vbNewLine & vbNewLine & "NOTE : The system will automatically add the refund line in, if previously purchased", vbInformation + vbYesNo, , , , , , RGBMSGBox_PromptColour) = vbYes) Then
            '''' capture details if CR008 is on and they haven't been taken already
            If GetBooleanParameter(-8, goDatabase.Connection) Then
                If (strCustName = "") Then
                    If (frmCustomerAddress.GetPricePromiseAddress(strCustName, strPostCode, strAddress, strTelNo) = False) Then
                        'cancel pressed so exit back to Reason List
                        'Call LoadReasonCodes(enrmPriceOverride)
                        CapturePricePromise = False
                        Exit Function
                    End If
                End If
                Call frmTill.DisableOrderButtons(False)
            End If

            Call RetrieveOriginalTran(lngQuantityReq, lngValidQuantity)
            If (lblOrigTranNo.Caption <> "") Then
                'Change request 008: New Price match Promise
                If GetBooleanParameter(-8, goDatabase.Connection) Then
                    Call SmartPriceMatch.CalculateOverridePrice(strSKU, strDesc, CInt(lblOrigStore.Caption), CDate(lblOrigDate.Caption), CLng(lblOrigTillNo.Caption), CLng(lblOrigTranNo.Caption), CDbl(lblOrigPrice.Caption), CDbl(curNewPrice), CDbl(curVATRate))
                    If SmartPriceMatch.CompetitorName = "" Then
                        Call frmTill.DisableOrderButtons(True)
                    End If
                Else
                    Call Me.Show(vbModal)
                End If
            End If
        Else
            'Change request 008: New Price match Promise
            If GetBooleanParameter(-8, goDatabase.Connection) Then
                Call SmartPriceMatch.CalculateOverridePrice(strSKU, strDesc, 0, Now(), 1, 0, CDbl(curOrigPrice), CDbl(curNewPrice), CDbl(curVATRate))
            Else
                Call Me.Show(vbModal)
            End If
        End If
    Else
        If GetBooleanParameter(-8, goDatabase.Connection) Then
            Call SmartPriceMatch.CalculateOverridePrice(strSKU, strDesc, 0, Now(), 1, 0, CDbl(curOrigPrice), CDbl(curNewPrice), CDbl(curVATRate))
        Else
            Call Me.Show(vbModal)
        End If
    End If

    
    'Pass out values to calling routine
    If GetBooleanParameter(-8, goDatabase.Connection) Then
        cmbCompName.Text = SmartPriceMatch.CompetitorName
        If SmartPriceMatch.CompetitorName <> "" Then
            curNewPrice = SmartPriceMatch.Overrideprice
            curOrigPrice = SmartPriceMatch.OriginalPrice
            If SmartPriceMatch.VATInclusive Then
                curCompPrice = SmartPriceMatch.CompetitorPrice 'ntxtCompPrice.Value
                blnVATInc = True
                curCompPriceExc = 0
            Else
                curCompPrice = SmartPriceMatch.CompetitorPrice
                blnVATInc = False
                curCompPriceExc = Round(SmartPriceMatch.CompetitorPrice * mcurVATRate, 2)
            End If
            Call modTill.SavePPDetailsNew(oTranHeader, mlngLineNo, strCustName, strAddress, strPostCode, strTelNo, SmartPriceMatch.CompetitorName, curCompPrice, SmartPriceMatch.VATInclusive, curCompPriceExc, lblOrigStore.Caption, GetDate(lblOrigDate.Caption), lblOrigTillNo.Caption, lblOrigTranNo.Caption, curOrigPrice)
        End If
    Else
        If (cmbCompName.Text <> "") Then
            curNewPrice = ntxtNewPrice.Value
            curOrigPrice = Val(lblOrigPrice.Caption)
            If (txtVATInc.Text = "Y") Then
                curCompPrice = ntxtCompPrice.Value
                blnVATInc = True
                curCompPriceExc = 0
            Else
                curCompPrice = Round(ntxtCompPrice.Value * mcurVATRate, 2)
                blnVATInc = False
                curCompPriceExc = ntxtCompPrice.Value
            End If
        End If
    
    End If
    
    CapturePricePromise = (cmbCompName.Text <> "")
End Function 'CaptureCustomerDetails

Private Sub cmbCompName_CloseUp()

    If (ucKeyPad1.Visible = False) Then Call cmbCompName_KeyPress(vbKeyReturn)

End Sub

Private Sub cmbCompName_GotFocus()

    If (cmbCompName.ListCount > 0) Then cmbCompName.ListDown = True
    mstrCompName = cmbCompName.Text
    If Len(cmbCompName.Text) > 0 Then
        cmbCompName.SelStart = 0
        cmbCompName.SelLength = Len(cmbCompName.Text)
    End If
    cmbCompName.BackColor = RGBEdit_Colour

End Sub

Private Sub cmbCompName_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If ((KeyCode = vbKeyUp) Or (KeyCode = vbKeyDown)) And (cmbCompName.ListDown = False) And (cmbCompName.ListCount > 0) Then cmbCompName.ListDown = True

End Sub

Private Sub cmbCompName_KeyPress(KeyAscii As Integer)

    DoEvents
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call DebugMsg(MODULE_NAME, "cmbCompname_keyPress", endlDebug, "Old='" & mstrCompName & "'-New='" & cmbCompName.Text & "'")
'        If (cmbCompName.Text <> mstrCompName) Then
            txtCompAdd1.Text = vbNullString
            txtCompAdd2.Text = vbNullString
            txtCompAdd3.Text = vbNullString
            txtCompAdd4.Text = vbNullString
            txtCompPostCode.Text = vbNullString
            txtCompTelNo.Text = vbNullString
            If (cmbCompName.ListIndex <> -1) Then
                Call DisplayCompetitor
                If (ntxtCompPrice.Visible = True) And (ntxtCompPrice.Enabled = True) Then Call ntxtCompPrice.SetFocus
                Exit Sub
            End If
 '       End If
        If (txtCompPostCode.Visible = True) And (txtCompPostCode.Enabled = True) Then Call txtCompPostCode.SetFocus
        Exit Sub
    End If
    
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
        KeyAscii = 0
        Exit Sub
    End If
    If (cmbCompName.ListDown = False) And (cmbCompName.ListCount > 0) Then cmbCompName.ListDown = True

End Sub

Private Sub cmbCompName_LostFocus()

    cmbCompName.BackColor = RGB_WHITE

End Sub

Private Sub cmdAccept_Click()

Dim curOrigPrice As Currency

    If (cmbCompName.Text = "") Then
        Call MsgBoxEx("Competitors Name has not been captured." & vbNewLine & "Ensure Name has been entered.", vbOKOnly, "Missing Competitor Details", , , , , RGBMsgBox_WarnColour)
        If (cmbCompName.Visible = True) And (cmbCompName.Enabled = True) Then Call cmbCompName.SetFocus
        Exit Sub
    End If

'    If (txtCompPostCode.Text = "") Or (txtCompAdd1.Text = "") Or (txtCompTelNo.Text = "") Then
'        Call MsgBoxEx("Not all of the required competitors details have been captured." & vbNewLine & "Ensure Name, Address, PostCode and Telephone have been entered.", vbOKOnly, "Missing Competitor Details", , , , , RGBMsgBox_WarnColour)
'        Call txtCompPostCode.SetFocus
'        Exit Sub
'    End If

    If (ntxtCompPrice.Value = 0) Then
        Call MsgBoxEx("Competitors price has not been captured." & vbNewLine & "Unable to proceed.", vbOKOnly, "Missing Competitor Details", , , , , RGBMsgBox_WarnColour)
        If (ntxtCompPrice.Visible = True) And (ntxtCompPrice.Enabled = True) Then Call ntxtCompPrice.SetFocus
        Exit Sub
    End If
    
    If (ntxtNewPrice.Value = 0) Then
        Call MsgBoxEx("Override price to, has not been captured." & vbNewLine & "Unable to proceed.", vbOKOnly, "Missing Competitor Details", , , , , RGBMsgBox_WarnColour)
        If (ntxtNewPrice.Visible = True) And (ntxtNewPrice.Enabled = True) Then Call ntxtNewPrice.SetFocus
        Exit Sub
    End If

    'Validate values entered
    'Get starting price from STKMAS, or from original sale price
    curOrigPrice = Val(lblCurrentPrice.Caption)
    If (Val(lblOrigPrice.Caption) > 0) Then curOrigPrice = Val(lblOrigPrice.Caption)
    
    Call DebugMsg(MODULE_NAME, "cmdAccept", endlDebug, "New=" & ntxtNewPrice.Value & " was " & curOrigPrice)
    'Check item is actually been discounted
    If ((ntxtNewPrice.Value + Val(lblNewWEEE.Caption)) > Val(lblCurrentPrice.Caption)) Then
        Call MsgBoxEx("New Price is HIGHER than retail", vbOKOnly, _
                          "Invalid price entered", , , , , RGBMsgBox_WarnColour)
        ntxtNewPrice.Value = Val(lblCurrentPrice.Caption) - Val(lblNewWEEE.Caption)
        If (ntxtNewPrice.Visible = True) And (ntxtNewPrice.Enabled = True) Then Call ntxtNewPrice.SetFocus
        Exit Sub
    End If
    
    'perform simple value check
    If ((curOrigPrice - ntxtNewPrice.Value) > 999.99) Then
        If MsgBoxEx("Price entered adjusts prices by more than 999.99" & _
                    vbNewLine & vbNewLine & "Continue?", vbYesNo, _
                    "Suspicious price entered", , , , , RGBMsgBox_WarnColour) = vbNo Then
            ntxtNewPrice.Value = curOrigPrice
            If (ntxtNewPrice.Visible = True) And (ntxtNewPrice.Enabled = True) Then ntxtNewPrice.SetFocus
            Exit Sub
        End If
    End If
    
    Call SaveDetails

End Sub

Private Sub cmdAccept_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub cmdCancel_Click()

    cmdReset.Value = True
    Call Me.Hide

End Sub

Private Sub cmdCancel_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub cmdGetCompetitor_Click()

    Call cmbCompName_KeyPress(vbKeyReturn)

End Sub

Private Sub cmdReset_Click()

    mblnCompPCEdited = False
    lblOrigStore.Caption = ""
    lblOrigDate.Caption = ""
    lblOrigTillNo.Caption = ""
    lblOrigTranNo.Caption = ""
    lblOrigPrice.Caption = ""
    lblWEEElbl.Visible = False
    lblWEEERate.Visible = False
    lblWEEERate.Caption = ""
    
    cmbCompName.Text = ""
    txtCompPostCode.Text = ""
    txtCompAdd1.Text = ""
    txtCompAdd2.Text = ""
    txtCompAdd3.Text = ""
    txtCompAdd4.Text = ""
    txtCompTelNo.Text = ""
    ntxtCompPrice.Value = 0
    
    If (cmbCompName.Enabled = True) And (cmbCompName.Visible = True) Then Call cmbCompName.SetFocus

End Sub

Private Sub cmdReset_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub RetrieveOriginalTran(lngReqQty As Long, lngValidQty As Long)

Dim strStoreNo  As String
Dim dteTrandate As Date
Dim strTillNo   As String
Dim strTranNo   As String
Dim curPrice    As Currency
Dim curWEEERate As Currency

    Load mfrmRetrieveTran
    Call mfrmRetrieveTran.SelectPricePromiseLine(lblSKU.Caption, strStoreNo, dteTrandate, strTillNo, strTranNo, curPrice, curWEEERate, lngReqQty, lngValidQty)
    Unload mfrmRetrieveTran
    DoEvents
    Screen.MousePointer = vbNormal
    If (strTranNo <> "") Then
        lblOrigStore.Caption = strStoreNo
        lblOrigDate.Caption = DisplayDate(dteTrandate, False)
        lblOrigTillNo.Caption = strTillNo
        lblOrigTranNo.Caption = strTranNo
        lblOrigPrice.Caption = Format(curPrice, "0.00")
        If (curWEEERate <> 0) Then
            lblWEEElbl.Visible = True
            lblWEEERate.Visible = True
            lblWEEERate.Caption = Format(curWEEERate, "0.00")
        End If
    End If

End Sub

Private Sub ntxtCompWEEE_Change()
    
    If (ntxtCompWEEE.Value < 0) Then ntxtCompWEEE.Value = Abs(ntxtCompWEEE.Value)

End Sub

Private Sub ntxtCompWEEE_GotFocus()
    
    ntxtCompWEEE.BackColor = RGBEdit_Colour

End Sub

Private Sub ntxtCompWEEE_LostFocus()
    
    ntxtCompWEEE.BackColor = RGB_WHITE
    If (mblnFlexPrice = False) Then ntxtNewPrice.Value = CalculateNewPrice

End Sub

Private Sub ucKeyPad1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

    ucKeyPad1.BackColor = vbGrayText
    mlngClickX = X
    mlngClickY = Y

End Sub
Private Sub ucKeyPad1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    If Button = 1 And Shift = 0 Then
        ucKeyPad1.Left = ucKeyPad1.Left + X - mlngClickX
        ucKeyPad1.Top = ucKeyPad1.Top + Y - mlngClickY
    End If

End Sub

Private Sub ucKeyPad1_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  
  ucKeyPad1.BackColor = RGB(0, 64, 0)
    
End Sub

Private Sub sbStatus_PanelClick(ByVal Panel As MSComctlLib.Panel)

Dim blnShow As Boolean

    Select Case (Panel.Key)
        Case ("NumPad"):
                    If ((Panel.Picture Is Nothing) = False) Then
                        ucKeyPad1.Visible = Not ucKeyPad1.Visible
                        If (ucKeyPad1.Visible = True) Then
                            Me.Height = ucKeyPad1.Top + ucKeyPad1.Height + sbStatus.Height + 440
                            Call CentreForm(Me)
                        Else
                            Me.Height = ucKeyPad1.Top + sbStatus.Height + 440
                            Call CentreForm(Me)
                        End If
                    End If
    End Select

End Sub

Private Sub Form_Activate()

    Call DebugMsg(MODULE_NAME, "CaptCust_Activate", endlDebug, "Activate-First=" & mblnFirstFocus)
    If (mblnFirstFocus = False) Then
        If (cmbCompName.Text = "") And (cmbCompName.Enabled = True) And (cmbCompName.Visible = True) Then cmbCompName.SetFocus
        mblnFirstFocus = True
    End If
    
End Sub

Public Sub InitialiseStatusBar()
    
    sbStatus.Panels(PANEL_VERNO).Text = "Ver. " & App.Major & "." & App.Minor & "." & App.Revision
    sbStatus.Panels(PANEL_VERNO).ToolTipText = "Program Version Number"
    sbStatus.Panels(PANEL_WSID + 1).Text = Right$("00" & goSession.CurrentEnterprise.IEnterprise_WorkstationID, 2) & " "
    sbStatus.Panels(PANEL_WSID + 1).ToolTipText = "Current Work-Station Number"
    sbStatus.Panels(PANEL_DATE + 1).Text = Format$(Date, "DD-MMM-YY")
    If (mblnShowKeypadIcon = False) Then sbStatus.Panels("NumPad").Picture = Nothing
    
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case (KeyCode)
        Case (mlngResetKeyCode):
            If cmdReset.Visible Then Call cmdReset_Click
        Case (mlngCloseKeyCode):
            If cmdCancel.Visible Then Call cmdCancel_Click
        Case (mlngUseKeyCode):
            If cmdAccept.Visible Then Call cmdAccept_Click
        Case (mlngFindKeyCode):
            If cmdGetCompetitor.Visible Then Call cmdGetCompetitor_Click
    End Select

'    If mintDuressKeyCode <> 0 And KeyCode = mintDuressKeyCode Then
'        KeyCode = 0
'        RaiseEvent Duress
'    End If

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)


    If (KeyAscii = vbKeyReturn) Then
        If ActiveControl Is Nothing = False Then
            If TypeOf ActiveControl Is TextBox Then
                Call SendKeys(vbTab)
                KeyAscii = 0
            End If
            If TypeOf ActiveControl Is CheckBox Then
                Call SendKeys(vbTab)
                KeyAscii = 0
            End If
            If (Left(ActiveControl.Name, 4) = "ntxt") Then
                Call SendKeys(vbTab)
                KeyAscii = 0
            End If
        End If
    End If

    If (KeyAscii = vbKeyEscape) Then
        If ActiveControl Is Nothing = False Then
            If TypeOf ActiveControl Is TextBox Then
                Call SendKeys("+{tab}")
                KeyAscii = 0
            End If
            If TypeOf ActiveControl Is CheckBox Then
                Call SendKeys("+{tab}")
                KeyAscii = 0
            End If
            If (Left(ActiveControl.Name, 4) = "ntxt") Then
                Call SendKeys("+{tab}")
                KeyAscii = 0
            End If
        End If
    End If

End Sub

Private Sub Form_Load()

Dim strKey  As String

    If (goSession.GetParameter(PRM_COUNTRY_CODE) = "IE") Then
        txtCompAdd1.Top = txtCompPostCode.Top
        txtCompAdd2.Top = txtCompAdd1.Top + 420
        txtCompAdd3.Top = txtCompAdd2.Top + 420
        txtCompAdd4.Top = txtCompAdd3.Top + 420
        txtCompPostCode.Visible = False
        lblCompAddress.Top = lblCompPostcode.Top
        lblCompPostcode.Visible = False
    End If
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    fraCaptCompDetails.BackColor = Me.BackColor
    fraOrigTran.BackColor = Me.BackColor
    fraCompPriceWEEE.BackColor = Me.BackColor
'    chkVATInc.BackColor = Me.BackColor
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
    RGBMSGBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
    mintDuressKeyCode = goSession.GetParameter(PRM_KEY_DURESS)
    
    Call InitialiseStatusBar
    
    strKey = goSession.GetParameter(PRM_KEY_RESET)
    cmdReset.Caption = strKey & "-" & cmdReset.Caption
    If Left$(strKey, 1) = "F" Then
        mlngResetKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngResetKeyCode = AscW(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_CLOSE)
    cmdCancel.Caption = strKey & "-" & cmdCancel.Caption
    If Left$(strKey, 1) = "F" Then
        mlngCloseKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngCloseKeyCode = AscW(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_SAVE)
    cmdAccept.Caption = strKey & "-" & cmdAccept.Caption
    If Left$(strKey, 1) = "F" Then
        mlngUseKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngUseKeyCode = AscW(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_SEARCH)
    cmdGetCompetitor.Caption = strKey & "-" & cmdGetCompetitor.Caption
    If Left$(strKey, 1) = "F" Then
        mlngFindKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngFindKeyCode = AscW(UCase$(strKey))
    End If
    
    ucKeyPad1.Visible = False
    Me.Height = ucKeyPad1.Top + sbStatus.Height + 440
    Call CentreForm(Me)
    
    Call GetCompetitorsList

End Sub 'Form_Load



Private Sub ntxtCompPrice_Change()
    
    If (ntxtCompPrice.Value < 0) Then ntxtCompPrice.Value = Abs(ntxtCompPrice.Value)

End Sub

Private Sub ntxtCompPrice_GotFocus()
    
    ntxtCompPrice.BackColor = RGBEdit_Colour
    fraCompPriceWEEE.Visible = ntxtCompWEEE.Visible

End Sub

Private Sub ntxtCompPrice_LostFocus()

    fraCompPriceWEEE.Visible = False
    ntxtCompPrice.BackColor = RGB_WHITE
    If (mblnFlexPrice = False) Then ntxtNewPrice.Value = CalculateNewPrice
    
End Sub

Private Sub ntxtNewPrice_Change()

    If (ntxtNewPrice.Value < 0) Then ntxtNewPrice.Value = Abs(ntxtNewPrice.Value)

End Sub

Private Sub ntxtNewPrice_GotFocus()
    
    ntxtNewPrice.BackColor = RGBEdit_Colour

End Sub

Private Sub ntxtNewPrice_LostFocus()
    
    ntxtNewPrice.BackColor = RGB_WHITE

End Sub

Private Sub txtCompAdd1_GotFocus()

    txtCompAdd1.BackColor = RGBEdit_Colour
    
End Sub

Private Sub txtCompAdd1_LostFocus()

    txtCompAdd1.BackColor = RGB_WHITE

End Sub

Private Sub txtCompAdd2_GotFocus()

    txtCompAdd2.BackColor = RGBEdit_Colour
    
End Sub

Private Sub txtCompAdd2_LostFocus()

    txtCompAdd2.BackColor = RGB_WHITE

End Sub

Private Sub txtCompAdd3_GotFocus()

    txtCompAdd3.BackColor = RGBEdit_Colour
    
End Sub

Private Sub txtCompAdd3_LostFocus()

    txtCompAdd3.BackColor = RGB_WHITE

End Sub

Private Sub txtCompAdd4_GotFocus()

    txtCompAdd4.BackColor = RGBEdit_Colour
    
End Sub

Private Sub txtCompAdd4_LostFocus()

    txtCompAdd4.BackColor = RGB_WHITE

End Sub



Private Sub txtCompTelNo_GotFocus()

    txtCompTelNo.BackColor = RGBEdit_Colour
    
End Sub

Private Sub txtCompTelNo_LostFocus()

    txtCompTelNo.BackColor = RGB_WHITE

End Sub

Private Sub txtCompAdd1_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub




Private Sub txtCompPostCode_Change()
    
    cmdReset.Visible = True
    mblnCompPCEdited = True

End Sub

Private Sub txtCompPostCode_GotFocus()

    txtCompPostCode.BackColor = RGBEdit_Colour
    
End Sub

Private Sub txtCompAdd2_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub txtCompAdd3_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub txtCompAdd4_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub txtCompTelNo_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub txtCompPostcode_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

End Sub


Private Sub SaveDetails()

Dim vntAddress  As Variant

'    Call modTill.SavePPDetails(moTranHeader, mstrCustName, mstrAddress, mstrPostCode, _
'                    mstrTelNo, mlngLineNo, cmbCompName.Text, txtCompAdd1.Text, txtCompAdd2.Text, _
'                    txtCompAdd3.Text, txtCompAdd4.Text, txtCompPostCode.Text, txtCompTelNo.Text, _
'                    IIf(txtVATInc.Text = "N", ntxtCompPrice.Value, ntxtCompPrice.Value), txtVATInc.Text = "Y", _
'                    IIf(txtVATInc.Text = "Y", 0, Round(ntxtCompPrice.Value * mcurVATRate, 2)), Left$(lblOrigStore.Caption, 3), GetDate(lblOrigDate.Caption), _
'                    lblOrigTillNo.Caption, lblOrigTranNo.Caption, Val(lblOrigPrice.Caption))
    
    'Added 10/07/07 as Capture address is now on other form, Address is 1 value, so split into lines
    If LenB(mstrAddress) <> 0 Then
        vntAddress = Split(mstrAddress, vbNewLine)
        mstrAddressLine1 = vntAddress(0)
        mstrAddressLine2 = vntAddress(1)
        mstrAddressLine3 = vntAddress(2)
    End If
    
    Call modTill.SavePPDetails(moTranHeader, mstrCustName, "", mstrAddressLine1, mstrAddressLine2, mstrAddressLine3, mstrPostCode, _
                    mstrTelNo, mlngLineNo, cmbCompName.Text, txtCompAdd1.Text, txtCompAdd2.Text, _
                    txtCompAdd3.Text, txtCompAdd4.Text, txtCompPostCode.Text, txtCompTelNo.Text, _
                    IIf(txtVATInc.Text = "N", ntxtCompPrice.Value, ntxtCompPrice.Value), txtVATInc.Text = "Y", _
                    IIf(txtVATInc.Text = "Y", 0, Round(ntxtCompPrice.Value * mcurVATRate, 2)), Left$(lblOrigStore.Caption, 3), GetDate(lblOrigDate.Caption), _
                    lblOrigTillNo.Caption, lblOrigTranNo.Caption, Val(lblOrigPrice.Caption))

    Call Me.Hide
          
End Sub 'Save

Private Function CalculateNewPrice() As Currency

Dim curNewPrice     As Currency
Dim curPriceDiff    As Currency
Dim curExtraDiff    As Currency

    If (txtVATInc.Text = "Y") Then
        curNewPrice = ntxtCompPrice.Value - ntxtCompWEEE.Value
    Else
        curNewPrice = Round((ntxtCompPrice.Value - ntxtCompWEEE.Value) * mcurVATRate, 2)
    End If
    Call DebugMsg(MODULE_NAME, "CalculateNewPrice", endlDebug, "Comp Price = " & curNewPrice)
    'Calculate difference between ours and competitors prices
    If (Val(lblOrigPrice.Caption) > 0) Then
        curPriceDiff = Val(lblOrigPrice.Caption) - curNewPrice
    Else
        curPriceDiff = Val(lblCurrentPrice.Caption) - curNewPrice - mcurWEEERate
    End If
    Call DebugMsg(MODULE_NAME, "CalculateNewPrice", endlDebug, "Comp Price = " & curNewPrice & ",Orig=" & lblOrigPrice.Caption & ",Diff=" & curPriceDiff & ",Mult=" & mdblPMMultiplier)
    'Apply factor and ensure does not exceed limit
    curExtraDiff = Round(curPriceDiff * (mdblPMMultiplier - 1), 2)
    If (curExtraDiff > mcurPMMaximum) And (mcurPMMaximum > 0) Then curExtraDiff = mcurPMMaximum
    Call DebugMsg(MODULE_NAME, "CalculateNewPrice", endlDebug, "New=" & curNewPrice - curExtraDiff & ",Comp=" & curNewPrice & ",Final Price Adj=" & curExtraDiff)
    
    CalculateNewPrice = curNewPrice - curExtraDiff
    
    If (CalculateNewPrice <= 0) Then CalculateNewPrice = 0.01

End Function

Private Sub GetCompetitorsList()

Dim oCompLineBO As cPriceLineInfo
Dim lngCompNo   As Long
Dim vntList     As Variant

Const PROCEDURE_NAME As String = MODULE_NAME & ".GetCompetitorsList"
 
    Set oCompLineBO = goDatabase.CreateBusinessObject(CLASSID_PRICELINEINFO)
    
    Call cmbCompName.Clear
        
    vntList = goDatabase.GetAggregateValue(AGG_DIST, oCompLineBO.GetField(FID_PRICELINEINFO_CompetitorName), Nothing)
    
    If (IsNull(vntList) = False) Then
        For lngCompNo = 0 To UBound(vntList) Step 1
            cmbCompName.AddItem (Trim(vntList(lngCompNo)))
            cmbCompName.ItemData(cmbCompName.NewIndex) = 1
        Next lngCompNo
    End If

    Set oCompLineBO = Nothing
    
    'if there are any lines on Price Lines on Tran then add any other competitors
    If ((moTranHeader.GetPricePromiseLine Is Nothing) = False) Then
        cmbCompName.SearchIgnoreCase = True
        cmbCompName.SearchMethod = SearchMethodExactMatch
        For lngCompNo = 1 To moTranHeader.GetPricePromiseLine.Count Step 1
            Set oCompLineBO = moTranHeader.GetPricePromiseLine(lngCompNo)
            cmbCompName.SearchText = oCompLineBO.CompetitorName
            cmbCompName.Action = ActionSearch
            If (cmbCompName.SearchIndex = -1) Then
                cmbCompName.AddItem (oCompLineBO.CompetitorName)
                cmbCompName.ItemData(cmbCompName.NewIndex) = lngCompNo * -1
            End If
        Next
    End If

End Sub      'GetCompetitorsList

Private Sub DisplayCompetitor()

Dim colCNames   As Collection
Dim oCompInfoBO As cPriceLineInfo
Dim lngCompNo   As Long
Dim vntList     As Variant

Const PROCEDURE_NAME As String = MODULE_NAME & ".DisplayCompetitors"
 
    If (cmbCompName.ItemData(cmbCompName.ListIndex) > 0) Then
        Set oCompInfoBO = goDatabase.CreateBusinessObject(CLASSID_PRICELINEINFO)
            
        Call oCompInfoBO.AddLoadFilter(CMP_EQUAL, FID_PRICELINEINFO_CompetitorName, cmbCompName.Text)
        Set colCNames = oCompInfoBO.LoadMatches
            
        If colCNames.Count = 0 Then Exit Sub
        Set oCompInfoBO = colCNames(1)
    Else
        Set oCompInfoBO = moTranHeader.GetPricePromiseLine(Abs(cmbCompName.ItemData(cmbCompName.ListIndex)))
    End If
    
    txtCompPostCode.Text = oCompInfoBO.CompetitorPostcode
    txtCompAdd1.Text = oCompInfoBO.CompetitorAddress1
    txtCompAdd2.Text = oCompInfoBO.CompetitorAddress2
    txtCompAdd3.Text = oCompInfoBO.CompetitorAddress3
    txtCompAdd4.Text = oCompInfoBO.CompetitorAddress4
    txtCompTelNo.Text = oCompInfoBO.CompetitorPhone
        
    Set oCompInfoBO = Nothing

End Sub 'DisplayCompetitor


Private Sub txtVATInc_GotFocus()
    
    fraCompPriceWEEE.Visible = False
    txtVATInc.BackColor = RGBEdit_Colour

End Sub

Private Sub txtVATInc_KeyPress(KeyAscii As Integer)

    If (UCase(Chr(KeyAscii)) = "Y") Or (UCase(Chr(KeyAscii)) = "N") Then
        txtVATInc.Text = UCase(Chr(KeyAscii))
        If (mblnFlexPrice = False) Then ntxtNewPrice.Value = CalculateNewPrice
    End If
    KeyAscii = 0

End Sub

Private Sub txtVATInc_LostFocus()

    txtVATInc.BackColor = RGB_WHITE

End Sub
