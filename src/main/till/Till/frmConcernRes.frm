VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Begin VB.Form frmConcernRes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Concern Resolution"
   ClientHeight    =   7770
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11280
   ControlBox      =   0   'False
   Icon            =   "frmConcernRes.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7770
   ScaleWidth      =   11280
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraCRLReason 
      BackColor       =   &H00C0FFFF&
      Height          =   3015
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   10935
      Begin VB.TextBox txtReason 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1260
         Left            =   2880
         MaxLength       =   1000
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   4
         Top             =   840
         Width           =   6975
      End
      Begin VB.ComboBox cboReasonList 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   2880
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   360
         Width           =   6975
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&OK"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   4680
         TabIndex        =   2
         Top             =   2280
         Width           =   1455
      End
      Begin VB.Label lblReason 
         BackColor       =   &H00C0FFFF&
         Caption         =   "Reason:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   600
         TabIndex        =   6
         Top             =   840
         Width           =   1935
      End
      Begin VB.Label lblReasonCode 
         BackColor       =   &H00C0FFFF&
         Caption         =   "Reason Code:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   600
         TabIndex        =   5
         Top             =   360
         Width           =   2175
      End
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   7395
      Width           =   11280
      _ExtentX        =   19897
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmConcernRes.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12171
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   970
            MinWidth        =   970
            Picture         =   "frmConcernRes.frx":0F30
            Key             =   "KeyBoard"
            Object.ToolTipText     =   "Show/Hide Keyboard"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "15:01"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   120
      Top             =   3120
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
End
Attribute VB_Name = "frmConcernRes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const MODULE_NAME As String = "frmConcernRes"

Public Event Duress()
Private mintDuressKeyCode As Integer

'********************************************************************************************
'07-01-2009 MO'C WIX 1345 - New form added to allow Concern Resolution Reason to be entered.
'********************************************************************************************

Dim mcolPaidOutCodes     As Collection

Public Cancel            As Boolean
Public ShowKeyPad        As Boolean

Private mstrReasonCode   As String
Private mstrReasonDesc   As String
Private mstrReasonAttr   As String
Private mblnManagerAuth  As Boolean
Private mblnSuperAuth    As Boolean
Private mstrActive       As String

Dim blnResizing          As Boolean

Private Sub cmdSave_Click()
    
    If cboReasonList.ListIndex > -1 Then
        Call GetInfoForReasonSelected(CStr(cboReasonList.ItemData(cboReasonList.ListIndex)))
    End If
    Me.Hide
End Sub

Private Sub Form_Resize()

    If blnResizing Then Exit Sub
    blnResizing = True
    Me.Height = fraCRLReason.Height + 360 + sbStatus.Height
    If (ucKeyPad1.Visible = True) Or ((Me.Visible = False) And (ShowKeyPad = True)) Then
        Me.Height = Me.Height + ucKeyPad1.Height
        Me.Width = ucKeyPad1.Width + 120
    Else
        Me.Width = fraCRLReason.Width + 120
        Me.Height = Me.Height + sbStatus.Height
    End If
    blnResizing = False
    
End Sub

Private Sub sbStatus_PanelClick(ByVal Panel As MSComctlLib.Panel)

Dim blnShow As Boolean

    Select Case (Panel.Key)
        Case ("KeyBoard"):
                If ((Panel.Picture Is Nothing) = False) Then
                    ucKeyPad1.Visible = Not ucKeyPad1.Visible
                    Form_Resize
                End If
    End Select
        
End Sub

Private Sub Form_Load()
    
    mblnManagerAuth = False
    blnResizing = False
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    lblReasonCode.BackColor = Me.BackColor
    lblReason.BackColor = Me.BackColor
    
    fraCRLReason.BackColor = Me.BackColor
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
    RGBMSGBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
    
    ShowKeyPad = mblnShowKeypadIcon
    
    If (ShowKeyPad = True) Then
        ucKeyPad1.Visible = True
    Else
        ucKeyPad1.Visible = False
    End If

    Call InitialiseStatusBar(sbStatus)
    
    Call CentreForm(Me)

    Call LoadReasonCodes
    
End Sub

Private Sub LoadReasonCodes()

Dim oReason As cPaidOutCode
Dim intReason As Integer

    Set oReason = goDatabase.CreateBusinessObject(CLASSID_PAIDOUTCODE)
    Call oReason.AddLoadFilter(CMP_GREATERTHAN, FID_PAIDOUTCODE_Code, "20")
    Set mcolPaidOutCodes = oReason.LoadMatches
    
    For intReason = 1 To mcolPaidOutCodes.Count
        Set oReason = mcolPaidOutCodes(intReason)
        cboReasonList.AddItem (oReason.Code & " - " & oReason.Description)
        cboReasonList.ItemData(cboReasonList.NewIndex) = Val(oReason.Code)
    Next intReason
    
End Sub

Private Sub GetInfoForReasonSelected(strReasonCode As String)
Dim oReason As cPaidOutCode
Dim intReason As Integer
Dim oSysCod As cTypesOfSaleOptions

  
    Set oReason = goDatabase.CreateBusinessObject(CLASSID_PAIDOUTCODE)
    Call oReason.AddLoadFilter(CMP_EQUAL, FID_PAIDOUTCODE_Code, strReasonCode)
    Set mcolPaidOutCodes = oReason.LoadMatches
    If mcolPaidOutCodes.Count > 0 Then
        Set oReason = mcolPaidOutCodes(1)
        mstrReasonAttr = oReason.AttributeUsed
        mstrReasonCode = strReasonCode
        mstrReasonDesc = txtReason.Text
        mblnManagerAuth = oReason.ManagerAuthorisation
        mblnSuperAuth = oReason.SupervisorAuthorisation
        mstrActive = oReason.Active
    End If

End Sub

Public Function GetReasonInfo(ByRef strReasonCode As String, ByRef strReasonDesc As String, ByRef blnManager As Boolean, ByRef strAttribute) As Boolean
    
    txtReason.Text = ""
    cboReasonList.ListIndex = -1
    mstrReasonCode = ""
    mstrReasonDesc = ""
    mstrReasonAttr = ""
    mblnManagerAuth = False
    mblnSuperAuth = False
    mstrActive = ""
    
    Me.Show vbModal
    
    strReasonCode = mstrReasonCode
    strReasonDesc = mstrReasonDesc
    blnManager = mblnManagerAuth
    strAttribute = mstrReasonAttr
    GetReasonInfo = IIf(cboReasonList.ListIndex > -1 And txtReason.Text <> "", True, False)
    
End Function
