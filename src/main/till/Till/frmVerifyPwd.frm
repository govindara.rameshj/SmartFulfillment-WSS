VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Begin VB.Form frmVerifyPwd 
   BackColor       =   &H0080FFFF&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Enter Refund Password"
   ClientHeight    =   3600
   ClientLeft      =   1680
   ClientTop       =   1755
   ClientWidth     =   7080
   ControlBox      =   0   'False
   Icon            =   "frmVerifyPwd.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3600
   ScaleWidth      =   7080
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   1500
      Top             =   3240
      Visible         =   0   'False
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin VB.Frame fraLogon 
      BorderStyle     =   0  'None
      Height          =   1935
      Left            =   180
      TabIndex        =   4
      Top             =   120
      Visible         =   0   'False
      Width           =   6735
      Begin VB.TextBox txtAuthID 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   2400
         MaxLength       =   3
         TabIndex        =   6
         Top             =   120
         Width           =   975
      End
      Begin VB.TextBox txtPassword 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         IMEMode         =   3  'DISABLE
         Left            =   2400
         MaxLength       =   5
         PasswordChar    =   "*"
         TabIndex        =   5
         Top             =   1320
         Width           =   1335
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Auth ID"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   120
         TabIndex        =   12
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label lblPasswordlbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Password"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   120
         TabIndex        =   11
         Top             =   1320
         Width           =   1695
      End
      Begin VB.Label lblFullName 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   3480
         TabIndex        =   10
         Top             =   720
         Width           =   2655
      End
      Begin VB.Label lblInitials 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   2400
         TabIndex        =   9
         Top             =   720
         Width           =   975
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Initials"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   120
         TabIndex        =   8
         Top             =   720
         Width           =   1335
      End
      Begin VB.Label lblLoggedUserID 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   2400
         TabIndex        =   7
         Top             =   120
         Visible         =   0   'False
         Width           =   975
      End
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   1260
      TabIndex        =   2
      Top             =   2220
      Width           =   1695
   End
   Begin VB.TextBox txtRefundPwd 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      IMEMode         =   3  'DISABLE
      Left            =   2580
      MaxLength       =   5
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   1140
      Width           =   2055
   End
   Begin VB.CommandButton cmdContinue 
      Caption         =   "Continue"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   4140
      TabIndex        =   3
      Top             =   2220
      Width           =   1695
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   13
      Top             =   3225
      Width           =   7080
      _ExtentX        =   12488
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmVerifyPwd.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   5106
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   635
            MinWidth        =   635
            Picture         =   "frmVerifyPwd.frx":0F30
            Key             =   "NumPad"
            Object.ToolTipText     =   "Show/Hide Num Pad"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "15:05"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "ENTER REFUND PASSWORD"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   60
      TabIndex        =   0
      Top             =   300
      Width           =   6975
   End
End
Attribute VB_Name = "frmVerifyPwd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>************************************************************************************
'* Module : frmRefundPwd
'* Date   : 17/02/03
'* Author : Unknown
'*$Archive: /Projects/OasysV2/VB/Till/frmVerifyPwd.frm $
'******************************************************************************************
'* Summary: Used to enter the Refund password.  Used for refund authorisation
'******************************************************************************************
'* $Author: Mauricem $ $Date: 12/03/04 9:56 $ $Revision: 6 $
'* Versions:
'* 17/02/03    Unknown
'*             Header added.
'</CAMH>***********************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmVerifyPwd"

Const PWD_TYPE_EFT    As Byte = 1
Const PWD_TYPE_REFUND As Byte = 2
Const PWD_TYPE_DURESS As Byte = 3
Const PWD_TYPE_SUPER  As Byte = 4
Const PWD_TYPE_MNGER  As Byte = 5

Private mblnPasswordOk   As Boolean
Private mstrPasswordType As String
Private mbytPasswordType As Byte

Private mstrOldUserID As String 'used to auto move to password on 3 key pressed
Private mstrOldPwd    As String 'used to auto logon on 5 key pressed

Public Event Duress()
Public Cancelled As Boolean
Private mintDuressKeyCode As Integer

Private moVerifyUserBO As cUser



Public Function VerifySupervisorPassword(ByVal blnShowNumPad As Boolean, Optional strMessage = vbNullString, Optional blnHideCancel As Boolean = False) As Boolean
    
    mbytPasswordType = PWD_TYPE_SUPER
    Cancelled = False
    Me.Caption = "Supervisor Authorisation Required"
    Me.BackColor = &H7FFF7F
    SetColors
    If LenB(strMessage) <> 0 Then
        mstrPasswordType = strMessage
    Else
        mstrPasswordType = "Supervisor Password"
    End If
    
    txtAuthID.Text = vbNullString
    txtPassword.Text = vbNullString
    
    fraLogon.Visible = True
    
    If (blnShowNumPad = True) Then
        ucKeyPad1.Visible = True
        Call ucKeyPad1.ShowNumPad
        Me.Height = Me.Height + ucKeyPad1.Height
    End If
    
    If blnHideCancel Then
        cmdCancel.Enabled = False
    Else
        cmdCancel.Enabled = True
    End If
    
    Call Me.Show(vbModal)
    VerifySupervisorPassword = mblnPasswordOk
    
End Function

Public Function VerifyManagerPassword(ByVal blnShowNumPad As Boolean, Optional strMessage = vbNullString) As Boolean
    
    mbytPasswordType = PWD_TYPE_MNGER
    Cancelled = False
    Me.Caption = "Manager Authorisation Required"
    Me.BackColor = &HFF7F7F
    SetColors
    If LenB(strMessage) <> 0 Then
        mstrPasswordType = strMessage
    Else
        mstrPasswordType = "Manager Password"
    End If
    
    txtAuthID.Text = vbNullString
    txtPassword.Text = vbNullString
    
    fraLogon.Visible = True
    
    If (blnShowNumPad = True) Then
        ucKeyPad1.Visible = True
        Call ucKeyPad1.ShowNumPad
        Me.Height = Me.Height + ucKeyPad1.Height
    End If
    
    Call Me.Show(vbModal)
    VerifyManagerPassword = mblnPasswordOk
    
End Function

Public Function VerifyRefundPassword(ByVal blnShowNumPad As Boolean) As Boolean

    mbytPasswordType = PWD_TYPE_REFUND
    Cancelled = False
    Me.Caption = "Enter Refunding Cashier"
    Me.BackColor = vbYellow
    SetColors
    mstrPasswordType = "Refunding Cashier"
    
    txtAuthID.Text = vbNullString
    txtPassword.Text = vbNullString
    
    fraLogon.Visible = True
    
    If (blnShowNumPad = True) Then
        ucKeyPad1.Visible = True
        Call ucKeyPad1.ShowNumPad
        Me.Height = Me.Height + ucKeyPad1.Height
    End If
    
    Call Me.Show(vbModal)
    VerifyRefundPassword = mblnPasswordOk

End Function

Public Function VerifyEFTPassword(ByVal blnShowNumPad As Boolean) As Boolean

    mbytPasswordType = PWD_TYPE_EFT
    Cancelled = False
    If (goSession Is Nothing) Then
        VerifyEFTPassword = True
        Exit Function
    End If
    
    Me.Caption = "Enter Authorisation Code"
    Me.BackColor = vbYellow
    SetColors
    mstrPasswordType = "Authorisation Code"
    
    txtAuthID.Text = vbNullString
    txtPassword.Text = vbNullString
    
    fraLogon.Visible = True
    
    If (blnShowNumPad = True) Then
        ucKeyPad1.Visible = True
        Call ucKeyPad1.ShowNumPad
        Me.Height = Me.Height + ucKeyPad1.Height
    End If
    
    Call Me.Show(vbModal)
    VerifyEFTPassword = mblnPasswordOk

End Function

Public Function VerifyDuressPassword(ByVal blnShowNumPad As Boolean) As Boolean

    mbytPasswordType = PWD_TYPE_DURESS
    Cancelled = False
    Me.Caption = "Enter Authorisation Code"
    Me.BackColor = vbYellow
    SetColors
    mstrPasswordType = "Authorisation Code"
    
    txtAuthID.Text = vbNullString
    txtPassword.Text = vbNullString
    txtPassword.MaxLength = 8
    
    fraLogon.Visible = True
    
    If (blnShowNumPad = True) Then
        ucKeyPad1.Visible = True
        Call ucKeyPad1.ShowNumPad
        Me.Height = Me.Height + ucKeyPad1.Height
    End If
    
    Call Me.Show(vbModal)
    
    txtPassword.MaxLength = 5
    
    VerifyDuressPassword = mblnPasswordOk

End Function

Private Sub cmdCancel_Click()

    If (cmdCancel.Visible = True) And (cmdCancel.Enabled = True) Then cmdCancel.SetFocus
    mblnPasswordOk = False
    Cancelled = True
    Call Me.Hide

End Sub

Private Sub cmdContinue_Click()
    
Const PROCEDURE_NAME As String = "cmdContinue_Click"
    
    If (cmdContinue.Visible = True) And (cmdContinue.Enabled = True) Then cmdContinue.SetFocus
    'Password entered correctly so report to calling form
    Screen.MousePointer = vbNormal
    If mbytPasswordType = PWD_TYPE_EFT Then
        mblnPasswordOk = VerifyAuthCode
    ElseIf mbytPasswordType = PWD_TYPE_REFUND Then
        mblnPasswordOk = VerifyRefundCode
    ElseIf mbytPasswordType = PWD_TYPE_SUPER Then
        mblnPasswordOk = VerifyAuthCode
    ElseIf mbytPasswordType = PWD_TYPE_MNGER Then
        mblnPasswordOk = VerifyAuthCode
    Else
        mblnPasswordOk = VerifyDuressCode
    End If
    If mblnPasswordOk = True Then Call Me.Hide

End Sub

Private Function VerifyDuressCode() As Boolean

Const PROCEDURE_NAME As String = "VerifyDuressCode"

Dim oSysOpt As cSystemOptions
Dim strPwd  As String 'used to hold password pre-padded with 0's to make 5 chars
    
    VerifyDuressCode = False
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Duress password verify")
    'check current duress password filled in
    VerifyDuressCode = VerifyAuthCode

End Function


Private Function VerifyRefundCode() As Boolean

Const PROCEDURE_NAME As String = "VerifyRefundCode"

Dim oSysOpt As cSystemOptions
Dim strPwd  As String 'used to hold password pre-padded with 0's to make 5 chars
    
    VerifyRefundCode = False
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Refund password verify")
    'check current refund password filled in
    VerifyRefundCode = VerifyAuthCode

End Function


Private Function VerifyAuthCode() As Boolean

Const PROCEDURE_NAME As String = "VerifyAuthCode"

Const PRM_CHECK_EXPIRED As Long = 106

Dim strFailedMsg    As String
Dim strTranID       As String
Dim blnCheckExpired As Boolean
Dim strPassword     As String

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Verify Authorisation Code")
    VerifyAuthCode = False
    
    blnCheckExpired = goSession.GetParameter(PRM_CHECK_EXPIRED)
    If (Not moVerifyUserBO Is Nothing) And (LenB(lblFullName.Caption) <> 0) Then
        Screen.MousePointer = vbHourglass
        'pad password to 5 characters
        'Referral 2007-067 - take out check for last 3 digits of password is 111
'        If mbytPasswordType = PWD_TYPE_DURESS Then
'            strPassword = Right$("00000000" & txtPassword.Text, 8)
'            If Right$(strPassword, 3) <> "111" Then
'                strFailedMsg = "Invalid user name or password entered"
'                Call MsgBoxEx(strFailedMsg, vbExclamation, Me.Caption, , , , , RGBMsgBox_WarnColour)
'                Screen.MousePointer = vbNormal
'                txtAuthID.SetFocus
'                Exit Function
'            End If
'            strPassword = Left$(strPassword, 5)
'        Else
            strPassword = Right$("00000" & txtPassword.Text, 5)
'        End If
        If ((strPassword = moVerifyUserBO.Password) And ((mbytPasswordType = PWD_TYPE_REFUND) Or (mbytPasswordType = PWD_TYPE_DURESS))) Or _
            ((strPassword = moVerifyUserBO.AuthorisationCode) And (mbytPasswordType <> PWD_TYPE_REFUND)) Then
            If (moVerifyUserBO.AuthorisationValidTill < Now()) And (blnCheckExpired = True) Then
                Call MsgBoxEx("Authorisation Code has expired for user '" & moVerifyUserBO.EmployeeID & "-" & moVerifyUserBO.FullName & "'", vbInformation, "Authorisation Expired", , , , , RGBMsgBox_WarnColour)
                Screen.MousePointer = vbNormal
                If (txtAuthID.Visible = True) And (txtAuthID.Enabled = True) Then txtAuthID.SetFocus
                Exit Function
            End If 'check that password has not expired
        Else
            strFailedMsg = "Invalid user name or password entered"
            Call MsgBoxEx(strFailedMsg, vbExclamation, Me.Caption, , , , , RGBMsgBox_WarnColour)
            Screen.MousePointer = vbNormal
            If (txtAuthID.Visible = True) And (txtAuthID.Enabled = True) Then txtAuthID.SetFocus
            Exit Function
        End If
        'Password entered correctly so report to calling form
        Screen.MousePointer = vbNormal
        If mbytPasswordType = PWD_TYPE_SUPER Then
            If moVerifyUserBO.Supervisor Or moVerifyUserBO.Manager Then
                VerifyAuthCode = True
            Else
                strFailedMsg = "Employee not a supervisor"
            End If
        ElseIf mbytPasswordType = PWD_TYPE_MNGER Then
            If moVerifyUserBO.Manager Then
                VerifyAuthCode = True
            Else
                strFailedMsg = "Employee not a manager"
            End If
        Else
            VerifyAuthCode = True
        End If
    
        If LenB(strFailedMsg) <> 0 Then
            Call MsgBoxEx(strFailedMsg, vbExclamation, Me.Caption, , , , , RGBMsgBox_WarnColour)
            Screen.MousePointer = vbNormal
            If (txtAuthID.Visible = True) And (txtAuthID.Enabled = True) Then txtAuthID.SetFocus
            Exit Function
        End If
    End If 'verify current user's password

End Function

Private Sub Form_Activate()

    If (txtRefundPwd.Visible = True) And (txtRefundPwd.Enabled = True) Then Call txtRefundPwd.SetFocus
    If (txtAuthID.Visible = True) And (txtAuthID.Enabled = True) Then Call txtAuthID.SetFocus
    Call ucKeyPad1.ShowNumPad

End Sub

Public Sub InitialiseStatusBar()
    
    sbStatus.Panels(PANEL_VERNO).Text = "Ver. " & App.Major & "." & App.Minor & "." & App.Revision
    sbStatus.Panels(PANEL_VERNO).ToolTipText = "Program Version Number"
    sbStatus.Panels(PANEL_WSID + 1).Text = Right$("00" & goSession.CurrentEnterprise.IEnterprise_WorkstationID, 2) & " "
    sbStatus.Panels(PANEL_WSID + 1).ToolTipText = "Current Work-Station Number"
    sbStatus.Panels(PANEL_DATE + 1).Text = Format$(Date, "DD-MMM-YY")
    If (mblnShowKeypadIcon = False) Then sbStatus.Panels("NumPad").Picture = Nothing

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    
    If mintDuressKeyCode <> 0 And KeyCode = mintDuressKeyCode Then
        KeyCode = 0
        RaiseEvent Duress
    End If

End Sub

Private Sub sbStatus_PanelClick(ByVal Panel As MSComctlLib.Panel)

Dim blnShow As Boolean

    Select Case (Panel.Key)
        Case ("NumPad"):
                        If ((Panel.Picture Is Nothing) = False) Then
                            ucKeyPad1.Visible = Not ucKeyPad1.Visible
                            If (ucKeyPad1.Visible = True) Then
                                Me.Height = 4080 + ucKeyPad1.Height + 120
                            Else
                                Me.Height = 4080
                            End If
                            Call CentreForm(Me)
                            
                        End If
    End Select

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyEscape And cmdCancel.Enabled = True Then cmdCancel.Value = True

End Sub

Private Sub Form_Load()

    mblnPasswordOk = False
    Me.BackColor = mlngBackColor

    mintDuressKeyCode = mintDuressKeyCode
    Call InitialiseStatusBar
    Call ucKeyPad1.ShowNumPad
    
End Sub

Private Sub Form_Resize()

    Call CentreForm(frmVerifyPwd)

End Sub

Private Sub txtAuthID_Change()

    lblFullName.Caption = vbNullString
    lblInitials.Caption = vbNullString
    txtPassword.Text = vbNullString
    txtPassword.Enabled = False
    
End Sub

Private Sub txtAuthID_GotFocus()

    txtAuthID.SelStart = 0
    txtAuthID.SelLength = Len(txtAuthID.Text)
    txtAuthID.BackColor = RGBEdit_Colour

End Sub

Private Sub txtAuthID_KeyPress(KeyAscii As Integer)

Const PROCEDURE_NAME As String = MODULE_NAME & ".txtAuthID_KeyPress"

Dim blnLoggedIn As Boolean
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        If Len(txtAuthID.Text) <> 3 Then txtAuthID.Text = Right$("000" & txtAuthID.Text, 3)
        Set moVerifyUserBO = goDatabase.CreateBusinessObject(CLASSID_USER)
        Call moVerifyUserBO.AddLoadFilter(CMP_EQUAL, FID_USER_EmployeeID, txtAuthID.Text)
        Call moVerifyUserBO.AddLoadFilter(CMP_EQUAL, FID_USER_Deleted, False)
        Call moVerifyUserBO.AddLoadField(FID_USER_Initials)
        Call moVerifyUserBO.AddLoadField(FID_USER_FullName)
        Call moVerifyUserBO.AddLoadField(FID_USER_AuthorisationCode)
        Call moVerifyUserBO.AddLoadField(FID_USER_AuthorisationValidTill)
        Call moVerifyUserBO.AddLoadField(FID_USER_Supervisor)
        Call moVerifyUserBO.AddLoadField(FID_USER_Manager)
        Call moVerifyUserBO.AddLoadField(FID_USER_Password)
        If LenB(moVerifyUserBO.EmployeeID) = 0 Then moVerifyUserBO.EmployeeID = "***" 'force invalid lookup
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "UserID Validated")
        
        Call moVerifyUserBO.LoadMatches
        If LenB(moVerifyUserBO.FullName) <> 0 Then
            lblInitials.Caption = moVerifyUserBO.Initials
            lblFullName.Caption = moVerifyUserBO.FullName
            txtPassword.Enabled = True
            txtPassword.Visible = True
            lblPasswordlbl.Visible = True
            If (txtPassword.Visible = True) And (txtPassword.Enabled = True) Then Call txtPassword.SetFocus
            If (Val(txtAuthID.Text) >= Val(mstrMaxValidCashier)) Then
                lblInitials.Caption = vbNullString
                lblFullName.Caption = "TRAINING ID"
                If (txtAuthID.Visible = True) And (txtAuthID.Enabled = True) Then Call txtAuthID.SetFocus
                Set moVerifyUserBO = Nothing
            End If
        Else
            txtPassword.Text = vbNullString
            txtPassword.Enabled = False
            lblInitials.Caption = vbNullString
            lblFullName.Caption = "INVALID ID"
            If (txtAuthID.Visible = True) And (txtAuthID.Enabled = True) Then Call txtAuthID.SetFocus
            txtAuthID.SelStart = 0
            txtAuthID.SelLength = Len(txtAuthID.Text)
            Set moVerifyUserBO = Nothing
        End If
        Screen.MousePointer = vbNormal
    End If 'enter pressed so check User ID is valid

End Sub

Private Sub txtAuthID_KeyUp(KeyCode As Integer, Shift As Integer)
    
    'Check if new keypressed makes up full User ID, so auto move to password
    If (Len(txtAuthID.Text) = 3) And (Len(mstrOldUserID) = 2) Then
        KeyCode = 0
        Call txtAuthID_KeyPress(vbKeyReturn)
    End If
    mstrOldUserID = txtAuthID.Text


End Sub

Private Sub txtAuthID_LostFocus()

    txtAuthID.BackColor = RGB_WHITE

End Sub

Private Sub txtPassword_GotFocus()
    
    txtPassword.SelStart = 0
    txtPassword.SelLength = Len(txtPassword.Text)
    txtPassword.BackColor = RGBEdit_Colour

End Sub

Private Sub txtPassword_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        cmdContinue.Value = True
    End If

End Sub


Private Sub txtPassword_KeyUp(KeyCode As Integer, Shift As Integer)
    
'    If (Len(txtPassword.Text) = 5) And (Len(mstrOldPwd) = 4) Then
'        KeyCode = 0
'        Call txtPassword_KeyPress(vbKeyReturn)
'    End If
'    mstrOldPwd = txtPassword.Text

End Sub

Private Sub txtPassword_LostFocus()
    
    txtPassword.BackColor = RGB_WHITE

End Sub

Private Sub txtRefundPwd_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        cmdContinue.Value = True
    End If

End Sub

Private Sub SetColors()
    
    fraLogon.BackColor = Me.BackColor
    cmdCancel.BackColor = Me.BackColor
    cmdContinue.BackColor = Me.BackColor

End Sub

Private Sub ucKeyPad1_Resize()
    
    ucKeyPad1.Width = ucKeyPad1.Width
    ucKeyPad1.Height = ucKeyPad1.Height

End Sub
