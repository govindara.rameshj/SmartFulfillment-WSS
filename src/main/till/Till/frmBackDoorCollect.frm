VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{FA666EFA-0D22-45D3-9849-6B0694037D1B}#2.1#0"; "EditNumberTill.ocx"
Begin VB.Form frmBackDoorCollect 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Back Door Collection Items Confirmation"
   ClientHeight    =   7920
   ClientLeft      =   165
   ClientTop       =   690
   ClientWidth     =   11895
   Icon            =   "frmBackDoorCollect.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7920
   ScaleWidth      =   11895
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdSelectAll 
      Caption         =   "Select All"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   7620
      TabIndex        =   20
      Top             =   6600
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdUndoAll 
      Caption         =   "Undo All"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   9720
      TabIndex        =   19
      Top             =   6600
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.Frame fraDiscount 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   120
      TabIndex        =   13
      Top             =   6480
      Visible         =   0   'False
      Width           =   6675
      Begin EditNumberTill.ucTillNumberText ntxtTotal 
         Height          =   285
         Left            =   5640
         TabIndex        =   1
         Top             =   240
         Width           =   900
         _ExtentX        =   1588
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinimumValue    =   0
      End
      Begin EditNumberTill.ucTillNumberText ntxtDiscValue 
         Height          =   285
         Left            =   5640
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   600
         Width           =   900
         _ExtentX        =   1588
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         MinimumValue    =   0
      End
      Begin EditNumberTill.ucTillNumberText ntxtDiscRate 
         Height          =   285
         Left            =   5640
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   960
         Width           =   900
         _ExtentX        =   1588
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         MinimumValue    =   0
         MaximumValue    =   100
      End
      Begin VB.Label Label10 
         BackStyle       =   0  'Transparent
         Caption         =   "Enter required deal group total"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   350
         Width           =   4095
      End
      Begin VB.Label Label9 
         BackStyle       =   0  'Transparent
         Caption         =   "Value discount to remove from the current total"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   680
         Width           =   5115
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "Percentage discount to remove from the current total"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   120
         TabIndex        =   16
         Top             =   1000
         Width           =   8955
      End
      Begin VB.Label lblAllocDisc 
         Caption         =   "0.00"
         Height          =   255
         Left            =   960
         TabIndex        =   21
         Top             =   240
         Visible         =   0   'False
         Width           =   735
      End
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   9720
      TabIndex        =   7
      Top             =   7200
      Width           =   1995
   End
   Begin VB.CommandButton cmdComplete 
      Caption         =   "Complete"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   7620
      TabIndex        =   6
      Top             =   7200
      Width           =   1995
   End
   Begin FPSpreadADO.fpSpread sprdItems 
      Height          =   6375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   11715
      _Version        =   458752
      _ExtentX        =   20664
      _ExtentY        =   11245
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   12
      MaxRows         =   1
      OperationMode   =   2
      RowHeaderDisplay=   0
      SelectBlockOptions=   2
      SpreadDesigner  =   "frmBackDoorCollect.frx":0A96
      UserResize      =   0
   End
   Begin VB.Frame fraStockKey 
      Caption         =   "Stock Availability Key"
      Height          =   950
      Left            =   180
      TabIndex        =   8
      Top             =   6960
      Visible         =   0   'False
      Width           =   6615
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         BackColor       =   &H0000FF00&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   600
         Width           =   375
      End
      Begin VB.Label Label5 
         Caption         =   "Stock level ok"
         Height          =   255
         Left            =   540
         TabIndex        =   14
         Top             =   600
         Width           =   2175
      End
      Begin VB.Label Label4 
         Caption         =   "Low stock level - please check"
         Height          =   255
         Left            =   3780
         TabIndex        =   12
         Top             =   240
         Width           =   2595
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   3360
         TabIndex        =   11
         Top             =   240
         Width           =   375
      End
      Begin VB.Label Label2 
         Caption         =   "Insufficient stock available"
         Height          =   255
         Left            =   540
         TabIndex        =   10
         Top             =   240
         Width           =   2175
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   375
      End
   End
   Begin VB.CommandButton cmdCollectAll 
      Caption         =   "Collect All"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   180
      TabIndex        =   4
      Top             =   7200
      Width           =   2055
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   2400
      TabIndex        =   5
      Top             =   7200
      Width           =   1995
   End
End
Attribute VB_Name = "frmBackDoorCollect"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>************************************************************************************
'* Module : frmBackDoorCollect
'* Date   : 17/02/03
'* Author : Unknown
'*$Archive: /Projects/OasysV2/VB/Till/frmTill.frm $
'******************************************************************************************
'* Summary:
'******************************************************************************************
'* $Author: Richardc $ $Date: 10/09/04 9:20 $ $Revision: 84 $
'* Versions:
'* 17/02/03    Unknown
'*             Header added.
'</CAMH>***********************************************************************************
Option Explicit

Const COL_BD_ITEMNO As Long = 1
Const COL_BD_PCODE As Long = 2
Const COL_BD_DESC As Long = 3
Const COL_BD_ONHAND As Long = 4
Const COL_BD_ONORDER As Long = 5
Const COL_BD_DELDATE As Long = 6
Const COL_BD_QTY As Long = 7
Const COL_BD_QTYTAKENOW As Long = 8
Const COL_BD_COLLECT As Long = 9
Const COL_BD_LINENO As Long = 10
Const COL_BD_TOBEDELIVERED = 11
Const COL_BD_TOTAL As Long = 12
Const COL_BD_DISCOUNTED_VALUE As Long = 13
Const COL_BD_DISCOUNTED_TOTAL As Long = 14

Private mcolEntries         As Collection
Private mstrAllocItems()    As String
Private mcurSelectedValue   As Currency

Private mlngResetKeyCode    As Long
Private mlngCloseKeyCode    As Long
Private mlngUseKeyCode      As Long
Private mlngAllKeyCode      As Long
Private mlngSelectAllKeyCode As Long
Private mlngUnSelectAllKeyCode As Long
Private mlngNumericPadSubtractkey As Long
Private mblnConfirmCollect  As Boolean
Private mblnIsCollection    As Boolean
Public mlngTakeNow          As Long
Private mlngRow             As Long
Private mblnEditModeChange  As Long
Private mblnCancel          As Boolean
Private mblnTranDiscount    As Boolean

'******************************************************
'P014-04 - Multiple SKU Price Override
'Author: Michael O'Cain
'Date:   21/12/2011
'******************************************************
Private mblnCancelled As Boolean
Private mcurOrigAmount As Currency
Public mlngTranDiscSupLevel As Long
Public mlngTranDiscMgrLevel As Long
Dim mlngItemsKeyCode As Long
Dim mlngAuthorisor As Long
Dim mStoreOpenDate As Date
Dim mdblTransactionTotal As Double
'******************************************************
'End P014-04 - Multiple SKU Price Override declarations
'******************************************************

Public Sub AddItem(lngItemNo As Long, strSKU As String, strDesc As String, strQuantity As String, strCollect As String, strStockLevel As String, strOnOrder As String, strSupplierCode As String, Optional LineValue As Currency = 0, Optional blnMultiSkuOverride As Boolean = False)

Dim lngRowNo As Long
Dim blnSOQ   As Boolean
Dim intQtyTakeNow As Integer
Dim lngQuantityOrdered As Long

    If (lngItemNo = -1) Then 'adding to Customer Order confirmation so consolidate qty if already exists
        sprdItems.Col = COL_BD_PCODE
        For lngRowNo = 1 To sprdItems.MaxRows Step 1
            sprdItems.Row = lngRowNo
            If (sprdItems.Text = strSKU) Then
                lngItemNo = lngRowNo
                sprdItems.Col = COL_BD_QTY
                strQuantity = Val(strQuantity) + Val(sprdItems.Value)
                sprdItems.Col = COL_BD_DELDATE
                sprdItems.Value = DisplayDate(NextDeliveryDate(strSKU, blnSOQ, strSupplierCode, lngQuantityOrdered), False)
                strOnOrder = CStr(lngQuantityOrdered)
                Exit For
            End If
        Next lngRowNo
        If (lngItemNo = -1) Then 'Not found so add a new line
            sprdItems.MaxRows = sprdItems.MaxRows + 1
            sprdItems.Row = sprdItems.MaxRows
            sprdItems.Col = COL_BD_DELDATE
            sprdItems.Value = DisplayDate(NextDeliveryDate(strSKU, blnSOQ, strSupplierCode, lngQuantityOrdered), False)
            strOnOrder = CStr(lngQuantityOrdered)
        End If
    Else 'Back Door collection so just add to list
        sprdItems.MaxRows = sprdItems.MaxRows + 1
        sprdItems.Row = sprdItems.MaxRows
    End If
    sprdItems.Col = COL_BD_ITEMNO
    sprdItems.Text = sprdItems.MaxRows
    sprdItems.Col = COL_BD_PCODE
    sprdItems.Text = strSKU
    sprdItems.Col = COL_BD_DESC
    sprdItems.Text = strDesc
    sprdItems.Col = COL_BD_QTY
    sprdItems.Text = strQuantity
    sprdItems.Col = COL_BD_QTYTAKENOW
    sprdItems.Text = "" 'IIf(mblnForDelivery, "0", strQuantity)
    sprdItems.Col = COL_BD_COLLECT
    If (strCollect = "") Then strCollect = "N"
    sprdItems.Text = strCollect
    sprdItems.Col = COL_BD_LINENO
    sprdItems.Text = lngItemNo
    sprdItems.Col = COL_BD_ONHAND
    sprdItems.Text = strStockLevel
    sprdItems.Col = COL_BD_ONORDER
    sprdItems.Text = strOnOrder
    sprdItems.Col = COL_BD_TOTAL
    sprdItems.Text = LineValue
    sprdItems.RowHeight(sprdItems.MaxRows) = sprdItems.MaxTextRowHeight(sprdItems.MaxRows)
    If strStockLevel <> "" Then
        If mblnForDelivery = True Or CLng(strStockLevel) <= 0 Then
            sprdItems.Col = COL_BD_QTYTAKENOW
            sprdItems.Text = "" 'CStr(0)
            sprdItems.Col = COL_BD_TOBEDELIVERED
            sprdItems.Text = strQuantity
        Else
'            sprdItems.Col = COL_BD_QTYTAKENOW
            intQtyTakeNow = 0 'CInt(sprdItems.Text)
            sprdItems.Col = COL_BD_TOBEDELIVERED
            sprdItems.Text = CInt(strQuantity) - intQtyTakeNow
        End If
    Else
        sprdItems.Col = COL_BD_QTYTAKENOW
        intQtyTakeNow = CLng(Val(sprdItems.Text))
        sprdItems.Col = COL_BD_TOBEDELIVERED
        sprdItems.Text = CLng(strQuantity) - intQtyTakeNow
    End If
    If (UCase(strCollect) = "Y") Then
        sprdItems.Col = COL_BD_QTYTAKENOW
        sprdItems.Text = CLng(strQuantity)
        sprdItems.Col = COL_BD_TOBEDELIVERED
        sprdItems.Text = "0"
    End If

    If blnMultiSkuOverride Then
        sprdItems.Col = COL_BD_DISCOUNTED_VALUE
        sprdItems.CellType = CellTypeNumber
        
        sprdItems.Col = COL_BD_DISCOUNTED_TOTAL
        sprdItems.CellType = CellTypeNumber
    
    End If
    
End Sub

Public Function ConfirmCollect() As Collection

    mblnIsCollection = True
    sprdItems.Col = COL_BD_ONHAND
    sprdItems.ColHidden = True
    sprdItems.Col = COL_BD_ONORDER
    sprdItems.ColHidden = True
    sprdItems.Col = COL_BD_DELDATE
    sprdItems.ColHidden = True
    sprdItems.Col = COL_BD_QTYTAKENOW
    sprdItems.ColHidden = True
    sprdItems.Col = COL_BD_TOBEDELIVERED
    sprdItems.ColHidden = True
    
    sprdItems.OperationMode = OperationModeRow
    Set ConfirmCollect = New Collection
    Me.Show (vbModal)
    DoEvents
    Set ConfirmCollect = mcolEntries

End Function

Public Function AllocateDiscount(ByVal curDiscTotal As Currency, ByRef strLines() As String, ByRef SelectedValue As Currency, Optional curTotal As Currency = 0, Optional Authorisor As Long = -1) As Boolean

    Me.Caption = "Select Items to allocate discount"
    
    mcurOrigAmount = 0
    mdblTransactionTotal = curTotal
    
    mlngAuthorisor = Authorisor
    mStoreOpenDate = Format(Now, "YYYY-MM-DD")
    
    sprdItems.MaxCols = 14
    sprdItems.Col = COL_BD_PCODE
    sprdItems.ColWidth(COL_BD_PCODE) = 10
  
    sprdItems.Col = COL_BD_ONHAND
    sprdItems.ColHidden = True
    sprdItems.Col = COL_CUSTORDER
    sprdItems.ColHidden = True
    sprdItems.Col = COL_BD_DELDATE
    sprdItems.ColHidden = True
    sprdItems.Col = COL_BD_QTYTAKENOW
    sprdItems.ColHidden = True
    sprdItems.Col = COL_BD_QTY
    sprdItems.ColHidden = False
    sprdItems.Row = 0
    sprdItems.Text = "Qty"
    sprdItems.Col = COL_BD_TOBEDELIVERED
    sprdItems.ColHidden = True
    sprdItems.Col = COL_BD_ONORDER
    sprdItems.ColHidden = True
    sprdItems.Col = COL_BD_TOTAL
    sprdItems.Row = 0
    sprdItems.Text = "Price"
    sprdItems.ColWidth(COL_BD_TOTAL) = 10
    
    sprdItems.ColHidden = False
    sprdItems.Col = COL_BD_COLLECT
    sprdItems.Row = 0
    sprdItems.Text = "Apply Discount"
    sprdItems.ColWidth(COL_BD_COLLECT) = 12
    sprdItems.Row = -1
    sprdItems.Text = "N"
    
    Call sprdItems.InsertCols(COL_BD_DISCOUNTED_VALUE, 2)
    sprdItems.Col = COL_BD_DISCOUNTED_VALUE
    sprdItems.Row = 0
    sprdItems.Text = "Disc Value"
    sprdItems.ColHidden = True
    
    sprdItems.Col = COL_BD_DISCOUNTED_TOTAL
    sprdItems.Row = 0
    sprdItems.Text = "Disc Total"
    sprdItems.ColHidden = True

    cmdCollectAll.Visible = False
    cmdReset.Visible = False
    
    Call GetTotalSelected
    
    fraDiscount.Visible = True
    'lblAllocDisc.Caption = Format(curDiscTotal, "0.00")
    mlngSelectAllKeyCode = vbKeyF9
    mlngUnSelectAllKeyCode = vbKeyF10
    
    mblnTranDiscount = True
    sprdItems.OperationMode = OperationModeRow
    
    cmdSelectAll.Visible = True
    cmdSelectAll.Enabled = True
    cmdUndoAll.Visible = True
    cmdUndoAll.Enabled = False
    
    ReDim mstrAllocItems(0)
    ApplyDiscountCalc
    
    Me.Show (vbModal)
    strLines = mstrAllocItems
    SelectedValue = mcurSelectedValue
    DoEvents
    AllocateDiscount = Not (UBound(strLines) <> 0)

End Function

Public Function ConfirmCreateOrder() As Boolean

Dim lngRowNo As Long
Dim lngQty   As Long
Dim lngOnHand   As Long
Dim blnSOQItem  As Boolean

    mblnIsCollection = False
    sprdItems.Col = COL_BD_COLLECT
    sprdItems.ColHidden = False
    sprdItems.Col = COL_BD_ITEMNO
    sprdItems.ColHidden = True
    fraStockKey.Visible = True
    cmdCollectAll.Visible = False
    cmdReset.Visible = False
        
    If mblnCreatingOrder = False Then
        sprdItems.Col = COL_BD_COLLECT
        sprdItems.ColHidden = True
        sprdItems.Col = COL_BD_QTYTAKENOW
        sprdItems.ColHidden = True
        sprdItems.Col = COL_BD_TOBEDELIVERED
        sprdItems.ColHidden = True
    Else
        sprdItems.Col = COL_BD_COLLECT
        sprdItems.ColHidden = False
        sprdItems.Col = COL_BD_QTYTAKENOW
        sprdItems.ColHidden = False
        sprdItems.Col = COL_BD_TOBEDELIVERED
        sprdItems.ColHidden = False

    End If
    
    If mblnForDelivery = False Then
        sprdItems.Row = 0
        sprdItems.Col = COL_BD_TOBEDELIVERED
        sprdItems.Text = "To Be Collected"
    Else
        sprdItems.Row = 0
        sprdItems.Col = COL_BD_TOBEDELIVERED
        sprdItems.Text = "To Be Delivered"
    End If
    
    sprdItems.OperationMode = OperationModeNormal
    'sprdItems.OperationMode = OperationModeRow
    
    Me.Caption = "View ordered items stock levels"
    
    For lngRowNo = 1 To sprdItems.MaxRows Step 1
        sprdItems.Row = lngRowNo
        sprdItems.Col = COL_BD_QTY
        lngQty = Val(sprdItems.Value)
        sprdItems.Col = COL_BD_ONHAND
        lngOnHand = Val(sprdItems.Value)
        If (lngOnHand < lngQty) Then
            sprdItems.BackColor = vbRed
        Else
            If ((lngOnHand * (goSession.GetParameter(PRM_QODSTOCK_CHK) / 100)) < lngQty) Then
                sprdItems.BackColor = vbYellow
            Else
                sprdItems.BackColor = vbGreen
            End If
        End If
        sprdItems.Col = COL_BD_COLLECT
        sprdItems.BackColor = vbCyan
        sprdItems.Col = COL_BD_QTYTAKENOW
        sprdItems.BackColor = vbCyan
    Next lngRowNo
    Call sprdItems.SetActiveCell(COL_BD_QTYTAKENOW, 1)
    Me.Show (vbModal)
    sprdItems.EditModeReplace = True
    DoEvents
    ConfirmCreateOrder = mblnConfirmCollect

End Function

Private Function NextDeliveryDate(strSKU As String, ByRef blnSOQOrder As Boolean, ByVal strSupplierCode As String, ByRef QuantityOrdered As Long) As Date

Dim rsDueDate   As New ADODB.Recordset
Dim rsQtyOnOrder As New ADODB.Recordset
Dim oSuppDetBO  As cSupplierDet
Dim lngToday    As Long
Dim lngLeadDays As Long
Dim lngTKEY     As Long

    blnSOQOrder = False
    Call rsDueDate.Open("SELECT DDAT, TKEY FROM PURHDR WHERE TKEY = (select min(HKEY) from purlin where skun = '" & strSKU & "' and dele=0)", goDatabase.Connection)
    If (rsDueDate.EOF = True) Then
        blnSOQOrder = True
        Set oSuppDetBO = goDatabase.CreateBusinessObject(CLASSID_SUPPLIERDET)
        Call oSuppDetBO.IBo_AddLoadFilter(CMP_EQUAL, FID_SUPPLIERDET_SupplierNo, strSupplierCode)
        Call oSuppDetBO.LoadMatches
        lngToday = Format(Date, "w", vbMonday)
        'Find out what today is and when next SOQ Review/Order date is
        lngLeadDays = oSuppDetBO.LeadDays(lngToday)
        NextDeliveryDate = DateAdd("d", lngLeadDays, Date)
    Else
        NextDeliveryDate = rsDueDate(0)
        lngTKEY = rsDueDate(1)
    End If
    'Get Order Quantity for SKU
    Call rsQtyOnOrder.Open("SELECT QTYO FROM PURLIN WHERE HKEY = " + CStr(lngTKEY) + " AND SKUN ='" & strSKU & "'", goDatabase.Connection)
    If (rsQtyOnOrder.EOF = True) Then
        QuantityOrdered = 0
    Else
        QuantityOrdered = rsQtyOnOrder(0)
    End If
End Function

Private Sub cmdCancel_Click()
    
Dim strMsg As String

    If mblnCreatingOrder = True And mstrOrderQuit <> "Y" Then
'        strMsg = goSession.GetParameter(PRM_TAKE_NOW_MSG)
'        mstrOrderQuit = "Y"
'        If MsgBoxEx(strMsg, vbYesNo + vbDefaultButton2, "Order", , , , , RGBMsgBox_WarnColour) = vbNo Then
'            mstrOrderQuit = "N"
'            Exit Sub
'        End If
    End If
    
    Set mcolEntries = Nothing
    mblnConfirmCollect = False
    mblnCreatingOrder = False
    mblnCancel = True
    Me.Hide
    
End Sub

Private Sub cmdCollectAll_Click()

Dim lngOldRow   As Long
    
    lngOldRow = sprdItems.Row
    sprdItems.Row = -1
    sprdItems.Col = COL_BD_COLLECT
    sprdItems.Text = "Y"
    sprdItems.Row = lngOldRow

End Sub

Private Sub cmdComplete_Click()

Dim lngLineNo           As Long
Dim blnInvalidDelivery  As Boolean
Dim intIndex            As Integer
Dim SkuList             As String
Dim QtyList             As String

    If mblnTranDiscount Then
    
        If goSession.GetParameter(981404) = True Then
        
            If CheckDiscountValues(SkuList, QtyList) Then
            
                Dim MultiSkuOveride As COMTPWickes_InterOp_Interface.IMultiplePriceOverride
                Dim Factory As New COMTPWickes_InterOp_Wrapper.MultiplePriceOverride
                Dim TotalPrice As Double
                TotalPrice = ntxtTotal.Value
                
                Set MultiSkuOveride = Factory.FactoryGet
                
                If MultiSkuOveride.SetNewPrice(SkuList, QtyList, mStoreOpenDate, TotalPrice, moUser.EmployeeID, mlngAuthorisor) Then
                   ' Call MsgBoxEx("Saved new Multi Sku Price Override.", vbOKOnly, "Allocated Override Discount", , , , , RGBMSGBox_PromptColour)
                Else
                    Call MsgBoxEx("Unable to save Multi Sku price override.", vbOKOnly, "Allocate Discount", , , , , RGBMsgBox_WarnColour)
                    Exit Sub
                End If
            Else
                Exit Sub
            End If
        End If
        
    Else
    
        CheckTakeNow (sprdItems.ActiveRow)
        If CheckTakeNow(sprdItems.ActiveRow, False) = True Then
            Exit Sub
        End If
        
        sprdItems.Col = COL_BD_TOBEDELIVERED
        For intIndex = 1 To sprdItems.MaxRows
            sprdItems.Row = intIndex
            If CInt(sprdItems.Text) = 0 Then
                blnInvalidDelivery = True
            Else
                blnInvalidDelivery = False
                Exit For
            End If
        Next intIndex
        
        If mblnForDelivery = True And CInt(sprdItems.Text) = 0 Then
            Call MsgBoxEx("Cannot have zero delivery Items." & vbCrLf & "Invalid delivery Amount.", vbOKOnly, "Allocate Discount", , , , , RGBMsgBox_WarnColour)
            Exit Sub
        End If
        
        mlngTakeNow = 0
        Set mcolEntries = New Collection
        ReDim mstrAllocItems(0)
        mcurSelectedValue = 0
        For lngLineNo = 1 To sprdItems.MaxRows Step 1
            sprdItems.Row = lngLineNo
            sprdItems.Col = COL_BD_COLLECT
            If (sprdItems.Text = "Y") Then
                sprdItems.Col = COL_BD_LINENO
                mcolEntries.Add (sprdItems.Text)
                ReDim Preserve mstrAllocItems(UBound(mstrAllocItems) + 1)
                mstrAllocItems(UBound(mstrAllocItems)) = sprdItems.Text & ""
                sprdItems.Col = COL_BD_TOTAL
                mcurSelectedValue = mcurSelectedValue + Val(sprdItems.Value)
                sprdItems.Col = COL_BD_QTYTAKENOW
            End If
            sprdItems.Col = COL_BD_QTYTAKENOW
            mlngTakeNow = mlngTakeNow + Val(sprdItems.Text)
        
        Next lngLineNo
        If (mcurSelectedValue < Val(lblAllocDisc.Caption)) Then
            Call MsgBoxEx("Value of selected items does not allocate all discount." & vbCrLf & "Discount must be fully allocated before saving.", vbOKOnly, "Allocate Discount", , , , , RGBMsgBox_WarnColour)
            ReDim mstrAllocItems(0)
            Exit Sub
        End If
    End If
    
    mblnConfirmCollect = True
    mblnCancel = False
    Me.Hide

End Sub

Private Sub cmdDeselectAll_Click()

End Sub

Private Sub cmdReset_Click()

Dim lngOldRow   As Long
    
    lngOldRow = sprdItems.Row
    sprdItems.Row = -1
    sprdItems.Col = COL_BD_COLLECT
    sprdItems.Text = "N"
    sprdItems.Row = lngOldRow

End Sub

Private Sub cmdSelectAll_Click()
    
    Dim lngRowNo As Long
            
    For lngRowNo = 1 To sprdItems.MaxRows Step 1
        
        sprdItems.Row = lngRowNo
        sprdItems.Col = COL_BD_COLLECT
        sprdItems.Text = "Y"
    Next lngRowNo
    ApplyDiscountCalc

End Sub

Private Sub cmdUndoAll_Click()
    
    Dim lngRowNo As Long
            
    For lngRowNo = 1 To sprdItems.MaxRows Step 1
        
        sprdItems.Row = lngRowNo
        sprdItems.Col = COL_BD_COLLECT
        sprdItems.Text = "N"
    Next lngRowNo
    ApplyDiscountCalc

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

Dim Ctrl As Control

    Select Case (KeyCode)
        Case (mlngCloseKeyCode):
                cmdCancel.Value = True
                KeyCode = 0
        Case (mlngResetKeyCode):
                cmdReset.Value = True
                KeyCode = 0
        Case (mlngUseKeyCode):
                cmdComplete.Value = True
                KeyCode = 0
        Case (mlngSelectAllKeyCode):
                cmdSelectAll.Value = True
                KeyCode = 0
        Case (mlngAllKeyCode):
                cmdCollectAll.Value = True
                KeyCode = 0
        Case (mlngUnSelectAllKeyCode):
                cmdUndoAll.Value = True
                KeyCode = 0
        Case (vbKeyReturn)
                If mblnTranDiscount Then Exit Sub
                CheckTakeNow (sprdItems.ActiveRow)
                sprdItems.EditMode = False
                KeyCode = 0
        Case (mlngItemsKeyCode):
                'cmdSelectItems.Value = True
                'KeyCode = 0
                'Exit Sub
        Case (vbKeySubtract)
                If mblnTranDiscount Then KeyCode = 0
        Case (mlngNumericPadSubtractkey)
                If mblnTranDiscount Then KeyCode = 0
    End Select

End Sub

Private Sub Form_Load()

Dim strKey  As String

    sprdItems.MaxRows = 0
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    strKey = goSession.GetParameter(PRM_KEY_RESET)
    cmdReset.Caption = strKey & "-" & cmdReset.Caption
    If Left$(strKey, 1) = "F" Then
        mlngResetKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngResetKeyCode = Asc(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_CLOSE)
    cmdCancel.Caption = strKey & "-" & cmdCancel.Caption
    If Left$(strKey, 1) = "F" Then
        mlngCloseKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngCloseKeyCode = Asc(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_SPECIAL)
    cmdCollectAll.Caption = strKey & "-" & cmdCollectAll.Caption
    If Left$(strKey, 1) = "F" Then
        mlngAllKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngAllKeyCode = Asc(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_SAVE)
    cmdComplete.Caption = strKey & "-" & cmdComplete.Caption
    If Left$(strKey, 1) = "F" Then
        mlngUseKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngUseKeyCode = Asc(UCase$(strKey))
    End If
    
    strKey = "F9"
    cmdSelectAll.Caption = strKey & "-" & cmdSelectAll.Caption
    
    strKey = "F10"
    cmdUndoAll.Caption = strKey & "-" & cmdUndoAll.Caption
    
    If Left$(strKey, 1) = "F" Then
        mlngItemsKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngItemsKeyCode = AscW(UCase$(strKey))
    End If

    mlngNumericPadSubtractkey = 189
    
    Call CentreForm(Me)

End Sub


Private Sub Form_Resize()
'    If Me.WindowState <> vbMinimized Then
'        Me.Width = Screen.Width
'        sprdItems.Width = Me.Width - 150
'        cmdCancel.Left = (Me.Width - cmdCancel.Width) - 150
'        cmdComplete.Left = (cmdCancel.Left - cmdComplete.Width) - 150
'        Me.Left = 75
'    End If
End Sub

Private Sub ntxtTotal_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeySubtract Or KeyCode = mlngNumericPadSubtractkey Then
        KeyCode = 0
    End If
End Sub

Private Sub sprdItems_BeforeEditMode(ByVal Col As Long, ByVal Row As Long, ByVal UserAction As FPSpreadADO.BeforeEditModeActionConstants, CursorPos As Variant, Cancel As Variant)
    If Col = COL_BD_QTYTAKENOW Then
        sprdItems.EditModeReplace = True
    End If
End Sub

Private Sub sprdItems_EditChange(ByVal Col As Long, ByVal Row As Long)
    mblnEditModeChange = True
End Sub

Private Sub sprdItems_EnterRow(ByVal Row As Long, ByVal RowIsLast As Long)
    mlngRow = Row
End Sub

Private Sub sprdItems_KeyPress(KeyAscii As Integer)

Dim intQtyTakeNow As Integer
Dim lngQtyOrdered As Long

    If sprdItems.ActiveCol = COL_BD_QTYTAKENOW Then
        Select Case KeyAscii
            Case vbKey0 To vbKey9
                KeyAscii = KeyAscii
            Case vbKeyBack
                KeyAscii = KeyAscii
            Case Else
                KeyAscii = 0
        End Select
        Exit Sub
    End If
    
    If mblnTranDiscount = False Then
        If mblnIsCollection = False And sprdItems.ActiveCol <> COL_BD_COLLECT Then
            Exit Sub
        End If
    End If
    
   
    If (KeyAscii = vbKeyReturn) Or (KeyAscii = vbKeySpace) Then 'just toggle
        sprdItems.Row = sprdItems.ActiveRow
        sprdItems.Col = COL_BD_COLLECT
        If (sprdItems.Text = "N") Then
            sprdItems.Text = "Y"
        Else
            sprdItems.Text = "N"
        End If
    End If

    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If (KeyAscii = vbKeyY) Then
        sprdItems.Row = sprdItems.ActiveRow
        sprdItems.Col = COL_BD_COLLECT
        sprdItems.Text = "Y"
    End If

    If (KeyAscii = vbKeyN) Then
        sprdItems.Row = sprdItems.ActiveRow
        sprdItems.Col = COL_BD_COLLECT
        sprdItems.Text = "N"
    End If
    
    If mblnIsCollection = False And mblnTranDiscount = False Then
        sprdItems.Col = COL_BD_QTYTAKENOW
        intQtyTakeNow = Val(sprdItems.Text)
        If intQtyTakeNow <= 0 Then
            sprdItems.Row = sprdItems.ActiveRow
            sprdItems.Col = COL_BD_COLLECT
            sprdItems.Text = "N"
        End If
    End If
    
    If mblnTranDiscount Then ApplyDiscountCalc

End Sub

Private Sub sprdItems_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)

    If Col = COL_BD_QTYTAKENOW And mblnEditModeChange = True Then
        CheckTakeNow (Row)
    End If
    
'    If NewCol = COL_BD_QTYTAKENOW Then
'        sprdItems.Col = COL_BD_QTYTAKENOW
'        sprdItems.EditMode = True
'        sprdItems.SelStart = 0
'        sprdItems.SelLength = Len(sprdItems.Text)
'        sprdItems.SelModeSelected = True
'        'SendKeys ("+{LEFT}")
'    End If
    
End Sub

Public Sub PopulateTakeNow()

Dim lngRow As Long
Dim lngItemNo As Long
Dim strBdSku As String
Dim strLineSku As String
Dim strBackDoorCollect As String
Dim intQtyTakeNow As Integer
Dim intLineQty As Integer

    If mblnCancel = True Then Exit Sub
    For lngRow = 1 To sprdItems.MaxRows Step 1
        sprdItems.Row = lngRow
        'Get Part Code
        sprdItems.Col = COL_BD_PCODE
        strBdSku = sprdItems.Text
        'Get Take Now Qty
        sprdItems.Col = COL_BD_QTYTAKENOW
        intQtyTakeNow = Val(sprdItems.Text)
        'Get Back Door Collect Info
        sprdItems.Col = COL_BD_COLLECT
        strBackDoorCollect = sprdItems.Text
        
        'Now amend the transaction lines wit appropriate take now and back door info
        With frmTill
            For lngItemNo = 2 To .sprdLines.MaxRows Step 1
                .sprdLines.Row = lngItemNo
                .sprdLines.Col = COL_VOIDED
                If (Val(.sprdLines.Value) = 0) Then
                    .sprdLines.Col = COL_SALETYPE
                    If (.sprdLines.Value <> "D") Then
                        .sprdLines.Col = COL_PARTCODE
                        If strBdSku = .sprdLines.Text Then
                            'Same as Back door sku
                            .sprdLines.Col = COL_QTY
                            intLineQty = Val(.sprdLines.Text)
                            .sprdLines.Col = COL_TAKE_NOW
                            If intQtyTakeNow > 0 Then
                               If intQtyTakeNow <= intLineQty Then
                                   .sprdLines.Text = CStr(intQtyTakeNow)
                                   intQtyTakeNow = 0
                               Else
                                   .sprdLines.Text = CStr(intLineQty)
                                   intQtyTakeNow = intQtyTakeNow - intLineQty
                               End If
                            Else
                                .sprdLines.Text = ""
                            End If
                            .sprdLines.Col = COL_BACKCOLLECT
                            .sprdLines.Text = strBackDoorCollect
                        End If
                    End If
                End If
            Next lngItemNo
        End With
    Next lngRow
End Sub


Private Function CheckTakeNow(Row As Integer, Optional blnShowMsg As Boolean = True) As Boolean

Dim lngCustomerOrder    As Long
Dim lngTakeNow          As Long
Dim lngQtyOnHand       As Long
Dim lngQtyOrdered      As Long
Dim blnError As Boolean

    blnError = False
    mblnEditModeChange = False
    sprdItems.Row = Row
    sprdItems.Col = COL_BD_ONHAND
    lngQtyOnHand = Val(sprdItems.Text)
    sprdItems.Col = COL_BD_QTY
    lngQtyOrdered = Val(sprdItems.Text)
    sprdItems.Col = COL_BD_QTYTAKENOW
    
    If Val(sprdItems.Text) > Val(lngQtyOrdered) Then
        Call MsgBoxEx("You have entered a quantity greater than the customer ordered.", vbOKOnly, "Invalid Entry", , , , , RGBMsgBox_WarnColour)
        sprdItems.Text = CStr(lngQtyOrdered)
        blnError = True
    End If
    
    If Val(sprdItems.Text) < 0 Then
        Call MsgBoxEx("You have entered a quantity less than zero.", vbOKOnly, "Invalid Entry", , , , , RGBMsgBox_WarnColour)
        sprdItems.Text = CStr(lngQtyOrdered)
        blnError = True
    End If
    
    If blnError = False Then
        If (Val(sprdItems.Text) > lngQtyOnHand) And Val(sprdItems.Text) <> 0 Then
            If blnShowMsg Then
                Call MsgBoxEx("You have entered a quantity greater than is available.", vbOKOnly, "Warning", , , , , RGBMsgBox_WarnColour)
            End If
        End If
    End If
    sprdItems.Col = COL_BD_QTY
    
    lngCustomerOrder = Val(sprdItems.Text)
    sprdItems.Col = COL_BD_QTYTAKENOW
    lngTakeNow = Val(sprdItems.Text)
    sprdItems.Col = COL_BD_TOBEDELIVERED
    sprdItems.Text = IIf((lngCustomerOrder - lngTakeNow) < 0, 0, (lngCustomerOrder - lngTakeNow))
    sprdItems.Col = COL_BD_QTYTAKENOW
    
    sprdItems.Col = COL_BD_QTYTAKENOW
    If sprdItems.Text = "0" Then
        sprdItems.Col = COL_BD_COLLECT
        sprdItems.Text = "N"
    End If
    CheckTakeNow = blnError
End Function

'******************************
'P014-04 Multiple Sku Select
'Author: Michael O'Cain
'Date:   21/12/2011
'
'******************************
Private Sub CalculateDiscounts(EditingField As Integer)

Dim curOrigTotal As Currency
Dim curNewTotal As Currency
Dim curDiscValue As Currency
Dim curDiscRate As Currency

    If (Me.Visible = False) Then Exit Sub
    
    mcurOrigAmount = GetTotalSelected()
    If mcurOrigAmount = 0 Then Exit Sub
    
    curDiscRate = ntxtDiscRate.Value
    curDiscValue = ntxtDiscValue.Value
    curNewTotal = ntxtTotal.Value
    curOrigTotal = mcurOrigAmount 'mdblTransactionTotal
    
    Select Case (EditingField)
        Case (0): ntxtDiscValue.Value = curOrigTotal - curNewTotal
                  curDiscValue = ntxtDiscValue.Value
                  ntxtDiscRate.Value = Format(((curOrigTotal - curNewTotal) / curOrigTotal) * 100, "0.00")
        Case (1): ntxtTotal.Value = curOrigTotal - curDiscValue
                  curNewTotal = ntxtTotal.Value
                  ntxtDiscRate.Value = Format((curOrigTotal - curNewTotal) / curOrigTotal * 100, "0.00")
        Case (2): ntxtDiscValue.Value = curOrigTotal * (curDiscRate / 100)
                  curDiscValue = ntxtDiscValue.Value
                  ntxtTotal.Value = curOrigTotal - curDiscValue
    End Select

End Sub

Private Sub ntxtTotal_Change()
    
    If (ActiveControl Is Nothing = False) Then
        If (ActiveControl.Name = "ntxtTotal") Then Call CalculateDiscounts(0)
    End If

End Sub

Private Sub ntxtTotal_GotFocus()

    ntxtTotal.SelectAllValue
    ntxtTotal.BackColor = RGBEdit_Colour

End Sub 'ntxtTotal_GotFocus

Private Sub ntxtTotal_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        mblnCancelled = True
        Call Me.Hide
    End If
    If (KeyAscii = vbKeySubtract) Then
        KeyAscii = 0
        'Call SendKeys(vbTab)
    End If

End Sub 'ntxtTotal_KeyPress

Private Sub ntxtTotal_LostFocus()

    ntxtTotal.BackColor = RGB_WHITE
    ApplyDiscountCalc
End Sub 'ntxtTotal_LostFocus

Private Sub ntxtDiscValue_Change()
    
    If (ActiveControl Is Nothing = False) Then
        If (ActiveControl.Name = "ntxtDiscValue") Then Call CalculateDiscounts(1)
    End If

End Sub

Private Sub ntxtDiscValue_GotFocus()

    ntxtDiscValue.SelectAllValue
    ntxtDiscValue.BackColor = RGBEdit_Colour

End Sub 'ntxtDiscValue_GotFocus

Private Sub ntxtDiscValue_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub 'ntxtDiscValue_KeyPress

Private Sub ntxtDiscValue_LostFocus()

    ntxtDiscValue.BackColor = RGB_WHITE
    ApplyDiscountCalc
    
End Sub 'ntxtDiscValue_LostFocus

Private Sub ntxtDiscRate_Change()

    If (ActiveControl Is Nothing = False) Then
        If (ActiveControl.Name = "ntxtDiscRate") Then Call CalculateDiscounts(2)
    End If

End Sub

Private Sub ntxtDiscRate_GotFocus()

    ntxtDiscRate.SelectAllValue
    ntxtDiscRate.BackColor = RGBEdit_Colour

End Sub 'ntxtDiscRate_GotFocus

Private Sub ntxtDiscRate_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub 'ntxtDiscRate_KeyPress

Private Sub ntxtDiscRate_LostFocus()

    ntxtDiscRate.BackColor = RGB_WHITE
    ApplyDiscountCalc
    
End Sub 'ntxtDiscRate_LostFocus

Private Sub ApplyDiscountCalc()


    Dim lngRowNo As Long
    Dim Total As Currency
    Dim DiscountValue As Currency
    Dim DiscountedTotal As Currency
    Dim NumberOfDiscounts As Integer
    Dim curDiscountRate As Currency
    
    'Get Total for Deal Group
    mcurOrigAmount = GetTotalSelected()
    Call CalculateDiscounts(0)
    
    NumberOfDiscounts = 0
    curDiscountRate = ntxtDiscRate.Value
    
    For lngRowNo = 1 To sprdItems.MaxRows Step 1
        
        sprdItems.Row = lngRowNo
        
        sprdItems.Col = COL_BD_TOTAL
        Total = Val(sprdItems.Value)
        
        sprdItems.Col = COL_BD_COLLECT
        If sprdItems.Text = "N" Then

            sprdItems.Col = COL_BD_DISCOUNTED_VALUE
            DiscountValue = Total
            sprdItems.Value = Format(Total, "0.00")
            
            sprdItems.Col = COL_BD_DISCOUNTED_TOTAL
            DiscountedTotal = 0
            sprdItems.Value = "0.00"
        
        Else
        
            NumberOfDiscounts = NumberOfDiscounts + 1
            
            sprdItems.Col = COL_BD_DISCOUNTED_VALUE
            DiscountValue = Total * (curDiscountRate / 100)
            sprdItems.Value = Format(DiscountValue, "0.00")
            
            sprdItems.Col = COL_BD_DISCOUNTED_TOTAL
            DiscountedTotal = Total - DiscountValue
            sprdItems.Value = Format(DiscountedTotal, "0.00")
            
        End If
    
    Next lngRowNo

    'mcurOrigAmount = GetDealGroupTotal()
    mcurOrigAmount = GetTotalSelected()
    
    cmdSelectAll.Enabled = True
    cmdUndoAll.Enabled = True
    
    If NumberOfDiscounts = sprdItems.MaxRows Then
        cmdSelectAll.Enabled = False
        cmdUndoAll.Enabled = True
    End If
    
    If NumberOfDiscounts = 0 Then
        cmdSelectAll.Enabled = True
        cmdUndoAll.Enabled = False
    End If
        
End Sub


Private Function CheckDiscountValues(ByRef SkuList As String, ByRef QtyList As String) As Boolean

    Dim lngRowNo As Long
    Dim intNumSelected As Integer
    Dim TotalDiscount As Currency
    Dim NewTotal As Currency
    Dim DiscRate As Currency
    Dim difference As Currency
    Dim lngKeyIn As Long
    Dim strSummary As String
    Dim SkuArray() As String
    Dim QtyArray() As Integer
    Dim Sku As String
    Dim Qty As Integer
    Dim Index As Integer
    Dim TotalQty As Integer
    
    ReDim Preserve SkuArray(0)
    ReDim Preserve QtyArray(0)
    
    SkuList = ""
    QtyList = ""
    strSummary = ""
    TotalDiscount = 0
    TotalQty = 0
    CheckDiscountValues = True
    
    Call ApplyDiscountCalc
    
    For lngRowNo = 1 To sprdItems.MaxRows Step 1
        
        sprdItems.Row = lngRowNo
    
        sprdItems.Col = COL_BD_COLLECT
        If sprdItems.Text = "Y" Then
                    
            sprdItems.Col = COL_BD_PCODE
            Sku = sprdItems.Text
            
            sprdItems.Col = COL_BD_QTY
            Qty = Val(sprdItems.Text)
            TotalQty = TotalQty + Qty
            
            Call AddToSkuArray(SkuArray, Sku, QtyArray, Qty)
            
            sprdItems.Col = COL_BD_DISCOUNTED_TOTAL
            TotalDiscount = TotalDiscount + sprdItems.Value
            
            intNumSelected = intNumSelected + 1
            
        End If
    
    Next lngRowNo
    
    'Check Total Discount matches lesser value, if not and its within a pound add difference to last row ?
    NewTotal = ntxtTotal.Value
    DiscRate = ntxtDiscRate.Value
    
    If NewTotal <= 0 Then
        Call MsgBoxEx("Negative or Zero deal group price value entered.", vbOKOnly, "Invalid Discount", , , , , RGBMsgBox_WarnColour)
        ntxtTotal.SetFocus
        CheckDiscountValues = False
        Exit Function
    End If
    
    If TotalQty = 1 Then
        Call MsgBoxEx("You need to add more items to the Override as the total cannot be reached with current values.", vbOKOnly, "Invalid Discount", , , , , RGBMsgBox_WarnColour)
        ntxtTotal.SetFocus
        CheckDiscountValues = False
        Exit Function
    End If
                
    'Check Total Discount is not greater than lesser value
    If TotalDiscount > mdblTransactionTotal Then
        Call MsgBoxEx("You have entered a value more than original transaction value.", vbOKOnly, "Invalid Discount", , , , , RGBMsgBox_WarnColour)
        ntxtTotal.SetFocus
        CheckDiscountValues = False
        Exit Function
    End If
    
    If intNumSelected = 0 Then
        Call MsgBoxEx("No Sku's selected for Multi Sku override.", vbOKOnly, "Invalid Discount", , , , , RGBMsgBox_WarnColour)
        CheckDiscountValues = False
        Exit Function
    End If
    
'    If intNumSelected = 1 Then
'        Call MsgBoxEx("Not enough Sku's selected for Multi Sku override.", vbOKOnly, "Invalid Discount", , , , , RGBMsgBox_WarnColour)
'        CheckDiscountValues = False
'        Exit Function
'    End If
    
    If DiscRate < 0 Then
        Call MsgBoxEx("Invalid discount tried to be applied, amend and try again.", vbOKOnly, "Invalid Discount", , , , , RGBMsgBox_WarnColour)
        ntxtTotal.SetFocus
        CheckDiscountValues = False
        Exit Function
    End If
    
    'All Ok prepare List for .Net
    For Index = 0 To UBound(SkuArray)
        If SkuArray(Index) <> "" Then
            SkuList = SkuList + SkuArray(Index) + ","
            QtyList = QtyList + CStr(QtyArray(Index)) + ","
            strSummary = strSummary + SkuArray(Index) + " " + GetDescriptionForSku(SkuArray(Index)) + " Qty: " + CStr(QtyArray(Index)) + vbCrLf
        End If
    Next Index
    
    If Len(strSummary) > 0 Then
        strSummary = strSummary + vbCrLf + "New Deal Group Total " & ChrW(163) & Format(NewTotal, "0.00") + vbCrLf
        strSummary = strSummary + vbCrLf + "Are you sure you want to save this temporary deal group override for today ?"
    End If
    
    lngKeyIn = MsgBoxEx("Summary of Temporary Deal Group." & vbCrLf & "For today " & Format(Now(), "dd MMM YYYY") & ": " & vbCrLf & vbCrLf & strSummary, vbYesNo + vbDefaultButton2, "Summary of Temporary Deal Group", , , , , RGBMSGBox_PromptColour)
    If (lngKeyIn = vbNo) Then
        CheckDiscountValues = False
        Exit Function
    End If
    
    If Len(SkuList) > 0 Then
        SkuList = Mid(SkuList, 1, Len(SkuList) - 1)
        QtyList = Mid(QtyList, 1, Len(QtyList) - 1)
    End If
    
    
End Function

Private Function GetTotalSelected() As Currency
Dim lngRowNo As Long
    
    GetTotalSelected = 0
    For lngRowNo = 1 To sprdItems.MaxRows Step 1
        
        sprdItems.Row = lngRowNo
        
        sprdItems.Col = COL_BD_COLLECT
        If sprdItems.Text = "Y" Then
            sprdItems.Row = lngRowNo
            sprdItems.Col = COL_BD_TOTAL
            GetTotalSelected = GetTotalSelected + sprdItems.Value
        End If
    
    Next lngRowNo
    
    fraDiscount.Caption = "Current Deal Group Total: " & ChrW(163) & Format(GetTotalSelected, "0.00")
End Function

Private Sub AddToSkuArray(ByRef SkuArray() As String, ByVal Skun As String, ByRef QtyArray() As Integer, CurrentQty As Integer)

    Dim blnAddNew As Boolean
    Dim Index As Integer
    blnAddNew = True
        
    For Index = 0 To (UBound(SkuArray) - 1)
        If SkuArray(Index) = Skun Then
            QtyArray(Index) = QtyArray(Index) + CurrentQty
            blnAddNew = False
            Exit For
        End If
    Next Index

    If blnAddNew Then
        SkuArray(Index) = Skun
        QtyArray(Index) = CurrentQty
        
        ReDim Preserve SkuArray(Index + 1)
        ReDim Preserve QtyArray(Index + 1)
    End If

End Sub

Private Function GetDescriptionForSku(Sku As String) As String

Dim lngRowNo As Long

    GetDescriptionForSku = ""
    
    For lngRowNo = 1 To sprdItems.MaxRows Step 1
        sprdItems.Row = lngRowNo
        sprdItems.Col = COL_BD_PCODE
        If sprdItems.Text = Sku Then
            sprdItems.Col = COL_BD_DESC
            GetDescriptionForSku = sprdItems.Text
            Exit Function
        End If
    Next lngRowNo

End Function
