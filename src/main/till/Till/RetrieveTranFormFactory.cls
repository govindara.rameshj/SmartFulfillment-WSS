VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "RetrieveTranFormFactory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Const m_TransactionLookupResetVoidedFlagParameterID As Long = 980979
Private m_InitialisedUseTransactionLookupResetVoidedFlagImplementation As Boolean
Private m_UseTransactionLookupResetVoidedFlagImplementation As Boolean

Friend Property Get UseTransactionLookupResetVoidedFlagImplementation() As Boolean

    UseTransactionLookupResetVoidedFlagImplementation = m_UseTransactionLookupResetVoidedFlagImplementation
End Property

Public Function FactoryGetTransactionLookupResetVoidedFlag() As ITxnLookupResetVoidFlag
    
    If UseTransactionLookupResetVoidedFlagImplementation Then
        'using implementation with RF0979 change
        Set FactoryGetTransactionLookupResetVoidedFlag = New TranLookupResetVoidFlag
    Else
        'using live implementation
        Set FactoryGetTransactionLookupResetVoidedFlag = New RetrieveTranFormLive
    End If
End Function

Private Sub Class_Initialize()

    GetUseTransactionLookupResetVoidedFlagParameterValue
End Sub

Friend Sub GetUseTransactionLookupResetVoidedFlagParameterValue()

    If Not m_InitialisedUseTransactionLookupResetVoidedFlagImplementation Then
On Error Resume Next
        m_UseTransactionLookupResetVoidedFlagImplementation = goSession.GetParameter(m_TransactionLookupResetVoidedFlagParameterID)
        m_InitialisedUseTransactionLookupResetVoidedFlagImplementation = True
On Error GoTo 0
    End If
End Sub

