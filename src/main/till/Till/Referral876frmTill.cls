VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Referral876frmTill"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements IfrmTill876

Private m_InProcessPaymentTypeMethod As Boolean

Private Function IfrmTill876_InProcessPaymentTypeMethod() As Boolean

    IfrmTill876_InProcessPaymentTypeMethod = m_InProcessPaymentTypeMethod
End Function

Private Sub IfrmTill876_SetForInProcessPaymentType()

    m_InProcessPaymentTypeMethod = True
End Sub

Private Sub IfrmTill876_SetForOutOfProcessPaymentType()

    m_InProcessPaymentTypeMethod = False
End Sub
