VERSION 5.00
Begin VB.Form frmComplete 
   BorderStyle     =   0  'None
   Caption         =   "Close cash drawer"
   ClientHeight    =   3255
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4575
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   3255
   ScaleWidth      =   4575
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox fraComplete 
      Appearance      =   0  'Flat
      Height          =   3255
      Left            =   0
      ScaleHeight     =   3225
      ScaleWidth      =   4545
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      Width           =   4575
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         Caption         =   "Close cash drawer"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   0
         TabIndex        =   1
         Top             =   600
         Width           =   4575
      End
   End
End
Attribute VB_Name = "frmComplete"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const MODULE_NAME As String = "frmComplete"

Private moCashDrawer As OPOSCashDrawer
Private mblnClosingDrawer As Boolean

Public Event Duress()
Private mintDuressKeyCode As Integer

Public Property Set CashDrawer(ByVal Value As OPOSCashDrawer)
    Set moCashDrawer = Value
End Property

Public Sub DisplayMessage(Optional ByVal Value As String = vbNullString)
    If LenB(Value) = 0 Then
        Label6.Caption = "Close cash drawer"
        mblnClosingDrawer = True
        If moCashDrawer.DrawerOpened = True Then
            Me.Show vbModal
        Else
            Exit Sub
        End If
    Else
        Label6.Caption = Value
        mblnClosingDrawer = False
    End If
End Sub

Private Sub Form_Activate()
    
    While moCashDrawer.DrawerOpened = True
        
        DoEvents
    Wend
    Me.Hide
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Call DebugMsg(MODULE_NAME, "KeyDown", endlDebug, "Key=" & KeyCode)

End Sub
