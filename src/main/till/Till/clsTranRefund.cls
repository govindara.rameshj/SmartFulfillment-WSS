VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsTranRefund"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarLineNo As Integer 'local copy
Private mvarPartCode As String 'local copy
Private mvarQuantity As Integer 'local copy
Private mvarPrice As Double 'local copy

Public Property Let Price(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Price = 5
    mvarPrice = vData
End Property


Public Property Get Price() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Price
    Price = mvarPrice
End Property


Public Property Let Quantity(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Quantity = 5
    mvarQuantity = vData
End Property


Public Property Get Quantity() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Quantity
    Quantity = mvarQuantity
End Property


Public Property Let PartCode(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PartCode = 5
    mvarPartCode = vData
End Property


Public Property Get PartCode() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PartCode
    PartCode = mvarPartCode
End Property


Public Property Let LineNo(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.LineNo = 5
    mvarLineNo = vData
End Property


Public Property Get LineNo() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.LineNo
    LineNo = mvarLineNo
End Property



