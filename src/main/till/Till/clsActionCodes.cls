VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsActionCodes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Public Enum enActionCode
    enacPark = 0
    enacVoid = 1
    enacOrder = 2
    enacQuantity = 3
    enacReversal = 4
    enacTender = 5
    enacEnquiry = 6
    enacOverride = 7
    enacLookUp = 8
    enacDeptSale = 9
    enacColleagueDiscount = 10
    enacSpecialItem = 11
    enacGiftVoucher = 12
    enacBackdoorItems = 13
    enacPIMEnquiry = 14
    enacSaveQuote = 15
    enacRecallQuote = 16
    enacCreateOrder = 17
    enacDiscountScheme = 18
    enacTransactionDisc = 19
    enacAmendPrice = 20
    enacBlank = 21 'MO'C WIX1376 - Added to Enable a blank button
End Enum

Public ActionCode As enActionCode
Public ButtonDesc As String
Public Visible As Boolean
Public HotKeyCode As String
Public ActionId As String   'MO'C WIX1376 - Added to Enable user to choose button order

