VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TillFormFactory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Const m_ProcessingPaymentParameterID As Long = 980876
Private m_InitialisedUseProcessingPaymentImplementation As Boolean
Private m_UseProcessingPaymentImplementation As Boolean

Private Const m_TrainingModeOrdersParameterID As Long = 980064
Private m_InitialisedUseTrainingModeOrdersImplementation As Boolean
Private m_UseTrainingModeOrdersImplementation As Boolean

Private Const m_BuyCouponsParameterID As Long = 983351
Private m_InitialisedUseBuyCouponsImplementation As Boolean
Private m_UseBuyCouponsImplementation As Boolean

Private Const m_GetCouponsParameterID As Long = 983352
Private m_InitialisedUseGetCouponsImplementation As Boolean
Private m_UseGetCouponsImplementation As Boolean

Private Const m_DotnetOfflineModeParameterID As Long = 984151
Private m_InitialisedUseDotnetOfflineModeImplementation As Boolean
Private m_UseDotnetOfflineModeImplementation As Boolean

Private Const m_RefundTenderTypeParameterID As Long = 980082
Private m_InitialisedUseRefundTenderTypeImplementation As Boolean
Private m_UseRefundTenderTypeImplementation As Boolean

Private Const m_PICRefundCountProcessParameterID As Long = 980081
Private m_InitialisedUsePICRefundCountProcessImplementation As Boolean
Private m_UsePICRefundCountProcessImplementation As Boolean

Private Const m_PreventRepeatPaymentParameterID As Long = 981094
Private m_InitialisedUsePreventRepeatPaymentImplementation As Boolean
Private m_UsePreventRepeatPaymentImplementation As Boolean

Friend Property Get UseProcessingPaymentImplementation() As Boolean
    
    UseProcessingPaymentImplementation = m_UseProcessingPaymentImplementation
End Property

Public Function FactoryGetProcessingPayment() As IProcessingPayment
    
    If UseProcessingPaymentImplementation Then
        'using implementation with 876 fix
        Set FactoryGetProcessingPayment = New ProcessingPayment
    Else
        'using live implementation
        Set FactoryGetProcessingPayment = New TillFormLive
    End If
End Function

Friend Property Get UseRefundTenderTypeImplementation() As Boolean
    
    UseRefundTenderTypeImplementation = m_UseRefundTenderTypeImplementation
End Property
Public Function FactoryGetRefundTenderType() As IRefundTenderType
    
    If UseRefundTenderTypeImplementation Then
        'using implementation with CR0082
        Set FactoryGetRefundTenderType = New RefundTenderType
    Else
        'using live implementation
        Set FactoryGetRefundTenderType = New TillFormLive
    End If
End Function


Friend Property Get UseTrainingModeOrdersImplementation() As Boolean
    
    UseTrainingModeOrdersImplementation = m_UseTrainingModeOrdersImplementation
End Property

Public Function FactoryGetTrainingModeOrders() As ITrainingModeOrders
    
    If UseTrainingModeOrdersImplementation Then
        'using implementation with CR 64
        Set FactoryGetTrainingModeOrders = New TrainingModeOrders
    Else
        'using live implementation
        Set FactoryGetTrainingModeOrders = New TillFormLive
    End If
End Function

Friend Property Get UseBuyCouponImplementation() As Boolean

    UseBuyCouponImplementation = m_UseBuyCouponsImplementation
End Property

Public Function FactoryGetBuyCoupon() As IBuyCoupon
    
    If UseBuyCouponImplementation Then
        'using implementation with P014-01 change
        Set FactoryGetBuyCoupon = New BuyCoupon
    Else
        'using live implementation
        Set FactoryGetBuyCoupon = New TillFormLive
    End If
End Function

Friend Property Get UseGetCouponImplementation() As Boolean

    UseGetCouponImplementation = m_UseGetCouponsImplementation
End Property

Public Function FactoryGetGetCoupon() As IGetCoupon
    
    If UseGetCouponImplementation Then
        'using implementation with P014-01 change
        Set FactoryGetGetCoupon = New GetCoupon
    Else
        'using live implementation
        Set FactoryGetGetCoupon = New TillFormLive
    End If
End Function

Friend Property Get UseDotnetOfflineModeImplementation() As Boolean

    UseDotnetOfflineModeImplementation = m_UseDotnetOfflineModeImplementation
End Property

Public Function FactoryGetDotnetOfflineMode() As IDotnetOfflineMode
    
    If UseDotnetOfflineModeImplementation Then
        'using implementation with P014-01 change
        Set FactoryGetDotnetOfflineMode = New DotnetOfflineMode
    Else
        'using live implementation
        Set FactoryGetDotnetOfflineMode = New TillFormLive
    End If
End Function

Friend Property Get UsePICRefundCountProcessImplementation() As Boolean

    UsePICRefundCountProcessImplementation = m_UsePICRefundCountProcessImplementation
End Property

Public Function FactoryGetPICRefundCountProcess() As IPICRefundCountProcess
    
    If UsePICRefundCountProcessImplementation Then
        'using implementation for CR0081 change
        Set FactoryGetPICRefundCountProcess = New PICRefundCountProcess
    Else
        'using live implementation
        Set FactoryGetPICRefundCountProcess = New TillFormLive
    End If
End Function

Friend Property Get UsePreventRepeatPaymentImplementation() As Boolean

    UsePreventRepeatPaymentImplementation = m_UsePreventRepeatPaymentImplementation
End Property

Public Function FactoryGetRepeatPayment() As IRepeatPayment
    
    If UsePreventRepeatPaymentImplementation Then
        'using implementation for RF1094
        DebugMsg "TillFormFactory", "FactoryGetRepeatPayment", endlDebug, "Returning PreventRepeatPayment implementation"
        Set FactoryGetRepeatPayment = New PreventRepeatPayment
    Else
        'using live implementation
        DebugMsg "TillFormFactory", "FactoryGetRepeatPayment", endlDebug, "Returning TillFormLive implementation"
        Set FactoryGetRepeatPayment = New TillFormLive
    End If
End Function

Private Sub Class_Initialize()

    GetUseProcessingPaymentParameterValue
    GetUseTrainingModeOrdersParameterValue
    GetUseBuyCouponsParameterValue
    GetUseGetCouponsParameterValue
    GetUseDotnetOfflineModeParameterValue
    GetUseRefundTenderTypeParameterValue
    GetUsePICRefundCountProcessParameterValue
    GetUsePreventRepeatPaymentParameterValue
End Sub

Friend Sub GetUseProcessingPaymentParameterValue()

    If Not m_InitialisedUseProcessingPaymentImplementation Then
On Error Resume Next
        m_UseProcessingPaymentImplementation = goSession.GetParameter(m_ProcessingPaymentParameterID)
        m_InitialisedUseProcessingPaymentImplementation = True
On Error GoTo 0
    End If
End Sub

Friend Sub GetUseTrainingModeOrdersParameterValue()

    If Not m_InitialisedUseTrainingModeOrdersImplementation Then
On Error Resume Next
        m_UseTrainingModeOrdersImplementation = goSession.GetParameter(m_TrainingModeOrdersParameterID)
        m_InitialisedUseTrainingModeOrdersImplementation = True
On Error GoTo 0
    End If
End Sub

Friend Sub GetUseBuyCouponsParameterValue()

    If Not m_InitialisedUseBuyCouponsImplementation Then
On Error Resume Next
        m_UseBuyCouponsImplementation = goSession.GetParameter(m_BuyCouponsParameterID)
        m_InitialisedUseBuyCouponsImplementation = True
On Error GoTo 0
    End If
End Sub

Friend Sub GetUseGetCouponsParameterValue()

    If Not m_InitialisedUseGetCouponsImplementation Then
On Error Resume Next
        m_UseGetCouponsImplementation = goSession.GetParameter(m_GetCouponsParameterID)
        m_InitialisedUseGetCouponsImplementation = True
On Error GoTo 0
    End If
End Sub

Friend Sub GetUseDotnetOfflineModeParameterValue()

    If Not m_InitialisedUseDotnetOfflineModeImplementation Then
On Error Resume Next
        m_UseDotnetOfflineModeImplementation = goSession.GetParameter(m_DotnetOfflineModeParameterID)
        m_InitialisedUseDotnetOfflineModeImplementation = True
On Error GoTo 0
    End If
End Sub

Friend Sub GetUseRefundTenderTypeParameterValue()

    If Not m_InitialisedUseRefundTenderTypeImplementation Then
On Error Resume Next
        m_UseRefundTenderTypeImplementation = goSession.GetParameter(m_RefundTenderTypeParameterID)
        m_InitialisedUseRefundTenderTypeImplementation = True
On Error GoTo 0
    End If
End Sub

Friend Sub GetUsePICRefundCountProcessParameterValue()

    If Not m_InitialisedUsePICRefundCountProcessImplementation Then
On Error Resume Next
        m_UsePICRefundCountProcessImplementation = goSession.GetParameter(m_PICRefundCountProcessParameterID)
        m_InitialisedUsePICRefundCountProcessImplementation = True
On Error GoTo 0
    End If
End Sub

Friend Sub GetUsePreventRepeatPaymentParameterValue()

    If Not m_InitialisedUsePreventRepeatPaymentImplementation Then
On Error Resume Next
        m_UsePreventRepeatPaymentImplementation = goSession.GetParameter(m_PreventRepeatPaymentParameterID)
        m_InitialisedUsePreventRepeatPaymentImplementation = True
On Error GoTo 0
    End If
End Sub

