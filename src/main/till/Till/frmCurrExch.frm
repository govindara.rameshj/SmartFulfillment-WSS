VERSION 5.00
Object = "{9A526B11-3010-11D5-99E4-000102897E9C}#2.2#0"; "NumTextControl.ocx"
Object = "{8DDE6232-1BB0-11D0-81C3-0080C7A2EF7D}#3.0#0"; "Flp32a30.ocx"
Begin VB.Form frmCurrExch 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Record Foreign Tender"
   ClientHeight    =   5325
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9525
   Icon            =   "frmCurrExch.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5325
   ScaleWidth      =   9525
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraTender 
      Caption         =   "Enter Tender Amount"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5175
      Left            =   60
      TabIndex        =   3
      Top             =   60
      Width           =   6315
      Begin VB.CommandButton cmdProcess 
         Caption         =   "F5 - Process"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   3840
         TabIndex        =   11
         Top             =   4380
         Width           =   2115
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "F12 - Cancel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   1500
         TabIndex        =   10
         Top             =   4380
         Width           =   2115
      End
      Begin NumTextControl.NumText ntxtTendered 
         Height          =   495
         Left            =   540
         TabIndex        =   7
         Top             =   2820
         Width           =   1875
         _ExtentX        =   3307
         _ExtentY        =   873
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         SelStart        =   4
         Text            =   "0.00"
      End
      Begin VB.Label lblAcceptedDenom 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   855
         Left            =   120
         TabIndex        =   15
         Top             =   3420
         Width           =   5895
      End
      Begin VB.Label lblMaxAllowed 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   120
         TabIndex        =   14
         Top             =   2340
         Width           =   5895
      End
      Begin VB.Label lblAmountDue 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   3960
         TabIndex        =   9
         Top             =   2820
         Width           =   1875
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Amount Due"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   4020
         TabIndex        =   8
         Top             =   1380
         Width           =   1815
      End
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Amount Tendered"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   600
         TabIndex        =   6
         Top             =   1380
         Width           =   1815
      End
      Begin VB.Label lblCurrency 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   1500
         TabIndex        =   5
         Top             =   540
         Width           =   4575
      End
      Begin VB.Label label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Cash :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   240
         TabIndex        =   4
         Top             =   540
         Width           =   1275
      End
   End
   Begin VB.PictureBox fraCurrencies 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   5475
      Left            =   0
      ScaleHeight     =   5475
      ScaleWidth      =   9495
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   9495
      Begin LpLib.fpList lstCurrency 
         Height          =   3660
         Left            =   180
         TabIndex        =   1
         Top             =   720
         Width           =   9255
         _Version        =   196608
         _ExtentX        =   16325
         _ExtentY        =   6456
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Columns         =   10
         Sorted          =   0
         LineWidth       =   1
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         MultiSelect     =   0
         WrapList        =   0   'False
         WrapWidth       =   0
         SelMax          =   -1
         AutoSearch      =   2
         SearchMethod    =   0
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   0
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   0
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   1
         BorderColor     =   0
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   0
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   -1  'True
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         DataField       =   ""
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         ColDesigner     =   "frmCurrExch.frx":0A96
      End
      Begin VB.CommandButton cmdCancelCurr 
         Caption         =   "F12 - Cancel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   4980
         TabIndex        =   13
         Top             =   4785
         Width           =   2115
      End
      Begin VB.CommandButton cmdUseCurrency 
         Caption         =   "F5 - Process"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   7320
         TabIndex        =   12
         Top             =   4785
         Width           =   2115
      End
      Begin VB.Label label2 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Select Currency"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   0
         TabIndex        =   2
         Top             =   180
         Width           =   8415
      End
   End
End
Attribute VB_Name = "frmCurrExch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim mcurAmountDue   As Currency
Dim mcolCurrencies  As Collection
Dim mdblExchRate    As Double
Dim curTendered     As Currency
Dim curLocalTend    As Currency 'used to hold Local equivelant value
Dim mcurMaxAccepted As Currency
Dim mcurMinDenom    As Currency
Dim mstrSysCurr     As String

Private Sub cmdCancel_Click()

    Call ShowCurrencies

End Sub

Private Sub cmdCancelCurr_Click()

    ntxtTendered.Value = 0
    Me.Hide

End Sub

Private Sub cmdProcess_Click()

    If (ntxtTendered.Value = 0) Then
        Call MsgBoxEx("No amount tendered has been entered" & vbNewLine & "Unable to proceed", vbInformation, "Invalid Tender", , , , , RGBMsgBox_WarnColour)
        Exit Sub
    End If
    
    If (ntxtTendered.Value > mcurMaxAccepted) And (mcurMaxAccepted > 0) Then
        Call MsgBoxEx("Tendered amount exceeds maximum allowed." & vbNewLine & "Unable to proceed", vbInformation, "Exceeded Tender Amount", , , , , RGBMsgBox_WarnColour)
        Exit Sub
    End If
    
    If (mcurMinDenom <> 0) Then
        If ((ntxtTendered.Value \ mcurMinDenom) <> (ntxtTendered.Value / mcurMinDenom)) Then
            Call MsgBoxEx("Tendered amount contains below allowed denominations." & vbNewLine & "Unable to proceed", vbInformation, "Unaccepted Denominations", , , , , RGBMsgBox_WarnColour)
            Exit Sub
        End If
    End If
    
    If (MsgBoxEx("Confirm Tender :" & vbNewLine & vbTab & "Cash : " & lblCurrency & vbNewLine & vbTab & "Amount Tendered : " & ntxtTendered.Value & " = " & Format(ntxtTendered.Value / mdblExchRate, "0.00") & " " & mstrSysCurr, vbInformation + vbOKCancel, "Confirm Tender", , , , , RGBMSGBox_PromptColour) = vbOK) Then
        curLocalTend = Format(ntxtTendered.Value / mdblExchRate, "0.00")
        Me.Hide
    End If
        
End Sub

Private Sub fraReasonsList_Click()

End Sub

Private Sub LoadCurrencies()

Dim cCurrBO     As cCurrencies
Dim oExchRateBO As cExchangeRate
Dim vntMaxDate  As Variant
Dim curExchRate As Currency
Dim curAmtDue   As Currency

Dim oRow    As CRowSelector

    For Each cCurrBO In mcolCurrencies
        If (cCurrBO.SystemDefaultCurrency = False) Then
            Set oExchRateBO = goDatabase.CreateBusinessObject(CLASSID_EXCHANGE_RATE)
            Set oRow = goSession.Root.CreateUtilityObject("CRowSelector")
            Call oRow.AddSelectValue(CMP_EQUAL, FID_EXCHANGE_RATE_CurrencyCode, cCurrBO.CurrencyCode)
            vntMaxDate = goDatabase.GetAggregateValue(AGG_MAX, oExchRateBO.GetField(FID_EXCHANGE_RATE_EffectiveDate), oRow)
            Call oExchRateBO.AddLoadFilter(CMP_EQUAL, FID_EXCHANGE_RATE_CurrencyCode, cCurrBO.CurrencyCode)
            If (IsNull(vntMaxDate) = False) Then Call oExchRateBO.AddLoadFilter(CMP_EQUAL, FID_EXCHANGE_RATE_EffectiveDate, CDate(vntMaxDate))
            If (oExchRateBO.LoadMatches.Count > 0) Then
                curExchRate = oExchRateBO.ExchangeRate * (10 ^ oExchRateBO.PowerFactor)
                curAmtDue = mcurAmountDue * curExchRate
                Call lstCurrency.AddItem(cCurrBO.CurrDescription & "(" & cCurrBO.CurrencyCode & ")" & vbTab & Format(curExchRate, "0.00000") & vbTab & Format(curAmtDue, "0.00") & vbTab & cCurrBO.CurrencyCode & vbTab & oExchRateBO.ExchangeRate & vbTab & oExchRateBO.PowerFactor & vbTab & cCurrBO.MaxAccepted & vbTab & cCurrBO.MinCashDenom & vbTab & cCurrBO.LargestDenom & vbTab & cCurrBO.CurrencySymbol)
            End If
        Else
            mstrSysCurr = cCurrBO.CurrencyCode
        End If
    Next
    
    If (lstCurrency.ListCount > 0) Then lstCurrency.ListIndex = 0
    
End Sub

Public Function GetCurrency(colCurrCodes As Collection, curAmountDue As Currency, strCurrCode As String, curExchRate As Currency, lngExchDecPos As Long, curAmountTend As Currency, curForeignAmt As Currency) As Boolean

    mcurAmountDue = curAmountDue
    Set mcolCurrencies = colCurrCodes
    Call ShowCurrencies
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    fraCurrencies.BackColor = Me.BackColor
    fraTender.BackColor = Me.BackColor
    Call LoadCurrencies
    lblAmountDue.Caption = curAmountDue
    Me.Show (vbModal)
    If (ntxtTendered.Value = 0) Then
        GetCurrency = False
    Else
        
        GetCurrency = True
        strCurrCode = lstCurrency.colList(3, lstCurrency.ListIndex)

        curExchRate = mdblExchRate
        lngExchDecPos = lstCurrency.colList(5, lstCurrency.ListIndex)
        curAmountTend = curLocalTend
        curForeignAmt = ntxtTendered.Value
    End If

End Function

Private Sub ShowCurrencies()

    fraCurrencies.Visible = True
    fraTender.Visible = False
    Me.Width = fraCurrencies.Width + 240
    Me.Height = 6220
    Call CentreForm(Me)

End Sub

Private Sub ShowExchRate()

    fraCurrencies.Visible = False
    fraTender.Visible = True
    Me.Width = fraTender.Width + 240
    Me.Height = 5800
    Call CentreForm(Me)

End Sub


Private Sub cmdUseCurrency_Click()

    Call lstCurrency_KeyPress(vbKeyReturn)

End Sub

Private Sub Form_Activate()

    If (lstCurrency.ListCount = 0) Then
        Call MsgBoxEx("No valid Currencies/Exchange Rates set up", vbOKOnly, "Currency Error", , , , , RGBMsgBox_WarnColour)
        Me.Hide
    End If

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If (Shift = 0) Then
        Select Case (KeyCode)
            Case (vbKeyF5):
                            If (cmdProcess.Visible = True) Then
                                cmdProcess.Value = True
                                KeyCode = 0
                                Exit Sub
                            End If
                            If (cmdUseCurrency.Visible = True) Then
                                cmdUseCurrency.Value = True
                                KeyCode = 0
                                Exit Sub
                            End If
            Case (vbKeyF12):
                            If (cmdCancel.Visible = True) Then
                                cmdCancel.Value = True
                                KeyCode = 0
                                Exit Sub
                            End If
                            If (cmdCancelCurr.Visible = True) Then
                                cmdCancelCurr.Value = True
                                KeyCode = 0
                                Exit Sub
                            End If
        End Select
    End If

End Sub


Private Sub lstCurrency_KeyPress(KeyAscii As Integer)

Dim lngIndex As Long
Dim strCurr  As String

    If (KeyAscii = vbKeyReturn) And (lstCurrency.ListIndex <> -1) Then
        mcurMaxAccepted = 0
        mcurMinDenom = 1
        lblMaxAllowed.Caption = ""
        lblMaxAllowed.Visible = False
        lblAcceptedDenom.Caption = ""
        lblAcceptedDenom.Visible = False
        lngIndex = lstCurrency.ListIndex
        strCurr = lstCurrency.colList(9, lngIndex)
        Call ShowExchRate
        lblCurrency.Caption = lstCurrency.colList(0, lngIndex)
        mdblExchRate = Val(lstCurrency.colList(1, lngIndex))
        lblAmountDue.Caption = lstCurrency.colList(2, lngIndex)
        mcurMaxAccepted = Val(lstCurrency.colList(6, lngIndex))
        mcurMinDenom = Val(lstCurrency.colList(7, lngIndex))
        If (mcurMaxAccepted > 0) Then lblMaxAllowed.Caption = "Maximum accepted is " & strCurr & mcurMaxAccepted
        If (mcurMinDenom > 0) Then lblAcceptedDenom.Caption = "Lowest accepted denom. is " & strCurr & mcurMinDenom & vbCrLf
        If (Val(lstCurrency.colList(8, lngIndex)) > 0) Then lblAcceptedDenom.Caption = lblAcceptedDenom.Caption & "Highest accepted denom. is " & strCurr & lstCurrency.colList(8, lngIndex)
        If (ntxtTendered.Visible = True) And (ntxtTendered.Enabled = True) Then ntxtTendered.SetFocus
        mcurMaxAccepted = Val(lstCurrency.colList(6, lngIndex))
        mcurMinDenom = Val(lstCurrency.colList(7, lngIndex))
        If (lblMaxAllowed.Caption <> "") Then lblMaxAllowed.Visible = True
        If (lblAcceptedDenom.Caption <> "") Then lblAcceptedDenom.Visible = True
    End If
    
    If (KeyAscii = vbKeyEscape) Then
        KeyAscii = 0
        DoEvents
        Me.Hide
    End If

End Sub

Private Sub ntxtTendered_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyEscape) Then
        Call ShowCurrencies
    End If
    If (KeyAscii = vbKeyReturn) Then
        cmdProcess.Value = 1
    End If

End Sub
