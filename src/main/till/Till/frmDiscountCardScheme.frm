VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Begin VB.Form frmDiscountCardScheme 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Process Discount Card"
   ClientHeight    =   8070
   ClientLeft      =   1710
   ClientTop       =   2940
   ClientWidth     =   12900
   ControlBox      =   0   'False
   Icon            =   "frmDiscountCardScheme.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8070
   ScaleWidth      =   12900
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer tmrCancel 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   480
      Top             =   1440
   End
   Begin VB.CommandButton cmdSwipe 
      Caption         =   "S to Swipe Card"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   3720
      TabIndex        =   4
      Top             =   2280
      Width           =   2535
   End
   Begin VB.CommandButton cmdKeyIn 
      Caption         =   "K to Key Card Number"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   120
      TabIndex        =   3
      Top             =   2280
      Width           =   3495
   End
   Begin VB.Frame fraCardDetails 
      Caption         =   "Confirm Card Details"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3315
      Left            =   120
      TabIndex        =   13
      Top             =   3120
      Visible         =   0   'False
      Width           =   8415
      Begin VB.Frame fraCardButtons 
         BorderStyle     =   0  'None
         Height          =   555
         Left            =   240
         TabIndex        =   20
         Top             =   2700
         Width           =   7995
         Begin VB.CommandButton cmdCardReject 
            Caption         =   "ESC - Reject Card"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   5100
            TabIndex        =   22
            Top             =   0
            Width           =   2895
         End
         Begin VB.CommandButton cmdCardOK 
            Caption         =   "Accept Card"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   0
            TabIndex        =   21
            Top             =   0
            Width           =   2175
         End
      End
      Begin VB.Label lblCardPostCode 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   2460
         TabIndex        =   19
         Top             =   2280
         Width           =   5775
      End
      Begin VB.Label lblCardPostCodelbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Post Code"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   300
         TabIndex        =   18
         Top             =   2280
         Width           =   1815
      End
      Begin VB.Label lblCardAddress 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   1320
         Left            =   2460
         TabIndex        =   17
         Top             =   900
         Width           =   5775
      End
      Begin VB.Label lblCardAddresslbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Address"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   300
         TabIndex        =   16
         Top             =   900
         Width           =   1815
      End
      Begin VB.Label lblCardName 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   2460
         TabIndex        =   15
         Top             =   480
         Width           =   5775
      End
      Begin VB.Label Label10 
         BackStyle       =   0  'Transparent
         Caption         =   "Name"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   300
         TabIndex        =   14
         Top             =   480
         Width           =   1815
      End
   End
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   0
      Top             =   2880
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin VB.CommandButton cmdEscape 
      Caption         =   "ESC to Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   6360
      TabIndex        =   23
      Top             =   2280
      Width           =   2175
   End
   Begin VB.Frame fraTranDetails 
      Caption         =   "Transaction Details"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   120
      TabIndex        =   12
      Top             =   180
      Width           =   8415
      Begin VB.Label lblAmount 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   7020
         TabIndex        =   11
         Top             =   540
         Width           =   1155
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "Amount"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   6000
         TabIndex        =   10
         Top             =   540
         Width           =   915
      End
      Begin VB.Label lblTranNo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   4980
         TabIndex        =   9
         Top             =   540
         Width           =   735
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "Tran No"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3960
         TabIndex        =   8
         Top             =   540
         Width           =   1035
      End
      Begin VB.Label lblTillID 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   3300
         TabIndex        =   7
         Top             =   540
         Width           =   435
      End
      Begin VB.Label lblTranDate 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   960
         TabIndex        =   6
         Top             =   540
         Width           =   1215
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Till No"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2460
         TabIndex        =   5
         Top             =   540
         Width           =   975
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Date"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   300
         TabIndex        =   25
         Top             =   540
         Width           =   855
      End
   End
   Begin VB.TextBox txtColCardNo 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   2760
      MaxLength       =   20
      TabIndex        =   2
      Top             =   1750
      Visible         =   0   'False
      Width           =   3495
   End
   Begin VB.CommandButton cmdProcess 
      Caption         =   "Process"
      Height          =   255
      Left            =   7920
      TabIndex        =   1
      Top             =   1680
      Visible         =   0   'False
      Width           =   735
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   24
      Top             =   7695
      Width           =   12900
      _ExtentX        =   22754
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   9
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmDiscountCardScheme.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11853
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   970
            MinWidth        =   970
            Picture         =   "frmDiscountCardScheme.frx":0F30
            Key             =   "KeyBoard"
            Object.ToolTipText     =   "Show/Hide Keyboard"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   573
            MinWidth        =   573
            Picture         =   "frmDiscountCardScheme.frx":15E2
            Key             =   "NumPad"
            Object.ToolTipText     =   "Show/Hide numeric keypad"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel8 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel9 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "09:54"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblWaiting 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Exit Requested.  Remaining until Timeout - 00 Seconds left."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   735
      Left            =   2100
      TabIndex        =   28
      Top             =   1680
      Visible         =   0   'False
      Width           =   5055
   End
   Begin VB.Label lblSwipeStatus 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Swipe Card through Chip and Pin Reader"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1320
      TabIndex        =   27
      Top             =   1320
      Visible         =   0   'False
      Width           =   6075
   End
   Begin VB.Label lblScanWarn 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "(The Chip and Pin device will be inactive for upto 35 seconds if not utilised)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   615
      Left            =   1440
      TabIndex        =   26
      Top             =   1680
      Visible         =   0   'False
      Width           =   5835
   End
   Begin VB.Label lblStatus 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Scan Card's Barcode"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1740
      TabIndex        =   0
      Top             =   1380
      Width           =   5595
   End
End
Attribute VB_Name = "frmDiscountCardScheme"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const MODULE_NAME As String = "frmDiscountCardScheme"

Const PRM_KEY_DISCOUNT_CARD As Long = 955           'MO'C Added 7/08/2007 - Whether we allow the keying in of a discount card

Dim mcurDiscountAmount      As Currency
Dim mcurTranTotal           As Currency
Dim moTranHeader            As cSale_Wickes.cPOSHeader
Dim moRetopt                As cEnterprise_Wickes.cRetailOptions
Dim mstrCardNumber          As String
Dim mstrLocationCode        As String
Dim mblnMSRActivated        As Boolean
Dim mblnSkipTrackRead       As Boolean              'Added 12/7/05-if scanned then ignore blank response from MSR
Dim mblnOperatorKeyedCardNo As Boolean
Dim moCardScheme            As cDiscountCardScheme  'MO'C Added 08/06/07
Dim mblnAllowKeyedDiscCard  As Boolean              'MO'C Added 07/08/2007
Dim mblnWaitCardConfirm     As Boolean

'Dim moReadCard              As frmReadCard
Dim moReadCard              As Object
'Dim moReadCard              As CCTender

Dim mlngClickX              As Long
Dim mlngClickY              As Long

Dim mlngTimeOut As Long

Public Event Duress()
Private mintDuressKeyCode As Integer

Public AllowScan As Boolean

' Commidea Into WSS
' Flag up if using Commidea PEDs.  Restricts what can be done
Private mblnUsingCommideaPED As Boolean
' Pass on integration & progress port settings
Private moCommideaCfg As CommideaConfiguration
Private mblnUserCancelled As Boolean
Private mblnInCommideaCardSwipe As Boolean
' So can handle duress keypress etc
Private WithEvents moCommidea As WSSCommidea.cCommidea
Attribute moCommidea.VB_VarHelpID = -1
' End of Commidea Into WSS

Public Property Let CardNumber(Value As String)

    Call DebugMsg(MODULE_NAME, "Let CardNumber", endlDebug, "CardNumber=" & Value)
    mblnSkipTrackRead = True
    Call ProcessCard(Value)
    Call DebugMsg(MODULE_NAME, "Let CardNumber", endlDebug, "Finished-" & Value)
    Call Me.Hide

' keep around
End Property

Public Function ValidateDiscount(ByRef blnShowKeyPad As Boolean, _
                                    ByRef oTranHeader As cSale_Wickes.cPOSHeader, _
                                    ByVal curNewTranAmount As Currency, _
                                    ByRef oRetailOptions As cRetailOptions, _
                                    ByRef strLocation As String, _
                                    ByRef strCardNo As String) As cDiscountCardScheme

    ucKeyPad1.Visible = blnShowKeyPad
    
    mstrCardNumber = vbNullString
    mstrLocationCode = vbNullString
    mcurTranTotal = curNewTranAmount
    Set moTranHeader = oTranHeader
    Set moRetopt = oRetailOptions
    Set moCardScheme = Nothing
    
    lblTranDate.Caption = Format(oTranHeader.TranDate, "DD/MM/YY")
    lblTillID.Caption = oTranHeader.TillID
    lblTranNo.Caption = oTranHeader.TransactionNo
    lblAmount.Caption = Format(curNewTranAmount, "0.00")
    Call DebugMsg(MODULE_NAME, "ValCollDisc", endlDebug, "Started")
    fraCardDetails.Visible = False
    'Me.Height = lblesc
    Call Me.Show(vbModal)
    Call DebugMsg(MODULE_NAME, "ValDiscount", endlDebug, "Got data")
    DoEvents
    strLocation = mstrLocationCode
    Call DebugMsg(MODULE_NAME, "ValCollDisc", endlTraceOut)
    
    If Not moCardScheme Is Nothing Then
        strCardNo = mstrCardNumber
        moTranHeader.DiscountCardNumber = strCardNo
    End If
    
    Set ValidateDiscount = moCardScheme
    
End Function

Private Sub ProcessCard(ByVal strDiscountCardNo As String)

Dim strCardNo      As String
Dim strLocnCode    As String
Dim lngKeyIn       As Long
Dim oDiscountCardList As cDiscountCardList
Dim strDiscountParams As String
Dim dblMinDiscountPercentage As Double
Dim dblMaxDiscountPercentage As Double
Dim intCommaPos As Integer

    AllowScan = False
    Call DebugMsg(MODULE_NAME, "ProcessCard", endlDebug, "Card=" & strDiscountCardNo & " " & (moReadCard Is Nothing))
    If Not moReadCard Is Nothing Then
        Call moReadCard.ExitWaitNow
        DoEvents
        If (goSession.GetParameter(PRM_EFT_METHOD) = 1) Then
            Set moReadCard = Nothing
        Else
            Unload moReadCard
        End If
        DoEvents
    End If
    
    strCardNo = strDiscountCardNo

    Set moCardScheme = GetCardScheme(strCardNo)
    If moCardScheme Is Nothing Then
        Debug.Print "No scheme found."
        Call MsgBoxEx("Card Rejected", vbOKOnly, "Card Validation Error", , , , , RGBMsgBox_WarnColour)
        GoTo EndProcess
    
    Else
        
        Debug.Print "Found scheme " & " : " & moCardScheme.SchemeID & " : " & moCardScheme.SchemeName

        cmdEscape.Visible = False
        fraCardDetails.Top = lblStatus.Top
        
        Set oDiscountCardList = goDatabase.CreateBusinessObject(CLASSID_DISCOUNTCARD_LIST)
        Call oDiscountCardList.AddLoadFilter(CMP_EQUAL, FID_DISCOUNTCARD_LIST_CardNumber, strCardNo)
        
        'Can we display customer details ?
        If oDiscountCardList.LoadMatches.Count > 0 Then
                    
            lblCardName.Caption = oDiscountCardList.CustomerName
            'Requested not display the card address
            
            lblCardAddress.Visible = False
            lblCardPostCode.Visible = False
            lblCardAddresslbl.Visible = False
            lblCardPostCodelbl.Visible = False
            fraCardDetails.Height = lblCardName.Height + lblCardName.Top + fraCardButtons.Height + 240
            fraCardButtons.Top = lblCardName.Height + lblCardName.Top + 120
            
            If oDiscountCardList.CardDeleted Then
                
                Call MsgBoxEx("Discount Card has been marked as deleted!", vbOKOnly, "Card Validation Error", , , , , RGBMsgBox_WarnColour)
                Set moCardScheme = Nothing
                Set oDiscountCardList = Nothing
                GoTo EndProcess
            
            End If
            
        End If
        
        
        'reject the card if it was keyed in and keying in is not allowed on this scheme
        If moCardScheme.AllowKeyedCardNumber = DISCOUNTCARD_KEYED_NOT_ALLOWED And mblnOperatorKeyedCardNo Then
            
            'fraCardDetails.Visible = False
            Call MsgBoxEx("Discount card cannot be keyed in!", vbOKOnly, "Card Validation Error", , , , , RGBMsgBox_WarnColour)
            Set moCardScheme = Nothing
            Set oDiscountCardList = Nothing
            mblnOperatorKeyedCardNo = False
            GoTo EndProcess
            
        ElseIf moCardScheme.AllowKeyedCardNumber = DISCOUNTCARD_KEYED_ALLOWED_WITH_AUTH And mblnOperatorKeyedCardNo Then
        
            'fraCardDetails.Visible = False
            Load frmVerifyPwd
            If frmVerifyPwd.VerifySupervisorPassword(ucKeyPad1.Visible, "Confirm Discount") = False Then
                Unload frmVerifyPwd
                'get out of tender mode and clear up any added lines
                Set moCardScheme = Nothing
                Set oDiscountCardList = Nothing
                mblnOperatorKeyedCardNo = False
'                mblnApplyDiscountScheme = False
                GoTo EndProcess
            End If
            moTranHeader.SupervisorUsed = True
            moTranHeader.DiscountSupervisor = frmVerifyPwd.txtAuthID.Text
            Unload frmVerifyPwd
        
        End If
        
        
        'Verify discount is within parameter limits
        If (goSession Is Nothing) Then Set goSession = GetRoot.CreateSession
        strDiscountParams = goSession.GetParameter(929) '0,100
        If strDiscountParams <> "" Then
            intCommaPos = InStr(strDiscountParams, ",")
            If intCommaPos > 0 Then
                dblMinDiscountPercentage = Val(Mid(strDiscountParams, 1, intCommaPos - 1))
                dblMaxDiscountPercentage = Val(Mid(strDiscountParams, intCommaPos + 1))
            End If
            
            If moCardScheme.DiscountRate < dblMinDiscountPercentage Or moCardScheme.DiscountRate > dblMaxDiscountPercentage Then
                Call MsgBoxEx("Discount Card has an out of range percentage (" & CStr(moCardScheme.DiscountRate) & "%). Manager Authorisation required.", vbOKOnly, "Discount Percentage Out of Range", , , , , RGBMsgBox_WarnColour)
                If frmVerifyPwd.VerifyManagerPassword(ucKeyPad1.Visible, "Confirm Discount") = False Then
                    Unload frmVerifyPwd
                    'get out of tender mode and clear up any added lines
                    Set moCardScheme = Nothing
                    Set oDiscountCardList = Nothing
                    mblnOperatorKeyedCardNo = False
                    GoTo EndProcess
                End If
            End If
        End If

        DoEvents
        Me.Height = fraCardDetails.Top + fraCardDetails.Height + sbStatus.Height + 600
        Me.Width = fraTranDetails.Width + 300
        fraCardDetails.Visible = True
        cmdSwipe.Visible = False
        cmdKeyIn.Visible = False
        ucKeyPad1.Visible = False
        
        If (cmdCardOK.Visible = True) And (cmdCardOK.Enabled = True) Then Call cmdCardOK.SetFocus

        'Wait for user to accept or reject the discount
        mblnWaitCardConfirm = False
        While (mblnWaitCardConfirm = False)
            DoEvents
        Wend

        If (fraCardDetails.Visible = False) Then GoTo EndProcess
        
    End If

        
    strLocnCode = Right$("000" & goSession.CurrentEnterprise.IEnterprise_StoreNumber, 3)
    
    mstrCardNumber = strCardNo
    mstrLocationCode = strLocnCode

EndProcess:

    'Set moCardScheme = Nothing
    Call DebugMsg(MODULE_NAME, "ProcessCard", endlDebug, "Ended" & mstrCardNumber & "@" & mstrLocationCode)
    Set moReadCard = Nothing
    AllowScan = True
    Call Me.Hide
    
End Sub

Private Sub cmdCardOK_Click()

    mblnWaitCardConfirm = True

End Sub

Private Sub cmdCardReject_Click()

    Set moCardScheme = Nothing
    fraCardDetails.Visible = False
    cmdSwipe.Visible = False
    cmdKeyIn.Visible = False
    cmdEscape.Visible = True
    DoEvents
    mblnWaitCardConfirm = True
End Sub

Private Sub cmdEscape_Click()

    If mblnUsingCommideaPED And mblnInCommideaCardSwipe Then
        If Not mblnUserCancelled Then
            Call MsgBoxEx("Cancel the card swipe on the PED.", vbOKOnly, "PED still waiting for card swipe", , , , , RGBMsgBox_WarnColour)
        Else
            'OK, escaped and cancelled, can leave now
            Call Form_KeyPress(vbKeyEscape)
        End If
    Else
        Call Form_KeyPress(vbKeyEscape)
    End If
End Sub

Private Sub cmdKeyIn_Click()
    
    Call DebugMsg(MODULE_NAME, "Form_keypress", endlDebug, "Keyed In Requested")
    txtColCardNo.Visible = True
    txtColCardNo.Enabled = True
    Call txtColCardNo.SetFocus
    If ((moReadCard Is Nothing) = False) Then
        Call moReadCard.ExitWaitNow
        DoEvents
        Unload moReadCard
        DoEvents
        Set moReadCard = Nothing
    End If

End Sub

Private Sub cmdProcess_Click()

    Call ProcessCard(txtColCardNo.Text)

End Sub

Private Sub cmdSwipe_Click()
    Dim mstrTrack1 As String
    Dim mstrTrack2 As String
    Dim mstrTrack3 As String
    Dim CardNumber As String
    Dim CardRead As Boolean
    Dim KeyInAllowed As Boolean
    Dim ScanWarmMess As String

    ' Commidea Into WSS
    ' Switch off swiping until Commidea PEDs can read track2
    If mblnUsingCommideaPED Then
        AllowScan = True
        ' Set up for card swipe - disable buttons, show warn message, etc
        ' Store current KeyIn setting and disable
        With cmdKeyIn
            KeyInAllowed = .Enabled
            .Enabled = False
        End With
        cmdSwipe.Enabled = False
        cmdEscape.Enabled = False
        With lblScanWarn
            ScanWarmMess = .Caption
            .Caption = "(The PED device will be inactive until card is swiped or the swipe is cancelled)"
            .Visible = True
        End With
        txtColCardNo.Visible = False
        
        Set moCommidea = New WSSCommidea.cCommidea
        Call moCommidea.Initialise(moCommideaCfg)
        mblnUserCancelled = False
        mblnInCommideaCardSwipe = True
        Do While Not CardRead And Not mblnUserCancelled
            CardRead = moCommidea.ReadMyDiscountCard(CardNumber)
            DoEvents
        Loop
        mblnInCommideaCardSwipe = False
        If CardRead Then
            mstrTrack2 = CardNumber
            If CardNumber <> "" Then
                Call ProcessCard(mstrTrack2)
            Else
                AllowScan = False
                Call MsgBoxEx("Discount cancelled.", vbOKOnly, "Discount", , , , , RGBMsgBox_WarnColour)
                Set moCardScheme = Nothing
                GoTo EndProcess
            End If
        End If
        ' reverse changes done in moCommidea_AwaitingMyDiscountCard event
        lblStatus.Visible = True
        lblSwipeStatus.Visible = False
        ' Set back up for all card read options
        ' Switch KeyIn button back to previous setting
        cmdKeyIn.Enabled = KeyInAllowed
        cmdSwipe.Enabled = True
        cmdEscape.Enabled = True
        ' Switch scan warning back to normal
        With lblScanWarn
            .Caption = ScanWarmMess
            .Visible = False
        End With
    Else
    ' End of Commidea Into WSS
        AllowScan = True
        If (goSession.GetParameter(PRM_EFT_METHOD) = 1) Then
            Call CreateLogicTender
            lblScanWarn.Caption = Replace(lblScanWarn.Caption, "35", goSession.GetParameter(PRM_EFT_SWIPE_TIMEOUT))
        Else
            Set moReadCard = New frmReadCard
            Load moReadCard
        End If
        
        Call DebugMsg(MODULE_NAME, "Form_Activate", endlDebug, "Starting to Activate-" & mblnMSRActivated)
        If (mblnMSRActivated = True) Then Exit Sub
        
        lblScanWarn.Visible = True
        lblStatus.Visible = False
        lblSwipeStatus.Visible = True
        cmdKeyIn.Enabled = False
        cmdSwipe.Enabled = False
        txtColCardNo.Visible = False
        cmdEscape.Enabled = (goSession.GetParameter(PRM_EFT_METHOD) <> 1)
    
        
        mlngTimeOut = 0
    
        mblnMSRActivated = True
        mblnSkipTrackRead = False
        Call DebugMsg(MODULE_NAME, "Form_Activate", endlDebug, "Activate Card Read")
        tmrCancel.Enabled = True
        Set moReadCard.moParentForm = Me
        If (moReadCard.RetrieveTracks(mstrTrack1, mstrTrack2, mstrTrack3) = True) And (txtColCardNo.Enabled = False) Then
            DoEvents
            If (mblnSkipTrackRead = False) Then
                If mstrTrack2 = "" Then
                    AllowScan = False
                    Call MsgBoxEx("Discount cancelled", vbOKOnly, "Discount", , , , , RGBMsgBox_WarnColour)
                    Set moCardScheme = Nothing
                    GoTo EndProcess
                Else
                    Call ProcessCard(mstrTrack2)
                End If
            End If 'Card was Read by MSR
        End If
        Call DebugMsg(MODULE_NAME, "Form_Activate", endlDebug, "Activated" & mstrTrack2)
        lblScanWarn.Visible = False
        mblnMSRActivated = False
        cmdKeyIn.Enabled = True
        cmdSwipe.Enabled = True
        lblStatus.Visible = True
        lblSwipeStatus.Visible = False
        lblWaiting.Visible = False
        cmdEscape.Enabled = True
        cmdEscape.Visible = True
    End If  ' If UsingCommideaPED
    
    Exit Sub

EndProcess:
    Call DebugMsg(MODULE_NAME, "ProcessCard", endlDebug, "Ended" & mstrCardNumber & "@" & mstrLocationCode)
    Set moReadCard = Nothing
    lblScanWarn.Visible = False
    Call Me.Hide
End Sub

Private Sub Form_Activate()

    AllowScan = True
    
    Call DebugMsg(MODULE_NAME, "Form_Activate", endlDebug, "Starting to Activate-" & mblnMSRActivated)


    If ucKeyPad1.Visible Then
        Me.Height = 8550
    Else
        Me.Height = cmdKeyIn.Top + cmdKeyIn.Height + sbStatus.Height + 600
        Me.Width = fraTranDetails.Width + 300
    End If
    
    Call DebugMsg(MODULE_NAME, "Form_Activate", endlDebug, "Activated")

    Exit Sub

EndProcess:

    Call DebugMsg(MODULE_NAME, "ProcessCard", endlDebug, "Ended" & mstrCardNumber & "@" & mstrLocationCode)
    Call Me.Hide

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyEscape) Then
        Call DebugMsg(MODULE_NAME, "Form_keypress", endlDebug, "Escaped")
        If mblnUsingCommideaPED And mblnInCommideaCardSwipe Then
            ' Already escaped, then make sure have cancelled on the ped
            If Not mblnUserCancelled Then
                Call MsgBoxEx("Cancel the card swipe on the PED.", vbOKOnly, "PED still waiting for card swipe", , , , , RGBMsgBox_WarnColour)
            End If
            Exit Sub
        End If
        If (tmrCancel.Enabled = True) Then
            lblScanWarn.Visible = False
            lblWaiting.Visible = True
            lblWaiting.Caption = Left$(lblWaiting.Caption, 43) & Format(35 - mlngTimeOut, "00") & Mid$(lblWaiting.Caption, 46)
            If ((moReadCard Is Nothing) = False) Then moReadCard.IgnoreTimeout = True
            DoEvents
            Exit Sub
        End If
        
'        If ((moReadCard Is Nothing) = False) Then
'            Call moReadCard.ExitWaitNow
'            DoEvents
'            On Error Resume Next
'            Unload moReadCard
'            On Error GoTo 0
'            Call Err.Clear
'            DoEvents
'            Set moReadCard = Nothing
'        End If
        If (cmdCardReject.Visible = True) Then
            cmdCardReject.Value = True
            Exit Sub
        End If
        
        DoEvents

        Call DebugMsg(MODULE_NAME, "Form_keypress", endlDebug, "Escaped")
        Call Me.Hide
    End If
    
    If (Asc(UCase(Chr(KeyAscii))) = vbKeyK) And (fraCardDetails.Visible = False) And (cmdKeyIn.Visible = True) And (cmdKeyIn.Enabled = True) Then
        Call DebugMsg(MODULE_NAME, "Form_keypress", endlDebug, "Keyed In Requested")
        Call cmdKeyIn_Click
        Exit Sub
'        txtColCardNo.Visible = True
'        txtColCardNo.Enabled = True
'        Call txtColCardNo.SetFocus
'        mblnSkipTrackRead = True
'        If ((moReadCard Is Nothing) = False) Then
'            Call moReadCard.ExitWaitNow
'            DoEvents
'            On Error Resume Next
'            Unload moReadCard
'            On Error GoTo 0
'            Call Err.Clear
'            DoEvents
'            Set moReadCard = Nothing
'        End If
    End If
    
    If (Asc(UCase(Chr(KeyAscii))) = vbKeyS) And (fraCardDetails.Visible = False) And (cmdSwipe.Visible = True) And (cmdSwipe.Enabled = True) Then
        Call DebugMsg(MODULE_NAME, "Form_keypress", endlDebug, "Swipe Requested")
        Call cmdSwipe_Click
        Exit Sub
    End If
    
    If (Asc(UCase(Chr(KeyAscii))) = vbKeyA) And (cmdCardOK.Visible = True) Then
        Call DebugMsg(MODULE_NAME, "Form_keypress", endlDebug, "Accept Pressed")
        Call cmdCardOK_Click
        Exit Sub
    End If
End Sub

Private Sub Form_Load()
    
    InitialiseStatusBar
    Call DebugMsg(MODULE_NAME, "Form_Load", endlTraceIn)
    mblnMSRActivated = False
    mblnOperatorKeyedCardNo = False
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    fraCardDetails.BackColor = Me.BackColor
    fraCardButtons.BackColor = Me.BackColor
    cmdProcess.BackColor = Me.BackColor
    
    'MO'C Added 7/8/2007 - to show the key discount label depending on the setting in the parameters table
    mblnAllowKeyedDiscCard = goSession.GetParameter(PRM_KEY_DISCOUNT_CARD)
    cmdKeyIn.Visible = mblnAllowKeyedDiscCard
    
    fraTranDetails.BackColor = Me.BackColor
    txtColCardNo.Visible = False
End Sub

Private Sub moCommidea_AwaitingMyDiscountCard()

    lblStatus.Visible = False
    lblSwipeStatus.Visible = True
End Sub

Private Sub moCommidea_MyDiscountCardError(ByVal CardError As String)

    Call MsgBoxEx(CardError, vbOKOnly, "There is a problem with the card", , , , , RGBMsgBox_WarnColour)
End Sub

Private Sub moCommidea_MyDiscountCardReadCancelled()

    Call MsgBoxEx("Card swipe cancelled on the PED.", vbOKOnly, "Card swipe cancelled", , , , , RGBMsgBox_WarnColour)
    mblnUserCancelled = True
End Sub

Private Sub CreateLogicTender()
'    Set moReadCard = New CCTender
'    Set moReadCard.Session = goSession
End Sub

Public Sub InitialiseStatusBar()
    
    sbStatus.Panels(PANEL_VERNO).Text = "Ver. " & App.Major & "." & App.Minor & "." & App.Revision
    sbStatus.Panels(PANEL_VERNO).ToolTipText = "Program Version Number"
    sbStatus.Panels(PANEL_WSID + 3).Text = Right$("00" & goSession.CurrentEnterprise.IEnterprise_WorkstationID, 2) & " "
    sbStatus.Panels(PANEL_WSID + 3).ToolTipText = "Current Work-Station Number"
    sbStatus.Panels(PANEL_DATE + 3).Text = Format$(Date, "DD-MMM-YY")
    If (mblnShowKeypadIcon = False) Then
        sbStatus.Panels("NumPad").Picture = Nothing
        sbStatus.Panels("KeyBoard").Picture = Nothing
    End If

End Sub

Public Sub SetForCommidea(ByVal commideaCfg As CommideaConfiguration)
    mblnUsingCommideaPED = True
    Set moCommideaCfg = commideaCfg
End Sub

Private Sub Form_Resize()
    Call CentreForm(Me)
End Sub

Private Sub Form_Unload(Cancel As Integer)

    Call DebugMsg(MODULE_NAME, "Form_Unload", endlTraceOut)

End Sub

Private Sub sbStatus_PanelClick(ByVal Panel As MSComctlLib.Panel)

Const MODE_NONE = 0
Const MODE_LARGE_KEYPAD = 1
Const MODE_NUM_KEYPAD = 2

Dim blnShow As Boolean
Dim byteMode As Byte

    byteMode = MODE_NONE
    blnShow = ucKeyPad1.Visible
    Select Case (Panel.Key)
        Case ("KeyBoard"):
                            byteMode = MODE_LARGE_KEYPAD
                            If ((Panel.Picture Is Nothing) = False) Then
                                If (ucKeyPad1.KeyboardShown = True) Then blnShow = Not blnShow
                                Call ucKeyPad1.ShowKeyboard
                                ucKeyPad1.Visible = blnShow
                            End If
                            
        Case ("NumPad"):
                            byteMode = MODE_NUM_KEYPAD
                            If ((Panel.Picture Is Nothing) = False) Then
                                If (blnShow = False) Then ucKeyPad1.Visible = True
                                If (ucKeyPad1.KeyboardShown = False) Then blnShow = Not blnShow
                                Call ucKeyPad1.ShowNumPad
                                ucKeyPad1.Visible = blnShow
                            End If
    End Select
    
    If blnShow Then
        If byteMode = MODE_LARGE_KEYPAD Then
            Me.Width = 12990
        Else
            Me.Width = fraTranDetails.Width + 300
        End If
        Me.Height = 8550 'fraCardDetails.Top + fraCardDetails.Height + sbStatus.Height + 600
    Else
        Me.Width = fraTranDetails.Width + 600
        Me.Height = cmdKeyIn.Top + cmdKeyIn.Height + sbStatus.Height + 600
    End If

End Sub

Public Sub ResetTimer()

    mlngTimeOut = 0
    tmrCancel.Enabled = True

End Sub

Private Sub tmrCancel_Timer()

    If (mlngTimeOut <= 35) And (lblWaiting.Visible = True) And ((mlngTimeOut Mod 5) = 0) Then
        lblWaiting.Caption = Left$(lblWaiting.Caption, 43) & Format(35 - mlngTimeOut, "00") & Mid$(lblWaiting.Caption, 46)
        lblWaiting.Refresh
    End If
    mlngTimeOut = mlngTimeOut + 1
    If (mlngTimeOut > 35) Then
        tmrCancel.Enabled = False
    End If
End Sub

Private Sub txtColCardNo_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        mblnOperatorKeyedCardNo = True
        Call cmdProcess_Click
    End If
    
    If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then
        KeyAscii = 0
    End If
End Sub

Private Sub ucKeyPad1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

    ucKeyPad1.BackColor = vbGrayText
    mlngClickX = X
    mlngClickY = Y

End Sub

Private Sub ucKeyPad1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    If Button = 1 And Shift = 0 Then
        ucKeyPad1.Left = ucKeyPad1.Left + X - mlngClickX
        ucKeyPad1.Top = ucKeyPad1.Top + Y - mlngClickY
    End If

End Sub

Private Sub ucKeyPad1_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  
  ucKeyPad1.BackColor = RGB(0, 64, 0)
    
End Sub




