VERSION 5.00
Object = "{6514F5A0-641C-11D2-9FD0-0020AF131A57}#3.0#0"; "fpFlp30.ocx"
Begin VB.Form frmOrderList 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Customer Orders for Today"
   ClientHeight    =   6540
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11415
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6540
   ScaleWidth      =   11415
   StartUpPosition =   2  'CenterScreen
   Begin LpADOLib.fpListAdo lstItems 
      Height          =   3210
      Left            =   120
      TabIndex        =   0
      Top             =   2520
      Width           =   11175
      _Version        =   196608
      _ExtentX        =   19711
      _ExtentY        =   5662
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   0   'False
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Columns         =   12
      Sorted          =   0
      LineWidth       =   1
      SelDrawFocusRect=   -1  'True
      ColumnSeparatorChar=   9
      ColumnSearch    =   -1
      ColumnWidthScale=   2
      RowHeight       =   -1
      MultiSelect     =   0
      WrapList        =   0   'False
      WrapWidth       =   0
      SelMax          =   -1
      AutoSearch      =   1
      SearchMethod    =   0
      VirtualMode     =   0   'False
      VRowCount       =   0
      DataSync        =   3
      ThreeDInsideStyle=   0
      ThreeDInsideHighlightColor=   -2147483633
      ThreeDInsideShadowColor=   -2147483627
      ThreeDInsideWidth=   1
      ThreeDOutsideStyle=   0
      ThreeDOutsideHighlightColor=   -2147483628
      ThreeDOutsideShadowColor=   -2147483632
      ThreeDOutsideWidth=   1
      ThreeDFrameWidth=   0
      BorderStyle     =   1
      BorderColor     =   -2147483642
      BorderWidth     =   1
      ThreeDOnFocusInvert=   0   'False
      ThreeDFrameColor=   -2147483633
      Appearance      =   1
      BorderDropShadow=   0
      BorderDropShadowColor=   -2147483632
      BorderDropShadowWidth=   3
      ScrollHScale    =   2
      ScrollHInc      =   0
      ColsFrozen      =   0
      ScrollBarV      =   1
      NoIntegralHeight=   0   'False
      HighestPrecedence=   0
      AllowColResize  =   0
      AllowColDragDrop=   0
      ReadOnly        =   0   'False
      VScrollSpecial  =   0   'False
      VScrollSpecialType=   0
      EnableKeyEvents =   -1  'True
      EnableTopChangeEvent=   -1  'True
      DataAutoHeadings=   -1  'True
      DataAutoSizeCols=   2
      SearchIgnoreCase=   -1  'True
      ScrollBarH      =   1
      VirtualPageSize =   0
      VirtualPagesAhead=   0
      ExtendCol       =   0
      ColumnLevels    =   1
      ListGrayAreaColor=   -2147483637
      GroupHeaderHeight=   -1
      GroupHeaderShow =   0   'False
      AllowGrpResize  =   0
      AllowGrpDragDrop=   0
      MergeAdjustView =   0   'False
      ColumnHeaderShow=   -1  'True
      ColumnHeaderHeight=   -1
      GrpsFrozen      =   0
      BorderGrayAreaColor=   -2147483637
      ExtendRow       =   0
      DataField       =   ""
      DataMember      =   ""
      OLEDragMode     =   0
      OLEDropMode     =   0
      EnableClickEvent=   -1  'True
      Redraw          =   -1  'True
      ResizeRowToFont =   0   'False
      TextTipMultiLine=   0
      ColDesigner     =   "frmOrderList.frx":0000
   End
   Begin VB.PictureBox picPictureBox1 
      BorderStyle     =   0  'None
      Height          =   675
      Left            =   120
      ScaleHeight     =   675
      ScaleWidth      =   11175
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   5760
      Width           =   11175
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Cancel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   9180
         TabIndex        =   14
         Top             =   120
         Width           =   1995
      End
      Begin VB.CommandButton cmdDtlRetrieve 
         Caption         =   "Select"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   0
         TabIndex        =   2
         Top             =   120
         Width           =   1995
      End
   End
   Begin VB.Label lblMobilePhone 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   330
      Left            =   7680
      TabIndex        =   16
      Top             =   2100
      Width           =   3630
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Mobile Phone"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   6150
      TabIndex        =   15
      Top             =   2100
      Width           =   1440
   End
   Begin VB.Label lblRefundStatus 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   330
      Left            =   9600
      TabIndex        =   13
      Top             =   1680
      Width           =   1710
   End
   Begin VB.Label lblOrderStat 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   330
      Left            =   7680
      TabIndex        =   12
      Top             =   1680
      Width           =   1710
   End
   Begin VB.Label lblStatus 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Order Status"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5820
      TabIndex        =   11
      Top             =   1680
      Width           =   1770
   End
   Begin VB.Label lblDeliveryAddr 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1530
      Left            =   7680
      TabIndex        =   10
      Top             =   60
      Width           =   3630
   End
   Begin VB.Label lblDeliveryAddrlbl 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Delivery Address"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   5820
      TabIndex        =   9
      Top             =   60
      Width           =   1770
   End
   Begin VB.Label lblLabel2 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Phone Number"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   150
      TabIndex        =   8
      Top             =   2100
      Width           =   1590
   End
   Begin VB.Label lblPhoneNumber 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   330
      Left            =   1800
      TabIndex        =   7
      Top             =   2100
      Width           =   3630
   End
   Begin VB.Label lblDateType 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Collection Date"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   120
      TabIndex        =   6
      Top             =   60
      Width           =   1620
   End
   Begin VB.Label lblDate 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   330
      Left            =   1800
      TabIndex        =   5
      Top             =   60
      Width           =   1350
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Address"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   825
      TabIndex        =   4
      Top             =   480
      Width           =   885
   End
   Begin VB.Label lblAddress 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1530
      Left            =   1800
      TabIndex        =   3
      Top             =   480
      Width           =   3630
   End
End
Attribute VB_Name = "frmOrderList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const COL_ORDERNO   As Long = 0
Const COL_ORDERDET  As Long = 1
Const COL_QTYORDER  As Long = 2
Const COL_QTYTAKEN  As Long = 3
Const COL_QTYREFUND As Long = 4
Const COL_VALORDER  As Long = 5
Const COL_ORDTYPE   As Long = 6
Const COL_ORDDATE   As Long = 7
Const COL_CUSTOMER  As Long = 8
Const COL_DELADDR   As Long = 9
Const COL_ORDSTAT   As Long = 10
Const COL_REFSTAT   As Long = 11

Private moCustomer As cReturnCust
Private moQuote    As cQuoteHeader
Private mOrderNumber As String


Private Sub cmdCancel_Click()

    Call Form_KeyPress(vbKeyEscape)

End Sub

Private Sub cmdDtlRetrieve_Click()

Dim vData As Variant

    vData = Split(lstItems.Text, vbTab)
    mOrderNumber = vData(COL_ORDERNO)
    Me.Hide
    
End Sub

Private Sub Form_Activate()
    
    If lstItems.ListCount = 0 Then
        If (Me.Caption = "Valid Quotes") Then
            Call MsgBoxEx("No quotes on record for today", vbOKOnly & vbInformation, "Customer Quotes", , , , , RGBMSGBox_PromptColour)
        Else
            Call MsgBoxEx("No orders on record for today", vbOKOnly & vbInformation, "Customer Orders", , , , , RGBMSGBox_PromptColour)
        End If
        mOrderNumber = vbNullString
        Me.Hide
        Exit Sub
    Else
        lstItems.Selected = True
    End If

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyEscape Then
        mOrderNumber = ""
        Me.Hide
    End If

End Sub

Public Function GetOrderNo() As String

    ListOrders
    Me.Show vbModal
    GetOrderNo = mOrderNumber
    
End Function

Public Function GetQuoteNo() As String

    ListQuotes
    Me.Show vbModal
    GetQuoteNo = mOrderNumber
    
End Function

Private Sub ListOrders()

Dim oOrder         As cCustOrderHeader
Dim colOrders      As Collection
Dim oQuote         As cQuoteHeader
Dim colQuote       As Collection
Dim strTranDetails As String

    Set moQuote = goDatabase.CreateBusinessObject(CLASSID_QUOTEHEADER)
    Call moQuote.AddLoadField(FID_QUOTEHEADER_IsSold)
    Call moQuote.AddLoadField(FID_QUOTEHEADER_TranDate)
    Call moQuote.AddLoadField(FID_QUOTEHEADER_TillID)
    Call moQuote.AddLoadField(FID_QUOTEHEADER_TransactionNumber)
    
    Set oOrder = goDatabase.CreateBusinessObject(CLASSID_CUSTORDERHEADER)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_OrderNumber)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_TranDate)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_TillID)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_TransactionNumber)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_RefundTranDate)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_RefundTillID)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_RefundTransactionNumber)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_UnitsOrdered)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_UnitsTaken)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_UnitsRefunded)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_MerchandiseValue)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_DeliveryConfirmed)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_CustomerNumber)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_DeliveryAddress1)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_DeliveryAddress2)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_DeliveryAddress3)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_DeliveryAddress4)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_IsForDelivery)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_DateRequired)
    Call oOrder.AddLoadField(FID_CUSTORDERHEADER_Cancelled)
    Call oOrder.AddLoadFilter(CMP_EQUAL, FID_CUSTORDERHEADER_OrderDate, Date)
    Set colOrders = oOrder.LoadMatches()
    
    If colOrders.Count = 0 Then Exit Sub
    
    For Each oOrder In colOrders
        With oOrder
            Call moQuote.ClearLoadFilter
            Call moQuote.AddLoadFilter(CMP_EQUAL, FID_QUOTEHEADER_QuoteNumber, .OrderNumber)
            Call moQuote.LoadMatches
            
            strTranDetails = vbNullString
            If moQuote.IsSold Then
                If oOrder.UnitsRefunded = 0 Then
                    strTranDetails = .TranDate & "-" & .TillID & "-" & .TransactionNumber
                Else
                    strTranDetails = .RefundTranDate & "-" & .RefundTillID & "-" & .RefundTransactionNumber
                End If
            End If
            
            Call lstItems.AddItem(.OrderNumber & vbTab & strTranDetails & vbTab & .UnitsOrdered & vbTab & _
                .UnitsTaken & vbTab & .UnitsRefunded & vbTab & Format(.MerchandiseValue, "0.00") & vbTab & _
                .IsForDelivery & vbTab & .DateRequired & vbTab & .CustomerNumber & vbTab & _
                IIf(.IsForDelivery, .DeliveryAddress1 & vbNewLine & .DeliveryAddress2 & vbNewLine & _
                .DeliveryAddress3 & vbNewLine & .DeliveryAddress4, "N/A") & vbTab & _
                IIf(Not moQuote.IsSold, "Ordered", _
                IIf(LenB(Trim$(.Cancelled)) = 0, "Paid For", "Refunded")) & vbTab & _
                IIf(.Cancelled = "1", "Cancelled", IIf(.Cancelled = "2", "Amended", "")))
        End With
    Next
    lstItems.Row = 0
    
End Sub

Private Sub ListQuotes()

Dim oOrder         As cCustOrderHeader
Dim colOrders      As Collection
Dim oQuote         As cQuoteHeader
Dim oAddress       As cReturnCust
Dim colQuote       As Collection
Dim strTranDetails As String


    Me.Caption = "Valid Quotes"
    lblDateType.Caption = "Quote Created"
    lblDateType.Visible = False
    lblDate.Visible = False
    lblDeliveryAddr.Visible = False
    lblDeliveryAddrlbl.Visible = False
    lblRefundStatus.Visible = False
    lblOrderStat.Visible = False
    lblStatus.Visible = False
    lstItems.Row = -1
    lstItems.ListApplyTo = ListApplyToIndividual
    lstItems.Col = COL_ORDERNO
    lstItems.ColHeaderText = "Quote No"
    
    Set moQuote = goDatabase.CreateBusinessObject(CLASSID_QUOTEHEADER)
    Call moQuote.AddLoadField(FID_QUOTEHEADER_QuoteNumber)
    Call moQuote.AddLoadField(FID_QUOTEHEADER_IsSold)
    Call moQuote.AddLoadField(FID_QUOTEHEADER_TranDate)
    Call moQuote.AddLoadField(FID_QUOTEHEADER_TillID)
    Call moQuote.AddLoadField(FID_QUOTEHEADER_TransactionNumber)
    Call moQuote.AddLoadField(FID_QUOTEHEADER_MerchandiseValue)
    Call moQuote.AddLoadFilter(CMP_GREATEREQUALTHAN, FID_QUOTEHEADER_ExpiryDate, Date)
    Call moQuote.AddLoadFilter(CMP_EQUAL, FID_QUOTEHEADER_IsSold, False)
    
    Set colQuote = moQuote.LoadMatches
    
    If colQuote.Count = 0 Then Exit Sub
    
    lstItems.Row = -1
    lstItems.ListApplyTo = ListApplyToIndividual
    lstItems.Col = COL_QTYORDER
    lstItems.ColHide = True
    lstItems.BackColor = vbGrayText
    lstItems.Col = COL_QTYTAKEN
    lstItems.BackColor = vbGrayText
    lstItems.ColHide = True
    lstItems.Col = COL_QTYREFUND
    lstItems.BackColor = vbGrayText
    lstItems.ColHide = True
    lstItems.ListApplyTo = ListApplyToAllRows
    
    For Each oQuote In colQuote
        With oQuote
            Set oAddress = goDatabase.CreateBusinessObject(CLASSID_RETURNCUST)
            Call oAddress.AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionDate, Date)
            Call oAddress.AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionNumber, oQuote.TransactionNumber)
            Call oAddress.AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_PCTillID, oQuote.TillID)
            Call oAddress.LoadMatches
            
            strTranDetails = vbNullString
            strTranDetails = .TranDate & "-" & .TillID & "-" & .TransactionNumber
            
            Call lstItems.AddItem(.QuoteNumber & vbTab & strTranDetails & vbTab & 0 & vbTab & _
                0 & vbTab & 0 & vbTab & Format(.MerchandiseValue, "0.00") & vbTab & _
                .IsForDelivery & vbTab & "N/A" & vbTab & .Customer & vbTab & _
                IIf(.IsForDelivery, oAddress.AddressLine1 & vbNewLine & oAddress.AddressLine2 & vbNewLine & _
                oAddress.AddressLine3 & vbNewLine & oAddress.AddressLine3, "N/A") & vbTab & _
                "Quoted", _
                "XX" & vbTab & _
                "XX")
        End With
    Next
    lstItems.Row = 0
    
End Sub

Private Sub Form_Load()
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    cmdDtlRetrieve.BackColor = Me.BackColor
    picPictureBox1.BackColor = Me.BackColor

End Sub

Private Sub lstItems_KeyPress(KeyAscii As Integer)

Dim vData       As Variant
Dim oTranHeader As cPOSHeader

    If KeyAscii = vbKeyReturn Then
        
        'get Quote Header to extract the pending DLTOTS request against the order
        vData = Split(lstItems.Text, vbTab)
        Set moQuote = goDatabase.CreateBusinessObject(CLASSID_QUOTEHEADER)
        Call moQuote.ClearLoadFilter
        Call moQuote.AddLoadFilter(CMP_EQUAL, FID_QUOTEHEADER_QuoteNumber, vData(COL_ORDERNO))
        Call moQuote.LoadMatches
        If (moQuote.QuoteNumber <> vData(COL_ORDERNO)) Then
            Call MsgBoxEx("unable to locate Quote Header for Order - '" & vData(COL_ORDERNO) & "'", vbOKOnly + vbInformation, "Invalid Order", , , , , RGBMsgBox_WarnColour)
            Exit Sub
        End If
        
        'For Order check if already processed
        If (lblDateType.Visible = True) Then
            'Retrieve last action required and ensure not already processed
            Set oTranHeader = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
            Call oTranHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, moQuote.TillID)
            Call oTranHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, moQuote.TranDate)
            Call oTranHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, moQuote.TransactionNumber)
            Call oTranHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_Voided, True)
            Call oTranHeader.IBo_LoadMatches
            If (oTranHeader.TranParked = False) And (oTranHeader.RecoveredFromParked = True) Then
                If (oTranHeader.TransactionCode = "SA") Then
                    Call MsgBoxEx("Order has already been Paid For", vbOKOnly + vbInformation, "Invalid Order Selected", , , , , RGBMsgBox_WarnColour)
                    Exit Sub
                End If
                If (oTranHeader.TransactionCode = "RF") Then
                    Call MsgBoxEx("Pending Refund for Order has already been Processed", vbOKOnly + vbInformation, "Invalid Order Selected", , , , , RGBMsgBox_WarnColour)
                    Exit Sub
                End If
            End If
        End If
        
        cmdDtlRetrieve.Value = True
    End If

End Sub

Private Sub lstItems_SelChange(ItemIndex As Long)

Dim colCustomer As Collection
Dim vData       As Variant
Dim vKey        As Variant

    lstItems.Row = ItemIndex
    vData = Split(lstItems.Text, vbTab)
    vKey = Split(vData(1), "-")

    If moCustomer Is Nothing Then
        Set moCustomer = goDatabase.CreateBusinessObject(CLASSID_RETURNCUST)
        Call moCustomer.AddLoadField(FID_RETURNCUST_CustomerName)
        Call moCustomer.AddLoadField(FID_RETURNCUST_HouseName)
        Call moCustomer.AddLoadField(FID_RETURNCUST_AddressLine1)
        Call moCustomer.AddLoadField(FID_RETURNCUST_AddressLine2)
        Call moCustomer.AddLoadField(FID_RETURNCUST_AddressLine3)
        Call moCustomer.AddLoadField(FID_RETURNCUST_PostCode)
        Call moCustomer.AddLoadField(FID_RETURNCUST_PhoneNo)
        Call moCustomer.AddLoadField(FID_RETURNCUST_MobileNo)
        Call moCustomer.AddLoadField(FID_RETURNCUST_Email)
        Call moCustomer.AddLoadField(FID_RETURNCUST_WorkTelNumber)
    Else
        Call moCustomer.ClearLoadFilter
    End If
    
    Call moQuote.ClearLoadFilter
    Call moQuote.AddLoadFilter(CMP_EQUAL, FID_QUOTEHEADER_QuoteNumber, vData(0))
    Call moQuote.LoadMatches
    
    If LenB(Trim$(vData(7))) > 0 Then
        Call moCustomer.AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionDate, moQuote.TranDate)
        Call moCustomer.AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_PCTillID, moQuote.TillID)
        Call moCustomer.AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionNumber, moQuote.TransactionNumber)
        Set colCustomer = moCustomer.LoadMatches
        If colCustomer.Count > 0 Then
            Set moCustomer = colCustomer.Item(1)
            With moCustomer
                lblAddress.Caption = .CustomerName & vbNewLine & .AddressLine1 & vbNewLine & .AddressLine2 & vbNewLine & .AddressLine3 & vbNewLine & .PostCode
                'Address = .CustomerName & vbNewLine & .HouseName & vbNewLine & .AddressLine1 & vbNewLine & .AddressLine2 & vbNewLine & .AddressLine3 & vbNewLine & .PostCode
                lblPhoneNumber.Caption = .PhoneNo
                lblMobilePhone.Caption = .MobileNo
            End With
        Else
            lblAddress.Caption = "N/A"
            lblPhoneNumber.Caption = "N/A"
            lblMobilePhone.Caption = "N/A"
        End If
    Else
        lblAddress.Caption = "N/A"
        lblPhoneNumber.Caption = "N/A"
        lblMobilePhone.Caption = "N/A"
    End If
    
    If CBool(vData(COL_ORDTYPE)) Then
        lblDateType.Caption = "Delivery Date"
    Else
        lblDateType.Caption = "Collection Date"
    End If
    lblDate = vData(COL_ORDDATE)
    
    lblDeliveryAddr = vData(COL_DELADDR)
    
    lblOrderStat = vData(COL_ORDSTAT)
    
    lblRefundStatus.Caption = vData(11)
    
    
End Sub
