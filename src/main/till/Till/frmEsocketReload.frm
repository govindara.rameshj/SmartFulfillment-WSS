VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "Mswinsck.ocx"
Object = "{C8530F8A-C19C-11D2-99D6-9419F37DBB29}#1.1#0"; "ccrpprg6.ocx"
Begin VB.Form frmEsocketReload 
   BackColor       =   &H000000FF&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "EPOS System reconnection"
   ClientHeight    =   2835
   ClientLeft      =   300
   ClientTop       =   1740
   ClientWidth     =   8865
   ControlBox      =   0   'False
   Icon            =   "frmEsocketReload.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2835
   ScaleWidth      =   8865
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Timer1 
      Interval        =   250
      Left            =   240
      Top             =   2280
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   6840
      TabIndex        =   1
      Top             =   2280
      Visible         =   0   'False
      Width           =   1635
   End
   Begin MSWinsockLib.Winsock Winsock1 
      Left            =   8400
      Top             =   1260
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin CCRProgressBar6.ccrpProgressBar pbStatus 
      Height          =   375
      Left            =   360
      Top             =   1740
      Width           =   8115
      _ExtentX        =   14314
      _ExtentY        =   661
      FillColor       =   16761024
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   0
      Max             =   6
   End
   Begin VB.Label lblStatus 
      BackStyle       =   0  'Transparent
      Caption         =   "Unable to establish link to EPOS Payment system.  Attempt to reconnect."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1395
      Left            =   360
      TabIndex        =   0
      Top             =   240
      Width           =   8175
   End
End
Attribute VB_Name = "frmEsocketReload"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim mlngNoTries     As Long
Public Reconnected  As Boolean

Private Sub cmdClose_Click()

    Call Me.Hide

End Sub

Private Sub Form_Load()

    lblStatus.Caption = "Unable to establish link to EPOS Payment system." & vbCrLf & "Attempting to reconnect." & vbCrLf & vbCrLf & "PLEASE WAIT"
    Call CentreForm(Me)
    Reconnected = False
    
End Sub

Private Sub LaunchESocket()
    
    pbStatus.Caption = "Launching EPOS Software : Attempt " & mlngNoTries
    On Error Resume Next
    Call Shell(Chr(34) & "C:\postilion\esocket.pos\bin\run_xml.bat" & Chr(34), vbMinimizedNoFocus)
    On Error GoTo 0
    pbStatus.Caption = "Calling EPOS Software (Waiting) : Attempt " & mlngNoTries
    Wait (6)
    pbStatus.Caption = "Launching EPOS Software (Waiting) : Attempt " & mlngNoTries
    Wait (4)

End Sub
Private Function Connect() As Boolean

    pbStatus.Caption = "Connecting to EPOS Software : Attempt " & mlngNoTries
    If (Winsock1.State = sckConnected) Then Winsock1.Close
    
    'Connect to the IP address of the machine with the eSocket software on it
    Call DebugMsg("Re-Connect", vbNullString, endlDebug, "Connecting to WinSck")
    Call Winsock1.Connect("127.0.0.1", "25000")

    While Winsock1.State = sckConnecting
        DoEvents
    Wend

    Select Case Winsock1.State
        Case sckConnected: Connect = True
        Case sckError: Connect = False
        Case Else
    End Select
    Call Winsock1.Close

End Function

Private Sub Timer1_Timer()
    
    Timer1.Enabled = False
    Timer1.Interval = 0
    pbStatus.Max = 6
    For mlngNoTries = 1 To 3 Step 1
        pbStatus.Value = mlngNoTries * 2 - 1
        Call LaunchESocket
        pbStatus.Value = mlngNoTries * 2
        If (Connect = True) Then
            Reconnected = True
            Exit For
        End If
    Next mlngNoTries
    If (Reconnected = False) Then
        cmdClose.Visible = True
        lblStatus.Caption = "Failed to establish link to EPOS Payment system." & vbCrLf & vbCrLf & "CONTACT : IT Support to report problem"
    Else
        If (Me.Visible = True) Then Call Me.Hide
    End If

End Sub
