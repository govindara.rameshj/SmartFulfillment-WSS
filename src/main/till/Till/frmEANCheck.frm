VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{8DDE6232-1BB0-11D0-81C3-0080C7A2EF7D}#3.0#0"; "Flp32a30.ocx"
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Begin VB.Form frmEANCheck 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "EAN/Barcode Check"
   ClientHeight    =   5475
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8520
   Icon            =   "frmEANCheck.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5475
   ScaleWidth      =   8520
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox fraReasonsList 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   5115
      Left            =   0
      ScaleHeight     =   5115
      ScaleWidth      =   8415
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   8415
      Begin LpLib.fpList lstReasonsList 
         Height          =   4350
         Left            =   1920
         TabIndex        =   5
         Top             =   540
         Width           =   4575
         _Version        =   196608
         _ExtentX        =   8070
         _ExtentY        =   7673
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Columns         =   2
         Sorted          =   0
         LineWidth       =   1
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         MultiSelect     =   0
         WrapList        =   0   'False
         WrapWidth       =   0
         SelMax          =   -1
         AutoSearch      =   2
         SearchMethod    =   0
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   0
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   0
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   0
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   0
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   0   'False
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         DataField       =   ""
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         ColDesigner     =   "frmEANCheck.frx":0A96
      End
      Begin VB.TextBox txtReasonDescription 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   240
         MaxLength       =   40
         TabIndex        =   7
         Top             =   5580
         Width           =   7935
      End
      Begin VB.CommandButton cmdListOk 
         Caption         =   "Select"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   6540
         TabIndex        =   6
         Top             =   4380
         Width           =   1875
      End
      Begin VB.Label lblReasonDescription 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Select SKU Keyed In Reason"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   60
         TabIndex        =   9
         Top             =   60
         Width           =   8415
      End
      Begin VB.Label lblReasonDesclbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Description"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   8
         Top             =   5220
         Width           =   7815
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   5460
      TabIndex        =   3
      Top             =   1740
      Width           =   1695
   End
   Begin VB.CommandButton cmdProcess 
      Caption         =   "Enter"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   3600
      TabIndex        =   2
      Top             =   1740
      Width           =   1695
   End
   Begin VB.TextBox txtSKU 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   2040
      TabIndex        =   1
      Top             =   720
      Width           =   5175
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   10
      Top             =   5100
      Width           =   8520
      _ExtentX        =   15028
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmEANCheck.frx":0ED5
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7646
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   635
            MinWidth        =   635
            Picture         =   "frmEANCheck.frx":187B
            Key             =   "NumPad"
            Object.ToolTipText     =   "Show/Hide Num Pad"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "15:41"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   7320
      Top             =   60
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin VB.Label lblType 
      BackStyle       =   0  'Transparent
      Caption         =   "SKU"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   900
      TabIndex        =   0
      Top             =   840
      Width           =   1275
   End
End
Attribute VB_Name = "frmEANCheck"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const MODULE_NAME As String = "frmEANCheck"

Const SKU_REJECT = 93
Const IN_HOUSE_SKU_MISS As String = 94
Const SKU_MISSING As String = 95
Const IN_HOUSE_EAN_MISS As String = 96
Const EAN_SKU_MISS_CODE As String = 97
Const EAN_MISS_CHECK_CODE As String = 98


Public Event Duress()
Private mintDuressKeyCode As Integer
Private mblnScanned     As Boolean
Private mstrUserID      As String
Private mblnKPHidden    As Boolean

Public Property Let UserID(Value As String)

    mstrUserID = Value
    
End Property


Private Sub cmdListCancel_Click()

End Sub

Private Sub cmdCancel_Click()

    Call Me.Hide

End Sub

Private Sub cmdListOk_Click()

    If (lstReasonsList.ListIndex <> -1) Then fraReasonsList.Visible = False

End Sub

Private Sub cmdProcess_Click()

Dim oItem       As cInventory
Dim lRow        As Long
Dim sErrMsg     As String
Dim blnContinue As Boolean
Dim oEANBO      As cEAN
Dim oEANCheckBO As cEANCheck
Dim strEANNo    As String

Dim strCorrectSKU   As String

Dim strTempCode  As String
Dim dblTempPrice As Double

Dim dteBlankDate  As Date

Dim strPartCode As String

Dim blnEANFound   As Boolean
Dim blnEANEntry   As Boolean
Dim blnInHouseEAN As Boolean
Dim blnInHouseErr As Boolean
Dim strEANSKU     As String
Dim blnKeyPad     As Boolean

Const PROCEDURE_NAME As String = MODULE_NAME & ".GetItem"

    On Error GoTo Catch
Try:
    'Main procedure code goes here
                
    strPartCode = txtSKU.Text
    If (strPartCode = mstrGiftVoucherSKU) Then
        Call MsgBoxEx("This SKU is reserved for Gift Vouchers only", vbExclamation, _
                      "Reserved SKU Entered", , , , , RGBMsgBox_WarnColour)
        txtSKU.Text = vbNullString
        'Clear out Line Quantity for Gift vouchers - ref 2010-016 1.
        frmTill.mdblLineQuantity = 1
        frmTill.lblQty.Caption = "1"
        frmTill.fraQty.Visible = False
        If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then txtSKU.SetFocus
        Exit Sub
    End If
        
    If Len(strPartCode) > 6 Then
        'check if Part Code is actually an EAN so look up as EAN
        strEANNo = Right$("0000000000000000" & strPartCode, 16)
        blnEANEntry = True
        Set oEANBO = goDatabase.CreateBusinessObject(CLASSID_EAN)
        Call oEANBO.AddLoadFilter(CMP_EQUAL, FID_EAN_EANNumber, strEANNo)
        Call oEANBO.LoadMatches
        If LenB(oEANBO.PartCode) = 0 Then  'EAN not found then down grade to In House EAN
            'Chop off all pre-leading Zero
            While (Left$(strEANNo, 1) = "0") And (Len(strEANNo) > 6)
                strEANNo = Mid$(strEANNo, 2)
            Wend
            'Check if First Character is a 2 and there are 8 characters
            'This is to notate a Client based EAN
            If (Left$(strEANNo, 1) = "2") And (Len(strEANNo) = 8) Then
                strPartCode = Mid$(strEANNo, 2, 6)
                blnInHouseErr = True
            Else
                frmTill.mblnStopScanning = True
                Load frmNotFound
                Call frmNotFound.DisplayNotFound(strEANNo)
                Set oEANCheckBO = CreateEANReject(mblnScanned, "0000", EAN_MISS_CHECK_CODE, txtSKU.Text, "EAN", False, "000000", False, "000000", mstrUserID)
                If (goSession.ISession_IsOnline = False) Then
                    Call moTranHeader.AddOfflineEANCheck(oEANCheckBO)
                End If
                txtSKU.Text = vbNullString
                Exit Sub
            End If
        Else
            'for barcode logging check if EAN is an In-House EAN
            'Chop off all pre-leading Zero
            While (Left$(strEANNo, 1) = "0") And (Len(strEANNo) > 6)
                strEANNo = Mid$(strEANNo, 2)
            Wend
            'Check if First Character is a 2 and there are 8 characters
            'This is to notate a Client based EAN
            If (Left$(strEANNo, 1) = "2") And (Len(strEANNo) = 8) Then
                blnInHouseEAN = True
            End If

            blnEANFound = True
            strPartCode = oEANBO.PartCode
            strEANSKU = strPartCode 'save for later saving into EANReject
        End If
    End If
    
    Set oItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
    Call oItem.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, strPartCode)
    Call oItem.AddLoadField(FID_INVENTORY_PartCode)
    Call oItem.AddLoadField(FID_INVENTORY_Description)
    
    
    'call oitem.AddLoadField(FID_INVENTORY_
    Call oItem.LoadMatches
    
    If LenB(oItem.Description) = 0 Then  'Item not found
        frmTill.mblnStopScanning = True
        Load frmNotFound
        Call frmNotFound.DisplayNotFound(strPartCode)
        If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then txtSKU.SetFocus
        Set oEANCheckBO = CreateEANReject(mblnScanned, "0000", IIf(blnEANEntry, IIf(blnInHouseEAN, "94", IIf(blnInHouseErr, "96", "97")), IIf(blnInHouseErr, "96", "95")), txtSKU.Text, IIf(blnEANEntry, IIf(blnInHouseEAN, "IN-HOUSE", IIf(blnInHouseErr, "IN-HOUSE", "EAN")), "SKU"), blnEANFound, strEANSKU, False, "000000", mstrUserID)
        If (goSession.ISession_IsOnline = False) Then
            Call moTranHeader.AddOfflineEANCheck(oEANCheckBO)
        End If
        txtSKU.Text = vbNullString
        Exit Sub
    End If
    
    If (MsgBoxEx("Item is '" & oItem.PartCode & "-" & oItem.Description & "' entered." & vbNewLine & "Does this match the Physical Item?", vbYesNo, "Confirm SKU match", , , , , RGBMSGBox_PromptColour) = vbNo) Then
        strCorrectSKU = ""
        While (strCorrectSKU = "") 'either valid SKU entered or Cancel pressed
TryAgain:
            Load frmEntry
            If (ucKeyPad1.Visible = True) Then Call frmEntry.ShowNumPad
            blnKeyPad = ucKeyPad1.Visible
            ucKeyPad1.Visible = False
            strCorrectSKU = frmEntry.GetEntry("Enter correct SKU for physical item", "Confirm correct SKU", RGBMSGBox_PromptColour, 6, True)
            ucKeyPad1.Visible = blnKeyPad
            Unload frmEntry
            If (strCorrectSKU <> "") Then 'SKU entered so retrieve
                Set oItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
                Call oItem.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, strCorrectSKU)
                Call oItem.AddLoadField(FID_INVENTORY_PartCode)
                Call oItem.AddLoadField(FID_INVENTORY_Description)
                Call oItem.LoadMatches
                
                If LenB(oItem.Description) = 0 Then  'Item not found
                    frmTill.mblnStopScanning = True
                    Load frmNotFound
                    Call frmNotFound.DisplayNotFound(strCorrectSKU)
                    strCorrectSKU = ""
                    'Go Back to Confirm Sku, need to re-confirm - 2008-021
                    GoSub TryAgain
                Else 'Item was not found
                    If (MsgBoxEx("Item is '" & oItem.PartCode & "-" & oItem.Description & "' entered." & vbNewLine & "Does this match the Physical Item?", vbYesNo, "Confirm SKU match", , , , , RGBMSGBox_PromptColour) = vbNo) Then
                        strCorrectSKU = ""
                    Else
                        Set oEANCheckBO = CreateEANReject(mblnScanned, "0000", SKU_REJECT, txtSKU.Text, IIf(blnEANEntry, IIf(blnInHouseEAN, "IN-HOUSE", IIf(blnInHouseErr, "IN-HOUSE", "EAN")), "SKU"), blnEANFound, strEANSKU, False, strCorrectSKU, mstrUserID)
                        If (goSession.ISession_IsOnline = False) Then
                            Call moTranHeader.AddOfflineEANCheck(oEANCheckBO)
                        End If
                        txtSKU.Text = vbNullString
                        If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then txtSKU.SetFocus
                    End If
                End If
            Else
                txtSKU.Text = vbNullString
                If (txtSKU.Visible = True) And (txtSKU.Enabled = True) Then txtSKU.SetFocus
                Exit Sub
            End If 'Cancelled Enter correct SKU
        Wend
    Else
        If (mblnScanned = False) Then
            Call MsgBoxEx("Entry was keyed in." & vbCrLf & "Select reason why entry was not scanned.", vbInformation, "Select Reason", , , , , RGBMSGBox_PromptColour)
            strTempCode = GetKeyedReasonCode
            Set oEANCheckBO = CreateEANReject(mblnScanned, "0000", strTempCode, txtSKU.Text, IIf(blnEANEntry, IIf(blnInHouseEAN, "IN-HOUSE", IIf(blnInHouseErr, "IN-HOUSE", "EAN")), "SKU"), blnEANFound, strEANSKU, False, "000000", mstrUserID)
            If (goSession.ISession_IsOnline = False) Then
                Call moTranHeader.AddOfflineEANCheck(oEANCheckBO)
            End If
        End If
        txtSKU.Text = vbNullString
    End If
    
        
    Set oItem = Nothing
    'GetItem = True

    'Tidy up
    GoSub Finally

    Exit Sub

    '<CAEH>
Catch:
    Call Err.Push 'Preserve Error Code
    'Tidy up.
    GoSub Finally

    Call Err.Pop 'Retrieve Error Code
    'Re-raise error to next handler up the chain.
    Call Err.Bubble("Till", PROCEDURE_NAME, CLng(Erl))
    Exit Sub
Resume Next
Finally:
    'Perform tidy up here

    Return
    '</CAEH>

End Sub

Public Function GetCorrectSKU(ByVal blnScanned As Boolean, Optional CreateLogEntry As Boolean = True, Optional UpdateLogID As Long = 0) As String

Const PROCEDURE_NAME As String = "GetCorrectSKU"

Dim strCorrectSKU As String
Dim blnKeyPad     As Boolean
Dim oItem         As cInventory
Dim oEANCheckBO   As cEANCheck

Try:
    Load frmEntry
    If (ucKeyPad1.Visible = True) Then Call frmEntry.ShowNumPad
    blnKeyPad = ucKeyPad1.Visible
    ucKeyPad1.Visible = False
    While (strCorrectSKU = "")
        strCorrectSKU = frmEntry.GetEntry("Enter correct SKU for physical item", "Confirm correct SKU", RGBMSGBox_PromptColour, 6, True)
        ucKeyPad1.Visible = blnKeyPad
        Unload frmEntry
        
        If (LenB(strCorrectSKU) = 0) Then 'cancel pressed so just exit
            GetCorrectSKU = ""
            Exit Function
        End If
        
        'strCorrectSKU = InputBoxEx("Enter correct SKU for physical item", "Confirm correct SKU", oItem.PartCode, enifAlphaNum, False, RGBMSGBox_PromptColour, 0, 6)
        Set oItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
        Call oItem.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, strCorrectSKU)
        Call oItem.AddLoadField(FID_INVENTORY_PartCode)
        Call oItem.AddLoadField(FID_INVENTORY_Description)
                    
        Call oItem.LoadMatches
        
        If LenB(oItem.Description) = 0 Then  'Item not found
            frmTill.mblnStopScanning = True
            Load frmNotFound
            Call frmNotFound.DisplayNotFound(strCorrectSKU)
            strCorrectSKU = ""
            'Confirm correct Sku not found, need to re-confirm - 2008-021
        Else
            If (MsgBoxEx("Item is '" & oItem.PartCode & "-" & oItem.Description & "' entered." & vbNewLine & "Does this match the Physical Item?", vbYesNo, "Confirm SKU match", , , , , RGBMSGBox_PromptColour) = vbNo) Then
                strCorrectSKU = ""
            Else
                If (CreateLogEntry = True) Then
                    If (UpdateLogID <> 0) Then
                        'Update existing EAN Check
                        Set oEANCheckBO = UpdateEANReject(UpdateLogID, strCorrectSKU)
                    Else
                        Set oEANCheckBO = CreateEANReject(blnScanned, moTranHeader.TransactionNo, SKU_REJECT, txtSKU.Text, "SKU", False, "", False, strCorrectSKU, mstrUserID)
                    End If
                    If (goSession.ISession_IsOnline = False) Then
                        Call moTranHeader.AddOfflineEANCheck(oEANCheckBO)
                    End If
                End If
            End If
        End If
    Wend
    Set oItem = Nothing
    
    GetCorrectSKU = strCorrectSKU

    'Tidy up
    GoSub Finally

    Exit Function

    '<CAEH>
Catch:
    Call Err.Push 'Preserve Error Code
    'Tidy up.
    GoSub Finally

    Call Err.Pop 'Retrieve Error Code
    'Re-raise error to next handler up the chain.
    Call Err.Bubble("Till", PROCEDURE_NAME, CLng(Erl))
    Exit Function
Resume Next
Finally:
    'Perform tidy up here

    Return
    '</CAEH>

    Me.Show (vbModal)

End Function

Public Property Let BarcodedValue(Value As String)

    txtSKU.Text = Value
    mblnScanned = True
    Call txtSKU_KeyPress(vbKeyReturn)
    mblnScanned = False
    
End Property

Private Function GetKeyedReasonCode() As String

Dim oBCCodes  As cBarcodeBroken
Dim lngCodeNo As Long
Dim lngHeight As Long
Dim blnAddMsg As Boolean
Dim strTempCode As String

    txtSKU.Enabled = False
    cmdProcess.Enabled = False
    cmdCancel.Enabled = False
    
    fraReasonsList.Width = Me.Width
    fraReasonsList.Left = 0
    lstReasonsList.Left = (Me.Width - lstReasonsList.Width) / 2
    
    If (lstReasonsList.Left < (cmdListOk.Width + 480)) Then
        lstReasonsList.Left = Me.Width - 480 - cmdListOk.Width - lstReasonsList.Width
    End If
    lblReasonDescription.Left = 120
    lblReasonDescription.Width = fraReasonsList.Width - 240
    
    fraReasonsList.Visible = True
    Call lstReasonsList.Clear
    
    txtReasonDescription.Text = "Select SKU Keyed In Reason"

    If mcolBarcodeFailedCodes Is Nothing Then
        Set oBCCodes = goDatabase.CreateBusinessObject(CLASSID_BARCODEFAILED)
        Set mcolBarcodeFailedCodes = oBCCodes.LoadMatches
    End If
        
    For Each oBCCodes In mcolBarcodeFailedCodes
        lstReasonsList.Col = 1
        Call lstReasonsList.AddItem(oBCCodes.Description & vbTab & oBCCodes.Code)
    Next
    
    cmdListOk.Left = lstReasonsList.Width + lstReasonsList.Left + 240
    'check if only 1 entry then auto select
    If lstReasonsList.ListCount = 1 Then
        Call lstReasonsList_KeyPress(vbKeyReturn)
    Else 'display list to select from
        If (lstReasonsList.Visible = True) And (lstReasonsList.Enabled = True) Then Call lstReasonsList.SetFocus
    End If
    lngHeight = Me.Height
    Me.Height = 5850
    lstReasonsList.Col = 1
    lstReasonsList.ColHide = True
    lstReasonsList.Col = 0
    lstReasonsList.ColWidth = 20
    Call CentreForm(Me)
    While (lstReasonsList.Visible = True)
        DoEvents
    Wend
    strTempCode = lstReasonsList.List(lstReasonsList.ListIndex)
    GetKeyedReasonCode = Mid$(strTempCode, InStr(lstReasonsList.List(lstReasonsList.ListIndex), vbTab) + 1)
    txtSKU.Enabled = True
    cmdProcess.Enabled = True
    cmdCancel.Enabled = True

    Me.Height = lngHeight
    Call CentreForm(Me)
    

End Function


Public Sub InitialiseStatusBar()
    
    sbStatus.Panels(PANEL_VERNO).Text = "Ver. " & App.Major & "." & App.Minor & "." & App.Revision
    sbStatus.Panels(PANEL_VERNO).ToolTipText = "Program Version Number"
    sbStatus.Panels(PANEL_WSID + 1).Text = Right$("00" & goSession.CurrentEnterprise.IEnterprise_WorkstationID, 2) & " "
    sbStatus.Panels(PANEL_WSID + 1).ToolTipText = "Current Work-Station Number"
    sbStatus.Panels(PANEL_DATE + 1).Text = Format$(Date, "DD-MMM-YY")
    If (mblnShowKeypadIcon = False) Then sbStatus.Panels("NumPad").Picture = Nothing

End Sub

Private Sub Form_Activate()

    ucKeyPad1.ShowNumPad
    If (mblnKPHidden = False) Then ucKeyPad1.Visible = False
    mblnKPHidden = True

End Sub

Private Sub sbStatus_PanelClick(ByVal Panel As MSComctlLib.Panel)

Dim blnShow As Boolean

    Select Case (Panel.Key)
        Case ("NumPad"):
                        If (lstReasonsList.Visible = False) And ((Panel.Picture Is Nothing) = False) Then
                            ucKeyPad1.Visible = Not ucKeyPad1.Visible
                            If (ucKeyPad1.Visible = True) Then
                                Me.Width = ucKeyPad1.Left + ucKeyPad1.Width + 240
                                Me.Height = ucKeyPad1.Height + sbStatus.Height + 480
                            Else
                                Me.Width = lblType.Left + cmdCancel.Width + cmdCancel.Left
                                Me.Height = 3625
                            End If
                            Call CentreForm(Me)
                            
                        End If
    End Select

End Sub

Private Sub Form_Load()

    Me.Height = 3625
    Call CentreForm(Me)
    Call InitialiseStatusBar
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)

End Sub

Private Sub lstReasonsList_KeyPress(KeyAscii As Integer)

    If (lstReasonsList.ListIndex <> -1) Then
        fraReasonsList.Visible = False
    End If
        
End Sub

Private Sub txtSKU_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then cmdProcess.Value = True

End Sub

Private Sub ucKeyPad1_Resize()
    
    ucKeyPad1.Width = ucKeyPad1.Width

End Sub
