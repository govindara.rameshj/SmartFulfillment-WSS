VERSION 5.00
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Object = "{6810AD67-518F-412B-B3CF-D5F6BA19FDA4}#5.1#0"; "ucPostCodeEntry.ocx"
Begin VB.Form frmPostcode 
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   1710
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   2925
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   1710
   ScaleWidth      =   2925
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   2580
      Top             =   1380
      Visible         =   0   'False
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin ucPostCodeEntry.ucPCEntry ucPostcodeEntry 
      Height          =   1695
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   2990
   End
End
Attribute VB_Name = "frmPostcode"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Cancel   As Boolean
Public PostCode As String
Public RegionPath  As String
Public CountryCode As String

Public Event Duress()
Private mintDuressKeyCode As Integer
Private mblnShowKeyPad As Boolean

Private Sub Form_Activate()

    ucPostcodeEntry.RegionPath = RegionPath
    ucPostcodeEntry.CountryCode = CountryCode

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
'    If mintDuressKeyCode <> 0 And KeyCode = mintDuressKeyCode Then
'        KeyCode = 0
'        RaiseEvent Duress
'    End If

End Sub

Private Sub Form_Load()
    
    Me.BackColor = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
    ucPostcodeEntry.RGBEdit_Colour = RGBEdit_Colour
    ucPostcodeEntry.BackColor = Me.BackColor
    mintDuressKeyCode = goSession.GetParameter(PRM_KEY_DURESS)
    
End Sub

Public Sub ShowNumPad()
    
    ucKeyPad1.Visible = True
    mblnShowKeyPad = True

End Sub

Private Sub Form_Resize()

    ucPostcodeEntry.Top = 0
    If (ucKeyPad1.Visible = True) Or ((Me.Visible = False) And (mblnShowKeyPad = True)) Then
        Me.Height = ucPostcodeEntry.Height + ucKeyPad1.Height
        Me.Width = ucKeyPad1.Width
    Else
        Me.Height = ucPostcodeEntry.Height
        Me.Width = ucPostcodeEntry.Width
    End If
    ucPostcodeEntry.Left = (Me.Width - ucPostcodeEntry.Width) / 2
    ucKeyPad1.Top = ucPostcodeEntry.Height
    ucKeyPad1.Left = 0
    Call CentreForm(Me)

End Sub

Private Sub ucPostcodeEntry_Cancel()
    Cancel = True
    Unload Me
End Sub

Private Sub ucPostcodeEntry_OK(strPostCode As String)
    Cancel = False
    PostCode = strPostCode
    Unload Me
End Sub
