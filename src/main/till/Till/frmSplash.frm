VERSION 5.00
Begin VB.Form frmSplash 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6300
   ClientLeft      =   225
   ClientTop       =   1380
   ClientWidth     =   8835
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmSplash.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   6300
   ScaleWidth      =   8835
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Image Image1 
      Height          =   1290
      Left            =   2565
      Picture         =   "frmSplash.frx":000C
      Top             =   2040
      Width           =   3705
   End
   Begin VB.Label lblStatus 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      TabIndex        =   1
      Top             =   5160
      Width           =   4695
   End
   Begin VB.Label lblLoading 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Loading..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3765
      TabIndex        =   0
      Top             =   4500
      Width           =   1245
   End
End
Attribute VB_Name = "frmSplash"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Implements ICcrpTimerNotifyEx
'Private tmr As ccrpTimer
'Private Done As Boolean

'Private Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, _
                                                   ByVal x As Long, ByVal y As Long, ByVal cx As Long, _
                                                   ByVal cy As Long, ByVal wFlags As Long) As Long
' SetWindowPos constants:
'Const HWND_TOPMOST = -1
'Const HWND_NOTOPMOST = -2

'Const SWP_NOSIZE = &H1
'Const SWP_NOMOVE = &H2
'Const SWP_NOACTIVATE = &H10

'Private Sub Form_Load()

'    Dim hWndInsertAfter As Long
    
'    Set tmr = New ccrpTimer
'    tmr.Interval = 10
'    Set tmr.NotifyEx = Me
'    tmr.Enabled = True
    
'    SetWindowPos Me.hwnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOSIZE Or SWP_NOMOVE Or SWP_NOACTIVATE
    
    'Call aniActivity.Open("activitybar.avi")

'End Sub

'Private Sub Form_Unload(Cancel As Integer)
'    Done = True
'End Sub

'Private Sub ICcrpTimerNotifyEx_Timer(ByVal Milliseconds As Long, ByVal tmr As ccrpTimers6.ccrpTimer)
'
'If Done Then
'    tmr.Enabled = False
'    Exit Sub
'End If
'
'Static pbPos As Long
'
'    pbPos = (pbPos + 1) Mod 100
'    pb.Value = pbPos
'    pb.Refresh
'
'End Sub
Public Sub DisplayStatus(strMessage As String)

    lblStatus.Caption = strMessage
    Call DebugMsg("SPLASH-TILL", "Loading", endlDebug, strMessage)
    lblStatus.Refresh

End Sub

