VERSION 5.00
Object = "{B3202B8D-B8B9-4655-9712-EAB5BFD920D4}#1.0#0"; "ItemFilter.ocx"
Begin VB.Form frmItemFilter 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Select Item"
   ClientHeight    =   5430
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6885
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5430
   ScaleWidth      =   6885
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin ItemFilter_UC_Wickes.ucItemFilter ucItemFind 
      Height          =   3075
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   5424
   End
End
Attribute VB_Name = "frmItemFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const MODULE_NAME As String = "frmItemFilter"

Private mstrSKU As String
Public ItemFilterVisible As Boolean

Public Sub ItemEnquiry(etEnquiryType As EnquiryType, Optional ByVal ViewOnly As Boolean = False)

    ucItemFind.ViewOnly = ViewOnly
    ucItemFind.ItemEnquiryType = etEnquiryType
    ucItemFind.Visible = True
    ItemFilterVisible = True
'    Me.Hide
    Call ucItemFind.FillScreen(ucItemFind)
'    Call ucItemFind.SetFocus
    ItemFilterVisible = False

    If (mstrSKU <> "") Then
        Call frmTill.ucItemFind_Apply(mstrSKU)
    Else
        frmTill.ucItemFind_Cancel
    End If

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    Call DebugMsg(MODULE_NAME, "KeyDown", endlDebug, "Key=" & KeyCode)

End Sub

Private Sub Form_Load()

    Call ucItemFind.Initialise(goSession, Me)

End Sub

Private Sub ucItemFind_Apply(PartCode As String)
    
    DoEvents
    mstrSKU = PartCode

End Sub

Private Sub ucItemFind_Cancel()

    DoEvents
    mstrSKU = ""

End Sub

Public Sub BarcodeScanned(strSKU As String)

    ucItemFind.LookupSKU = strSKU

End Sub

Public Function GetCorrectSKU() As String

    ucItemFind.ViewOnly = False
    ucItemFind.ItemEnquiryType = etFullItems
    ucItemFind.Visible = True
    ItemFilterVisible = True
    frmItemFilter.Visible = False
    ucItemFind.CausesValidation = True
    Call ucItemFind.FillScreen(ucItemFind)
    ItemFilterVisible = False
    GetCorrectSKU = mstrSKU
        
End Function
