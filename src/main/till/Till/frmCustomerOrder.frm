VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Begin VB.Form frmCustomerOrder 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Customer Order Details"
   ClientHeight    =   5115
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5565
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5115
   ScaleWidth      =   5565
   StartUpPosition =   2  'CenterScreen
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   5400
      Top             =   540
      Visible         =   0   'False
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   2100
      TabIndex        =   11
      Top             =   4020
      Width           =   1455
   End
   Begin VB.CommandButton cmdList 
      Caption         =   "F8 - List"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   480
      TabIndex        =   8
      Top             =   4020
      Width           =   1455
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   3720
      TabIndex        =   7
      Top             =   4020
      Width           =   1455
   End
   Begin VB.TextBox txtTranDate 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   2280
      Locked          =   -1  'True
      TabIndex        =   1
      TabStop         =   0   'False
      Text            =   "15/06/2005"
      Top             =   2160
      Width           =   1515
   End
   Begin VB.TextBox txtTranNumb 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   2280
      MaxLength       =   4
      TabIndex        =   3
      Text            =   "0000"
      Top             =   3360
      Width           =   735
   End
   Begin VB.TextBox txtTranTill 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2280
      MaxLength       =   2
      TabIndex        =   2
      Text            =   "00"
      Top             =   2760
      Width           =   435
   End
   Begin VB.TextBox txtOrderNumber 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   2280
      MaxLength       =   6
      TabIndex        =   0
      Text            =   "123456"
      Top             =   780
      Width           =   1035
   End
   Begin VB.OptionButton optEntryMode 
      Caption         =   "Enter Order &Details"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   1
      Left            =   120
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   1500
      Width           =   5355
   End
   Begin VB.OptionButton optEntryMode 
      Caption         =   "Enter Order &Number"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   0
      Left            =   120
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   120
      Value           =   -1  'True
      Width           =   5355
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   12
      Top             =   4740
      Width           =   5565
      _ExtentX        =   9816
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmCustomerOrder.frx":0000
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   635
            MinWidth        =   635
            Picture         =   "frmCustomerOrder.frx":09A6
            Key             =   "NumPad"
            Object.ToolTipText     =   "Show/Hide Num Pad"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "15:51"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblTransactionNo 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Transaction No."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   180
      TabIndex        =   6
      Top             =   3360
      Width           =   1995
   End
   Begin VB.Label lblTill 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Till"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   180
      TabIndex        =   5
      Top             =   2760
      Width           =   360
   End
   Begin VB.Label lblDate 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Date"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   180
      TabIndex        =   4
      Top             =   2160
      Width           =   570
   End
End
Attribute VB_Name = "frmCustomerOrder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public AllowScan    As Boolean

Private mblnGetOrder As Boolean
Private mOrderNumber As String
Private mQuoteNumber As String

'Error handler variables
Private errorNo As Long
Private errorDesc As String

Public Sub InitialiseStatusBar(ByRef sbInfo As StatusBar)
    
    sbInfo.Panels(PANEL_VERNO).Text = "Ver. " & App.Major & "." & App.Minor & "." & App.Revision
    sbInfo.Panels(PANEL_VERNO).ToolTipText = "Program Version Number"
    sbInfo.Panels(PANEL_WSID + 1).Text = goSession.CurrentEnterprise.IEnterprise_WorkstationID & " "
    sbInfo.Panels(PANEL_WSID + 1).ToolTipText = "Current Work-Station Number"
    sbInfo.Panels(PANEL_DATE + 1).Text = Format$(Date, "DD-MMM-YY")
    If (mblnShowKeypadIcon = False) Then sbStatus.Panels("NumPad").Picture = Nothing

End Sub

Public Function GetCustomerOrderNo() As String

    On Error GoTo HandleException

    mblnGetOrder = True
    mOrderNumber = ""
    Me.Show vbModal
    GetCustomerOrderNo = mOrderNumber
    
    Exit Function

HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("GetCustomerOrderNo", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Function

Public Function GetQuote() As String
    
    On Error GoTo HandleException
    
    Me.Caption = "Recall Quote"
    mblnGetOrder = False
    mQuoteNumber = ""
    optEntryMode(0).Caption = Replace(optEntryMode(0).Caption, "Order", "Quote")
    optEntryMode(1).Caption = Replace(optEntryMode(1).Caption, "Order", "Quote")
    mblnGetOrder = False
    Me.Show vbModal
    GetQuote = mQuoteNumber
    
    Exit Function

HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("GetQuote", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)

End Function

Public Property Let OrderNumber(ByVal Value As String)

    optEntryMode(1).Value = True
    
    If Len(Value) < 13 Then Value = String(13 - Len(Value), "0") & Value
    txtTranTill.Text = Left$(Value, 2)
    txtTranNumb.Text = Mid$(Value, 3, 4)
    txtTranDate.Text = Mid$(Value, 7, 2) & "/" & Mid$(Value, 9, 2) & "/" & Mid$(Value, 11, 2)
    'txtOrderNumber.Text = Value
    cmdOK.Value = True
End Property

Private Sub cmdCancel_Click()

    Call Form_KeyPress(vbKeyEscape)

End Sub

Private Sub cmdList_Click()

    AllowScan = False
    Load frmOrderList
    If (mblnGetOrder = True) Then
        mOrderNumber = frmOrderList.GetOrderNo
    Else
        mQuoteNumber = frmOrderList.GetQuoteNo
    End If
    Unload frmOrderList
    AllowScan = True

    If (mOrderNumber <> "") Or (mQuoteNumber <> "") Then Me.Hide
    
End Sub

Private Sub cmdOK_Click()
    
Dim oCustOrdHead As cCustOrderHeader
Dim oQuoteHead   As cQuoteHeader
Dim blnSkipCheck As Boolean
Dim colOrders    As Collection
    
    blnSkipCheck = False
    If (txtOrderNumber.Enabled) Then
        If (mblnGetOrder = True) Then
            Set oCustOrdHead = goDatabase.CreateBusinessObject(CLASSID_CUSTORDERHEADER)
            Call oCustOrdHead.AddLoadFilter(CMP_EQUAL, FID_CUSTORDERHEADER_OrderNumber, txtOrderNumber.Text)
            Call oCustOrdHead.AddLoadField(FID_CUSTORDERHEADER_OrderDate)
            Set colOrders = oCustOrdHead.LoadMatches
            
            If colOrders.Count = 0 Then
                Call MsgBoxEx("Order number is NOT on file", vbOKOnly + vbExclamation, "Customer Order", , , , , RGBMsgBox_WarnColour)
                If (txtOrderNumber.Visible = True) And (txtOrderNumber.Enabled = True) Then txtOrderNumber.SetFocus
            Else
                Set oCustOrdHead = colOrders(1)
                If CDate(oCustOrdHead.OrderDate) <> Date Then
                    Call MsgBoxEx("Order number is NOT for today", vbOKOnly + vbExclamation, "Customer Order", _
                                    , , , , RGBMsgBox_WarnColour)
                    If (txtOrderNumber.Visible = True) And (txtOrderNumber.Enabled = True) Then txtOrderNumber.SetFocus
                Else
                    If VerifyOrder(txtOrderNumber.Text, blnSkipCheck) Then
                        mOrderNumber = txtOrderNumber.Text
                        Me.Hide
                    End If
                End If
            End If
        Else
            'check for quote details
            Set oQuoteHead = goDatabase.CreateBusinessObject(CLASSID_QUOTEHEADER)
            Call oQuoteHead.AddLoadFilter(CMP_EQUAL, FID_QUOTEHEADER_QuoteNumber, txtOrderNumber.Text)
            Set colOrders = oQuoteHead.LoadMatches
            
            If colOrders.Count = 0 Then
                Call MsgBoxEx("Quote number is NOT on file", vbOKOnly + vbExclamation, "Recall Quote", , , , , RGBMsgBox_WarnColour)
                If (txtOrderNumber.Visible = True) And (txtOrderNumber.Enabled = True) Then txtOrderNumber.SetFocus
            Else
                Set oQuoteHead = colOrders(1)
                If (oQuoteHead.IsCancelled = True) Then
                    Call MsgBoxEx("Quote number has been cancelled", vbOKOnly + vbExclamation, "Recall Quote", _
                                    , , , , RGBMsgBox_WarnColour)
                    If (txtOrderNumber.Visible = True) And (txtOrderNumber.Enabled = True) Then txtOrderNumber.SetFocus
                    Exit Sub
                End If
                If (oQuoteHead.ExpiryDate < Date) Then
                    If (MsgBoxEx("Quote has expired. Do you wish to retrieve quote?" & vbNewLine & "Any discounts and/or promotions will be removed and re-calculated when saved.", vbYesNo + vbExclamation, "Recall Quote", _
                                    , , , , RGBMsgBox_WarnColour) <> vbYes) Then
                        If (txtOrderNumber.Visible = True) And (txtOrderNumber.Enabled = True) Then txtOrderNumber.SetFocus
                        Exit Sub
                    Else
                        blnSkipCheck = True
                    End If 'user does not wish to retrieve expired quote
                Else
                    blnSkipCheck = True
                End If
                If (oQuoteHead.IsSold = True) Then
                    Call MsgBoxEx("Quote has already been changed to an Order and Paid For", vbOKOnly + vbExclamation, "Recall Quote", _
                                    , , , , RGBMsgBox_WarnColour)
                    If (txtOrderNumber.Visible = True) And (txtOrderNumber.Enabled = True) Then txtOrderNumber.SetFocus
                    Exit Sub
                Else
                    If VerifyOrder(txtOrderNumber.Text, blnSkipCheck) Then
                        If (mblnGetOrder = True) Then
                            mOrderNumber = txtOrderNumber.Text
                        Else
                            mQuoteNumber = txtOrderNumber.Text
                        End If
                        Me.Hide
                    End If
                End If
            End If
        End If
    Else
        Set oQuoteHead = goDatabase.CreateBusinessObject(CLASSID_QUOTEHEADER)
        Err.Clear
        On Error Resume Next
        Call oQuoteHead.AddLoadFilter(CMP_EQUAL, FID_QUOTEHEADER_TranDate, CDate(txtTranDate.Text))
        If Err.Number > 0 Then
            On Error GoTo 0
            Call MsgBoxEx("Invalid barcode scanned", vbOKOnly + vbExclamation, "Customer Order", , , , , RGBMsgBox_WarnColour)
            optEntryMode(0).Value = True
            If (txtOrderNumber.Visible = True) And (txtOrderNumber.Enabled = True) Then txtOrderNumber.SetFocus
            txtTranDate.Text = Format(Date, "Short Date")
            txtTranTill.Text = vbNullString
            txtTranNumb.Text = vbNullString
            Exit Sub
        End If
        On Error GoTo 0
        Call oQuoteHead.AddLoadFilter(CMP_EQUAL, FID_QUOTEHEADER_TillID, txtTranTill.Text)
        Call oQuoteHead.AddLoadFilter(CMP_EQUAL, FID_QUOTEHEADER_TransactionNumber, txtTranNumb.Text)
        Call oQuoteHead.AddLoadField(FID_QUOTEHEADER_QuoteNumber)
        Call oQuoteHead.LoadMatches
        If LenB(oQuoteHead.QuoteNumber) > 0 Then
            If VerifyOrder(oQuoteHead.QuoteNumber, True) Then
                If (mblnGetOrder = True) Then
                    mOrderNumber = oQuoteHead.QuoteNumber
                Else
                    mQuoteNumber = oQuoteHead.QuoteNumber
                End If
                Me.Hide
            End If
        Else
            Call MsgBoxEx("Order NOT found", vbOKOnly + vbExclamation, "Customer Order", , , , , RGBMsgBox_WarnColour)
            If (txtTranTill.Visible = True) And (txtTranTill.Enabled = True) Then txtTranTill.SetFocus
        End If
    End If

End Sub

Private Function VerifyOrder(strOrderNo As String, blnSkipChecks As Boolean) As Boolean

Dim oTranHeader     As cPOSHeader
Dim oQuoteHeader    As cQuoteHeader
Dim colParked       As Collection
        
    On Error GoTo HandleException
    
    'get Quote Header to extract the pending DLTOTS request against the order
    Set oQuoteHeader = goDatabase.CreateBusinessObject(CLASSID_QUOTEHEADER)
    Call oQuoteHeader.ClearLoadFilter
    Call oQuoteHeader.AddLoadFilter(CMP_EQUAL, FID_QUOTEHEADER_QuoteNumber, strOrderNo)
    Call oQuoteHeader.LoadMatches
    If (oQuoteHeader.QuoteNumber <> strOrderNo) Then
        Call MsgBoxEx("Unable to locate Quote Header for Order - '" & strOrderNo & "'", vbOKOnly + vbInformation, "Invalid Order", , , , , RGBMsgBox_WarnColour)
        Exit Function
    End If
    
    'Check Parked Transactions to see if one exist for this Order
    Set oTranHeader = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    Call oTranHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, Date)
    Call oTranHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranParked, True)
    Call oTranHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_RecoveredFromParked, False)
    Call oTranHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_FromDCOrders, True)
    Call oTranHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_OrderNumber, strOrderNo)
    Set colParked = oTranHeader.IBo_LoadMatches
    If (colParked.Count > 0) Then
        If (oTranHeader.TransactionCode = "SA") Then
            Call MsgBoxEx("Order has an existing Sale already parked." & vbCrLf & "Parked transaction will now be recalled.", vbOKOnly + vbInformation, "Invalid Order Selected", , , , , RGBMsgBox_WarnColour)
        End If
        If (oTranHeader.TransactionCode = "RF") Then
            Call MsgBoxEx("Order has an existing Refund already parked." & vbCrLf & "Parked transaction will now be recalled.", vbOKOnly + vbInformation, "Invalid Order Selected", , , , , RGBMsgBox_WarnColour)
        End If
        Call frmTill.DisplayParkedTran(oTranHeader, True)
        mOrderNumber = strOrderNo
        Me.Hide
        Exit Function
    End If
    
    'Retrieve last action required and ensure not already processed
    Set oTranHeader = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    Call oTranHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, oQuoteHeader.TillID)
    Call oTranHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, oQuoteHeader.TranDate)
    Call oTranHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, oQuoteHeader.TransactionNumber)
    Call oTranHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_Voided, True)
    Call oTranHeader.IBo_AddLoadField(FID_POSHEADER_TranParked)
    Call oTranHeader.IBo_AddLoadField(FID_POSHEADER_RecoveredFromParked)
    Call oTranHeader.IBo_AddLoadField(FID_POSHEADER_TransactionCode)
    Call oTranHeader.IBo_LoadMatches
    If (oTranHeader.TranParked = False) And (oTranHeader.RecoveredFromParked = True) And (blnSkipChecks = False) Then
        If (oTranHeader.TransactionCode = "SA") Then
            Call MsgBoxEx("Order has already been Paid For", vbOKOnly + vbInformation, "Invalid Order Selected", , , , , RGBMsgBox_WarnColour)
            Exit Function
        End If
        If (oTranHeader.TransactionCode = "RF") Then
            Call MsgBoxEx("Pending Refund for Order has already been Processed", vbOKOnly + vbInformation, "Invalid Order Selected", , , , , RGBMsgBox_WarnColour)
            Exit Function
        End If
    End If
    
    VerifyOrder = True
    
    Exit Function

HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("VerifyOrder", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Function

Private Sub sbStatus_PanelClick(ByVal Panel As MSComctlLib.Panel)
    
    Select Case (Panel.Key)
        Case ("NumPad"):
                If ((Panel.Picture Is Nothing) = False) Then
                    ucKeyPad1.Visible = Not ucKeyPad1.Visible
                    If (ucKeyPad1.Visible = True) Then
                        Me.Width = Me.Width + ucKeyPad1.Width + 240
                    Else
                        Me.Width = ucKeyPad1.Left
                    End If
                    Call CentreForm(Me)
                End If
    End Select

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If (KeyCode = vbKeyF8) Then cmdList.Value = True

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

    On Error GoTo HandleException
    
    If KeyAscii = vbKeyEscape Then
        Me.Hide
    ElseIf KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys("{TAB}")
    ElseIf UCase$(Chr$(KeyAscii)) = "N" Then
        KeyAscii = 0
        optEntryMode.Item(0).Value = True
    ElseIf UCase$(Chr$(KeyAscii)) = "D" Then
        KeyAscii = 0
        optEntryMode.Item(1).Value = True
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("Form_KeyPress", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub

Private Sub Form_Load()

    txtOrderNumber.Text = vbNullString
    txtTranDate.Text = vbNullString
    txtTranTill.Text = vbNullString
    txtTranNumb.Text = vbNullString
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    optEntryMode(0).BackColor = Me.BackColor
    optEntryMode(1).BackColor = Me.BackColor
    cmdOK.BackColor = Me.BackColor
    cmdList.BackColor = Me.BackColor
    
    txtTranDate.Text = Format(Date, "Short Date")
    mOrderNumber = vbNullString
    Call ucKeyPad1.ShowNumPad
    Call InitialiseStatusBar(sbStatus)
    AllowScan = True
    
End Sub

Private Sub optEntryMode_Click(Index As Integer)
    
    On Error GoTo HandleException
    
    If Index = 0 Then
        txtTranDate.Enabled = False
        txtTranTill.Enabled = False
        txtTranNumb.Enabled = False
        txtOrderNumber.Enabled = True
        If (txtOrderNumber.Visible = True) And (txtOrderNumber.Enabled = True) Then txtOrderNumber.SetFocus
        AllowScan = True
    ElseIf Index = 1 Then
        txtOrderNumber.Enabled = False
        AllowScan = False
        txtTranDate.Enabled = True
        txtTranTill.Enabled = True
        txtTranNumb.Enabled = True
        If (txtTranTill.Visible = True) And (txtTranTill.Enabled = True) Then txtTranTill.SetFocus
    End If
    
    Exit Sub
    
HandleException:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call ErrorHandler("optEntryMode_Click", Err.Description)
    
    Call Err.Raise(errorNo, Err.Source, errorDesc)
    
End Sub


Private Sub txtOrderNumber_GotFocus()
    txtOrderNumber.SelStart = 0
    txtOrderNumber.SelLength = Len(txtOrderNumber.Text)
End Sub

Private Sub txtOrderNumber_LostFocus()
    txtOrderNumber.Text = Right$("000000" & txtOrderNumber.Text, 6)
End Sub

Private Sub txtTranDate_GotFocus()
    txtTranDate.SelStart = 0
    txtTranDate.SelLength = Len(txtTranDate.Text)
End Sub

Private Sub txtTranNumb_GotFocus()
    txtTranNumb.SelStart = 0
    txtTranNumb.SelLength = Len(txtTranNumb.Text)
End Sub

Private Sub txtTranNumb_LostFocus()
    If LenB(Trim$(txtTranNumb.Text)) <> 0 Then
        txtTranNumb.Text = Right$("0000" & txtTranNumb.Text, 4)
    End If
End Sub

Private Sub txtTranTill_GotFocus()
    txtTranTill.SelStart = 0
    txtTranTill.SelLength = Len(txtTranTill.Text)
End Sub

Private Sub txtTranTill_LostFocus()
    If LenB(Trim$(txtTranTill.Text)) <> 0 Then
        txtTranTill.Text = Right$("00" & txtTranTill.Text, 2)
    End If
End Sub

Private Sub ucKeyPad1_Resize()
    
    ucKeyPad1.Width = ucKeyPad1.Width
    ucKeyPad1.Height = ucKeyPad1.Height

End Sub
