VERSION 5.00
Object = "{D165F572-038D-4ED4-A4A8-EB3E27C39F74}#1.0#0"; "CaptureCustUC.ocx"
Begin VB.Form frmItemPrompts 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Item Confirmation"
   ClientHeight    =   2835
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9735
   Icon            =   "frmItemPrompts.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2835
   ScaleWidth      =   9735
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraAddress 
      Height          =   3795
      Left            =   240
      TabIndex        =   11
      Top             =   2040
      Visible         =   0   'False
      Width           =   9255
      Begin ucCaptureCustomer_Wickes.ucCaptureCust ucccCaptureCust 
         Height          =   3915
         Left            =   -120
         TabIndex        =   12
         Top             =   0
         Width           =   9510
         _ExtentX        =   16775
         _ExtentY        =   8811
      End
   End
   Begin VB.Frame fraMessage 
      Height          =   1515
      Left            =   240
      TabIndex        =   2
      Top             =   180
      Width           =   9255
      Begin VB.Label lblMessage4 
         Alignment       =   2  'Center
         Caption         =   "QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   180
         TabIndex        =   10
         Top             =   1080
         Width           =   8895
      End
      Begin VB.Label lblMessage3 
         Alignment       =   2  'Center
         Caption         =   "QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   180
         TabIndex        =   9
         Top             =   780
         Width           =   8895
      End
      Begin VB.Label lblMessage2 
         Alignment       =   2  'Center
         Caption         =   "QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   180
         TabIndex        =   8
         Top             =   480
         Width           =   8895
      End
      Begin VB.Label lblMessage1 
         Alignment       =   2  'Center
         Caption         =   "QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   180
         TabIndex        =   7
         Top             =   180
         Width           =   8895
      End
   End
   Begin VB.PictureBox pctImage 
      Height          =   1560
      Left            =   350
      ScaleHeight     =   1500
      ScaleWidth      =   9000
      TabIndex        =   1
      Top             =   180
      Width           =   9060
   End
   Begin VB.TextBox txtFocus 
      Height          =   285
      Left            =   540
      Locked          =   -1  'True
      TabIndex        =   0
      Text            =   "Text1"
      Top             =   600
      Width           =   315
   End
   Begin VB.Frame fraCustomBtns 
      BorderStyle     =   0  'None
      Height          =   855
      Left            =   240
      TabIndex        =   13
      Top             =   1740
      Visible         =   0   'False
      Width           =   9255
      Begin VB.CommandButton cmdCustomBtn 
         Caption         =   "Command1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   800
         Index           =   0
         Left            =   0
         TabIndex        =   14
         Top             =   60
         Width           =   1755
      End
   End
   Begin VB.Frame fraButtons 
      BorderStyle     =   0  'None
      Height          =   795
      Left            =   1620
      TabIndex        =   3
      Top             =   1800
      Width           =   6435
      Begin VB.CommandButton cmdOK 
         Caption         =   "OK"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Left            =   2220
         TabIndex        =   6
         Top             =   60
         Width           =   2055
      End
      Begin VB.CommandButton cmdNo 
         Caption         =   "&NO"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Left            =   4380
         TabIndex        =   5
         Top             =   60
         Width           =   2055
      End
      Begin VB.CommandButton cmdYes 
         Caption         =   "&YES"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Left            =   0
         TabIndex        =   4
         Top             =   60
         Width           =   2055
      End
   End
End
Attribute VB_Name = "frmItemPrompts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmItemPrompts
'* Date   : 25/03/06
'* Author : mauricem
'*$Archive: $
'*
'**********************************************************************************************
'* Summary:
'*
'* This form is used to prompt the till through a list of Item Prompts associatated with an item
'*
'**********************************************************************************************
'* $Author: mauricem $ $Date: 25/03/06 08:50 $ $Revision: 0 $
'*
'* Date         Author      Vers.   Comment
'* ========     =========   =====   =========================
'* 25/03/06     mauricem    1.0.0   Initial form completed
'*
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmItemPrompts"

Const PROMPT_TYPE_DISP_PRINT As Long = 0
Const PROMPT_TYPE_DISP As Long = 1
Const PROMPT_TYPE_PRINT As Long = 2
Const PROMPT_TYPE_TRAILER As Long = 3
Const PROMPT_TYPE_FINAL_DISP As Long = 4

Const CAPT_ADDRESS_NONE As String = "0"
Const CAPT_ADDRESS_POSTCODE As String = "1"
Const CAPT_ADDRESS_NAME_PCODE As String = "2"
Const CAPT_ADDRESS_FULL As String = "3"

Const RESP_TYPE_OK As Long = 0
Const RESP_TYPE_YESNO As Long = 1
Const RESP_TYPE_CUSTOM3 As Long = 2
Const RESP_TYPE_CUSTOM As Long = 9

Const YES_PRESSED As String = "Y"
Const NO_PRESSED  As String = "N"
Const OK_PRESSED  As String = "OK"
Const NOT_DONE    As String = ""

Const PROMPT_GRP_AGE As Long = 0
Const PROMPT_GRP_ONCEONLY As Long = 1
Const PROMPT_GRP_RE_OCCUR As Long = 2
Const PROMPT_GRP_VOLUME As Long = 3
Const PROMPT_GRP_QTY As Long = 4
Const PROMPT_GRP_MISSING_SKU As Long = 5


Public strUserId As String

Dim moCurrentPrompt As cGroupPrompts
Dim moPromptBO      As cPrompts
Dim moPriorPromptBO As cPrompts
Dim mcolSKUs        As Collection
Dim mstrMissingSKU  As String
Dim mblnPassed      As Boolean
Dim mblnSkipped     As Boolean
Dim mblnFromQuoteRefund As Boolean

Dim moFSO   As FileSystemObject

Dim mstrAddress As String
Dim mblnSupAuth As Boolean
Dim mblnMgrAuth As Boolean
    
Dim mstrPromptID    As String
Dim mblnAgePrompt   As Boolean
Dim mbytAge         As Byte
Dim mstrAge         As String
Dim mstrCustPrompt  As String

Dim mlngHighestAgePassed As Long
Dim mlngLowestAgeFailed  As Long

Dim blnCaptureAddress  As Boolean
Dim mblnWaitForAddress As Boolean
Dim mblnAddressEsc     As Boolean
Dim mcolValues  As Collection

Dim mstrCurrentGrp  As String
Dim moItemBO        As cInventory
Dim moItemWickesBO  As cInventoryWickes
Dim mdblVolume      As Double   'Added WIX1389 to hold Total Volume for Transaction
Dim mintTotalQty    As Integer  'Added WIX1391 to hold Total Qty for transaction

Dim mblnShowNumPad  As Boolean

Public Function ProcessPrompts(ByRef oItemBO As cInventory, _
                               ByRef oItemWickesBO As cInventoryWickes, _
                               ByRef oItemPromptBO As cGroupPrompts, _
                               ByRef colLinePrints As Collection, _
                               ByRef colTrailerPrints As Collection, _
                               ByRef colValues As Collection, _
                               ByRef lngHighestAgePassed As Long, _
                               ByRef lngLowestAgeFailed As Long, _
                               ByRef colRejects As Collection, _
                               ByVal blnShowNumPad As Boolean, _
                               ByVal dblVolume As Double, _
                               ByVal intQty, _
                               ByVal colSKUs As Collection, _
                               ByVal blnFromQuoteRefund As Boolean) As Boolean
                               
Dim strRejectMsg1   As String
Dim strRejectMsg2   As String
Dim strRejectMsg3   As String
Dim strRejectMsg4   As String
Dim oItemRejectBO   As cSale_Wickes.cSKUReject

    mblnPassed = False
    mblnSkipped = False
    mblnShowNumPad = blnShowNumPad
    mblnFromQuoteRefund = blnFromQuoteRefund
    mstrMissingSKU = ""
    mstrCustPrompt = ""
    
    'set module variables
    Set moItemBO = oItemBO
    Set moItemWickesBO = oItemWickesBO
    mdblVolume = dblVolume
    mintTotalQty = intQty
    Set mcolSKUs = colSKUs
    Set mcolValues = colValues
    
    mlngHighestAgePassed = lngHighestAgePassed
    mlngLowestAgeFailed = lngLowestAgeFailed
    
    'configure form
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    fraButtons.BackColor = Me.BackColor
    
    'Retrieve first prompt
    Set moPromptBO = oItemPromptBO.FirstPrompt
    Set moCurrentPrompt = oItemPromptBO
    
    'Display prompt
    Call DisplayPrompt(moPromptBO)
    
'    Call Me.Show(vbModal)
    If (mblnSkipped = True) Then
        ProcessPrompts = True
        Exit Function
    End If
    
    'Pass out latest ages qualified
    lngHighestAgePassed = mlngHighestAgePassed
    lngLowestAgeFailed = mlngLowestAgeFailed
    
    'Pass out if item passed or failed
    If (mblnPassed = False) Then 'if failed, then notify user each time, prompt occurs
        If ((moPromptBO Is Nothing) = False) Then
            Call MsgBoxEx(moPromptBO.RejectedText1 & vbCrLf & moPromptBO.RejectedText2 & vbCrLf & Mid(moPromptBO.DataItem, 1, 40) & vbCrLf & Mid(moPromptBO.DataGroup, 1, 40), vbOKOnly, "Item Previously Rejected", , , , , RGBMsgBox_WarnColour)
            strRejectMsg1 = moPromptBO.RejectedText1
            strRejectMsg2 = moPromptBO.RejectedText2
            If moPromptBO.PromptGroup = PROMPT_GRP_QTY Then
                strRejectMsg3 = moPromptBO.DataItem
                strRejectMsg4 = moPromptBO.DataGroup
            End If
        Else
            If ((moPriorPromptBO Is Nothing) = False) Then
                Call MsgBoxEx(moPriorPromptBO.RejectedText1 & vbCrLf & moPriorPromptBO.RejectedText2 & vbCrLf & Mid(moPromptBO.DataItem, 1, 40) & vbCrLf & Mid(moPromptBO.DataGroup, 1, 40), vbOKOnly, "Item Previously Rejected", , , , , RGBMsgBox_WarnColour)
                strRejectMsg1 = moPriorPromptBO.RejectedText1
                strRejectMsg2 = moPriorPromptBO.RejectedText2
                If moPromptBO.PromptGroup = PROMPT_GRP_QTY Then
                    strRejectMsg3 = moPromptBO.DataItem
                    strRejectMsg4 = moPromptBO.DataGroup
                End If
            Else
'                If ((moCurrentPrompt Is Nothing) = False) Then
'                    Call MsgBoxEx(moCurrentPrompt.Yes_Goto)
                    '& vbCrLf & moPriorPromptBO.RejectedText2, vbOKOnly, "Item Previously Rejected", , , , , RGBMsgBox_WarnColour)
                    'strRejectMsg1 = moPriorPromptBO.RejectedText1
                    'strRejectMsg2 = moPriorPromptBO.RejectedText2
'                End If
            End If
        End If
        'WIX1218 - log failure if flagged on First Prompt (Prompt Sequence 000001)
        If (colRejects Is Nothing) Then Set colRejects = New Collection
        If (oItemPromptBO.LogReject = True) Then
            Set oItemRejectBO = goDatabase.CreateBusinessObject(CLASSID_TRANREJECT)
            oItemRejectBO.TranDate = moTranHeader.TranDate
            oItemRejectBO.TranNo = moTranHeader.TransactionNo
            oItemRejectBO.TranTillID = moTranHeader.TillID
            oItemRejectBO.SequenceNo = colRejects.Count + 1
            If oItemBO Is Nothing = False Then oItemRejectBO.PartCode = oItemBO.PartCode
            oItemRejectBO.RejectText = strRejectMsg1 & vbCrLf & strRejectMsg2
            oItemRejectBO.GroupID = oItemPromptBO.GroupID
            oItemRejectBO.PromptID = oItemPromptBO.PromptID
            oItemRejectBO.UserID = strUserId
            'Add to collection, ignore error if already added
            On Error Resume Next
            Call colRejects.Add(oItemRejectBO, oItemBO.PartCode & strRejectMsg1 & strRejectMsg2)
            On Error GoTo 0
        End If
    End If
    'WIX1396 - if passed but Custom Prompt selected
    If (mblnPassed = True) And (mstrCustPrompt <> "") Then 'if passed, then notify user each time, prompt occurs
        If (colRejects Is Nothing) Then Set colRejects = New Collection
        If (oItemPromptBO.LogReject = True) Then
            Set oItemRejectBO = goDatabase.CreateBusinessObject(CLASSID_TRANREJECT)
            oItemRejectBO.TranDate = moTranHeader.TranDate
            oItemRejectBO.TranNo = moTranHeader.TransactionNo
            oItemRejectBO.TranTillID = moTranHeader.TillID
            oItemRejectBO.SequenceNo = colRejects.Count + 1
            If oItemBO Is Nothing = False Then oItemRejectBO.PartCode = oItemBO.PartCode
            oItemRejectBO.RejectText = vbCrLf & mstrCustPrompt
            oItemRejectBO.GroupID = oItemPromptBO.GroupID
            oItemRejectBO.PromptID = oItemPromptBO.PromptID
            oItemRejectBO.UserID = strUserId
            'Add to collection, ignore error if already added
            On Error Resume Next
            Call colRejects.Add(oItemRejectBO, oItemBO.PartCode & mstrCustPrompt)
            On Error GoTo 0
        End If
    End If
    
    ProcessPrompts = mblnPassed
    
End Function

Private Sub DisplayPrompt(ByRef oPromptBO As cPrompts)

Dim blnShowImage    As Boolean
Dim blnVolMatched   As Boolean
Dim blnQtyExceeded  As Boolean
Dim strKey      As String
Dim strHotKey   As String
Dim strResult   As String
Dim lngItemNo   As Long

Dim oPromptItems As cItemGrpPrompts


    cmdOK.Visible = False
    cmdOK.Default = False
    cmdYes.Visible = False
    cmdNo.Visible = False
    
    'Perform check to see if Volume Prompt that value is met
    If (oPromptBO.PromptGroup = PROMPT_GRP_VOLUME) Then
        blnVolMatched = False
        If (mdblVolume > 0) Then
            Select Case (oPromptBO.DataCondition)
                Case ("<"): blnVolMatched = (mdblVolume < oPromptBO.PromptValue)
                Case ("<="): blnVolMatched = (mdblVolume <= oPromptBO.PromptValue)
                Case (">"): blnVolMatched = (mdblVolume > oPromptBO.PromptValue)
                Case (">="): blnVolMatched = (mdblVolume >= oPromptBO.PromptValue)
                Case ("<>"): blnVolMatched = (mdblVolume <> oPromptBO.PromptValue)
                Case Else: blnVolMatched = (mdblVolume = oPromptBO.PromptValue)
            End Select
        End If
        If (blnVolMatched = False) Then
            mblnSkipped = True
            Exit Sub
        End If
    End If
    
    'WIX 1391 - Check to see if the prompt is a Type 4 the new Quantity Check for EON Light Bulbs
    If (oPromptBO.PromptGroup = PROMPT_GRP_QTY) Then
        blnQtyExceeded = False
        If mintTotalQty > 0 Then
            Select Case (oPromptBO.DataCondition)
                Case ("<"): blnQtyExceeded = (mintTotalQty < oPromptBO.PromptValue)
                Case ("<="): blnQtyExceeded = (mintTotalQty <= oPromptBO.PromptValue)
                Case (">"): blnQtyExceeded = (mintTotalQty > oPromptBO.PromptValue)
                Case (">="): blnQtyExceeded = (mintTotalQty >= oPromptBO.PromptValue)
                Case ("<>"): blnQtyExceeded = (mintTotalQty <> oPromptBO.PromptValue)
                Case Else: blnQtyExceeded = (mintTotalQty = oPromptBO.PromptValue)
            End Select
        End If
        If blnQtyExceeded = False Then
            mblnSkipped = True
            Exit Sub
        End If
        If blnQtyExceeded = True Then
            mblnPassed = False
            Exit Sub
        End If
    End If
    
    'WIX 1391 - Check to see if the prompt is a Type 5 for Missing SKUs Prompt
    If (oPromptBO.PromptGroup = PROMPT_GRP_MISSING_SKU) Then
        If (moItemBO Is Nothing = False) Then 'Ignore this Prompt until End of Transaction
            mblnSkipped = True
            Exit Sub
        Else
            If (mblnFromQuoteRefund = True) Then
                mblnSkipped = True
                Exit Sub
            End If
            Set oPromptItems = goDatabase.CreateBusinessObject(CLASSID_ITEM_GRPPROMPT)
            Call oPromptItems.IBo_AddLoadFilter(CMP_EQUAL, FID_ITEM_GRPPROMPT_PromptGroupID, oPromptBO.PromptID)
            For lngItemNo = 1 To mcolSKUs.Count Step 1
                Call oPromptItems.IBo_AddLoadFilter(CMP_SELECTLIST, FID_ITEM_GRPPROMPT_PartCode, mcolSKUs(lngItemNo))
            Next lngItemNo
                        
            'if no missing SKU(s) then just skip
            If (oPromptItems.LoadMatches.Count > 0) Then
                mblnSkipped = True
                Exit Sub
            End If
            
            'Check that Date is valid if specified
            If (InStr(oPromptBO.DataCondition, ":") > 0) Then
                Dim CompareToday As Long
                CompareToday = Format(Now, "YYYYMMDD")
                Dim StartDate As Long
                Dim EndDate As Long
                StartDate = 0
                EndDate = 29991231
                If (InStr(oPromptBO.DataCondition, ":") > 0) Then
                    StartDate = Val(Left$(oPromptBO.DataCondition, InStr(oPromptBO.DataCondition, ":") - 1))
                End If
                If (Right$(oPromptBO.DataCondition, 1) <> ":") Then
                    EndDate = Val(Mid$(oPromptBO.DataCondition, InStr(oPromptBO.DataCondition, ":") + 1))
                End If
                If (CompareToday < StartDate) Or (CompareToday > EndDate) Then
                    mblnSkipped = True
                    Exit Sub
                End If
            End If
        
        End If
    End If
    
    blnShowImage = False
    'Check first prompts to determine if Logo required
    If (oPromptBO.ImagePath <> "") Then
        Set moFSO = New FileSystemObject
        If moFSO.FileExists(oPromptBO.ImagePath) = True Then
            pctImage.Picture = LoadPicture(oPromptBO.ImagePath)
            blnShowImage = True
        End If
    End If 'image for prompt
    
    If (blnShowImage = True) Then
        pctImage.Visible = True
        fraMessage.Top = pctImage.Top * 2 + pctImage.Height
        fraButtons.Top = pctImage.Top * 3 + fraMessage.Height + pctImage.Height
        Me.Height = Me.Height + pctImage.Top + pctImage.Height
    Else
        If (fraMessage.Top <> pctImage.Top) Then
            pctImage.Visible = False
            fraMessage.Top = pctImage.Top
            fraButtons.Top = pctImage.Top * 2 + fraMessage.Height
            Me.Height = Me.Height - (pctImage.Top + pctImage.Height)
        End If
    End If
    
    'Show command buttons for available responses
    cmdOK.Caption = "OK"
    cmdYes.Caption = "&Yes"
    cmdNo.Caption = "&No"
    fraCustomBtns.Visible = False
    If (oPromptBO.ResponseType = RESP_TYPE_OK) Then
        cmdOK.Visible = True
        cmdOK.Default = True
    End If
    If (oPromptBO.ResponseType = RESP_TYPE_YESNO) Then
        cmdYes.Visible = True
        cmdNo.Visible = True
    End If
    If (oPromptBO.ResponseType = RESP_TYPE_CUSTOM3) Then
        cmdOK.Visible = True
        cmdYes.Visible = True
        cmdNo.Visible = True
        If (oPromptBO.DataCondition <> "") Then
            Dim strButtonText() As String
            strButtonText = Split(oPromptBO.DataCondition, ",")
            If UBound(strButtonText) >= 0 Then cmdYes.Caption = "F3" & vbCrLf & strButtonText(0)
            If UBound(strButtonText) >= 1 Then cmdOK.Caption = "F5" & vbCrLf & strButtonText(1)
            If UBound(strButtonText) >= 2 Then cmdNo.Caption = "F7" & vbCrLf & strButtonText(2)
        End If
    End If
    If (oPromptBO.ResponseType = RESP_TYPE_CUSTOM) Then
        cmdOK.Visible = False
        cmdYes.Visible = False
        cmdNo.Visible = False
        fraCustomBtns.Visible = True
        cmdCustomBtn(0).Visible = False
        If (oPromptBO.DataCondition <> "") Then
            Dim strButtonTexts() As String
            strButtonText = Split(oPromptBO.DataCondition, ",")
            While cmdCustomBtn.UBound > 0
                Unload cmdCustomBtn(cmdCustomBtn.UBound)
            Wend
            
            For lngItemNo = 0 To UBound(strButtonText) Step 1
                Load cmdCustomBtn(cmdCustomBtn.Count)
                strHotKey = Mid$(strButtonText(lngItemNo), 2)
                
                cmdCustomBtn(cmdCustomBtn.UBound).Caption = Left(strHotKey, InStr(strHotKey, ":") - 1) & vbCrLf & Mid$(strButtonText(lngItemNo), InStr(strButtonText(lngItemNo), ")") + 1)
                cmdCustomBtn(cmdCustomBtn.UBound).Visible = True
                cmdCustomBtn(cmdCustomBtn.UBound).Left = (cmdCustomBtn.UBound - 1) * (cmdCustomBtn(0).Width + 110)
                cmdCustomBtn(cmdCustomBtn.UBound).Tag = strButtonText(lngItemNo)
            Next lngItemNo
        End If
    End If
    
    
    'Display any user messages
    If (oPromptBO.PromptType = PROMPT_TYPE_DISP_PRINT) Or (oPromptBO.PromptType = PROMPT_TYPE_DISP) Or (oPromptBO.PromptType = PROMPT_TYPE_FINAL_DISP) Then
        lblMessage1.Caption = ProcessDataItems(oPromptBO.ScreenText1)
        lblMessage2.Caption = ProcessDataItems(oPromptBO.ScreenText2)
        lblMessage3.Caption = ProcessDataItems(oPromptBO.ScreenText3)
        lblMessage4.Caption = ProcessDataItems(oPromptBO.ScreenText4)
    End If
    
    'Create list of any print messages
    If (oPromptBO.PromptType = PROMPT_TYPE_DISP_PRINT) Or (oPromptBO.PromptType = PROMPT_TYPE_DISP) Then
        lblMessage1.Caption = ProcessDataItems(oPromptBO.ScreenText1)
        lblMessage2.Caption = ProcessDataItems(oPromptBO.ScreenText2)
        lblMessage3.Caption = ProcessDataItems(oPromptBO.ScreenText3)
        lblMessage4.Caption = ProcessDataItems(oPromptBO.ScreenText4)
    End If
    
    'Preserve Prompt infor for user by response buttons
    mstrAddress = oPromptBO.CollectInfo
    mblnSupAuth = oPromptBO.SupervisorAuth
    mblnMgrAuth = oPromptBO.ManagerAuth
    mblnAgePrompt = (oPromptBO.PromptGroup = 0)
    
    'If prompt is an age based entry, get additional values
    mstrPromptID = oPromptBO.PromptID
    mstrAge = oPromptBO.PromptValue
    If (moItemWickesBO Is Nothing = False) Then
        mstrAge = Replace(mstrAge, "{$SM:ISOL}", moItemWickesBO.SolventAge)
        mstrAge = Replace(mstrAge, "{$SM:IOFF}", moItemWickesBO.OffensiveWeaponAge)
    End If
    mbytAge = Val(mstrAge)
    
    If (mbytAge = 0) Then
        mblnAgePrompt = False
        Call DebugMsg(MODULE_NAME, "DisplayPrompt", endlDebug, "***** WARNING ***** : Prompt-" & mstrPromptID & " has been set as a AGE Prompt but has not AGE Value - flipped to normal prompt")
    End If
    
    Call CentreForm(Me)
           
    'Check if prompt has already been completed, so do not prompt again
    'This only applies to Age Checks -all other prompts must be re-done
    strResult = NOT_DONE
    strKey = "P" & moPromptBO.PromptID & mstrAge

    On Error Resume Next
    If (oPromptBO.PromptGroup = 0) Or (oPromptBO.PromptGroup = 1) Then strResult = mcolValues(strKey)
    Call Err.Clear
    On Error GoTo 0

    'mimic previous keypressed
    Select Case (strResult)
        Case (YES_PRESSED): Call cmdYes_Click
        Case (NO_PRESSED):  Call cmdNo_Click
        Case (OK_PRESSED):  Call cmdOK_Click
        Case (NOT_DONE):
                            If (mblnAgePrompt = True) Then 'for age prompts - check if age limits already met
                                If (mbytAge >= mlngLowestAgeFailed) And (mlngLowestAgeFailed > 0) Then
                                    Call cmdNo_Click
                                    Exit Sub
                                End If
                                If (mbytAge <= mlngHighestAgePassed) And (mlngHighestAgePassed > 0) Then
                                    Call cmdYes_Click
                                    Exit Sub
                                End If
                            End If
                            If (Me.Visible = False) Then Call Me.Show(vbModal)
    End Select

End Sub 'DisplayPrompt

Private Function ProcessDataItems(strSource As String) As String

Dim strOutput As String
Dim strDateRange As String
Dim strDateValue As String

    strOutput = strSource
    If (moItemBO Is Nothing = False) Then
        strOutput = Replace(strOutput, "{$SKU}", moItemBO.PartCode)
        strOutput = Replace(strOutput, "{$DESCR}", moItemBO.Description)
    End If
    If (moItemWickesBO Is Nothing = False) Then
        strOutput = Replace(strOutput, "{$ISOL}", moItemWickesBO.SolventAge)
        strOutput = Replace(strOutput, "{$IOFF}", moItemWickesBO.OffensiveWeaponAge)
    End If
    If (InStr(strOutput, "{$DATE") > 0) Then
        strDateRange = Mid$(strOutput, InStr(strOutput, "{$DATE") + 6)
        strDateValue = Left$(strDateRange, InStr(strDateRange, "}"))
        strDateRange = Left$(strDateRange, InStr(strDateRange, "}") - 1)
        strDateRange = Format(DateAdd("d", 1, DateAdd("yyyy", Val(strDateRange), Now)), "DD/MM/YY")
        strOutput = Replace(strOutput, "{$DATE" & strDateValue, strDateRange)
    End If
    
    ProcessDataItems = strOutput

End Function 'ProcessDataItems

Private Sub cmdCustomBtn_Click(Index As Integer)

Dim strKeyCode As String

    strKeyCode = Mid$(cmdCustomBtn(Index).Tag, InStr(cmdCustomBtn(Index).Tag, ":") + 1, 1)
    mstrCustPrompt = Mid$(cmdCustomBtn(Index).Caption, InStr(cmdCustomBtn(Index).Caption, vbCrLf) + 2)
    Select Case (strKeyCode)
        Case ("Y"):
                cmdYes.Value = True
                Exit Sub
        Case ("O"):
                cmdOK.Value = True
                Exit Sub
        Case ("N"):
                cmdNo.Value = True
                Exit Sub
    End Select

End Sub

Private Sub cmdNo_Click()

Dim blnVerifyCancelled As Boolean
Dim strPromptKey    As String
Dim oPromptNo      As cPrompts

    strPromptKey = "P" & moPromptBO.PromptID & mstrAge
    Set moPriorPromptBO = moPromptBO
    Set oPromptNo = moPromptBO
    If (moCurrentPrompt.Next_No_Prompt(moPromptBO) = False) Then
        If (mblnAgePrompt = True) And ((mbytAge < mlngLowestAgeFailed) Or (mlngLowestAgeFailed = 0)) Then mlngLowestAgeFailed = mbytAge
    
        'Preserve key response for next time prompt is displayed
        On Error Resume Next
        Call mcolValues.Add(NO_PRESSED, strPromptKey)
        Call Err.Clear
        On Error GoTo 0
        'rollback move forward to extract Reject Message
        Set moPromptBO = oPromptNo
        Call Me.Hide
        Exit Sub
    End If
    
    'For response if completed prompts or moving to next, check if Auth required
    If ((mblnSupAuth = True) Or (mblnMgrAuth = True)) And (Me.Visible = True) Then
        blnVerifyCancelled = False
        If (VerifyPassword(blnVerifyCancelled) = False) Then
            If (blnVerifyCancelled = False) Then
                'Preserve key response for next time prompt is displayed
                On Error Resume Next
                Call mcolValues.Add(NO_PRESSED, strPromptKey)
                Call Err.Clear
                On Error GoTo 0
                Call Me.Hide
            End If
            Exit Sub
        End If
    End If 'Authorisation Required
    
    'Preserve key response for next time prompt is displayed
    On Error Resume Next
    Call mcolValues.Add(NO_PRESSED, strPromptKey)
    Call Err.Clear
    On Error GoTo 0

    If ((moPromptBO Is Nothing) = True) Then 'Completed OK
        mblnPassed = True
        Call Me.Hide
        Exit Sub
    End If
    Call DisplayPrompt(moPromptBO)

End Sub

Private Sub cmdOK_Click()

Dim oPromptOK   As cPrompts
Dim blnVerifyCancelled As Boolean

    'Preserve key response for next time prompt is displayed
    On Error Resume Next
    Call mcolValues.Add(OK_PRESSED, "P" & moPromptBO.PromptID & mstrAge)
    Call Err.Clear
    On Error GoTo 0
    
    Set oPromptOK = moPromptBO
    If (moCurrentPrompt.Next_OK_Prompt(moPromptBO) = False) Then
        'rollback move forward to extract Reject Message - specific to OK responses only
        Set moPromptBO = oPromptOK
        Call Me.Hide
        Exit Sub
    End If
    
    'For response if completed propmts or moving to next, check if Auth required
    If ((mblnSupAuth = True) Or (mblnMgrAuth = True)) And (Me.Visible = True) Then
        If (VerifyPassword(blnVerifyCancelled) = False) Then
            Call Me.Hide
            Exit Sub
        End If
    End If 'Authorisation Required
    
    If (mstrAddress <> CAPT_ADDRESS_NONE) Then Call CaptureAddress
    
    If ((moPromptBO Is Nothing) = True) Then
        mblnPassed = True
        Call Me.Hide
        Exit Sub
    End If
    Call DisplayPrompt(moPromptBO)

End Sub

Private Sub cmdYes_Click()

Dim strPromptID As String
Dim blnVerifyCancelled As Boolean
Dim oPromptYes  As cPrompts

    'For response if completed prompts or moving to next, check if Auth required
    If ((mblnSupAuth = True) Or (mblnMgrAuth = True)) And (Me.Visible = True) Then
        blnVerifyCancelled = False
        If (VerifyPassword(blnVerifyCancelled) = False) Then
            If (blnVerifyCancelled = False) Then
                On Error Resume Next
                Call mcolValues.Add(NO_PRESSED, strPromptID & mstrAge)
                Call Err.Clear
                On Error GoTo 0
                Call Me.Hide
            End If
            Exit Sub
        End If
    End If 'Authorisation Required
    
    strPromptID = "P" & moPromptBO.PromptID
    
    Set oPromptYes = moPromptBO
    If (moCurrentPrompt.Next_Yes_Prompt(moPromptBO) = False) Then
        'Preserve key response for next time prompt is displayed
        On Error Resume Next
        Call mcolValues.Add(YES_PRESSED, strPromptID & mstrAge)
        Call Err.Clear
        On Error GoTo 0
        'rollback move forward to extract Reject Message
        Set moPromptBO = oPromptYes
        Call Me.Hide
        Exit Sub
    End If
    If (moPromptBO Is Nothing = False) Then
        If (moCurrentPrompt.Yes_Goto = "ADDSKU") Then
            Dim lngCurrentMode As Long
            Dim lngEntryMode As Long
            Dim oPromptItems As cItemGrpPrompts

            Set oPromptItems = goDatabase.CreateBusinessObject(CLASSID_ITEM_GRPPROMPT)
            Call oPromptItems.IBo_AddLoadFilter(CMP_EQUAL, FID_ITEM_GRPPROMPT_PromptGroupID, moCurrentPrompt.GroupID)
            Call oPromptItems.IBo_AddLoadFilter(CMP_GREATERTHAN, FID_ITEM_GRPPROMPT_PartCode, "000000")
            Call oPromptItems.LoadMatches
                            
            lngCurrentMode = frmTill.mlngCurrentMode
            lngEntryMode = frmTill.mlngEntryMode
            frmTill.mlngCurrentMode = encmRecord
            frmTill.mlngEntryMode = enemSKU
            frmTill.txtSKU.Text = oPromptItems.PartCode
            Call frmTill.txtSKU_KeyPress(vbKeyReturn)
            frmTill.mlngCurrentMode = lngCurrentMode
            frmTill.mlngEntryMode = lngEntryMode
            Call Me.Hide
            Exit Sub
        End If
    End If
        
    'Preserve key response for next time prompt is displayed
    On Error Resume Next
    Call mcolValues.Add(YES_PRESSED, strPromptID & mstrAge)
    Call Err.Clear
    On Error GoTo 0
    
    If ((moPromptBO Is Nothing) = True) Then
        If (mblnAgePrompt = True) And (mbytAge > mlngHighestAgePassed) Then mlngHighestAgePassed = mbytAge
        mblnPassed = True
        Call Me.Hide
        Exit Sub
    End If
    Call DisplayPrompt(moPromptBO)

End Sub

Private Function VerifyPassword(ByRef blnCancelled As Boolean)

    Load frmVerifyPwd
    blnCancelled = False
        
    If (mblnSupAuth = True) And (mblnMgrAuth = False) Then
        If (frmVerifyPwd.VerifySupervisorPassword(mblnShowNumPad) = True) Then
            moTranHeader.SupervisorUsed = True
            VerifyPassword = True
        Else 'invalid authorisation code, or escape pressed
            blnCancelled = frmVerifyPwd.Cancelled
            Unload frmVerifyPwd
            Exit Function
        End If
    End If ' Supervisor required
    If (mblnMgrAuth = True) Then
        If (frmVerifyPwd.VerifyManagerPassword(mblnShowNumPad) = True) Then
            moTranHeader.SupervisorUsed = True
            VerifyPassword = True
        Else 'invalid authorisation code, or escape pressed
            blnCancelled = frmVerifyPwd.Cancelled
            Unload frmVerifyPwd
            Exit Function
        End If
    End If ' Supervisor required
    Unload frmVerifyPwd
    VerifyPassword = True

End Function

Private Sub CaptureAddress()

Dim strName     As String
Dim strPostCode As String
Dim strAddress  As String
Dim strTelNo    As String
    
    mblnAddressEsc = False
    If (Me.Visible = False) Then Exit Sub
    Select Case (mstrAddress)
        Case (CAPT_ADDRESS_POSTCODE):
            If (mstrPostCode = "") Then
                Load frmPostcode
                With frmPostcode
                    'If (ucKeyPad.true) Then .ShowNumPad
                    .PostCode = vbNullString
                    .Show vbModal
                    If (.Cancel = False) Then
                        mstrPostCode = .PostCode
                    End If
                End With
            End If
        Case (CAPT_ADDRESS_NAME_PCODE), (CAPT_ADDRESS_FULL):
            fraAddress.Visible = True
            fraButtons.Visible = False
            Me.Height = Me.Height + ucccCaptureCust.Height
            Call ucccCaptureCust.Initialise(goSession, Me)
            Call ucccCaptureCust.Reset
            ucccCaptureCust.NameOnly = True
            Call ucccCaptureCust.ShowRefundDetails(vbNullString, Date, vbNullString, vbNullString, False, True)
            mblnWaitForAddress = True
            Call CentreForm(Me)
            DoEvents
            If (mstrAddress = CAPT_ADDRESS_NAME_PCODE) Then
                Call ucccCaptureCust.CaptureWarrantyAddress(strName, strPostCode, strAddress, strTelNo)
            Else
                Call ucccCaptureCust.CaptureAddress(strName, strPostCode, strAddress, strTelNo)
            End If
'            While mblnWaitForAddress = True
'                DoEvents
'            Wend
'            If (Cancel = False) Then
'                strName = mstrName
'                strPostCode = mstrPostCode
'                strAddress = mstrAddress
'                strTelNo = mstrTelNo
'            End If
    End Select

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

Dim strHotKey As String
Dim strKeyCode As String
Dim lngItemNo As Long
    
    If (Shift = 0) Then
        If (cmdYes.Visible = True) And (cmdOK.Visible = True) And (cmdNo.Visible = True) Then
            Select Case (KeyCode)
                Case (vbKeyF3):
                        cmdYes.Value = True
                        KeyCode = 0
                        Exit Sub
                Case (vbKeyF5):
                        cmdOK.Value = True
                        KeyCode = 0
                        Exit Sub
                Case (vbKeyF7):
                        cmdNo.Value = True
                        KeyCode = 0
                        Exit Sub
            End Select
        Else
            If (fraCustomBtns.Visible = True) And (KeyCode >= vbKeyF1) And (KeyCode <= vbKeyF12) Then
                strHotKey = "(F" & ((KeyCode - vbKeyF1) + 1) & ":"
                For lngItemNo = 0 To cmdCustomBtn.UBound Step 1
                    If InStr(cmdCustomBtn(lngItemNo).Tag, strHotKey) > 0 Then
                        strKeyCode = Mid$(cmdCustomBtn(lngItemNo).Tag, InStr(cmdCustomBtn(lngItemNo).Tag, ":") + 1, 1)
                        mstrCustPrompt = Mid$(cmdCustomBtn(lngItemNo).Caption, InStr(cmdCustomBtn(lngItemNo).Caption, vbCrLf) + 2)
                        Select Case (strKeyCode)
                            Case ("Y"):
                                    cmdYes.Value = True
                                    KeyCode = 0
                                    Exit Sub
                            Case ("O"):
                                    cmdOK.Value = True
                                    KeyCode = 0
                                    Exit Sub
                            Case ("N"):
                                    cmdNo.Value = True
                                    KeyCode = 0
                                    Exit Sub
                        End Select
                        Exit For
                    End If
                Next lngItemNo
            End If
        End If
    End If

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

Dim strKey As String
    
    strKey = UCase(Chr(KeyAscii))
    If (strKey = "Y") Or (strKey = "N") And (fraButtons.Visible = True) Then
        If (strKey = "Y") And (cmdYes.Visible = True) Then cmdYes.Value = True
        If (strKey = "N") And (cmdNo.Visible = True) Then cmdNo.Value = True
    End If

End Sub

Private Sub ucccCaptureCust_Cancel()

    mblnWaitForAddress = False
    mblnAddressEsc = True
    
End Sub

Private Sub ucccCaptureCust_Resize()

    fraAddress.Height = ucccCaptureCust.Height

End Sub

Private Sub ucccCaptureCust_Save(Name As String, PostCode As String, Address As String, TelNo As String, MobileNo As String, WorkTelNo As String, Email As String)
    mblnWaitForAddress = False
End Sub
