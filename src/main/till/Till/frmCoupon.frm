VERSION 5.00
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Begin VB.Form frmCoupon 
   BackColor       =   &H0080FFFF&
   Caption         =   "Coupon Details"
   ClientHeight    =   2100
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4755
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   2100
   ScaleWidth      =   4755
   StartUpPosition =   2  'CenterScreen
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   0
      Top             =   2040
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin VB.Frame fraCoupon 
      BackColor       =   &H0080FFFF&
      BorderStyle     =   0  'None
      Height          =   1995
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   4635
      Begin VB.TextBox txtCouponNo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   2160
         MaxLength       =   6
         TabIndex        =   2
         Top             =   0
         Width           =   1275
      End
      Begin VB.TextBox txtPostCode 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   2160
         MaxLength       =   8
         TabIndex        =   4
         Top             =   660
         Width           =   2415
      End
      Begin VB.CommandButton cmdCancel 
         BackColor       =   &H0080FFFF&
         Caption         =   "Cancel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   0
         TabIndex        =   5
         Top             =   1320
         Width           =   1695
      End
      Begin VB.CommandButton cmdContinue 
         BackColor       =   &H0080FFFF&
         Caption         =   "Continue"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   2880
         TabIndex        =   6
         Top             =   1320
         Width           =   1695
      End
      Begin VB.Label lblCouponNo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Coupon No."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   0
         TabIndex        =   1
         Top             =   60
         Width           =   2100
      End
      Begin VB.Label lblPostCode 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Post Code"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   0
         TabIndex        =   3
         Top             =   720
         Width           =   1845
      End
   End
End
Attribute VB_Name = "frmCoupon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Cancel As Boolean

Public Event Duress()
Private mintDuressKeyCode As Integer

Private Sub cmdCancel_Click()

    If (cmdCancel.Visible = True) And (cmdCancel.Enabled = True) Then cmdCancel.SetFocus
    Cancel = True
    Call Me.Hide

End Sub

Private Sub cmdContinue_Click()
    
    If (cmdContinue.Visible = True) And (cmdContinue.Enabled = True) Then cmdContinue.SetFocus
    DoEvents
    If txtCouponNo.Text = "000000" Then
        Call MsgBoxEx("Invalid Coupon Number", , , , , , , RGBMsgBox_WarnColour)
        If (txtCouponNo.Visible = True) And (txtCouponNo.Enabled = True) Then txtCouponNo.SetFocus
        Exit Sub
    End If
    
    If (goSession.GetParameter(PRM_COUNTRY_CODE) <> "IE") Then
        If Not PostCodeIsOK(txtPostCode.Text) Then
            Call MsgBoxEx("Invalid Postcode Format", , , , , , , RGBMsgBox_WarnColour)
            If (txtPostCode.Visible = True) And (txtPostCode.Enabled = True) Then txtPostCode.SetFocus
            Exit Sub
        End If
    End If

    Cancel = False
    Me.Hide

End Sub

Private Sub Form_Activate()
    If (txtCouponNo.Visible = True) And (txtCouponNo.Enabled = True) Then txtCouponNo.SetFocus
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If mintDuressKeyCode <> 0 And KeyCode = mintDuressKeyCode Then
        KeyCode = 0
        RaiseEvent Duress
    End If

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        If Left$(ActiveControl.Name, 3) = "txt" Then
            Call SendKeys("{Tab}")
        End If
    ElseIf KeyAscii = vbKeyEscape Then
        cmdCancel.Value = True
    End If
    
End Sub

Private Sub Form_Load()
    
    mintDuressKeyCode = goSession.GetParameter(PRM_KEY_DURESS)
    If (goSession.GetParameter(PRM_COUNTRY_CODE) = "IE") Then
        lblPostCode.Visible = False
        txtPostCode.Visible = False
    End If

End Sub

Public Sub GetCouponInfo(ByRef blnShowKeyPad As Boolean, ByRef strCouponNo As String, ByRef strPostCode As String)

'    If LenB(strPostCode) <> 0 Then
'        txtPostCode.Text = strPostCode
'    End If
    txtPostCode.Text = vbNullString
    txtCouponNo.Text = vbNullString
    
    If (blnShowKeyPad = False) Then
        ucKeyPad1.Visible = False
    Else
        ucKeyPad1.Visible = True
        Me.Height = fraCoupon.Height + ucKeyPad1.Height + 540
        Me.Width = ucKeyPad1.Width + 120
        fraCoupon.Left = (Me.Width - fraCoupon.Width) / 2
    End If
        
    
    Me.Show vbModal
    If Not Cancel Then
        strCouponNo = txtCouponNo.Text
        strPostCode = txtPostCode.Text
    End If

End Sub

Private Sub txtCouponNo_GotFocus()
    
    txtCouponNo.SelStart = 0
    txtCouponNo.SelLength = Len(txtCouponNo.Text)
    txtCouponNo.BackColor = RGBEdit_Colour

End Sub

Private Sub txtCouponNo_KeyPress(KeyAscii As Integer)

    If (KeyAscii > 32) And (Not Chr$(KeyAscii) Like "[0-9]") Then
        KeyAscii = 0
    End If

End Sub

Private Sub txtCouponNo_LostFocus()
    
    txtCouponNo.Text = Right$("000000" & txtCouponNo.Text, 6)
    txtCouponNo.BackColor = RGB_WHITE

End Sub

Private Sub txtPostcode_GotFocus()

    txtPostCode.SelStart = 0
    txtPostCode.SelLength = Len(txtPostCode.Text)
    txtPostCode.BackColor = RGBEdit_Colour

End Sub

Private Sub txtPostcode_KeyPress(KeyAscii As Integer)
    
    If Chr$(KeyAscii) Like "[a-z]" Then KeyAscii = Asc(UCase$(Chr$(KeyAscii)))
    
    If (Not Chr$(KeyAscii) Like "[0-9A-Z ]") And (KeyAscii >= 32) Then KeyAscii = 0

End Sub

Private Sub txtPostcode_LostFocus()
    
    txtPostCode.BackColor = RGB_WHITE

End Sub

Private Function PostCodeIsOK(ByVal strCode As String, Optional ByVal blnAllowPartial As Boolean = False) As Boolean

    PostCodeIsOK = False
    
    strCode = Trim$(strCode)
    
    If strCode Like "[A-Z][1-9A-Z]*[0-9 ][0-9][A-Z][A-Z]" Then
        PostCodeIsOK = True
        Exit Function
    End If
    
    If Not blnAllowPartial Then Exit Function

' Check for valid Partial Postcode
    

End Function


