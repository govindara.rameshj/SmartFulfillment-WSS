VERSION 5.00
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Object = "{9BA18739-054D-4172-8E42-118133CE2FC4}#1.0#0"; "EditDateCtl.ocx"
Begin VB.Form frmEntry 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   3525
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5745
   ControlBox      =   0   'False
   Icon            =   "frmEntry.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3525
   ScaleWidth      =   5745
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdItemFilter 
      Caption         =   "F8-SKU Enquiry"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   2040
      TabIndex        =   5
      Top             =   2820
      Visible         =   0   'False
      Width           =   3015
   End
   Begin VB.CommandButton cmdProceed 
      Caption         =   "OK"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      HelpContextID   =   555
      Left            =   660
      TabIndex        =   2
      Top             =   2220
      Width           =   1935
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      HelpContextID   =   555
      Left            =   3120
      TabIndex        =   3
      Top             =   2220
      Width           =   1935
   End
   Begin VB.TextBox txtEntry 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   900
      MaxLength       =   9
      TabIndex        =   1
      Top             =   1560
      Width           =   3915
   End
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   2460
      Top             =   3420
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin ucEditDate.ucDateText ucdtEntry 
      Height          =   555
      Left            =   900
      TabIndex        =   4
      Top             =   1560
      Width           =   3915
      _ExtentX        =   6906
      _ExtentY        =   979
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DateFormat      =   "DD/MM/YY"
      Text            =   ""
   End
   Begin VB.Label lblMessage 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1275
      Left            =   60
      TabIndex        =   0
      Top             =   180
      Width           =   5595
   End
End
Attribute VB_Name = "frmEntry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim mblnShowKeyPad  As Boolean
Dim mstrEntry      As String
Dim mblnSKU        As Boolean
Dim mblnOlderDate  As Boolean

Public Function GetEntry(Message As String, Title As String, BackColour As Long, lngLength As Long, blnSKU As Boolean) As String

    mstrEntry = ""
    Me.BackColor = BackColour
    lblMessage.Caption = Message
    txtEntry.MaxLength = lngLength
    ucdtEntry.Visible = False
    mblnSKU = blnSKU
    If (blnSKU = True) Then cmdItemFilter.Visible = blnSKU
    Me.Caption = Title
    Call Me.Show(vbModal)
    GetEntry = mstrEntry

End Function

Public Function GetDateEntry(Message As String, Title As String, BackColour As Long, blnOlderDate As Boolean) As Date

    ucdtEntry.Text = DisplayDate(Now, False)
    txtEntry.Visible = False
    Me.BackColor = BackColour
    lblMessage.Caption = Message
    ucdtEntry.Visible = True
    mblnOlderDate = blnOlderDate
    Me.Caption = Title
    Call Me.Show(vbModal)
    GetDateEntry = GetDate(ucdtEntry.Text)

End Function

Private Sub cmdCancel_Click()

    mstrEntry = ""
    Me.Hide
    
End Sub

Private Sub cmdItemFilter_Click()

Dim strSKU As String

    strSKU = frmItemFilter.GetCorrectSKU
    
    If (strSKU <> "") Then
        txtEntry.Text = strSKU
        cmdProceed.Value = True
    End If

End Sub

Private Sub cmdProceed_Click()
                
    mstrEntry = txtEntry.Text
    If (mblnSKU = True) Then mstrEntry = Right("000000" & Trim$(txtEntry.Text), 6)
    Me.Hide

End Sub

Private Sub Form_Activate()
    
    ucKeyPad1.ShowNumPad
    Call Form_Resize

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

Dim lngKeyIn As Integer
Dim lngRowNo As Integer
    
    Call DebugMsg(vbNullString, vbNullString, endlDebug, "KDn" & KeyCode & "/" & Shift)
    If (Shift = 4) And (KeyCode = vbKeyF4) Then KeyCode = 0 'disbale the alt-f4 (close window)
    
    If (cmdItemFilter.Visible = True) And (KeyCode = vbKeyF8) Then
        cmdItemFilter.Value = True
        KeyCode = 0
    End If

End Sub

Private Sub Form_Load()
                
    ucKeyPad1.Visible = False
    Me.BackColor = RGBQuery_BackColour
    Call CentreForm(Me)

End Sub

Public Sub ShowNumPad()
    
    ucKeyPad1.Visible = True
    mblnShowKeyPad = True
    ucKeyPad1.ShowNumPad

End Sub

Private Sub Form_Resize()

    If (ucKeyPad1.Visible = True) Or ((Me.Visible = False) And (mblnShowKeyPad = True)) Then
        Me.Height = ucKeyPad1.Top + ucKeyPad1.Height + 640
    Else
        Me.Height = 3900
    End If
    ucKeyPad1.Left = (Me.Width - ucKeyPad1.Width) / 2
    Call CentreForm(Me)

End Sub

Private Sub txtEntry_KeyPress(KeyAscii As Integer)

    If (Len(txtEntry.Text) > 0) And (KeyAscii = vbKeyReturn) Then cmdProceed.Value = True

End Sub

Private Sub ucKeyPad1_Resize()
    
    ucKeyPad1.Width = ucKeyPad1.Width

End Sub

