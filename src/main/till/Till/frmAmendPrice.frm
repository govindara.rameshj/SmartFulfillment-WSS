VERSION 5.00
Object = "{9A526B11-3010-11D5-99E4-000102897E9C}#2.2#0"; "NumTextControl.ocx"
Begin VB.Form frmAmendPrice 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Perform Item Price Amendment"
   ClientHeight    =   3975
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9435
   Icon            =   "frmAmendPrice.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3975
   ScaleWidth      =   9435
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtReason 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   2220
      MultiLine       =   -1  'True
      TabIndex        =   8
      Top             =   1740
      Width           =   6855
   End
   Begin VB.CommandButton cmdAccept 
      Caption         =   "Accept"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3840
      TabIndex        =   10
      Top             =   3240
      Width           =   1515
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5640
      TabIndex        =   11
      Top             =   3240
      Width           =   1635
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2220
      TabIndex        =   9
      Top             =   3240
      Width           =   1335
   End
   Begin NumTextControl.NumText ntxtNewPrice 
      Height          =   435
      Left            =   2220
      TabIndex        =   6
      Top             =   1200
      Width           =   1395
      _ExtentX        =   2461
      _ExtentY        =   767
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxLength       =   12
      SelStart        =   4
      Text            =   "0.00"
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Reason"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   7
      Top             =   1680
      Width           =   1935
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Override to"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   240
      TabIndex        =   5
      Top             =   1200
      Width           =   1575
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "Item"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   1455
   End
   Begin VB.Label lblSKU 
      Appearance      =   0  'Flat
      BackColor       =   &H0080FFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "000000"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1080
      TabIndex        =   1
      Top             =   240
      Width           =   1095
   End
   Begin VB.Label lblDescr 
      Appearance      =   0  'Flat
      BackColor       =   &H0080FFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   2220
      TabIndex        =   2
      Top             =   240
      Width           =   6855
   End
   Begin VB.Label lblCurrentPrice 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H0080FFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   2220
      TabIndex        =   4
      Top             =   720
      Width           =   1395
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Current Price"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   3
      Top             =   720
      Width           =   1935
   End
End
Attribute VB_Name = "frmAmendPrice"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public NewPrice  As Currency
Public Reason As String

Dim mlngResetKeyCode As Long
Dim mlngCloseKeyCode As Long
Dim mlngUseKeyCode As Long

Public Function ConfirmAmendPrice(ItemSKU As String, ItemDesc As String, CurrentPrice As Currency) As Boolean

    lblSKU.Caption = ItemSKU
    lblDescr.Caption = ItemDesc
    lblCurrentPrice.Caption = Format(CurrentPrice, "0.00")
    
    ntxtNewPrice.Value = CurrentPrice
    txtReason.Text = ""
    
    NewPrice = 0
    Reason = ""
    
    Call Me.Show(vbModal)

    NewPrice = ntxtNewPrice.Value
    Reason = txtReason.Text
    
    If (Reason <> "") Then
        ConfirmAmendPrice = True
    Else
        ConfirmAmendPrice = False
    End If

End Function
Private Sub cmdAccept_Click()

    If (ntxtNewPrice.Value = Val(lblCurrentPrice.Caption)) Then
        Call MsgBoxEx("New price is the same as Current Price." & vbCrLf & "Unable to Amend Price", vbOKOnly, "Accept Price Amendment", , , , , RGBMsgBox_WarnColour)
        If (ntxtNewPrice.Visible = True) And (ntxtNewPrice.Enabled = True) Then Call ntxtNewPrice.SetFocus
        Exit Sub
    End If
    If (ntxtNewPrice.Value > Val(lblCurrentPrice.Caption)) Then
        Call MsgBoxEx("New price cannot exceed Current Price." & vbCrLf & "Unable to Amend Price", vbOKOnly, "Accept Price Amendment", , , , , RGBMsgBox_WarnColour)
        If (ntxtNewPrice.Visible = True) And (ntxtNewPrice.Enabled = True) Then Call ntxtNewPrice.SetFocus
        Exit Sub
    End If
    If (ntxtNewPrice.Value = 0) Then
        Call MsgBoxEx("New price cannot be zero." & vbCrLf & "Unable to Amend Price", vbOKOnly, "Accept Price Amendment", , , , , RGBMsgBox_WarnColour)
        If (ntxtNewPrice.Visible = True) And (ntxtNewPrice.Enabled = True) Then Call ntxtNewPrice.SetFocus
        Exit Sub
    End If
    If (txtReason.Text = "") Then
        Call MsgBoxEx("No Reason/Description entered for price amendment." & vbCrLf & "Unable to Amend Price", vbOKOnly, "Accept Price Amendment", , , , , RGBMsgBox_WarnColour)
        If (txtReason.Visible = True) And (txtReason.Enabled = True) Then Call txtReason.SetFocus
        Exit Sub
    End If
    Me.Hide

End Sub

Private Sub cmdCancel_Click()
    
    NewPrice = 0
    Reason = ""
    Me.Hide

End Sub

Private Sub cmdReset_Click()

    ntxtNewPrice.Value = Val(lblCurrentPrice.Caption)
    txtReason.Text = ""
    If (ntxtNewPrice.Visible = True) And (ntxtNewPrice.Enabled = True) Then ntxtNewPrice.SetFocus

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    Select Case (KeyCode)
        Case (mlngResetKeyCode):
            If cmdReset.Visible Then Call cmdReset_Click
        Case (mlngCloseKeyCode):
            If cmdCancel.Visible Then Call cmdCancel_Click
        Case (mlngUseKeyCode):
            If cmdAccept.Visible Then Call cmdAccept_Click
    End Select

End Sub

Private Sub Form_Load()

Dim strKey As String

    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    strKey = goSession.GetParameter(PRM_KEY_RESET)
    cmdReset.Caption = strKey & "-" & cmdReset.Caption
    If Left$(strKey, 1) = "F" Then
        mlngResetKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngResetKeyCode = AscW(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_CLOSE)
    cmdCancel.Caption = strKey & "-" & cmdCancel.Caption
    If Left$(strKey, 1) = "F" Then
        mlngCloseKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngCloseKeyCode = AscW(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_SAVE)
    cmdAccept.Caption = strKey & "-" & cmdAccept.Caption
    If Left$(strKey, 1) = "F" Then
        mlngUseKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngUseKeyCode = AscW(UCase$(strKey))
    End If
    

End Sub

Private Sub ntxtNewPrice_GotFocus()
    
    ntxtNewPrice.BackColor = RGBEdit_Colour

End Sub

Private Sub ntxtNewPrice_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub ntxtNewPrice_LostFocus()
    
    ntxtNewPrice.BackColor = vbWhite

End Sub

Private Sub txtReason_GotFocus()
    
    txtReason.BackColor = RGBEdit_Colour

End Sub

Private Sub txtReason_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub txtReason_LostFocus()
    
    txtReason.BackColor = vbWhite

End Sub
