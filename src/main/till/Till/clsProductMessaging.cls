VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsProductMessaging"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'<CAMH>****************************************************************************************
'* Module : clsProductMessaging
'* Date   : 25/01/05
'* Author : tjnorris
'*$Archive: /Projects/OasysV2/VB/Product Messaging/clsProductMessaging.cls $
'*
'**********************************************************************************************
'* Summary:
'*
'* This class finds the required age limits on the set Inventory Item and
'* returns a message associated with an action for the caller to handle.
'* Also, palleted SKUs and quarantined SKUs are checked for.
'*
'**********************************************************************************************
'* $Author: tjnorris $ $Date: 25/01/08 08:50 $ $Revision: 0 $
'*
'* Date         Author      Vers.   Comment
'* ========     =========   =====   =========================
'* 25/01/08     tjnorris    1.0.0   Class created.
'*
'</CAMH>***************************************************************************************

Option Explicit

Private Enum enQuarantineLevel
    enqFull = 0
    enqPartial = 1
    enqNoQuarantine = 3
    
End Enum

Private mblnHasWarranty         As Boolean
Private mblnAbortItemSale       As Boolean
Private mlngHighestAgePassed    As Long
Private mlngLowestAgeFailed     As Long
Private mlngAgeInQuestion       As Long
Private moInventoryItem         As cInventory

'*****************************
'*                           *
'*   Public Let Properties   *
'*                           *
'*****************************

Public Property Let HighestAgePassed(ByVal lngHighestAgePassed As Long)
    mlngHighestAgePassed = lngHighestAgePassed
    
End Property

Public Property Let LowestAgeFailed(ByVal lngLowestAgeFailed As Long)
    mlngLowestAgeFailed = lngLowestAgeFailed
    
End Property

Public Property Set Product(ByVal objInventoryBO As cInventory)
Dim lngErr As Long

    On Error GoTo CreateObjErr

    Set moInventoryItem = objInventoryBO
    Call FindInventoryItemFlags

    Exit Property
    
CreateObjErr:

    lngErr = Err.Number And 65535

    Call MsgBoxEx("Error Number: " & lngErr & vbNewLine & _
                  "Error Source: " & Err.Source & vbNewLine & _
                  "Error Description: " & Err.Description & vbNewLine & vbNewLine, _
                  vbExclamation, _
                  "Error - clsProductMessaging.SetProduct", , , , , RGBMsgBox_WarnColour)

End Property

'*****************************
'*                           *
'*   Public Get Properties   *
'*                           *
'*****************************

Public Property Get AgeInQuestion() As Long
    AgeInQuestion = mlngAgeInQuestion
        
End Property

Public Property Get AbortItemSale() As Boolean
    AbortItemSale = mblnAbortItemSale
    
End Property

Public Property Get HasWarranty() As Boolean
    HasWarranty = mblnHasWarranty

End Property

'*****************************
'*                           *
'*   Class Init and Destroy  *
'*                           *
'*****************************

Private Sub Class_Initialize()

    On Error GoTo ObjectCreateErr
    
    mblnHasWarranty = False
    mblnAbortItemSale = False
    mlngHighestAgePassed = 0
    mlngLowestAgeFailed = 0
    
    Exit Sub
    
ObjectCreateErr:
    
    Call MsgBoxEx("Error initializing clsProductMessaging.", vbExclamation, "Error - clsProductMessaging.Initialize", , , , , RGBMsgBox_WarnColour)
    
End Sub

Private Sub Class_Terminate()

    If Not (moInventoryItem Is Nothing) Then
        Set moInventoryItem = Nothing
    End If

End Sub

'************************************************************
'*                                                          *
'*   FindInventoryItemFlags()                               *
'*                                                          *
'*   This subroutine finds Inventory Wickes data for the    *
'*   set Inventory BO item.  It checks for any required     *
'*   ages set on the item and builds a message and returns  *
'*   an action for the operator.                            *
'*                                                          *
'************************************************************

Private Sub FindInventoryItemFlags()
Dim colInventoryWickes       As Collection
Dim objInventoryWickes       As cInventoryWickes
Dim eqlQuarantineLevel       As enQuarantineLevel
Dim strStartOfMessage        As String
Dim strProductMessage        As String
Dim blnPasswordVerified      As Boolean
Dim blnIsPalletSku           As Boolean
Dim blnPriceDiscrepancy      As Boolean
Dim lngRequiredWeaponAge     As Long
Dim lngRequiredSolventAge    As Long
Dim lngMsgBoxResponse        As Long
Dim lngErr                   As Long
Dim intErr                   As Integer
Dim strErr                   As String

    On Error GoTo HandleErr

    intErr = 1
    Set objInventoryWickes = goDatabase.CreateBusinessObject(CLASSID_INVENTORYWICKES)
    
    intErr = 2
    With objInventoryWickes
        .AddLoadField FID_INVENTORYWICKES_OffensiveWeaponAge
'        .AddLoadField FID_INVENTORYWICKES_SolventAge
        .AddLoadField FID_INVENTORYWICKES_QuarantineFlag
        .AddLoadField FID_INVENTORYWICKES_PalletedSku
        .AddLoadField FID_INVENTORYWICKES_PricingDiscrepency
        
        .AddLoadFilter CMP_EQUAL, FID_INVENTORYWICKES_PartCode, Format(moInventoryItem.PartCode, "000000")
        Set colInventoryWickes = .LoadMatches
        
    End With
    
    intErr = 3
    mblnHasWarranty = False
    mblnAbortItemSale = False
    strStartOfMessage = moInventoryItem.PartCode & " - " & _
                        moInventoryItem.Description & vbNewLine & vbNewLine
                         
    intErr = 4
    If (colInventoryWickes.Count > 0) Then
        
        intErr = 5
        Set objInventoryWickes = colInventoryWickes(1)
        
        Call SetRequiredAgeLimits(objInventoryWickes, lngRequiredSolventAge, lngRequiredWeaponAge)
        
        intErr = 6
        If (IsQuarantined(objInventoryWickes, eqlQuarantineLevel)) Then
            Select Case eqlQuarantineLevel
                Case enqFull
                    Call MsgBoxEx(strStartOfMessage & _
                                  "This product is subject to quarantine - Contact Management." & vbNewLine & _
                                  "It cannot be sold and will NOT be added to this transaction.", _
                                  vbExclamation + vbOKOnly, "Full Quarantined Item", , , , , RGBMsgBox_WarnColour)
                    mblnAbortItemSale = True
                    Exit Sub
                    
                Case enqPartial
                    If MsgBoxEx("This product can only be sold with Management consultation", vbYesNo + vbDefaultButton2, "Quarantine Product", "Esc-Reject Item", "Enter-Get Approval", , , RGBMSGBox_PromptColour) = 7 Then
                        Call Load(frmVerifyPwd)
                        blnPasswordVerified = frmVerifyPwd.VerifySupervisorPassword(strStartOfMessage & _
                                              "This product can only be sold with management consultation.")
                        Call Unload(frmVerifyPwd)
                    End If
                    If (blnPasswordVerified = False) Then
                        mblnAbortItemSale = True
                        Exit Sub
                        
                    End If
                    
                    
                Case enqNoQuarantine
                    'Shouldn't get here
                    'but do nothing incase.
                    
            End Select
            
        End If
        
        intErr = 8
        If (lngRequiredSolventAge > mlngHighestAgePassed) Then
            
            mlngAgeInQuestion = lngRequiredSolventAge
            If (mlngLowestAgeFailed = 0) Or (lngRequiredSolventAge < mlngLowestAgeFailed) Then
                lngMsgBoxResponse = MsgBoxEx(strStartOfMessage & _
                                             "Restricted to persons aged " & CStr(lngRequiredSolventAge) & _
                                             " and over - Solvent.", vbExclamation + vbYesNo, _
                                             "Age Verification", , , , , RGBMSGBox_PromptColour)
                If (lngMsgBoxResponse = vbNo) Then
                    mblnAbortItemSale = True
                    Exit Sub
                ElseIf CheckIfSure = False Then
                    mblnAbortItemSale = True
                    Exit Sub
                End If
            
            ElseIf (mlngLowestAgeFailed <= lngRequiredSolventAge) Then
                Call MsgBoxEx(strStartOfMessage & _
                              "Already failed AGE QUALIFICATION of " & _
                              CStr(mlngLowestAgeFailed), vbExclamation + vbOKOnly, _
                              "Failed Age Qualification", , , , , RGBMSGBox_PromptColour)
                mblnAbortItemSale = True
                Exit Sub
                
            End If '(mlngLowestAgeFailed = 0)
            
        End If '(lngRequiredSolventAge > 0)
        
        intErr = 7
        If (lngRequiredWeaponAge > mlngHighestAgePassed) Then
            
            mlngAgeInQuestion = lngRequiredWeaponAge
            If (mlngLowestAgeFailed = 0) Or (lngRequiredWeaponAge < mlngLowestAgeFailed) Then
                lngMsgBoxResponse = MsgBoxEx(strStartOfMessage & _
                                             "Restricted to persons aged " & CStr(lngRequiredWeaponAge) & _
                                             " and over - Offensive Weapon.", _
                                             vbExclamation + vbYesNo, _
                                             "Age Verification", , , , , RGBMSGBox_PromptColour)
                If (lngMsgBoxResponse = vbNo) Then
                    mblnAbortItemSale = True
                    Exit Sub
                ElseIf CheckIfSure = False Then
                    mblnAbortItemSale = True
                    Exit Sub
                End If
            
            ElseIf (mlngLowestAgeFailed <= lngRequiredWeaponAge) Then
                Call MsgBoxEx(strStartOfMessage & _
                              "Already failed AGE QUALIFICATION of " & _
                              CStr(mlngLowestAgeFailed), vbExclamation + vbOKOnly, _
                              "Failed Age Qualification", , , , , RGBMSGBox_PromptColour)
                mblnAbortItemSale = True
                Exit Sub
                
            End If '(mlngLowestAgeFailed = 0)
        
        End If
        
        intErr = 12
        blnPriceDiscrepancy = objInventoryWickes.PricingDiscrepency
        
        If (blnPriceDiscrepancy) Then
            lngMsgBoxResponse = MsgBoxEx(strStartOfMessage & _
                                "Refer to the Booklet Amendments Notice for" & vbNewLine & _
                                "further information on this product." & vbNewLine & vbNewLine & _
                                "Press <Esc> to Reject Item", _
                                vbInformation + vbOKOnly, _
                                "Pricing Discrepancy", , , , , RGBMSGBox_PromptColour)
                          
            If (lngMsgBoxResponse <> vbOK) Then
                mlngAgeInQuestion = mlngLowestAgeFailed
                mblnAbortItemSale = True
                Exit Sub
            End If
                                 
        End If
        
        intErr = 9
        blnIsPalletSku = objInventoryWickes.PalletedSku
        If (blnIsPalletSku) Then
            
            lngMsgBoxResponse = MsgBoxEx(strStartOfMessage & _
                                "A pallet desposit is required for this item." & vbNewLine & _
                                "Please ensure that any deposit is paid.", vbExclamation + vbYesNo, _
                                "Pallet Item", , , , , RGBMsgBox_WarnColour)
             If (lngMsgBoxResponse = vbNo) Then
                mblnAbortItemSale = True
                Exit Sub
                
            End If
            
        End If 'blnIsPalletSku
        
        intErr = 11
        mblnHasWarranty = moInventoryItem.Warranty
        If (mblnHasWarranty) Then
            Call MsgBoxEx(strStartOfMessage & _
                          "This item carries a product warranty." & vbNewLine & _
                          "Please enter customer details.", _
                          vbInformation + vbOKOnly, _
                          "Warranty Notification", , , , , RGBMSGBox_PromptColour)
                                 
        End If 'mblnHasWarranty
        
    Else 'nothing was found.
        Call MsgBoxEx(strStartOfMessage & _
                      "The item could not be found in" & vbNewLine & _
                      "the inventory business object.", vbExclamation + vbOKOnly, _
                      "Error - clsProductMessaging", , , , , RGBMsgBox_WarnColour)
        
    End If 'colInventoryWickes.Count > 0
    
    intErr = 10
    If (Not IsEmpty(colInventoryWickes)) Then Set colInventoryWickes = Nothing
    If (Not (objInventoryWickes Is Nothing)) Then Set objInventoryWickes = Nothing
    
    Exit Sub
    
HandleErr:

    lngErr = Err.Number And 65535
    
    If (Not IsEmpty(colInventoryWickes)) Then Set colInventoryWickes = Nothing
    If (Not (objInventoryWickes Is Nothing)) Then Set objInventoryWickes = Nothing
    
    Select Case intErr
        Case 1: strErr = "Error creating Inventory Wickes object."
        Case 2: strErr = "Error retrieving Inventory Wickes data."
        Case 3: strErr = "Error accessing Inventory data."
        Case 4: strErr = "Error accessing collection count property."
        Case 5: strErr = "Error accessing Inventory Wickes object."
        Case 6: strErr = "Error reading Quarantine data."
        Case 7: strErr = "Error setting Offensive Weapons warnings."
        Case 8: strErr = "Error setting Solvent warnings."
        Case 9: strErr = "Error reading Palleted SKU data."
        Case 10: strErr = "Error destroying Inventory Wickes objects."
        Case 11: strErr = "Error reading warranty data."
        Case 12: strErr = "Error reading pricing discrepancy data."
        
    End Select
    
    Call MsgBoxEx("Error Number: " & lngErr & vbNewLine & _
                  "Error Source: " & Err.Source & vbNewLine & _
                  "Error Description: " & Err.Description & vbNewLine & vbNewLine & _
                  "Custom Error: " & strErr, _
                  vbExclamation, _
                  "Error - clsProductMessaging.FindInventoryItemFlag", , , , , RGBMsgBox_WarnColour)

End Sub

Private Function IsQuarantined(ByVal oInventory As cInventoryWickes, _
                               ByRef eqlType As enQuarantineLevel) As Boolean
Dim strFlag As String

    strFlag = UCase(oInventory.QuarantineFlag)
    
    If ((strFlag = "Q") Or (strFlag = "B")) And (1 = 2) Then 'never used
        If (strFlag = "Q") Then 'the item is fully quarantined
            eqlType = enqFull
        Else
            eqlType = enqPartial
        End If
        IsQuarantined = True
        
    Else
        eqlType = enqNoQuarantine
        IsQuarantined = False
        
    End If
    
End Function

Private Sub SetRequiredAgeLimits(ByVal oInventory As cInventoryWickes, _
                                 ByRef lngSolventAge As Long, _
                                 ByRef lngWeaponAge As Long)

    lngSolventAge = CLng(oInventory.SolventAge)
    lngWeaponAge = CLng(oInventory.OffensiveWeaponAge)

End Sub

Private Function CheckIfSure() As Boolean
    
    Load frmCheckIfSure
    
    frmCheckIfSure.Show vbModal
    If frmCheckIfSure.Passed Then
        CheckIfSure = True
    Else
        CheckIfSure = False
    End If
    
    Unload frmCheckIfSure
    
End Function
