VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "frmTillFactory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Const m_StopEnterKeypressDuringCardPaymentParameterID As Long = 980876

Private m_InitialisedUseStopEnterKeypressDuringCardPaymentImplementation As Boolean
Private m_UseStopEnterKeypressDuringCardPaymentImplementation As Boolean

Friend Property Get UseReferral870Implementation() As Boolean

    UseReferral870Implementation = m_UseReferral870Implementation
End Property

Friend Property Get UseStopEnterKeypressDuringCardPaymentImplementation() As Boolean
    
    UseStopEnterKeypressDuringCardPaymentImplementation = m_UseStopEnterKeypressDuringCardPaymentImplementation
End Property

Public Function FactoryGetStopEnterKeypressDuringCardPayment() As IfrmTill876
    
    If UseStopEnterKeypressDuringCardPaymentImplementation Then
        'using implementation with 876 fix
        Set FactoryGetStopEnterKeypressDuringCardPayment = New StopEnterKeypressDuringCardPaymentfrmTill
    Else
        'using live implementation
        Set FactoryGetStopEnterKeypressDuringCardPayment = New LivefrmTill
    End If
End Function

Private Sub Class_Initialize()

    GetUseStopEnterKeypressDuringCardPaymentParameterValue
End Sub

Friend Sub GetUseStopEnterKeypressDuringCardPaymentParameterValue()

    If Not m_InitialisedUseStopEnterKeypressDuringCardPaymentImplementation Then
On Error Resume Next
        m_UseStopEnterKeypressDuringCardPaymentImplementation = goSession.GetParameter(m_StopEnterKeypressDuringCardPaymentParameterID)
        m_InitialisedUseStopEnterKeypressDuringCardPaymentImplementation = True
On Error GoTo 0
    End If
End Sub
