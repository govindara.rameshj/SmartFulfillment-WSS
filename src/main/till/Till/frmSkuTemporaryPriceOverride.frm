VERSION 5.00
Object = "{FA666EFA-0D22-45D3-9849-6B0694037D1B}#2.1#0"; "EditNumberTill.ocx"
Begin VB.Form frmSkuTemporaryPriceOverride 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Single SKU Price Override"
   ClientHeight    =   3195
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9480
   Icon            =   "frmSkuTemporaryPriceOverride.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   9480
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtSkuDescription 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   3000
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   840
      Width           =   6315
   End
   Begin VB.TextBox txtExistingPrice 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   3000
      Locked          =   -1  'True
      TabIndex        =   2
      Top             =   1320
      Width           =   1095
   End
   Begin VB.TextBox txtSkuCode 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   3000
      Locked          =   -1  'True
      TabIndex        =   0
      Top             =   360
      Width           =   1095
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&OK"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   3173
      TabIndex        =   4
      Top             =   2400
      Width           =   1455
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   4853
      TabIndex        =   5
      Top             =   2400
      Width           =   1455
   End
   Begin EditNumberTill.ucTillNumberText uctntPrice 
      Height          =   420
      Left            =   3000
      TabIndex        =   3
      Top             =   1800
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   741
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   204
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinimumValue    =   0
   End
   Begin VB.Label lblOverridePrice 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "New Override Price"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   240
      TabIndex        =   9
      Top             =   1800
      Width           =   2475
   End
   Begin VB.Label lblSkuCode 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "SKU Code"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   240
      TabIndex        =   8
      Top             =   360
      Width           =   2475
   End
   Begin VB.Label lblSkuDescription 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "SKU Description"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   240
      TabIndex        =   7
      Top             =   840
      Width           =   2475
   End
   Begin VB.Label lblExistingPrice 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Existing Price"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   240
      TabIndex        =   6
      Top             =   1320
      Width           =   2475
   End
End
Attribute VB_Name = "frmSkuTemporaryPriceOverride"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private blnCancelled As Boolean
Private currNewPrice As Currency

Private intUserOneID As Integer
Private intUserTwoID As Integer

Private mlngNumericPadSubtractkey As Long

Public Function CancelPressed() As Boolean
   
   CancelPressed = blnCancelled

End Function

Public Function NewPrice() As Currency
   
   NewPrice = currNewPrice

End Function

Public Sub UserOneID(ByVal UserID As Integer)
   
   intUserOneID = UserID

End Sub

Public Sub UserTwoID(ByVal UserID As Integer)
   
   intUserTwoID = UserID

End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Private Sub Form_Activate()

   uctntPrice.SetFocus

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    Select Case (KeyCode)
        Case (vbKeySubtract)
                KeyCode = 0
        Case (mlngNumericPadSubtractkey)
                KeyCode = 0
    End Select
End Sub

Private Sub Form_Load()
    mlngNumericPadSubtractkey = 189
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Private Sub uctntPrice_GotFocus()

   uctntPrice.BackColor = RGBEdit_Colour

End Sub

Private Sub uctntPrice_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case (KeyCode)
        Case (vbKeySubtract)
                KeyCode = 0
        Case (mlngNumericPadSubtractkey)
                KeyCode = 0
    End Select
End Sub

Private Sub uctntPrice_KeyPress(KeyAscii As Integer)

 If KeyAscii = vbKeyReturn Then
   
      If uctntPrice.Value < 0 Then uctntPrice.Value = 0
   
      cmdOK.SetFocus
   
   End If

End Sub

Private Sub uctntPrice_LostFocus()

   uctntPrice.BackColor = RGB_WHITE

   If uctntPrice.Value < 0 Then uctntPrice.Value = 0

End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Private Sub cmdCancel_Click()
      
   blnCancelled = True
   Unload Me
   
End Sub

Private Sub cmdOK_Click()

'   not required now; price entered via user control which does not accept non-numeric input
'
'   'validation : not numeric
'   If IsNumeric(uctntPrice.Value) = False Then
'
'      Call MsgBoxEx("Empty or non-numeric value entered", vbOKOnly, "WARNING", , , , , RGBMsgBox_WarnColour)
'      Exit Sub
'
'   End If

   'validation : negative
   If uctntPrice.Value <= 0 Then
      
      Call MsgBoxEx("Zero or negative value entered", vbOKOnly, "WARNING", , , , , RGBMsgBox_WarnColour)
      uctntPrice.SetFocus
      Exit Sub
   
   End If

   currNewPrice = uctntPrice.Value

   If NormalRetailPrice(txtSkuCode.Text) < currNewPrice Then
   
      Call MsgBoxEx("Override price is greater than current retail price", vbInformation, "Information", , , , , RGBMsgBox_WarnColour)
      uctntPrice.SetFocus
      Exit Sub

   End If

   'write new sku override price to database
   Dim v As COMTPWickes_InterOp_Interface.ITemporaryPriceChangeEvent
   Dim Factory As New COMTPWickes_InterOp_Wrapper.TemporaryPriceChangeEvent
      
   Set v = Factory.FactoryGet

   If v.SetNewPrice(txtSkuCode.Text, Now, currNewPrice, intUserOneID, intUserTwoID) = False Then

      Call MsgBoxEx("ERROR : Unable to save new SKU price to database" & vbCrLf & vbCrLf & "A critical error has occurred", vbOKOnly, "", , , , , RGBMSGBox_PromptColour)
      Exit Sub

   End If

   blnCancelled = False
   Unload Me

End Sub

Private Function NormalRetailPrice(ByVal strPartCode As String) As Double

   Dim oItem As cInventory

   Set oItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
   
   Call oItem.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, strPartCode)
   Call oItem.AddLoadField(FID_INVENTORY_NormalSellPrice)

    Call oItem.LoadMatches
    
   NormalRetailPrice = oItem.NormalSellPrice

End Function
