Attribute VB_Name = "modItemSearch"
Option Explicit

' SKU enquiry type, PIM items only or full product listing.
Public Enum EnquiryType
    etPIMOnly = 1
    etFullItems = 2
    etNotSet = 3
End Enum


