VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{B3202B8D-B8B9-4655-9712-EAB5BFD920D4}#1.0#0"; "ItemFilter.ocx"
Begin VB.Form frmItemSearch 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Item Search"
   ClientHeight    =   11220
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   15645
   Icon            =   "frmItemSearch.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   11220
   ScaleWidth      =   15645
   StartUpPosition =   2  'CenterScreen
   Begin ItemFilter_UC_Wickes.ucItemFilter ucItemSearch 
      Height          =   11055
      Left            =   240
      ScaleHeight     =   10995
      ScaleWidth      =   15195
      TabIndex        =   1
      Top             =   120
      Width           =   15255
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   10845
      Width           =   15645
      _ExtentX        =   27596
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmItemSearch.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   20241
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "22-Aug-05"
            TextSave        =   "22-Aug-05"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "12:59"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmItemSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'*
'*$Archive: /Projects/OasysHW/VB/Item Search/frmItemSearch.frm $
'**********************************************************************************************
'* $Author: Davidf $ $Date: 10/07/06 10:08 $ $Revision: 0 $
'* $Versions:1.0.00$
'*
'**********************************************************************************************
'* Summary: Application used to allow the user to search for products using the ItemFilter
'*                      ActiveX control.
'*
'**********************************************************************************************
'* Versions:
'*
'* 10/07/06 DaveF   v1.0.0  Initial build. WIX1156.
'*
'* 28/11/06 DaveF   v1.0.2  Compiled with the new version of ItemFilter_UC which now just acts
'*                              as an interface to ItemFilter.exe.
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*
'**********************************************************************************************
'</CAMH>***************************************************************************************

Option Explicit

Private Const MODULE_NAME  As String = "frmItemSearch"

Private metEnquiryType As EnquiryType

Private Sub Form_Load()
 
Const PROCEDURE_NAME As String = "Form_Load"

Dim strErrSource  As String

    On Error GoTo FormLoad_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, App.EXEName & " - Started on " & Now())
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Load")
    Me.WindowState = vbMaximized
'   Get the system setup.
    Call GetRoot
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    
    ' Initialise the item search usercontrol to start building the hierarchy trees.
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Initialise UserControl")
    Call ucItemSearch.Initialise(goSession, Me)
    
    ' Get which enquiry type to use.
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Get Enquiry Type")
    Call GetEnquiryType
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Load Successful")
    
    Exit Sub
    
FormLoad_Error:
   
    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, App.EXEName)
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " - Error No - " & Err.Number & " - Error Desc - " & Err.Description)
    Call Err.Report(MODULE_NAME, PROCEDURE_NAME, 0, False)
    
    Unload Me
 
End Sub

Private Sub Form_Activate()

Const PROCEDURE_NAME As String = "Form_Activate"
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Activate")
    
    ' Load the Item Search user control.
    ucItemSearch.ItemEnquiryType = metEnquiryType
    ucItemSearch.Visible = True
    Call ucItemSearch.FillScreen(ucItemSearch)
'    ucItemSearch.SetFocus
 '   Call ucItemSearch.SetFocus
    Me.Visible = False

    Exit Sub
    
Form_Activate_Error:

End Sub

' Unload the form and empty all the used objects.
Private Sub Form_Unload(Cancel As Integer)

Const PROCEDURE_NAME As String = "Form_Unload"
    
    On Error GoTo Form_Unload_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Unload")

    Exit Sub
    
Form_Unload_Error:

    End
    
End Sub

Private Sub GetEnquiryType()

Const PROCEDURE_NAME As String = "GetEnquiryType"
    
    On Error GoTo GetEnquiryType_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")

    Load frmItemSearchTypeSelect
    frmItemSearchTypeSelect.BackColor = Me.BackColor
    frmItemSearchTypeSelect.lblMessage.BackColor = Me.BackColor
    frmItemSearchTypeSelect.Show vbModal
    If (frmItemSearchTypeSelect.EnquiryType = etNotSet) Then
        Unload Me
    End If
    metEnquiryType = frmItemSearchTypeSelect.EnquiryType
    Unload frmItemSearchTypeSelect
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "End")
    
    Exit Sub
    
GetEnquiryType_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " - Error No - " & Err.Number & " - Error Desc - " & Err.Description)
    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, App.EXEName)

End Sub

Private Sub ucItemSearch_Cancel()

    Unload Me
 
End Sub
