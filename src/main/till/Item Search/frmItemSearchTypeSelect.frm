VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmItemSearchTypeSelect 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Item Search"
   ClientHeight    =   3120
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5775
   Icon            =   "frmItemSearchTypeSelect.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3120
   ScaleWidth      =   5775
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdFullProducts 
      Caption         =   "F6 - &Full"
      Default         =   -1  'True
      Height          =   375
      Left            =   2280
      TabIndex        =   1
      Top             =   1643
      Width           =   1335
   End
   Begin VB.CommandButton cmdPIMProducts 
      Caption         =   "F5 - &PIM"
      Height          =   375
      Left            =   688
      TabIndex        =   0
      Top             =   1643
      Width           =   1335
   End
   Begin VB.CommandButton cmdExit 
      Cancel          =   -1  'True
      Caption         =   "F12 - &Exit"
      Height          =   375
      Left            =   3872
      TabIndex        =   2
      Top             =   1643
      Width           =   1215
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   4
      Top             =   2745
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmItemSearchTypeSelect.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   2831
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "22-Aug-05"
            TextSave        =   "22-Aug-05"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "15:34"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Shape Shape1 
      BorderStyle     =   6  'Inside Solid
      BorderWidth     =   2
      DrawMode        =   1  'Blackness
      Height          =   2415
      Left            =   240
      Top             =   173
      Width           =   5295
   End
   Begin VB.Label lblMessage 
      Alignment       =   2  'Center
      Caption         =   "Please select PIM or Full Product listing view"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   900
      TabIndex        =   3
      Top             =   743
      Width           =   3975
   End
End
Attribute VB_Name = "frmItemSearchTypeSelect"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'*
'*$Archive: /Projects/OasysHW/VB/Item Search/frmItemSearchTypeSelect.frm $
'**********************************************************************************************
'* $Author: Davidf $ $Date: 10/07/06 10:08 $ $Revision: 0 $
'* $Versions:1.0.00$
'*
'**********************************************************************************************
'* Summary: Used to allow the user to select what type of Enquiry to use, PIM or full items.
'*
'**********************************************************************************************
'* Versions:
'*
'* 10/07/06 DaveF   v1.0.0  Initial build. WIX1156.
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*
'**********************************************************************************************
'</CAMH>***************************************************************************************

Option Explicit

Private Const MODULE_NAME  As String = "frmItemSearchTypeSelect"

Private metEnquiryType As EnquiryType

Private Sub Form_Load()

Const PROCEDURE_NAME As String = "Form_Load"

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Load")
    
    Call InitialiseStatusBar(sbStatus)

End Sub

' Form Key Down event.
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If Shift = 0 Then ' No shift/ctrl/alt etc. combinations
        Select Case KeyCode
            Case vbKeyF5
                KeyCode = 0
                cmdPIMProducts_Click    ' Save the list data.
                
            Case vbKeyF6
                KeyCode = 0
                cmdFullProducts_Click   ' View the Audit report.
                        
            Case vbKeyF12
                KeyCode = 0
                cmdExit_Click           ' Exit system.
        End Select
    End If
    
End Sub

Public Property Get EnquiryType()

    EnquiryType = metEnquiryType
    
End Property

'   Exit the system.
Private Sub cmdExit_Click()

Const PROCEDURE_NAME As String = "cmdExit_Click"
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdExit Click")
    
    metEnquiryType = etNotSet
    Me.Hide
    
End Sub

Private Sub cmdFullProducts_Click()

Const PROCEDURE_NAME As String = "cmdFullProducts_Click"

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdFullProducts_Click")
    
    metEnquiryType = etFullItems
    Me.Hide
    
End Sub

Private Sub cmdPIMProducts_Click()

Const PROCEDURE_NAME As String = "cmdPIMProducts_Click"

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdPIMProducts_Click")
    
    metEnquiryType = etPIMOnly
    Me.Hide

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

Const PROCEDURE_NAME As String = "Form_QueryUnload"

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Unload")
    
    ' If the user clicked the close form control button then handle shutting down.
    If (UnloadMode = 0) Then
        metEnquiryType = etNotSet
    End If
    
End Sub
