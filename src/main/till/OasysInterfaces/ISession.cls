VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ISession"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
' $Archive: /Projects/OasysV2/VB/OasysInterfaces/ISession.cls $
' $Revision: 6 $
' $Date: 10/11/02 2:43p $
' $Author: JohnG $
'
'----------------------------------------------------------------------------
' Interface only, so no data or procedure please!
'----------------------------------------------------------------------------

Public Property Get Root() As IOasysRoot
End Property

Public Property Get IsInteractive() As Boolean
End Property

Public Property Get IsOnline() As Boolean
End Property

Public Property Let IsOnline(ByVal Value As Boolean)
End Property
