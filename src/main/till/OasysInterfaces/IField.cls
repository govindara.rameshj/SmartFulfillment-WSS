VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IField"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
' $Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/IField.cls $
' $Revision: 15 $
' $Date: 31/10/02 15:02 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
'----------------------------------------------------------------------------

Public Enum enFieldType
    FieldTypeInvalid = 0    ' Initial value
    FieldTypeString = 1
    FieldTypeLong = 2
    FieldTypeBool = 3
    FieldTypeDate = 4
    FieldTypeDouble = 5
    FieldTypeGroup = 6
End Enum

Public Enum enFieldInterfaceType
    FieldInterfaceNative = 0    ' Default
    FieldInterfaceIField = 1
End Enum

' The enComparator lists the types of comparison supported by the IField interface.
Public Enum enComparator
    CMP_INVALID = 0     ' Initial value
    CMP_EQUAL = 1
    CMP_NOTEQUAL = 2
    CMP_LESSTHAN = 3
    CMP_GREATERTHAN = 4
    CMP_COMPARE = 5     ' !!!! For String this is a compare no case !!!!!!
    CMP_SELECTALL = 6   ' ie. *
    CMP_SELECTLIST = 7  ' ie. any of list of values
    CMP_LIKE = 8        ' ie. with leading or trailing % as wildcard
    CMP_LESSEQUALTHAN = 9
    CMP_GREATEREQUALTHAN = 10
    CMP_INRANGE = 11    ' ie. Not less than first value or greater than second
End Enum

' The enBoInterfaceType lists the types of interface supported by IBo.Interface()
Public Enum enBoInterfaceType
    BO_INTERFACE_NATIVE = 0     ' Default, native interface
    BO_INTERFACE_IBO = 1        ' The IBo interface
End Enum

Public Type tFieldProps
    ' Id combines class id in top int and property id in bottom int
    nId As Long
End Type

Public Property Get FieldType() As enFieldType
End Property
Public Property Let Id(nId As Long)
End Property
Public Property Get Id() As Long
End Property
Public Property Let SeqNo(lSeqNo As Long)
End Property
Public Property Get SeqNo() As Long
End Property
Public Property Get DisplayValue() As String
End Property
Public Property Get SqlLiteral() As String
End Property
Public Property Let ValueAsVariant(Value As Variant)
End Property
Public Property Get ValueAsVariant() As Variant
End Property
Public Function Duplicate() As IField
End Function
Public Function CreateNew() As IField
End Function
Public Function Matches(nComparator As enComparator, vtValue As Variant) As Integer
End Function
Public Property Get IsEmpty() As Boolean
End Property
Public Property Let IsEmpty(ByVal Value As Boolean)
End Property
Public Function Interface(Optional eInterfaceType As enFieldInterfaceType) As iField
End Function

