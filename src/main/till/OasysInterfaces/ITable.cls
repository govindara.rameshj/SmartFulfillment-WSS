VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ITable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : ITable
'* Date   : 28/09/04
'* Author : johng
'*$Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/ITable.cls $
'----------------------------------------------------------------------------
' Typed Interface only, so no data or procedure please!
'----------------------------------------------------------------------------
'</CAMH>***************************************************************************************

Option Explicit

Public Function Initialise(nTableId As Integer, strNameList As String) As Boolean
End Function

Public Function InitialiseForeignKey(strName As String, nCid As Long) As Boolean
End Function

Public Property Get TableId() As Integer
End Property

Public Property Get SimpleColumnName(oField As IField) As String
End Property

Public Property Get TableName() As String
End Property

Public Property Get SqlColumnName(oField As IField) As String
End Property

Public Property Get SqlLiteral(oField As IField) As String
End Property

Public Property Get IsAutoIncrement(nCid As Long) As Boolean
End Property

Public Property Get IsDistinct(nCid As Long) As Boolean
End Property

Public Property Get IsSum(nCid As Long) As Boolean
End Property

Public Property Get IsSimpleField(nCid As Long) As Boolean
End Property

Public Property Get FieldCount(nCid As Long) As Integer
End Property

Public Property Get KeyCount() As Integer
End Property


