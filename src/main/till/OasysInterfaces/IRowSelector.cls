VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IRowSelector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
' $Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/CRowSelector.cls $
' $Revision: 12 $
' $Date: 18/11/02 13:04 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
'* Date   : 28/09/04
'* Author : johng
'*$Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/ITable.cls $
'----------------------------------------------------------------------------
' Typed Interface only, so no data or procedure please!
'----------------------------------------------------------------------------
Public Sub Clear()
End Sub

Public Sub Add(oFieldSelector As IFieldSelector)
End Sub

Public Sub AddSelection(Comparator As enComparator, oField As IField, Optional bIsSelectOnly As Boolean)
End Sub

Public Sub AddSelectAll(FieldID As Long)
End Sub

Public Sub AddSelectValue(Comparator As enComparator, FieldID As Long, Value As Variant)
End Sub

Public Property Get Count() As Long
End Property

Public Sub Remove(nInstance As Integer)
End Sub

Public Property Get FieldSelector(nInstance As Integer) As IFieldSelector
End Property

Public Property Set FieldSelector(nInstance As Integer, oFSelector As IFieldSelector)
End Property

Public Function GetInstanceFromFid(nFid As Long) As Integer
End Function

Public Property Set SysRoot(oSysRoot As ISysRoot)
End Property

Public Function Merge(oRowSelector As IRowSelector) As Boolean
End Function

Public Function Override(oRowSelector As IRowSelector) As Boolean
End Function

Public Property Get DebugString() As String
End Property

Public Function ExtractFieldRow() As IRow
End Function
