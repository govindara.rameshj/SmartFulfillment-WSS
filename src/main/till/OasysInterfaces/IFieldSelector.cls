VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IFieldSelector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
' $Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/CFieldSelector.cls $
' $Revision: 15 $
' $Date: 25/02/03 11:07 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
'* Date   : 28/09/04
'* Author : johng
'*$Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/IFieldSelector.cls $
'----------------------------------------------------------------------------
' Typed Interface only, so no data or procedure please!
'----------------------------------------------------------------------------

Public Type tFSelectorProps
    nComparator     As enComparator
    oSingleField    As IField
    ' When unset, a selection includes the item in the returned row.  When set, it is used to select the row and then discarded.
    bIsSelectOnly As Boolean
    ' When set, reverses the sense of the returned boolean value for all operations, except COMPARE which returns tri-state.
    bNegateResult As Boolean
End Type

Public Sub Initialise(Comparator As enComparator, oField As IField, Optional bIsSelectOnly As Boolean)
End Sub

Public Sub InitialiseAsSelectAll(FieldID As Long)
End Sub

Public Sub AddField(oField As IField)
End Sub

Public Function Duplicate() As IFieldSelector
End Function

Public Property Let FSelectorProps(Props As tFSelectorProps)
End Property

Public Property Get Comparator() As enComparator
End Property

Public Property Get IsSelectOnly() As Long
End Property

Public Property Get FieldCount() As Integer
End Property

Public Property Get Field(Optional nInstance As Integer) As IField
End Property

Public Property Set SysRoot(oSysRoot As ISysRoot)
End Property

Public Sub Clear()
End Sub

Public Function Matches(vtValue As Variant) As Integer
End Function

Public Property Get NotComparator() As Boolean
End Property

Public Property Let NotComparator(Value As Boolean)
End Property

Public Property Get IDbDbParam() As Long
End Property

Public Property Let IDbDbParam(nDbParam As Long)
End Property

