VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IBoSortKeys"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'* Date   : 28/09/04
'* Author : johng
'*$Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/IBoSortKeys.cls $
'----------------------------------------------------------------------------
' Typed Interface only, so no data or procedure please!
'----------------------------------------------------------------------------
Public Type typeSortKey
    nFieldId        As Long
    bIsDescending   As Boolean
End Type
Private m_colKeys   As Collection   ' each As typeSortKey

Public Sub Clear()
End Sub

Public Sub Add(nFieldId As Long, Optional bIsDescending As Boolean)
End Sub

Public Property Get Count() As Long
End Property

Private Property Get Field(nInstance As Integer) As typeSortKey
End Property

Public Property Get FieldId(nInstance As Integer) As Long
End Property

Public Property Get IsDescending(nInstance As Integer) As Boolean
End Property

Friend Function Duplicate() As IBoSortKeys
End Function


