VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ITailoredData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
#Const ccDebug = 0
'
' $Archive: /Projects/OasysV2/VB/OasysStartCom/ITailoredData.cls $
' $Revision: 5 $
' $Date: 12/02/02 11:06a $
' $Author: Mauricem $
'
'----------------------------------------------------------------------------
' Interface only, so no data or procedure please!
' The data will be isolated inside a customer-specific dll.
'----------------------------------------------------------------------------
' This interface defines how we pass tailoring details between
' the Tailoring object and the tailored object.  The context
' gives the type of tailored object.  Initially restricted to
' Client and Enterprise, but we could extend it if necessary.
' Hopefully we won't need to.
' Within a context, there will be one or more data record types
' each identified by a record type number, unique within context.

' We want to be able to add Record Types in the future without
' breaking this interface.  So we don't formally define the
' types here.  However, we do want them to be unique, so we
' simply document them here for reference, but don't change
' the actual code, so we don't break compatibility.
'Private Const TAILOR_CONTEXT_NOTHING = 0
'Private Const TAILOR_CONTEXT_CLIENT = 1
'Private Const TAILOR_CONTEXT_ENTERPRISE = 2
'
'Private Const TAILOR_REC_NOTHING = 0
'Private Const TAILOR_CLIENT_REC_BASE = 1
'Private Const TAILOR_ENTERPRISE_REC_BASE = 1
'----------------------------------------------------------------------------
Public Type tTailoringSubContext
    ' This logical data type describes a lower level context
    nContextType    As Integer
    nInstanceCount  As Integer
End Type

Public Type tTailoringDataRec
    ' This record type represents the physical one that is returned
    nContextType    As Integer      ' Client or Enterprise
    nRecType        As Integer      ' Logical type: Base, Extended, etc.
    nFormatVersion  As Integer      ' Allows for slight format changes
    varBuffer       As Variant      ' The raw data as a byte array
End Type

' The tClientBaseRecV1 data type may be extended within a particular tailoring object
' We only change it here on the rare occasions that we don't mind
' breaking compatibility.  Similarly for tEnterpriseBaseRecV1.
Public Type tClientBaseRecV1
    ' The udtSubContext logical data type lists child level contexts
    arrtSubContext() As tTailoringSubContext
    ' nDataVersion should be incremented whenever any data set
    ' by a Tailoring class is changed, whether for Client or Enterprise.
    nDataVersion     As Integer
    strShortName     As String
    strFullName      As String
    ' The client Mnemonic is used, amongst other things to distinguish
    ' tailoring files.  "Generic" is a special Mnemonic for an untailored
    ' client, although it should be transparent.
    strMnemonic      As String
    strStoreNo       As String
    strWorkstationID As String
End Type

Public Type tEnterpriseBaseRecV1
    ' The udtSubContext logical data type lists child level contexts
    udtSubContext() As tTailoringSubContext
    strShortName     As String
    strFullName      As String
    ' The Enterprise Mnemonic is used, amongst other things to distinguish
    ' data sources.  For clients with only one enterprise, the Enterprise Mnemonic
    ' defaults to the same as the Client Mnemonic.
    strMnemonic      As String
    strStoreNo       As String
    strWorkstationID As String
End Type

Public Function Initialise(Root As IOasysRoot) As Boolean
End Function

Public Property Get RecordCount(nContextType As Integer, Optional nInstance As Integer) As Integer
End Property

Public Property Get Record(nContextType As Integer, _
                Optional nInstance As Integer, _
                Optional nRecNumber As Integer) _
                As tTailoringDataRec
End Property

