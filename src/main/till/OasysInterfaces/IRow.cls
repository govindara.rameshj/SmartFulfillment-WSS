VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IRow"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
' $Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/CRow.cls $
' $Revision: 6 $
' $Date: 10/11/02 9:23a $
' $Author: Mauricem $
'
'----------------------------------------------------------------------------
'* Date   : 28/09/04
'* Author : johng
'*$Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/IRow.cls $
'----------------------------------------------------------------------------
' Typed Interface only, so no data or procedure please!
'----------------------------------------------------------------------------
Public Sub Clear()
End Sub

Public Sub Add(oField As IField)
End Sub

Public Property Get Count() As Long
End Property

Public Property Get Field(nInstance As Integer) As IField
End Property

Friend Function Duplicate() As IRow
End Function

