VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IBoDatabase"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
' $Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/IBoDatabase.cls $
' $Revision: 23 $
' $Date: 24/03/03 9:55 $
' $Author: Mauricem $
'
'----------------------------------------------------------------------------
' When we save a BO to the database we have the options to save regardless,
' save only if it doesn't already exist, save only if is does exist or delete.
' This must match the same enum in modCommon.bas
Public Enum enSaveType
    SaveTypeAllCases = 0             ' Default value
    SaveTypeIfNew = 1
    SaveTypeIfExists = 2
    SaveTypeDelete = 3
'    SaveTypeSelective = 4            ' Update just the selected fields (if key exists)
'                                     ' SaveTypeSelective is dangerous, aoid if possible.
End Enum
Public Enum enDbErrorType
    DbErrorTypeNoError = 0
    DbErrorTypeUnknown = 1           ' Default value
    DbErrorTypeDuplicateKey = 2
End Enum
'----------------------------------------------------------------------------

Public Property Get Version() As String
End Property
Public Function Initialise(oSession As ISession, oSysRoot As ISysRoot, oEnterprise As IEnterprise) As IBoDatabase
    ' Although the session has an IOasysRoot interface within it, we also
    ' need the system interface as well.
End Function
Public Function GetView(RowSelector As IRowSelector) As IView
End Function
Public Function GetSelectView(oRow As IRowSelector, Optional RowSelector As IRowSelector) As String
End Function
Public Function GetLiteralView(RowSelector As IRowSelector, _
                               DataFormat As Long, _
                               sLineSeparator As String, _
                               sDataSeparator As String, _
                               sTextDelim As String) As String
End Function
Public Function GetBoCollection(RowSelector As IRowSelector) As Collection
End Function
Public Function GetSortedBoCollection(RowSelector As IRowSelector, SortKeys As IBoSortKeys) As Collection
End Function
Public Function GetBo(RowSelector As IRowSelector) As IBo
End Function
Public Function CreateBusinessObject(nClassId As Integer) As IBo
End Function
Public Function CreateBoSelector(Optional nFieldId As Long, Optional nComparator As enComparator, Optional Value As Variant) As IRowSelector
End Function
Public Static Function CreateBoFromRow(oRow As IRow) As IBo
End Function
Public Function SaveBo(eSave As enSaveType, oBo As IBo) As Boolean
    ' Note that to use SaveBo, you must have implemented IBo_GetAllFieldsAsRow()
End Function
Public Function SavePartialBo(oBo As IBo, oRow As IRow) As Boolean
End Function
Public Function DeleteBoCollection(RowSelector As IRowSelector) As Boolean
    ' The selector determines which Bos comprise the collection for
    ' deletion from persistent storage.
End Function
Public Property Get LastErrorType() As enDbErrorType
End Property
Public Function GetAggregateValue(nFunctionId As Integer, oField As IField, RowSelector As IRowSelector) As Variant
End Function
Public Function DoBulkUpdate(oTarget As Object, oSource As Object, oRowSelector As IRowSelector) As Boolean
' This routine requires the target class to support the IsBulkUpdateValid() method.
' Target and Source are either both IField objects or both CRow objects
' If the FID for target and source are the same, then the source provides the value,
' otherwise the source provides the FID.
End Function

Public Function ExecuteCommand(CommandText As String, Optional RecordsAffected As Long, Optional Options As Long) As ADODB.Recordset
End Function

Public Function StartTransaction() As Boolean
End Function

Public Function CommitTransaction() As Boolean
End Function
Public Property Get ConnectionString() As String
End Property

Public Function RollbackTransaction() As Boolean
End Function

Public Property Get Connection() As ADODB.Connection
End Property


