VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : IView
'* Date   : 28/09/04
'* Author : johng
'*$Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/IView.cls $
'----------------------------------------------------------------------------
' Typed Interface only, so no data or procedure please!
'----------------------------------------------------------------------------

'----------------------------------------------------------------------------
' Interface IView - Oasys View interface
'</CAMH>***************************************************************************************
Option Explicit

Public Sub Add(oRow As IRow)
End Sub
Public Property Get Count() As Long
End Property
Public Property Get Row(nInstance As Integer) As IRow
End Property


