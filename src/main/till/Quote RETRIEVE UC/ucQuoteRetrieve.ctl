VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{6514F5A0-641C-11D2-9FD0-0020AF131A57}#3.0#0"; "fpFlp30.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Object = "{9BA18739-054D-4172-8E42-118133CE2FC4}#1.0#0"; "EditDateCtl.ocx"
Begin VB.UserControl ucQuoteRetrieve 
   ClientHeight    =   7215
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11850
   KeyPreview      =   -1  'True
   ScaleHeight     =   7215
   ScaleWidth      =   11850
   Begin CTSProgBar.ucpbProgressBar ucpbProgress 
      Height          =   1860
      Left            =   3480
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   3000
      Visible         =   0   'False
      Width           =   5100
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      Object.Width           =   5100
      Object.Height          =   1860
   End
   Begin VB.PictureBox fraTran 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   6255
      Left            =   60
      ScaleHeight     =   6225
      ScaleWidth      =   11685
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   720
      Width           =   11715
      Begin VB.PictureBox picPictureBox2 
         BorderStyle     =   0  'None
         Height          =   615
         Left            =   60
         ScaleHeight     =   615
         ScaleWidth      =   11535
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   5460
         Width           =   11535
         Begin VB.CommandButton cmdList 
            Caption         =   "List"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   6660
            TabIndex        =   40
            Top             =   60
            Width           =   1515
         End
         Begin VB.CommandButton cmdOtherTran 
            Caption         =   "Tran at other Store"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   6660
            TabIndex        =   5
            Top             =   60
            Visible         =   0   'False
            Width           =   3015
         End
         Begin VB.CommandButton cmdReset 
            Caption         =   "Reset"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   0
            TabIndex        =   7
            Top             =   60
            Width           =   1455
         End
         Begin VB.CommandButton cmdCancel 
            Caption         =   "Cancel"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   9780
            TabIndex        =   6
            Top             =   60
            Width           =   1755
         End
         Begin VB.CommandButton cmdDetails 
            Caption         =   "OK"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   5160
            TabIndex        =   4
            Top             =   60
            Width           =   1395
         End
         Begin VB.CommandButton cmdNoProof 
            Caption         =   "No Proof of Purchase"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   1560
            TabIndex        =   8
            Top             =   60
            Visible         =   0   'False
            Width           =   3495
         End
      End
      Begin VB.Frame fraTranSearch 
         BorderStyle     =   0  'None
         Height          =   555
         Left            =   120
         TabIndex        =   13
         Top             =   120
         Width           =   11115
         Begin VB.TextBox txtTranNo 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   9960
            MaxLength       =   4
            TabIndex        =   3
            Text            =   "0000"
            Top             =   0
            Width           =   975
         End
         Begin VB.TextBox txtTillId 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   7320
            MaxLength       =   2
            TabIndex        =   2
            Text            =   "00"
            Top             =   0
            Width           =   615
         End
         Begin VB.TextBox txtStoreNo 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   1680
            MaxLength       =   3
            TabIndex        =   0
            Text            =   "000"
            Top             =   0
            Width           =   855
         End
         Begin ucEditDate.ucDateText dtxtTranDate 
            Height          =   555
            Left            =   3660
            TabIndex        =   1
            Top             =   0
            Width           =   1815
            _ExtentX        =   3201
            _ExtentY        =   979
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DateFormat      =   "DD/MM/YY"
            Text            =   "13/05/03"
         End
         Begin VB.Label Label2 
            Caption         =   "Store No"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   0
            TabIndex        =   14
            Top             =   0
            Width           =   1575
         End
         Begin VB.Label Label5 
            Caption         =   "Tran No"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   8400
            TabIndex        =   17
            Top             =   0
            Width           =   1455
         End
         Begin VB.Label Label4 
            Caption         =   "Till ID"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   6120
            TabIndex        =   16
            Top             =   0
            Width           =   1095
         End
         Begin VB.Label Label3 
            Caption         =   "Date"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   2760
            TabIndex        =   15
            Top             =   0
            Width           =   855
         End
      End
      Begin MSAdodcLib.Adodc adodcSearch 
         Height          =   330
         Left            =   8400
         Top             =   120
         Visible         =   0   'False
         Width           =   1200
         _ExtentX        =   2117
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "Adodc1"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.Frame fraTranID 
         Height          =   495
         Left            =   120
         TabIndex        =   21
         Top             =   120
         Visible         =   0   'False
         Width           =   4575
         Begin VB.Label Label1 
            Caption         =   "Tran ID"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   0
            TabIndex        =   22
            Top             =   0
            Width           =   1455
         End
      End
      Begin VB.Frame fraTranName 
         Caption         =   "Frame1"
         Height          =   495
         Left            =   120
         TabIndex        =   18
         Top             =   120
         Visible         =   0   'False
         Width           =   7695
         Begin VB.TextBox txtName 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   1680
            MaxLength       =   40
            TabIndex        =   19
            Top             =   0
            Width           =   4935
         End
         Begin VB.Label Label6 
            Caption         =   "Name"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   0
            TabIndex        =   20
            Top             =   0
            Width           =   1215
         End
      End
   End
   Begin VB.PictureBox fraDetails 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   6255
      Left            =   60
      ScaleHeight     =   6225
      ScaleWidth      =   11685
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      Width           =   11715
      Begin FPSpreadADO.fpSpread sprdDetails 
         Height          =   4605
         Left            =   60
         TabIndex        =   41
         Top             =   780
         Width           =   11535
         _Version        =   458752
         _ExtentX        =   20346
         _ExtentY        =   8123
         _StockProps     =   64
         ColsFrozen      =   2
         DisplayRowHeaders=   0   'False
         EditModeReplace =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   10
         MaxRows         =   1
         SpreadDesigner  =   "ucQuoteRetrieve.ctx":0000
         UserResize      =   0
      End
      Begin VB.PictureBox picPictureBox1 
         BorderStyle     =   0  'None
         Height          =   615
         Left            =   60
         ScaleHeight     =   615
         ScaleWidth      =   11475
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   5400
         Width           =   11475
         Begin VB.CommandButton cmdRetrieveAll 
            Caption         =   "Retrieve All"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   2460
            TabIndex        =   26
            Top             =   120
            Width           =   2235
         End
         Begin VB.CommandButton cmdDtlRetrieve 
            Caption         =   "Retrieve"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   0
            TabIndex        =   25
            Top             =   120
            Width           =   1995
         End
         Begin VB.CommandButton cmdDtlCancel 
            Caption         =   "Cancel"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   9540
            TabIndex        =   27
            Top             =   120
            Width           =   1935
         End
      End
      Begin VB.Label Label12 
         Caption         =   "Store No"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   35
         Top             =   180
         Width           =   1575
      End
      Begin VB.Label Label11 
         Caption         =   "Date"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2880
         TabIndex        =   34
         Top             =   180
         Width           =   855
      End
      Begin VB.Label Label10 
         Caption         =   "Till ID"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6360
         TabIndex        =   33
         Top             =   180
         Width           =   1095
      End
      Begin VB.Label Label9 
         Caption         =   "Tran No"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   8640
         TabIndex        =   32
         Top             =   180
         Width           =   1455
      End
      Begin VB.Label lblStoreNo 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1800
         TabIndex        =   31
         Top             =   180
         Width           =   855
      End
      Begin VB.Label lblTillID 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7560
         TabIndex        =   30
         Top             =   180
         Width           =   615
      End
      Begin VB.Label lblTranNo 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   10200
         TabIndex        =   29
         Top             =   180
         Width           =   1095
      End
      Begin VB.Label lblTranDate 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   3840
         TabIndex        =   28
         Top             =   180
         Width           =   2175
      End
   End
   Begin VB.PictureBox fraParked 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   6255
      Left            =   60
      ScaleHeight     =   6225
      ScaleWidth      =   11685
      TabIndex        =   36
      TabStop         =   0   'False
      Top             =   780
      Width           =   11715
      Begin LpADOLib.fpListAdo lstParked 
         Height          =   5130
         Left            =   240
         TabIndex        =   39
         Top             =   240
         Width           =   11235
         _Version        =   196608
         _ExtentX        =   19817
         _ExtentY        =   9049
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   0   'False
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Columns         =   5
         Sorted          =   1
         LineWidth       =   1
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         MultiSelect     =   0
         WrapList        =   0   'False
         WrapWidth       =   0
         SelMax          =   -1
         AutoSearch      =   1
         SearchMethod    =   0
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   1
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   2
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   2
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   -1  'True
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         DataField       =   ""
         DataMember      =   ""
         OLEDragMode     =   0
         OLEDropMode     =   0
         EnableClickEvent=   -1  'True
         Redraw          =   -1  'True
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         ColDesigner     =   "ucQuoteRetrieve.ctx":0707
      End
      Begin VB.CommandButton cmdParkRetrieve 
         Caption         =   "Select"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   240
         TabIndex        =   38
         Top             =   5520
         Width           =   1995
      End
      Begin VB.CommandButton cmdParkCancel 
         Caption         =   "Cancel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   9540
         TabIndex        =   37
         Top             =   5520
         Width           =   1935
      End
   End
   Begin VB.Label lblAction 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Retrieve original transaction (Enter details/Scan barcode)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   60
      TabIndex        =   9
      Top             =   180
      Width           =   11715
   End
End
Attribute VB_Name = "ucQuoteRetrieve"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>**************************************************************************************
'* Module : ucTillRetrieve
'* Date   : 01/10/03
'* Author : mauricem
' $Archive: /Projects/OasysV2/VB/Common/ucTillRetrieve.ctl $
'********************************************************************************************
'* Summary: User control used by the till to retrieve existing transactions from the database
'********************************************************************************************
'* $Revision: 28 $ $ Date: 1/22/03 5:21p $ $Author: Mauricem $
'* Versions:
'* 01/10/03    mauricem
'*             Header added.
'</CAMH>*************************************************************************************
Option Explicit

Const MODULE_NAME As String = "ucTillRetrieve"

Const COL_DTL_SKU As Long = 1
Const COL_DTL_DESC As Long = 2
Const COL_DTL_QTY As Long = 3
Const COL_DTL_PRICE As Long = 4
Const COL_DTL_TOTAL As Long = 5
Const COL_DTL_LINENO As Long = 6
Const COL_DTL_QTY_RFND As Long = 7
Const COL_DTL_QTY_REASON As Long = 8
Const COL_DTL_RFND_PRICE As Long = 9

Dim mblnInit        As Boolean 'flag to ensure initialise can only be called once
Dim moParent        As Object
Dim mblnReload      As Boolean
Dim mblnParkedOnly  As Boolean
Dim mblnPreview     As Boolean
Dim mblnSelectLine  As Boolean
Dim mblnSalesOnly   As Boolean 'used to set Selection criteria
Dim mblnVerifySKU   As Boolean

Dim mstrPriceSKU    As String
Dim mlngPriceQty    As Long
Dim mdtePPValidFrom As Date

Dim mstrActionMsg   As String

Dim mlngSearchKeyCode As Long
Dim mlngResetKeyCode  As Long
Dim mlngCloseKeyCode  As Long
Dim mlngUseKeyCode    As Long
Dim mlngDetailKeyCode As Long
Dim mlngOtherKeyCode  As Long
Dim mlngVoidKeyCode   As Long
Dim mlngTenderKeyCode As Long
Dim mlngListKeyCode   As Long
Dim mlngRetAllKeyCode As Long
Dim mlngNoProofKeyCode As Long

Dim mcolLines   As Collection

Dim mstrRefundCodes As String
Dim mstrUseReason   As String



Public AllowScan As Boolean
Public Event Apply(StoreNumber As String, Trandate As Date, TillID As String, TranNumber As String)
Public Event Cancel()
Public Event NoOriginalTran(RefundReasonCode As String)
Public Event OtherStoreTran(StoreNumber As String, Trandate As Date, TillID As String, TranNumber As String, RefundReasonCode As String)
Public Event SelectTransactionLine(ByRef strPartCode As String, _
                                   ByRef dblQuantity As Double, _
                                   ByRef dblPrice As Double, _
                                   ByRef strTranID As String, _
                                   ByRef dteTrandate As Date, _
                                   ByRef strTillID As String, _
                                   ByRef lngLineNo As Long, _
                                   ByRef blnVoided As Boolean, _
                                   ByRef blnTendered As Boolean)

Public Event SelectTransactionLines(ByRef strTranID As String, _
                                   ByRef dteTrandate As Date, _
                                   ByRef strTillID As String, _
                                   ByRef colLines As Collection)

Public RefundReasons As Collection

Public PriceOverrideCodes As Collection

Public Sub FillScreen(ByRef ucMe As Object)

    'Move user control to top-left corner of Form
    ucMe.Left = 0
    ucMe.Top = 0
    'Match control size to container form
    UserControl.Width = moParent.Width
    UserControl.Height = moParent.Height
    'Move frame to center of screen
    lblAction.Left = ((UserControl.Width - fraTran.Width) / 2) - 80
    lblAction.Top = ((UserControl.Height - (lblAction.Height + fraTran.Height)) / 2) - 80
    fraTran.Left = lblAction.Left
    fraTran.Top = ((UserControl.Height - (lblAction.Height + fraTran.Height)) / 2) + lblAction.Height
    fraDetails.Top = fraTran.Top
    fraDetails.Left = fraTran.Left
    fraParked.Top = fraTran.Top
    fraParked.Left = fraTran.Left

End Sub

Public Sub AllowVoid(ByVal blnShowVoid As Boolean)

'    cmdDtlVoid.Visible = blnShowVoid

End Sub

Public Sub AllowTender(ByVal blnShowTender As Boolean)

'    cmdDtlTender.Visible = blnShowTender

End Sub

Public Sub Reset()

    txtStoreNo.Text = goSession.CurrentEnterprise.IEnterprise_StoreNumber
    dtxtTranDate.Text = Format$(Now(), "DD/MM/YY")
    txtTillId.Text = vbNullString
    txtTranNo.Text = vbNullString
    fraDetails.Visible = False
    fraParked.Visible = False
    fraTran.Visible = True
    AllowScan = True
    cmdReset.Visible = False
    mblnSalesOnly = False
    
End Sub

Public Sub Show()
    
    If Not moParent Is Nothing Then
        mblnPreview = moParent.KeyPreview
        moParent.KeyPreview = False
    Else
        Call DebugMsg(MODULE_NAME, "UserControl_Show", endlDebug, "No Parent to set KeyPreview")
    End If
    txtTranNo.SetFocus

End Sub

Public Property Let ParkedOnly(Value As Boolean)
    
    If Value = True Then
        mstrActionMsg = "Retrieve parked transaction (Enter details/Scan barcode)"
        dtxtTranDate.Text = Format$(Now(), "DD/MM/YY")
        lblStoreNo.Caption = txtStoreNo.Text
        dtxtTranDate.Enabled = False
        txtStoreNo.Enabled = False
        cmdOtherTran.Visible = False
        cmdNoProof.Visible = False
        cmdList.Visible = True
        mblnParkedOnly = True
    Else
        mstrActionMsg = "Retrieve original transaction (Enter details/Scan barcode)"
        dtxtTranDate.Text = Format$(Now(), "DD/MM/YY")
        txtStoreNo.Text = vbNullString
        dtxtTranDate.Enabled = True
        txtStoreNo.Enabled = True
        cmdNoProof.Visible = True
        cmdOtherTran.Visible = True
        cmdList.Visible = False
        mblnParkedOnly = False
    End If
    lblAction.Caption = mstrActionMsg

End Property

Public Property Let PricePromiseSKU(Value As String)
    
    mstrPriceSKU = Value
    If (Value = "") Then
        mblnVerifySKU = False
    Else
        mblnVerifySKU = True
        mdtePPValidFrom = DateAdd("d", goSession.GetParameter(PRM_PRICEMATCHDAYS) * -1, Date)
    End If
    

End Property

Public Property Let PricePromiseQty(Value As Long)
    
    mlngPriceQty = Value

End Property

Public Property Get PricePromiseValidQty() As Long
    
    PricePromiseValidQty = mlngPriceQty

End Property

Public Sub Initialise(ByRef CurrentSession As Object, Optional ByRef ParentForm As Object = Nothing)

Dim oBOCol  As Collection
Dim lItemNo As Long
Dim strKey  As String

    If mblnInit Then Exit Sub
    ' Set up the object and field that we wish to select on
    
    Screen.MousePointer = vbHourglass
    Set moParent = ParentForm
    
    Set goSession = CurrentSession
    Set goDatabase = goSession.Database
    
    Call CreateErrorObject(SYSTEM_NAME & "{" & App.Title & "}", VBA.Err, Err)
    
    UserControl.BackColor = goSession.GetParameter(PRM_QUERY_BORDERCOLOUR)
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    RGBQuery_BackColour = goSession.GetParameter(PRM_QUERY_BACKCOLOUR)
    
    lstParked.BackColor = goSession.GetParameter(PRM_QUERY_GRIDEVENCOLOUR)
    sprdDetails.GrayAreaBackColor = lstParked.BackColor
    lstParked.ListApplyTo = ListApplyToEvenRows
    lstParked.BackColor = goSession.GetParameter(PRM_QUERY_GRIDEVENCOLOUR)
    lstParked.ListApplyTo = ListApplyToOddRows
    lstParked.BackColor = goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR)
    lstParked.Row = -1
    lstParked.RowHeight = goSession.GetParameter(PRM_QUERY_ROWHEIGHT)
    Call sprdDetails.SetOddEvenRowColor(goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR), vbBlack, goSession.GetParameter(PRM_QUERY_GRIDEVENCOLOUR), vbBlack)
    
    mblnVerifySKU = goSession.GetParameter(PRM_REFUND_VERIFY_SKU)

    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
    RGBMSGBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
        
    strKey = goSession.GetParameter(PRM_KEY_RESET)
    cmdReset.Caption = strKey & "-" & cmdReset.Caption
    If Left$(strKey, 1) = "F" Then
        mlngResetKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngResetKeyCode = Asc(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_CLOSE)
    cmdCancel.Caption = strKey & "-" & cmdCancel.Caption
    cmdDtlCancel.Caption = strKey & "-" & cmdDtlCancel.Caption
    cmdParkCancel.Caption = strKey & "-" & cmdParkCancel.Caption
    If Left$(strKey, 1) = "F" Then
        mlngCloseKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngCloseKeyCode = Asc(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_DETAILS)
    cmdDetails.Caption = strKey & "-" & cmdDetails.Caption
    If Left$(strKey, 1) = "F" Then
        mlngDetailKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngDetailKeyCode = Asc(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_REVERSAL)
    cmdOtherTran.Caption = strKey & "-" & cmdOtherTran.Caption
    If Left$(strKey, 1) = "F" Then
        mlngOtherKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngOtherKeyCode = Asc(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_SAVE)
    cmdDtlRetrieve.Caption = strKey & "-" & cmdDtlRetrieve.Caption
    cmdParkRetrieve.Caption = strKey & "-" & cmdParkRetrieve.Caption
    If Left$(strKey, 1) = "F" Then
        mlngUseKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngUseKeyCode = Asc(UCase$(strKey))
    End If
    mstrUseReason = strKey & "-Process"

    strKey = goSession.GetParameter(PRM_KEY_LOOKUP)
    cmdList.Caption = strKey & "-" & cmdList.Caption
    cmdRetrieveAll.Caption = strKey & "-" & cmdRetrieveAll.Caption
    cmdNoProof.Caption = strKey & "-" & cmdNoProof.Caption
    If Left$(strKey, 1) = "F" Then
        mlngListKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngListKeyCode = Asc(UCase$(strKey))
    End If
    mlngRetAllKeyCode = mlngListKeyCode
    mlngNoProofKeyCode = mlngListKeyCode
'    strKey = goSession.GetParameter(PRM_KEY_VOID)
'    cmdDtlVoid.Caption = strKey & "-" & cmdDtlVoid.Caption
'    If Left$(strKey, 1) = "F" Then
'        mlngVoidKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
'    Else
'        mlngVoidKeyCode = Asc(UCase$(strKey))
'    End If
'
'    strKey = goSession.GetParameter(PRM_KEY_TENDER)
'    cmdDtlTender.Caption = strKey & "-" & cmdDtlTender.Caption
'    If Left$(strKey, 1) = "F" Then
'        mlngTenderKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
'    Else
'        mlngTenderKeyCode = Asc(UCase$(strKey))
'    End If
    

    cmdReset.Visible = False
    cmdDetails.Visible = False
    mblnInit = True
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdDetails_Click()
    
Dim strLineData   As String
Dim strStoreNo    As String
Dim dteTrandate   As Date
Dim strTillID     As String
Dim strTranNumber As String
Dim strName       As String
Dim strDesc       As String
Dim lngNoLines    As Long
Dim lngLineNo     As Long
Dim lngXtraSpace  As Long
Dim lngNoCols     As Long
Dim colLines      As Collection
Dim oTranLine     As cPOSLine
Dim oTillTran     As cPOSHeader
Dim oStockItem    As cInventory
Dim strSKU        As String
Dim blnCancel     As Boolean
Dim lngBoughtQty  As Long
    
    If (mstrRefundCodes = "") Then Call SplitRefundCodes
'    If (mstrOPriceCodes = "") Then Call SplitPriceCodes
    
    cmdDetails.SetFocus
    DoEvents
    cmdDetails.Enabled = False
    'Added 13/3/04 - extra security check to enter sku for refund against selected transaction
    If (mblnVerifySKU = True) And (mstrPriceSKU = "") Then
        strSKU = InputBox("Enter SKU for refunded item", "SKU Validation", vbNullString)
        If LenB(strSKU) = 0 Then Exit Sub
        If Len(strSKU) < PARTCODE_LEN Then strSKU = Left$(PARTCODE_PAD, PARTCODE_LEN - Len(strSKU)) & strSKU
    End If
    
    If (mstrPriceSKU <> "") Then
        strSKU = mstrPriceSKU
    End If
    
    
    If mblnParkedOnly Then
        If dtxtTranDate.Text <> Format$(Now(), "DD/MM/YY") Then
            AllowScan = False
            Call MsgBoxEx("Transaction must be dated today", vbExclamation, "Recall Parked Transaction", , , , , RGBMsgBox_WarnColour)
            AllowScan = True
            dtxtTranDate.Text = Format$(Now(), "DD/MM/YY")
            cmdDetails.Enabled = True
            Exit Sub
        End If
    End If
    
    'Extract unique look up details for transaction
    strTillID = txtTillId.Text
    dteTrandate = CDate(dtxtTranDate.Text)
    strTranNumber = txtTranNo.Text
    strStoreNo = txtStoreNo.Text
    
    If strStoreNo <> Right$("000" & goSession.CurrentEnterprise.IEnterprise_StoreNumber, 3) Then
        If mblnParkedOnly Then
            AllowScan = False
            Call MsgBoxEx("Transaction from another store", vbOKOnly, "Display Transaction Details", , , , , RGBMSGBox_PromptColour)
            AllowScan = True
            Exit Sub
        Else
            'if Refunding and Other Store Number entered, then inform User and Click on Other Store Tran
            AllowScan = False
            Call MsgBoxEx("Transaction from another store", vbOKOnly, "Display Transaction Details", , , , , RGBMSGBox_PromptColour)
            AllowScan = True
            cmdOtherTran.Value = True
            cmdDetails.Enabled = True
            Exit Sub
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    
    'Retrieve Transaction Header
    Set oTillTran = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    Call oTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, strTillID)
    Call oTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, dteTrandate)
    Call oTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, strTranNumber)
    'Call oTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_StoreNumber, strStoreNo)
    If oTillTran.LoadMatches.Count > 0 Then
        If mblnParkedOnly Then
            If Not oTillTran.TranParked Then
                AllowScan = False
                Call MsgBoxEx("Specified transaction was not parked", vbOKOnly, "Display Transaction Details", , , , , RGBMsgBox_WarnColour)
                AllowScan = True
                txtTillId.SetFocus
                
                Screen.MousePointer = vbNormal
                cmdDetails.Enabled = True
                Exit Sub
            End If
        Else
            If oTillTran.TranParked = True Then
                AllowScan = False
                Call MsgBoxEx("Specified transaction was a parked transaction", vbOKOnly, "Display Transaction Details", , , , , RGBMsgBox_WarnColour)
                AllowScan = True
                txtTillId.SetFocus
                
                Screen.MousePointer = vbNormal
                cmdDetails.Enabled = True
                Exit Sub
            End If
            
            If oTillTran.Voided = True Then
                AllowScan = False
                Call MsgBoxEx("Specified transaction was voided", vbOKOnly, "Display Transaction Details", , , , , RGBMsgBox_WarnColour)
                AllowScan = True

                txtTillId.SetFocus
                
                Screen.MousePointer = vbNormal
                cmdDetails.Enabled = True
                Exit Sub
            End If
            
            If oTillTran.TrainingMode = True Then
                AllowScan = False
                Call MsgBoxEx("Specified transaction was a training-mode transaction", vbOKOnly, "Display Transaction Details", , , , , RGBMsgBox_WarnColour)
                AllowScan = True

                txtTillId.SetFocus
                
                Screen.MousePointer = vbNormal
                cmdDetails.Enabled = True
                Exit Sub
            End If
        End If
    End If
    
    If (oTillTran.Lines.Count = 0) Or (oTillTran.TillID <> txtTillId.Text) Then
        AllowScan = False
        If (oTillTran.TillID <> txtTillId.Text) Then
            If (goSession.ISession_IsOnline = True) Then
                Call MsgBoxEx("Transaction could not be found - check entered details.", vbOKOnly, "Display Transaction Details", , , , , RGBMsgBox_WarnColour)
            Else
                If (MsgBoxEx("Transaction could not be found - check entered details." & vbNewLine & "Currently Offline - Confirm transaction details are correct", vbYesNo, "Invalid Transaction Details", , , , , RGBMsgBox_WarnColour) = vbYes) Then
                    cmdOtherTran.Tag = "SKIP"
                    cmdOtherTran.Value = True
                    Exit Sub
                End If
            End If
        Else
            Call MsgBoxEx("No line items available for display", vbOKOnly, "Display Transaction Details", , , , , RGBMSGBox_PromptColour)
        End If
        AllowScan = True
        If txtStoreNo.Enabled = True Then
            txtStoreNo.SetFocus
        ElseIf dtxtTranDate.Enabled Then
            dtxtTranDate.SetFocus
        Else
            txtTillId.SetFocus
        End If
        
        Screen.MousePointer = vbNormal
        cmdDetails.Enabled = True
        Exit Sub
    End If
    lblStoreNo.Caption = txtStoreNo.Text
    lblTranDate.Caption = DisplayDate(dteTrandate, False)
    lblTranNo.Caption = strTranNumber
    lblTillID.Caption = strTillID
    If (mblnSelectLine = True) Then lblAction.Caption = "Select line to refund"
    If ((mblnSelectLine = True) And (mstrPriceSKU <> "")) Or (mblnParkedOnly = True) Then
        If (mblnParkedOnly = False) Then lblAction.Caption = "Select line for Price Match/Promise"
        sprdDetails.Col = COL_DTL_QTY_RFND
        If (sprdDetails.ColHidden = False) Then
            lngXtraSpace = sprdDetails.ColWidth(COL_DTL_QTY_RFND) + sprdDetails.ColWidth(COL_DTL_QTY_REASON) + _
                 sprdDetails.ColWidth(COL_DTL_RFND_PRICE)
        End If
        sprdDetails.ColHidden = True
        sprdDetails.Col = COL_DTL_QTY_REASON
        sprdDetails.ColHidden = True
        sprdDetails.Col = COL_DTL_RFND_PRICE
        sprdDetails.ColHidden = True
        cmdRetrieveAll.Visible = False
        sprdDetails.OperationMode = OperationModeRow
        
        'If first time columns hidden then allocate space to other columns
        If (lngXtraSpace > 0) Then
            'get the number of visible columns to split the extra space up into
            For lngLineNo = 1 To sprdDetails.MaxCols Step 1
                sprdDetails.Col = lngLineNo
                If (sprdDetails.ColHidden = False) Then lngNoCols = lngNoCols + 1
            Next lngLineNo
            'split the extra space into the visible columns, except SKU which is added to Description
            'sprdDetails.ColWidth(COL_skuESC) = (lngXtraSpace / lngNoCols) + sprdDetails.ColWidth(COL_DESC)
            For lngLineNo = COL_PARTCODE To sprdDetails.MaxCols Step 1
                sprdDetails.Col = lngLineNo
                If (sprdDetails.ColHidden = False) Then sprdDetails.ColWidth(lngLineNo) = (lngXtraSpace / lngNoCols) + sprdDetails.ColWidth(lngLineNo)
            Next lngLineNo
        End If
            
    End If
        
    
    'Display lines on Receipt
    sprdDetails.MaxRows = 0
    sprdDetails.LeftCol = 1
    Set colLines = oTillTran.Lines
    lngNoLines = colLines.Count
    For lngLineNo = 1 To lngNoLines Step 1
        Set oTranLine = colLines(lngLineNo)
        With oTranLine
            If ((mblnVerifySKU = True) And (LenB(strSKU) <> 0) And (strSKU = .PartCode)) Or _
                  (mblnVerifySKU = False) Then
                If ((.QuantitySold > 0) Or mblnParkedOnly) And (Not .LineReversed) Then
                    Set oStockItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
                    Call oStockItem.IBo_AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, .PartCode)
                    Call oStockItem.IBo_AddLoadField(FID_INVENTORY_Description)
                    Call oStockItem.IBo_LoadMatches
                    strDesc = oStockItem.Description
                    sprdDetails.MaxRows = sprdDetails.MaxRows + 1
                    sprdDetails.Row = sprdDetails.MaxRows
                    sprdDetails.Col = COL_DTL_SKU
                    sprdDetails.Text = .PartCode
                    sprdDetails.Col = COL_DTL_DESC
                    sprdDetails.Text = strDesc
                    sprdDetails.Col = COL_DTL_QTY
                    sprdDetails.Text = .QuantitySold
                    If (lngBoughtQty = 0) Then lngBoughtQty = .QuantitySold
                    sprdDetails.Col = COL_DTL_PRICE
                    sprdDetails.Text = Format$((.ExtendedValue - .DealGroupMarginAmount - .MultiBuyMarginAmount - .HierarchyMarginAmount - .QtyBreakMarginAmount) / .QuantitySold, "0.00")
                    sprdDetails.Col = COL_DTL_TOTAL
                    sprdDetails.Text = Format$((.ExtendedValue - .DealGroupMarginAmount - .MultiBuyMarginAmount - .HierarchyMarginAmount - .QtyBreakMarginAmount), "0.00")
                    sprdDetails.Col = COL_DTL_LINENO
                    sprdDetails.Text = .SequenceNo
                    sprdDetails.Col = COL_DTL_QTY_RFND
                    sprdDetails.Text = "0"
                    sprdDetails.Col = COL_DTL_QTY_REASON
                    sprdDetails.Text = ""
                    sprdDetails.RowHeight(sprdDetails.MaxRows) = sprdDetails.MaxTextRowHeight(sprdDetails.MaxRows)
                End If
            End If 'item must be displayed
        End With
    Next lngLineNo
    If (mblnVerifySKU = True) And (LenB(strSKU) <> 0) And (mlngPriceQty > lngBoughtQty) Then
        Call DebugMsg(MODULE_NAME, "cmdDetails_click", endlDebug, "Verify SKU" & strSKU & " Qty=" & lngBoughtQty)
        AllowScan = False
        Call MsgBoxEx("There was only " & lngBoughtQty & " of SKU - '" & strSKU & "' purchased on the selected receipt" & vbCrLf & "Quantity has been adjusted on Price Match to Purchased Quantity", vbOKOnly, "Price Match Quantity Exceeded", , , , , RGBMsgBox_WarnColour)
        AllowScan = True
        mlngPriceQty = lngBoughtQty
    End If
    If (lngNoLines > 0) And (sprdDetails.MaxRows = 0) Then
        If mblnVerifySKU Then
            AllowScan = False
            Call MsgBoxEx("SKU - '" & strSKU & "' was not present on selected receipt" & vbCrLf & "Reselect transaction and enter SKU", vbOKOnly, "Entered SKU not verified", , , , , RGBMsgBox_WarnColour)
            AllowScan = True
        Else
            AllowScan = False
            Call MsgBoxEx("No sale lines present in selected transaction" & vbCrLf & "Reselect transaction", vbOKOnly, "Invalid Transaction", , , , , RGBMsgBox_WarnColour)
            AllowScan = True
            Screen.MousePointer = vbNormal
            txtTillId.Text = vbNullString
            txtTranNo.Text = vbNullString
            txtTillId.SetFocus
            cmdDetails.Enabled = True
            Exit Sub
        End If
    End If
    Screen.MousePointer = vbNormal
    If (sprdDetails.MaxRows > 0) Then
        fraDetails.Visible = True
        fraParked.Visible = False
        fraTran.Visible = False
        AllowScan = False
        'DoEvents
        sprdDetails.Col = COL_DTL_QTY_RFND
        If (sprdDetails.ColHidden = False) Then Call sprdDetails_LeaveCell(COL_DTL_QTY_RFND, 1, COL_DTL_QTY_RFND, 1, blnCancel)
        Call sprdDetails.SetActiveCell(COL_DTL_QTY_RFND, 1)
        If (sprdDetails.Visible = True) Then
            If sprdDetails.Enabled = True Then Call sprdDetails.SetFocus
        End If
    End If
    If (mblnVerifySKU = True) And (sprdDetails.MaxRows = 1) Then
        fraDetails.Visible = False
        fraParked.Visible = False
        fraTran.Visible = True
        AllowScan = True
        Call sprdDetails.SetActiveCell(1, 1)
        cmdDtlRetrieve.Value = True
    End If
    
    If mblnParkedOnly Then
        If oTillTran.RecoveredFromParked Then
            AllowScan = False
            Call MsgBoxEx("Transaction has already been recovered" & vbNewLine & "Select another transaction", vbOKOnly, "Entered SKU not verified", , , , , RGBMsgBox_WarnColour)
            AllowScan = True
            cmdDtlCancel.Value = True
            cmdDetails.Enabled = True
            Exit Sub
        End If
    End If
    
    cmdDetails.Enabled = True

End Sub

Private Sub cmdDtlCancel_Click()

    lblAction.Caption = mstrActionMsg
    fraDetails.Visible = False
    fraParked.Visible = False
    fraTran.Visible = True
    AllowScan = True
    If txtStoreNo.Enabled Then
        txtStoreNo.SetFocus
    ElseIf dtxtTranDate.Enabled Then
        dtxtTranDate.SetFocus
    Else
        txtTillId.SetFocus
    End If
    
End Sub

Private Sub cmdDtlRetrieve_Click()

Dim strLineData   As String
Dim strStoreNo    As String
Dim strPartCode   As String
Dim dblQuantity   As Double
Dim curPrice      As Currency
Dim dteTrandate   As Date
Dim strTillID     As String
Dim strTranNumber As String
Dim lngNoLines    As Long
Dim lngLineNo     As Long
    
    cmdDtlRetrieve.Enabled = False
    lblAction.Caption = mstrActionMsg
    If (mblnSelectLine = True) Then
        If (mstrPriceSKU <> "") Then
            sprdDetails.Row = sprdDetails.ActiveRow
            sprdDetails.Col = COL_DTL_SKU
            strPartCode = sprdDetails.Text
            sprdDetails.Col = COL_DTL_PRICE
            curPrice = Val(sprdDetails.Value)
            RaiseEvent SelectTransactionLine(strPartCode, 1, CDbl(curPrice), lblTranNo.Caption, GetDate(lblTranDate.Caption), lblTillID.Caption, 1, False, False)
        Else
            If (GetRefundItems = True) Then
                If mcolLines.Count = 0 Then
                    Call MsgBoxEx("No Refund quantities has been entered against any of the lines.", vbOKOnly, "No quantities entered", , , , , RGBMsgBox_WarnColour)
                    cmdDtlRetrieve.Enabled = True
                    Exit Sub
                End If
                RaiseEvent SelectTransactionLines(lblTranNo.Caption, GetDate(lblTranDate.Caption), lblTillID.Caption, mcolLines)
            Else
                cmdDtlRetrieve.Enabled = True
                Exit Sub
            End If
        End If
    Else
        RaiseEvent Apply(lblStoreNo.Caption, GetDate(lblTranDate.Caption), lblTillID.Caption, lblTranNo.Caption)
    End If
    cmdDtlRetrieve.Enabled = True

End Sub

Private Sub cmdDtlTender_Click()
        
    RaiseEvent SelectTransactionLine(vbNullString, 0, 0, vbNullString, Date, vbNullString, 0, False, True)

End Sub

Private Sub cmdDtlVoid_Click()

    RaiseEvent SelectTransactionLine(vbNullString, 0, 0, vbNullString, Date, vbNullString, 0, True, False)

End Sub

Private Sub cmdList_Click()
    
Dim lngNoTrans As Long
Dim lngLineNo  As Long
Dim colTrans   As Collection
Dim oTillTran  As cPOSHeader
Dim oRetCust   As cReturnCust
Dim colCusts   As Collection
Dim strCusName As String
    
    cmdList.SetFocus
    'DoEvents
    
    Screen.MousePointer = vbHourglass

    Set oTillTran = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    Call oTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, Date)
    Call oTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranParked, True)
    Call oTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_RecoveredFromParked, False)
    Call oTillTran.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_FromDCOrders, True)
    Call oTillTran.AddLoadField(FID_POSHEADER_TranDate)
    Call oTillTran.AddLoadField(FID_POSHEADER_TillID)
    Call oTillTran.AddLoadField(FID_POSHEADER_TransactionNo)
    Call oTillTran.AddLoadField(FID_POSHEADER_TransactionTime)
    Call oTillTran.AddLoadField(FID_POSHEADER_OrderNumber)
    Set colTrans = oTillTran.LoadMatches
    
    If colTrans.Count = 0 Then
        AllowScan = False
        Call MsgBoxEx("No parked transactions available", vbOKOnly, "Retrieve Parked Transaction", , , , , RGBMsgBox_WarnColour)
        AllowScan = True
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    fraDetails.Visible = False
    fraParked.Visible = True
    fraTran.Visible = False
    AllowScan = False
    
    Set oRetCust = goDatabase.CreateBusinessObject(CLASSID_RETURNCUST)
    Call lstParked.Clear
    For Each oTillTran In colTrans
        With oTillTran
            Call oRetCust.ClearLoadFilter
            Call oRetCust.ClearLoadField
            
            Call oRetCust.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionDate, .Trandate)
            Call oRetCust.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_PCTillID, .TillID)
            Call oRetCust.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionNumber, .TransactionNo)
            Call oRetCust.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_LineNumber, 0)
            
            Call oRetCust.IBo_AddLoadField(FID_RETURNCUST_CustomerName)
            
            'Added 26/4/05 - to handle if DLRCUS record missing then use 'No Name' instead
            Set colCusts = oRetCust.IBo_LoadMatches
            If (colCusts.Count > 0) Then
                Set oRetCust = oRetCust.IBo_LoadMatches.Item(1)
                strCusName = oRetCust.CustomerName
            Else
                strCusName = "No Name"
            End If
            
            Call lstParked.AddItem(strCusName & vbTab & .TillID & vbTab & _
                .TransactionNo & vbTab & Format$(Left$(.TransactionTime, 2) & _
                ":" & Mid$(.TransactionTime, 3, 2), "H:MM") & vbTab & .OrderNumber)
        End With
    Next oTillTran
    lstParked.ListIndex = 0
    lstParked.SetFocus
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdNoProof_Click()

Dim strCode As String
        
    If ((mblnSelectLine = True) And (mstrPriceSKU <> "")) Then
        Call MsgBoxEx("Price Match/Promise is not allowed with no Proof of Purchase", vbOKOnly, _
            "No Proof of Purchase", , , , , RGBMSGBox_PromptColour)
        Exit Sub
    End If
    
    Load frmRefundCode
    frmRefundCode.CloseKeyCode = mlngCloseKeyCode
    frmRefundCode.UseKeyCode = mlngUseKeyCode
    strCode = frmRefundCode.GetRefundReasonCode(cmdDtlCancel.Caption, RefundReasons)
    Unload frmRefundCode
    If (strCode = "") Then Exit Sub
    
    RaiseEvent NoOriginalTran(strCode)

End Sub

Private Sub cmdOtherTran_Click()

Dim strCode  As String

Dim lngQty As Long
Dim curPrice As Currency

    
    If (txtStoreNo.Text = Right$("000" & goSession.CurrentEnterprise.IEnterprise_StoreNumber, 3)) And (cmdOtherTran.Tag <> "SKIP") Then
        Call MsgBoxEx("Enter originating store number for Transaction", vbOKOnly, "Capture transaction details", , , , , RGBMsgBox_WarnColour)
        txtStoreNo.SetFocus
        Exit Sub
    End If
    cmdOtherTran.Tag = ""
    
    'for Price Match/Promise - ensure Tran Details filled in and get Price
    If (mstrPriceSKU <> "") And (GetDate(dtxtTranDate.Text) < mdtePPValidFrom) Then
        Call MsgBoxEx("Transaction Date is before acceptable limit for Price Match/Promise", vbOKOnly, "Capture transaction details", , , , , RGBMsgBox_WarnColour)
        dtxtTranDate.SetFocus
        Exit Sub
    End If
    
    If (txtTillId.Text = "") Then
        Call MsgBoxEx("Till number for originating transaction not entered.", vbOKOnly, "Capture transaction details", , , , , RGBMsgBox_WarnColour)
        txtTillId.SetFocus
        Exit Sub
    End If
    If (txtTranNo.Text = "") Then
        Call MsgBoxEx("Transaction number for originating transaction not entered.", vbOKOnly, "Capture transaction details", , , , , RGBMsgBox_WarnColour)
        txtTranNo.SetFocus
        Exit Sub
    End If
    
    If (mstrPriceSKU <> "") Then 'performing price promise validation
        'After check mades, prompt to get unit price
        curPrice = Val(InputBoxEx("Enter original unit price from receipt for item.", "Confirm price", "0.00", enifNumeric, False, vbYellow, -2, 7))
        If (curPrice = 0) Then Exit Sub
        RaiseEvent SelectTransactionLine(mstrPriceSKU, 1, CDbl(curPrice), txtTranNo.Text, GetDate(dtxtTranDate.Text), txtTillId.Text, 1, False, False)
    Else
        'for Other Store refund - get Refund Reason
        Load frmRefundCode
        frmRefundCode.CloseKeyCode = mlngCloseKeyCode
        strCode = frmRefundCode.GetRefundReasonCode(cmdDtlCancel.Caption, RefundReasons)
        Unload frmRefundCode
        If (strCode = "") Then Exit Sub
        RaiseEvent OtherStoreTran(txtStoreNo.Text, dtxtTranDate.Text, txtTillId.Text, txtTranNo.Text, strCode)
    End If

End Sub

Private Sub cmdParkCancel_Click()

    Call Reset

End Sub

Private Sub cmdParkRetrieve_Click()
    
Dim varLine As Variant

    varLine = Split(lstParked.List(lstParked.ListIndex), vbTab)
    
    dtxtTranDate.Text = Format$(Now(), "DD/MM/YY")
    txtTillId.Text = varLine(1)
    txtTranNo.Text = varLine(2)
    
    fraParked.Visible = False
    fraTran.Visible = True
    AllowScan = True
    cmdDetails.Visible = True
    cmdDetails.Value = True

End Sub

Private Sub cmdReset_Click()

    Call Reset
    If txtStoreNo.Enabled Then
        txtStoreNo.SetFocus
    ElseIf dtxtTranDate.Enabled Then
        dtxtTranDate.SetFocus
    Else
        txtTillId.SetFocus
    End If

End Sub

Public Sub SelectTransactionLine(blnSelectLine As Boolean)

    txtStoreNo.Text = goSession.CurrentEnterprise.IEnterprise_StoreNumber
    mblnSelectLine = blnSelectLine
    mblnSalesOnly = True

End Sub

Private Sub cmdRetrieveAll_Click()

Dim lngRowNo As Long
Dim strCode  As String
Dim lngQty   As Long

    
    Load frmRefundCode
    frmRefundCode.CloseKeyCode = mlngCloseKeyCode
    strCode = frmRefundCode.GetRefundReasonCode(cmdDtlCancel.Caption, RefundReasons)
    Unload frmRefundCode
    If (strCode = "") Then Exit Sub
    
    Set mcolLines = New Collection
    For lngRowNo = 1 To sprdDetails.MaxRows Step 1
        sprdDetails.Row = lngRowNo
        sprdDetails.Col = COL_DTL_QTY
        lngQty = Val(sprdDetails.Value)
        sprdDetails.Col = COL_DTL_QTY_RFND
        sprdDetails.Value = lngQty
        sprdDetails.Col = COL_DTL_QTY_REASON
        sprdDetails.CellType = CellTypeComboBox
        sprdDetails.TypeComboBoxList = strCode
        sprdDetails.Text = strCode
    Next
    
    Call GetRefundItems
    RaiseEvent SelectTransactionLines(lblTranNo.Caption, GetDate(lblTranDate.Caption), lblTillID.Caption, mcolLines)

End Sub

Private Function GetRefundItems() As Boolean

Dim lngRowNo        As Long
Dim cLine           As cRefundLine
Dim oPriceCodeBO    As cPriceOverrideCode

    GetRefundItems = True
    Set oPriceCodeBO = PriceOverrideCodes(1)
    
    Set mcolLines = New Collection
    For lngRowNo = 1 To sprdDetails.MaxRows Step 1
        sprdDetails.Row = lngRowNo
        sprdDetails.Col = COL_DTL_QTY_RFND
        If (Val(sprdDetails.Value) > 0) Then
            Set cLine = New cRefundLine
            sprdDetails.Col = COL_DTL_SKU
            cLine.PartCode = sprdDetails.Text
            sprdDetails.Col = COL_DTL_QTY_RFND
            cLine.RefundQuantity = sprdDetails.Text
            sprdDetails.Col = COL_DTL_LINENO
            cLine.LineNo = sprdDetails.Text
            sprdDetails.Col = COL_DTL_PRICE
            cLine.Price = sprdDetails.Text
            sprdDetails.Col = COL_DTL_QTY_REASON
            cLine.RefundReasonCode = Left$(sprdDetails.Text, 2)
            'added 5/10/06 - check that reason code entered else clear and exit
            If (cLine.RefundReasonCode = "") Then
                Set mcolLines = New Collection
                Call MsgBoxEx("No Refund Reason entered for line" & vbNewLine & "Select reason from list to process", vbInformation, "Missing information", , , , , RGBMsgBox_WarnColour)
                Call sprdDetails_EditMode(COL_DTL_QTY_RFND, sprdDetails.Row, 0, True)
                GetRefundItems = False
                Exit Function
            End If
            
            sprdDetails.Col = COL_DTL_RFND_PRICE
            If (Val(sprdDetails.Value) <> cLine.Price) And (sprdDetails.Value <> "") Then
                cLine.Price = Val(sprdDetails.Value)
                cLine.PriceOverrideReason = oPriceCodeBO.Code
            End If
            Call mcolLines.Add(cLine)
        End If
    Next
    
End Function
Private Sub cmdCancel_Click()

    RaiseEvent Cancel

End Sub

Private Sub lstParked_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        cmdParkRetrieve.Value = True
    End If 'Enter has been pressed to select entry
    If (KeyAscii = vbKeyEscape) Then
        cmdParkCancel.Value = True
    End If

End Sub

Private Sub sprdDetails_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)

Dim lngQty      As Long
Dim curPrice    As Currency
Dim blnCancel   As Boolean
Dim strValue    As String

    If (Mode = 1) And (Col = COL_DTL_RFND_PRICE) Then
        sprdDetails.Row = Row
        sprdDetails.Col = Col
        sprdDetails.CellType = CellTypeEdit
        sprdDetails.TypeHAlign = TypeHAlignRight
        sprdDetails.TypeMaxEditLen = 10
        sprdDetails.TypeEditCharSet = TypeEditCharSetNumeric
    End If
    If (Mode = 0) And (ChangeMade = False) And (Col = COL_DTL_QTY_REASON) Then
        Call sprdDetails.SetActiveCell(COL_DTL_RFND_PRICE, Row)
        Exit Sub
    End If
    If (Mode = 0) And (ChangeMade = True) Then
        sprdDetails.Row = Row
        Select Case (Col)
            Case (COL_DTL_QTY_RFND):
                sprdDetails.Col = COL_DTL_QTY
                lngQty = Val(sprdDetails.Value)
                sprdDetails.Col = COL_DTL_QTY_RFND
                If (lngQty < Val(sprdDetails.Value)) Then
                    Call MsgBoxEx("Refund quantity is higher than purchased quantity.  Refund quantity changed to purchased quantity.", vbOKOnly, "Refund Quantity Exceeded", , , , , RGBMsgBox_WarnColour)
                    sprdDetails.Value = lngQty
                End If
                sprdDetails.Col = COL_DTL_QTY_RFND
                If (Val(sprdDetails.Value) > 0) Then
                    sprdDetails.Col = COL_DTL_QTY_REASON
                    sprdDetails.Lock = False
                    If (sprdDetails.Text = "") Then
                        sprdDetails.CellType = CellTypeComboBox
                        sprdDetails.TypeComboBoxList = mstrRefundCodes
                        sprdDetails.Text = Left$(mstrRefundCodes, InStr(mstrRefundCodes, vbTab) - 1)
                    End If
                    sprdDetails.Col = COL_DTL_PRICE
                    curPrice = sprdDetails.Value
                    sprdDetails.Col = COL_DTL_RFND_PRICE
                    If (sprdDetails.Text = "") Then sprdDetails.Value = curPrice
                    sprdDetails.Lock = False
                    sprdDetails.Col = COL_DTL_QTY_REASON
                End If
                sprdDetails.Col = COL_DTL_QTY_RFND
                If (Val(sprdDetails.Value) = 0) Then
                    sprdDetails.Col = COL_DTL_QTY_REASON
                    sprdDetails.Text = ""
                    sprdDetails.Lock = True
                    sprdDetails.Col = COL_DTL_RFND_PRICE
                    sprdDetails.Text = ""
                    sprdDetails.Lock = True
                Else
                    Call sprdDetails.SetActiveCell(COL_DTL_QTY_REASON, Row)
                    sprdDetails.EditMode = True
                    Call SendKeys("%{Down}")
                End If
            Case (COL_DTL_QTY_REASON):
                    Call sprdDetails.SetActiveCell(COL_DTL_RFND_PRICE, Row)
                    Call sprdDetails_LeaveCell(COL_DTL_QTY_RFND, 1, COL_DTL_RFND_PRICE, 1, blnCancel)
            Case (COL_DTL_RFND_PRICE):
                sprdDetails.Col = COL_DTL_RFND_PRICE
                strValue = sprdDetails.Text
                If (InStr(strValue, ".") = 0) Then sprdDetails.Text = Val(strValue) / 100
                sprdDetails.CellType = CellTypeNumber
                sprdDetails.Value = Abs(sprdDetails.Value)
                strValue = sprdDetails.Value
                'if value has changed then check if not original price and ask for auth
                sprdDetails.Col = COL_DTL_PRICE
                If (strValue <> sprdDetails.Text) Then
                    Load frmVerifyPwd
                    If (frmVerifyPwd.VerifySupervisorPassword = False) Then
                        strValue = sprdDetails.Text
                        sprdDetails.Col = COL_DTL_RFND_PRICE
                        sprdDetails.Text = strValue
                    End If
                    Unload frmVerifyPwd
                End If
                
            End Select
    End If

End Sub

Private Sub sprdDetails_KeyPress(KeyAscii As Integer)

    If (sprdDetails.ActiveCol = COL_DTL_QTY_REASON) And (sprdDetails.EditMode = False) Then
        sprdDetails.Col = sprdDetails.ActiveCol
        sprdDetails.Row = sprdDetails.ActiveRow
        If (sprdDetails.Lock = True) Then
            Call sprdDetails.SetActiveCell(COL_DTL_QTY_RFND, sprdDetails.Row)
            sprdDetails.EditMode = True
        End If
        
    End If

End Sub

Private Sub sprdDetails_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)


    If (NewCol < COL_DTL_QTY_RFND) And (mblnVerifySKU = False) Then
        Cancel = True
    End If
    
End Sub

Private Sub txtTranNo_Change()

    cmdReset.Visible = True

End Sub

Private Sub txtTranNo_GotFocus()

    txtTranNo.SelStart = 0
    txtTranNo.SelLength = Len(txtTranNo.Text)
    txtTranNo.BackColor = RGBEdit_Colour

End Sub

Private Sub txtTranNo_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        If LenB(txtTranNo.Text) <> 0 Then txtTranNo.Text = Format$(Val(txtTranNo.Text), "0000")
        If LenB(txtTillId.Text) <> 0 And LenB(txtTranNo.Text) <> 0 Then
            cmdDetails.Visible = True
            cmdDetails.Value = True
            Exit Sub
        End If
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

    Call EnforceNumeric(KeyAscii)

End Sub

Private Sub txtTranNo_LostFocus()

    txtTranNo.BackColor = RGB_WHITE
    
End Sub

Private Sub txtStoreNo_Change()

    cmdReset.Visible = True

End Sub

Private Sub txtStoreNo_GotFocus()

    txtStoreNo.SelStart = 0
    txtStoreNo.SelLength = Len(txtStoreNo.Text)
    txtStoreNo.BackColor = RGBEdit_Colour

End Sub

Private Sub txtStoreNo_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If
    
    Call EnforceNumeric(KeyAscii)

End Sub

Private Sub txtStoreNo_LostFocus()

    txtStoreNo.BackColor = RGB_WHITE
    
End Sub

Private Sub dtxtTranDate_Change()

    cmdReset.Visible = True

End Sub

Private Sub dtxtTranDate_GotFocus()

    dtxtTranDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtTranDate_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub dtxtTranDate_LostFocus()

    dtxtTranDate.BackColor = RGB_WHITE
    
End Sub

Private Sub txtTillID_Change()

    cmdReset.Visible = True

End Sub

Private Sub txtTillID_GotFocus()

    txtTillId.SelStart = 0
    txtTillId.SelLength = Len(txtTillId.Text)
    txtTillId.BackColor = RGBEdit_Colour

End Sub

Private Sub txtTillID_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

    Call EnforceNumeric(KeyAscii)

End Sub

Private Sub txtTillID_LostFocus()

    txtTillId.BackColor = RGB_WHITE
    If LenB(txtTillId.Text) <> 0 Then txtTillId.Text = Format$(Val(txtTillId.Text), "00")
    
End Sub

Private Sub UserControl_Hide()
    
    Call DebugMsg(MODULE_NAME, "UC_Hide", endlDebug, "Resetting preview")
    If Not moParent Is Nothing Then moParent.KeyPreview = mblnPreview

End Sub

Private Sub UserControl_Initialize()

    Call DebugMsg(MODULE_NAME, "Initialize", endlDebug)

End Sub

Private Sub UserControl_KeyDown(KeyCode As Integer, Shift As Integer)

Dim Ctrl As Control

    Select Case (KeyCode)
        Case (mlngCloseKeyCode):
                Call DebugMsg(MODULE_NAME, "UC_KeyDown", endlDebug)
                If (mlngCloseKeyCode <> 0) And (cmdDtlCancel.Visible = True) Then
                    cmdDtlCancel.Value = True
                ElseIf (mlngCloseKeyCode <> 0) And (cmdCancel.Visible = True) Then
                    cmdCancel.Value = True
                ElseIf (mlngCloseKeyCode <> 0) And (cmdParkCancel.Visible = True) Then
                    cmdParkCancel.Value = True
                End If
                KeyCode = 0
        Case (mlngResetKeyCode):
                If (mlngResetKeyCode <> 0) And (cmdReset.Visible = True) Then Call Reset
                KeyCode = 0
        Case (mlngOtherKeyCode):
                If (mlngOtherKeyCode <> 0) And (cmdOtherTran.Visible = True) Then
                    Call cmdOtherTran.SetFocus
                    cmdOtherTran.Value = True
                End If
                KeyCode = 0
        Case (mlngUseKeyCode):
                If (mlngUseKeyCode <> 0) And (cmdDtlRetrieve.Visible = True) Then cmdDtlRetrieve.Value = True
                If (mlngUseKeyCode <> 0) And (cmdParkRetrieve.Visible = True) Then cmdParkRetrieve.Value = True
        Case (mlngDetailKeyCode):
                If (mlngDetailKeyCode <> 0) And cmdDetails.Visible Then cmdDetails.Value = True
        Case (mlngListKeyCode):
                If (mlngListKeyCode <> 0) And (cmdList.Visible = True) Then cmdList.Value = True
                If (mlngListKeyCode <> 0) And (cmdRetrieveAll.Visible = True) Then cmdRetrieveAll.Value = True
                If (mlngListKeyCode <> 0) And (cmdNoProof.Visible = True) Then cmdNoProof.Value = True
        Case vbKeyTab
            If Shift = 0 Then
                FirstActiveControl.SetFocus
            ElseIf Shift = 1 Then
                LastActiveControl.SetFocus
            End If
    End Select

End Sub

Private Sub UserControl_Resize()

    If moParent Is Nothing Then
        UserControl.Width = 11655
        UserControl.Height = 7950
    End If

End Sub

Public Property Let TillID(Value As String)
    txtTillId.SetFocus
    txtTillId.Text = Value
End Property

Public Property Let TranID(Value As String)
    txtTranNo.SetFocus
    txtTranNo.Text = Value
End Property

Public Property Let StoreNo(Value As String)
    lblStoreNo.Caption = Value
    txtStoreNo.Text = Value
End Property

Public Property Get StoreNo() As String
    StoreNo = txtStoreNo.Text
End Property

Public Sub ShowDetails()
    cmdDetails.Visible = True
    cmdDetails.Value = True
End Sub

Public Property Let TransactionDate(Value As Date)
    If dtxtTranDate.Enabled Then dtxtTranDate.SetFocus
    dtxtTranDate.Text = Format(Value, "DD/MM/YY")
End Property

Private Sub EnforceNumeric(ByRef intKeyAscii As Integer)
    
    If (intKeyAscii >= vbKeySpace) And (Not Chr$(intKeyAscii) Like "[0-9]") Then
        intKeyAscii = 0
    End If

End Sub

Private Function FirstActiveControl() As Control

Dim lowTab  As Long
Dim Ctrl    As Control
Dim retCtrl As Control

    lowTab = -1
    
    On Error Resume Next
    For Each Ctrl In Controls
        If Ctrl.Visible = True And Ctrl.Enabled = True And Ctrl.TabStop = True Then
            If Err.Number = 0 Then
                If (Ctrl.TabIndex < lowTab) Or (lowTab = -1) Then
                    lowTab = Ctrl.TabIndex
                    Set retCtrl = Ctrl
                End If
            End If
            Err.Clear
        End If
    Next Ctrl
    
    On Error GoTo 0
    Set FirstActiveControl = retCtrl
    
End Function

Private Function LastActiveControl() As Control

Dim highTab  As Long
Dim Ctrl    As Control
Dim retCtrl As Control

    highTab = -1
    
    On Error Resume Next
    For Each Ctrl In Controls
        If Ctrl.Visible = True And Ctrl.Enabled = True And Ctrl.TabStop = True Then
            If Err.Number = 0 Then
                If (Ctrl.TabIndex > highTab) Or (highTab = -1) Then
                    highTab = Ctrl.TabIndex
                    Set retCtrl = Ctrl
                End If
            End If
            Err.Clear
        End If
    Next Ctrl
    
    On Error GoTo 0
    Set LastActiveControl = retCtrl
    
End Function

Private Sub SplitRefundCodes()

Dim oRefundCodeBO As cRefundCode

    
    mstrRefundCodes = ""
    If ((RefundReasons Is Nothing) = False) Then
        For Each oRefundCodeBO In RefundReasons
            mstrRefundCodes = mstrRefundCodes & oRefundCodeBO.Code & "-" & oRefundCodeBO.Description & vbTab
        Next
    End If
    
End Sub

Private Sub SplitPriceCodes()

Dim oPriceCodeBO As cPriceOverrideCode

    
    'mstrOPriceCodes = ""
    'If ((PriceOverrideCodes Is Nothing) = False) Then
    '    For Each oPriceCodeBO In PriceOverrideCodes
    '        mstrOPriceCodes = mstrOPriceCodes & oPriceCodeBO.Code & "-" & oPriceCodeBO.Description & vbTab
    '    Next
    'End If
    
End Sub

