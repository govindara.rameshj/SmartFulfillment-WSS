VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMaintDisplay 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Maintain display locations for item"
   ClientHeight    =   6180
   ClientLeft      =   1545
   ClientTop       =   1110
   ClientWidth     =   6345
   Icon            =   "frmMaintDisplay.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6180
   ScaleWidth      =   6345
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   375
      Left            =   4920
      TabIndex        =   5
      Top             =   5340
      Width           =   1215
   End
   Begin FPSpreadADO.fpSpread sprdItems 
      Height          =   4575
      Left            =   240
      TabIndex        =   0
      Top             =   600
      Width           =   5895
      _Version        =   458752
      _ExtentX        =   10398
      _ExtentY        =   8070
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   5
      MaxRows         =   1
      OperationMode   =   2
      RowHeaderDisplay=   0
      SelectBlockOptions=   0
      SpreadDesigner  =   "frmMaintDisplay.frx":058A
      UserResize      =   0
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   6
      Top             =   5805
      Width           =   6345
      _ExtentX        =   11192
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmMaintDisplay.frx":0968
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   3836
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "13:11"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblQuantity 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   5520
      TabIndex        =   4
      Top             =   240
      Width           =   615
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Number"
      Height          =   255
      Left            =   3960
      TabIndex        =   3
      Top             =   240
      Width           =   1335
   End
   Begin VB.Label lblPartCode 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   1440
      TabIndex        =   2
      Top             =   240
      Width           =   1935
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "SKU"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   240
      Width           =   1215
   End
End
Attribute VB_Name = "frmMaintDisplay"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const COL_LOC As Long = 1
Const COL_QTY As Long = 2
Const COL_DATE As Long = 3
Const COL_INIT As Long = 4
Const COL_EDIT As Long = 5

Const EDIT_NONE As Long = 0
Const EDIT_DEL As Long = 1
Const EDIT_NEW As Long = 2
Const EDIT_UPD As Long = 3

Dim mlngOldWidth As Long
Dim mlngOldHeight As Long


Public Sub LoadLocations(ByVal strPartCode As String)

Dim oStockLoc    As cStockLocation
Dim colStockLoc  As Collection
Dim lngLocNo     As Long

    Set oStockLoc = goDatabase.CreateBusinessObject(CLASSID_STOCKLOCATION)
    
    lblPartCode.Caption = strPartCode

    Call oStockLoc.IBo_AddLoadFilter(CMP_LIKE, FID_STOCKLOCATION_LocationNo, "D%")
    Call oStockLoc.IBo_AddLoadFilter(CMP_EQUAL, FID_STOCKLOCATION_PartCode, strPartCode)
    
    Set colStockLoc = oStockLoc.IBo_LoadMatches
    
    sprdItems.MaxRows = 0
    For lngLocNo = 1 To colStockLoc.Count Step 1
        sprdItems.MaxRows = sprdItems.MaxRows + 1
        sprdItems.Row = sprdItems.MaxRows
        sprdItems.Col = COL_LOC
        sprdItems.Text = colStockLoc(CLng(lngLocNo)).LocationNo
        sprdItems.Col = COL_QTY
        sprdItems.Text = colStockLoc(CLng(lngLocNo)).LastStockCountQuantity
        sprdItems.Col = COL_DATE
        sprdItems.Text = DisplayDate(colStockLoc(CLng(lngLocNo)).LastUpdated, False)
        sprdItems.Col = COL_INIT
        sprdItems.Text = colStockLoc(CLng(lngLocNo)).ChangedBy
        sprdItems.Col = COL_EDIT
        sprdItems.Text = EDIT_NONE
    Next lngLocNo
    sprdItems.MaxRows = sprdItems.MaxRows + 1
    sprdItems.Row = sprdItems.MaxRows
    sprdItems.Col = COL_LOC
    sprdItems.Lock = False
    
    Set oStockLoc = Nothing

End Sub

Private Sub Form_Load()

    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    Call InitialiseStatusBar(sbStatus)

End Sub
