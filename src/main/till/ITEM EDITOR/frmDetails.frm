VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmDetails 
   Caption         =   "View Details"
   ClientHeight    =   7515
   ClientLeft      =   1710
   ClientTop       =   840
   ClientWidth     =   8775
   Icon            =   "frmDetails.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   7515
   ScaleWidth      =   8775
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F9-Print"
      Height          =   375
      Left            =   7680
      TabIndex        =   6
      Top             =   6660
      Width           =   975
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10-Exit"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   6660
      Width           =   975
   End
   Begin FPSpreadADO.fpSpread sprdDetails 
      Height          =   6015
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   8535
      _Version        =   458752
      _ExtentX        =   15055
      _ExtentY        =   10610
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   7
      MaxRows         =   1
      RowHeaderDisplay=   0
      SpreadDesigner  =   "frmDetails.frx":058A
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   7
      Top             =   7140
      Width           =   8775
      _ExtentX        =   15478
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmDetails.frx":0A53
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7646
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "13:11"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblDescription 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   3240
      TabIndex        =   5
      Top             =   120
      Width           =   3015
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Description"
      Height          =   255
      Left            =   2280
      TabIndex        =   4
      Top             =   120
      Width           =   975
   End
   Begin VB.Label lblPartCode 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   960
      TabIndex        =   2
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Part Code"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   855
   End
End
Attribute VB_Name = "frmDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const COL_DATE As Long = 1
Const COL_ORDERNO As Long = 2
Const COL_LINENO As Long = 3
Const COL_QTY As Long = 4
Const COL_QTYDEL As Long = 5
Const COL_QTYBO As Long = 6
Const COL_RETCODE As Long = 7

Dim mstrViewDescription As String

Private Sub cmdExit_Click()

    Unload Me

End Sub

Private Sub SetQuantityPlaces()

Dim lngQtyDecNum   As Long
Dim lngValueDecNum As Long

    lngQtyDecNum = goSession.GetParameter(PRM_QTY_DEC_PLACES)
    lngValueDecNum = goSession.GetParameter(PRM_VALUE_DEC_PLACES)

    sprdDetails.Row = -1
    sprdDetails.Col = COL_QTY
    sprdDetails.TypeNumberDecPlaces = lngQtyDecNum
    sprdDetails.Col = COL_QTYDEL
    sprdDetails.TypeNumberDecPlaces = lngQtyDecNum
    sprdDetails.Col = COL_QTYBO
    sprdDetails.TypeNumberDecPlaces = lngQtyDecNum

End Sub

Public Function DisplayOpenReturns(strPartCode As String, strDescription As String)

Dim oReturns   As cReturnLine
Dim colReturns As Collection
Dim lngLineNo  As Long

    mstrViewDescription = "Open Supplier Returns"
    Me.Caption = "View " & mstrViewDescription & " Details"
    Call SetQuantityPlaces
    Call Me.Show
    lblPartCode.Caption = strPartCode
    lblDescription.Caption = strDescription
    Set oReturns = goDatabase.CreateBusinessObject(CLASSID_RETURNLINE)
    
    Call oReturns.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNLINE_PartCode, strPartCode)
    Call oReturns.IBo_AddLoadFilter(CMP_GREATERTHAN, FID_RETURNLINE_ReturnID, 0)
    Call oReturns.IBo_AddLoadFilter(CMP_GREATERTHAN, FID_RETURNLINE_LineNo, 0)
    Set colReturns = oReturns.IBo_LoadMatches
        
    sprdDetails.Col = COL_DATE
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_QTYDEL
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_QTYBO
    sprdDetails.ColHidden = True
    
    sprdDetails.MaxRows = colReturns.Count
    For lngLineNo = 1 To colReturns.Count Step 1
        sprdDetails.Row = lngLineNo
        sprdDetails.Col = COL_ORDERNO
        sprdDetails.Text = colReturns(lngLineNo).ReturnID
        sprdDetails.Col = COL_LINENO
        sprdDetails.Text = colReturns(lngLineNo).LineNo
        sprdDetails.Col = COL_QTY
        sprdDetails.Text = colReturns(lngLineNo).QuantityReturned
        sprdDetails.Col = COL_RETCODE
        sprdDetails.Text = colReturns(lngLineNo).ReturnCode
    Next lngLineNo

    Set colReturns = Nothing
    
    Call Me.Hide
    Call Me.Show(vbModal)

End Function

Public Function DisplayPurchaseOrders(strPartCode As String, strDescription As String)

Dim oOrders   As cPurchaseLine
Dim colOrders As Collection
Dim lngLineNo As Long

    mstrViewDescription = "Purchase Orders"
    Me.Caption = "View " & mstrViewDescription & " Details"
    Call SetQuantityPlaces
    Call Me.Show
    lblPartCode.Caption = strPartCode
    lblDescription.Caption = strDescription
    Set oOrders = goDatabase.CreateBusinessObject(CLASSID_PURCHASEORDERLINE)
    
    Call oOrders.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDERLINE_PartCode, strPartCode)
    Call oOrders.IBo_AddLoadFilter(CMP_GREATERTHAN, FID_PURCHASEORDERLINE_PurchaseOrderNo, 0)
    Call oOrders.IBo_AddLoadFilter(CMP_GREATERTHAN, FID_PURCHASEORDERLINE_LineNo, 0)
    Call oOrders.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDERLINE_Complete, False)
    Set colOrders = oOrders.IBo_LoadMatches
        
    sprdDetails.Col = COL_DATE
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_RETCODE
    sprdDetails.ColHidden = True
    
    sprdDetails.MaxRows = colOrders.Count
    For lngLineNo = 1 To colOrders.Count Step 1
        sprdDetails.Row = lngLineNo
        sprdDetails.Col = COL_ORDERNO
        sprdDetails.Text = colOrders(lngLineNo).PurchaseOrderNo
        sprdDetails.Col = COL_LINENO
        sprdDetails.Text = colOrders(lngLineNo).LineNo
        sprdDetails.Col = COL_QTY
        sprdDetails.Text = colOrders(lngLineNo).OrderQuantity
        sprdDetails.Col = COL_QTYDEL
        sprdDetails.Text = colOrders(lngLineNo).QuantityReceived
        sprdDetails.Col = COL_QTYBO
        sprdDetails.Text = colOrders(lngLineNo).QuantityBackordered
    Next lngLineNo

    Set colOrders = Nothing
    
    Call Me.Hide
    Call Me.Show(vbModal)

End Function

Private Sub cmdPrint_Click()

    sprdDetails.PrintHeader = "/c/fb1" & mstrViewDescription & " for " & lblPartCode.Caption & "-" & lblDescription.Caption
    sprdDetails.PrintFooter = "/lPrinted - " & Format$(Now(), "DD/MM/YY HH:NN") & "/r /p of /pc"
    sprdDetails.PrintSmartPrint = True
    sprdDetails.PrintJobName = mstrViewDescription & "(" & lblPartCode.Caption & ")"
    Call sprdDetails.PrintSheet

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case (KeyCode)
        Case (vbKeyF9):  Call cmdPrint_Click
        Case (vbKeyF10): Call cmdExit_Click
    End Select

End Sub

Private Sub Form_Load()

    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)

End Sub

Private Sub Form_Resize()

    sprdDetails.Width = Me.Width - sprdDetails.Left * 3
    sprdDetails.Height = Me.Height - sbStatus.Height * 2 - 240 - cmdExit.Height - sprdDetails.Top
    cmdExit.Top = sprdDetails.Top + sprdDetails.Height + 120
    cmdPrint.Top = cmdExit.Top
    cmdPrint.Left = sprdDetails.Width - cmdPrint.Width + sprdDetails.Left

End Sub

