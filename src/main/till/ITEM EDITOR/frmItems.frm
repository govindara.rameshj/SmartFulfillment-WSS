VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{F537A415-F16A-11D6-B070-0020AFC9A0C8}#1.0#0"; "HotkeyList.ocx"
Object = "{6514F5A0-641C-11D2-9FD0-0020AF131A57}#3.0#0"; "fpFlp30.ocx"
Object = "{C8C70ABD-5F8E-11D7-B075-0020AFC9A0C8}#1.0#0"; "EditNumberCtl.ocx"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Object = "{B3202B8D-B8B9-4655-9712-EAB5BFD920D4}#1.0#0"; "ItemFilter.ocx"
Begin VB.Form frmItems 
   Appearance      =   0  'Flat
   BackColor       =   &H00FFC0C0&
   Caption         =   "Item Editor"
   ClientHeight    =   8265
   ClientLeft      =   -75
   ClientTop       =   615
   ClientWidth     =   12135
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "frmItems.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8265
   ScaleWidth      =   12135
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin CTSProgBar.ucpbProgressBar ucpbItem 
      Height          =   1860
      Left            =   3517
      TabIndex        =   84
      Top             =   3202
      Visible         =   0   'False
      Width           =   5100
      _ExtentX        =   8996
      _ExtentY        =   3281
      FillColor       =   16711680
      Object.Width           =   5100
      Object.Height          =   1860
   End
   Begin prjHotkeyList.ucHotkeyList uchlHelpKeys 
      Height          =   2835
      Left            =   4200
      TabIndex        =   88
      Top             =   4080
      Visible         =   0   'False
      Width           =   3510
      _ExtentX        =   6191
      _ExtentY        =   5001
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   90
      Top             =   0
      Width           =   12135
      _ExtentX        =   21405
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   13
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "New"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Save"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Sep1"
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Delete"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Sep2"
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Previous"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Next"
            ImageIndex      =   4
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Sep3"
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Search"
            ImageIndex      =   7
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "sep4"
            Style           =   3
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Refresh"
            ImageIndex      =   8
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Sep5"
            Style           =   3
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   4
            Object.Width           =   1000
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   3000
         TabIndex        =   91
         Top             =   0
         Width           =   3855
         Begin VB.Label lblNoMatcheslbl 
            BackStyle       =   0  'Transparent
            Caption         =   "No Matches"
            Height          =   255
            Left            =   0
            TabIndex        =   93
            Top             =   90
            Width           =   1095
         End
         Begin VB.Label lblNoMatches 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000E&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "N/A"
            Height          =   255
            Left            =   1200
            TabIndex        =   92
            Top             =   90
            Width           =   1095
         End
      End
   End
   Begin MSComctlLib.Slider sldDetails 
      Height          =   3855
      Left            =   0
      TabIndex        =   143
      Top             =   360
      Visible         =   0   'False
      Width           =   90
      _ExtentX        =   159
      _ExtentY        =   6800
      _Version        =   393216
      Orientation     =   1
      Max             =   100
      SelStart        =   95
      TickStyle       =   3
      Value           =   95
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItems.frx":058A
            Key             =   "MovePrev"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItems.frx":0694
            Key             =   "New"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItems.frx":07A6
            Key             =   "Save"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItems.frx":08B8
            Key             =   "MoveNext"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItems.frx":09C2
            Key             =   "Delete"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItems.frx":0AD4
            Key             =   "Preview"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItems.frx":0C26
            Key             =   "Search"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmItems.frx":0D38
            Key             =   "Refresh"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   89
      Top             =   7890
      Width           =   12135
      _ExtentX        =   21405
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmItems.frx":108A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13600
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "10:22"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin TabDlg.SSTab tabDetails 
      Height          =   4755
      Left            =   120
      TabIndex        =   54
      Top             =   3000
      Width           =   11895
      _ExtentX        =   20981
      _ExtentY        =   8387
      _Version        =   393216
      Tabs            =   8
      TabsPerRow      =   8
      TabHeight       =   520
      BackColor       =   16761024
      ForeColor       =   16711680
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Configuration"
      TabPicture(0)   =   "frmItems.frx":3256
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "frConfig"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "ucifSearch"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Movements"
      TabPicture(1)   =   "frmItems.frx":3272
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "frMovements"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Suppliers"
      TabPicture(2)   =   "frmItems.frx":328E
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "frSuppliers"
      Tab(2).Control(1)=   "frSupplier"
      Tab(2).ControlCount=   2
      TabCaption(3)   =   "EANs"
      TabPicture(3)   =   "frmItems.frx":32AA
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "frEANs"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "Summary"
      TabPicture(4)   =   "frmItems.frx":32C6
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "frSummary"
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "Price / Cost"
      TabPicture(5)   =   "frmItems.frx":32E2
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "frPrices"
      Tab(5).ControlCount=   1
      TabCaption(6)   =   "SPARE"
      TabPicture(6)   =   "frmItems.frx":32FE
      Tab(6).ControlEnabled=   0   'False
      Tab(6).ControlCount=   0
      TabCaption(7)   =   "Tech. Info"
      TabPicture(7)   =   "frmItems.frx":331A
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "frTechInfo"
      Tab(7).ControlCount=   1
      Begin ItemFilter_UC_Wickes.ucItemFilter ucifSearch 
         Height          =   495
         Left            =   0
         TabIndex        =   162
         Top             =   4680
         Width           =   11895
         _ExtentX        =   20981
         _ExtentY        =   873
      End
      Begin VB.Frame frTechInfo 
         BorderStyle     =   0  'None
         Height          =   3975
         Left            =   -74880
         TabIndex        =   108
         Top             =   360
         Width           =   11655
         Begin VB.TextBox txtTechInfo 
            Height          =   3435
            Left            =   60
            MultiLine       =   -1  'True
            TabIndex        =   109
            Top             =   120
            Width           =   11475
         End
      End
      Begin VB.Frame frSuppliers 
         BorderStyle     =   0  'None
         Height          =   2775
         Left            =   -74880
         TabIndex        =   94
         Top             =   480
         Width           =   11655
         Begin VB.CommandButton cmdDelSupplier 
            Appearance      =   0  'Flat
            Caption         =   "Del Supplier"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   8820
            TabIndex        =   97
            Top             =   2280
            Width           =   1335
         End
         Begin VB.CommandButton cmdSaveSupplier 
            Appearance      =   0  'Flat
            Caption         =   "Save Updates"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   7440
            TabIndex        =   96
            Top             =   2280
            Visible         =   0   'False
            Width           =   1395
         End
         Begin VB.CommandButton cmdNewSupp 
            Appearance      =   0  'Flat
            Caption         =   "New Supplier"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   10260
            TabIndex        =   98
            Top             =   2280
            Width           =   1335
         End
         Begin FPSpreadADO.fpSpread sprdSuppliers 
            Height          =   2175
            Left            =   60
            TabIndex        =   95
            Top             =   0
            Width           =   11535
            _Version        =   458752
            _ExtentX        =   20346
            _ExtentY        =   3836
            _StockProps     =   64
            ArrowsExitEditMode=   -1  'True
            EditEnterAction =   4
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxCols         =   5
            MaxRows         =   1
            RowHeaderDisplay=   0
            ScrollBars      =   2
            ShadowColor     =   12632256
            SpreadDesigner  =   "frmItems.frx":3336
            UserResize      =   0
            VisibleCols     =   5
            VisibleRows     =   1
         End
      End
      Begin VB.Frame frPrices 
         BorderStyle     =   0  'None
         Height          =   4155
         Left            =   -74880
         TabIndex        =   107
         Top             =   480
         Width           =   11655
         Begin ucEditNumber.ucNumberText ntxtCostPrice 
            Height          =   285
            Left            =   1080
            TabIndex        =   131
            Top             =   2160
            Width           =   975
            _ExtentX        =   1720
            _ExtentY        =   503
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinimumValue    =   0
         End
         Begin FPSpreadADO.fpSpread sprdPrices 
            Height          =   1935
            Left            =   120
            TabIndex        =   129
            Top             =   60
            Width           =   11415
            _Version        =   458752
            _ExtentX        =   20135
            _ExtentY        =   3413
            _StockProps     =   64
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxCols         =   4
            MaxRows         =   2
            RetainSelBlock  =   0   'False
            RowHeaderDisplay=   0
            ScrollBars      =   0
            SpreadDesigner  =   "frmItems.frx":3829
         End
         Begin FPSpreadADO.fpSpread sprdCost 
            Height          =   1875
            Left            =   2280
            TabIndex        =   134
            Top             =   2160
            Width           =   9255
            _Version        =   458752
            _ExtentX        =   16325
            _ExtentY        =   3307
            _StockProps     =   64
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxCols         =   7
            MaxRows         =   2
            RetainSelBlock  =   0   'False
            RowHeaderDisplay=   0
            ScrollBars      =   0
            ShadowColor     =   12632256
            SpreadDesigner  =   "frmItems.frx":3DA9
         End
         Begin VB.Label Label12 
            BackStyle       =   0  'Transparent
            Caption         =   "Cost Price"
            Height          =   255
            Left            =   120
            TabIndex        =   130
            Top             =   2160
            Width           =   975
         End
         Begin VB.Label Label16 
            BackStyle       =   0  'Transparent
            Caption         =   "Margin"
            Height          =   255
            Left            =   120
            TabIndex        =   132
            Top             =   2520
            Width           =   975
         End
         Begin VB.Label lblMargin 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0.00"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   1080
            TabIndex        =   133
            Top             =   2520
            Width           =   975
         End
      End
      Begin VB.Frame frSummary 
         BorderStyle     =   0  'None
         Height          =   4275
         Left            =   -74880
         TabIndex        =   106
         Top             =   360
         Width           =   11655
         Begin VB.Frame frPeriodTotals 
            Caption         =   "Period Totals"
            Height          =   1275
            Left            =   0
            TabIndex        =   125
            Top             =   780
            Width           =   11655
            Begin FPSpreadADO.fpSpread sprdPeriodTot 
               Height          =   975
               Left            =   1560
               TabIndex        =   126
               Top             =   180
               Width           =   9975
               _Version        =   458752
               _ExtentX        =   17595
               _ExtentY        =   1720
               _StockProps     =   64
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MaxCols         =   10
               MaxRows         =   2
               ScrollBars      =   0
               SpreadDesigner  =   "frmItems.frx":5EAE
            End
         End
         Begin VB.CommandButton cmdEnqBranches 
            Height          =   280
            Left            =   6780
            Picture         =   "frmItems.frx":68EB
            Style           =   1  'Graphical
            TabIndex        =   124
            ToolTipText     =   "View Credit Application History"
            Top             =   480
            Width           =   255
         End
         Begin VB.CommandButton cmdEnqOnReturn 
            Height          =   280
            Left            =   6780
            Picture         =   "frmItems.frx":6B35
            Style           =   1  'Graphical
            TabIndex        =   115
            ToolTipText     =   "View Returns Details"
            Top             =   120
            Width           =   255
         End
         Begin VB.CommandButton cmdEnqCustOrder 
            Height          =   280
            Left            =   2880
            Picture         =   "frmItems.frx":6D7F
            Style           =   1  'Graphical
            TabIndex        =   121
            ToolTipText     =   "View Credit Application History"
            Top             =   480
            Width           =   255
         End
         Begin VB.CommandButton cmdEnqOnOrder 
            Height          =   280
            Left            =   2880
            Picture         =   "frmItems.frx":6FC9
            Style           =   1  'Graphical
            TabIndex        =   112
            ToolTipText     =   "View Purchase Order details"
            Top             =   120
            Width           =   255
         End
         Begin VB.CommandButton cmdEnqOnDisplay 
            Height          =   280
            Left            =   10500
            Picture         =   "frmItems.frx":7213
            Style           =   1  'Graphical
            TabIndex        =   118
            ToolTipText     =   "View display items"
            Top             =   120
            Width           =   255
         End
         Begin VB.Frame frWeeklyTotals 
            Caption         =   "WeeklyTotals"
            Height          =   2235
            Left            =   0
            TabIndex        =   127
            Top             =   2040
            Width           =   11655
            Begin FPSpreadADO.fpSpread sprdWeekTot 
               Height          =   1935
               Left            =   1560
               TabIndex        =   128
               Top             =   180
               Width           =   7335
               _Version        =   458752
               _ExtentX        =   12938
               _ExtentY        =   3413
               _StockProps     =   64
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MaxCols         =   6
               MaxRows         =   14
               RetainSelBlock  =   0   'False
               RowHeaderDisplay=   0
               ScrollBarExtMode=   -1  'True
               ScrollBarMaxAlign=   0   'False
               ScrollBars      =   2
               ScrollBarShowMax=   0   'False
               SpreadDesigner  =   "frmItems.frx":745D
            End
         End
         Begin VB.Label Label24 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Qty On Return"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   3960
            TabIndex        =   113
            Top             =   120
            Width           =   1335
         End
         Begin VB.Label lblQtyOnReturn 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0.00"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   5520
            TabIndex        =   114
            Top             =   120
            Width           =   1215
         End
         Begin VB.Label lblQtyOnCustOrder 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0.00"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   1620
            TabIndex        =   120
            Top             =   480
            Width           =   1215
         End
         Begin VB.Label lblQtyOnCustOrderlbl 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Qty on Cust Order"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   60
            TabIndex        =   119
            Top             =   480
            Width           =   1695
         End
         Begin VB.Label lblQtyForBrancheslbl 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Qty For Branches"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   3960
            TabIndex        =   122
            Top             =   480
            Width           =   1575
         End
         Begin VB.Label lblQtyForBranches 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0.00"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   5520
            TabIndex        =   123
            Top             =   480
            Width           =   1215
         End
         Begin VB.Label lblQtyOnOrder 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0.00"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   1620
            TabIndex        =   111
            Top             =   120
            Width           =   1215
         End
         Begin VB.Label Label6 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Qty On Order"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   60
            TabIndex        =   110
            Top             =   120
            Width           =   1335
         End
         Begin VB.Label lblQtyOnDisplay 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0.00"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   9360
            TabIndex        =   117
            Top             =   120
            Width           =   1095
         End
         Begin VB.Label Label9 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "On Display Qty"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   7800
            TabIndex        =   116
            Top             =   120
            Width           =   1455
         End
      End
      Begin VB.Frame frEANs 
         BorderStyle     =   0  'None
         Height          =   3735
         Left            =   -74880
         TabIndex        =   101
         Top             =   480
         Width           =   11715
         Begin VB.CommandButton cmdNewEAN 
            Appearance      =   0  'Flat
            Caption         =   "New EAN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   10260
            TabIndex        =   105
            Top             =   2280
            Width           =   1335
         End
         Begin VB.CommandButton cmdDelEAN 
            Appearance      =   0  'Flat
            Caption         =   "Del EAN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   8820
            TabIndex        =   104
            Top             =   2280
            Width           =   1335
         End
         Begin VB.CommandButton cmdSaveEAN 
            Appearance      =   0  'Flat
            Caption         =   "Save Updates"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   7440
            TabIndex        =   103
            Top             =   2280
            Visible         =   0   'False
            Width           =   1395
         End
         Begin FPSpreadADO.fpSpread sprdEANs 
            Height          =   2175
            Left            =   60
            TabIndex        =   102
            Top             =   0
            Width           =   11535
            _Version        =   458752
            _ExtentX        =   20346
            _ExtentY        =   3836
            _StockProps     =   64
            ArrowsExitEditMode=   -1  'True
            EditEnterAction =   4
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxCols         =   4
            MaxRows         =   1
            RetainSelBlock  =   0   'False
            RowHeaderDisplay=   0
            ScrollBars      =   2
            ShadowColor     =   12632256
            SpreadDesigner  =   "frmItems.frx":7C33
            UserResize      =   0
            VisibleCols     =   4
            VisibleRows     =   1
         End
      End
      Begin VB.Frame frSupplier 
         BorderStyle     =   0  'None
         Height          =   3855
         Left            =   -74880
         TabIndex        =   100
         Top             =   480
         Width           =   9375
         Begin VB.Label lblSupplierlbl 
            Caption         =   "Supplier"
            Height          =   255
            Left            =   0
            TabIndex        =   142
            Top             =   0
            Width           =   855
         End
         Begin VB.Label lblSupplier 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   " "
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   960
            TabIndex        =   141
            Top             =   0
            Width           =   3495
         End
         Begin VB.Label Label25 
            Caption         =   "Address"
            Height          =   255
            Left            =   0
            TabIndex        =   140
            Top             =   480
            Width           =   855
         End
         Begin VB.Label lblSuppAddress 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   975
            Left            =   960
            TabIndex        =   139
            Top             =   480
            Width           =   3495
         End
         Begin VB.Label Label27 
            Caption         =   "Phone No"
            Height          =   255
            Left            =   0
            TabIndex        =   138
            Top             =   1680
            Width           =   855
         End
         Begin VB.Label lblSuppPhoneNo 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   960
            TabIndex        =   137
            Top             =   1680
            Width           =   1695
         End
         Begin VB.Label Label31 
            Caption         =   "Fax No"
            Height          =   255
            Left            =   0
            TabIndex        =   136
            Top             =   2040
            Width           =   735
         End
         Begin VB.Label lblSuppfaxNo 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   960
            TabIndex        =   135
            Top             =   2040
            Width           =   1695
         End
      End
      Begin VB.Frame frMovements 
         BorderStyle     =   0  'None
         Caption         =   " "
         Height          =   4095
         Left            =   -74880
         TabIndex        =   99
         Top             =   480
         Width           =   11655
         Begin VB.PictureBox frCriteria 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            ForeColor       =   &H80000008&
            Height          =   2415
            Left            =   2880
            ScaleHeight     =   2385
            ScaleWidth      =   5685
            TabIndex        =   144
            Top             =   720
            Visible         =   0   'False
            Width           =   5715
            Begin VB.TextBox Text5 
               Alignment       =   1  'Right Justify
               Enabled         =   0   'False
               Height          =   285
               Left            =   4320
               TabIndex        =   154
               Text            =   "0.00"
               Top             =   1440
               Width           =   1215
            End
            Begin VB.TextBox txtRemSDate 
               Appearance      =   0  'Flat
               Height          =   285
               Left            =   1440
               TabIndex        =   153
               ToolTipText     =   "First Transaction to be retrieved from (DDMMYYYY)"
               Top             =   360
               Width           =   975
            End
            Begin VB.TextBox txtRemEDate 
               Appearance      =   0  'Flat
               Height          =   285
               Left            =   2760
               TabIndex        =   152
               ToolTipText     =   "Last Transaction to be retrieved from (DDMMYYYY)"
               Top             =   360
               Width           =   975
            End
            Begin VB.ComboBox cmbCritTran 
               Height          =   315
               ItemData        =   "frmItems.frx":80F4
               Left            =   1440
               List            =   "frmItems.frx":8116
               Sorted          =   -1  'True
               Style           =   2  'Dropdown List
               TabIndex        =   151
               Top             =   720
               Width           =   3135
            End
            Begin VB.TextBox txtCritRef 
               Height          =   285
               Left            =   2760
               TabIndex        =   150
               Top             =   1080
               Width           =   1815
            End
            Begin VB.TextBox txtCritAmt 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   2760
               TabIndex        =   149
               Text            =   "0.00"
               Top             =   1440
               Width           =   1215
            End
            Begin VB.CommandButton cmdEscCrit 
               Caption         =   "Cancel"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   4500
               TabIndex        =   148
               Top             =   1860
               Width           =   1035
            End
            Begin VB.CommandButton cmdApplyCrit 
               Caption         =   "Apply"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   3360
               TabIndex        =   147
               Top             =   1860
               Width           =   1035
            End
            Begin VB.ComboBox cmbRemRef 
               Height          =   315
               ItemData        =   "frmItems.frx":81A0
               Left            =   1440
               List            =   "frmItems.frx":81AD
               Style           =   2  'Dropdown List
               TabIndex        =   146
               Top             =   1080
               Width           =   1215
            End
            Begin VB.ComboBox cmbRemAmt 
               Height          =   315
               ItemData        =   "frmItems.frx":81C1
               Left            =   1440
               List            =   "frmItems.frx":81D4
               Style           =   2  'Dropdown List
               TabIndex        =   145
               Top             =   1440
               Width           =   1215
            End
            Begin VB.Label Label29 
               BackStyle       =   0  'Transparent
               Caption         =   "to"
               Height          =   255
               Left            =   4080
               TabIndex        =   161
               Top             =   1440
               Width           =   255
            End
            Begin VB.Label lbl 
               BackStyle       =   0  'Transparent
               Height          =   285
               Left            =   120
               TabIndex        =   160
               Top             =   360
               Width           =   495
            End
            Begin VB.Label Label40 
               BackStyle       =   0  'Transparent
               Caption         =   "Tran Type"
               Height          =   255
               Left            =   120
               TabIndex        =   159
               Top             =   720
               Width           =   1095
            End
            Begin VB.Label Label39 
               BackStyle       =   0  'Transparent
               Caption         =   "Reference"
               Height          =   255
               Left            =   120
               TabIndex        =   158
               Top             =   1080
               Width           =   1095
            End
            Begin VB.Label Label38 
               BackStyle       =   0  'Transparent
               Caption         =   "Quantity"
               Height          =   255
               Left            =   120
               TabIndex        =   157
               Top             =   1440
               Width           =   975
            End
            Begin VB.Label Label28 
               BackStyle       =   0  'Transparent
               Caption         =   "to"
               Height          =   255
               Left            =   2520
               TabIndex        =   156
               Top             =   360
               Width           =   255
            End
            Begin VB.Label lbltitle 
               BackStyle       =   0  'Transparent
               Caption         =   "Select Transaction History Display Criteria"
               Height          =   285
               Left            =   120
               TabIndex        =   155
               Top             =   60
               Width           =   5475
            End
         End
         Begin VB.CommandButton cmdTranFilter 
            Caption         =   "Filters"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   0
            TabIndex        =   82
            Top             =   3720
            Width           =   1035
         End
         Begin VB.CommandButton cmdExport 
            Caption         =   "Export"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   10560
            TabIndex        =   83
            Top             =   3720
            Width           =   1095
         End
         Begin FPSpreadADO.fpSpread sprdTrans 
            Height          =   3615
            Left            =   0
            TabIndex        =   81
            Top             =   0
            Width           =   11640
            _Version        =   458752
            _ExtentX        =   20532
            _ExtentY        =   6376
            _StockProps     =   64
            ArrowsExitEditMode=   -1  'True
            BackColorStyle  =   1
            ColHeaderDisplay=   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxCols         =   12
            MaxRows         =   1
            RowHeaderDisplay=   0
            SpreadDesigner  =   "frmItems.frx":81F1
            UserResize      =   1
            VisibleCols     =   5
            VisibleRows     =   1
         End
      End
      Begin VB.Frame frConfig 
         BorderStyle     =   0  'None
         Height          =   4215
         Left            =   120
         TabIndex        =   85
         Top             =   360
         Width           =   11655
         Begin ucEditNumber.ucNumberText ntxtNoShelfLabels 
            Height          =   285
            Left            =   10200
            TabIndex        =   66
            Top             =   120
            Width           =   975
            _ExtentX        =   1720
            _ExtentY        =   503
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinimumValue    =   0
         End
         Begin VB.CheckBox chkAllowReturns 
            Alignment       =   1  'Right Justify
            Caption         =   "Allow Returns"
            Height          =   255
            Left            =   6000
            TabIndex        =   63
            Top             =   2280
            Width           =   1815
         End
         Begin VB.CheckBox chkDecorItem 
            Alignment       =   1  'Right Justify
            Caption         =   "Decor Item"
            Height          =   255
            Left            =   6000
            TabIndex        =   62
            Top             =   1920
            Width           =   1815
         End
         Begin VB.CheckBox chkBinJob 
            Alignment       =   1  'Right Justify
            Caption         =   "Bin Job"
            Height          =   255
            Left            =   6000
            TabIndex        =   61
            Top             =   1560
            Width           =   1815
         End
         Begin VB.CheckBox chkCatchAll 
            Alignment       =   1  'Right Justify
            Caption         =   "Catch All"
            Height          =   255
            Left            =   6000
            TabIndex        =   60
            Top             =   1200
            Width           =   1815
         End
         Begin VB.CheckBox chkWarranty 
            Alignment       =   1  'Right Justify
            Caption         =   "Warranty"
            Height          =   255
            Left            =   6000
            TabIndex        =   59
            Top             =   840
            Width           =   1815
         End
         Begin VB.CheckBox chkPrePriced 
            Alignment       =   1  'Right Justify
            Caption         =   "Pre-priced"
            Height          =   255
            Left            =   6000
            TabIndex        =   58
            Top             =   480
            Width           =   1815
         End
         Begin VB.CheckBox chkItemTagged 
            Alignment       =   1  'Right Justify
            Caption         =   "Item Tagged"
            Height          =   255
            Left            =   6000
            TabIndex        =   57
            Top             =   120
            Width           =   1815
         End
         Begin VB.TextBox txtStarRate 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   10200
            TabIndex        =   68
            Text            =   "1"
            Top             =   480
            Width           =   975
         End
         Begin VB.CheckBox chkDiscAllowed 
            Alignment       =   1  'Right Justify
            Caption         =   "Discount Allowed"
            Height          =   255
            Left            =   6000
            TabIndex        =   64
            Top             =   2640
            Width           =   1815
         End
         Begin VB.CheckBox chkWarehoused 
            Alignment       =   1  'Right Justify
            Caption         =   "Warehouse Item"
            Height          =   255
            Left            =   8040
            TabIndex        =   69
            Top             =   1200
            Width           =   2295
         End
         Begin VB.CheckBox chk4MDisc 
            Alignment       =   1  'Right Justify
            Caption         =   "4 SQM Discount"
            Height          =   255
            Left            =   8040
            TabIndex        =   70
            Top             =   1560
            Width           =   2295
         End
         Begin VB.TextBox txtFlag1 
            Height          =   285
            Left            =   9600
            TabIndex        =   72
            Top             =   1920
            Width           =   1935
         End
         Begin VB.TextBox txtFlag2 
            Height          =   285
            Left            =   9600
            TabIndex        =   74
            Top             =   2280
            Width           =   1935
         End
         Begin VB.TextBox txtFlag3 
            Height          =   285
            Left            =   9600
            TabIndex        =   76
            Top             =   2640
            Width           =   1935
         End
         Begin VB.TextBox txtFlag5 
            Height          =   285
            Left            =   9600
            TabIndex        =   80
            Top             =   3360
            Width           =   1935
         End
         Begin VB.TextBox txtFlag4 
            Height          =   285
            Left            =   9600
            TabIndex        =   78
            Top             =   3000
            Width           =   1935
         End
         Begin VB.Frame frUnitConfig 
            Caption         =   "Unit configuration"
            Height          =   4155
            Left            =   0
            TabIndex        =   55
            Top             =   0
            Width           =   5895
            Begin FPSpreadADO.fpSpread sprdUnits 
               Height          =   3615
               Left            =   120
               TabIndex        =   56
               Top             =   360
               Width           =   5655
               _Version        =   458752
               _ExtentX        =   9975
               _ExtentY        =   6376
               _StockProps     =   64
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MaxCols         =   7
               MaxRows         =   1
               RetainSelBlock  =   0   'False
               RowHeaderDisplay=   0
               SpreadDesigner  =   "frmItems.frx":8799
               UserResize      =   0
            End
         End
         Begin VB.Label lblStarRate 
            Caption         =   "Star Rating"
            Height          =   255
            Left            =   8040
            TabIndex        =   67
            Top             =   480
            Width           =   2055
         End
         Begin VB.Label Label7 
            Caption         =   "No of Shelf Labels"
            Height          =   255
            Left            =   8040
            TabIndex        =   65
            Top             =   120
            Width           =   1695
         End
         Begin VB.Label lblFlag1 
            Caption         =   "lblFlag1"
            Height          =   255
            Left            =   8040
            TabIndex        =   71
            Top             =   1920
            Width           =   1575
         End
         Begin VB.Label lblFlag2 
            Caption         =   "lblFlag2"
            Height          =   255
            Left            =   8040
            TabIndex        =   73
            Top             =   2280
            Width           =   1575
         End
         Begin VB.Label lblFlag3 
            Caption         =   "lblFlag3"
            Height          =   255
            Left            =   8040
            TabIndex        =   75
            Top             =   2640
            Width           =   1575
         End
         Begin VB.Label lblFlag4 
            Caption         =   "lblFlag4"
            Height          =   255
            Left            =   8040
            TabIndex        =   77
            Top             =   3000
            Width           =   1575
         End
         Begin VB.Label lblFlag5 
            Caption         =   "lblFlag5"
            Height          =   255
            Left            =   8040
            TabIndex        =   79
            Top             =   3360
            Width           =   1575
         End
      End
   End
   Begin VB.Frame frDetails 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      Height          =   5775
      Left            =   0
      TabIndex        =   0
      Top             =   360
      Width           =   12015
      Begin LpADOLib.fpComboAdo cmbStSearch 
         Height          =   315
         Left            =   1200
         TabIndex        =   6
         Top             =   120
         Visible         =   0   'False
         Width           =   6375
         _Version        =   196608
         _ExtentX        =   11245
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Text            =   ""
         Columns         =   3
         Sorted          =   0
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         WrapList        =   0   'False
         WrapWidth       =   0
         AutoSearch      =   1
         SearchMethod    =   0
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   0
         ThreeDInsideStyle=   1
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   2
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         DataFieldList   =   ""
         ColumnEdit      =   -1
         ColumnBound     =   -1
         Style           =   2
         MaxDrop         =   8
         ListWidth       =   -1
         EditHeight      =   -1
         GrayAreaColor   =   -2147483633
         ListLeftOffset  =   0
         ComboGap        =   -2
         MaxEditLen      =   150
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   -1  'True
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         EnableClickEvent=   -1  'True
         ListPosition    =   0
         ButtonThreeDAppearance=   0
         DataMemberList  =   ""
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         AutoSearchFill  =   0   'False
         AutoSearchFillDelay=   500
         EditMarginLeft  =   1
         EditMarginTop   =   1
         EditMarginRight =   0
         EditMarginBottom=   3
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         AutoMenu        =   -1  'True
         EditAlignH      =   0
         EditAlignV      =   0
         ColDesigner     =   "frmItems.frx":A73E
      End
      Begin ucEditNumber.ucNumberText ntxtSellPrice 
         Height          =   285
         Left            =   1440
         TabIndex        =   30
         Top             =   1200
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin ucEditNumber.ucNumberText ntxtSpecialPrice 
         Height          =   285
         Left            =   10680
         TabIndex        =   36
         Top             =   1200
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinimumValue    =   0
      End
      Begin ucEditNumber.ucNumberText ntxtMinLevel 
         Height          =   285
         Left            =   5760
         TabIndex        =   50
         Top             =   2280
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinimumValue    =   0
      End
      Begin ucEditNumber.ucNumberText ntxtWeight 
         Height          =   285
         Left            =   10680
         TabIndex        =   20
         Top             =   480
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinimumValue    =   0
      End
      Begin ucEditNumber.ucNumberText ntxtHeight 
         Height          =   285
         Left            =   8520
         TabIndex        =   18
         Top             =   480
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin ucEditNumber.ucNumberText ntxtWidth 
         Height          =   285
         Left            =   6840
         TabIndex        =   15
         Top             =   480
         Width           =   735
         _ExtentX        =   1296
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.ComboBox cmbStatus 
         Height          =   315
         ItemData        =   "frmItems.frx":AA81
         Left            =   10680
         List            =   "frmItems.frx":AA8B
         Style           =   2  'Dropdown List
         TabIndex        =   46
         Top             =   1920
         Width           =   1215
      End
      Begin VB.ComboBox cmbVAT 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   4800
         Style           =   2  'Dropdown List
         TabIndex        =   32
         Top             =   1200
         Width           =   1095
      End
      Begin VB.TextBox txtPriceBand 
         Height          =   285
         Left            =   10680
         MaxLength       =   1
         TabIndex        =   40
         Text            =   "0"
         Top             =   1560
         Width           =   1215
      End
      Begin VB.TextBox txtSellInPacks 
         Height          =   285
         Left            =   5760
         TabIndex        =   44
         Top             =   1920
         Width           =   1215
      End
      Begin VB.TextBox txtBuyInPacks 
         Height          =   285
         Left            =   1440
         TabIndex        =   42
         Top             =   1920
         Width           =   1215
      End
      Begin VB.Frame frHide 
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   3360
         TabIndex        =   87
         Top             =   840
         Visible         =   0   'False
         Width           =   8535
         Begin VB.Label Label14 
            BackStyle       =   0  'Transparent
            Caption         =   "Last Sold"
            Height          =   255
            Left            =   0
            TabIndex        =   23
            Top             =   0
            Width           =   975
         End
         Begin VB.Label lblLastSold 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   1440
            TabIndex        =   24
            Top             =   0
            Width           =   1095
         End
         Begin VB.Label lblLastOrdered 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   4560
            TabIndex        =   26
            Top             =   0
            Width           =   1215
         End
         Begin VB.Label Label47 
            BackStyle       =   0  'Transparent
            Caption         =   "Last Ordered"
            Height          =   255
            Left            =   2880
            TabIndex        =   25
            Top             =   0
            Width           =   1215
         End
         Begin VB.Label lblLastReceived 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   7320
            TabIndex        =   28
            Top             =   0
            Width           =   1215
         End
         Begin VB.Label Label49 
            BackStyle       =   0  'Transparent
            Caption         =   "Last Received"
            Height          =   255
            Left            =   5880
            TabIndex        =   27
            Top             =   0
            Width           =   1335
         End
      End
      Begin VB.TextBox txtSize 
         Height          =   285
         Left            =   10080
         TabIndex        =   10
         Top             =   120
         Width           =   1815
      End
      Begin VB.TextBox txtCatalogueNo 
         Height          =   285
         Left            =   1440
         TabIndex        =   22
         Top             =   840
         Width           =   1695
      End
      Begin VB.CheckBox chkItemObs 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFC0C0&
         Caption         =   "Item Obsolete"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   480
         Width           =   1575
      End
      Begin VB.CommandButton cmdSearch 
         Height          =   255
         Left            =   3240
         Picture         =   "frmItems.frx":AAA2
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   120
         Width           =   255
      End
      Begin VB.CommandButton cmdPrev 
         Height          =   255
         Left            =   1200
         Picture         =   "frmItems.frx":ACEC
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   120
         Width           =   255
      End
      Begin VB.CommandButton cmdNext 
         Height          =   255
         Left            =   3000
         Picture         =   "frmItems.frx":AFC2
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   120
         Width           =   255
      End
      Begin VB.TextBox txtDescription 
         Height          =   285
         Left            =   4800
         MaxLength       =   30
         TabIndex        =   8
         Top             =   120
         Width           =   4335
      End
      Begin VB.Frame frLevels 
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   120
         TabIndex        =   53
         Top             =   2280
         Visible         =   0   'False
         Width           =   11895
         Begin ucEditNumber.ucNumberText ntxtMaxLevel 
            Height          =   285
            Left            =   10560
            TabIndex        =   52
            Top             =   0
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   503
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblStockAvail 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0.00"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   1320
            TabIndex        =   48
            Top             =   0
            Width           =   1215
         End
         Begin VB.Label Label5 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Min Level"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   4320
            TabIndex        =   49
            Top             =   0
            Width           =   855
         End
         Begin VB.Label Label17 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Max Level"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   9120
            TabIndex        =   51
            Top             =   0
            Width           =   1095
         End
         Begin VB.Label lblCrUsedlbl 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Current Level"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   0
            TabIndex        =   47
            Top             =   0
            Width           =   1335
         End
      End
      Begin VB.Label lblStatuslbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Status"
         Height          =   255
         Left            =   9240
         TabIndex        =   45
         Top             =   1920
         Width           =   1095
      End
      Begin VB.Label Label11 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "VAT"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   3360
         TabIndex        =   31
         Top             =   1200
         Width           =   495
      End
      Begin VB.Label lblSellPriceEx 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   7920
         TabIndex        =   34
         Top             =   1200
         Width           =   1215
      End
      Begin VB.Label Label19 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Sell Price(Exc)"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   6240
         TabIndex        =   33
         Top             =   1200
         Width           =   1335
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Weight"
         Height          =   255
         Left            =   9240
         TabIndex        =   19
         Top             =   480
         Width           =   615
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Selling Units"
         Height          =   255
         Left            =   4440
         TabIndex        =   43
         Top             =   1920
         Width           =   1335
      End
      Begin VB.Label Label15 
         BackStyle       =   0  'Transparent
         Caption         =   "Height"
         Height          =   255
         Left            =   7800
         TabIndex        =   17
         Top             =   480
         Width           =   615
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Width"
         Height          =   255
         Left            =   6240
         TabIndex        =   16
         Top             =   480
         Width           =   615
      End
      Begin VB.Label Label50 
         BackStyle       =   0  'Transparent
         Caption         =   "Size"
         Height          =   255
         Left            =   9240
         TabIndex        =   9
         Top             =   120
         Width           =   615
      End
      Begin VB.Label Label45 
         BackStyle       =   0  'Transparent
         Caption         =   "Man Prod Code"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   840
         Width           =   1455
      End
      Begin VB.Label Label43 
         BackStyle       =   0  'Transparent
         Caption         =   "Date Created"
         Height          =   255
         Left            =   3360
         TabIndex        =   13
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label lblDateObselete 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2040
         TabIndex        =   12
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label Label33 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Price Band"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   9240
         TabIndex        =   39
         Top             =   1560
         Width           =   1455
      End
      Begin VB.Label lblItemID 
         BackStyle       =   0  'Transparent
         Caption         =   "lblItemID"
         Height          =   255
         Left            =   7200
         TabIndex        =   86
         Top             =   3480
         Width           =   1215
      End
      Begin VB.Label Label13 
         BackStyle       =   0  'Transparent
         Caption         =   "Special Price"
         Height          =   255
         Left            =   9240
         TabIndex        =   35
         Top             =   1200
         Width           =   1335
      End
      Begin VB.Label lblDateCreated 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   4800
         TabIndex        =   14
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label Label52 
         BackStyle       =   0  'Transparent
         Caption         =   "Buy Units"
         Height          =   255
         Left            =   120
         TabIndex        =   41
         Top             =   1920
         Width           =   855
      End
      Begin VB.Label Label34 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Department"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   120
         TabIndex        =   37
         Top             =   1560
         Width           =   975
      End
      Begin VB.Label lblPartCode 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1440
         TabIndex        =   3
         Top             =   120
         Width           =   1575
      End
      Begin VB.Label Label26 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Product Group"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   4440
         TabIndex        =   38
         Top             =   1560
         Width           =   1575
      End
      Begin VB.Label Label8 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Sell Price"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label Label18 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Part Code"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   1455
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Description"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   3720
         TabIndex        =   7
         Top             =   120
         Width           =   1095
      End
   End
   Begin VB.Menu mnuItems 
      Caption         =   "&Items"
      Begin VB.Menu mnuNew 
         Caption         =   "&New"
      End
      Begin VB.Menu mnuSave 
         Caption         =   "&Save"
      End
      Begin VB.Menu mnuDelete 
         Caption         =   "&Delete"
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFuzzy 
         Caption         =   "Fuzzy Match"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuILocate 
         Caption         =   "Locate Item"
         Shortcut        =   {F3}
      End
      Begin VB.Menu mnuRefresh 
         Caption         =   "Refresh Details"
      End
      Begin VB.Menu mnuSep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "View"
      Begin VB.Menu mnuINext 
         Caption         =   "Move Next"
      End
      Begin VB.Menu mnuIPrev 
         Caption         =   "Move Previous"
      End
      Begin VB.Menu mnuVSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuVExpandDetails 
         Caption         =   "Expand Details &+"
      End
      Begin VB.Menu mnuVShrinkDetails 
         Caption         =   "Shrink Details &-"
      End
      Begin VB.Menu mnuVSep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuVSortBy 
         Caption         =   "Sort By"
         Begin VB.Menu mnuVSBPartCode 
            Caption         =   "Part Code"
         End
         Begin VB.Menu mnuVSBDescription 
            Caption         =   "Description"
            Checked         =   -1  'True
         End
      End
   End
End
Attribute VB_Name = "frmItems"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'<CAMH>****************************************************************************************
'* Module : frmItems
'* Date   : 03/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Item Editor/frmItems.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 7/06/04 10:14 $ $Revision: 11 $
'* Versions:
'*  03/10/02    mauricem    1.0.0   Header added.
'*  30/03/05    tjnorris    1.1.0   Updated to work with changes made to ItemFilter.ocx
'*                                  Initial attempt for Wickes conversion.
'*
'*  28/11/06    DaveF       1.1.16  Compiled to use the new interface only ItemFilter_UC.
'*
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME   As String = "frmItems"

Const TranPayment = 1
Const DelEntry = 2
Const NewEntry = 3

Const UNIT_DESC     As Long = 1
Const UNIT_THISITEM As Long = 2
Const UNIT_BUYIN    As Long = 3
Const UNIT_SELLIN   As Long = 4
Const UNIT_FACTOR   As Long = 5
Const UNIT_CODE     As Long = 6
Const UNIT_EDITED   As Long = 7

Const EAN_NO        As Long = 1
Const EAN_USED      As Long = 2
Const EAN_DATE      As Long = 3

Const PRM_FLAG1_LABEL As Long = 111
Const PRM_FLAG2_LABEL As Long = 112
Const PRM_FLAG3_LABEL As Long = 113
Const PRM_FLAG4_LABEL As Long = 114
Const PRM_FLAG5_LABEL As Long = 115

Const TAB_CONFIG = 0
Const TAB_TRANS = 1
Const TAB_SUPPLIER = 2
Const TAB_EANS = 3
Const TAB_SUMMARY = 4
Const TAB_PRICES = 5
Const TAB_SPARE = 6
Const TAB_TEXT = 7

Const RGB_OldColour As Long = RGB_WHITE

Dim rstUnits        As Recordset
Dim cItem           As cInventory
Dim data_edited     As Integer

Dim Rem_Tran_All    As Byte
Dim Rem_Tran_Inv    As Byte
Dim Rem_Ref_All     As Byte
Dim Rem_Amt_All     As Byte
Dim Status_All      As Byte
Dim Dflt_SortBy     As Byte
Dim lTabTop         As Long

Dim adoconn         As ADODB.Connection
Dim blnFullView     As Boolean ' flag set upon load to indicate if Branch or Head Offive View

Dim blnRszConfig    As Boolean
Dim blnRszTrans     As Boolean
Dim blnRszSupp      As Boolean
Dim blnRszEANs      As Boolean
Dim blnRszSumm      As Boolean
Dim blnRszPrices    As Boolean
Dim blnRszCost      As Boolean
Dim blnRszInfo      As Boolean

Dim blnSuppLoaded   As Boolean
Dim blnEANLoaded    As Boolean
Dim blnTechLoaded   As Boolean

Dim mblnMultipleSuppliers As Boolean
Dim mblnIgnoreFirstNext As Boolean
Dim mstrSuppNo      As String 'hold current items supplier number for displaying when TAB clicked on

Private Sub ClearScreen()
Dim lngRowNo As Long

    'clear currently displayed Item from Main Screen
    lblPartCode.Caption = vbNullString
    txtDescription.Text = vbNullString
    txtSize.Text = vbNullString
    lblDateCreated.Caption = "N/A"
    ntxtWidth.Value = 0
    ntxtHeight.Value = 0
    ntxtWeight.Value = 0
    txtCatalogueNo.Text = vbNullString
    lblLastSold.Caption = "N/A"
    lblLastOrdered.Caption = "N/A"
    ntxtSpecialPrice.Value = 0
    ntxtSellPrice.Value = 0
    ntxtCostPrice.Value = 0
    lblStockAvail.Caption = "0.00"
    sprdUnits.Row = -1
    sprdUnits.Col = UNIT_THISITEM
    sprdUnits.Value = 0
    sprdUnits.Col = UNIT_BUYIN
    sprdUnits.Value = 0
    sprdUnits.Lock = True
    sprdUnits.Col = UNIT_SELLIN
    sprdUnits.Value = 0
    sprdUnits.Lock = True
    sprdUnits.Col = UNIT_FACTOR
    sprdUnits.Value = 0
    sprdUnits.Lock = True
    txtSellInPacks.Text = vbNullString
    txtBuyInPacks.Text = vbNullString
    ntxtMinLevel.Value = 0
    ntxtMaxLevel.Value = 0
    txtPriceBand.Text = "0"
    
    If cmbVAT.Enabled Then cmbVAT.ListIndex = -1
    cmbStatus.ListIndex = Status_All
'    ntxtActualWeight.Value = 0
'    ntxtProdWeight.Value = 0
    
    
    chkItemTagged.Value = 0
    chkPrePriced.Value = 0
    chkWarranty.Value = 0
    chkCatchAll.Value = 0
    chkBinJob.Value = 0
    chkDecorItem.Value = 0
    chkAllowReturns.Value = 0
    chkDiscAllowed.Value = 0
    ntxtNoShelfLabels.Value = 0
    lblMargin.Caption = "0.000"
    txtStarRate.Text = vbNullString
    cmbVAT.ListIndex = -1
    chkWarehoused.Value = 0
    chk4MDisc.Value = 0
    txtFlag1.Text = vbNullString
    txtFlag2.Text = vbNullString
    txtFlag3.Text = vbNullString
    txtFlag4.Text = vbNullString
    txtFlag5.Text = vbNullString
    sprdSuppliers.MaxRows = 0
    sprdSuppliers.MaxRows = 3
    
    sprdWeekTot.Col = 3
    sprdWeekTot.Row = -1
    sprdWeekTot.Text = 0
  
    If (Not blnFullView) Then
        For lngRowNo = 1 To sprdUnits.MaxRows Step 1
            sprdUnits.Row = lngRowNo
            sprdUnits.RowHidden = True
            
        Next lngRowNo
    End If
    
    lblSupplier.Caption = vbNullString
    lblSuppAddress.Caption = vbNullString
    lblSuppPhoneNo.Caption = vbNullString
    lblSuppfaxNo.Caption = vbNullString
    
    txtTechInfo.Text = vbNullString
    blnEANLoaded = False
    blnTechLoaded = False
  
End Sub


Private Sub cmbCritTran_GotFocus()
    Call SendKeys(KEY_DROPDOWN)

End Sub

Private Sub cmbRemAmt_GotFocus()
    Call SendKeys(KEY_DROPDOWN)

End Sub

Private Sub cmbRemRef_GotFocus()
    Call SendKeys(KEY_DROPDOWN)

End Sub

Private Sub cmbStatus_GotFocus()
    SendKeys (KEY_DROPDOWN)

End Sub

Private Sub cmbStSearch_CloseUp()

    If cmbStSearch.ListIndex = -1 Then Exit Sub
    
    cmbStSearch.Row = cmbStSearch.ListIndex
    cmbStSearch.Text = cmbStSearch.List(cmbStSearch.Row)
    Call DisplayItem
    cmbStSearch.Visible = False
    mblnIgnoreFirstNext = True

End Sub

Private Sub cmbStSearch_GotFocus()
    cmbStSearch.ListDown = True

End Sub

Private Sub cmbStSearch_LostFocus()
    cmbStSearch.Visible = False

End Sub

Private Sub cmbRemAmt_Click()

    If cmbRemAmt.Text <> "All" Then
        txtCritAmt.Enabled = True
        If txtCritAmt.Visible Then txtCritAmt.SetFocus
    
    Else
        txtCritAmt.Enabled = False
    
    End If

End Sub

Private Sub cmbRemRef_Click()
  
    If cmbRemRef.Text <> "All" Then
        txtCritRef.Enabled = True
        If txtCritRef.Visible Then txtCritRef.SetFocus
        
    Else
        txtCritRef.Enabled = False
        
    End If

End Sub
'Private Sub cmbStSearch_VirtualRequest(ActionRequested As Integer, RowFirst As Long, RowCount As Long, pos As Long, Ret As Long)

'    If Not cItems Is Nothing Then Call Process_Combo_VirtualRequest(cmbStSearch, cItems, ActionRequested, RowFirst, RowCount, pos, Ret)

'End Sub

Private Sub cmbVAT_Click()
    Call DisplayPriceExVAT

End Sub

Private Sub DisplayPriceExVAT()
    lblSellPriceEx.Caption = Format$(ntxtSellPrice.Value / ((100 + Val(cmbVAT.Text)) / 100), "0.00")

End Sub

Private Sub cmbVAT_GotFocus()
    SendKeys (KEY_DROPDOWN)

End Sub

Private Sub cmdApplyCrit_Click()
Dim dteStartDate As Date
Dim dteEndDate   As Date

    Screen.MousePointer = 11
    ucpbItem.Title = "Accessing Item History"
    ucpbItem.Caption1 = "Retrieving Transaction History within Criteria"
    ucpbItem.Visible = True
    frCriteria.Visible = False
    'retreive starting date for period if date left blank
    
    '**************************************************
    '* convert entered date from DD/MM/YY to YYYYMMDD *
    '* to match how date stored in database           *
    '**************************************************
    If LenB(txtRemSDate.Text) <> 0 Then dteStartDate = GetDate(txtRemSDate.Text)
    If LenB(txtRemEDate.Text) <> 0 Then dteEndDate = GetDate(txtRemEDate.Text)
    DoEvents
  
    MsgBox "Check Date Criteria and Show Type below"
    Call FillInTransactions(lblPartCode.Caption, CRIT_ALL, dteStartDate, dteEndDate, cmbCritTran.ItemData(cmbCritTran.ListIndex), sprdTrans, ucpbItem, True)
    ucpbItem.Visible = False
    If (sprdTrans.Enabled And sprdTrans.Visible) Then Call sprdTrans.SetFocus
    Screen.MousePointer = 0

End Sub



Private Sub cmdEnqOnDisplay_Click()

    Load frmMaintDisplay
    frmMaintDisplay.LoadLocations (lblPartCode.Caption)
    Call frmMaintDisplay.Show(vbModal)

End Sub


Private Sub cmdEscCrit_Click()
    

    If (sprdTrans.Enabled And sprdTrans.Visible) Then Call sprdTrans.SetFocus
    frCriteria.Visible = False

End Sub

'Removed 1.1.0
'Private Sub cmdNewSupplier_Click()
'Dim cItems As Recordset
'
'    'Check if supplier combo populated, else fill in
''  If cmbItems.ListCount = 0 Then
''    cmbItems.AddItem "*CREATE ITEM*"
''    Call cItems.Open("SELECT PartCode,PartDescription1,SellingPrice,ItemID FROM Items WHERE SalesAccID > 0", adoconn)
''    While Not cItems.EOF
''      cmbItems.AddItem cItems.Fields("PartCode") & vbTab & cItems.Fields("PartDescription1")
''      cmbItems.Row = cmbItems.NewIndex
''      cmbItems.ItemData = cItems.Fields("ItemID")
''      cItems.MoveNext
''    Wend
''  End If
'
'    sprdSuppliers.LeftCol = 1
'    sprdSuppliers.Row = sprdSuppliers.MaxRows 'move to last row on contacts rable
'
'    If sprdSuppliers.Row > 0 Then 'verify only if data exists
'        sprdSuppliers.Col = 1
'        If LenB(sprdSuppliers.Text) = 0 Then
'            MsgBox ("Enter valid contact name for created entries"), , "Invalid function"
'            Exit Sub
'
'        End If
'    End If
'
'    'if last entry used then add new line to table
'    sprdSuppliers.MaxRows = sprdSuppliers.MaxRows + 1
'    sprdSuppliers.Col = 5
'    sprdSuppliers.Row = sprdSuppliers.MaxRows
'    sprdSuppliers.Text = NewEntry 'set new entry flag
'    sprdSuppliers.Col = 1
'    sprdSuppliers.Action = 0
'    sprdSuppliers.EditMode = True
'
'End Sub

'Removed 1.1.0
'Private Sub cmdHO_Click()
'Dim lHeight  As Long
'Dim lTabNo   As Long
'Dim lDispTab As Long
'
'    frLevels.Visible = Not frLevels.Visible
'    frHide.Visible = Not frHide.Visible
'
'    If (Not frHide.Visible) Then 'Head Office
'        blnFullView = True
'        Me.Caption = "Item Editor"
'        tabDetails.TabVisible(TAB_CONFIG) = False
'        tabDetails.TabVisible(TAB_TRANS) = False
'        tabDetails.TabVisible(TAB_SUMMARY) = False
'        lHeight = tabDetails.Height + tabDetails.Top
'        tabDetails.Top = frHide.Top + 120 + frHide.Height + frDetails.Top
'        tabDetails.Height = lHeight - tabDetails.Top
'        txtStarRate.Visible = False
'        lblStarRate.Visible = False
'
'    Else
'        blnFullView = False
'        Me.Caption = "Item Enquiry"
'        txtStarRate.Visible = True
'        lblStarRate.Visible = True
'        tabDetails.TabVisible(TAB_CONFIG) = True
'        lHeight = tabDetails.Height + tabDetails.Top - lTabTop
'        tabDetails.Top = lTabTop
'        tabDetails.Height = lHeight
'        Toolbar1.Buttons("Save").Visible = False
'        Toolbar1.Buttons("Delete").Visible = False
'        Toolbar1.Buttons("New").Visible = False
'        Toolbar1.Buttons("Sep1").Visible = False
'        Toolbar1.Buttons("Sep2").Visible = False
'        mnuSave.Visible = False
'        mnuDelete.Visible = False
'        mnuNew.Visible = False
'        mnuSep1.Visible = False
'        mnuSep2.Visible = False
'
'    End If
'
'    'Number the Tab captions with
'    'hotkeys for easy keyboard access
'    lDispTab = 1
'    For lTabNo = 0 To tabDetails.Tabs - 1 Step 1
'        If tabDetails.TabVisible(lTabNo) Then
'            tabDetails.TabCaption(lTabNo) = "&" & lDispTab & ". " & tabDetails.TabCaption(lTabNo)
'            lDispTab = lDispTab + 1
'
'        End If
'    Next lTabNo
'
'End Sub

Private Sub cmdNewEAN_Click()
    sprdEANs.MaxRows = sprdEANs.MaxRows + 1

End Sub

Private Sub cmdNext_Click()

    If (Not mblnIgnoreFirstNext) Then Call mnuINext_Click
    mblnIgnoreFirstNext = False
    
End Sub

Private Sub cmdNext_LostFocus()
    mblnIgnoreFirstNext = False

End Sub

Private Sub cmdPrev_Click()
  Call mnuIPrev_Click

End Sub


Private Sub cmdTranFilter_Click()

  frCriteria.Visible = True
  If txtRemSDate.Enabled Then txtRemSDate.SetFocus

End Sub

Private Sub DisplayItem()
Dim lngLineNo As Long
Dim lngRowNo  As Long
  
    On Error GoTo bad_Item

    Screen.MousePointer = vbHourglass
    
    'Get full Item BO from DB
    cmbStSearch.Col = 0
    lblItemID.Caption = cmbStSearch.ColText
    Set cItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
    Call cItem.IBo_AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, lblItemID.Caption)
    Call cItem.IBo_Load
    
    'Reset screen
    Call ClearScreen
    
    lblPartCode.Caption = cItem.PartCode
    txtDescription.Text = cItem.Description
'  lblQtyReserved.Caption = Format$(cItem.QuantityReserved, "0.000")
    lblDateObselete.Caption = DisplayDate(cItem.DateDeleted, False)
    
    If (Not IsDate(lblDateObselete.Caption)) Then
        lblDateObselete.Visible = False
        chkItemObs.Value = 0
        
    Else
        lblDateObselete.Visible = True
        chkItemObs.Value = 1
        
    End If
    
    lblDateCreated.Caption = DisplayDate(cItem.DateCreated, False)
    ntxtWeight.Value = cItem.Weight
    ntxtMinLevel.Value = cItem.MinQuantity
    ntxtMaxLevel.Value = cItem.MaxQuantity
    lblLastSold.Caption = DisplayDate(cItem.LastSold, False)
    ntxtSellPrice.Value = cItem.NormalSellPrice
    ntxtCostPrice.Value = cItem.CostPrice
    mstrSuppNo = cItem.SupplierNo
    lblLastOrdered.Caption = DisplayDate(cItem.LastOrdered, False)
    lblLastReceived.Caption = DisplayDate(cItem.LastReceived, False)
    lblStockAvail.Caption = Format$(cItem.QuantityAtHand, "0.000")
    
    'Fill in Configuration TAB
    sprdUnits.Col = UNIT_THISITEM
    sprdUnits.Lock = False
    'Show units factors used
    
    'Select current buy-in units
    sprdUnits.Col = UNIT_CODE
            
    'Select current sell-in units
    sprdUnits.Col = UNIT_CODE
    
    If (Not blnFullView) Then
        sprdUnits.Col = -1
        sprdUnits.Row = -1
        sprdUnits.Lock = True
        
    End If
               
    chkItemTagged.Value = Abs(cItem.ItemTagged)
    chkWarranty.Value = Abs(cItem.Warranty)
    chkCatchAll.Value = Abs(cItem.CatchAll)
    cmbVAT.ListIndex = -1
    
    For lngLineNo = 0 To cmbVAT.ListCount - 1 Step 1
        If cItem.VATRate = cmbVAT.ItemData(lngLineNo) Then
            cmbVAT.ListIndex = lngLineNo
            Exit For
            
        End If
    Next lngLineNo
    
    'Fill In Summary TAB
    lblQtyOnOrder.Caption = cItem.QuantityOnOrder
    If tabDetails.Tab = TAB_EANS Then Call LoadEANs
    
    lblMargin.Caption = Format$((Val(lblSellPriceEx.Caption) - ntxtCostPrice.Value) / Val(lblSellPriceEx.Caption) * 100, "0.000")
    
    If (tabDetails.Tab = TAB_SUPPLIER) And (Not mblnMultipleSuppliers) Then Call DisplaySupplier
    
    'fill in Period Totals - Fill in Values first
    sprdPeriodTot.Row = 1
    sprdPeriodTot.Col = 1
    sprdPeriodTot.Text = cItem.ValueSoldYesterday
    sprdPeriodTot.Col = 2
    sprdPeriodTot.Text = cItem.ValueSoldThisPeriod
    sprdPeriodTot.Col = 3
    sprdPeriodTot.Text = cItem.ValueSoldThisYear
    sprdPeriodTot.Col = 4
    sprdPeriodTot.Text = cItem.ValueSoldLastYear
    'fill in Period Totals - Fill in Units first
    sprdPeriodTot.Row = 2
    sprdPeriodTot.Col = 1
    sprdPeriodTot.Text = cItem.UnitsSoldYesterday
    sprdPeriodTot.Col = 2
    sprdPeriodTot.Text = cItem.UnitsSoldThisPeriod
    sprdPeriodTot.Col = 3
    sprdPeriodTot.Text = cItem.UnitsSoldThisYear
    sprdPeriodTot.Col = 4
    sprdPeriodTot.Text = cItem.UnitsSoldLastYear
    sprdPeriodTot.Col = 5
    sprdPeriodTot.Text = cItem.UnitsSoldLastPeriod
    sprdPeriodTot.Col = 8
    sprdPeriodTot.Text = cItem.DaysOutStockPeriod

    sprdWeekTot.MaxRows = 14
    sprdWeekTot.Col = 3
    
    For lngLineNo = 1 To 14 Step 1
        sprdWeekTot.Row = lngLineNo
        sprdWeekTot.Text = cItem.UnitsSoldWeek(lngLineNo)
        
    Next lngLineNo

    If LenB(cItem.Status) <> 0 Then
        cmbStatus.ListIndex = 1
        For lngLineNo = 0 To cmbStatus.ListCount - 1 Step 1
            If Left$(cmbStatus.List(lngLineNo), 2) = cItem.Status Then
                cmbStatus.ListIndex = lngLineNo
                Exit For
                
            End If
        Next lngLineNo
    End If

'  'extract Discount Rate
'  For lngLineNo = 0 To cmbDiscount.ListCount - 1 Step 1
'    If cmbDiscount.ItemData(lngLineNo) = cItem.DiscountCode Then
'      cmbDiscount.ListIndex = lngLineNo
'      Exit For
'    End If
'  Next lngLineNo
'  'extract Discount Rate

  cmbCritTran.ListIndex = Rem_Tran_All
  cmbRemAmt.ListIndex = Rem_Amt_All
  cmbRemRef.ListIndex = Rem_Ref_All
  txtCritRef.Text = vbNullString
  txtCritAmt.Text = "0.00"
  
  data_edited = False
  ucpbItem.Visible = False
  
  'If LenB(lblDateFmt.Caption) = 0 Then lblDateFmt.Caption = mstrDateFmt
  Screen.MousePointer = vbNormal

Exit Sub

bad_Item:

  If Err.Number <> 94 Then
      Call Err.Report(MODULE_NAME, "Display_Item", 1, True, "Display Item Details", "Error retreiving detected :-")
      Screen.MousePointer = 0
      ucpbItem.Visible = False
      Exit Sub
  End If
  Resume Next
  
End Sub


Private Sub fill_in_combos()
Dim lngItemNo As Long
Dim oUOM      As cUnitOfMeasure
Dim colBO     As Collection
Dim oVATRates As cVATRates
  
On Error GoTo bad_lookup_combos
  
'    Set oUOM = goDatabase.CreateBusinessObject(CLASSID_UNITOFMEASURE)
'
'    ucpbItem.Caption1 = "Loading Units of Measure"
'    ucpbItem.Refresh
'
'    Set colBO = oUOM.IBo_LoadMatches
'    sprdUnits.Row = -1
'    sprdUnits.Col = -1
'    sprdUnits.Lock = True
'    sprdUnits.MaxRows = 0
'
'    For lngItemNo = 1 To colBO.Count Step 1
'        sprdUnits.MaxRows = sprdUnits.MaxRows + 1
'        sprdUnits.Row = sprdUnits.MaxRows
'        sprdUnits.Col = UNIT_DESC
'        sprdUnits.Text = colBO(lngItemNo).Description
'        sprdUnits.Col = UNIT_CODE
'        sprdUnits.Text = colBO(lngItemNo).UnitOfMeasureCode
'
'    Next lngItemNo
'    Set colBO = Nothing

    ucpbItem.Caption1 = "Loading VAT Rates"
    ucpbItem.Refresh
    Set oVATRates = goDatabase.CreateBusinessObject(CLASSID_VATRATES)
    
    Call oVATRates.IBo_Load
    For lngItemNo = 1 To oVATRates.VATRateCount Step 1
        cmbVAT.AddItem (oVATRates.VATRate(lngItemNo))
        cmbVAT.ItemData(cmbVAT.NewIndex) = lngItemNo
    Next lngItemNo
    Set colBO = Nothing

  Exit Sub
  
bad_lookup_combos:
  
  MsgBox (Error$), vbExclamation, "Error accessing General Look-Ups"
  Exit Sub
  
End Sub

Private Sub LoadEANs()
Dim colEANs     As Collection
Dim oEAN        As cInventory
Dim lngItemNo   As Long
Dim blnVisible  As Boolean
    
    Set oEAN = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
    
    blnVisible = ucpbItem.Visible
    Screen.MousePointer = vbHourglass
    ucpbItem.Visible = True
    ucpbItem.Caption1 = "Loading EANs"
    ucpbItem.Refresh
    
    Call oEAN.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, lblPartCode.Caption)
    Call oEAN.AddLoadField(FID_INVENTORY_EAN)
    Call oEAN.AddLoadField(FID_INVENTORY_DateCreated)
    Call oEAN.AddLoadField(FID_INVENTORY_DateFirstStock)
    
    Set colEANs = oEAN.LoadMatches
    sprdEANs.Row = -1
    sprdEANs.Col = -1
    sprdEANs.Lock = True
    sprdEANs.MaxRows = 0
    
    For Each oEAN In colEANs
        sprdEANs.MaxRows = sprdEANs.MaxRows + 1
        sprdEANs.Row = sprdEANs.MaxRows
        sprdEANs.Col = EAN_NO
        sprdEANs.Text = oEAN.EAN
        sprdEANs.Col = EAN_USED
        sprdEANs.Text = oEAN.DateFirstStock
        sprdEANs.Col = EAN_DATE
        sprdEANs.Text = DisplayDate(oEAN.DateCreated, False)
    Next oEAN
    
    Set colEANs = Nothing
    
    blnEANLoaded = True
    ucpbItem.Visible = blnVisible
    Screen.MousePointer = vbNormal

End Sub

Private Sub LoadSuppliers()
Dim colEAN     As Collection
Dim oEAN        As cEAN
Dim lngItemNo   As Long
Dim blnVisible  As Boolean
    
    Set oEAN = goDatabase.CreateBusinessObject(CLASSID_EAN)
    
    blnVisible = ucpbItem.Visible
    Screen.MousePointer = vbHourglass
    ucpbItem.Visible = True
    ucpbItem.Caption1 = "Loading EANs"
    ucpbItem.Refresh
    
    Call oEAN.IBo_AddLoadFilter(CMP_EQUAL, FID_EAN_PartCode, lblPartCode.Caption)
    Set colEAN = oEAN.IBo_LoadMatches
    sprdSuppliers.Row = -1
    sprdSuppliers.Col = -1
    sprdSuppliers.Lock = True
    sprdSuppliers.MaxRows = 0
    
    For Each oEAN In colEAN
        sprdSuppliers.MaxRows = sprdSuppliers.MaxRows + 1
        sprdSuppliers.Row = sprdSuppliers.MaxRows
        sprdSuppliers.Col = EAN_NO
        sprdSuppliers.Text = oEAN.EANNumber
        sprdSuppliers.Col = EAN_USED
        'sprdSuppliers.Text = oEAN.AddedAtStore
        sprdSuppliers.Col = EAN_DATE
        'sprdSuppliers.Text = DisplayDate(oEAN.DateAdded, False)
    Next
    
    Set colEAN = Nothing
    Set oEAN = Nothing
    
    blnSuppLoaded = True
    ucpbItem.Visible = blnVisible
    Screen.MousePointer = vbNormal

End Sub


Private Sub fill_in_items()
Dim SQL_Str As String
Dim cItems As Recordset 'contains all items for a supplier

    On Error GoTo bad_items

    'TEST DATA
    Exit Sub
    Screen.MousePointer = 11
    sprdSuppliers.Redraw = False
    SQL_Str = "SELECT SellingPrices.ItemID,PartCode,PartDescription1,SellingPrices.SellingPrice,"
    SQL_Str = SQL_Str & "UnitID FROM SellingPrices INNER JOIN Items ON SellingPrices.ItemID = Items.ItemID "
    SQL_Str = SQL_Str & " WHERE ClientID = " & lblItemID.Caption
    Call cItems.Open(SQL_Str, adoconn)
    If cItems.EOF Then
        sprdSuppliers.MaxRows = 0
        Screen.MousePointer = 0
        Exit Sub
    End If

    sprdSuppliers.LeftCol = 1
    sprdSuppliers.Row = 0
    sprdSuppliers.MaxRows = 0
    
    While Not cItems.EOF
        sprdSuppliers.MaxRows = sprdSuppliers.MaxRows + 1
        sprdSuppliers.Row = sprdSuppliers.Row + 1
        sprdSuppliers.Col = 1
        sprdSuppliers.Text = cItems.Fields("PartCode")
        sprdSuppliers.Col = 2
        sprdSuppliers.Text = cItems.Fields("PartDescription")
        sprdSuppliers.Col = 3
        sprdSuppliers.Text = Val(cItems.Fields("SellingPrice"))
        sprdSuppliers.Col = 5
        sprdSuppliers.Text = cItems.Fields("ItemID")
        sprdSuppliers.Col = 4
        sprdSuppliers.Text = 0
        cItems.MoveNext
    Wend
    sprdSuppliers.Redraw = True
    cItems.Close
    Screen.MousePointer = 0
    
    Exit Sub

bad_items:

    If (Err <> 94) And (Err <> 13) Then
        MsgBox (Error$), , "Error loading Product Lists from Table"
        Screen.MousePointer = 0
        sprdSuppliers.Redraw = True
        cItems.Close
        Exit Sub
    End If
    
    Resume Next

End Sub



Private Sub cmdSearch_Click()
  Call mnuILocate_Click

End Sub

Private Sub cmdEnqOnReturn_Click()
    
    Load frmDetails
    Call frmDetails.DisplayOpenReturns(lblPartCode.Caption, txtDescription.Text)

End Sub


Private Sub cmdEnqBranches_Click()

    Load frmDetails
    Call frmDetails.Show(vbModal)

End Sub

Private Sub cmdEnqOnOrder_Click()

    Load frmDetails
    Call frmDetails.DisplayPurchaseOrders(lblPartCode.Caption, txtDescription.Text)

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyF1 Then uchlHelpKeys.Visible = True
    
    If KeyCode = vbKeyF9 Then
        Screen.MousePointer = vbHourglass
        Call Me.ucifSearch.Initialise(goSession, Me)
        ucifSearch.Visible = True
        Call ucifSearch.FillScreen(ucifSearch)
        ucifSearch.SetFocus
        Screen.MousePointer = vbDefault
        
    End If
'    If KeyCode = vbKEYF3 Then Call mnuILocate_Click -executed by menu HotKey
    If (KeyCode = 107) And (Shift = 2) Then Call mnuVExpandDetails_Click
    If (KeyCode = 109) And (Shift = 2) Then Call mnuVShrinkDetails_Click
    If (KeyCode = 190) And (Shift = 2) And cmdNext.Visible Then cmdNext.Value = True
    If (KeyCode = 188) And (Shift = 2) And cmdPrev.Visible Then cmdPrev.Value = True
  
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

Dim blnMove As Boolean

    blnMove = False
    If KeyAscii = 13 Then
        If TypeOf ActiveControl Is Slider Then blnMove = True
        If TypeOf ActiveControl Is TextBox Then blnMove = True
        If TypeOf ActiveControl Is ComboBox Then blnMove = True
        If TypeOf ActiveControl Is CheckBox Then blnMove = True
        If TypeOf ActiveControl Is ucNumberText Then blnMove = True
        If TypeOf ActiveControl Is SSTab Then blnMove = True
        If blnMove Then
            KeyAscii = 0
            SendKeys (vbTab)
            DoEvents
        End If

    End If

    blnMove = False
    If KeyAscii = 27 Then
        If TypeOf ActiveControl Is Slider Then blnMove = True
        If TypeOf ActiveControl Is TextBox Then blnMove = True
        If TypeOf ActiveControl Is ComboBox Then blnMove = True
        If TypeOf ActiveControl Is CheckBox Then blnMove = True
        If TypeOf ActiveControl Is ucNumberText Then blnMove = True
        If TypeOf ActiveControl Is SSTab Then blnMove = True
        If blnMove Then
            KeyAscii = 0
            Call SendKeys("+{tab}")
            DoEvents
        End If

    End If

End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyF1 Then uchlHelpKeys.Visible = False

End Sub

Private Sub Form_Load()
Const PRM_MULTIPLE_SUPPLIERS As Long = 118
Const PRM_DISP_BRANCH_QTY As Long = 310
Const PRM_DISP_CUSTORD_QTY As Long = 311
Dim lngItemNo       As Long
Dim no_cols         As Double 'used for setting Balances width
Dim oEnterprise     As cSystemDates
Dim lngBackColor    As Long
Dim lngQtyDecNum    As Long
Dim lngValueDecNum  As Long

    'On Error Resume Next

    Call uchlHelpKeys.AddHotKey("F2", "Fuzzy Match")
    Call uchlHelpKeys.AddHotKey("F3", "Item list select")
    Call uchlHelpKeys.AddHotKey("Alt <", "Previous item")
    Call uchlHelpKeys.AddHotKey("Alt >", "Next item")
    
    ucpbItem.Title = "Loading Editor Enquiry"
    ucpbItem.Visible = True
    ucpbItem.Caption1 = "Loading"
    ucpbItem.Value = 0
    ucpbItem.Max = 5
        
    Me.Show
    DoEvents
    
    lTabTop = tabDetails.Top
  
    ucpbItem.Caption1 = "Setting Up Periods"
    ucpbItem.Value = 1
    ucpbItem.Refresh
    
    'Set up the object and field
    'that we wish to select on.
    Set goRoot = GetRoot
    Call InitialiseStatusBar(sbStatus)
    Call ConfigureFlags
    
    lngBackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    lngQtyDecNum = goSession.GetParameter(PRM_QTY_DEC_PLACES)
    lngValueDecNum = goSession.GetParameter(PRM_VALUE_DEC_PLACES)
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    mblnMultipleSuppliers = goSession.GetParameter(PRM_MULTIPLE_SUPPLIERS)
    
    frSuppliers.Visible = mblnMultipleSuppliers
    
    If (Not goSession.GetParameter(PRM_DISP_BRANCH_QTY)) Then
        lblQtyForBrancheslbl.Visible = False
        lblQtyForBranches.Visible = False
        cmdEnqBranches.Visible = False
        
    End If
    
    If (Not goSession.GetParameter(PRM_DISP_CUSTORD_QTY)) Then
        lblQtyOnCustOrderlbl.Visible = False
        lblQtyOnCustOrder.Visible = False
        cmdEnqCustOrder.Visible = False
        
    End If
    
    Me.BackColor = lngBackColor
    frDetails.BackColor = lngBackColor
    frLevels.BackColor = lngBackColor
    frHide.BackColor = lngBackColor
    tabDetails.BackColor = lngBackColor
    chkItemObs.BackColor = lngBackColor
    
'    tabDetails.TabVisible(TAB_SUPPLIER) = goSession.GetParameter(118)
    
    Me.Show
    
'    Set cItems = goDatabase.createbusinessObject(CLASSID_INVENTORYS)
    
    Set oEnterprise = goDatabase.CreateBusinessObject(CLASSID_SYSTEMDATES)
    Call oEnterprise.IBo_SetLoadField(CMP_EQUAL, FID_SYSTEMDATES_ID, "01")
    Call oEnterprise.IBo_Load
    
    For lngItemNo = 1 To 13 Step 1
        sprdWeekTot.Row = lngItemNo
        sprdWeekTot.Col = 2
        sprdWeekTot.Text = Format$(oEnterprise.WeekEndDates(lngItemNo), "dd/mm/yy")
        sprdWeekTot.Col = 1
        sprdWeekTot.Text = Format$(DateAdd("d", -7, oEnterprise.WeekEndDates(lngItemNo)), "dd/mm/yy")
    Next lngItemNo
    
        
    'cmdHO.Value = True
    ucifSearch.UseList = True
    
    ucpbItem.Caption1 = "Retrieving List of Items"
    ucpbItem.Value = 2
    ucpbItem.Refresh
    DoEvents
    
    cmbStSearch.VRowCount = 0
    
    ucpbItem.Caption1 = "Filling in LookUps"
    ucpbItem.Value = 3
    ucpbItem.Refresh
    Call fill_in_combos
    
    ucpbItem.Caption1 = "Resetting Display"
    ucpbItem.Value = 4
    ucpbItem.Refresh
    Call ClearScreen
     
    ucpbItem.Caption1 = "Resetting criteria"
    ucpbItem.Value = 5
    ucpbItem.Refresh
    Rem_Tran_All = 0
    Rem_Tran_Inv = 0
    cmbCritTran.ListIndex = 0
    
    While (cmbCritTran.Text <> "Invoice") And (Rem_Tran_Inv < cmbCritTran.ListCount)
      Rem_Tran_Inv = Rem_Tran_Inv + 1
     ' cmbCritTran.ListIndex = Rem_Tran_Inv
     
    Wend
    
    cmbCritTran.ListIndex = 0
    While (cmbCritTran.Text <> "All") And (Rem_Tran_All < cmbCritTran.ListCount)
      Rem_Tran_All = Rem_Tran_All + 1
      cmbCritTran.ListIndex = Rem_Tran_All
      
    Wend
    
    Rem_Ref_All = 0
    cmbRemRef.ListIndex = 0
    While (cmbRemRef.Text <> "All") And (Rem_Ref_All < cmbRemRef.ListCount)
      Rem_Ref_All = Rem_Ref_All + 1
      cmbRemRef.ListIndex = Rem_Ref_All
      
    Wend
    
    Rem_Amt_All = 0
    cmbRemAmt.ListIndex = 0
    While (cmbRemAmt.Text <> "All") And (Rem_Amt_All < cmbRemAmt.ListCount)
      Rem_Amt_All = Rem_Amt_All + 1
      cmbRemAmt.ListIndex = Rem_Amt_All
      
    Wend
    
    cmbStatus.ListIndex = 0
    While (cmbStatus.ItemData(Status_All) <> 0) And (Status_All < cmbCritTran.ListCount)
      Status_All = Status_All + 1
      cmbStatus.ListIndex = Rem_Tran_All
      
    Wend
    
    ucpbItem.Caption1 = "Loading item selection"
    ucpbItem.Refresh
    ucpbItem.Visible = False
    
    If LenB(goSession.ProgramParams) = 0 Then
        Call mnuFuzzy_Click
    Else
        If Left$(goSession.ProgramParams, 4) = "SKU=" Then Call ucifSearch_Apply(Mid$(goSession.ProgramParams, 5))
    End If
    
End Sub

Private Sub ConfigureFlags()
Dim lngTop As Long
Dim lngGap As Long

    lngGap = lblFlag2.Top - lblFlag1.Top
    lngTop = lblFlag1.Top
    lblFlag1.Caption = goSession.GetParameter(PRM_FLAG1_LABEL)
    If LenB(lblFlag1.Caption) = 0 Then
        lblFlag1.Visible = False
        txtFlag1.Visible = False
        
    Else
        lngTop = lngTop + lngGap
        
    End If
    
        
    lblFlag2.Top = lngTop
    txtFlag2.Top = lngTop
    lblFlag2.Caption = goSession.GetParameter(PRM_FLAG2_LABEL)
    If LenB(lblFlag2.Caption) = 0 Then
        lblFlag2.Visible = False
        txtFlag2.Visible = False
        
    Else
        lngTop = lngTop + lngGap
        
    End If
    
    lblFlag3.Top = lngTop
    txtFlag3.Top = lngTop
    lblFlag3.Caption = goSession.GetParameter(PRM_FLAG3_LABEL)
    
    If LenB(lblFlag3.Caption) = 0 Then
        lblFlag3.Visible = False
        txtFlag3.Visible = False
        
    Else
        lngTop = lngTop + lngGap
        
    End If
    
    lblFlag4.Top = lngTop
    txtFlag4.Top = lngTop
    lblFlag4.Caption = goSession.GetParameter(PRM_FLAG4_LABEL)
    
    If LenB(lblFlag4.Caption) = 0 Then
        lblFlag4.Visible = False
        txtFlag4.Visible = False
        
    Else
        lngTop = lngTop + lngGap
        
    End If
    
    lblFlag5.Top = lngTop
    txtFlag5.Top = lngTop
    lblFlag5.Caption = goSession.GetParameter(PRM_FLAG5_LABEL)
    
    If LenB(lblFlag5.Caption) = 0 Then
        lblFlag5.Visible = False
        txtFlag5.Visible = False
        
    End If
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Dim intKeyIn As Integer
  
    If (data_edited And mnuSave.Visible) Then
        intKeyIn = MsgBox("Item has been Edited" & Chr$(13) & "Save Changes (Y/N)", vbYesNoCancel, "Exit from Item Editor")
        Select Case (intKeyIn)
            Case (vbYes):    Call mnuSave_Click
            Case (vbCancel): Cancel = 1
            
        End Select
    End If

End Sub

Private Sub Form_Resize()

    Call DebugMsg(MODULE_NAME, "Resize", endlTraceIn)
    If Me.WindowState = vbMinimized Then Exit Sub

    If Width < 12390 Then
        Width = 12390
        Exit Sub
        
    End If
    
    If Height < 9000 Then
        Height = 9000
        Exit Sub
        
    End If
    
    blnRszConfig = True
    blnRszTrans = True
    blnRszSupp = True
    blnRszEANs = True
    blnRszSumm = True
    blnRszPrices = True
    blnRszCost = True
    blnRszInfo = True
    
    tabDetails.Width = Me.Width - (tabDetails.Left * 3)
    tabDetails.Height = Me.Height - (sbStatus.Height * 3) - tabDetails.Top - 120
    Call ResizeTabs
    Call DebugMsg(MODULE_NAME, "Resize", endlTraceOut)

End Sub
Private Sub ResizeTabs()
    
    If (tabDetails.Tab = TAB_TRANS) And (blnRszTrans) Then
        frMovements.Width = tabDetails.Width - frMovements.Left * 2
        sprdTrans.Width = frMovements.Width - 60
        frMovements.Height = tabDetails.Height - frMovements.Top - 120
        sprdTrans.Height = frMovements.Height - cmdTranFilter.Height - 240
        cmdTranFilter.Top = sprdTrans.Height + 120
        cmdExport.Top = cmdTranFilter.Top
        cmdExport.Left = sprdTrans.Left + sprdTrans.Width - cmdExport.Width
        blnRszTrans = False
        
    End If
    
    If (tabDetails.Tab = TAB_SUPPLIER) And (blnRszSupp) Then
        If mblnMultipleSuppliers = True Then
            frSuppliers.Width = tabDetails.Width - frSuppliers.Left * 2
            frSuppliers.Height = tabDetails.Height - frSuppliers.Top - 240
            sprdSuppliers.Width = frSuppliers.Width - 60
            sprdSuppliers.Height = frSuppliers - cmdNewEAN.Height - 240
            cmdNewSupp.Top = sprdSuppliers.Height + 120
            cmdDelSupplier.Top = cmdNewSupp.Top
            cmdSaveSupplier.Top = cmdNewSupp.Top
            cmdNewSupp.Left = sprdSuppliers.Left + sprdSuppliers.Width - cmdNewSupp.Width - 60
            cmdDelSupplier.Left = cmdNewSupp.Left - cmdDelSupplier.Width - 120
            cmdSaveSupplier.Left = cmdDelSupplier.Left - cmdSaveSupplier.Width - 120
            
        End If
        
        blnRszSupp = False
        If (Not blnSuppLoaded) And (LenB(lblPartCode.Caption) <> 0) Then LoadSuppliers
        
    End If
    
    If (tabDetails.Tab = TAB_EANS) And (blnRszEANs = True) Then
        frEANs.Width = tabDetails.Width - frEANs.Left * 2
        sprdEANs.Width = frEANs.Width - 60
        frEANs.Height = tabDetails.Height - frEANs.Top - 120
        sprdEANs.Height = frEANs.Height - cmdNewEAN.Height - 240
        cmdNewEAN.Top = sprdEANs.Height + 120
        cmdDelEAN.Top = cmdNewEAN.Top
        cmdSaveEAN.Top = cmdNewEAN.Top
        cmdNewEAN.Left = sprdEANs.Left + sprdEANs.Width - cmdNewEAN.Width
        cmdDelEAN.Left = cmdNewEAN.Left - cmdDelEAN.Width - 120
        cmdSaveEAN.Left = cmdDelEAN.Left - cmdSaveEAN.Width - 120
        blnRszEANs = False
        If (Not blnEANLoaded) And (LenB(lblPartCode.Caption) <> 0) Then LoadEANs
        
    End If
    
    If (tabDetails.Tab = TAB_TEXT) And (blnRszInfo) Then
        frTechInfo.Width = tabDetails.Width - frTechInfo.Left * 2
        txtTechInfo.Width = frTechInfo.Width
        frTechInfo.Height = tabDetails.Height - frTechInfo.Top - 120
        txtTechInfo.Height = frTechInfo.Height - 240
        blnRszInfo = False
        If (Not blnTechLoaded) And (LenB(lblPartCode.Caption) <> 0) Then
            txtTechInfo = cItem.Description
            blnTechLoaded = True
            
        End If
    End If

End Sub

Private Sub lblNoMatches_Change()

    If Val(lblNoMatches.Caption) = 1 Then
        cmdNext.Visible = False
        cmdPrev.Visible = False
        mnuINext.Visible = False
        mnuIPrev.Visible = False
        Toolbar1.Buttons("Next").Visible = False
        Toolbar1.Buttons("Previous").Visible = False
        
    Else
        cmdNext.Visible = True
        cmdPrev.Visible = True
        mnuINext.Visible = True
        mnuIPrev.Visible = True
        Toolbar1.Buttons("Next").Visible = True
        Toolbar1.Buttons("Previous").Visible = True
        
    End If

End Sub

Private Sub mnuExit_Click()
    Unload Me

End Sub

Private Sub mnuFuzzy_Click()
        
    Screen.MousePointer = vbHourglass
    Call Me.ucifSearch.Initialise(goSession, Me)
    ucifSearch.Visible = True
    Call ucifSearch.FillScreen(ucifSearch)
    If ucifSearch.Visible Then ucifSearch.SetFocus
    Screen.MousePointer = vbNormal

End Sub

Private Sub mnuRefresh_Click()
Dim intKeyIn As Integer
            
    If (data_edited And mnuSave.Visible) Then
        intKeyIn = MsgBox("Item has been Edited" & Chr$(13) & "Save Changes (Y/N)", vbYesNoCancel, "Refresh Item Details")
        Select Case (intKeyIn)
            Case (vbYes): Call mnuSave_Click
                        If data_edited Then Exit Sub
            Case (vbCancel): Exit Sub
            
        End Select
    End If
    
    Call DisplayItem

End Sub

Private Sub ntxtNoShelfLabels_GotFocus()
    ntxtNoShelfLabels.BackColor = RGBEdit_Colour

End Sub

Private Sub ntxtNoShelfLabels_LostFocus()
    ntxtNoShelfLabels.BackColor = RGB_OldColour

End Sub

Private Sub sprdPeriodTot_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then Call SendKeys("{tab}")
    If KeyAscii = 27 And sprdPeriodTot.EditMode = False Then Call SendKeys("+{tab}")

End Sub

Private Sub sprdSuppliers_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then Call SendKeys("{tab}")
    If KeyAscii = 27 And sprdSuppliers.EditMode = False Then Call SendKeys("+{tab}")

End Sub

Private Sub sprdWeekTot_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then Call SendKeys("{tab}")
    If KeyAscii = 27 And sprdWeekTot.EditMode = False Then Call SendKeys("+{tab}")

End Sub

Private Sub txtStarRate_GotFocus()
    txtStarRate.BackColor = RGBEdit_Colour

End Sub

Private Sub txtStarRate_LostFocus()
    txtStarRate.BackColor = RGB_OldColour

End Sub
Private Sub ntxtCostPrice_GotFocus()
    ntxtCostPrice.BackColor = RGBEdit_Colour

End Sub

Private Sub ntxtCostPrice_LostFocus()
    ntxtCostPrice.BackColor = RGB_OldColour

End Sub


Private Sub txtCritRef_GotFocus()
    txtCritRef.BackColor = RGBEdit_Colour

End Sub

Private Sub txtCritRef_LostFocus()
    txtCritRef.BackColor = RGB_OldColour

End Sub


Private Sub ntxtSellPrice_Change()
    Call DisplayPriceExVAT

End Sub

Private Sub ntxtSellPrice_GotFocus()
    ntxtSellPrice.BackColor = RGBEdit_Colour

End Sub

Private Sub ntxtSellPrice_LostFocus()
    ntxtSellPrice.BackColor = RGB_OldColour

End Sub

Private Sub ntxtMinLevel_GotFocus()
    ntxtMinLevel.BackColor = RGBEdit_Colour

End Sub

Private Sub ntxtMinLevel_LostFocus()
    ntxtMinLevel.BackColor = RGB_OldColour

End Sub

Private Sub ntxtMaxLevel_GotFocus()
    ntxtMaxLevel.BackColor = RGBEdit_Colour

End Sub

Private Sub ntxtMaxLevel_LostFocus()
    ntxtMaxLevel.BackColor = RGB_OldColour

End Sub

Private Sub txtSellInPacks_GotFocus()
    txtSellInPacks.BackColor = RGBEdit_Colour

End Sub

Private Sub txtSellInPacks_LostFocus()
    txtSellInPacks.BackColor = RGB_OldColour

End Sub


Private Sub ntxtWidth_GotFocus()
    ntxtWidth.BackColor = RGBEdit_Colour

End Sub

Private Sub ntxtWidth_LostFocus()
    ntxtWidth.BackColor = RGB_OldColour

End Sub

Private Sub ntxtHeight_GotFocus()
    ntxtHeight.BackColor = RGBEdit_Colour

End Sub

Private Sub ntxtHeight_LostFocus()
    ntxtHeight.BackColor = RGB_OldColour

End Sub

Private Sub ntxtWeight_GotFocus()
    ntxtWeight.BackColor = RGBEdit_Colour

End Sub

Private Sub ntxtWeight_LostFocus()
    ntxtWeight.BackColor = RGB_OldColour

End Sub

Private Sub ntxtSpecialPrice_GotFocus()
    ntxtSpecialPrice.BackColor = RGBEdit_Colour

End Sub

Private Sub ntxtSpecialPrice_LostFocus()
    ntxtSpecialPrice.BackColor = RGB_OldColour

End Sub

Private Sub sprdCost_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then Call SendKeys("{tab}")
    If KeyAscii = 27 And (Not sprdCost.EditMode) Then Call SendKeys("+{tab}")

End Sub

Private Sub sprdEANs_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then Call SendKeys("{tab}")
    If KeyAscii = 27 And (Not sprdEANs.EditMode) Then Call SendKeys("+{tab}")

End Sub

Private Sub sprdPrices_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then Call SendKeys("{tab}")
    If KeyAscii = 27 And (Not sprdPrices.EditMode) Then Call SendKeys("+{tab}")

End Sub

Private Sub sprdTrans_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then Call SendKeys("{tab}")
    If KeyAscii = 27 And (Not sprdTrans.EditMode) Then Call SendKeys("+{tab}")

End Sub

Private Sub sprdUnits_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
Dim lngRowNo As Long

    If ((Col = UNIT_BUYIN) Or (Col = UNIT_SELLIN)) And (ButtonDown = 1) Then
        For lngRowNo = 1 To sprdUnits.MaxRows Step 1
            sprdUnits.Col = Col
            sprdUnits.Row = lngRowNo
            If (lngRowNo <> Row) And (sprdUnits.Value = 1) Then sprdUnits.Value = 0
            
        Next lngRowNo
    End If
    
    If (Col = UNIT_THISITEM) Then
        sprdUnits.Col = Col
        sprdUnits.Row = Row
        If sprdUnits.Value = 0 Then 'if not for this item - disable for Sell and Buy In and Factor
            sprdUnits.Col = UNIT_BUYIN
            sprdUnits.Lock = True
            sprdUnits.Value = 0
            sprdUnits.Col = UNIT_SELLIN
            sprdUnits.Lock = True
            sprdUnits.Value = 0
            sprdUnits.Col = UNIT_FACTOR
            sprdUnits.Lock = True
            sprdUnits.Text = "0"
            
        Else
            sprdUnits.Col = UNIT_BUYIN
            sprdUnits.Lock = False
            sprdUnits.Value = 0
            sprdUnits.Col = UNIT_SELLIN
            sprdUnits.Lock = False
            sprdUnits.Value = 0
            sprdUnits.Col = UNIT_FACTOR
            sprdUnits.Lock = False
            sprdUnits.Text = "1"
            
        End If
    End If

End Sub

Private Sub sprdUnits_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then Call SendKeys("{tab}")
    If KeyAscii = 27 And (Not sprdUnits.EditMode) Then Call SendKeys("+{tab}")

End Sub

Private Sub tabDetails_Click(PreviousTab As Integer)
Dim lngTabNo As Long

    'Enable frame for selected tab
    Select Case (tabDetails.Tab)
        Case (TAB_CONFIG):   frConfig.Enabled = True
        Case (TAB_TRANS):    frMovements.Enabled = True
        Case (TAB_SUPPLIER): frSupplier.Enabled = True
                             frSuppliers.Enabled = True
        Case (TAB_EANS):     frEANs.Enabled = True
        Case (TAB_SUMMARY):  frSummary.Enabled = True
        Case (TAB_PRICES):   frPrices.Enabled = True
        Case (TAB_SPARE):
        Case (TAB_TEXT):     frTechInfo.Enabled = True
        
    End Select
    
    'disable all other frames for non-selected tab
    For lngTabNo = 0 To tabDetails.Tabs Step 1
        If lngTabNo <> tabDetails.Tab Then
            Select Case (lngTabNo)
                Case (TAB_CONFIG):   frConfig.Enabled = False
                Case (TAB_TRANS):    frMovements.Enabled = False
                Case (TAB_SUPPLIER): frSupplier.Enabled = False
                                     frSuppliers.Enabled = False
                Case (TAB_EANS):     frEANs.Enabled = False
                Case (TAB_SUMMARY):  frSummary.Enabled = False
                Case (TAB_PRICES):   frPrices.Enabled = False
                Case (TAB_SPARE):
                Case (TAB_TEXT):     frTechInfo.Enabled = False
                
            End Select
        End If
    Next lngTabNo
        
    Call ResizeTabs
    If (tabDetails.Tab = TAB_SUPPLIER) And (Not mblnMultipleSuppliers) Then Call DisplaySupplier

End Sub

Private Sub DisplaySupplier()
Dim oSupplier As cSupplier
Dim colSupplier As Collection
    
    If LenB(lblSupplier.Caption) = 0 Then
        Set oSupplier = goDatabase.CreateBusinessObject(CLASSID_SUPPLIER)
        Call oSupplier.AddLoadFilter(CMP_EQUAL, FID_SUPPLIER_SupplierNo, mstrSuppNo)
        Call oSupplier.AddLoadField(FID_SUPPLIERS_Name)
        'Call oSupplier.AddLoadField(FID_SUPPLIERS_Name) 'need address
        'Call oSupplier.AddLoadField(FID_SUPPLIERS_Name) 'need fax no.
        'Call oSupplier.AddLoadField(FID_SUPPLIERS_Name) 'need phone no.
        Set colSupplier = oSupplier.LoadMatches
        
        If colSupplier.Count <> 0 Then
            Set oSupplier = colSupplier(1)
            lblSupplier.Caption = mstrSuppNo & "-" & oSupplier.Name
            'lblSuppAddress.Caption = ""
            'lblSuppfaxNo.Caption = ""
            'lblSuppPhoneNo.Caption = ""
            
        End If
        
        Set colSupplier = Nothing
        
    End If
    
End Sub

Private Sub tabDetails_KeyPress(KeyAscii As Integer)


'    If KeyAscii = 13 Then
'        Call MsgBox("hello")
'    End If

End Sub

Private Sub txtBuyInPacks_GotFocus()
    txtBuyInPacks.BackColor = RGBEdit_Colour

End Sub

Private Sub txtBuyInPacks_LostFocus()
    txtBuyInPacks.BackColor = RGB_OldColour

End Sub

Private Sub txtCatalogueNo_GotFocus()
    txtCatalogueNo.BackColor = RGBEdit_Colour

End Sub

Private Sub txtCatalogueNo_LostFocus()
    txtCatalogueNo.BackColor = RGB_OldColour

End Sub

Private Sub txtDescription_GotFocus()
    txtDescription.BackColor = RGBEdit_Colour

End Sub

Private Sub txtDescription_LostFocus()
    txtDescription.BackColor = RGB_OldColour

End Sub

Private Sub txtFlag1_GotFocus()
    txtFlag1.BackColor = RGBEdit_Colour

End Sub

Private Sub txtFlag1_LostFocus()
    txtFlag1.BackColor = RGB_OldColour

End Sub

Private Sub txtFlag2_GotFocus()
    txtFlag2.BackColor = RGBEdit_Colour

End Sub

Private Sub txtFlag2_LostFocus()
    txtFlag2.BackColor = RGB_OldColour

End Sub
Private Sub txtFlag3_GotFocus()
    txtFlag3.BackColor = RGBEdit_Colour

End Sub

Private Sub txtFlag3_LostFocus()
    txtFlag3.BackColor = RGB_OldColour

End Sub

Private Sub txtFlag4_GotFocus()
    txtFlag4.BackColor = RGBEdit_Colour

End Sub

Private Sub txtFlag4_LostFocus()
    txtFlag4.BackColor = RGB_OldColour

End Sub

Private Sub txtFlag5_GotFocus()
    txtFlag5.BackColor = RGBEdit_Colour

End Sub

Private Sub txtFlag5_LostFocus()
    txtFlag5.BackColor = RGB_OldColour

End Sub

Private Sub txtPriceBand_GotFocus()
    txtPriceBand.BackColor = RGBEdit_Colour

End Sub

Private Sub txtPriceBand_LostFocus()
    txtPriceBand.BackColor = RGB_OldColour

End Sub

Private Sub txtSize_GotFocus()
    txtSize.BackColor = RGBEdit_Colour

End Sub

Private Sub txtSize_LostFocus()
    
    txtSize.BackColor = RGB_OldColour

End Sub

Private Sub txtTechInfo_GotFocus()
    txtTechInfo.BackColor = RGBEdit_Colour

End Sub

Private Sub txtTechInfo_LostFocus()
    txtTechInfo.BackColor = RGB_OldColour

End Sub

Private Sub ucifSearch_Apply(PartCode As String)
    
    'clear out previous entries
    Call cmbStSearch.Clear
    Call cmbStSearch.AddItem(PartCode)
    cmbStSearch.Visible = False
    
    lblNoMatches.Caption = 1
    ucifSearch.Visible = False
    cmbStSearch.ListIndex = 0
    Call DisplayItem
    Call txtDescription.SetFocus

End Sub

Private Sub ucifSearch_Cancel()

    ucifSearch.Visible = False
    If Val(lblNoMatches.Caption) = 0 Then
        If MsgBox("No items selected to view" & vbCrLf & "Exit enquiry now", vbYesNo + vbInformation, Me.Caption) = vbYes Then
            Call Unload(Me)
            End
        Else
            Call mnuFuzzy_Click
        End If
        
    End If

End Sub

Private Sub ucifSearch_LostFocus()
    ucifSearch.Visible = False

End Sub


Private Sub mnuDelete_Click()
Dim rstTrans As New ADODB.Recordset
Dim resp_key As Long

    resp_key = MsgBox("Confirm Delete Item - '" & txtDescription.Text & "'" & vbCrLf & _
                      "WARNING - Deleted Creditors cannot be retrieved", _
                      vbYesNo + vbQuestion, "Confirm Delete Item")
    If (resp_key = vbYes) Then
        Call rstTrans.Open("SELECT COUNT(*) FROM Transactions WHERE ClientType = " & 1 & _
                           " AND ClientID = " & lblItemID.Caption, goDatabase.Connection) ' adoconn)
        If (rstTrans(0) > 0) Then
            If MsgBox("Item - '" & txtDescription.Text & "' has " & rstTrans(0) & _
                      " transactions in history" & vbCrLf & _
                      "Confirm - Delete Item and move Transactions into History", _
                      vbYesNo, "Delete Item") = vbNo Then
                rstTrans.Close
                Exit Sub
            
            End If
        End If
        
        rstTrans.Close
        Call ClearScreen
        lblItemID.Caption = "lblItemID"
       
    End If
  
End Sub

Private Sub mnuILocate_Click()
Dim ItemNo As Long

    If (cmbStSearch.ListCount = 0) Then
        If MsgBox("No items retrieved from selection" & vbCrLf & _
                  "Use item filter to select items (Y/N)", _
                  vbExclamation + vbYesNo, "Access item search") = vbYes Then
            Call mnuFuzzy_Click
            Exit Sub
            
        End If
    End If
    
    If cmbStSearch.ListCount = 1 Then
        Call MsgBox("Only 1 item retrieved from selection" & vbCrLf & _
                    "Unable to locate on only 1 item", vbExclamation, "Access item search")
    End If
    
    If (cmbStSearch.ListCount > 1) Then
        Screen.MousePointer = 11

'  If cmbStSearch.ListCount = 0 Then
'    ucpbItem.Min = 0
'    ucpbItem.Max = cItems.Count
'    ucpbItem.Value = 0
'    ucpbItem.Title = "Populating Item selection list"
'    ucpbItem.Caption1 = vbNullString
'    ucpbItem.Visible = True
'    DoEvents
'    cmbStSearch.Clear
'    Call cItems.MoveFirst
'    While Not cItems.EndOfList
'      ucpbItem.Value = ucpbItem.Value + 1
'      cmbStSearch.AddItem cItems.EntryString
'      Call cItems.MoveNext
'    Wend
'  End If
        ucpbItem.Visible = False
        cmbStSearch.Visible = True
        cmbStSearch.SetFocus
        Screen.MousePointer = 0
        
    End If

End Sub

Private Sub mnuINext_Click()

Dim intKeyIn As Long
    
    If (data_edited And mnuSave.Visible) Then
        intKeyIn = MsgBox("Item has been Edited" & Chr$(13) & "Save Changes (Y/N)", vbYesNoCancel, "Move to Next Item")
        Select Case (intKeyIn)
           Case (vbYes): Call mnuSave_Click
                         If data_edited Then Exit Sub
           Case (vbCancel): Exit Sub
           
        End Select
    End If
    
    If cmbStSearch.ListIndex = cmbStSearch.ListCount - 1 Then
        MsgBox ("No more Items in Database"), , "Error moving to next Item"
        Exit Sub
        
    End If
    
    cmbStSearch.ListIndex = cmbStSearch.ListIndex + 1
    Call DisplayItem

End Sub

Private Sub mnuIPrev_Click()
Dim intKeyIn As Long
  
    If (data_edited And mnuSave.Visible) Then
        intKeyIn = MsgBox("Item has been Edited" & Chr$(13) & "Save Changes (Y/N)", vbYesNoCancel, "Move to Previous Item")
        Select Case (intKeyIn)
           Case (vbYes): Call mnuSave_Click
                         If data_edited Then Exit Sub
           Case (vbCancel): Exit Sub
           
        End Select
    End If
    
    If cmbStSearch.ListIndex = 0 Then
        MsgBox ("No previous Items in Database"), , "Error moving to previous Item"
        Exit Sub
        
    End If
    
    cmbStSearch.ListIndex = cmbStSearch.ListIndex - 1
    Call DisplayItem

End Sub

Private Sub mnuVExpandDetails_Click()

  sldDetails.Value = sldDetails.Value - 5
  Call Slddetails_Click

End Sub

Private Sub mnuNew_Click()

Dim intKeyIn As Integer

    If data_edited Then
        intKeyIn = MsgBox("Item has been Edited" & Chr$(13) & "Save Changes (Y/N)", vbYesNoCancel, "Create New Item")
        Select Case (intKeyIn)
            Case (vbYes): Call mnuSave_Click
            Case (vbCancel): Exit Sub
            
        End Select
    End If
    
    Call ClearScreen
    lblPartCode.Caption = "TBA"
    '  txtDescription1.BackColor = rgbYELLOW
    lblItemID.Caption = -1

End Sub

Private Sub mnuSave_Click()
Dim lngLineNo As Long
Dim lngRowNo  As Long
'Dim cItem As clsItemCode
  
    If LenB(txtBuyInPacks.Text) = 0 Then
        MsgBox ("Enter Valid Buy In Packs and Re-Save"), , "Save Item"
        Screen.MousePointer = 0
        Exit Sub
    End If
    
    If LenB(txtSellInPacks.Text) = 0 Then
        MsgBox ("Select Sell In Packs and Re-Save"), , "Save Item"
        Screen.MousePointer = 0
        Exit Sub
        
    End If
    
    If cmbVAT.ListIndex = -1 Then
        MsgBox ("Select Valid VAT Rating and Re-Save"), , "Save Item"
        Screen.MousePointer = 0
        tabDetails = TAB_CONFIG
        cmbVAT.SetFocus
        Exit Sub
        
    End If

    cItem.PartCode = lblPartCode.Caption
    cItem.Description = txtDescription.Text
    sprdUnits.Col = UNIT_THISITEM
    sprdUnits.Lock = False
    
    'Show units factors used
    
    cItem.Weight = ntxtWeight.Value
    cItem.MinQuantity = ntxtMinLevel.Value
    cItem.MaxQuantity = ntxtMaxLevel.Value
    
    cItem.ItemTagged = chkItemTagged.Value
    cItem.Warranty = chkWarranty.Value
    cItem.CatchAll = chkCatchAll.Value
'    chkDiscAllowed.Value = Abs(Not cItem.ExcludeFromDisc)
    cItem.VATRate = cmbVAT.ItemData(cmbVAT.ListIndex)
    
    If lblItemID.Caption <> "-1" Then
        Call cItem.IBo_SaveIfExists
        
    Else
        goDatabase.StartTransaction
        Call cItem.IBo_SaveIfNew
        goDatabase.CommitTransaction
        
    End If
'  lblItemID.Caption = cItem.ID
    data_edited = False
    Screen.MousePointer = 0

End Sub


Private Sub mnuVSBPartCode_Click()

  If Not mnuVSBPartCode.Checked Then
    mnuVSBDescription.Checked = False
    mnuVSBPartCode.Checked = True
'    Call cItems.SetOrder(dbAccounts, PCdOPartCode)

  End If

End Sub

Private Sub mnuVSBDescription_Click()
  
  If Not mnuVSBDescription.Checked Then
    mnuVSBDescription.Checked = True
    mnuVSBPartCode.Checked = False
'    Call cItems.SetOrder(dbAccounts, PcdOPartDesc)

  End If

End Sub

Private Sub mnuVShrinkDetails_Click()

  sldDetails.Value = sldDetails.Value + 5
  Call Slddetails_Click

End Sub

Private Sub Slddetails_Click()

    If sldDetails.Value < 9 Then sldDetails.Value = 9
    If sldDetails.Value > 95 Then sldDetails.Value = 95
    
    tabDetails.Top = sldDetails.Value * 40 + 60
    tabDetails.Height = (Screen.Height - 380 * 2) - (tabDetails.Top + 300)
    
    If tabDetails.Height > 680 Then
    '    spltTrans.Height = tabDetails.Height - (560 + Me.cmdTranFilter.Height)
    '    sprdSuppliers.Height = spltTrans.Height
        Call ResizeTabs
      
    Else
        sprdSuppliers.Height = 0
      
    End If

End Sub




Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
Dim intKeyIn As Integer

'On Error GoTo bad_toolbar_op

    Select Case (Button.Key)
        Case ("Refresh"):  Call mnuRefresh_Click
        Case ("Save"):     Call mnuSave_Click
        Case ("Previous"): Call mnuIPrev_Click
        Case ("Next"):     Call mnuINext_Click
        Case ("New"):      Call mnuNew_Click
        Case ("Delete"):   Call mnuDelete_Click
        Case ("Search"):   Call mnuILocate_Click
        
    End Select

End Sub


Private Sub txtDescription_Change()

  data_edited = True
  
  If LenB(txtDescription.Text) = 0 Then
    txtDescription.BackColor = RGB(192, 255, 255)
  Else
    txtDescription.BackColor = RGB_WHITE
  End If

End Sub


Private Sub ucifSearch_UseList(SearchCriteria As Collection, oMatchingItems As Object)
Dim lCritNo As Integer
Dim lFieldID As Long
Dim strrow As String

    Screen.MousePointer = vbHourglass
    
    'Remove previous entries
    Call cmbStSearch.Clear
    
    'Link  retrieved list to display list
    With oMatchingItems.Recordset
        If (.RecordCount > 0) Then
            .MoveFirst
            While (Not .EOF)
                strrow = !skun & vbTab & !descr & vbTab & !Status
                cmbStSearch.AddItem strrow
                .MoveNext
                
            Wend
        End If
    End With
    
    'Set cmbStSearch.DataSourceList = oMatchingItems
    cmbStSearch.Col = 0
    cmbStSearch.ColWidth = 10
    cmbStSearch.ColHeaderText = "SKU"
    cmbStSearch.Col = 1
    cmbStSearch.ColWidth = 45
    cmbStSearch.ColHeaderText = "DESCRIPTION"
    cmbStSearch.ColSortSeq = 0
    cmbStSearch.ColSortDataType = ColSortDataTypeTextNoCase
    cmbStSearch.ColSorted = SortedAscending
    cmbStSearch.Col = 2
    cmbStSearch.ColWidth = 11
    cmbStSearch.ColHeaderText = "STATUS"
    
    
    'Select and display first item
    lblNoMatches.Caption = cmbStSearch.ListCount
    ucifSearch.Visible = False
    cmbStSearch.ListIndex = 0
    cmbStSearch.Visible = False
    
    If cmbStSearch.ListCount = 0 Then
        Call ClearScreen
    Else
        Call DisplayItem
    End If
    
    Call txtDescription.SetFocus
    Screen.MousePointer = vbNormal

End Sub
