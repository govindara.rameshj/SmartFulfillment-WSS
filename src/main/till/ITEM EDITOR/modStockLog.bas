Attribute VB_Name = "modStockLog"
'<CAMH>****************************************************************************************
'* Module : modStockLog
'* Date   : 03/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Item Editor/modStockLog.bas $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Damians $
'* $Date: 20/05/04 15:14 $
'* $Revision: 10 $
'* Versions:
'* 03/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "modStockLog"

Public Const TTYPE_RET As Long = 1
Public Const TTYPE_IST_ALL As Long = 2
Public Const TTYPE_IST_IN As Long = 3
Public Const TTYPE_IST_OUT As Long = 4
Public Const TTYPE_REC As Long = 5
Public Const TTYPE_REFUNDS As Long = 6
Public Const TTYPE_SALES_INC As Long = 7
Public Const TTYPE_ADJ As Long = 8
Public Const TTYPE_VAR As Long = 9
Public Const TTYPE_WOF As Long = 10
Public Const TTYPE_SPEC_REFUNDS As Long = 11
Public Const TTYPE_SPEC_SALES_INC As Long = 12

Public Const TCODE_BULK As String = "BS"
Public Const TCODE_ISTIN As String = "II"
Public Const TCODE_ISTOUT As String = "IO"
Public Const TCODE_ADJ As String = "AD"
'Public Const TCODE_WOF As String = "AD"
Public Const TCODE_RECEIPT As String = "RE"
Public Const TCODE_RETURN As String = "SR"
Public Const TCODE_VAR As String = "SV"
Public Const TCODE_REFUND As String = "RF"
Public Const TCODE_SALE As String = "SA"

Public Const COL_LOG_DATE As Long = 1
Public Const COL_LOG_TIME As Long = 2
Public Const COL_LOG_SKU As Long = 3
Public Const COL_LOG_DESC As Long = 4
Public Const COL_LOG_SIZE As Long = 5
Public Const COL_LOG_QTY As Long = 6
Public Const COL_LOG_PRIOR As Long = 7
Public Const COL_LOG_COST As Long = 8
Public Const COL_LOG_SOLDAT As Long = 9
Public Const COL_LOG_ORIGDOC As Long = 10
Public Const COL_LOG_REFERENCE As Long = 11
Public Const COL_LOG_TYPE_REFERENCE As Long = 12
Public Const COL_LOG_LINETYPE As Long = 13
Public Const COL_LOG_KEY As Long = 14
Public Const COL_LOG_SDATE As Long = 15
Public Const COL_LOG_TTYPE As Long = 16

Public mcolUsers   As New Collection
Private mcolStores As New Collection

Public Const CRIT_LESSTHAN As Long = 2
Public Const CRIT_GREATERTHAN As Long = 1
Public Const CRIT_EQUALS As Long = 0
Public Const CRIT_ALL As Long = 3
Public Const CRIT_FROM As Long = 4

Public Sub FillInTransactions(ByVal strPartCode As String, _
                              ByVal lngDateCrit As Long, _
                              ByVal dteStartDate As Date, _
                              ByVal dteEndDate As Date, _
                              ByVal lngTranType As Long, _
                              ByRef sprdHistory As fpSpread, _
                              ByRef ucpbProgress As ucpbProgressBar, _
                              ByVal blnShowType As Boolean)

Dim oStockLog      As cStockLog
Dim lngLineNo      As Integer
Dim colStockLogBO  As Collection
Dim strTranType    As String
Dim strOtherType   As String
Dim oUserBO        As cUser
Dim blnShowWof     As Boolean
Dim blnCheckKey    As Boolean
Dim oItem          As cInventory
Dim colItemNames   As Collection
Dim colUsers       As Collection
Dim strSpecialSKU  As String

On Error GoTo bad_transaction

    Screen.MousePointer = 11

    sprdHistory.MaxRows = 0
    If mcolUsers.Count = 0 Then
        Set oUserBO = goDatabase.CreateBusinessObject(CLASSID_USER)
        Call oUserBO.AddLoadField(FID_USER_EmployeeID)
        Call oUserBO.AddLoadField(FID_USER_FullName)
        Set colUsers = oUserBO.LoadMatches
        For lngLineNo = 1 To colUsers.Count Step 1
            Call mcolUsers.Add(CStr(colUsers(lngLineNo).FullName), CStr(colUsers(lngLineNo).EmployeeID))
        Next lngLineNo
    End If
    
    Set oStockLog = goDatabase.CreateBusinessObject(CLASSID_STOCKLOG)
    
    strTranType = vbNullString
    Set colItemNames = New Collection
    If lngTranType <> 0 Then
        
        Select Case (lngTranType)
            'Case (TTYPE_BULK): strTranType = strTranType & TCODE_BULK
            Case (TTYPE_IST_ALL): strTranType = strTranType & TCODE_ISTIN
                              strOtherType = strOtherType & TCODE_ISTOUT
            Case (TTYPE_IST_IN): strTranType = strTranType & TCODE_ISTIN
            Case (TTYPE_IST_OUT): strTranType = strTranType & TCODE_ISTOUT
            Case (TTYPE_ADJ): strTranType = strTranType & TCODE_ADJ
                              blnShowWof = False
            Case (TTYPE_REC): strTranType = strTranType & TCODE_RECEIPT
            Case (TTYPE_RET): strTranType = strTranType & TCODE_RETURN
            Case (TTYPE_VAR): strTranType = strTranType & TCODE_VAR
            Case (TTYPE_SALES_INC): strTranType = strTranType & TCODE_REFUND
                              strOtherType = strOtherType & TCODE_SALE
            Case (TTYPE_REFUNDS): strTranType = strTranType & TCODE_REFUND
            Case (TTYPE_WOF): strTranType = strTranType & TCODE_ADJ
                              blnShowWof = True
            Case (TTYPE_SPEC_SALES_INC): strTranType = strTranType & TCODE_REFUND
                            strOtherType = strOtherType & TCODE_SALE
                            strSpecialSKU = "90%"
            Case (TTYPE_SPEC_REFUNDS): strTranType = strTranType & TCODE_REFUND
                            strSpecialSKU = "90%"
            Case Else
            
        End Select
        
        blnCheckKey = False
    Else
        blnCheckKey = True
    End If
    '*******************************************************
    '* create list of all transactions that match criteria *
    '*******************************************************
    
    If LenB(strTranType) <> 0 Then Call oStockLog.AddLoadFilter(CMP_EQUAL, FID_STOCKLOG_MovementType, strTranType)
    If LenB(strPartCode) <> 0 Then 'if viewing 1 part code then filter and hide SKU column
        Call oStockLog.AddLoadFilter(CMP_EQUAL, FID_STOCKLOG_PartCode, strPartCode)
        sprdHistory.Col = COL_LOG_SKU
        sprdHistory.ColHidden = True
        sprdHistory.Col = COL_LOG_DESC
        sprdHistory.ColHidden = True
        sprdHistory.Col = COL_LOG_SIZE
        sprdHistory.ColHidden = True
    Else
        sprdHistory.Col = COL_LOG_SKU
        sprdHistory.ColHidden = False
        sprdHistory.Col = COL_LOG_DESC
        sprdHistory.ColHidden = False
        sprdHistory.Col = COL_LOG_SIZE
        sprdHistory.ColHidden = False
    End If
    
    'Add Special's criteria
    If LenB(strSpecialSKU) <> 0 Then
        Call oStockLog.AddLoadFilter(CMP_LIKE, FID_STOCKLOG_PartCode, strSpecialSKU)
    End If
    
    'Set Date criteria
    '=== =============
    Select Case (lngDateCrit)
        Case Is = CRIT_LESSTHAN
            Call oStockLog.AddLoadFilter(CMP_LESSEQUALTHAN, FID_STOCKLOG_LogDate, dteStartDate)
        Case Is = CRIT_GREATERTHAN
            Call oStockLog.AddLoadFilter(CMP_GREATEREQUALTHAN, FID_STOCKLOG_LogDate, dteStartDate)
        Case Is = CRIT_EQUALS
            Call oStockLog.AddLoadFilter(CMP_EQUAL, FID_STOCKLOG_LogDate, dteStartDate)
        Case CRIT_FROM
            Call oStockLog.AddLoadFilter(CMP_GREATEREQUALTHAN, FID_STOCKLOG_LogDate, dteStartDate)
            Call oStockLog.AddLoadFilter(CMP_LESSEQUALTHAN, FID_STOCKLOG_LogDate, dteEndDate)
    End Select
    
    'Remove Sold At Column for all reports apart from ALL, Sold, Refund
    If (lngTranType = 0) Or (lngTranType = TTYPE_SALES_INC) Or (lngTranType = TTYPE_REFUNDS) _
                Or (lngTranType = TTYPE_SPEC_SALES_INC) Or (lngTranType = TTYPE_SPEC_REFUNDS) Then
        sprdHistory.Col = COL_LOG_SOLDAT
        sprdHistory.ColHidden = False
    Else
        sprdHistory.Col = COL_LOG_SOLDAT
        sprdHistory.ColHidden = True
    End If
    
    Set colStockLogBO = New Collection
    Set colStockLogBO = oStockLog.LoadMatches
    
    If colStockLogBO.Count > 0 Then ucpbProgress.Max = colStockLogBO.Count
    
    For lngLineNo = 1 To colStockLogBO.Count Step 1
        ucpbProgress.Value = lngLineNo
        
        Call DisplayLine(colStockLogBO(CLng(lngLineNo)), sprdHistory, colItemNames, colUsers, blnShowType, blnShowWof, blnCheckKey, True)
    Next lngLineNo
            
    
    'Check if Tran type made up of multiple types, if so then get next
    If LenB(strOtherType) <> 0 Then
        Set oStockLog = goDatabase.CreateBusinessObject(CLASSID_STOCKLOG)
    
        '*************************************************************
        '* create list of all transactions that for second tran type *
        '*************************************************************
        Call oStockLog.AddLoadFilter(CMP_EQUAL, FID_STOCKLOG_MovementType, strOtherType)
        If LenB(strPartCode) <> 0 Then Call oStockLog.AddLoadFilter(CMP_EQUAL, FID_STOCKLOG_PartCode, strPartCode)
    
        'Check if start and end dates are required, if so add to filters
        'If Year(dteStartDate) > 1899 Then Call oStockLog.AddLoadFilter(CMP_GREATEREQUALTHAN, FID_STOCKLOG_LogDate, dteStartDate)
        'If Year(dteEndDate) > 1899 Then Call oStockLog.AddLoadFilter(CMP_LESSEQUALTHAN, FID_STOCKLOG_LogDate, dteEndDate)
        
        'Add Special's criteria
        If LenB(strSpecialSKU) <> 0 Then
            Call oStockLog.AddLoadFilter(CMP_LIKE, FID_STOCKLOG_PartCode, strSpecialSKU)
        End If
        
        Select Case (lngDateCrit)
            Case Is = CRIT_LESSTHAN
                Call oStockLog.AddLoadFilter(CMP_LESSEQUALTHAN, FID_STOCKLOG_LogDate, dteStartDate)
            Case Is = CRIT_GREATERTHAN
                Call oStockLog.AddLoadFilter(CMP_GREATEREQUALTHAN, FID_STOCKLOG_LogDate, dteStartDate)
            Case Is = CRIT_EQUALS
                Call oStockLog.AddLoadFilter(CMP_EQUAL, FID_STOCKLOG_LogDate, dteStartDate)
            Case CRIT_FROM
                Call oStockLog.AddLoadFilter(CMP_GREATEREQUALTHAN, FID_STOCKLOG_LogDate, dteStartDate)
                Call oStockLog.AddLoadFilter(CMP_LESSEQUALTHAN, FID_STOCKLOG_LogDate, dteEndDate)
        End Select
        
        Set colStockLogBO = New Collection
        Set colStockLogBO = oStockLog.LoadMatches
        
        If colStockLogBO.Count > 0 Then ucpbProgress.Max = colStockLogBO.Count
        
        For lngLineNo = 1 To colStockLogBO.Count Step 1
            ucpbProgress.Value = lngLineNo
            
            Call DisplayLine(colStockLogBO(CLng(lngLineNo)), sprdHistory, colItemNames, colUsers, blnShowType, blnShowWof, blnCheckKey, True)
        Next lngLineNo
    
    End If
    
    sprdHistory.ColWidth(COL_LOG_REFERENCE) = sprdHistory.MaxTextColWidth(COL_LOG_REFERENCE)
    sprdHistory.ColWidth(COL_LOG_ORIGDOC) = sprdHistory.MaxTextColWidth(COL_LOG_ORIGDOC)
    ucpbProgress.Value = 0
    
    Screen.MousePointer = 0
    
    Exit Sub
    
bad_transaction:

If Err.Number <> 94 And Err.Number <> 13 Then
    MsgBox (Error$), , "Error retreiving Transactions"
    Screen.MousePointer = 0
    Exit Sub
End If

Resume Next
  
End Sub

Private Sub GetStores()

Dim oStore     As cStore
Dim lngStoreNo As Long
Dim colStores  As Collection

    Set oStore = goDatabase.CreateBusinessObject(CLASSID_STORE)
    Call oStore.AddLoadField(FID_STORE_StoreNumber)
    Call oStore.AddLoadField(FID_STORE_AddressLine1)
    Set colStores = oStore.LoadMatches
    For lngStoreNo = 1 To colStores.Count Step 1
        Call mcolStores.Add(CStr(colStores(lngStoreNo).AddressLine1), CStr(colStores(lngStoreNo).StoreNumber))
    Next lngStoreNo

End Sub

Private Sub DisplayLine(oStockLogBO As cStockLog, _
                        ByRef sprdHistory As fpSpread, _
                        ByRef colItemNames As Collection, _
                        ByRef colUsers As Collection, _
                        ByVal blnShowType As Boolean, _
                        ByVal blnShowWof As Boolean, _
                        ByVal blnCheckKey As Boolean, _
                        ByVal blnShowAdjReason As Boolean)
    
Dim strItemDesc     As String
Dim oItem           As cInventory
Dim oTranBO         As cPOSHeader
Dim strDelim        As String
Dim strKey          As String
Dim strStoreName    As String
Dim oAdjReason      As cStockAdjustReason 'Used for stockadjument reason
Dim strComment      As String
Dim strTypeField    As String
Dim strRefField     As String
    
    'Determines if this is a authentic adjustment
    If (oStockLogBO.MovementType = TCODE_ADJ) And blnCheckKey = False Then
        If blnShowWof = False And Val(Mid$(oStockLogBO.TransactionKey, 9, 2)) >= 90 Then Exit Sub
        If blnShowWof = True And Val(Mid$(oStockLogBO.TransactionKey, 9, 2)) <= 89 Then Exit Sub
    End If
    
    sprdHistory.MaxRows = sprdHistory.MaxRows + 1
    
    sprdHistory.Row = sprdHistory.MaxRows
    sprdHistory.Col = COL_LOG_KEY
    sprdHistory.Text = oStockLogBO.Key
    sprdHistory.Col = COL_LOG_DATE
    sprdHistory.Text = DisplayDate(oStockLogBO.LogDate, False)
    sprdHistory.Col = COL_LOG_TIME
    sprdHistory.Text = Format$(oStockLogBO.LogTime, "@@:@@:@@")
    sprdHistory.Col = COL_LOG_SKU
    sprdHistory.Text = oStockLogBO.PartCode
    If sprdHistory.ColHidden = False Then
            
        strItemDesc = vbNullString
        On Error Resume Next
        'attempt to get Item Description and Size from list of previously stock logs
        strItemDesc = colItemNames(sprdHistory.Text)
        If Err.Number <> 0 Then 'if not found then get from database
            Set oItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
            Call oItem.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, sprdHistory.Text)
            Call oItem.AddLoadField(FID_INVENTORY_Description)
            Call oItem.LoadMatches
            strItemDesc = oItem.Description
            Set oItem = Nothing
            'Add retrieved name to list, so that it does not have to be retrieved again
            Call colItemNames.Add(strItemDesc, sprdHistory.Text)
        Else
            strItemDesc = Left$(strItemDesc, InStr(strItemDesc, vbTab) - 1)
        End If
        Call Err.Clear
        'Display Customer name for Order
        sprdHistory.Col = COL_LOG_DESC
        sprdHistory.Text = strItemDesc
        sprdHistory.Col = COL_LOG_SIZE
    End If
    
    sprdHistory.Col = COL_LOG_ORIGDOC
    If blnShowType = True Then
        If Left$(oStockLogBO.PartCode, 2) = "90" Then
            sprdHistory.Text = "Special "
        End If
        
        sprdHistory.Text = sprdHistory.Text & oStockLogBO.MovementDesc
        strDelim = "-"
    Else
        strDelim = vbNullString
    End If
    strKey = vbNullString
    Select Case (oStockLogBO.MovementType)
        Case (TCODE_SALE):    sprdHistory.Text = sprdHistory.Text & strDelim & " " & Mid$(oStockLogBO.TransactionKey, 3)
        Case (TCODE_REFUND):  sprdHistory.Text = sprdHistory.Text & strDelim & " " & Mid$(oStockLogBO.TransactionKey, 3)
        Case (TCODE_RECEIPT): sprdHistory.Text = sprdHistory.Text & strDelim & "DRL" & oStockLogBO.TransactionKey
        Case (TCODE_ISTIN):   sprdHistory.Text = sprdHistory.Text & strDelim & "DRL" & oStockLogBO.TransactionKey
                              If mcolStores.Count = 0 Then Call GetStores
                              strKey = oStockLogBO.TransactionKey
                              If Len(strKey) > 16 Then
                                  strKey = Mid$(strKey, 17)
                              Else
                                  strKey = "000"
                              End If
                              On Error Resume Next
                              strStoreName = mcolStores(strKey)
                              Call Err.Clear
                              On Error GoTo 0
                              If LenB(strStoreName) = 0 Then strStoreName = strKey
                              strKey = "-From " & strStoreName
        Case (TCODE_ISTOUT):  sprdHistory.Text = sprdHistory.Text & strDelim & "DRL" & oStockLogBO.TransactionKey
                              strKey = oStockLogBO.TransactionKey
                              If Len(strKey) > 16 Then
                                  strKey = Mid$(strKey, 17)
                              Else
                                  strKey = "000"
                              End If
                              If mcolStores.Count = 0 Then Call GetStores
                              On Error Resume Next
                              strStoreName = mcolStores(strKey)
                              Call Err.Clear
                              On Error GoTo 0
                              If LenB(strStoreName) = 0 Then strStoreName = strKey
                              strKey = "-To " & strStoreName
        Case (TCODE_ADJ):
                            If blnShowAdjReason = True Then
                                'Show Stock Adjustment
                                If Val(Mid$(oStockLogBO.TransactionKey, 9, 2)) <= 89 Then
                                    If blnShowType = True Then
                                        sprdHistory.Text = "Stock Adjustment"
                                    End If
                                End If
                                
                                'Show Stock WriteOff
                                If Val(Mid$(oStockLogBO.TransactionKey, 9, 2)) >= 90 Then
                                    If blnShowType = True Then
                                        sprdHistory.Text = "Write Off"
                                    End If
                                End If
                                
                                strComment = Mid$(oStockLogBO.TransactionKey, 11)
                                
                                'Create Stock Adjustment Code Business Object
                                Set oAdjReason = goDatabase.CreateBusinessObject(CLASSID_STOCKADJCODE)
                                Call oAdjReason.AddLoadFilter(CMP_EQUAL, FID_STOCKADJCODE_AdjustmentNo, Mid$(oStockLogBO.TransactionKey, 9, 2))
                                Call oAdjReason.IBo_Load
                                
                                sprdHistory.Text = sprdHistory.Text & strDelim & Mid$(oStockLogBO.TransactionKey, 9, 2) & "-" & oAdjReason.Description
                            Else
                                'Show Stock Adjustment
                                If Val(Mid$(oStockLogBO.TransactionKey, 9, 2)) <= 89 Then
                                    If blnShowType = True Then sprdHistory.Text = "Stock Adjustment"
                                    sprdHistory.Text = sprdHistory.Text & strDelim & oStockLogBO.TransactionKey
                                End If
    
                                'Show Stock WriteOff
                                If Val(Mid$(oStockLogBO.TransactionKey, 9, 2)) >= 90 Then
                                    If blnShowType = True Then sprdHistory.Text = "Write Off"
                                    sprdHistory.Text = sprdHistory.Text & strDelim & oStockLogBO.TransactionKey
                                End If
                            End If
                             
        Case (TCODE_RETURN):  Set oTranBO = goDatabase.CreateBusinessObject(CLASSID_RETURNNOTE)
                              Call oTranBO.AddLoadFilter(CMP_EQUAL, FID_RETURNNOTE_DocNo, oStockLogBO.TransactionKey)
                              Call oTranBO.AddLoadField(FID_RETURNNOTE_SupplierReturnNumber)
                              Call oTranBO.AddLoadField(FID_RETURNNOTE_ShortageNote)
                              Call oTranBO.LoadMatches
                              If LenB(oTranBO.ExternalDocumentNo) = 0 Then
                                  sprdHistory.Text = sprdHistory.Text & strDelim & "NA"
                              End If
                              sprdHistory.Text = sprdHistory.Text & "/DRL" & oStockLogBO.TransactionKey
        Case (TCODE_VAR):     'Do nothing
        
        Case Else: sprdHistory.Text = sprdHistory.Text & strDelim & oStockLogBO.MovementType
    End Select
    
    sprdHistory.Col = COL_LOG_PRIOR
    sprdHistory.Text = oStockLogBO.StartStockQty
    sprdHistory.Col = COL_LOG_SOLDAT
    If (oStockLogBO.MovementType = TCODE_SALE) Or (oStockLogBO.MovementType = TCODE_REFUND) Then
        sprdHistory.Text = (oStockLogBO.EndStockQty - oStockLogBO.StartStockQty) * oStockLogBO.StartPrice
    Else
        sprdHistory.BackColor = RGB_GREY
    End If
    sprdHistory.Col = COL_LOG_QTY
    sprdHistory.Text = oStockLogBO.EndStockQty - oStockLogBO.StartStockQty
    sprdHistory.Col = COL_LOG_REFERENCE
    
    'We will only have a comment if it is a stock adjustment
    If LenB(strComment) = 0 Then
        On Error Resume Next
        sprdHistory.Text = mcolUsers(oStockLogBO.EmployeeID)
        On Error GoTo 0
        If LenB(sprdHistory.Text) = 0 Then sprdHistory.Text = oStockLogBO.EmployeeID
        sprdHistory.Text = sprdHistory.Text & strKey
    Else
        sprdHistory.Text = strComment
    End If
    
    sprdHistory.Col = COL_LOG_ORIGDOC
    strTypeField = sprdHistory.Text
    
    sprdHistory.Col = COL_LOG_REFERENCE
    strRefField = sprdHistory.Text
    
    sprdHistory.Col = COL_LOG_TYPE_REFERENCE
    sprdHistory.TypeMaxEditLen = 500
    sprdHistory.TypeTextWordWrap = False
    sprdHistory.TypeEditMultiLine = True
           
    If LenB(strTypeField) <> 0 Then
        sprdHistory.Text = strTypeField
    End If
    
    If LenB(strRefField) <> 0 Then
        If LenB(sprdHistory.Text) <> 0 Then
            sprdHistory.Text = sprdHistory.Text & vbCrLf
        End If
        
        sprdHistory.Text = sprdHistory.Text & strRefField
    End If
    
    sprdHistory.Col = COL_LOG_LINETYPE
    sprdHistory.Text = 1 'flag as line of data
    sprdHistory.BackColor = RGB_BLUE
    
    sprdHistory.Col = COL_LOG_SDATE
    sprdHistory.Text = Format$(oStockLogBO.LogDate, "YYYYMMDD") & oStockLogBO.LogTime
    sprdHistory.Col = COL_LOG_TTYPE
    sprdHistory.Text = oStockLogBO.MovementType

End Sub
