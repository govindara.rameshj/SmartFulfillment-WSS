VERSION 5.00
Begin VB.Form frmUpdate 
   Caption         =   "Topps - Create DLTOTS"
   ClientHeight    =   5610
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6150
   Icon            =   "frmUpdate.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5610
   ScaleWidth      =   6150
   StartUpPosition =   1  'CenterOwner
   Begin VB.Label lblStockTake 
      BackStyle       =   0  'Transparent
      Height          =   735
      Left            =   240
      TabIndex        =   1
      Top             =   4320
      Width           =   5535
   End
   Begin VB.Label lblStatus 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3735
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   5895
   End
End
Attribute VB_Name = "frmUpdate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim madoConn            As ADODB.Connection


Private Sub ReCreateLRevLines()

Dim rsSummary    As New Recordset
Dim rsNextSeq    As New Recordset
Dim rsDLLINE    As New Recordset
Dim strSql      As String
Dim blnUpdateHdr As Boolean
Dim dteStkTake  As Date
Dim lngCount    As Long

Dim strAdjCode   As String
Dim dteDate     As Date
Dim strTranNo   As String

Dim lngStkONHA  As Long

Dim oFSO As New FileSystemObject
Dim tsLogFile As TextStream

On Error GoTo UpdateError

    Set madoConn = New ADODB.Connection
'    Call madoConn.Open("WIXTill")
    Call madoConn.Open("Oasys")
    'create FileSystemObject
    
    lngCount = 0
    
    lblStatus.Caption = "Accessing List Of Reversed Lines"
    Call rsSummary.Open("select dl.date1,dl.till,dl.tran,sum(extp) AS Total from dlline dl Inner join DLTOTS dt ON dt.Date1=dl.DATE1 AND dt.TILL = dl.TILL AND dt.TRAN=dl.TRAN where void=0 and lrev=1 and dl.date1>='2008-10-01' group by dl.date1,dl.till,dl.tran having sum(extp)<>0", madoConn)
    Me.Refresh
    Set tsLogFile = oFSO.OpenTextFile("c:\wix\ReCreateLRev.Log", ForAppending, True)
    'read the context of the text file and add it to the text hold variable
    While Not rsSummary.EOF
    
        lblStatus.Caption = "Updating = " & rsSummary.Fields("Date1") & "-" & rsSummary.Fields("Till") & "-" & rsSummary.Fields("Tran")
        tsLogFile.WriteLine (rsSummary.Fields("Date1") & "," & rsSummary.Fields("Till") & "," & rsSummary.Fields("Tran") & "," & rsSummary.Fields("Total"))
        Me.Refresh
        
        Call rsNextSeq.Open("SELECT MAX(NUMB) AS MaxLineNo FROM DLLINE WHERE DATE1 = '" & Format(rsSummary.Fields("DATE1"), "YYYY-MM-DD") & "' AND TILL='" & rsSummary.Fields("TILL") & "' AND TRAN='" & rsSummary.Fields("TRAN") & "'", madoConn)
        lngCount = rsNextSeq.Fields("MaxLineNo")
        rsNextSeq.Close
        
        Call rsDLLINE.Open("SELECT * FROM DLLINE WHERE DATE1 = '" & Format(rsSummary.Fields("DATE1"), "YYYY-MM-DD") & "' AND TILL='" & rsSummary.Fields("TILL") & "' AND TRAN='" & rsSummary.Fields("TRAN") & "' AND LREV=1 AND SPARE=''", madoConn, adOpenForwardOnly, adLockReadOnly)
        
'        Call DebugMsg("mm", "", endlDebug, "Starting STKMAS= " & lngStkONHA & ":" & rsSummary.Fields("SKUN"))
        
        While (Not rsDLLINE.EOF)
        lngCount = lngCount + 1
            strSql = "INSERT INTO DLLINE (DATE1,TILL,TRAN,NUMB,SKUN,DEPT,IBAR,SUPV,QUAN,SPRI,PRIC," & _
                "PRVE,EXTP,EXTC,RITM,PORC,ITAG,CATA,VSYM,TPPD,TPME,POPD,POME,QBPD,QBME,DGPD,DGME,MBPD," & _
                "MBME,HSPD,HSME,ESPD,ESME,LREV,ESEQ,CTGY,GRUP,SGRP,STYL,QSUP,ESEV,IMDN,SALT,VATN,VATV," & _
                "BDCO,BDCOInd,RCOD,SPARE,WEERATE,WEESEQN,WEECOST) VALUES ('"
            strSql = strSql & Format(rsDLLINE.Fields("DATE1"), "YYYY-MM-DD") & "','" & rsDLLINE.Fields("TILL") & "','" & rsDLLINE.Fields("TRAN") & "'," & _
                lngCount & ",'" & rsDLLINE.Fields("SKUN") & "','" & rsDLLINE.Fields("DEPT") & "'," & IIf(rsDLLINE.Fields("IBAR"), 1, 0) & ",'" & _
                rsDLLINE.Fields("SUPV") & "'," & rsDLLINE.Fields("QUAN") * -1 & "," & rsDLLINE.Fields("SPRI") & "," & rsDLLINE.Fields("PRIC") & "," & _
                rsDLLINE.Fields("PRVE") & "," & rsDLLINE.Fields("EXTP") * -1 & "," & rsDLLINE.Fields("EXTC") & "," & IIf(rsDLLINE.Fields("RITM"), 1, 0) & "," & _
                rsDLLINE.Fields("PORC") & "," & IIf(rsDLLINE.Fields("ITAG"), 1, 0) & "," & IIf(rsDLLINE.Fields("CATA"), 1, 0) & ",'" & rsDLLINE.Fields("VSYM") & "'," & _
                rsDLLINE.Fields("TPPD") & ",'" & rsDLLINE.Fields("TPME") & "'," & rsDLLINE.Fields("POPD") & ",'" & rsDLLINE.Fields("POME") & "'," & _
                rsDLLINE.Fields("QBPD") & ",'" & rsDLLINE.Fields("QBME") & "'," & rsDLLINE.Fields("DGPD") & ",'" & rsDLLINE.Fields("DGME") & "'," & _
                rsDLLINE.Fields("MBPD") & ",'" & rsDLLINE.Fields("MBME") & "'," & rsDLLINE.Fields("HSPD") & ",'" & rsDLLINE.Fields("HSME") & "'," & _
                rsDLLINE.Fields("ESPD") & ",'" & rsDLLINE.Fields("ESME") & "'," & IIf(rsDLLINE.Fields("LREV"), 1, 0) & ",'" & rsDLLINE.Fields("ESEQ") & "','" & _
                rsDLLINE.Fields("CTGY") & "','" & rsDLLINE.Fields("GRUP") & "','" & rsDLLINE.Fields("SGRP") & "','" & rsDLLINE.Fields("STYL") & "','" & _
                rsDLLINE.Fields("QSUP") & "'," & rsDLLINE.Fields("ESEV") & "," & IIf(rsDLLINE.Fields("IMDN"), 1, 0) & ",'" & rsDLLINE.Fields("SALT") & "'," & _
                rsDLLINE.Fields("VATN") & "," & rsDLLINE.Fields("VATV") * -1 & ",'" & rsDLLINE.Fields("BDCO") & "'," & IIf(rsDLLINE.Fields("BDCOInd"), 1, 0) & ",'" & _
                rsDLLINE.Fields("RCOD") & "','CTS" & rsDLLINE.Fields("NUMB") & "','" & rsDLLINE.Fields("WEERATE") & "','" & rsDLLINE.Fields("WEESEQN") & "'," & rsDLLINE.Fields("WEECOST") & ")"
    
            'First record is the starting position so skip this one
            Call madoConn.Execute(strSql)
            Call madoConn.Execute("UPDATE DLLINE SET SPARE='CTS-" & lngCount & "' WHERE DATE1='" & Format(rsDLLINE.Fields("DATE1"), "YYYY-MM-DD") & _
                "' AND TILL='" & rsDLLINE.Fields("TILL") & "' AND TRAN='" & rsDLLINE.Fields("TRAN") & "' AND NUMB=" & rsDLLINE.Fields("NUMB"))
            Call rsDLLINE.MoveNext
        Wend
        rsDLLINE.Close
        Call rsSummary.MoveNext
    Wend
    
    'close the text file
    Call rsSummary.Close
    Call tsLogFile.Close
    
    Call madoConn.Close
    lblStatus.Caption = lblStatus.Caption & "Update Complete"
    
    Exit Sub
    
UpdateError:
    Call MsgBox(Err.Description)
    Call Err.Clear
    
    
End Sub 'Update

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
Dim oFSO As New FileSystemObject
Dim tsLogFile As TextStream
    
    Me.Show
    Call ReCreateLRevLines
    Set tsLogFile = oFSO.OpenTextFile(App.Path & "\" & App.EXEName & ".LOG", ForAppending, True)
    Call tsLogFile.Close
    End

End Sub 'form_load

