VERSION 5.00
Begin VB.Form frmStoreNums 
   Caption         =   "Convert Failure email"
   ClientHeight    =   2505
   ClientLeft      =   4680
   ClientTop       =   4515
   ClientWidth     =   6195
   Icon            =   "frmStoreNums.frx":0000
   LinkTopic       =   "Form3"
   ScaleHeight     =   2505
   ScaleWidth      =   6195
   Begin VB.TextBox Text2 
      Height          =   495
      Left            =   1320
      MultiLine       =   -1  'True
      TabIndex        =   3
      Text            =   "frmStoreNums.frx":058A
      Top             =   2640
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.ListBox LB 
      Height          =   450
      Left            =   480
      Sorted          =   -1  'True
      TabIndex        =   2
      Top             =   2640
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Continue"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4560
      TabIndex        =   1
      Top             =   2040
      Width           =   1575
   End
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   720
      Width           =   6015
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   $"frmStoreNums.frx":0590
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   6015
   End
End
Attribute VB_Name = "frmStoreNums"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command2_Click()

Dim aryStores()        As String
Dim lngNum             As Long
Dim intInit            As Integer
Dim lngLBIndex         As Long
Dim strTextToMatch     As String
Dim intLV              As Integer
Dim intNumOfDuplicates As Integer

    If (Me.Text1.Text = "") Then
        Exit Sub
    End If
    
    Text1.Text = Replace(Text1.Text, "(2 days)", "")
    Text1.Text = Replace(Text1.Text, "(3 days)", "")
    Text1.Text = Replace(Text1.Text, "(4 days)", "")
    Text1.Text = Replace(Text1.Text, "(5 days)", "")
    Text1.Text = Replace(Text1.Text, "(6 days)", "")
    Text1.Text = Replace(Text1.Text, "(7 days)", "")
    Text1.Text = Replace(Text1.Text, "(8 days)", "")
    Text1.Text = Replace(Text1.Text, "(9 days)", "")
    
    Text1.Text = Replace(Text1.Text, "STHOF", "")
    Text1.Text = Replace(Text1.Text, "STHOA", "")
    Text1.Text = Replace(Text1.Text, "STHPO", "")
    Text1.Text = Replace(Text1.Text, "flash", "")
    Text1.Text = Replace(Text1.Text, " ", "")
    Text1.Text = Replace(Text1.Text, "-", "")
    Text1.Text = Replace(Text1.Text, "orders", "")
    Text1.Text = Replace(Text1.Text, "activty", "")
    Text1.Text = Replace(Text1.Text, "activity", "")
    Text1.Text = Replace(Text1.Text, ".", "")
    Text1.Text = Replace(Text1.Text, vbCrLf, ",")
    Text1.Text = Replace(Text1.Text, ",", vbCrLf)
    
    Text1.Text = Replace(Text1.Text, "100%", "")

    aryStores() = Split(Text1.Text, vbCrLf)

    lngNum = 1
    intInit = aryStores(1)

    Text1.Text = Text1.Text & vbCrLf

    On Error GoTo skiperror

    While intInit <> ""
        intInit = aryStores(lngNum)
        LB.AddItem intInit
        lngNum = lngNum + 1
    Wend

skiperror:

    On Error GoTo skiperror2
    
    lngLBIndex = 0
    Do Until lngLBIndex = LB.ListCount
        'With this index, loop through all of the elements of the listbox in reverse order,
        'removing any element that has the same text as this one.
        strTextToMatch = LB.List(lngLBIndex)  'Text of this item to check duplicates for
        For intLV = LB.ListCount - 1 To lngLBIndex + 1 Step -1  'loop through remaining items to check for duplicates.
        'Notice: LB.ListCount -1 is the index of the last item in the listbox.
        'LBIndex + 1 is the item immediately after the one that we are checking duplicates on.
          If LB.List(intLV) = strTextToMatch Then
            'We've found a duplicate!  Delete it and then count it.
            LB.RemoveItem intLV
            intNumOfDuplicates = intNumOfDuplicates + 1
          End If
        Next intLV
        'Go to the next item: all duplicates of this have been found.
        lngLBIndex = lngLBIndex + 1
    Loop
    
    If LB.Text = "" Then LB.RemoveItem (0)
    
skiperror2:
    
    Text2.Text = "Generated by Wickes Polling Failures v" & App.Major & "." & App.Minor & "." & App.Revision & " at " & time & " on " & Date & vbCrLf
        
    intInit = 0
    
    While intInit < LB.ListCount
        Text2.Text = Text2.Text & vbCrLf & LB.List(0)
        LB.RemoveItem (0)
    Wend
        
    Printer.Print Text2.Text
    Printer.EndDoc
    
End Sub
