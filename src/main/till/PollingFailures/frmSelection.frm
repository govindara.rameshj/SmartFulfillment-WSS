VERSION 5.00
Begin VB.Form frmSelection 
   Caption         =   "Failures Selection"
   ClientHeight    =   7560
   ClientLeft      =   4680
   ClientTop       =   1935
   ClientWidth     =   6255
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmSelection.frx":0000
   LinkTopic       =   "Form2"
   ScaleHeight     =   7560
   ScaleWidth      =   6255
   Begin VB.Frame Frame4 
      Caption         =   "Night Failure Related (Select a task to enable)"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1455
      Left            =   120
      TabIndex        =   9
      Top             =   5640
      Width           =   6015
      Begin VB.CommandButton but4 
         Caption         =   "This is the FAILED task, and HAS been resumed"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   360
         Width           =   5775
      End
      Begin VB.CommandButton but5 
         Caption         =   "This is the FAILED task, but HAS NOT been resumed"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   720
         Width           =   5775
      End
      Begin VB.CommandButton but7 
         Caption         =   "This task FAILED and did NOT run in retry"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   1080
         Width           =   5775
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Store/User Related"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1455
      Left            =   120
      TabIndex        =   5
      Top             =   4080
      Width           =   6015
      Begin VB.CommandButton but2 
         Caption         =   "Night routine was started late"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   720
         Width           =   5775
      End
      Begin VB.CommandButton but3 
         Caption         =   "CLOSE/DATES not confirmed (PTCLOS/GSNDAT)"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1080
         Width           =   5775
      End
      Begin VB.CommandButton but1 
         Caption         =   "Night routine was NOT started"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   5775
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "SYSDAT Details"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   6015
      Begin VB.Label sysdets 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   5655
      End
   End
   Begin VB.CommandButton but6 
      Caption         =   "No Failure"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   7200
      Width           =   6015
   End
   Begin VB.Frame Frame1 
      Caption         =   "Task List"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3015
      Left            =   120
      TabIndex        =   0
      Top             =   960
      Width           =   6015
      Begin VB.ListBox lstFailures 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2595
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   5775
      End
   End
End
Attribute VB_Name = "frmSelection"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub but1_Click()
    
Dim strResult As String

    strResult = "N/A - Night has not been started"
    Unload Me
    frmPollingFailures.Show
    frmPollingFailures.endit (strResult)

End Sub

Private Sub but2_Click()

Dim strResult As String

    strResult = "N/A - Night was started late"
    Unload Me
    frmPollingFailures.Show
    frmPollingFailures.endit (strResult)

End Sub

Private Sub but3_Click()

Dim strResult As String
    
    strResult = "N/A - Store did not confirm PTCLOS/GSNDAT"
    Unload Me
    frmPollingFailures.Show
    frmPollingFailures.endit (strResult)

End Sub

Private Sub but4_Click()

Dim MyConn       As ADODB.Connection
Dim sqlscript    As String
Dim MyRecSet     As ADODB.Recordset
Dim Tnum         As String
Dim num          As String
Dim prevtaskETIM As String
Dim prevtaskNAME As String
Dim strResult    As String

    If (frmSelection.lstFailures.ListIndex = 0) Then
        MsgBox "No previous failure located, unable to complete"
        Exit Sub
    End If
    
    If (frmSelection.lstFailures.ListIndex > 0) Then
        Tnum = Left(frmSelection.lstFailures.List(frmSelection.lstFailures.ListIndex - 1), 4)
        num = Replace(Tnum, "T", "")
        
        On Error Resume Next
        
        Set MyConn = New ADODB.Connection
        
        MyConn.Open frmPollingFailures.txtStore.Text
        sqlscript = "SELECT * FROM NITLOG WHERE DATE1 = '" & frmPollingFailures.txtDate.Text & "' AND TASK = '" & num & "'"
        Set MyRecSet = MyConn.Execute(sqlscript)
        prevtaskETIM = Format(Left(MyRecSet.Fields.Item("etim").Value, 4), "##:##")
        prevtaskNAME = MyRecSet.Fields.Item("prog").Value
        Set MyRecSet = Nothing
        Set MyConn = Nothing
    End If
    strResult = "T" & num & " Failed at " & prevtaskETIM
    Unload Me
    frmPollingFailures.Show
    frmPollingFailures.endit (strResult)
    
End Sub

Private Sub but5_Click()

Dim MyConn       As ADODB.Connection
Dim sqlscript    As String
Dim MyRecSet     As ADODB.Recordset
Dim Tnum         As String
Dim num          As String
Dim prevtaskETIM As String
Dim prevtaskSTIM As String
Dim strResult    As String
    
    If (frmSelection.lstFailures.ListIndex > 0) Then
        Tnum = Left(frmSelection.lstFailures.List(frmSelection.lstFailures.ListIndex), 4)
        num = Replace(Tnum, "T", "")
        
        On Error Resume Next
        Set MyConn = New ADODB.Connection
        MyConn.Open frmPollingFailures.txtStore.Text
        
        sqlscript = "SELECT * FROM NITLOG WHERE DATE1 = '" & frmPollingFailures.txtDate.Text & "' AND TASK = '" & num & "'"
        Set MyRecSet = MyConn.Execute(sqlscript)
        prevtaskETIM = Format(Left(MyRecSet.Fields.Item("etim").Value, 4), "##:##")
        prevtaskSTIM = Format(Left(MyRecSet.Fields.Item("stim").Value, 4), "##:##")
        Set MyRecSet = Nothing
        Set MyConn = Nothing
    End If
    strResult = "T" & num & " started at " & prevtaskSTIM & " and has not yet been resumed"
    Unload Me
    frmPollingFailures.Show
    frmPollingFailures.endit (strResult)

End Sub

Private Sub but6_Click()

Dim strResult As String
    
    strResult = "N/A - No Failure"
    Unload Me
    frmPollingFailures.Show
    frmPollingFailures.endit (strResult)

End Sub

Private Sub but7_Click()

Dim MyConn       As ADODB.Connection
Dim sqlscript    As String
Dim MyRecSet     As ADODB.Recordset
Dim Tnum         As String
Dim num          As String
Dim prevtaskETIM As String
Dim prevtaskSTIM As String
Dim strResult    As String
    
    If (frmSelection.lstFailures.ListIndex > 0) Then
        Tnum = Left(frmSelection.lstFailures.List(frmSelection.lstFailures.ListIndex), 4)
        num = Replace(Tnum, "T", "")
        
        On Error Resume Next
        Set MyConn = New ADODB.Connection
        MyConn.Open frmPollingFailures.txtStore.Text
        
        sqlscript = "SELECT * FROM NITLOG WHERE DATE1 = '" & frmPollingFailures.txtDate.Text & "' AND TASK = '" & num & "'"
        Set MyRecSet = MyConn.Execute(sqlscript)
        prevtaskETIM = Format(Left(MyRecSet.Fields.Item("etim").Value, 4), "##:##")
        prevtaskSTIM = Format(Left(MyRecSet.Fields.Item("stim").Value, 4), "##:##")
        Set MyRecSet = Nothing
        Set MyConn = Nothing
        'possibly use the propertime() function above instead, but i haven't noticed any probs
    End If
    strResult = "T" & num & " started at " & prevtaskSTIM & " and failed, but was not run in a retry"
    Unload Me
    frmPollingFailures.Show
    frmPollingFailures.endit (strResult)

End Sub

Private Sub lstFailures_Click()

    but4.Enabled = True
    but5.Enabled = True
    but7.Enabled = True

End Sub
