VERSION 5.00
Begin VB.Form frmComments 
   Caption         =   "Additional Information"
   ClientHeight    =   1905
   ClientLeft      =   4275
   ClientTop       =   4920
   ClientWidth     =   7230
   ControlBox      =   0   'False
   Icon            =   "frmComments.frx":0000
   LinkTopic       =   "Form3"
   ScaleHeight     =   1905
   ScaleWidth      =   7230
   Begin VB.CommandButton Command2 
      Caption         =   "Skip"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3840
      TabIndex        =   7
      Top             =   1560
      Width           =   1575
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Continue"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5520
      TabIndex        =   6
      Top             =   1560
      Width           =   1575
   End
   Begin VB.Frame Frame1 
      Caption         =   "Additional Information"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   6975
      Begin VB.TextBox Text2 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3720
         TabIndex        =   5
         Top             =   910
         Width           =   3135
      End
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1200
         TabIndex        =   4
         Top             =   570
         Width           =   5655
      End
      Begin VB.Label Label3 
         Caption         =   "Currently being investigated on CTS ref:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   960
         Width           =   3615
      End
      Begin VB.Label Label2 
         Caption         =   "Comments:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Would you like to add any additional information to this store details?"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   6135
      End
   End
End
Attribute VB_Name = "frmComments"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()

    frmPollingFailures.Visible = True
    If (Text1.Text <> "") Then frmPollingFailures.lstStoreData.AddItem "Comments: " & Text1.Text
    If (Text2.Text <> "") Then frmPollingFailures.lstStoreData.AddItem "Currently being investigated on CTS ref: " & Text2.Text
    Unload Me
    
End Sub

Private Sub Command2_Click()

Dim Result As Integer

    Result = MsgBox("Are you sure you would like to skip?", vbYesNo, "Confirm Skip")
    
    If Result = vbYes Then
        frmPollingFailures.Visible = True
        Unload Me
    End If

End Sub
