VERSION 5.00
Begin VB.Form frmAmend 
   Caption         =   "Amend Row: x"
   ClientHeight    =   855
   ClientLeft      =   4680
   ClientTop       =   5865
   ClientWidth     =   6600
   ControlBox      =   0   'False
   Icon            =   "frmAmend.frx":0000
   LinkTopic       =   "Form3"
   ScaleHeight     =   855
   ScaleWidth      =   6600
   Begin VB.Frame Frame1 
      Caption         =   "Original Text"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   6375
      Begin VB.CommandButton Command2 
         Caption         =   "Cancel"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5400
         TabIndex        =   3
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Change"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4440
         TabIndex        =   2
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox orig 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   1
         Text            =   "This is the original text"
         Top             =   240
         Width           =   4215
      End
   End
   Begin VB.Label rownum 
      BackStyle       =   0  'Transparent
      Height          =   255
      Left            =   6000
      TabIndex        =   4
      Top             =   0
      Visible         =   0   'False
      Width           =   375
   End
End
Attribute VB_Name = "frmAmend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()

    Call AmendNow(orig.Text, rownum)
    Unload Me

End Sub

Private Sub Command2_Click()

    Unload Me

End Sub
