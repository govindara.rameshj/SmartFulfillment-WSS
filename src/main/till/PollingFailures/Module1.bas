Attribute VB_Name = "modMain"
Option Explicit

Function UpdateStatus(newstatus As String)

    frmPollingFailures.lblStatus = newstatus
    frmPollingFailures.Refresh
    DoEvents

End Function

Function propertime(time As String) As String

Dim aryTime() As String
Dim strReturn1 As String
Dim strReturn2 As String

    aryTime = Split(time, ":")
    
    If (Len(aryTime(0)) = 2) Then strReturn1 = aryTime(0)
    If (Len(aryTime(0)) = 1) Then strReturn1 = "0" & aryTime(0)
    If (Len(aryTime(0)) = 0) Then strReturn1 = "00"
    
    If (Len(aryTime(1)) = 2) Then strReturn2 = aryTime(1)
    If (Len(aryTime(1)) = 1) Then strReturn2 = "0" & aryTime(1)
    If (Len(aryTime(1)) = 0) Then strReturn2 = "00"
    
    propertime = strReturn1 & ":" & strReturn2

End Function

Function AmendRow(row As String)

    frmAmend.Caption = "Amend Row : " & row
    frmAmend.orig.Text = frmPollingFailures.lstStoreData.Text
    frmAmend.rownum = row
    frmAmend.Show

End Function

Function AmendNow(newtext As String, row As String)

    frmPollingFailures.lstStoreData.RemoveItem (row)
    frmPollingFailures.lstStoreData.AddItem newtext, row

End Function
