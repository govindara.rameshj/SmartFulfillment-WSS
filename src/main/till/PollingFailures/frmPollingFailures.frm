VERSION 5.00
Object = "{C8530F8A-C19C-11D2-99D6-9419F37DBB29}#1.1#0"; "ccrpprg6.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmPollingFailures 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Wickes Polling Failures"
   ClientHeight    =   4455
   ClientLeft      =   3450
   ClientTop       =   3855
   ClientWidth     =   8430
   Icon            =   "frmPollingFailures.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4455
   ScaleWidth      =   8430
   Begin VB.Frame strcontrol 
      Caption         =   "Store Control"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3135
      Left            =   5880
      TabIndex        =   4
      Top             =   120
      Width           =   2415
      Begin VB.TextBox txtStore 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   480
         TabIndex        =   7
         Top             =   720
         Width           =   1455
      End
      Begin VB.CommandButton cmdStart 
         Caption         =   "Go"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   720
         TabIndex        =   6
         Top             =   1920
         Width           =   975
      End
      Begin VB.TextBox txtDate 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   480
         TabIndex        =   5
         Text            =   "Date Here"
         Top             =   1440
         Width           =   1455
      End
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Enter the store number (wix***) and confirm polling date (yesterday) then click Go."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   855
         Left            =   120
         TabIndex        =   13
         Top             =   2280
         Width           =   2175
      End
      Begin VB.Line Line1 
         X1              =   240
         X2              =   2160
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Enter Store Number"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   360
         Width           =   1935
      End
      Begin VB.Line Line2 
         X1              =   240
         X2              =   2160
         Y1              =   1320
         Y2              =   1320
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Confirm Polling Date"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   1080
         Width           =   1935
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Results"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3135
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   5655
      Begin VB.ListBox lstStoreData 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2790
         ItemData        =   "frmPollingFailures.frx":058A
         Left            =   120
         List            =   "frmPollingFailures.frx":058C
         TabIndex        =   3
         Top             =   240
         Width           =   5415
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   120
      TabIndex        =   0
      Top             =   3360
      Width           =   5655
      Begin CCRProgressBar6.ccrpProgressBar prog 
         Height          =   255
         Left            =   120
         Top             =   600
         Width           =   5415
         _ExtentX        =   9551
         _ExtentY        =   450
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblStatus 
         Alignment       =   2  'Center
         Caption         =   "...Waiting..."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   5415
      End
   End
   Begin VB.Frame resultoptions 
      Caption         =   "Result Options"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   5880
      TabIndex        =   10
      Top             =   3360
      Visible         =   0   'False
      Width           =   2415
      Begin MSComDlg.CommonDialog cdialog 
         Left            =   0
         Top             =   840
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Clear Results"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   600
         Width           =   2175
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Save && Clear Results"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   2175
      End
   End
   Begin VB.Label version 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "v1.0.0"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   20.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6120
      TabIndex        =   14
      Top             =   3600
      Width           =   1815
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuConvert 
         Caption         =   "Failure Conversion"
      End
      Begin VB.Menu brk 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
      End
   End
End
Attribute VB_Name = "frmPollingFailures"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'*
'*$Archive: /Projects/OasysHW/VB/PollingFailures/frmPollingFailures.frm $
'**********************************************************************************************
'* $Author: Jeremy Janisch $ $Date: 02/11/05 10:08 $ $Revision: 0 $
'* $Versions:1.0.00$
'*
'**********************************************************************************************
'* Summary:
'*
'**********************************************************************************************
'* Versions:
'*
'* 01/12/05 DaveF   v1.0.1  General coding tidy.
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*
'**********************************************************************************************
'</CAMH>***************************************************************************************

Option Explicit

Private Sub cmdStart_Click()

Dim MyConn    As ADODB.Connection
Dim sqlscript As String
Dim MyRecSet  As ADODB.Recordset
Dim strTODT   As String
Dim strTMDT   As String
Dim strNite   As String
Dim strRETY   As String
Dim strSTSK   As String
Dim strSTIM   As String
Dim strETIM   As String
Dim strdesc   As String
    
    On Error GoTo cmdStart_Click_Error
    
    txtStore.Text = Replace(txtStore.Text, "wix", "")
    txtStore.Text = Replace(txtStore.Text, "wickes", "")
    
    txtStore.Text = "wix" & txtStore.Text
    
    resultoptions.Visible = False
    strcontrol.Visible = False
    
    If (lstStoreData.ListCount > 5) Then
        lstStoreData.AddItem ""
        lstStoreData.AddItem ""
    End If
    
    prog.Max = 10
    
    UpdateStatus ("Connecting...")
    Set MyConn = New ADODB.Connection
    
    On Error Resume Next
    MyConn.Open txtStore.Text
    If (MyConn.State = adStateClosed) Then
        Call MsgBox("The DSN - " & txtStore.Text & " - could not be found." & vbCrLf & vbCrLf & _
                    "Please check this problem.", vbInformation + vbOKOnly, "DSN Not Found")
        prog.Value = 0
        UpdateStatus ("Error Connecting!")
        strcontrol.Visible = True
        Set MyConn = Nothing
        Exit Sub
    End If
    prog.Value = 1
    
    If (MyConn.State = adStateOpen) Then
    
        UpdateStatus ("Pulling Data: Checking SYSDAT Details...")
        sqlscript = "SELECT * FROM SYSDAT"
        Set MyRecSet = MyConn.Execute(sqlscript)
        strTODT = MyRecSet.Fields.Item("todt").Value
        strTMDT = MyRecSet.Fields.Item("tmdt").Value
        strNite = MyRecSet.Fields.Item("nite").Value
        strRETY = MyRecSet.Fields.Item("rety").Value
        
        frmSelection.sysdets = "TODT: " & strTODT & " - TMDT: " & strTMDT & " - NITE: " & strNite & " - RETY: " & strRETY
        
        MyConn.Close
                
        UpdateStatus ("Pulling Initial NITLOG Data")
        prog.Value = 2
        
        MyConn.Open txtStore.Text
        If MyConn.State = 1 Then
            sqlscript = "SELECT * FROM NITLOG WHERE DATE1 = '" & Format(txtDate.Text, "yyyy-mm-dd") & "'"
            Set MyRecSet = MyConn.Execute(sqlscript)
            
            While Not MyRecSet.EOF
                strSTSK = MyRecSet.Fields.Item("task").Value
                
                strSTIM = propertime(Format(Left(MyRecSet.Fields.Item("stim").Value, 4), "##:##"))
                strETIM = propertime(Format(Left(MyRecSet.Fields.Item("etim").Value, 4), "##:##"))
                
                strdesc = MyRecSet.Fields.Item("prog").Value
                
                frmSelection.lstFailures.AddItem "T" & strSTSK & " | " & strSTIM & " | " & strETIM & " | " & strdesc
                
                DoEvents
                frmPollingFailures.Refresh
                
                MyRecSet.MoveNext
            Wend
        End If
        
        frmPollingFailures.Hide
        MyConn.Close
        frmSelection.Show
    
    End If
    
    Set MyRecSet = Nothing
    Set MyConn = Nothing
    
    Exit Sub

cmdStart_Click_Error:
        
    Set MyRecSet = Nothing
    Set MyConn = Nothing

End Sub

Private Sub Command2_Click()

Dim filename      As String
Dim intFileNumber As Integer

    cdialog.Filter = "Text files (*.txt)|*.txt|All Files (*.*)|*.*"
    cdialog.ShowSave
    prog.Value = 0
    filename = cdialog.filename
    
    intFileNumber = FreeFile
    
    Open filename For Output As intFileNumber
    Print #intFileNumber, "Results generated using Wickes Polling Failures v" & App.Major & "." & App.Minor & "." & App.Revision & " by JJ at " & time & " on " & Date
    Print #intFileNumber, ""
    Print #intFileNumber, ""
    While (lstStoreData.ListCount > "0")
        Print #intFileNumber, lstStoreData.List(0)
        lstStoreData.RemoveItem (0)
    Wend
    Close #intFileNumber
    
    lstStoreData.Clear
    strcontrol.Visible = True
    prog.Value = 0
    resultoptions.Visible = False
    UpdateStatus ("Saved results to " & cdialog.FileTitle)
    txtStore.SetFocus
    cmdStart.Default = True

End Sub

Private Sub Command3_Click()
    
    lstStoreData.Clear
    strcontrol.Visible = True
    prog.Value = 0
    UpdateStatus ("Cleared results")
    resultoptions.Visible = False
    txtStore.TabIndex = 1
    cmdStart.Default = True

End Sub

Private Sub Form_Load()

    version.Caption = "v" & App.Major & "." & App.Minor & "." & App.Revision
    txtStore.TabIndex = 1
    cmdStart.Default = True
    
    txtDate.Text = Format(DateAdd("d", Date, -1), "YYYY-MM-DD")

End Sub

Function endit(strresult As String)

Dim MyConn    As ADODB.Connection
Dim sqlscript As String
Dim MyRecSet  As ADODB.Recordset
Dim strSTSK   As String
Dim strSDAT   As String
Dim strSTIM   As String
Dim strETSK   As String
Dim strEDAT   As String
Dim strETIM   As String
Dim strSTHOFT As String
Dim strSTHOFD As String
Dim strSTHPOT As String
Dim strSTHPOD As String
Dim strSTHOAT As String
Dim strSTHOAD As String
Dim strTODT   As String
Dim strTMDT   As String
Dim strNite   As String
Dim strRETY   As String
Dim strStatus As String
Dim strCompleted As String

    On Error Resume Next
    
    Set MyConn = New ADODB.Connection
    
    MyConn.Open txtStore.Text
    
    UpdateStatus ("Pulling Data: Checking Start Time...")
    sqlscript = "SELECT * FROM NITLOG WHERE DATE1 = '" & txtDate.Text & "' AND TASK = '001'"
    Set MyRecSet = MyConn.Execute(sqlscript)
    If Not (MyRecSet.EOF) Then
        strSTSK = MyRecSet.Fields.Item("task").Value
        strSTIM = MyRecSet.Fields.Item("stim").Value
        strSDAT = MyRecSet.Fields.Item("sdat").Value
        strSTIM = Mid(strSTIM, 1, 2) & ":" & Mid(strSTIM, 3, 2)
    End If
    prog.Value = 3
    
    UpdateStatus ("Pulling Data: Checking End Time...")
    sqlscript = "SELECT * FROM NITLOG WHERE DATE1 = '" & txtDate.Text & "' order by task desc"
    Set MyRecSet = MyConn.Execute(sqlscript)
    If Not (MyRecSet.EOF) Then
        strETSK = MyRecSet.Fields.Item("task").Value
        strETIM = MyRecSet.Fields.Item("etim").Value
        strEDAT = MyRecSet.Fields.Item("edat").Value
        strETIM = Mid(strETIM, 1, 2) & ":" & Mid(strETIM, 3, 2)
    End If
    prog.Value = 4
    
    UpdateStatus ("Pulling Data: Checking STHOF Time...")
    sqlscript = "SELECT * FROM NITLOG WHERE DATE1 = '" & txtDate.Text & "' AND TASK = '032'"
    Set MyRecSet = MyConn.Execute(sqlscript)
    If Not (MyRecSet.EOF) Then
        strSTHOFT = MyRecSet.Fields.Item("etim").Value
        strSTHOFD = MyRecSet.Fields.Item("edat").Value
        strSTHOFT = Mid(strSTHOFT, 1, 2) & ":" & Mid(strSTHOFT, 3, 2)
    End If
    prog.Value = 5
    
    UpdateStatus ("Pulling Data: Checking STHPO Time...")
    sqlscript = "SELECT * FROM NITLOG WHERE DATE1 = '" & txtDate.Text & "' AND TASK = '204'"
    Set MyRecSet = MyConn.Execute(sqlscript)
    If Not (MyRecSet.EOF) Then
        strSTHPOT = MyRecSet.Fields.Item("etim").Value
        strSTHPOD = MyRecSet.Fields.Item("edat").Value
        strSTHPOT = Mid(strSTHPOT, 1, 2) & ":" & Mid(strSTHPOT, 3, 2)
    End If
    prog.Value = 6
    
    UpdateStatus ("Pulling Data: Checking STHOA Time...")
    sqlscript = "SELECT * FROM NITLOG WHERE DATE1 = '" & txtDate.Text & "' AND TASK = '257'"
    Set MyRecSet = MyConn.Execute(sqlscript)
    If Not (MyRecSet.EOF) Then
        strSTHOAT = MyRecSet.Fields.Item("etim").Value
        strSTHOAD = MyRecSet.Fields.Item("edat").Value
        strSTHOAT = Mid(strSTHOAT, 1, 2) & ":" & Mid(strSTHOAT, 3, 2)
    End If
    prog.Value = 7
    
    UpdateStatus ("Pulling Data: Checking STHOA Time...")
    sqlscript = "SELECT * FROM NITLOG WHERE DATE1 = '" & txtDate.Text & "' AND TASK = '257'"
    Set MyRecSet = MyConn.Execute(sqlscript)
    If Not (MyRecSet.EOF) Then
        strSTHOAT = MyRecSet.Fields.Item("etim").Value
        strSTHOAD = MyRecSet.Fields.Item("edat").Value
        strSTHOAT = Mid(strSTHOAT, 1, 2) & ":" & Mid(strSTHOAT, 3, 2)
    End If
    prog.Value = 8
    
    UpdateStatus ("Pulling Data: Checking SYSDAT Details...")
    sqlscript = "SELECT * FROM SYSDAT"
    Set MyRecSet = MyConn.Execute(sqlscript)
    If Not (MyRecSet.EOF) Then
        strTODT = MyRecSet.Fields.Item("todt").Value
        strTMDT = MyRecSet.Fields.Item("tmdt").Value
        strNite = MyRecSet.Fields.Item("nite").Value
        strRETY = MyRecSet.Fields.Item("rety").Value
    End If
    prog.Value = 9
    
    MyConn.Close
    
    If (strNite = "000") Then strStatus = "Complete" Else strStatus = "Not complete"
    If (strSTHOFT = "") Then strSTHOFT = "N/A"
    If (strSTHPOT = "") Then strSTHPOT = "N/A"
    If (strSTHOAT = "") Then strSTHOAT = "N/A"
    If (strRETY = "False") Then strRETY = "N" Else strRETY = "Y"
    
    If (strNite = "000") Then strCompleted = "(T" & strETSK & ") " & strETIM & " - " & strEDAT Else strCompleted = "N/A"
    
    If (strSTHOFD = "") Then
        strSTHOFT = "N/A"
        strSTHOFD = "N/A"
    End If
    If (strSTHOAD = "") Then
        strSTHOAT = "N/A"
        strSTHOAD = "N/A"
    End If
    
    If (strSTHPOD = "") Then
        strSTHPOT = "N/A"
        strSTHPOD = "N/A"
    End If
    
    lstStoreData.AddItem "***" & UCase(txtStore.Text) & "*** - Night Status: " & strStatus
    lstStoreData.AddItem "Store Closed at: (T" & strSTSK & ") " & strSTIM & " - " & strSDAT
    lstStoreData.AddItem "Night Routine Failed: " & strresult
    lstStoreData.AddItem "Night Routine Completed: " & strCompleted
    lstStoreData.AddItem "-----TVC DETAILS:-----"
    lstStoreData.AddItem "STHOF : " & strSTHOFT & " - " & strSTHOFD
    lstStoreData.AddItem "STHPO : " & strSTHPOT & " - " & strSTHPOD
    lstStoreData.AddItem "STHOA : " & strSTHOAT & " - " & strSTHOAD
    lstStoreData.AddItem "-----SYSDAT DETAILS:-----"
    lstStoreData.AddItem "TODT : " & strTODT
    lstStoreData.AddItem "TMDT : " & strTMDT
    lstStoreData.AddItem "NITE : " & strNite
    lstStoreData.AddItem "RETY : " & strRETY
    prog.Value = 10
    
    UpdateStatus ("Complete!")
    
    frmPollingFailures.Visible = False
    frmComments.Show
    
    resultoptions.Visible = True
    strcontrol.Visible = True

    Set MyRecSet = Nothing
    Set MyConn = Nothing

End Function

Private Sub lstStoreData_DblClick()

    AmendRow (lstStoreData.ListIndex)

End Sub

Private Sub mnuConvert_Click()

    frmStoreNums.Show

End Sub

Private Sub mnuExit_Click()

    End

End Sub
