VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmPIMProductUpdate 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "PIM Product Update"
   ClientHeight    =   3555
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6960
   Icon            =   "frmPIMProductUpdate.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3555
   ScaleWidth      =   6960
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ProgressBar prgProgress 
      Height          =   375
      Left            =   840
      TabIndex        =   3
      Top             =   2280
      Visible         =   0   'False
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "F12 - &Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   3946
      TabIndex        =   1
      Top             =   1170
      Width           =   1575
   End
   Begin VB.CommandButton cmdRunUpdate 
      Caption         =   "F5 - &Update PIM Products"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   1426
      TabIndex        =   0
      Top             =   1170
      Width           =   1575
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   2
      Top             =   3180
      Width           =   6960
      _ExtentX        =   12277
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmPIMProductUpdate.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   4921
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "28-June-06"
            TextSave        =   "28-June-06"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "13:30"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "F10 - &Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   3946
      TabIndex        =   4
      Top             =   1170
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Shape Shape1 
      Height          =   2655
      Left            =   473
      Shape           =   4  'Rounded Rectangle
      Top             =   270
      Width           =   6015
   End
End
Attribute VB_Name = "frmPIMProductUpdate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'*
'*$Archive: /Projects/OasysHW/VB/PIM Product Update/frmPIMProductUpdate.frm $
'**********************************************************************************************
'* $Author: Davidf $ $Date: 28/06/06 10:08 $ $Revision: 0 $
'* $Versions:1.0.00$
'*
'**********************************************************************************************
'* Summary: Application called by overnight process to scan the PIM image folder and update
'*                      the table PIMMAS with details for SKU images found.
'*
'**********************************************************************************************
'* Versions:
'*
'* 28/06/06 DaveF   v1.0.0  Initial build. WIX1156.
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*
'*  (/P='AUTO')
'*
'*  P = "AUTO" = Run by Over night process, "" = manual processing.
'*
'**********************************************************************************************
'</CAMH>***************************************************************************************

Option Explicit

Private Const MODULE_NAME  As String = "frmPIMProductUpdate"

Private Const PROCESSING_TYPE_AUTO As String = "AUTO"

Private Const THUMBNAIL_IMAGE_FILE_EXTENSION As String = ".jpg"
Private Const LARGE_IMAGE_FILE_EXTENSION As String = "L.jpg"

Private mcnnDBConnection As ADODB.Connection

' Path to images.
Private mstrPIMImagePath As String

' Mode of processing, over night or manual.
Private Enum ProcessingMode
    ptAuto = 1
    ptManual = 2
End Enum
Dim mblnFromNightlyClose      As Boolean

Private mblnIsUpdatingData As Boolean
Private mblnCancelUpdate   As Boolean

Private Sub Form_Load()

Const PROCEDURE_NAME As String = "Form_Load"

    On Error GoTo Form_Load_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Started on " & Now())

'   Get the system setup.
    Call GetRoot
    
    Call DecodeParameters(goSession.ProgramParams)
    Call InitialiseStatusBar(sbStatus)

    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)

    ' Get if running as part of the over night process or manually.
'   Set up the database connection.
    Set mcnnDBConnection = goSession.Database.Connection
    If (mcnnDBConnection.State = adStateOpen) Then
        mcnnDBConnection.Close
    End If
    If (mcnnDBConnection Is Nothing) Then
        If (mblnFromNightlyClose = False) Then
            Call MsgBox("The Database connection to DSN - " & goSession.Database.Connection.DefaultDatabase & " - Failed")
        End If
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "The Database connection to DSN - " & goSession.Database.ConnectionString & " - Failed")
        Err.Description = "The Database connection to DSN - " & goSession.Database.ConnectionString & " - Failed"
        Call Err.Report(MODULE_NAME, PROCEDURE_NAME, 0, False)
        Err.Clear
        Unload Me
    End If
    
    ' Get the path to the PIM image folder.
    mstrPIMImagePath = goSession.GetParameter(PRM_PIM_ONLY_IMAGE_PATH)
    If (Right(mstrPIMImagePath, 1) <> "\") Then
        mstrPIMImagePath = mstrPIMImagePath & "\"
    End If
    
    ' Automatically start the processing if running as part of the over night process.
    If (mblnFromNightlyClose = True) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Processing Mode - Automatic")
        Call cmdRunUpdate_Click
    Else
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Processing Mode - Manual")
    End If
       
    Exit Sub
    
Form_Load_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - " & Err.Number & " " & Err.Description)
    If (mblnFromNightlyClose = False) Then
        Call MsgBoxEx(PROCEDURE_NAME & " Error - " & Err.Number & " " & Err.Description, vbCritical + vbOKOnly, _
                        PROCEDURE_NAME & " Error", , , , , RGBMsgBox_WarnColour)
    Else
        Call Err.Report(MODULE_NAME, PROCEDURE_NAME, 0, False)
        Err.Clear
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    Select Case KeyCode
        Case vbKeyF5
            KeyCode = 0
            Call cmdRunUpdate_Click         ' Process the update.
        Case vbKeyF10
            KeyCode = 0
            If (cmdCancel.Visible = True) Then
                Call cmdCancel_Click        ' Cancel the update.
            End If
        Case vbKeyF12
            KeyCode = 0
            If (cmdClose.Visible = True) Then
                Call cmdClose_Click         ' Close the form.
            End If
    End Select
    
End Sub

Private Sub Successful_End()
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Erase file so that close program will know that this program
' completed successfully
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If Dir(App.Path & "\TASKCOMP") <> "" Then
        Kill (App.Path & "\TASKCOMP")
    End If
End Sub

Private Sub DecodeParameters(strCommand As String)

Const CALLED_FROM As String = "CF"
Const CF_CLOSE    As String = "C"
Dim strTempDate As String
Dim dteEndDate  As Date
Dim strSection  As String
Dim vntSection  As Variant
Dim lngSectNo   As Long
Dim blnSetOpts  As Boolean
                    
    Call DebugMsg(MODULE_NAME, "Updating Nightly Routine", endlDebug, _
                    "Parameters Passed = " & strCommand)

    vntSection = Split(strCommand, ",")
    
    mblnFromNightlyClose = False
    
    If UBound(vntSection) = 0 Then Exit Sub
    
    For lngSectNo = 0 To UBound(vntSection) Step 1
        
        Debug.Print lngSectNo & "-" & vntSection(lngSectNo)
        strSection = vntSection(lngSectNo)
        
        Select Case (Right$(strSection, 1))
        Case CF_CLOSE
            Debug.Print Mid$(strSection, 4)
            If Mid(strSection, 3) = CF_CLOSE Then
                mblnFromNightlyClose = True
            End If
        End Select
       
    Next lngSectNo
    
End Sub


Private Sub Form_Unload(Cancel As Integer)

    If (mblnIsUpdatingData = True) Then
        Cancel = True
    Else
        If Not (mcnnDBConnection Is Nothing) Then
            If (mcnnDBConnection.State = adStateOpen) Then
                mcnnDBConnection.Close
            End If
            Set mcnnDBConnection = Nothing
        End If
    End If
    
End Sub

' Cancel the update.
Private Sub cmdCancel_Click()

    mblnCancelUpdate = True
    
End Sub


' Close the form.
Private Sub cmdClose_Click()

    Unload Me
    
End Sub

' Run the update.
Private Sub cmdRunUpdate_Click()

Const PROCEDURE_NAME As String = "cmdRunUpdate_Click"

Dim dbCommand         As ADODB.Command
Dim fsoFileSystem     As Scripting.FileSystemObject
Dim objFile           As Scripting.File
Dim strSQl            As String
Dim strSKU            As String
Dim lngCounter        As Long
Dim lngRecordsUpdated As Long
Dim lngRecordsAdded   As Long

    On Error GoTo cmdRunUpdate_Click_Error
    
    Screen.MousePointer = vbHourglass
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Started on " & Now())

    mblnIsUpdatingData = True
    mblnCancelUpdate = False
    cmdCancel.Visible = True
    cmdClose.Visible = False
    
    ' Test if the image folder exists.
    Set fsoFileSystem = New Scripting.FileSystemObject
    If (fsoFileSystem.FolderExists(mstrPIMImagePath) = True) Then
        ' Open the database connection.
        mcnnDBConnection.CursorLocation = adUseClient
        If (mcnnDBConnection.State = adStateClosed) Then
            mcnnDBConnection.Open
        End If
        ' Create and set-up the command object.
        Set dbCommand = New ADODB.Command
        dbCommand.ActiveConnection = mcnnDBConnection
        dbCommand.CommandType = adCmdText
        ' Start a database transaction.
        mcnnDBConnection.BeginTrans
        
        ' Delete any existing data from table PIMMAS.
        strSQl = "DELETE FROM PIMMAS"
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "SQL - " & strSQl)
        dbCommand.CommandText = strSQl
        dbCommand.Execute
        
        ' Show and set-up the progress bar.
        'Referral 2008-002 - Check if at least 1 file exists before setting Max
        If (fsoFileSystem.GetFolder(mstrPIMImagePath).Files.Count > 0) Then
            prgProgress.Max = fsoFileSystem.GetFolder(mstrPIMImagePath).Files.Count
        End If
        prgProgress.Value = 0
        prgProgress.Visible = True
        lngRecordsAdded = 0
        ' Loop over the thumbnail files in the folder and if the SKU exists in table STKMAS update table PIMMAS with
        '   the SKUN and hierarchy details.
        For Each objFile In fsoFileSystem.GetFolder(mstrPIMImagePath).Files
            prgProgress.Value = prgProgress + 1
            If (mblnCancelUpdate = True) Then
                Exit For
            End If
            DoEvents
            ' Test if a thumbnail image or a large image.
            If (InStr(objFile.Name, LARGE_IMAGE_FILE_EXTENSION) = 0) Then
                ' Get the SKU from the file.
                strSKU = Replace(objFile.Name, THUMBNAIL_IMAGE_FILE_EXTENSION, "")
                ' Add the details found in table STKMAS for the SKU to table PIMMAS.
                strSQl = "INSERT INTO PIMMAS " & _
                            "SELECT a.SKUN, a.CTGY AS CatNo, b.DESCR AS CatDesc, a.GRUP AS GrpNo, c.DESCR AS GrpDesc, " & _
                            "a.SGRP AS SGrpNo, d.DESCR AS SGRPDesc, a.STYL AS StylNo, e.DESCR AS StylDesc " & _
                            "FROM STKMAS a INNER JOIN HIECAT b ON a.CTGY = b.NUMB " & _
                            "INNER JOIN HIEGRP c ON a.CTGY = c.NUMB AND a.GRUP = c.GROU " & _
                            "INNER JOIN HIESGP d ON a.CTGY = c.NUMB AND a.GRUP = c.GROU AND a.SGRP = d.SGRP " & _
                            "INNER JOIN HIESTY e ON a.CTGY = c.NUMB AND a.GRUP = c.GROU AND a.SGRP = d.SGRP AND a.STYL = e.STYL " & _
                            "WHERE a.SKUN = '" & strSKU & "'"
                Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "SQL - " & strSQl)
                dbCommand.CommandText = strSQl
                dbCommand.Execute lngRecordsUpdated
                If (lngRecordsUpdated <> 0) Then
                    lngRecordsAdded = lngRecordsAdded + 1
                End If
            End If
        Next objFile
        If (mblnCancelUpdate = True) Then
            ' Rollback the database transaction.
            mcnnDBConnection.RollbackTrans
        Else
            ' Commit the database transaction.
            mcnnDBConnection.CommitTrans
        End If
    Else
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "PIM Image path - " & mstrPIMImagePath & " not found")
        Err.Description = "PIM Image path - " & mstrPIMImagePath & " not found"
        Call Err.Report(MODULE_NAME, PROCEDURE_NAME, 0, False)
        Err.Clear
    End If

    If (mcnnDBConnection.State = adStateOpen) Then
        mcnnDBConnection.Close
    End If

    Set objFile = Nothing
    Set fsoFileSystem = Nothing
    
    mblnIsUpdatingData = False
    prgProgress.Visible = False
    cmdCancel.Visible = False
    cmdClose.Visible = True
    
    Screen.MousePointer = vbNormal

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "The update has completed successfully")
    If (mblnFromNightlyClose = False) And (mblnCancelUpdate = False) Then
        Call MsgBoxEx("The update has completed successfully. " & lngRecordsAdded & " items included.", vbInformation + vbOKOnly, _
                        "Update Complete", , , , , RGBMSGBox_PromptColour)
    End If
    
    ' If running as part of the over night process then close the application after processing.
    If (mblnFromNightlyClose = True) Then
        Call Successful_End
        End
    End If
    
    Exit Sub
    
cmdRunUpdate_Click_Error:

    ' Rollback the database transaction.
    If Not (mcnnDBConnection Is Nothing) Then
        mcnnDBConnection.RollbackTrans
        If (mcnnDBConnection.State = adStateOpen) Then
            mcnnDBConnection.Close
        End If
    End If
    
    Set objFile = Nothing
    Set fsoFileSystem = Nothing
    
    mblnIsUpdatingData = False
    prgProgress.Visible = False
    cmdCancel.Visible = False
    cmdClose.Visible = True
    
    Screen.MousePointer = vbNormal
    
    Call Err.Report(MODULE_NAME, PROCEDURE_NAME, 0, False)
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - " & Err.Number & " " & Err.Description)
    If (mblnFromNightlyClose = False) Then
        Call MsgBoxEx(PROCEDURE_NAME & " Error - " & Err.Number & " " & Err.Description, vbCritical + vbOKOnly, _
                        PROCEDURE_NAME & " Error", , , , , RGBMsgBox_WarnColour)
    Else
        Err.Clear
        Unload Me
    End If
    
End Sub
