cd ..

cd "Activity Log Viewer"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjActivityLogViewer.vbp"
cd ..
Pause

cd "Bank Deposits"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjBankDeposits.vbp"
cd ..
Pause

cd "Change Password"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjChangePwd.vbp"
cd ..
Pause

cd "Change Refund Password"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "RefundPassword.vbp"
cd ..
Pause

cd "Clear Password"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjClearPwd.vbp"
cd ..
Pause

cd "Customer Order Reprint"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjCustOrderReprint.vbp"
cd ..
Pause

cd "Customer Order US"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjOrder.vbp"
cd ..
Pause

cd "Daily Banking Report"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjDailyBankReport.vbp"
cd ..
Pause

cd "Dept Editor"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjDeptEditor.vbp"
cd ..
Pause

cd "Despatch Methods Editor"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "DespatchMethodsEditor.vbp"
cd ..
Pause

cd "Display Items Viewer"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjDispItemsViewer.vbp"
cd ..
Pause

cd "End Of Day Report"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjEndOfDay.vbp"
cd ..
Pause

cd "IST Trans Viewer"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjISTTransViewer.vbp"
cd ..
Pause

cd "Item Editor"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjItem.vbp"
cd ..
Pause

cd "Item Enquiry"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjItemEnquiry.vbp"
cd ..
Pause

cd "Line Comments Editor"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "Line Comments Editor.vbp"
cd ..
Pause

cd "Maint Display Items"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjMaintDispItems.vbp"
cd ..
Pause

cd "Maint Night Master"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjMaintNightMaster.vbp"
cd ..
Pause

cd "Menu"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjMenu.vbp"
cd ..
Pause

cd "Menu Editor"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjMenuEditor.vbp"
cd ..
Pause

cd "Night Log Viewer"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjNightLogViewer.vbp"
cd ..
Pause

cd "Night Master Editor"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjNightMasterEditor.vbp"
cd ..
Pause

cd "Orders"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjOrder.vbp"
cd ..
Pause

cd "Parameter Editor"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjParamEditor.vbp"
cd ..
Pause

cd "Payments"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjPayments.vbp"
cd ..
Pause

cd "Price Change Viewer"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjPriceChangeViewer.vbp"
cd ..
Pause

cd "Purchase Order Enquiry"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjPOEnquiry.vbp"
cd ..
Pause

cd "Receipts Viewer"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjReceiptsViewer.vbp"
cd ..
Pause

cd "Refunds Report"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjRefundReport.vbp"
cd ..
Pause

cd "Retailer Options"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "RetailerOptions.vbp"
cd ..
Pause

cd "Sales Order Viewer"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjSalesOrderViewer.vbp"
cd ..
Pause

cd "Shipments Viewer"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjShipmentsViewer.vbp"
cd ..
Pause

cd "Stock Adj Reasons Editor"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjAdjReasonEditor.vbp"
cd ..
Pause

cd "Stock Adjust"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjStockAdjust.vbp"
cd ..
Pause

cd "Stock Adjustment Code Editor"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjStockAdjCodeEditor.vbp"
cd ..
Pause

cd "Stock Level Report"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjStockLevelViewer.vbp"
cd ..
Pause

cd "Stock Movement Enquiry"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjStMoveEnq.vbp"
cd ..
Pause

cd "Stock Valuation Report"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjStockValReport.vbp"
cd ..
Pause

cd "Stock Write-Off"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjStockWriteOff.vbp"
cd ..
Pause

cd "StockTakeUI"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "StockTakeUI.vbp"
cd ..
Pause

cd "Store Editor"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjMaintStore.vbp"
cd ..
Pause

cd "Store Open"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjStoreOpen.vbp"
cd ..
Pause

cd "Supplier Editor"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjSuppliers.vbp"
cd ..
Pause

cd "Supplier Returns Viewer"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjSupplierReturnsViewer.vbp"
cd ..
Pause

cd "Tender Types Editor"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjTenderTypeEditor.vbp"
cd ..
Pause

cd "Till"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjTillFunction.vbp"
cd ..
Pause

cd "Timesheet"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjTimesheet.vbp"
cd ..
Pause

cd "Trader Editor"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjMaintTrader.vbp"
cd ..
Pause

cd "User Editor"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjUserEditor.vbp"
cd ..
Pause

cd "Weekly Report"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjWeeklyReport.vbp"
cd ..
Pause

cd "Workstation Editor"
"C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE" /m "prjWSEditor.vbp"
cd ..
Pause

cd "Make Files"