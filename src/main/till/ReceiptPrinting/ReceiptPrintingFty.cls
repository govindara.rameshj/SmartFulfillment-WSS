VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ReceiptPrintingFty"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private moSession As Session

Private Const m_DropFSCFooterParameterID As Long = 980016
Private m_InitialisedUseDropFSCFooterImplementation As Boolean
Private m_UseDropFSCFooterImplementation As Boolean

Private Const m_GetCouponParameterID As Long = 983352
Private m_InitialisedUseGetCouponImplementation As Boolean
Private m_UseGetCouponImplementation As Boolean

Private Const m_DropSecondSignatureForRefundsParameterID As Long = 980097
Private m_InitialisedUseDropSecondSignatureForRefundsImplementation As Boolean
Private m_UseDropSecondSignatureForRefundsImplementation As Boolean

Friend Property Get UseDropFSCFooterImplementation() As Boolean
    
    UseDropFSCFooterImplementation = m_UseDropFSCFooterImplementation
End Property

Public Function FactoryGetDropFSCFooter() As IReceiptPrintFSC
    
    If UseDropFSCFooterImplementation Then
        'using implementation with change request 16 alterations
        Set FactoryGetDropFSCFooter = New ReceiptPrintFSC
    Else
        'using live implementation
        Set FactoryGetDropFSCFooter = New ReceiptPrintLive
    End If
End Function

Friend Property Get UseGetCouponImplementation() As Boolean
    
    UseGetCouponImplementation = m_UseGetCouponImplementation
End Property

Public Function FactoryGetGetCoupon() As IReceiptPrintGtCpn
    
    If UseGetCouponImplementation Then
        'using implementation with Get Coupon changes
        Set FactoryGetGetCoupon = New ReceiptPrintGetCpn
    Else
        'using live implementation
        Set FactoryGetGetCoupon = New ReceiptPrintLive
    End If
End Function

Friend Property Get UseDropSecondSignatureForRefundsImplementation() As Boolean
    
    UseDropSecondSignatureForRefundsImplementation = m_UseDropSecondSignatureForRefundsImplementation
End Property

Public Function FactoryDropSecondSignatureForRefunds() As IReceiptPrintSig
    
    If UseDropSecondSignatureForRefundsImplementation Then
        'using implementation with change request 16 alterations
        Set FactoryDropSecondSignatureForRefunds = New ReceiptPrintDrpSig
    Else
        'using live implementation
        Set FactoryDropSecondSignatureForRefunds = New ReceiptPrintLive
    End If
End Function

Public Sub Initialise(ByRef oSession As Session)

    Set moSession = oSession
    GetDropFSCFooterParameterValue
    GetGetCouponParameterValue
    GetDropSecondSignatureForRefundsParameterValue
End Sub

Friend Sub GetDropFSCFooterParameterValue()

    If Not m_InitialisedUseDropFSCFooterImplementation Then
On Error Resume Next
        m_UseDropFSCFooterImplementation = moSession.GetParameter(m_DropFSCFooterParameterID)
        m_InitialisedUseDropFSCFooterImplementation = True
On Error GoTo 0
    End If
End Sub

Friend Sub GetGetCouponParameterValue()

    If Not m_InitialisedUseGetCouponImplementation Then
On Error Resume Next
        m_UseGetCouponImplementation = moSession.GetParameter(m_GetCouponParameterID)
        m_InitialisedUseGetCouponImplementation = True
On Error GoTo 0
    End If
End Sub

Friend Sub GetDropSecondSignatureForRefundsParameterValue()

    If Not m_InitialisedUseDropSecondSignatureForRefundsImplementation Then
On Error Resume Next
        m_UseDropSecondSignatureForRefundsImplementation = moSession.GetParameter(m_DropSecondSignatureForRefundsParameterID)
        m_InitialisedUseDropSecondSignatureForRefundsImplementation = True
On Error GoTo 0
    End If
End Sub


