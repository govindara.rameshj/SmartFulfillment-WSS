VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ReceiptPrintLive"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IReceiptPrintFSC
Implements IReceiptPrintGtCpn
Implements IReceiptPrintSig
Private Function IReceiptPrintFSC_GetFSCTrailer(ByVal FSCTrailerPathFile As String) As String
    Dim fsoFSCTrailer   As New FileSystemObject
    Dim tsFSCTrailer    As TextStream

    Set tsFSCTrailer = fsoFSCTrailer.OpenTextFile(FSCTrailerPathFile, ForReading, False, TristateUseDefault)
    IReceiptPrintFSC_GetFSCTrailer = tsFSCTrailer.ReadAll
    Call tsFSCTrailer.Close
End Function

Private Function IReceiptPrintGtCpn_EnableGetCoupon() As Boolean

    IReceiptPrintGtCpn_EnableGetCoupon = False
End Function

Private Function IReceiptPrintSig_GetSecondSignature(ByVal signatureText As String) As String
    IReceiptPrintSig_GetSecondSignature = signatureText & vbNewLine & vbNewLine
End Function

Private Function IReceiptPrintSig_GetFirstSignature(ByVal signatureText As String) As String
    IReceiptPrintSig_GetFirstSignature = signatureText & vbNewLine & vbNewLine
End Function

