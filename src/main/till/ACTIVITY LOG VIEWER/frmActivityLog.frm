VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Object = "{9BA18739-054D-4172-8E42-118133CE2FC4}#1.0#0"; "EditDateCtl.ocx"
Begin VB.Form frmActivityLog 
   BackColor       =   &H00E0E0E0&
   Caption         =   "Activity Log Viewer"
   ClientHeight    =   6735
   ClientLeft      =   510
   ClientTop       =   1110
   ClientWidth     =   9630
   Icon            =   "frmActivityLog.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   6735
   ScaleWidth      =   9630
   WindowState     =   2  'Maximized
   Begin VB.Frame frCriteria 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Selection Criteria"
      Height          =   1095
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   7455
      Begin ucEditDate.ucDateText dtxtEndDate 
         Height          =   315
         Left            =   6120
         TabIndex        =   19
         Top             =   240
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DateFormat      =   "DD/MM/YY"
         Text            =   "05/10/12"
      End
      Begin ucEditDate.ucDateText dtxtStartDate 
         Height          =   315
         Left            =   4680
         TabIndex        =   18
         Top             =   240
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DateFormat      =   "DD/MM/YY"
         Text            =   "05/10/12"
      End
      Begin VB.ComboBox cmbDateCrit 
         Height          =   315
         ItemData        =   "frmActivityLog.frx":0A96
         Left            =   3720
         List            =   "frmActivityLog.frx":0AA9
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   240
         Width           =   735
      End
      Begin VB.ComboBox cmbFunction 
         Height          =   315
         ItemData        =   "frmActivityLog.frx":0AC3
         Left            =   3720
         List            =   "frmActivityLog.frx":0AD3
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   600
         Width           =   1815
      End
      Begin VB.ComboBox cmbWorkStation 
         Height          =   315
         Left            =   1200
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   600
         Width           =   1695
      End
      Begin VB.ComboBox cmbUserID 
         Height          =   315
         Left            =   720
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   240
         Width           =   2175
      End
      Begin VB.CommandButton cmdApply 
         Caption         =   "F7-Search"
         Height          =   375
         Left            =   6240
         TabIndex        =   4
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label5 
         BackColor       =   &H00E0E0E0&
         Caption         =   "to"
         Height          =   255
         Left            =   5880
         TabIndex        =   11
         Top             =   240
         Width           =   255
      End
      Begin VB.Label Label4 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Period"
         Height          =   255
         Left            =   3000
         TabIndex        =   10
         Top             =   240
         Width           =   495
      End
      Begin VB.Label Label3 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Function"
         Height          =   255
         Left            =   3000
         TabIndex        =   13
         Top             =   600
         Width           =   735
      End
      Begin VB.Label Label2 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Work Station"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackColor       =   &H00E0E0E0&
         Caption         =   "&User"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   495
      End
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10-Exit"
      Height          =   375
      Left            =   120
      TabIndex        =   7
      Top             =   5880
      Width           =   975
   End
   Begin CTSProgBar.ucpbProgressBar ucProgress 
      Height          =   1815
      Left            =   960
      TabIndex        =   14
      Top             =   1680
      Width           =   5055
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      FillColor       =   16744703
      Object.Width           =   5055
      Object.Height          =   1815
   End
   Begin FPSpreadADO.fpSpread sprdEdit 
      Height          =   4455
      Left            =   120
      TabIndex        =   5
      Top             =   1320
      Width           =   9375
      _Version        =   458752
      _ExtentX        =   16536
      _ExtentY        =   7858
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   11
      MaxRows         =   1
      RowHeaderDisplay=   0
      SpreadDesigner  =   "frmActivityLog.frx":0B10
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   15
      Top             =   6360
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmActivityLog.frx":1E11
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9816
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "16:13"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F9-Print"
      Height          =   375
      Left            =   8520
      TabIndex        =   6
      Top             =   5880
      Width           =   975
   End
   Begin VB.Label lblNoMatches 
      Alignment       =   1  'Right Justify
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "N/A"
      Height          =   255
      Left            =   2760
      TabIndex        =   17
      Top             =   5880
      Width           =   1215
   End
   Begin VB.Label lblNoMatcheslbl 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Number of Matches"
      Height          =   255
      Left            =   1200
      TabIndex        =   16
      Top             =   5880
      Width           =   1455
   End
End
Attribute VB_Name = "frmActivityLog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'<CAMH>****************************************************************************************
'* Module :
'* Date   : 23/01/03
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Activity Log Viewer/frmActivityLog.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Damians $ $Date: 7/06/04 10:02 $ $Revision: 6 $
'* Versions:
'* 19/09/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmActivityLog"

Private Const COL_KEY As Long = 1
Private Const COL_STDATE As Long = 2
Private Const COL_ENDDATE As Long = 3
Private Const COL_USERID As Long = 4
Private Const COL_WSID As Long = 5
Private Const COL_GROUP As Long = 6
Private Const COL_PROGID As Long = 7
Private Const COL_LOGIN As Long = 8
Private Const COL_LOGOUT As Long = 9
Private Const COL_FORCELOG  As Long = 10

Private colUserNames As Collection
Private colWSNames As Collection

Private Const CRIT_LESSTHAN As Long = 2
Private Const CRIT_GREATERTHAN As Long = 1
Private Const CRIT_EQUALS As Long = 0
Private Const CRIT_ALL As Long = 3
Private Const CRIT_FROM As Long = 4

Const BORDER_WIDTH As Long = 60



Private Sub cmbDateCrit_Click()

    Select Case (cmbDateCrit.ItemData(cmbDateCrit.ListIndex))
        Case (CRIT_LESSTHAN), (CRIT_GREATERTHAN), (CRIT_EQUALS):
                    dtxtStartDate.Enabled = True
                    dtxtEndDate.Enabled = False
'                    dtxtStartDate.SetFocus
        Case (CRIT_ALL):
                    dtxtStartDate.Enabled = False
                    dtxtEndDate.Enabled = False
        Case (CRIT_FROM):
                    dtxtStartDate.Enabled = True
                    dtxtEndDate.Enabled = True
'                    dtxtStartDate.SetFocus
    End Select

End Sub

Private Sub cmbDateCrit_GotFocus()

    Call SendKeys(KEY_DROPDOWN)

End Sub

Private Sub cmbDateCrit_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If

End Sub

Private Sub cmbFunction_GotFocus()

    Call SendKeys(KEY_DROPDOWN)

End Sub

Private Sub cmbFunction_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If

End Sub

Private Sub cmbUserID_GotFocus()

    Call SendKeys(KEY_DROPDOWN)

End Sub

Private Sub cmbUserID_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If

End Sub

Private Sub cmbUserID_LostFocus()
    'if they haven't selected anything
    If cmbUserID.ListIndex = -1 Then
        'Set the combo box to all
        cmbUserID.ListIndex = cmbUserID.ListCount - 1
    End If
End Sub

Private Sub cmbWorkStation_GotFocus()

    Call SendKeys(KEY_DROPDOWN)

End Sub

Private Sub cmbWorkStation_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If

End Sub

Private Sub cmdApply_Click()

Const FORCE_OUT As Long = 1
Const LOG_IN As Long = 2
Const LOG_OUT As Long = 3
Const PROGRAMS As Long = 4

Dim lEntryNo    As Long
Dim oEntry      As cActivityLog
Dim colEntries  As Collection
Dim strValue    As String
Dim oMenuItem   As cMenuItem

On Error GoTo DisplayError
    
    Screen.MousePointer = vbHourglass
    ucProgress.Value = 0
    ucProgress.Caption1 = "Loading List from Database"
    ucProgress.Visible = True
    lblNoMatches.Caption = "Searching"
    DoEvents
    
    'Create single element that List must be retrieved for
    Set oEntry = goDatabase.CreateBusinessObject(CLASSID_ACTIVITYLOG)
    
    'Set User ID criteria if selected by user
    If cmbUserID.ItemData(cmbUserID.ListIndex) <> -1 Then
        strValue = Left$(cmbUserID.Text, InStr(cmbUserID.Text, " - ") - 1)
        Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_ACTIVITYLOG_EmployeeID, strValue)
    End If
    
    'Set Work Station criteria if selected by user
    If cmbWorkStation.ItemData(cmbWorkStation.ListIndex) <> -1 Then
        strValue = Left$(cmbWorkStation.Text, InStr(cmbWorkStation.Text, " - ") - 1)
        Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_ACTIVITYLOG_WorkstationID, strValue)
    End If
    
    'Set Date criteria if selected by user
    Select Case (cmbDateCrit.ItemData(cmbDateCrit.ListIndex))
        Case (CRIT_LESSTHAN):    Call oEntry.IBo_AddLoadFilter(CMP_LESSEQUALTHAN, FID_ACTIVITYLOG_SystemDate, GetDate(dtxtStartDate.Text))
        Case (CRIT_GREATERTHAN): Call oEntry.IBo_AddLoadFilter(CMP_GREATEREQUALTHAN, FID_ACTIVITYLOG_SystemDate, GetDate(dtxtStartDate.Text))
        Case (CRIT_EQUALS):      Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_ACTIVITYLOG_SystemDate, GetDate(dtxtStartDate.Text))
        Case (CRIT_FROM):        Call oEntry.IBo_AddLoadFilter(CMP_GREATEREQUALTHAN, FID_ACTIVITYLOG_SystemDate, GetDate(dtxtStartDate.Text))
                                 Call oEntry.IBo_AddLoadFilter(CMP_LESSEQUALTHAN, FID_ACTIVITYLOG_SystemDate, GetDate(dtxtEndDate.Text))
    End Select
    
    'Set Activity type criteria if selected by user
    If cmbFunction.ItemData(cmbFunction.ListIndex) <> 0 Then
        Select Case (cmbFunction.ItemData(cmbFunction.ListIndex))
            Case (FORCE_OUT):
                        Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_ACTIVITYLOG_GroupID, "00")
                        Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_ACTIVITYLOG_ProgramID, "00")
                        Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_ACTIVITYLOG_AutoLoggedOut, True)
            Case (LOG_IN):
                        Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_ACTIVITYLOG_GroupID, "00")
                        Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_ACTIVITYLOG_ProgramID, "00")
                        Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_ACTIVITYLOG_LoggingIn, True)
            Case (LOG_OUT):
                        Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_ACTIVITYLOG_GroupID, "00")
                        Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_ACTIVITYLOG_ProgramID, "00")
                        Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_ACTIVITYLOG_LoggingIn, False)
            Case (PROGRAMS):
                        Call oEntry.IBo_AddLoadFilter(CMP_NOTEQUAL, FID_ACTIVITYLOG_GroupID, "00")
        End Select
    End If
    
    'Make call on entries to return collection of alll entries
    Set colEntries = oEntry.IBo_LoadMatches

    ucProgress.Caption1 = "Populating grid"
    ucProgress.Value = 1
    sprdEdit.MaxRows = 0
    If colEntries.Count > 0 Then
        ucProgress.Max = colEntries.Count
        sbStatus.Panels(PANEL_INFO).Text = "Populating grid"
        DoEvents
        For lEntryNo = 1 To colEntries.Count Step 1
            Set oEntry = colEntries(lEntryNo)
            sprdEdit.MaxRows = sprdEdit.MaxRows + 1
            ucProgress.Value = sprdEdit.MaxRows
            sprdEdit.Row = sprdEdit.MaxRows
            sprdEdit.Col = COL_KEY
            sprdEdit.Text = oEntry.Key
            sprdEdit.Col = COL_STDATE
            sprdEdit.Text = DisplayDate(oEntry.SystemDate, False) & Format$(oEntry.StartTime, " 00:00:00")
            sprdEdit.Col = COL_ENDDATE
            If Year(oEntry.EndTime) = 1899 Then
                sprdEdit.BackColor = RGB_LTGREY
            Else
                sprdEdit.Text = DisplayDate(oEntry.EndDate, False) & Format$(oEntry.EndTime, " 00:00:00")
            End If
            
            On Error Resume Next 'ignore error if User or Workstation not found
            sprdEdit.Col = COL_USERID
            sprdEdit.Text = colUserNames(oEntry.EmployeeID)
            sprdEdit.Col = COL_WSID
            sprdEdit.Text = colWSNames(oEntry.WorkstationID)
            On Error GoTo DisplayError
            
            If oEntry.GroupID = "00" And oEntry.ProgramID = "00" Then
                If oEntry.LoggingIn = True Then
                    sprdEdit.Col = COL_LOGIN
                    sprdEdit.Value = 1
                Else
                    sprdEdit.Col = COL_LOGOUT
                    sprdEdit.Value = 1
                End If
                sprdEdit.Col = COL_GROUP
                sprdEdit.BackColor = RGB_LTGREY
                sprdEdit.Col = COL_PROGID
                sprdEdit.BackColor = RGB_LTGREY
                sprdEdit.Col = COL_FORCELOG
                sprdEdit.Value = oEntry.AutoLoggedOut
            Else
                Set oMenuItem = goDatabase.CreateBusinessObject(CLASSID_MENU)

                Call oMenuItem.IBo_AddLoadField(FID_MENU_TEXT)
                Call oMenuItem.IBo_AddLoadFilter(CMP_EQUAL, FID_MENU_ID, oEntry.GroupID)
                Call oMenuItem.IBo_Load
                
                sprdEdit.Col = COL_GROUP
                sprdEdit.Text = oMenuItem.MenuText
            
                oMenuItem.IBo_ClearLoadFilter
                Call oMenuItem.IBo_AddLoadFilter(CMP_EQUAL, FID_MENU_ID, oEntry.ProgramID)
                Call oMenuItem.IBo_Load
                
                sprdEdit.Col = COL_PROGID
                sprdEdit.Text = oMenuItem.MenuText
                sprdEdit.Col = COL_LOGIN
                sprdEdit.CellType = CellTypeStaticText
                sprdEdit.BackColor = RGB_LTGREY
                sprdEdit.Col = COL_LOGOUT
                sprdEdit.CellType = CellTypeStaticText
                sprdEdit.BackColor = RGB_LTGREY
                sprdEdit.Col = COL_FORCELOG
                sprdEdit.CellType = CellTypeStaticText
                sprdEdit.BackColor = RGB_LTGREY
            End If
            'reset edit flag that may have been automatically triggered
        Next
    End If 'any matched records to display
    lblNoMatches.Caption = sprdEdit.MaxRows
    ucProgress.Visible = False
    sbStatus.Panels(PANEL_INFO).Text = vbNullString
    Call sprdEdit.SetFocus
    Screen.MousePointer = vbNormal
    Exit Sub
    
DisplayError:

    Call Err.Report(MODULE_NAME, "cmdApply_click", 1, True, "Error displaying data", "Error occurred when display selected criteria")
    ucProgress.Visible = False
    sbStatus.Panels(PANEL_INFO).Text = vbNullString
    lblNoMatches.Caption = "Error(" & Err.Number & ")"
    Screen.MousePointer = vbNormal
    Exit Sub
    
End Sub

Private Sub cmdApply_GotFocus()

    cmdApply.FontBold = True

End Sub

Private Sub cmdApply_LostFocus()
    
    cmdApply.FontBold = False

End Sub

Private Sub cmdExit_Click()

    Unload Me

End Sub

Private Sub dtxtEndDate_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If

End Sub

Private Sub dtxtStartDate_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If

End Sub

Private Sub sprdEdit_GotFocus()
    
    sprdEdit.SelBackColor = RGB_BLUE
    sprdEdit.SelForeColor = RGB_WHITE

End Sub

Private Sub sprdEdit_LostFocus()
    
    sprdEdit.SelBackColor = -1
    sprdEdit.SelForeColor = -1
    sbStatus.Panels(PANEL_INFO).Text = vbNullString

End Sub

Private Sub cmdPrint_Click()

    sprdEdit.PrintSmartPrint = True
    sprdEdit.PrintFooter = "/lPrinted - " & Format$(Now, "DD/MM/YY HH:NN") & "/r /p of /pc"
    sprdEdit.PrintHeader = "/c/fb1Activity Log"
    sprdEdit.PrintJobName = "Activity Log"
    Call sprdEdit.PrintSheet

End Sub
Private Sub cmdPrint_GotFocus()
    
    cmdPrint.FontBold = True

End Sub

Private Sub cmdPrint_LostFocus()
    
    cmdPrint.FontBold = False

End Sub

Private Sub cmdExit_GotFocus()
    
    cmdExit.FontBold = True

End Sub

Private Sub cmdExit_LostFocus()
    
    cmdExit.FontBold = False

End Sub

Private Sub dtxtEndDate_GotFocus()

    dtxtEndDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtEndDate_LostFocus()
    
    dtxtEndDate.BackColor = lblNoMatches.BackColor

End Sub

Private Sub dtxtStartDate_GotFocus()
    
    dtxtStartDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtStartDate_LostFocus()
    
    dtxtStartDate.BackColor = lblNoMatches.BackColor

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case (Shift)
        Case (0):
            Select Case (KeyCode)
                Case (vbKeyF7):  'if F7 then call search
                    If cmdApply.Visible = True Then Call cmdApply_Click
                Case (vbKeyF9):  'if F9 then call print
                    If cmdPrint.Visible = True Then Call cmdPrint_Click
                Case (vbKeyF10): 'if F10 then exit
                    Call cmdExit_Click
            End Select 'Key pressed with no Shift/Alt combination
    End Select

End Sub
Private Sub LoadCombos()

Dim oEntry      As cUser
Dim colList     As Collection
Dim lngItem     As Long
Dim oWSConfig   As cWorkStation
    
    'Populate users list
    Set oEntry = goDatabase.CreateBusinessObject(CLASSID_USER)
    Call oEntry.IBo_AddLoadField(FID_USER_EmployeeID)
    Call oEntry.IBo_AddLoadField(FID_USER_Initials)
    Call oEntry.IBo_AddLoadField(FID_USER_FullName)
    
    Set colList = oEntry.IBo_LoadMatches
    
    Set colUserNames = New Collection
    cmbUserID.Clear
    For lngItem = 1 To colList.Count Step 1
        Set oEntry = colList(lngItem)
        Call cmbUserID.AddItem(oEntry.EmployeeID & " - " & oEntry.FullName & "(" & oEntry.Initials & ")")
        Call colUserNames.Add(oEntry.EmployeeID & " - " & oEntry.FullName & "(" & oEntry.Initials & ")", oEntry.EmployeeID)
    Next lngItem
    Call cmbUserID.AddItem("ALL")
    cmbUserID.ItemData(cmbUserID.NewIndex) = -1
    cmbUserID.ListIndex = cmbUserID.NewIndex

    'populate work station list
    Set oWSConfig = goDatabase.CreateBusinessObject(CLASSID_WORKSTATIONCONFIG)
    Call oWSConfig.IBo_AddLoadField(FID_WORKSTATIONCONFIG_id)
    Call oWSConfig.IBo_AddLoadField(FID_WORKSTATIONCONFIG_Description)
    
    Set colList = oWSConfig.IBo_LoadMatches
    
    Set colWSNames = New Collection
    cmbWorkStation.Clear
    For lngItem = 1 To colList.Count Step 1
        Set oWSConfig = colList(lngItem)
        Call cmbWorkStation.AddItem(oWSConfig.Id & " - " & oWSConfig.Description)
        Call colWSNames.Add(oWSConfig.Id & " - " & oWSConfig.Description, oWSConfig.Id)
    Next lngItem
    Call cmbWorkStation.AddItem("ALL")
    cmbWorkStation.ItemData(cmbWorkStation.NewIndex) = -1
    cmbWorkStation.ListIndex = cmbWorkStation.NewIndex

    Call cmbFunction.AddItem("ALL")
    cmbFunction.ListIndex = cmbFunction.NewIndex
    
    For lngItem = 0 To cmbDateCrit.ListCount Step 1
        If cmbDateCrit.ItemData(lngItem) = CRIT_ALL Then
            cmbDateCrit.ListIndex = lngItem
            Exit For
        End If
    Next lngItem
    
End Sub

Private Sub Form_Load()

    Me.Show
    
    sbStatus.Panels(PANEL_INFO).Text = "Accessing session"
    ' Set up the object and field that we wish to select on
    Set goRoot = GetRoot
    'Display initial values in Status Bar
    Call InitialiseStatusBar(sbStatus)
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    ucProgress.FillColor = Me.BackColor
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
        
    sbStatus.Panels(PANEL_INFO).Text = "Loading Editor"
    ucProgress.Value = 0
    ucProgress.Caption1 = "Loading Lookup Lists from Database"
    ucProgress.Visible = True
    DoEvents
    
    mstrDateFmt = UCase$(goSession.GetParameter(125))
    dtxtStartDate.DateFormat = mstrDateFmt
    dtxtStartDate.Text = Format$(Now(), mstrDateFmt)
    dtxtEndDate.DateFormat = mstrDateFmt
    dtxtEndDate.Text = Format$(Now(), mstrDateFmt)
       
    Call LoadCombos
    
    ucProgress.Visible = False
    sbStatus.Panels(PANEL_INFO).Text = vbNullString
        
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

    sbStatus.Panels(PANEL_INFO).Text = "Confirm Exit"
    'on exit check if changes should be saved
    sbStatus.Panels(PANEL_INFO).Text = vbNullString

End Sub

Private Sub Form_Resize()

Dim lSpacing As Long

    If Me.WindowState = vbMinimized Then Exit Sub
    lSpacing = sprdEdit.Left
    
    'Check form is not below minimum width
    If Me.Width < cmdPrint.Width + (lSpacing * 4) Then
        Me.Width = cmdPrint.Width + (lSpacing * 4)
        Exit Sub
    End If
    'Check form is not below minimum height
    If Me.Height < sprdEdit.Top + cmdPrint.Height * 3 + sbStatus.Height Then
        Me.Height = sprdEdit.Top + cmdPrint.Height * 3 + sbStatus.Height
        Exit Sub
    End If
    
    'relocate Progress bar
    If (Me.Width - ucProgress.Width) > 0 Then
        ucProgress.Left = (Me.Width - ucProgress.Width) / 2
    Else
        ucProgress.Left = 0
    End If
    If (Me.Height - ucProgress.Height) > 0 Then
        ucProgress.Top = (Me.Height - ucProgress.Height) / 2
    Else
        ucProgress.Top = 0
    End If
    
    
    'start resizing
    sprdEdit.Width = Me.Width - sprdEdit.Left * 2 - BORDER_WIDTH '* 2
    sprdEdit.Height = Me.Height - (cmdPrint.Height + sprdEdit.Top + (lSpacing * 3) + (BORDER_WIDTH * 6) + sbStatus.Height)
    
    cmdExit.Top = sprdEdit.Top + sprdEdit.Height + lSpacing
    cmdPrint.Top = cmdExit.Top
    
    lblNoMatcheslbl.Top = cmdExit.Top + lSpacing / 2
    lblNoMatches.Top = lblNoMatcheslbl.Top
    
    cmdPrint.Left = (sprdEdit.Width - cmdPrint.Width) + sprdEdit.Left

End Sub

