VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cReceivedIntegrationMessage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

    Private Const ModuleName As String = "cReceivedIntegrationMessage"

    Private Enum ParameterID
        epResult = 1
        epTerminateLoop = 2
        epTotalTranValue = 3
        epCashbackValue = 4
        epGratuityValue = 5
        epPAN = 6
        epExpiryDate = 7
        epIssueNumber = 8
        epStartDate = 9
        epTransactionDate = 10
        epMerchantNumber = 11
        epTerminalID = 12
        epSchemeName = 13
        epFloorLimit = 14
        epEFTSequenceNumber = 15
        epAuthorisationCode = 16
        epReferralTelephoneNumber = 17
        epMessage = 18
    End Enum

' Property fields
    Private myResult As Integer
    Private myTerminateLoop As Integer
    Private myTotalTranValue As Double
    Private myCashbackValue As Double
    Private myGratuityValue As Double
    Private myPAN As String
    Private myExpiryDate As String
    Private myIssueNumber As String
    Private myStartDate As String
    Private myTransactionDate As String
    Private myMerchantNumber As String
    Private myTerminalID As String
    Private mySchemeName As String
    Private myFloorLimit As Double
    Private myEFTSequenceNumber As String
    Private myAuthorisationCode As String
    Private myReferralTelephoneNumber As String
    Private myMessage As String
    Private myUnderstood As Boolean
    Private myErrorMessage As String
' End of property fields

    Private myParameters As Collection

' Public methods

    Public Sub Initialise()

        Set myParameters = New Collection
        Call BuildDataArray
    End Sub
' End of Public methods

' Properties

    '        1. Transaction result:
    '           '0' - Completed
    '           '7' - Declined
    '           '-nn' - All other negative values are
    '           used to define error conditions
    '           Appendix B contains a full list of error
    '           codes and messages.
    '           Screenless transaction results:
    '           '0' - Completed
    '           '2' - Referred
    '           '5' - Declined
    '           '6' - Authorised
    '           '7' - Reversed
    '           '-nn' - Negative values are used to
    '           define error conditions
    Public Property Get Result() As Integer
    
        Result = myResult
    End Property

        ' 2. Teminate Loop Reserved, Ignore
    Public Property Get TerminateLoop() As Integer

        TerminateLoop = myTerminateLoop
    End Property
    
    '  3. Values will be truncated to the correct
    '    number of decimal places
    '    required for the transaction currency.
    '    For example:
    '    1.23 = 1 (one Japanese Yen)
    '    Field will show total that will be debited
    Public Property Get TotalTranValue() As Double

        TotalTranValue = myTotalTranValue
    End Property

    ' 4 Cashback Value Double As above
    Public Property Get CashBackValue() As Double

        CashBackValue = myCashbackValue
    End Property
       
    ' 5 Gratuity Value Double As above
    Public Property Get GratuityValue() As Double

        GratuityValue = myGratuityValue
    End Property
        
    ' 6 PAN Integer The Primary Account Number
    ' (Card Number).
    ' Please note: This value will not be
    ' returned in full due to PCI requirements.
    ' The PAN will be masked apart from the
    ' last four digits, e.g. '************1234'
    Public Property Get PAN() As String

        PAN = myPAN
    End Property

    ' Expiry Date MMYY Integer Card Expiry Month and Year.
    Public Property Get ExpiryDate() As String

        ExpiryDate = myExpiryDate
    End Property

    ' 8 Issue Number Integer Card Issue Number. Blank when scheme
    ' does not provide an issue number.
    Public Property Get IssueNumber() As String

        IssueNumber = myIssueNumber
    End Property

    ' 9 Start MMYY Integer Card start month and year
    Public Property Get StartDate() As String

        StartDate = myStartDate
    End Property

    ' 10 Transaction Date / Time Integer CCYYMMDDHHMMSS
    Public Property Get TransactionDate() As String

        TransactionDate = myTransactionDate
    End Property

    ' 11 Merchant Number Integer The Merchant Number for the given' card scheme and account.
    Public Property Get MerchantNumber() As String

        MerchantNumber = myMerchantNumber
    End Property

    ' 12 Terminal ID Integer Terminal ID used for this transaction.
    Public Property Get TerminalID() As String

        TerminalID = myTerminalID
    End Property

    ' 13 Scheme Name String Card scheme name e.g. visa etc
    Public Property Get SchemeName() As String

        SchemeName = mySchemeName
    End Property

    ' 14 Floor Limit Integer Floor limit for the card scheme/account.
    Public Property Get FloorLimit() As Double

        FloorLimit = myFloorLimit
    End Property
    
    ' 15 EFT Sequence Number Integer Four digits in the range 0001 - 9999.
    ' (Prefixed with "OL" when offline)
    Public Property Get EFTSequenceNumber() As String

        EFTSequenceNumber = myEFTSequenceNumber
    End Property
    
    ' 16 Authorisation Code String Blank if the transaction is declined or is
    ' below the floor limit.
    Public Property Get AuthorisationCode() As String

        AuthorisationCode = myAuthorisationCode
    End Property
    
    ' 17 Referral Telephone Number. Reserved, ignore
    Public Property Get ReferralTelephoneNumber() As String

        ReferralTelephoneNumber = myReferralTelephoneNumber
    End Property
    
    ' 18 Customer Verification Method /
    ' Authorisation Message / Error Message
    ' / Status Message
    ' String As returned by communications process.
    ' Normally direct from acquirer. Also
    ' contains status message if enabled
    Public Property Get Message() As String

        Message = myMessage
    End Property
    
    ' Non paramter based properties
    Public Property Get ErrorMessage() As String
    
        ErrorMessage = myErrorMessage
    End Property
    
     ' This is used to parse the output message.
    ' It is the number of returned parameters from Commidea
    Public Property Get NumberOfParameters() As Integer
    
        NumberOfParameters = 18
    End Property
    
    Public Property Get Understood() As Boolean
    
        Understood = myUnderstood
    End Property

' End of Properties

' Friend methods
    
    ' this method will use the comma as a delimiter and populate the values above
    ' 0,1,10.00,0.00,0.00,************0002,1210,,,20100625142804,21249872,22736839,Visa,0.00,2247,789DE,,CONFIRMED,Keyed,826,,,,,,,3,,0,0,0
    Friend Function ParseReceivedMessage(ByVal Message As String) As Boolean
        Dim MessageLen As Integer
        ReDim ParamValues(1 To Me.NumberOfParameters) As String
        Dim Delimiter As String
        Dim Param As Integer

    On Error GoTo Catch

Try:
        Clean
        MessageLen = Len(Message)
        If MessageLen > 0 Then
            '  define which character is seperating fields
            Delimiter = ","
            ParamValues = SplitTo1BasedStringArray(Message, Delimiter)
            For Param = 1 To Me.NumberOfParameters
                ' only bother setting the rtn fields if there is a value
                If Len(ParamValues(Param)) > 0 Then
                    Call SetPropertyFromParameterValue(ParamValues(Param), Param)
                End If
            Next Param
        End If

        myUnderstood = True
        ParseReceivedMessage = True

        Exit Function
        
Catch:
        Call Err.Raise(vbObjectError + 1, ModuleNameStub & "ParseReceivedMessage", "Failed to parse received integration message.")
    End Function
' End of Friend methods

' Private methods
    
    Private Sub Clean()
        
        myResult = 0
        myTerminateLoop = 0
        myTotalTranValue = 0#
        myCashbackValue = 0#
        myGratuityValue = 0#
        myPAN = ""
        myExpiryDate = ""
        myIssueNumber = ""
        myStartDate = ""
        myTransactionDate = ""
        myMerchantNumber = ""
        myTerminalID = ""
        mySchemeName = ""
        myFloorLimit = 0#
        myEFTSequenceNumber = ""
        myAuthorisationCode = ""
        myReferralTelephoneNumber = ""
        myErrorMessage = ""
        myMessage = ""
        myUnderstood = False
    End Sub
    
    Private Sub CreateParameterInfoAndAddtoArray(ByVal Name As String, ByVal PropertyName As String, ByVal ParamID As ParameterID, ByVal ParamType As ParamValueType)
        Dim ParamInfo As cParameterInfo
        
        Set ParamInfo = New cParameterInfo
        Call ParamInfo.Initialise(Name, PropertyName, ParamID, ParamType)
        Call myParameters.Add(ParamInfo, Key:=Trim(CStr(ParamID)))
    End Sub

    Private Sub BuildDataArray()
        
        Call CreateParameterInfoAndAddtoArray("Result", "Result", ParameterID.epResult, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("TerminateLoop", "TerminateLoop", ParameterID.epTerminateLoop, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("TotalTranValueProcessed", "TotalTranValueProcessed", ParameterID.epTotalTranValue, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("CashBackValue", "CashBackValue", ParameterID.epCashbackValue, ParamValueType.epvtDouble)
        Call CreateParameterInfoAndAddtoArray("GratuityValue", "GratuityValue", ParameterID.epGratuityValue, ParamValueType.epvtDouble)
        Call CreateParameterInfoAndAddtoArray("PAN", "PAN", ParameterID.epPAN, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("ExpiryDate", "ExpiryDate", ParameterID.epExpiryDate, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("IssueNumber", "IssueNumber", ParameterID.epIssueNumber, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("StartDate", "StartDate", ParameterID.epStartDate, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("TransactionDate", "TransactionDate", ParameterID.epTransactionDate, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("MerchantNumber", "MerchantNumber", ParameterID.epMerchantNumber, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("TerminalID", "TerminalID", ParameterID.epTerminalID, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("SchemeName", "SchemeName", ParameterID.epSchemeName, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("FloorLimit", "FloorLimit", ParameterID.epFloorLimit, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("EFTSequenceNumber", "EFTSequenceNumber", ParameterID.epEFTSequenceNumber, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("AuthorisationCode", "AuthorisationCode", ParameterID.epAuthorisationCode, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("ReferralTelephoneNumber", "ReferralTelephoneNumber", ParameterID.epReferralTelephoneNumber, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("Message", "CustomerVerification", ParameterID.epMessage, ParamValueType.epvtString)
   End Sub
    
    Private Sub SetPropertyFromParameterValue(ByVal ParamValue As String, ByVal ParamID As Integer)
        Dim TempInt As Integer
        Dim TempDbl As Double
        Dim TempDate As Date
        Dim CharPos As Integer
        Dim ParamChar As String
        
        Select Case ParamID
            Case ParameterID.epResult:
                If ParamToInt(TempInt, ParamValue) Then
                    myResult = TempInt
                    
                End If
            Case ParameterID.epTerminateLoop:
                If ParamToInt(TempInt, ParamValue) Then
                    myTerminateLoop = TempInt
                End If
            Case ParameterID.epTotalTranValue:
                If ParamToDouble(TempDbl, ParamValue) Then
                    myTotalTranValue = TempDbl
                End If
            Case ParameterID.epCashbackValue:
                If ParamToDouble(TempDbl, ParamValue) Then
                    myCashbackValue = TempDbl
                End If
            Case ParameterID.epGratuityValue:
                If ParamToDouble(TempDbl, ParamValue) Then
                    myGratuityValue = TempDbl
                End If
            Case ParameterID.epPAN:
                If Len(ParamValue) > 4 Then
                    For CharPos = 1 To Len(ParamValue) - 4
                        ParamChar = Mid(ParamValue, CharPos, 1)
                        If ParamChar <> "*" _
                        And ParamChar <> "-" Then
                            Exit For
                        End If
                    Next CharPos
                    If CharPos > Len(ParamValue) - 4 Then
                        For CharPos = 1 To 4
                            ParamChar = Mid(ParamValue, Len(ParamValue) - 4 + CharPos, 1)
                            If Not IsNumeric(ParamChar) Then
                                Exit For
                            End If
                        Next CharPos
                        If CharPos > 4 Then
                           myPAN = ParamValue
                        End If
                    End If
                End If
            Case ParameterID.epExpiryDate: ' expiry date
                myExpiryDate = ParamValue
            Case ParameterID.epIssueNumber:
                myIssueNumber = ParamValue
                If myIssueNumber = "" Then
                    ' Default value - empty strings might cause problems in other processes
                    myIssueNumber = "00"
                End If
            Case ParameterID.epStartDate:
                myStartDate = ParamValue
            Case ParameterID.epTransactionDate:
                myTransactionDate = ParamValue
            Case ParameterID.epMerchantNumber:
                myMerchantNumber = ParamValue
            Case ParameterID.epTerminalID:
                myTerminalID = ParamValue
            Case ParameterID.epSchemeName:
                mySchemeName = ParamValue
            Case ParameterID.epFloorLimit:
                If ParamToDouble(TempDbl, ParamValue) Then
                    myFloorLimit = TempDbl
                End If
            Case ParameterID.epEFTSequenceNumber:
                If ParamToInt(TempInt, ParamValue) Then
                    If TempInt >= 1 And TempInt <= 9999 Then
                        myEFTSequenceNumber = ParamValue
                    End If
                Else
                    If Len(ParamValue) >= 3 Then
                        If StrComp(Left(ParamValue, 2), "OL", vbTextCompare) = 0 Then
                            If ParamToInt(TempInt, Mid(ParamValue, 3)) Then
                                If TempInt >= 1 And TempInt <= 9999 Then
                                    myEFTSequenceNumber = ParamValue
                                End If
                            End If
                        End If
                    End If
                End If
            Case ParameterID.epAuthorisationCode:
                myAuthorisationCode = ParamValue
            Case ParameterID.epReferralTelephoneNumber:
                myReferralTelephoneNumber = ParamValue
            Case ParameterID.epMessage:
                myMessage = ParamValue
            Case Else
                myErrorMessage = "Unknown integration return message parameter."
        End Select
    End Sub
    
    Private Function ModuleNameStub() As String
    
        ModuleNameStub = AppNameStub & ModuleName & "."
    End Function
' End of private methods
