VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "mswinsck.ocx"
Begin VB.Form frmLogout 
   Caption         =   "Logging out"
   ClientHeight    =   585
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5415
   LinkTopic       =   "Form1"
   ScaleHeight     =   585
   ScaleWidth      =   5415
   StartUpPosition =   1  'CenterOwner
   Begin MSWinsockLib.Winsock wskIntegration 
      Left            =   0
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      RemotePort      =   25000
   End
   Begin VB.Label lblActivity 
      Alignment       =   2  'Center
      Caption         =   "Logging off Pin Entry Device"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   204
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5415
   End
End
Attribute VB_Name = "frmLogout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Implements ICommideaForm

Private moCommideaCfg As CommideaConfiguration
Private myReceivedMessage As String
Private myMessageReceived As Boolean
Private mblnConnectionError As Boolean
Private myIntegrationError As String

Public Event Duress()

' Control events

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If moCommideaCfg.UnderDuressKeyCode <> 0 And KeyCode = moCommideaCfg.UnderDuressKeyCode Then
        KeyCode = 0
        RaiseEvent Duress
    End If
End Sub

'Integration Socket events
Private Sub wskIntegration_ConnectionRequest(ByVal requestID As Long)

    ' Check if the control's State is closed. If not,
    ' close the connection before accepting the new
    ' connection.
    With wskIntegration
        If .State <> sckClosed Then
            .Close
        End If
        ' Accept the request with the requestID
        ' parameter.
        .Accept requestID
    End With
End Sub

Private Sub wskIntegration_DataArrival(ByVal bytesTotal As Long)
    Dim Received As String
    Dim Extra As String

    If bytesTotal > 0 Then
        Do
            Call wskIntegration.GetData(Extra, vbString)
            Received = Received & Extra
        Loop While Right(Received, 2) <> vbCrLf And Received <> "" And Asc(Left(Received, 1)) <> 6
        DebugMsg ModuleName, "wskIntegration_DataArrival", endlDebug, "Integration Message Received: " & Received
        If Received <> "" Then
            ' Make sure its the right format
            ' - Ocius sends Asc(6) to acknowledge receipt of message,
            ' but this often come out as part of the following data
            ' sent to winsock, sometimes it arrives on its own.  If
            ' remove it on own the message is now blank so will loop
            ' for more data; if together this will just strip it off
            If Asc(Left(Received, 1)) = 6 Then
                ' Remove the message delimiters
                Received = Mid(Received, 2)
            End If
            If Right(Received, 2) = vbCrLf Then
                ' Remove the message delimiters
                Received = Left(Received, Len(Received) - 2)
            End If
            ReceivedMessage = Received
        End If
    End If
End Sub

Private Sub wskIntegration_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)

    Select Case Number
        Case 1
            myIntegrationError = Trim(CStr(Number)) & ";" & Description & ";;Connection Error"
        Case 10060, 10061
            myIntegrationError = Trim(CStr(Number)) & ";" & Description & ";Please ensure Ocius is running and logged in;Ocius Sentinel Connection Error"
        Case Else
            myIntegrationError = Trim(CStr(Number)) & ";" & Description & ";Please check the connection cables, Network availability etc;General"
    End Select
    wskIntegration.Close
End Sub
' End of Integration Socket events
' End of Control events

Public Function LogOut(ByRef TimedOut As Boolean, _
                       Optional ByVal LogoutOption As LogoutRecordFunction = LogoutRecordFunction.elrfLogout) As Boolean
    Dim LogoutRecord As New cLogoutRecord
    Dim Start As Date
    Dim Finish As Date
    Dim TimeTaken As Integer

    ' Make sure connected before starting
    Start = Time
    Do
        Finish = Time
        Call FireUpSocket(moCommideaCfg.IntegrationPort)
        TimeTaken = ((Minute(Finish) * 60) + Second(Finish)) - ((Minute(Start) * 60) + Second(Start))
    Loop While TimeTaken < moCommideaCfg.ConnectionTimeout And Not Me.wskIntegration.State = sckConnected
    If Me.wskIntegration.State = sckConnected Then
        Call Me.Show
        ' Generate a login record to send to PED via socket
        Call LogoutRecord.Initialise(LogoutOption)
        ' Send login request to PED
        If SendMessage(LogoutRecord) Then
            With LogoutRecord
                If .LogoutSuccessful Then
                    ' Can have 'user not logged in message' as being a successful log out
                    If InStr(1, .ResultMessage, "user not", vbTextCompare) > 0 Then
                        TimedOut = True
                    End If
                Else
                    TimedOut = True
                End If
            End With
        Else
            TimedOut = True
        End If
        ' Show & return the result
        Me.lblActivity.Caption = LogoutRecord.ResultMessage
        LogOut = LogoutRecord.LogoutSuccessful
    Else
        Me.lblActivity.Caption = "Failed communicating with PED."
        Me.Caption = "Cannot log out."
    End If
End Function

Public Sub CloseMe()

    Call UnloadCommideaForm(Me)
End Sub

Private Function SendMessage(ByRef IntegrationRecord As Object) As Boolean
    
Retry:
    With Me.wskIntegration
        Select Case .State
            Case sckConnected
                Call DebugMsg(ModuleName, "SendMessage", endlDebug, "Calling Send")
                MessageReceived = False
                ReceivedMessage = ""
                .SendData (IntegrationRecord.ToIntegrationMessage) 'Send Transaction
                ' Make sure the data is actually sent, now
                DoEvents
                Call DebugMsg(ModuleName, "SendMessage", endlDebug, "WaitForData")
                Call Wait(1)
                Call WaitForData
                IntegrationRecord.ReceivedMessage = ReceivedMessage
                SendMessage = MessageReceived
            Case sckError
                Call DebugMsg(ModuleName, "SendMessage", endlDebug, "Error Found")
            Case Else
        End Select
        Call DebugMsg(ModuleName, "SendMessage", endlDebug, "Closing Integration Winsock connection")
        Call .Close
    End With
End Function ' SendMessage

Private Function FireUpSocket(ByVal IntegrationPort As String) As Boolean

    ' Set up the integration socket to receive data
    With Me.wskIntegration
        Call .Close
        .RemotePort = IntegrationPort
        .RemoteHost = "127.0.0.1"    ' .LocalIP
        Call DebugMsg(ModuleName, "FireUpSocket", endlDebug, "Integration Winsock state is currently " & .State)
        Call DebugMsg(ModuleName, "FireUpSocket", endlDebug, "Integration Winsock connecting to " & "127.0.0.1")    ' .LocalIP)
        Call .Connect
        While .State = sckConnecting
            DoEvents
        Wend
        Call DebugMsg(ModuleName, "FireUpSocket", endlDebug, "Integration Winsock state is currently " & .State)
    End With
    FireUpSocket = Me.wskIntegration.State = sckConnected
End Function

Private Property Get MessageReceived() As Boolean

    MessageReceived = myMessageReceived
End Property

Private Property Let MessageReceived(ByVal NewMessageReceived As Boolean)

    myMessageReceived = NewMessageReceived
End Property

Private Property Get ReceivedMessage() As String

    ReceivedMessage = myReceivedMessage
End Property

Private Property Let ReceivedMessage(ByVal NewReceivedMessage As String)

    If NewReceivedMessage = "" Then
        myReceivedMessage = NewReceivedMessage
    Else
        ' Sometimes just get a single Asc() = 6 character before message,
        ' this can be ignored (treated as though blank)
        If Len(NewReceivedMessage) = 1 And Asc(NewReceivedMessage) = 6 Then
            myReceivedMessage = ""
        Else
            myReceivedMessage = NewReceivedMessage
        End If
    End If
End Property

Private Sub WaitForData()
    Dim Start As Date
    Dim Finish As Date
    Dim TimeTaken As Integer

    Start = Time

    Call DebugMsg(ModuleName, "WaitForData", endlDebug, "Output message = " & ReceivedMessage)
    Do
        Call Wait(1)
        DoEvents
        Finish = Time
        TimeTaken = ((Minute(Finish) * 60) + Second(Finish)) - ((Minute(Start) * 60) + Second(Start))
    Loop While ReceivedMessage = "" And TimeTaken < moCommideaCfg.LoginTimeout
    MessageReceived = TimeTaken < moCommideaCfg.LoginTimeout
    Call DebugMsg(ModuleName, "WaitForData", endlDebug, "Output message = " & ReceivedMessage)
End Sub 'WaitForData

Private Function ModuleName() As String

    ModuleName = "frmLogout"
End Function


Public Sub ICommideaForm_InitializeBeforeLoad(ByVal commideaCfg As CommideaConfiguration)
    Set moCommideaCfg = commideaCfg
End Sub

Public Sub ICommideaForm_ResetAfterUnload()
    Set moCommideaCfg = Nothing
End Sub

