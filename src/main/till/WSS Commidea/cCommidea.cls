VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cCommidea"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Const ModuleName As String = "cCommidea"

Private moCommideaCfg As CommideaConfiguration

' Local property variables
Private myHostIPAddress As String
Private myLoggedIn As Boolean

' End of Local property varaibles
' End of Property Variables

' Modular storage
Private mblnRetry As Boolean
Private mblnRetryLogin As Boolean
Private mblnInLaunchAndLogin As Boolean
Private mblnConnectionError As Boolean
Private myUserID As String
Private myUserPIN As String

Private myPEDTypeService As New PEDTypeService
' End of modular storage

' Events
Public Event Duress()
Public Event InvalidTokenID()
Public Event AwaitingMyDiscountCard()
Public Event MyDiscountCardError(ByVal CardError As String)
Public Event MyDiscountCardReadCancelled()
' For handling form events
Private WithEvents AddCommandsForm As frmAdditionalCommands
Attribute AddCommandsForm.VB_VarHelpID = -1
Private WithEvents ReadCardForm As frmReadCardDetails
Attribute ReadCardForm.VB_VarHelpID = -1
Private WithEvents LoginForm As frmLogin
Attribute LoginForm.VB_VarHelpID = -1
Private WithEvents LogoutForm As frmLogout
Attribute LogoutForm.VB_VarHelpID = -1
Private WithEvents TransactionForm As frmTransaction
Attribute TransactionForm.VB_VarHelpID = -1

Private Sub AddCommandsForm_Duress()

    RaiseEvent Duress
End Sub

Private Sub LoginForm_Duress()

    RaiseEvent Duress
End Sub

Private Sub LogoutForm_Duress()

    RaiseEvent Duress
End Sub

Private Sub ReadCardForm_AwaitingCard()

    RaiseEvent AwaitingMyDiscountCard
End Sub

Private Sub ReadCardForm_CardError(ByVal ErrorMessage As String)

    RaiseEvent MyDiscountCardError(ErrorMessage)
End Sub

Private Sub ReadCardForm_UserCancelled()

    RaiseEvent MyDiscountCardReadCancelled
End Sub

Private Sub TransactionForm_Duress()

    RaiseEvent Duress
End Sub

' Methods

Public Function Authorise(oAuthorizationRequest As AuthorizationRequest) As AuthorizationResult
    
    Const MethodName = "Authorise"

    Call DebugMsg(ModuleName, MethodName, endlDebug, "Information recieved from till. " & "Amount: " & Format(oAuthorizationRequest.TxnValue, "0.00") & " Tran Type: " & Trim(CStr(oAuthorizationRequest.transactionType)))
    
    Call DebugMsg(ModuleName, MethodName, endlDebug, "Loading TransactionEngine")
    Dim transactionEngine As ITransactionEngine
    Set transactionEngine = New frmTransaction
    Call DebugMsg(ModuleName, MethodName, endlDebug, "Loaded TransactionEngine")
    
    Call DebugMsg(ModuleName, MethodName, endlDebug, "Setting appropriate WithEvents reference to TransactionEngine")
    Set TransactionForm = transactionEngine
    Call DebugMsg(ModuleName, MethodName, endlDebug, "Set appropriate WithEvents reference to TransactionEngine")
    
    Call DebugMsg(ModuleName, MethodName, endlDebug, "Initialising TransactionEngine; Tillid=" & moCommideaCfg.TillID & ", ShowKeypadIcon=" & IIf(moCommideaCfg.ShowKeypadIcon, "true", "false") & ", PosPrinter=" & moCommideaCfg.PrinterInfo.Printer.Name)
    Call transactionEngine.Initialise(moCommideaCfg)
    Call DebugMsg(ModuleName, MethodName, endlDebug, "Initialised TransactionEngine")
    
    ' Initiate the transaction
    Call DebugMsg(ModuleName, MethodName, endlDebug, "Calling TransactionEngine.Authorise")
    
    oAuthorizationRequest.PEDType = GetPEDTypeForCurrentOciusSentinelVersion()
    
    Dim oAutorizationResult As AuthorizationResult
    Set oAutorizationResult = transactionEngine.Authorise(oAuthorizationRequest)
    
    If Not oAutorizationResult.Completed Then
        Call MakeHumanReadableFailureReason(oAuthorizationRequest, oAutorizationResult)
    End If
    
    Set TransactionForm = Nothing
    Call transactionEngine.Dispose
    Set transactionEngine = Nothing
    
    Set Authorise = oAutorizationResult
    
End Function

Public Sub Initialise(ByVal commideaCfg As CommideaConfiguration)

On Error GoTo Catch

Try:
    Set moCommideaCfg = commideaCfg
    
    ' Get this before setting up Integration Socket
    myHostIPAddress = Utilities.GetHostIPAddress()
    
    myLoggedIn = moCommideaCfg.AlreadyLoggedIn
    
    Exit Sub
Catch:
    
End Sub

' CR0034
' Add function to retrieve the PTID from the PED
' Referral 818 - Add ByRef Software ready flag parameter to indicate when software install is not yet complete
Public Function GetPTID(ByRef PTID As String, _
                        ByVal UserID As String, _
                        ByVal UserPIN As String, _
                        ByRef LoginTimedOut As Boolean, _
                        ByRef RestartRequired As Boolean, _
                        ByRef SoftwareReady As Boolean) As Boolean
    Dim Continue As Boolean
    Dim WasSuccessful As Boolean
    WasSuccessful = False

    Call DebugMsg(ModuleName, "GetPTID", endlTraceIn)
    If OciusSentinelLaunched Then
        Call DebugMsg(ModuleName, "GetPTID", endlDebug, "Launching Ocius Sentinel")
        If Not LoggedIn Then
            Call DebugMsg(ModuleName, "GetPTID", endlDebug, "Logging in to PED")
            ' Referral 818 - Pass on ByRef Software ready flag parameter to indicate when software install is not yet complete
            If LogIn(UserID, UserPIN, RestartRequired, LoginTimedOut, SoftwareReady) Then
                Continue = True
            End If
        Else
            Continue = True
        End If
    End If
    If Continue Then
        Call DebugMsg(ModuleName, "GetPTID", endlDebug, "Ocius Sentinel and PED ready")
        Call LoadCommideaForm(frmAdditionalCommands, moCommideaCfg)
        Set AddCommandsForm = frmAdditionalCommands
        With frmAdditionalCommands
            Call DebugMsg(ModuleName, "GetPTID", endlDebug, "Getting PTID")
            WasSuccessful = .GetPTID(PTID)
            Call DebugMsg(ModuleName, "GetPTID", endlDebug, "PTID = " & IIf(WasSuccessful, PTID, "Failed getting PTID"))
        End With
        Set AddCommandsForm = Nothing
        Call UnloadCommideaForm(frmAdditionalCommands)
    End If
    GetPTID = WasSuccessful
    Call DebugMsg(ModuleName, "GetPTID", endlTraceOut)
End Function
' End of CR0034

Public Function GetSoftwareVersion(ByRef SoftwareVersion As String, _
                                   ByVal UserID As String, _
                                   ByVal UserPIN As String, _
                                   ByRef LoginTimedOut As Boolean, _
                                   ByRef RestartRequired As Boolean, _
                                   ByRef SoftwareReady As Boolean) As Boolean
    Dim Continue As Boolean
    Dim WasSuccessful As Boolean

    Call DebugMsg(ModuleName, "GetSoftwareVersion", endlTraceIn)
    If OciusSentinelLaunched Then
        Call DebugMsg(ModuleName, "GetSoftwareVersion", endlDebug, "Launching Ocius Sentinel")
        If Not LoggedIn Then
            Call DebugMsg(ModuleName, "GetSoftwareVersion", endlDebug, "Logging in to PED")
            If LogIn(UserID, UserPIN, RestartRequired, LoginTimedOut, SoftwareReady) Then
                Continue = True
            End If
        Else
            Continue = True
        End If
    End If
    If Continue Then
        Call DebugMsg(ModuleName, "GetSoftwareVersion", endlDebug, "Ocius Sentinel and PED ready")
        Call LoadCommideaForm(frmAdditionalCommands, moCommideaCfg)
        Set AddCommandsForm = frmAdditionalCommands
        With frmAdditionalCommands
            Call DebugMsg(ModuleName, "GetSoftwareVersion", endlDebug, "Getting SoftwareVersion")
            WasSuccessful = .GetSoftwareVersion(SoftwareVersion)
            Call DebugMsg(ModuleName, "GetSoftwareVersion", endlDebug, "SoftwareVersion = " & IIf(WasSuccessful, SoftwareVersion, "Failed getting software version"))
        End With
        Set AddCommandsForm = Nothing
        Call UnloadCommideaForm(frmAdditionalCommands)
    End If
    GetSoftwareVersion = WasSuccessful
    Call DebugMsg(ModuleName, "GetSoftwareVersion", endlTraceOut)
End Function

' Referral 818 - Add ByRef Software ready flag parameter to indicate when software install is not yet complete
Public Function LaunchOciusSentinelAndLogin(ByVal UserID As String, ByVal UserPIN As String, _
                                            ByVal LaunchParams As String, ByRef LoginTimedOut As Boolean, _
                                            ByRef RestartRequired As Boolean, _
                                            ByRef SoftwareReady As Boolean) As Boolean
    Dim Start As Date
    Dim Finish As Date
    Dim TimeTaken As Integer

    myUserID = UserID
    myUserPIN = UserPIN

On Error GoTo DebugErr

    Call LaunchOciusSentinel(LaunchParams)
    
    If OciusSentinelLaunched Then
        If Not LoggedIn Then
            ' Switch off socket error messaging
            mblnInLaunchAndLogin = True
            ' Repeat til logged in or timeout
            Start = Time
            Call DebugMsg(ModuleName, "LaunchOciusSentinelAndLogin", endlDebug, "Timeout limit = " & CStr(moCommideaCfg.LoginTimeout))
            Do
                Finish = Time
                TimeTaken = ((Minute(Finish) * 60) + Second(Finish)) - ((Minute(Start) * 60) + Second(Start))
                ' Referral 818 - Pass on ByRef Software ready flag parameter to indicate when software install is not yet complete
                Call LogIn(UserID, UserPIN, RestartRequired, LoginTimedOut, SoftwareReady)
                Call DebugMsg(ModuleName, "LaunchOciusSentinelAndLogin", endlDebug, "Loop criteria TimeTaken < LoginTimeout = " & CStr(TimeTaken < moCommideaCfg.LoginTimeout))
                Call DebugMsg(ModuleName, "LaunchOciusSentinelAndLogin", endlDebug, "Not LoggedIn = " & CStr(Not LoggedIn))
                Call DebugMsg(ModuleName, "LaunchOciusSentinelAndLogin", endlDebug, "Not RestartRequired = " & CStr(Not RestartRequired))
                Call DebugMsg(ModuleName, "LaunchOciusSentinelAndLogin", endlDebug, "SoftwareReady = " & CStr(SoftwareReady))
            Loop While TimeTaken < moCommideaCfg.LoginTimeout And Not LoggedIn And Not RestartRequired And SoftwareReady
            Call DebugMsg(ModuleName, "LaunchOciusSentinelAndLogin", endlDebug, "Time taken = " & CStr(TimeTaken))
        End If
        If LoggedIn Then
            LaunchOciusSentinelAndLogin = True
            Call DebugMsg(ModuleName, "LaunchOciusSentinelAndLogin", endlDebug, "Logged in successfully")
        Else
            ' error failed logging in
            Call DebugMsg(ModuleName, "LaunchOciusSentinelAndLogin", endlDebug, "Logged in failed")
        End If
    Else
        ' error failed launching
        Call DebugMsg(ModuleName, "LaunchOciusSentinelAndLogin", endlDebug, "Launch failed")
    End If
    
    Exit Function

DebugErr:
    Dim Num As Long
    Dim Source As String
    Dim Description As String
    
    Call DebugMsg(ModuleName, "LaunchOciusSentinelAndLogin", endlDebug, "Error raised : " & Err.Description)
    Call DebugMsg(ModuleName, "LaunchOciusSentinelAndLogin", endlDebug, "Timed Out = " & CStr(LoginTimedOut) & "; Software Ready = " & CStr(SoftwareReady) & "; Restart Requested = " & CStr(RestartRequired))
    Call DebugMsg(ModuleName, "LaunchOciusSentinelAndLogin", endlTraceOut)
    Num = Err.Number
    Source = Err.Source
    Description = Err.Description
    On Error GoTo 0
    Call Err.Raise(Num, Source, Description)
    Call DebugMsg(ModuleName, "LaunchOciusSentinelAndLogin", endlTraceOut)
End Function

Public Function LaunchOciusSentinel(Optional ByVal LaunchParams As String) As Boolean
    Dim ValidParams As String
    Dim LaunchArgs() As String
    Dim LaunchArg As Integer
    Dim fsoCreateIniFile As New FileSystemObject
    
    If Not OciusSentinelLaunched Then
        LaunchArgs = Split(LaunchParams, ",")
        If UBound(LaunchArgs) >= 0 Then
            ValidParams = Join(ValidLaunchOciusSentinelParameters, " ")
            ' Arguments have to be in an ini file, cannot be part of the command line
            With fsoCreateIniFile
                If .FolderExists(moCommideaCfg.PathToOciusSentinel) Then
                    With .CreateTextFile(OciusSentinelIniFile, True)
                        ' Add any (valid) parameters
                        For LaunchArg = 0 To UBound(LaunchArgs)
                            If LaunchArgs(LaunchArg) & "" <> "" Then
                                If InStr(1, ValidParams, LaunchArgs(LaunchArg), vbTextCompare) > 0 Then
                                    Call .WriteLine("/" & UCase(LaunchArgs(LaunchArg)))
                                End If
                            End If
                        Next LaunchArg
                        .Close
                    End With
                End If
            End With
        End If
        LaunchOciusSentinel = OciusSentinelLaunch()
    Else
        LaunchOciusSentinel = True
    End If
End Function

' Referral 818 - Add ByRef Software ready flag parameter to indicate when software install is not yet complete
Private Function LogIn(ByVal UserID As String, ByVal UserPIN As String, _
                      ByRef RestartRequired As Boolean, _
                      ByRef TimedOut As Boolean, _
                      ByRef SoftwareReady As Boolean) As Boolean
    Dim LoginRecord As New cL2Record

    If Not LoggedIn Then
        myUserID = UserID
        myUserPIN = UserPIN
        Call LoadCommideaForm(frmLogin, moCommideaCfg)
        Set LoginForm = frmLogin
        With frmLogin
            LoggedIn = .LogIn(UserID, UserPIN)
            RestartRequired = .RestartRequired
            TimedOut = .TimedOut
            ' Referral 818 - Pass back Software ready flag parameter to indicate when software install is not yet complete
            SoftwareReady = .SoftwareReady
            Call .CloseMe
        End With
        LogIn = LoggedIn
        Set LoginForm = Nothing
        Call UnloadCommideaForm(frmLogin)
    End If
End Function

Public Function OciusSentinelClose(ByRef TimedOut As Boolean, _
                                   ByVal LogOut As Boolean)

    If LogOut Then
        Call LoadCommideaForm(frmLogout, moCommideaCfg)
        Set LogoutForm = frmLogout
        Call frmLogout.LogOut(TimedOut, LogoutOption:=elrfExit)
        Set LogoutForm = Nothing
        Call UnloadCommideaForm(frmLogout)
    End If
    ' If timed out on logout or not logged out, might need to still close the OciusSentinel software
    If TimedOut Or Not LogOut Then
        If OciusSentinelLaunched Then
            ' Try to force the app to close
            Call CloseOciusSentinel(0)
        End If
    End If
End Function

Public Function OciusSentinelLaunch(Optional ByVal MaxNumberOfAttempts As Integer = 3) As Boolean
    Dim NoTries As Integer

    ' Parameter for number of tries?
    For NoTries = 1 To MaxNumberOfAttempts
        OciusSentinelLaunch = LaunchOS()
        If OciusSentinelLaunch Then
            Exit For
        End If
    Next NoTries
End Function

' Referral 818 - Add ByRef Software ready flag parameter to indicate when software install is not yet complete
Public Function ProcessOfflineTransactions(ByVal UserID As String, _
                                           ByVal UserPIN As String, _
                                           ByRef LoginTimedOut As Boolean, _
                                           ByRef RestartRequired As Boolean, _
                                           ByRef SoftwareReady As Boolean) As Boolean
    Dim Continue As Boolean

    If OciusSentinelLaunched Then
        If Not LoggedIn Then
            ' Referral 818 - Pass on ByRef Software ready flag parameter to indicate when software install is not yet complete
            If LogIn(UserID, UserPIN, RestartRequired, LoginTimedOut, SoftwareReady) Then
                Continue = True
            End If
        Else
            Continue = True
        End If
    Else
        ' Do we really want to launch here?
    End If
    If Continue Then
        Call LoadCommideaForm(frmAdditionalCommands, moCommideaCfg)
        Set AddCommandsForm = frmAdditionalCommands
        With frmAdditionalCommands
            ProcessOfflineTransactions = .ProcessOfflineTransactions()
        End With
        Set AddCommandsForm = Nothing
        Call UnloadCommideaForm(frmAdditionalCommands)
    End If
End Function

Public Function ReadMyDiscountCard(ByRef CardNumber As String) As Boolean

    Call LoadCommideaForm(frmReadCardDetails, moCommideaCfg)
    Set ReadCardForm = frmReadCardDetails
    With frmReadCardDetails
        Call .Initialise(egcdomNonEFT, egcdrcsRemove, egcdpvStandard)
        ReadMyDiscountCard = .ReadCard(CardNumber)
    End With
    Set ReadCardForm = Nothing
    Call UnloadCommideaForm(frmReadCardDetails)
End Function

Public Sub ResetFocus(ByVal FormName As String)

    Call Utilities.ResetFocus(FormName)
End Sub
' End of Methods

' Properties

Private Property Get HostIPAddress() As String

    HostIPAddress = myHostIPAddress
End Property

Private Property Let HostIPAddress(ByVal NewHostIPAddress As String)

    myHostIPAddress = NewHostIPAddress
End Property

Private Property Get LoggedIn() As Boolean

    LoggedIn = myLoggedIn
End Property

Private Property Let LoggedIn(ByVal newLoggedIn As Boolean)

    myLoggedIn = newLoggedIn
End Property
    
Private Property Get OciusSentinelApplication() As String

    OciusSentinelApplication = moCommideaCfg.PathToOciusSentinel
    If Right(OciusSentinelApplication, 1) <> "\" Then
        OciusSentinelApplication = OciusSentinelApplication & "\"
    End If
    OciusSentinelApplication = OciusSentinelApplication & "OciusSentinel.exe"
End Property

Private Property Get OciusSentinelIniFile() As String

    OciusSentinelIniFile = moCommideaCfg.PathToOciusSentinel
    If Right(OciusSentinelIniFile, 1) <> "\" Then
        OciusSentinelIniFile = OciusSentinelIniFile & "\"
    End If
    OciusSentinelIniFile = OciusSentinelIniFile & "Ocius.ini"
End Property

Private Property Get ValidLaunchOciusSentinelParameters() As String()
    ReDim VLOSP(1 To 12) As String

    VLOSP(1) = "AL"
    VLOSP(2) = "CRECNODISP"
    VLOSP(3) = "CUTCMD:"
    VLOSP(4) = "DISABLEFUNCTION"
    VLOSP(5) = "ENABLEVISIBLELOGOUT"
    VLOSP(6) = "EXTENDLOG"
    VLOSP(7) = "EXTVCH"
    VLOSP(8) = "PRINTER:"
    VLOSP(9) = "PRNPATH:"
    VLOSP(10) = "RAW"
    VLOSP(11) = "SKIPSTARTCONNFAIL"
    VLOSP(12) = "STARTMINIMISED"
    ValidLaunchOciusSentinelParameters = VLOSP
End Property
' End of Properties
    
' Private methods

Private Function LaunchOS() As Boolean
    Dim fsoFindOcius As New FileSystemObject

    If OciusSentinelLaunched Then
        LaunchOS = True
    Else
        If fsoFindOcius.FileExists(OciusSentinelApplication) Then
            ' Wrap path and args in quotes
            
            If Shell(Chr(34) & OciusSentinelApplication & Chr(34), vbMinimizedNoFocus) <> 0 Then
                LaunchOS = True
            End If
        End If
    End If
End Function

Private Function ModuleNameStub() As String

    ModuleNameStub = AppNameStub & ModuleName & "."
End Function

Private Function GetPEDTypeForCurrentOciusSentinelVersion() As PEDType
    
    GetPEDTypeForCurrentOciusSentinelVersion = myPEDTypeService.GetPEDType(moCommideaCfg.SoftwareVersion)
End Function

Private Sub MakeHumanReadableFailureReason(oAuthorizationRequest As AuthorizationRequest, oAutorizationResult As AuthorizationResult)
    
    If Not oAutorizationResult.Details Is Nothing Then
        Dim ResultTitle As String
        Dim ResultVerb As String
        Dim ResultReason As Integer
        Dim PleaseTryAgain As String
        PleaseTryAgain = vbNewLine & "Please try again."
        
        If oAuthorizationRequest.RequestType = Transaction Then
            Dim oOutputRecordV4 As cOutputTRecordV4
            Set oOutputRecordV4 = oAutorizationResult.Details
        
            With oOutputRecordV4
                If Not .OutputTRecordV3 Is Nothing Then
                    Select Case .TResult
                        Case TransactionResultNonErrorScreenless.etrnesAuthorised
                            ' Authorised, but auth code rejected,
                            ' otherwise the frmTransaction.Authorise
                            ' above returns true for an Authorise result
                            ResultTitle = "Transaction Rejected"
                            ResultVerb = "was rejected"
                        Case TransactionResultNonErrorScreenless.etrnesDeclined
                            ResultTitle = "Transaction Declined"
                            ResultVerb = "was declined"
                        Case TransactionResultNonErrorScreenless.etrnesReversed
                            ResultTitle = "Transaction Reversed"
                            ResultVerb = "was reversed"
                        Case ErrorID_UnspecifiedErrorCheckLog
                            If StrComp(.CustomerVerification, "invalid tokenid", vbTextCompare) = 0 Then
                                ResultTitle = "Cannot Refund Using Original Card Details"
                                ResultVerb = "failed"
                                If oAutorizationResult.FailureReason = "" Then
                                    oAutorizationResult.FailureReason = LCase(.CustomerVerification)
                                End If
                                RaiseEvent InvalidTokenID
                            Else
                                ResultTitle = "Error Processing Transaction"
                                ResultVerb = "failed"
                                If oAutorizationResult.FailureReason = "" Then
                                    oAutorizationResult.FailureReason = LCase(.CustomerVerification)
                                End If
                            End If
                        Case Is < 0
                            ' Referral 820.1
                            ' Can get other reasons why a CNP account on file refund fails
                            ' so catch them here, so will set up to allow operator to
                            ' attempt a different card type
                            If oAuthorizationRequest.transactionType = TRecordTransactionType.etrttRefund _
                            And oAuthorizationRequest.TRecordModifier = TRecordModifier.etrmCNPAccountOnFile Then
                                ResultTitle = "Cannot Refund Using Original Card Details"
                                ResultVerb = "failed"
                                If oAutorizationResult.FailureReason = "" Then
                                    oAutorizationResult.FailureReason = LCase(.CustomerVerification)
                                End If
                                RaiseEvent InvalidTokenID
                            Else
                                ResultTitle = "Error Processing Transaction"
                                ResultVerb = "failed"
                                If oAutorizationResult.FailureReason = "" Then
                                    oAutorizationResult.FailureReason = LCase(.CustomerVerification)
                                End If
                            End If
                            ' End of Referral 820
                        Case Else
                            ResultTitle = "Unknown result"
                            ResultVerb = "failed"
                            If oAutorizationResult.FailureReason = "" Then
                                oAutorizationResult.FailureReason = LCase(.CustomerVerification)
                            End If
                    End Select
                    
                    oAutorizationResult.FailureReason = ResultTitle & ";" & "The transaction " & ResultVerb & " (" & oAutorizationResult.FailureReason & ")"
                Else
                    oAutorizationResult.FailureReason = "Unknown Error;The transaction failed"
                End If
            End With
            oAutorizationResult.FailureReason = oAutorizationResult.FailureReason & PleaseTryAgain
        Else
            Dim responseRecord As BGiftResponseREcord
            Set responseRecord = oAutorizationResult.Details
            
            Select Case responseRecord.RecordResultCode
                Case BGiftResponseRecordResultCode.Rejected
                    ResultTitle = "Transaction Rejected"
                    ResultReason = responseRecord.RecordResponseCode
                
                Case BGiftResponseRecordResultCode.Reversed
                    ResultTitle = "Transaction Reversed"
                    ResultReason = responseRecord.RecordResponseCode
                    
                Case Is <= BGiftResponseRecordResultCode.Error
                    ResultTitle = "Transaction Error"
                    ResultReason = responseRecord.RecordResultCode
                    
                Case Else
                    ResultTitle = "Unknown result"
                    ResultReason = 0
                    
            End Select

            oAutorizationResult.FailureReason = ResultTitle & ";" & "Reason Code " & CStr(ResultReason) & ". " & responseRecord.AuthorisationMessage & ". "
        End If 'requestType = Transaction
    Else 'Not Result Is Nothing
        oAutorizationResult.FailureReason = "Unknown Error;The transaction failed" & PleaseTryAgain
    End If 'Not Result Is Nothing

End Sub

' End of Private methods
