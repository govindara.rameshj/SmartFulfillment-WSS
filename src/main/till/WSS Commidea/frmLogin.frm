VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "mswinsck.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmLogin 
   Caption         =   "Logging in"
   ClientHeight    =   585
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6285
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   585
   ScaleWidth      =   6285
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.ProgressBar pbrDownload 
      Height          =   600
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Visible         =   0   'False
      Width           =   6270
      _ExtentX        =   11060
      _ExtentY        =   1058
      _Version        =   393216
      Appearance      =   1
   End
   Begin MSWinsockLib.Winsock wskProgress 
      Left            =   420
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      RemotePort      =   25001
   End
   Begin MSWinsockLib.Winsock wskIntegration 
      Left            =   0
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      RemotePort      =   25000
   End
   Begin VB.Label lblActivity 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   204
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   0
      TabIndex        =   0
      Tag             =   "Logging on to Pin Entry Device"
      Top             =   0
      Width           =   6255
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Implements ICommideaForm

Private moCommideaCfg As CommideaConfiguration
Private myProgressMessage() As String
Private myReceivedMessage As String
Private myMessageReceived As Boolean
Private mblnInLaunchAndLogin As Boolean
Private mblnConnectionError As Boolean
Private myIntegrationError As String
Private myProgressError As String
Private myTimedOut As Boolean
Private myRestartRequired As Boolean
Private myPEDLicenceCancelled As Boolean    ' Referral 818 - Record whether licence has been rejected (because merchant name does not match the one supplied)

Public Event Duress()

' Control events

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If moCommideaCfg.UnderDuressKeyCode <> 0 And KeyCode = moCommideaCfg.UnderDuressKeyCode Then
        KeyCode = 0
        RaiseEvent Duress
    End If
End Sub

'Integration Socket events
Private Sub wskIntegration_ConnectionRequest(ByVal requestID As Long)

    ' Check if the control's State is closed. If not,
    ' close the connection before accepting the new
    ' connection.
    With wskIntegration
        If .State <> sckClosed Then
            .Close
        End If
        ' Accept the request with the requestID
        ' parameter.
        .Accept requestID
    End With
End Sub

Private Sub wskIntegration_DataArrival(ByVal bytesTotal As Long)
    Dim Received As String
    Dim Extra As String

    If bytesTotal > 0 Then
        Do
            Call wskIntegration.GetData(Extra, vbString)
            Received = Received & Extra
        Loop While Right(Received, 2) <> vbCrLf And Received <> "" And Asc(Left(Received, 1)) <> 6
        DebugMsg ModuleName, "wskIntegration_DataArrival", endlDebug, "Integration Message Received: " & Received
        If Received <> "" Then
            ' Make sure its the right format
            ' - Ocius sends Asc(6) to acknowledge receipt of message,
            ' but this often come out as part of the following data
            ' sent to winsock, sometimes it arrives on its own.  If
            ' remove it on own the message is now blank so will loop
            ' for more data; if together this will just strip it off
            If Asc(Left(Received, 1)) = 6 Then
                ' Remove the message delimiters
                Received = Mid(Received, 2)
            End If
            If Right(Received, 2) = vbCrLf Then
                ' Remove the message delimiters
                Received = Left(Received, Len(Received) - 2)
            End If
            ReceivedMessage = Received
        End If
    End If
End Sub

Private Sub wskIntegration_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)

    Select Case Number
        Case 1
            myIntegrationError = Trim(CStr(Number)) & ";" & Description & ";;Connection Error"
        Case 10060, 10061
            myIntegrationError = Trim(CStr(Number)) & ";" & Description & ";Please ensure Ocius is running and logged in;Ocius Sentinel Connection Error"
        Case Else
            myIntegrationError = Trim(CStr(Number)) & ";" & Description & ";Please check the connection cables, Network availability etc;General"
    End Select
    wskIntegration.Close
End Sub
' End of Integration Socket events

' Progress socket events
Private Sub wskProgress_ConnectionRequest(ByVal requestID As Long)

    ' Check if the control's State is closed. If not,
    ' close the connection before accepting the new
    ' connection.
    With wskProgress
        If .State <> sckClosed Then
            .Close
        End If
        ' Accept the request with the requestID
        ' parameter.
        .Accept requestID
    End With
End Sub

Private Sub wskProgress_DataArrival(ByVal bytesTotal As Long)
    Dim Received As String
    Dim Extra As String
    Dim Messages() As String
    Dim NextMess As Integer

    If bytesTotal > 0 Then
        Do
            Do While wskProgress.State <> sckConnected
                DoEvents
            Loop
            Call wskProgress.GetData(Extra, vbString)
            Received = Received & Extra
        Loop While Right(Received, 2) <> vbCrLf And Received <> ""
        If Received <> "" Then
            ' Make sure its the right format
            ' - Ocius sends Asc(6) to acknowledge receipt of message,
            ' but this often come out as part of the following data
            ' sent to winsock, sometimes it arrives on its own.  If
            ' remove it on own the message is now blank so will loop
            ' for more data; if together this will just strip it off
            If Asc(Left(Received, 1)) = 6 Then
                ' Remove the message delimiters
                Received = Mid(Received, 2)
            End If
            ' Remove the message delimiters
            Received = Left(Received, Len(Received) - 2)
            
            ' Might get many messages at once, so load them up
            Messages = Split(Received, vbCrLf)
            For NextMess = 0 To UBound(Messages)
                ReDim Preserve myProgressMessage(UBound(myProgressMessage) + 1) As String
                myProgressMessage(UBound(myProgressMessage)) = Messages(NextMess)
                Debug.Print "Progress Message Received: " & Messages(NextMess)
            Next NextMess
        End If
    End If
End Sub

Private Sub wskProgress_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)

    Select Case Number
        Case 1
            myProgressError = Trim(CStr(Number)) & ";" & Description & ";There was a problem connecting to the Pin Pad;Connection Error"
        Case 10060, 10061
            myProgressError = Trim(CStr(Number)) & ";" & Description & ";There was a problem connecting to the Pin Pad." & vbNewLine & "Please ensure Ocius is running and logged in;Ocius Sentinel Connection Error"
        Case Else
            myProgressError = Trim(CStr(Number)) & ";" & Description & ";Please check the connection cables, Network availability etc;General"
    End Select
    wskProgress.Close
End Sub
' End of Progress Socket events
' End of Control events

Public Function LogIn(ByVal UserID As String, ByVal UserPIN As String) As Boolean
    Dim LoginRecord As New cL2Record
    Dim Tries As Integer
    Dim oProgressMessage As cOutputProgressMessage
    Dim ErrorParts() As String
    Dim myReceivedMessage As cReceivedIntegrationMessage
    Dim ErrorReadingMessage As Boolean
    Dim LoginRequestSent As Boolean
    Dim DownloadFile As String
    Dim DownloadProgress As Integer
    Dim UpdateStart As Date
    Dim UpdateTime As Date
    Dim UpdateTaken As Long
    Dim LicenceMerchantName As String   ' Referral 818 - Used to compare against value supplied in PED message to see if can auto accept the licence
    Dim LicenceConfirmation As cContinueTransactionRecord   ' Referral 818 - Used to send continuation message confirming or rejecting the licence (because merchant name not match the one supplied)

    Call DebugMsg(ModuleName, "Login", endlTraceIn)
    Call DebugMsg(ModuleName, "Login", endlDebug, "Initialising data")
    ' Initialise this, so can see if been set or not
    UpdateStart = CDate("1/1/1111")
    ' Referral 818 - default to false and set true if get appropriate message
    myPEDLicenceCancelled = False
    ' set the time limit on logging in - in case port all ready in use by another program, etc
    ' dont want to be logging in forever
    ' Set up the sockets
    ReDim myProgressMessage(0) As String
    ReceivedMessage = ""
    ' Generate a login record to send to PED via socket
    Call LoginRecord.Initialise(UserID, UserPIN)
    ' Connect to sockets and if succeed then see what's what
    Call DebugMsg(ModuleName, "Login", endlDebug, "Firing up sockets")
    If FireUpSockets(moCommideaCfg.IntegrationPort, moCommideaCfg.ProgressPort) Then
        Do
            ' Reset flag, so if have a message/error can switch flag off. So no messages/error means must be a time out
            myTimedOut = True
            Call DebugMsg(ModuleName, "Login", endlDebug, "Waiting for data")
            WaitForData
            If ReceivedMessage <> "" Then
                Call DebugMsg(ModuleName, "Login", endlDebug, "Got integeration message : " & ReceivedMessage)
                myTimedOut = False      ' Have message so WaitForData did not time out
                If Not Me.Visible Then
                    Call Me.Show
                End If
                Set myReceivedMessage = New cReceivedIntegrationMessage
                ErrorReadingMessage = True
                Call DebugMsg(ModuleName, "Login", endlDebug, "Parsing message")
                If Not myReceivedMessage Is Nothing Then
                    With myReceivedMessage
                        Call .Initialise
                        If .ParseReceivedMessage(ReceivedMessage) Then
                            Me.lblActivity.Caption = .Message
                            ErrorReadingMessage = False
                            ' Just sent a login request and got a success message back, so must have logged in
                            If LoginRequestSent And .Result = 0 Then
                                LogIn = True
                                ' Pause so can see message
                                Call Wait(1)
                                Call DebugMsg(ModuleName, "Login", endlDebug, "Successful login message")
                            End If
                        End If
                    End With
                End If
                If ErrorReadingMessage Then
                    Me.lblActivity.Caption = "Unable to read Integration Output Message"
                    ' Pause so can see message
                    Call Wait(2)
                    Call DebugMsg(ModuleName, "Login", endlDebug, "Unable to parse message")
                End If
                Tries = 3
                LoginRequestSent = False
            ElseIf WinSockError <> "" Then
                myTimedOut = False      ' Have error so WaitForData did not time out
                Call DebugMsg(ModuleName, "Login", endlDebug, "Got socket error")
                ' Don't show error to start with, as most likely just
                ' waiting for the PED to get up to speed
                Tries = Tries + 1
                If Tries = 3 Then
                    If Not Me.Visible Then
                        Call Me.Show
                    End If
                    ErrorParts = Split(WinSockError, ";")
                    Call ClearError
                    Me.Caption = ErrorParts(1)
                    Me.lblActivity.Caption = ErrorParts(2) & " (" & ErrorParts(0) & ")"
                    Call DebugMsg(ModuleName, "Login", endlDebug, "Socket error; " & WinSockError)
                Else
                    If Not Me.Visible Then
                        Call Me.Show
                        Me.lblActivity.Caption = "Waiting for Pin Entry Device."
                    End If
                    Call DebugMsg(ModuleName, "Login", endlDebug, "Acceptable socket error; still waiting for PED")
                End If
                ' Pause so can see message
                Call Wait(2)
                Call ClearError
            ElseIf Not NextProgressMessage Is Nothing Then
                myTimedOut = False      ' Have message so WaitForData did not time out
                Call DebugMsg(ModuleName, "Login", endlDebug, "Got progress message : " & ReceivedMessage)
                Do While Not NextProgressMessage Is Nothing
                    Set oProgressMessage = ReadNextProgressMessage
                    If Not oProgressMessage Is Nothing Then
                        With oProgressMessage
                            If .Result = 100 Then
                                Select Case .StatusID
                                    Case esiLoginRequired
                                        Call DebugMsg(ModuleName, "Login", endlDebug, "Progress message : Login required")
                                        If Not Me.Visible Then
                                            Call Me.Show
                                        End If
                                        Me.lblActivity.Caption = "Logging on to Pin Entry Device"
                                        Call SendMessage(LoginRecord)
                                        LoginRequestSent = True
                                        Tries = Tries + 1
                                        Call ResetFocus(Me.Caption)
                                    Case esiReady
                                        Call DebugMsg(ModuleName, "Login", endlDebug, "Progress message : Ready")
                                        ' Now reading the integration output message
                                        ' from the Login request, probably won't get here
                                        ' after a successful login
                                        If Not Me.Visible Then
                                            Call Me.Show
                                        End If
                                        Me.lblActivity.Caption = "Logged In Successfully"
                                        Call Wait(1)
                                        LogIn = True
                                        Tries = 3
                                        Exit Do
                                    Case esiContinueRequired
                                        Call DebugMsg(ModuleName, "Login", endlDebug, "Progress message : Continue Required")
                                        ' Don't think this ever happens now
                                        Call SendContinueTransactionRecord
                                    Case esiInitialisingPED
                                        Call DebugMsg(ModuleName, "Login", endlDebug, "Progress message : Intialising PED")
                                        ' Let them know to switch it on etc
                                        If Not Me.Visible Then
                                            Call Me.Show
                                        End If
                                        Me.lblActivity.Caption = "Pin Entry Device still initialising."
                                        ' Pause so can see message, and give PED chance
                                        'to finish initialising
                                        Call Wait(1)
                                        Tries = 3
                                    Case esiUpdatingPED
                                        Call DebugMsg(ModuleName, "Login", endlDebug, "Progress message : Updating PED")
                                        ' Let them know to switch it on etc
                                        If Not Me.Visible Then
                                            Call Me.Show
                                        End If
                                        Me.lblActivity.Caption = "Pin Entry Device updating."
                                        ' Set the start time if not done already
                                        If DateDiff("d", UpdateStart, CDate("1/1/1111")) = 0 Then
                                            UpdateStart = Now
                                        End If
                                        ' Only advance 'Tries' after a length of time, to allow time for the PED
                                        ' to update.  '* 2' should mean get 6 times login timeout duration, as
                                        ' have 3 tries.  Special case to normal login timeout, when updating PED
                                        ' need longer to let update finish
                                        UpdateTaken = DateDiff("s", UpdateStart, Now)
                                        If UpdateTaken > moCommideaCfg.LoginTimeout * 2 Then
                                            Tries = Tries + 1
                                        End If
                                    Case esiPEDUnavailable
                                        Call DebugMsg(ModuleName, "Login", endlDebug, "Progress message : PED Unavailable")
                                        ' Let them know to switch it on etc
                                        If Not Me.Visible Then
                                            Call Me.Show
                                        End If
                                        Me.lblActivity.Caption = "PED Unavailable. On/connected?"
                                        ' Pause so can see message
                                        Call Wait(2)
                                        Tries = 3
                                    Case esiPerformingDownload
                                        Call DebugMsg(ModuleName, "Login", endlDebug, "Progress message : PED Performing Download")
                                        If Not Me.Visible Then
                                            Call Me.Show
                                        End If
                                        Me.Caption = Trim("Downloading" & " " & .GetParameter(epmpFilename))
                                        Me.lblActivity.Visible = False
                                        With Me.pbrDownload
                                            .Min = 0
                                            .Max = 100
                                            .Value = 0
                                            .Visible = True
                                        End With
                                        Call Wait(1)
                                    Case esiDownloadingFile
                                        Call DebugMsg(ModuleName, "Login", endlDebug, "Progress message : PED Downloading File")
                                        If Not Me.Visible Then
                                            Call Me.Show
                                            Me.lblActivity.Visible = False
                                            With Me.pbrDownload
                                                .Min = 0
                                                .Max = 100
                                                .Value = 0
                                                .Visible = True
                                            End With
                                        End If
                                        Me.Caption = Trim("Downloading" & " " & .GetParameter(epmpFilename))
                                        If IsNumeric(Replace(.GetParameter(epmpProgress), "%", "")) Then
                                            Me.pbrDownload.Value = CInt(Replace(.GetParameter(epmpProgress), "%", ""))
                                        End If
                                    Case esiDownloadComplete
                                        Call DebugMsg(ModuleName, "Login", endlDebug, "Progress message : PED Download Complete")
                                        If Not Me.Visible Then
                                            Call Me.Show
                                        End If
                                        Me.Caption = "Logging in"
                                        With Me.lblActivity
                                            .Caption = "Updating Pin Entry Device"
                                            .Visible = True
                                        End With
                                        With Me.pbrDownload
                                            .Min = 0
                                            .Max = 100
                                            .Value = 0
                                            .Visible = False
                                        End With
                                        Call Wait(1)
                                    Case esiRestartAfterSoftwareUpdate
                                        Call DebugMsg(ModuleName, "Login", endlDebug, "Progress message : PED Update Requires Restart")
                                        If Not Me.Visible Then
                                            Call Me.Show
                                        End If
                                        With Me.lblActivity
                                            .Caption = "Must restart after PED update"
                                            .Visible = True
                                        End With
                                        Call Wait(1)
                                        Tries = 3
                                        myRestartRequired = True
                                    ' Referral 818 - Manage request from PED to accept/reject new licence (occurs when partial install of ped software has been done)
                                    Case esiLicenceDetailConfirmation
                                        Call DebugMsg(ModuleName, "Login", endlDebug, "Progress message : PED Licence Confirmation Required")
                                        Call DebugMsg(ModuleName, "Login", endlDebug, "Extracting Licence Merchant Name from Message")
                                        LicenceMerchantName = .GetParameter(epmpMerchantName)
                                        Call DebugMsg(ModuleName, "Login", endlDebug, "Licence Merchant Name : " & LicenceMerchantName)
                                        Set LicenceConfirmation = New cContinueTransactionRecord
                                        If Len(LicenceMerchantName) > 0 And StrComp(LicenceMerchantName, moCommideaCfg.LicenceMerchantName, vbTextCompare) = 0 Then
                                            Call DebugMsg(ModuleName, "Login", endlDebug, "Accepting Licence")
                                            Call LicenceConfirmation.Initialise(etraAcceptLicenceKey)
                                        Else
                                            Call DebugMsg(ModuleName, "Login", endlDebug, "Rejecting Licence")
                                            If Not Me.Visible Then
                                                Call Me.Show
                                            End If
                                            With Me.lblActivity
                                                .Caption = "PED licensing cancelled."
                                                .Visible = True
                                            End With
                                            Call Wait(3)
                                            Tries = 3
                                            myPEDLicenceCancelled = True
                                            ' Only cancel to allow later acceptance
                                            Call LicenceConfirmation.Initialise(etraCancelLicenceKeyVerification)
                                        End If
                                        Call DebugMsg(ModuleName, "Login", endlDebug, "Sending Licence Accept/Reject message")
                                        Call SendMessage(LicenceConfirmation)
                                    Case Else
                                        Call DebugMsg(ModuleName, "Login", endlDebug, "Non specific message response")
                                        If Not Me.Visible Then
                                            Call Me.Show
                                        End If
                                        If LoginRecord.ResultMessage <> "" Then
                                            Me.lblActivity.Caption = LoginRecord.ResultMessage
                                        Else
                                            Me.lblActivity.Caption = "Unknown Pin Entry Device response"
                                        End If
                                        ' Pause so can see message
                                        Call Wait(2)
                                        Tries = 3
                                End Select
                            Else
                                ' error
                            End If
                        End With
                    Else
                        ' error
                    End If
                Loop
            Else
                ' Must have timed out
                myTimedOut = True
                Tries = 3
                Call DebugMsg(ModuleName, "Login", endlDebug, "Timed out waiting for log in")
            End If
        Loop While Tries < 3
        If Me.Visible Then
            Me.Hide
        End If
    End If
    
    Call DebugMsg(ModuleName, "Login", endlTraceOut)
End Function

Public Sub CloseMe()

    Call UnloadCommideaForm(Me)
End Sub

Public Property Get RestartRequired() As Boolean

    RestartRequired = myRestartRequired
End Property

' Referral 818  - Flag up whether the new licence has been accepted or rejected
Public Property Get PEDLicenceCancelled() As Boolean

    PEDLicenceCancelled = myPEDLicenceCancelled
End Property

Public Property Get TimedOut() As Boolean

    TimedOut = myTimedOut
End Property

' Referral 818  - If rejected licence the software is not yet ready.  Might be other reasons in future to add to this, hence separate property
Public Property Get SoftwareReady() As Boolean

    SoftwareReady = Not myPEDLicenceCancelled
End Property

Private Function SendMessage(ByRef IntegrationRecord As Object) As Boolean
    
    Call DebugMsg(ModuleName, "SendMessage", endlTraceIn)
    With Me.wskIntegration
        Select Case .State
            Case sckConnected
                Call DebugMsg(ModuleName, "SendMessage", endlDebug, "Calling Send")
                MessageReceived = False
                ReceivedMessage = ""
                Call .SendData(IntegrationRecord.ToIntegrationMessage)  'Send Transaction
                ' Make sure the data is actually sent, now
                DoEvents
                Call DebugMsg(ModuleName, "SendMessage", endlDebug, "Message Sent")
            Case sckError
                Call DebugMsg(ModuleName, "SendMessage", endlDebug, "Error Found")
            Case Else
        End Select
    End With
    Call DebugMsg(ModuleName, "SendMessage", endlTraceOut)
End Function ' SendMessage

Private Sub SendContinueTransactionRecord()
    Dim ContinueTRec As New cContinueTransactionRecord
    
    If Not ContinueTRec Is Nothing Then
        Call ContinueTRec.Initialise(ContinueTRecordAction.etraContinueTxn)
        With wskIntegration
            Call DebugMsg(ModuleName, "SendContinueTransactionRecord", endlDebug, "Integration Winsock state is currently " & .State)
            Select Case .State
                Case sckConnected
                    Call DebugMsg(ModuleName, "SendContinueTransactionRecord", endlDebug, "Sending Continue Transaction Record - Action ID = " & Trim(CStr(ContinueTRecordAction.etraContinueTxn)))
                    ' Now send the new transaction
                    Call .SendData(ContinueTRec.ToIntegrationMessage)  'Send Transaction
                    ' Make sure the data is actually sent, now
                    DoEvents
                    Call DebugMsg(ModuleName, "SendContinueTransactionRecord", endlDebug, "Sent Continue Transaction Record - Action ID = " & Trim(CStr(ContinueTRecordAction.etraContinueTxn)))
                Case sckError
            End Select
        End With
    End If
End Sub

Private Property Get MessageReceived() As Boolean

    MessageReceived = myMessageReceived
End Property

Private Property Let MessageReceived(ByVal NewMessageReceived As Boolean)

    myMessageReceived = NewMessageReceived
End Property

Private Property Get ReceivedMessage() As String

    ReceivedMessage = myReceivedMessage
End Property

Private Property Let ReceivedMessage(ByVal NewReceivedMessage As String)

    If NewReceivedMessage = "" Then
        myReceivedMessage = NewReceivedMessage
    Else
        ' Sometimes just get a single Asc() = 6 character before message,
        ' this can be ignored (treated as though blank)
        If Len(NewReceivedMessage) = 1 And Asc(NewReceivedMessage) = 6 Then
            myReceivedMessage = ""
        Else
            myReceivedMessage = NewReceivedMessage
        End If
    End If
End Property

Private Property Get ProgressMessage() As String()

    ProgressMessage = myProgressMessage
End Property

Private Function NextProgressMessage() As cOutputProgressMessage

    If UBound(ProgressMessage()) > 0 Then
        Set NextProgressMessage = New cOutputProgressMessage
        If Not NextProgressMessage Is Nothing Then
            With NextProgressMessage
                .Initialise
                ' Always the 1st message as move all messages up 1 after this one is processed
                If Not .ParseReceivedMessage(ProgressMessage()(1)) Then
                    NextProgressMessage = Nothing
                End If
            End With
        End If
    End If
End Function

Private Function ReadNextProgressMessage() As cOutputProgressMessage
    Dim Delete As Integer

    If UBound(ProgressMessage()) > 0 Then
        ' read the message and then remove read message from list
        ' read...
        Set ReadNextProgressMessage = NextProgressMessage
        ' remove...
        ' Move message up one (overwriting the 1st that has now been processed)
        For Delete = 1 To UBound(myProgressMessage) - 1
            myProgressMessage(Delete) = myProgressMessage(Delete + 1)
        Next Delete
        ' Remove duplicate last message from end
        ReDim Preserve myProgressMessage(UBound(myProgressMessage) - 1) As String
    Else
        Set ReadNextProgressMessage = Nothing
    End If
End Function

Private Function NextProgressMessageIs(ByVal IsID As ProgressStatusID) As Boolean
    Dim NextMess As cOutputProgressMessage

    Set NextMess = NextProgressMessage
    If Not NextMess Is Nothing Then
        If NextMess.StatusID = IsID Then
            NextProgressMessageIs = True
        End If
    End If
End Function

Private Function ConnectionError() As Boolean

    If WinSockError <> "" Then
        ConnectionError = True
    End If
End Function

Private Function WinSockError() As String

    If myIntegrationError <> "" Then
        WinSockError = myIntegrationError
    Else
        If myProgressError <> "" Then
            WinSockError = myProgressError
        End If
    End If
End Function

Private Sub ClearError()

    myIntegrationError = ""
    myProgressError = ""
End Sub

Private Function FireUpSockets(ByVal IntegrationPort As String, ByVal ProgressPort As String) As Boolean

    ' Set up the progress socket to receive data
    With Me.wskProgress
        Call .Close
        .RemotePort = ProgressPort
        .RemoteHost = "127.0.0.1"    ' .LocalIP
        Call DebugMsg(ModuleName, "FireUpSockets", endlDebug, "Progress Winsock state is currently " & .State)
        Call DebugMsg(ModuleName, "FireUpSockets", endlDebug, "Progress Winsock connecting to " & "127.0.0.1")    ' .LocalIP)
        Call .Connect
        While .State = sckConnecting
            DoEvents
        Wend
        Call DebugMsg(ModuleName, "FireUpSockets", endlDebug, "Progress Winsock state is currently " & .State)
    End With
    With Me.wskIntegration
        Call .Close
        .RemotePort = IntegrationPort
        .RemoteHost = "127.0.0.1"    ' .LocalIP
        Call DebugMsg(ModuleName, "FireUpSockets", endlDebug, "Integration Winsock state is currently " & .State)
        Call DebugMsg(ModuleName, "FireUpSockets", endlDebug, "Integration Winsock connecting to " & "127.0.0.1")    ' .LocalIP)
        Call .Connect
        While .State = sckConnecting
            DoEvents
        Wend
        Call DebugMsg(ModuleName, "FireUpSockets", endlDebug, "Integration Winsock state is currently " & .State)
    End With
    FireUpSockets = Me.wskIntegration.State = sckConnected And Me.wskProgress.State = sckConnected
End Function

Private Sub WaitForData()
    Dim Start As Date
    Dim Finish As Date
    Dim TimeTaken As Integer

    Start = Time
    Call DebugMsg(ModuleName, "WaitForData", endlDebug, "Starting Wait loop at " & Format(Start, "h:nn:ss") & ".")
    Do
        Call Wait(1)
        DoEvents
        Finish = Time
        TimeTaken = ((Minute(Finish) * 60) + Second(Finish)) - ((Minute(Start) * 60) + Second(Start))
    Loop While ReceivedMessage = "" And NextProgressMessage Is Nothing And Not ConnectionError And TimeTaken < moCommideaCfg.LoginTimeout
    Call DebugMsg(ModuleName, "WaitForData", endlDebug, "Finished Wait loop at " & Format(Finish, "h:nn:ss") & ".")
    If ReceivedMessage <> "" Then
        MessageReceived = True
    End If
End Sub 'WaitForData

Private Function ModuleName() As String

    ModuleName = "frmLogin"
End Function


Public Sub ICommideaForm_InitializeBeforeLoad(ByVal commideaCfg As CommideaConfiguration)
    Set moCommideaCfg = commideaCfg
End Sub

Public Sub ICommideaForm_ResetAfterUnload()
    Set moCommideaCfg = Nothing
End Sub
