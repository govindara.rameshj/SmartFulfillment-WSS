VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AuthorizationResult"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' details of commidea transaction
Private mDetails As IAuthorizationDetails

' Build up error
Private mFailureReason As String

' Store the formatted lines on a commidea receipt
Private mReceipts() As String

' Store successful completion of commidea transaction
Private mCompleted As Boolean

Public Sub Init()
    mCompleted = False
    ' Set here, in case user cancels transaction before any receipt printing done, so will always have
    ' a boundary to the dimension to check against
    ReDim mReceipts(0)

End Sub

Public Property Get Completed() As Boolean
    Completed = mCompleted
End Property

Friend Property Let Completed(ByVal newCompleted As Boolean)
    mCompleted = newCompleted
End Property

Public Property Get FailureReason() As String
    FailureReason = mFailureReason
End Property

Friend Property Let FailureReason(ByVal newFailureReason As String)
    mFailureReason = newFailureReason
End Property

Public Property Get Receipts() As String()
    Receipts = mReceipts
End Property

Friend Property Let Receipts(ByRef newReceipts() As String)
    mReceipts = newReceipts
End Property

Public Property Get Details() As IAuthorizationDetails
    Set Details = mDetails
End Property

Friend Property Set Details(ByVal newDetails As IAuthorizationDetails)
    Set mDetails = newDetails
End Property




