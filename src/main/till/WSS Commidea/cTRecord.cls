VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cTRecord"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IRequestRecord

Public Enum TRecordTransactionType
    etrttPurchase = 1
    etrttRefund = 2
    etrttCashAdvance = 4
End Enum

'The modifier meanings have been updated to
'account for the different types of CNP transactions:
Public Enum TRecordModifier
    etrmCardholderPresent = 0
    etrmOfflineCapture = 2
    etrmCNPMailOrder = 8
    etrmAuthorisationOnly = 10
    etrmRecoveredTransaction = 40
    etrmAllowZeroValuePreAuth = 80
    etrmUnattendedDevice = 100
    etrmCPCMinusIAndLRecords = 400
    etrmAllowElectronCNPTeleSpreadBetting = 800
    etrmCNPTelephoneOrder = 1000
    etrmCNPAccountOnFile = 2000
End Enum

Public Enum TRecordAccountOnFileRegistration
    etraofrNotSet = 0
    etraofrDoNotRegister = 1
    etraofrRegister = 2    ' Must use if want TokenID returned (3 doesn't bring back tokenid?)
    etraofrRegisterOnly = 3
    etraofrRegisterDeclineTxnIfFail = 4
End Enum


' Account Number This field is no longer used. Field 24, Account ID, is
'used to define the merchant parameter set to use.
'If populated this field will be ignored.
Private myAccountNumber As String
' Mandatory
Private myTransactionType As TRecordTransactionType
' Mandatory
Private myModifier As TRecordModifier
' Reserved, leave empty
Private myPoSRoutingBillID As String
' Reserved, leave empty
Private myPANTrack2 As String
' Reserved, leave empty
Private myCSC As String
' Reserved, leave empty
Private myExpiryDate As String
' Reserved, leave empty
Private myIssueNo As String
' Reserved, leave empty
Private myStartDate As String
' Total value of the transaction applies to: Purchase,
' Mandatory Refund, Cheque Guarantee, and Cash Advance.
' Provision of the decimal point is recommended
' although optional. For example:
' 1.23 = pound1.23
' 123=pound123.00,
' 000001.2389 = pound1.23.
' Values should always be positive, and will be
' truncated to the correct number of decimal places
' required for the currency. For example: 1.23 =
' 1Yen (one Japanese Yen) (0 decimal places)).
Private myTxnValue As String
'  Reserved for future use (not currently supported)
Private myCashbackValue As String
' Reserved, leave empty
Private myBankAccNo As String
' Reserved, leave empty
Private mySortCode As String
' Reserved, leave empty
Private myChequeNo As String
' Reserved, leave empty
Private myChequeType As String
' Reserved, leave empty
Private myCardholderName As String
' Reserved, leave empty
Private myCardholderBillingAddress As String
' Reserved, leave empty
Private myEFTSN As String
' Reserved, leave empty
Private myAuthSource As String
' Reserved, leave empty
Private myAuthCode As String
' Reserved, leave empty
Private myTxnDateTime  As String
' Reference numbers can be supplied up to a
' maximum length of 50 characters.
' When forwarded to the acquirer these are truncated
' to 25 characters
' Using WSS Unique Transaction Tender Reference here
Private myReference As String
' Optional
' Defines merchant parameter set to be used
Private myAccountID As String
' Reserved for future use (not currently supported)
Private myGratuity As String
' Reserved for future use (not currently supported)
Private myNDIValue
' When performing a 'Register Only' registration,
' the value of the transaction must be set to '0.00'
' and a modifier of '0000' (Cardholder Present) used
Private myRegisterForAccountOnFile As TRecordAccountOnFileRegistration
' Token ID value if processing an Account On File Payment
Private myTokenID As String

Private myTransactionSuccessful As Boolean
' Identify the format of output record
Private myIsV2 As Boolean
' Supply either version of output record
Private myOutputTRecord As cOutputTRecord
Private myOutputTRecordV2 As cOutputTRecordV2
' Result of transaction in form of transaction output message
Private myReceivedMessage As String
Private myResultMessage As String
' Collection of Progress Messages associated with a particular transaction
Private myProgressMessages As Collection

Public Sub Initialise(ByVal transactionType As TRecordTransactionType, _
                      ByVal Modifier As TRecordModifier, _
                      ByVal TxnValue As Double, _
                      ByVal WSSUniqueTransTenderRef As String, _
                      Optional ByVal RegisterForAOF As TRecordAccountOnFileRegistration = TRecordAccountOnFileRegistration.etraofrDoNotRegister, _
                      Optional ByVal AOFTokenID As String = "", _
                      Optional ByVal AccountID As String = "", _
                      Optional ByVal IsV2 As Boolean = True, _
                      Optional ByVal IsScreenless As Boolean = True)

    myTransactionType = transactionType
    myModifier = Modifier
    myTxnValue = FormatTransactionAmount(TxnValue)
    myReference = WSSUniqueTransTenderRef
    myRegisterForAccountOnFile = RegisterForAOF
    Select Case myRegisterForAccountOnFile
        Case etraofrRegisterOnly
            myTxnValue = "0.00"
            myModifier = etrmCardholderPresent
    End Select
    myTokenID = AOFTokenID
    myAccountID = AccountID
    If IsV2 Then
        Set myOutputTRecordV2 = New cOutputTRecordV2
        Call myOutputTRecordV2.Initialise(IsScreenless)
    Else
        Set myOutputTRecord = New cOutputTRecord
        Call myOutputTRecord.Initialise(IsScreenless)
    End If
    myIsV2 = IsV2
End Sub

Public Function IRequestRecord_ToIntegrationMessage() As String

    'Pass empty AccountId as pre-last parameter
    IRequestRecord_ToIntegrationMessage = "T," _
                         & myAccountNumber & "," _
                         & transactionType & "," _
                         & Modifier & "," _
                         & myPoSRoutingBillID & "," _
                         & myPANTrack2 & "," _
                         & myCSC & "," _
                         & myExpiryDate & "," _
                         & myIssueNo & "," _
                         & myStartDate & "," _
                         & myTxnValue & "," _
                         & myCashbackValue & "," _
                         & myBankAccNo & "," _
                         & mySortCode & "," _
                         & myChequeNo & "," _
                         & myChequeType & "," _
                         & myCardholderName & "," _
                         & myCardholderBillingAddress & "," _
                         & myEFTSN & "," _
                         & myAuthSource & "," _
                         & myAuthCode & "," _
                         & myTxnDateTime & "," _
                         & myReference & "," _
                         & "" & "," _
                         & myGratuity & ","
    IRequestRecord_ToIntegrationMessage = IRequestRecord_ToIntegrationMessage _
                         & myNDIValue & "," _
                         & RegisterForAccountOnFile & "," _
                         & myTokenID & vbCrLf
End Function

' TODO seems only ToIntegrationMessage() is used actually. May be we should delete all antoher public members?
Public Property Get OutputTRecord() As cOutputTRecord

    Set OutputTRecord = myOutputTRecord
End Property

Public Property Get OutputTRecordV2() As cOutputTRecordV2

    Set OutputTRecordV2 = myOutputTRecordV2
End Property

Public Property Get ProgressMessages() As Collection

    Set ProgressMessages = myProgressMessages
End Property

Public Property Get ReceivedMessage() As String

    ReceivedMessage = myReceivedMessage
End Property

Public Property Let ReceivedMessage(ByVal NewMessage As String)

    myReceivedMessage = NewMessage
    If myIsV2 Then
        With myOutputTRecordV2
            If .ParseReceivedMessage(myReceivedMessage) Then
                Select Case .TResult
                    Case Is >= 0
                        myTransactionSuccessful = True
                    Case Else
                        myTransactionSuccessful = False
                End Select
            Else
                ' error failed parsing message received from PED
            End If
            ' Status/Error message returned here
            myResultMessage = .CustomerVerification
        End With
    Else
        With myOutputTRecord
            If .ParseReceivedMessage(myReceivedMessage) Then
                Select Case .TResult
                    Case Is >= 0
                        myTransactionSuccessful = True
                    Case Else
                        myTransactionSuccessful = False
                End Select
            Else
                ' error failed parsing message received from PED
            End If
        End With
    End If
End Property

Public Property Get ResultMessage() As String

    ResultMessage = myResultMessage
End Property

Public Property Get TransactionSuccessful() As Boolean

    TransactionSuccessful = myTransactionSuccessful
End Property

Private Property Get Modifier() As String

    Modifier = Right$("0000" & Trim(CStr(myModifier)), 4)
End Property

Private Property Get RegisterForAccountOnFile() As String

    RegisterForAccountOnFile = Trim(CStr(myRegisterForAccountOnFile))
End Property

Private Property Get transactionType() As String

    transactionType = Right$("00" & Trim(CStr(myTransactionType)), 2)
End Property
