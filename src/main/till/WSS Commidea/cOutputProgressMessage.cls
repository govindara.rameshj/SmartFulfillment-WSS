VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cOutputProgressMessage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Const ModuleName As String = "cOutputProgressMessage"

Public Type ProgressMessageParameter
    Name As String
    Value As String
End Type

Private Enum ParameterID
    epResult = 0
    epTerminateLoop = 1
    epStatusID = 2
    epStatus = 3
    epParameters = 4
End Enum

Public Enum ProgressStatusID
    esiProcessingTxn = 0
    esiWaitingForGratuity
    esiGratuityBeingEntered
    esiPresentCard
    esiSwipeCard
    esiCardInserted
    esiCardRemoved
    esiCardProcessing
    esiChangeCard
    esiContactTxnRequired
    esiKeyInCardDetails
    esiWaitingForCashback
    esiPinEntry
    esiRiskManagementComplete
    esiAuthorisingTxn
    esiWaitingForResult
    esiAuthResultReceived
    esiPrintingMerchantReceipt
    esiSignatureConfirmationRequired
    esiContinueRequired
    esiConfirmAuthCode
    esiConfirmingTxn
    esiRejectingTxn
    esiFinalResultReceived
    esiVoiceReferral
    esiRemoveCard
    esiAuthResultError
    esiFallbackToSwipeDisabled
    esiDownloadingFile
    esiUpdatingPED
    esiInvalidPEDConfig
    esiCardDataRetrieval
    esiStartingTransaction
    esiPerformingDownload
    esiRequestingReport
    esiGratuitySelectionRequired
    esiExpiryDateRequired
    esiStartDateRequired
    esiIssueNumberRequired
    esiAVSHouseNumberRequired
    esiAVSPostCodeRequired
    esiCSCRequired
    esiCustomerPresentNotPresentSelectionRequired
    esiCustomerNotPresentOptionSelectionRequired
    esiEnterChargeAuthCode
    esiLoginRequired
    esiReady
    esiCardNotAccepted
    esiCardBlocked
    esiTransactionCancelled
    esiInvalidExpiry
    esiGrauityInvalid
    esiInvalidCard
    esiPrintingCustomerReceipt
    esiInitialisingPED
    esiPEDUnavailable
    esiCardAppSelection
    esiRetryDownload
    esiRestartAfterSoftwareUpdate
    esiRequestingDCC
    esiDCCCurrencyChoice
    esiCardholderDCCCurrencyChoice
    esiUnsafeDownload
    esiUnexpectedLogin
    esiStartBarclaysBonusTxn
    esiUpdateBarclaysBonusTxn
    esiCancelBarclaysBonusTxn
    esiConfirmGratuity
    esiRegisterForAccountOnFileDecision
    esiAwaitingTokenID
    esiBarclaysBonusDiscountSummary
    esiBarclaysBonusUseBonus
    esiBarclaysBonusEnterRedemption
    esiBarclaysBonusNotAvailable
    esiDownloadComplete
    esiDownloadStillBeingPrepared
    esiServerConnectionFailed
    esiResumeDownload
    esiPayPointAccountExtractionFailed
    esiPayPointAmountOutsideAllowedRange
    esiPayPointCardExpired
    esiPayPointInitialised
    esiPayPointInitialisationFailed
    esiPayPointInitialising
    esiPayPointInvalidAccount
    esiPayPointInvalidAmount
    esiPayPointInvalidCaptureMethod
    esiPayPointInvalidCardNumber
    esiPayPointInvalidConfiguration
    esiPayPointInvalidDenomination
    esiPayPointInvalidExpiryDate
    esiPayPointInvalidScheme
    esiPayPointInvalidSchemeOption
    esiPayPointInvalidTopupType
    esiPayPointInvalidServiceProvider
    esiPayPointInvalidTrack2Format
    esiPayPointInvalidTransactionType
    esiPayPointKeyedEntryNotAllowed
    esiPayPointMerchantReferenceRequired
    esiPayPointNoAccounts
    esiPayPointProcessingTransaction
    esiPayPointRetryConfirmationDecision
    esiPayPointSchemeNotRecognised
    esiPayPointTransactionCancelled
    esiPayPointTransactionTypeNotAllowed
    esiPayPointSelectSchemeOption
    esiPayPointDownloadRequired
    esiPayPointSelectAccount
    esiPrintingPayPointReceipt
    esiLicenceDetailConfirmation
    esiLicenceFileRequired
    esiPayPointServiceUnavailable
    esiParkRetailGiftAccountExtractionFailed
    esiesiParkRetailGiftAmountOutsideAllowedRange
    esiParkRetailGiftCardExpired
    esiParkRetailGiftInitialisationFailed
    esiParkRetailGiftInitialising
    esiParkRetailGiftInvalidAccount
    esiParkRetailGiftInvalidAmount
    esiParkRetailGiftInvalidCaptureMethod
    esiParkRetailGiftInvalidCardNumber
    esiParkRetailGiftInvalidConfiguration
    esiParkRetailGiftInvalidExpiryDate
    esiParkRetailGiftInvalidTrack2Format
    esiParkRetailGiftInvalidTransactionType
    esiParkRetailGiftKeyedEntryNotAllowed
    esiParkRetailGiftMerchantReferenceRequired
    esiParkRetailGiftNoAccounts
    esiParkRetailGiftServiceUnavailable
    esiParkRetailGiftProcessingTransaction
    esiParkRetailGiftSchemeNotRecognised
    esiParkRetailGiftSelectAccount
    esiParkRetailGiftTransactionCancelled
    esiParkRetailGiftTransactionTypeNotAllowed
    esiPrintingParkRetailGiftReceipt
    esiPEDInESDRecoveryMode
    esiUpdateTransactionValueDecision
    esiUpdateBarclaycardFreedomConfig
    esiProcessingKeyExchange
    esiBarclaysGiftInitialising
    esiGlobalBlueTaxFreeShoppingInitialising
    esiGiveXInitialising
    esiMVoucherInitialising
    esiPerformingPostConfirmReversal
    esiInvalidAmount
    esiMerchantReferenceRequired
    esiMerchantReferenceLengthInvalid
    esiInitialising
    esiAccountExtractionFailed
    esiAmountOutsideAllowedRange
    esiEnterAmount
    esiConfirmAuthorisation
    esiCardExpired
    esiTransaxSVInitialisationFailed
    esiTransaxSVInitialising
    esiInvalidAccount
    esiInvalidCaptureMethod
    esiInvalidCardNumber
    esiInvalidConfiguration
    esiInvalidExpiryDate
    esiInvalidTrack2Format
    esiInvalidTransactionType
    esiKeyedEntryNotAllowed
    esiNoAccounts
    esiCardSchemeNotRecognised
    esiTransactionTypeNotAllowed
    esiTransaxSVServiceUnavailable
    esiInvalidIssueNumber
    esiInvalidCardSecurityCode
    esiConfirmationFailed
    esiPrintingReceipt
    esiWaitingForDonation
    esiPinBlocked
    esiPinTryLimitExceeded
    esiFurtherPaymentRequired
    esiSVSInitialising
    esiSVSInitialisationFailed
    esiSVSInvalidConfiguration
    esiSVSServiceUnavailable
    esiCannotContinueTransaction
    esiPartPayment
    esiObtainSTAN
    esiInvalidSTAN
    esiIVRRequired
    esiPromptForAuthorisationCode
    esiInvalidAuthCode
    esiOfflineTxnFailed
    esiEnterReference
    esiPrintShopCopy
    esiMustInsertCard
    esiPINEntered
    esiCardSwiped
    esiPINBypassed
    esiCancellingTransaction
    esiCardPresented
    esiServerError
    esiOnHoldTransaction
End Enum

Public Enum ProgressMessageParameterType
    epmpTxnValue = 1
    epmpCashbackValue = 2
    epmpGratuityValue = 3
    epmpTotalAmount = 4
    epmpAuthCode = 5
    epmpVRTelNo = 6
    epmpCard = 7
    epmpExpiry = 8
    epmpMerchantID = 9
    epmpTerminalID = 10
    epmpFilename = 11
    epmpProgress = 12
    epmpSchemeID = 13
    epmpMerchantName = 14
End Enum

' Property fields
Private myResult As Integer
Private myTerminateLoop As Integer
Private myStatusID As String
Private myStatus As String
Private myMessageParameters As Collection
' End of property fields

Private myParameters As Collection

' Public methods

Public Sub Initialise()

    Set myParameters = New Collection
    Call BuildDataArray
End Sub
' End of Public methods

' Properties

' Result
' This indicates the result code (100)
Public Property Get Result() As Integer

    Result = myResult
End Property

Public Property Let Result(ByVal newResult As Integer)
    
    myResult = newResult
End Property
    
' 2 Terminate loop
' Reserved - ignore
Public Property Get TerminateLoop() As Integer

    TerminateLoop = myTerminateLoop
End Property

Public Property Let TerminateLoop(ByVal NewTerminateLoop As Integer)
    
    myTerminateLoop = NewTerminateLoop
End Property

' 3 Status ID - indicates the progress status.
Public Property Get StatusID() As ProgressStatusID

    StatusID = myStatusID
End Property

Public Property Let StatusID(ByVal NewStatusID As ProgressStatusID)
    
    myStatusID = NewStatusID
End Property

' 4 Status
' A text representation of the progress status
Public Property Get Status() As String

    Status = myStatus
End Property

Public Property Let Status(ByVal NewStatus As String)
    
    myStatus = NewStatus
End Property

' Referral 818 - Added missing MERCHANT NAME parameter
' 5 Parameters
' This field will contain any information that the PoS may require that is
' associated with that status. Each parameter will be ';' delimited and will
' be defined as follows:
' <Name>=<Value>
' Here is a list of all possible parameters:
'   TXN Value; CASHBACK Value; GRATUITY Value;
'   TOTAL AMOUNT; AUTH CODE; VR TEL NO; CARD;
'   EXPIRY; MID; TID; FileName; PROGRESS;
'   SCHEME ID; MERCHANT NAME
' Stored as a Collection of ProgressMessageParameters
Public Property Get Parameters() As Collection

    Set Parameters = myMessageParameters
End Property

Public Property Let Parameters(ByVal NewParameters As Collection)

    Set myMessageParameters = NewParameters
End Property

' Non parameter based properties

Public Property Get ValidProgressMessageParameters() As String()
    ReDim VPMP(1 To 14) As String
    
    VPMP(epmpTxnValue) = "TXN VALUE"
    VPMP(epmpCashbackValue) = "CASHBACK VALUE"
    VPMP(epmpGratuityValue) = "GRATUITY VALUE"
    VPMP(epmpTotalAmount) = "TOTAL AMOUNT"
    VPMP(epmpAuthCode) = "AUTH CODE"
    VPMP(epmpVRTelNo) = "VR TEL NO"
    VPMP(epmpCard) = "CARD"
    VPMP(epmpExpiry) = "EXPIRY"
    VPMP(epmpMerchantID) = "MID"
    VPMP(epmpTerminalID) = "TID"
    VPMP(epmpFilename) = "FILENAME"
    VPMP(epmpProgress) = "PROGRESS"
    VPMP(epmpSchemeID) = "SCHEMEID"
    VPMP(epmpMerchantName) = "MERCHANT NAME"
    ValidProgressMessageParameters = VPMP
End Property

 ' This is used to parse the output record.
' It is the number of returned parameters from Commidea
Public Property Get NumberOfParameters() As Integer

    NumberOfParameters = 5
End Property

Public Property Get CustomerFacingMessage() As String

    ' NB these to come from xml file if it exists
    Select Case StatusID
        Case 1
            CustomerFacingMessage = "Enter Gratuity"
        Case 3
            CustomerFacingMessage = "Present Card / Insert Card"
        Case 4
            CustomerFacingMessage = "Swipe Card / Remove Card"
        Case 5, 6, 14, 15, 16, 17, 18, 21, 23, 32, 34, 53, 54, 59, 98, 100, 101, 109
            CustomerFacingMessage = "Please Wait"
        Case 7
            CustomerFacingMessage = "Do Not Remove Card"
        Case 8
            CustomerFacingMessage = "Use Alternative Payment Method"
        Case 10
            CustomerFacingMessage = "Key Card Number"
        Case 11
            CustomerFacingMessage = "Enter Cashback"
        Case 12
            CustomerFacingMessage = "Enter PIN"
        Case 22
            CustomerFacingMessage = "Declined"
        Case 24
            CustomerFacingMessage = "Referral"
        Case 25
            CustomerFacingMessage = "Remove Card"
        Case 28, 29, 33, 83, 110
            CustomerFacingMessage = "Loading"
        Case 36
            CustomerFacingMessage = "Expires MM/YY"
        Case 37
            CustomerFacingMessage = "Valid From MM/YY"
        Case 38
            CustomerFacingMessage = "Issue Number"
        Case 39
            CustomerFacingMessage = "Enter House Number"
        Case 40
            CustomerFacingMessage = "Enter Post Code"
        Case 41
            CustomerFacingMessage = "Enter Card Security Code"
        Case 42
            CustomerFacingMessage = "Customer Present? Yes/No"
        Case 44
            CustomerFacingMessage = "Enter Charge Auth Code"
        Case 46
            CustomerFacingMessage = "Ready"
        Case 47, 80, 104
            CustomerFacingMessage = "Not Accepted"
        Case 49
            CustomerFacingMessage = "Transaction Void"
        Case 56
            CustomerFacingMessage = "Select Payment Type"
        Case 61
            CustomerFacingMessage = "Please select currency"
        Case 79
            CustomerFacingMessage = "Invalid Amount"
        Case 102
            CustomerFacingMessage = "Invalid Scheme"
        Case 103
            CustomerFacingMessage = "Transaction Void"
        Case 105
            CustomerFacingMessage = "Select Scheme"
        Case 108
            CustomerFacingMessage = "Printing Receipt"
        Case 212
            CustomerFacingMessage = "Failed To Read"
        Case Else
            If StatusID > 197 Then
                CustomerFacingMessage = "Unknown"
            Else
                CustomerFacingMessage = ""
            End If
    End Select
End Property

Public Property Get OperatorMessage() As String
    Dim Param As Integer

    Select Case StatusID
        Case 0
            OperatorMessage = "Processing Transaction"
        Case 1
            OperatorMessage = "Waiting For Gratuity"
        Case 2
            OperatorMessage = "Gratuity Being Entered"
        Case 3
            OperatorMessage = "Awaiting Card"
        Case 4
            OperatorMessage = "Swipe Card"
        Case 5
            OperatorMessage = "Card Inserted"
        Case 6
            OperatorMessage = "Card Removed"
        Case 7
            OperatorMessage = "Card Processing"
        Case 8
            OperatorMessage = "Change Card"
        Case 9
            OperatorMessage = "Contact Transaction Required"
        Case 10
            OperatorMessage = "Key In Card Details"
        Case 11
            OperatorMessage = "Waiting For Cashback"
        Case 12
            OperatorMessage = "Pin Entry"
        Case 13
            OperatorMessage = "Risk Management Complete"
        Case 14
            OperatorMessage = "Authorising Transaction"
        Case 15
            OperatorMessage = "Waiting For Result"
        Case 16
            OperatorMessage = "Auth Result Received"
        Case 17
            OperatorMessage = "Printing Merchant Receipt"
        Case 18
            OperatorMessage = "Signature Confirmation Required"
        Case 19
            OperatorMessage = "Continue Required"
        Case 20
            OperatorMessage = "Confirm Auth Code"
        Case 21
            OperatorMessage = "Confirming Transaction"
        Case 22
            OperatorMessage = "Rejecting Transaction"
        Case 23
            OperatorMessage = "Final Result Received"
        Case 24
            OperatorMessage = "Voice Referral"
        Case 25
            OperatorMessage = "Remove Card"
        Case 26
            OperatorMessage = "Auth Result Error"
        Case 27
            OperatorMessage = "Fallback To Swipe Disabled"
        Case 28
            OperatorMessage = "Downloading File"
        Case 29
            OperatorMessage = "Updating PED"
        Case 30
            OperatorMessage = "Invalid PED Configuration"
        Case 31
            OperatorMessage = "Card Data Retrieval"
        Case 32
            OperatorMessage = "Starting Transaction"
        Case 33
            OperatorMessage = "Performing Download"
        Case 34
            OperatorMessage = "Requesting Report"
        Case 35
            OperatorMessage = "Gratuity Selection Required"
        Case 36
            OperatorMessage = "Expiry Date Required"
        Case 37
            OperatorMessage = "Start Date Required"
        Case 38
            OperatorMessage = "Issue Number Required"
        Case 39
            OperatorMessage = "AVS House Number Required"
        Case 40
            OperatorMessage = "AVS Post Code Required"
        Case 41
            OperatorMessage = "CSC Required"
        Case 42
            OperatorMessage = "Customer Present / Not Present Selection Required"
        Case 43
            OperatorMessage = "Customer / Not Present Option Selection Required"
        Case 44
            OperatorMessage = "Enter Charge Auth Code"
        Case 45
            OperatorMessage = "Login Required"
        Case 46
            OperatorMessage = "Ready"
        Case 47
            OperatorMessage = "Card Not Accepted"
        Case 48
            OperatorMessage = "Card Blocked"
        Case 49
            OperatorMessage = "Transaction Cancelled"
        Case 50
            OperatorMessage = "Invalid Expiry"
        Case 51
            OperatorMessage = "Gratuity Invalid"
        Case 52
            OperatorMessage = "Invalid Card"
        Case 53
            OperatorMessage = "Printing Customer Receipt"
        Case 54
            OperatorMessage = "Initialising PED"
        Case 55
            OperatorMessage = "PED Unavailable"
        Case 56
            OperatorMessage = "Card Application Selection"
        Case 57
            OperatorMessage = "Retry Download"
        Case 58
            OperatorMessage = "Restart After Software Update"
        Case 59
            OperatorMessage = "Requesting DCC"
        Case 60
            OperatorMessage = "DCC Currency Choice"
        Case 61
            OperatorMessage = "Cardholder DCC Currency Choice"
        Case 62
            OperatorMessage = "Unsafe Download"
        Case 63
            OperatorMessage = "Unexpected Login"
        Case 64
            OperatorMessage = "Start Barclays Bonus Transaction"
        Case 65
            OperatorMessage = "Update Barclays Bonus"
        Case 66
            OperatorMessage = "Cancel Barclays Bonus Transaction"
        Case 67
            OperatorMessage = "Confirm Gratuity"
        Case 68
            OperatorMessage = "Register For Account On File Decision"
        Case 69
            OperatorMessage = "Awaiting Token ID"
        Case 70
            OperatorMessage = "Barclays Bonus Discount Summary"
        Case 71
            OperatorMessage = "Barclays Bonus Use Bonus"
        Case 72
            OperatorMessage = "Barclays Bonus Enter Redemption"
        Case 73
            OperatorMessage = "Barclays Bonus Not Available"
        Case 74
            OperatorMessage = "Download Complete"
        Case 75
            OperatorMessage = "Download Still Being Prepared"
        Case 76
            OperatorMessage = "Server Connection Failed"
        Case 77
            OperatorMessage = "Resume Download"
        Case 78
            OperatorMessage = "PayPoint Account Extraction Failed"
        Case 79
            OperatorMessage = "PayPoint Amount Outside Allowed Range"
        Case 80
            OperatorMessage = "PayPoint Card Expired"
        Case 81
            OperatorMessage = "PayPoint Initialised"
        Case 82
            OperatorMessage = "PayPoint Initialisation Failed"
        Case 83
            OperatorMessage = "PayPoint Initialising"
        Case 84
            OperatorMessage = "PayPoint Invalid Account"
        Case 85
            OperatorMessage = "PayPoint Invalid Amount"
        Case 86
            OperatorMessage = "PayPoint Invalid Capture Method"
        Case 87
            OperatorMessage = "PayPoint Invalid Card Number"
        Case 88
            OperatorMessage = "PayPoint Invalid Configuration"
        Case 89
            OperatorMessage = "PayPoint Invalid Denomination"
        Case 90
            OperatorMessage = "PayPoint Invalid Expiry Date"
        Case 91
            OperatorMessage = "PayPoint Invalid Scheme"
        Case 92
            OperatorMessage = "PayPoint Invalid Scheme Option"
        Case 93
            OperatorMessage = "PayPoint Invalid Top-up Type"
        Case 94
            OperatorMessage = "PayPoint Invalid Service Provider"
        Case 95
            OperatorMessage = "PayPoint Invalid Track2 Format"
        Case 96
            OperatorMessage = "PayPoint Invalid Transaction Type"
        Case 97
            OperatorMessage = "PayPoint Keyed Entry Not Allowed"
        Case 98
            OperatorMessage = "PayPoint Merchant Reference Required"
        Case 99
            OperatorMessage = "PayPoint No Accounts"
        Case 100
            OperatorMessage = "PayPoint Processing Transaction"
        Case 101
            OperatorMessage = "PayPoint Retry"
        Case 102
            OperatorMessage = "PayPoint Scheme Not Recognised"
        Case 103
            OperatorMessage = "PayPoint Transaction Cancelled"
        Case 104
            OperatorMessage = "PayPoint Transaction Type Not Allowed"
        Case 105
            OperatorMessage = "PayPoint Select Scheme Option"
        Case 106
            OperatorMessage = "PayPoint Download Required"
        Case 107
            OperatorMessage = "PayPoint Select Account"
        Case 108
            OperatorMessage = "Printing PayPoint Receipt"
        Case 109
            OperatorMessage = "Licence Detail Confirmation"
        Case 110
            OperatorMessage = "Licence File Required"
        Case 111
            OperatorMessage = "Pay Point Service Unavailable"
        Case 112
            OperatorMessage = "Park Retail Gift Account Extraction Failed"
        Case 113
            OperatorMessage = "Park Retail Gift Amount Outside Allowed Range"
        Case 114
            OperatorMessage = "Park Retail Gift Card Expired"
        Case 115
            OperatorMessage = "Park Retail Gift Initialisation Failed"
        Case 116
            OperatorMessage = "Park Retail Gift Initialising"
        Case 117
            OperatorMessage = "Park Retail Gift Invalid Account"
        Case 118
            OperatorMessage = "Park Retail Gift Invalid Amount"
        Case 119
            OperatorMessage = "Park Retail Gift Invalid Capture Method"
        Case 120
            OperatorMessage = "Park Retail Gift Invalid Card Number"
        Case 121
            OperatorMessage = "Park Retail Gift Invalid Configuration"
        Case 122
            OperatorMessage = "Park Retail Gift Invalid Expiry Date"
        Case 123
            OperatorMessage = "Park Retail Gift Invalid Track2 Format"
        Case 124
            OperatorMessage = "Park Retail Gift Invalid Transaction Type"
        Case 125
            OperatorMessage = "Park Retail Gift Keyed Entry Not Allowed"
        Case 126
            OperatorMessage = "Park Retail Gift Merchant Reference Required"
        Case 127
            OperatorMessage = "Park Retail Gift No Accounts"
        Case 128
            OperatorMessage = "Park Retail Gift Service Unavailable"
        Case 129
            OperatorMessage = "Park Retail Gift Processing Transaction"
        Case 131
            OperatorMessage = "Park Retail Gift Scheme Not Recognised"
        Case 132
            OperatorMessage = "Park Retail Gift Select Account"
        Case 133
            OperatorMessage = "Park Retail Gift Transaction Cancelled"
        Case 134
            OperatorMessage = "Park Retail Gift Transaction Type Not Allowed"
        Case 135
            OperatorMessage = "Printing Park Retail Gift Receipt"
        Case 136
            OperatorMessage = "PED In ESD Recovery Mode2"
        Case 137
            OperatorMessage = "Update Transaction Value Decision"
        Case 138
            OperatorMessage = "Update Barclaycard Freedom Config"
        Case 139
            OperatorMessage = "Processing Key Exchange"
        Case 140
            OperatorMessage = "Barclays Gift Initialising"
        Case 141
            OperatorMessage = "Global Blue Tax Free Shopping Initialising"
        Case 142
            OperatorMessage = "GiveX Initialising"
        Case 143
            OperatorMessage = "M-Voucher Initialising"
        Case 144
            OperatorMessage = "Performing Post Confirm Reversal"
        Case 145
            OperatorMessage = "Invalid Amount"
        Case 146
            OperatorMessage = "Merchant Reference Required"
        Case 147
            OperatorMessage = "Merchant Reference Length Invalid"
        Case 148
            OperatorMessage = "Initialising"
        Case 149
            OperatorMessage = "Account Extraction Failed"
        Case 150
            OperatorMessage = "Amount Outside Allowed Range"
        Case 151
            OperatorMessage = "Enter Amount"
        Case 152
            OperatorMessage = "Confirm Authorisation"
        Case 153
            OperatorMessage = "Card Expired"
        Case 154
            OperatorMessage = "Transax SV Initialisation Failed"
        Case 155
            OperatorMessage = "Transax SV Initialising"
        Case 156
            OperatorMessage = "Invalid Account"
        Case 157
            OperatorMessage = "Invalid Capture Method"
        Case 158
            OperatorMessage = "Invalid Card Number"
        Case 159
            OperatorMessage = "Invalid Configuration"
        Case 160
            OperatorMessage = "Invalid Expiry Date"
        Case 161
            OperatorMessage = "Invalid Track2 Format"
        Case 162
            OperatorMessage = "Invalid Transaction Type"
        Case 163
            OperatorMessage = "Keyed Entry Not Allowed"
        Case 164
            OperatorMessage = "No Accounts"
        Case 165
            OperatorMessage = "Card Scheme Not Recognised"
        Case 166
            OperatorMessage = "Transaction Type Not Allowed"
        Case 167
            OperatorMessage = "Transax SV Service Unavailable"
        Case 168
            OperatorMessage = "Invalid Issue Number"
        Case 169
            OperatorMessage = "Invalid Card Security Code"
        Case 170
            OperatorMessage = "Confirmation Failed"
        Case 171
            OperatorMessage = "Printing Receipt"
        Case 172
            OperatorMessage = "Waiting For Donation"
        Case 173
            OperatorMessage = "Pin Blocked"
        Case 174
            OperatorMessage = "Pin Try Limit Exceeded"
        Case 175
            OperatorMessage = "Further Payment Required"
        Case 176
            OperatorMessage = "SVS Initialising"
        Case 177
            OperatorMessage = "SVS Initialisation Failed"
        Case 178
            OperatorMessage = "SVS Invalid Configuration"
        Case 179
            OperatorMessage = "SVS Service Unavailable"
        Case 180
            OperatorMessage = "Cannot Continue Transaction"
        Case 181
            OperatorMessage = "Part Payment"
        Case 182
            OperatorMessage = "Obtain STAN"
        Case 183
            OperatorMessage = "Invalid STAN"
        Case 184
            OperatorMessage = "IVR Required"
        Case 185
            OperatorMessage = "Prompt For Authorisation Code"
        Case 186
            OperatorMessage = "Invalid Auth Code"
        Case 187
            OperatorMessage = "Offline Txn Failed"
        Case 188
            OperatorMessage = "Enter Reference"
        Case 189
            OperatorMessage = "Print Shop Copy"
        Case 190
            OperatorMessage = "Must Insert Card"
        Case 191
            OperatorMessage = "PIN Entered"
        Case 192
            OperatorMessage = "Card Swiped"
        Case 193
            OperatorMessage = "PIN Bypassed"
        Case 194
            OperatorMessage = "Cancelling Transaction"
        Case 195
            OperatorMessage = "Card Presented"
        Case 196
            OperatorMessage = "Server Error"
        Case 197
            OperatorMessage = "On Hold Transaction"
        Case 212
            OperatorMessage = "Failed To Read Card"
        Case Else
            If StatusID > 197 Then
                OperatorMessage = "Unknown Progress Message Status"
                If Not Status = vbNullString Then
                    OperatorMessage = OperatorMessage + vbNewLine + Status
                End If
                OperatorMessage = OperatorMessage + " (" + CStr(StatusID) + ")"
            Else
                OperatorMessage = ""
            End If
    End Select
End Property

Public Property Get ParametersMessage() As String
    Dim Param As Integer
    
    If Not Parameters Is Nothing Then
        With Parameters
            For Param = 1 To .Count
                With .Item(Param)
                    Select Case .Name
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpTxnValue)
                            ParametersMessage = vbNewLine & "Transaction value: " & .Value
                        Case ValidProgressMessageParameters()(epmpCashbackValue)
                            If CDbl(.Value) <> 0# Then
                                ParametersMessage = vbNewLine & "Cashback value: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpGratuityValue)
                            If CDbl(.Value) <> 0# Then
                                ParametersMessage = vbNewLine & "Gratuity value: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpTotalAmount)
                            If CDbl(.Value) <> 0# Then
                                ParametersMessage = vbNewLine & "Total value: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpAuthCode)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Authorisation Code: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpVRTelNo)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Voice Referral Tel. No.: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpCard)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Card Application: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpExpiry)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Expiry Date: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpMerchantID)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Machine ID: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpTerminalID)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Terminal ID: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpFilename)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Filename: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpProgress)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Progress: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpSchemeID)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Scheme ID: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpMerchantName)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Merchant Name: " & .Value
                            End If
                        Case Else
                    End Select
                End With
            Next Param
        End With
    End If
    ' Remove preceding vbNewLine
    If Len(ParametersMessage) > Len(vbNewLine) Then
        ParametersMessage = Mid(ParametersMessage, Len(vbNewLine) + 1)
    End If
End Property
' End of Properties

' Public Methods

Public Function GetParameter(ByVal ParameterType As ProgressMessageParameterType) As String
    Dim Count As Integer

    If ParameterType > 0 And ParameterType <= UBound(ValidProgressMessageParameters) And Not myMessageParameters Is Nothing Then
        For Count = 1 To myMessageParameters.Count
            If StrComp(myMessageParameters.Item(Count).Name, ValidProgressMessageParameters()(ParameterType), vbTextCompare) = 0 Then
                GetParameter = myMessageParameters.Item(Count).Value
                Exit For
            End If
        Next
    End If
End Function
' End of Public Methods

' Private methods

Private Sub CreateParameterInfoAndAddtoArray(ByVal Name As String, ByVal PropertyName As String, ByVal ParamID As ParameterID, ByVal ParamType As ParamValueType)
    Dim ParamInfo As cParameterInfo
    
    Set ParamInfo = New cParameterInfo
    Call ParamInfo.Initialise(Name, PropertyName, ParamID, ParamType)
    Call myParameters.Add(ParamInfo, Key:=Trim(CStr(ParamID)))
End Sub

Private Sub BuildDataArray()
    
    Call CreateParameterInfoAndAddtoArray("Result", "Result", ParameterID.epResult, ParamValueType.epvtInteger)
    Call CreateParameterInfoAndAddtoArray("TerminateLoop", "TerminateLoop", ParameterID.epTerminateLoop, ParamValueType.epvtInteger)
    Call CreateParameterInfoAndAddtoArray("StatusID", "StatusID", ParameterID.epStatusID, ParamValueType.epvtInteger)
    Call CreateParameterInfoAndAddtoArray("Status", "Status", ParameterID.epStatus, ParamValueType.epvtString)
    Call CreateParameterInfoAndAddtoArray("Parameters", "Parameters", ParameterID.epParameters, ParamValueType.epvtString)
End Sub

' this method will use the comma as a delimiter and populate the values above
Friend Function ParseReceivedMessage(ByVal Message As String) As Boolean
    Dim MessageLen As Integer
    Dim ParamValues() As String
    Dim Delimiter As String
    Dim Param As Integer

On Error GoTo Catch

Try:
    ' if the transaction fails then return. Success is 0
    Call DebugMsg(ModuleName, "ParseReceivedMessage", endlTraceIn)
    Call DebugMsg(ModuleName, "ParseReceivedMessage", endlDebug, "Progress Message = " & Message)
    MessageLen = Len(Message)
    If MessageLen > 0 Then
        '  define which character is seperating fields
        Delimiter = ","
        ParamValues = Split(Message, Delimiter)
        For Param = 0 To NumberOfParameters - 1
            Call DebugMsg(ModuleName, "ParseReceivedMessage", endlDebug, "Parsing parameter = " & ParamValues(Param))
            ' only bother setting the rtn fields if there is a value
            If Len(ParamValues(Param)) > 0 Then
                Call SetPropertyFromParameterValue(ParamValues(Param), Param)
            End If
        Next Param
    End If

    ParseReceivedMessage = True

    Call DebugMsg(ModuleName, "ParseReceivedMessage", endlTraceOut)
    
    Exit Function

Catch:
    Call DebugMsg(ModuleName, "ParseReceivedMessage", endlDebug, "Failed Parsing message = " & Message)
    Call DebugMsg(ModuleName, "ParseReceivedMessage", endlDebug, "Failed Parsing parameter id = " & Param)
    Call DebugMsg(ModuleName, "ParseReceivedMessage", endlDebug, "Failed Parsing parameter = " & ParamValues(Param))
    Call DebugMsg(ModuleName, "ParseReceivedMessage", endlDebug, "Error whilst parsing was '" & Err.Description & "'")
    Call DebugMsg(ModuleName, "ParseReceivedMessage", endlTraceOut)
    Call Err.Raise(vbObjectError + 1, ModuleNameStub & "ParseReceivedMessage", "Failed to parse output Progress Message." & vbCrLf & Err.Description)
End Function

Private Sub SetPropertyFromParameterValue(ByVal ParamValue As String, ByVal ParamID As Integer)
    Dim TempInt As Integer
    Dim Delimiter As String
    Dim MessParams() As String
    Dim MessParam As Integer
    Dim Param() As String
    Dim MessParameter As ProgressMessageParameter
    Dim ValidProgressMessParams As String
    Dim IsError As Boolean
    
    Call DebugMsg(ModuleName, "SetPropertyFromParameterValue", endlTraceIn)
    IsError = True
    Select Case ParamID
        Case ParameterID.epResult:
            If ParamToInt(TempInt, ParamValue) Then
                Result = TempInt
                IsError = False
            End If
        Case ParameterID.epTerminateLoop:
            If ParamToInt(TempInt, ParamValue) Then
                TerminateLoop = TempInt
                IsError = False
            End If
        Case ParameterID.epStatusID:
            If ParamToInt(TempInt, ParamValue) Then
                If TempInt >= ProgressStatusID.esiProcessingTxn Then
                    StatusID = TempInt
                    IsError = False
                End If
            End If
        Case ParameterID.epStatus:
            Status = ParamValue
            IsError = False
        Case ParameterID.epParameters
            Set myMessageParameters = New Collection
            If Len(ParamValue) > 0 Then
                Delimiter = ";"
                MessParams = Split(ParamValue, Delimiter)
                ValidProgressMessParams = Join(ValidProgressMessageParameters, Delimiter)
                Delimiter = "="
                IsError = False
                For MessParam = 0 To UBound(MessParams)
                    Param() = Split(MessParams(MessParam), Delimiter)
                    If InStr(1, ValidProgressMessParams, Param(0), vbTextCompare) > 0 Then
                        MessParameter.Name = Param(0)
                        MessParameter.Value = Param(1)
                        Call myMessageParameters.Add(MessParameter, Key:=MessParameter.Name)
                    Else
                        IsError = True
                    End If
                Next MessParam
            End If
        Case Else
            Call DebugMsg(ModuleName, "SetPropertyFromParameterValue", endlDebug, "Failed setting property from parameter value = " & ParamValue)
            Call DebugMsg(ModuleName, "SetPropertyFromParameterValue", endlDebug, "Failed setting property from parameter id = " & ParamID)
    End Select
    Call DebugMsg(ModuleName, "SetPropertyFromParameterValue", endlTraceOut)
    If IsError Then
        Call Err.Raise(vbObjectError + 1, ModuleNameStub & "SetPropertyFromParameterValue", myParameters(ParamID).ConversionErrorMess)
    End If
End Sub

Private Function ModuleNameStub() As String

    ModuleNameStub = AppNameStub & ModuleName & "."
End Function

