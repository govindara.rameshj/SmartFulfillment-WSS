VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cAdditionalCommand"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Enum AdditionalIntegrationCommand
    eaicSubmitOfflineTxns
    eaicReport
    eaicRequestInfo
    eaicRequestLastMessage
    eaicScreenPos
    eaicStayOffLine
    eaicUpdateApp
    eaicWinState
End Enum

Public Enum AdditionalIntegrationReport
    eairNotSet = 0
    eairZReport = 1
    eairXReport = 2
    eairTxnReport = 3
    eairQReport = 5
    eairReprintLastReport = 10
    eairProductList = 100
    eairReprintCustomerReceipt = 101
    eairReprintMerchantReceipt = 102
    eairSystemInfo = 103
    eairLiveStore = 201
    eairSessionReport = 202
    eairSummarySettlement = 203
    eairDetailedSettlement = 204
End Enum

Public Enum AdditionalIntegrationRequestInfo
    eairiNotSet = 0
    eairiPTID = 1
    eairiSoftwareVersion = 2
    eairiLoginStatus = 3
    eairiTransServerConnTest = 4
    eairiPEDStatus = 5
End Enum

Public Enum AdditionalIntegrationWinState
    eaiwsNotSet = -1
    eaiwsNormal = 0
    eaiwsMinimised = 1
    eaiwsMaximised = 2
End Enum


Private myCommand As AdditionalIntegrationCommand
Private myReportType As AdditionalIntegrationReport
Private myReportFileName As String
Private myRequestType As AdditionalIntegrationRequestInfo
Private myScreenPosX As Long
Private myScreenPosY As Long
Private myStayOfflineSetting As Integer
Private myNoMessage As Boolean
Private myWinState As AdditionalIntegrationWinState

' Result of continuation transaction in form of progress message
Private myReceivedMessage As String
Private myResultMessage As String
Private myOutputTRecordV4 As cOutputTRecordV4

Public Sub Initialise(ByVal Command As AdditionalIntegrationCommand, _
                      Optional ByVal ReportType As AdditionalIntegrationReport = eairNotSet, _
                      Optional ByVal ReportFileName As String = "", _
                      Optional ByVal NoMessage As Boolean, _
                      Optional ByVal RequestType As AdditionalIntegrationRequestInfo = eairiNotSet, _
                      Optional ByVal ScreenPosX As Long, Optional ByVal ScreenPosY As Long, _
                      Optional ByVal StayOfflineSetting As Integer = -1, _
                      Optional ByVal WinState As AdditionalIntegrationWinState = AdditionalIntegrationWinState.eaiwsNotSet)

    myCommand = Command
    myReportType = ReportType
    myReportFileName = ReportFileName
    myRequestType = RequestType
    myScreenPosX = ScreenPosX
    myScreenPosY = ScreenPosY
    myStayOfflineSetting = StayOfflineSetting
    myNoMessage = NoMessage
    myWinState = WinState
    Set myOutputTRecordV4 = New cOutputTRecordV4
    If Not myOutputTRecordV4 Is Nothing Then
        Call myOutputTRecordV4.Initialise
    End If
End Sub

Public Function ToIntegrationMessage() As String

    Select Case myCommand
        Case AdditionalIntegrationCommand.eaicReport
            If myReportType <> eairNotSet Then
                ToIntegrationMessage = "REP," & Trim(CStr(myReportType))
                If myReportFileName <> "" Then
                    ToIntegrationMessage = ToIntegrationMessage & "," & myReportFileName
                Else
                    If myNoMessage Then
                        ToIntegrationMessage = ToIntegrationMessage & ","
                    End If
                End If
                If myNoMessage Then
                    ToIntegrationMessage = ToIntegrationMessage & ",NOMSG"
                End If
            End If
        Case AdditionalIntegrationCommand.eaicRequestInfo
            If myRequestType <> AdditionalIntegrationRequestInfo.eairiNotSet Then
                ToIntegrationMessage = "REQINFO," & Trim(CStr(myRequestType))
            End If
        Case AdditionalIntegrationCommand.eaicRequestLastMessage
            ToIntegrationMessage = "REQLASTMSG,"
        Case AdditionalIntegrationCommand.eaicScreenPos
            ToIntegrationMessage = "SCRPOS," & Trim(CStr(myScreenPosX)) & "," & Trim(CStr(myScreenPosY))
        Case AdditionalIntegrationCommand.eaicStayOffLine
            If myStayOfflineSetting > -1 And myStayOfflineSetting < 100 Then
                ToIntegrationMessage = "STAYOFFLINE," & Trim(CStr(myStayOfflineSetting))
            End If
        Case AdditionalIntegrationCommand.eaicSubmitOfflineTxns
            ToIntegrationMessage = "OLS,"
            If myNoMessage Then
                ToIntegrationMessage = ToIntegrationMessage & "NOMSG"
            End If
        Case AdditionalIntegrationCommand.eaicUpdateApp
            ToIntegrationMessage = "UPDAPP,"
        Case AdditionalIntegrationCommand.eaicWinState
            If myWinState <> AdditionalIntegrationWinState.eaiwsNotSet Then
                ToIntegrationMessage = "WINSTATE," & Trim(CStr(myWinState))
            End If
        Case Else
    End Select
    ToIntegrationMessage = ToIntegrationMessage & vbCrLf
End Function

Public Property Get OutputTRecordV4() As cOutputTRecordV4

    Set OutputTRecordV4 = myOutputTRecordV4
End Property

Public Property Get ReceivedMessage() As String

    ReceivedMessage = myReceivedMessage
End Property

Public Property Let ReceivedMessage(ByVal NewMessage As String)

    myReceivedMessage = NewMessage
    With myOutputTRecordV4
        If .ParseReceivedMessage(myReceivedMessage) Then
'            Select Case .Result
'                Case Is > 0
'                Case Else
'            End Select
        Else
            ' error failed parsing message received from PED
        End If
'        ' Status/Error message returned here
'        myResultMessage = .Status
    End With
End Property

Public Property Get ResultMessage() As String

    ResultMessage = myResultMessage
End Property

