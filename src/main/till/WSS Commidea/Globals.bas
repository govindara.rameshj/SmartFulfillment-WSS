Attribute VB_Name = "Globals"
Option Explicit

Public Const PANEL_VERNO As Integer = 2
Public Const PANEL_INFO As Integer = 3
Public Const PANEL_WSID As Integer = 4
Public Const PANEL_DATE As Integer = 5

Public Const PRM_TRANSNO      As Long = 911

Private Const ModuleName As String = "Globals"

' Property variables
Private myMaxValidCashier As String

Public Enum CurrencyCodeISO4217
    eccUICFrancSpecialSettlementCurrency = 0
    eccLek = 8
    eccAlgerianDinar = 12
    eccArgentinePeso = 32
    eccAustralianDollar = 36
    eccBahamianDollar = 44
    eccBahrainiDinar = 48
    eccBangladeshiTaka = 50
    eccArmenianDram = 51
    eccBarbadosDollar = 52
    eccBermudianDollarCustomarilyKnownAsBermudaDollar = 60
    eccNgultrum = 64
    eccBoliviano = 68
    eccPula = 72
    eccBelizeDollar = 84
    eccSolomonIslandsDollar = 90
    eccBruneiDollar = 96
    eccKyat = 104
    eccBurundianFranc = 108
    eccRiel = 116
    eccCanadianDollar = 124
    eccCapeVerdeEscudo = 132
    eccCaymanIslandsDollar = 136
    eccSriLankaRupee = 144
    eccChileanPeso = 152
    eccChineseYuan = 156
    eccColombianPeso = 170
    eccComoroFranc = 174
    eccCostaRicanColon = 188
    eccCroatianKuna = 191
    eccCubanPeso = 192
    eccCzechKoruna = 203
    eccDanishKrone = 208
    eccDominicanPeso = 214
    eccEthiopianBirr = 230
    eccNakfa = 232
    eccKroon = 233
    eccFalklandIslandsPound = 238
    eccFijiDollar = 242
    eccDjiboutiFranc = 262
    eccDalasi = 270
    eccGibraltarPound = 292
    eccQuetzal = 320
    eccGuineaFranc = 324
    eccGuyanaDollar = 328
    eccHaitiGourde = 332
    eccLempira = 340
    eccHongKongDollar = 344
    eccForint = 348
    eccIcelandKrona = 352
    eccIndianRupee = 356
    eccRupiah = 360
    eccIranianRial = 364
    eccIraqiDinar = 368
    eccIsraeliNewSheqel = 376
    eccJamaicanDollar = 388
    eccJapaneseYen = 392
    eccTenge = 398
    eccJordanianDinar = 400
    eccKenyanShilling = 404
    eccNorthKoreanWon = 408
    eccSouthKoreanWon = 410
    eccKuwaitiDinar = 414
    eccSom = 417
    eccKip = 418
    eccLebanesePound = 422
    eccLesothoLoti = 426
    eccLatvianLats = 428
    eccLiberianDollar = 430
    eccLibyanDinar = 434
    eccLithuanianLitas = 440
    eccPataca = 446
    eccMalawianKwacha = 454
    eccMalaysianRinggit = 458
    eccRufiyaa = 462
    eccOuguiya = 478
    eccMauritiusRupee = 480
    eccMexicanPeso = 484
    eccTugrik = 496
    eccMoldovanLeu = 498
    eccMoroccanDirham = 504
    eccRialOmani = 512
    eccNamibianDollar = 516
    eccNepaleseRupee = 524
    eccNetherlandsAntilleanGuilder = 532
    eccArubanGuilder = 533
    eccVatu = 548
    eccNewZealandDollar = 554
    eccCordobaOro = 558
    eccNaira = 566
    eccNorwegianKrone = 578
    eccPakistanRupee = 586
    eccBalboa = 590
    eccKina = 598
    eccGuarani = 600
    eccNuevoSol = 604
    eccPhilippinePeso = 608
    eccQatariRial = 634
    eccRussianRouble = 643
    eccRwandaFranc = 646
    eccSaintHelenaPound = 654
    eccDobra = 678
    eccSaudiRiyal = 682
    eccSeychellesRupee = 690
    eccLeone = 694
    eccSingaporeDollar = 702
    eccVietnameseDong = 704
    eccSomaliShilling = 706
    eccSouthAfricanRand = 710
    eccLilangeni = 748
    eccSwedishKronaOrKronor = 752
    eccSwissFranc = 756
    eccSyrianPound = 760
    eccBaht = 764
    eccPaanga = 776
    eccTrinidadAndTobagoDollar = 780
    eccUnitedArabEmiratesDirham = 784
    eccTunisianDinar = 788
    eccUgandaShilling = 800
    eccDenar = 807
    eccEgyptianPound = 818
    eccPoundSterling = 826
    eccTanzanianShilling = 834
    eccUSDollar = 840
    eccPesoUruguayo = 858
    eccUzbekistanSom = 860
    eccSamoanTala = 882
    eccYemeniRial = 886
    eccZambainKwacha = 894
    eccNewTaiwanDollar = 901
    eccCubanConvertiblePeso = 931
    eccZimbabweDollar = 932
    eccManat = 934
    eccCedi = 936
    eccVenezuelanBolivarFuerte = 937
    eccSudanesePound = 938
    eccSerbianDinar = 941
    eccMetical = 943
    eccAzerbaijanianManat = 944
    eccRomanianNewLeu = 946
    eccWIREuroComplementaryCurrency = 947
    eccWIRFrancComplementaryCurrency = 948
    eccTurkishLira = 949
    eccCFAFrancBEAC = 950
    eccEastCaribbeanDollar = 951
    eccCFAFrancBCEAO = 952
    eccCFPFranc = 953
    eccEuropeanCompositeUnitEURCOBondMarketUnit = 955
    eccEuropeanMonetaryUnitEMU6BondMarketUnit = 956
    eccEuropeanUnitOfAccount9EUA9BondMarketUnit = 957
    eccEuropeanUnitOfAccount17EUA17BondMarketUnit = 958
    eccGoldOneTroyOunce = 959
    eccSpecialDrawingRights = 960
    eccSilverOneTroyOunce = 961
    eccPlatinumOneTroyOunce = 962
    eccCodeReservedForTestingPurposes = 963
    eccPalladiumOneTroyOunce = 964
    eccSurinamDollar = 968
    eccMalagasyAriary = 969
    eccUnidadDeValorReal = 970
    eccAfghani = 971
    eccSomoni = 972
    eccKwanza = 973
    eccBelarusianRuble = 974
    eccBulgarianLev = 975
    eccFrancCongolais = 976
    eccConvertibleMarks = 977
    eccEuro = 978
    eccMexicanUnidadDeInversionUDIFundsCode = 979
    eccHryvnia = 980
    eccLari = 981
    eccBolivianMvdolFundsCode = 984
    eccZloty = 985
    eccBrazilianReal = 986
    eccUnidadDeFomentoFundsCode = 990
    eccUnitedStatesDollarNextDayFundsCode = 997
    eccUnitedStatesDollarSameDayFunds = 998
    eccNoCurrency = 999
End Enum
    
' Parameter shared functionality
' Output record parameter data types
Public Enum ParamValueType
    epvtString
    epvtInteger
    epvtDouble
    epvtFloat
    epvtAdditionalParamList
End Enum


' Routines to convert data in parameter string values to
' appropriate type if its content is correct format for
' that type

Public Function ParamToInt(ByRef Value As Integer, ByVal Param As String) As Boolean

    If IsNumeric(Param) Then
        If CDbl(CInt(Param)) - CDbl(Param) = 0# Then
            Value = CInt(Param)
            ParamToInt = True
        End If
    End If
End Function

Public Function ParamToDouble(ByRef Value As Double, ByVal Param As String) As Boolean

    If IsNumeric(Param) Then
        Value = CDbl(Param)
        ParamToDouble = True
    End If
End Function

Public Function ParamToDate(ByRef Value As Date, ByVal Param As String, EndOfMonth As Boolean) As Boolean
    Dim ParamChar As String
    Dim MonthPart As String
    Dim YearPart As String
    Dim DayPart As String
    
    If Len(Param) = 4 Then
        MonthPart = Left(Param, 2)
        If CInt(MonthPart) >= 1 And CInt(MonthPart) <= 12 Then
            YearPart = Mid(Param, 3)
            If CInt(YearPart) >= 0 Then
                If EndOfMonth Then
                    Select Case CInt(MonthPart)
                        Case 2
                            If CInt(YearPart) Mod 4 = 0 Then
                                DayPart = "28"
                            Else
                                DayPart = "29"
                            End If
                        Case 4, 6, 9, 11
                            DayPart = "30"
                        Case Else
                            DayPart = "31"
                    End Select
                Else
                    DayPart = "1"
                End If
                Value = CDate(DayPart & "/" & MonthPart & "/" & "20" & YearPart)
                ParamToDate = True
            End If
        End If
    End If
End Function
' End of Parameter value conversion routines

' End of Parameter shared functionality

Public Function CurrencyAmount(ByVal CurrencyCode As CurrencyCodeISO4217, ByVal amount As Double) As String

    Select Case CurrencyCode
        Case CurrencyCodeISO4217.eccPoundSterling
            CurrencyAmount = Format(amount, ChrW(163) & "0.00")
        Case Else
            CurrencyAmount = Format(amount, ChrW(163) & "0.00")
    End Select
End Function

Public Function SplitTo1BasedStringArray(ByVal ToSplit As String, ByVal Delimiter As String) As String()
    Dim OriginalSplit() As String
    Dim FinalSplit() As String
    Dim Copy As Integer

    OriginalSplit = Split(ToSplit, Delimiter)
    ReDim FinalSplit(1 To UBound(OriginalSplit) + 1)
    For Copy = 0 To UBound(OriginalSplit)
        FinalSplit(Copy + 1) = OriginalSplit(Copy)
    Next Copy
    SplitTo1BasedStringArray = FinalSplit
End Function

' Properties

'Receipt Properties

Public Function GetCustomerReceiptFilePath(ByVal commideaCfg As CommideaConfiguration) As String

    GetCustomerReceiptFilePath = Replace(commideaCfg.CustRecPath & "\" & commideaCfg.CustRecFilename, "\\", "\")
End Function

Public Property Get GetMerchantReceiptFilePath(ByVal commideaCfg As CommideaConfiguration) As String

    GetMerchantReceiptFilePath = Replace(commideaCfg.MerRecPath & "\" & commideaCfg.MerRecFilename, "\\", "\")
End Property


'End of Receipt Properties


' End of Properties

Private Function ModuleNameStub() As String

    ModuleNameStub = AppNameStub & ModuleName & "."
End Function

Public Sub LoadCommideaForm(oForm As Form, commideaCfg As CommideaConfiguration)
    Dim commideaForm As ICommideaForm
    Set commideaForm = oForm
    Call commideaForm.InitializeBeforeLoad(commideaCfg)
    Call Load(oForm)
End Sub

Public Sub UnloadCommideaForm(oForm As Form)
    Dim commideaForm As ICommideaForm
    Set commideaForm = oForm
    Call commideaForm.ResetAfterUnload
    Call Unload(oForm)
End Sub

