VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "mswinsck.ocx"
Begin VB.Form frmReadCardDetails 
   Caption         =   "Read My Card"
   ClientHeight    =   450
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   1560
   LinkTopic       =   "Form1"
   ScaleHeight     =   450
   ScaleWidth      =   1560
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin MSWinsockLib.Winsock wskProgress 
      Left            =   0
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      RemotePort      =   25001
   End
   Begin MSWinsockLib.Winsock wskIntegration 
      Left            =   480
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      RemotePort      =   25000
   End
End
Attribute VB_Name = "frmReadCardDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Implements ICommideaForm

Private moCommideaCfg As CommideaConfiguration
Private myProgressMessage() As String
Private myReceivedMessage As String
Private myMessageReceived As Boolean
Private mblnInLaunchAndLogin As Boolean
Private mblnConnectionError As Boolean
Private myIntegrationError As String
Private myProgressError As String
Private myOperationMode As GetCardDetailsOperationMode
Private myRemoveCardSetting As GetCardDetailsRemoveCardSetting
Private myPOSVersion
Private myIsEFT As Boolean

Public Event AwaitingCard()
Public Event CardError(ByVal ErrorMessage As String)
Public Event UserCancelled()

' Control events

'Integration Socket events
Private Sub wskIntegration_ConnectionRequest(ByVal requestID As Long)

    ' Check if the control's State is closed. If not,
    ' close the connection before accepting the new
    ' connection.
    With wskIntegration
        If .State <> sckClosed Then
            .Close
        End If
        ' Accept the request with the requestID
        ' parameter.
        .Accept requestID
    End With
End Sub

Private Sub wskIntegration_DataArrival(ByVal bytesTotal As Long)
    Dim Received As String
    Dim Extra As String

    If bytesTotal > 0 Then
        Do
            Call wskIntegration.GetData(Extra, vbString)
            Received = Received & Extra
        Loop While Right(Received, 2) <> vbCrLf And Received <> "" And Asc(Left(Received, 1)) <> 6
        DebugMsg ModuleName, "wskIntegration_DataArrival", endlDebug, "Integration Message Received: " & Received
        If Received <> "" Then
            ' Make sure its the right format
            ' - Ocius sends Asc(6) to acknowledge receipt of message,
            ' but this often come out as part of the following data
            ' sent to winsock, sometimes it arrives on its own.  If
            ' remove it on own the message is now blank so will loop
            ' for more data; if together this will just strip it off
            If Asc(Left(Received, 1)) = 6 Then
                ' Remove the message delimiters
                Received = Mid(Received, 2)
            End If
            If Right(Received, 2) = vbCrLf Then
                ' Remove the message delimiters
                Received = Left(Received, Len(Received) - 2)
            End If
            ReceivedMessage = Received
        End If
    End If
End Sub

Private Sub wskIntegration_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)

    Select Case Number
        Case 1
            myIntegrationError = Trim(CStr(Number)) & ";" & Description & ";;Connection Error"
        Case 10060, 10061
            myIntegrationError = Trim(CStr(Number)) & ";" & Description & ";Please ensure Ocius is running and logged in;Ocius Sentinel Connection Error"
        Case Else
            myIntegrationError = Trim(CStr(Number)) & ";" & Description & ";Please check the connection cables, Network availability etc;General"
    End Select
    wskIntegration.Close
End Sub
' End of Integration Socket events

' Progress socket events
Private Sub wskProgress_ConnectionRequest(ByVal requestID As Long)

    ' Check if the control's State is closed. If not,
    ' close the connection before accepting the new
    ' connection.
    With wskProgress
        If .State <> sckClosed Then
            .Close
        End If
        ' Accept the request with the requestID
        ' parameter.
        .Accept requestID
    End With
End Sub

Private Sub wskProgress_DataArrival(ByVal bytesTotal As Long)
    Dim Received As String
    Dim Extra As String
    Dim Messages() As String
    Dim NextMess As Integer

    If bytesTotal > 0 Then
        Do
            Call wskProgress.GetData(Extra, vbString)
            Received = Received & Extra
        Loop While Right(Received, 2) <> vbCrLf And Received <> ""
        If Received <> "" Then
            ' Make sure its the right format
            ' - Ocius sends Asc(6) to acknowledge receipt of message,
            ' but this often come out as part of the following data
            ' sent to winsock, sometimes it arrives on its own.  If
            ' remove it on own the message is now blank so will loop
            ' for more data; if together this will just strip it off
            If Asc(Left(Received, 1)) = 6 Then
                ' Remove the message delimiters
                Received = Mid(Received, 2)
            End If
            ' Remove the message delimiters
            Received = Left(Received, Len(Received) - 2)
            
            ' Might get many messages at once, so load them up
            Messages = Split(Received, vbCrLf)
            For NextMess = 0 To UBound(Messages)
                ReDim Preserve myProgressMessage(UBound(myProgressMessage) + 1) As String
                myProgressMessage(UBound(myProgressMessage)) = Messages(NextMess)
                Debug.Print "Progress Message Received: " & Messages(NextMess)
            Next NextMess
        End If
    End If
End Sub

Private Sub wskProgress_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)

    Select Case Number
        Case 1
            myProgressError = Trim(CStr(Number)) & ";" & Description & ";There was a problem connecting to the Pin Pad;Connection Error"
        Case 10060, 10061
            myProgressError = Trim(CStr(Number)) & ";" & Description & ";There was a problem connecting to the Pin Pad." & vbNewLine & "Please ensure Ocius is running and logged in;Ocius Sentinel Connection Error"
        Case Else
            myProgressError = Trim(CStr(Number)) & ";" & Description & ";Please check the connection cables, Network availability etc;General"
    End Select
    wskProgress.Close
End Sub
' End of Progress Socket events
' End of Control events

Public Sub Initialise(ByVal OperationMode As GetCardDetailsOperationMode, _
                      ByVal RemoveCardSetting As GetCardDetailsRemoveCardSetting, _
                      Optional ByVal POSVersion As GetCardDetailsPOSVersion = GetCardDetailsPOSVersion.egcdpvStandard, _
                      Optional ByVal IsEFTCard As Boolean = False)
    
    myOperationMode = OperationMode
    myRemoveCardSetting = RemoveCardSetting
    myPOSVersion = POSVersion
    myIsEFT = IsEFTCard
End Sub

Public Function ReadCard(ByRef CardNumber As String) As Boolean
    Dim oGetCardDetailsRecord As New cGetCardDetails
    Dim oProgressMessage As cOutputProgressMessage
    Dim ErrorParts() As String
    Dim ErrorReadingMessage As Boolean
    Dim GetCardDetailsRequestSent As Boolean
    Dim Start As Date
    Dim Finish As Date
    Dim TimeTaken As Integer
    Dim StillReading As Boolean
    Dim CardError As String
        
    If Not oGetCardDetailsRecord Is Nothing Then
        ReDim myProgressMessage(0) As String
        MessageReceived = False
        ReceivedMessage = ""
        Call DebugMsg(ModuleName, "ReadCard", endlDebug, "Connecting to sockets")
        ' Make sure connected before starting
        Start = Time
        Do
            Finish = Time
            Call FireUpSockets(moCommideaCfg.IntegrationPort, moCommideaCfg.ProgressPort)
            TimeTaken = ((Minute(Finish) * 60) + Second(Finish)) - ((Minute(Start) * 60) + Second(Start))
        Loop While TimeTaken < moCommideaCfg.ConnectionTimeout And Not SocketsConnected
        Call DebugMsg(ModuleName, "ReadCard", endlDebug, "Time taken to connect to sockets = " & CStr(TimeTaken))
        ' Generate a GetCardDetails record to send to PED via socket
        Call oGetCardDetailsRecord.Initialise(myOperationMode, myRemoveCardSetting, myPOSVersion, myIsEFT)
        If SendMessage(oGetCardDetailsRecord) Then
            StillReading = True
            CardError = ""
            Do
                Call WaitForData
                If ReceivedMessage <> "" Then
                    With oGetCardDetailsRecord
                        .ReceivedMessage = ReceivedMessage
                        If Not .OutputCardDetails Is Nothing Then
                            With .OutputCardDetails
                                Select Case .Result
                                    Case 0 '    - Success
                                        CardNumber = .CardData
                                        ReadCard = True
                                    Case -4 '   - Card not recognised
                                        CardError = "Card not recognised"
                                    Case -5 '   - EFT Card presented when expecting a non EFT, or vice-a-versa
                                        If myIsEFT Then
                                            CardError = "Not a Cr/Dr card"
                                        Else
                                            CardError = "Not a Loyalty/Discount card"
                                        End If
                                    Case -12 '  - Invalid record
                                        CardError = "Invalid record"
                                    Case -30 '  - Processing failed
                                        CardError = "Card processing failed"
                                    Case -70 '  - Operation mode not supported
                                        If myIsEFT Then
                                            CardError = "Reading Cr/Dr card not supported"
                                        Else
                                            CardError = "Reading Loyalty/Discount card not supported"
                                        End If
                                    Case -85 '  - User not logged in
                                        CardError = "You are not logged into the PED"
                                    Case -90 '  - Service not allowed
                                        CardError = "Cannot read card at this stage"
                                    Case -99 '  - Retrieval cancelled
                                        RaiseEvent UserCancelled
                                    Case -135 ' - Timeout waiting for card
                                        CardError = "Timed out waiting for card"
                                    Case Else
                                        CardError = "Unknown error reading card"
                                End Select
                            End With
                        Else
                            CardError = "Failed understanding card details from PED"
                        End If
                    End With
                    StillReading = False
                ElseIf WinSockError <> "" Then
                    CardError = WinSockError
                    ErrorParts = Split(WinSockError, ";")
                    CardError = ErrorParts(1) & vbCrLf & ErrorParts(2) & " (" & ErrorParts(0) & ")"
                    Call ClearError
                    StillReading = False
                ElseIf Not NextProgressMessage Is Nothing Then
                    Do While Not NextProgressMessage Is Nothing
                        Set oProgressMessage = ReadNextProgressMessage
                        If Not oProgressMessage Is Nothing Then
                            With oProgressMessage
                                If .Result = 100 Then
                                    Select Case .StatusID
                                        Case esiReady
                                            If ReceivedMessage = "" Then
                                                RaiseEvent AwaitingCard
                                            Else
                                                ' Get a ready message after succesful card read
                                                StillReading = False
                                                Exit Do
                                            End If
                                        Case esiPresentCard
                                            RaiseEvent AwaitingCard
                                        Case Else
                                            ' add code here if any other messages are expected
                                    End Select
                                Else
                                    ' error
                                    CardError = .OperatorMessage
                                    StillReading = False
                                    Exit Do
                                End If
                            End With
                        Else
                            ' error
                            CardError = "Failed reading card details"
                            StillReading = False
                            Exit Do
                        End If
                    Loop
                End If
                If CardError <> "" Then
                    RaiseEvent CardError(CardError)
                End If
            Loop While StillReading
        End If
    End If
End Function

Public Sub CloseMe()

    Call UnloadCommideaForm(Me)
End Sub

Private Function SendMessage(ByRef GetCardDetailsRecord As Object) As Boolean
    
    With Me.wskIntegration
        Select Case .State
            Case sckConnected
                Call DebugMsg(ModuleName, "SendMessage", endlDebug, "Calling Send")
                MessageReceived = False
                ReceivedMessage = ""
                Call .SendData(GetCardDetailsRecord.ToIntegrationMessage)  'Send Transaction
                ' Make sure the data is actually sent, now
                DoEvents
                Call DebugMsg(ModuleName, "SendMessage", endlDebug, "WaitForData")
                SendMessage = True
            Case sckError
                Call DebugMsg(ModuleName, "SendMessage", endlDebug, "Error Found")
            Case Else
        End Select
    End With
End Function ' SendMessage

Private Property Get MessageReceived() As Boolean

    MessageReceived = myMessageReceived
End Property

Private Property Let MessageReceived(ByVal NewMessageReceived As Boolean)

    myMessageReceived = NewMessageReceived
End Property

Private Property Get ReceivedMessage() As String

    ReceivedMessage = myReceivedMessage
End Property

Private Property Let ReceivedMessage(ByVal NewReceivedMessage As String)

    If NewReceivedMessage = "" Then
        myReceivedMessage = NewReceivedMessage
    Else
        ' Sometimes just get a single Asc() = 6 character before message,
        ' this can be ignored (treated as though blank)
        If Len(NewReceivedMessage) = 1 And Asc(NewReceivedMessage) = 6 Then
            myReceivedMessage = ""
        Else
            myReceivedMessage = NewReceivedMessage
        End If
    End If
End Property

Private Property Get ProgressMessage() As String()

    ProgressMessage = myProgressMessage
End Property

Private Function NextProgressMessage() As cOutputProgressMessage

    If UBound(ProgressMessage()) > 0 Then
        Set NextProgressMessage = New cOutputProgressMessage
        If Not NextProgressMessage Is Nothing Then
            With NextProgressMessage
                .Initialise
                ' Always the 1st message as move all messages up 1 after this one is processed
                If Not .ParseReceivedMessage(ProgressMessage()(1)) Then
                    NextProgressMessage = Nothing
                End If
            End With
        End If
    End If
End Function

Private Function ReadNextProgressMessage() As cOutputProgressMessage
    Dim Delete As Integer

    If UBound(ProgressMessage()) > 0 Then
        ' read the message and then remove read message from list
        ' read...
        Set ReadNextProgressMessage = NextProgressMessage
        ' remove...
        ' Move message up one (overwriting the 1st that has now been processed)
        For Delete = 1 To UBound(myProgressMessage) - 1
            myProgressMessage(Delete) = myProgressMessage(Delete + 1)
        Next Delete
        ' Remove duplicate last message from end
        ReDim Preserve myProgressMessage(UBound(myProgressMessage) - 1) As String
    Else
        Set ReadNextProgressMessage = Nothing
    End If
End Function

Private Function NextProgressMessageIs(ByVal IsID As ProgressStatusID) As Boolean
    Dim NextMess As cOutputProgressMessage

    Set NextMess = NextProgressMessage
    If Not NextMess Is Nothing Then
        If NextMess.StatusID = IsID Then
            NextProgressMessageIs = True
        End If
    End If
End Function

Private Function ConnectionError() As Boolean

    If WinSockError <> "" Then
        ConnectionError = True
    End If
End Function

Private Function WinSockError() As String

    If myIntegrationError <> "" Then
        WinSockError = myIntegrationError
    Else
        If myProgressError <> "" Then
            WinSockError = myProgressError
        End If
    End If
End Function

Private Sub ClearError()

    myIntegrationError = ""
    myProgressError = ""
End Sub

Private Function FireUpSockets(ByVal IntegrationPort As String, ByVal ProgressPort As String) As Boolean

    ' Set up the progress socket to receive data
    With Me.wskProgress
        Call .Close
        .RemotePort = ProgressPort
        .RemoteHost = "127.0.0.1"    ' .LocalIP
        Call DebugMsg(ModuleName, "FireUpSockets", endlDebug, "Progress Winsock state is currently " & .State)
        Call DebugMsg(ModuleName, "FireUpSockets", endlDebug, "Progress Winsock connecting to " & "127.0.0.1")    ' .LocalIP)
        Call .Connect
        While .State = sckConnecting
            DoEvents
        Wend
        Call DebugMsg(ModuleName, "FireUpSockets", endlDebug, "Progress Winsock state is currently " & .State)
    End With
    With Me.wskIntegration
        Call .Close
        .RemotePort = IntegrationPort
        .RemoteHost = "127.0.0.1"    ' .LocalIP
        Call DebugMsg(ModuleName, "FireUpSockets", endlDebug, "Integration Winsock state is currently " & .State)
        Call DebugMsg(ModuleName, "FireUpSockets", endlDebug, "Integration Winsock connecting to " & "127.0.0.1")    ' .LocalIP)
        Call .Connect
        While .State = sckConnecting
            DoEvents
        Wend
        Call DebugMsg(ModuleName, "FireUpSockets", endlDebug, "Integration Winsock state is currently " & .State)
    End With
    FireUpSockets = Me.wskIntegration.State = sckConnected And Me.wskProgress.State = sckConnected
End Function

Private Function SocketsConnected()

    SocketsConnected = Me.wskIntegration.State = sckConnected And Me.wskProgress.State = sckConnected
End Function

Private Sub WaitForData()
    Dim Start As Date
    Dim Finish As Date
    Dim TimeTaken As Integer

    Start = Time
    Do
        Call Wait(1)
        DoEvents
        Finish = Time
        TimeTaken = ((Minute(Finish) * 60) + Second(Finish)) - ((Minute(Start) * 60) + Second(Start))
    Loop While ReceivedMessage = "" And NextProgressMessage Is Nothing And Not ConnectionError
    If ReceivedMessage <> "" Then
        MessageReceived = True
    End If
End Sub 'WaitForData

Private Function ModuleName() As String

    ModuleName = "frmReadCardDetails"
End Function


Public Sub ICommideaForm_InitializeBeforeLoad(ByVal commideaCfg As CommideaConfiguration)
    Set moCommideaCfg = commideaCfg
End Sub

Public Sub ICommideaForm_ResetAfterUnload()
    Set moCommideaCfg = Nothing
End Sub
