VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "BGiftRequestRecord"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IRequestRecord

Public Enum BGiftRequestTransactionType
    BalanceEnquiry = 1
    NewCardOrTopUp = 2
    Sale = 3
    Refund = 4
    Cash = 5
    Void = 6
End Enum

' Account ID under which to process the transaction.
' Conditional.
' Note: This is not required for void transactions. It is mandatory for all other transaction types.
Private myAccountID As String

' Barclays Gift Transaction type.
' Mandatory.
Private myTransactionType As BGiftRequestTransactionType

' Transaction value to be processed
' Conditional
' Note: This is not required for Void / Balance Enquiry transaction types. For all other transaction types this is mandatory.
Private myAmount As Double ' Decimal actually

' Merchant supplied reference for the purpose of tracking the transaction.
' Conditional
' Note: The requirement of this field will be dependent upon Merchant settings.
Private myReference As String


Public Sub Initialise(AccountID As String, _
                      transactionType As BGiftRequestTransactionType, _
                      amount As Double, _
                      reference As String)

    myAccountID = AccountID
    myTransactionType = transactionType
    myAmount = amount
    myReference = reference
    
End Sub

Public Function IRequestRecord_ToIntegrationMessage() As String

    IRequestRecord_ToIntegrationMessage = "BGIFT" _
                         & "," & myAccountID _
                         & "," & myTransactionType _
                         & "," & FormatTransactionAmount(myAmount) _
                         & "," & myReference _
                         & "," & vbCrLf
End Function
