VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IContinueTRecordCreator"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function GetVoiceReferralAuthorisedContinueTransactionRecord(ByVal AuthorisationCode As String) As cContinueTransactionRecord
End Function

Public Function GetReprintContinueTransactionRecord() As cContinueTransactionRecord
End Function

Public Function GetContinueTransactionRecordForContinueTRecordActionType(ByVal ContinueType As ContinueTRecordAction) As cContinueTransactionRecord
End Function

Public Function GetChargeAuthCodeContinueTransactionRecord(ByVal AuthorisationCode As String) As cContinueTransactionRecord
End Function

Public Function GetCashbackRequiredContinueTransactionRecord(ByVal CashbackAmount As String) As cContinueTransactionRecord
End Function

Public Function GetReplaceAccountContinueTransactionRecord(ByVal ManagerPin As String) As cContinueTransactionRecord
End Function

Public Function GetSelectPaypointAccountContinueTransactionRecord(ByVal AccountID As String) As cContinueTransactionRecord
End Function

Public Function GetSelectPaypointOptionContinueTransactionRecord(ByVal OptionId As String) As cContinueTransactionRecord
End Function
