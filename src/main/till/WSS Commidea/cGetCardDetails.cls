VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cGetCardDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Enum GetCardDetailsOperationMode
    egcdomNonEFT = 0
    egcdomEFT = 1
End Enum

Public Enum GetCardDetailsRemoveCardSetting
    egcdrcsDoNotRemove = 0
    egcdrcsRemove = 1
End Enum

Public Enum GetCardDetailsPOSVersion
    egcdpvStandard = 0
    egcdpvVersion1ReturnCardHash = 1
End Enum


Private myOperationMode As GetCardDetailsOperationMode
Private myRemoveCardSetting As GetCardDetailsRemoveCardSetting
Private myPOSVersion As GetCardDetailsPOSVersion

' Result of continuation transaction in form of progress message
Private myReceivedMessage As String
Private myResultMessage As String
Private myOutputCardDetails As cOutputCardDetails

Public Sub Initialise(ByVal OperationMode As GetCardDetailsOperationMode, _
                      ByVal RemoveCardSetting As GetCardDetailsRemoveCardSetting, _
                      Optional ByVal POSVersion As GetCardDetailsPOSVersion = GetCardDetailsPOSVersion.egcdpvStandard, _
                      Optional ByVal IsEFTCard As Boolean = False)

    myOperationMode = OperationMode
    myRemoveCardSetting = RemoveCardSetting
    myPOSVersion = POSVersion
    Set myOutputCardDetails = New cOutputCardDetails
    If Not myOutputCardDetails Is Nothing Then
        Call myOutputCardDetails.Initialise(IsEFTCard)
    End If
End Sub

Public Function ToIntegrationMessage() As String

    ' POS Version optional but 0 value is the same as not sending it, so just default to 0 (see Initialise)
    ToIntegrationMessage = "GDET," _
                         & Trim(CStr(myOperationMode)) & "," _
                         & Trim(CStr(myRemoveCardSetting)) & "," _
                         & Trim(CStr(myPOSVersion)) & vbCrLf
End Function

Public Property Get OutputCardDetails() As cOutputCardDetails

    Set OutputCardDetails = myOutputCardDetails
End Property

Public Property Get ReceivedMessage() As String

    ReceivedMessage = myReceivedMessage
End Property

Public Property Let ReceivedMessage(ByVal NewMessage As String)

    myReceivedMessage = NewMessage
    With myOutputCardDetails
        If .ParseReceivedMessage(myReceivedMessage) Then
'            Select Case .Result
'                Case Is > 0
'                Case Else
'            End Select
        Else
            ' error failed parsing message received from PED
        End If
'        ' Status/Error message returned here
'        myResultMessage = .Status
    End With
End Property

Public Property Get ResultMessage() As String

    ResultMessage = myResultMessage
End Property

