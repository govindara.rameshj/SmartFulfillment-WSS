VERSION 5.00
Begin VB.Form frmVoiceReferral 
   Caption         =   "Voice Referral"
   ClientHeight    =   5160
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7215
   LinkTopic       =   "Form1"
   ScaleHeight     =   5160
   ScaleWidth      =   7215
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAuthorise 
      Caption         =   "&Authorise"
      Default         =   -1  'True
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Left            =   4620
      TabIndex        =   12
      Top             =   4515
      Width           =   1170
   End
   Begin VB.CommandButton cmdAbort 
      Cancel          =   -1  'True
      Caption         =   "A&bort"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Left            =   5880
      TabIndex        =   13
      Top             =   4515
      Width           =   1170
   End
   Begin VB.TextBox txtAuthCode 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3885
      TabIndex        =   11
      Top             =   3780
      Width           =   3165
   End
   Begin VB.Label lblPromptAuthCode 
      Caption         =   "Authorisation &Code:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   525
      TabIndex        =   10
      Top             =   3885
      Width           =   3165
   End
   Begin VB.Label lblAmount 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   3990
      TabIndex        =   9
      Top             =   3150
      Width           =   3060
   End
   Begin VB.Label lblPromptAmount 
      Caption         =   "Amount:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   525
      TabIndex        =   8
      Top             =   3150
      Width           =   1275
   End
   Begin VB.Label lblTerminalID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   3990
      TabIndex        =   7
      Top             =   2625
      Width           =   3060
   End
   Begin VB.Label lblPromptTerminalID 
      Caption         =   "Terminal ID:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   525
      TabIndex        =   6
      Top             =   2625
      Width           =   2010
   End
   Begin VB.Label lblMerchantID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   3990
      TabIndex        =   5
      Top             =   2100
      Width           =   3060
   End
   Begin VB.Label lblPromptMerchantID 
      Caption         =   "Merchant ID:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   525
      TabIndex        =   4
      Top             =   2100
      Width           =   2010
   End
   Begin VB.Label lblTelNo 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   3990
      TabIndex        =   3
      Top             =   1575
      Width           =   3060
   End
   Begin VB.Label lblPromptTelNo 
      Caption         =   "Tel. No.:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   525
      TabIndex        =   2
      Top             =   1575
      Width           =   1380
   End
   Begin VB.Label lblPromptCallAuthorisationCentre 
      Alignment       =   2  'Center
      Caption         =   " Call Authorisation Centre"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   0
      TabIndex        =   1
      Top             =   840
      UseMnemonic     =   0   'False
      Width           =   7215
   End
   Begin VB.Label lblPromptForCard 
      Alignment       =   2  'Center
      Caption         =   "(ask for customer's card)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   19.5
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   0
      TabIndex        =   0
      Top             =   0
      UseMnemonic     =   0   'False
      Width           =   7215
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmVoiceReferral"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Implements ICommideaForm

Private moCommideaCfg As CommideaConfiguration
Private mbAuthorised As Boolean

Public Event Duress()

Private Sub cmdAbort_Click()

    If Me.Visible Then
        Me.Hide
    End If
    Call DebugMsg(ModuleName, "Abort", endlTraceOut)
End Sub

Private Sub cmdAuthorise_Click()

    Call DebugMsg(ModuleName, "cmdAuthorise", endlTraceIn)
    If Len(txtAuthCode.Text) > 9 Then
        Call DebugMsg(ModuleName, "cmdAuthorise", endlDebug, "Authorisation code exceeds maximum length of 9.")
        Call MsgBoxEx("WARNING : Authorisation Code cannot be more than 9 characters long.", vbExclamation, "Authorisation Code is too long.")
        txtAuthCode.SetFocus
    Else
        Call DebugMsg(ModuleName, "cmdAuthorise", endlDebug, "Authorisation code OK.")
        mbAuthorised = True
        If Me.Visible Then
            Me.Hide
        End If
    End If
    Call DebugMsg(ModuleName, "cmdAuthorise", endlTraceOut)
End Sub

Private Sub Form_Load()

    Me.BackColor = moCommideaCfg.MsgBoxPromptColour
    Me.Visible = False
    Me.lblPromptForCard.BackColor = moCommideaCfg.MsgBoxPromptColour
    Me.lblPromptCallAuthorisationCentre.BackColor = vbButtonFace
    Me.lblPromptTelNo.BackColor = moCommideaCfg.MsgBoxPromptColour
    Me.lblPromptMerchantID.BackColor = moCommideaCfg.MsgBoxPromptColour
    Me.lblPromptTerminalID.BackColor = moCommideaCfg.MsgBoxPromptColour
    Me.lblPromptAmount.BackColor = moCommideaCfg.MsgBoxPromptColour
    Me.lblTelNo.BackColor = moCommideaCfg.MsgBoxPromptColour
    Me.lblMerchantID.BackColor = moCommideaCfg.MsgBoxPromptColour
    Me.lblTerminalID.BackColor = moCommideaCfg.MsgBoxPromptColour
    Me.lblAmount.BackColor = moCommideaCfg.MsgBoxPromptColour
    Me.lblPromptAuthCode.BackColor = moCommideaCfg.MsgBoxPromptColour
    Call DebugMsg(ModuleName, "Form_Load", endlTraceOut)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If moCommideaCfg.UnderDuressKeyCode <> 0 And KeyCode = moCommideaCfg.UnderDuressKeyCode Then
        KeyCode = 0
        RaiseEvent Duress
    End If
End Sub

Private Sub txtAuthCode_Change()

    cmdAuthorise.Enabled = txtAuthCode.Text <> ""
End Sub

Private Sub txtAuthCode_KeyPress(KeyAscii As Integer)
    If (KeyAscii >= Asc("A") And KeyAscii <= Asc("Z")) _
        Or (KeyAscii >= Asc("a") And KeyAscii <= Asc("z")) _
        Or (KeyAscii >= Asc("0") And KeyAscii <= Asc("9")) Then
        ' pass all printable keys
        If Len(txtAuthCode.Text) = 9 Then
            KeyAscii = 0
        ElseIf KeyAscii >= Asc("a") And KeyAscii <= Asc("z") Then
            KeyAscii = KeyAscii - 32
        End If
    ElseIf KeyAscii = 13 Or KeyAscii = 8 Or KeyAscii = 9 Or KeyAscii = 27 Then
        ' pass keys: enter, backspace, tab, escape
    Else
        KeyAscii = 0
    End If
End Sub

Private Sub txtAuthCode_LostFocus()

    cmdAuthorise.Enabled = txtAuthCode.Text <> ""
End Sub

Public Property Get AuthorisationCode()

    If mbAuthorised Then
        AuthorisationCode = txtAuthCode.Text
    End If
End Property

Public Function ShowMe(ByVal ReferralTelNo As String, ByVal MerchantNo As String, ByVal TerminalID As String, ByVal amount As String) As Boolean
    Me.lblAmount.Caption = amount
    Me.lblMerchantID.Caption = MerchantNo
    Me.lblTelNo.Caption = ReferralTelNo
    Me.lblTerminalID.Caption = TerminalID
    mbAuthorised = False
    Call Me.Show(vbModal)
    ShowMe = mbAuthorised
    Call DebugMsg(ModuleName, "ShowMe", endlTraceOut)
End Function

Private Function ModuleName() As String

    ModuleName = "frmVoiceReferral"
End Function


Public Sub ICommideaForm_InitializeBeforeLoad(ByVal commideaCfg As CommideaConfiguration)
    Set moCommideaCfg = commideaCfg
End Sub

Public Sub ICommideaForm_ResetAfterUnload()
    Set moCommideaCfg = Nothing
End Sub

