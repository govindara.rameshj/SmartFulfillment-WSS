Attribute VB_Name = "ProcessList"
Option Explicit

Public Declare Function GetVersionExA Lib "kernel32" (lpVersionInformation As OSVERSIONINFO) As Integer

Public Type OSVERSIONINFO
   dwOSVersionInfoSize  As Long 'Specifies the length, in bytes, of the structure.
   dwMajorVersion       As Long 'Major Version Number
   dwMinorVersion       As Long 'Minor Version Number
   dwBuildNumber        As Long 'Build Version Number
   dwPlatformId         As Long 'Operating System Running, see below
   szCSDVersion As String * 128 'Windows NT: Contains a null-terminated string,
                                'such as "Service Pack 3", that indicates the latest
                                'Service Pack installed on the system.
                                'If no Service Pack has been installed, the string is empty.
                                'Windows 95: Contains a null-terminated string that provides
                                'arbitrary additional information about the operating system
End Type

Public Const hNull = 0

'  dwPlatformId defines:
Public Const VER_PLATFORM_WIN32s = 0            'Win32s on Windows 3.1.
Public Const VER_PLATFORM_WIN32_WINDOWS = 1     'Win32 on Windows 95 or Windows 98.
                                                'For Windows 95, dwMinorVersion is 0.
                                                'For Windows 98, dwMinorVersion is 1.
Public Const VER_PLATFORM_WIN32_NT = 2          'Win32 on Windows NT.

'See also KB article Q175030

'===========================================================
'WINDOWS NT ONLY
'-----------------------------------------------------------
'PSAPI.DLL does not operate on Windows 95/98, use the
'ToolHelp32 APIs instead.
'Remember to distribute the PSAPI.DLL file (available in the
'Platform SDK. with any executable that uses it, as it is
'not currently distributed with the operating system.
'===========================================================

Public Declare Function CloseHandle Lib "Kernel32.dll" (ByVal Handle As Long) As Long
Public Declare Function OpenProcess Lib "Kernel32.dll" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcId As Long) As Long
Public Declare Function EnumProcesses Lib "psapi.dll" (ByRef lpidProcess As Long, ByVal cb As Long, ByRef cbNeeded As Long) As Long
Public Declare Function GetModuleFileNameExA Lib "psapi.dll" (ByVal hProcess As Long, ByVal hModule As Long, ByVal ModuleName As String, ByVal nSize As Long) As Long
Public Declare Function EnumProcessModules Lib "psapi.dll" (ByVal hProcess As Long, ByRef lphModule As Long, ByVal cb As Long, ByRef cbNeeded As Long) As Long

Public Const PROCESS_QUERY_INFORMATION = 1024
Public Const PROCESS_VM_READ = 16
Public Const MAX_PATH = 260
Public Const STANDARD_RIGHTS_REQUIRED = &HF0000
Public Const SYNCHRONIZE = &H100000
'STANDARD_RIGHTS_REQUIRED Or SYNCHRONIZE Or &HFFF
Public Const PROCESS_ALL_ACCESS = &H1F0FFF

'==========================================
'WINDOWS 95/98 ONLY, ToolHelp32 APIs don't
'exist on Windows NT, use PSAPI.DLL instead
'==========================================

Public Declare Function CreateToolhelp32Snapshot Lib "kernel32" (ByVal dwFlags As Long, ByVal th32ProcessID As Long) As Long
Public Declare Function Process32First Lib "kernel32" (ByVal hSnapshot As Long, lppe As PROCESSENTRY32) As Long
Public Declare Function Process32Next Lib "kernel32" (ByVal hSnapshot As Long, lppe As PROCESSENTRY32) As Long
Public Declare Function Module32First Lib "kernel32" (ByVal hSnapshot As Long, lpme As MODULEENTRY32) As Long
Public Declare Function Module32Next Lib "kernel32" (ByVal hSnapshot As Long, lpme As MODULEENTRY32) As Long

Private Const MAX_MODULE_NAME32 As Integer = 255
Private Const MAX_MODULE_NAME32plus As Integer = MAX_MODULE_NAME32 + 1
Public Const TH32CS_SNAPPROCESS = &H2&
Public Const TH32CS_SNAPMODULE = &H8&

Public Type PROCESSENTRY32
   dwSize               As Long 'Specifies the length, in bytes, of the structure.
   cntUsage             As Long 'Number of references to the process.
   th32ProcessID        As Long 'Identifier of the process.
   th32DefaultHeapID    As Long 'Identifier of the default heap for the process.
   th32ModuleID         As Long 'Module identifier of the process. (Associated exe)
   cntThreads           As Long 'Number of execution threads started by the process.
   th32ParentProcessID  As Long 'Identifier of the process that created the process being examined.
   pcPriClassBase       As Long 'Base priority of any threads created by this process.
   dwFlags              As Long 'Reserved; do not use.
   szExeFile            As String * MAX_PATH 'Path and filename of the executable file for the process.
End Type

Public Type MODULEENTRY32
    dwSize          As Long 'Specifies the length, in bytes, of the structure.
    th32ModuleID    As Long 'Module identifier in the context of the owning process.
    th32ProcessID   As Long 'Identifier of the process being examined.
    GlblcntUsage    As Long 'Global usage count on the module.
    ProccntUsage    As Long 'Module usage count in the context of the owning process.
    modBaseAddr     As Long 'Base address of the module in the context of the owning process.
    modBaseSize     As Long 'Size, in bytes, of the module.
    hModule         As Long 'Handle to the module in the context of the owning process.
    szModule        As String * MAX_MODULE_NAME32plus 'String containing the module name.
    szExePath       As String * MAX_PATH 'String containing the location (path) of the module.
End Type

' Required for forcing Ocius Sentinel closure
Private Type LUID
   lowpart As Long
   highpart As Long
End Type

Private Type TOKEN_PRIVILEGES
    PrivilegeCount As Long
    LuidUDT As LUID
    Attributes As Long
End Type

Private Const TOKEN_ADJUST_PRIVILEGES = &H20
Private Const TOKEN_QUERY = &H8
Private Const SE_PRIVILEGE_ENABLED = &H2
'Private Const PROCESS_ALL_ACCESS = &H1F0FFF

Private Declare Function GetVersion Lib "kernel32" () As Long
Private Declare Function GetCurrentProcess Lib "kernel32" () As Long
'Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Private Declare Function OpenProcessToken Lib "advapi32" (ByVal ProcessHandle As Long, ByVal DesiredAccess As Long, TokenHandle As Long) As Long
Private Declare Function LookupPrivilegeValue Lib "advapi32" Alias "LookupPrivilegeValueA" (ByVal lpSystemName As String, ByVal lpName As String, lpLuid As LUID) As Long
Private Declare Function AdjustTokenPrivileges Lib "advapi32" (ByVal TokenHandle As Long, ByVal DisableAllPrivileges As Long, NewState As TOKEN_PRIVILEGES, ByVal BufferLength As Long, PreviousState As Any, ReturnLength As Any) As Long
'Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function TerminateProcess Lib "kernel32" (ByVal hProcess As Long, ByVal uExitCode As Long) As Long
' End of Required for forcing Ocius Sentinel closure


' Required for process list
Public Function GetOSVersion() As Long
    '=======================================
    'Returns the Operating System being used
    '1 = Windows 95 / Windows 98
    '2 = Windows NT
    '=======================================
    Dim osinfo   As OSVERSIONINFO
    Dim retvalue As Integer
    
    With osinfo
        .dwOSVersionInfoSize = 148
        .szCSDVersion = Space$(128)
        retvalue = GetVersionExA(osinfo)
        GetOSVersion = .dwPlatformId
    End With
End Function

Public Function StrZToStr(s As String) As String
   StrZToStr = Left$(s, Len(s) - 1)
End Function

'=========================================================
'=========================================================
'Checks the list of processes and the
'module used by each process for Ocius Sentinel exe
'=========================================================
Public Function OciusProcessRunning95() As Boolean
    Dim lReturnID       As Long
    Dim hSnapProcess    As Long
    Dim hSnapModule     As Long
    Dim sName           As String
    Dim proc            As PROCESSENTRY32
    Dim module          As MODULEENTRY32
    Dim iProcesses      As Integer
    Dim iModules        As Integer
    
    'Get a 'at this moment' snapshot of all the processes
    hSnapProcess = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0)
    
    If hSnapProcess <> hNull Then
        'Initialize the processentry structure
        proc.dwSize = Len(proc)
        'Get first process
        lReturnID = Process32First(hSnapProcess, proc)
        'Iterate through each process with an ID that <> 0
        Do While lReturnID
            'Add the process to the listbox
            sName = StrZToStr(proc.szExeFile)
            If InStr(1, sName, "OciusSentinel.exe", vbTextCompare) > 0 Then
                OciusProcessRunning95 = True
                'Close the Process snapshot handle
                CloseHandle hSnapProcess
                lReturnID = 0
            Else
                'Get next process
                lReturnID = Process32Next(hSnapProcess, proc)
            End If
        Loop
        'Close the Process snapshot handle
        CloseHandle hSnapProcess
    End If
End Function

'=========================================================
'Checks the list of processes and the
'module used by each process for Ocius Sentinel exe
'=========================================================
Public Function OciusProcessRunningNT() As Boolean
    Dim cb                  As Long
    Dim cbNeeded            As Long
    Dim NumElements         As Long
    Dim ProcessIDs()        As Long
    Dim cbNeeded2           As Long
'    Dim NumElements2        As Long
    Dim Modules(1 To 1024)  As Long
    Dim lRet                As Long
    Dim ModuleName          As String
    Dim nSize               As Long
    Dim hProcess            As Long
    Dim i                   As Long
    Dim sModName            As String
    
    'Get the array containing the process id's for each process object
    cb = 8
    cbNeeded = 96
    
    'One important note should be made. Although the documentation
    'names the returned DWORD "cbNeeded", there is actually no way
    'to find out how big the passed in array must be. EnumProcesses()
    'will never return a value in cbNeeded that is larger than the
    'size of array value that you passed in the cb parameter.
    
    'if cbNeeded == cb upon return, allocate a larger array
    'and try again until cbNeeded is smaller than cb.
    Do While cb <= cbNeeded
       cb = cb * 2
       ReDim ProcessIDs(cb / 4) As Long
       lRet = EnumProcesses(ProcessIDs(1), cb, cbNeeded)
    Loop
    
    'calculate how many process IDs were returned
    NumElements = cbNeeded / 4
    
    For i = 1 To NumElements
        'Get a handle to the Process
        hProcess = OpenProcess(PROCESS_QUERY_INFORMATION Or PROCESS_VM_READ, 0, ProcessIDs(i))
        ' Iterate through each process with an ID that <> 0
        If hProcess Then
            'Retrieve the number of bytes that the array of module handles requires
            lRet = EnumProcessModules(hProcess, Modules(1), 1024, cbNeeded2)
            'Get an array of the module handles for the specified process
            lRet = EnumProcessModules(hProcess, Modules(1), cbNeeded2, cbNeeded2)
            'If the Module Array is retrieved, Get the ModuleFileName
            If lRet <> 0 Then
                'Fill the ModuleName buffer with spaces
                ModuleName = Space(MAX_PATH)
                'Preset buffer size
                nSize = 500
                'Get the module file name
                lRet = GetModuleFileNameExA(hProcess, Modules(1), ModuleName, nSize)
                'Get the module file name out of the buffer, lRet is how
                'many characters the string is, the rest of the buffer is spaces
                sModName = Left$(ModuleName, lRet)
                If InStr(1, sModName, "OciusSentinel.exe", vbTextCompare) > 0 Then
                    OciusProcessRunningNT = True
                    'Close the handle to the process
                    lRet = CloseHandle(hProcess)
                    Exit For
                End If
            End If
        End If
        'Close the handle to the process
        lRet = CloseHandle(hProcess)
    Next
End Function

Private Function GetOciusPID() As Long

    Select Case GetOSVersion()
        Case VER_PLATFORM_WIN32_WINDOWS
            GetOciusPID = GetOciusPID95()
        Case VER_PLATFORM_WIN32_NT
            GetOciusPID = GetOciusPIDNT()
        Case Else
            'MsgBox "Operating system not recognized!", vbCritical
    End Select
End Function

'=========================================================
'=========================================================
'Checks the list of processes and the
'module used by each process for Ocius Sentinel exe
'=========================================================
Private Function GetOciusPID95() As Long
    Dim lReturnID       As Long
    Dim hSnapProcess    As Long
    Dim hSnapModule     As Long
    Dim sName           As String
    Dim proc            As PROCESSENTRY32
    Dim module          As MODULEENTRY32
    Dim iProcesses      As Integer
    Dim iModules        As Integer
    
    'Get a 'at this moment' snapshot of all the processes
    hSnapProcess = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0)
    
    If hSnapProcess <> hNull Then
        'Initialize the processentry structure
        proc.dwSize = Len(proc)
        'Get first process
        lReturnID = Process32First(hSnapProcess, proc)
        'Iterate through each process with an ID that <> 0
        Do While lReturnID
            'Add the process to the listbox
            sName = StrZToStr(proc.szExeFile)
            If InStr(1, sName, "OciusSentinel.exe", vbTextCompare) > 0 Then
                GetOciusPID95 = lReturnID
                'Close the Process snapshot handle
                CloseHandle hSnapProcess
                lReturnID = 0
            Else
                'Get next process
                lReturnID = Process32Next(hSnapProcess, proc)
            End If
        Loop
        'Close the Process snapshot handle
        CloseHandle hSnapProcess
    End If
End Function

'=========================================================
'Checks the list of processes and the
'module used by each process for Ocius Sentinel exe
'=========================================================
Private Function GetOciusPIDNT() As Long
    Dim cb                  As Long
    Dim cbNeeded            As Long
    Dim NumElements         As Long
    Dim ProcessIDs()        As Long
    Dim cbNeeded2           As Long
'    Dim NumElements2        As Long
    Dim Modules(1 To 1024)  As Long
    Dim lRet                As Long
    Dim ModuleName          As String
    Dim nSize               As Long
    Dim hProcess            As Long
    Dim i                   As Long
    Dim sModName            As String
    
    'Get the array containing the process id's for each process object
    cb = 8
    cbNeeded = 96
    
    'One important note should be made. Although the documentation
    'names the returned DWORD "cbNeeded", there is actually no way
    'to find out how big the passed in array must be. EnumProcesses()
    'will never return a value in cbNeeded that is larger than the
    'size of array value that you passed in the cb parameter.
    
    'if cbNeeded == cb upon return, allocate a larger array
    'and try again until cbNeeded is smaller than cb.
    Do While cb <= cbNeeded
       cb = cb * 2
       ReDim ProcessIDs(cb / 4) As Long
       lRet = EnumProcesses(ProcessIDs(1), cb, cbNeeded)
    Loop
    
    'calculate how many process IDs were returned
    NumElements = cbNeeded / 4
    
    For i = 1 To NumElements
        'Get a handle to the Process
        hProcess = OpenProcess(PROCESS_QUERY_INFORMATION Or PROCESS_VM_READ, 0, ProcessIDs(i))
        ' Iterate through each process with an ID that <> 0
        If hProcess Then
            'Retrieve the number of bytes that the array of module handles requires
            lRet = EnumProcessModules(hProcess, Modules(1), 1024, cbNeeded2)
            'Get an array of the module handles for the specified process
            lRet = EnumProcessModules(hProcess, Modules(1), cbNeeded2, cbNeeded2)
            'If the Module Array is retrieved, Get the ModuleFileName
            If lRet <> 0 Then
                'Fill the ModuleName buffer with spaces
                ModuleName = Space(MAX_PATH)
                'Preset buffer size
                nSize = 500
                'Get the module file name
                lRet = GetModuleFileNameExA(hProcess, Modules(1), ModuleName, nSize)
                'Get the module file name out of the buffer, lRet is how
                'many characters the string is, the rest of the buffer is spaces
                sModName = Left$(ModuleName, lRet)
                If InStr(1, sModName, "OciusSentinel.exe", vbTextCompare) > 0 Then
                    GetOciusPIDNT = ProcessIDs(i)
                    'Close the handle to the process
                    lRet = CloseHandle(hProcess)
                    Exit For
                End If
            End If
        End If
        'Close the handle to the process
        lRet = CloseHandle(hProcess)
    Next
End Function

Public Function CloseOciusSentinel(Optional ByVal ExitCode As Long) As Boolean
    Dim hToken As Long
    Dim hProcess As Long
    Dim tp As TOKEN_PRIVILEGES
    
    ' Windows NT/2000 require a special treatment
    ' to ensure that the calling process has the
    ' privileges to shut down the system
    
    ' under NT the high-order bit (that is, the sign bit)
    ' of the value retured by GetVersion is cleared
    If GetVersion() >= 0 Then
        ' open the tokens for the current process
        ' exit if any error
        If OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES Or _
            TOKEN_QUERY, hToken) = 0 Then
            GoTo CleanUp
        End If
        
        ' retrieves the locally unique identifier (LUID) used
        ' to locally represent the specified privilege name
        ' (first argument = "" means the local system)
        ' Exit if any error
        If LookupPrivilegeValue("", "SeDebugPrivilege", tp.LuidUDT) = 0 Then
            GoTo CleanUp
        End If
    
        ' complete the TOKEN_PRIVILEGES structure with the # of
        ' privileges and the desired attribute
        tp.PrivilegeCount = 1
        tp.Attributes = SE_PRIVILEGE_ENABLED
    
        ' try to acquire debug privilege for this process
        ' exit if error
        If AdjustTokenPrivileges(hToken, False, tp, 0, ByVal 0&, _
            ByVal 0&) = 0 Then
            GoTo CleanUp
        End If
    End If
    
    ' now we can finally open the other process
    ' while having complete access on its attributes
    ' exit if any error
    hProcess = OpenProcess(PROCESS_ALL_ACCESS, 0, GetOciusPID)
    If hProcess Then
        ' call was successful, so we can kill the application
        ' set return value for this function
        CloseOciusSentinel = (TerminateProcess(hProcess, ExitCode) <> 0)
        ' close the process handle
        CloseHandle hProcess
    End If
    
    If GetVersion() >= 0 Then
        ' under NT restore original privileges
        tp.Attributes = 0
        AdjustTokenPrivileges hToken, False, tp, 0, ByVal 0&, ByVal 0&
        
CleanUp:
        If hToken Then CloseHandle hToken
    End If
End Function

