VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cContinueTransactionRecord"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Enum ContinueTRecordAction
    etraNotSpecified = 0
    etraBypassPIN = 1
    etraContinueTxn = 2
    etraConfirmSignature = 3
    etraRejectSignature = 4
    etraReprintReceipt = 5
    etraKeyedEntryRequired = 6
    etraVoiceReferralAuthorised = 7
    etraVoiceReferralRejected = 8
    etraGratuityRequired = 9
    etraGratuityNotRequired = 10
    etraGratuityOnPED = 11
    etraCancelTransaction = 12
    etraAlternatePayment = 13
    etraCustomerPresentRequired = 14
    etraCustomerNotPresentRequired = 15
    etraECommerceRequired = 16
    etraMailOrderRequired = 17
    etraTelephoneOrderRequired = 18
    etraChangeCard = 19
    etraConfirmAuthCode = 20
    etraRejectAuthCode = 21
    etraChargeAuthCode = 22
    etraReverseUATICCTxn = 23
    etraRetryDevice = 24
    etraContinueWithoutDevice = 25
    etraAbortDeviceConnectivity = 26
    etraRetryDownload = 27
    etraCancelDownload = 28
    etraCashbackRequired = 29
    etraCashbackNotRequired = 30
    etraRestart = 31
    etraAcceptUnsafeDownload = 32
    etraRejectUnsafe = 33
    etraReplaceAccount = 34
    etraCancelReplaceAccount = 35
    etraConfirmGratuity = 36
    etraChangeGratuity = 37
    etraAccountOnFileRegistrationRequired = 38
    etraAccountOnFileRegistrationNotRequired = 39
    etraReconnectToServer = 40
    etraAbortReconnectToServer = 41
    etraSelectPaypointAccount = 42
    etraSelectPaypointOption = 43
    etraRetryPaypointConfirmation = 44
    etraCancelPaypointConfirmation = 45
    etraAcceptLicenceKey = 46
    etraRejectLicenceKey = 47
    etraCancelLicenceKeyVerification = 48
    etraContinueLicenceKeyVerification = 49
End Enum

Public Enum ContinuationMessageActionParameter
    ecmapRePrintOpt = 1
    ecmapAuthCode = 2
    ecmapCardNumber = 3
    ecmapGratuity = 4
    ecmapExpiryDate = 5
    ecmapStartDate = 6
    ecmapIssueNumber = 7
    ecmapAVSHouse = 8
    ecmapAVSPostCode = 9
    ecmapCSC = 10
    ecmapCashback = 11
    ecmapMgrPIN = 12
    ecmapPayPointAccID = 13
    ecmapPayPointOptionID = 14
End Enum

Public Type ContinuationMessageParameter
    Name As String
    Value As String
End Type

Private myActionID As ContinueTRecordAction
Private myActionParameters As Collection

' Result of continuation transaction in form of progress message
Private myReceivedMessage As String
Private myResultMessage As String
Private myOutputTRecordV4 As cOutputTRecordV4

Public Sub Initialise(ByVal ActionID As ContinueTRecordAction, _
                      Optional ByVal ActionParameters As Collection = Nothing)
    Dim Param As Integer

    myActionID = ActionID
    Set myActionParameters = ActionParameters
    If Not myActionParameters Is Nothing Then
        With myActionParameters
            For Param = 1 To .Count
                ' Check for being correct type?
                ' Reprint option parameter value is always 'FILE'
                If StrComp(.Item(Param).Name, ContinuationMessageActionParameters()(ContinuationMessageActionParameter.ecmapRePrintOpt), vbTextCompare) = 0 Then
                    .Item(Param).Value = "FILE"
                End If
            Next Param
        End With
    End If
    
    Set myOutputTRecordV4 = New cOutputTRecordV4
    If Not myOutputTRecordV4 Is Nothing Then
        Call myOutputTRecordV4.Initialise
    End If
End Sub

Public Function ToIntegrationMessage() As String
    Dim Param As Integer

    ToIntegrationMessage = "CONTTXN," & Trim(CStr(myActionID)) & ","
    Select Case myActionID
        Case ContinueTRecordAction.etraReprintReceipt
            If Not myActionParameters Is Nothing Then
                With myActionParameters
                    For Param = 1 To .Count
                        If StrComp(.Item(Param).Name, ContinuationMessageActionParameters()(ContinuationMessageActionParameter.ecmapRePrintOpt), vbTextCompare) = 0 Then
                            ToIntegrationMessage = ToIntegrationMessage _
                                                 & ContinuationMessageActionParameters()(ContinuationMessageActionParameter.ecmapRePrintOpt) _
                                                 & "=FILE"
                            ' Delimit if more potential params
                            If Param < .Count Then
                                ToIntegrationMessage = ToIntegrationMessage _
                                                     & ";"
                            End If
                        End If
                    Next Param
                    ' Remove any final ';'
                    If Right(ToIntegrationMessage, 1) = ";" Then
                        ToIntegrationMessage = Left(ToIntegrationMessage, Len(ToIntegrationMessage) - 1)
                    End If
                End With
            End If
        Case ContinueTRecordAction.etraVoiceReferralAuthorised, ContinueTRecordAction.etraChargeAuthCode
            If Not myActionParameters Is Nothing Then
                With myActionParameters
                    For Param = 1 To .Count
                        If StrComp(.Item(Param).Name, ContinuationMessageActionParameters()(ContinuationMessageActionParameter.ecmapAuthCode), vbTextCompare) = 0 Then
                            ToIntegrationMessage = ToIntegrationMessage _
                                                 & ContinuationMessageActionParameters()(ContinuationMessageActionParameter.ecmapAuthCode) _
                                                 & "=" & .Item(Param).Value
                            ' Delimit if more potential params
                            If Param < .Count Then
                                ToIntegrationMessage = ToIntegrationMessage _
                                                     & ";"
                            End If
                        End If
                    Next Param
                    ' Remove any final ';'
                    If Right(ToIntegrationMessage, 1) = ";" Then
                        ToIntegrationMessage = Left(ToIntegrationMessage, Len(ToIntegrationMessage) - 1)
                    End If
                End With
            End If
        Case ContinueTRecordAction.etraCashbackRequired
            If Not myActionParameters Is Nothing Then
                With myActionParameters
                    For Param = 1 To .Count
                        If StrComp(.Item(Param).Name, ContinuationMessageActionParameters()(ContinuationMessageActionParameter.ecmapCashback), vbTextCompare) = 0 Then
                            ToIntegrationMessage = ToIntegrationMessage _
                                                 & ContinuationMessageActionParameters()(ContinuationMessageActionParameter.ecmapCashback) _
                                                 & "=" & .Item(Param).Value
                            ' Delimit if more potential params
                            If Param < .Count Then
                                ToIntegrationMessage = ToIntegrationMessage _
                                                     & ";"
                            End If
                        End If
                    Next Param
                    ' Remove any final ';'
                    If Right(ToIntegrationMessage, 1) = ";" Then
                        ToIntegrationMessage = Left(ToIntegrationMessage, Len(ToIntegrationMessage) - 1)
                    End If
                End With
            End If
        Case ContinueTRecordAction.etraReplaceAccount
            If Not myActionParameters Is Nothing Then
                With myActionParameters
                    For Param = 1 To .Count
                        If StrComp(.Item(Param).Name, ContinuationMessageActionParameters()(ContinuationMessageActionParameter.ecmapMgrPIN), vbTextCompare) = 0 Then
                            ToIntegrationMessage = ToIntegrationMessage _
                                                 & ContinuationMessageActionParameters()(ContinuationMessageActionParameter.ecmapMgrPIN) _
                                                 & "=" & .Item(Param).Value
                            ' Delimit if more potential params
                            If Param < .Count Then
                                ToIntegrationMessage = ToIntegrationMessage _
                                                     & ";"
                            End If
                        End If
                    Next Param
                    ' Remove any final ';'
                    If Right(ToIntegrationMessage, 1) = ";" Then
                        ToIntegrationMessage = Left(ToIntegrationMessage, Len(ToIntegrationMessage) - 1)
                    End If
                End With
            End If
        Case ContinueTRecordAction.etraSelectPaypointAccount
            If Not myActionParameters Is Nothing Then
                With myActionParameters
                    For Param = 1 To .Count
                        If StrComp(.Item(Param).Name, ContinuationMessageActionParameters()(ContinuationMessageActionParameter.ecmapPayPointAccID), vbTextCompare) = 0 Then
                            ToIntegrationMessage = ToIntegrationMessage _
                                                 & ContinuationMessageActionParameters()(ContinuationMessageActionParameter.ecmapPayPointAccID) _
                                                 & "=" & .Item(Param).Value
                            ' Delimit if more potential params
                            If Param < .Count Then
                                ToIntegrationMessage = ToIntegrationMessage _
                                                     & ";"
                            End If
                        End If
                    Next Param
                    ' Remove any final ';'
                    If Right(ToIntegrationMessage, 1) = ";" Then
                        ToIntegrationMessage = Left(ToIntegrationMessage, Len(ToIntegrationMessage) - 1)
                    End If
                End With
            End If
        Case ContinueTRecordAction.etraSelectPaypointOption
            If Not myActionParameters Is Nothing Then
                With myActionParameters
                    For Param = 1 To .Count
                        If StrComp(.Item(Param).Name, ContinuationMessageActionParameters()(ContinuationMessageActionParameter.ecmapPayPointOptionID), vbTextCompare) = 0 Then
                            ToIntegrationMessage = ToIntegrationMessage _
                                                 & ContinuationMessageActionParameters()(ContinuationMessageActionParameter.ecmapPayPointOptionID) _
                                                 & "=" & .Item(Param).Value
                            ' Delimit if more params
                            If Param < .Count Then
                                ToIntegrationMessage = ToIntegrationMessage _
                                                     & ";"
                            End If
                        End If
                    Next Param
                    ' Remove any final ';'
                    If Right(ToIntegrationMessage, 1) = ";" Then
                        ToIntegrationMessage = Left(ToIntegrationMessage, Len(ToIntegrationMessage) - 1)
                    End If
                End With
            End If
        Case Else
    End Select
    ToIntegrationMessage = ToIntegrationMessage & vbCrLf
End Function

Public Function ContinuationMessageActionParameters() As String()
    ReDim CMAP(1 To 14) As String

    CMAP(ecmapRePrintOpt) = "REPRINTOPT"
    CMAP(ecmapAuthCode) = "AUTHCODE"
    CMAP(ecmapCardNumber) = "CARDNUMBER"
    CMAP(ecmapGratuity) = "GRATUITY"
    CMAP(ecmapExpiryDate) = "EXPIRYDATE"
    CMAP(ecmapStartDate) = "STARTDATE"
    CMAP(ecmapIssueNumber) = "ISSUENUMBER"
    CMAP(ecmapAVSHouse) = "AVSHOUSE"
    CMAP(ecmapAVSPostCode) = "AVSPOSTCODE"
    CMAP(ecmapCSC) = "CSC"
    CMAP(ecmapCashback) = "CASHBACK"
    CMAP(ecmapMgrPIN) = "MGRPIN"
    CMAP(ecmapPayPointAccID) = "PAYPOINTACCID"
    CMAP(ecmapPayPointOptionID) = "PAYPOINTOPTIONID"
    ContinuationMessageActionParameters = CMAP
End Function

Public Property Get OutputTRecordV4() As cOutputTRecordV4

    Set OutputTRecordV4 = myOutputTRecordV4
End Property

Public Property Get ReceivedMessage() As String

    ReceivedMessage = myReceivedMessage
End Property

Public Property Let ReceivedMessage(ByVal NewMessage As String)

    myReceivedMessage = NewMessage
    With myOutputTRecordV4
        If .ParseReceivedMessage(myReceivedMessage) Then
'            Select Case .Result
'                Case Is > 0
'                Case Else
'            End Select
        Else
            ' error failed parsing message received from PED
        End If
'        ' Status/Error message returned here
'        myResultMessage = .Status
    End With
End Property

Public Property Get ResultMessage() As String

    ResultMessage = myResultMessage
End Property


