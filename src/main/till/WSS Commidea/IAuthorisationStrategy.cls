VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IAuthorisationStrategy"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Enum TransactionCompleteState
    NotCOmpleted = 0
    NearlyCompleted = 1
    Completed = 2
End Enum


Public Sub Initialize(newTranEngine As ITransactionEngine)
End Sub

Public Function ProcessReversedTransactionResult(authResult As IAuthorizationDetails) As TransactionCompleteState
End Function

' Process esiContinueRequired progress status
Public Sub ProcessContinueRequiredProgressStatus()
End Sub

' Process any ProgressStatus which is not covered by frmTransaction.DoAuthorisation loop
' Returns true if the processinf has been done. False if it is required to be processed by caller.
Public Function ExtraProcessProgressStatus(StatusID As ProgressStatusID) As Boolean
End Function

' Process esiContinueRequired progress status.
' Returns true if the processinf has been done. False if it is required to be processed by caller.
Public Function ProcessSignatureConfirmationRequiredProgressStatus() As Boolean
End Function

' Returns counter which is used to calculate receipt printing attempts while
' esiPrinting* progress message processing
Public Function GetReceiptPrintCounter() As Counter
End Function

' Called after a colleague chooses if receipt is printed incorrectly and it requires reprint
' Returns true if PED prints new receipt immediately, false - if not.
Public Function ProcessReceiptRePrintRequired() As Boolean
End Function

' Change th UI state if receipt printing is failed
Public Sub ShowStateForReceiptPrintFailure(ReceiptType As String, cancelledAfterMaxReprintAttempts As Boolean)
End Sub

' Return true if the passed StatusID should not be processed by receipt printing routine.
Public Function SkipPrintingReceiptProcessing(StatusID As ProgressStatusID) As Boolean
End Function
