VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cLogoutRecord"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Enum LogoutRecordFunction
    elrfLogout = 0
    elrfQReport = 1
    elrfExit = 2
    elrfExitAndQ = 3
    elrfSubmitOfflineTxns = 4
End Enum

Private myLogoutRecordFunction As LogoutRecordFunction
Private myReceivedMessage As String
Private myLogoutSuccessful As Boolean
Private myResultMessage As String

Private myReceivedRecord As cReceivedIntegrationMessage

Public Sub Initialise(Optional ByVal LogoutFunction As LogoutRecordFunction = elrfLogout)

    myLogoutRecordFunction = LogoutFunction
    ' Parse the received message into one of these
    Set myReceivedRecord = New cReceivedIntegrationMessage
End Sub

Public Function ToIntegrationMessage() As String

    ' Not allowing any menu options at the moment as running screenless
    ToIntegrationMessage = "O," & Trim(CStr(myLogoutRecordFunction)) & vbCrLf
End Function

Public Property Get LogoutSuccessful() As Boolean

    LogoutSuccessful = myLogoutSuccessful
End Property

Public Property Get ReceivedMessage() As String

    ReceivedMessage = myReceivedMessage
End Property

Public Property Let ReceivedMessage(ByVal NewMessage As String)

    myReceivedMessage = NewMessage
    With myReceivedRecord
        If .ParseReceivedMessage(myReceivedMessage) Then
            Select Case .Result
                Case 0, -85
                    ' Successful log out (0), or no user logged in (-85)
                    myLogoutSuccessful = True
                Case Else
                    myLogoutSuccessful = False
            End Select
            ' Status/Error message returned here
            myResultMessage = .Message
        End If
    End With
End Property

Public Property Get ResultMessage() As String

    ResultMessage = myResultMessage
End Property
