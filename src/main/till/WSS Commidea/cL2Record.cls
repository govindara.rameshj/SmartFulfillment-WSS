VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cL2Record"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Type SubMenuReports
    LiveStore As Boolean
    X As Boolean
    Z As Boolean
    Session As Boolean
    Transaction As Boolean
    SummarySettlement As Boolean
    DetailedSettlement As Boolean
    Q As Boolean
    StoredOfflineTxn As Boolean
End Type

Public Type SubMenuVoucherType
    Enabled As Boolean
    Disabled As Boolean
End Type

Public Type SubMenuTransactionManagement
    Reports As SubMenuReports
    SubmitOfflineTxns As Boolean
End Type

Public Type SubMenuReprint
    MerchantReceipt As Boolean
    CustomerReceipt As Boolean
    LastReport As Boolean
End Type

Public Type SubMenuSystem
    PrintStoredAIDs As Boolean
    SystemInfo As Boolean
    VoucherType As SubMenuVoucherType
    ChangeSetupPIN As Boolean
    Download As Boolean
End Type

Public Type SubMenuOrdering
    ProductList As Boolean
    PlaceOrder As Boolean
End Type

Public Type SubMenuUserManagement
    ChangeUserID As Boolean
    ChangeUserPIN As Boolean
    ChangeStationaryPIN As Boolean
End Type

Public Type SubMenuTerminalManagement
    Power As Boolean
    RebootPinpad As Boolean
    SignalStrength As Boolean
End Type

Public Type SubMenuMenu
    TransactionManagement As SubMenuTransactionManagement
    Reprint As SubMenuReprint
    System As SubMenuSystem
    Ordering As SubMenuOrdering
    UserManagement As SubMenuUserManagement
    TerminalManagement As SubMenuTerminalManagement
End Type

Public Type SubMenuSelectBill
    CloseBillOnPOS As Boolean
    GetBill As Boolean
    GetPrintBill As Boolean
    PrintBill As Boolean
End Type

Public Type SubMenuOther
    Reserved As Boolean
    PayPoint As Boolean
    Givex As Boolean
    MiVoucher As Boolean
    UKASH As Boolean
    GRTFS As Boolean
    BarclaysBonus As Boolean
    BarclaysGift As Boolean
    Chockstone As Boolean
End Type

Public Type MenuOptions
    NewTransation As Boolean
    Menu As SubMenuMenu
    SelectBill As SubMenuSelectBill
    Logoff As Boolean
    Other As SubMenuOther
End Type

Private myUserID As String
Private myPIN As String
Private myMenuOptions As MenuOptions ' Not in use in screenless mode
Private myReceivedMessage As String
Private myLoginSuccessful As Boolean
Private myResultMessage As String

Private myReceivedRecord As cReceivedIntegrationMessage

Public Sub Initialise(Optional ByVal UserID As String = "", Optional ByVal PIN As String = "")

    myUserID = UserID
    myPIN = PIN
    'myMenuOptions = MenuOpts
    ' Parse the received message into one of these
    Set myReceivedRecord = New cReceivedIntegrationMessage
End Sub

Public Function ToIntegrationMessage() As String

    ' Not allowing any menu options at the moment as running screenless
    ToIntegrationMessage = "L2," & myUserID & "," & myPIN & ", " & vbCrLf
End Function

Public Property Get LoginSuccessful() As Boolean

    LoginSuccessful = myLoginSuccessful
End Property

Public Property Get ReceivedMessage() As String

    ReceivedMessage = myReceivedMessage
End Property

Public Property Let ReceivedMessage(ByVal NewMessage As String)

    myReceivedMessage = NewMessage
    With myReceivedRecord
        If .ParseReceivedMessage(myReceivedMessage) Then
            Select Case .Result
                Case 0, -84
                    ' Successful log in (0), or this user all ready logged in (0)
                    myLoginSuccessful = True
                Case Else
                    myLoginSuccessful = False
            End Select
            ' Status/Error message returned here
            myResultMessage = .Message
        End If
    End With
End Property

Public Property Get ResultMessage() As String

    ResultMessage = myResultMessage
End Property

