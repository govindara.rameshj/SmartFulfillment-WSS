VERSION 5.00
Begin VB.Form frmValidateSignature 
   Caption         =   "Validate Signature"
   ClientHeight    =   1500
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3420
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   1500
   ScaleWidth      =   3420
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdValidate 
      Caption         =   "&Accept"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Left            =   105
      TabIndex        =   1
      Top             =   840
      Width           =   1170
   End
   Begin VB.CommandButton cmdReject 
      Cancel          =   -1  'True
      Caption         =   "&Decline"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Left            =   2100
      TabIndex        =   2
      Top             =   840
      Width           =   1170
   End
   Begin VB.Label lblPromptValidateSignature 
      Alignment       =   2  'Center
      Caption         =   " Signature OK?"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   0
      TabIndex        =   0
      Top             =   0
      UseMnemonic     =   0   'False
      Width           =   3330
   End
End
Attribute VB_Name = "frmValidateSignature"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Implements ICommideaForm

Private moCommideaCfg As CommideaConfiguration

Private mbValidated As Boolean
Private ReturnKeyPressed As Boolean

Public Event Duress()

Private Sub cmdReject_Click()

    If Me.Visible Then
        Me.Hide
    End If
    Call DebugMsg(ModuleName, "Reject", endlTraceOut)
End Sub

Private Sub cmdValidate_Click()

    mbValidated = True
    If Me.Visible Then
        Me.Hide
    End If
    Call DebugMsg(ModuleName, "Validate", endlTraceOut)
End Sub

Private Sub Form_Load()

    Me.BackColor = moCommideaCfg.MsgBoxPromptColour
    Me.Visible = False
    Me.lblPromptValidateSignature.BackColor = moCommideaCfg.MsgBoxPromptColour
    Call DebugMsg(ModuleName, "Form_Load", endlTraceOut)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 And ReturnKeyPressed = True Then
        Call DebugMsg(ModuleName, "Return Key Pressed again. Clearing the Key Press", endlDebug, "Return Key pressed...")
        KeyCode = 0
    End If
    If KeyCode = 13 Then ReturnKeyPressed = True
    If moCommideaCfg.UnderDuressKeyCode <> 0 And KeyCode = moCommideaCfg.UnderDuressKeyCode Then
        KeyCode = 0
        RaiseEvent Duress
    End If
End Sub

Public Function ShowMe() As Boolean

    ' Reset in case called again before unloading
    mbValidated = False
    Call Me.Show(vbModal)
    ShowMe = mbValidated
    Call DebugMsg(ModuleName, "ShowMe", endlTraceOut)
End Function

Private Function ModuleName() As String

    ModuleName = "frmValidateSignature"
End Function


Public Sub ICommideaForm_InitializeBeforeLoad(ByVal commideaCfg As CommideaConfiguration)
    Set moCommideaCfg = commideaCfg
End Sub

Public Sub ICommideaForm_ResetAfterUnload()
    Set moCommideaCfg = Nothing
End Sub

