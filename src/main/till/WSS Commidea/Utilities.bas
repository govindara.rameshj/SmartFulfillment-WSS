Attribute VB_Name = "Utilities"
Option Explicit

Private Const ModuleName As String = "Utilities"


' Required for GetHostIPAddress
' Adapted from http://support.microsoft.com/kb/160215
Private Const WS_VERSION_REQD = &H101
Private Const WS_VERSION_MAJOR = WS_VERSION_REQD \ &H100 And &HFF&
Private Const WS_VERSION_MINOR = WS_VERSION_REQD And &HFF&
'Private Const MIN_SOCKETS_REQD = 1
Private Const SOCKET_ERROR = -1
Private Const WSADescription_Len = 256
Private Const WSASYS_Status_Len = 128

Private Type HOSTENT
    hName As Long
    hAliases As Long
    hAddrType As Integer
    hLength As Integer
    hAddrList As Long
End Type

Private Type WSADATA
    wversion As Integer
    wHighVersion As Integer
    szDescription(0 To WSADescription_Len) As Byte
    szSystemStatus(0 To WSASYS_Status_Len) As Byte
    iMaxSockets As Integer
    iMaxUdpDg As Integer
    lpszVendorInfo As Long
End Type

Private Declare Function WSAGetLastError Lib "WSOCK32.DLL" () As Long
Private Declare Function WSAStartup Lib "WSOCK32.DLL" (ByVal wVersionRequired As Integer, lpWSAData As WSADATA) As Long
Private Declare Function WSACleanup Lib "WSOCK32.DLL" () As Long

Private Declare Function gethostname Lib "WSOCK32.DLL" (ByVal hostname$, ByVal HostLen As Long) As Long
Private Declare Function gethostbyname Lib "WSOCK32.DLL" (ByVal hostname$) As Long
Private Declare Sub RtlMoveMemory Lib "kernel32" (hpvDest As Any, ByVal hpvSource&, ByVal cbCopy&)
' End of Required for GetHostIPAddress

' Required for ResetFocus
Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Private Declare Function BringWindowToTop Lib "user32" (ByVal hwnd As Long) As Long
' End of Required for ResetFocus

' Required to make sure modal form stays on top
Private Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal X As Long, y, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
Private Const HWND_TOPMOST = -1
Private Const HWND_NOTOPMOST = -2
Private Const SWP_NOMOVE = &H2
Private Const SWP_NOSIZE = &H1
Private Const TOPMOST_FLAGS = SWP_NOMOVE Or SWP_NOSIZE

Public Sub MakeNormal(hwnd As Long)

    SetWindowPos hwnd, HWND_NOTOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS
End Sub

Public Sub MakeTopMost(windowName As String)
    Dim hwnd As Long
    
    hwnd = FindWindow(vbNullString, windowName)
    SetWindowPos hwnd, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS
End Sub
' End of Required for modal form

' Required for GetHostIPAddress
Public Function GetHostIPAddress() As String
    Dim hostname As String * 256
    Dim hostent_addr As Long
    Dim host As HOSTENT
    Dim hostip_addr As Long
    Dim temp_ip_address() As Byte
    Dim i As Integer
    Dim ip_address As String
    Dim ErrorMessage As String
    
    ErrorMessage = SocketsInitialize
    If ErrorMessage = "" Then
        If gethostname(hostname, 256) = SOCKET_ERROR Then
            ErrorMessage = "Windows Sockets error " & Str(WSAGetLastError())
        Else
            hostname = Trim$(hostname)
            hostent_addr = gethostbyname(hostname)
            If hostent_addr = 0 Then
                ErrorMessage = "Winsock.dll is not responding."
            Else
                RtlMoveMemory host, hostent_addr, LenB(host)
                RtlMoveMemory hostip_addr, host.hAddrList, 4
                'will have all of the IP address if machine is  multi-homed, just get 1st
                If host.hLength > 0 Then
                    ReDim temp_ip_address(1 To host.hLength)
                    RtlMoveMemory temp_ip_address(1), hostip_addr, host.hLength
                    For i = 1 To host.hLength
                        ip_address = ip_address & temp_ip_address(i) & "."
                    Next
                    GetHostIPAddress = Mid$(ip_address, 1, Len(ip_address) - 1)
                End If
                ErrorMessage = SocketsCleanup
            End If
        End If
    End If
    If ErrorMessage <> "" Then
        Call Err.Raise(vbObjectError + 1, AppNameStub & ModuleName & "." & "GetHostIPAddress", ErrorMessage)
    End If
End Function

Private Function hibyte(ByVal wParam As Integer)

    hibyte = wParam \ &H100 And &HFF&
End Function

Private Function lobyte(ByVal wParam As Integer)

    lobyte = wParam And &HFF&
End Function

Private Function SocketsCleanup() As String
    Dim lReturn As Long

    lReturn = WSACleanup()

    If lReturn <> 0 Then
        SocketsCleanup = "Socket error " & Trim$(Str$(lReturn)) & " occurred in Cleanup"
    End If
End Function

Private Function SocketsInitialize() As String
    Dim WSAD As WSADATA
    Dim iReturn As Integer
    Dim sLowByte As String, sHighByte As String, sMsg As String

    If WSAStartup(WS_VERSION_REQD, WSAD) <> 0 Then
        SocketsInitialize = "Winsock.dll is not responding."
    Else
        If lobyte(WSAD.wversion) < WS_VERSION_MAJOR _
        Or (lobyte(WSAD.wversion) = WS_VERSION_MAJOR _
        And hibyte(WSAD.wversion) < WS_VERSION_MINOR) Then
            sHighByte = Trim$(Str$(hibyte(WSAD.wversion)))
            sLowByte = Trim$(Str$(lobyte(WSAD.wversion)))
            SocketsInitialize = "Windows Sockets version " & sLowByte & "." & sHighByte
            SocketsInitialize = sMsg & " is not supported by winsock.dll "
        End If
    
    '    'iMaxSockets is not used in winsock 2. So the following check is only
    '    'necessary for winsock 1. If winsock 2 is requested,
    '    'the following check can be skipped.
    
    '    If WSAD.iMaxSockets < MIN_SOCKETS_REQD Then
    '        sMsg = "This application requires a minimum of "
    '        sMsg = sMsg & Trim$(Str$(MIN_SOCKETS_REQD)) & " supported sockets."
    '        MsgBox sMsg
    '        End
    '    End If
    End If
End Function
' End of Required for GetHostIPAddress

Public Function OciusSentinelLaunched() As Boolean

    Select Case GetOSVersion()
        Case VER_PLATFORM_WIN32_WINDOWS
            OciusSentinelLaunched = OciusProcessRunning95()
        Case VER_PLATFORM_WIN32_NT
            OciusSentinelLaunched = OciusProcessRunningNT()
        Case Else
            'MsgBox "Operating system not recognized!", vbCritical
    End Select
End Function


Public Function AppNameStub() As String

    ' Title the right thing to use?
    AppNameStub = App.Title & "."
End Function

' Wrestle focus back (off Ocius Sentinel)
Public Function ResetFocus(strName As String) As Boolean
    Dim WinWnd As Long
    
    'the code to find and show window
    Call DebugMsg("Utilities", "ResetFocus", endlTraceIn, "Attempting to reset focus from Ocius Sentinel to " & strName)
    WinWnd = FindWindow(vbNullString, strName)
    If WinWnd <> 0 Then
        Call DebugMsg("Utilities", "ResetFocus", endlTraceIn, "Found window for " & strName & ". Handle = " & CStr(WinWnd))
        Call DebugMsg("Utilities", "ResetFocus", endlTraceIn, "Attempting to bring " & strName & " to the top")
        If BringWindowToTop(WinWnd) Then
            Call DebugMsg("Utilities", "ResetFocus", endlTraceIn, "Brought " & strName & " to top, now calling AppActivate")
            On Error Resume Next
            Call AppActivate(strName)
            On Error GoTo 0
            Call DebugMsg("Utilities", "ResetFocus", endlTraceIn, "Called AppActivate")
        Else
            Call DebugMsg("Utilities", "ResetFocus", endlTraceIn, "Failed to bring " & strName & " to the top")
        End If
    Else
        Call DebugMsg("Utilities", "ResetFocus", endlTraceIn, "Failed resetting focus from Ocius Sentinel to " & strName)
    End If
    Call DebugMsg("Utilities", "ResetFocus", endlTraceOut, "Leaving reset focus")
End Function


Private Function ModuleNameStub() As String

    ModuleNameStub = AppNameStub & ModuleName & "."
End Function

' What if have a different currency?  Not likely for a while
Public Function FormatTransactionAmount(ByVal Value As Double) As String

    Value = CDbl(Abs(Value * 100) / 100)
    FormatTransactionAmount = FormatNumber(Value, 2, vbTrue, vbFalse, vbFalse)
End Function

