VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cParameterInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

    Private myID As Integer
    Private myName As String
    Private myDataTableColName As String
    Private myValueType As ParamValueType

    Private Const ModuleName As String = "cParameterInfo"

    Public Sub Initialise(ByVal Name As String, ByVal DataTableColName As String, ByVal ID As Integer, ByVal ValType As ParamValueType)

        myName = Name
        myDataTableColName = DataTableColName
        myID = ID
        myValueType = ValType
    End Sub
    
    Public Function ConversionErrorMess() As String
    
        ConversionErrorMess = "Failed converting " & myName & " parameter into a" & IIf(InStr(1, "aeiou", Left(ToString, 1), vbTextCompare) > 0, "n ", " ") & ToString
    End Function
    
    Public Function ToString() As String
    
        Select Case myValueType
            Case ParamValueType.epvtString
                ToString = "string"
            Case ParamValueType.epvtInteger
                ToString = "integer"
            Case ParamValueType.epvtDouble
                ToString = "double"
            Case ParamValueType.epvtFloat
                ToString = "float"
            Case ParamValueType.epvtAdditionalParamList
                ToString = "collection of additional parameters"
            Case Else
                ToString = "unknown type"
        End Select
    End Function
        
    Private Function ModuleNameStub() As String
    
        ModuleNameStub = AppNameStub & ModuleName & "."
    End Function

