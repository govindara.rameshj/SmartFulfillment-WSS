VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ITransactionEngine"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Sub Initialise(ByVal commideaCfg As CommideaConfiguration)
End Sub

Public Sub Dispose()
End Sub


Public Function Authorise(ByVal oAuthorizationRequest As AuthorizationRequest) As AuthorizationResult
                          
End Function

Public Property Get ProgressMessage() As String()
End Property

Public Sub SendContinueTransactionRecord(ByVal ContinueType As ContinueTRecordAction, Optional ByVal AuthorisationCode As String = "")
End Sub

Public Sub ShowStateForReceiptPrintFailure(ReceiptType As String)
End Sub

