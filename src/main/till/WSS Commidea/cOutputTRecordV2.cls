VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cOutputTRecordV2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

    Public Enum AVSPostCodeCheckResult
        eavspccrNotProvided = 0
        eavspccrNotChecked = 1
        eavspccrMatched = 2
        eavspccrNotMatched = 4
        eavspccrReserved = 8
    End Enum
    
    Public Enum AVSHouseNumberCheckResult
        eavshncrNotProvided = 0
        eavshncrNotChecked = 1
        eavshncrMatched = 2
        eavshncrNotMatched = 4
        eavshncrReserved = 8
    End Enum
    
    Public Enum AccountOnFileRegistrationResult
            eaofrrNotSet
            eaofrrPerformed
            eaofrrSuccess
            eaofrrFailed
    End Enum
    
    Public Enum CSCCheckResult
        ecscrNotProvided = 0
        ecscrNotChecked = 1
        ecscrMatched = 2
        ecscrNotMatched = 4
        ecscrReserved = 8
    End Enum
    
    Public Enum CaptureMethodType
        ecmtContactless = 1
        ecmtSwipe = 2
        ecmtICC = 3
        ecmtKeyed = 4
    End Enum

    Private Const ModuleName As String = "cOutputTRecordV2"

    Private Enum ParameterID
        epCaptureMethod = 19
        epTransactionCurrencyCode = 20
        epOriginalTransactionValue = 21
        epOriginalCashbackValue = 22
        epOriginalGratuityValue = 23
        epOriginalTransactionCurrencyCode = 24
        epBarclaysBonusDiscountValue = 25
        epBarclaysBonusRedemptionValue = 26
        epAccountonFileRegistration = 27
        epTokenID = 28
        epAVSPostCode = 29
        epAVSHouseNumber = 30
        epCSCResult = 31
    End Enum

' Property fields
    Private myCaptureMethod As String
    Private myTransactionCurrencyCode As Integer
    Private myOriginalTransactionValue As Integer
    Private myOriginalCashbackValue As Integer
    Private myOriginalGratuityValue As Integer
    Private myOriginalTransactionCurrencyCode As Integer
    Private myBarclaysBonusDiscountValue As Integer
    Private myBarclaysBonusRedemptionValue As Integer
    Private myAccountonFileRegistration As Integer
    Private myTokenID  As String
    Private myAVSPostCode As Integer
    Private myCSCResult As Integer
    Private myAVSHouseNumber As Integer
    ' Non param based properties
    Private myUnderstood  As Boolean
    Private myErrorMessage As String
' End of property fields

    Private myOutputTRecord As cOutputTRecord
    Private myReceivedMessage As String
    Private myParameters As Collection
    Private myParameterTitles() As String
    Private myParameterColumnNames() As String


' Public methods

    Public Sub Initialise(Optional ByVal IsScreenless As Boolean = True)

        Set myOutputTRecord = New cOutputTRecord
        Call myOutputTRecord.Initialise(IsScreenless)
        Set myParameters = New Collection
        Call BuildDataArray
    End Sub
' End of Public methods

' Properties


    '        1. Transaction result:
    '           '0' - Completed
    '           '7' - Declined
    '           '-nn' - All other negative values are
    '           used to define error conditions
    '           Appendix B contains a full list of error
    '           codes and messages.
    '           Screenless transaction results:
    '           '0' - Completed
    '           '2' - Referred
    '           '5' - Declined
    '           '6' - Authorised
    '           '7' - Reversed
    '           '-nn' - Negative values are used to
    '           define error conditions
    Public Property Get TResult() As Integer
    
        TResult = myOutputTRecord.TResult
    End Property

        ' 2. Teminate Loop Reserved, Ignore
    Public Property Get TerminateLoop() As Integer

        TerminateLoop = myOutputTRecord.TerminateLoop
    End Property
    
    '  3. Values will be truncated to the correct
    '    number of decimal places
    '    required for the transaction currency.
    '    For example:
    '    1.23 = 1 (one Japanese Yen)
    '    Field will show total that will be debited
    Public Property Get TotalTranValueProcessed() As Double

        TotalTranValueProcessed = myOutputTRecord.TotalTranValueProcessed
    End Property

    ' 4 Cashback Value Double As above
    Public Property Get CashBackValue() As Double

        CashBackValue = myOutputTRecord.CashBackValue
    End Property
       
    ' 5 Gratuity Value Double As above
    Public Property Get GratuityValue() As Double

        GratuityValue = myOutputTRecord.GratuityValue
    End Property
        
    ' 6 PAN Integer The Primary Account Number
    ' (Card Number).
    ' Please note: This value will not be
    ' returned in full due to PCI requirements.
    ' The PAN will be masked apart from the
    ' last four digits, e.g. '************1234'
    Public Property Get PAN() As String

        PAN = myOutputTRecord.PAN
    End Property

    ' Expiry Date MMYY Integer Card Expiry Month and Year.
    Public Property Get ExpiryDate() As String

        ExpiryDate = myOutputTRecord.ExpiryDate
    End Property

    ' 8 Issue Number Integer Card Issue Number. Blank when scheme
    ' does not provide an issue number.
    Public Property Get IssueNumber() As String

        IssueNumber = myOutputTRecord.IssueNumber
    End Property

    ' 9 Start MMYY Integer Card start month and year
    Public Property Get StartDate() As String

        StartDate = myOutputTRecord.StartDate
    End Property

    ' 10 Transaction Date / Time Integer CCYYMMDDHHMMSS
    Public Property Get TransactionDate() As String

        TransactionDate = myOutputTRecord.TransactionDate
    End Property

    ' 11 Merchant Number Integer The Merchant Number for the given' card scheme and account.
    Public Property Get MerchantNumber() As String

        MerchantNumber = myOutputTRecord.MerchantNumber
    End Property

    ' 12 Terminal ID Integer Terminal ID used for this transaction.
    Public Property Get TerminalID() As String

        TerminalID = myOutputTRecord.TerminalID
    End Property

    ' 13 Scheme Name String Card scheme name e.g. visa etc
    Public Property Get SchemeName() As String

        SchemeName = myOutputTRecord.SchemeName
    End Property

    ' 14 Floor Limit Integer Floor limit for the card scheme/account.
    Public Property Get FloorLimit() As Double

        FloorLimit = myOutputTRecord.FloorLimit
    End Property
    
    ' 15 EFT Sequence Number Integer Four digits in the range 0001 - 9999.
    ' (Prefixed with "OL" when offline)
    Public Property Get EFTSequenceNumber() As String
        If Mid(myOutputTRecord.EFTSequenceNumber, 1, 2) = "OL" Then
            EFTSequenceNumber = Mid(myOutputTRecord.EFTSequenceNumber, 3)
        Else
            EFTSequenceNumber = myOutputTRecord.EFTSequenceNumber
        End If
    End Property
    
    ' 16 Authorisation Code String Blank if the transaction is declined or is
    ' below the floor limit.
    Public Property Get AuthorisationCode() As String

        AuthorisationCode = myOutputTRecord.AuthorisationCode
    End Property
    
    ' 17 Referral Telephone Number. Reserved, ignore
    Public Property Get ReferralTelephoneNumber() As String

        ReferralTelephoneNumber = myOutputTRecord.ReferralTelephoneNumber
    End Property
    
    ' 18 Customer Verification Method /
    ' Authorisation Message / Error Message
    ' / Status Message
    ' String As returned by communications process.
    ' Normally direct from acquirer. Also
    ' contains status message if enabled
    Public Property Get CustomerVerification() As String

        CustomerVerification = myOutputTRecord.CustomerVerification
    End Property
    
    ' 19 Capture Method String Valid values are:
    ' Contactless
    ' Swipe
    ' ICC
    ' Keyed
    Public Property Get CaptureMethod() As String

        CaptureMethod = myCaptureMethod
    End Property
    
    ' 20 Transaction Currency Code Reserved for Dynamic Currency Conversion
    Public Property Get TransactionCurrencyCode() As Integer

        TransactionCurrencyCode = myTransactionCurrencyCode
    End Property
    
    ' 21 Original Transaction Value Reserved for Dynamic Currency Conversion
    Public Property Get OriginalTransactionValue() As Integer

        OriginalTransactionValue = myOriginalTransactionValue
    End Property
    
    ' 22 Original Cashback Value Reserved for Dynamic Currency Conversion
    Public Property Get OriginalCashbackValue() As Integer

        OriginalCashbackValue = myOriginalCashbackValue
    End Property

    ' 23 Original Gratuity Value Reserved for Dynamic Currency Conversion
    Public Property Get OriginalGratuityValue() As Integer

        OriginalGratuityValue = myOriginalGratuityValue
    End Property
    
    ' 24 Original Transaction Currency
    ' Code
    ' Reserved for Dynamic Currency Conversion
    Public Property Get OriginalTransactionCurrencyCode() As Integer

        OriginalTransactionCurrencyCode = myOriginalTransactionCurrencyCode
    End Property
    
    ' 25 Barclays Bonus Discount Value Reserved for Barclays Bonus
    Public Property Get BarclaysBonusDiscountValue() As Integer

        BarclaysBonusDiscountValue = myBarclaysBonusDiscountValue
    End Property
    
    ' 26 Barclays Bonus Redemption Value Reserved for Barclays Bonus
    Public Property Get BarclaysBonusRedemptionValue() As Integer

        BarclaysBonusRedemptionValue = myBarclaysBonusRedemptionValue
    End Property
    
    ' 27 Account on File Registration
    ' Result
        ' Integer This is the result of the Account on File
        ' registration. Valid values are:
        '0' - Not Set
        '1' - Performed
        '2' - Success
        '3' - Failed
    Public Property Get AccountonFileRegistration() As Integer

        AccountonFileRegistration = myAccountonFileRegistration
    End Property
    
    ' 28 Token ID String This is the token allocated to the payment
    ' details as part of the Account on File
    ' registration process or the token used for
    ' the Account on File payment.
    Public Property Get TokenID() As String

        TokenID = myTokenID
    End Property
    
    ' 29 AVS Post Code Result Integer This is the result of any AVS post code
    ' checking. Valid values are:
    ' '0' - Not Provided
    ' '1' - Not Checked
    ' '2' - Matched
    ' '4' - Not Matched
    ' '8' - Reserved
    Public Property Get AVSPostCode() As Integer

        AVSPostCode = myAVSPostCode
    End Property

    ' 30 AVS House Number Result Integer This is the result of any AVS house number
    ' checking. Valid values are:
    ' '0' - Not Provided
    ' '1' - Not Checked
    ' '2' - Matched
    ' '4' - Not Matched
    ' '8' - Reserved
    Public Property Get AVSHouseNumber() As Integer

        AVSHouseNumber = myAVSHouseNumber
    End Property
    
    ' 31 CSC Result Integer This is the result of any CSC verification.
    ' Valid values are:
    ' '0' - Not Provided
    ' '1' - Not Checked
    ' '2' - Matched
    ' '4' - Not Matched
    ' '8' - Reserved
    Public Property Get CSCResult() As Integer

        CSCResult = myCSCResult
    End Property
    
    ' Non Parameter based properties
    
    Public Property Get CaptureMethods(ByVal MethodType As CaptureMethodType)
        Dim Methods(1 To 4) As String
    
        Methods(ecmtContactless) = "Contactless"
        Methods(ecmtSwipe) = "Swipe"
        Methods(ecmtICC) = "ICC"
        Methods(ecmtKeyed) = "Keyed"
        CaptureMethods = Methods(MethodType)
    End Property
    
    Public Property Get ErrorAction() As String
    
        ErrorAction = myOutputTRecord.ErrorAction
    End Property
    
    Public Property Get ErrorDescription() As String
    
        ErrorDescription = myOutputTRecord.ErrorDescription
    End Property
    
    Public Property Get ErrorMessage() As String
    
        ErrorMessage = myOutputTRecord.ErrorMessage
    End Property
    
    Public Property Get OutputTRecord() As cOutputTRecord
    
        Set OutputTRecord = myOutputTRecord
    End Property
    
    Public Property Get Screenless() As Boolean
    
        Screenless = myOutputTRecord.Screenless
    End Property
    
     ' This is used to parse the output record.
    ' It is the number of returned parameters from Commidea
    Public Property Get NumberOfParameters() As Integer
    
        NumberOfParameters = 31
    End Property
    
    Public Property Get ResultMessage() As String
    
        ResultMessage = myOutputTRecord.ResultMessage
    End Property
    
    Public Property Get Successful() As Boolean
    
        Successful = myOutputTRecord.Successful
    End Property
    
    Public Property Get Understood() As Boolean
    
        Understood = myUnderstood
    End Property
    ' End of Non Parameter based properties

' End of Properties

' Friend methods
    
    Friend Function ParseReceivedMessage(ByVal Message As String) As Boolean

    On Error GoTo Catch

Try:
        Clean
        ' 1st half of the Output record is the same basic structure as the cOutoutTRecord, so use that
        If myOutputTRecord.ParseReceivedMessage(Message) Then
            ' Now finish off parsing the rest of the message locally
            If LocalParseReceivedMessage(Message) Then
                ParseReceivedMessage = True
            End If
        End If

        myUnderstood = True
        ParseReceivedMessage = True

        Exit Function
Catch:
        Call Err.Raise(vbObjectError + 1, ModuleNameStub & "ParseReceivedMessage", "Failed to Parse output Transaction Record.")
    End Function
' End of friend methods

' Private methods

    Private Sub Clean()
        
        myCaptureMethod = ""
        myAccountonFileRegistration = AccountOnFileRegistrationResult.eaofrrNotSet
        myTokenID = ""
        myAVSPostCode = AVSPostCodeCheckResult.eavspccrNotProvided
        myAVSHouseNumber = AVSHouseNumberCheckResult.eavshncrNotProvided
        myCSCResult = CSCCheckResult.ecscrNotProvided
        myUnderstood = False
    End Sub
    
    Private Sub CreateParameterInfoAndAddtoArray(ByVal Name As String, ByVal PropertyName As String, ByVal ParamID As ParameterID, ByVal ParamType As ParamValueType)
        Dim ParamInfo As cParameterInfo
        
        Set ParamInfo = New cParameterInfo
        Call ParamInfo.Initialise(Name, PropertyName, ParamID, ParamType)
        Call myParameters.Add(ParamInfo, Key:=Trim(CStr(ParamID)))
    End Sub

    Private Sub BuildDataArray()
        
        Call CreateParameterInfoAndAddtoArray("CaptureMethod", "CaptureMethod", ParameterID.epCaptureMethod, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("TransactionCurrencyCode", "TransactionCurrencyCode", ParameterID.epTransactionCurrencyCode, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("OriginalTransactionValue", "OriginalTransactionValue", ParameterID.epOriginalTransactionValue, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("OriginalCashbackValue", "OriginalCashbackValue", ParameterID.epOriginalCashbackValue, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("OriginalGratuityValue", "OriginalGratuityValue", ParameterID.epOriginalGratuityValue, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("OriginalTransactionCurrencyCode", "OriginalTransactionCurrencyCode", ParameterID.epOriginalTransactionCurrencyCode, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("BarclaysBonusDiscountValue", "BarclaysBonusDiscountValue", ParameterID.epBarclaysBonusDiscountValue, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("BarclaysBonusRedemptionValue", "BarclaysBonusRedemptionValue", ParameterID.epBarclaysBonusRedemptionValue, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("AccountonFileRegistration", "AccountonFileRegistration", ParameterID.epAccountonFileRegistration, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("TokenId", "TokenId", ParameterID.epTokenID, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("AVSPostCode", "AVSPostCode", ParameterID.epAVSPostCode, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("AVSHouseNumber", "AVSHouseNumber", ParameterID.epAVSHouseNumber, ParamValueType.epvtString)
        Call CreateParameterInfoAndAddtoArray("CSCResult", "CSCResult", ParameterID.epCSCResult, ParamValueType.epvtString)
   End Sub
    
    Private Function LocalParseReceivedMessage(ByVal Message As String) As Boolean
        Dim MessageLen As Integer
        ReDim ParamValues(1 To Me.NumberOfParameters) As String
        Dim Delimiter As String
        Dim Param As Integer

    On Error GoTo Catch

Try:
        Delimiter = ","
        ParamValues = SplitTo1BasedStringArray(Message, Delimiter)
        ' Even though specify V2, if an error will only get original version format,
        ' ie no parameters 18 - 31.
        If UBound(ParamValues) > myOutputTRecord.NumberOfParameters Then
            For Param = myOutputTRecord.NumberOfParameters To Me.NumberOfParameters
                ' only bother setting the rtn fields if there is a value
                If Len(ParamValues(Param)) > 0 Then
                    Call SetPropertyFromParameterValue(ParamValues(Param), Param)
                End If
            Next Param
        End If

        myUnderstood = myOutputTRecord.Understood
        LocalParseReceivedMessage = True

        Exit Function
Catch:
        Call Err.Raise(vbObjectError + 1, ModuleNameStub & "LocalParseReceivedMessage", "Failed to parse output Transaction Record V2.")
    End Function
    
    Private Sub SetPropertyFromParameterValue(ByVal ParamValue As String, ByVal ParamID As ParameterID)
        Dim TempInt As Integer
        Dim TempDbl As Double
        Dim TempDate As Date
        Dim DayPart As String
    '   Dim CharPos As Integer

        Select Case ParamID
            Case ParameterID.epCaptureMethod:
                If StrComp("Contactless", ParamValue, vbTextCompare) = 0 _
                Or StrComp("Swipe", ParamValue, vbTextCompare) = 0 _
                Or StrComp("ICC", ParamValue, vbTextCompare) = 0 _
                Or StrComp("Keyed", ParamValue, vbTextCompare) = 0 Then
                    myCaptureMethod = ParamValue
                End If
            Case ParameterID.epTransactionCurrencyCode:
                If ParamToInt(TempInt, ParamValue) Then
                    myTransactionCurrencyCode = TempInt
                End If
            Case ParameterID.epOriginalTransactionValue:
                If ParamToInt(TempInt, ParamValue) Then
                    myOriginalTransactionValue = TempInt
                End If
            Case ParameterID.epOriginalCashbackValue:
                If ParamToInt(TempInt, ParamValue) Then
                    myOriginalCashbackValue = TempInt
                End If
            Case ParameterID.epOriginalGratuityValue:
                If ParamToInt(TempInt, ParamValue) Then
                    myOriginalGratuityValue = TempInt
                End If
            Case ParameterID.epOriginalTransactionCurrencyCode:
                If ParamToInt(TempInt, ParamValue) Then
                    myOriginalTransactionCurrencyCode = TempInt
                End If
            Case ParameterID.epBarclaysBonusDiscountValue:
                If ParamToInt(TempInt, ParamValue) Then
                    myBarclaysBonusDiscountValue = TempInt
                End If
            Case ParameterID.epBarclaysBonusRedemptionValue:
                If ParamToInt(TempInt, ParamValue) Then
                    myBarclaysBonusRedemptionValue = TempInt
                End If
            Case ParameterID.epAccountonFileRegistration:
                If ParamToInt(TempInt, ParamValue) Then
                    If TempInt = AccountOnFileRegistrationResult.eaofrrNotSet _
                    Or TempInt = AccountOnFileRegistrationResult.eaofrrPerformed _
                    Or TempInt = AccountOnFileRegistrationResult.eaofrrSuccess _
                    Or TempInt = AccountOnFileRegistrationResult.eaofrrFailed Then
                        myAccountonFileRegistration = TempInt
                    End If
                End If
            Case ParameterID.epTokenID:
                myTokenID = ParamValue
            Case ParameterID.epAVSPostCode:
                If ParamToInt(TempInt, ParamValue) Then
                    If TempInt = AVSPostCodeCheckResult.eavspccrNotProvided _
                    Or TempInt = AVSPostCodeCheckResult.eavspccrNotChecked _
                    Or TempInt = AVSPostCodeCheckResult.eavspccrMatched _
                    Or TempInt = AVSPostCodeCheckResult.eavspccrNotMatched _
                    Or TempInt = AVSPostCodeCheckResult.eavspccrReserved Then
                        myAVSPostCode = TempInt
                    End If
                End If
            Case ParameterID.epAVSHouseNumber:
                If ParamToInt(TempInt, ParamValue) Then
                    If TempInt = AVSHouseNumberCheckResult.eavshncrNotProvided _
                    Or TempInt = AVSHouseNumberCheckResult.eavshncrNotChecked _
                    Or TempInt = AVSHouseNumberCheckResult.eavshncrMatched _
                    Or TempInt = AVSHouseNumberCheckResult.eavshncrNotMatched _
                    Or TempInt = AVSHouseNumberCheckResult.eavshncrReserved = 8 Then
                        myAVSHouseNumber = TempInt
                    End If
                End If
            Case ParameterID.epCSCResult:
                If ParamToInt(TempInt, ParamValue) Then
                    If TempInt = CSCCheckResult.ecscrNotProvided _
                    Or TempInt = CSCCheckResult.ecscrNotChecked _
                    Or TempInt = CSCCheckResult.ecscrMatched _
                    Or TempInt = CSCCheckResult.ecscrNotMatched _
                    Or TempInt = CSCCheckResult.ecscrReserved Then
                        myCSCResult = TempInt
                    End If
                End If
            Case Else
                myErrorMessage = "Unknown Output TRecord V2 parameter."
        End Select
    End Sub
    
    Private Function ModuleNameStub() As String
    
        ModuleNameStub = AppNameStub & ModuleName & "."
    End Function

