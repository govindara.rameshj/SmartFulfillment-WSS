VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Vx810AuthorisationStrategy"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements IAuthorisationStrategy

Private myReprintCount As Integer

Private myTranEngine As ITransactionEngine

Public Sub IAuthorisationStrategy_Initialize(tranEngine As ITransactionEngine)
    Set myTranEngine = tranEngine
    myReprintCount = 0
End Sub

Public Function IAuthorisationStrategy_ProcessReversedTransactionResult(authResult As IAuthorizationDetails) As TransactionCompleteState
    Dim Result As TransactionCompleteState
    
    ' Set this to force further pass(es) of loop to pick
    ' up more progress messages, printing receipts etc
    Result = TransactionCompleteState.NearlyCompleted
    
    ' Referral 785
    ' Check for an offline transaction that again would have avoided
    ' the normal route (does not do a rejected authorisation progress message)
    ' so will not have set TransactionCompleted, but there are no more messages
    ' to come, so end it now
    If Len(authResult.EFTSequenceNumber & "") > 0 Then
        If StrComp(Left(authResult.EFTSequenceNumber, 2), "OL", vbTextCompare) = 0 Then
            Result = TransactionCompleteState.Completed
        End If
    End If
    
    IAuthorisationStrategy_ProcessReversedTransactionResult = Result
    
End Function

Public Sub IAuthorisationStrategy_ProcessContinueRequiredProgressStatus()
    If myReprintCount = 0 Then
        Call myTranEngine.SendContinueTransactionRecord(etraContinueTxn)
    Else
        myReprintCount = myReprintCount - 1
    End If
End Sub

Public Function IAuthorisationStrategy_ExtraProcessProgressStatus(StatusID As ProgressStatusID) As Boolean
    Dim Result As Boolean
    Result = True
    
    Select Case StatusID
        Case Else
            Result = False
    End Select
    
    IAuthorisationStrategy_ExtraProcessProgressStatus = Result

End Function

' VX810 doesnt do extra processing
Public Function IAuthorisationStrategy_ProcessSignatureConfirmationRequiredProgressStatus() As Boolean
    IAuthorisationStrategy_ProcessSignatureConfirmationRequiredProgressStatus = False
    myReprintCount = 0
End Function

' For VX810 use new counter for each esiPrinting* progress message processing
Public Function IAuthorisationStrategy_GetReceiptPrintCounter() As Counter
    Dim toReturn As New Counter
    toReturn.Initialise
    Set IAuthorisationStrategy_GetReceiptPrintCounter = toReturn
End Function

' For VX810 tell Ocius to reprint immediately
' Returns true as PED prints new receipt immediately.
Public Function IAuthorisationStrategy_ProcessReceiptRePrintRequired() As Boolean
    ' Reprint the receipt
    Call myTranEngine.SendContinueTransactionRecord(etraReprintReceipt)
    IAuthorisationStrategy_ProcessReceiptRePrintRequired = True
    myReprintCount = myReprintCount + 1
End Function

' For VX810 always change state
Public Sub IAuthorisationStrategy_ShowStateForReceiptPrintFailure(ReceiptType As String, cancelledAfterMaxReprintAttempts As Boolean)
    Call myTranEngine.ShowStateForReceiptPrintFailure(ReceiptType)
End Sub

' For VX810 allow all statuses
Public Function IAuthorisationStrategy_SkipPrintingReceiptProcessing(StatusID As ProgressStatusID) As Boolean
    IAuthorisationStrategy_SkipPrintingReceiptProcessing = False
End Function



