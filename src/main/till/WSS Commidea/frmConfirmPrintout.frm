VERSION 5.00
Begin VB.Form frmConfirmReceiptPrint 
   Caption         =   "Printing Receipt"
   ClientHeight    =   1500
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4590
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   1500
   ScaleWidth      =   4590
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer tmrTimeout 
      Enabled         =   0   'False
      Interval        =   60000
      Left            =   1680
      Top             =   480
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&OK"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Left            =   735
      TabIndex        =   1
      Top             =   840
      Width           =   1170
   End
   Begin VB.CommandButton cmdReprint 
      Cancel          =   -1  'True
      Caption         =   "&Re-print"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Left            =   2730
      TabIndex        =   2
      Top             =   840
      Width           =   1170
   End
   Begin VB.Label lblPromptConfirmReceiptPrint 
      Alignment       =   2  'Center
      Caption         =   " Receipt printed OK?"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   0
      TabIndex        =   0
      Top             =   0
      UseMnemonic     =   0   'False
      Width           =   4485
   End
End
Attribute VB_Name = "frmConfirmReceiptPrint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Implements ICommideaForm

Private moCommideaCfg As CommideaConfiguration
Private mbOKed As Boolean

Private Const ESC_PRINT_BIG       As String = "|2C"
Private Const ESC_PRINT_LARGE     As String = "|3C"
Private Const ESC_PRINT_HUGE      As String = "|4C"
Private Const ESC_PRINT_BOLD      As String = "|bC"
Private Const ESC_PRINT_UNDERLINE As String = "|uC"
Private Const ESC_PRINT_NORMAL    As String = "|N"

Public Event Duress()

Private ReturnKeyPressed As Boolean

Private Sub cmdReprint_Click()

    If Me.Visible Then
        Me.Hide
    End If
    Call DebugMsg(ModuleName, "Reprint", endlTraceOut)
End Sub

Private Sub cmdOK_Click()

    mbOKed = True
    If Me.Visible Then
        Me.Hide
    End If
    Call DebugMsg(ModuleName, "OK", endlTraceOut)
End Sub

Private Sub Form_Load()

    Me.BackColor = moCommideaCfg.MsgBoxPromptColour
    Me.Visible = False
    Me.lblPromptConfirmReceiptPrint.BackColor = moCommideaCfg.MsgBoxPromptColour
    Call DebugMsg(ModuleName, "Form_Load", endlTraceOut)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 And ReturnKeyPressed = True Then
        Call DebugMsg("frmConfirmReceipt", "Return Key Pressed Already Clearing the Key Press", endlDebug, "Return Key pressed...")
        KeyCode = 0
    End If
    If KeyCode = 13 Then ReturnKeyPressed = True
    
    If moCommideaCfg.UnderDuressKeyCode <> 0 And KeyCode = moCommideaCfg.UnderDuressKeyCode Then
        KeyCode = 0
        RaiseEvent Duress
    End If
    
End Sub

Public Function PerformPrint(ByVal ShowMe As Boolean, ByVal ReceiptType As String, _
                             ByRef Receipt() As String, ByRef VoidReceipt As Boolean) As Boolean
    Dim PrintedOK As Boolean
    Dim ReadOK As Boolean

    ' Reset in case called again before unloading
    mbOKed = False
    tmrTimeout.Interval = moCommideaCfg.PrintConfirmTimeout * 1000
    Me.Caption = "Printing " & ReceiptType & " Receipt"
    Call MakeTopMost(Me.Caption)
    If ReceiptType = "Merchant" Then
        ' As Merchant receipts are switched off, will only get here if require signature validation
        ' so is safe to print out the signature slip, i.e. no need for full wickes receipt incorporation
        Call DebugMsg(ModuleName, "PerformPrint", endlDebug, "Printing Merchant Receipt")
        PrintedOK = PrintOciusCreditCardSlip(GetMerchantReceiptFilePath(moCommideaCfg), VoidReceipt)
        If ShowMe Then
            Call DebugMsg(ModuleName, "PerformPrint", endlDebug, "Showing form")
            tmrTimeout.Enabled = True
            Call Me.Show(vbModal)
        End If
        PerformPrint = (Not ShowMe Or mbOKed) And PrintedOK
    Else
        If moCommideaCfg.UseSeparateReceipt Then
            ' Customer receipts are not being incorporated into Wickes receipt, so just
            ', so just print here
            Call DebugMsg(ModuleName, "PerformPrint", endlDebug, "Printing Customer Receipt")
            PrintedOK = PrintOciusCreditCardSlip(GetCustomerReceiptFilePath(moCommideaCfg), VoidReceipt)
            If ShowMe Then
                tmrTimeout.Enabled = True
                Call Me.Show(vbModal)
            End If
            PerformPrint = (Not ShowMe Or mbOKed) And PrintedOK
        Else
            ' Customer receipts need to be incorporated into Wickes receipt
            ' collect the data here
            Call DebugMsg(ModuleName, "PerformPrint", endlDebug, "Reading Customer Receipt")
            ReadOK = ReadOciusCreditCardSlip(GetCustomerReceiptFilePath(moCommideaCfg), Receipt, VoidReceipt)
            PerformPrint = ReadOK
        End If
    End If
    Call DebugMsg(ModuleName, "PerformPrint", endlTraceOut)
End Function

Private Function ModuleName() As String

    ModuleName = "frmConfirmReceiptPrint"
End Function

Private Function PrintOciusCreditCardSlip(ByVal CCSlipFilename As String, ByRef VoidReceipt As Boolean) As Boolean
    Dim fsoCCSlipFile As New FileSystemObject
    Dim tsCCSlipFile As TextStream
    Dim CCSlipLine As String
    Dim AfterUnderline As Boolean
    Dim lngSaveSpacing  As Long
    Dim LineCount As Integer
    Dim LeadingSpace As String
    Dim BigBoldLineLen As Integer
    Dim NextChar As Integer
    Dim amount As Double
    Dim BigBoldEscSeq As String
    Dim LargeBoldEscSeq As String
    Dim TotalNotAmount As Boolean
    Dim Pos As Integer
    Dim TimeStart As Date
    Dim TimeEnd As Date
    Dim TimeTaken As Long
    
    Const ProcedureName = "PrintOciusCreditCardSlip"

'#If INDEBUG Then
'    PrintOciusCreditCardSlip = True
'    Exit Function
'#End If
    Call DebugMsg(ModuleName, ProcedureName, endlTraceIn)
    
    Dim oPrinterInfo As Object
    Set oPrinterInfo = moCommideaCfg.PrinterInfo
    
    If Not oPrinterInfo Is Nothing Then
        ' Wait a couple of seconds or until the merchant receipt appears (always deleted after printing) - can be a bit slow on some machines
        Call DebugMsg(ModuleName, ProcedureName, endlDebug, "Waiting for receipt file.")
        TimeStart = Now()
        Do
           TimeTaken = DateDiff("s", TimeStart, Now())
        Loop While TimeTaken < 3 And Not fsoCCSlipFile.FileExists(CCSlipFilename)
        ' Limit number of chars in a big bold line, so not go over to next line. Lines generally have extra spaces that can be removed
        BigBoldLineLen = 22
        BigBoldEscSeq = Chr$(27) & ESC_PRINT_BIG & Chr$(27) & ESC_PRINT_BOLD
        LargeBoldEscSeq = Chr$(27) & ESC_PRINT_LARGE & Chr$(27) & ESC_PRINT_BOLD
        If fsoCCSlipFile.FileExists(CCSlipFilename) Then
            Call DebugMsg(ModuleName, ProcedureName, endlDebug, "Receipt file '" & CCSlipFilename & "' found and being opened.  Waited " & CStr(TimeTaken) & " seconds.")
            Set tsCCSlipFile = fsoCCSlipFile.OpenTextFile(CCSlipFilename, IOMode:=ForReading)
            If Not tsCCSlipFile Is Nothing Then
                Call DebugMsg(ModuleName, ProcedureName, endlDebug, "Receipt file '" & CCSlipFilename & "' opened")
                lngSaveSpacing = oPrinterInfo.Printer.RecLineSpacing
                oPrinterInfo.Printer.RecLineSpacing = oPrinterInfo.Printer.RecLineHeight
                
                oPrinterInfo.PrintLogo
                Call DoRecPrint(oPrinterInfo, vbNewLine)
                
                Call DebugMsg(ModuleName, ProcedureName, endlDebug, "Reading receipt file '" & CCSlipFilename & "'")
                Do Until tsCCSlipFile.AtEndOfStream
                    LineCount = LineCount + 1
                    Call DebugMsg(ModuleName, ProcedureName, endlDebug, "Reading receipt file line no. " & CStr(LineCount))
                    CCSlipLine = tsCCSlipFile.ReadLine
                    If InStr(1, CCSlipLine, "***", vbTextCompare) > 0 _
                    Or InStr(1, CCSlipLine, "##", vbTextCompare) > 0 _
                    Or AfterUnderline _
                    Or StrComp(Left(Trim(CCSlipLine), 4), "SALE", vbTextCompare) = 0 _
                    Or StrComp(Left(Trim(CCSlipLine), 3), "PIN", vbTextCompare) = 0 _
                    Or StrComp(Left(Trim(CCSlipLine), 9), "SIGNATURE", vbTextCompare) = 0 _
                    Or StrComp(Left(Trim(CCSlipLine), 6), "AMOUNT", vbTextCompare) = 0 _
                    Or StrComp(Left(Trim(CCSlipLine), 5), "TOTAL", vbTextCompare) = 0 Then
                  ' Or StrComp(Left(Trim(CCSlipLine), 7), "ACCOUNT", vbTextCompare) = 0
                        Call DebugMsg(ModuleName, ProcedureName, endlDebug, "Printing big bold line - " & CCSlipLine)
                        ' Slightly different formating for some of the big bold lines
                        If InStr(1, CCSlipLine, "*** ") > 0 _
                        Or InStr(1, CCSlipLine, "##") _
                        Or StrComp(Left(Trim(CCSlipLine), 3), "PIN", vbTextCompare) = 0 _
                        Or StrComp(Left(Trim(CCSlipLine), 9), "SIGNATURE", vbTextCompare) = 0 Then
                            CCSlipLine = Trim(CCSlipLine)
                            If Len(CCSlipLine) < BigBoldLineLen Then
                                LeadingSpace = Space((BigBoldLineLen - Len(CCSlipLine)) / 2)
                                CCSlipLine = LeadingSpace & CCSlipLine & Space(BigBoldLineLen - Len(LeadingSpace) - Len(CCSlipLine))
                            Else
                                If InStr(1, CCSlipLine, "cardholder copy", vbTextCompare) > 0 Then
                                    ' Very specific too long...and no double spaces to get rid of
                                    ' so just give a value that fits
                                    CCSlipLine = " **CARDHOLDER COPY**"
                                Else
                                    ' try to remove internal spaces until get to right max length
                                    Do
                                        Pos = Pos + 1
                                        If Pos < Len(CCSlipLine) - 1 Then
                                            If Mid(CCSlipLine, Pos, 2) = "  " Then
                                                CCSlipLine = Left(CCSlipLine, Pos) & Mid(CCSlipLine, Pos + 2)
                                            End If
                                        End If
                                    Loop While Pos < Len(CCSlipLine) And Len(CCSlipLine) > BigBoldLineLen
                                End If
                            End If
                            If InStr(1, CCSlipLine, "##  VOID", vbTextCompare) > 0 Then
                                VoidReceipt = True
                            End If
                            CCSlipLine = BigBoldEscSeq & CCSlipLine & vbNewLine
                        ElseIf AfterUnderline Then
                            ' Don't do the 'Please keep this receipt' or 'thank you for...' or 'Account on File...' line in big bold
                            If InStr(1, CCSlipLine, "receipt", vbTextCompare) = 0 _
                            And InStr(1, CCSlipLine, "thank you", vbTextCompare) = 0 _
                            And InStr(1, CCSlipLine, "account on", vbTextCompare) = 0 Then
                                CCSlipLine = Trim(CCSlipLine)
                                LeadingSpace = Space((BigBoldLineLen - Len(CCSlipLine)) / 2)
                                CCSlipLine = LeadingSpace & CCSlipLine & Space(BigBoldLineLen - Len(LeadingSpace) - Len(CCSlipLine))
                                CCSlipLine = BigBoldEscSeq & CCSlipLine & vbNewLine
                            Else
                                CCSlipLine = CCSlipLine & vbNewLine
                            End If
                        ElseIf StrComp(Left(Trim(CCSlipLine), 4), "SALE", vbTextCompare) = 0 Then
                            CCSlipLine = Replace(CCSlipLine, "SALE", "", Compare:=vbTextCompare) & vbNewLine
                            CCSlipLine = BigBoldEscSeq & "SALE" & Chr$(27) & ESC_PRINT_NORMAL & CCSlipLine
                        ElseIf StrComp(Left(Trim(CCSlipLine), 6), "AMOUNT", vbTextCompare) = 0 _
                        Or StrComp(Left(Trim(CCSlipLine), 5), "TOTAL", vbTextCompare) = 0 Then
                            TotalNotAmount = CBool(InStr(1, CCSlipLine, "TOTAL", vbTextCompare) > 0)
                            ' Extract the amount and reformat with currency symbol and spacing
                            For NextChar = 1 To Len(CCSlipLine)
                                If IsNumeric(Mid(CCSlipLine, NextChar, 1)) Then
                                    Exit For
                                End If
                            Next NextChar
                            If NextChar <= Len(CCSlipLine) Then
                                amount = CDbl(Mid(CCSlipLine, NextChar))
                            Else
                                amount = 0#
                            End If
                            CCSlipLine = moCommideaCfg.CurrencyCharacter & Format(amount, "0.00 ;0.00-")
                            If TotalNotAmount Then
                                ' Just a bit bigger for the total
                                ' nope, too big
                                'CCSlipLine = LargeBoldEscSeq & "TOTAL" & Space(BigBoldLineLen - Len("TOTAL") - Len(CCSlipLine)) & CCSlipLine
                                CCSlipLine = BigBoldEscSeq & "TOTAL" & Space(BigBoldLineLen - Len("TOTAL") - Len(CCSlipLine)) & CCSlipLine & vbNewLine
                            Else
                                CCSlipLine = BigBoldEscSeq & "AMOUNT" & Space(BigBoldLineLen - Len("AMOUNT") - Len(CCSlipLine)) & CCSlipLine & vbNewLine
                            End If
                        Else
                            ' Everything else
                            CCSlipLine = CCSlipLine & vbNewLine
                        End If
                        Call DoRecPrint(oPrinterInfo, CCSlipLine)
                    ElseIf InStr(1, CCSlipLine, "---") > 0 Then
                        Call DebugMsg(ModuleName, ProcedureName, endlDebug, "Printing single divide line")
                        Call DoRecPrint(oPrinterInfo, String$(40, Chr$(196)) & vbNewLine)
                    Else
                        Call DebugMsg(ModuleName, ProcedureName, endlDebug, "Printing line - " & CCSlipLine)
                        Call DoRecPrint(oPrinterInfo, CCSlipLine & vbNewLine)
                    End If
                    If InStr(1, CCSlipLine, "---") > 0 Then
                        AfterUnderline = True
                    Else
                        AfterUnderline = False
                    End If
                Loop
            End If
            tsCCSlipFile.Close
            Set tsCCSlipFile = Nothing
            ' Delete the file now, in case new receipt print fails and new customer gets old receipt file
            
            Call DeleteReceiptFile(fsoCCSlipFile, CCSlipFilename)
            
            Set fsoCCSlipFile = Nothing
            Call DebugMsg(ModuleName, ProcedureName, endlDebug, "Printing footer and cut paper")
            Call DoRecPrint(oPrinterInfo, vbNewLine)
            Call oPrinterInfo.CutReceipt
            oPrinterInfo.Printer.RecLineSpacing = lngSaveSpacing
            PrintOciusCreditCardSlip = True
            Call DebugMsg(ModuleName, ProcedureName, endlDebug, "Succesfully printed receipt")
        Else
            Call DebugMsg(ModuleName, ProcedureName, endlDebug, "Receipt file not found.  Waited for " & CStr(TimeTaken) & " seconds.")
        End If
    Else
        Call DebugMsg(ModuleName, ProcedureName, endlDebug, "No printer control")
        PrintOciusCreditCardSlip = True
    End If
    
    Call DebugMsg(ModuleName, ProcedureName, endlTraceOut)
    Exit Function
                               
Err_Print_CCSlip:
    If Not tsCCSlipFile Is Nothing Then
        tsCCSlipFile.Close
        Set tsCCSlipFile = Nothing
        ' Delete the file, in case new receipt print fails and new customer gets old receipt file
        Call fsoCCSlipFile.DeleteFile(CCSlipFilename, True)
    End If
    Call MsgBoxEx("WARNING : An error has occurred when printing Credit Card Slip - system will attempt to continue to complete transaction", vbOKOnly, "Print Error")
    Call Err.Clear
    Call DebugMsg(ModuleName, ProcedureName, endlTraceOut)
End Function

Private Sub DoRecPrint(ByVal printInfo As Object, strOutput As String)
    Dim lngResultCode As Long
    Dim lngExtended   As Long
    Dim blnOK         As Boolean

'#If INDEBUG = -1 Then
'    Exit Sub
'#End If

    printInfo.DoRecPrint (strOutput)
    
    DoEvents
End Sub

Private Function ReadOciusCreditCardSlip(ByVal CCSlipFilename As String, _
                                         ByRef Receipt() As String, _
                                         ByRef VoidReceipt As Boolean) As Boolean
    Dim fsoCCSlipFile As New FileSystemObject
    Dim tsCCSlipFile As TextStream
    Dim CCSlipLine As String
    Dim LineCount As Integer
    Dim LeadingSpace As String
    Dim LineLen As Integer
    Dim NextChar As Integer
    Dim amount As Double
    Dim TotalOrAmount As String
    Dim Pos As Integer
    Dim BoxTopLeftCorner As String
    Dim BoxHorizontal As String
    Dim BoxTopRightCorner As String
    Dim BoxVertical As String
    Dim BoxBottomLeftCorner As String
    Dim BoxBottomRightCorner As String
    Dim BoxLeft As Integer
    Dim BoxedWordStart  As Integer
    Dim BoxWordEnd  As Integer
    Dim BoxedWord As String
    Dim ColonStartLine As Integer
    Dim ColonEndLine As Integer
    Dim AfterReceiptHeaderEnd As Boolean
    Dim BeforeReceiptFooterStart As Boolean

    ReDim Receipt(0) As String
    ' Limit number of chars in a big bold line, so not go over to next line. Lines generally have extra spaces that can be removed
    LineLen = 40
    BoxTopLeftCorner = Chr$(201)
    BoxHorizontal = Chr$(205)
    BoxTopRightCorner = Chr$(187)
    BoxVertical = Chr$(186)
    BoxBottomLeftCorner = Chr$(200)
    BoxBottomRightCorner = Chr$(188)
    
    Call DebugMsg(ModuleName, "PrintOciusCreditCardSlip", endlDebug, "Receipt file '" & CCSlipFilename & "' found and being opened")
    
    Call DebugMsg(ModuleName, "ReadOciusCreditCardSlip", endlDebug, "Checking for receipt file '" & CCSlipFilename & "'")
    If fsoCCSlipFile.FileExists(CCSlipFilename) Then
        Call DebugMsg(ModuleName, "ReadOciusCreditCardSlip", endlDebug, "Receipt file '" & CCSlipFilename & "' found and being opened")
        Set tsCCSlipFile = fsoCCSlipFile.OpenTextFile(CCSlipFilename, IOMode:=ForReading)
        If Not tsCCSlipFile Is Nothing Then
            Call DebugMsg(ModuleName, "ReadOciusCreditCardSlip", endlDebug, "Receipt file '" & CCSlipFilename & "' opened")
            Call DebugMsg(ModuleName, "ReadOciusCreditCardSlip", endlDebug, "Reading receipt file '" & CCSlipFilename & "'")
            ' Advance to the first valid receipt line - skip header and store name etc
            ' use header last line param, then advance to next non blank line
            Do
                LineCount = LineCount + 1
                Call DebugMsg(ModuleName, "ReadOciusCreditCardSlip", endlDebug, "Reading receipt file line no. " & CStr(LineCount))
                CCSlipLine = tsCCSlipFile.ReadLine
                ' Check a 'last header line' has been supplied
                If Len(moCommideaCfg.ReceiptHeaderLastLine) = 0 Then
                    ' No 'header last line' specified, then just ignore preceding blank lines and initial CARDHOLDER COPY line
                    If Len(Trim(CCSlipLine)) > 0 And InStr(1, CCSlipLine, "CARDHOLDER COPY", vbTextCompare) = 0 Then
                        AfterReceiptHeaderEnd = True
                    End If
                Else
                    ' Check for 'header last line' contents
                    If InStr(1, CCSlipLine, moCommideaCfg.ReceiptHeaderLastLine, vbTextCompare) > 0 Then
                        AfterReceiptHeaderEnd = True
                        CCSlipLine = tsCCSlipFile.ReadLine
                    End If
                End If
            Loop While (Not AfterReceiptHeaderEnd Or (AfterReceiptHeaderEnd And Len(Trim(CCSlipLine)) = 0)) And Not tsCCSlipFile.AtEndOfStream
            ' Initialise the boxed word stuff.  There should only be 0 or 1 of them
            BoxLeft = 0
            BoxedWordStart = 0
            BoxWordEnd = 0
            BoxedWord = ""
            Do Until tsCCSlipFile.AtEndOfStream
                ' Get line to the right length etc
                If Len(CCSlipLine) < LineLen Then
                    ' Centre if start with blank
                    If Left$(CCSlipLine, 1) = " " Then
                        LeadingSpace = Space((LineLen - Len(Trim$(CCSlipLine))) / 2)
                        CCSlipLine = LeadingSpace & Trim$(CCSlipLine) & Space(LineLen - Len(LeadingSpace) - Len(CCSlipLine))
                    End If
                Else
                    ' try to remove internal spaces until get to right max length
                    Pos = 1
                    Do
                        If Pos < Len(CCSlipLine) - 1 Then
                            If Mid(CCSlipLine, Pos, 2) = "  " Then
                                CCSlipLine = Left(CCSlipLine, Pos) & Mid(CCSlipLine, Pos + 2)
                            End If
                        End If
                        Pos = Pos + 1
                    Loop While Pos < Len(CCSlipLine) And Len(CCSlipLine) > LineLen
                End If
                ' Check for box, to replace '#'s with lines
                BoxLeft = InStr(1, CCSlipLine, "##")
                If BoxLeft > 0 Then
                    ' Already got boxed word? Then this must be the last line
                    If BoxedWord <> "" Then
                        ' Create bottom line of box, dependant on length of boxed word
                        CCSlipLine = BoxBottomLeftCorner & String$(Len(BoxedWord), BoxHorizontal) & BoxBottomRightCorner
                        CCSlipLine = Space(CInt((LineLen - Len(CCSlipLine)) / 2)) & CCSlipLine
                        CCSlipLine = CCSlipLine & Space(LineLen - Len(CCSlipLine))
                        ' Reset this as might be more than one box per receipt and is used in above condition on a per box basis
                        BoxedWord = ""
                        Call DebugMsg(ModuleName, "ReadOciusCreditCardSlip", endlDebug, "Created bottom box line - " & CCSlipLine)
                    ElseIf InStr(1, Trim(CCSlipLine), "## ") Then
                        BoxedWordStart = BoxLeft + Len("##")
                        BoxWordEnd = InStr(BoxedWordStart + 1, CCSlipLine, "#", vbTextCompare) - 1
                        BoxedWord = Mid(CCSlipLine, BoxedWordStart, BoxWordEnd - BoxedWordStart + 1)
                        ' Create top line of box, dependant on length of boxed word, and replace the previous receipt line with it
                        CCSlipLine = BoxTopLeftCorner & String$(Len(BoxedWord), BoxHorizontal) & BoxTopRightCorner
                        CCSlipLine = Space(CInt((LineLen - Len(CCSlipLine)) / 2)) & CCSlipLine
                        CCSlipLine = CCSlipLine & Space(LineLen - Len(CCSlipLine))
                        Call DebugMsg(ModuleName, "ReadOciusCreditCardSlip", endlDebug, "Replacing top box line - all hashes replaced with " & CCSlipLine)
                        ' Don't forget the new line as replacing this line not inserting a new one
                        CCSlipLine = CCSlipLine & vbNewLine
                        Receipt(UBound(Receipt)) = CCSlipLine
                        ' Create word line of box, dependant on length of boxed word (will be added to receipt later on)
                        CCSlipLine = BoxVertical & BoxedWord & BoxVertical
                        CCSlipLine = Space(CInt((LineLen - Len(CCSlipLine)) / 2)) & CCSlipLine
                        CCSlipLine = CCSlipLine & Space(LineLen - Len(CCSlipLine))
                        Call DebugMsg(ModuleName, "ReadOciusCreditCardSlip", endlDebug, "Created box word line - " & CCSlipLine)
                    Else
                        ' must be 1st line of box, so add as is for now and alter on next loop when have the boxed word (see above)
                        ' i.e. - do nothing here
                    End If
                    If InStr(1, CCSlipLine, "VOID", vbTextCompare) > 0 Then
                        VoidReceipt = True
                    End If
                End If
                If StrComp(Left(Trim(CCSlipLine), 6), "AMOUNT", vbTextCompare) = 0 _
                Or StrComp(Left(Trim(CCSlipLine), 5), "TOTAL", vbTextCompare) = 0 Then
                    TotalOrAmount = Trim(Left$(Trim(CCSlipLine), 6))
                    ' Extract the amount and reformat with currency symbol and spacing
                    For NextChar = 1 To Len(CCSlipLine)
                        If IsNumeric(Mid(CCSlipLine, NextChar, 1)) Then
                            Exit For
                        End If
                    Next NextChar
                    If NextChar <= Len(CCSlipLine) Then
                        amount = CDbl(Mid(CCSlipLine, NextChar))
                    Else
                        amount = 0#
                    End If
                    CCSlipLine = moCommideaCfg.CurrencyCharacter & Format(amount, "0.00 ;0.00-")
                    CCSlipLine = TotalOrAmount & Space(LineLen - Len(TotalOrAmount) - Len(CCSlipLine)) & CCSlipLine
                End If
                If InStr(1, CCSlipLine, "---") > 0 Then
                    CCSlipLine = String$(LineLen, Chr$(196))
                    Call DebugMsg(ModuleName, "ReadOciusCreditCardSlip", endlDebug, "Creating single divide line")
                End If
                Call DebugMsg(ModuleName, "ReadOciusCreditCardSlip", endlDebug, "Saving line - " & CCSlipLine)
                ReDim Preserve Receipt(UBound(Receipt) + 1) As String
                Receipt(UBound(Receipt)) = CCSlipLine & vbNewLine
                LineCount = LineCount + 1
                Call DebugMsg(ModuleName, "ReadOciusCreditCardSlip", endlDebug, "Reading receipt file line no. " & CStr(LineCount))
                CCSlipLine = tsCCSlipFile.ReadLine
            Loop
            If UBound(Receipt) > 0 Then
                ' Count backwards to before the first line of footer last underline, then look for 1st non blank line before that all that came after is footer
                For LineCount = UBound(Receipt) To 1 Step -1
                    If BeforeReceiptFooterStart Then
                        If Len(Trim(Receipt(LineCount))) > Len(vbNewLine) Then
                            Exit For
                        End If
                    Else
                        ' Check a 'first footer line' has been supplied
                        If moCommideaCfg.ReceiptFooterFirstLine <> "" Then
                            If InStr(1, Receipt(LineCount), moCommideaCfg.ReceiptFooterFirstLine, vbTextCompare) > 0 Then
                                BeforeReceiptFooterStart = True
                            End If
                        Else
                            ' No 'first footer line' specified, then just ignore any trailing blank lines
                            If Len(Trim(Receipt(LineCount))) > Len(vbNewLine) Then
                                ' Need this line, so advance counter to earlier blank line
                                LineCount = LineCount + 1
                                BeforeReceiptFooterStart = True
                            End If
                        End If
                    End If
                Next LineCount
                ' remove footer and last single divide line
                If LineCount > 0 Then
                    ReDim Preserve Receipt(LineCount) As String
                End If
            End If
        End If
        tsCCSlipFile.Close
        Set tsCCSlipFile = Nothing
        ' Delete the file, in case new receipt print fails and new customer gets old receipt file
        Call fsoCCSlipFile.DeleteFile(CCSlipFilename, True)
        Set fsoCCSlipFile = Nothing
        Call DebugMsg(ModuleName, "ReadOciusCreditCardSlip", endlDebug, "Succesfully read receipt")
        Call DebugMsg(ModuleName, "ReadOciusCreditCardSlip", endlDebug, "Justifying colons in read receipt")
        
        For LineCount = 1 To UBound(Receipt)
            If InStr(1, Receipt(LineCount), ":", vbTextCompare) > 0 And Not IsTimeColon(Receipt(LineCount), InStr(1, Receipt(LineCount), ":", vbTextCompare)) Then
                If ColonStartLine = 0 Then
                    ColonStartLine = LineCount
                End If
            Else
                If ColonStartLine > 0 Or IsTimeColon(Receipt(LineCount), InStr(1, Receipt(LineCount), ":", vbTextCompare)) Then
                    If ColonStartLine > 0 Then
                        ColonEndLine = LineCount - 1
                        Call JustifyColons(Receipt, ColonStartLine, ColonEndLine, LineLen)
                        ColonStartLine = 0
                        ColonEndLine = 0
                    End If
                End If
            End If
        Next LineCount
        If ColonStartLine > 0 Then
            ColonEndLine = LineCount - 1
            Call JustifyColons(Receipt, ColonStartLine, ColonEndLine, LineLen)
        End If
        ReadOciusCreditCardSlip = True
    End If
    
    Exit Function
                               
Err_Print_CCSlip:
    If Not tsCCSlipFile Is Nothing Then
        tsCCSlipFile.Close
        Set tsCCSlipFile = Nothing
    End If
    If Not fsoCCSlipFile Is Nothing Then
        ' Delete the file, in case new receipt print fails and new customer gets old receipt file
        Call fsoCCSlipFile.DeleteFile(CCSlipFilename, True)
    End If
    Set fsoCCSlipFile = Nothing
    Call MsgBox("WARNING : An error has occurred when reading Credit Card Slip - system will attempt to continue to complete transaction", vbOKOnly, "Read Receipt file Error")
    Call Err.Clear
End Function

Private Sub JustifyColons(ByRef Receipt() As String, ByVal ColonStartLine As Integer, ByVal ColonEndLine As Integer, ByVal LineLen As Integer)
    Dim ColonLineCount As Integer
    Dim CharsToColon As Integer
    Dim SpacesToAdd As Integer
    Dim NewLine As String
    
    For ColonLineCount = ColonStartLine To ColonEndLine
        If InStr(1, Receipt(ColonLineCount), ":", vbTextCompare) > CharsToColon Then
            CharsToColon = InStr(1, Receipt(ColonLineCount), ":", vbTextCompare)
        End If
    Next ColonLineCount
    For ColonLineCount = ColonStartLine To ColonEndLine
        If InStr(1, Receipt(ColonLineCount), ":", vbTextCompare) <= CharsToColon Then
            SpacesToAdd = CharsToColon - InStr(1, Receipt(ColonLineCount), ":", vbTextCompare)
            ' add spaces before colon to make the colons of consecutive lines justified
            NewLine = Left$(Receipt(ColonLineCount), InStr(1, Receipt(ColonLineCount), ":", vbTextCompare) - 1) _
                    & Space(SpacesToAdd) & ":" _
                    & Trim$(Mid$(Receipt(ColonLineCount), InStr(1, Receipt(ColonLineCount), ":", vbTextCompare) + 1))
            ' Make sure it still fits on the line before using it
            If Len(NewLine) <= LineLen Then
                Receipt(ColonLineCount) = NewLine
            End If
        End If
    Next ColonLineCount
End Sub

Private Function IsTimeColon(ByVal Line As String, ByVal ColonPos As Integer) As Boolean

    If Len(Line) >= 8 And ColonPos > 2 Then
        If Len(Line) - ColonPos >= 5 Then
            IsTimeColon = IsDate(Format(Now(), "dd/MM/YYYY") & " " & Mid$(Line, ColonPos - 2, 8))
            If Not IsTimeColon And ColonPos > 5 Then
                IsTimeColon = IsDate(Format(Now(), "dd/MM/YYYY") & " " & Mid$(Line, ColonPos - 5, 8))
            End If
        ElseIf Len(Line) - ColonPos >= 2 Then
            IsTimeColon = IsDate(Format(Now(), "dd/MM/YYYY") & " " & Mid$(Line, ColonPos - 5, 8))
        End If
    End If
End Function

Private Sub tmrTimeout_Timer()
    tmrTimeout.Enabled = False
    Call cmdReprint_Click
    
End Sub


Public Sub ICommideaForm_InitializeBeforeLoad(ByVal commideaCfg As CommideaConfiguration)
    Set moCommideaCfg = commideaCfg
End Sub

Public Sub ICommideaForm_ResetAfterUnload()
    Set moCommideaCfg = Nothing
End Sub

Private Sub DeleteReceiptFile(ByVal fso As FileSystemObject, ByVal filePath As String)
    
    ' Sometimes TextStream.Close() returns before the file is closed. So in case of error wait 1 sec and try again
    On Error GoTo WaitAndTryAgain
    
    Call fso.DeleteFile(filePath, True)
    Exit Sub
    
WaitAndTryAgain:
    Wait (1)
    Call fso.DeleteFile(filePath, True)

End Sub
