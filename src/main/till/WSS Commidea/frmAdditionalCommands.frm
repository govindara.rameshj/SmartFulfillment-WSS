VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "mswinsck.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmAdditionalCommands 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ocius Sentinel"
   ClientHeight    =   7155
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7425
   ClipControls    =   0   'False
   Icon            =   "frmAdditionalCommands.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7155
   ScaleWidth      =   7425
   StartUpPosition =   1  'CenterOwner
   Visible         =   0   'False
   Begin VB.Frame fraEntry 
      Height          =   6255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7095
      Begin VB.PictureBox fraInsertCard 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   2535
         Left            =   10
         ScaleHeight     =   2535
         ScaleWidth      =   7050
         TabIndex        =   3
         Top             =   1200
         Width           =   7057
         Begin VB.Label lblInsert 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1695
            Left            =   240
            TabIndex        =   4
            Top             =   120
            Width           =   6615
         End
      End
      Begin VB.Label lblStatus 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H0000FFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   0
         TabIndex        =   2
         Top             =   5760
         Width           =   7095
      End
      Begin VB.Label lblAction 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Enter Credit Card"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   615
         Left            =   0
         TabIndex        =   1
         Top             =   60
         Width           =   7095
      End
   End
   Begin VB.Timer tmrLaunch 
      Left            =   960
      Top             =   0
   End
   Begin MSWinsockLib.Winsock wskProgress 
      Left            =   0
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      RemotePort      =   25001
   End
   Begin MSWinsockLib.Winsock wskIntegration 
      Left            =   480
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      RemotePort      =   25000
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   5
      Top             =   6780
      Width           =   7425
      _ExtentX        =   13097
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmAdditionalCommands.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   5715
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   635
            MinWidth        =   635
            Picture         =   "frmAdditionalCommands.frx":1030
            Key             =   "NumPad"
            Object.ToolTipText     =   "Show/Hide Num Pad"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "17:54"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmAdditionalCommands"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Implements ICommideaForm

Private Const ModuleName As String = "frmAdditionalCommands"
Private myReceivedMessage As String
Private myProgressMessage() As String
Private myMessageReceived As Boolean
Private myProgressError As String
Private myIntegrationError As String
Private mblCommsDown As Boolean
Private moCommideaCfg As CommideaConfiguration

Public Event Duress()

' Control events

Private Sub Form_Load()

    Me.BackColor = moCommideaCfg.BackgroundColour
    fraEntry.BackColor = Me.BackColor
    Call DebugMsg(ModuleName, "Form_Load", endlTraceOut)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If moCommideaCfg.UnderDuressKeyCode <> 0 And KeyCode = moCommideaCfg.UnderDuressKeyCode Then
        KeyCode = 0
        RaiseEvent Duress
    End If
End Sub

'Integration Socket events
Private Sub wskIntegration_ConnectionRequest(ByVal requestID As Long)

    ' Check if the control's State is closed. If not,
    ' close the connection before accepting the new
    ' connection.
    With wskIntegration
        If .State <> sckClosed Then
            .Close
        End If
        ' Accept the request with the requestID
        ' parameter.
        .Accept requestID
    End With
End Sub

Private Sub wskIntegration_DataArrival(ByVal bytesTotal As Long)
    Dim Received As String
    Dim Extra As String

    If bytesTotal = 0 Then
        wskIntegration.Close
    Else
        Do
            Call wskIntegration.GetData(Extra, vbString)
            Received = Received & Extra
        Loop While Right(Received, 2) <> vbCrLf And Asc(Left(Received, 1)) <> 6
        Debug.Print "Integration Message Received: " & Received
        If Received <> "" Then
            ' Make sure its the right format
            ' - Ocius sends Asc(6) to acknowledge receipt of message,
            ' but this often come out as part of the following data
            ' sent to winsock, sometimes it arrives on its own.  If
            ' remove it on own the message is now blank so will loop
            ' for more data; if together this will just strip it off
            If Asc(Left(Received, 1)) = 6 Then
                ' Remove the message delimiters
                Received = Mid(Received, 2)
            End If
            If Right(Received, 2) = vbCrLf Then
                ' Remove the message delimiters
                Received = Left(Received, Len(Received) - 2)
            End If
        End If
        ReceivedMessage = Received
    End If
End Sub

Private Sub wskIntegration_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)

    Select Case Number
        Case 1
            myIntegrationError = Trim(CStr(Number)) & ";" & Description & ";;Connection Error"
        Case 10060, 10061
            myIntegrationError = Trim(CStr(Number)) & ";" & Description & ";Please ensure Ocius is running and logged in;Ocius Sentinel Connection Error"
        Case Else
            myIntegrationError = Trim(CStr(Number)) & ";" & Description & ";Please check the connection cables, Network availability etc;General"
    End Select
    wskIntegration.Close
End Sub
' End of Integration Socket events

' Progress socket events
Private Sub wskProgress_ConnectionRequest(ByVal requestID As Long)

    ' Check if the control's State is closed. If not,
    ' close the connection before accepting the new
    ' connection.
    With wskProgress
        If .State <> sckClosed Then
            .Close
        End If
        ' Accept the request with the requestID
        ' parameter.
        .Accept requestID
    End With
End Sub

Private Sub wskProgress_DataArrival(ByVal bytesTotal As Long)
    Dim Received As String
    Dim Extra As String
    Dim Messages() As String
    Dim NextMess As Integer

    If bytesTotal = 0 Then
        wskProgress.Close
    Else
        Do While Right(Received, 2) <> vbCrLf
            Call wskProgress.GetData(Extra, vbString)
            Received = Received & Extra
        Loop
        If Received <> "" Then
            ' Make sure its the right format
            ' - Ocius sends Asc(6) to acknowledge receipt of message,
            ' but this often come out as part of the following data
            ' sent to winsock, sometimes it arrives on its own.  If
            ' remove it on own the message is now blank so will loop
            ' for more data; if together this will just strip it off
            If Asc(Left(Received, 1)) = 6 Then
                ' Remove the message delimiters
                Received = Mid(Received, 2)
            End If
            If Right(Received, 2) = vbCrLf Then
                ' Remove the message delimiters
                Received = Left(Received, Len(Received) - 2)
            End If
        End If
        ' Might get many messages at once, so load them up
        Messages = Split(Received, vbCrLf)
        For NextMess = 0 To UBound(Messages)
            ReDim Preserve myProgressMessage(UBound(myProgressMessage) + 1) As String
            myProgressMessage(UBound(myProgressMessage)) = Messages(NextMess)
            Debug.Print "Progress Message Received: " & Messages(NextMess)
        Next NextMess
    End If
End Sub

Private Sub wskProgress_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)

    Select Case Number
        Case 1
            myProgressError = Trim(CStr(Number)) & ";" & Description & ";There was a problem connecting to the Pin Pad;Connection Error"
        Case 10060, 10061
            myProgressError = Trim(CStr(Number)) & ";" & Description & ";There was a problem connecting to the Pin Pad." & vbNewLine & "Please ensure Ocius is running and logged in;Ocius Sentinel Connection Error"
        Case Else
            myProgressError = Trim(CStr(Number)) & ";" & Description & ";Please check the connection cables, Network availability etc;General"
    End Select
    wskProgress.Close
End Sub
' End of Progress Socket events


' End of Control events

' Public methods

' CR0034
' Add function to request info from the PED - PTID
Public Function GetPTID(ByRef PTID As String) As Boolean
    
    GetPTID = GetRequestForInformation(PTID, eairiPTID)
End Function
' End of CR0034

Public Function GetSoftwareVersion(ByRef VersionNumber As String) As Boolean
    GetSoftwareVersion = GetRequestForInformation(VersionNumber, eairiSoftwareVersion)
End Function

Public Function ProcessOfflineTransactions() As Boolean
    Dim oProgressMessage As cOutputProgressMessage
    Dim oReceivedMessage As cOutputTRecordV4
    Dim oAdditionalCommand As New cAdditionalCommand
    Dim TransactionCompleted As Boolean
    Dim TransactionNearlyCompleted As Boolean
    Dim ErrorParts() As String
    Dim Start As Date
    Dim Finish As Date
    Dim TimeTaken As Integer
    
On Error GoTo ProcessOfflineTxnsError

    If Not oAdditionalCommand Is Nothing Then
        Call oAdditionalCommand.Initialise(eaicSubmitOfflineTxns, NoMessage:=True)
        ReDim myProgressMessage(0) As String
        MessageReceived = False
        ReceivedMessage = ""
        ' Make sure connected before starting
        Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Connecting sockets = ")
        Start = Time
        Do
            Finish = Time
            Call FireUpSockets(moCommideaCfg.IntegrationPort, moCommideaCfg.ProgressPort)
            TimeTaken = ((Minute(Finish) * 60) + Second(Finish)) - ((Minute(Start) * 60) + Second(Start))
        Loop While TimeTaken < moCommideaCfg.ConnectionTimeout And Not SocketsConnected
        Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Time taken to connect to sockets = " & CStr(TimeTaken))
        If SocketsConnected Then
            Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Sockets connected")
            Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Sending Additional Command = '" & oAdditionalCommand.ToIntegrationMessage & "'")
            If SendTransaction(oAdditionalCommand.ToIntegrationMessage) Then
                Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Additional Command '" & oAdditionalCommand.ToIntegrationMessage & "' sent")
                Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Calling initialise Status Bar")
                Call InitialiseStatusBar
                Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Showing form")
                Call Me.Show
                Do
                    lblAction.Caption = "Please Wait"
                    lblStatus.Caption = "Submitting offline transactions"
                    lblInsert.Caption = ""
                    Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Waiting for PED response")
                    Call WaitForData
                    Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Got PED response or error")
                    If ReceivedMessage <> "" Then
                        Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Reading Integration message")
                        Set oReceivedMessage = New cOutputTRecordV4
                        If Not oReceivedMessage Is Nothing Then
                            With oReceivedMessage
                                Call .Initialise
                                If .ParseReceivedMessage(ReceivedMessage) Then
                                    ReceivedMessage = ""
                                    If .Successful Then
                                        Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Integration message received - Success (Result ID " & .TResult & ")")
                                        Select Case .TResult
                                            Case TransactionResultNonErrorScreenless.etrnesCompleted
                                                Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Integration message - Process Offline Transactions Completed")
                                                lblAction.Caption = "Finished"
                                                lblStatus.Caption = .CustomerVerification
                                                lblInsert.Caption = ""
                                                ProcessOfflineTransactions = True
                                                TransactionCompleted = True
                                            Case Else
                                        End Select
                                    Else
                                        Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Integration message received - Error: " & .ErrorMessage & " (Result ID " & .TResult & ")")
                                        lblAction.Caption = "Failed submitting offline transactions"
                                        lblStatus.Caption = .CustomerVerification
                                        lblInsert.Caption = ""
                                        ProcessOfflineTransactions = False
                                        TransactionCompleted = True
                                    End If
                                Else
                                    Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Failed reading Integration message ")
                                    Call MsgBoxEx("Failed translating the message from PED.", vbExclamation, "Error has occurred in processing the transaction.", , , , , moCommideaCfg.MsgBoxWarningColour)
                                End If
                            End With
                        End If
                    End If
                    Do While Not NextProgressMessage Is Nothing
                        Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Reading Progress message")
                        Set oProgressMessage = ReadNextProgressMessage
                        If Not oProgressMessage Is Nothing Then
                            With oProgressMessage
                                If .Result = 100 Then
                                    Select Case .StatusID
                                        Case esiContinueRequired
                                            Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Progress message received - 'Continue Required'")
                                            Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Sending 'Continue Required' message")
                                            Call SendContinueTransactionRecord(etraContinueTxn)
                                            Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "'Continue Required' message sent")
                                        Case Else
                                    End Select
                                Else
                                    Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Progress message received - Error: " & .Status & " (Result ID " & .Result & ")")
                                    ' error manage problem via progress message
                                End If
                            End With
                        Else
                            ' error initialising progress message
                            Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Failed reading Progress message ")
                        End If
                    Loop
                    If ConnectionError Then
                        ErrorParts = Split(WinSockError, ";")
                        Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Connection error. " & ErrorParts(0) & ". " & ErrorParts(1) & ". " & ErrorParts(2) & ". " & ErrorParts(3))
                        Call MsgBoxEx("WARNING: (" & ErrorParts(0) & " " & ErrorParts(1) & ") " & vbNewLine & ErrorParts(2), _
                                      vbCritical, _
                                      ErrorParts(3), , , , , _
                                      moCommideaCfg.MsgBoxWarningColour)
                        myProgressError = ""
                        myIntegrationError = ""
                        ReDim ErrorParts(0) As String
                        TransactionCompleted = True
                    End If
                Loop While Not TransactionCompleted
                Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Hiding form")
                If Me.Visible Then
                    Me.Hide
                End If
            Else
                Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Failed sending 'Process Offline Txn' command")
                Call MsgBoxEx("WARNING: Failed sending 'Process Offline Transaction' command to PED.", _
                            vbCritical, _
                            "Cannot process any offline transaction.", , , , , moCommideaCfg.MsgBoxWarningColour)
            End If
            DoEvents
            CloseSockets
        End If
    Else
        Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Failed Connecting sockets")
        Call MsgBoxEx("WARNING: Failed setting up communication with the PED.", _
                    vbCritical, _
                    "Cannot continue processing offline transactions.", , , , , moCommideaCfg.MsgBoxWarningColour)
    End If

    Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Leaving function")
    Exit Function

ProcessOfflineTxnsError:
    Call DebugMsg(ModuleName, "ProcessOfflineTransactions", endlDebug, "Error has occurred whilst processing offline transactions. " & Err.Number)
    Call MsgBoxEx("Error has occurred whilst processing offline transactions." & vbNewLine & Err.Number & ":" & Err.Description, vbExclamation, "Error Detected", , , , , moCommideaCfg.MsgBoxWarningColour)
    Call Err.Clear
End Function


' End of public methods


Private Property Get MessageReceived() As Boolean

    MessageReceived = myMessageReceived
End Property

Private Property Let MessageReceived(ByVal NewMessageReceived As Boolean)

    myMessageReceived = NewMessageReceived
End Property

Private Property Get ProgressMessage() As String()

    ProgressMessage = myProgressMessage
End Property

Private Property Get ReceivedMessage() As String

    ReceivedMessage = myReceivedMessage
End Property

Private Property Let ReceivedMessage(ByVal NewReceivedMessage As String)

    If NewReceivedMessage = "" Then
        myReceivedMessage = NewReceivedMessage
    Else
        ' Sometimes just get a single Asc() = 6 character before message,
        ' this can be ignored (treated as though blank)
        If Len(NewReceivedMessage) = 1 And Asc(NewReceivedMessage) = 6 Then
            myReceivedMessage = ""
        Else
            myReceivedMessage = NewReceivedMessage
        End If
    End If
End Property

Private Function GetRequestForInformation(ByRef RequestedInformation As String, _
                                        ByVal RequestedInformationType As AdditionalIntegrationRequestInfo, _
                                        Optional ByVal ReadyStateRequired As Boolean = True) As Boolean
    Dim oProgressMessage As cOutputProgressMessage
    Dim oReceivedMessage As cOutputTRecordV4
    Dim oAdditionalCommand As New cAdditionalCommand
    Dim TransactionCompleted As Boolean
    Dim TransactionNearlyCompleted As Boolean
    Dim ErrorParts() As String
    Dim Start As Date
    Dim Finish As Date
    Dim TimeTaken As Integer
    
On Error GoTo RequestedInformationTypeError

    If Not oAdditionalCommand Is Nothing Then
        Call oAdditionalCommand.Initialise(eaicRequestInfo, NoMessage:=True, RequestType:=RequestedInformationType)
        ReDim myProgressMessage(0) As String
        MessageReceived = False
        ReceivedMessage = ""
        ' Make sure connected before starting
        Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Connecting sockets = ")
        Start = Time
        Do
            Finish = Time
            Call FireUpSockets(moCommideaCfg.IntegrationPort, moCommideaCfg.ProgressPort, ReadyStateRequired)
            TimeTaken = ((Minute(Finish) * 60) + Second(Finish)) - ((Minute(Start) * 60) + Second(Start))
        Loop While TimeTaken < moCommideaCfg.ConnectionTimeout And Not SocketsConnected
        Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Time taken to connect to sockets = " & CStr(TimeTaken))
        If SocketsConnected Then
            Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Sockets connected")
            Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Sending Additional Command = '" & oAdditionalCommand.ToIntegrationMessage & "'")
            If SendTransaction(oAdditionalCommand.ToIntegrationMessage) Then
                Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Additional Command '" & oAdditionalCommand.ToIntegrationMessage & "' sent")
                Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Calling initialise Status Bar")
                Call InitialiseStatusBar
                Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Showing form")
                Call Me.Show
                Do
                    lblAction.Caption = "Please Wait"
                    lblStatus.Caption = GetCaptionForRequestedInformationType(RequestedInformationType)
                    lblInsert.Caption = ""
                    Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Waiting for PED response")
                    Call WaitForData
                    Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Got PED response or error")
                    If ReceivedMessage <> "" Then
                        Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Reading Integration message")
                        Set oReceivedMessage = New cOutputTRecordV4
                        If Not oReceivedMessage Is Nothing Then
                            With oReceivedMessage
                                Call .Initialise
                                If .ParseReceivedMessage(ReceivedMessage) Then
                                    ReceivedMessage = ""
                                    If .Successful Then
                                        Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Integration message received - Success (Result ID " & .TResult & ")")
                                        Select Case .TResult
                                            Case TransactionResultNonErrorScreenless.etrnesCompleted
                                                Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Integration message - REQINFO Completed")
                                                lblAction.Caption = "Finished"
                                                lblStatus.Caption = .CustomerVerification
                                                lblInsert.Caption = ""
                                                RequestedInformation = .CustomerVerification
                                                GetRequestForInformation = True
                                                TransactionCompleted = True
                                            Case Else
                                        End Select
                                    Else
                                        Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Integration message received - Error: " & .ErrorMessage & " (Result ID " & .TResult & ")")
                                        lblAction.Caption = "Failed " & GetCaptionForRequestedInformationType(RequestedInformationType)
                                        lblStatus.Caption = .CustomerVerification
                                        lblInsert.Caption = ""
                                        RequestedInformationType = False
                                        TransactionCompleted = True
                                    End If
                                Else
                                    Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Failed reading Integration message ")
                                    Call MsgBoxEx("Failed translating the message from PED.", vbExclamation, "Error has occurred in getting Request Information.", , , , , moCommideaCfg.MsgBoxWarningColour)
                                End If
                            End With
                        End If
                    End If
                    Do While Not NextProgressMessage Is Nothing
                        Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Reading Progress message")
                        Set oProgressMessage = ReadNextProgressMessage
                        If Not oProgressMessage Is Nothing Then
                            With oProgressMessage
                                If .Result = 100 Then
                                    Select Case .StatusID
                                        Case esiContinueRequired
                                            Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Progress message received - 'Continue Required'")
                                            Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Sending 'Continue Required' message")
                                            Call SendContinueTransactionRecord(etraContinueTxn)
                                            Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "'Continue Required' message sent")
                                        Case Else
                                    End Select
                                Else
                                    Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Progress message received - Error: " & .Status & " (Result ID " & .Result & ")")
                                End If
                            End With
                        Else
                            Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Failed reading Progress message")
                        End If
                    Loop
                    If ConnectionError Then
                        ErrorParts = Split(WinSockError, ";")
                        Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Connection error. " & ErrorParts(0) & ". " & ErrorParts(1) & ". " & ErrorParts(2) & ". " & ErrorParts(3))
                        Call MsgBoxEx("WARNING: (" & ErrorParts(0) & " " & ErrorParts(1) & ") " & vbNewLine & ErrorParts(2), _
                                      vbCritical, _
                                      ErrorParts(3), , , , , _
                                      moCommideaCfg.MsgBoxWarningColour)
                        myProgressError = ""
                        myIntegrationError = ""
                        ReDim ErrorParts(0) As String
                        TransactionCompleted = True
                    End If
                Loop While Not TransactionCompleted
                Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Hiding form")
                If Me.Visible Then
                    Me.Hide
                End If
            Else
                Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Failed sending 'REQINFO' command")
                Call MsgBoxEx("WARNING: Failed sending 'Request Information' command to PED.", _
                            vbCritical, _
                            "Cannot get requested information.", , , , , moCommideaCfg.MsgBoxWarningColour)
            End If
            DoEvents
            CloseSockets
        End If
    Else
        Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Failed Connecting sockets")
        Call MsgBoxEx("WARNING: Failed setting up communication with the PED.", _
                    vbCritical, _
                    "Cannot continue getting requested information.", , , , , moCommideaCfg.MsgBoxWarningColour)
    End If

    Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Leaving function")
    Exit Function

RequestedInformationTypeError:
    Call DebugMsg(ModuleName, "RequestedInformationType", endlDebug, "Error has occurred whilst getting requested information. " & Err.Number)
    Call MsgBoxEx("Error has occurred whilst getting requested information." & vbNewLine & Err.Number & ":" & Err.Description, vbExclamation, "Error Detected", , , , , moCommideaCfg.MsgBoxWarningColour)
    Call Err.Clear
End Function

Private Function GetCaptionForRequestedInformationType(RequestedInformationType As AdditionalIntegrationRequestInfo) As String

    Select Case RequestedInformationType
        Case eairiPTID
            GetCaptionForRequestedInformationType = "Getting PED ID (PTID)"
        Case eairiSoftwareVersion
            GetCaptionForRequestedInformationType = "Getting Ocius Sentinel Version"
        Case eairiLoginStatus
            GetCaptionForRequestedInformationType = "Getting PED Login Status"
        Case eairiTransServerConnTest
            GetCaptionForRequestedInformationType = "Performing PED Connection Test"
        Case eairiPEDStatus
            GetCaptionForRequestedInformationType = "Getting PED Status"
        Case Else
            GetCaptionForRequestedInformationType = "Unknown or not implemented Request Information Type"
    End Select
End Function

Private Function CancelPreviousTransaction() As Boolean
    Dim ProgMess As cOutputProgressMessage
    
    Call SendContinueTransactionRecord(etraCancelTransaction)
    Call WaitForData
    Set ProgMess = ReadNextProgressMessage
    If Not ProgMess Is Nothing Then
        If ProgMess.StatusID = esiTransactionCancelled Then
            CancelPreviousTransaction = True
        End If
    End If
End Function

Private Sub CloseSockets()

    Call DebugMsg(ModuleName, "CloseSockets", endlDebug, "Closing Progress Winsock connection")
    wskProgress.Close
    Call DebugMsg(ModuleName, "CloseSockets", endlDebug, "Closing Integration Winsock connection")
    wskIntegration.Close
End Sub

Private Sub CommsGoneDown()

    Call MsgBoxEx("Comms failure, could not connect to authorisation server.", , "Validate Failed", , , , , moCommideaCfg.MsgBoxWarningColour)
    sbStatus.Panels(PANEL_INFO).Text = sbStatus.Panels(PANEL_INFO).Text & " PED Offline"
    mblCommsDown = True
End Sub

Private Function ConnectionError() As Boolean

    If WinSockError <> "" Then
        ConnectionError = True
    End If
End Function

Private Function FireUpSockets(ByVal IntegrationPort As String, ByVal ProgressPort As String, Optional ByVal RequireReadyState As Boolean = True) As Boolean

    Call FireUpProgressSocket(ProgressPort)
    Call FireUpIntegrationSocket(IntegrationPort)
    FireUpSockets = Me.wskIntegration.State = sckConnected And Me.wskProgress.State = sckConnected
End Function

Private Sub FireUpProgressSocket(ByVal ProgressPort As String)
    
    With Me.wskProgress
        Call .Close
        .RemotePort = ProgressPort
        .RemoteHost = "127.0.0.1"
        Call DebugMsg(ModuleName, "FireUpProgressSocket", endlDebug, "Progress Winsock state is currently " & .State)
        Call DebugMsg(ModuleName, "FireUpProgressSocket", endlDebug, "Progress Winsock connecting to " & "127.0.0.1")    ' .LocalIP)
        Call .Connect
        While .State = sckConnecting
            DoEvents
        Wend
        Call DebugMsg(ModuleName, "FireUpProgressSocket", endlDebug, "Progress Winsock state is currently " & .State)
    End With
End Sub

Private Sub FireUpIntegrationSocket(ByVal IntegrationPort As String)
    
    With Me.wskIntegration
        Call .Close
        .RemotePort = IntegrationPort
        .RemoteHost = "127.0.0.1"
        If GetPEDReadyForNextTransaction Then
            Call DebugMsg(ModuleName, "FireUpIntegrationSocket", endlDebug, "Closing any previous Integration Winsock connection")
            Call DebugMsg(ModuleName, "FireUpIntegrationSocket", endlDebug, "Integration Winsock state is currently " & .State)
            Call DebugMsg(ModuleName, "FireUpIntegrationSocket", endlDebug, "Integration Winsock connecting to " & "127.0.0.1")    ' .LocalIP)
            Call .Connect
            While .State = sckConnecting
                DoEvents
            Wend
            Call DebugMsg(ModuleName, "FireUpIntegrationSocket", endlDebug, "Integration Winsock state is currently " & .State)
        Else
            Call DebugMsg(ModuleName, "FireUpIntegrationSocket", endlDebug, "PED not Ready.  Integration Winsock state is currently " & .State)
        End If
    End With
End Sub

Private Function SocketsConnected()

    SocketsConnected = Me.wskIntegration.State = sckConnected And Me.wskProgress.State = sckConnected
End Function

Private Function GetPEDReadyForNextTransaction() As Boolean

    Call DebugMsg(ModuleName, "GetPEDReadyForNextTransaction", endlTraceIn)
    Call DebugMsg(ModuleName, "GetPEDReadyForNextTransaction", endlDebug, "Checking next message is 'Ready'")
    
    Do While UBound(myProgressMessage) = 0
        DoEvents
    Loop
    
    If NextProgressMessageIs(esiReady) Then
        Call DebugMsg(ModuleName, "GetPEDReadyForNextTransaction", endlDebug, "Next message is 'Ready'")
        ' Just read next message to remove it from list
        ReadNextProgressMessage
        GetPEDReadyForNextTransaction = True
    Else
        Call DebugMsg(ModuleName, "GetPEDReadyForNextTransaction", endlDebug, "Next message is NOT 'Ready'")
        If NextProgressMessageIs(esiContinueRequired) Then
            ' Just read next message to remove it from list
            ReadNextProgressMessage
            GetPEDReadyForNextTransaction = True
        End If
    End If
End Function

Private Function NextProgressMessage() As cOutputProgressMessage

    If UBound(ProgressMessage()) > 0 Then
        Set NextProgressMessage = New cOutputProgressMessage
        If Not NextProgressMessage Is Nothing Then
            With NextProgressMessage
                .Initialise
                ' Always the 1st message as move all messages up 1 after this one is processed
                If Not .ParseReceivedMessage(ProgressMessage()(1)) Then
                    NextProgressMessage = Nothing
                End If
            End With
        End If
    End If
End Function

Private Function ReadNextProgressMessage() As cOutputProgressMessage
    Dim Delete As Integer

    If UBound(ProgressMessage()) > 0 Then
        ' read the message and then remove read message from list
        ' read...
        Set ReadNextProgressMessage = NextProgressMessage
        ' remove...
        ' Move message up one (overwriting the 1st that has now been processed)
        For Delete = 1 To UBound(myProgressMessage) - 1
            myProgressMessage(Delete) = myProgressMessage(Delete + 1)
        Next Delete
        ' Remove duplicate last message from end
        ReDim Preserve myProgressMessage(UBound(myProgressMessage) - 1) As String
    Else
        Set ReadNextProgressMessage = Nothing
    End If
End Function

Private Function NextProgressMessageIs(ByVal IsID As ProgressStatusID) As Boolean
    Dim NextMess As cOutputProgressMessage

    Set NextMess = NextProgressMessage
    If Not NextMess Is Nothing Then
        If NextMess.StatusID = IsID Then
            NextProgressMessageIs = True
        End If
    End If
End Function

Private Sub SendContinueTransactionRecord(ByVal ContinueType As ContinueTRecordAction, Optional ByVal AddParamValue As String = "")
    Dim ContinueTRec As New cContinueTransactionRecord
    Dim ActionParams As Collection
    Dim ActionParam As ContinuationMessageParameter
    
    If Not ContinueTRec Is Nothing Then
        Select Case ContinueType
            Case ContinueTRecordAction.etraVoiceReferralAuthorised
                Set ActionParams = New Collection
                ActionParam.Name = ContinueTRec.ContinuationMessageActionParameters()(ContinuationMessageActionParameter.ecmapAuthCode)
                ActionParam.Value = AddParamValue
                Call ActionParams.Add(ActionParam)
                Call ContinueTRec.Initialise(ContinueType, ActionParams)
            Case Else
                Call ContinueTRec.Initialise(ContinueType)
        End Select
        With wskIntegration
            Call DebugMsg(ModuleName, "SendContinueTransactionRecord", endlDebug, "Integration Winsock state is currently " & .State)
            Select Case .State
                Case sckConnected
                    Call DebugMsg(ModuleName, "SendContinueTransactionRecord", endlDebug, "Sending Continue Transaction Record - Action ID = " & Trim(CStr(ContinueType)))
                    ' Now send the new transaction
                    Call .SendData(ContinueTRec.ToIntegrationMessage)  'Send Transaction
                    ' Make sure the data is actually sent, now
                    DoEvents
                    Call DebugMsg(ModuleName, "SendContinueTransactionRecord", endlDebug, "Sent Continue Transaction Record - Action ID = " & Trim(CStr(ContinueType)))
                Case sckError
            End Select
        End With
    End If
End Sub

Private Function SendTransaction(ByVal Command As String) As Boolean
    
Retry:
    With wskIntegration
        Select Case .State
            Case sckConnected
                Call DebugMsg(ModuleName, "SendTransaction", endlDebug, "Calling send transaction")
                ' Now send the new transaction
                Call .SendData(Command)  'Send Transaction
                ' Make sure the data is actually sent, now
                DoEvents
                Call DebugMsg(ModuleName, "SendTransaction", endlDebug, "Transaction sent")
                SendTransaction = True
            Case sckError
                Call DebugMsg(ModuleName, "SendTransaction", endlDebug, "Error Found")
            Case Else
        End Select
    End With
End Function ' SendTransaction

Private Sub WaitForData()

    Do
        Call Wait(1)
        DoEvents
    Loop While ReceivedMessage = "" And NextProgressMessage Is Nothing And Not ConnectionError
    If ReceivedMessage <> "" Then
        MessageReceived = True
    End If
End Sub

Private Function WinSockError() As String

    If myIntegrationError <> "" Then
        WinSockError = myIntegrationError
    Else
        If myProgressError <> "" Then
            WinSockError = myProgressError
        End If
    End If
End Function

Private Sub InitialiseStatusBar()

    With sbStatus
        .Panels(PANEL_VERNO).Text = "Ver. " & App.Major & "." & App.Minor & "." & App.Revision
        .Panels(PANEL_VERNO).ToolTipText = "Program Version Number"
        .Panels(PANEL_WSID + 1).Text = Right$("00" & moCommideaCfg.TillID, 2) & " "
        .Panels(PANEL_WSID + 1).ToolTipText = "Current Work-Station Number"
        .Panels(PANEL_DATE + 1).Text = Format$(Date, "DD-MMM-YY")
        If (moCommideaCfg.ShowKeypadIcon = False) Then
            sbStatus.Panels("NumPad").Picture = Nothing
        End If
    End With
End Sub

Public Sub ICommideaForm_InitializeBeforeLoad(ByVal commideaCfg As CommideaConfiguration)
    Set moCommideaCfg = commideaCfg
End Sub

Public Sub ICommideaForm_ResetAfterUnload()
    Set moCommideaCfg = Nothing
End Sub

