VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cContinueTransactionRecordCreator"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function GetVoiceReferralAuthorisedContinueTransactionRecord(ByVal AuthorisationCode As String) As cContinueTransactionRecord
    Dim ContinueTRec As New cContinueTransactionRecord
    Dim ActionParams As Collection
    Dim ActionParam As ContinuationMessageParameter

    Set ActionParams = New Collection
    ActionParam.Name = ContinueTRec.ContinuationMessageActionParameters()(ContinuationMessageActionParameter.ecmapAuthCode)
    ActionParam.Value = AuthorisationCode
    Call ActionParams.Add(ActionParam)
    Call ContinueTRec.Initialise(ContinueTRecordAction.etraVoiceReferralAuthorised, ActionParams)
    
    GetVoiceReferralAuthorisedContinueTransactionRecord = ContinueTRec
End Function

Public Function GetReprintContinueTransactionRecord() As cContinueTransactionRecord
    Dim ContinueTRec As New cContinueTransactionRecord
    Dim ActionParams As Collection
    Dim ActionParam As ContinuationMessageParameter

    Set ActionParams = New Collection
    ActionParam.Name = ContinueTRec.ContinuationMessageActionParameters()(ContinuationMessageActionParameter.ecmapRePrintOpt)
    ActionParam.Value = "FILE"
    Call ActionParams.Add(ActionParam)
    Call ContinueTRec.Initialise(ContinueTRecordAction.etraReprintReceipt, ActionParams)
    
    GetReprintContinueTransactionRecord = ContinueTRec
End Function

Public Function GetContinueTransactionRecordForContinueTRecordActionType(ByVal ContinueType As ContinueTRecordAction) As cContinueTransactionRecord
    Dim ContinueTRec As New cContinueTransactionRecord

    Call ContinueTRec.Initialise(ContinueType)
    
    GetContinueTransactionRecordForContinueTRecordActionType = ContinueTRec
End Function
