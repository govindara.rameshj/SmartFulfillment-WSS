VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cOutputTRecord"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

    Public Enum TransactionResultNonError
        etrneCompleted = 0
        etrneDeclined = 7
    End Enum
    
    Public Enum TransactionResultNonErrorScreenless
        etrnesCompleted = 0
        etrnesReferred = 2
        etrnesDeclined = 5
        etrnesAuthorised = 6
        etrnesReversed = 7
    End Enum
    
    Private Const ModuleName As String = "cOutputTRecord"

' Property fields
    Private myScreenless As Boolean
    Private myTResultMessage As String
' End of property fields

    Private myReceivedMessage As cReceivedIntegrationMessage

    Private myParameters As Collection

' Public methods

    Public Sub Initialise(Optional ByVal IsScreenless As Boolean = True)

        Set myReceivedMessage = New cReceivedIntegrationMessage
        myReceivedMessage.Initialise
        myScreenless = IsScreenless
    End Sub
' End of Public methods

' Properties

    '        1. Transaction result:
    '           '0' - Completed
    '           '7' - Declined
    '           '-nn' - All other negative values are
    '           used to define error conditions
    '           Appendix B contains a full list of error
    '           codes and messages.
    '           Screenless transaction results:
    '           '0' - Completed
    '           '2' - Referred
    '           '5' - Declined
    '           '6' - Authorised
    '           '7' - Reversed
    '           '-nn' - Negative values are used to
    '           define error conditions
    Public Property Get TResult() As Integer
    
        TResult = myReceivedMessage.Result
    End Property

        ' 2. Teminate Loop Reserved, Ignore
    Public Property Get TerminateLoop() As Integer

        TerminateLoop = myReceivedMessage.TerminateLoop
    End Property
    
    '  3. Values will be truncated to the correct
    '    number of decimal places
    '    required for the transaction currency.
    '    For example:
    '    1.23 = 1 (one Japanese Yen)
    '    Field will show total that will be debited
    Public Property Get TotalTranValueProcessed() As Double

        TotalTranValueProcessed = myReceivedMessage.TotalTranValue
    End Property

    ' 4 Cashback Value Double As above
    Public Property Get CashBackValue() As Double

        CashBackValue = myReceivedMessage.CashBackValue
    End Property
       
    ' 5 Gratuity Value Double As above
    Public Property Get GratuityValue() As Double

        GratuityValue = myReceivedMessage.GratuityValue
    End Property
        
    ' 6 PAN Integer The Primary Account Number
    ' (Card Number).
    ' Please note: This value will not be
    ' returned in full due to PCI requirements.
    ' The PAN will be masked apart from the
    ' last four digits, e.g. '************1234'
    Public Property Get PAN() As String

        PAN = myReceivedMessage.PAN
    End Property

    ' Expiry Date MMYY Integer Card Expiry Month and Year.
    Public Property Get ExpiryDate() As String

        ExpiryDate = myReceivedMessage.ExpiryDate
    End Property

    ' 8 Issue Number Integer Card Issue Number. Blank when scheme
    ' does not provide an issue number.
    Public Property Get IssueNumber() As String

        IssueNumber = myReceivedMessage.IssueNumber
    End Property

    ' 9 Start MMYY Integer Card start month and year
    Public Property Get StartDate() As String

        StartDate = myReceivedMessage.StartDate
    End Property

    ' 10 Transaction Date / Time Integer CCYYMMDDHHMMSS
    Public Property Get TransactionDate() As String

        TransactionDate = myReceivedMessage.TransactionDate
    End Property

    ' 11 Merchant Number Integer The Merchant Number for the given' card scheme and account.
    Public Property Get MerchantNumber() As String

        MerchantNumber = myReceivedMessage.MerchantNumber
    End Property

    ' 12 Terminal ID Integer Terminal ID used for this transaction.
    Public Property Get TerminalID() As String

        TerminalID = myReceivedMessage.TerminalID
    End Property

    ' 13 Scheme Name String Card scheme name e.g. visa etc
    Public Property Get SchemeName() As String

        SchemeName = myReceivedMessage.SchemeName
    End Property

    ' 14 Floor Limit Integer Floor limit for the card scheme/account.
    Public Property Get FloorLimit() As Double

        FloorLimit = myReceivedMessage.FloorLimit
    End Property
    
    ' 15 EFT Sequence Number Integer Four digits in the range 0001 - 9999.
    ' (Prefixed with "OL" when offline)
    Public Property Get EFTSequenceNumber() As String
        If Mid(myReceivedMessage.EFTSequenceNumber, 1, 2) = "OL" Then
            EFTSequenceNumber = Mid(myReceivedMessage.EFTSequenceNumber, 3)
        Else
            EFTSequenceNumber = myReceivedMessage.EFTSequenceNumber
        End If
    End Property
    
    ' 16 Authorisation Code String Blank if the transaction is declined or is
    ' below the floor limit.
    Public Property Get AuthorisationCode() As String

        AuthorisationCode = myReceivedMessage.AuthorisationCode
    End Property
    
    ' 17 Referral Telephone Number. Reserved, ignore
    Public Property Get ReferralTelephoneNumber() As String

        ReferralTelephoneNumber = myReceivedMessage.ReferralTelephoneNumber
    End Property
    
    ' 18 Customer Verification Method /
    ' Authorisation Message / Error Message
    ' / Status Message
    ' String As returned by communications process.
    ' Normally direct from acquirer. Also
    ' contains status message if enabled
    Public Property Get CustomerVerification() As String

        CustomerVerification = myReceivedMessage.Message
    End Property
    
    ' Non Parameter based properties
    
    Public Property Get ErrorAction() As String
    
        If TResult < 0 Then
            ErrorAction = Errors.Action(TResult)
        End If
    End Property
    
    Public Property Get ErrorDescription() As String
    
        If TResult < 0 Then
            ErrorDescription = Errors.Description(TResult)
        End If
    End Property
    
    Public Property Get ErrorMessage() As String
    
        If TResult < 0 Then
            ErrorMessage = Errors.Message(TResult)
        End If
    End Property
    
     ' This is used to parse the output TRecord.
    ' It is the number of returned parameters from Commidea
    Public Property Get NumberOfParameters() As Integer
    
        NumberOfParameters = 18
    End Property
    
    Public Property Get ReceivedIntegrationMessage() As cReceivedIntegrationMessage
    
        Set ReceivedIntegrationMessage = myReceivedMessage
    End Property
    
    Public Property Get ResultMessage() As String
    
        If Screenless Then
            Select Case TResult
                Case TransactionResultNonErrorScreenless.etrnesCompleted
                    ResultMessage = "Completed"
                Case TransactionResultNonErrorScreenless.etrnesReferred
                    ResultMessage = "Referred"
                Case TransactionResultNonErrorScreenless.etrnesDeclined
                    ResultMessage = "Declined"
                Case TransactionResultNonErrorScreenless.etrnesAuthorised
                    ResultMessage = "Authorised"
                Case TransactionResultNonErrorScreenless.etrnesReversed
                    ResultMessage = "Reversed"
                Case Is < 0
                    ' Error, so pick up error message
                    ResultMessage = ErrorMessage
                Case Else
                    ResultMessage = "Unknown Result"
            End Select
        Else
            Select Case TResult
                Case TransactionResultNonError.etrneCompleted
                    ResultMessage = "Completed"
                Case TransactionResultNonError.etrneDeclined
                    ResultMessage = "Declined"
                Case Is < 0
                    ' Error, so pick up error message
                    ResultMessage = ErrorMessage
                Case Else
                    ResultMessage = "Unknown Result"
            End Select
        End If
    End Property
    
    Public Property Get Screenless() As Boolean
    
        Screenless = myScreenless
    End Property
    
    Public Property Get Successful() As Boolean
    
        If TResult >= 0 Then
            Successful = True
        End If
    End Property
    
    Public Property Get Understood() As Boolean
    
        Understood = myReceivedMessage.Understood
    End Property
    ' End of Non Parameter based properties

' End of Properties

' Friend methods
    
    Friend Function ParseReceivedMessage(ByVal Message As String) As Boolean

    On Error GoTo Catch

Try:
        ' Output record is the same basic structure as the default ReceivedIntegrationMessage, so use that
        If myReceivedMessage.ParseReceivedMessage(Message) Then
            ParseReceivedMessage = True
        End If
        
        Exit Function
Catch:
        Call Err.Raise(vbObjectError + 1, ModuleNameStub & "ParseReceivedMessage", "Failed to Parse output Transaction Record.")
    End Function
' End of friend methods
    
' Private methods
    
    Private Function ModuleNameStub() As String
    
        ModuleNameStub = AppNameStub & ModuleName & "."
    End Function
' End of Properties

