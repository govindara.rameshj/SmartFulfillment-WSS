VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cOutputCardDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Enum CardDetailsResult
    ecdrSuccess = 0
    ecdrNotRecognised = -4
    ecdrUnexpectedEFTOrNonEFT = -5
    ecdrNonEFTInvalidRecord = -12
    ecdrNonEFTProcessingFailed = -30
    ecdrNonEFTOperationModeNotSupported = -70
    ecdrNonEFTUserNotLoggedIn = -85
    ecdrNonEFTServiceNotAllowed = -90
    ecdrNonEFTRetrievalCancelled = -99
    ecdrNonEFTTimeoutWaitingForCard = -135
End Enum

Private Const ModuleName As String = "cOutputCardDetails"

Private Enum ParameterID
    epResult
    epCardData
    epSchemeName
    epHash
End Enum

' Property fields
Private myResult As Integer
Private myCardData As String
Private mySchemeName As String
Private myHash As String
Private myIsEFTCard As Boolean
' End of property fields

Private myParameters As Collection

' Public methods

Public Sub Initialise(Optional ByVal EFTCard As Boolean = True)

    myIsEFTCard = EFTCard
    Set myParameters = New Collection
    Call BuildDataArray
End Sub
' End of Public methods

' Properties

'        1. Get Card Details result:
'           '0'    - Success
'           '-nn'  - Negative value indicates an error as follows
'           '-4'   - Card not recognised
'           '-5'   - EFT Card presented when expecting a non EFT, or vice-a-versa
'           '-12'  - Invalid record
'           '-30'  - Processing failed
'           '-70'  - Operation mode not supported
'           '-85'  - User not logged in
'           '-90'  - Service not allowed
'           '-99'  - Retrieval cancelled
'           '-135' - Timeout waiting for card
Public Property Get Result() As Integer

    Result = myResult
End Property
    
' 2 Card Data
' For EFT identified cards, this will be
' the starred number where all the digits
' bar the 1st 6 are starred, e.g. '123456**********'.
' For Non EFT cards, this will be the full track 2
Public Property Get CardData() As String

    CardData = myCardData
End Property

' 3 Scheme Name String Card scheme name e.g. visa etc
Public Property Get SchemeName() As String

    SchemeName = mySchemeName
End Property

' The SHA-256 hash of the PAN, with salt to ensure the same PAN gives different hash for different merchants
' NB only get this if GetCardDetails record had POSVersion set to 1
Public Property Get Hash() As String

    Hash = myHash
End Property

' This is used to parse the output record.
' It is the number of returned parameters from Commidea
Public Property Get NumberOfParameters() As Integer

    NumberOfParameters = 4
End Property
' End of Properties

' Private methods

Private Sub CreateParameterInfoAndAddtoArray(ByVal Name As String, ByVal PropertyName As String, ByVal ParamID As ParameterID, ByVal ParamType As ParamValueType)
    Dim ParamInfo As cParameterInfo
    
    Set ParamInfo = New cParameterInfo
    Call ParamInfo.Initialise(Name, PropertyName, ParamID, ParamType)
    Call myParameters.Add(ParamInfo, Key:=Trim(CStr(ParamID)))
End Sub

Private Sub BuildDataArray()
    
    Call CreateParameterInfoAndAddtoArray("Result", "Result", ParameterID.epResult, ParamValueType.epvtInteger)
    Call CreateParameterInfoAndAddtoArray("CardData", "CardData", ParameterID.epCardData, ParamValueType.epvtString)
    Call CreateParameterInfoAndAddtoArray("SchemeName", "SchemeName", ParameterID.epSchemeName, ParamValueType.epvtString)
    Call CreateParameterInfoAndAddtoArray("Hash", "Hash", ParameterID.epHash, ParamValueType.epvtString)
End Sub

' this method will use the comma as a delimiter and populate the values above
Friend Function ParseReceivedMessage(ByVal Message As String) As Boolean
    Dim MessageLen As Integer
    ReDim ParamValues(Me.NumberOfParameters) As String
    Dim Delimiter As String
    Dim Param As Integer

On Error GoTo Catch

Try:
    ' if the transaction fails then return. Success is 0
    MessageLen = Len(Message)
    If MessageLen > 0 Then
        '  define which character is seperating fields
        Delimiter = ","
        ParamValues = Split(Message, Delimiter)
        ' Not always get last parameter (only comes if card card details record with POSVersion = 1)
        For Param = 0 To NumberOfParameters - 2
            ' only bother setting the rtn fields if there is a value
            If Len(ParamValues(Param)) > 0 Then
                Call SetPropertyFromParameterValue(ParamValues(Param), Param)
            End If
        Next Param
        ' If have the final parameter then do that one too (Hash, only avaiable with Get Card Details record with POSVersion = 1)
        If UBound(ParamValues) = Param Then
            ' only bother setting the rtn fields if there is a value
            If Len(ParamValues(Param)) > 0 Then
                Call SetPropertyFromParameterValue(ParamValues(Param), Param)
            End If
        End If
    End If
    
    ParseReceivedMessage = True
    
    Exit Function
    
Catch:
    Call Err.Raise(vbObjectError + 1, ModuleNameStub & "ParseReceivedMessage", "Failed to parse output Card Details.")
End Function

Private Sub SetPropertyFromParameterValue(ByVal ParamValue As String, ByVal ParamID As Integer)
    Dim TempInt As Integer
    Dim TempDbl As Double
    Dim TempDate As Date
    Dim CharPos As Integer
    Dim ParamChar As String
    
    Select Case ParamID
        Case ParameterID.epResult:
            If ParamToInt(TempInt, ParamValue) Then
                If TempInt = CardDetailsResult.ecdrSuccess _
                Or TempInt = CardDetailsResult.ecdrNotRecognised _
                Or TempInt = CardDetailsResult.ecdrUnexpectedEFTOrNonEFT _
                Or TempInt = CardDetailsResult.ecdrNonEFTInvalidRecord _
                Or TempInt = CardDetailsResult.ecdrNonEFTProcessingFailed _
                Or TempInt = CardDetailsResult.ecdrNonEFTOperationModeNotSupported _
                Or TempInt = CardDetailsResult.ecdrNonEFTUserNotLoggedIn _
                Or TempInt = CardDetailsResult.ecdrNonEFTServiceNotAllowed _
                Or TempInt = CardDetailsResult.ecdrNonEFTRetrievalCancelled _
                Or TempInt = CardDetailsResult.ecdrNonEFTTimeoutWaitingForCard Then
                    myResult = TempInt
                End If
            Else
                'err.Raise vbobjecterror+1,
            End If
        Case ParameterID.epCardData:
            If Len(ParamValue) > 6 Then
                For CharPos = 1 To 6
                    ParamChar = Mid(ParamValue, CharPos, 1)
                    If Not IsNumeric(ParamChar) Then
                        Exit For
                    End If
                Next CharPos
                If CharPos > 6 Then
                    For CharPos = 7 To Len(ParamValue)
                        ParamChar = Mid(ParamValue, CharPos, 1)
                        If ParamChar <> "*" _
                        And ParamChar <> "-" Then
                            Exit For
                        End If
                    Next CharPos
                End If
                If CharPos > Len(ParamValue) Then
                   myCardData = ParamValue
                End If
            End If
        Case ParameterID.epSchemeName:
            mySchemeName = ParamValue
        Case ParameterID.epHash:
            myHash = ParamValue
        Case Else
    End Select
End Sub

Private Function ModuleNameStub() As String

    ModuleNameStub = AppNameStub & ModuleName & "."
End Function
