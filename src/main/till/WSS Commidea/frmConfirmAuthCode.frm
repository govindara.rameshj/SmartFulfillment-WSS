VERSION 5.00
Begin VB.Form frmConfirmAuthCode 
   Caption         =   "Confirm Transaction"
   ClientHeight    =   3135
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5940
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   3135
   ScaleWidth      =   5940
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdConfirm 
      Caption         =   "Con&firm"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Left            =   1325
      TabIndex        =   5
      Top             =   2500
      Width           =   1150
   End
   Begin VB.CommandButton cmdAbort 
      Cancel          =   -1  'True
      Caption         =   "A&bort"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Left            =   3440
      TabIndex        =   6
      Top             =   2500
      Width           =   1150
   End
   Begin VB.Label lblPromptCSC 
      Alignment       =   2  'Center
      Caption         =   "Security Code - not checked"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   0
      TabIndex        =   1
      Top             =   630
      Width           =   5955
   End
   Begin VB.Label lblPromptAVSPostcode 
      Alignment       =   2  'Center
      Caption         =   "Postcode Check - not checked"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   0
      TabIndex        =   3
      Top             =   1530
      Width           =   5955
   End
   Begin VB.Label lblPromptAVSHouse 
      Alignment       =   2  'Center
      Caption         =   "House Number Check - not checked"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   0
      TabIndex        =   2
      Top             =   1080
      Width           =   5955
   End
   Begin VB.Label lblPromptAuthCode 
      Alignment       =   2  'Center
      Caption         =   "Authorisation Code: "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   0
      TabIndex        =   4
      Top             =   1980
      Width           =   5955
   End
   Begin VB.Label lblPromptConfirmAuthorisationCode 
      Alignment       =   2  'Center
      Caption         =   "Complete the Transaction?"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   0
      TabIndex        =   0
      Top             =   0
      UseMnemonic     =   0   'False
      Width           =   5955
   End
End
Attribute VB_Name = "frmConfirmAuthCode"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Implements ICommideaForm

Private moCommideaCfg As CommideaConfiguration
Private mbConfirmed As Boolean
Private GapToButtons As Long
Private ReturnKeyPressed As Boolean

Public Event Duress()

Private Sub cmdAbort_Click()

    mbConfirmed = False
    If Me.Visible Then
        Me.Hide
    End If
    Call DebugMsg(ModuleName, "Abort", endlTraceOut)
End Sub

Private Sub cmdConfirm_Click()

    mbConfirmed = True
    If Me.Visible Then
        Me.Hide
    End If
    Call DebugMsg(ModuleName, "Confirm", endlTraceOut)
End Sub

Private Sub Form_Activate()

    Me.cmdConfirm.SetFocus
End Sub

Private Sub Form_Load()

    Me.BackColor = moCommideaCfg.MsgBoxPromptColour
    Me.Visible = False
    Me.lblPromptConfirmAuthorisationCode.BackColor = moCommideaCfg.MsgBoxPromptColour
    Me.lblPromptAuthCode.BackColor = moCommideaCfg.MsgBoxPromptColour
    Me.lblPromptCSC.BackColor = moCommideaCfg.MsgBoxPromptColour
    Me.lblPromptAVSHouse.BackColor = moCommideaCfg.MsgBoxPromptColour
    Me.lblPromptAVSPostcode.BackColor = moCommideaCfg.MsgBoxPromptColour
    Call DebugMsg(ModuleName, "Form_Load", endlTraceOut)
    ' Save gap between top of text and top of buttons below
    GapToButtons = cmdAbort.Top - Me.lblPromptAuthCode.Top
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 And ReturnKeyPressed = True Then
        Call DebugMsg(ModuleName, "Return Key Pressed again. Clearing the Key Press", endlDebug, "Return Key pressed...")
        KeyCode = 0
    End If
    If KeyCode = 13 Then ReturnKeyPressed = True
    If moCommideaCfg.UnderDuressKeyCode <> 0 And KeyCode = moCommideaCfg.UnderDuressKeyCode Then
        KeyCode = 0
        RaiseEvent Duress
    End If
End Sub

Public Function ShowMe(ByVal AuthCode As String, _
                       ByVal AVSHouseNoResult As AVSHouseNumberCheckResult, _
                       ByVal AVSPostcodeResult As AVSPostCodeCheckResult, _
                       ByVal CSCResult As CSCCheckResult, _
                       Optional ByVal AutoAbort As Boolean = False _
                       ) As Boolean
    Dim ShrinkFormBy As Long
    Dim AVSFail As Boolean

    If LCase(AuthCode) = "confirmed" Then
        ' If CNP refund then no authorising - its all ready confirmed,
        ' so display a 'do you want to proceed with txn' type message instead,
        ' i.e. just an opportunity to cancel
        ' Save amount move buttons up by, to use to shrink form height with
        ShrinkFormBy = cmdAbort.Top - Me.lblPromptCSC.Top - GapToButtons
        With Me.lblPromptAuthCode
            .Visible = False
            cmdAbort.Top = Me.lblPromptCSC.Top + GapToButtons
            cmdConfirm.Top = Me.lblPromptCSC.Top + GapToButtons
        End With
        Me.lblPromptAVSHouse.Visible = False
        Me.lblPromptAVSPostcode.Visible = False
        Me.lblPromptCSC.Visible = False
        Me.Caption = "Confirm Transaction"
        Me.Height = Me.Height - ShrinkFormBy
    Else
        ' Show explanation for Auth Code confirmation requirement - if there is one
        If CSCResult = ecscrNotChecked Then
            ' Nothing doing, just here so Else if for all other conditions
            AVSFail = True
        ElseIf CSCResult = ecscrNotMatched Then
            With Me.lblPromptCSC
                .Caption = Replace(.Caption, "checked", "matched", Compare:=vbTextCompare)
            End With
            AVSFail = True
        ElseIf CSCResult = ecscrNotProvided Then
            With Me.lblPromptCSC
                .Caption = Replace(.Caption, "not checked", "not provided", Compare:=vbTextCompare)
            End With
            AVSFail = True
        Else
            ' Don't display CSC result unless failed
            With Me.lblPromptCSC
                .Visible = False
                ' Save distance moving by
                ShrinkFormBy = Me.lblPromptAVSHouse.Top - .Top
                ' Move everything up
                With Me.lblPromptAVSPostcode
                    Me.lblPromptAuthCode.Top = .Top
                    .Top = Me.lblPromptAVSHouse.Top
                End With
                Me.lblPromptAVSHouse.Top = .Top
                ' leave a gap from textbox to buttons
                With Me.lblPromptAuthCode
                    Me.cmdAbort.Top = .Top + GapToButtons
                End With
                Me.cmdConfirm.Top = Me.cmdAbort.Top
                ' and shrink the form
                Me.Height = Me.Height - ShrinkFormBy
            End With
        End If
        If AVSHouseNoResult = eavshncrNotChecked Then
            ' Nothing doing, just here so Else if for all other conditions
            ' Now allowing a Not Checked on House Number
            'AVSFail = True
        ElseIf AVSHouseNoResult = eavshncrNotMatched Then
            With Me.lblPromptAVSHouse
                .Caption = Replace(.Caption, "checked", "matched", Compare:=vbTextCompare)
            End With
            AVSFail = True
        ElseIf AVSHouseNoResult = eavshncrNotProvided Then
            With Me.lblPromptAVSHouse
                .Caption = Replace(.Caption, "not checked", "not provided", Compare:=vbTextCompare)
            End With
            ' Now allowing a Not Provided on House Number
            'AVSFail = True
        Else
            With Me.lblPromptAVSHouse
                .Caption = Replace(.Caption, "not checked", "ok", Compare:=vbTextCompare)
            End With
        End If
        If AVSPostcodeResult = eavspccrNotChecked Then
            ' Nothing doing, just here so Else if for all other conditions
            AVSFail = True
        ElseIf AVSPostcodeResult = eavspccrNotMatched Then
            With Me.lblPromptAVSPostcode
                .Caption = Replace(.Caption, "checked", "matched", Compare:=vbTextCompare)
            End With
            AVSFail = True
        ElseIf AVSPostcodeResult = eavspccrNotProvided Then
            With Me.lblPromptAVSPostcode
                .Caption = Replace(.Caption, "not checked", "not provided", Compare:=vbTextCompare)
            End With
            AVSFail = True
        Else
            With Me.lblPromptAVSPostcode
                .Caption = Replace(.Caption, "not checked", "ok", Compare:=vbTextCompare)
            End With
        End If
        With Me.lblPromptAuthCode
            .Caption = .Caption & " " & UCase(AuthCode)
            .Visible = CBool(AuthCode <> "")
        End With
    End If
    If AVSFail = True And AutoAbort Then
        cmdAbort_Click
    Else
        Call Me.Show(vbModal)
    End If
    ShowMe = mbConfirmed
    Call DebugMsg(ModuleName, "ShowMe", endlTraceOut)
End Function

Private Function ModuleName() As String

    ModuleName = "frmConfirmAuthCode"
End Function


Public Sub ICommideaForm_InitializeBeforeLoad(ByVal commideaCfg As CommideaConfiguration)
    Set moCommideaCfg = commideaCfg
End Sub

Public Sub ICommideaForm_ResetAfterUnload()
    Set moCommideaCfg = Nothing
End Sub


