VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AuthorizationRequest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' 820 or 810
Private mPEDType As PEDType

' EFT(CreditCard) or BGIFT (GiftCard)
Private mRequestType As CommideaRequestType

' Transaction type for EFT request type, not used for BGIFT request type
' Payment, cashback or refund
Private mTransactionType As TRecordTransactionType

' Transaction type modifier for EFT request type, not used for BGIFT request type
' Modify Integration T record sent to commidea. Account on File, CNP etc
Private mTRecordModifier As TRecordModifier

' Transaction type for BGIFT request type, not used for EFT request type
Private mBgiftTransactionType As BGiftRequestTransactionType

' Transaction value
Private mTxnValue As Double

' Reference to WSS transaction
' Reference to give to commidea record to enable reconciling of dlpaid etc and commidea data
Private mWSSUniqueTransTenderRef As String

Private mRegisterForAOF As TRecordAccountOnFileRegistration
Private mAOFTokenID As String
Private mAccountID As String
Private mIsScreenless As Boolean

Public Property Get PEDType() As PEDType
    PEDType = mPEDType
End Property

Public Property Let PEDType(ByVal newPEDType As PEDType)
    mPEDType = newPEDType
End Property

Public Property Get RequestType() As CommideaRequestType
    RequestType = mRequestType
End Property

Public Property Let RequestType(ByVal newRequestType As CommideaRequestType)
    mRequestType = newRequestType
End Property

Public Property Get transactionType() As TRecordTransactionType
    transactionType = mTransactionType
End Property

Public Property Let transactionType(ByVal newTransactionType As TRecordTransactionType)
    mTransactionType = newTransactionType
End Property

Public Property Get TRecordModifier() As TRecordModifier
    TRecordModifier = mTRecordModifier
End Property

Public Property Let TRecordModifier(ByVal newTRecordModifier As TRecordModifier)
    mTRecordModifier = newTRecordModifier
End Property

Public Property Get BgiftTransactionType() As BGiftRequestTransactionType
    BgiftTransactionType = mBgiftTransactionType
End Property

Public Property Let BgiftTransactionType(ByVal newBgiftTransactionType As BGiftRequestTransactionType)
    mBgiftTransactionType = newBgiftTransactionType
End Property

Public Property Get TxnValue() As Double
    TxnValue = mTxnValue
End Property

Public Property Let TxnValue(ByVal newTxnValue As Double)
    mTxnValue = newTxnValue
End Property

Public Property Get WSSUniqueTransTenderRef() As String
    WSSUniqueTransTenderRef = mWSSUniqueTransTenderRef
End Property

Public Property Let WSSUniqueTransTenderRef(ByVal newWSSUniqueTransTenderRef As String)
    mWSSUniqueTransTenderRef = newWSSUniqueTransTenderRef
End Property

Public Property Get RegisterForAOF() As TRecordAccountOnFileRegistration
    RegisterForAOF = mRegisterForAOF
End Property

Public Property Let RegisterForAOF(ByVal newRegisterForAOF As TRecordAccountOnFileRegistration)
    mRegisterForAOF = newRegisterForAOF
End Property

Public Property Get AccountID() As String
    AccountID = mAccountID
End Property

Public Property Let AccountID(ByVal newAccountID As String)
    mAccountID = newAccountID
End Property

Public Property Get AOFTokenID() As String
    AOFTokenID = mAOFTokenID
End Property

Public Property Let AOFTokenID(ByVal newAOFTokenID As String)
    mAOFTokenID = newAOFTokenID
End Property

Public Property Get IsScreenless() As Boolean
    IsScreenless = mIsScreenless
End Property

Public Property Let IsScreenless(ByVal newIsScreenless As Boolean)
    mIsScreenless = newIsScreenless
End Property


Friend Function CreateAuthorizationDetails() As IAuthorizationDetails

    If (mRequestType = CommideaRequestType.BarclaycardGift) Then
        Dim record As BGiftResponseREcord
        Set record = New BGiftResponseREcord
        Set CreateAuthorizationDetails = record
    Else
        Dim oOutputTRecord As cOutputTRecordV4
        Set oOutputTRecord = New cOutputTRecordV4
        ' Generate a output record to store & parse received message in
        Call oOutputTRecord.Initialise(mIsScreenless)
        Set CreateAuthorizationDetails = oOutputTRecord
    End If

End Function
                
Friend Function CreateRequestRecord() As IRequestRecord

    If (RequestType = CommideaRequestType.BarclaycardGift) Then
        Dim record As BGiftRequestRecord
        Set record = New BGiftRequestRecord
        ' TODO OriginalTransactionID
        Call record.Initialise(AccountID, BgiftTransactionType, TxnValue, WSSUniqueTransTenderRef)
        Set CreateRequestRecord = record
    Else
        Dim oTRecord As cTRecord
        Set oTRecord = New cTRecord
        ' Generate a T Record to send to PED via socket
        Call oTRecord.Initialise(transactionType, TRecordModifier, TxnValue, WSSUniqueTransTenderRef, RegisterForAOF, AOFTokenID, AccountID)
        Set CreateRequestRecord = oTRecord
    End If

End Function





