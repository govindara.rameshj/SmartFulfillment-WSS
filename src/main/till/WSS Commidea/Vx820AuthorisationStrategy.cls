VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Vx820AuthorisationStrategy"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements IAuthorisationStrategy

Private myPrintCount As Integer
Private myReprintRequested As Boolean
Private myTranEngine As ITransactionEngine
Private myAuthoriseResultReceived As Boolean
Private myReceiptPrintCounter As Counter

Public Sub IAuthorisationStrategy_Initialize(tranEngine As ITransactionEngine)
    Set myTranEngine = tranEngine
    
    myPrintCount = 0
    myReprintRequested = False
    myAuthoriseResultReceived = False
    
    Set myReceiptPrintCounter = New Counter
    myReceiptPrintCounter.Initialise
    
End Sub

Public Function IAuthorisationStrategy_ProcessReversedTransactionResult(authResult As IAuthorizationDetails) As TransactionCompleteState
    Dim Result As TransactionCompleteState
    
    If Not myAuthoriseResultReceived And HaveProgressMessage Then
        myAuthoriseResultReceived = HavePendingProgressMessage(esiAuthResultReceived)
    End If
    
    If myAuthoriseResultReceived Then
        Result = TransactionCompleteState.NearlyCompleted
    Else
        Result = TransactionCompleteState.Completed
    End If
    
    IAuthorisationStrategy_ProcessReversedTransactionResult = Result
    
End Function

Private Function HaveProgressMessage() As Boolean

    HaveProgressMessage = UBound(myTranEngine.ProgressMessage()) > 0
End Function

Private Function HavePendingProgressMessage(ByVal IsID As ProgressStatusID) As Boolean
    Dim OutputProgressMessage As cOutputProgressMessage
    Dim NextMessage As Integer
    
    If HaveProgressMessage Then
        Set OutputProgressMessage = New cOutputProgressMessage
        If Not OutputProgressMessage Is Nothing Then
            With OutputProgressMessage
                .Initialise
                For NextMessage = 1 To UBound(myTranEngine.ProgressMessage())
                    If .ParseReceivedMessage(myTranEngine.ProgressMessage()(NextMessage)) Then
                        If .StatusID = IsID Then
                            HavePendingProgressMessage = True
                        End If
                    End If
                Next NextMessage
            End With
        End If
    End If
End Function

Public Sub IAuthorisationStrategy_ProcessContinueRequiredProgressStatus()
    If myReprintRequested Then
        Call myTranEngine.SendContinueTransactionRecord(etraReprintReceipt)
        myReprintRequested = False
    Else
        Call myTranEngine.SendContinueTransactionRecord(etraContinueTxn)
    End If

End Sub

Public Function IAuthorisationStrategy_ExtraProcessProgressStatus(StatusID As ProgressStatusID) As Boolean
    Dim Result As Boolean
    Result = True
    
    Select Case StatusID
        Case esiAuthResultReceived
            myAuthoriseResultReceived = True
        Case Else
            Result = False
    End Select
    
    IAuthorisationStrategy_ExtraProcessProgressStatus = Result
End Function

' VX820 may require reprint before this status
Public Function IAuthorisationStrategy_ProcessSignatureConfirmationRequiredProgressStatus() As Boolean
    If myReprintRequested Then
        Call myTranEngine.SendContinueTransactionRecord(etraReprintReceipt)
        myReprintRequested = False
        IAuthorisationStrategy_ProcessSignatureConfirmationRequiredProgressStatus = True
    Else
        IAuthorisationStrategy_ProcessSignatureConfirmationRequiredProgressStatus = False
    End If
End Function

' For VX820 use one counter for each esiPrinting* progress message processing
Public Function IAuthorisationStrategy_GetReceiptPrintCounter() As Counter
    Set IAuthorisationStrategy_GetReceiptPrintCounter = myReceiptPrintCounter
End Function

' For VX820 save the reprint flag. It is used to tell Ocius to reprint when processing SignatureConfirmationRequired.
' Returns false as PED does not print immediately, so there is no new receipt printed immediately.
Public Function IAuthorisationStrategy_ProcessReceiptRePrintRequired() As Boolean
    myReprintRequested = True
    IAuthorisationStrategy_ProcessReceiptRePrintRequired = False
End Function

' For VX820 change state only if the failure reason is not cancellation after max reprint attempts
Public Sub IAuthorisationStrategy_ShowStateForReceiptPrintFailure(ReceiptType As String, cancelledAfterMaxReprintAttempts As Boolean)
    If Not cancelledAfterMaxReprintAttempts Then
        Call myTranEngine.ShowStateForReceiptPrintFailure(ReceiptType)
    End If
End Sub

' For VX820 skip esiPrintingReceipt
Public Function IAuthorisationStrategy_SkipPrintingReceiptProcessing(StatusID As ProgressStatusID) As Boolean
    IAuthorisationStrategy_SkipPrintingReceiptProcessing = StatusID = ProgressStatusID.esiPrintingReceipt
End Function
