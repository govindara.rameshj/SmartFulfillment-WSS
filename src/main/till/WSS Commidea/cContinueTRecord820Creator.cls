VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cContinueTRecord820Creator"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Implements IContinueTRecordCreator

Public Function IContinueTRecordCreator_GetVoiceReferralAuthorisedContinueTransactionRecord(ByVal AuthorisationCode As String) As cContinueTransactionRecord
    
    Set IContinueTRecordCreator_GetVoiceReferralAuthorisedContinueTransactionRecord = CreateContinueTransactionRecordWithParameter(etraVoiceReferralAuthorised, ecmapAuthCode, AuthorisationCode)
End Function

Public Function IContinueTRecordCreator_GetReprintContinueTransactionRecord() As cContinueTransactionRecord
    
    Set IContinueTRecordCreator_GetReprintContinueTransactionRecord = CreateContinueTransactionRecordWithoutParameter(etraReprintReceipt)
End Function

Public Function IContinueTRecordCreator_GetContinueTransactionRecordForContinueTRecordActionType(ByVal ContinueType As ContinueTRecordAction) As cContinueTransactionRecord
    
    Set IContinueTRecordCreator_GetContinueTransactionRecordForContinueTRecordActionType = CreateContinueTransactionRecordWithoutParameter(ContinueType)
End Function

Public Function IContinueTRecordCreator_GetChargeAuthCodeContinueTransactionRecord(ByVal AuthorisationCode As String) As cContinueTransactionRecord

    Set IContinueTRecordCreator_GetChargeAuthCodeContinueTransactionRecord = CreateContinueTransactionRecordWithParameter(etraChargeAuthCode, ecmapAuthCode, AuthorisationCode)
End Function

Public Function IContinueTRecordCreator_GetCashbackRequiredContinueTransactionRecord(ByVal CashbackAmount As String) As cContinueTransactionRecord

    Set IContinueTRecordCreator_GetCashbackRequiredContinueTransactionRecord = CreateContinueTransactionRecordWithParameter(etraCashbackRequired, ecmapCashback, CashbackAmount)
End Function

Public Function IContinueTRecordCreator_GetReplaceAccountContinueTransactionRecord(ByVal ManagerPin As String) As cContinueTransactionRecord

    Set IContinueTRecordCreator_GetReplaceAccountContinueTransactionRecord = CreateContinueTransactionRecordWithParameter(etraReplaceAccount, ecmapMgrPIN, ManagerPin)
End Function

Public Function IContinueTRecordCreator_GetSelectPaypointAccountContinueTransactionRecord(ByVal AccountID As String) As cContinueTransactionRecord

    Set IContinueTRecordCreator_GetSelectPaypointAccountContinueTransactionRecord = CreateContinueTransactionRecordWithParameter(etraSelectPaypointAccount, ecmapPayPointAccID, AccountID)
End Function

Public Function IContinueTRecordCreator_GetSelectPaypointOptionContinueTransactionRecord(ByVal OptionId As String) As cContinueTransactionRecord

    Set IContinueTRecordCreator_GetSelectPaypointOptionContinueTransactionRecord = CreateContinueTransactionRecordWithParameter(etraSelectPaypointOption, ecmapPayPointOptionID, OptionId)
End Function

Private Function CreateContinueTransactionRecordWithParameter(ByVal ContinueType As ContinueTRecordAction, ByVal ContinuationMessageParameterType As ContinuationMessageActionParameter, ByVal ParameterValue As String)
    Dim ContinueTRec As New cContinueTransactionRecord
    Dim ActionParams As Collection
    Dim ActionParam As ContinuationMessageParameter

    Set ActionParams = New Collection
    ActionParam.Name = ContinueTRec.ContinuationMessageActionParameters()(ContinuationMessageParameterType)
    ActionParam.Value = ParameterValue
    Call ActionParams.Add(ActionParam)
    Call ContinueTRec.Initialise(ContinueType, ActionParams)

    Set CreateContinueTransactionRecordWithParameter = ContinueTRec
End Function

Private Function CreateContinueTransactionRecordWithoutParameter(ByVal ContinueType As ContinueTRecordAction) As cContinueTransactionRecord
    Dim ContinueTRec As New cContinueTransactionRecord

    Call ContinueTRec.Initialise(ContinueType)
    
    Set CreateContinueTransactionRecordWithoutParameter = ContinueTRec
End Function
