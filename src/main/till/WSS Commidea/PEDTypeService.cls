VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PEDTypeService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Const PED820_OciusSentinel_StartVersion = "3.06.4.43"

Public Enum PEDType
    ePEDType810
    ePEDType820
End Enum

Public Property Get PED820OciusSentinelStartVersion() As String

    PED820OciusSentinelStartVersion = PED820_OciusSentinel_StartVersion
End Property

Public Function GetPEDType(ByVal OciusSentinelVersion As String) As PEDType
    Dim PaddedPED820StartVersion As String
    Dim PaddedOciusSentinelVersion As String
    
    GetPEDType = ePEDType820
    
    PaddedOciusSentinelVersion = GetOciusSentinelVersionPaddedToSameNumberOfSectionsAsPED820StartVersion(OciusSentinelVersion)
    PaddedPED820StartVersion = GetPED820StartVersionPaddedToSameNumberOfSectionsAsOciusSentinelVersion(OciusSentinelVersion)
    If OciusSentinelVersionIsEarlierThanPED820StartVersion(PaddedOciusSentinelVersion, PaddedPED820StartVersion) Then
        GetPEDType = ePEDType810
    End If
End Function

Private Function GetOciusSentinelVersionPaddedToSameNumberOfSectionsAsPED820StartVersion(ByVal OciusSentinelVersion) As String
    Dim NumberOfSectionsInPED820StartVersion As Integer
    Dim NumberOfSectionsInOciusSentinelVersion As Integer

    GetOciusSentinelVersionPaddedToSameNumberOfSectionsAsPED820StartVersion = OciusSentinelVersion

    NumberOfSectionsInPED820StartVersion = NumberOfVersionSections(PED820OciusSentinelStartVersion)
    NumberOfSectionsInOciusSentinelVersion = NumberOfVersionSections(OciusSentinelVersion)
    If NumberOfSectionsInPED820StartVersion > NumberOfSectionsInOciusSentinelVersion Then
        GetOciusSentinelVersionPaddedToSameNumberOfSectionsAsPED820StartVersion = PadRightComparisonVersionWithZeroValueSections(OciusSentinelVersion, (NumberOfSectionsInPED820StartVersion - NumberOfSectionsInOciusSentinelVersion))
    End If
End Function

Private Function GetPED820StartVersionPaddedToSameNumberOfSectionsAsOciusSentinelVersion(ByVal OciusSentinelVersion) As String
    Dim NumberOfSectionsInPED820StartVersion As Integer
    Dim NumberOfSectionsInOciusSentinelVersion As Integer

    GetPED820StartVersionPaddedToSameNumberOfSectionsAsOciusSentinelVersion = PED820OciusSentinelStartVersion
    
    NumberOfSectionsInPED820StartVersion = NumberOfVersionSections(PED820OciusSentinelStartVersion)
    NumberOfSectionsInOciusSentinelVersion = NumberOfVersionSections(OciusSentinelVersion)
    If NumberOfSectionsInOciusSentinelVersion > NumberOfSectionsInPED820StartVersion Then
        GetPED820StartVersionPaddedToSameNumberOfSectionsAsOciusSentinelVersion = PadRightComparisonVersionWithZeroValueSections(PED820OciusSentinelStartVersion, (NumberOfSectionsInOciusSentinelVersion - NumberOfSectionsInPED820StartVersion))
    End If
End Function

Private Function NumberOfVersionSections(ByVal Version As String) As Integer
    Dim VersionSections() As String
    Dim UpperBoundOfVersionSections As Integer
    
    If Len(Version) > 0 Then
        VersionSections = Split(Version, ".")
        UpperBoundOfVersionSections = UBound(VersionSections)
    Else
        UpperBoundOfVersionSections = -1
    End If
    NumberOfVersionSections = UpperBoundOfVersionSections + 1
End Function

Private Function PadRightComparisonVersionWithZeroValueSections(ByVal ComparisonVersion As String, ByVal NumberOfZeroValueSectionsToPadOnRight As Integer) As String
    Dim InterimVersion As String
    Dim CharToBeReplaced As String
    Dim PointZero As String
    
    CharToBeReplaced = "~"
    PointZero = ".0"
    InterimVersion = ComparisonVersion & String(NumberOfZeroValueSectionsToPadOnRight, CharToBeReplaced)
    PadRightComparisonVersionWithZeroValueSections = Replace(InterimVersion, CharToBeReplaced, PointZero)
End Function

Private Function OciusSentinelVersionIsEarlierThanPED820StartVersion(ByVal OciusSentinelVersion As String, ByVal PED820OciusSentinelVersion As String) As Boolean
    Dim PED820OciusSentinelVersionParts() As String
    Dim OciusSentinelVersionParts() As String
    Dim Part As Integer
    
    OciusSentinelVersionIsEarlierThanPED820StartVersion = False
    
    PED820OciusSentinelVersionParts = Split(PED820OciusSentinelVersion, ".")
    OciusSentinelVersionParts = Split(OciusSentinelVersion, ".")
    For Part = 0 To UBound(OciusSentinelVersionParts)
        If VersionSectionValuesAreDifferent(OciusSentinelVersionParts(Part), PED820OciusSentinelVersionParts(Part)) Then
            If OciusSentinelVersionSectionIsEarlierThanPED820OciusSentinelVersionSection(OciusSentinelVersionParts(Part), PED820OciusSentinelVersionParts(Part)) Then
                OciusSentinelVersionIsEarlierThanPED820StartVersion = True
            End If
            Exit For
        End If
    Next Part
End Function

Private Function VersionSectionValuesAreDifferent(ByVal OciusSentinelVersionSection As String, ByVal PED820OciusSentinelStartVersionSection As String) As Boolean

    VersionSectionValuesAreDifferent = False
    
    If Val(OciusSentinelVersionSection) <> Val(PED820OciusSentinelStartVersionSection) Then
        VersionSectionValuesAreDifferent = True
    End If
End Function

Private Function OciusSentinelVersionSectionIsEarlierThanPED820OciusSentinelVersionSection(ByVal OciusSentinelVersionSection As String, ByVal PED820OciusSentinelStartVersionSection As String) As Boolean

    OciusSentinelVersionSectionIsEarlierThanPED820OciusSentinelVersionSection = False
    
    If Val(OciusSentinelVersionSection) < Val(PED820OciusSentinelStartVersionSection) Then
        OciusSentinelVersionSectionIsEarlierThanPED820OciusSentinelVersionSection = True
    End If
End Function
