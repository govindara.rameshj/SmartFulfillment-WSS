VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "BGiftResponseREcord"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Implements IAuthorizationDetails

Public Enum BGiftResponseRecordResultCode
    Success = 0
    Reversed = 1
    Rejected = 2
    Error = -1 'All negative values are errors (Error =-ve in Sentinel manual)
End Enum

Public Enum BGiftResponseRecordResponseCode
    Ok = 0
    NoCard = 10
    CardValidButNotForThisMerchant = 20
    PartialPurchaseTransaction = 30
    ExchangeRateNotPresent = 40
    MaximumLoadExceeded = 50
    MinimumLoadNotMet = 51
    MaximumValueOnCardExceeded = 52
    CardExpired = 60
    MessageTypeInvalid = 71
    VoidPeriodElapsed = 72
    MessageDetailsIncorrect = 73
    InsufficientFunds = 74
    CallCallCentre = 99
    ConfigurationProblem = -1
    EmptyCode = -2
End Enum

'Result of the transaction
Private resultCode As BGiftResponseRecordResultCode

Private responseCode As BGiftResponseRecordResponseCode

'Authorisation Code assigned by Card Commerce
Private transactionAuthCode As String

'Amount of the transaction
Private amount As Double ' Actually Decimal

'Remaining Balance of the card returned in the transaction response
Private remainingCardBalance As Double ' Actually Decimal

'Authorisation message indicating the result of the transaction
Private messageText As String

'Message number allocated to the transaction by VeriFone
Private MessageNumber As String

'Transaction date/time in the format DD MMM YYYY HH:MM:SS
Private transactionDateTime As String

'Transaction ID assigned by VeriFone's processing system
Private TransactionId As Double 'Actually Decimal

Public Property Get RecordResultCode() As BGiftResponseRecordResultCode
    RecordResultCode = resultCode
End Property

Public Property Get RecordResponseCode() As BGiftResponseRecordResponseCode
    RecordResponseCode = responseCode
End Property

Public Property Get AuthorisationMessage() As String
    AuthorisationMessage = messageText
End Property

Public Property Get RecordTransactionId() As Double
    RecordTransactionId = TransactionId
End Property

Public Property Get RecordMessageNumber() As String
    RecordMessageNumber = MessageNumber
End Property

Public Property Get IAuthorizationDetails_PAN() As String
    IAuthorizationDetails_PAN = "0000000000000000" ' TODO return real card number if it is possible
End Property

Public Property Get IAuthorizationDetails_ExpiryDate() As String
    IAuthorizationDetails_ExpiryDate = "" ' TODO parse it from Message?
End Property

Public Property Get IAuthorizationDetails_IssueNumber() As String
    IAuthorizationDetails_IssueNumber = ""
End Property

Public Property Get IAuthorizationDetails_StartDate() As String
    IAuthorizationDetails_StartDate = ""
End Property

Public Property Get IAuthorizationDetails_MerchantNumber() As String
    IAuthorizationDetails_MerchantNumber = ""
End Property

Public Property Get IAuthorizationDetails_TerminalID() As String
    IAuthorizationDetails_TerminalID = ""
End Property

Public Property Get IAuthorizationDetails_SchemeName() As String
    IAuthorizationDetails_SchemeName = "Barclaycard gift"
End Property

Public Property Get IAuthorizationDetails_EFTSequenceNumber() As String
    IAuthorizationDetails_EFTSequenceNumber = "0000"
End Property


Public Property Get IAuthorizationDetails_AuthorisationCode() As String
    IAuthorizationDetails_AuthorisationCode = transactionAuthCode
End Property

Public Property Get IAuthorizationDetails_CustomerVerification() As String
    IAuthorizationDetails_CustomerVerification = ""
End Property

Public Property Get IAuthorizationDetails_CaptureMethod() As String
    IAuthorizationDetails_CaptureMethod = ""
End Property

Public Property Get IAuthorizationDetails_TokenID() As String
    IAuthorizationDetails_TokenID = ""
End Property

Public Property Get IAuthorizationDetails_CardNumberHash() As String
    IAuthorizationDetails_CardNumberHash = ""
End Property

Public Property Get IAuthorizationDetails_TransactionDate() As String
    IAuthorizationDetails_TransactionDate = "" 'Do not set default value! To print receipts correctly
End Property

Public Property Get IAuthorizationDetails_TransactionAmount() As Double
    IAuthorizationDetails_TransactionAmount = amount
End Property

Public Function IAuthorizationDetails_IsAuthorized() As Boolean
    IAuthorizationDetails_IsAuthorized = True 'TODO
End Function

Friend Function IAuthorizationDetails_ParseReceivedMessage(ByVal Message As String) As Boolean

    Dim values() As String
    values = SplitTo1BasedStringArray(Message, ",")
    
    If (UBound(values) <> 9) Then
        Call Err.Raise(vbObjectError + 1, "BGiftResponseRecord.ParseReceivedMessage", "Unexpected number of values in BGiftResponseRecord.")
    End If
    
    resultCode = values(1)
    If Len(values(2)) > 0 Then
        responseCode = values(2)
    Else
        responseCode = EmptyCode
    End If
    transactionAuthCode = values(3)
    
    If Len(values(4)) > 0 Then
        amount = values(4)
    Else
        amount = 0
    End If
        
    If Len(values(5)) > 0 Then
        remainingCardBalance = values(5)
    Else
        remainingCardBalance = 0
    End If

    messageText = ParseMessageText(values(6), resultCode, responseCode)
    MessageNumber = values(7)
    transactionDateTime = values(8)
    If Len(values(9)) > 0 Then
        TransactionId = values(9)
    Else
        TransactionId = 0
    End If
    
    IAuthorizationDetails_ParseReceivedMessage = True

    Exit Function
End Function

Private Function ParseMessageText(ByVal msg As String, ByVal rsltCode As Integer, ByVal rspCode As Integer) As String
    If rsltCode = -99 And rspCode = BGiftResponseRecordResponseCode.EmptyCode Then
        ParseMessageText = "Transaction cancelled by User"
        Exit Function
    End If
    
    If rsltCode = BGiftResponseRecordResultCode.Rejected And rspCode = BGiftResponseRecordResponseCode.CallCallCentre Then
        ParseMessageText = "Please call Gift Card Helpline"
        Exit Function
    End If
    
    If rsltCode = BGiftResponseRecordResultCode.Rejected And rspCode = BGiftResponseRecordResponseCode.CardExpired Then
        ParseMessageText = "Gift Card Has Expired"
        Exit Function
    End If
    
    If rsltCode = BGiftResponseRecordResultCode.Reversed And rspCode = BGiftResponseRecordResponseCode.PartialPurchaseTransaction Then
        ParseMessageText = "Insufficient Balance"
        Exit Function
    End If
    
        If rsltCode = -64 Then
        ParseMessageText = "No response please try again"
        Exit Function
    End If
        
        If rsltCode = -30 Then
        ParseMessageText = "Time out error"
        Exit Function
    End If
        
    ParseMessageText = msg
End Function


