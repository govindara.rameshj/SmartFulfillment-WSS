VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cOutputTRecordV3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


Private Const ModuleName As String = "cOutputTRecordV3"

Private Enum ParameterID
    epCardNumberHash = 32
End Enum

' Property fields
Private myCardNumberHash As String
' End of property fields

Private myOutputTRecordV2 As cOutputTRecordV2
Private myReceivedMessage As String
Private myParameters As Collection
Private myParameterTitles() As String
Private myParameterColumnNames() As String
Private myUnderstood As Boolean
Private myErrorMessage As String

' Public methods

Public Sub Initialise(Optional ByVal IsScreenless As Boolean = True)

    Set myOutputTRecordV2 = New cOutputTRecordV2
    Call myOutputTRecordV2.Initialise(IsScreenless)
    Set myParameters = New Collection
    Call BuildDataArray
End Sub
' End of Public methods

' Properties


'        1. Transaction result:
'           '0' - Completed
'           '7' - Declined
'           '-nn' - All other negative values are
'           used to define error conditions
'           Appendix B contains a full list of error
'           codes and messages.
'           Screenless transaction results:
'           '0' - Completed
'           '2' - Referred
'           '5' - Declined
'           '6' - Authorised
'           '7' - Reversed
'           '-nn' - Negative values are used to
'           define error conditions
Public Property Get TResult() As Integer

    TResult = myOutputTRecordV2.TResult
End Property

    ' 2. Teminate Loop Reserved, Ignore
Public Property Get TerminateLoop() As Integer

    TerminateLoop = myOutputTRecordV2.TerminateLoop
End Property

'  3. Values will be truncated to the correct
'    number of decimal places
'    required for the transaction currency.
'    For example:
'    1.23 = 1 (one Japanese Yen)
'    Field will show total that will be debited
Public Property Get TotalTranValueProcessed() As Double

    TotalTranValueProcessed = myOutputTRecordV2.TotalTranValueProcessed
End Property

' 4 Cashback Value Double As above
Public Property Get CashBackValue() As Double

    CashBackValue = myOutputTRecordV2.CashBackValue
End Property
   
' 5 Gratuity Value Double As above
Public Property Get GratuityValue() As Double

    GratuityValue = myOutputTRecordV2.GratuityValue
End Property
    
' 6 PAN Integer The Primary Account Number
' (Card Number).
' Please note: This value will not be
' returned in full due to PCI requirements.
' The PAN will be masked apart from the
' last four digits, e.g. '************1234'
Public Property Get PAN() As String

    PAN = myOutputTRecordV2.PAN
End Property

' Expiry Date MMYY Integer Card Expiry Month and Year.
Public Property Get ExpiryDate() As String

    ExpiryDate = myOutputTRecordV2.ExpiryDate
End Property

' 8 Issue Number Integer Card Issue Number. Blank when scheme
' does not provide an issue number.
Public Property Get IssueNumber() As String

    IssueNumber = myOutputTRecordV2.IssueNumber
End Property

' 9 Start MMYY Integer Card start month and year
Public Property Get StartDate() As String

    StartDate = myOutputTRecordV2.StartDate
End Property

' 10 Transaction Date / Time Integer CCYYMMDDHHMMSS
Public Property Get TransactionDate() As String

    TransactionDate = myOutputTRecordV2.TransactionDate
End Property

' 11 Merchant Number Integer The Merchant Number for the given' card scheme and account.
Public Property Get MerchantNumber() As String

    MerchantNumber = myOutputTRecordV2.MerchantNumber
End Property

' 12 Terminal ID Integer Terminal ID used for this transaction.
Public Property Get TerminalID() As String

    TerminalID = myOutputTRecordV2.TerminalID
End Property

' 13 Scheme Name String Card scheme name e.g. visa etc
Public Property Get SchemeName() As String

    SchemeName = myOutputTRecordV2.SchemeName
End Property

' 14 Floor Limit Integer Floor limit for the card scheme/account.
Public Property Get FloorLimit() As Double

    FloorLimit = myOutputTRecordV2.FloorLimit
End Property

' 15 EFT Sequence Number Integer Four digits in the range 0001 - 9999.
' (Prefixed with "OL" when offline)
Public Property Get EFTSequenceNumber() As String
        If Mid(myOutputTRecordV2.EFTSequenceNumber, 1, 2) = "OL" Then
            EFTSequenceNumber = Mid(myOutputTRecordV2.EFTSequenceNumber, 3)
        Else
            EFTSequenceNumber = myOutputTRecordV2.EFTSequenceNumber
        End If
End Property

' 16 Authorisation Code String Blank if the transaction is declined or is
' below the floor limit.
Public Property Get AuthorisationCode() As String

    AuthorisationCode = myOutputTRecordV2.AuthorisationCode
End Property

' 17 Referral Telephone Number. Reserved, ignore
Public Property Get ReferralTelephoneNumber() As String

    ReferralTelephoneNumber = myOutputTRecordV2.ReferralTelephoneNumber
End Property

' 18 Customer Verification Method /
' Authorisation Message / Error Message
' / Status Message
' String As returned by communications process.
' Normally direct from acquirer. Also
' contains status message if enabled
Public Property Get CustomerVerification() As String

    CustomerVerification = myOutputTRecordV2.CustomerVerification
End Property

' 19 Capture Method String Valid values are:
' Contactless
' Swipe
' ICC
' Keyed
Public Property Get CaptureMethod() As String

    CaptureMethod = myOutputTRecordV2.CaptureMethod
End Property

' 20 Transaction Currency Code Reserved for Dynamic Currency Conversion
Public Property Get TransactionCurrencyCode() As Integer

    TransactionCurrencyCode = myOutputTRecordV2.TransactionCurrencyCode
End Property

' 21 Original Transaction Value Reserved for Dynamic Currency Conversion
Public Property Get OriginalTransactionValue() As Integer

    OriginalTransactionValue = myOutputTRecordV2.OriginalTransactionValue
End Property

' 22 Original Cashback Value Reserved for Dynamic Currency Conversion
Public Property Get OriginalCashbackValue() As Integer

    OriginalCashbackValue = myOutputTRecordV2.OriginalCashbackValue
End Property

' 23 Original Gratuity Value Reserved for Dynamic Currency Conversion
Public Property Get OriginalGratuityValue() As Integer

    OriginalGratuityValue = myOutputTRecordV2.OriginalGratuityValue
End Property

' 24 Original Transaction Currency
' Code
' Reserved for Dynamic Currency Conversion
Public Property Get OriginalTransactionCurrencyCode() As Integer

    OriginalTransactionCurrencyCode = myOutputTRecordV2.OriginalTransactionCurrencyCode
End Property

' 25 Barclays Bonus Discount Value Reserved for Barclays Bonus
Public Property Get BarclaysBonusDiscountValue() As Integer

    BarclaysBonusDiscountValue = myOutputTRecordV2.BarclaysBonusDiscountValue
End Property

' 26 Barclays Bonus Redemption Value Reserved for Barclays Bonus
Public Property Get BarclaysBonusRedemptionValue() As Integer

    BarclaysBonusRedemptionValue = myOutputTRecordV2.BarclaysBonusRedemptionValue
End Property

' 27 Account on File Registration
' Result
    ' Integer This is the result of the Account on File
    ' registration. Valid values are:
    '0' - Not Set
    '1' - Performed
    '2' - Success
    '3' - Failed
Public Property Get AccountonFileRegistration() As Integer

    AccountonFileRegistration = myOutputTRecordV2.AccountonFileRegistration
End Property

' 28 Token ID String This is the token allocated to the payment
' details as part of the Account on File
' registration process or the token used for
' the Account on File payment.
Public Property Get TokenID() As String

    TokenID = myOutputTRecordV2.TokenID
End Property

' 29 AVS Post Code Result Integer This is the result of any AVS post code
' checking. Valid values are:
' '0' - Not Provided
' '1' - Not Checked
' '2' - Matched
' '4' - Not Matched
' '8' - Reserved
Public Property Get AVSPostCode() As Integer

    AVSPostCode = myOutputTRecordV2.AVSPostCode
End Property

' 30 AVS House Number Result Integer This is the result of any AVS house number
' checking. Valid values are:
' '0' - Not Provided
' '1' - Not Checked
' '2' - Matched
' '4' - Not Matched
' '8' - Reserved
Public Property Get AVSHouseNumber() As Integer

    AVSHouseNumber = myOutputTRecordV2.AVSHouseNumber
End Property

' 31 CSC Result Integer This is the result of any CSC verification.
' Valid values are:
' '0' - Not Provided
' '1' - Not Checked
' '2' - Matched
' '4' - Not Matched
' '8' - Reserved
Public Property Get CSCResult() As Integer

    CSCResult = myOutputTRecordV2.CSCResult
End Property

' Hash of card number (PAN) generated by the WinTI3 infrastructure,
' only available for txns that have been sent for online for authorisation
Public Property Get CardNumberHash() As String

    CardNumberHash = myCardNumberHash
End Property

' Non Parameter based properties

Public Property Get CaptureMethods(ByVal MethodType As CaptureMethodType)
    
    CaptureMethods = myOutputTRecordV2.CaptureMethods(MethodType)
End Property

Public Property Get ErrorAction() As String

    ErrorAction = myOutputTRecordV2.ErrorAction
End Property

Public Property Get ErrorDescription() As String

    ErrorDescription = myOutputTRecordV2.ErrorDescription
End Property

Public Property Get ErrorMessage() As String

    ErrorMessage = myOutputTRecordV2.ErrorMessage
End Property

Public Property Get OutputTRecordV2() As cOutputTRecordV2

    Set OutputTRecordV2 = myOutputTRecordV2
End Property

Public Property Get Screenless() As Boolean

    Screenless = myOutputTRecordV2.Screenless
End Property

 ' This is used to parse the output record.
' It is the number of returned parameters from Commidea
Public Property Get NumberOfParameters() As Integer

    NumberOfParameters = 32
End Property

Public Property Get ResultMessage() As String

    ResultMessage = myOutputTRecordV2.ResultMessage
End Property
    
Public Property Get Successful() As Boolean

    Successful = myOutputTRecordV2.Successful
End Property

Public Property Get Understood() As Boolean

    Understood = myUnderstood
End Property
' End of Non Parameter based properties

' End of Properties

' Friend methods

Friend Function ParseReceivedMessage(ByVal Message As String) As Boolean

On Error GoTo Catch

Try:
    Clean
    ' 1st half of the Output record is the same basic structure as the cOutoutTRecord, so use that
    If myOutputTRecordV2.ParseReceivedMessage(Message) Then
        ' Now finish off parsing the rest of the message locally
        If LocalParseReceivedMessage(Message) Then
            ParseReceivedMessage = True
        End If
    End If

    myUnderstood = True
    ParseReceivedMessage = True

    Exit Function
Catch:
    Call Err.Raise(vbObjectError + 1, ModuleNameStub & "ParseReceivedMessage", "Failed to Parse output Transaction Record.")
End Function
' End of friend methods

' Private methods

Private Sub Clean()
    
    myCardNumberHash = ""
    myUnderstood = False
End Sub

Private Sub CreateParameterInfoAndAddtoArray(ByVal Name As String, ByVal PropertyName As String, ByVal ParamID As ParameterID, ByVal ParamType As ParamValueType)
    Dim ParamInfo As cParameterInfo
    
    Set ParamInfo = New cParameterInfo
    Call ParamInfo.Initialise(Name, PropertyName, ParamID, ParamType)
    Call myParameters.Add(ParamInfo, Key:=Trim(CStr(ParamID)))
End Sub

Private Sub BuildDataArray()
    
    Call CreateParameterInfoAndAddtoArray("CardNumberHash", "CardNumberHash", ParameterID.epCardNumberHash, ParamValueType.epvtString)
End Sub

Private Function LocalParseReceivedMessage(ByVal Message As String) As Boolean
    Dim MessageLen As Integer
    ReDim ParamValues(1 To Me.NumberOfParameters) As String
    Dim Delimiter As String
    Dim Param As Integer

On Error GoTo Catch

Try:
    Delimiter = ","
    ParamValues = SplitTo1BasedStringArray(Message, Delimiter)
    ' Even though specify V3, if an error will only get original version format,
    ' ie no parameters 18 - 32.
    If UBound(ParamValues) > myOutputTRecordV2.NumberOfParameters Then
        For Param = myOutputTRecordV2.NumberOfParameters To Me.NumberOfParameters
            ' only bother setting the rtn fields if there is a value
            If Len(ParamValues(Param)) > 0 Then
                Call SetPropertyFromParameterValue(ParamValues(Param), Param)
            End If
        Next Param
    End If

    myUnderstood = myOutputTRecordV2.Understood
    LocalParseReceivedMessage = True

    Exit Function
Catch:
    Call Err.Raise(vbObjectError + 1, ModuleNameStub & "LocalParseReceivedMessage", "Failed to parse output Transaction Record V2.")
End Function

Private Sub SetPropertyFromParameterValue(ByVal ParamValue As String, ByVal ParamID As ParameterID)

    Select Case ParamID
        Case ParameterID.epCardNumberHash:
            myCardNumberHash = ParamValue
        Case Else
            myErrorMessage = "Unknown Output TRecord V3 parameter."
    End Select
End Sub

Private Function ModuleNameStub() As String

    ModuleNameStub = AppNameStub & ModuleName & "."
End Function
