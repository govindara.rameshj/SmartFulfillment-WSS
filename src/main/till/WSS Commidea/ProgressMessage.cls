VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cOutputProgressMessage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Const ModuleName As String = "cOutputProgressMessage"

Public Type ProgressMessageParameter
    Name As String
    Value As String
End Type

Private Enum ParameterID
    epResult
    epTerminateLoop
    epStatusID
    epStatus
    epParameters
End Enum

Public Enum ProgressStatusID
    esiProcessingTxn
    esiWaitingForGratuity
    esiGratuityBeingEntered
    esiPresentCard
    esiSwipeCard
    esiCardInserted
    esiCardRemoved
    esiCardProcessing
    esiChangeCard
    esiContactTxnRequired
    esiKeyInCardDetails
    esiWaitingForCashback
    esiPinEntry
    esiRiskManagementComplete
    esiAuthorisingTxn
    esiWaitingForResult
    esiAuthResultReceived
    esiPrintingReceipt
    esiSignatureConfirmationRequired
    esiContinueRequired
    esiConfirmAuthCode
    esiConfirmingTxn
    esiRejectingTxn
    esiFinalResultReceived
    esiVoiceReferral
    esiRemoveCard
    esiAuthResultError
    esiFallbackToSwipeDisabled
    esiDownloadingFile
    esiUpdatingPED
    esiInvalidPEDConfig
    esiCardDataRetrieval
    esiStartingTransaction
    esiPerformingDownload
    esiRequestingReport
    esiGratuitySelectionRequired
    esiExpiryDateRequired
    esiStartDateRequired
    esiIssueNumberRequired
    esiAVSHouseNumberRequired
    esiAVSPostCodeRequired
    esiCSCRequired
    esiCustomerPresentNotPresentSelectionRequired
    esiCustomerNotPresentOptionSelectionRequired
    esiEnterChargeAuthCode
    esiLoginRequired
    esiReady
    esiCardNotAccepted
    esiCardBlocked
    esiTransactionCancelled
    esiInvalidExpiry
    esiGrauityInvalid
    esiInvalidCard
    esiPrintingCustomerReceipt
    esiInitialisingPED
    esiPEDUnavailable
    esiCardAppSelection
    esiRetryDownload
    esiRestartAfterSoftwareUpdate
    esiRequestingDCC
    esiDCCCurrencyChoice
    esiCardholderDCCCurrencyChoice
    esiUnsafeDownload
    esiUnexpectedLogin
    esiStartBarclaysBonusTxn
    esiUpdateBarclaysBonusTxn
    esiCancelBarclaysBonusTxn
    esiConfirmGratuity
    esiRegisterForAccountOnFileDecision
    esiAwaitingTokenID
    esiServerConnectionFailed
End Enum

Public Enum ProgressMessageParameterType
    epmpTxnValue = 1
    epmpCashbackValue = 2
    epmpGratuityValue = 3
    epmpTotalAmount = 4
    epmpAuthCode = 5
    epmpVRTelNo = 6
    epmpCard = 7
    epmpExpiry = 8
    epmpMerchantID = 9
    epmpTerminalID = 10
    epmpFilename = 11
    epmpProgress = 12
    epmpSchemeID = 13
    epmpMerchantName = 14
End Enum

' Property fields
Private myResult As Integer
Private myTerminateLoop As Integer
Private myStatusID As String
Private myStatus As String
Private myMessageParameters As Collection
' End of property fields

Private myParameters As Collection

' Public methods

Public Sub Initialise()

    Set myParameters = New Collection
    Call BuildDataArray
End Sub
' End of Public methods

' Properties

' Result
' This indicates the result code (100)
Public Property Get Result() As Integer

    Result = myResult
End Property

Public Property Let Result(ByVal newResult As Integer)
    
    myResult = newResult
End Property
    
' 2 Terminate loop
' Reserved - ignore
Public Property Get TerminateLoop() As Integer

    TerminateLoop = myTerminateLoop
End Property

Public Property Let TerminateLoop(ByVal NewTerminateLoop As Integer)
    
    myTerminateLoop = NewTerminateLoop
End Property

' 3 Status ID - indicates the progress status.
Public Property Get StatusID() As ProgressStatusID

    StatusID = myStatusID
End Property

Public Property Let StatusID(ByVal NewStatusID As ProgressStatusID)
    
    myStatusID = NewStatusID
End Property

' 4 Status
' A text representation of the progress status
Public Property Get Status() As String

    Status = myStatus
End Property

Public Property Let Status(ByVal NewStatus As String)
    
    myStatus = NewStatus
End Property

' 5 Parameters
' This field will contain any information that the PoS may require that is
' associated with that status. Each parameter will be ';' delimited and will
' be defined as follows:
' <Name>=<Value>
' Here is a list of all possible parameters:
'   TXN Value; CASHBACK Value; GRATUITY Value;
'   TOTAL AMOUNT; AUTH CODE; VR TEL NO; CARD;'
'   EXPIRY; MID; TID; FileName; PROGRESS
' Stored as a Collection of ProgressMessageParameters
Public Property Get Parameters() As Collection

    Set Parameters = myMessageParameters
End Property

Public Property Let Parameters(ByVal NewParameters As Collection)

    Set myMessageParameters = NewParameters
End Property

' Non parameter based properties

Public Property Get ValidProgressMessageParameters() As String()
    ReDim VPMP(1 To 14) As String
    
    VPMP(1) = "TXN VALUE"
    VPMP(2) = "CASHBACK VALUE"
    VPMP(3) = "GRATUITY VALUE"
    VPMP(4) = "TOTAL AMOUNT"
    VPMP(5) = "AUTH CODE"
    VPMP(6) = "VR TEL NO"
    VPMP(7) = "CARD"
    VPMP(8) = "EXPIRY"
    VPMP(9) = "MID"
    VPMP(10) = "TID"
    VPMP(11) = "FILENAME"
    VPMP(12) = "PROGRESS"
    VPMP(11) = "SCHEMEID"
    VPMP(12) = "MERCHANT NAME"
    ValidProgressMessageParameters = VPMP
End Property

 ' This is used to parse the output record.
' It is the number of returned parameters from Commidea
Public Property Get NumberOfParameters() As Integer

    NumberOfParameters = 5
End Property

Public Property Get CustomerFacingMessage() As String

    ' NB these to come from xml file if it exists
    Select Case StatusID
        Case 0, 5, 6, 14, 15, 16, 17, 18, 21, 23, 32, 34, 53, 54, 59
            CustomerFacingMessage = "Please Wait"
        Case 1
            CustomerFacingMessage = "Enter Gratuity"
        Case 3
            CustomerFacingMessage = "Present/Insert Card"
        Case 4
            CustomerFacingMessage = "Swipe/Remove Card"
        Case 7
            CustomerFacingMessage = "Do not remove card"
        Case 8
            CustomerFacingMessage = "Use alternative payment method"
        Case 9
            CustomerFacingMessage = "Insert card"
        Case 10
            CustomerFacingMessage = "Key card number"
        Case 11
            CustomerFacingMessage = "Enter cashback"
        Case 12
            CustomerFacingMessage = "Enter PIN"
        Case 20
            CustomerFacingMessage = "Voice Referral"    ' Display all info on screen to allow referral - Merchant Number, Correct Bank Tel No. and Txn details
        Case 22
            CustomerFacingMessage = "Declined"
        Case 24
            CustomerFacingMessage = "Referral"
        Case 25
            CustomerFacingMessage = "Remove card"
        Case 28, 29, 33, 83
            CustomerFacingMessage = "Loading"
        Case 47, 80
            CustomerFacingMessage = "Not accepted"
        Case 36
            CustomerFacingMessage = "Expires MM/YY"
        Case 37
            CustomerFacingMessage = "Valid from MM/YY"
        Case 38
            CustomerFacingMessage = "Issue number"
        Case 39
            CustomerFacingMessage = "Enter house number"
        Case 40
            CustomerFacingMessage = "Enter postcode"
        Case 41
            CustomerFacingMessage = "Enter card security code"
        Case 42
            CustomerFacingMessage = "Customer present? Yes/No"
        Case 44
            CustomerFacingMessage = "Enter Charge Auth code"
        Case 46
            CustomerFacingMessage = "Ready"
        Case 49
            CustomerFacingMessage = "Transaction void"
        Case 56
            CustomerFacingMessage = "Select payment type"
        Case 61
            CustomerFacingMessage = "Please select currency"    ' include currency options
        Case 79
            CustomerFacingMessage = "Invalid Amount"
        Case Else
            CustomerFacingMessage = ""
    End Select
End Property

Public Property Get OperatorMessage() As String
    Dim Param As Integer

    ' NB these to come from xml file if it exists
    Select Case StatusID
        Case 0
            OperatorMessage = "Processing Transaction"
        Case 1
            OperatorMessage = "Waiting For Gratuity"
        Case 2
            OperatorMessage = "Gratuity Being Entered"
        Case 3
            OperatorMessage = "Awaiting Card"
        Case 4
            OperatorMessage = "Swipe/Remove Card"
        Case 5
            OperatorMessage = "Card Inserted"
        Case 6
            OperatorMessage = "Card Removed"
        Case 7
            OperatorMessage = "Card Processing"
        Case 8
            OperatorMessage = "Change Card"
        Case 9
            OperatorMessage = "Contact Transaction Required"
        Case 10
            OperatorMessage = "Key in Card Details"
        Case 11
            OperatorMessage = "Waiting For Cashback"
        Case 12
            OperatorMessage = "PIN Entry"
        Case 13
            OperatorMessage = "Risk Management Complete"
        Case 14
            OperatorMessage = "Authorising Transaction"
        Case 15
            OperatorMessage = "Waiting For Result"
        Case 16
            OperatorMessage = "Authorisation Result Received"
        Case 17
            OperatorMessage = "Printing Merchant Receipt"
        Case 18
            OperatorMessage = "Signature Confirmation Required"
        Case 19
            OperatorMessage = "Continue Required"
        Case 20
            OperatorMessage = "Confirm Authorisation Code"
        Case 21
            OperatorMessage = "Confirming Transaction"
        Case 22
            OperatorMessage = "Rejecting Transaction"
        Case 23
            OperatorMessage = "Final Result Received"
        Case 24
            OperatorMessage = "Voice Referral"
        Case 25
            OperatorMessage = "Remove Card"
        Case 26
            OperatorMessage = "Authorisation Result Error"
        Case 27
            OperatorMessage = "Fallback to Swipe Disabled"
        Case 28
            OperatorMessage = "Downloading File"
        Case 29
            OperatorMessage = "Updating PED"
        Case 30
            OperatorMessage = "Invalid PED Configuration"
        Case 31
            OperatorMessage = "Card Data Retreival"
        Case 32
            OperatorMessage = "Starting Transaction"
        Case 33
            OperatorMessage = "Performing Download"
        Case 34
            OperatorMessage = "Requesting Report"
        Case 35
            OperatorMessage = "Gratuity Selection Required"
        Case 36
            OperatorMessage = "Expiry Date Required"
        Case 37
            OperatorMessage = "Start Date Required"
        Case 38
            OperatorMessage = "Issue Number Required"
        Case 39
            OperatorMessage = "AVS House Number Required"
        Case 40
            OperatorMessage = "AVS Postcode Required"
        Case 41
            OperatorMessage = "CSC Required"
        Case 42
            OperatorMessage = "Customer Present/Not Present Selection Required"
        Case 43
            OperatorMessage = "Customer Not Present Option Selection Required"
        Case 44
            OperatorMessage = "Enter Charge Autorisation Code"
        Case 45
            OperatorMessage = "Login required"
        Case 46
            OperatorMessage = "Ready"
        Case 47
            OperatorMessage = "Card Not Accepted"
        Case 48
            OperatorMessage = "Card Blocked"
        Case 49
            OperatorMessage = "Transaction Cancelled"
        Case 50
            OperatorMessage = "Invalid Expiry"
        Case 51
            OperatorMessage = "Gratuity Invalid"
        Case 52
            OperatorMessage = "Invalid Card"
        Case 53
            OperatorMessage = "Printing Customer Receipt"
        Case 54
            OperatorMessage = "Initialising PED"
        Case 55
            OperatorMessage = "PED Unavailable"
        Case 56
            OperatorMessage = "Card Application Selection"
        Case 57
            OperatorMessage = "Retry Download"
        Case 58
            OperatorMessage = "Restart After Software Update"
        Case 59
            OperatorMessage = "Requesting DCC"
        Case 60
            OperatorMessage = "DCC Currency Choice"
        Case 61
            OperatorMessage = "Cardholder DCC Currency Choice"
        Case 62
            OperatorMessage = "Unsafe Download"
        Case 63
            OperatorMessage = "Unexpected login"
        Case 64
            OperatorMessage = "Start Barclays Bonus Transaction"
        Case 65
            OperatorMessage = "Update Barclays Bonus Transaction"
        Case 66
            OperatorMessage = "Cancel Barclays Bonus Transaction"
        Case 67
            OperatorMessage = "Confirm Gratuity"
        Case 68
            OperatorMessage = "Register For Account on File Decision"
        Case 69
            OperatorMessage = "Awaiting Token ID"
        Case 70
            OperatorMessage = "Barclays Bonus Discount Summary"
        Case 71
            OperatorMessage = "Barclays Bonus Use Bonus"
        Case 72
            OperatorMessage = "Barclays Bonus Enter Redemption"
        Case 73
            OperatorMessage = "Barclays Bonus Not Available"
        Case 74
            OperatorMessage = "Download Complete"
        Case 75
            OperatorMessage = "Download Still Being Prepared"
        Case 76
            OperatorMessage = "Server Connection Failed"
        Case 77
            OperatorMessage = "Resume Download"
        Case 78
            OperatorMessage = "Paypoint Account Extraction Failed"
        Case 79
            OperatorMessage = "Paypoint Amount Outside Allowed Range"
        Case 80
            OperatorMessage = "Paypoint Card Expired"
        Case 81
            OperatorMessage = "Paypoint Initialised"
        Case 82
            OperatorMessage = "Paypoint Initialisation Failed"
        Case 83
            OperatorMessage = "Paypoint Initialising"
        Case 84
            OperatorMessage = "Paypoint Invalid Account"
        Case 85
            OperatorMessage = "Paypoint Invalid Amount"
        Case 86
            OperatorMessage = "Paypoint Invalid Capture Method"
        Case 87
            OperatorMessage = "Paypoint Invalid Card Number"
        Case 88
            OperatorMessage = "Paypoint Invalid Configuration"
        Case 89
            OperatorMessage = "Paypoint Invalid Denomination"
        Case 90
            OperatorMessage = "Paypoint Invalid Expiry Date"
        Case 91
            OperatorMessage = "Paypoint Invalid Scheme"
        Case 92
            OperatorMessage = "Paypoint Invalid Scheme Option"
        Case 93
            OperatorMessage = "Paypoint Invalid Top-up Type"
        Case 94
            OperatorMessage = "Paypoint Invalid Service"
        Case Else
            OperatorMessage = ""
    End Select
End Property
'
'Public Property Get StatusType() As String
'
'    Select Case StatusID
'        Case 0, 5, 6, 7, 14, 15, 16, 17, 18, 21, 23, 28, 29, 31, 32, 33, 34, 46, 53, 54
'            StatusType = "Info"
'        Case 1, 3, 4, 9, 10, 11, 12, 20, 24, 25, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 56, 67
'            StatusType = "Action"
'        Case 2, 13, 19, 30, 45, 55, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 68, 69, 70
'            StatusType = "None"
'        Case 8, 22, 27, 47, 48, 49, 50
'            StatusType = "Finish"
'        Case 26, 51, 52
'            StatusType = "Error"
'    End Select
'End Property

Public Property Get ParametersMessage() As String
    Dim Param As Integer
    
    If Not Parameters Is Nothing Then
        With Parameters
            For Param = 1 To .Count
                With .Item(Param)
                    Select Case .Name
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpTxnValue)
                            ParametersMessage = vbNewLine & "Transaction value: " & .Value
                        Case ValidProgressMessageParameters()(epmpCashbackValue)
                            If CDbl(.Value) <> 0# Then
                                ParametersMessage = vbNewLine & "Cashback value: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpGratuityValue)
                            If CDbl(.Value) <> 0# Then
                                ParametersMessage = vbNewLine & "Gratuity value: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpTotalAmount)
                            If CDbl(.Value) <> 0# Then
                                ParametersMessage = vbNewLine & "Total value: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpAuthCode)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Authorisation Code: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpVRTelNo)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Voice Referral Tel. No.: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpCard)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Card Application: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpExpiry)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Expiry Date: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpMerchantID)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Machine ID: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpTerminalID)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Terminal ID: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpFilename)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Filename: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpProgress)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Progress: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpSchemeID)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Scheme ID: " & .Value
                            End If
                        Case ValidProgressMessageParameters()(ProgressMessageParameterType.epmpMerchantName)
                            If .Value <> "" Then
                                ParametersMessage = vbNewLine & "Merchant Name: " & .Value
                            End If
                        Case Else
                    End Select
                End With
            Next Param
        End With
    End If
    ' Remove preceding vbNewLine
    If Len(ParametersMessage) > Len(vbNewLine) Then
        ParametersMessage = Mid(ParametersMessage, Len(vbNewLine) + 1)
    End If
End Property
' End of Properties

' Private methods

Private Sub CreateParameterInfoAndAddtoArray(ByVal Name As String, ByVal PropertyName As String, ByVal ParamID As ParameterID, ByVal ParamType As ParamValueType)
    Dim ParamInfo As cParameterInfo
    
    Set ParamInfo = New cParameterInfo
    Call ParamInfo.Initialise(Name, PropertyName, ParamID, ParamType)
    Call myParameters.Add(ParamInfo, Key:=Trim(CStr(ParamID)))
End Sub

Private Sub BuildDataArray()
    
    Call CreateParameterInfoAndAddtoArray("Result", "Result", ParameterID.epResult, ParamValueType.epvtInteger)
    Call CreateParameterInfoAndAddtoArray("TerminateLoop", "TerminateLoop", ParameterID.epTerminateLoop, ParamValueType.epvtInteger)
    Call CreateParameterInfoAndAddtoArray("StatusID", "StatusID", ParameterID.epStatusID, ParamValueType.epvtInteger)
    Call CreateParameterInfoAndAddtoArray("Status", "Status", ParameterID.epStatus, ParamValueType.epvtString)
    Call CreateParameterInfoAndAddtoArray("Parameters", "Parameters", ParameterID.epParameters, ParamValueType.epvtString)
End Sub

' this method will use the comma as a delimiter and populate the values above
Friend Function ParseReceivedMessage(ByVal Message As String) As Boolean
    Dim MessageLen As Integer
    Dim ParamValues() As String
    Dim Delimiter As String
    Dim Param As Integer

On Error GoTo Catch

Try:
    ' if the transaction fails then return. Success is 0
    MessageLen = Len(Message)
    If MessageLen > 0 Then
        '  define which character is seperating fields
        Delimiter = ","
        ParamValues = Split(Message, Delimiter)
        For Param = 0 To NumberOfParameters - 1
            ' only bother setting the rtn fields if there is a value
            If Len(ParamValues(Param)) > 0 Then
                Call SetPropertyFromParameterValue(ParamValues(Param), Param)
            End If
        Next Param
    End If

    ParseReceivedMessage = True

    Exit Function

Catch:
    Call Err.Raise(vbObjectError + 1, ModuleNameStub & "ParseReceivedMessage", "Failed to parse output Progress Message.")
End Function

Private Sub SetPropertyFromParameterValue(ByVal ParamValue As String, ByVal ParamID As Integer)
    Dim TempInt As Integer
    Dim Delimiter As String
    Dim MessParams() As String
    Dim MessParam As Integer
    Dim Param() As String
    Dim MessParameter As ProgressMessageParameter
    Dim ValidProgressMessParams As String
    Dim IsError As Boolean
    
    IsError = True
    Select Case ParamID
        Case ParameterID.epResult:
            If ParamToInt(TempInt, ParamValue) Then
                Result = TempInt
                IsError = False
            End If
        Case ParameterID.epTerminateLoop:
            If ParamToInt(TempInt, ParamValue) Then
                TerminateLoop = TempInt
                IsError = False
            End If
        Case ParameterID.epStatusID:
            If ParamToInt(TempInt, ParamValue) Then
                If TempInt >= ProgressStatusID.esiProcessingTxn _
                And TempInt <= ProgressStatusID.esiServerConnectionFailed Then
                    StatusID = TempInt
                    IsError = False
                End If
            End If
        Case ParameterID.epStatus:
            Status = ParamValue
            IsError = False
        Case ParameterID.epParameters
            Set myMessageParameters = New Collection
            If Len(ParamValue) > 0 Then
                Delimiter = ";"
                MessParams = Split(ParamValue, Delimiter)
                ValidProgressMessParams = Join(ValidProgressMessageParameters, Delimiter)
                Delimiter = "="
                IsError = False
                For MessParam = 0 To UBound(MessParams) - 1
                    Param() = Split(MessParams(MessParam), Delimiter)
                    If InStr(1, ValidProgressMessParams, Param(0), vbTextCompare) > 0 Then
                        MessParameter.Name = Param(0)
                        MessParameter.Value = Param(1)
                        Call myMessageParameters.Add(MessParameter, Key:=MessParameter.Name)
                    Else
                        IsError = True
                    End If
                Next MessParam
            End If
        Case Else
    End Select
    If IsError Then
        Call Err.Raise(vbObjectError + 1, ModuleNameStub & "SetPropertyFromParameterValue", myParameters(ParamID).ConversionErrorMess)
    End If
End Sub

Private Function ModuleNameStub() As String

    ModuleNameStub = AppNameStub & ModuleName & "."
End Function

