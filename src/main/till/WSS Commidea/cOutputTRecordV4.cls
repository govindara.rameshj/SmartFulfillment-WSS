VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cOutputTRecordV4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Implements IAuthorizationDetails

Private Const ModuleName As String = "cOutputTRecordV4"

Private Enum ParameterID
    epVGISReference = 33
End Enum

' Property fields
Private myVGISReference As String
' End of property fields

Private myOutputTRecordV3 As cOutputTRecordV3
Private myReceivedMessage As String
Private myParameters As Collection
Private myParameterTitles() As String
Private myParameterColumnNames() As String
Private myUnderstood As Boolean
Private myErrorMessage As String

' Public methods

Public Sub Initialise(Optional ByVal IsScreenless As Boolean = True)

    Set myOutputTRecordV3 = New cOutputTRecordV3
    Call myOutputTRecordV3.Initialise(IsScreenless)
    Set myParameters = New Collection
    Call BuildDataArray
End Sub
' End of Public methods

' Properties


'        1. Transaction result:
'           '0' - Completed
'           '7' - Declined
'           '-nn' - All other negative values are
'           used to define error conditions
'           Appendix B contains a full list of error
'           codes and messages.
'           Screenless transaction results:
'           '0' - Completed
'           '2' - Referred
'           '5' - Declined
'           '6' - Authorised
'           '7' - Reversed
'           '-nn' - Negative values are used to
'           define error conditions
Public Property Get TResult() As Integer

    TResult = myOutputTRecordV3.TResult
End Property

Public Function IAuthorizationDetails_IsAuthorized() As Boolean
    IAuthorizationDetails_IsAuthorized = (TResult = etrnesAuthorised) Or (TResult = etrnesCompleted)
End Function

    ' 2. Teminate Loop Reserved, Ignore
Public Property Get TerminateLoop() As Integer

    TerminateLoop = myOutputTRecordV3.TerminateLoop
End Property

'  3. Values will be truncated to the correct
'    number of decimal places
'    required for the transaction currency.
'    For example:
'    1.23 = 1 (one Japanese Yen)
'    Field will show total that will be debited
Public Property Get TotalTranValueProcessed() As Double

    TotalTranValueProcessed = myOutputTRecordV3.TotalTranValueProcessed
End Property

' 4 Cashback Value Double As above
Public Property Get CashBackValue() As Double

    CashBackValue = myOutputTRecordV3.CashBackValue
End Property
   
' 5 Gratuity Value Double As above
Public Property Get GratuityValue() As Double

    GratuityValue = myOutputTRecordV3.GratuityValue
End Property
    
' 6 PAN Integer The Primary Account Number
' (Card Number).
' Please note: This value will not be
' returned in full due to PCI requirements.
' The PAN will be masked apart from the
' last four digits, e.g. '************1234'
Public Property Get IAuthorizationDetails_PAN() As String

    IAuthorizationDetails_PAN = myOutputTRecordV3.PAN
End Property

' Expiry Date MMYY Integer Card Expiry Month and Year.
Public Property Get IAuthorizationDetails_ExpiryDate() As String

    IAuthorizationDetails_ExpiryDate = myOutputTRecordV3.ExpiryDate
End Property

' 8 Issue Number Integer Card Issue Number. Blank when scheme
' does not provide an issue number.
Public Property Get IAuthorizationDetails_IssueNumber() As String

    IAuthorizationDetails_IssueNumber = myOutputTRecordV3.IssueNumber
End Property

' 9 Start MMYY Integer Card start month and year
Public Property Get IAuthorizationDetails_StartDate() As String

    IAuthorizationDetails_StartDate = myOutputTRecordV3.StartDate
End Property

' 10 Transaction Date / Time Integer CCYYMMDDHHMMSS
Public Property Get IAuthorizationDetails_TransactionDate() As String
    IAuthorizationDetails_TransactionDate = myOutputTRecordV3.TransactionDate
End Property

Public Property Get IAuthorizationDetails_TransactionAmount() As Double
    IAuthorizationDetails_TransactionAmount = myOutputTRecordV3.TotalTranValueProcessed ' TODO
End Property

' 11 Merchant Number Integer The Merchant Number for the given' card scheme and account.
Public Property Get MerchantNumber() As String
    MerchantNumber = myOutputTRecordV3.MerchantNumber
End Property

Public Property Get IAuthorizationDetails_MerchantNumber() As String
    IAuthorizationDetails_MerchantNumber = MerchantNumber
End Property

' 12 Terminal ID Integer Terminal ID used for this transaction.
Public Property Get TerminalID() As String
    TerminalID = myOutputTRecordV3.TerminalID
End Property

Public Property Get IAuthorizationDetails_TerminalID() As String
    IAuthorizationDetails_TerminalID = TerminalID
End Property

' 13 Scheme Name String Card scheme name e.g. visa etc
Public Property Get IAuthorizationDetails_SchemeName() As String

    IAuthorizationDetails_SchemeName = myOutputTRecordV3.SchemeName
End Property

' 14 Floor Limit Integer Floor limit for the card scheme/account.
Public Property Get FloorLimit() As Double

    FloorLimit = myOutputTRecordV3.FloorLimit
End Property

' 15 EFT Sequence Number Integer Four digits in the range 0001 - 9999.
' (Prefixed with "OL" when offline)
Public Property Get EFTSequenceNumber() As String
    If Mid(myOutputTRecordV3.EFTSequenceNumber, 1, 2) = "OL" Then
        EFTSequenceNumber = Mid(myOutputTRecordV3.EFTSequenceNumber, 3)
    Else
        EFTSequenceNumber = myOutputTRecordV3.EFTSequenceNumber
    End If
End Property

Public Property Get IAuthorizationDetails_EFTSequenceNumber() As String
    IAuthorizationDetails_EFTSequenceNumber = EFTSequenceNumber
End Property

' 16 Authorisation Code String Blank if the transaction is declined or is
' below the floor limit.
Public Property Get IAuthorizationDetails_AuthorisationCode() As String

    IAuthorizationDetails_AuthorisationCode = myOutputTRecordV3.AuthorisationCode
End Property

' 17 Referral Telephone Number. Reserved, ignore
Public Property Get ReferralTelephoneNumber() As String

    ReferralTelephoneNumber = myOutputTRecordV3.ReferralTelephoneNumber
End Property

' 18 Customer Verification Method /
' Authorisation Message / Error Message
' / Status Message
' String As returned by communications process.
' Normally direct from acquirer. Also
' contains status message if enabled
Public Property Get CustomerVerification() As String

    CustomerVerification = myOutputTRecordV3.CustomerVerification
End Property

Public Property Get IAuthorizationDetails_CustomerVerification() As String
    IAuthorizationDetails_CustomerVerification = CustomerVerification
End Property

' 19 Capture Method String Valid values are:
' Contactless
' Swipe
' ICC
' Keyed
Public Property Get CaptureMethod() As String
    CaptureMethod = myOutputTRecordV3.CaptureMethod
End Property

Public Property Get IAuthorizationDetails_CaptureMethod() As String
    IAuthorizationDetails_CaptureMethod = CaptureMethod
End Property

' 20 Transaction Currency Code Reserved for Dynamic Currency Conversion
Public Property Get TransactionCurrencyCode() As Integer

    TransactionCurrencyCode = myOutputTRecordV3.TransactionCurrencyCode
End Property

' 21 Original Transaction Value Reserved for Dynamic Currency Conversion
Public Property Get OriginalTransactionValue() As Integer

    OriginalTransactionValue = myOutputTRecordV3.OriginalTransactionValue
End Property

' 22 Original Cashback Value Reserved for Dynamic Currency Conversion
Public Property Get OriginalCashbackValue() As Integer

    OriginalCashbackValue = myOutputTRecordV3.OriginalCashbackValue
End Property

' 23 Original Gratuity Value Reserved for Dynamic Currency Conversion
Public Property Get OriginalGratuityValue() As Integer

    OriginalGratuityValue = myOutputTRecordV3.OriginalGratuityValue
End Property

' 24 Original Transaction Currency
' Code
' Reserved for Dynamic Currency Conversion
Public Property Get OriginalTransactionCurrencyCode() As Integer

    OriginalTransactionCurrencyCode = myOutputTRecordV3.OriginalTransactionCurrencyCode
End Property

' 25 Barclays Bonus Discount Value Reserved for Barclays Bonus
Public Property Get BarclaysBonusDiscountValue() As Integer

    BarclaysBonusDiscountValue = myOutputTRecordV3.BarclaysBonusDiscountValue
End Property

' 26 Barclays Bonus Redemption Value Reserved for Barclays Bonus
Public Property Get BarclaysBonusRedemptionValue() As Integer

    BarclaysBonusRedemptionValue = myOutputTRecordV3.BarclaysBonusRedemptionValue
End Property

' 27 Account on File Registration
' Result
    ' Integer This is the result of the Account on File
    ' registration. Valid values are:
    '0' - Not Set
    '1' - Performed
    '2' - Success
    '3' - Failed
Public Property Get AccountonFileRegistration() As Integer

    AccountonFileRegistration = myOutputTRecordV3.AccountonFileRegistration
End Property

' 28 Token ID String This is the token allocated to the payment
' details as part of the Account on File
' registration process or the token used for
' the Account on File payment.
Public Property Get IAuthorizationDetails_TokenID() As String

    IAuthorizationDetails_TokenID = myOutputTRecordV3.TokenID
End Property

' 29 AVS Post Code Result Integer This is the result of any AVS post code
' checking. Valid values are:
' '0' - Not Provided
' '1' - Not Checked
' '2' - Matched
' '4' - Not Matched
' '8' - Reserved
Public Property Get AVSPostCode() As Integer

    AVSPostCode = myOutputTRecordV3.AVSPostCode
End Property

' 30 AVS House Number Result Integer This is the result of any AVS house number
' checking. Valid values are:
' '0' - Not Provided
' '1' - Not Checked
' '2' - Matched
' '4' - Not Matched
' '8' - Reserved
Public Property Get AVSHouseNumber() As Integer

    AVSHouseNumber = myOutputTRecordV3.AVSHouseNumber
End Property

' 31 CSC Result Integer This is the result of any CSC verification.
' Valid values are:
' '0' - Not Provided
' '1' - Not Checked
' '2' - Matched
' '4' - Not Matched
' '8' - Reserved
Public Property Get CSCResult() As Integer

    CSCResult = myOutputTRecordV3.CSCResult
End Property

' Hash of card number (PAN) generated by the WinTI3 infrastructure,
' only available for txns that have been sent for online for authorisation
Public Property Get IAuthorizationDetails_CardNumberHash() As String

    IAuthorizationDetails_CardNumberHash = myOutputTRecordV3.CardNumberHash
End Property

'33
' The VGIS reference when a VGIS txn has been processed
Public Property Get VGISReference() As String

    VGISReference = myVGISReference
End Property

' Non Parameter based properties

Public Property Get CaptureMethods(ByVal MethodType As CaptureMethodType)
    
    CaptureMethods = myOutputTRecordV3.CaptureMethods(MethodType)
End Property

Public Property Get ErrorAction() As String

    ErrorAction = myOutputTRecordV3.ErrorAction
End Property

Public Property Get ErrorDescription() As String

    ErrorDescription = myOutputTRecordV3.ErrorDescription
End Property

Public Property Get ErrorMessage() As String

    ErrorMessage = myOutputTRecordV3.ErrorMessage
End Property

Public Property Get OutputTRecordV3() As cOutputTRecordV3

    Set OutputTRecordV3 = myOutputTRecordV3
End Property

Public Property Get Screenless() As Boolean

    Screenless = myOutputTRecordV3.Screenless
End Property

 ' This is used to parse the output record.
' It is the number of returned parameters from Commidea
Public Property Get NumberOfParameters() As Integer

    NumberOfParameters = 33
End Property

Public Property Get ResultMessage() As String

    ResultMessage = myOutputTRecordV3.ResultMessage
End Property
    
Public Property Get Successful() As Boolean

    Successful = myOutputTRecordV3.Successful
End Property

Public Property Get Understood() As Boolean

    Understood = myUnderstood
End Property
' End of Non Parameter based properties

' End of Properties

' Friend methods

Friend Function IAuthorizationDetails_ParseReceivedMessage(ByVal Message As String) As Boolean

On Error GoTo Catch

Try:
    Clean
    ' 1st half of the Output record is the same basic structure as the cOutoutTRecord, so use that
    If myOutputTRecordV3.ParseReceivedMessage(Message) Then
        ' Now finish off parsing the rest of the message locally
        If LocalParseReceivedMessage(Message) Then
            IAuthorizationDetails_ParseReceivedMessage = True
        End If
    End If

    myUnderstood = True
    IAuthorizationDetails_ParseReceivedMessage = True

    Exit Function
Catch:
    Call Err.Raise(vbObjectError + 1, ModuleNameStub & "ParseReceivedMessage", "Failed to Parse output Transaction Record.")
End Function

Friend Function ParseReceivedMessage(ByVal Message As String) As Boolean
    ParseReceivedMessage = IAuthorizationDetails_ParseReceivedMessage(Message)
End Function

' End of friend methods

' Private methods

Private Sub Clean()
    
    myVGISReference = ""
    myUnderstood = False
End Sub

Private Sub CreateParameterInfoAndAddtoArray(ByVal Name As String, ByVal PropertyName As String, ByVal ParamID As ParameterID, ByVal ParamType As ParamValueType)
    Dim ParamInfo As cParameterInfo
    
    Set ParamInfo = New cParameterInfo
    Call ParamInfo.Initialise(Name, PropertyName, ParamID, ParamType)
    Call myParameters.Add(ParamInfo, Key:=Trim(CStr(ParamID)))
End Sub

Private Sub BuildDataArray()
    
    Call CreateParameterInfoAndAddtoArray("VGISReference", "VGISReference", ParameterID.epVGISReference, ParamValueType.epvtString)
End Sub

Private Function LocalParseReceivedMessage(ByVal Message As String) As Boolean
    Dim MessageLen As Integer
    ReDim ParamValues(1 To Me.NumberOfParameters) As String
    Dim Delimiter As String
    Dim Param As Integer

On Error GoTo Catch

Try:
    Delimiter = ","
    ParamValues = SplitTo1BasedStringArray(Message, Delimiter)
    ' Even though specify V4, if an error will only get original version format,
    ' ie no parameters 18 - 33.
    If UBound(ParamValues) > myOutputTRecordV3.NumberOfParameters Then
        For Param = myOutputTRecordV3.NumberOfParameters To Me.NumberOfParameters
            ' only bother setting the rtn fields if there is a value
            If Len(ParamValues(Param)) > 0 Then
                Call SetPropertyFromParameterValue(ParamValues(Param), Param)
            End If
        Next Param
    End If

    myUnderstood = myOutputTRecordV3.Understood
    LocalParseReceivedMessage = True

    Exit Function
Catch:
    Call Err.Raise(vbObjectError + 1, ModuleNameStub & "LocalParseReceivedMessage", "Failed to parse output Transaction Record V2.")
End Function

Private Sub SetPropertyFromParameterValue(ByVal ParamValue As String, ByVal ParamID As ParameterID)

    Select Case ParamID
        Case ParameterID.epVGISReference:
            myVGISReference = ParamValue
        Case Else
            myErrorMessage = "Unknown Output TRecord V4 parameter."
    End Select
End Sub

Private Function ModuleNameStub() As String

    ModuleNameStub = AppNameStub & ModuleName & "."
End Function

