VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CommideaConfiguration"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mSoftwareVersion As String
Private mPathToOciusSentinel As String
Private mTillID As String
    
' Default is 25000 but just in case want to change that with parameters
Private mIntegrationPort As String
    
' Ditto 25001
Private mProgressPort As String
    
' Length of time in seconds to keep trying to login before timing out and failing
Private mLoginTimeout As Integer
    
' Length of time (seconds) allowed before stop trying
' to connect sockets (before an integration message is
' sent to Ocius Sentinel/PED)
Private mConnectionTimeout As Integer
    
Private mShowKeypadIcon As Boolean
Private mBackgroundColour As Long
Private mEditColour As Long
Private mMsgBoxPromptColour As Long
Private mMsgBoxWarningColour As Long
Private mAlreadyLoggedIn As Boolean
Private mCustRecPath As String
Private mCustRecFilename As String
Private mMerRecPath As String
Private mMerRecFilename As String
Private mLastValidCashierID As String
Private mUnderDuressKeyCode As Integer
Private mCurrencyCharacter As String
Private mUseSeparateReceipt As Boolean
Private mReceiptHeaderLastLine As String
Private mReceiptFooterFirstLine As String

' Referral 818 - name on licence for compaison when completing a ped software install automatically.
Private mLicenceMerchantName As String

' Referral 1093 - Use modal frmTransaction to prevent user interaction with till form during processing of txn
Private mUseModalAuthorisation As Boolean

Private mPrintConfirmTimeout As Integer
Private mPartialGCPaymentTimeout As Integer

' Must be OPOSPOSPrinter, but get continual bin compatability issues on building
Private mPrinterInfo As Object

Public Property Get SoftwareVersion() As String
    SoftwareVersion = mSoftwareVersion
End Property

Public Property Let SoftwareVersion(ByVal newSoftwareVersion As String)
    mSoftwareVersion = newSoftwareVersion
End Property

Public Property Get PathToOciusSentinel() As String
    PathToOciusSentinel = mPathToOciusSentinel
End Property

Public Property Let PathToOciusSentinel(ByVal newPathToOciusSentinel As String)
    mPathToOciusSentinel = newPathToOciusSentinel
End Property

Public Property Get TillID() As String
    TillID = mTillID
End Property

Public Property Let TillID(ByVal newTillID As String)
    mTillID = newTillID
End Property
    
Public Property Get IntegrationPort() As String
    IntegrationPort = mIntegrationPort
End Property

Public Property Let IntegrationPort(ByVal newIntegrationPort As String)
    mIntegrationPort = newIntegrationPort
End Property
    
Public Property Get ProgressPort() As String
    ProgressPort = mProgressPort
End Property

Public Property Let ProgressPort(ByVal newProgressPort As String)
    mProgressPort = newProgressPort
End Property
    
Public Property Get LoginTimeout() As Integer
    LoginTimeout = mLoginTimeout
End Property

Public Property Let LoginTimeout(ByVal newLoginTimeout As Integer)
    mLoginTimeout = newLoginTimeout
End Property

Public Property Get ConnectionTimeout() As Integer
    ConnectionTimeout = mConnectionTimeout
End Property

Public Property Let ConnectionTimeout(ByVal newLoginTimeout As Integer)
    mConnectionTimeout = newLoginTimeout
End Property
    
    
Public Property Get ShowKeypadIcon() As Boolean
    ShowKeypadIcon = mShowKeypadIcon
End Property

Public Property Let ShowKeypadIcon(ByVal newShowKeypadIcon As Boolean)
    mShowKeypadIcon = newShowKeypadIcon
End Property
    
    
Public Property Get BackgroundColour() As Long
    BackgroundColour = mBackgroundColour
End Property

Public Property Let BackgroundColour(ByVal newBackgroundColour As Long)
    mBackgroundColour = newBackgroundColour
End Property
    
    
Public Property Get EditColour() As Long
    EditColour = mEditColour
End Property

Public Property Let EditColour(ByVal newEditColour As Long)
    mEditColour = newEditColour
End Property
    
    
Public Property Get MsgBoxWarningColour() As Long
    MsgBoxWarningColour = mMsgBoxWarningColour
End Property

Public Property Let MsgBoxWarningColour(ByVal newMsgBoxWarningColour As Long)
    mMsgBoxWarningColour = newMsgBoxWarningColour
End Property
    
    
Public Property Get MsgBoxPromptColour() As Long
    MsgBoxPromptColour = mMsgBoxPromptColour
End Property

Public Property Let MsgBoxPromptColour(ByVal newMsgBoxPromptColour As Long)
    mMsgBoxPromptColour = newMsgBoxPromptColour
End Property
    
    
Public Property Get AlreadyLoggedIn() As Boolean
    AlreadyLoggedIn = mAlreadyLoggedIn
End Property

Public Property Let AlreadyLoggedIn(ByVal newAlreadyLoggedIn As Boolean)
    mAlreadyLoggedIn = newAlreadyLoggedIn
End Property
    
    
Public Property Get CustRecPath() As String
    CustRecPath = mCustRecPath
End Property

Public Property Let CustRecPath(ByVal newCustRecPath As String)
    mCustRecPath = newCustRecPath
End Property
    
    
Public Property Get CustRecFilename() As String
    CustRecFilename = mCustRecFilename
End Property

Public Property Let CustRecFilename(ByVal newCustRecFilename As String)
    mCustRecFilename = newCustRecFilename
End Property
    
    
Public Property Get MerRecPath() As String
    MerRecPath = mMerRecPath
End Property

Public Property Let MerRecPath(ByVal newMerRecPath As String)
    mMerRecPath = newMerRecPath
End Property
    
    
Public Property Get MerRecFilename() As String
    MerRecFilename = mMerRecFilename
End Property

Public Property Let MerRecFilename(ByVal newMerRecFilename As String)
    mMerRecFilename = newMerRecFilename
End Property
    
    
Public Property Get LastValidCashierID() As String
    LastValidCashierID = mLastValidCashierID
End Property

Public Property Let LastValidCashierID(ByVal newLastValidCashierID As String)
    mLastValidCashierID = newLastValidCashierID
End Property
    
    
Public Property Get UnderDuressKeyCode() As Integer
    UnderDuressKeyCode = mUnderDuressKeyCode
End Property

Public Property Let UnderDuressKeyCode(ByVal newUnderDuressKeyCode As Integer)
    mUnderDuressKeyCode = newUnderDuressKeyCode
End Property
    
    
Public Property Get CurrencyCharacter() As String
    CurrencyCharacter = mCurrencyCharacter
End Property

Public Property Let CurrencyCharacter(ByVal newCurrencyCharacter As String)
    mCurrencyCharacter = newCurrencyCharacter
End Property
    
    
Public Property Get UseSeparateReceipt() As Boolean
    UseSeparateReceipt = mUseSeparateReceipt
End Property

Public Property Let UseSeparateReceipt(ByVal newUseSeparateReceipt As Boolean)
    mUseSeparateReceipt = newUseSeparateReceipt
End Property
    
    
Public Property Get ReceiptHeaderLastLine() As String
    ReceiptHeaderLastLine = mReceiptHeaderLastLine
End Property

Public Property Let ReceiptHeaderLastLine(ByVal newReceiptHeaderLastLine As String)
    mReceiptHeaderLastLine = newReceiptHeaderLastLine
End Property
    
    
Public Property Get ReceiptFooterFirstLine() As String
    ReceiptFooterFirstLine = mReceiptFooterFirstLine
End Property

Public Property Let ReceiptFooterFirstLine(ByVal newReceiptFooterFirstLine As String)
    mReceiptFooterFirstLine = newReceiptFooterFirstLine
End Property
    
    
Public Property Get LicenceMerchantName() As String
    LicenceMerchantName = mLicenceMerchantName
End Property

Public Property Let LicenceMerchantName(ByVal newLicenceMerchantName As String)
    mLicenceMerchantName = newLicenceMerchantName
End Property
    
    
Public Property Get UseModalAuthorisation() As Boolean
    UseModalAuthorisation = mUseModalAuthorisation
End Property

Public Property Let UseModalAuthorisation(ByVal newUseModalAuthorisation As Boolean)
    mUseModalAuthorisation = newUseModalAuthorisation
End Property
   
    
Public Property Get PrintConfirmTimeout() As Integer
    PrintConfirmTimeout = mPrintConfirmTimeout
End Property

Public Property Let PrintConfirmTimeout(ByVal newPrintConfirmTimeout As Integer)
    mPrintConfirmTimeout = newPrintConfirmTimeout
End Property
    
    
Public Property Get PartialGCPaymentTimeout() As Integer
    PartialGCPaymentTimeout = mPartialGCPaymentTimeout
End Property

Public Property Let PartialGCPaymentTimeout(ByVal newPartialGCPaymentTimeout As Integer)
    mPartialGCPaymentTimeout = newPartialGCPaymentTimeout
End Property


Public Property Get PrinterInfo() As Object
    Set PrinterInfo = mPrinterInfo
End Property

Public Property Set PrinterInfo(ByVal newPrinterInfo As Object)
    Set mPrinterInfo = newPrinterInfo
End Property
