VERSION 5.00
Object = "{CCB90150-B81E-11D2-AB74-0040054C3719}#1.0#0"; "OPOSPOSPrinter.ocx"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "mswinsck.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Begin VB.Form frmTransaction 
   BackColor       =   &H80000009&
   Caption         =   "Card Transaction"
   ClientHeight    =   7125
   ClientLeft      =   2715
   ClientTop       =   1275
   ClientWidth     =   7395
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7125
   ScaleWidth      =   7395
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer tmrTimeout 
      Enabled         =   0   'False
      Interval        =   60000
      Left            =   0
      Top             =   0
   End
   Begin VB.Timer tmrAuthorise 
      Left            =   0
      Top             =   6360
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   6750
      Width           =   7395
      _ExtentX        =   13044
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmTransaction.frx":0000
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   5212
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   635
            MinWidth        =   635
            Picture         =   "frmTransaction.frx":0AA6
            Key             =   "NumPad"
            Object.ToolTipText     =   "Show/Hide Num Pad"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "16:44"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   780
      Top             =   5820
      Visible         =   0   'False
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin VB.Frame fraEntry 
      Height          =   6255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   7095
      Begin VB.PictureBox fraInsertCard 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   2535
         Left            =   10
         ScaleHeight     =   2505
         ScaleWidth      =   7035
         TabIndex        =   39
         Top             =   1200
         Visible         =   0   'False
         Width           =   7058
         Begin VB.Timer tmrESocket 
            Left            =   0
            Top             =   0
         End
         Begin VB.Label lblInsert 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1695
            Left            =   240
            TabIndex        =   40
            Top             =   120
            Width           =   6615
         End
      End
      Begin VB.PictureBox fraComplete 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   2295
         Left            =   0
         ScaleHeight     =   2265
         ScaleWidth      =   7065
         TabIndex        =   33
         Top             =   1200
         Visible         =   0   'False
         Width           =   7095
         Begin VB.CommandButton cmdExit 
            Caption         =   "EXIT"
            Height          =   495
            Left            =   2760
            TabIndex        =   36
            Top             =   1200
            Width           =   975
         End
         Begin VB.CommandButton cmdCancel 
            Caption         =   "&No"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   204
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   5160
            TabIndex        =   35
            Top             =   1320
            Width           =   1815
         End
         Begin VB.CommandButton cmdConfirm 
            Caption         =   "&Yes"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   204
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   120
            TabIndex        =   34
            Top             =   1320
            Width           =   1815
         End
         Begin VB.Label lblPrintChq 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Print front of cheque?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   120
            TabIndex        =   38
            Top             =   2160
            Visible         =   0   'False
            Width           =   6975
         End
         Begin VB.Label lblPrintPrompt 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Insert Cheque - Esc Cancel"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   120
            TabIndex        =   37
            Top             =   240
            Visible         =   0   'False
            Width           =   6855
         End
      End
      Begin VB.PictureBox fraAccountNo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   2415
         Left            =   0
         ScaleHeight     =   2385
         ScaleWidth      =   7065
         TabIndex        =   26
         Top             =   1320
         Visible         =   0   'False
         Width           =   7095
         Begin VB.TextBox txtAccountNo 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   2280
            MaxLength       =   10
            TabIndex        =   29
            Top             =   1680
            Width           =   3015
         End
         Begin VB.TextBox txtSortCode 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   2280
            MaxLength       =   6
            TabIndex        =   28
            Top             =   960
            Width           =   1455
         End
         Begin VB.TextBox txtChequeNo 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   2280
            MaxLength       =   6
            TabIndex        =   27
            Top             =   240
            Width           =   1455
         End
         Begin VB.Label lblAccountNo 
            BackStyle       =   0  'Transparent
            Caption         =   "Account No"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   240
            TabIndex        =   32
            Top             =   1680
            Width           =   1935
         End
         Begin VB.Label lblSortCode 
            BackStyle       =   0  'Transparent
            Caption         =   "Sort Code"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   240
            TabIndex        =   31
            Top             =   960
            Width           =   1695
         End
         Begin VB.Label lblChequeNo 
            BackStyle       =   0  'Transparent
            Caption         =   "Cheque No"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   240
            TabIndex        =   30
            Top             =   240
            Width           =   1935
         End
      End
      Begin VB.PictureBox fraChequeType 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   1215
         Left            =   0
         ScaleHeight     =   1215
         ScaleWidth      =   7095
         TabIndex        =   21
         Top             =   660
         Visible         =   0   'False
         Width           =   7095
         Begin VB.CommandButton cmdEscCheque 
            Caption         =   "Esc-Cancel"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   204
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   5040
            TabIndex        =   25
            Top             =   600
            Width           =   2055
         End
         Begin VB.CommandButton cmdCompanyChq 
            Caption         =   "C-Company"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   204
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   2400
            TabIndex        =   23
            Top             =   0
            Width           =   2300
         End
         Begin VB.CommandButton cmdPersonalChq 
            Caption         =   "P-Personal"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   204
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   0
            TabIndex        =   24
            Top             =   0
            Width           =   2300
         End
         Begin VB.CommandButton cmdNoTransaxChq 
            Caption         =   "N-No Transax"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   204
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   4800
            TabIndex        =   22
            Top             =   0
            Width           =   2300
         End
      End
      Begin VB.PictureBox fraBorder 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   4455
         Left            =   0
         ScaleHeight     =   4425
         ScaleWidth      =   7065
         TabIndex        =   2
         Top             =   1200
         Width           =   7095
         Begin VB.PictureBox fraChequeCard 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   3675
            Left            =   120
            ScaleHeight     =   3675
            ScaleWidth      =   6855
            TabIndex        =   6
            Top             =   -120
            Visible         =   0   'False
            Width           =   6855
            Begin VB.TextBox txtCVV 
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   555
               Left            =   2280
               MaxLength       =   3
               TabIndex        =   14
               Top             =   3120
               Width           =   975
            End
            Begin VB.TextBox txtIssueNo 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   555
               Left            =   2280
               MaxLength       =   3
               TabIndex        =   12
               Top             =   2400
               Width           =   975
            End
            Begin VB.TextBox txtStartMonth 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   3240
               MaxLength       =   2
               TabIndex        =   11
               Top             =   960
               Width           =   735
            End
            Begin VB.TextBox txtCardNo 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   2280
               MaxLength       =   19
               TabIndex        =   10
               Top             =   240
               Width           =   4575
            End
            Begin VB.TextBox txtStartYear 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   4320
               MaxLength       =   2
               TabIndex        =   9
               Top             =   960
               Width           =   735
            End
            Begin VB.TextBox txtEndMonth 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   3240
               MaxLength       =   2
               TabIndex        =   8
               Top             =   1680
               Width           =   735
            End
            Begin VB.TextBox txtEndYear 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   4320
               MaxLength       =   2
               TabIndex        =   7
               Top             =   1680
               Width           =   735
            End
            Begin VB.TextBox txtCustPresent 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   555
               Left            =   5760
               MaxLength       =   2
               TabIndex        =   13
               Text            =   "Y"
               Top             =   2400
               Width           =   495
            End
            Begin VB.Label lblCVV 
               BackStyle       =   0  'Transparent
               Caption         =   "CVV"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   20
               Top             =   3120
               Width           =   1935
            End
            Begin VB.Label lblCustPresent 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Cust Present"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   435
               Left            =   3540
               TabIndex        =   19
               Top             =   2400
               Width           =   2055
            End
            Begin VB.Label lblIssueNo 
               BackStyle       =   0  'Transparent
               Caption         =   "Issue No"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   18
               Top             =   2400
               Width           =   1935
            End
            Begin VB.Label lblStartMonthYear 
               BackStyle       =   0  'Transparent
               Caption         =   "Start Month / Year"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   17
               Top             =   960
               Width           =   3015
            End
            Begin VB.Label lblCardNo 
               BackStyle       =   0  'Transparent
               Caption         =   "Card No"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   16
               Top             =   240
               Width           =   1935
            End
            Begin VB.Label lblEndMonthYear 
               BackStyle       =   0  'Transparent
               Caption         =   "End Month / Year"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   15
               Top             =   1680
               Width           =   2895
            End
         End
         Begin VB.PictureBox fraAuthNum 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   735
            Left            =   120
            ScaleHeight     =   735
            ScaleWidth      =   6855
            TabIndex        =   3
            Top             =   3600
            Width           =   6855
            Begin VB.TextBox txtAuthNum 
               BackColor       =   &H00C0FFFF&
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   3240
               MaxLength       =   6
               TabIndex        =   4
               Top             =   180
               Width           =   1815
            End
            Begin VB.Label lblAuthNum 
               BackStyle       =   0  'Transparent
               Caption         =   "Authorisation No"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   240
               TabIndex        =   5
               Top             =   180
               Width           =   3015
            End
         End
      End
      Begin VB.Label lblAction 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Enter Card"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   615
         Left            =   0
         TabIndex        =   45
         Top             =   60
         Visible         =   0   'False
         Width           =   7095
      End
      Begin VB.Label lblAuthTranMod 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Authorisation Server Mode: "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   0
         TabIndex        =   44
         Top             =   720
         Visible         =   0   'False
         Width           =   7095
      End
      Begin VB.Label lblChequeType 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   0
         TabIndex        =   43
         Top             =   600
         Visible         =   0   'False
         Width           =   7095
      End
      Begin VB.Label lblStatus 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H0000FFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   990
         Left            =   0
         TabIndex        =   41
         Top             =   5265
         Width           =   7095
      End
      Begin VB.Label lblActionRequired 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Type of Cheque"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   0
         TabIndex        =   42
         Top             =   105
         Visible         =   0   'False
         Width           =   7095
      End
   End
   Begin MSWinsockLib.Winsock wskIntegration 
      Left            =   0
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      RemotePort      =   25000
   End
   Begin MSWinsockLib.Winsock wskProgress 
      Left            =   480
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      RemotePort      =   25001
   End
   Begin VB.Timer tmrLaunch 
      Left            =   960
      Top             =   0
   End
   Begin OposPOSPrinter_1_9_LibCtl.OPOSPOSPrinter OPOSPOSPrinter1 
      Left            =   2205
      OleObjectBlob   =   "frmTransaction.frx":0EF4
      Top             =   2100
   End
End
Attribute VB_Name = "frmTransaction"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Implements ITransactionEngine

Private moCommideaCfg As CommideaConfiguration
Private moAuthorizationRequest As AuthorizationRequest
Private moAuthorizationResult As AuthorizationResult

Private myReceivedMessage() As String
Private myReceivedBuffer As String
Private myProgressMessage() As String
Private myProgressBuffer As String
Private myMessageReceived As Boolean
Private myProgressError As String
Private myIntegrationError As String
Private myTranStart As Date
Private mbStop As Boolean
Private mbSwipeCard As Boolean
Private myContinueTRecordCreator As IContinueTRecordCreator

Public Event Duress()
Private WithEvents ConfirmAuthCodeForm As frmConfirmAuthCode
Attribute ConfirmAuthCodeForm.VB_VarHelpID = -1
Private WithEvents ConfirmReceiptPrintForm As frmConfirmReceiptPrint
Attribute ConfirmReceiptPrintForm.VB_VarHelpID = -1
Private WithEvents ValidateSignatureForm As frmValidateSignature
Attribute ValidateSignatureForm.VB_VarHelpID = -1
Private WithEvents VoiceReferralForm As frmVoiceReferral
Attribute VoiceReferralForm.VB_VarHelpID = -1

Private Sub ConfirmAuthCodeForm_Duress()

    RaiseEvent Duress
End Sub

Private Sub ConfirmReceiptPrintForm_Duress()

    RaiseEvent Duress
End Sub

Private Sub Form_Load()

    Me.BackColor = moCommideaCfg.BackgroundColour
    fraEntry.BackColor = Me.BackColor
    fraBorder.Visible = False
    fraChequeType.BackColor = Me.BackColor
    tmrTimeout.Interval = moCommideaCfg.PartialGCPaymentTimeout * 1000
    Call InitialiseStatusBar
    Me.Visible = False
    Call DebugMsg(ModuleName, "Form_Load", endlTraceOut)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If moCommideaCfg.UnderDuressKeyCode <> 0 And KeyCode = moCommideaCfg.UnderDuressKeyCode Then
        KeyCode = 0
        RaiseEvent Duress
        Exit Sub
    End If
    
    If KeyCode = vbKeyY And tmrTimeout.Enabled Then
        ShowOciusWindow
    End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
    tmrTimeout.Enabled = False
End Sub

Private Sub tmrAuthorise_Timer()

On Error GoTo AuthoriseEFTError

    tmrAuthorise.Enabled = False
    DoAuthorisation
    
    Exit Sub

AuthoriseEFTError:
    Call MsgBoxEx("Error has occurred in EFT system" & vbNewLine & Err.Number & ":" & Err.Description, vbExclamation, "Error Detected", , , , , moCommideaCfg.MsgBoxWarningColour)
    Call Err.Clear
End Sub

Private Sub tmrTimeout_Timer()
    lblInsert.Caption = "Press Y to open Barclays Gift window to continue transaction"
End Sub

Private Sub ShowOciusWindow()
    Call ResetFocus("Ocius")
End Sub

Private Sub ValidateSignatureForm_Duress()

    RaiseEvent Duress
End Sub

Private Sub VoiceReferralForm_Duress()

    RaiseEvent Duress
End Sub

' Control events
'Integration Socket events
Private Sub wskIntegration_ConnectionRequest(ByVal requestID As Long)

    ' Check if the control's State is closed. If not,
    ' close the connection before accepting the new
    ' connection.
    With wskIntegration
        If .State <> sckClosed Then
            .Close
        End If
        ' Accept the request with the requestID
        ' parameter.
        .Accept requestID
    End With
End Sub

Private Sub wskIntegration_DataArrival(ByVal bytesTotal As Long)
    Dim Received() As String
    Dim endOfMessage As Boolean
    Dim Extra As String
    Dim NextMess  As Integer

    DebugMsg ModuleName, "wskIntegration_DataArrival", endlTraceIn
    If bytesTotal > 0 Then

        If wskIntegration.State <> sckConnected Then
            Debug.Print "Integration socket state not connected - is : " & wskIntegration.State
            DebugMsg ModuleName, "wskIntegration_DataArrival", endlDebug, "Integration socket state not connected - is : " & wskIntegration.State
        End If
        Call wskIntegration.GetData(Extra, vbString)
        DebugMsg ModuleName, "wskIntegration_DataArrival", endlDebug, "Read from buffer: " & Extra
        
        Extra = Replace(Extra, Chr(6), "")
        
        myReceivedBuffer = myReceivedBuffer + Extra
        
        Received = Split(myReceivedBuffer, vbCrLf)
        Select Case UBound(Received)
            Case 0
                myReceivedBuffer = Received(0)
            Case Is > 0
                For NextMess = 0 To UBound(Received) - 1
                    ReDim Preserve myReceivedMessage(UBound(myReceivedMessage) + 1) As String
                    myReceivedMessage(UBound(myReceivedMessage)) = Received(NextMess)
                    Debug.Print "Received Message Received: " & Received(NextMess)
                    DebugMsg ModuleName, "wskIntegration_DataArrival", endlDebug, "Integration Message Received: " & Received(NextMess)
                Next NextMess
                myReceivedBuffer = Received(NextMess)
            Case Else
                DebugMsg ModuleName, "wskIntegration_DataArrival", endlDebug, "Nothing except maybe Asc 6 in the Winsock buffer"
        End Select
        DebugMsg ModuleName, "wskIntegration_DataArrival", endlDebug, "Integration Buffer: " & myReceivedBuffer
    Else
        DebugMsg ModuleName, "wskIntegration_DataArrival", endlDebug, "DataArrival with BytesTotal 0"
    End If
    DebugMsg ModuleName, "wskIntegration_DataArrival", endlTraceOut
End Sub

Private Sub wskIntegration_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)

    Call DebugMsg(ModuleName, "wskIntegration_Error", endlDebug, "Winsock error " & Number & ". " & Description)
    Select Case Number
        Case 1
            myIntegrationError = Trim(CStr(Number)) & ";" & Description & ";;Connection Error"
        Case 10060, 10061
            myIntegrationError = Trim(CStr(Number)) & ";" & Description & ";Please ensure Ocius is running and logged in;Ocius Sentinel Connection Error"
        Case Else
            myIntegrationError = Trim(CStr(Number)) & ";" & Description & ";Please check the connection cables, Network availability etc;General"
    End Select
    wskIntegration.Close
End Sub
' End of Integration Socket events

' Progress socket events
Private Sub wskProgress_ConnectionRequest(ByVal requestID As Long)

    ' Check if the control's State is closed. If not,
    ' close the connection before accepting the new
    ' connection.
    With wskProgress
        If .State <> sckClosed Then
            .Close
        End If
        ' Accept the request with the requestID
        ' parameter.
        .Accept requestID
    End With
End Sub

Private Sub wskProgress_DataArrival(ByVal bytesTotal As Long)
    Dim Extra As String
    Dim Messages() As String
    Dim NextMess As Integer

    DebugMsg ModuleName, "wskProgress_DataArrival", endlTraceIn
    If bytesTotal > 0 Then
        
        If wskProgress.State <> sckConnected Then
            Debug.Print "Progress socket state not connected - is : " & wskProgress.State
            DebugMsg ModuleName, "wskIntegration_DataArrival", endlDebug, "Progress socket state not connected - is : " & wskProgress.State
        End If
        
        Call wskProgress.GetData(Extra, vbString)
        DebugMsg ModuleName, "wskProgress_DataArrival", endlDebug, "Read from buffer: " & Extra
        myProgressBuffer = myProgressBuffer & Extra
        
        ' Might get many messages at once, so load them up
        Messages = Split(myProgressBuffer, vbCrLf)
        If UBound(Messages) >= 0 Then
            For NextMess = 0 To UBound(Messages) - 1
                ReDim Preserve myProgressMessage(UBound(myProgressMessage) + 1) As String
                myProgressMessage(UBound(myProgressMessage)) = Messages(NextMess)
                DebugMsg ModuleName, "wskProgress_DataArrival", endlDebug, "Progress Message added: " & Messages(NextMess)
                Debug.Print "Progress Message Received: " & Messages(NextMess)
            Next NextMess
            myProgressBuffer = Messages(NextMess)
            DebugMsg ModuleName, "wskProgress_DataArrival", endlDebug, "Progress buffer: " & myProgressBuffer
        End If
    Else
        DebugMsg ModuleName, "wskProgress_DataArrival", endlDebug, "DataArrival with BytesTotal 0"
    End If
    DebugMsg ModuleName, "wskProgress_DataArrival", endlTraceOut
End Sub

Private Sub wskProgress_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)

    Call DebugMsg(ModuleName, "wskProgress_Error", endlDebug, "Winsock error " & Number & ". " & Description)

    Select Case Number
        Case 1
            myProgressError = Trim(CStr(Number)) & ";" & Description & ";There was a problem connecting to the Pin Pad;Connection Error"
        Case 10060, 10061
            myProgressError = Trim(CStr(Number)) & ";" & Description & ";There was a problem connecting to the Pin Pad." & vbNewLine & "Please ensure Ocius is running and logged in;Ocius Sentinel Connection Error"
        Case Else
            myProgressError = Trim(CStr(Number)) & ";" & Description & ";Please check the connection cables, Network availability etc;General"
    End Select
    Call wskProgress_DataArrival(1)
    wskProgress.Close
End Sub
' End of Progress Socket events
' End of Control events

Public Sub ITransactionEngine_Initialise(ByVal commideaCfg As CommideaConfiguration)
    Set moCommideaCfg = commideaCfg
    
    Call DebugMsg(ModuleName, "Initialise", endlTraceIn)
    Call DebugMsg(ModuleName, "Initialise", endlDebug, "Set TillId=" & moCommideaCfg.TillID)
    Call DebugMsg(ModuleName, "Initialise", endlDebug, "Set ShowKeypadIcon=" & IIf(moCommideaCfg.ShowKeypadIcon, "true", "false"))
    
    Dim printerName As String
    If (Not moCommideaCfg.PrinterInfo Is Nothing) Then
        printerName = moCommideaCfg.PrinterInfo.Printer.Name
    Else
        printerName = "<N/A>"
    End If
    
    Call DebugMsg(ModuleName, "Initialise", endlDebug, "Set local posprinter reference to " & printerName)
    Call DebugMsg(ModuleName, "Initialise", endlTraceOut)
End Sub

Public Function ITransactionEngine_Authorise(ByVal oAuthorizationRequest As AuthorizationRequest) As AuthorizationResult

    On Error GoTo AuthoriseEFTError
        
    Call DebugMsg(ModuleName, "Authorise", endlTraceIn)
        
    txtCardNo.Text = ""
    txtStartMonth = ""
    txtStartYear = ""
    txtEndMonth = ""
    txtEndYear = ""
    txtIssueNo.Text = ""
    txtCVV.Text = ""
    txtAuthNum.Text = ""
    
    fraBorder.Visible = False
    fraInsertCard.Visible = True
    lblStatus.Visible = True
    lblAction.Visible = True
    txtAuthNum.BackColor = moCommideaCfg.EditColour
    txtIssueNo.BackColor = moCommideaCfg.EditColour
    
    Set moAuthorizationRequest = oAuthorizationRequest
    
    Dim factory As New PEDTypeFactory
    Set myContinueTRecordCreator = factory.CreateContinueTRecordCreator(moAuthorizationRequest.PEDType)

    Set moAuthorizationResult = New AuthorizationResult
    Call moAuthorizationResult.Init
    
    
    If moCommideaCfg.UseModalAuthorisation Then
        Call DebugMsg(ModuleName, "Authorise", endlDebug, "Using Modal Authorisation")
        With tmrAuthorise
            .Interval = 100
            .Enabled = True
        End With
        Call Me.Show(vbModal)
    Else
        Call DebugMsg(ModuleName, "Authorise", endlDebug, "Using Non-Modal Authorisation")
        Me.Show
        DoAuthorisation
    End If
    
    Set ITransactionEngine_Authorise = moAuthorizationResult

    Call DebugMsg(ModuleName, "Authorise", endlTraceOut)
    
    Exit Function

AuthoriseEFTError:
    Call MsgBoxEx("Error has occurred in EFT system" & vbNewLine & Err.Number & ":" & Err.Description, vbExclamation, "Error Detected", , , , , moCommideaCfg.MsgBoxWarningColour)
    Call Err.Clear
End Function

Private Function SendTransaction(record As IRequestRecord) As Boolean
    
Retry:
    With wskIntegration
        Select Case .State
            Case sckConnected
                Call DebugMsg(ModuleName, "SendTransaction", endlDebug, "Calling send transaction")
                ' Now send the new transaction
                Call .SendData(record.ToIntegrationMessage)  'Send Transaction
                ' Make sure the data is actually sent, now
                DoEvents
                Call DebugMsg(ModuleName, "SendTransaction", endlDebug, "Transaction sent")
                SendTransaction = True
            Case sckError
                Call DebugMsg(ModuleName, "SendTransaction", endlDebug, "Error Found")
            Case Else
        End Select
    End With
End Function ' SendTransaction

Private Property Get ITransactionEngine_ProgressMessage() As String()
    ITransactionEngine_ProgressMessage = ProgressMessage()
End Property

Private Property Get ProgressMessage() As String()

    ProgressMessage = myProgressMessage
End Property

Private Function NextProgressMessage() As cOutputProgressMessage

    If UBound(ProgressMessage()) > 0 Then
        Set NextProgressMessage = New cOutputProgressMessage
        If Not NextProgressMessage Is Nothing Then
            With NextProgressMessage
                .Initialise
                ' Always the 1st message as move all messages up 1 after this one is processed
                If Not .ParseReceivedMessage(ProgressMessage()(1)) Then
                    NextProgressMessage = Nothing
                End If
            End With
        End If
    End If
End Function

Private Function ReadNextProgressMessage() As cOutputProgressMessage
    Dim Delete As Integer

    If UBound(ProgressMessage()) > 0 Then
        ' read the message and then remove read message from list
        ' read...
        Set ReadNextProgressMessage = NextProgressMessage
        ' remove...
        ' Move message up one (overwriting the 1st that has now been processed)
        For Delete = 1 To UBound(myProgressMessage) - 1
            myProgressMessage(Delete) = myProgressMessage(Delete + 1)
        Next Delete
        ' Remove duplicate last message from end
        ReDim Preserve myProgressMessage(UBound(myProgressMessage) - 1) As String
    Else
        Set ReadNextProgressMessage = Nothing
    End If
End Function

Private Function NextProgressMessageIs(ByVal IsID As ProgressStatusID) As Boolean
    Dim NextMess As cOutputProgressMessage

    Set NextMess = NextProgressMessage
    If Not NextMess Is Nothing Then
        If NextMess.StatusID = IsID Then
            NextProgressMessageIs = True
        End If
    End If
End Function

Private Function GetPEDReadyForNextTransaction() As Boolean
    Do While UBound(myProgressMessage) = 0
        DoEvents
    Loop

    If NextProgressMessageIs(esiReady) Then
        ' Just read next message to remove it from list
        ReadNextProgressMessage
        GetPEDReadyForNextTransaction = True
    Else
        If NextProgressMessageIs(esiContinueRequired) Then
            ' Just read next message to remove it from list
            ReadNextProgressMessage
            GetPEDReadyForNextTransaction = True
        End If
        If CancelPreviousTransaction Then
            If NextProgressMessageIs(esiReady) Then
                GetPEDReadyForNextTransaction = True
                ' Just read next message to remove it from list
                ReadNextProgressMessage
            End If
        End If
    End If
End Function

Private Function ConnectionError() As Boolean

    If WinSockError <> "" Then
        ConnectionError = True
    End If
End Function

Private Function WinSockError() As String

    If myIntegrationError <> "" Then
        WinSockError = myIntegrationError
    Else
        If myProgressError <> "" Then
            WinSockError = myProgressError
        End If
    End If
End Function

Private Function CancelPreviousTransaction() As Boolean
    Dim ProgMess As cOutputProgressMessage
    
    Call SendContinueTransactionRecord(etraCancelTransaction)
    Call WaitForData
    Set ProgMess = ReadNextProgressMessage
    If Not ProgMess Is Nothing Then
        If ProgMess.StatusID = esiTransactionCancelled Then
            CancelPreviousTransaction = True
        End If
    End If
End Function

Public Sub ITransactionEngine_SendContinueTransactionRecord(ByVal ContinueType As ContinueTRecordAction, Optional ByVal AuthorisationCode As String = "")
    Call SendContinueTransactionRecord(ContinueType, AuthorisationCode)
End Sub

Private Sub SendContinueTransactionRecord(ByVal ContinueType As ContinueTRecordAction, Optional ByVal AuthorisationCode As String = "")
    Dim ContinueTRec As cContinueTransactionRecord
    
    DebugMsg ModuleName, "SendContinueTransactionRecord", endlTraceIn
    If Not myContinueTRecordCreator Is Nothing Then
        Select Case ContinueType
            Case ContinueTRecordAction.etraVoiceReferralAuthorised
                Set ContinueTRec = myContinueTRecordCreator.GetVoiceReferralAuthorisedContinueTransactionRecord(AuthorisationCode)
            Case ContinueTRecordAction.etraReprintReceipt
                Set ContinueTRec = myContinueTRecordCreator.GetReprintContinueTransactionRecord
            Case Else
                Set ContinueTRec = myContinueTRecordCreator.GetContinueTransactionRecordForContinueTRecordActionType(ContinueType)
        End Select
        With wskIntegration
            Call DebugMsg(ModuleName, "SendContinueTransactionRecord", endlDebug, "Integration Winsock state is currently " & .State)
            Select Case .State
                Case sckConnected
                    Call DebugMsg(ModuleName, "SendContinueTransactionRecord", endlDebug, "Sending Continue Transaction Record - Action ID = " & Trim(CStr(ContinueType)))
                    ' Now send the new transaction
                    Call .SendData(ContinueTRec.ToIntegrationMessage)  'Send Transaction
                    ' Make sure the data is actually sent, now
                    DoEvents
                    Call DebugMsg(ModuleName, "SendContinueTransactionRecord", endlDebug, "Sent Continue Transaction Record - Action ID = " & Trim(CStr(ContinueType)))
                Case sckError
            End Select
        End With
    Else
        DebugMsg ModuleName, "SendContinueTransactionRecord", endlDebug, "Do not have a ContinueTransactionRecordCreator, cannot send continue transaction message."
    End If
    DebugMsg ModuleName, "SendContinueTransactionRecord", endlTraceOut
End Sub

Private Sub WaitForData()

    Do
        Call Wait(1)
        DoEvents
    Loop While Not HaveReceivedMessage And NextProgressMessage Is Nothing And Not ConnectionError
End Sub

Private Function FireUpSockets(ByVal IntegrationPort As String, ByVal ProgressPort As String) As Boolean

    FireUpSockets = FireUpProgressSocket(ProgressPort) And FireUpIntegrationSocket(IntegrationPort)
End Function

Private Function FireUpProgressSocket(ByVal ProgressPort As String) As Boolean

    ' Set up the progress socket to receive data
    With Me.wskProgress
        Call .Close
        .RemotePort = ProgressPort
        .RemoteHost = "127.0.0.1"    ' .LocalIP
        Call DebugMsg(ModuleName, "FireUpSockets", endlDebug, "Progress Winsock state is currently " & .State)
        Call DebugMsg(ModuleName, "FireUpSockets", endlDebug, "Progress Winsock connecting to " & "127.0.0.1")    ' .LocalIP)
        Call .Connect
        While .State = sckConnecting
            DoEvents
        Wend
        Call DebugMsg(ModuleName, "FireUpSockets", endlDebug, "Progress Winsock state is currently " & .State)
    End With
    FireUpProgressSocket = Me.wskProgress.State = sckConnected
End Function

Private Function FireUpIntegrationSocket(ByVal IntegrationPort As String) As Boolean

    With Me.wskIntegration
        Call .Close
        .RemotePort = IntegrationPort
        .RemoteHost = "127.0.0.1"    ' .LocalIP
        ' Make sure have a 'ready' message before continuing
        If GetPEDReadyForNextTransaction() Then
            Call DebugMsg(ModuleName, "FireUpSockets", endlDebug, "Closing any previous Integration Winsock connection")
            Call DebugMsg(ModuleName, "FireUpSockets", endlDebug, "Integration Winsock state is currently " & .State)
            Call DebugMsg(ModuleName, "FireUpSockets", endlDebug, "Integration Winsock connecting to " & "127.0.0.1")    ' .LocalIP)
            Call .Connect
            While .State = sckConnecting
                DoEvents
            Wend
            Call DebugMsg(ModuleName, "FireUpSockets", endlDebug, "Integration Winsock state is currently " & .State)
        End If
    End With
    FireUpIntegrationSocket = Me.wskIntegration.State = sckConnected
End Function

Private Function SocketsConnected()

    SocketsConnected = Me.wskIntegration.State = sckConnected And Me.wskProgress.State = sckConnected
End Function

Private Sub CloseSockets()

    Call DebugMsg(ModuleName, "CloseSockets", endlDebug, "Closing Progress Winsock connection")
    wskProgress.Close
    Call DebugMsg(ModuleName, "CloseSockets", endlDebug, "Closing Integration Winsock connection")
    wskIntegration.Close
End Sub

Private Function ModuleName() As String

    ModuleName = "frmTransaction"
End Function

Private Sub InitialiseStatusBar()

    With sbStatus
        .Panels(PANEL_VERNO).Text = "Ver. " & App.Major & "." & App.Minor & "." & App.Revision
        .Panels(PANEL_VERNO).ToolTipText = "Program Version Number"
        .Panels(PANEL_WSID + 1).Text = Right$("00" & moCommideaCfg.TillID, 2) & " "
        .Panels(PANEL_WSID + 1).ToolTipText = "Current Work-Station Number"
        .Panels(PANEL_DATE + 1).Text = Format$(Date, "DD-MMM-YY")
        If (moCommideaCfg.ShowKeypadIcon = False) Then
            sbStatus.Panels("NumPad").Picture = Nothing
        End If
    End With
End Sub

Private Function GenerateAuthCodeDeclineReason(ByVal FailureReason As String, _
                                                      ByVal AVSHouseNumberResult As AVSHouseNumberCheckResult, _
                                                      ByVal AVSPostcodeResult As AVSPostCodeCheckResult, _
                                                      ByVal CSCCheck As CSCCheckResult) As String

    If AVSHouseNumberResult = eavshncrNotMatched Then
        FailureReason = "house no."
        If AVSPostcodeResult = eavspccrNotMatched Then
            FailureReason = FailureReason & " and postcode not matched - "
        ElseIf AVSPostcodeResult = eavspccrNotChecked Then
            FailureReason = FailureReason & " not matched and postcode not checked - "
        ElseIf AVSPostcodeResult = eavspccrNotProvided Then
            FailureReason = FailureReason & " not matched and postcode not provided - "
        Else
            FailureReason = FailureReason & " not matched - "
        End If
    ElseIf AVSHouseNumberResult = eavshncrNotChecked Then
        FailureReason = "house no."
        If AVSPostcodeResult = eavspccrNotMatched Then
            FailureReason = FailureReason & " not checked and postcode not matched - "
        ElseIf AVSPostcodeResult = eavspccrNotChecked Then
            FailureReason = FailureReason & " and postcode not checked - "
        ElseIf AVSPostcodeResult = eavspccrNotProvided Then
            FailureReason = FailureReason & " not checked and postcode not provided - "
        Else
            FailureReason = FailureReason & " not checked - "
        End If
    ElseIf AVSHouseNumberResult = eavshncrNotProvided Then
        FailureReason = "house no."
        If AVSPostcodeResult = eavspccrNotMatched Then
            FailureReason = FailureReason & " not provided and postcode not matched - "
        ElseIf AVSPostcodeResult = eavspccrNotChecked Then
            FailureReason = FailureReason & " not provided and postcode not checked - "
        ElseIf AVSPostcodeResult = eavspccrNotProvided Then
            FailureReason = FailureReason & " and postcode not provided - "
        Else
            FailureReason = FailureReason & " not provided - "
        End If
    ElseIf AVSPostcodeResult = eavspccrNotMatched Then
        FailureReason = "postcode not matched - "
    ElseIf AVSPostcodeResult = eavspccrNotChecked Then
        FailureReason = "postcode not checked - "
    ElseIf AVSPostcodeResult = eavspccrNotProvided Then
        FailureReason = "postcode not provided - "
    Else
        FailureReason = "authorisation code"
    End If
    FailureReason = FailureReason & " declined"
    GenerateAuthCodeDeclineReason = FailureReason
End Function

Private Function DeleteOldReceiptFile(ByVal ReceiptType As String) As Boolean
    Dim fsoReceipt As New FileSystemObject
    Dim receiptFile As String

    receiptFile = IIf(ReceiptType = "Merchant", GetMerchantReceiptFilePath(moCommideaCfg), GetCustomerReceiptFilePath(moCommideaCfg))
    With fsoReceipt
        If .FileExists(receiptFile) Then
            ' Try to delete it forcibly
            Call .DeleteFile(receiptFile, True)
            ' Is it still there?
            DeleteOldReceiptFile = Not .FileExists(receiptFile)
        End If
    End With
End Function

Private Function NoNewReceipt(ByVal ReceiptType As String, ByVal AuthorizationResult As IAuthorizationDetails) As Boolean
    Dim tranDate As Date
    Dim fsoReceipt As New FileSystemObject
    Dim receiptFile As String
    Dim ReceiptDate As Date

    NoNewReceipt = True
    If Not AuthorizationResult Is Nothing Then
        With AuthorizationResult
            If .TransactionDate = "" Then
                tranDate = myTranStart
            Else
                ' Convert string back to a full date
                tranDate = CDate(Mid(.TransactionDate, 7, 2) & "/" & Mid(.TransactionDate, 5, 2) & "/" & Mid(.TransactionDate, 1, 4) & " " & Mid(.TransactionDate, 9, 2) & ":" & Mid(.TransactionDate, 11, 2) & ":" & Mid(.TransactionDate, 13))
            End If
        End With
    End If
    receiptFile = IIf(ReceiptType = "Merchant", GetMerchantReceiptFilePath(moCommideaCfg), GetCustomerReceiptFilePath(moCommideaCfg))
    With fsoReceipt
        If .FileExists(receiptFile) Then
            With .GetFile(receiptFile)
                ReceiptDate = .DateLastModified
                If DateDiff("s", .DateCreated, .DateLastModified) < 0 Then
                    ReceiptDate = .DateCreated
                    If DateDiff("s", .DateLastAccessed, .DateCreated) < 0 Then
                        ReceiptDate = .DateLastAccessed
                    End If
                ElseIf DateDiff("s", .DateLastAccessed, .DateLastModified) < 0 Then
                    ReceiptDate = .DateLastAccessed
                End If
            End With
            If DateDiff("s", ReceiptDate, tranDate) < 0 Then
                NoNewReceipt = False
            End If
        End If
    End With
End Function

Private Function NoNewGiftCardReceipt(ByRef ReceiptType As String, ByVal AuthorizationResult As IAuthorizationDetails) As Boolean
    Dim tranDate As Date
    Dim fsoReceipt As New FileSystemObject
    Dim receiptFile As String
    Dim ReceiptDate As Date

    ReceiptType = ""
    NoNewGiftCardReceipt = True
    If Not AuthorizationResult Is Nothing Then
        With AuthorizationResult
            If .TransactionDate = "" Then
                tranDate = myTranStart
            Else
                ' Convert string back to a full date
                tranDate = DateAdd("h", Mid(.TransactionDate, 13, 2), DateAdd("n", Mid(.TransactionDate, 16, 2), DateAdd("s", Mid(.TransactionDate, 19, 2), CDate(Mid(.TransactionDate, 1, 11)))))
            End If
        End With
    End If
    receiptFile = GetMerchantReceiptFilePath(moCommideaCfg)
    With fsoReceipt
        If .FileExists(receiptFile) Then
            With .GetFile(receiptFile)
                ReceiptDate = .DateLastModified
                If DateDiff("s", .DateCreated, .DateLastModified) < 0 Then
                    ReceiptDate = .DateCreated
                    If DateDiff("s", .DateLastAccessed, .DateCreated) < 0 Then
                        ReceiptDate = .DateLastAccessed
                    End If
                ElseIf DateDiff("s", .DateLastAccessed, .DateLastModified) < 0 Then
                    ReceiptDate = .DateLastAccessed
                End If
            End With
            If DateDiff("s", ReceiptDate, tranDate) < 0 Then
                NoNewGiftCardReceipt = False
                ReceiptType = "Merchant"
                Exit Function
            End If
        End If
    End With
    receiptFile = GetCustomerReceiptFilePath(moCommideaCfg)
    With fsoReceipt
        If .FileExists(receiptFile) Then
            With .GetFile(receiptFile)
                ReceiptDate = .DateLastModified
                If DateDiff("s", .DateCreated, .DateLastModified) < 0 Then
                    ReceiptDate = .DateCreated
                    If DateDiff("s", .DateLastAccessed, .DateCreated) < 0 Then
                        ReceiptDate = .DateLastAccessed
                    End If
                ElseIf DateDiff("s", .DateLastAccessed, .DateLastModified) < 0 Then
                    ReceiptDate = .DateLastAccessed
                End If
            End With
            If DateDiff("s", ReceiptDate, tranDate) < 0 Then
                NoNewGiftCardReceipt = False
                ReceiptType = "Customer"
            End If
        End If
    End With
End Function

Private Sub WaitForRemoveCard()
    Dim RemoveCard As Boolean
    Dim CardRemoved As Boolean
    Dim NextProgressMessage As cOutputProgressMessage
    Dim Message As Integer

    Do
        Call WaitForData
        For Message = 1 To UBound(ProgressMessage)
            Set NextProgressMessage = New cOutputProgressMessage
            With NextProgressMessage
                .Initialise
                If .ParseReceivedMessage(ProgressMessage()(Message)) Then
                    If .StatusID = esiRemoveCard Then
                        RemoveCard = True
                    ElseIf .StatusID = esiCardRemoved Then
                        CardRemoved = True
                    End If
                End If
            End With
        Next Message
        If RemoveCard And Not CardRemoved Then
            Call MsgBoxEx("Please remove the card from the Pin Entry Device before printing.", vbInformation, "Remove the card from Pin Entry Device.", , , , , moCommideaCfg.MsgBoxPromptColour)
        End If
    Loop While RemoveCard And Not CardRemoved
End Sub

Private Sub ConnectToSockets()
    Dim Start As Date
    Dim Finish As Date
    Dim TimeTaken As Integer

    Start = Time
    Do
        Finish = Time
        Call FireUpSockets(moCommideaCfg.IntegrationPort, moCommideaCfg.ProgressPort)
        TimeTaken = ((Minute(Finish) * 60) + Second(Finish)) - ((Minute(Start) * 60) + Second(Start))
    Loop While TimeTaken < moCommideaCfg.ConnectionTimeout And Not SocketsConnected
    
    Call DebugMsg(ModuleName, "DoAuthorisation", endlDebug, "Time taken to connect to sockets = " & CStr(TimeTaken))
End Sub

Private Sub DoAuthorisation()
    Dim AVSPostcodeResult As AVSPostCodeCheckResult
    Dim AVSHouseNumberResult As AVSHouseNumberCheckResult
    Dim CSCResult As CSCCheckResult
    Dim AuthCode As String
    Dim TransactionNearlyCompleted As Boolean
    Dim AuthorisedMessReceieved As Boolean
    Dim TransactionCompleted As Boolean
    Dim VoidReceipt As Boolean
    Dim oProgressMessage As cOutputProgressMessage
    Dim DisplayRemoveCardMessage As Boolean
    Dim ReceiptType As String
    Dim printCounter As Counter
    Dim tempReceipts() As String
    Dim TransactionRejected As Boolean
    Dim CancellingForReceiptPrintFailure As Boolean
    Dim AlreadyPrinted As Boolean
    Dim ErrorParts() As String
    Dim authStrategy As IAuthorisationStrategy
    Dim continueRequiredCount As Integer

    On Error GoTo AuthoriseEFTError

    continueRequiredCount = 0
    ReDim myProgressMessage(0) As String
    ReDim myReceivedMessage(0) As String
    Call DebugMsg(ModuleName, "DoAuthorisation", endlDebug, "Connecting to sockets")

    ' Make sure connected before starting
    ConnectToSockets
    
    If SocketsConnected Then
        
        If moAuthorizationRequest.RequestType = BarclaycardGift And moAuthorizationRequest.BgiftTransactionType = Sale Then
            
            Dim oAdditionalCommand As New cAdditionalCommand
            Call oAdditionalCommand.Initialise(eaicWinState, , , , , , , , eaiwsNormal)
            wskIntegration.SendData (oAdditionalCommand.ToIntegrationMessage)
            Call ResetFocus(Me.Caption)
            
            Dim additionalResponse As String
            additionalResponse = ""
            
            Call WaitForData
            If HaveReceivedMessage Then
                additionalResponse = GetNextReceivedMessage()
                Call ResetFocus(Me.Caption)
            End If
    
            Wait (1)
            Call ResetFocus(Me.Caption)
            
            ConnectToSockets
        
        End If
        
        Dim requestRecord As IRequestRecord
        Set requestRecord = moAuthorizationRequest.CreateRequestRecord()
        
        Dim oAuthorizationDetails As IAuthorizationDetails
        Set oAuthorizationDetails = moAuthorizationRequest.CreateAuthorizationDetails()
            
        AVSPostcodeResult = -1
        AVSHouseNumberResult = -1
        CSCResult = -1
    
        ' Keep this in case need to check print receipt file date against it when no transaction taken place
        myTranStart = Now
        
        ' Send the transaction
        If SendTransaction(requestRecord) Then
            Set authStrategy = CreateAuthorisationStrategy(moAuthorizationRequest.PEDType)
            
            Do 'While Not TransactionCompleted
                Call WaitForData
                If HaveReceivedMessage Then
                    If oAuthorizationDetails.ParseReceivedMessage(GetNextReceivedMessage()) Then
                        If moAuthorizationRequest.RequestType = Transaction Then
                                
                            Dim oOutputTRecordV4 As cOutputTRecordV4
                            Set oOutputTRecordV4 = oAuthorizationDetails
                                    
                            With oOutputTRecordV4
                                If .Successful Then
                                    Select Case .TResult
                                        Case TransactionResultNonErrorScreenless.etrnesAuthorised
                                            lblAction.Caption = "Transaction Authorised"
                                            lblStatus.Caption = "Transaction Authorised"
                                            lblInsert.Caption = ""
                                            ' Not a complete transaction yet
                                            ' but if doing a CNP sale txn, need to check avs, csc check results
                                            ' as if fail, might end early
                                            If (moAuthorizationRequest.TRecordModifier = etrmCNPAccountOnFile _
                                            Or moAuthorizationRequest.TRecordModifier = etrmCNPMailOrder _
                                            Or moAuthorizationRequest.TRecordModifier = etrmCNPTelephoneOrder) _
                                            And moAuthorizationRequest.transactionType <> etrttRefund Then
                                                ' Save auth code (in customer verification), for display,'
                                                ' should the code need confirming
                                                AuthCode = Trim(Replace(.CustomerVerification, "Auth Code:", ""))
                                                AVSPostcodeResult = .AVSPostCode
                                                AVSHouseNumberResult = .AVSHouseNumber
                                                CSCResult = .CSCResult
                                                If CSCResult = ecscrNotMatched Then
                                                    moAuthorizationResult.FailureReason = "security code not matched"
                                                    TransactionNearlyCompleted = True
                                                ElseIf CSCResult = ecscrNotChecked Then
                                                    moAuthorizationResult.FailureReason = "security code not checked"
                                                    TransactionNearlyCompleted = True
                                                ElseIf CSCResult = ecscrNotProvided Then
                                                    moAuthorizationResult.FailureReason = "security code not provided"
                                                    TransactionNearlyCompleted = True
                                                End If
                                            End If
                                            AuthorisedMessReceieved = True
                                        Case TransactionResultNonErrorScreenless.etrnesCompleted
                                            lblAction.Caption = "Transaction Completed"
                                            lblStatus.Caption = "Transaction Completed"
                                            lblInsert.Caption = ""
                                            moAuthorizationResult.Completed = True
                                            TransactionCompleted = True
                                        Case TransactionResultNonErrorScreenless.etrnesDeclined
                                            lblAction.Caption = "Transaction Declined"
                                            lblStatus.Caption = "Transaction Declined"
                                            lblInsert.Caption = ""
                                            TransactionCompleted = True
                                        Case TransactionResultNonErrorScreenless.etrnesReferred
                                            lblAction.Caption = "Voice Referral"
                                            lblStatus.Caption = "Transaction Referred"
                                            lblInsert.Caption = ""
                                            Call LoadCommideaForm(frmVoiceReferral, moCommideaCfg)
                                            Set VoiceReferralForm = frmVoiceReferral
                                            If frmVoiceReferral.ShowMe(.ReferralTelephoneNumber, .MerchantNumber, .TerminalID, CurrencyAmount(.TransactionCurrencyCode, .TotalTranValueProcessed)) Then
                                                Call SendContinueTransactionRecord(etraVoiceReferralAuthorised, frmVoiceReferral.AuthorisationCode)
                                            Else
                                                Call SendContinueTransactionRecord(etraVoiceReferralRejected)
                                                moAuthorizationResult.FailureReason = "voice referral rejected"
                                            End If
                                            Set VoiceReferralForm = Nothing
                                            Call UnloadCommideaForm(frmVoiceReferral)
                                            Call ResetFocus(Me.Caption)
                                        Case TransactionResultNonErrorScreenless.etrnesReversed
                                            lblAction.Caption = "Transaction Reversed"
                                            lblStatus.Caption = "Transaction Reversed"
                                            lblInsert.Caption = .CustomerVerification
                                            If TransactionNearlyCompleted _
                                            Or (Not AuthorisedMessReceieved And VoidReceipt) Then
                                                ' If reject an authorised txn, e.g. signature rejection
                                                ' then this flag will have been set (see Customer Receipt print reject)
                                                ' Now need to end the loop, as will get no more messages
                                                ' Can also get here via 'Card Removed During PIN Entry Error'
                                                TransactionCompleted = True
                                            Else
                                                Dim completeState As TransactionCompleteState
                                                completeState = authStrategy.ProcessReversedTransactionResult(oAuthorizationDetails)
                                                TransactionNearlyCompleted = completeState = TransactionCompleteState.NearlyCompleted
                                                TransactionCompleted = completeState = TransactionCompleteState.Completed
                                            End If
                                            If moAuthorizationResult.FailureReason = "" Then
                                                moAuthorizationResult.FailureReason = LCase(.CustomerVerification)
                                            End If
                                        Case Else
                                    End Select '.TResult
                                        
                                Else '.Successful
                                    
                                    Select Case .TResult
                                        Case ErrorID_UnspecifiedErrorCheckLog
                                            If StrComp(.CustomerVerification, "invalid tokenid", vbTextCompare) = 0 Then
                                                ' Referral 820
                                                ' Improve messages and correct original work around now that
                                                lblAction.Caption = "Cannot refund to original card details"
                                                lblStatus.Caption = "Try again to refund to a different card or other payment type"
                                                lblInsert.Caption = "The original card details might be no longer valid (card or details expired)."
                                                Call Wait(5)
                                                ' Give a reason, if not all ready got one
                                                If moAuthorizationResult.FailureReason = "" Then
                                                    moAuthorizationResult.FailureReason = "the original card details might be no longer valid - card or details expired"
                                                End If
                                                ' End of Referral 820
                                            Else
                                                lblAction.Caption = .ErrorMessage
                                                lblStatus.Caption = .ErrorAction
                                                lblInsert.Caption = .ErrorDescription
                                                Call Wait(3)
                                                ' Give a reason, if not all ready got one
                                                If moAuthorizationResult.FailureReason = "" Then
                                                    moAuthorizationResult.FailureReason = LCase(.CustomerVerification)
                                                End If
                                            End If
                                            TransactionNearlyCompleted = True
                                        Case Else
                                            lblAction.Caption = .ErrorMessage
                                            lblStatus.Caption = .ErrorAction
                                            lblInsert.Caption = .ErrorDescription
                                            TransactionCompleted = True
                                            Call Wait(3)
                                            ' Give a reason, if not all ready got one
                                            If moAuthorizationResult.FailureReason = "" Then
                                                moAuthorizationResult.FailureReason = LCase(.CustomerVerification)
                                            End If
                                    End Select '.TResult
                                        
                                End If '.Successful
                            End With
    
                        Else 'moAuthorizationRequest.RequestType = Transaction
                                
                            tmrTimeout.Enabled = False
                            
                            Dim responseRecord As BGiftResponseREcord
                            Set responseRecord = oAuthorizationDetails
                            
                            Select Case responseRecord.RecordResultCode
                                Case BGiftResponseRecordResultCode.Success
                                    AuthorisedMessReceieved = True
                                    moAuthorizationResult.Completed = True
                                
                                Case BGiftResponseRecordResultCode.Rejected
                                    lblStatus.Caption = "Transaction Rejected"
                                
                                Case BGiftResponseRecordResultCode.Reversed
                                    lblStatus.Caption = "Transaction Reversed"
                                    
                                Case Is <= BGiftResponseRecordResultCode.Error
                                    lblStatus.Caption = "Transaction Error"
                            End Select
                            
                            TransactionCompleted = True
                            
                            If Not responseRecord.RecordResultCode = Success Then
                                lblAction.Caption = responseRecord.AuthorisationMessage
                                lblInsert.Caption = ""
                                Call Wait(3)
                            End If
                        
                        End If 'moAuthorizationRequest.RequestType = Transaction
                                
                        Set moAuthorizationResult.Details = oAuthorizationDetails
    
                    Else 'authorizationResult.ParseReceivedMessage(GetNextReceivedMessage())
                        Set moAuthorizationResult.Details = Nothing
                        Call MsgBoxEx("Failed translating the message from PED.", vbExclamation, "Error has occurred in processing the transaction.", , , , , moCommideaCfg.MsgBoxWarningColour)
                        Call ResetFocus(Me.Caption)
                    End If ''authorizationResult.ParseReceivedMessage(GetNextReceivedMessage())
                        
                End If 'HaveReceivedMessage
                    
                Do While Not NextProgressMessage Is Nothing
                    Set oProgressMessage = ReadNextProgressMessage
                    If Not oProgressMessage Is Nothing Then
                        With oProgressMessage
                            If .Result = 100 Then
                            
                                Dim statusProcessed As Boolean
                                statusProcessed = True
                                
                                Select Case .StatusID
                                    Case esiRemoveCard
                                        ' Flag up to keep remove card message til get 'card removed' trans output record
                                        DisplayRemoveCardMessage = True
                                        lblAction.Caption = .CustomerFacingMessage
                                        lblStatus.Caption = .OperatorMessage
                                        lblInsert.Caption = .ParametersMessage
                                    Case esiCardRemoved
                                        ' Swicth off flag
                                        DisplayRemoveCardMessage = False
                                        lblAction.Caption = .CustomerFacingMessage
                                        lblStatus.Caption = .OperatorMessage
                                        lblInsert.Caption = .ParametersMessage
                                    Case esiContinueRequired
                                        If moAuthorizationRequest.RequestType = BarclaycardGift And mbSwipeCard = True Then
                                            tmrTimeout.Enabled = False
                                            
                                            continueRequiredCount = continueRequiredCount + 1
                                
                                            lblAction.Caption = "Please Wait"
                                            lblStatus.Caption = "Printing Receipt"
                                            lblInsert.Caption = ""
                                            
                                            If Not NoNewGiftCardReceipt(ReceiptType, oAuthorizationDetails) Then
                                                                                                
                                                ' Workaround for Ocius  v3.4.5.5. It passes the customer receipt from the last tran as merchant receipt. But it passes it correctly after reprint.
                                                ' So treat 1st ContinueRequired from vx810 as Merchant always.
                                                If ReceiptType = "Merchant" Or moAuthorizationRequest.PEDType = ePEDType810 And continueRequiredCount = 1 Then
                                                    ' Skip Merchant receipt for GC. Dont print it, even if it comes from Ocius
                                                    DeleteOldReceiptFile ReceiptType 'may be "Customer" because of VX810 bug
                                                Else
                                                    Set printCounter = authStrategy.GetReceiptPrintCounter
                                                    printCounter.Initialise
                                                    
                                                    Call LoadCommideaForm(frmConfirmReceiptPrint, moCommideaCfg)
                                                    Set ConfirmReceiptPrintForm = frmConfirmReceiptPrint
                                                    
                                                    tempReceipts = moAuthorizationResult.Receipts
                                                    
                                                    Dim showPrintForm As Boolean
                                                    showPrintForm = (moAuthorizationRequest.PEDType = ePEDType820)
                                                    
                                                    Do While _
                                                        Not frmConfirmReceiptPrint.PerformPrint(showPrintForm, ReceiptType, tempReceipts, VoidReceipt) _
                                                        And printCounter.Value < 3
                                                        
                                                        If showPrintForm Then
                                                            If (Not authStrategy.ProcessReceiptRePrintRequired) Then
                                                                Exit Do
                                                            End If
                                                        End If
                                                        
                                                        Call printCounter.Increment
                                                    Loop
                                                    
                                                    moAuthorizationResult.Receipts = tempReceipts
                                                    
                                                    Set ConfirmReceiptPrintForm = Nothing
                                                    Call UnloadCommideaForm(frmConfirmReceiptPrint)
                                                End If
                                            End If
                                        End If

                                        authStrategy.ProcessContinueRequiredProgressStatus
                                        
                                        If moAuthorizationRequest.RequestType = BarclaycardGift And mbSwipeCard = True And moAuthorizationRequest.PEDType = ePEDType810 Then
                                            ShowOciusWindow
                                        Else
                                            Call ResetFocus(Me.Caption)
                                        End If
                                        
                                    Case esiWaitingForCashback
                                        ' Not allowing cashback at the moment
                                        ' TODO - Alanl - Add a parameter to enable cashback and pass in to here
                                        Call SendContinueTransactionRecord(etraCashbackNotRequired)
                                        Call ResetFocus(Me.Caption)
                                        
                                    Case esiPrintingReceipt, esiPrintingMerchantReceipt, esiPrintingCustomerReceipt
                                        Dim skip As Boolean
                                        skip = authStrategy.SkipPrintingReceiptProcessing(.StatusID)
                                        If Not skip Then
                                            ' Leave remove card message up
                                            If Not DisplayRemoveCardMessage Then
                                                lblAction.Caption = .CustomerFacingMessage
                                            End If
                                            lblStatus.Caption = .OperatorMessage
                                            lblInsert.Caption = .ParametersMessage
                                            
                                            Dim TransactionCompletedChanged As Boolean
                                            TransactionCompletedChanged = False
                                            If .StatusID = esiPrintingReceipt Or .StatusID = esiPrintingMerchantReceipt Then
                                                ReceiptType = "Merchant"
                                                ' Referral 500 - On QA till get an error when send re-print before the card
                                                ' is removed.  So wait until the card has been removed before proceed with
                                                ' printing, which then leads on to re-print option
                                                Call WaitForRemoveCard
                                            Else
                                                ReceiptType = "Customer"
                                                If TransactionNearlyCompleted Then
                                                    TransactionCompleted = True
                                                    TransactionCompletedChanged = True
                                                Else
                                                    If TransactionRejected Then
                                                        TransactionNearlyCompleted = True
                                                    End If
                                                End If
                                            End If
                                            
                                            Dim continueCheckingForNewReceipt As Boolean
                                            continueCheckingForNewReceipt = True
                                            Do While continueCheckingForNewReceipt _
                                                And NoNewReceipt(ReceiptType, oAuthorizationDetails) _
                                                And Not CancellingForReceiptPrintFailure
                                                
                                                ' try to remove the old receipt to see if that helps print the new
                                                If DeleteOldReceiptFile(ReceiptType) Then
                                                    ' There is the potential that get stuck in an infinite loop here if NoNewReceipt
                                                    ' and DeleteOldReceiptFile both return true all the time
                                                    continueCheckingForNewReceipt = authStrategy.ProcessReceiptRePrintRequired
                                                Else
                                                    ' Cannot cancel the transaction here,
                                                    ' save it for signature validation stage
                                                    CancellingForReceiptPrintFailure = True
                                                End If
                                            Loop 'Do While continueCheckingForNewReceipt ...
                                            
                                            Dim cancelledAfterMaxReprintAttempts As Boolean
                                            cancelledAfterMaxReprintAttempts = False
                                            
                                            If Not CancellingForReceiptPrintFailure Then
                                                Set printCounter = authStrategy.GetReceiptPrintCounter
                                                
                                                AlreadyPrinted = True
                                                Call LoadCommideaForm(frmConfirmReceiptPrint, moCommideaCfg)
                                                Set ConfirmReceiptPrintForm = frmConfirmReceiptPrint
                                                
                                                tempReceipts = moAuthorizationResult.Receipts
                                                
                                                Do While _
                                                    Not frmConfirmReceiptPrint.PerformPrint(True, ReceiptType, tempReceipts, VoidReceipt) _
                                                        And printCounter.Value < 3
                                                        
                                                    If (Not authStrategy.ProcessReceiptRePrintRequired) Then
                                                        ' Reset TransactionCompleted flag: workaround for "Invalid Token" case
                                                        ' To force processing of PrintingReceipt and ContinueRequired messages
                                                        If TransactionCompletedChanged Then
                                                            TransactionCompleted = False
                                                        End If
                                                        Exit Do
                                                    End If
                                                    
                                                    ' Only reading customer receipts for later printing
                                                    ' so no receipt to see yet for visual confirmation
                                                    ' of OK print.  So automatic reprint has to have
                                                    ' a way of being stopped, so use a count
                                                        If StrComp(ReceiptType, "Customer", vbTextCompare) = 0 And Not moCommideaCfg.UseSeparateReceipt Then
                                                        Call printCounter.Increment
                                                    End If
                                                Loop
                                                
                                                moAuthorizationResult.Receipts = tempReceipts
                                                
                                                ' Give it 3 goes, but then quit.  Cannot proceed if not read (for printing later) or printed a proper print file
                                                If printCounter.Value = 3 Then
                                                    CancellingForReceiptPrintFailure = True
                                                    cancelledAfterMaxReprintAttempts = True
                                                End If
                                                Set ConfirmReceiptPrintForm = Nothing
                                                Call UnloadCommideaForm(frmConfirmReceiptPrint)
                                            End If
                                            
                                            If CancellingForReceiptPrintFailure Then
                                                Call authStrategy.ShowStateForReceiptPrintFailure(ReceiptType, cancelledAfterMaxReprintAttempts)
                                            End If
                                            
                                            Call ResetFocus(Me.Caption)
                                        Else
                                            statusProcessed = False
                                        End If
                                        
                                    Case esiSignatureConfirmationRequired
                                        ' If failed to print the receipt, must cancel the transaction
                                        If CancellingForReceiptPrintFailure Then
                                            Call SendContinueTransactionRecord(etraRejectSignature)
                                            moAuthorizationResult.FailureReason = "signature receipt print failed"
                                        Else
                                            Dim processed As Boolean
                                            processed = authStrategy.ProcessSignatureConfirmationRequiredProgressStatus()
                                            If Not processed Then
                                                ' Leave remove card message up
                                                If Not DisplayRemoveCardMessage Then
                                                    lblAction.Caption = .CustomerFacingMessage
                                                End If
                                                lblStatus.Caption = .OperatorMessage
                                                lblInsert.Caption = .ParametersMessage
                                                Call LoadCommideaForm(frmValidateSignature, moCommideaCfg)
                                                Set ValidateSignatureForm = frmValidateSignature
                                                If frmValidateSignature.ShowMe() Then
                                                    Call SendContinueTransactionRecord(etraConfirmSignature)
                                                Else
                                                    Call SendContinueTransactionRecord(etraRejectSignature)
                                                    moAuthorizationResult.FailureReason = "signature declined"
                                                    ' If reject an authorised txn, need to start flagging up the end is nigh
                                                    ' to be picked up by ReceivedMessage of 'Reversed', so that can
                                                    ' cause the end of the loop
                                                    'TransactionNearlyCompleted = True
                                                    TransactionRejected = True
                                                End If
                                            End If
                                        End If
                                        
                                    Call ResetFocus(Me.Caption)
                                        Set ValidateSignatureForm = Nothing
                                        Call UnloadCommideaForm(frmValidateSignature)
                                        
                                    Case esiVoiceReferral
                                        ' Not getting this here now, comes as integration output record instead
                                        
                                    Case esiConfirmAuthCode
                                        Call LoadCommideaForm(frmConfirmAuthCode, moCommideaCfg)
                                        Set ConfirmAuthCodeForm = frmConfirmAuthCode
                                        ' Added flag to auto abort should there be any failure of avs checking
                                        ' left code in, so can switch this back on if need be
                                        If frmConfirmAuthCode.ShowMe(AuthCode, AVSHouseNumberResult, AVSPostcodeResult, CSCResult, True) Then
                                            Call SendContinueTransactionRecord(etraConfirmAuthCode)
                                            ' Authorised and accepted
                                            moAuthorizationResult.Completed = True
                                        Else
                                                Call SendContinueTransactionRecord(etraRejectAuthCode)
                                                ' Should get a 'Rejecting Transaction' progress message later.  Let
                                                ' taht set up appropriate Transaction completion flag(s)
                                                ' Just set up a failure reason for non CNP aof refund - i.e. those
                                                ' that require card detail entry
                                                If moAuthorizationRequest.TRecordModifier = etrmCNPAccountOnFile _
                                                And moAuthorizationRequest.transactionType = etrttRefund _
                                                And moAuthorizationRequest.AOFTokenID <> "" Then
                                                moAuthorizationResult.FailureReason = "rejected"
                                            Else
                                                moAuthorizationResult.FailureReason = GenerateAuthCodeDeclineReason(moAuthorizationResult.FailureReason, AVSHouseNumberResult, AVSPostcodeResult, CSCResult)
                                            End If
                                        End If
                                        Set ConfirmAuthCodeForm = Nothing
                                        Call UnloadCommideaForm(frmConfirmAuthCode)
                                        Call ResetFocus(Me.Caption)
                                    
                                    Case esiLoginRequired
                                        moAuthorizationResult.FailureReason = "not logged in"
                                        
                                    Case esiCardNotAccepted
                                        moAuthorizationResult.FailureReason = "card not accepted"
                                            
                                    Case esiRejectingTxn
                                        ' If reject an authorised txn, need to start flagging up the end is nigh
                                        ' to be picked up by ReceivedMessage of 'Reversed', so that can
                                        ' cause the end of the loop
                                        'TransactionNearlyCompleted = True
                                        TransactionRejected = True
                                        If InStr(1, moAuthorizationResult.FailureReason, "declined", vbTextCompare) = 0 _
                                            And InStr(1, moAuthorizationResult.FailureReason, "security code", vbTextCompare) = 0 _
                                            And InStr(1, moAuthorizationResult.FailureReason, "signature receipt", vbTextCompare) = 0 Then
                                            moAuthorizationResult.FailureReason = "rejected"
                                        End If
                                        If Not oAuthorizationDetails Is Nothing Then
                                            If moAuthorizationRequest.RequestType = Transaction Then
                                                
                                                Dim oOutputTRecordV4Tmp As cOutputTRecordV4
                                                Set oOutputTRecordV4Tmp = oAuthorizationDetails
                                        
                                                With oOutputTRecordV4Tmp
                                                    ' Might have removed a card to early
                                                    If .TResult = TransactionResultNonErrorScreenless.etrnesAuthorised _
                                                    And .CaptureMethod = .CaptureMethods(ecmtICC) Then
                                                        moAuthorizationResult.FailureReason = moAuthorizationResult.FailureReason & " - card removed too early?"
                                                    End If
                                                End With
                                            End If
                                        End If
                                        
                                    Case esiInitialisingPED
                                        ' Wierd one this.  Get this message if remove card during PIN entry and then replace it
                                        ' Does the receipt printing first, so if that has been flagged, assume we are on the
                                        ' path for this particular error, so when get a reversed result
                                        ' have something to identify fact that not going to get any more progress messages
                                        If AlreadyPrinted Then
                                            TransactionNearlyCompleted = True
                                        End If
                                        
                                    Case esiReady
                                        mbSwipeCard = False
                                        statusProcessed = False
                                        
                                    Case esiSwipeCard
                                        If moAuthorizationRequest.RequestType = BarclaycardGift Then
                                            If moAuthorizationRequest.BgiftTransactionType = Sale Then
                                                tmrTimeout.Enabled = True
                                            End If
                                            mbSwipeCard = True
                                        End If
                                        statusProcessed = False
                                        
                                    Case Else
                                        statusProcessed = False
                                End Select '.StatusID
                                
                                ' Reset timer, if any message received
                                If tmrTimeout.Enabled Then
                                    tmrTimeout.Enabled = False
                                    tmrTimeout.Enabled = True
                                End If
                                
                                If Not statusProcessed Then
                                    Dim extraProcessProgressStatusProcessed As Boolean
                                    extraProcessProgressStatusProcessed = authStrategy.ExtraProcessProgressStatus(.StatusID)
                                    If (Not extraProcessProgressStatusProcessed) Then
                                        ' Leave previous message up if completed AND just got a ready message
                                        If TransactionCompleted And .StatusID = esiReady Then
                                        Else
                                            ' Leave remove card message up
                                            If Not DisplayRemoveCardMessage Then
                                                lblAction.Caption = .CustomerFacingMessage
                                            End If
                                            lblStatus.Caption = .OperatorMessage
                                            lblInsert.Caption = .ParametersMessage
                                            If Len(lblInsert.Caption) = 0 And .StatusID = esiSwipeCard And moAuthorizationRequest.RequestType = BarclaycardGift Then
                                                lblInsert.Caption = "Total value: " & Format(Abs(moAuthorizationRequest.TxnValue), "0.00")
                                            End If
                                        End If
                                    End If
                                End If
                                
                            Else
                                ' error manage problem via progress message
                            End If
                        End With
                    Else
                        ' error initialising progress message
                    End If
                Loop 'Not NextProgressMessage Is Nothing
                
                If ConnectionError Then
                    ErrorParts = Split(WinSockError, ";")
                    Call MsgBoxEx("WARNING: (" & ErrorParts(0) & " " & ErrorParts(1) & ") " & vbNewLine & ErrorParts(2), _
                                  vbCritical, _
                                  ErrorParts(3), , , , , _
                                  moCommideaCfg.MsgBoxWarningColour)
                    myProgressError = ""
                    myIntegrationError = ""
                    ReDim ErrorParts(0) As String
                    TransactionCompleted = True
                End If
            Loop While Not TransactionCompleted
        Else 'SendTransaction(requestRecord)
            Call MsgBoxEx("WARNING: Failed sending transaction details to PED.", _
                        vbCritical, _
                        "Cannot continue with EFT.", , , , , moCommideaCfg.MsgBoxWarningColour)
        End If 'SendTransaction(requestRecord)
            
        DoEvents
        CloseSockets
            
    Else 'SocketsConnected
        Call MsgBoxEx("WARNING: Failed setting up communication with the PED.", _
                    vbCritical, _
                    "Cannot continue with EFT.", , , , , moCommideaCfg.MsgBoxWarningColour)
    End If 'SocketsConnected

    Me.Hide

    Exit Sub

AuthoriseEFTError:
    Call MsgBoxEx("Error has occurred in EFT system" & vbNewLine & Err.Number & ":" & Err.Description, vbExclamation, "Error Detected", , , , , moCommideaCfg.MsgBoxWarningColour)
    Call Err.Clear
    
End Sub

Private Function HaveReceivedMessage() As Boolean

    HaveReceivedMessage = UBound(myReceivedMessage) > 0
End Function

Private Function GetNextReceivedMessage() As String
    Dim Delete As Integer
    
    GetNextReceivedMessage = myReceivedMessage(1)
    
    For Delete = 1 To UBound(myReceivedMessage) - 1
        myReceivedMessage(Delete) = myReceivedMessage(Delete + 1)
    Next
    ReDim Preserve myReceivedMessage(UBound(myReceivedMessage) - 1)
End Function

Private Function CreateAuthorisationStrategy(thePedType As PEDType) As IAuthorisationStrategy
    Dim Result As IAuthorisationStrategy
    Dim factory As New PEDTypeFactory
    Set Result = factory.CreateAuthorisationStrategy(thePedType)
    
    Dim tranEngine As ITransactionEngine
    Set tranEngine = Me
    Result.Initialize tranEngine
    
    Set CreateAuthorisationStrategy = Result
End Function

Public Sub ITransactionEngine_ShowStateForReceiptPrintFailure(ReceiptType As String)
    lblAction.Caption = "Receipt print file failed"
    lblStatus.Caption = "Failed reading " & ReceiptType & " receipt file."
    If StrComp(ReceiptType, "Merchant", vbTextCompare) = 0 Then
        lblInsert.Caption = "Cancelling the transaction"
    Else
        With lblStatus
            .Caption = .Caption & vbCrLf & "Do not give receipt to customer."
        End With
        lblInsert.Caption = "Possible incorrect customer receipt."
        Call Wait(2)
    End If
End Sub

Public Sub ITransactionEngine_Dispose()
    Set moCommideaCfg = Nothing
End Sub
