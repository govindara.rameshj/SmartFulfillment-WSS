VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{FDAC2480-F4ED-4632-AA78-DCA210A74E49}#6.0#0"; "SPR32X60.ocx"
Object = "{905F077A-73D3-48CB-BCBB-2EF90EBA28A1}#1.0#0"; "EditDateCtl.ocx"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Begin VB.Form frmBankDeposits 
   Caption         =   "Cashing Routine"
   ClientHeight    =   7575
   ClientLeft      =   1575
   ClientTop       =   780
   ClientWidth     =   8400
   Icon            =   "frmBankDeposits.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   ScaleHeight     =   7575
   ScaleWidth      =   8400
   WindowState     =   2  'Maximized
   Begin CTSProgBar.ucpbProgressBar ucpbProgressBar 
      Height          =   1860
      Left            =   1560
      TabIndex        =   36
      Top             =   2040
      Visible         =   0   'False
      Width           =   5100
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      Object.Width           =   5100
      Object.Height          =   1860
   End
   Begin VB.CommandButton cmdLatestDate 
      Caption         =   "F3-Latest Date"
      Height          =   375
      Left            =   4200
      TabIndex        =   35
      Top             =   120
      Width           =   1335
   End
   Begin VB.Frame fraTotals 
      Caption         =   "Totals"
      Height          =   1575
      Left            =   3120
      TabIndex        =   18
      Top             =   2400
      Visible         =   0   'False
      Width           =   3015
      Begin VB.Label lblTotalAmount 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   5
         Left            =   1920
         TabIndex        =   28
         Top             =   1200
         Width           =   1005
      End
      Begin VB.Label lblTotalAmount 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   4
         Left            =   960
         TabIndex        =   27
         Top             =   1200
         Width           =   1005
      End
      Begin VB.Label lblTotalAmount 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   3
         Left            =   1920
         TabIndex        =   26
         Top             =   720
         Width           =   1005
      End
      Begin VB.Label lblTotalAmount 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   2
         Left            =   960
         TabIndex        =   25
         Top             =   720
         Width           =   1005
      End
      Begin VB.Label lblTotalAmount 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   1
         Left            =   1920
         TabIndex        =   24
         Top             =   480
         Width           =   1005
      End
      Begin VB.Label lblTotalAmount 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   0
         Left            =   960
         TabIndex        =   23
         Top             =   480
         Width           =   1005
      End
      Begin VB.Label labTot 
         Caption         =   "Difference "
         Height          =   255
         Index           =   3
         Left            =   200
         TabIndex        =   22
         Top             =   1240
         Width           =   1095
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000003&
         X1              =   0
         X2              =   3000
         Y1              =   1080
         Y2              =   1080
      End
      Begin VB.Label labTot 
         Caption         =   "Tendered"
         Height          =   255
         Index           =   2
         Left            =   200
         TabIndex        =   21
         Top             =   780
         Width           =   1095
      End
      Begin VB.Label labTot 
         Caption         =   "Sales "
         Height          =   255
         Index           =   1
         Left            =   200
         TabIndex        =   20
         Top             =   500
         Width           =   1095
      End
      Begin VB.Label labTot 
         Caption         =   "Current          New "
         Height          =   255
         Index           =   0
         Left            =   1320
         TabIndex        =   19
         Top             =   240
         Width           =   1575
      End
   End
   Begin VB.CommandButton cmdAcceptDate 
      Caption         =   "F7-Accept Date"
      Height          =   375
      Left            =   2640
      TabIndex        =   4
      Top             =   120
      Visible         =   0   'False
      Width           =   1455
   End
   Begin ucEditDate.ucDateText dtxtOtherDate 
      Height          =   285
      Left            =   1320
      TabIndex        =   1
      Top             =   120
      Visible         =   0   'False
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   503
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DateFormat      =   "DD/MM/YY"
      Text            =   "29/04/03"
   End
   Begin VB.CommandButton cmdOtherDates 
      Caption         =   "F4-Other Date"
      Height          =   375
      Left            =   2640
      TabIndex        =   17
      Top             =   120
      Width           =   1455
   End
   Begin VB.CommandButton cmdSetBalanced 
      Caption         =   "Ctrl-F5 Set Balanced"
      Height          =   400
      Left            =   3000
      TabIndex        =   13
      Top             =   7080
      Visible         =   0   'False
      Width           =   1800
   End
   Begin VB.Frame fraTillErrors 
      Height          =   1335
      Left            =   240
      TabIndex        =   15
      Top             =   4440
      Width           =   6375
      Begin VB.TextBox txtSignature 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Index           =   1
         Left            =   5040
         MaxLength       =   5
         PasswordChar    =   "*"
         TabIndex        =   11
         Top             =   840
         Width           =   975
      End
      Begin VB.TextBox txtReason1 
         Height          =   285
         Left            =   1560
         MaxLength       =   65
         TabIndex        =   6
         Top             =   240
         Width           =   4455
      End
      Begin VB.TextBox txtSignature 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Index           =   0
         Left            =   3000
         MaxLength       =   6
         TabIndex        =   10
         Top             =   840
         Width           =   975
      End
      Begin VB.TextBox txtReason2 
         Height          =   285
         Left            =   1560
         MaxLength       =   65
         TabIndex        =   8
         Top             =   540
         Width           =   4455
      End
      Begin VB.Label labReason 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Password"
         Height          =   255
         Index           =   4
         Left            =   3960
         TabIndex        =   34
         Top             =   860
         Width           =   975
      End
      Begin VB.Label labReason 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Payroll No"
         Height          =   255
         Index           =   3
         Left            =   1920
         TabIndex        =   33
         Top             =   860
         Width           =   975
      End
      Begin VB.Label labReason 
         BackStyle       =   0  'Transparent
         Caption         =   "Electronic Signature"
         Height          =   255
         Index           =   2
         Left            =   100
         TabIndex        =   9
         Top             =   860
         Width           =   1455
      End
      Begin VB.Label labReason 
         BackStyle       =   0  'Transparent
         Caption         =   "Margin Comments"
         Height          =   255
         Index           =   1
         Left            =   100
         TabIndex        =   7
         Top             =   560
         Width           =   1335
      End
      Begin VB.Label labReason 
         BackStyle       =   0  'Transparent
         Caption         =   "Explain Till Error"
         Height          =   255
         Index           =   0
         Left            =   100
         TabIndex        =   5
         Top             =   260
         Width           =   1335
      End
   End
   Begin VB.Frame fraReports 
      Caption         =   "Reports"
      Height          =   615
      Left            =   240
      TabIndex        =   29
      Top             =   5880
      Width           =   8055
      Begin VB.CommandButton cmdSummaryReport 
         Caption         =   "Alt-&Select Period"
         Height          =   350
         Left            =   6000
         TabIndex        =   37
         Top             =   190
         Width           =   1935
      End
      Begin VB.CommandButton cmdWeeklyReport 
         Caption         =   "Alt-&Profit"
         Height          =   350
         Left            =   3960
         TabIndex        =   32
         Top             =   190
         Width           =   1935
      End
      Begin VB.CommandButton cmdEndOfDayRep 
         Caption         =   "Alt-&Daily"
         Height          =   350
         Left            =   2160
         TabIndex        =   31
         Top             =   190
         Width           =   1695
      End
      Begin VB.CommandButton cmdDailyBankRep 
         Caption         =   "Alt-&Banking"
         Height          =   350
         Left            =   120
         TabIndex        =   30
         Top             =   190
         Width           =   1935
      End
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "F5-Save"
      Height          =   400
      Left            =   6120
      TabIndex        =   14
      Top             =   7080
      Width           =   855
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10-Exit"
      Height          =   400
      Left            =   240
      TabIndex        =   12
      Top             =   7080
      Width           =   1095
   End
   Begin FPSpreadADO.fpSpread sprdBank 
      Height          =   3735
      Left            =   240
      TabIndex        =   3
      Top             =   600
      Width           =   6495
      _Version        =   393216
      _ExtentX        =   11456
      _ExtentY        =   6588
      _StockProps     =   64
      EditModeReplace =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   5
      MaxRows         =   1
      SpreadDesigner  =   "frmBankDeposits.frx":058A
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   16
      Top             =   7200
      Width           =   8400
      _ExtentX        =   14817
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmBankDeposits.frx":0A71
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   6985
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "18:49"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblControlDate 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1320
      TabIndex        =   2
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label lblDate 
      BackStyle       =   0  'Transparent
      Caption         =   "Process date"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   1095
   End
End
Attribute VB_Name = "frmBankDeposits"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmBankDeposits
'* Date   : 15/04/03
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Bank Deposits/frmBankDeposits.frm $
'**********************************************************************************************
'* Summary: Main cashing up routine to access sub reports
'**********************************************************************************************
'* $Author: Damians $ $Date: 7/06/04 10:03 $ $Revision: 17 $
'* Versions:
'* 15/08/02    mauricem
'*             Header added.
'* 23/05/03    KeithB
'*         (A) Add F4-Other Date functionality
'*             NB The header record moCashHeader is now updated with correct date.
'*         (B) Label till error lines as 'Explain Till error' etc
'*         (C) Split till error line 3 into two 'Electronic Signature' boxes
'*         (D) Remove F9 print key
'*         (E) Display the 'Balanced' hotkey when Payroll Number & Password are
'*             entered in the 'Electronic Signature' boxes.
'*         (F) Ensure F10 works.  Add a check if a 'Save' is pending.
'*         (G) Add Totals Display
'*         (H) Remove F3-Reset Balanced - it is illogical as data saved as soon
'*             as Set Balanced is pressed & date in header is incremented.
'*         (I) Add date check file lookup to check for closed date
'* 27/05/03    Keith B
'*             Add check for next working day using -
'*                   new module modNxtWrkDay   Function GetNextOpenDay
'* 04/06/03    KeithB
'*         (A) Move reports frame to bottom of screen
'*         (B) Payroll No & Password are NOT padded with leading zeroes
'*         (C) Check if data should be saved before printing
'*         (D) Call print programs with parameter "DB=YYYYMMDD" where YYYY=year MM=month DD=day
'* 05/06/03    KeithB   Remove ability to minimise.
'* 06/06/03    KeithB   Change button caption from 'New' to 'New/Correction'
'* 12/06/03    KeithB
'*         (A) Add F3-Latest Date  F4-Other Date
'*         (B) When viewing 'Other Date' disable the spreadsheet control
'*         (C) Relabel the print keys
'*         (D) Check no previous instance of the program is running
'*
'</CAMH>***************************************************************************************
Option Explicit

Private Const MODULE_NAME As String = "frmBankDeposits"

Private Const COL_TYPE            As Long = 0
Private Const COL_CURRENT_AMOUNT  As Long = 1
Private Const COL_NEW_AMOUNT      As Long = 3
Private Const COL_AS_TENDERED     As Long = 4
Private Const COL_DIFFERENCE      As Long = 5

Private Const ROW_TOTAL = 42

Private Const PRM_DATE_CHECK_FILE As Long = 143
Private Const PRM_DAILY_MENUID    As Long = 671
Private Const PRM_EOD_MENUID      As Long = 670
Private Const PRM_WEEKLY_MENUID   As Long = 672

Private Const DISPLAY_RETRIEVED_PASSWORD As Boolean = False

Private Const F4_OTHER_DATES As String = "F4-Other Date."
' Private Const F4_LATEST_DATE As String = "F4-Latest Date"

Private Const MAX_PAYROLL_LEN As Integer = 6
Private Const MAX_PASSWRD_LEN As Integer = 5
Private Const PAD_PAYROLL_LEN As Integer = 20
Private Const PAD_PASSWRD_LEN As Integer = 5
Private Const MAX_COMMENT_LEN As Integer = 30

Private Enum enElectonicSignature
    enEsPayRollNo = 0
    enEsPassword = 1
End Enum

Dim moCashControl       As Object
Dim moCashHeader        As Object
Dim mblnCreate          As Boolean
Dim mdteControlDate     As Date
Dim moDailyMenuItem     As Object
Dim moEODMenuItem       As Object
Dim moWeeklyMenuItem    As Object
Dim mlngPayAmtDecPlaces As Long   'hold number of decimal places to show for pay amount
Dim mstrValueFormat     As String
Dim mstrDateCheckFile   As String ' hold path & file name of DOS date check file
Dim mstrMaxValidCashier As String

Private Enum enTotalType
    enttCurrentSales = 0
    enttNewSales
    enttCurrentTendered
    enttNewTendered
    enttCurrentDifference
    enttNewDifference
End Enum
'
Private Sub cmdAcceptDate_Click()
    cmdAcceptDate.Tag = "Y"
End Sub

Private Sub cmdDailyBankRep_Click()

Dim lngMenuID As Long
Dim strParam  As String

    ' 04/06/03 KVB check if data should be saved first
    If CheckIfContinueAllowed("Are you sure you want to print?") = False Then
        Exit Sub
    End If
    
    On Error GoTo cmdDailyBankRep_Err
    
    If moDailyMenuItem Is Nothing = True Then
        lngMenuID = goSession.GetParameter(PRM_DAILY_MENUID)
        Set moDailyMenuItem = goDatabase.CreateBusinessObject(CLASSID_MENU)
        Call moDailyMenuItem.IBo_AddLoadFilter(CMP_EQUAL, FID_MENU_ID, lngMenuID)
        Call moDailyMenuItem.IBo_LoadMatches
    End If
        
    strParam = "DB=" & Format$(mdteControlDate, "YYYYMMDD") & ",DCS"
    
    ' Call CallReport(moDailyMenuItem.ExecPath, _
                    moDailyMenuItem.Parameters, _
                    moDailyMenuItem.WaitEXE, moDailyMenuItem.AddPath, moDailyMenuItem.SendSession)
    
    If sprdBank.Enabled = True Then
        sprdBank.SetFocus
        Call sprdBank.SetActiveCell(COL_NEW_AMOUNT, 1)
    End If
    
    Call CallReport(moDailyMenuItem.ExecPath, _
                    strParam, _
                    moDailyMenuItem.WaitEXE, moDailyMenuItem.AddPath, moDailyMenuItem.SendSession)
    
    Exit Sub
    
cmdDailyBankRep_Err:
    Call ShowReportErr(Err.Number, Err.Description, "Daily Banking Report")
    
End Sub

Private Sub cmdEndOfDayRep_Click()

Dim lngMenuID As Long
Dim strParam  As String

    ' 04/06/03 KVB check if data should be saved first
    If CheckIfContinueAllowed("Are you sure you want to print?") = False Then
        Exit Sub
    End If

    On Error GoTo cmdEndOfDayRep_Err
    
    If moEODMenuItem Is Nothing = True Then
        lngMenuID = goSession.GetParameter(PRM_EOD_MENUID)
        Set moEODMenuItem = goDatabase.CreateBusinessObject(CLASSID_MENU)
        Call moEODMenuItem.IBo_AddLoadFilter(CMP_EQUAL, FID_MENU_ID, lngMenuID)
        Call moEODMenuItem.IBo_LoadMatches
    End If
    
    strParam = "DB=" & Format$(mdteControlDate, "YYYYMMDD") & ",DCS"
    
    'Call CallReport(moEODMenuItem.ExecPath, _
                    moEODMenuItem.Parameters, _
                    moEODMenuItem.WaitEXE, moEODMenuItem.AddPath, moEODMenuItem.SendSession)
                    
    If sprdBank.Enabled = True Then
        sprdBank.SetFocus
        Call sprdBank.SetActiveCell(COL_NEW_AMOUNT, 1)
    End If
    
    Call CallReport(moEODMenuItem.ExecPath, _
                    strParam, _
                    moEODMenuItem.WaitEXE, moEODMenuItem.AddPath, moEODMenuItem.SendSession)
    Exit Sub
    
cmdEndOfDayRep_Err:
        Call ShowReportErr(Err.Number, Err.Description, "End of Day Report")
        
End Sub

Private Sub cmdExit_Click()

    If CheckIfContinueAllowed("Are you sure you want to Exit?") = False Then
        Exit Sub
    End If

    cmdSave.Visible = False

    Call Unload(Me)
    End

End Sub

Private Sub cmdlatestDate_Click()
    
    ' "latest date" wanted again
    cmdAcceptDate.Tag = "Latest"
    
    Set moCashHeader = Nothing
    Call LoadHeader ' refresh the header
    lblControlDate.Caption = DisplayDate(moCashHeader.CurrentDate, False)
    Set moCashControl = Nothing
    Call LoadControlRecord(moCashHeader.CurrentDate)
    cmdOtherDates.Caption = F4_OTHER_DATES
    Call SetGridLockAndDisplay(False, True, True)
    sprdBank.SetFocus
    
    Call DisplayTotals(moCashHeader.CurrentDate)

End Sub

Private Sub cmdOtherDates_Click()

Dim dteNewDate  As Date


    If cmdOtherDates.Caption = F4_OTHER_DATES Then
    
        If CheckIfContinueAllowed("Are you sure you want to change the date?") = False Then
            Exit Sub
        End If
        Call ShowOrHide(False)
        
        dtxtOtherDate.Visible = True
        'cmdAcceptDate.Left = cmdOtherDates.Left
        cmdAcceptDate.Tag = "N"
        cmdAcceptDate.Visible = True
        Call dtxtOtherDate.SetFocus
        
        Do
            DoEvents
        Loop Until cmdAcceptDate.Tag = "Y" Or cmdAcceptDate.Tag = "Latest"
        
        cmdAcceptDate.Visible = False
        
        ' if F3 was clicked the requirement is for latest date again.
        If cmdAcceptDate.Tag = "Latest" Then
            Exit Sub
        End If
                
        dteNewDate = GetDate(dtxtOtherDate.Text)
        
        lblControlDate.Caption = dtxtOtherDate.Text
        Set moCashControl = Nothing
        Call LoadControlRecord(dteNewDate)
        dtxtOtherDate.Visible = False
        
        If dteNewDate <> moCashHeader.CurrentDate Then
            ' we ARE working on an old date
            ' so do NOT Allow amendment
            Call SetGridLockAndDisplay(True, False, False)
            
            cmdSave.Visible = False
            ' cmdOtherDates.Caption = F4_LATEST_DATE
            Call cmdDailyBankRep.SetFocus
            
        Else
            ' the date has been left at CurrentDate or
            ' changed to CurrentDate
            Call SetGridLockAndDisplay(False, True, True)
            sprdBank.SetFocus
        End If

    Else
        ' "latest date" wanted again
        sprdBank.Col = COL_DIFFERENCE
        sprdBank.ColHidden = False
                
        sprdBank.ColHidden = True
        Set moCashHeader = Nothing
        Call LoadHeader ' refresh the header
        lblControlDate.Caption = DisplayDate(moCashHeader.CurrentDate, False)
        Set moCashControl = Nothing
        Call LoadControlRecord(moCashHeader.CurrentDate)
        cmdOtherDates.Caption = F4_OTHER_DATES
        Call SetGridLockAndDisplay(False, True, True)
        sprdBank.SetFocus
    End If
    Call sprdBank.SetActiveCell(0, 0)
        
End Sub

Private Sub cmdResetDay_Click()
    
    ' Reset the Balanced flag
    moCashControl.DayBalanced = False

End Sub

Private Sub cmdSave_Click()

Dim dteControlDate As Date

    dteControlDate = GetDate(lblControlDate.Caption)
    
    Call FillCashControlFields(dteControlDate, txtReason1.Text, txtReason2.Text, _
                               txtSignature(enEsPayRollNo).Text, _
                               txtSignature(enEsPassword).Text)

    Call UpdateCashControl
    
    ' moCashHeader.CurrentDate = GetDate(lblControlDate.Caption)
    ' Call moCashHeader.IBo_SaveIfExists
    
    sbStatus.Panels(PANEL_INFO).Text = "Data for " & lblControlDate.Caption & " SAVED. " & _
                                       "[" & Format$(Now, mstrDateFmt & " HH:NN:SS") & "]"
    
    cmdSave.Visible = False
    
End Sub

Public Sub CallReport(ByVal strExecPath As String, _
                      ByVal strParams As String, _
                      ByVal blnWaitForEXE As Boolean, _
                      ByVal blnAddPath As Boolean, _
                      ByVal blnSendSession As Boolean)

On Error GoTo Bad_Report

Const SW_MAX As Long = 9

Dim lngProcessID As Long
        
    If InStr(strParams, "${D}") > 0 Then
        strParams = Left$(strParams, InStr(strParams, "${D}") - 1) & Format$(Date, "YYYYMMDD") & Mid$(strParams, InStr(strParams, "${D}") + 4)
    End If
    If blnAddPath = True Then strExecPath = App.Path & "\" & strExecPath
    If blnSendSession = True Then
        strExecPath = strExecPath & " " & goSession.CreateCommandLine(strParams)
    Else
        strExecPath = strExecPath & " " & strParams
    End If
    Call DebugMsg(MODULE_NAME, "callMenuOption", endlDebug, strExecPath)
    If blnWaitForEXE = True Then
        Call ShellWait(strExecPath, SW_MAX)
    Else
        lngProcessID = Shell(strExecPath, vbNormalFocus)
    End If
    Exit Sub
    
Bad_Report:

    Call MsgBoxEx("Invalid set-up for call report option" & vbCrLf & "  " & strExecPath & vbCrLf & Err.Number & "-" & Err.Description, vbExclamation, "Select Report Option", , , , , RGBMsgBox_WarnColour)
    Call Err.Clear
    Resume Next
    
End Sub

Private Sub cmdSetBalanced_Click()

Dim dteControlDate  As Date
Dim dteNextDateOpen As Date
Dim intPnt          As Integer
Dim strMessage      As String
Dim intResponse     As Integer
Dim intSetFocus     As Integer

    cmdSetBalanced.Enabled = False
    Screen.MousePointer = vbHourglass
    strMessage = vbNullString
    
    txtSignature(enEsPassword).Text = Trim$(txtSignature(enEsPassword).Text)
    txtSignature(enEsPayRollNo).Text = Trim$(txtSignature(enEsPayRollNo).Text)
    
    If LenB(txtSignature(enEsPassword).Text) = 0 Then
        intSetFocus = enEsPassword
        strMessage = "Password MUST be entered"
    End If
    If LenB(txtSignature(enEsPayRollNo).Text) = 0 Then
        intSetFocus = enEsPayRollNo
        strMessage = "Payroll no MUST be entered"
    End If
        
    ' Check the password
    If LenB(strMessage) = 0 Then
        Call CheckUserPassWord(intSetFocus, strMessage)
    End If

    cmdSetBalanced.Enabled = True
    Screen.MousePointer = Default
        
    On Error Resume Next
    'Commented out by M. Milne to remove validation of user info -requested by C hampson
'    If LenB(strMessage) <> 0 Then
'        Call MsgBox(strMessage, vbInformation, "Unable to set balanced")
'        txtSignature(intSetFocus).SetFocus
'        Exit Sub
'    End If
    

    strMessage = "Please confirm that you wish to set these totals as 'balanced'." & vbCrLf & vbCrLf & _
                 "NOTE: If you click the 'yes' button -" & vbCrLf & vbCrLf & _
                    Space$(12) & "The data for " & lblControlDate.Caption & " will be saved and" & vbCrLf & _
                    Space$(12) & "the program will move on to the next working day." & vbCrLf & vbCrLf & _
                    Space$(12) & "You will NOT be able to change the totals for " & lblControlDate.Caption & "." & vbCrLf & vbCrLf & _
                 "Do you wish to continue?"
    intResponse = MsgBoxEx(strMessage, vbQuestion + vbYesNo + vbDefaultButton2, "SET AS BALANCED", , , , , RGBMSGBox_PromptColour)
    If intResponse = vbNo Then
        Exit Sub
    End If

    dteControlDate = GetDate(lblControlDate.Caption)
    
    Call FillCashControlFields(dteControlDate, txtReason1.Text, txtReason2.Text, _
                               txtSignature(enEsPayRollNo).Text, _
                               txtSignature(enEsPassword).Text)
    
    ' set the Balanced flag
    moCashControl.DayBalanced = True
    moCashControl.CommNumber = "0000"

    Call UpdateCashControl
    
    sbStatus.Panels(PANEL_INFO).Text = "Data for " & lblControlDate.Caption & " set as BALANCED"
    
    On Error Resume Next
    cmdSave.Visible = False
    cmdSetBalanced.Visible = False
    
    ' Having updated the control record, we need to
    ' (a) increment the date in the header record
    ' (b) update the header record
    ' (c) write a new blank control record
    ' ==============================================
    
    ' (a) increment the date in the header record
    '     ========================================
    '     NB moCashHeader.CurrentDate is a date field!
    '        move the date forward avoiding -
    '        (i) days the store is closed.
    '       (ii) Bank Holidays etc as held in the file Date.Chk
    ' Call SetNextOpenDay(moCashHeader.CurrentDate, dteNextDateOpen)
    dteNextDateOpen = GetNextOpenDay(moCashHeader.CurrentDate)
    
    ' put the new date into the Header field 'Current date'
    moCashHeader.CurrentDate = dteNextDateOpen
    
    ' (b) update the header record
    '     ========================
    Call moCashHeader.IBo_SaveIfExists
    
    ' (c) write a new blank Control Record moCashControl
    '     ================================ =============
    Call FillCashControlFields(dteNextDateOpen, vbNullString, vbNullString, vbNullString, vbNullString)
    
    ' the Balanced flag MUST be set to False
    moCashControl.DayBalanced = False
    moCashControl.CommNumber = "0000"
    For intPnt = 1 To 10 Step 1
        moCashControl.OtherIncome(intPnt) = 0
        moCashControl.MiscOutgoings(intPnt) = 0
    Next intPnt
    moCashControl.TotalDeposits = 0
    moCashControl.TotalVouchers = 0
    moCashControl.TotalFloats = 0
    
    For intPnt = 1 To 40 Step 1
        moCashControl.BankingAmount(intPnt) = 0
        moCashControl.BankSlipNumbers(intPnt) = vbNullString
    Next intPnt
    
    Call moCashControl.IBo_SaveIfNew
    
    sbStatus.Panels(PANEL_INFO).Text = sbStatus.Panels(PANEL_INFO).Text & " Date Incremented"
    
    ' now reread everything ready to go again
    On Error Resume Next
    
    Set moCashHeader = Nothing
    Set moCashControl = Nothing
    
    Call LoadHeader
    
    lblControlDate.Caption = DisplayDate(moCashHeader.CurrentDate, False)
    Call LoadControlRecord(moCashHeader.CurrentDate)
    
    Call DisplayTotals(moCashHeader.CurrentDate)
    sprdBank.SetFocus
    
End Sub

Private Sub cmdWeeklyReport_Click()

Dim lngMenuID As Long
Dim strParam  As String

    ' 04/06/03 KVB check if data should be saved first
    If CheckIfContinueAllowed("Are you sure you want to print?") = False Then
        Exit Sub
    End If

    On Error GoTo cmdWeeklyReport_Err
    
    If moWeeklyMenuItem Is Nothing = True Then
        lngMenuID = goSession.GetParameter(PRM_WEEKLY_MENUID)
        Set moWeeklyMenuItem = goDatabase.CreateBusinessObject(CLASSID_MENU)
        Call moWeeklyMenuItem.IBo_AddLoadFilter(CMP_EQUAL, FID_MENU_ID, lngMenuID)
        Call moWeeklyMenuItem.IBo_LoadMatches
    End If
    
    strParam = "DB=" & Format$(mdteControlDate, "YYYYMMDD") & ",DCS"
    
    If sprdBank.Enabled = True Then
        sprdBank.SetFocus
        Call sprdBank.SetActiveCell(COL_NEW_AMOUNT, 1)
    End If
    
    Call CallReport(moWeeklyMenuItem.ExecPath, _
                    strParam, _
                    moWeeklyMenuItem.WaitEXE, moWeeklyMenuItem.AddPath, moWeeklyMenuItem.SendSession)
    Exit Sub
    
cmdWeeklyReport_Err:
    Call ShowReportErr(Err.Number, Err.Description, "Weekly Profit Report")
End Sub

Private Sub cmdSummaryReport_Click()

Dim lngMenuID As Long
Dim strParam  As String

    On Error GoTo cmdSummaryReport_Err
    
    If moWeeklyMenuItem Is Nothing = True Then
        lngMenuID = goSession.GetParameter(PRM_WEEKLY_MENUID)
        Set moWeeklyMenuItem = goDatabase.CreateBusinessObject(CLASSID_MENU)
        Call moWeeklyMenuItem.IBo_AddLoadFilter(CMP_EQUAL, FID_MENU_ID, lngMenuID)
        Call moWeeklyMenuItem.IBo_LoadMatches
    End If
    
    strParam = "SR"
    
    If sprdBank.Enabled = True Then
        sprdBank.SetFocus
        Call sprdBank.SetActiveCell(COL_NEW_AMOUNT, 1)
    End If
    
    Call CallReport(moWeeklyMenuItem.ExecPath, _
                    strParam, _
                    moWeeklyMenuItem.WaitEXE, moWeeklyMenuItem.AddPath, moWeeklyMenuItem.SendSession)
    Exit Sub
    
cmdSummaryReport_Err:
    Call ShowReportErr(Err.Number, Err.Description, "Summary Profit Report")
End Sub

Private Sub dtxtOtherDate_KeyDown(KeyCode As Integer, Shift As Integer)
    'Debug.Print KeyCode
    If KeyCode = vbKeyReturn Then
        cmdAcceptDate.Tag = "Y"
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    Select Case Shift
    Case Is = 0
        Select Case KeyCode
        Case vbKeyF3:
'            If cmdResetDay.Visible = True And cmdResetDay.Enabled = True Then
'                Call cmdResetDay_Click
'            End If
            If cmdLatestDate.Visible = True Then
                Call cmdlatestDate_Click
            End If
                
        Case vbKeyF4:
            If cmdOtherDates.Visible = True Then
                Call cmdOtherDates_Click
            End If
        Case vbKeyF5:
            If cmdSave.Visible = True Then
                Call cmdSave_Click
            End If
        Case vbKeyF7:
            If cmdAcceptDate.Visible = True Then Call cmdAcceptDate_Click
        Case vbKeyF10
            Call cmdExit_Click
        End Select
    
    Case Is = 2 ' ctrl pressed
        Select Case KeyCode
        Case vbKeyF5
            If cmdSetBalanced.Visible = True Then
                cmdSetBalanced_Click
            End If
        End Select
    End Select
End Sub

Private Sub Form_Load()

Dim oLookUp As Object
    
    If App.PrevInstance = True Then
        Call MsgBoxEx("Previous Bank Deposits program running," & vbCrLf & vbCrLf & _
                    "Only one instance of this program can be run at any time.", _
                    vbExclamation, "CTS - Bank Deposits", , , , , RGB_RED)
        Call BringAppToFront
        End
    End If

    Call GetRoot
    
    Call InitialiseStatusBar(sbStatus)
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
    RGBMSGBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
    
    mstrDateCheckFile = goSession.GetParameter(PRM_DATE_CHECK_FILE)
    
    ' get display format for pounds & pence from param file
    mlngPayAmtDecPlaces = goSession.GetParameter(PRM_VALUE_DEC_PLACES)
    mstrValueFormat = String$(mlngPayAmtDecPlaces, "0") ' Replace(Space(mlngPayAmtDecPlaces), " ", "0")
    If LenB(mstrValueFormat) <> 0 Then
        mstrValueFormat = "0." & mstrValueFormat
    End If

    mstrDateFmt = UCase$(goSession.GetParameter(PRM_DATE_FORMAT))
    
    txtSignature(enEsPayRollNo).MaxLength = MAX_PAYROLL_LEN
    txtSignature(enEsPassword).MaxLength = MAX_PASSWRD_LEN
    
    'Retrieve last Cashier ID, all cashiers above this are Training Codes
    Set oLookUp = goDatabase.CreateBusinessObject(CLASSID_RETAILEROPTIONS)
    Call oLookUp.IBo_AddLoadField(FID_RETAILEROPTIONS_MaxValidCashierNumber)
    Call oLookUp.IBo_LoadMatches
    mstrMaxValidCashier = oLookUp.MaxValidCashierNumber
    Set oLookUp = Nothing
    
    Call LoadHeader
    
    lblControlDate.Caption = DisplayDate(moCashHeader.CurrentDate, False)
    
    Call LoadControlRecord(moCashHeader.CurrentDate)
    
    Call DisplayTotals(moCashHeader.CurrentDate)
    cmdOtherDates.Caption = F4_OTHER_DATES
    
End Sub

Private Sub LoadHeader()

Dim lngTenderNo As Long

    ' Table CBSHDR - CLASSID_CASHBALANCEHDR
    Set moCashHeader = goDatabase.CreateBusinessObject(CLASSID_CASHBALANCEHDR)
    
    Call moCashHeader.IBo_Load
    
    sprdBank.ReDraw = False
    'Display different tender types on table
    sprdBank.MaxRows = 0
    For lngTenderNo = 1 To 40 Step 1
        sprdBank.MaxRows = sprdBank.MaxRows + 1
        sprdBank.Row = sprdBank.MaxRows
        sprdBank.Col = COL_TYPE
        sprdBank.Text = moCashHeader.TenderType(lngTenderNo)
        sprdBank.Col = COL_AS_TENDERED
        sprdBank.Text = 0
        sprdBank.Col = COL_AS_TENDERED
        sprdBank.Text = 0
        sprdBank.Col = COL_DIFFERENCE
        sprdBank.Text = 0
    Next lngTenderNo

    sprdBank.MaxRows = sprdBank.MaxRows + 3
    sprdBank.RowHeight(sprdBank.MaxRows - 2) = 1
    sprdBank.Row = sprdBank.MaxRows
    sprdBank.Col = -1
    sprdBank.Lock = True
    sprdBank.Row = sprdBank.MaxRows - 1
    sprdBank.Col = -1
    sprdBank.Lock = True
    sprdBank.Row = sprdBank.MaxRows - 2
    sprdBank.Col = -1
    sprdBank.Lock = True
    sprdBank.Row = sprdBank.MaxRows - 1
    sprdBank.Col = COL_TYPE
    sprdBank.Text = "TOTALS"
    sprdBank.Col = COL_CURRENT_AMOUNT
    sprdBank.Formula = "SUM(" & ColToText(COL_CURRENT_AMOUNT) & "1:" & ColToText(COL_CURRENT_AMOUNT) & "40)"
    sprdBank.Col = COL_NEW_AMOUNT
    sprdBank.Formula = "SUM(" & ColToText(COL_NEW_AMOUNT) & "1:" & ColToText(COL_NEW_AMOUNT) & "40)"
    sprdBank.Col = COL_AS_TENDERED
    sprdBank.Formula = "SUM(" & ColToText(COL_AS_TENDERED) & "1:" & ColToText(COL_AS_TENDERED) & "40)"
    sprdBank.Col = COL_DIFFERENCE
    sprdBank.Formula = "SUM(" & ColToText(COL_DIFFERENCE) & "1:" & ColToText(COL_DIFFERENCE) & "40)"
    sprdBank.RowHeight(sprdBank.MaxRows) = 1
    
    'hide all rows with no tender type descriptions
    For lngTenderNo = 1 To 40 Step 1
        sprdBank.Row = lngTenderNo
        If LenB(moCashHeader.TenderType(lngTenderNo)) = 0 Then
            sprdBank.RowHidden = True
        Else
            sprdBank.Col = COL_DIFFERENCE
            sprdBank.Formula = "C" & lngTenderNo & "-D" & lngTenderNo
        End If
    Next lngTenderNo
    sprdBank.ReDraw = True

End Sub

Private Sub LoadControlRecord(ByVal dteDate As Date)

Dim lngTenderNo        As Long
Dim strSignaturePadded As String

    ' read CBSCTL records for dteDate
    ' Cash balance Control - CLASSID_CASHBALANCECONTROL
    ' N.B.  There may be no records.
    
    mdteControlDate = dteDate
    
    Set moCashControl = goDatabase.CreateBusinessObject(CLASSID_CASHBALANCECONTROL)
    
    Call moCashControl.IBo_AddLoadFilter(CMP_EQUAL, FID_CASHBALANCECONTROL_ControlDate, dteDate)
    
    Call moCashControl.IBo_LoadMatches
    
    ' moCashControl.ControlDate will contain -
    '    a valid date e.g. 21/03/03 if records exist
    '    00:00:00 if there are NO records for this date.
    
    If Year(moCashControl.ControlDate) > 1899 Then
        txtReason1.Text = moCashControl.Comment1
        txtReason2.Text = moCashControl.Comment2
        
        strSignaturePadded = Left$(moCashControl.Comment3 & _
                             Space$(MAX_COMMENT_LEN), MAX_COMMENT_LEN)
        
        txtSignature(enEsPayRollNo).Text = Trim$(Left$(strSignaturePadded, 6))
        
        If DISPLAY_RETRIEVED_PASSWORD = True Then
            txtSignature(enEsPassword).Text = Trim$(Mid$(strSignaturePadded, PAD_PAYROLL_LEN + 1, MAX_PASSWRD_LEN))
        Else
            txtSignature(enEsPassword).Text = vbNullString
        End If
        
        For lngTenderNo = 1 To 40 Step 1
            sprdBank.Row = lngTenderNo
            sprdBank.Col = COL_CURRENT_AMOUNT
            sprdBank.Text = moCashControl.BankingAmount(lngTenderNo)
            sprdBank.Col = COL_NEW_AMOUNT
            sprdBank.Text = moCashControl.BankingAmount(lngTenderNo)
        Next lngTenderNo
        
        mblnCreate = False
    Else
        For lngTenderNo = 1 To 40 Step 1
            sprdBank.Row = lngTenderNo
            sprdBank.Col = COL_CURRENT_AMOUNT
            sprdBank.Text = 0
            sprdBank.Col = COL_NEW_AMOUNT
            sprdBank.Text = 0
        Next lngTenderNo
        txtReason1.Text = vbNullString
        txtReason2.Text = vbNullString
        txtSignature(enEsPayRollNo).Text = vbNullString
        txtSignature(enEsPassword).Text = vbNullString
        mblnCreate = True
    End If
    
    fraTotals.Caption = "  Totals for " & DisplayDate(dteDate, False) & "  "
    
    Call GetSalesTotal(dteDate)
    Call CalculateTotals(COL_CURRENT_AMOUNT)
    Call CalculateTotals(COL_NEW_AMOUNT)
    
    Call ShowOrHide(True)
    cmdSave.Visible = False
    
    dtxtOtherDate.Visible = False
    
'    If moCashControl.DayBalanced = True Then
'        cmdResetDay.Enabled = True
'    Else
'        cmdResetDay.Enabled = False
'    End If

    Call sprdBank.SetActiveCell(COL_NEW_AMOUNT, 1)

End Sub

Private Sub Form_Resize()

Const MINIMUM_SIZE        As Long = 7600
Const VERTICAL_SEPERATION As Long = 120

Dim dblTotWidth            As Long

    If Me.WindowState = vbMinimized Then
        Exit Sub
    End If
    
    If Me.Width < MINIMUM_SIZE Then
        Me.Width = MINIMUM_SIZE
        Exit Sub
    End If
    
    If Me.Height < MINIMUM_SIZE Then
        Me.Height = MINIMUM_SIZE
        Exit Sub
    End If
    
    sprdBank.Width = Me.Width - (sprdBank.Left * 2)
    sprdBank.Height = Me.Height - sprdBank.Top - fraReports.Height - fraTillErrors.Height - cmdExit.Height - 480 - sbStatus.Height - cmdExit.Height
    
    ' fraTillErrors.Top = fraReports.Top + fraReports.Height + VERTICAL_SEPERATION
    fraTillErrors.Top = sprdBank.Top + sprdBank.Height + VERTICAL_SEPERATION
    fraTillErrors.Width = sprdBank.Width
    
    ' fraReports.Top = sprdBank.Top + sprdBank.Height + VERTICAL_SEPERATION
    fraReports.Top = fraTillErrors.Top + fraTillErrors.Height + VERTICAL_SEPERATION
    fraReports.Width = sprdBank.Width
    ' fraReports.Visible = True
    
    'cmdExit.Top = fraTillErrors.Top + fraTillErrors.Height + VERTICAL_SEPERATION
    cmdExit.Top = fraReports.Top + fraReports.Height + VERTICAL_SEPERATION
    cmdSave.Top = cmdExit.Top
    cmdSetBalanced.Top = cmdExit.Top
    'cmdPrint.Top = cmdExit.Top
        
    txtReason1.Width = fraTillErrors.Width - (txtReason1.Left * 2)
    txtReason2.Width = txtReason1.Width
    ' txtReason3.Width = txtReason1.Width
    
    dblTotWidth = cmdDailyBankRep.Width + cmdEndOfDayRep.Width + cmdWeeklyReport.Width + cmdSummaryReport.Width
    dblTotWidth = (fraReports.Width - dblTotWidth) / 8 'split into 6 equal spaces
    cmdDailyBankRep.Left = dblTotWidth
    cmdEndOfDayRep.Left = cmdDailyBankRep.Left + cmdDailyBankRep.Width + (dblTotWidth * 2)
    cmdWeeklyReport.Left = cmdEndOfDayRep.Left + cmdEndOfDayRep.Width + (dblTotWidth * 2)
    cmdSummaryReport.Left = cmdWeeklyReport.Left + cmdSummaryReport.Width + (dblTotWidth * 2)
    cmdSave.Left = sprdBank.Width + sprdBank.Left - cmdSave.Width
    'cmdPrint.Left = cmdSave.Left

    fraTotals.Left = sprdBank.Left + sprdBank.Width - (fraTotals.Width + 300)
    fraTotals.Top = sprdBank.Top + sprdBank.Height - (fraTotals.Height + 300)
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    If CheckIfContinueAllowed("Are you sure you want to Exit?") = False Then
        Cancel = True
    End If

End Sub

Private Sub sprdBank_Change(ByVal Col As Long, ByVal Row As Long)
    ' fired after press enter or tab to next field
    Call CalculateTotals(Col)
End Sub

Private Sub sprdBank_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
    
    
    If ChangeMade = True Then
        cmdSave.Visible = True
    End If

End Sub

Private Sub sprdBank_KeyDown(KeyCode As Integer, Shift As Integer)

    Debug.Print sprdBank.ActiveCol

    Select Case KeyCode
    Case Is = vbKeyReturn
        ' enter pressed so we want to go down 1 row
        If sprdBank.ActiveRow = sprdBank.MaxRows Then
            ' already at the end of the table
            Call ChangeColumnOrTabOut
        Else
            sprdBank.Row = sprdBank.ActiveRow + 1
            If sprdBank.RowHidden = True Then
                ' next row is hidden so can't go down
                Call ChangeColumnOrTabOut
            Else
                ' row is visible so imitate down arrow key press
                Call SendKeys("{DOWN}")
                'Call sprdBank.SetActiveCell(sprdBank.ActiveCol, sprdBank.Active + 1)
            End If
        End If
        
    Case Is = vbKeyEscape
        ' escape pressed so we want to go up 1
        If sprdBank.ActiveRow = 1 Then
            If sprdBank.ActiveCol = COL_NEW_AMOUNT Then
                ' go back one column
                Call sprdBank.SetActiveCell(COL_CURRENT_AMOUNT, 1)
            Else
                ' already at top of grid
                Call SendKeys("+{tab}")
            End If
        Else
            ' imitate up arrow key press
            Call SendKeys("{UP}")
        End If
    End Select
        
End Sub




Private Sub txtReason1_Change()

    cmdSave.Visible = True

End Sub

Private Sub txtReason1_GotFocus()

    txtReason1.BackColor = RGBEdit_Colour
    txtReason1.SelStart = 0
    txtReason1.SelLength = Len(txtReason1.Text)

End Sub

Private Sub txtReason1_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then Call SendKeys(vbTab)

End Sub

Private Sub txtReason1_LostFocus()

    txtReason1.BackColor = RGB_WHITE

End Sub

Private Sub txtReason2_Change()

    cmdSave.Visible = True

End Sub

Private Sub txtReason2_GotFocus()

    txtReason2.BackColor = RGBEdit_Colour
    txtReason2.SelStart = 0
    txtReason2.SelLength = Len(txtReason2.Text)

End Sub
Private Sub txtReason2_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then SendKeys (vbTab)
    If KeyAscii = vbKeyEscape Then SendKeys ("+{tab}")

End Sub

Private Sub txtReason2_LostFocus()

    txtReason2.BackColor = RGB_WHITE

End Sub




Private Sub ShowOrHide(ByVal blnTrueOrFalse As Boolean)
    
    sprdBank.Visible = blnTrueOrFalse
    
    fraReports.Visible = blnTrueOrFalse
    fraTillErrors.Visible = blnTrueOrFalse
'    fraTotals.Visible = blnTrueOrFalse
    
    cmdSave.Visible = blnTrueOrFalse
    
    ' KVB 23/05/03 Remove F3-Reset Balanced button.
    'cmdResetDay.Visible = blnTrueOrFalse
    
    lblControlDate.Visible = blnTrueOrFalse

End Sub

Private Sub txtSignature_Change(Index As Integer)
    
    cmdSave.Visible = True
    
End Sub


Private Sub txtSignature_GotFocus(Index As Integer)
    
    txtSignature(Index).BackColor = RGBEdit_Colour
    txtSignature(Index).SelStart = 0
    txtSignature(Index).SelLength = Len(txtSignature(Index).Text)

End Sub


Private Sub txtSignature_KeyPress(Index As Integer, KeyAscii As Integer)
    
    txtSignature(Index).BackColor = RGBEdit_Colour
    
    Select Case KeyAscii
    Case vbKey0 To vbKey9
        '  acceptable numeric keys
    Case vbKeyBack, vbKeyTab, vbKeyDelete
        ' acceptable back & delete keys
    Case vbKeyReturn
        ' return pressed so tab on
        Call SendKeys("{tab}")
    Case vbKeyEscape
        ' escape pressed do tab back
        Call SendKeys("+{tab}")
    Case Else
        ' not acceptable keypress - discard.
        ' Debug.Print Time$, KeyAscii
        KeyAscii = 0
        txtSignature(Index).BackColor = RGB_RED
        Beep
    End Select
    
End Sub

Private Sub txtSignature_LostFocus(Index As Integer)
    
Dim blnValidSignature As Boolean
Dim strMessage        As String

    txtSignature(Index).BackColor = RGB_WHITE
    
    cmdSetBalanced.Visible = False
    
    Call CheckForValidSignature(Index, Index, blnValidSignature, True)
    
    If blnValidSignature = False Then
        strMessage = "Invalid "
        If Index = enEsPayRollNo Then
            strMessage = strMessage & "payroll number."
        Else
            strMessage = strMessage & "password"
        End If
        
        Call MsgBoxEx(strMessage, vbInformation, "Error in electronic signature", , , , , RGBMsgBox_WarnColour)
        txtSignature(Index).Text = vbNullString
        txtSignature(Index).SetFocus
        Exit Sub
    End If
    
    ' now check both signatures - do not allow nulls
    Call CheckForValidSignature(0, 1, blnValidSignature, False)
    
    If blnValidSignature = True Then
        ' Debug.Print "txtSignature_LostFocus"
        cmdSetBalanced.Visible = True
    End If
    
End Sub



Private Sub CheckForValidSignature(ByVal intFirstIndex As Integer, _
                                   ByVal intLastIndex As Integer, _
                                   ByRef blnValidSignature As Boolean, _
                                   ByRef blnAllowNull As Boolean)
Dim intSignature As Integer
    
    blnValidSignature = True
    
    For intSignature = intFirstIndex To intLastIndex Step 1
    
        If blnAllowNull = True And LenB(Trim$(txtSignature(intSignature).Text)) = 0 Then
            ' OK - we allow a null field
        Else
            If Val(txtSignature(intSignature).Text) <= 0 Then
                blnValidSignature = False
            End If
        End If
        
    Next intSignature
  
End Sub

Private Function CheckIfContinueAllowed(ByVal strMessageLine As String) _
                                        As Boolean

Dim strMessage  As String
Dim intResponse As Integer
    
    If cmdSave.Visible = True Then
        strMessage = "Changes made will be LOST" & vbCrLf & _
                     "unless you save first." & vbCrLf & vbCrLf & _
                     strMessageLine & vbCrLf
        intResponse = MsgBoxEx(strMessage, vbQuestion + vbYesNo + vbDefaultButton2, "WARNING", , , , , RGBMsgBox_WarnColour)
        If intResponse = vbNo Then
            CheckIfContinueAllowed = False
            Exit Function
        End If
    End If

    CheckIfContinueAllowed = True
    
End Function

Private Sub ChangeColumnOrTabOut()
    
    If sprdBank.ActiveCol = COL_CURRENT_AMOUNT Then
        ' go right to next the next column
        Call sprdBank.SetActiveCell(COL_NEW_AMOUNT, 1)
    Else
        '  tab out instead
        Call SendKeys("{tab}")
    End If
End Sub

Private Sub ShowReportErr(lngErrNumber As Long, strErrDescription As String, _
                          strReportName As String)
Dim strMessage As String

    strMessage = "Unable to run " & strReportName & vbCrLf & vbCrLf & _
                "Error number " & lngErrNumber & vbCrLf & _
                strErrDescription
    
    Call MsgBoxEx(strMessage, vbInformation, strReportName, , , , , RGBMsgBox_WarnColour)
    
End Sub

Private Sub CalculateTotals(ByVal lngCol As Long)

Dim lngRow        As Long
Dim dblTot        As Double
Dim dblDifference As Double
Dim intSales      As Integer
Dim intTendered   As Integer
Dim intDifference As Integer

    If lngCol = COL_NEW_AMOUNT Or lngCol = COL_CURRENT_AMOUNT Then
    
        lngRow = sprdBank.Row
        sprdBank.Row = ROW_TOTAL
        sprdBank.Col = lngCol
        dblTot = Val(sprdBank.Text)
        sprdBank.Row = lngRow
        
        Select Case lngCol
        Case Is = COL_CURRENT_AMOUNT
            intSales = enttCurrentSales
            intTendered = enttCurrentTendered
            intDifference = enttCurrentDifference
        Case Is = COL_NEW_AMOUNT
            intSales = enttNewSales
            intTendered = enttNewTendered
            intDifference = enttNewDifference
        End Select
    
        lblTotalAmount(intTendered).Caption = Format$(dblTot, mstrValueFormat)
    
        dblDifference = Val(lblTotalAmount(intSales).Caption) - dblTot
        lblTotalAmount(intDifference).Caption = Format$(dblDifference, mstrValueFormat)
    
    End If
    
End Sub

Private Sub FillCashControlFields(ByVal dteControlDate As Date, _
                                  ByVal strComment1 As String, _
                                  ByVal strComment2 As String, _
                                  ByVal strSignature1 As String, _
                                  ByVal strSignature2 As String)
Dim lngTenderNo  As Long
Dim strSignature As String
    
    ' set the control date
    moCashControl.ControlDate = dteControlDate
    
    ' fill the BankingAmount array from the grid (column COL_NEW_AMOUNT)
    For lngTenderNo = 1 To 40 Step 1
        sprdBank.Row = lngTenderNo
        sprdBank.Col = COL_NEW_AMOUNT
        moCashControl.BankingAmount(lngTenderNo) = Val(sprdBank.Text)
    Next lngTenderNo
    
    ' fill Comment1, Comment2 & Comment3
    moCashControl.Comment1 = strComment1
    moCashControl.Comment2 = strComment2
    
    ' Comment3 is made up of the two signature fields
    If LenB(strSignature1) = 0 And LenB(strSignature2) = 0 Then
        strSignature = vbNullString
    Else
        ' construct string with -
        '   password then trailing blanks then
        '   password then trailing blanks
        strSignature = Left$(strSignature1 + Space$(PAD_PAYROLL_LEN), PAD_PAYROLL_LEN) + _
                       Left$(strSignature2 + Space$(PAD_PASSWRD_LEN), PAD_PASSWRD_LEN)
        ' then pad out to MAX_COMMENT_LEN
        strSignature = Left$(strSignature + Space$(MAX_COMMENT_LEN), MAX_COMMENT_LEN)
    End If
    
    moCashControl.Comment3 = strSignature
    
End Sub

Private Sub UpdateCashControl()
    
    If mblnCreate = True Then
        Call moCashControl.IBo_SaveIfNew
        mblnCreate = False
    Else
        Call moCashControl.IBo_SaveIfExists
    End If

End Sub

Private Sub GetSalesTotal(ByVal dteDate As Date)

Dim oRow                As Object
Dim oSelectField        As Object
Dim oBO                 As Object
Dim dblTotalSalesAmount As Double
Dim strMessage          As String

    lblTotalAmount(enttCurrentSales) = vbNullString
    lblTotalAmount(enttNewSales) = vbNullString
    
    Set oRow = goSession.Root.CreateUtilityObject("CRowSelector")
    
    ' Table is DLTOTS  (CLASSID_POSHEADER)
    ' ====================================
    Set oBO = goSession.Database.CreateBusinessObject(CLASSID_POSHEADER)
    
    ' dteDate = "26 Mar 2003" '  <<<<< test only <<<
    
    'Create field for 1st selection criteria Key
    Set oSelectField = oBO.IBo_GetField(FID_POSHEADER_TranDate)
    If oSelectField Is Nothing Then Exit Sub
    oSelectField.IField_ValueAsVariant = dteDate
    Call oRow.AddSelection(CMP_EQUAL, oSelectField)
    
    'Create field for 2nd selection criteria Key
    Set oSelectField = oBO.IBo_GetField(FID_POSHEADER_TransactionCode)
    If oSelectField Is Nothing Then Exit Sub
    oSelectField.IField_ValueAsVariant = "CC"
    Call oRow.AddSelection(CMP_NOTEQUAL, oSelectField)

    'Create field for 3rd selection criteria Key
    Set oSelectField = oBO.IBo_GetField(FID_POSHEADER_TransactionCode)
    If oSelectField Is Nothing Then Exit Sub
    oSelectField.IField_ValueAsVariant = "CO"
    Call oRow.AddSelection(CMP_NOTEQUAL, oSelectField)

    'Create field for 4th selection criteria Key
    Set oSelectField = oBO.IBo_GetField(FID_POSHEADER_Voided)
    If oSelectField Is Nothing Then Exit Sub
    oSelectField.IField_ValueAsVariant = False
    Call oRow.AddSelection(CMP_EQUAL, oSelectField)

    'Create field for 5th selection criteria Key
    Set oSelectField = oBO.IBo_GetField(FID_POSHEADER_CashierID)
    If oSelectField Is Nothing Then Exit Sub
    oSelectField.IField_ValueAsVariant = mstrMaxValidCashier
    Call oRow.AddSelection(CMP_LESSEQUALTHAN, oSelectField)

    ' Now get the sum of the field TotalSaleAmount
    ' N.B. - getAggregateValue may return error if no data fits selection criteria
    On Error GoTo GetSalesTotal_Err
    dblTotalSalesAmount = goSession.Database.GetAggregateValue(AGG_SUM, oBO.IBo_GetField(FID_POSHEADER_TotalSaleAmount), oRow)
    
    On Error Resume Next
    lblTotalAmount(enttCurrentSales) = Format$(dblTotalSalesAmount, mstrValueFormat)
    lblTotalAmount(enttNewSales) = Format$(dblTotalSalesAmount, mstrValueFormat)
        
    Set oRow = Nothing
    Set oSelectField = Nothing
    Set oBO = Nothing
    
    Exit Sub
    
GetSalesTotal_Err:
    If Err.Number = 94 Then
        'error 94 is O.K - "invalid use of null"
        '         means no data fits selection criteria
    Else
        strMessage = "Error accumumulating sales total." & vbCrLf & vbCrLf & _
                     "Error number " & Err.Number & vbCrLf & vbCrLf & _
                     Err.Description
        Call MsgBoxEx(strMessage, vbInformation, "Error in routine GetSalesTotal", , , , , RGBMsgBox_WarnColour)
    End If
    Resume Next
    
End Sub

Private Sub SetGridLockAndDisplay(ByVal blnLockGrid As Boolean, _
                                  ByVal blnEnableTillErrors As Boolean, _
                                  ByVal blnResetBalVisible As Boolean)
    
Dim lngColNo As Long
    
    For lngColNo = COL_CURRENT_AMOUNT To COL_NEW_AMOUNT Step 1
    
        sprdBank.Col = lngColNo
        sprdBank.Row = -1
        sprdBank.Lock = blnLockGrid
        If blnLockGrid = True Then
            sprdBank.LockBackColor = RGB_LTGREY
            sprdBank.Col = COL_DIFFERENCE
            sprdBank.ColHidden = True
            sprdBank.Col = COL_NEW_AMOUNT
            sprdBank.ColHidden = True
            sprdBank.Col = COL_AS_TENDERED
            sprdBank.ColHidden = True
        Else
            sprdBank.LockBackColor = RGB_WHITE
            sprdBank.Col = COL_DIFFERENCE
            sprdBank.ColHidden = False
            sprdBank.Col = COL_NEW_AMOUNT
            sprdBank.ColHidden = False
            sprdBank.Col = COL_AS_TENDERED
            sprdBank.ColHidden = False
        End If
        
    Next lngColNo
    
    ' mod requested by maurice Milne on 12/6/03
    ' - disable the spread sheet control when on 'Other date'
    If blnLockGrid = True Then
        sprdBank.Enabled = False
    Else
        sprdBank.Enabled = True
    End If
    
    fraTillErrors.Enabled = blnEnableTillErrors
    
    txtReason1.BackColor = sprdBank.LockBackColor
    txtReason2.BackColor = sprdBank.LockBackColor
    txtSignature(enEsPayRollNo).BackColor = sprdBank.LockBackColor
    txtSignature(enEsPassword).BackColor = sprdBank.LockBackColor
    
    For lngColNo = enttCurrentSales To enttNewDifference Step 1
        lblTotalAmount(lngColNo).BackColor = sprdBank.LockBackColor
    Next lngColNo

    ' "reset balance" button
    ' KVB 23/05/03 Remove F3-Reset Balanced button.
    'cmdResetDay.Visible = blnResetBalVisible

End Sub

Private Sub CheckUserPassWord(ByRef intSetFocus As Integer, _
                              ByRef strMessage As String)

Dim oUser As Object

    On Error GoTo CheckUserPassWord_Err
    Set oUser = goDatabase.CreateBusinessObject(CLASSID_USER)
    Call oUser.IBo_AddLoadFilter(CMP_EQUAL, FID_USER_PayrollNumber, txtSignature(enEsPayRollNo).Text)
    Call oUser.IBo_LoadMatches
    
    If oUser.IBo_Load = False Then
        strMessage = "Invalid Payroll Number"
        intSetFocus = enEsPayRollNo
    Else
        ' we have retrieved a record with that payroll no
        If oUser.Password <> txtSignature(enEsPassword).Text Then
            strMessage = "Invalid Password"
            intSetFocus = enEsPassword
        End If
    End If
    
CheckUserPassWord_Done:
    On Error Resume Next
    Set oUser = Nothing
    Exit Sub
    
CheckUserPassWord_Err:
    Call MsgBoxEx("Error " & Err.Number & vbCrLf & Err.Description, vbCritical, "Error in CheckUserPassword", , , , , RGBMsgBox_WarnColour)
    strMessage = "Password check failed"
    intSetFocus = enEsPassword
    Resume CheckUserPassWord_Done
    
End Sub

Private Sub BringAppToFront()

Dim strAppTitle As String
Dim lngRetCode  As Long
Dim lngHwnd     As Long
    
    strAppTitle = Me.Caption
    
    ' Note - to prevent this routine from trying to reactivate itself
    '        must change Me.Caption - see the following line.
    Me.Caption = "Closing down " & strAppTitle
    
    'clear current app title and form caption to aviod reselecting myself
    lngHwnd = FindWindow(vbNullString, strAppTitle)
    lngRetCode = ShowWindow(lngHwnd, SW_RESTORE)
    lngRetCode = BringWindowToTop(lngHwnd)
    On Error Resume Next
    Call AppActivate(strAppTitle)
    End

End Sub

Private Sub DisplayTotals(dteBankingDate As Date)

Const CHEQUE_TENDER_CODE As Integer = 2

Dim lEntryNo             As Long
Dim lngLineNo            As Long
Dim oEntry               As Object
Dim colEntries           As Collection
Dim oLine                As Object
Dim colLines             As Collection
Dim strValue             As String
Dim lngTopRow            As Long
Dim dblMargin            As Double
Dim dblPriceExc          As Double

Dim dblTotalAmount       As Double
Dim dblTotalSoldAt       As Double
Dim dblTotalTradeSoldAt  As Double
Dim dblTotalMargin       As Double
Dim dblTotalTradeMargin  As Double
Dim dblTotalCash         As Double
Dim dblTotalOtherTender  As Double
Dim dblThisSale          As Double
Dim dblThisSaleCash      As Double
Dim dblLookupTimesQuant  As Double
Dim dblTotalTendered()   As Double
Dim lngLastRowToSort     As Long
Dim strCarRegistrationNo As String

Dim intPntLabel          As Integer
Dim intTenderType        As Integer
Dim intCntTenderTypes    As Integer
Dim intWeekDay           As Integer
Dim intSortSequence      As Integer
Dim dteHoldDate          As Date
Dim blnSqMetreDiscount   As Boolean ' 02/05/03 modify the discount calculation
Dim varSortKeys          As Variant

Const CONST_RETAIL_CODE  As String = "00000000"
Const ZERO_CUSTORDNO     As String = "000000"

    On Error GoTo DisplayError

    Screen.MousePointer = vbHourglass
    ucpbProgressBar.Visible = True
    ucpbProgressBar.Caption1 = "Loading List from Database"
    DoEvents
    
    'Create single element that List must be retrieved for
    Set oEntry = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    
    dblTotalAmount = 0
    dblTotalSoldAt = 0
    dblTotalTradeSoldAt = 0
    dblTotalMargin = 0
    dblTotalTradeMargin = 0
    dblTotalCash = 0
    dblTotalOtherTender = 0
    
    'Set Date criteria for currentt date
      Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, dteBankingDate)
    
    'Set up Transaction selection criteria
    Call oEntry.IBo_AddLoadFilter(CMP_NOTEQUAL, FID_POSHEADER_TransactionCode, "CC")
    Call oEntry.IBo_AddLoadFilter(CMP_NOTEQUAL, FID_POSHEADER_TransactionCode, "CO")
    Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_Voided, False)
    Call oEntry.IBo_AddLoadFilter(CMP_LESSEQUALTHAN, FID_POSHEADER_CashierID, mstrMaxValidCashier)
    
    'Select fields to load from table
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_TranDate)
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_TransactionTime)
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_TransactionNo)
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_TransactionCode)
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_TotalSaleAmount)
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_DiscountAmount)
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_TillID)
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_TranParked)
    
    'Make call on entries to return collection of all entries
    Set colEntries = oEntry.IBo_LoadMatches
    
    ' prepare an array to store tendered amounts (NOT cash)
    ' this array may be resized later
    intCntTenderTypes = 1
    ReDim dblTotalTendered(intCntTenderTypes)
    
        
    'Display retrieved data
    ucpbProgressBar.Caption1 = "Populating grid"
    ucpbProgressBar.Min = 0
    ucpbProgressBar.Value = 0
    
    If colEntries.Count > 0 Then
        ucpbProgressBar.Max = colEntries.Count
        sbStatus.Panels(PANEL_INFO).Text = "Populating grid"
        DoEvents
        
        For lEntryNo = 1 To colEntries.Count Step 1
                    
            ucpbProgressBar.Value = ucpbProgressBar.Value + 1
            If colEntries(lEntryNo).TranParked = False Then 'ignore all parked
                dblTotalAmount = dblTotalAmount + colEntries(lEntryNo).TotalSaleAmount
                
                'Get Payment Lines, if any for Sale
                Set oLine = goDatabase.CreateBusinessObject(CLASSID_POSPAYMENT)
                Call oLine.IBo_AddLoadFilter(CMP_EQUAL, FID_POSPAYMENT_TillID, colEntries(lEntryNo).TillID)
                Call oLine.IBo_AddLoadFilter(CMP_EQUAL, FID_POSPAYMENT_TransactionDate, colEntries(lEntryNo).TranDate)
                Call oLine.IBo_AddLoadFilter(CMP_EQUAL, FID_POSPAYMENT_TransactionNumber, colEntries(lEntryNo).TransactionNo)
    '            Call oLine.IBo_AddLoadFilter(CMP_GREATERTHAN, FID_pospayment_SequenceNo, 0)
                Call oLine.IBo_AddLoadField(FID_POSPAYMENT_TenderType)
                Call oLine.IBo_AddLoadField(FID_POSPAYMENT_TenderAmount)
                
                ' 22/04/03 kvb - also get the Authorisation Code
                Call oLine.IBo_AddLoadField(FID_POSPAYMENT_AuthorisationCode)
                
                'Make call on entries to return collection of all entries
                Set colLines = oLine.IBo_LoadMatches
                
                ' 22/04/03 kvb
                ' accumulate total cheque / mastercard payments for this Sale
                ' because any remainder is Cash.
                
                dblThisSale = 0
                dblThisSaleCash = 0
                
                For lngLineNo = 1 To colLines.Count Step 1
                    
                    ' nb 100 paid is -100 in colLines(lngLineNo).TenderAmount
                    '     so here we reverse the sign.
                    dblThisSale = dblThisSale + (-1 * colLines(lngLineNo).TenderAmount)
                    
                Next lngLineNo
    
                ' now subtract this from the Total Sale Amount
                dblThisSaleCash = colEntries(lEntryNo).TotalSaleAmount - dblThisSale
                            
                dblTotalCash = dblTotalCash + dblThisSaleCash
                dblTotalOtherTender = dblTotalOtherTender + dblThisSale
                
                'Display Lines
                For lngLineNo = 1 To colLines.Count Step 1
                                
                    ' if the tender type is larger than allowed for in array dblTotalTendered()
                    ' make the array bigger.
                    If colLines(lngLineNo).TenderType > UBound(dblTotalTendered) Then
                        ReDim Preserve dblTotalTendered(colLines(lngLineNo).TenderType)
                        intCntTenderTypes = colLines(lngLineNo).TenderType
                    End If
                    
                    ' accumulate in array of tendered (NB This is NOT cash)
                    dblTotalTendered(colLines(lngLineNo).TenderType) = dblTotalTendered(colLines(lngLineNo).TenderType) - colLines(lngLineNo).TenderAmount
                    
                Next lngLineNo
            End If 'ignore transaction because TranParked
        
        Next lEntryNo
        
    End If 'any matched records to display
    
    sprdBank.Col = COL_AS_TENDERED
    For intTenderType = 0 To intCntTenderTypes Step 1
        If dblTotalTendered(intTenderType) <> 0 Then
            sprdBank.Row = intTenderType
            Call DebugMsg(MODULE_NAME, "Display Totals", endlDebug, intTenderType & "-" & dblTotalTendered(intTenderType))
            If sprdBank.RowHidden = False Then sprdBank.Text = dblTotalTendered(intTenderType)
        End If
    Next intTenderType
    sprdBank.Row = 1
    sprdBank.Text = dblTotalCash
    ucpbProgressBar.Visible = False
    sbStatus.Panels(PANEL_INFO).Text = vbNullString
    
    Screen.MousePointer = vbNormal
    Me.Refresh
    
    Exit Sub
    
DisplayError:

    Call Err.Report(MODULE_NAME, "cmdApply_click", 1, True, "Error displaying data", "Error occurred when display selected criteria")
    ucpbProgressBar.Visible = False
    sbStatus.Panels(PANEL_INFO).Text = vbNullString
    Screen.MousePointer = vbNormal
    Exit Sub
    Resume Next

End Sub

