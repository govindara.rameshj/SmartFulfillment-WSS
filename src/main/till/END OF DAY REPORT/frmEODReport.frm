VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Object = "{9BA18739-054D-4172-8E42-118133CE2FC4}#1.0#0"; "EditDateCtl.ocx"
Begin VB.Form EODReport 
   BackColor       =   &H00E0E0E0&
   Caption         =   "End of Day Report"
   ClientHeight    =   6075
   ClientLeft      =   1080
   ClientTop       =   2145
   ClientWidth     =   10545
   Icon            =   "frmEODReport.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   6075
   ScaleWidth      =   10545
   WindowState     =   2  'Maximized
   Begin FPSpreadADO.fpSpread sprdEOD 
      Height          =   3975
      Left            =   120
      TabIndex        =   33
      Top             =   960
      Width           =   10335
      _Version        =   458752
      _ExtentX        =   18230
      _ExtentY        =   7011
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SpreadDesigner  =   "frmEODReport.frx":058A
   End
   Begin VB.Frame fraTotals 
      BackColor       =   &H00E0E0E0&
      Height          =   735
      Left            =   1200
      TabIndex        =   14
      Top             =   4920
      Visible         =   0   'False
      Width           =   7455
      Begin VB.CommandButton cmdShowCash 
         Caption         =   "F4-Show All Totals"
         Height          =   525
         Left            =   6000
         TabIndex        =   10
         Top             =   75
         Visible         =   0   'False
         Width           =   1200
      End
      Begin VB.Label lblTotalAmount 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   7
         Left            =   6360
         TabIndex        =   30
         Top             =   400
         Width           =   855
      End
      Begin VB.Label lblTotalAmount 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   6
         Left            =   6360
         TabIndex        =   29
         Top             =   100
         Width           =   855
      End
      Begin VB.Label lblTotalAmount 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   5
         Left            =   4635
         TabIndex        =   28
         Top             =   400
         Width           =   855
      End
      Begin VB.Label lblTotalAmount 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   4
         Left            =   4635
         TabIndex        =   27
         Top             =   100
         Width           =   855
      End
      Begin VB.Label lblTotalAmount 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   3
         Left            =   2760
         TabIndex        =   26
         Top             =   400
         Width           =   855
      End
      Begin VB.Label lblTotalAmount 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   2
         Left            =   2760
         TabIndex        =   25
         Top             =   100
         Width           =   855
      End
      Begin VB.Label lblTotalAmount 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   1
         Left            =   840
         TabIndex        =   24
         Top             =   400
         Width           =   855
      End
      Begin VB.Label lblTotalAmount 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   0
         Left            =   840
         TabIndex        =   23
         Top             =   100
         Width           =   855
      End
      Begin VB.Label lblTots 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Other"
         Height          =   255
         Index           =   7
         Left            =   5760
         TabIndex        =   22
         Top             =   420
         Width           =   615
      End
      Begin VB.Label lblTots 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Cash"
         Height          =   255
         Index           =   6
         Left            =   5760
         TabIndex        =   21
         Top             =   120
         Width           =   735
      End
      Begin VB.Line linSeperator 
         BorderColor     =   &H80000004&
         BorderWidth     =   3
         X1              =   5670
         X2              =   5670
         Y1              =   120
         Y2              =   690
      End
      Begin VB.Label lblTots 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         Caption         =   "Margin Trade"
         Height          =   255
         Index           =   5
         Left            =   3660
         TabIndex        =   20
         Top             =   405
         Width           =   975
      End
      Begin VB.Label lblTots 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         Caption         =   "Margin"
         Height          =   255
         Index           =   4
         Left            =   3880
         TabIndex        =   19
         Top             =   120
         Width           =   735
      End
      Begin VB.Label lblTots 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Sold @ Trade"
         Height          =   255
         Index           =   3
         Left            =   1740
         TabIndex        =   18
         Top             =   405
         Width           =   1095
      End
      Begin VB.Label lblTots 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Sold @"
         Height          =   255
         Index           =   2
         Left            =   2160
         TabIndex        =   17
         Top             =   120
         Width           =   615
      End
      Begin VB.Label lblTots 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Inc Trade"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   16
         Top             =   400
         Width           =   735
      End
      Begin VB.Label lblTots 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         Caption         =   "Total"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   15
         Top             =   120
         Width           =   615
      End
   End
   Begin VB.Frame fraCriteria 
      BackColor       =   &H00E0E0E0&
      Caption         =   " Selection criteria "
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   100
      Width           =   9615
      Begin VB.CommandButton cmdApply 
         Caption         =   "F7-Search"
         Height          =   375
         Left            =   8160
         TabIndex        =   6
         Top             =   240
         Width           =   1095
      End
      Begin VB.ComboBox cmbDateCrit 
         Height          =   315
         ItemData        =   "frmEODReport.frx":075E
         Left            =   600
         List            =   "frmEODReport.frx":0760
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   240
         Width           =   1425
      End
      Begin ucEditDate.ucDateText dtxtEndDate 
         Height          =   285
         Left            =   5280
         TabIndex        =   5
         Top             =   240
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DateFormat      =   "DD/MM/YY"
         Text            =   ""
      End
      Begin ucEditDate.ucDateText dtxtStartDate 
         Height          =   285
         Left            =   2160
         TabIndex        =   3
         Top             =   240
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DateFormat      =   "DD/MM/YY"
         Text            =   ""
      End
      Begin VB.Label labDay 
         BackStyle       =   0  'Transparent
         Height          =   285
         Index           =   1
         Left            =   6480
         TabIndex        =   32
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label labDay 
         BackStyle       =   0  'Transparent
         Height          =   285
         Index           =   0
         Left            =   3360
         TabIndex        =   31
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Period"
         Height          =   255
         Left            =   100
         TabIndex        =   1
         Top             =   280
         Width           =   495
      End
      Begin VB.Label lblToDate 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "to"
         Height          =   255
         Left            =   4700
         TabIndex        =   4
         Top             =   285
         Width           =   375
      End
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10-Exit"
      Height          =   495
      Left            =   120
      TabIndex        =   9
      Top             =   5040
      Width           =   1215
   End
   Begin CTSProgBar.ucpbProgressBar ucProgress 
      Height          =   1815
      Left            =   960
      TabIndex        =   7
      Top             =   1680
      Width           =   5055
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      FillColor       =   16744703
      Object.Width           =   5055
      Object.Height          =   1815
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   8
      Top             =   5700
      Width           =   10545
      _ExtentX        =   18600
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmEODReport.frx":0762
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10795
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "11:45"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F9-Print"
      Height          =   495
      Left            =   8760
      TabIndex        =   11
      Top             =   5040
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblNoLines 
      Alignment       =   1  'Right Justify
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   1080
      TabIndex        =   13
      Top             =   5400
      Width           =   975
   End
   Begin VB.Label lblNoLineslbl 
      BackStyle       =   0  'Transparent
      Caption         =   "No Matches"
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   5400
      Width           =   975
   End
End
Attribute VB_Name = "EODReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module :
'* Date   : 19/09/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/End Of Day Report/frmEODReport.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 7/06/04 10:07 $ $Revision: 19 $
'* Versions:
'* 19/09/02    mauricem
'*             Header added.
'* 22/04/03-   KeithB
'* 24/04/03    Calculate cash payments, accumulate & display totals
'*             Modify date selection
'* 14/05/03    Remove collection mcolUsers. Use new common routine GetFullName instead.
'* 15/05/03    Add 'Week ending' selection - must be a Saturday
'*             Add button 'F4-Show All totals' - see parameter 'SB' below.
'*             Add Function SetDateCrit to set correct period criteria in drop down combo box.
'* 30/05/03    KeithB allow date parameter of DB when called from Bank Deposits (Daily Banking)
'*             Add program version number to printed footer.
'* 03/06/03    KeithB
'*         (a) Display margin with TWO decimal places
'*         (b) Add Sale/LOAN  and  Refu/LOAN to column COL_TYPE
'*         (c) Add ** NEW ORDER ** to column  when TranParked is true
'*         (d) sprdEOD.Sort does NOT work when cells have been added with AddCellSpan
'*             DO NOT USE ADDCELLSPAN IN THIS PROGRAM
'*             AllowCellOverflow set to true to compensate for lack of spanned cells.
'* 06/06/03    KeithB
'*             Add day to screen (Sub DisPlayTheDay)
'*             Add day to report title.
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*      TS  Start Date followed by one of the following =  >  <
'*                           e.g. (/P='TS=20030326,  or  (/P>'TS>20030429,
'*      DB  Dated - when called from Daily Banking
'*                           e.g. (/P='DB=20030326
'*      TD  End date         e.g. TD20030329
'*      WE  Week Ending      e.g. WE=20030329     (note date should be a Saturday)
'*      SB  Show Button      if parameter is present Cash & Other Totals CAN be revealed
'*                           if parameter is absent  Cash & Other Totals cannot be revealed
'*      DC  Destination Code DCP Printer or DCS Preview
'**********************************************************************************************
'* SAMPLE (/P='DB=20031020,SB,STO,DCS,GRD,DL0' /u='044' -c='ffff')
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "EODReport"

Const COL_TIME     As Long = 1
Const COL_TILL     As Long = 2
Const COL_ID       As Long = 3
Const COL_TYPE     As Long = 4 ' Nb Type and SKU occupy the same column
Const COL_SKU      As Long = 4
Const COL_DESC     As Long = 5
Const COL_SIZE     As Long = 6 ' 25/04/03 KVB  COL_SIZE added
Const COL_AMOUNT   As Long = 7
Const COL_VAT      As Long = 8
Const COL_QTY      As Long = 9
Const COL_SOLDAT   As Long = 10
Const COL_DISC     As Long = 11
Const COL_MARGIN   As Long = 12
Const COL_MARGPERC As Long = 13
Const COL_STOCK    As Long = 14
Const COL_DEPOSIT  As Long = 15
Const COL_TENDER   As Long = 16
Const COL_PAYAMT   As Long = 17
Const COL_TDATE    As Long = 18
'06/05/03 KVB sort the grid
Const COL_SORTDATE     As Long = 19
Const COL_SORTTIME     As Long = 20
Const COL_SORTTILLID   As Long = 21
Const COL_SORTTYPE     As Long = 22
Const COL_SORTSEQUENCE As Long = 23


Const CASH_PARAMETER   As Integer = 650
Const BORDER_WIDTH     As Long = 60

Private mstrCashPmtCode As String
Private mstrValueFormat As String ' format to display value
Dim mlngQtyDecNum       As Long   'hold number of decimal places to show for quantities
Dim mlngPayAmtDecPlaces As Long   'hold number of decimal places to show for pay amount
Dim colEntries          As Collection
Dim mcolTypesOfSales    As Collection
Dim moTenderoptions     As cTenderOptions
Dim moMiscIncomeDesc    As cPaidInCode
'Dim mcolUsers          As Collection
Dim mcolDiscReasons     As Collection
Dim mstrDestCode        As String 'hold passed in reports final destination code
Dim mblnDateFromParam   As Boolean
Dim mblnFromBanking     As Boolean 'flagged if called from Cash Routine - hide criteria
Dim mstrMaxValidCashier As String 'hold the last valid UserID - all above this are Training trans
Dim madblMiscInc(10)    As Double

Private mstrExitCaption As String

Private Const CRIT_EQUALS      As Long = 0
Private Const CRIT_GREATERTHAN As Long = 1
Private Const CRIT_LESSTHAN    As Long = 2
Private Const CRIT_ALL         As Long = 3
Private Const CRIT_FROM        As Long = 4
Private Const CRIT_WEEKENDING  As Long = 5

Private Enum enTotalDisplay
    entdAmount = 0
    entdAmountTradeSales
    entdSoldAt
    entdSoldAtTradeSales
    entdMargin
    entdMarginTradeSales
    entdTenderedCash
    entdTenderedOther
End Enum

' 06/05/03 KVB used to sort the grid in column COL_SORTTYPE
Private Enum enSortTypes
    enstBlankLine = 0
    enstDateLine
    enstHeader
    enstLine
    enstPayment
    enstCarReg
    enstLineAcrossBelowEntry
End Enum



Private Sub cmbDateCrit_Click()

Dim dteEndDate       As Date
Dim dteSuggestedDate As Date
    
    On Error Resume Next
    
    Select Case (cmbDateCrit.ItemData(cmbDateCrit.ListIndex))
        Case CRIT_LESSTHAN, CRIT_GREATERTHAN, CRIT_EQUALS
            dtxtStartDate.Visible = True
            labDay(0).Visible = True
            dtxtEndDate.Visible = False
            labDay(1).Visible = False
            lblToDate.Visible = False
            dtxtStartDate.SetFocus
        Case CRIT_ALL
            dtxtStartDate.Visible = False
            labDay(0).Visible = False
            dtxtEndDate.Visible = False
            labDay(1).Visible = False
            lblToDate.Visible = False
            cmdApply.SetFocus
        Case CRIT_FROM
            dtxtStartDate.Visible = True
            labDay(0).Visible = True
            dtxtEndDate.Visible = True
            labDay(1).Visible = True
            Call DisplayTheDay(dtxtEndDate.Text, 1)
            lblToDate.Caption = "to"
            lblToDate.Visible = True
            dtxtStartDate.SetFocus
        Case CRIT_WEEKENDING
            dtxtStartDate.Visible = False
            labDay(0).Visible = False
            dtxtEndDate.Visible = True
            labDay(1).Visible = True
            Call DisplayTheDay(dtxtEndDate.Text, 1)
            'lblToDate.Caption = "Sat"
            lblToDate.Visible = False
            ' the end date must be a Saturday
            dteEndDate = GetDate(dtxtEndDate.Text)
            ' if it is not, go on to next Saturday after the date entered
            If Weekday(dteEndDate) <> vbSaturday Then
                Call FindNextSaturday(dteEndDate, dteSuggestedDate)
                dtxtEndDate.Text = Format(dteSuggestedDate, mstrDateFmt)
            End If
            dtxtEndDate.SetFocus
    End Select

End Sub

Private Sub cmdShowCash_Click()

    cmdShowCash.Visible = False
    ' show Cash & Other in fraTotals
    lblTots(entdTenderedCash).Visible = True
    lblTotalAmount(entdTenderedCash).Visible = True
    lblTots(entdTenderedOther).Visible = True
    lblTotalAmount(entdTenderedOther).Visible = True
    
End Sub

Private Sub dtxtEndDate_GotFocus()

    dtxtEndDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtEndDate_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        Call SendKeys(vbTab)
        KeyAscii = 0
    End If
    If KeyAscii = vbKeyEscape Then
        Call SendKeys("+{tab}")
        KeyAscii = 0
    End If

End Sub

Private Sub dtxtEndDate_LostFocus()

    dtxtEndDate.BackColor = lblNoLines.BackColor
    Call DisplayTheDay(dtxtEndDate.Text, 1)

End Sub

Private Sub dtxtStartDate_GotFocus()
    
    dtxtStartDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtStartDate_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        Call SendKeys(vbTab)
        KeyAscii = 0
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
        KeyAscii = 0
    End If

End Sub

Private Sub dtxtStartDate_LostFocus()

    dtxtStartDate.BackColor = lblNoLines.BackColor
    
    Call DisplayTheDay(dtxtStartDate.Text, 0)
    
End Sub

Private Sub sprdEOD_GotFocus()
    
    sprdEOD.SelBackColor = RGB_BLUE
    sprdEOD.SelForeColor = RGB_WHITE

End Sub

Private Sub sprdEOD_LostFocus()
    
    sprdEOD.SelBackColor = -1
    sprdEOD.SelForeColor = -1
    sbStatus.Panels(PANEL_INFO).Text = ""

End Sub

Private Sub cmdApply_Click()

Const CHEQUE_TENDER_CODE As Integer = 2

Dim lEntryNo             As Long
Dim lngLineNo            As Long
Dim oEntry               As cPOSHeader
Dim colEntries           As Collection
Dim oLine                As cPOSLine
Dim colLines             As Collection
Dim oPart                As cInventory
Dim strValue             As String
Dim lngTopRow            As Long
Dim dblMargin            As Double
Dim dblPriceExc          As Double

Dim dblTotalAmount       As Double
Dim dblTotalTradeAmount  As Double
Dim dblTotalSoldAt       As Double
Dim dblTotalTradeSoldAt  As Double
Dim dblTotalMargin       As Double
Dim dblTotalTradeMargin  As Double
Dim dblTotalCash         As Double
Dim dblTotalOtherTender  As Double
Dim dblTotalVat          As Double
Dim dblTotalTradeVat     As Double
Dim dblTotLookupTimesQuant As Double
Dim dblTotLookupTimesQuantTrade As Double
Dim dblTotExtendedTrade  As Double
Dim dblTotExtended       As Double
Dim dblThisSale          As Double
Dim dblThisSaleCash      As Double
Dim dblLookupTimesQuant  As Double
Dim dblTotalTendered()   As Double
Dim lngLastRowToSort     As Long
Dim strCarRegistrationNo As String

Dim intPntLabel          As Integer
Dim intTenderType        As Integer
Dim intCntTenderTypes    As Integer
Dim intWeekDay           As Integer
Dim intSortSequence      As Integer
Dim dteHoldDate          As Date
Dim blnSqMetreDiscount   As Boolean ' 02/05/03 modify the discount calculation
Dim varSortKeys          As Variant

Dim strRoundingCalc      As String

Const CONST_RETAIL_CODE  As String = "00000000"
Const ZERO_CUSTORDNO     As String = "000000"

    On Error GoTo DisplayError

    sprdEOD.MaxRows = 0
    fraTotals.Visible = False
    cmdPrint.Visible = False

    Select Case cmbDateCrit.ItemData(cmbDateCrit.ListIndex)
    Case Is = CRIT_FROM
        Call CheckDateRange
    Case Is = CRIT_WEEKENDING
        If CheckWeekEnding = False Then
            Exit Sub
        End If
    End Select
    
    Screen.MousePointer = vbHourglass
    ucProgress.Visible = True
    ucProgress.Caption1 = "Loading List from Database"
    lblNoLines.Caption = "Searching"
    cmdApply.Enabled = False
    DoEvents
    
    'Create single element that List must be retrieved for
    Set oEntry = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    
    dblTotalAmount = 0
    dblTotalTradeAmount = 0
    dblTotalSoldAt = 0
    dblTotalTradeSoldAt = 0
    dblTotalMargin = 0
    dblTotalTradeMargin = 0
    dblTotalCash = 0
    dblTotalOtherTender = 0
    dblTotalVat = 0
    dblTotalTradeVat = 0
    dblTotLookupTimesQuant = 0
    dblTotExtended = 0
    dblTotLookupTimesQuantTrade = 0
    dblTotExtendedTrade = 0
    
    For intPntLabel = 1 To 10 Step 1
        madblMiscInc(intPntLabel) = 0
    Next intPntLabel
    
    For intPntLabel = entdAmount To entdTenderedOther Step 1
        lblTotalAmount(intPntLabel).Caption = ""
    Next intPntLabel
        
    'Set Date criteria if selected by user
    Select Case cmbDateCrit.ItemData(cmbDateCrit.ListIndex)
    Case CRIT_LESSTHAN
      Call oEntry.IBo_AddLoadFilter(CMP_LESSEQUALTHAN, FID_POSHEADER_TranDate, GetDate(dtxtStartDate.Text))
    Case CRIT_GREATERTHAN
      Call oEntry.IBo_AddLoadFilter(CMP_GREATEREQUALTHAN, FID_POSHEADER_TranDate, GetDate(dtxtStartDate.Text))
    Case CRIT_EQUALS
      Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, GetDate(dtxtStartDate.Text))
    Case CRIT_FROM, CRIT_WEEKENDING
      Call oEntry.IBo_AddLoadFilter(CMP_GREATEREQUALTHAN, FID_POSHEADER_TranDate, GetDate(dtxtStartDate.Text))
      Call oEntry.IBo_AddLoadFilter(CMP_LESSEQUALTHAN, FID_POSHEADER_TranDate, GetDate(dtxtEndDate.Text))
    End Select
    
    ' attempt to force order by transaction time FID_POSHEADER_TransactionTime
'    Call oEntry.IBo_AddLoadFilter(CMP_GREATEREQUALTHAN, FID_POSHEADER_TransactionTime, "00:00:00")
    
    
    'Set up Transaction selection criteria
    Call oEntry.IBo_AddLoadFilter(CMP_NOTEQUAL, FID_POSHEADER_TransactionCode, "CC")
    Call oEntry.IBo_AddLoadFilter(CMP_NOTEQUAL, FID_POSHEADER_TransactionCode, "CO")
    Call oEntry.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_Voided, False)
    Call oEntry.IBo_AddLoadFilter(CMP_LESSEQUALTHAN, FID_POSHEADER_CashierID, mstrMaxValidCashier)
    
    'Select fields to load from table
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_TranDate)
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_TransactionTime)
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_TransactionNo)
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_TransactionCode)
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_OrderNumber)
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_TotalSaleAmount)
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_TaxAmount)
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_DiscountAmount)
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_TillID)
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_CashierID)
    '03/06/03 add fields LoanTile, TranParked & TrainingMode
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_TranParked)
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_TrainingMode)
    '04/6/04 added Misc Income Code
    Call oEntry.IBo_AddLoadField(FID_POSHEADER_ReasonCode)
    
    'Make call on entries to return collection of all entries
    Set colEntries = oEntry.IBo_LoadMatches
    
    ' set max no of columns
    sprdEOD.MaxCols = COL_SORTSEQUENCE
    
    ' set no of decimal places for Quantity & Stock
    sprdEOD.Row = -1
    sprdEOD.Col = COL_QTY
    sprdEOD.TypeNumberDecPlaces = mlngQtyDecNum
    sprdEOD.Col = COL_STOCK
    sprdEOD.TypeNumberDecPlaces = mlngQtyDecNum
    ' set no of decimal places for
    sprdEOD.Col = COL_PAYAMT
    sprdEOD.TypeNumberDecPlaces = mlngPayAmtDecPlaces
    
    ' prepare an array to store tendered amounts (NOT cash)
    ' this array may be resized later
    intCntTenderTypes = 1
    ReDim dblTotalTendered(intCntTenderTypes)
    
        
    'Display retrieved data
    ucProgress.Caption1 = "Populating grid"
    ucProgress.Min = 0
    ucProgress.Value = 0
    
    sprdEOD.ReDraw = False
    
    'Initialise Part Obj to load selected
    Set oPart = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
    Call oPart.IBo_AddLoadField(FID_INVENTORY_Description)
                
    If colEntries.Count > 0 Then
        ucProgress.Max = colEntries.Count
        sbStatus.Panels(PANEL_INFO).Text = "Populating grid"
        DoEvents
        
        For lEntryNo = 1 To colEntries.Count Step 1
                    
            sprdEOD.MaxRows = sprdEOD.MaxRows + 1
            sprdEOD.Row = sprdEOD.MaxRows
            ucProgress.Value = ucProgress.Value + 1
                        
            ' 02/05/03 KVB add a line indicating date
            '          unless report is for one day only
            If dteHoldDate <> colEntries(lEntryNo).TranDate Then
                dteHoldDate = colEntries(lEntryNo).TranDate
                intWeekDay = Weekday(dteHoldDate)
                If cmbDateCrit.ItemData(cmbDateCrit.ListIndex) <> CRIT_EQUALS Then
                    ' add sort info for the blank line
                    intSortSequence = 0
                    Call FillSortCols(sprdEOD, dteHoldDate, enstBlankLine, 0, colEntries(lEntryNo).TransactionTime, colEntries(lEntryNo).TillID)
                    'move down a row & display day and date
                    sprdEOD.MaxRows = sprdEOD.MaxRows + 1
                    sprdEOD.Row = sprdEOD.MaxRows
                    'Call sprdEOD.AddCellSpan(COL_TIME, sprdEOD.Row, COL_TDATE - 1, 1)
                    sprdEOD.Col = COL_TIME
                    sprdEOD.FontBold = True
                    sprdEOD.Text = WeekdayName(intWeekDay, False, vbSunday) & "  " & Format(dteHoldDate, mstrDateFmt)
                    
                    ' add the sort columns (nb intSortSequence = 0)
                    Call FillSortCols(sprdEOD, dteHoldDate, enstDateLine, 0, colEntries(lEntryNo).TransactionTime, colEntries(lEntryNo).TillID)
                    ' and move down one row
                    sprdEOD.MaxRows = sprdEOD.MaxRows + 1
                    sprdEOD.Row = sprdEOD.MaxRows
                End If
            End If
            
            
            ' add the sort columns for the header data
            intSortSequence = 0
            Call FillSortCols(sprdEOD, dteHoldDate, enstHeader, intSortSequence, colEntries(lEntryNo).TransactionTime, colEntries(lEntryNo).TillID)
            
            sprdEOD.Col = COL_TIME
            sprdEOD.Text = Format(colEntries(lEntryNo).TransactionTime, "@@:@@:@@")
            sprdEOD.Col = COL_ID
            sprdEOD.Text = colEntries(lEntryNo).TransactionNo
            
            sprdEOD.Col = COL_TYPE
            On Error Resume Next
            sprdEOD.Text = mcolTypesOfSales(colEntries(lEntryNo).TransactionCode)
            If colEntries(lEntryNo).LoanTile = "L" Then
                ' "Loan Tile"
                sprdEOD.Text = Left$(sprdEOD.Text, 4) & "/" & "LOAN"
            End If
            
            On Error GoTo 0
            
'            If colEntries(lEntryNo).TransactionCode = "SA" Then
                dblTotalAmount = dblTotalAmount + colEntries(lEntryNo).TotalSaleAmount
'            End If
            
            ' If this is a trader, put Traders Name in COL_MARGIN
            If colEntries(lEntryNo).TradersCard <> CONST_RETAIL_CODE Then
                sprdEOD.Col = COL_DESC
                sprdEOD.Text = colEntries(lEntryNo).TradersCard
            End If
            
            sprdEOD.Col = COL_AMOUNT
            sprdEOD.Text = colEntries(lEntryNo).TotalSaleAmount
            
            sprdEOD.Col = COL_VAT
            sprdEOD.Text = colEntries(lEntryNo).TaxAmount
            
            ' accumulate VAT
            dblTotalVat = dblTotalVat + colEntries(lEntryNo).TaxAmount
            If colEntries(lEntryNo).TradersCard <> CONST_RETAIL_CODE Then
                dblTotalTradeVat = dblTotalTradeVat + colEntries(lEntryNo).TaxAmount
            End If
            
            'If against Customer Order number then display, else leave blank
            If colEntries(lEntryNo).CustomerOrderNo <> ZERO_CUSTORDNO Then
                sprdEOD.Col = COL_QTY
                sprdEOD.CellType = CellTypeEdit
                sprdEOD.Text = "  " & colEntries(lEntryNo).CustomerOrderNo
            End If
            
            ' Name
            On Error Resume Next
            sprdEOD.Col = COL_SOLDAT
            sprdEOD.CellType = CellTypeEdit
            sprdEOD.Text = GetFullName(colEntries(lEntryNo).CashierID)
            ' Salesman if different
            If (colEntries(lEntryNo).CashierID <> colEntries(lEntryNo).SalesmanCode) Then
                sprdEOD.Col = COL_DISC
                sprdEOD.CellType = CellTypeEdit
                sprdEOD.Text = "/" & GetFullName(colEntries(lEntryNo).SalesmanCode)
            End If
            On Error GoTo 0
            
            sprdEOD.Col = COL_TDATE
            sprdEOD.Text = DisplayDate(colEntries(lEntryNo).TranDate, False)
            
            ' fill the description column COL_DESC
            ' =========================== ========
            sprdEOD.Col = COL_DESC
            
            Select Case colEntries(lEntryNo).TransactionCode
            Case Is = "M+"
                If colEntries(lEntryNo).CustomerOrderNo <> ZERO_CUSTORDNO Then
                    If (colEntries(lEntryNo).OrderNumber <> colEntries(lEntryNo).CustomerOrderNo) Then
                        sprdEOD.Text = "*NEW ORD/DEP*"
                    Else
                        sprdEOD.Text = "*EDIT ORD/DEP*"
                    End If
                Else
                    If (colEntries(lEntryNo).ReasonCode > 0) Then
                        If (moMiscIncomeDesc Is Nothing) = True Then
                            Set moMiscIncomeDesc = goDatabase.CreateBusinessObject(CLASSID_TILLLOOKUPS)
                            Call moMiscIncomeDesc.IBo_AddLoadField(FID_TILLLOOKUPS_MiscIncomeAccountNo)
                            Call moMiscIncomeDesc.IBo_LoadMatches
                        End If
                        madblMiscInc(colEntries(lEntryNo).ReasonCode) = madblMiscInc(colEntries(lEntryNo).ReasonCode) + colEntries(lEntryNo).TotalSaleAmount
                        sprdEOD.MaxRows = sprdEOD.MaxRows + 1
                        sprdEOD.Row = sprdEOD.MaxRows
                        sprdEOD.Col = COL_TYPE
                        sprdEOD.Text = moMiscIncomeDesc.MiscIncomeAccountNo(colEntries(lEntryNo).ReasonCode)
                        sprdEOD.Row = sprdEOD.MaxRows - 1
                    End If
                End If
            Case Is = "M-"
                If colEntries(lEntryNo).CustomerOrderNo Then
                    If (colEntries(lEntryNo).OrderNumber <> colEntries(lEntryNo).CustomerOrderNo) Then
                        sprdEOD.Text = "*NEW ORD/DEP*"
                    Else
                        sprdEOD.Text = "*Canc ORD/DEP*"
                    End If
                End If
            Case Is = "SA"
                If colEntries(lEntryNo).TranParked = True Then
                    sprdEOD.Text = "* NEW ORDER *"
                    sprdEOD.Col = COL_TYPE
                    sprdEOD.Text = ""
                End If
            Case Is = "RF"
                If colEntries(lEntryNo).TranParked = True Then
                    sprdEOD.Text = "* NEW *"
                End If
            End Select
            
            If colEntries(lEntryNo).TrainingMode = True Then
                If sprdEOD.Text <> "" Then
                    sprdEOD.Text = sprdEOD.Text & " "
                End If
                sprdEOD.Text = sprdEOD.Text & "TRAINING"
            End If
            
            sprdEOD.Col = COL_TILL
            sprdEOD.Text = colEntries(lEntryNo).TillID
            'Preserve current line for Sale Header
            lngTopRow = sprdEOD.MaxRows
            
            'Get Lines, if any for Sale
            Set oLine = goDatabase.CreateBusinessObject(CLASSID_POSLINE)
            Call oLine.IBo_AddLoadFilter(CMP_EQUAL, FID_POSLINE_TillID, colEntries(lEntryNo).TillID)
            Call oLine.IBo_AddLoadFilter(CMP_EQUAL, FID_POSLINE_TranDate, colEntries(lEntryNo).TranDate)
            Call oLine.IBo_AddLoadFilter(CMP_EQUAL, FID_POSLINE_TransactionNumber, colEntries(lEntryNo).TransactionNo)
'            Call oLine.IBo_AddLoadFilter(CMP_GREATERTHAN, FID_POSLINE_SequenceNo, 0)
            Call oLine.IBo_AddLoadField(FID_POSLINE_PartCode)
            Call oLine.IBo_AddLoadField(FID_POSLINE_QuantitySold)
            Call oLine.IBo_AddLoadField(FID_POSLINE_ExtendedValue)
            Call oLine.IBo_AddLoadField(FID_POSLINE_LookUpPrice)
            Call oLine.IBo_AddLoadField(FID_POSLINE_ExtendedCost)
            ' 02/05/03  Add VAT code
            Call oLine.IBo_AddLoadField(FID_POSLINE_VATCode)
            
            'Make call on entries to return collection of all entries
            Set colLines = oLine.IBo_LoadMatches
            lngTopRow = sprdEOD.MaxRows
            
            'Display Lines
            For lngLineNo = 1 To colLines.Count Step 1
            
                ' VAT Code - can be "a", "b" or "c"
                'Debug.Print colLines(lngLineNo).VatCode
                    
                sprdEOD.MaxRows = sprdEOD.MaxRows + 1
                sprdEOD.Row = sprdEOD.MaxRows
                
                ' write sort columns for the line
                intSortSequence = intSortSequence + 1
                Call FillSortCols(sprdEOD, dteHoldDate, enstLine, intSortSequence, colEntries(lEntryNo).TransactionTime, colEntries(lEntryNo).TillID)
                
                sprdEOD.Col = COL_SKU
                sprdEOD.Text = colLines(lngLineNo).PartCode
                
                ' put Part Description (from STKMAS file) into COL_DESC
                sprdEOD.Col = COL_DESC
                
                ' have to get the flag for square metre discount
                ' from the STKMAS file

'                Set oPart = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
                Call oPart.IBo_ClearLoadFilter
                Call oPart.IBo_AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, colLines(lngLineNo).PartCode)
'                Call oPart.IBo_AddLoadFilter(FID_INVENTORY_ToppsRecord)
'                Call oPart.IBo_AddLoadFilter(FID_INVENTORY_Description)
'                Call oPart.IBo_AddLoadFilter(FID_INVENTORY_SizeDescription)
                
                Call oPart.IBo_LoadMatches
                
                'Debug.Print oPart.Description, oPart.ToppsRecord
                
                sprdEOD.Text = oPart.Description
                
                ' 25/04/03 kvb add a column for 'Size'
                sprdEOD.Col = COL_SIZE
                'sprdEOD.Text = oPart.SizeDescription
                
                'sprdEOD.Text = colLines(lngLineNo).Description
                sprdEOD.Col = COL_QTY
                sprdEOD.Text = colLines(lngLineNo).QuantitySold
                            
                ' this is "sold @"
                sprdEOD.Col = COL_SOLDAT
                sprdEOD.Text = colLines(lngLineNo).ExtendedValue
                
                dblTotalSoldAt = dblTotalSoldAt + colLines(lngLineNo).ExtendedValue
                
                If colEntries(lEntryNo).TradersCard <> CONST_RETAIL_CODE Then
                    dblTotalTradeSoldAt = dblTotalTradeSoldAt + colLines(lngLineNo).ExtendedValue
                End If
                
                ' Discount %
                ' ==========
                dblLookupTimesQuant = colLines(lngLineNo).LookUpPrice * colLines(lngLineNo).QuantitySold
                
                If dblLookupTimesQuant <> 0 Then
                    dblTotLookupTimesQuant = dblTotLookupTimesQuant + dblLookupTimesQuant
                    dblTotExtended = dblTotExtended + colLines(lngLineNo).ExtendedValue
                    
                    If colEntries(lEntryNo).TradersCard <> CONST_RETAIL_CODE Then
                        dblTotLookupTimesQuantTrade = dblTotLookupTimesQuantTrade + dblLookupTimesQuant
                        dblTotExtendedTrade = dblTotExtendedTrade + colLines(lngLineNo).ExtendedValue
                    End If
                    
                    sprdEOD.Col = COL_DISC
                    sprdEOD.Text = ((dblLookupTimesQuant - colLines(lngLineNo).ExtendedValue) _
                                   / dblLookupTimesQuant) * 100
                                                  
                    If colEntries(lEntryNo).TradersCard <> CONST_RETAIL_CODE Then
                        Debug.Print "KVB"; colEntries(lEntryNo).TransactionNo; "/"; lngLineNo; " "; sprdEOD.Text; "% ";
                        Debug.Print "dblLookupTimesQuant="; dblLookupTimesQuant;
                        Debug.Print " dblTotLookupTimesQuantTrade="; dblTotLookupTimesQuantTrade;
                        Debug.Print " dblTotExtendedTrade="; dblTotExtendedTrade
                    End If
                End If
                
'                If colLines(lngLineNo).ExtendedValue <> 0 Then
'                    sprdEOD.Col = COL_DISC
'                    sprdEOD.Text = ((colLines(lngLineNo).LookUpPrice * colLines(lngLineNo).QuantitySold) - colLines(lngLineNo).ExtendedValue) / colLines(lngLineNo).ExtendedValue
'                    sprdEOD.Col = COL_DISC
'                End If
                
                ' this is Margin
                ' 03/06/03 KVB Display margin with TWO decimal places
                sprdEOD.Col = COL_MARGIN
                strRoundingCalc = Format(colLines(lngLineNo).ExtendedValue / 1.175, "0.000")
                dblPriceExc = Val(Left$(strRoundingCalc, Len(strRoundingCalc) - 1))
                sprdEOD.Text = (dblPriceExc) - colLines(lngLineNo).ExtendedCost
                dblMargin = Val(sprdEOD.Text)
                
                dblTotalMargin = dblTotalMargin + dblMargin
                If colEntries(lEntryNo).TradersCard <> CONST_RETAIL_CODE Then
                    dblTotalTradeMargin = dblTotalTradeMargin + dblMargin
                End If
                
                sprdEOD.Col = COL_MARGPERC
                Call DebugMsg(MODULE_NAME, "cmdApply_Click", endlDebug, dblPriceExc & "-" & dblMargin)
                If colLines(lngLineNo).ExtendedValue <> 0 Then
                    sprdEOD.Text = dblMargin / (dblPriceExc) * 100
                End If
                sprdEOD.Col = COL_STOCK
                sprdEOD.Text = colLines(lngLineNo).OnHand - colLines(lngLineNo).QuantitySold
                Set oLine = colLines(lngLineNo)
            Next lngLineNo
            If colEntries(lEntryNo).RefundReasonCode <> "" Then
                sprdEOD.MaxRows = sprdEOD.MaxRows + 1
                sprdEOD.Row = sprdEOD.MaxRows
                
                intSortSequence = intSortSequence + 1
                Call FillSortCols(sprdEOD, dteHoldDate, enstLine, intSortSequence, colEntries(lEntryNo).TransactionTime, colEntries(lEntryNo).TillID)

                sprdEOD.Col = COL_SKU
                'Call sprdEOD.AddCellSpan(COL_SKU, sprdEOD.Row, 3, 1)
                sprdEOD.Text = colEntries(lEntryNo).RefundReasonCode
            End If
            If sprdEOD.MaxRows <> lngTopRow Then
                'Call sprdEOD.AddCellSpan(COL_TIME, lngTopRow + 1, 2, sprdEOD.MaxRows - lngTopRow)
            End If
        
            sprdEOD.Row = lngTopRow
            'Get Payment Lines, if any for Sale
            Set oLine = goDatabase.CreateBusinessObject(CLASSID_POSPAYMENT)
            Call oLine.IBo_AddLoadFilter(CMP_EQUAL, FID_POSPAYMENT_TillID, colEntries(lEntryNo).TillID)
            Call oLine.IBo_AddLoadFilter(CMP_EQUAL, FID_POSPAYMENT_TransactionDate, colEntries(lEntryNo).TranDate)
            Call oLine.IBo_AddLoadFilter(CMP_EQUAL, FID_POSPAYMENT_TransactionNumber, colEntries(lEntryNo).TransactionNo)
'            Call oLine.IBo_AddLoadFilter(CMP_GREATERTHAN, FID_pospayment_SequenceNo, 0)
            Call oLine.IBo_AddLoadField(FID_POSPAYMENT_TenderType)
            Call oLine.IBo_AddLoadField(FID_POSPAYMENT_TenderAmount)
            
            ' 22/04/03 kvb - also get the Authorisation Code
            Call oLine.IBo_AddLoadField(FID_POSPAYMENT_AuthorisationCode)
            
            'Make call on entries to return collection of all entries
            Set colLines = oLine.IBo_LoadMatches
            
            ' 22/04/03 kvb
            ' accumulate total cheque / mastercard payments for this Sale
            ' because any remainder is Cash.
            
            dblThisSale = 0
            dblThisSaleCash = 0
            strCarRegistrationNo = ""
            
            For lngLineNo = 1 To colLines.Count Step 1
                
                ' nb 100 paid is -100 in colLines(lngLineNo).TenderAmount
                '     so here we reverse the sign.
                dblThisSale = dblThisSale + (-1 * colLines(lngLineNo).TenderAmount)
                
                If colLines(lngLineNo).TenderType = CHEQUE_TENDER_CODE Then
                    strCarRegistrationNo = colLines(lngLineNo).AuthorisationCode
                End If
                
            Next lngLineNo

            ' now subtract this from the Total Sale Amount
            dblThisSaleCash = colEntries(lEntryNo).TotalSaleAmount - dblThisSale
                        
            dblTotalCash = dblTotalCash + dblThisSaleCash
            dblTotalOtherTender = dblTotalOtherTender + dblThisSale
            
            If dblThisSaleCash <> 0 Then
                
                sprdEOD.Row = lngTopRow
                sprdEOD.Col = COL_TENDER
                
                If mstrCashPmtCode <> "" Then
                    sprdEOD.Text = moTenderTypes.Description(mstrCashPmtCode)
                Else
                    sprdEOD.Text = "Cash"
                End If

                sprdEOD.Col = COL_PAYAMT
                sprdEOD.CellType = CellTypeNumber
                sprdEOD.Text = dblThisSaleCash
                
            End If
            
            'Display Lines
            For lngLineNo = 1 To colLines.Count Step 1
             
                If sprdEOD.Row = sprdEOD.MaxRows Then
                    sprdEOD.MaxRows = sprdEOD.MaxRows + 1
                    'Call sprdEOD.AddCellSpan(COL_TYPE, sprdEOD.MaxRows, COL_TENDER - COL_TYPE, 1)
                End If
                sprdEOD.Row = lngTopRow + lngLineNo
                
                 ' add sort sequence only if this is a new line
                sprdEOD.Col = COL_SORTTYPE
                If sprdEOD.Text = "" Then
                    intSortSequence = intSortSequence + 1
                    Call FillSortCols(sprdEOD, dteHoldDate, enstLine, intSortSequence, colEntries(lEntryNo).TransactionTime, colEntries(lEntryNo).TillID)
                End If
                
                sprdEOD.Col = COL_TENDER
                ' sprdEOD.Text = colLines(lngLineNo).Tendertype & "-" & moTenderTypes.Description(colLines(lngLineNo).Tendertype)
                sprdEOD.Text = moTenderTypes.Description(colLines(lngLineNo).TenderType)
                
                ' if the tender type is larger than allowed for in array dblTotalTendered()
                ' make the array bigger.
                If colLines(lngLineNo).TenderType > UBound(dblTotalTendered) Then
                    ReDim Preserve dblTotalTendered(colLines(lngLineNo).TenderType)
                    intCntTenderTypes = colLines(lngLineNo).TenderType
                End If
                
                ' accumulate in array of tendered (NB This is NOT cash)
                dblTotalTendered(colLines(lngLineNo).TenderType) = dblTotalTendered(colLines(lngLineNo).TenderType) - colLines(lngLineNo).TenderAmount
                
                
                ' 22/04/03 kvb As before please note that
                '     100 paid is -100 in colLines(lngLineNo).TenderAmount
                '     so here we reverse the sign.
                sprdEOD.Col = COL_PAYAMT
                sprdEOD.CellType = CellTypeNumber
                sprdEOD.Text = -colLines(lngLineNo).TenderAmount
            Next lngLineNo
            
            
            If sprdEOD.MaxRows <> lngTopRow Then
                'Call sprdEOD.AddCellSpan(COL_TIME, lngTopRow + 1, 2, sprdEOD.MaxRows - lngTopRow)
                'Call sprdEOD.AddCellSpan(COL_TILL, lngTopRow + 1, 1, sprdEOD.MaxRows - lngTopRow)
            End If
            
            ' 23/04/03 kvb If payment type of 2 detected, display the car registration no
            '              which was stored in auth code of payment.
            If strCarRegistrationNo <> "" Then
                sprdEOD.MaxRows = sprdEOD.MaxRows + 1
            
                'Call sprdEOD.AddCellSpan(COL_TIME, sprdEOD.MaxRows, 2, 1)
                'Call sprdEOD.AddCellSpan(COL_TENDER, sprdEOD.MaxRows, 1, 1)
                'Call sprdEOD.AddCellSpan(COL_PAYAMT, sprdEOD.MaxRows, 2, 1)
                
                sprdEOD.Row = sprdEOD.MaxRows
                
                Call FillSortCols(sprdEOD, dteHoldDate, enstCarReg, 0, colEntries(lEntryNo).TransactionTime, colEntries(lEntryNo).TillID)
                
                sprdEOD.Col = COL_TENDER
                sprdEOD.Text = "Car Reg."
                
                sprdEOD.Col = COL_PAYAMT
                sprdEOD.Text = strCarRegistrationNo
                
            End If
            
            ' 02/05/03 KVB finished one entry - draw a line across
            DrawLineAcross
            Call FillSortCols(sprdEOD, dteHoldDate, enstLineAcrossBelowEntry, 0, colEntries(lEntryNo).TransactionTime, colEntries(lEntryNo).TillID)
            
            lngLastRowToSort = sprdEOD.MaxRows
            
        Next lEntryNo
        
    End If 'any matched records to display
    
    lblNoLines.Caption = colEntries.Count
    
    ' 24/04.03 KVB add totals to the spreadsheet
    sprdEOD.MaxRows = sprdEOD.MaxRows + 3
    sprdEOD.Row = sprdEOD.MaxRows - 2
    
    ' first a seperator row
    ' set the height of the row specified to 1 with background colour of black
    sprdEOD.Col = -1
    sprdEOD.RowHeight(sprdEOD.Row) = 1
    sprdEOD.BackColor = vbBlack
    
    sprdEOD.Row = sprdEOD.MaxRows - 1
    
    sprdEOD.Col = COL_DESC
    sprdEOD.Text = "Totals"
    
    sprdEOD.Col = COL_AMOUNT
    sprdEOD.Text = dblTotalAmount
    
    sprdEOD.Col = COL_VAT
    sprdEOD.Text = dblTotalVat
    
    sprdEOD.Col = COL_SOLDAT
    sprdEOD.Text = dblTotalSoldAt
    
    If dblTotLookupTimesQuant <> 0 Then
        sprdEOD.Col = COL_DISC
        sprdEOD.Text = ((dblTotLookupTimesQuant - dblTotExtended) / dblTotLookupTimesQuant) * 100
    End If
    
    
    sprdEOD.Col = COL_MARGIN
    sprdEOD.Text = dblTotalMargin
    
    sprdEOD.Col = COL_TENDER
    If mstrCashPmtCode <> "" Then
        sprdEOD.Text = moTenderTypes.Description(mstrCashPmtCode)
    Else
        sprdEOD.Text = "Cash"
    End If

    sprdEOD.Col = COL_PAYAMT
    sprdEOD.CellType = CellTypeNumber
    sprdEOD.Text = dblTotalCash
   
    ' add Trade Sales Total Line to the spreadsheet
    sprdEOD.Row = sprdEOD.Row + 1
    
    sprdEOD.Col = COL_DESC
    sprdEOD.Text = "Including Trade Sales of"
    
    sprdEOD.Col = COL_AMOUNT
    sprdEOD.Text = dblTotalTradeAmount
    
    sprdEOD.Col = COL_VAT
    sprdEOD.Text = dblTotalTradeVat
    
    sprdEOD.Col = COL_SOLDAT
    sprdEOD.Text = dblTotalTradeSoldAt
    
    If dblTotLookupTimesQuantTrade <> 0 Then
        sprdEOD.Col = COL_DISC
        sprdEOD.Text = ((dblTotLookupTimesQuantTrade - dblTotExtendedTrade) / dblTotLookupTimesQuantTrade) * 100
    End If
    
    sprdEOD.Col = COL_MARGIN
    sprdEOD.Text = dblTotalTradeMargin
    
    
    lngTopRow = sprdEOD.MaxRows
    'Step through Misc Income List and check if any need displaying
    For intTenderType = 1 To 10 Step 1
        If madblMiscInc(intTenderType) > 0 Then
            sprdEOD.MaxRows = sprdEOD.MaxRows + 1
            sprdEOD.Row = sprdEOD.MaxRows
            sprdEOD.Col = COL_DESC
            sprdEOD.Text = "Including " & moMiscIncomeDesc.MiscIncomeAccountNo(intTenderType)
            sprdEOD.Col = COL_AMOUNT
            sprdEOD.Text = madblMiscInc(intTenderType)
'            sprdEOD.Col = COL_VAT
'            sprdEOD.Text = 0
'            sprdEOD.Col = COL_SOLDAT
'            sprdEOD.Text = 0
'            sprdEOD.Col = COL_DISC
'            sprdEOD.Text = 0
'            sprdEOD.Col = COL_MARGIN
'            sprdEOD.Text = 0
        End If
    Next intTenderType
    
    'sprdEOD.Col = COL_TENDER
    'sprdEOD.Text = moTenderTypes.Description(1)
     
    'sprdEOD.Col = COL_PAYAMT
    'sprdEOD.CellType = CellTypeNumber
    'sprdEOD.Text = dblTotalTendered(1)
   
    sprdEOD.Row = lngTopRow
    For intTenderType = 0 To intCntTenderTypes Step 1
        
        If (dblTotalTendered(intTenderType) <> 0) And (moTenderTypes.TenderType(intTenderType) < 100) Then
        
            sprdEOD.Col = COL_TENDER
            sprdEOD.Text = moTenderTypes.Description(intTenderType)
            
            sprdEOD.Col = COL_PAYAMT
            sprdEOD.CellType = CellTypeNumber
            sprdEOD.Text = dblTotalTendered(intTenderType)
            
            If (intTenderType <> intCntTenderTypes) And (sprdEOD.Row = sprdEOD.MaxRows) Then
                sprdEOD.MaxRows = sprdEOD.MaxRows + 1
            End If
            sprdEOD.Row = sprdEOD.Row + 1
            
        End If
        
    Next intTenderType
    
    'sprdEOD.Col = COL_TENDER
    'sprdEOD.Text = "Other"
     
    'sprdEOD.Col = COL_PAYAMT
    'sprdEOD.CellType = CellTypeNumber
    'sprdEOD.Text = dblTotalOtherTender
   
    ' display  Totals  and  Trade Sales Totals  in frame called fraTotals
    lblTotalAmount(entdAmount).Caption = Format$(dblTotalAmount, mstrValueFormat)
    lblTotalAmount(entdAmountTradeSales).Caption = Format$(dblTotalTradeAmount, mstrValueFormat)
    lblTotalAmount(entdSoldAt).Caption = Format$(dblTotalSoldAt, mstrValueFormat)
    lblTotalAmount(entdSoldAtTradeSales).Caption = Format$(dblTotalTradeSoldAt, mstrValueFormat)
    lblTotalAmount(entdMargin).Caption = Format$(dblTotalMargin, mstrValueFormat)
    lblTotalAmount(entdMarginTradeSales).Caption = Format$(dblTotalTradeMargin, mstrValueFormat)
    lblTotalAmount(entdTenderedCash).Caption = Format$(dblTotalCash, mstrValueFormat)
    lblTotalAmount(entdTenderedOther).Caption = Format$(dblTotalOtherTender, mstrValueFormat)
    
    ' if amount received (cash plus other tender) not equal total sold
    ' flag it with red background
    If dblTotalCash + dblTotalOtherTender = dblTotalAmount Then
        lblTotalAmount(entdAmount).BackColor = vbWhite
        lblTotalAmount(entdTenderedCash).BackColor = vbWhite
        lblTotalAmount(entdTenderedOther).BackColor = vbWhite
    Else
        lblTotalAmount(entdAmount).BackColor = vbRed
        lblTotalAmount(entdTenderedCash).BackColor = vbRed
        lblTotalAmount(entdTenderedOther).BackColor = vbRed
    End If
    
    ucProgress.Visible = False
    sbStatus.Panels(PANEL_INFO).Text = ""
    
    If Val(lblNoLines.Caption) > 0 Then
        fraTotals.Visible = True
        cmdPrint.Visible = True
        sbStatus.Panels(PANEL_INFO).Text = ""
    Else
        sbStatus.Panels(PANEL_INFO).Text = "No matches found"
    End If
    

    varSortKeys = Array(COL_SORTDATE, COL_SORTTIME, COL_SORTTILLID, COL_SORTTYPE, COL_SORTSEQUENCE)
    
    sprdEOD.SortKey(1) = COL_SORTDATE
    sprdEOD.SortKeyOrder(1) = SortKeyOrderAscending

    sprdEOD.SortKey(2) = COL_SORTTIME
    sprdEOD.SortKeyOrder(2) = SortKeyOrderAscending
    
    sprdEOD.SortKey(3) = COL_SORTTILLID
    sprdEOD.SortKeyOrder(3) = SortKeyOrderAscending
    
    sprdEOD.SortKey(4) = COL_SORTTYPE
    sprdEOD.SortKeyOrder(4) = SortKeyOrderAscending
    
    sprdEOD.SortKey(5) = COL_SORTSEQUENCE
    sprdEOD.SortKeyOrder(5) = SortKeyOrderAscending
    
    Call sprdEOD.Sort(-1, -1, -1, lngLastRowToSort, SortByRow)
    
    sprdEOD.MaxCols = COL_TDATE
    
    cmdApply.Enabled = True
    sprdEOD.ReDraw = True
    sprdEOD.Refresh
    Call sprdEOD.SetFocus
    cmdApply.Caption = "F7-Refresh"
    Screen.MousePointer = vbNormal
    Me.Refresh
    
    Exit Sub
    
DisplayError:

    Call Err.Report(MODULE_NAME, "cmdApply_click", 1, True, "Error displaying data", "Error occurred when display selected criteria")
    ucProgress.Visible = False
    sbStatus.Panels(PANEL_INFO).Text = ""
    lblNoLines.Caption = "Error(" & Err.Number & ")"
    Screen.MousePointer = vbNormal
    Exit Sub
    Resume Next

End Sub

Private Sub cmdApply_GotFocus()

    cmdApply.FontBold = True

End Sub

Private Sub cmdApply_LostFocus()
    
    cmdApply.FontBold = False

End Sub

Private Sub cmdExit_Click()

    Unload Me

End Sub

Private Sub cmdPrint_Click()

Dim lngShadowColor        As Long
Dim blnGridShowHorizontal As Boolean
Dim blnGridShowVertical   As Boolean
Dim strHeader             As String
Dim oPrintStore           As cStore

    '25/04/03  get name and address for current store to put at top of document
    ' Debug.Print goSession.CurrentEnterprise.IEnterprise_StoreNumber
    If oPrintStore Is Nothing Then
        Set oPrintStore = goSession.Database.CreateBusinessObject(CLASSID_STORE)
        Call oPrintStore.IBo_AddLoadFilter(CMP_EQUAL, FID_STORE_StoreNumber, goSession.CurrentEnterprise.IEnterprise_StoreNumber)
        Call oPrintStore.IBo_Load
    End If

    ' 23/04/03 kvb when printing change the header background to white
    '              & hide the grid lines.
    lngShadowColor = sprdEOD.ShadowColor
    blnGridShowHorizontal = sprdEOD.GridShowHoriz
    blnGridShowVertical = sprdEOD.GridShowVert
    
    ' ensure the background color of the column and row headers is white
    sprdEOD.ShadowColor = vbWhite

    sprdEOD.GridShowHoriz = False
    sprdEOD.GridShowVert = False
    
    ' 06/05/03 KVB use PrintColor to ensure that horizontal lines (inverted color) are shown
    sprdEOD.PrintColor = True
        
    ' 02/05/03  KVB  remove Printed dd/mm/yy from strHeader
    strHeader = "Store No: " & Str$(goSession.CurrentEnterprise.IEnterprise_StoreNumber) & "  " & _
                oPrintStore.AddressLine1 & "  -  End of Day Report  "
    Select Case (cmbDateCrit.ItemData(cmbDateCrit.ListIndex))
        Case (CRIT_EQUALS):
                    strHeader = strHeader & "for "
                    If labDay(0).Visible = True Then strHeader = strHeader & labDay(0).Caption & " "
                    strHeader = strHeader & dtxtStartDate.Text
'Private Const CRIT_LESSTHAN    As Long = 2
'Private Const CRIT_GREATERTHAN As Long = 1
'Private Const CRIT_ALL         As Long = 3
'Private Const CRIT_FROM        As Long = 4
        Case (CRIT_WEEKENDING):
                    strHeader = strHeader & "from "
                    If labDay(0).Visible = True Then strHeader = strHeader & labDay(1).Caption & " "
                    strHeader = strHeader & dtxtStartDate.Text & " to "
                    If labDay(1).Visible = True Then strHeader = strHeader & labDay(1).Caption & " "
                    strHeader = strHeader & dtxtEndDate.Text
    End Select
    
    ' /c is Centre text      /fb1 is Font bold on
    sprdEOD.PrintHeader = "/c/fb1" & strHeader
    
    sprdEOD.PrintJobName = "End of Day Report"
    
    sprdEOD.PrintFooter = GetFooter

    sprdEOD.PrintOrientation = PrintOrientationLandscape
    Load frmPreview
    Call frmPreview.SetPreview("End of Day Report", sprdEOD, mstrExitCaption)
    frmPreview.chkSmartPrint.Value = vbChecked ' default to SmartPrint
    frmPreview.Show vbModal
    Unload frmPreview
    
    sprdEOD.ShadowColor = lngShadowColor
    sprdEOD.GridShowHoriz = blnGridShowHorizontal
    sprdEOD.GridShowVert = blnGridShowVertical
    
    sprdEOD.Refresh
    
    ' clean up
    On Error Resume Next
    Set oPrintStore = Nothing
        
End Sub
Private Sub cmdPrint_GotFocus()
    
    cmdPrint.FontBold = True

End Sub

Private Sub cmdPrint_LostFocus()
    
    cmdPrint.FontBold = False

End Sub

Private Sub cmdExit_GotFocus()
    
    cmdExit.FontBold = True

End Sub

Private Sub cmdExit_LostFocus()
    
    cmdExit.FontBold = False

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case (Shift)
        Case (0):
            Select Case (KeyCode)
                Case vbKeyF4 ' if F4 then call 'Show All Totals' button
                    If cmdShowCash.Visible = True Then cmdShowCash_Click
                Case (vbKeyF7):  'if F7 then call search
                    If cmdApply.Visible = True Then Call cmdApply_Click
                Case (vbKeyF9):  'if F9 then call print
                    If cmdPrint.Visible = True Then Call cmdPrint_Click
                Case (vbKeyF10): 'if F10 then exit
                    Call cmdExit_Click
            End Select 'Key pressed with no Shift/Alt combination
    End Select

End Sub
Private Sub Form_Load()

Dim lngItem  As Long
Dim oLookUp  As cTypesOfSaleOption
Dim colList  As Collection
    
    Me.Show
    
    sbStatus.Panels(PANEL_INFO).Text = "Accessing session"
    ' Set up the object and field that we wish to select on
    Set goRoot = GetRoot
    'Display initial values in Status Bar
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    ucProgress.FillColor = Me.BackColor
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    
    Call InitialiseStatusBar(sbStatus)
    
    ' 22/04/03 kvb  get pointer from param file to word 'Cash'
    mstrCashPmtCode = goSession.GetParameter(CASH_PARAMETER)
    If mstrCashPmtCode = "" Then
        Call MsgBox("No pointer set to description for 'Cash'", vbInformation, "WARNING - pointer not set.")
    End If
    
    ' 24/04/03 kvb  get display format for pounds & pence from param file
    mlngPayAmtDecPlaces = goSession.GetParameter(PRM_VALUE_DEC_PLACES)
    mstrValueFormat = Replace(Space(mlngPayAmtDecPlaces), " ", "0")
    If mstrValueFormat <> "" Then
        mstrValueFormat = "0." & mstrValueFormat
    End If
    
    ' 25/04/03 kvb  get no of decimal places for quantity & stock
    mlngQtyDecNum = goSession.GetParameter(PRM_QTY_DEC_PLACES)
    
    sbStatus.Panels(PANEL_INFO).Text = "Loading Editor"
    ucProgress.Visible = True
    ucProgress.Caption1 = "Loading List from Database"
    DoEvents
    
    sbStatus.Panels(PANEL_INFO).Text = "Populating grid"
    DoEvents
    
    'Only required if Date selection is used
    mstrDateFmt = UCase$(goSession.GetParameter(125))
    dtxtStartDate.DateFormat = mstrDateFmt
    dtxtStartDate.Text = Format(Now(), mstrDateFmt)
    dtxtEndDate.DateFormat = mstrDateFmt
    dtxtEndDate.Text = Format(Now(), mstrDateFmt)
    
    cmbDateCrit.Clear
    Call cmbDateCrit.AddItem("After")
    cmbDateCrit.ItemData(0) = CRIT_GREATERTHAN
    Call cmbDateCrit.AddItem("Before")
    cmbDateCrit.ItemData(1) = CRIT_LESSTHAN
    Call cmbDateCrit.AddItem("Dated")
    cmbDateCrit.ItemData(2) = CRIT_EQUALS
    Call cmbDateCrit.AddItem("All")
    cmbDateCrit.ItemData(3) = CRIT_ALL
    Call cmbDateCrit.AddItem("From")
    cmbDateCrit.ItemData(4) = CRIT_FROM
    Call cmbDateCrit.AddItem("Week Ending")
    cmbDateCrit.ItemData(5) = CRIT_WEEKENDING
    
    For lngItem = 0 To cmbDateCrit.ListCount - 1 Step 1
        If cmbDateCrit.ItemData(lngItem) = CRIT_ALL Then
            cmbDateCrit.ListIndex = lngItem
            Exit For
        End If
    Next lngItem
    'Only required if Date selection-END
    
    mstrMaxValidCashier = GetLastValidUserID
    
    ' Create a collection for types of Sale - file TOSOPT - CLASSID_TYPESOFSALEOPTION
    Set oLookUp = goDatabase.CreateBusinessObject(CLASSID_TYPESOFSALEOPTION)
    Call oLookUp.IBo_AddLoadField(FID_TYPESOFSALEOPTION_TypeOfSaleCode)
    Call oLookUp.IBo_AddLoadField(FID_TYPESOFSALEOPTION_Description)
    Set colList = oLookUp.IBo_LoadMatches
    
    Set mcolTypesOfSales = New Collection
    For lngItem = 1 To colList.Count Step 1
        Call mcolTypesOfSales.Add("" & colList(lngItem).Description, _
                                  "" & colList(lngItem).TypeOfSaleCode)
    Next lngItem
    Set colList = Nothing
    
    'Tender Types
    Set moTenderTypes = goDatabase.CreateBusinessObject(CLASSID_TENDERTYPES)
    Call moTenderTypes.IBo_Load


    ''User for cashier and salesman
    'Set oLookUp = goDatabase.CreateBusinessObject(CLASSID_USER)
    'Call oLookUp.IBO_AddLoadField(FID_USER_EmployeeID)
    'Call oLookUp.IBO_AddLoadField(FID_USER_FullName)
    'Set colList = oLookUp.IBO_LoadMatches
    'Set mcolUsers = New Collection
    'For lngItem = 1 To colList.Count Step 1
    '    Call mcolUsers.Add("" & colList(lngItem).FullName, "" & colList(lngItem).EmployeeId)
    'Next lngItem
    'Set colList = Nothing

    ucProgress.Visible = False
    sbStatus.Panels(PANEL_INFO).Text = ""
    
    ' hide Cash & Other in fraTotals
    lblTots(entdTenderedCash).Visible = False
    lblTotalAmount(entdTenderedCash).Visible = False
    lblTots(entdTenderedOther).Visible = False
    lblTotalAmount(entdTenderedOther).Visible = False
    
    Call LoadDiscounts
    
    sprdEOD.Col = COL_VAT
    sprdEOD.ColHidden = True
    
    Call DecodeParameters(goSession.ProgramParams)
    
    If mblnFromBanking = True Then 'remove additional criteri - not required
        For lngItem = cmbDateCrit.ListCount - 1 To 0 Step -1
            If cmbDateCrit.ItemData(lngItem) = CRIT_GREATERTHAN Then Call cmbDateCrit.RemoveItem(lngItem)
            If cmbDateCrit.ItemData(lngItem) = CRIT_LESSTHAN Then Call cmbDateCrit.RemoveItem(lngItem)
            If cmbDateCrit.ItemData(lngItem) = CRIT_ALL Then Call cmbDateCrit.RemoveItem(lngItem)
            If cmbDateCrit.ItemData(lngItem) = CRIT_FROM Then Call cmbDateCrit.RemoveItem(lngItem)
        Next lngItem
        
        'after delete extra criteria, re-find ALL position
        For lngItem = 0 To cmbDateCrit.ListCount - 1 Step 1
            If cmbDateCrit.ItemData(lngItem) = CRIT_ALL Then
                cmbDateCrit.ListIndex = lngItem
                Exit For
            End If
        Next lngItem
    End If
    
    If mstrDestCode = DEST_REPORT_CODE_PREVIEW Then
        Call cmdPrint_Click
        End
    End If
    If mstrDestCode = DEST_REPORT_CODE_PRINTER Then
        Call cmdPrint_Click
        End
    End If

End Sub
Private Sub DecodeParameters(strCommand As String)

Const TRAN_START_DATE    As String = "TS"
Const SELECTION_DATE_DB  As String = "DB"  ' This program called from Daily Banking
Const TRAN_END_DATE      As String = "TD"
Const TRAN_WEEKENDING    As String = "WE"
Const SHOW_BUTTON        As String = "SB"

Dim strTempDate  As String
Dim dteTempDate  As Date
Dim strSection   As String
Dim strSignFound As String
Dim vntSection   As Variant
Dim lngSectNo    As Long
Dim lngItem      As Long
Dim blnSetOpts   As Boolean

    vntSection = Split(strCommand, ",")
    
    blnSetOpts = False
    mblnDateFromParam = False
    
    For lngSectNo = 0 To UBound(vntSection) Step 1
        
        'Debug.Print lngSectNo & "-" & vntSection(lngSectNo)
        strSection = vntSection(lngSectNo)
        Select Case (Left$(strSection, 2))
            Case TRAN_START_DATE, SELECTION_DATE_DB
                mblnDateFromParam = True
                dteTempDate = GetParamDate(Right$(strSection, 8))
                dtxtStartDate.Text = Format(dteTempDate, mstrDateFmt)
                strTempDate = Mid$(strSection, 3) ' strip off the TS or the DB
                
                strSignFound = Left$(strTempDate, Len(strTempDate) - 8)
                blnSetOpts = True
                
                cmbDateCrit.ListIndex = SetDateCrit(strSignFound, cmbDateCrit)
                If (Left$(strSection, 2) = SELECTION_DATE_DB) Then
'                    Me.MinButton = False
                    cmdExit.Caption = "F10-Back to Banking"
                    mstrExitCaption = "Back to Banking"
                    mblnFromBanking = True
                End If
                
            Case TRAN_WEEKENDING
                mblnDateFromParam = True
                dteTempDate = GetParamDate(Right$(strSection, 8))
                dtxtEndDate.Text = Format(dteTempDate, mstrDateFmt)
                cmbDateCrit.ListIndex = SetDateCrit(TRAN_WEEKENDING, cmbDateCrit)
                blnSetOpts = True
                        
            Case TRAN_END_DATE
                mblnDateFromParam = True
                dteTempDate = GetParamDate(Right$(strSection, 8))
                dtxtEndDate.Text = Format(dteTempDate, mstrDateFmt)
                blnSetOpts = True
                
            Case DEST_CODE
                strTempDate = Mid$(strSection, 3)
                mstrDestCode = strTempDate
                blnSetOpts = True
                
            Case Is = SHOW_BUTTON
                cmdShowCash.Visible = True
        End Select
    
    Next lngSectNo
    
    If blnSetOpts = True Then Call cmdApply_Click

End Sub

Private Sub LoadDiscounts()

    Set mcolDiscReasons = New Collection
    
    ' 22/04/03 kvb  add code "00" and "" to catch inconsistent data
    Call mcolDiscReasons.Add("?", "00")
    Call mcolDiscReasons.Add("? no code", "")
    
    Call mcolDiscReasons.Add("Job Lot", "01")
    Call mcolDiscReasons.Add("Non Core", "02")
    Call mcolDiscReasons.Add("Price Change", "03")
    Call mcolDiscReasons.Add("Damaged", "04")
    Call mcolDiscReasons.Add("Make the Sale", "05")
    Call mcolDiscReasons.Add("Trader Not Yet Registered", "06")
    Call mcolDiscReasons.Add("Combined > 4 Sq.M.", "07")
    Call mcolDiscReasons.Add("Package Deal", "08")
    Call mcolDiscReasons.Add("Voucher", "09")
    Call mcolDiscReasons.Add("Long Term Arrangement", "10")
    
End Sub

Private Sub Form_Resize()

Dim lSpacing As Long

    If Me.WindowState = vbMinimized Then Exit Sub
    lSpacing = sprdEOD.Left
    
    'Check form is not below minimum width
    If Me.Width < cmdPrint.Width + cmdExit.Width + lblNoLines.Width + (lSpacing * 4) Then
        Me.Width = cmdPrint.Width + cmdExit.Width + lblNoLines.Width + (lSpacing * 4)
        Exit Sub
    End If
    'Check form is not below minimum height
    If Me.Height < 6480 Then
        Me.Height = 6480
        Exit Sub
    End If
    
    'relocate Progress bar
    If (Me.Width - ucProgress.Width) > 0 Then
        ucProgress.Left = (Me.Width - ucProgress.Width) / 2
    Else
        ucProgress.Left = 0
    End If
    If (Me.Height - ucProgress.Height) > 0 Then
        ucProgress.Top = (Me.Height - ucProgress.Height) / 2
    Else
        ucProgress.Top = 0
    End If
    
    'start resizing
    sprdEOD.Width = Me.Width - sprdEOD.Left * 2 - BORDER_WIDTH * 2
    ' sprdEOD.Height = Me.Height - (cmdExit.Height + sprdEOD.Top + (lSpacing * 3) + (BORDER_WIDTH * 4) + sbStatus.Height)
    sprdEOD.Height = Me.Height - (sprdEOD.Top + fraTotals.Height + (lSpacing * 8))
    
    cmdExit.Top = sprdEOD.Top + sprdEOD.Height + lSpacing
    cmdPrint.Top = cmdExit.Top
    
    lblNoLineslbl.Top = cmdExit.Top + cmdExit.Height ' + lSpacing / 2
    'lblNoLineslbl.Top = frCriteria.Top + (frCriteria.Height / 2.5)
    
    lblNoLines.Top = lblNoLineslbl.Top
    
    cmdPrint.Left = (sprdEOD.Width - cmdPrint.Width) + sprdEOD.Left
    
    fraTotals.Top = (sprdEOD.Top + sprdEOD.Height) + (lSpacing * 0.1)
    ' centre the totals frame
    fraTotals.Left = (Me.Width - fraTotals.Width) / 2
    
End Sub


Private Function DrawLineAcross()

    sprdEOD.MaxRows = sprdEOD.MaxRows + 1
    sprdEOD.Row = sprdEOD.MaxRows
    
    sprdEOD.Col = -1
    ' set the height of the row specified to 1 with background colour of black
     sprdEOD.RowHeight(sprdEOD.Row) = 1
    ' sprdEOD.FontSize = 4
    sprdEOD.BackColor = vbBlack
    sprdEOD.ForeColor = vbWhite
    
    'Call sprdEOD.AddCellSpan(COL_TIME, sprdEOD.Row, COL_TDATE, 1)
        
End Function

Private Function SetDateCrit(ByVal strSign As String, cmbDateRange As ComboBox) As Long

Dim intItemValue As Integer
Dim intItem      As Integer

    SetDateCrit = -1
    
    Select Case UCase(Trim(strSign))
    Case Is = ">"
      intItemValue = CRIT_GREATERTHAN
    Case Is = "<"
      intItemValue = CRIT_LESSTHAN
    Case Is = "="
      intItemValue = CRIT_EQUALS
    Case Is = "ALL" ' all
      intItemValue = CRIT_ALL
    Case Is = ">="
      intItemValue = CRIT_FROM
    Case Is = "WE"
      intItemValue = CRIT_WEEKENDING
    Case Else
      Call MsgBox("Invalid sign " & strSign & vbCrLf & _
                  "passed to date selection.", vbInformation, "Error decoding date parameter")
      Exit Function
    End Select
    
    ' look for the value intItemValue in cmbDateRange.ItemData(intItem)
    For intItem = 0 To cmbDateRange.ListCount - 1 Step 1
        If cmbDateRange.ItemData(intItem) = intItemValue Then
            ' found intItemValue set the pointer to the ListIndex
            SetDateCrit = intItem
            Exit For
        End If
    Next intItem
    
End Function

Private Sub CheckDateRange()
    
Dim dteEndDate            As Date
Dim dteStartDate          As Date

    dteEndDate = GetDate(dtxtEndDate.Text)
    dteStartDate = GetDate(dtxtStartDate.Text)
    
    ' If dates entered back to front - swap them!
    ' e.g. If user entered from 12/05/03 to 10/05/03
    '      change it to    from 10/05/03 to 12/05/03
    If dteStartDate > dteEndDate Then
        dtxtEndDate.Text = Format(dteStartDate, mstrDateFmt)
        dtxtStartDate.Text = Format(dteEndDate, mstrDateFmt)
        dteEndDate = GetDate(dtxtEndDate.Text)
        Call DisplayTheDay(dtxtEndDate.Text, 1)
        dteStartDate = GetDate(dtxtStartDate.Text)
        Call DisplayTheDay(dtxtStartDate.Text, 0)
    End If
    
End Sub

Private Function CheckWeekEnding() As Boolean

Dim dteEndDate       As Date
Dim dteSuggestedDate As Date
Dim strMessage       As String
Dim intResponse      As Integer

    CheckWeekEnding = False

    ' the end date must be a Saturday
    dteEndDate = GetDate(dtxtEndDate.Text)
    
    ' if it is not, go on to next Saturday after the date entered
    If Weekday(dteEndDate) <> vbSaturday Then
        Call FindNextSaturday(dteEndDate, dteSuggestedDate)
        strMessage = "Date MUST be a Saturday" & vbCrLf & vbCrLf & _
                     "Do you want to set it to " & Format(dteSuggestedDate, mstrDateFmt)
        intResponse = MsgBox(strMessage, vbQuestion + vbYesNo + vbDefaultButton1, "Selection period")
        If intResponse = vbNo Then
            On Error Resume Next
            dtxtEndDate.SetFocus
            Exit Function
        End If
        
        dteEndDate = dteSuggestedDate
        dtxtEndDate.Text = Format(dteEndDate, mstrDateFmt)
    End If
    
    ' start date must be 6 days before end date.
    dtxtStartDate.Text = Format(DateAdd("d", -6, dteEndDate), mstrDateFmt)
    CheckWeekEnding = True
    Me.Refresh
    
End Function

Private Sub FindNextSaturday(ByVal dteEndDate As Date, ByRef dteSuggestedDate As Date)

Dim intDaysMovedOn As Integer

    On Error GoTo FindNextSaturday_End
    
    dteSuggestedDate = dteEndDate
    intDaysMovedOn = 0
    
    Do
        If Weekday(dteSuggestedDate) = vbSaturday Then
            Exit Do
        Else
            intDaysMovedOn = intDaysMovedOn + 1
            dteSuggestedDate = DateAdd("d", 1, dteSuggestedDate)  ' go forward
        End If
    Loop Until intDaysMovedOn > 7 ' should never happen
    
FindNextSaturday_End:

End Sub

Private Sub DisplayTheDay(ByVal strDate As String, ByVal intLabNo As Integer)

' show the day (e.g. Saturday) in labDay().Caption

Dim dteTheDate As Date

    On Error Resume Next
    labDay(intLabNo).Caption = ""
    dteTheDate = GetDate(strDate)
    labDay(intLabNo).Caption = Format(dteTheDate, "dddd")
    
End Sub

Private Sub FillSortCols(sprdDisp As fpSpread, _
                        dteHoldDate As Date, _
                        intType As Integer, _
                        intSequence As Integer, _
                        sTransactionTime As String, _
                        sTillId As String)
    
    sprdDisp.Col = COL_SORTDATE
    sprdDisp.Text = Format(dteHoldDate, "YYYYmmDD")
    
    ' strTransactionTime is in format hhmmss
    sprdDisp.Col = COL_SORTTIME
    sprdDisp.Text = "T" & Format(sTransactionTime, "@@:@@:@@")
    
    sprdDisp.Col = COL_SORTTILLID
    sprdDisp.Text = sTillId
    
    sprdDisp.Col = COL_SORTTYPE
    sprdDisp.Text = intType
    
    sprdDisp.Col = COL_SORTSEQUENCE
    sprdDisp.Text = intSequence
    
End Sub


