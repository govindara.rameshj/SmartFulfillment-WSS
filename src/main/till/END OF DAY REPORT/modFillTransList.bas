Attribute VB_Name = "modFillTransList"
Option Explicit

Public Sub DisplayEODLine(oLine As Object, sprdDisp As vaSpread, dteTrandate As Date, strTillID As String)

Dim intSortSequence As Integer
Dim oPartBOBO As Object

    sprdDisp.MaxRows = sprdDisp.MaxRows + 1
    sprdDisp.Row = sprdDisp.MaxRows
    
    ' write sort columns for the line
    intSortSequence = intSortSequence + 1
    Call FillSortCols(sprdDisp, dteHoldDate, enstLine, intSortSequence, colEntries(lEntryNo).TransactionTime, colEntries(lEntryNo).TillID)
    
    sprdDisp.Col = COL_SKU
    sprdDisp.Text = oLine.PartCode
    
    ' put Part Description (from STKMAS file) into COL_DESC
    sprdDisp.Col = COL_DESC
    
    ' have to get the flag for square metre discount
    ' from the STKMAS file

'                Set oPartBO = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
    Call oPartBO.IBo_ClearLoadFilter
    Call oPartBO.IBo_AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, oLine.PartCode)
'                Call oPartBO.IBo_AddLoadFilter(FID_INVENTORY_ToppsRecord)
'                Call oPartBO.IBo_AddLoadFilter(FID_INVENTORY_Description)
'                Call oPartBO.IBo_AddLoadFilter(FID_INVENTORY_SizeDescription)
    
    Call oPartBO.IBo_LoadMatches
    
    'Debug.Print oPartBO.Description, oPartBO.ToppsRecord
    
    ' if there is an "M" in the ToppsRecord field and this is a retail sale
    ' then "Square metre discount" is allowed.
    ' following discussions with M.Milne this code is NOT implemented
    If (oPartBO.ToppsRecord = "M") And (colEntries(lEntryNo).TradersCard = CONST_RETAIL_CODE) Then
        blnSqMetreDiscount = True
    Else
        blnSqMetreDiscount = False
    End If
    
    sprdDisp.Text = oPartBO.Description
    
    ' 25/04/03 kvb add a column for 'Size'
    sprdDisp.Col = COL_SIZE
    sprdDisp.Text = oPartBO.SizeDescription
    
    'sprdDisp.Text = oline.Description
    sprdDisp.Col = COL_QTY
    sprdDisp.Text = oLine.QuantitySold
                
    ' this is "sold @"
    sprdDisp.Col = COL_SOLDAT
    sprdDisp.Text = oLine.ExtendedValue
    
    If colEntries(lEntryNo).TradersCard <> CONST_RETAIL_CODE Then
        dblTotalTradeSoldAt = dblTotalTradeSoldAt + oLine.ExtendedValue
    End If
    
    ' Discount %
    ' ==========
    dblLookupTimesQuant = oLine.LookUpPrice * oLine.QuantitySold
    
    If dblLookupTimesQuant <> 0 Then
        
        sprdDisp.Col = COL_DISC
        sprdDisp.Text = ((dblLookupTimesQuant - oLine.ExtendedValue) _
                       / dblLookupTimesQuant) * 100
                                      
        If colEntries(lEntryNo).TradersCard <> CONST_RETAIL_CODE Then
            Debug.Print "KVB"; colEntries(lEntryNo).TransactionNo; "/"; lngLineNo; " "; sprdDisp.Text; "% ";
            Debug.Print "dblLookupTimesQuant="; dblLookupTimesQuant;
            Debug.Print " dblTotLookupTimesQuantTrade="; dblTotLookupTimesQuantTrade;
            Debug.Print " dblTotExtendedTrade="; dblTotExtendedTrade
        End If
    End If
    
'                If oline.ExtendedValue <> 0 Then
'                    sprdDisp.Col = COL_DISC
'                    sprdDisp.Text = ((oline.LookUpPrice * oline.QuantitySold) - oline.ExtendedValue) / oline.ExtendedValue
'                    sprdDisp.Col = COL_DISC
'                End If
    
    ' this is Margin
    ' 03/06/03 KVB Display margin with TWO decimal places
    sprdDisp.Col = COL_MARGIN
    dblPriceExc = Val(Format(oLine.ExtendedValue / 1.175, "0.00"))
    sprdDisp.Text = (dblPriceExc) - oLine.ExtendedCost
    dblMargin = Val(sprdDisp.Text)
    
    sprdDisp.Col = COL_MARGPERC
    Call DebugMsg(MODULE_NAME, "cmdApply_Click", endlDebug, dblPriceExc & "-" & dblMargin)
    If oLine.ExtendedValue <> 0 Then sprdDisp.Text = dblMargin / (dblPriceExc) * 100
    
    sprdDisp.Col = COL_STOCK
    sprdDisp.Text = oLine.OnHand - oLine.QuantitySold
    If oLine.DiscountReasonCode <> "00" Then
        sprdDisp.MaxRows = sprdDisp.MaxRows + 1
        sprdDisp.Row = sprdDisp.MaxRows
        
        intSortSequence = intSortSequence + 1
        Call FillSortCols(dteHoldDate, enstLine, intSortSequence, colEntries(lEntryNo).TransactionTime, colEntries(lEntryNo).TillID)

        sprdDisp.Col = COL_DISC
        'Call sprdDisp.AddCellSpan(COL_DISC, sprdDisp.Row, 3, 1)
        sprdDisp.CellType = CellTypeEdit
        sprdDisp.Text = mcolDiscReasons(oLine.DiscountReasonCode)
    End If

End Sub


Public Sub FillSortCols(sprdDisp As vaSpread, _
                         dteHoldDate As Date, _
                         intType As Integer, _
                         intSequence As Integer, _
                         sTransactionTime As String, _
                         sTillId As String)
    
    sprdDisp.Col = COL_SORTDATE
    sprdDisp.Text = Format(dteHoldDate, "YYYYmmDD")
    
    ' strTransactionTime is in format hhmmss
    sprdDisp.Col = COL_SORTTIME
    sprdDisp.Text = "T" & Format(sTransactionTime, "@@:@@:@@")
    
    sprdDisp.Col = COL_SORTTILLID
    sprdDisp.Text = sTillId
    
    sprdDisp.Col = COL_SORTTYPE
    sprdDisp.Text = intType
    
    sprdDisp.Col = COL_SORTSEQUENCE
    sprdDisp.Text = intSequence
    
End Sub



