Attribute VB_Name = "modTOSEdit"
Option Explicit
'------------------------------------------------------------------------------
' TOSOPT linkage table routines
' return tender control description by collection index

Public colTOSTenderLinks   As Collection   ' (TOSTEN)
Public colTOSControls      As Collection   ' (TOSCTL)
Public colTOSOptions        As Collection   ' (TOSOPT)

Public Function TOSLoadBO() As Boolean

    Dim oTOSTenderLink      As cTypesOfSaleTenders
    Dim oTOSControl         As cTypesOfSaleControl
    Dim oTOSOptions         As cTypesOfSaleOptions
    
    Dim oBoSortKeys As OasysDbXfaceCom_Wickes.CBoSortKeys
    Dim oRowSel     As CRowSelector
    
    ' Load the Type of Sale (TOS) controls table (TOSCTL)
    Set oRowSel = goDatabase.CreateBoSelector(CLASSID_TYPESOFSALETENDERS)
    Set oBoSortKeys = New CBoSortKeys
    Call oBoSortKeys.Add(FID_TYPESOFSALETENDERS_TenderNo, False)
    Call oBoSortKeys.Add(FID_TYPESOFSALETENDERS_TOSDisplaySeq, False)
    Set colTOSTenderLinks = goDatabase.GetSortedBoCollection(oRowSel, oBoSortKeys)
    
    ' Load the Type of Sale (TOS) controls table (TOSCTL)
    'Set oTOSTenderLink = goDatabase.CreateBusinessObject(CLASSID_TYPESOFSALETENDERS)
    'oTOSTenderLink.AddLoadField FID_TYPESOFSALETENDERS_TenderNo
    'oTOSTenderLink.AddLoadField FID_TYPESOFSALETENDERS_TOSDisplaySeq
    'Set colTOSTenderLinks = oTOSTenderLink.LoadMatches
    
    ' Load the Type of Sale and Tenders linkage table (TOSTEN)
    Set oTOSControl = goDatabase.CreateBusinessObject(CLASSID_TYPESOFSALECONTROL)
    oTOSControl.AddLoadField FID_TYPESOFSALECONTROL_Code
    oTOSControl.AddLoadField FID_TYPESOFSALECONTROL_Description
    oTOSControl.AddLoadField FID_TYPESOFSALECONTROL_ValidForUse
    'oTOSControl.AddLoadFilter CMP_NOTEQUAL, FID_TYPESOFSALECONTROL_ValidForUse, False

    Set colTOSControls = oTOSControl.LoadMatches
     
    ' Load the Type of Sale options table (TOSOPT)
    Set oTOSOptions = goDatabase.CreateBusinessObject(CLASSID_TYPESOFSALEOPTIONS)
    oTOSOptions.AddLoadField FID_TYPESOFSALEOPTIONS_DisplaySeq
    oTOSOptions.AddLoadField FID_TYPESOFSALEOPTIONS_Description
    oTOSOptions.AddLoadField FID_TYPESOFSALEOPTIONS_Code

    Set colTOSOptions = oTOSOptions.LoadMatches
    TOSLoadBO = True
    
End Function

Property Get GetTOSOptionDescription(ByVal DSEQ As String) As String
    
    Dim oTOSOption As cTypesOfSaleOptions
    
    GetTOSOptionDescription = DSEQ & " - "
    
    For Each oTOSOption In colTOSOptions
        If oTOSOption.DisplaySeq = DSEQ Then
            GetTOSOptionDescription = GetTOSOptionDescription & oTOSOption.Description
            Exit Sub
        End If
    Next oTOSOption
    GetTOSOptionDescription = GetTOSOptionDescription & "*Not Found*"
    
End Property
Property Get GetTOSTenderOptionSelected(ByVal TenderNo As String, ByVal DSEQ As String) As Boolean
    
    Dim oTOSTender As cTypesOfSaleTenders
    
    For Each oTOSTender In colTOSTenderLinks
        If oTOSTender.TenderNo = TenderNo And oTOSTender.TOSDisplaySeq = DSEQ Then
            GetTOSTenderOptionSelected = True
            Exit Sub
        End If
    Next oTOSTender
    GetTOSTenderOptionSelected = False
    
End Property

'Returns a built combo box text string with current TOS in it
Property Get GetTOSTenderOptionsList(ByVal TenderNo As String) As String
    
    Dim oTOSTender As cTypesOfSaleTenders
    
    For Each oTOSTender In colTOSTenderLinks
        If oTOSTender.TenderNo = TenderNo Then
            If GetTOSTenderOptionsList <> "" Then
                GetTOSTenderOptionsList = GetTOSTenderOptionsList & Chr$(9)
            End If
            GetTOSTenderOptionsList = GetTOSTenderOptionsList & GetTOSOptionDescription(oTOSTender.TOSDisplaySeq)
        End If
    Next oTOSTender
    If GetTOSTenderOptionsList <> "" Then
        GetTOSTenderOptionsList = GetTOSTenderOptionsList & Chr$(9)
    Else
        GetTOSTenderOptionsList = GetTOSTenderOptionsList & "None "
    End If
    GetTOSTenderOptionsList = GetTOSTenderOptionsList & "(EDIT LIST)"
    
End Property

'Finds the TOS control entry and returns true if valid
Property Get GetTOSTenderOptionValid(ByVal TOSCode As String) As Boolean
    
    Dim oTOSControl As cTypesOfSaleControl
    
    GetTOSTenderOptionValid = False
    For Each oTOSControl In colTOSControls
        If TOSCode = oTOSControl.Code Then
            If oTOSControl.ValidForUse = True Then GetTOSTenderOptionValid = True
            Exit Sub
        End If
    Next oTOSControl

End Property
