VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmTenderOptionsEditor 
   Caption         =   "Edit Tender Options"
   ClientHeight    =   4110
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12105
   Icon            =   "frmTenderOptionsEditor.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4110
   ScaleWidth      =   12105
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10-Exit"
      Height          =   375
      Left            =   6840
      TabIndex        =   5
      Top             =   3240
      Width           =   1335
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "F8-Delete"
      Height          =   375
      Left            =   1560
      TabIndex        =   4
      Top             =   3240
      Width           =   1215
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save"
      Enabled         =   0   'False
      Height          =   375
      Left            =   4800
      TabIndex        =   3
      Top             =   3240
      Width           =   1215
   End
   Begin VB.CommandButton cmdAddRecord 
      Caption         =   "Add"
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   3240
      Width           =   1215
   End
   Begin FPSpreadADO.fpSpread sprdEdit 
      Height          =   2895
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   11895
      _Version        =   458752
      _ExtentX        =   20981
      _ExtentY        =   5106
      _StockProps     =   64
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GridShowHoriz   =   0   'False
      GridSolid       =   0   'False
      MaxCols         =   15
      RestrictCols    =   -1  'True
      SelectBlockOptions=   0
      SpreadDesigner  =   "frmTenderOptionsEditor.frx":058A
      UserResize      =   1
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   1
      Top             =   3735
      Width           =   12105
      _ExtentX        =   21352
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmTenderOptionsEditor.frx":0CEC
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13520
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "19:26"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmTenderOptionsEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmTenderOptionsEditor
'* Date   : 11/22/04
'* Author :
'**********************************************************************************************
'* Summary: Allows user edit of the tender options business object (TENOPT and TOSTEN)
'**********************************************************************************************
'* Versions:
'</CAMH>***************************************************************************************
Option Explicit

Private Const SPRD_COL_TENDER_NO = 1
Private Const SPRD_COL_TENDER_DESC = 2
Private Const SPRD_COL_TENDER_TYPE_ID = 3
Private Const SPRD_COL_TENDER_TYPE_DESC = 4
'Private Const SPRD_COL_TENDER_VALID_TOS = 5

Private Const SPRD_COL_TENDER_OVER_TENDER_ALLOWED = 5 'TTOT
Private Const SPRD_COL_TENDER_CAPTURE_CC = 6  'TTCC
Private Const SPRD_COL_TENDER_USE_EFTPOS = 7 'TTEF
Private Const SPRD_COL_TENDER_OPEN_DRAWER = 8 'TODR
Private Const SPRD_COL_TENDER_DEFAULT_REMAINING_AMOUNT = 9 'TATA

Private Const SPRD_COL_TENDER_PRINT_CHEQUE_FRONT = 10 'PFOC
Private Const SPRD_COL_TENDER_PRINT_CHEQUE_BACK = 11 'PBOC

Private Const SPRD_COL_TENDER_MIN_VALUE_ACCEPTED = 12 'MINV
Private Const SPRD_COL_TENDER_MAX_VALUE_ACCEPTED = 13 'MAXV
Private Const SPRD_COL_TENDER_CREDIT_CARD_FLOOR_LIMIT = 14 'FLLM

Private Const SPRD_COL_CHANGE_FLAG = 15

Private Const ROW_EDIT_TYPE_NONE = 0
Private Const ROW_EDIT_TYPE_CHANGE = 1
Private Const ROW_EDIT_TYPE_ADD = 2
Private Const ROW_EDIT_TYPE_DELETE = 3

Private colTenderControls   As Collection
Private colTOSTenderLinks   As Collection   ' (TOSTEN)
Private colTOSControls      As Collection   ' (TOSCTL)
Private colTOSOptions       As Collection   ' (TOSOPT)


Dim mEditedFlag As Long     ' save is needed

Private Sub Form_Load()

Dim oTenderOption       As cTenderOptions
Dim colTenderOptions    As Collection
Dim lIndex              As Long
Dim oControl            As Control

    GetRoot
    Call InitialiseStatusBar(sbStatus)
    
    KeyPreview = True '       F keys for buttons
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    sprdEdit.BackColor = goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR)
    mEditedFlag = False
    
    For Each oControl In Me.Controls
        If TypeName(oControl) = "CommandButton" Then
            oControl.BackColor = Me.BackColor
        End If
    Next
    
    Call TenderControlLoadBO
    Call TOSLoadBO
    
    ' Load the tender options table
    Set oTenderOption = goDatabase.CreateBusinessObject(CLASSID_TENDEROPTIONS)
    oTenderOption.AddLoadField FID_TENDEROPTIONS_CaptureCreditCard
    oTenderOption.AddLoadField FID_TENDEROPTIONS_DefaultRemainingAmount
    oTenderOption.AddLoadField FID_TENDEROPTIONS_Description
    oTenderOption.AddLoadField FID_TENDEROPTIONS_END_OF_STATIC
    oTenderOption.AddLoadField FID_TENDEROPTIONS_MaximumAccepted
    oTenderOption.AddLoadField FID_TENDEROPTIONS_MinimumAccepted
    oTenderOption.AddLoadField FID_TENDEROPTIONS_OpenCashDrawer
    oTenderOption.AddLoadField FID_TENDEROPTIONS_OverTenderAllowed
    oTenderOption.AddLoadField FID_TENDEROPTIONS_PrintChequeBack
    oTenderOption.AddLoadField FID_TENDEROPTIONS_PrintChequeFront
    oTenderOption.AddLoadField FID_TENDEROPTIONS_TenderNo
    oTenderOption.AddLoadField FID_TENDEROPTIONS_TendType
    oTenderOption.AddLoadField FID_TENDEROPTIONS_UseEFTPOS
    
   ' oTenderOption.AddLoadFilter CMP_GREATEREQUALTHAN, FID_PARAMETER_ID, 200
   ' oTenderOption.AddLoadFilter CMP_LESSTHAN, FID_PARAMETER_ID, 300
    Set colTenderOptions = oTenderOption.LoadMatches
    
    ' Build the spreadsheet
    sprdEdit.MaxRows = 0
    sprdEdit.EditModeReplace = True
    ' load the spreadsheet data
    For Each oTenderOption In colTenderOptions
        lIndex = lIndex + 1
        Call AddRowToSprd(lIndex, oTenderOption)
        SetRowEditedFlag(lIndex) = ROW_EDIT_TYPE_NONE
    Next
    
    ' Sort by ID
    Call SortSpreadSheet
End Sub

Private Sub Form_Unload(Cancel As Integer)

    If mEditedFlag = True Then
        Select Case MsgBox("Save Changes before exit? (Y/N)", vbYesNoCancel, "Parameter Editor")
            Case vbCancel
                Cancel = 1
            Case vbYes
                Call SaveTableChanges
                Cancel = 0
            Case vbNo
                Cancel = 0
        End Select
    End If
End Sub

Private Sub SortSpreadSheet()
    
    sprdEdit.SortKey(1) = 1
    sprdEdit.SortKeyOrder(1) = SortKeyOrderAscending
    ' Sort data in first five columns and rows by column 1 and 3
    sprdEdit.Sort -1, -1, -1, -1, SortByRow
    sprdEdit.ReDraw = True
    
End Sub

' Sets a parameter's edit status in the spreadsheet
Private Property Let SetRowEditedFlag(ByVal row As Long, ByVal Value As Long)
    
    With sprdEdit
        .row = row
        ' Add the ID
        .Col = SPRD_COL_CHANGE_FLAG
        .CellType = CellTypeNumber
        .Value = Value
        If Value = ROW_EDIT_TYPE_NONE Then          ' do nothing
        
        ElseIf Value = ROW_EDIT_TYPE_DELETE Then    ' Hide the deleted row
           .row = row
           .RowHidden = True
            mEditedFlag = True  ' save is needed
        Else                                        ' Changed row
            mEditedFlag = True  ' save is needed
        End If
    End With
    
    If mEditedFlag = True Then cmdSave.Enabled = True

End Property

Private Property Get GetRowEditedFlag(ByVal row As Long) As Long
    With sprdEdit
        .row = row
        ' Add the ID
        .Col = SPRD_COL_CHANGE_FLAG
        .CellType = CellTypeNumber
        GetRowEditedFlag = .Value
    End With
End Property

Private Sub SetCheckboxValue(ByVal Col As Long, ByVal Value As Boolean)
    With sprdEdit
        .Col = Col
        .CellType = CellTypeCheckBox
        .TypeCheckCenter = True
        .Value = Value
    End With
End Sub

Private Sub AddRowToSprd(ByVal row As Long, ByVal oRec As cTenderOptions)
    With sprdEdit
        If row > .MaxRows Then
            .MaxRows = row
        End If
        .row = row
        .Col = SPRD_COL_TENDER_NO
        .Value = oRec.TenderNo
            
        .Col = SPRD_COL_TENDER_DESC
        .Value = oRec.Description
           
        .Col = SPRD_COL_TENDER_TYPE_ID
        .Value = oRec.TendType
            
        .Col = SPRD_COL_TENDER_TYPE_DESC
        .CellType = CellTypeComboBox
        .TypeComboBoxList = TenderControlList()
        .TypeComboBoxCurSel = TenderControlIndex(oRec.TendType)
        
        Call SetCheckboxValue(SPRD_COL_TENDER_CAPTURE_CC, oRec.CaptureCreditCard)
        Call SetCheckboxValue(SPRD_COL_TENDER_DEFAULT_REMAINING_AMOUNT, oRec.DefaultRemainingAmount)
        Call SetCheckboxValue(SPRD_COL_TENDER_OPEN_DRAWER, oRec.OpenCashDrawer)
        Call SetCheckboxValue(SPRD_COL_TENDER_OVER_TENDER_ALLOWED, oRec.OverTenderAllowed)
        Call SetCheckboxValue(SPRD_COL_TENDER_PRINT_CHEQUE_BACK, oRec.PrintChequeBack)
        Call SetCheckboxValue(SPRD_COL_TENDER_PRINT_CHEQUE_FRONT, oRec.PrintChequeFront)
        Call SetCheckboxValue(SPRD_COL_TENDER_USE_EFTPOS, oRec.UseEFTPOS)
        
        .Col = SPRD_COL_TENDER_MIN_VALUE_ACCEPTED
        .Value = oRec.MinimumAccepted
        
        .Col = SPRD_COL_TENDER_MAX_VALUE_ACCEPTED
        .Value = oRec.MaximumAccepted
        
        .Col = SPRD_COL_TENDER_CREDIT_CARD_FLOOR_LIMIT
        .Value = oRec.CreditCardFloorLimit
        
    End With
End Sub

Private Function GetCheckboxValue(ByVal Col As Long) As Long
    With sprdEdit
        .Col = Col
        GetCheckboxValue = .Value
    End With
End Function

Public Function GetTenderNumberIndex(ByVal TenderNumber As String) As Long

    Dim lIndex          As Long
    Dim oTenderOption   As cTenderOptions

    sprdEdit.Col = SPRD_COL_TENDER_NO
    For lIndex = 1 To sprdEdit.MaxRows
        
        sprdEdit.row = lIndex
        If sprdEdit.Value = TenderNumber Then
            GetTenderNumberIndex = lIndex
            Exit Function
        End If
        lIndex = lIndex + 1
    Next lIndex
    GetTenderNumberIndex = -1

End Function

Private Sub cmdAddRecord_Click()

    Dim oNewEntry   As cTenderOptions
    Dim lRow        As Long
    
    Set oNewEntry = goDatabase.CreateBusinessObject(CLASSID_TENDEROPTIONS)
    
    Load frmTenderOptionsEditorAdd
    If frmTenderOptionsEditorAdd.UserEnterNewRecord(oNewEntry) = True Then
        
        lRow = sprdEdit.MaxRows + 1
        Call AddRowToSprd(lRow, oNewEntry)
        SetRowEditedFlag(lRow) = ROW_EDIT_TYPE_ADD
    
    End If

    Unload frmTenderOptionsEditorAdd
    
End Sub

Private Sub sprdEdit_KeyPress(KeyAscii As Integer)

    If sprdEdit.ActiveCol = SPRD_COL_TENDER_NO Then
        If KeyAscii < 128 Then KeyAscii = 0
    End If

End Sub

Sub sprdedit_EditMode(ByVal Col As Long, ByVal row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
    
    If Col <> SPRD_COL_TENDER_NO Then
        SetRowEditedFlag(row) = ROW_EDIT_TYPE_CHANGE
    Else
        sprdEdit.EditMode = False
    End If
    
End Sub

Private Sub cmdDelete_Click()
    
    Dim strDesc As String
    With sprdEdit
        .row = .ActiveRow
        .Col = SPRD_COL_TENDER_DESC
        If MsgBox("Delete the entry -""" & .Value & """(Y/N)?", vbYesNoCancel) = vbYes Then
            SetRowEditedFlag(.ActiveRow) = ROW_EDIT_TYPE_DELETE
        End If
    End With
    
End Sub

Private Sub cmdSave_Click()
  
  If mEditedFlag = True Then Call SaveTableChanges
    
End Sub

Private Sub cmdExit_Click()
    
    Unload Me
    
End Sub

' Updates the Parameter table through IBo
Private Sub SaveTableChanges()

    Dim oTenderOption   As cTenderOptions
    Dim lIndex          As Long
    Dim lSaveType       As Long
    
    If mEditedFlag = True Then
        With sprdEdit
            For lIndex = 1 To .MaxRows
                .row = lIndex
                .Col = SPRD_COL_CHANGE_FLAG
                lSaveType = .Value
                .Value = ROW_EDIT_TYPE_NONE
                
                If lSaveType <> ROW_EDIT_TYPE_NONE Then
                 
                    Set oTenderOption = goDatabase.CreateBusinessObject(CLASSID_TENDEROPTIONS)

                    .Col = SPRD_COL_TENDER_NO
                    oTenderOption.TenderNo = .Value
                
                    .Col = SPRD_COL_TENDER_DESC
                    oTenderOption.Description = .Value
           
                    .Col = SPRD_COL_TENDER_TYPE_ID
                    oTenderOption.TendType = .Value
            
                    ' .Col = SPRD_COL_TENDER_TYPE_DESC
        
                    oTenderOption.CaptureCreditCard = GetCheckboxValue(SPRD_COL_TENDER_CAPTURE_CC)
                    oTenderOption.DefaultRemainingAmount = GetCheckboxValue(SPRD_COL_TENDER_DEFAULT_REMAINING_AMOUNT)
                    oTenderOption.OpenCashDrawer = GetCheckboxValue(SPRD_COL_TENDER_OPEN_DRAWER)
                    oTenderOption.OverTenderAllowed = GetCheckboxValue(SPRD_COL_TENDER_OVER_TENDER_ALLOWED)
                    oTenderOption.PrintChequeBack = GetCheckboxValue(SPRD_COL_TENDER_PRINT_CHEQUE_BACK)
                    oTenderOption.PrintChequeFront = GetCheckboxValue(SPRD_COL_TENDER_PRINT_CHEQUE_FRONT)
                    oTenderOption.UseEFTPOS = GetCheckboxValue(SPRD_COL_TENDER_USE_EFTPOS)
        
                    .Col = SPRD_COL_TENDER_MIN_VALUE_ACCEPTED
                    oTenderOption.MinimumAccepted = .Value
        
                    .Col = SPRD_COL_TENDER_MAX_VALUE_ACCEPTED
                    oTenderOption.MaximumAccepted = .Value
            
                    .Col = SPRD_COL_TENDER_CREDIT_CARD_FLOOR_LIMIT
                    oTenderOption.CreditCardFloorLimit = .Value

                    Select Case lSaveType
                        Case ROW_EDIT_TYPE_ADD
                            If oTenderOption.SaveIfNew = False Then
                                If oTenderOption.SaveIfExists = False Then MsgBox "Add Error"
                            End If
                            
                        Case ROW_EDIT_TYPE_CHANGE
                            If oTenderOption.SaveIfExists = False Then
                                If oTenderOption.SaveIfNew = False Then MsgBox "Update Error"
                            End If
                        Case ROW_EDIT_TYPE_DELETE
                            If oTenderOption.Delete = False Then MsgBox "Delete error"
                    End Select
                End If
            Next lIndex
        End With
    
    cmdSave.Enabled = False
    mEditedFlag = False
    End If
    Exit Sub
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        Case vbKeyF8:
            cmdDelete_Click
        Case vbKeyF10:
            cmdExit_Click
    End Select
    
End Sub

Private Sub Form_Resize()

Dim lSpacing As Long
    
    Dim iBtnCnt     As Long
    Dim iBtnWidth   As Long
    Dim iBtnHeight  As Long
    Dim i           As Long
    Dim iBtnGap     As Long
    Dim iBtnCInc    As Long
    Const BORDER_WIDTH = 60
        
    iBtnCnt = 4
    iBtnWidth = cmdAddRecord.Width
    iBtnHeight = cmdAddRecord.Height
    iBtnGap = cmdAddRecord.Top - sprdEdit.Top + sprdEdit.Height
    If Me.WindowState = vbMinimized Then Exit Sub
    lSpacing = sprdEdit.Left
    
    'Check form is not below minimum width
    If Me.Width < iBtnWidth * iBtnCnt + (lSpacing * 4) Then
        Me.Width = iBtnWidth * iBtnCnt + (lSpacing * 4)
       ' Exit Sub
    End If
    'Check form is not below minimum height
    If Me.Height < sprdEdit.Top + iBtnHeight * 3 + sbStatus.Height Then
        Me.Height = sprdEdit.Top + iBtnHeight * 3 + sbStatus.Height
        Exit Sub
    End If
    
    'start resizing
    sprdEdit.Width = Me.Width - sprdEdit.Left * 2 - BORDER_WIDTH '* 2
    sprdEdit.Height = Me.Height - (iBtnHeight + sprdEdit.Top + (lSpacing * 3) + (BORDER_WIDTH * 6) + sbStatus.Height)
    
    iBtnCInc = (sprdEdit.Width - iBtnWidth) / 3
    i = sprdEdit.Left
    cmdAddRecord.Left = i
    cmdDelete.Left = i + iBtnCInc
    cmdSave.Left = i + iBtnCInc * 2
    cmdExit.Left = i + iBtnCInc * 3
    
    i = sprdEdit.Top + sprdEdit.Height + lSpacing
    cmdAddRecord.Top = i
    cmdDelete.Top = i
    cmdSave.Top = i
    cmdExit.Top = i
    
    cmdDelete.Width = iBtnWidth
    cmdDelete.Height = iBtnHeight
    cmdSave.Width = iBtnWidth
    cmdSave.Height = iBtnHeight
    cmdExit.Width = iBtnWidth
    cmdExit.Height = iBtnHeight

End Sub

Public Sub LoadTenderCodesComboBox(ByRef cmbRet As ComboBox)

    Dim oTenderControl  As cTenderControl
    Dim lIndex          As Long
    
    cmbRet.Clear
    For Each oTenderControl In colTenderControls
        Call cmbRet.AddItem(oTenderControl.TendType & " - " & oTenderControl.Description, lIndex)
        lIndex = lIndex + 1
    Next oTenderControl
        
End Sub


Public Function TenderControlLoadBO() As Boolean
    
    Dim oTenderControl As cTenderControl
    ' Load the tender controls table
    Set oTenderControl = goDatabase.CreateBusinessObject(CLASSID_TENDERCONTROL)
    oTenderControl.AddLoadField FID_TENDERCONTROL_TendType
    oTenderControl.AddLoadField FID_TENDERCONTROL_Description
    oTenderControl.AddLoadField FID_TENDERCONTROL_ValidForUse
    oTenderControl.AddLoadFilter CMP_NOTEQUAL, FID_TENDERCONTROL_ValidForUse, False
    
    Set colTenderControls = oTenderControl.LoadMatches
    TenderControlLoadBO = True
    
End Function

Public Property Get TenderControlDescription(ByVal index As Long) As String
    
    Dim oTender As cTenderControl
    Set oTender = colTenderControls(index)
    TenderControlDescription = oTender.TendType & " - " & oTender.Description
    
End Property

' returns the tender control collection index for the ID
Public Property Get TenderControlIndex(ByVal id As Long) As Long
    
    Dim lIndex  As Long
    Dim oTender As cTenderControl
    For Each oTender In colTenderControls
        If id = oTender.TendType Then
            TenderControlIndex = lIndex
        End If
        lIndex = lIndex + 1
    Next oTender
    
End Property

' Returns a built combo box text string with possible tenders in it
Public Property Get TenderControlList() As String
    Dim lIndex As Long
    For lIndex = 1 To colTenderControls.Count
        If lIndex > 1 Then
            TenderControlList = TenderControlList & Chr$(9)
        End If
        TenderControlList = TenderControlList & TenderControlDescription(lIndex)
    Next lIndex
End Property

' TOSOPT linkage table routines
' return tender control description by collection index

Public Function TOSLoadBO() As Boolean

    Dim oTOSTenderLink      As cTypesOfSaleTenders
    Dim oTOSControl         As cTypesOfSaleControl
    Dim oTOSOptions         As cTypesOfSaleOptions
    
    Dim oBoSortKeys As OasysDbXfaceCom_Wickes.CBoSortKeys
    Dim oRowSel     As CRowSelector
    
    ' Load the Type of Sale (TOS) controls table (TOSCTL)
    Set oRowSel = goDatabase.CreateBoSelector(CLASSID_TYPESOFSALETENDERS)
    Set oBoSortKeys = New CBoSortKeys
    Call oBoSortKeys.Add(FID_TYPESOFSALETENDERS_TenderNo, False)
    Call oBoSortKeys.Add(FID_TYPESOFSALETENDERS_TOSDisplaySeq, False)
    Set colTOSTenderLinks = goDatabase.GetSortedBoCollection(oRowSel, oBoSortKeys)
    
    ' Load the Type of Sale (TOS) controls table (TOSCTL)
    'Set oTOSTenderLink = goDatabase.CreateBusinessObject(CLASSID_TYPESOFSALETENDERS)
    'oTOSTenderLink.AddLoadField FID_TYPESOFSALETENDERS_TenderNo
    'oTOSTenderLink.AddLoadField FID_TYPESOFSALETENDERS_TOSDisplaySeq
    'Set colTOSTenderLinks = oTOSTenderLink.LoadMatches
    
    ' Load the Type of Sale and Tenders linkage table (TOSTEN)
    Set oTOSControl = goDatabase.CreateBusinessObject(CLASSID_TYPESOFSALECONTROL)
    oTOSControl.AddLoadField FID_TYPESOFSALECONTROL_Code
    oTOSControl.AddLoadField FID_TYPESOFSALECONTROL_Description
    oTOSControl.AddLoadField FID_TYPESOFSALECONTROL_ValidForUse
    'oTOSControl.AddLoadFilter CMP_NOTEQUAL, FID_TYPESOFSALECONTROL_ValidForUse, False

    Set colTOSControls = oTOSControl.LoadMatches
     
    ' Load the Type of Sale options table (TOSOPT)
    Set oTOSOptions = goDatabase.CreateBusinessObject(CLASSID_TYPESOFSALEOPTIONS)
    oTOSOptions.AddLoadField FID_TYPESOFSALEOPTIONS_DisplaySeq
    oTOSOptions.AddLoadField FID_TYPESOFSALEOPTIONS_Description
    oTOSOptions.AddLoadField FID_TYPESOFSALEOPTIONS_Code

    Set colTOSOptions = oTOSOptions.LoadMatches
    TOSLoadBO = True
    
End Function

Property Get GetTOSOptionDescription(ByVal DSEQ As String) As String
    
    Dim oTOSOption As cTypesOfSaleOptions
    
    GetTOSOptionDescription = DSEQ & " - "
    
    For Each oTOSOption In colTOSOptions
        If oTOSOption.DisplaySeq = DSEQ Then
            GetTOSOptionDescription = GetTOSOptionDescription & oTOSOption.Description
            Exit Sub
        End If
    Next oTOSOption
    GetTOSOptionDescription = GetTOSOptionDescription & "*Not Found*"
    
End Property
Property Get GetTOSTenderOptionSelected(ByVal TenderNo As String, ByVal DSEQ As String) As Boolean
    
    Dim oTOSTender As cTypesOfSaleTenders
    
    For Each oTOSTender In colTOSTenderLinks
        If oTOSTender.TenderNo = TenderNo And oTOSTender.TOSDisplaySeq = DSEQ Then
            GetTOSTenderOptionSelected = True
            Exit Sub
        End If
    Next oTOSTender
    GetTOSTenderOptionSelected = False
    
End Property

'Returns a built combo box text string with current TOS in it
Property Get GetTOSTenderOptionsList(ByVal TenderNo As String) As String
    
    Dim oTOSTender As cTypesOfSaleTenders
    
    For Each oTOSTender In colTOSTenderLinks
        If oTOSTender.TenderNo = TenderNo Then
            If LenB(GetTOSTenderOptionsList) <> 0 Then
                GetTOSTenderOptionsList = GetTOSTenderOptionsList & Chr$(9)
            End If
            GetTOSTenderOptionsList = GetTOSTenderOptionsList & GetTOSOptionDescription(oTOSTender.TOSDisplaySeq)
        End If
    Next oTOSTender
    If LenB(GetTOSTenderOptionsList) <> 0 Then
        GetTOSTenderOptionsList = GetTOSTenderOptionsList & Chr$(9)
    Else
        GetTOSTenderOptionsList = GetTOSTenderOptionsList & "None "
    End If
    GetTOSTenderOptionsList = GetTOSTenderOptionsList & "(EDIT LIST)"
    
End Property

'Finds the TOS control entry and returns true if valid
Property Get GetTOSTenderOptionValid(ByVal TOSCode As String) As Boolean
    
    Dim oTOSControl As cTypesOfSaleControl
    
    GetTOSTenderOptionValid = False
    For Each oTOSControl In colTOSControls
        If TOSCode = oTOSControl.Code Then
            If oTOSControl.ValidForUse = True Then GetTOSTenderOptionValid = True
            Exit Sub
        End If
    Next oTOSControl

End Property

