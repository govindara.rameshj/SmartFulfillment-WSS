Attribute VB_Name = "modTenderControl"
Option Explicit
'---------------------------------------------------------------------------
' TENCTL - Tender control routines
' return tender control description by collection index
'

Private colTenderControls As Collection

Public Function TenderControlLoadBO() As Boolean
    
    Dim oTenderControl As cTenderControl
    ' Load the tender controls table
    Set oTenderControl = goDatabase.CreateBusinessObject(CLASSID_TENDERCONTROL)
    oTenderControl.AddLoadField FID_TENDERCONTROL_TendType
    oTenderControl.AddLoadField FID_TENDERCONTROL_Description
    oTenderControl.AddLoadField FID_TENDERCONTROL_ValidForUse
    oTenderControl.AddLoadFilter CMP_NOTEQUAL, FID_TENDERCONTROL_ValidForUse, False
    
    Set colTenderControls = oTenderControl.LoadMatches
    TenderControlLoadBO = True
    
End Function

Public Property Get TenderControlDescription(ByVal index As Long) As String
    
    Dim oTender As cTenderControl
    Set oTender = colTenderControls(index)
    TenderControlDescription = oTender.TendType & " - " & oTender.Description
    
End Property

' returns the tender control collection index for the ID
Public Property Get TenderControlIndex(ByVal id As Long) As Long
    
    Dim lIndex  As Long
    Dim oTender As cTenderControl
    For Each oTender In colTenderControls
        If id = oTender.TendType Then
            TenderControlIndex = lIndex
        End If
        lIndex = lIndex + 1
    Next oTender
    
End Property

' Returns a built combo box text string with possible tenders in it
Public Property Get TenderControlList() As String
    Dim lIndex As Long
    For lIndex = 1 To colTenderControls.Count
        If lIndex > 1 Then
            TenderControlList = TenderControlList & Chr$(9)
        End If
        TenderControlList = TenderControlList & TenderControlDescription(lIndex)
    Next lIndex
End Property

