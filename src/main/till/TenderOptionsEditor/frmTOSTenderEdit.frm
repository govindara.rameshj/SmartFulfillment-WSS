VERSION 5.00
Begin VB.Form frmTOSTenderEdit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Edit Valid Types of Sale"
   ClientHeight    =   6615
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   3285
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6615
   ScaleWidth      =   3285
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   1920
      TabIndex        =   26
      Top             =   6000
      Width           =   1215
   End
   Begin VB.CommandButton cmdUpdate 
      Caption         =   "Update"
      Height          =   375
      Left            =   120
      TabIndex        =   25
      Top             =   6000
      Width           =   1095
   End
   Begin VB.PictureBox pbxChkBoxes 
      Height          =   5655
      Left            =   120
      ScaleHeight     =   5595
      ScaleWidth      =   2955
      TabIndex        =   0
      Top             =   120
      Width           =   3015
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   23
         Left            =   960
         TabIndex        =   24
         Top             =   1920
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   22
         Left            =   1080
         TabIndex        =   23
         Top             =   1440
         UseMaskColor    =   -1  'True
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   21
         Left            =   1560
         TabIndex        =   22
         Top             =   1320
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   20
         Left            =   1080
         TabIndex        =   21
         Top             =   1560
         Width           =   2400
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   19
         Left            =   1200
         TabIndex        =   20
         Top             =   1560
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   18
         Left            =   1320
         TabIndex        =   19
         Top             =   1680
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   17
         Left            =   1200
         TabIndex        =   18
         Top             =   1920
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   16
         Left            =   1080
         TabIndex        =   17
         Top             =   1680
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   15
         Left            =   1680
         TabIndex        =   16
         Top             =   1800
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   14
         Left            =   1200
         TabIndex        =   15
         Top             =   1320
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   13
         Left            =   960
         TabIndex        =   14
         Top             =   1680
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   12
         Left            =   1560
         TabIndex        =   13
         Top             =   1440
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   11
         Left            =   720
         TabIndex        =   12
         Top             =   1440
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   10
         Left            =   1560
         TabIndex        =   11
         Top             =   1680
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   9
         Left            =   1320
         TabIndex        =   10
         Top             =   1800
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   8
         Left            =   720
         TabIndex        =   9
         Top             =   1800
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   7
         Left            =   600
         TabIndex        =   8
         Top             =   1680
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   6
         Left            =   600
         TabIndex        =   7
         Top             =   1440
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   5
         Left            =   720
         TabIndex        =   6
         Top             =   1680
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   4
         Left            =   840
         TabIndex        =   5
         Top             =   1800
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   3
         Left            =   480
         TabIndex        =   4
         Top             =   1560
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   2
         Left            =   480
         TabIndex        =   3
         Top             =   1920
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   1
         Left            =   840
         TabIndex        =   2
         Top             =   1680
         Width           =   1935
      End
      Begin VB.CheckBox chkTOSValid 
         Caption         =   "Check1"
         Height          =   375
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   2520
      End
   End
End
Attribute VB_Name = "frmTOSTenderEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmTenderOptionsEditor
'* Date   : 11/22/04
'* Author :
'**********************************************************************************************
'* Summary: Allows user edit of the tender options business object (TENOPT and TOSTEN)
'**********************************************************************************************
'* Versions:
'</CAMH>***************************************************************************************
Option Explicit

Dim mBlnRecordReady As Boolean
Dim mTenderNo As String

Public Function EditValidTOSTEN(ByRef TenderNo As String) As Boolean
       
    Dim oTOSOptions As cTypesOfSaleOptions
    Dim lIndex As Long
    Dim lOfs As Long
    
    mTenderNo = TenderNo
    mBlnRecordReady = False
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    pbxChkBoxes.BackColor = goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR)

    For lIndex = 0 To chkTOSValid.UBound
        chkTOSValid(lIndex).Enabled = False
        chkTOSValid(lIndex).Visible = False
    Next lIndex
    lIndex = 0
    For Each oTOSOptions In colTOSOptions
        If GetTOSTenderOptionValid(oTOSOptions.Code) = True And _
            oTOSOptions.DisplaySeq < "98" Then
            chkTOSValid(lIndex).Caption = oTOSOptions.DisplaySeq & " - (" & oTOSOptions.Code & ") " & oTOSOptions.Description
            If GetTOSTenderOptionSelected(mTenderNo, oTOSOptions.DisplaySeq) = True Then
                chkTOSValid(lIndex).Value = 1
            Else
                chkTOSValid(lIndex).Value = 0
            End If
            chkTOSValid(lIndex).Top = chkTOSValid(0).Height * lIndex
            chkTOSValid(lIndex).Left = chkTOSValid(0).Left
            chkTOSValid(lIndex).Left = chkTOSValid(0).Left
            chkTOSValid(lIndex).Height = chkTOSValid(0).Height
            chkTOSValid(lIndex).Width = chkTOSValid(0).Width
            chkTOSValid(lIndex).Enabled = True
            chkTOSValid(lIndex).Visible = True
            chkTOSValid(lIndex).TabIndex = lIndex
            chkTOSValid(lIndex).BackColor = pbxChkBoxes.BackColor
            lIndex = lIndex + 1
        End If
    Next oTOSOptions
    cmdUpdate.TabIndex = lIndex
    cmdCancel.TabIndex = lIndex + 1
    
    If chkTOSValid.Count = 0 Then
        MsgBox "There are no valid Types of Sale"
        Me.Hide
    End If
    
    lOfs = chkTOSValid(0).Height * lIndex
    pbxChkBoxes.Height = lOfs
    
    lOfs = lOfs + pbxChkBoxes.Top + 48
    cmdUpdate.Top = lOfs
    cmdCancel.Top = lOfs
    
    lOfs = lOfs + cmdUpdate.Height
    
    Const BORDER_WIDTH = 60
    Const BORDER_LENGTH = 24

    Me.Width = pbxChkBoxes.Width + (pbxChkBoxes.Left + BORDER_WIDTH) * 2
    
    lIndex = chkTOSValid.UBound
    Me.Height = lOfs + 4 * 160
    
    Call Me.Show(vbModal)

End Function

    
Private Sub cmdCancel_Click()
    
    mBlnRecordReady = False
    Me.Hide
    
End Sub
