VERSION 5.00
Begin VB.Form frmTenderOptionsEditorAdd 
   Caption         =   "Add Tender Option"
   ClientHeight    =   6030
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5355
   Icon            =   "frmTenderOptionsEditorAdd.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6030
   ScaleWidth      =   5355
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   3840
      TabIndex        =   13
      Top             =   5520
      Width           =   1335
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "Add"
      Height          =   375
      Left            =   120
      TabIndex        =   12
      Top             =   5520
      Width           =   1335
   End
   Begin VB.PictureBox pbxOne 
      BorderStyle     =   0  'None
      Height          =   5175
      Left            =   120
      ScaleHeight     =   5175
      ScaleWidth      =   5055
      TabIndex        =   14
      Top             =   120
      Width           =   5055
      Begin VB.TextBox txtCreditCardFloorLimit 
         Height          =   285
         Left            =   2040
         TabIndex        =   20
         Text            =   ".00"
         Top             =   2040
         Width           =   1095
      End
      Begin VB.TextBox txtMaxTender 
         Height          =   285
         Left            =   2040
         MaxLength       =   22
         TabIndex        =   4
         Text            =   ".00"
         Top             =   1680
         Width           =   1095
      End
      Begin VB.TextBox txtMinTender 
         Height          =   285
         Left            =   2040
         MaxLength       =   22
         TabIndex        =   3
         Text            =   ".00"
         Top             =   1320
         Width           =   1095
      End
      Begin VB.ComboBox cmbTenderID 
         Height          =   315
         Left            =   2040
         TabIndex        =   2
         Top             =   960
         Width           =   2055
      End
      Begin VB.TextBox txtTenderDescription 
         Height          =   285
         Left            =   2040
         MaxLength       =   20
         TabIndex        =   1
         Top             =   600
         Width           =   2055
      End
      Begin VB.TextBox txtTenderNumber 
         Height          =   285
         Left            =   2040
         MaxLength       =   2
         TabIndex        =   0
         Top             =   240
         Width           =   495
      End
      Begin VB.CheckBox chkPrintCheckBack 
         Caption         =   "Force print of back of cheque"
         Height          =   375
         Left            =   2040
         TabIndex        =   11
         Top             =   4800
         Width           =   2415
      End
      Begin VB.CheckBox chkPrintCheckFront 
         Caption         =   "Allow option to print front of cheque"
         Height          =   375
         Left            =   2040
         TabIndex        =   10
         Top             =   4440
         Width           =   2895
      End
      Begin VB.CheckBox chkDefaultRemaining 
         Caption         =   " Default remaining amount to tender"
         Height          =   375
         Left            =   2040
         TabIndex        =   9
         Top             =   4050
         Width           =   2895
      End
      Begin VB.CheckBox chkOpenDrawer 
         Caption         =   "Open cash drawer for this tender"
         Height          =   375
         Left            =   2040
         TabIndex        =   8
         Top             =   3675
         Width           =   2655
      End
      Begin VB.CheckBox chkUseEFTPoS 
         Caption         =   " Use EFTPoS for this Tender"
         Height          =   375
         Left            =   2040
         TabIndex        =   7
         Top             =   3285
         Width           =   2415
      End
      Begin VB.CheckBox chkCaptureCC 
         Caption         =   "Capture credit card details"
         Height          =   375
         Left            =   2040
         TabIndex        =   6
         Top             =   2880
         Width           =   2295
      End
      Begin VB.CheckBox chkOverTender 
         Caption         =   "Over tender allowed"
         Height          =   375
         Left            =   2040
         TabIndex        =   5
         Top             =   2520
         Width           =   1935
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Credit card floor limit"
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   2040
         Width           =   1695
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Minimum value of tender"
         Height          =   255
         Left            =   0
         TabIndex        =   19
         Top             =   1320
         Width           =   1935
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Maximum value of tender"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   1680
         Width           =   1815
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Description"
         Height          =   255
         Left            =   960
         TabIndex        =   17
         Top             =   600
         Width           =   975
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Type"
         Height          =   255
         Left            =   1200
         TabIndex        =   16
         Top             =   960
         Width           =   735
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Tender Number"
         Height          =   255
         Left            =   720
         TabIndex        =   15
         Top             =   240
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmTenderOptionsEditorAdd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mTenderOption           As cTenderOptions
Private mBlnRecordReady         As Boolean
Private mStrDefaultTenderNumber As String
Private mStrMinTenderLast       As String
Private mStrMaxTenderLast       As String
Private mStrCreditCardFloorLast As String

' Main add function
Public Function UserEnterNewRecord(ByRef oRetTenderOption As cTenderOptions) As Boolean
    
    Dim cCtrl As Control
    
    ' setup the dynamic user entries
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    pbxOne.BackColor = goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR)
             
    For Each cCtrl In Me.Controls
        Debug.Print TypeName(cCtrl)
        If TypeName(cCtrl) = "CheckBox" Then
            cCtrl.BackColor = pbxOne.BackColor
        End If
        If TypeName(cCtrl) = "CommandButton" Then
            cCtrl.BackColor = Me.BackColor
        End If
    Next cCtrl
    
    Call frmTenderOptionsEditor.LoadTenderCodesComboBox(cmbTenderID)
    cmbTenderID.ListIndex = 0
    
    Set mTenderOption = goDatabase.CreateBusinessObject(CLASSID_TENDEROPTIONS)
    
    ' get the new entries
    
    KeyPreview = True '       F keys for buttons

    Call Me.Show(vbModal)
            
    Set oRetTenderOption = mTenderOption
    Set mTenderOption = Nothing
    
    UserEnterNewRecord = mBlnRecordReady
    
End Function

Private Sub cmdAdd_Click()

    txtTenderNumber.Text = TenderNumberValidate(txtTenderNumber.Text)
    
    If LenB(txtTenderNumber.Text) = 0 Then
        MsgBox "Invalid tender number, please enter a number from 01 to 99"
        txtTenderNumber.Text = vbNullString
        Call txtTenderNumber.SetFocus
        Exit Sub
    End If
    
    If frmTenderOptionsEditor.GetTenderNumberIndex(txtTenderNumber.Text) >= 0 Then
        MsgBox "This tender number already exists, please select another one"
        Call txtTenderNumber.SetFocus
        Exit Sub
    End If
    
    If LenB(txtTenderDescription.Text) = 0 Then
        MsgBox "Please enter a description"
        Call txtTenderDescription.SetFocus
        Exit Sub
    End If
    
    
    mTenderOption.TenderNo = txtTenderNumber.Text
    mTenderOption.TendType = Left$(cmbTenderID.Text, 2)
    mTenderOption.Description = txtTenderDescription
    mTenderOption.MinimumAccepted = Val(txtMinTender.Text)
    mTenderOption.MaximumAccepted = Val(txtMaxTender.Text)
    mTenderOption.CreditCardFloorLimit = Val(txtCreditCardFloorLimit.Text)

    If chkOverTender.Value = 1 Then mTenderOption.OverTenderAllowed = True
    If chkCaptureCC.Value = 1 Then mTenderOption.CaptureCreditCard = True
    If chkUseEFTPoS.Value = 1 Then mTenderOption.UseEFTPOS = True
    If chkOpenDrawer.Value = 1 Then mTenderOption.OpenCashDrawer = True
    If chkDefaultRemaining.Value = 1 Then mTenderOption.DefaultRemainingAmount = True
    If chkPrintCheckFront.Value = 1 Then mTenderOption.PrintChequeFront = True
    If chkPrintCheckBack.Value = 1 Then mTenderOption.PrintChequeBack = True
    
    mBlnRecordReady = True
    Me.Hide
    Exit Sub
    
End Sub

Private Sub cmdCancel_Click()
    
    mBlnRecordReady = False
    Me.Hide

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

    Dim cCtrl           As Control
    Dim cCtrlNew        As Control
    Dim lTmp            As Long
    Dim lTabCurrent     As Long
    Dim lTabClosest     As Long
    Dim lTabDistance    As Long
    
    'On Error GoTo LError:

    Select Case KeyAscii
        ' Make enter like tab
        Case vbKeyReturn:
            lTabCurrent = Me.ActiveControl.TabIndex
            lTabClosest = 0
            lTabDistance = 9999999
            Set cCtrlNew = txtTenderNumber
            
            For Each cCtrl In Me.Controls
       
                If TypeName(cCtrl) = "CheckBox" _
                Or TypeName(cCtrl) = "TextBox" _
                Or TypeName(cCtrl) = "ComboBox" _
                And cCtrl.Enabled = True And cCtrl.Visible = True And cCtrl.TabIndex <> lTabCurrent Then
                    lTmp = cCtrl.TabIndex - lTabCurrent
                    If lTmp > 0 And lTmp < lTabDistance Then
                        lTabDistance = lTmp
                        lTabClosest = cCtrl.TabIndex
                        Set cCtrlNew = cCtrl
                    End If
                End If
       
            Next cCtrl
                    
            Call cCtrlNew.SetFocus
        'Case vbKeyF8:
        '    Call cmdDelete_Click
        'Case vbKeyF10:
        '    Call cmdExit_Click
    End Select
    
End Sub

Public Function TenderNumberValidate(ByVal TenderNumber As String) As String
        
    If Len(TenderNumber) = 1 Then TenderNumber = "0" & TenderNumber
    If Len(TenderNumber) > 2 Then TenderNumber = vbNullString
    If TenderNumber Like "[0-9][0-9]" = False Then TenderNumber = vbNullString
    If TenderNumber > "97" Then TenderNumber = vbNullString
    
    TenderNumberValidate = TenderNumber

End Function

Private Sub Form_Load()
    Set Me.Icon = frmTenderOptionsEditor.Icon
End Sub

Private Sub txtCreditCardFloorLimit_Change()
    
    On Error GoTo LError
    
    txtCreditCardFloorLimit.Text = FormatNumber(txtCreditCardFloorLimit.Text, , , , vbFalse)
    mStrCreditCardFloorLast = txtCreditCardFloorLimit.Text
    Exit Sub
    
LError:
    txtCreditCardFloorLimit.Text = mStrCreditCardFloorLast

End Sub

Private Sub txtMinTender_Change()
    
    On Error GoTo LError
    
    txtMinTender.Text = FormatNumber(txtMinTender.Text, , , , vbFalse)
    mStrMinTenderLast = txtMinTender.Text
    Exit Sub
    
LError:
    txtMinTender.Text = mStrMinTenderLast
    
End Sub

Private Sub txtMaxTender_Change()
    
    On Error GoTo LError
    
    txtMaxTender.Text = FormatNumber(txtMaxTender.Text, 2, vbUseDefault, vbUseDefault, vbFalse)
    mStrMaxTenderLast = txtMaxTender.Text
    Exit Sub
    
LError:
    txtMaxTender.Text = vbNullString
    txtMaxTender.Text = mStrMaxTenderLast
    
End Sub
