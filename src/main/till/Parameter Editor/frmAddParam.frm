VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmAddParam 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Add New Parameter"
   ClientHeight    =   2985
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5460
   Icon            =   "frmAddParam.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2985
   ScaleWidth      =   5460
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox pbxOne 
      BorderStyle     =   0  'None
      Height          =   2175
      Left            =   120
      ScaleHeight     =   2175
      ScaleWidth      =   5175
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   120
      Width           =   5175
      Begin VB.TextBox txtID 
         Height          =   375
         Left            =   1080
         MaxLength       =   5
         TabIndex        =   0
         Top             =   120
         Width           =   1095
      End
      Begin VB.TextBox txtDescription 
         Height          =   375
         Left            =   1080
         MaxLength       =   20
         TabIndex        =   1
         Top             =   660
         Width           =   2535
      End
      Begin VB.ComboBox cmbParamType 
         Height          =   315
         ItemData        =   "frmAddParam.frx":058A
         Left            =   1080
         List            =   "frmAddParam.frx":059D
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1140
         Width           =   1455
      End
      Begin VB.TextBox txtStringValue 
         Height          =   375
         Left            =   1080
         MaxLength       =   50
         TabIndex        =   7
         Top             =   1620
         Width           =   3975
      End
      Begin VB.CheckBox chkBoolValue 
         Height          =   375
         Left            =   3720
         TabIndex        =   6
         Top             =   600
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.TextBox txtNumValue 
         Height          =   375
         Left            =   2760
         TabIndex        =   5
         Top             =   1140
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.TextBox txtColorValue 
         Height          =   375
         Left            =   3600
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   120
         Visible         =   0   'False
         Width           =   1455
      End
      Begin MSComDlg.CommonDialog dlgColorPicker 
         Left            =   4560
         Top             =   1080
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Value"
         Height          =   375
         Left            =   0
         TabIndex        =   13
         Top             =   1680
         Width           =   960
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Type"
         Height          =   255
         Left            =   0
         TabIndex        =   12
         Top             =   1200
         Width           =   960
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Description"
         Height          =   375
         Left            =   0
         TabIndex        =   11
         Top             =   720
         Width           =   960
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "ID"
         Height          =   255
         Left            =   0
         TabIndex        =   10
         Top             =   240
         Width           =   960
      End
   End
   Begin VB.CommandButton btnCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   3960
      TabIndex        =   9
      Top             =   2520
      Width           =   1335
   End
   Begin VB.CommandButton btnAdd 
      Caption         =   "Add"
      Height          =   375
      Left            =   120
      TabIndex        =   8
      Top             =   2520
      Width           =   1215
   End
End
Attribute VB_Name = "frmAddParam"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frnAddParam
'* Date   : 11/17/04
'* Author :
'**********************************************************************************************
'* Summary: Allows user edit of the parameters (Parameters) business object
'**********************************************************************************************
'* Versions:
'</CAMH>***************************************************************************************

Option Explicit

' These constants are from the cEnterprise class cParameter.cls and are
'  used to define parameter field types
Const TYPE_STRING = 0
Const TYPE_LONG = 1
Const TYPE_DOUBLE = 2
Const TYPE_BOOLEAN = 3
Const TYPE_COLOUR = 4 'same as long, but for editor - set using color selector

Private mOParamRec As cParameter
Private mBlnRecordReady As Boolean
Private mlTxtValueTop As Long
Private mlTxtValueLeft As Long

' Main add function
Public Function UserEnterNewRecord(ByRef oRetParamRec As cParameter) As Boolean
    
    ' setup the dynamic user entries
    Call SetupForm
    
    mBlnRecordReady = False             ' default to user cancel
    Set mOParamRec = goDatabase.CreateBusinessObject(CLASSID_PARAMETER)
    
    ' get the new entries
    Call Me.Show(vbModal)
    Set oRetParamRec = mOParamRec
    Set mOParamRec = Nothing
    UserEnterNewRecord = mBlnRecordReady
    
End Function

Private Sub SetupForm()
    
    Dim ctrl As Control
    ' set backgroup for form, labels, checkboxes...
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    pbxOne.BackColor = goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR)
    
    For Each ctrl In Me.Controls
    ' For each control, look for labels and set their background
        If TypeName(ctrl) = "CommandButton" Then
            ctrl.BackColor = Me.BackColor
        ElseIf TypeName(ctrl) = "CheckBox" Then
            ctrl.BackColor = pbxOne.BackColor
        End If
    Next ctrl
    
    ' init runtime control things
    cmbParamType.ListIndex = 0
    mlTxtValueTop = txtStringValue.Top
    mlTxtValueLeft = txtStringValue.Left

End Sub
' Add the new param and close the form
Private Sub btnAdd_Click()
    Dim strMsg As String
    mOParamRec.ID = Val(txtID.Text)
    If mOParamRec.ID < 1 Or mOParamRec.ID > 99999 Then
        strMsg = "Please enter an ID from 1 to 99999"
        txtID.SetFocus
        GoTo ErrBadEntry
    End If
    
    mOParamRec.Description = txtDescription.Text
    If LenB(mOParamRec.Description) = 0 Then
        strMsg = "Please enter a description"
        txtDescription.SetFocus
        GoTo ErrBadEntry
        End If
    
    mOParamRec.ParamType = cmbParamType.ListIndex
    Select Case mOParamRec.ParamType
        Case TYPE_STRING:
            mOParamRec.Value = txtStringValue.Text
        Case TYPE_LONG:
            mOParamRec.Value = txtNumValue.Text
       Case TYPE_DOUBLE:
            mOParamRec.Value = txtNumValue.Text
        Case TYPE_BOOLEAN:
            mOParamRec.Value = chkBoolValue.Value
        Case TYPE_COLOUR:
            mOParamRec.Value = txtColorValue.BackColor
        Case Else
            GoTo ErrBadEntry
    End Select

    mBlnRecordReady = True
    Me.Hide
    Exit Sub
    
ErrBadEntry:
    MsgBox strMsg, vbOKOnly, Me.Caption
End Sub
' abort the add
Private Sub btnCancel_Click()
    
    mBlnRecordReady = False
    Me.Hide
    
End Sub

' changes the type of control to receive input when user changes the param type
Private Sub cmbParamType_Click()

    txtStringValue.Enabled = False
    txtStringValue.Visible = False
    
    chkBoolValue.Top = mlTxtValueTop
    chkBoolValue.Left = mlTxtValueLeft
    chkBoolValue.Enabled = False
    chkBoolValue.Visible = False
    
    txtNumValue.Top = mlTxtValueTop
    txtNumValue.Left = mlTxtValueLeft
    txtNumValue.Enabled = False
    txtNumValue.Visible = False
    
    txtColorValue.Top = mlTxtValueTop
    txtColorValue.Left = mlTxtValueLeft
    txtColorValue.Enabled = False
    txtColorValue.Visible = False
   
    Select Case cmbParamType.ListIndex
        Case TYPE_STRING:
            txtStringValue.Enabled = True
            txtStringValue.Visible = True
        Case TYPE_LONG:
            txtNumValue.Enabled = True
            txtNumValue.Visible = True
       Case TYPE_DOUBLE:
            txtNumValue.Enabled = True
            txtNumValue.Visible = True
        Case TYPE_BOOLEAN:
            chkBoolValue.Enabled = True
            chkBoolValue.Visible = True
        Case TYPE_COLOUR:
            txtColorValue.Enabled = True
            txtColorValue.Visible = True
    End Select

End Sub

' Color dialog for selecting colors. Called when entering edit mode
'   from a color type param
Private Sub txtColorValue_Click()
   
   dlgColorPicker.CancelError = True
   On Error GoTo ErrHandler
   
   dlgColorPicker.Flags = cdlCCRGBInit
   dlgColorPicker.ShowColor
   txtColorValue.BackColor = dlgColorPicker.Color
   Exit Sub
   
ErrHandler: ' user pressed cancel
   Exit Sub

End Sub

Private Sub txtColorValue_KeyPress(KeyAscii As Integer)
    Call txtColorValue_Click
End Sub

Private Sub txtDescription_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then cmbParamType.SetFocus
End Sub

Private Sub txtID_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then txtDescription.SetFocus
End Sub

Private Sub cmbParamType_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        Call cmbParamType_Click
        Select Case cmbParamType.ListIndex
            Case TYPE_STRING:
                txtStringValue.SetFocus
            Case TYPE_LONG:
                txtNumValue.SetFocus
            Case TYPE_DOUBLE:
                txtNumValue.SetFocus
            Case TYPE_BOOLEAN:
                chkBoolValue.SetFocus
            Case TYPE_COLOUR:
                txtColorValue.SetFocus
        End Select
    End If
    
End Sub

Private Sub txtstringvalue_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then txtID.SetFocus
End Sub

Private Sub txtNumValue_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then txtID.SetFocus
End Sub

Private Sub txtBooleanValue_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then txtID.SetFocus
End Sub

Private Sub chkBoolValue_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then txtID.SetFocus
End Sub






