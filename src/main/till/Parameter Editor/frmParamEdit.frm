VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmParamEdit 
   Caption         =   "Parameter Editor"
   ClientHeight    =   7065
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9165
   Icon            =   "frmParamEdit.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7065
   ScaleWidth      =   9165
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10-Exit"
      Height          =   375
      Left            =   7440
      TabIndex        =   5
      Top             =   6120
      Width           =   1335
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "F8-Delete"
      Height          =   375
      Left            =   2400
      TabIndex        =   3
      Top             =   6120
      Width           =   1215
   End
   Begin MSComDlg.CommonDialog dlgColorPicker 
      Left            =   4080
      Top             =   6120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save"
      Enabled         =   0   'False
      Height          =   375
      Left            =   5040
      TabIndex        =   4
      Top             =   6120
      Width           =   1215
   End
   Begin VB.CommandButton cmdAddRecord 
      Caption         =   "Add"
      Height          =   375
      Left            =   240
      TabIndex        =   2
      Top             =   6120
      Width           =   1215
   End
   Begin FPSpreadADO.fpSpread sprdEdit 
      Height          =   5775
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   8775
      _Version        =   458752
      _ExtentX        =   15478
      _ExtentY        =   10186
      _StockProps     =   64
      AllowCellOverflow=   -1  'True
      ColHeaderDisplay=   0
      ColsFrozen      =   6
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GridShowHoriz   =   0   'False
      GridSolid       =   0   'False
      MaxCols         =   6
      RestrictRows    =   -1  'True
      RowHeaderDisplay=   0
      SelectBlockOptions=   1
      SpreadDesigner  =   "frmParamEdit.frx":058A
      UserResize      =   2
      VisibleCols     =   5
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   6690
      Width           =   9165
      _ExtentX        =   16166
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmParamEdit.frx":0975
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   8334
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "19:11"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmParamEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frnParamEdit
'* Date   : 11/17/04
'* Author :
'**********************************************************************************************
'* Summary: Allows user edit of the parameters (Parameters) business object
'**********************************************************************************************
'* Versions:
'</CAMH>***************************************************************************************
Option Explicit

Private Const SPRD_COL_ID = 1
Private Const SPRD_COL_DESC = 2
Private Const SPRD_COL_TYPE = 3
Private Const SPRD_COL_VALUE = 4
Private Const SPRD_COL_CHANGE_FLAG = 6

Private Const ROW_EDIT_TYPE_NONE = 0
Private Const ROW_EDIT_TYPE_CHANGE = 1
Private Const ROW_EDIT_TYPE_ADD = 2
Private Const ROW_EDIT_TYPE_DELETE = 3

Dim mBackColor  As Long
Dim mEditedFlag As Boolean     ' save is needed

' These constants are from the cEnterprise class cParameter.cls and are
'  used to define parameter field types
Const TYPE_STRING = 0
Const TYPE_LONG = 1
Const TYPE_DOUBLE = 2
Const TYPE_BOOLEAN = 3
Const TYPE_COLOUR = 4 'same as long, but for editor - set using color selector

Private Sub Form_Load()

Dim oParameter  As cParameter
Dim colParams   As Collection
Dim lngIndex    As Long

    GetRoot
    Call InitialiseStatusBar(sbStatus)
    
    KeyPreview = True '       F keys for buttons
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    mBackColor = goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR)
    sprdEdit.BackColor = mBackColor
    mEditedFlag = False
    
    Set oParameter = goDatabase.CreateBusinessObject(CLASSID_PARAMETER)
    oParameter.AddLoadField FID_PARAMETER_ID
    oParameter.AddLoadField FID_PARAMETER_Type
    oParameter.AddLoadField FID_PARAMETER_Description
    oParameter.AddLoadField FID_PARAMETER_Boolean
    oParameter.AddLoadField FID_PARAMETER_String
    oParameter.AddLoadField FID_PARAMETER_Long
    oParameter.AddLoadField FID_PARAMETER_Decimal
   ' oParameter.AddLoadFilter CMP_GREATEREQUALTHAN, FID_PARAMETER_ID, 200
   ' oParameter.AddLoadFilter CMP_LESSTHAN, FID_PARAMETER_ID, 300
    
    ' Build the spreadsheet
    sprdEdit.MaxRows = 0
    sprdEdit.EditModeReplace = True
    ' load the spreadsheet data
    Set colParams = oParameter.LoadMatches
    'For lngIndex = 1 To colParams.Count
    For Each oParameter In colParams
        lngIndex = lngIndex + 1
'        Set oParameter = colParams(lngIndex)
        Call AddRowToSprd(lngIndex, oParameter)
        SetRowEditedFlag(lngIndex) = ROW_EDIT_TYPE_NONE
    Next 'lngIndex
    ' Sort by ID
    Call SortSpreadSheet
End Sub

Private Sub Form_Unload(Cancel As Integer)

    If mEditedFlag = True Then
        Select Case MsgBox("Save Changes before exit? (Y/N)", vbYesNoCancel, "Parameter Editor")
            Case vbCancel
                Cancel = 1
            Case vbYes
                Call SaveParamChanges
                Cancel = 0
            Case vbNo
                Cancel = 0
        End Select
    End If
End Sub

Private Sub SortSpreadSheet()
    
    sprdEdit.SortKey(1) = 1
    sprdEdit.SortKeyOrder(1) = SortKeyOrderAscending
    ' Sort data in first five columns and rows by column 1 and 3
    sprdEdit.Sort -1, -1, -1, -1, SortByRow
    sprdEdit.ReDraw = True
    
End Sub


' Sets a parameter's edit status in the spreadsheet
Private Property Let SetRowEditedFlag(ByVal row As Long, ByVal Value As Long)
    
    With sprdEdit
        .row = row
        ' Add the ID
        .Col = SPRD_COL_CHANGE_FLAG
        .CellType = CellTypeNumber
        .Value = Value
        If Value = ROW_EDIT_TYPE_NONE Then
            ' do nothing
        ElseIf Value = ROW_EDIT_TYPE_DELETE Then
           .row = row
           .RowHidden = True
            mEditedFlag = True  ' save is needed
        Else
            mEditedFlag = True  ' save is needed
        End If
    End With
    If mEditedFlag = True Then
        cmdSave.Enabled = True
    End If
End Property

Private Property Get GetRowEditedFlag(ByVal row As Long) As Long
    With sprdEdit
        .row = row
        ' Add the ID
        .Col = SPRD_COL_CHANGE_FLAG
        .CellType = CellTypeNumber
        GetRowEditedFlag = .Value
    End With
End Property

Private Sub AddRowToSprd(ByVal row As Long, ByVal ParamRec As cParameter)
    With sprdEdit
        If row > .MaxRows Then
            .MaxRows = row
        End If
        .row = row
        ' Add the ID
        .Col = SPRD_COL_ID
        .CellType = CellTypeNumber
        .TypeNumberMin = 1
        .TypeNumberMax = 99999
        .TypeNumberDecPlaces = 0
        .Value = Val(ParamRec.ID)
        .TypeMaxEditLen = 5
            
        ' Add the description
        .Col = SPRD_COL_DESC
        .Value = ParamRec.Description
        .TypeMaxEditLen = 20
           
        ' Add the parameter type
        .Col = SPRD_COL_TYPE
        .Value = Str$(ParamRec.ParamType)
            
        ' Add the paramter value
        Call SetParamRowType(.row, ParamRec.ParamType, ParamRec.Value)
    End With
End Sub

' Initialize a row for its type of parameter
Private Sub SetParamRowType(ByVal row As Long, ByVal ParamType As Integer, ByRef ParmValue As Variant)
    
    With sprdEdit
        .row = row
        .Col = SPRD_COL_VALUE
        ' reset cell color and size
        .BackColor = mBackColor
        ' Call .RemoveCellSpan(SPRD_COL_VALUE, .row) ' remove for sort
        .Value = ParmValue
        Select Case ParamType
            Case TYPE_STRING:
                .CellType = CellTypeEdit
                .TypeMaxEditLen = 50
                .Value = """" & .Value & """"
                ' Call .AddCellSpan(SPRD_COL_VALUE, .row, 3, 1) ' removed for sort
            Case TYPE_LONG:
                .CellType = CellTypeNumber
                .TypeNumberMin = -2147483647
                .TypeNumberMax = 2147483647
                .TypeNumberDecPlaces = 0
                .TypeMaxEditLen = 10
            Case TYPE_DOUBLE:
                .CellType = CellTypeNumber
                .TypeNumberMin = -99999999999999#
                .TypeNumberMax = 99999999999999#
                .TypeNumberDecPlaces = 5
                .TypeMaxEditLen = 18
                .TypeNumberLeadingZero = TypeLeadingZeroIntl
            Case TYPE_BOOLEAN:
                .CellType = CellTypeCheckBox
                .TypeCheckCenter = False
                .TypeCheckType = TypeCheckTypeNormal
            Case TYPE_COLOUR:
                .Value = vbNullString
                .BackColor = Val(ParmValue)
            Case Else:
                MsgBox .Value, vbOKOnly, "Invalid row type, Row= " & Str$(row) & ", type=" & Str$(ParamType)
        End Select
    End With
    
End Sub

' Color dialog for selecting colors. Called when entering edit mode
'   from a color type param
Private Sub ParamColorSelect(ByVal row As Long)
   
   dlgColorPicker.CancelError = True
   On Error GoTo ErrHandler
   
   sprdEdit.row = row
   dlgColorPicker.Flags = cdlCCRGBInit
   dlgColorPicker.ShowColor
   sprdEdit.Col = SPRD_COL_VALUE
   sprdEdit.BackColor = dlgColorPicker.Color
   Exit Sub
   
ErrHandler: ' user pressed cancel
   Exit Sub

End Sub

' Event called when the user begins to edit a cell
Sub sprdedit_EditMode(ByVal Col As Long, ByVal row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
    
    Dim iRowType As Integer
    
    ' get param type
    sprdEdit.row = row
    sprdEdit.Col = SPRD_COL_TYPE
    iRowType = sprdEdit.Value
    ' move to current col
    sprdEdit.Col = Col
    
    If Mode = 1 Then ' entering edit mode
        Select Case Col
            Case SPRD_COL_ID:       ' can't edit the ID
                sprdEdit.EditMode = False
            Case SPRD_COL_DESC:
            Case SPRD_COL_TYPE:
            Case SPRD_COL_VALUE:
            Select Case iRowType
                Case TYPE_STRING:   ' remove the quotes to edit the text field
                    If Len(sprdEdit.Value) < 3 Then
                        sprdEdit.Value = vbNullString
                    Else
                        sprdEdit.Value = Mid$(sprdEdit.Value, 2, Len(sprdEdit.Value) - 2)
                    End If
                Case TYPE_LONG:
                Case TYPE_DOUBLE:
                Case TYPE_BOOLEAN:
                Case TYPE_COLOUR:   ' Do the color dialog and get out of edit mode
                    Call ParamColorSelect(sprdEdit.row)
                    sprdEdit.EditMode = False
                    
                Case Else:          ' Bad param type
                    MsgBox sprdEdit.Value, vbOKOnly, "Invalid row type " & Str$(Col) & ", " & Str$(row) & ", type=" & Str$(iRowType)
            End Select
        End Select
        
    ElseIf Mode = 0 Then            ' exiting edit mode
        Select Case Col
            Case SPRD_COL_ID:
            Case SPRD_COL_DESC:
            Case SPRD_COL_TYPE:     ' Reset row for new param type
                Call SetParamRowType(row, iRowType, vbNullString)
            Case SPRD_COL_VALUE:
            Select Case iRowType
                Case TYPE_STRING:   ' Put the quotes back on the string
                    sprdEdit.Value = """" & sprdEdit.Value & """"
                Case TYPE_LONG:
                Case TYPE_DOUBLE:
                Case TYPE_BOOLEAN:
                Case TYPE_COLOUR:
                Case Else:
                    MsgBox sprdEdit.Value, vbOKOnly, "Invalid row type " & Str$(Col) & ", " & Str$(row) & ", type=" & Str$(iRowType)
            End Select
        End Select
        If Col <> SPRD_COL_ID Then
            SetRowEditedFlag(row) = ROW_EDIT_TYPE_CHANGE
        End If
    End If
End Sub

' Updates the Parameter table through IBo
Private Sub SaveParamChanges()

    Dim oParameter  As cParameter
    Dim lIndex      As Long
    Dim lSaveType   As Long
    
    On Error GoTo LError
    
    If mEditedFlag = True Then
        With sprdEdit
            For lIndex = 1 To .MaxRows
                .row = lIndex
                .Col = SPRD_COL_CHANGE_FLAG
                lSaveType = .Value
                .Value = ROW_EDIT_TYPE_NONE
                If lSaveType <> ROW_EDIT_TYPE_NONE Then
                    Set oParameter = goDatabase.CreateBusinessObject(CLASSID_PARAMETER)
                    .Col = SPRD_COL_ID
                    oParameter.ID = .Value
                    .Col = SPRD_COL_DESC
                    oParameter.Description = .Value
                    .Col = SPRD_COL_TYPE
                    oParameter.ParamType = .Value
                    .Col = SPRD_COL_VALUE
                    If oParameter.ParamType = TYPE_COLOUR Then
                        oParameter.Value = .BackColor
                    ElseIf oParameter.ParamType = TYPE_STRING Then
                        If Len(.Value) < 3 Then
                            oParameter.Value = vbNullString
                        Else
                            oParameter.Value = Mid$(.Value, 2, Len(.Value) - 2)
                        End If
                    ElseIf oParameter.ParamType = TYPE_BOOLEAN Then
                        oParameter.Value = .Value
                    Else
                        oParameter.Value = Val(.Value)
                    End If
                    Select Case lSaveType
                        Case ROW_EDIT_TYPE_ADD
                            oParameter.IBo_SaveIfNew
                        Case ROW_EDIT_TYPE_CHANGE
                            oParameter.IBo_SaveIfExists
                        Case ROW_EDIT_TYPE_DELETE
                            oParameter.IBo_Delete
                    End Select
                End If
            Next lIndex
        End With
    
    cmdSave.Enabled = False
    mEditedFlag = False
    End If
    Exit Sub
    
LError:
    MsgBox "Error saving parameters, line=" & lIndex & ", ID=" & oParameter.ID
    
End Sub

Private Sub cmdAddRecord_Click()

    Dim row As Long
    Dim oParam As cParameter
    Dim lIndex As Long
    
    Set oParam = goDatabase.CreateBusinessObject(CLASSID_PARAMETER)
    Load frmAddParam
    If frmAddParam.UserEnterNewRecord(oParam) = True Then
        sprdEdit.Col = SPRD_COL_ID
        For lIndex = 1 To sprdEdit.MaxRows
            sprdEdit.row = lIndex
            If oParam.ID = sprdEdit.Value Then
                MsgBox "ID is not unique, please re-enter paramater"
                Exit Sub
            End If
        Next
        
        row = sprdEdit.MaxRows + 1
        Call AddRowToSprd(row, oParam)
        SetRowEditedFlag(row) = ROW_EDIT_TYPE_ADD
    End If
    Unload frmAddParam
    
End Sub

Private Sub cmdDelete_Click()
    
    Dim strDesc As String
    With sprdEdit
        .row = .ActiveRow
        .Col = SPRD_COL_DESC
        If MsgBox("Delete the entry -""" & .Value & """(Y/N)?", vbYesNoCancel) = vbYes Then
            SetRowEditedFlag(.ActiveRow) = ROW_EDIT_TYPE_DELETE
        End If
    End With
    
End Sub

Private Sub cmdSave_Click()
  
  If mEditedFlag = True Then Call SaveParamChanges
    
End Sub

Private Sub cmdExit_Click()
    
    Unload Me
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        Case vbKeyF8:
            cmdDelete_Click
        Case vbKeyF10:
            cmdExit_Click
    End Select
    
End Sub
Private Sub Form_Resize()

Dim lSpacing As Long
    
    Dim iBtnCnt     As Long
    Dim iBtnWidth   As Long
    Dim iBtnHeight  As Long
    Dim i           As Long
    Dim iBtnGap     As Long
    Dim iBtnCInc    As Long
    Const BORDER_WIDTH = 60
        
    iBtnCnt = 4
    iBtnWidth = cmdAddRecord.Width
    iBtnHeight = cmdAddRecord.Height
    iBtnGap = cmdAddRecord.Top - sprdEdit.Top + sprdEdit.Height
    If Me.WindowState = vbMinimized Then Exit Sub
    lSpacing = sprdEdit.Left
    
    'Check form is not below minimum width
    If Me.Width < iBtnWidth * iBtnCnt + (lSpacing * 4) Then
        Me.Width = iBtnWidth * iBtnCnt + (lSpacing * 4)
       ' Exit Sub
    End If
    'Check form is not below minimum height
    If Me.Height < sprdEdit.Top + iBtnHeight * 3 + sbStatus.Height Then
        Me.Height = sprdEdit.Top + iBtnHeight * 3 + sbStatus.Height
        Exit Sub
    End If
    
    'start resizing
    sprdEdit.Width = Me.Width - sprdEdit.Left * 2 - BORDER_WIDTH '* 2
    sprdEdit.Height = Me.Height - (iBtnHeight + sprdEdit.Top + (lSpacing * 3) + (BORDER_WIDTH * 6) + sbStatus.Height)
    
    iBtnCInc = (sprdEdit.Width - iBtnWidth) / 3
    i = sprdEdit.Left
    cmdAddRecord.Left = i
    cmdDelete.Left = i + iBtnCInc
    cmdSave.Left = i + iBtnCInc * 2
    cmdExit.Left = i + iBtnCInc * 3
    
    i = sprdEdit.Top + sprdEdit.Height + lSpacing
    cmdAddRecord.Top = i
    cmdDelete.Top = i
    cmdSave.Top = i
    cmdExit.Top = i
    
    cmdDelete.Width = iBtnWidth
    cmdDelete.Height = iBtnHeight
    cmdSave.Width = iBtnWidth
    cmdSave.Height = iBtnHeight
    cmdExit.Width = iBtnWidth
    cmdExit.Height = iBtnHeight

End Sub

