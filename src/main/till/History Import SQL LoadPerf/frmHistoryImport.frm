VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmHistoryImport 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Performing History Import SQL Loading"
   ClientHeight    =   6300
   ClientLeft      =   1650
   ClientTop       =   1755
   ClientWidth     =   6210
   Icon            =   "frmHistoryImport.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6300
   ScaleWidth      =   6210
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstFiles 
      Height          =   4935
      Left            =   120
      TabIndex        =   0
      Top             =   720
      Width           =   5955
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   2
      Top             =   5925
      Width           =   6210
      _ExtentX        =   10954
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmHistoryImport.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   3598
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "11:13"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblStatus 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   5955
   End
End
Attribute VB_Name = "frmHistoryImport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>************************************************************************************
'* Module : frmHistoryImport
'* Date   : 09/11/07
'* Author : Unknown
'*$Archive: /Projects/OasysV2/VB/Till/frmTill.frm $
'******************************************************************************************
'* Summary:
'******************************************************************************************
'* $Author: Richardc $ $Date: 10/09/04 9:20 $ $Revision: 84 $
'* Versions:
'* 17/02/03    Unknown
'*             Header added.
'* 09/11/07    Mauricem
'*             Added SKU Reject record SR
'</CAMH>***********************************************************************************

Option Explicit

Const MODULE_NAME As String = "modHistoryImport"

' Types of Sale
Const TT_SALE       As String = "SA"
Const TT_REFUND     As String = "RF"
Const TT_SIGNON     As String = "CO"
Const TT_COLLECT    As String = "IC"
Const TT_SIGNOFF    As String = "CC"
Const TT_OPENDRAWER As String = "OD"
Const TT_LOGO       As String = "RL"
Const TT_XREAD      As String = "XR"
Const TT_ZREAD      As String = "ZR"
Const TT_MISCIN     As String = "M+"
Const TT_MISCOUT    As String = "M-"
Const TT_RECALL     As String = "RP"
Const TT_QUOTE      As String = "QD"
Const TT_EXT_SALE   As String = "ES"
Const TT_TELESALES  As String = "TS"
Const TT_TELEREFUND As String = "TR"
Const TT_REPRINTREC As String = "RR"
Const TT_LOOKUP     As String = "LU"
Const TT_CORRIN     As String = "C+"
Const TT_CORROUT    As String = "C-"

Public Enum enFmtType
    enftString = 0
    enftNumber = 1
    enftDate = 2
    enftYesNo = 3
    enftCode = 4
End Enum

Private Enum enFlashTotalCategory
    enftSales = 1
    enftSaleCorrections
    enftRefunds
    enftRefundCorrections
    enftSignon
    enftSignOff
    enftMiscPaidIn
    enftMiscPaidOut
    enftMiscOpenDrawer
    enftRepritLogo
    enftVoidedTransactions
    enftPriceOverrides
    enftSpare
End Enum

Dim moFSO As FileSystemObject

Private moCashierTotals As cCashierTotals
Private moTillTotals    As cTillTotals
Private moSaleTotals    As cSaleTotals
Private mblnAccCashier  As Boolean
Private mstrDuplPath    As String 'location to move duplicated transaction files to
Private mstrErrorPath   As String 'location to save transaction files with Errors In
Private mstrOldFilePath As String 'location to save transaction files older than 1 year


Sub Form_Load()

Dim oSysopt As cSystemOptions
Dim oRetOpt As cRetailOptions
Dim oFiles  As Files
Dim oFile   As File

Dim strFileHist As String
Dim strFileDone As String

Dim strTillNo   As String
            
    Call GetRoot
    If goSession.ISession_IsOnline = False Then End
    
    Call InitialiseStatusBar(sbStatus)
                
    Me.Show
    
    strTillNo = Right$("00" & goSession.CurrentEnterprise.IEnterprise_WorkstationID, 2)
    
    Set oSysopt = goDatabase.CreateBusinessObject(CLASSID_SYSTEMOPTIONS)
    oSysopt.LoadMatches
    
    Set oRetOpt = goDatabase.CreateBusinessObject(CLASSID_RETAILEROPTIONS)
    oRetOpt.LoadMatches
    mblnAccCashier = (oRetOpt.AccountabilityType = "C")
            
    strFileHist = oSysopt.TillLocalDrive & ":\" & Trim$(oSysopt.TillPathName) & "\HISTORY\"
    strFileDone = oSysopt.TillLocalDrive & ":\" & Trim$(oSysopt.TillPathName) & "\UPDATED\"
    mstrDuplPath = oSysopt.TillRemoteDrive & ":\" & Trim$(oSysopt.TillPathName) & "\UPDATED\DUPL\"
    mstrErrorPath = oSysopt.TillRemoteDrive & ":\" & Trim$(oSysopt.TillPathName) & "\UPDATED\ERRORS\"
    mstrOldFilePath = oSysopt.TillRemoteDrive & ":\" & Trim$(oSysopt.TillPathName) & "\UPDATED\OLDFILES\"
    Set moFSO = New Scripting.FileSystemObject
    
    If (moFSO.FolderExists(strFileHist) = False) Then
        Call DebugMsg(MODULE_NAME, "Main", endlDebug, "Folder(Hist) does not exist so creating-'" & strFileHist & "'")
        Call moFSO.CreateFolder(strFileHist)
    End If
    
    If (moFSO.FolderExists(strFileDone) = False) Then
        Call DebugMsg(MODULE_NAME, "Main", endlDebug, "Folder(Done) does not exist so creating-'" & strFileDone & "'")
        Call moFSO.CreateFolder(strFileDone)
    End If
    
    Dim rsData As New ADODB.Recordset
    While 1 = 1
        lstFiles.AddItem ("Reading lots of data" & Format(Now(), "HH:NN:SS DD/MM/YY"))
        lstFiles.ListIndex = lstFiles.ListCount - 1
        lstFiles.Refresh
        Call rsData.Open("select * from dltots inner join dlline on dltots.date1=dlline.DATE1 and dltots.till=dlline.till and dltots.[tran]=dlline.[tran] inner join dlpaid on dltots.date1=dlpaid.DATE1 and dltots.till=dlpaid.till and dltots.[tran]=dlpaid.[tran] inner join STKMAS on DLLINE.SKUN=STKMAS.SKUN inner join STKLOG on DLLINE.SKUN=STKLOG.skun ORDER BY STKLOG.SKUN DESC", goDatabase.Connection)
        lstFiles.AddItem ("Loading data" & Format(Now(), "HH:NN:SS DD/MM/YY"))
        While Not rsData.EOF
            rsData.MoveNext
        Wend
        Call rsData.Close
    Wend
    lstFiles.AddItem ("Processing Completed")
    lstFiles.ListIndex = lstFiles.ListCount - 1
    Call DebugMsg(MODULE_NAME, "Main", endlTraceOut)
    End
        
End Sub
                
                
Private Sub ProcessStockLogFiles()

Const DONE_FOLDER As String = "F:\WIX\LockDone\"

Dim oFiles As Files
Dim oFile  As File
    
    lblStatus.Caption = "Accessing Stock Log Files List"
    If (moFSO.FolderExists(DONE_FOLDER) = False) Then Call moFSO.CreateFolder(DONE_FOLDER)
    
    Set oFiles = moFSO.GetFolder("F:\WIX").Files
            
    lstFiles.AddItem ("Accessed Files (" & oFiles.Count & " file(s) in folder)")
    For Each oFile In oFiles
        If (Left$(oFile.Name, 2) = goSession.CurrentEnterprise.IEnterprise_WorkstationID) And (InStr(oFile.Name, ".") = 0) Then
            lstFiles.AddItem ("Processing File - " & oFile.Name)
            lstFiles.ListIndex = lstFiles.ListCount - 1
            If (ProcessLockedFile(oFile.Path) = True) Then
                'Rename file to add .HST extension
                If (moFSO.FileExists("F:\WIX\" & oFile.Name & ".HST") = True) Then Call moFSO.DeleteFile("F:\WIX\" & oFile.Name & ".HST")
                oFile.Name = oFile.Name & ".HST"
'                'Move file to the processed folder
                If (moFSO.FileExists(DONE_FOLDER & oFile.Name) = True) Then Call moFSO.DeleteFile(DONE_FOLDER & oFile.Name)
                lstFiles.AddItem (" - Processed File - " & oFile.Name)
                lstFiles.ListIndex = lstFiles.ListCount - 1
                Call oFile.Move(DONE_FOLDER)
            End If
        End If
    Next
    lstFiles.AddItem ("Processing Completed")
    lstFiles.ListIndex = lstFiles.ListCount - 1
    Call DebugMsg(MODULE_NAME, "ProcessStockLogFiles", endlTraceOut)
    
End Sub

Private Function ProcessLockedFile(ByVal strFilePath As String) As Boolean

Dim tsLog  As TextStream

Const POS_PARTCODE As Long = 0
Const POS_MOVETYPE As Long = 1
Const POS_QTYSOLD As Long = 2
Const POS_QTYMARKDOWN As Long = 3
Const POS_KEY As Long = 4
Const POS_USERID As Long = 5
Const POS_PRICE As Long = 6
Const POS_STKPRICE As Long = 7

Dim strData     As String
Dim strPartCode As String
Dim strMoveType As String
Dim strQuery    As String
Dim lngSoldQty  As Long
Dim lngMarkQty  As Long

Dim vntData     As Variant
Dim rsStock     As New Recordset
Dim lngStartQty As Long
Dim lngStartRet As Long
Dim lngStartMDQ As Long
Dim lngStartWOQ As Long

Dim rsSTKMAS

    Set tsLog = moFSO.OpenTextFile(strFilePath, ForReading, False)
    While (tsLog.AtEndOfStream = False)
        strData = tsLog.ReadLine
    Wend
    tsLog.Close
    If (Left$(strData, 1) = "*") And (Right$(strData, 1) = "*") Then
        strData = Mid$(strData, 2)
        If (Right$(strData, 1) = "*") Then strData = Left$(strData, Len(strData) - 1)
        vntData = Split(strData, ",")
        If (UBound(vntData) < 4) Then
            Call DebugMsg(MODULE_NAME, "ProcessLockedFile", endlDebug, "Skipped file as too few columns in file")
            ProcessLockedFile = True
            Exit Function
        End If
        strPartCode = vntData(POS_PARTCODE)
        strMoveType = vntData(POS_MOVETYPE)
        lngSoldQty = Val(vntData(POS_QTYSOLD))
        lngMarkQty = Val(vntData(POS_QTYMARKDOWN))
    End If
        
    'Determine Stock Movement type based on Quantity Sign and if Back Door Collection
'    Select Case (strMoveType)
'        Case ("01"): lngSoldQty = lngSoldQty
'        Case ("02"): lngSoldQty = lngSoldQty * -1
'        Case ("04"): lngMarkQty = lngSoldQty
'                     lngSoldQty = 0
'
'    End Select
    
    Set rsStock = goDatabase.ExecuteCommand("select ONHA ""QuantityAtHand"", RETQ ""UnitsInOpenReturns"", MDNQ ""MarkDownQuantity"", WTFQ ""WriteOffQuantity"" FROM STKMAS WHERE SKUN = '" & strPartCode & "'")
    Call DebugMsg(MODULE_NAME, "Save", endlTraceOut, "stkmas Read")
    If rsStock.RecordCount = 0 Then Return
    
    Dim lngQuantityAtHand As Long
            
    lngStartQty = rsStock!QuantityAtHand
    lngStartRet = rsStock!UnitsInOpenReturns
    lngStartMDQ = rsStock!MarkDownQuantity
    lngStartWOQ = rsStock!WriteOffQuantity
        
    goDatabase.StartTransaction
    On Error Resume Next
'        Call goDatabase.ExecuteCommand("update stkmas set ONHA = ONHA + " & lngsolqty & ", MDNQ = MNDQ + " & lngMarkQty & ", SALU1 = " & lngUnitsSoldYesterday & ", SALU2 = " & lngUnitsSoldThisWeek & ", SALU4 = " & lngUnitsSoldThisPeriod & ", SALU6 = " & lngUnitsSoldThisYear & ", SALV1 = " & curValueSoldYesterday & ", SALV2 = " & curValueSoldThisWeek & ", SALV4 = " & curValueSoldThisPeriod & ", SALV6 = " & curValueSoldThisYear & " where SKUN = '" & mPartCode & "'")
    Call goDatabase.ExecuteCommand("update stkmas set ONHA = ONHA - " & lngSoldQty & ", MDNQ = MDNQ - " & lngMarkQty & " where SKUN = '" & strPartCode & "'")
    If (Err.Number <> 0) Then
        Call DebugMsg(MODULE_NAME, "ProcessLockedFile", endlDebug, "Error on Update STKMAS : " & Err.Number & ":" & Err.Description)
        goDatabase.RollbackTransaction
        Exit Function
    End If
    
    strQuery = "INSERT INTO STKLOG (DATE1, TIME, SKUN, TYPE, KEYS, ICOM, EEID, SSTK, SRET, " & _
            "SPRI, SMDN, SWTF, ESTK, ERET, EPRI, EMDN, EWTF, DAYN) VALUES ('" & _
            Format(Date, "yyyy-mm-dd") & "', '" & Format(Now, "HHMMSS") & "', '" & strPartCode & _
            "', '" & strMoveType & "', '" & vntData(POS_KEY) & "', 0, '" & vntData(POS_USERID) & _
            "', " & lngStartQty & ", " & lngStartRet & ", " & Val(vntData(POS_PRICE)) & ", " & lngStartMDQ & ", " & _
            lngStartWOQ & ", " & lngStartQty - lngSoldQty & ", " & lngStartRet & ", " & Val(vntData(POS_STKPRICE)) & ", " & _
            lngStartMDQ - lngMarkQty & ", " & lngStartWOQ & ", " & CLng(Date) - 1 & ")"
            
    Call goDatabase.ExecuteCommand(strQuery)
    If (Err.Number <> 0) Then
        Call DebugMsg(MODULE_NAME, "ProcessLockedFile", endlDebug, "Error on Insert STKLOG : " & Err.Number & ":" & Err.Description)
        goDatabase.RollbackTransaction
        Exit Function
    End If
    Set tsLog = Nothing
        
    ProcessLockedFile = True
    goDatabase.CommitTransaction
    
End Function

Private Function ProcessFile(ByVal strFileName As String) As Boolean

Dim tsInputFile As TextStream

Dim oPOSHeader   As cPOSHeader
Dim oPOSPmt      As cPOSPayment
Dim oRetCustBO   As cReturnCust
Dim colRetCust   As Collection
Dim oHotVoucherBO As cGiftVoucherHotFile
Dim colHotVoucher As Collection
Dim oCashier    As cUser

Dim strData     As String
Dim strLineCode As String
Dim blnTranStarted As Boolean

Dim blnECChecksOnly As Boolean

Dim oItemRejectBO   As cSale_Wickes.cSKUReject

Dim colPromptRejects    As New Collection

Dim dteTran As Date
Dim strTillID As String
Dim strTranNo As String


On Error GoTo Bad_ProcessFile
    
    blnTranStarted = False
    Set oPOSHeader = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    oPOSHeader.OnlineUpdate = True
    
    Set colRetCust = New Collection
    Set colHotVoucher = New Collection
    
    lstFiles.AddItem (" - Importing Data")
    If (InStr(strFileName, "EC.") > 0) Then blnECChecksOnly = True
    
    Set tsInputFile = moFSO.OpenTextFile(strFileName, ForReading, False)
    
    While (tsInputFile.AtEndOfStream = False)
        strData = tsInputFile.ReadLine
        strLineCode = Left$(strData, 2)
        strData = Mid$(strData, 3)
        Select Case (strLineCode)
            Case ("DT"): If (ProcessPOSHeader(oPOSHeader, strData, strFileName, tsInputFile) = False) Then Exit Function
            Case ("DL"): Call ProcessPOSLine(oPOSHeader, strData, strFileName)
            Case ("DP"): Call ProcessPOSPmt(oPOSHeader, strData, strFileName)
            Case ("DV"): Call ProcessEventLine(oPOSHeader, strData, strFileName)
            Case ("DR"): Call ProcessRetCust(oPOSHeader, strData, oRetCustBO, strFileName)
                         Call colRetCust.Add(oRetCustBO)
            Case ("DA"): Call ProcessEANs(oPOSHeader, strData, strFileName)
            Case ("DG"): Call ProcessGiftVouchers(oPOSHeader, strData, strFileName)
            Case ("GH"): Call ProcessHotVouchers(oPOSHeader, strData, oHotVoucherBO, strFileName)
                         Call colHotVoucher.Add(oHotVoucherBO)
            Case ("DO"): Call ProcessPriceOverLines(oPOSHeader, strData, strFileName)
            Case ("DC"): Call ProcessOverrideCust(oPOSHeader, strData, strFileName)
            Case ("EC"): Call ProcessEANCheck(oPOSHeader, strData, strFileName, blnECChecksOnly)
            Case ("SR"): Call ProcessSKUReject(oPOSHeader, strData, strFileName, colPromptRejects)
        End Select
        
    Wend
    
    Call tsInputFile.Close
    Set tsInputFile = Nothing
    
    If (blnECChecksOnly = False) Then
 '       Call goDatabase.StartTransaction
        blnTranStarted = True
        oPOSHeader.Offline = True
        If (oPOSHeader.IBo_SaveIfNew = True) Then
            lstFiles.AddItem (" - Saved Data")
            For Each oRetCustBO In colRetCust
                If (oRetCustBO.IBo_SaveIfNew = False) Then
                    Call Err.Raise(1, "ProcessFile-" & strFileName, "Saving Ret Cust-" & strFileName)
                End If
            Next
            Call oPOSHeader.SaveGiftVouchers
            For Each oHotVoucherBO In colHotVoucher
                If (oHotVoucherBO.IBo_SaveIfNew = False) Then
                    Call Err.Raise(1, "ProcessFile-" & strFileName, "Saving HOT Vouchers-" & strFileName)
                End If
            Next

            'Added 09/11/07 - Save any Item Reject prompts
            For Each oItemRejectBO In colPromptRejects
                Call oItemRejectBO.SaveIfNew
            Next
            
            'Added 12/1/06 - if non-training and for Today, then update flash totals on server
            With oPOSHeader
                If (.TrainingMode = False) And (.TranDate = Date) Then
                    If (.Voided = False) Then
                        If (mblnAccCashier = True) Then Call LoadCashierFlash(.cashierId)
                        Call ZReadTranUpdate(.TillID, .TransactionNo, .TransactionCode, .TotalSaleAmount, _
                                .DiscountAmount, .MerchandiseAmount, .NonMerchandiseAmount, .cashierId)
                        Call UpdateFlashTotals(False, .TransactionCode, 0, .TotalSaleAmount)
                        'Update ZReads with Tendered values
                    Else
                        Call UpdateFlashTotals(False, "", 0, .TotalSaleAmount)
                    End If
                    'Update Flash Totals for all Payments received
                    For Each oPOSPmt In .Payments
                        Call UpdateFlashTotals(True, "", oPOSPmt.TenderType, oPOSPmt.TenderAmount)
                    Next
                End If 'Update Flash Totals
            End With
            
            'if Transaction is a sign off - then flag cashier and till as logged off
            If (oPOSHeader.TransactionCode = TT_SIGNOFF) Then
                If (mblnAccCashier = True) Then
                    Call LoadCashierFlash(oPOSHeader.cashierId)
                    Set oCashier = goDatabase.CreateBusinessObject(CLASSID_USER)
                    Call oCashier.IBo_AddLoadFilter(CMP_EQUAL, FID_USER_EmployeeID, oPOSHeader.cashierId)
                    Call oCashier.IBo_AddLoadField(FID_USER_TillReceiptName)
                    Call oCashier.IBo_AddLoadField(FID_USER_Password)
                    Call oCashier.IBo_LoadMatches
                    moCashierTotals.CurrentTillNumber = "99"
                    moCashierTotals.CashierName = oCashier.TillReceiptName
                    moCashierTotals.SecurityCode = oCashier.Password
                    Set oCashier = Nothing
                    moCashierTotals.SaveIfExists
                End If
                
                With moTillTotals
                    .PreviousCashierNumber = .CashierNumber
                    .CashierNumber = "999"
                    .CurrentTransactionNumber = oPOSHeader.TransactionNo
                    .TillCheckingEvents = False
                    .TimeLastSignedOff = Format$(Now, "HHMM")
                    .SaveIfExists
                End With
            End If
                
            'Added 22/07/2008 - set RTI flags
            On Error Resume Next
            dteTran = oPOSHeader.TranDate
            strTillID = oPOSHeader.TillID
            strTranNo = oPOSHeader.TransactionNo
            Call goDatabase.ExecuteCommand("UPDATE DLTOTS SET RTI='S' WHERE DATE1='" & Format(dteTran, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND Tran='" & strTranNo & "'")
            Call goDatabase.ExecuteCommand("UPDATE DLLINE SET RTI='S' WHERE DATE1='" & Format(dteTran, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND Tran='" & strTranNo & "'")
            Call goDatabase.ExecuteCommand("UPDATE DLPAID SET RTI='S' WHERE DATE1='" & Format(dteTran, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND Tran='" & strTranNo & "'")
            Call goDatabase.ExecuteCommand("UPDATE DLRCUS SET RTI='S' WHERE DATE1='" & Format(dteTran, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND Tran='" & strTranNo & "'")
            Call goDatabase.ExecuteCommand("UPDATE DLEVNT SET RTI='S' WHERE DATE1='" & Format(dteTran, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND Tran='" & strTranNo & "'")
            Call goDatabase.ExecuteCommand("UPDATE DLANAS SET RTI='S' WHERE DATE1='" & Format(dteTran, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND Tran='" & strTranNo & "'")
            Call goDatabase.ExecuteCommand("UPDATE DLGIFT SET RTI='S' WHERE DATE1='" & Format(dteTran, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND Tran='" & strTranNo & "'")
            Call goDatabase.ExecuteCommand("UPDATE DLOCUS SET RTI='S' WHERE DATE1='" & Format(dteTran, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND Tran='" & strTranNo & "'")
            Call goDatabase.ExecuteCommand("UPDATE DLOLIN SET RTI='S' WHERE DATE1='" & Format(dteTran, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND Tran='" & strTranNo & "'")
            Call goDatabase.ExecuteCommand("UPDATE DLEANCHK SET RTI='S' WHERE DATE1='" & Format(dteTran, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND Tran='" & strTranNo & "'")
            Call goDatabase.ExecuteCommand("UPDATE DLREJECT SET RTI='S' WHERE DATE1='" & Format(dteTran, "YYYY-MM-DD") & "' AND TILL='" & strTillID & "' AND Tran='" & strTranNo & "'")
'            Call DebugMsg(MODULE_NAME, "ProcessFile", endlDebug, "SQL=" & "UPDATE STKLOG SET RTI='S' WHERE KEYS LIKE '" & Format(dteTran, "DD/MM/YY") & strTillID & strTranNo & "%'")
'            Call goDatabase.ExecuteCommand("UPDATE STKLOG SET RTI='S' WHERE KEYS LIKE '" & Format(dteTran, "DD/MM/YY") & strTillID & strTranNo & "%'")
            Call Err.Clear
            On Error GoTo Bad_ProcessFile


'            Call goDatabase.CommitTransaction
        Else
            lstFiles.AddItem (" - Save File - File Skipped")
            On Error Resume Next
            Call goDatabase.RollbackTransaction
            blnTranStarted = False
            Call Err.Clear
            On Error GoTo Bad_ProcessFile
            Call Err.Raise(1, "ProcessFile-" & strFileName, "Saving File-" & strFileName & " failed in " & oPOSHeader.SaveFailedStatus)
        End If
    End If 'Save Header if full transaction
    
    ProcessFile = True
    Exit Function
    

Bad_ProcessFile:

    Call Err.Report(MODULE_NAME, "ProcessFile-" & strFileName, 1, False)
    On Error Resume Next
    If (blnTranStarted = True) Then Call goDatabase.RollbackTransaction
    Call Err.Clear
    If (tsInputFile Is Nothing = False) Then tsInputFile.Close
    If (moFSO.FileExists(mstrErrorPath & Right$(strFileName, 10)) = False) Then
        Call moFSO.MoveFile(strFileName, mstrErrorPath)
    End If
        
End Function

Private Sub LoadCashierFlash(strCashierID As String)
        
    ' Initialise Cashier Totals
    If ((moCashierTotals Is Nothing) = True) Then Set moCashierTotals = goDatabase.CreateBusinessObject(CLASSID_CASHIERTOTALS)
                                       
    'Check if Cashier is already loaded
    If (moCashierTotals.CashierNumber <> strCashierID) Then
        Call moCashierTotals.AddLoadFilter(CMP_EQUAL, FID_CASHIERTOTALS_CashierNumber, strCashierID)
        If (moCashierTotals.LoadMatches.Count < 1) Then 'no totals found so create one
            moCashierTotals.CashierNumber = strCashierID
            Set moSaleTotals = goDatabase.CreateBusinessObject(CLASSID_SALETOTALS)
            moSaleTotals.FloatAmount = 0
            moSaleTotals.SaveIfNew
            moCashierTotals.SaleTotalsKey = moSaleTotals.SaleTotalKey
            moCashierTotals.SaveIfNew
        Else
            Set moSaleTotals = goDatabase.CreateBusinessObject(CLASSID_SALETOTALS)
            Call moSaleTotals.AddLoadFilter(CMP_EQUAL, FID_SALETOTALS_SaleTotalKey, _
                                            moCashierTotals.SaleTotalsKey)
            moSaleTotals.LoadMatches
        End If
    Else 'match found so load up Sales Totals
        Set moSaleTotals = goDatabase.CreateBusinessObject(CLASSID_SALETOTALS)
        Call moSaleTotals.AddLoadFilter(CMP_EQUAL, FID_SALETOTALS_SaleTotalKey, _
                                        moCashierTotals.SaleTotalsKey)
        moSaleTotals.LoadMatches
    End If 'only load if the cashier is not already loaded from previous Transaction

End Sub

Private Function GetValue(strData As String, enftDataType As enFmtType, Optional ByVal lngLength As Long = 1, Optional ByVal lngDecPlaces As Long = 0) As Variant

Dim strValue    As String 'used to hold value whilst it is being formatted
Dim curValue    As Currency
Dim dteNullDate As Date

    Select Case (enftDataType)
        Case (enftString), (enftCode):
                        GetValue = RTrim$(Left$(strData, lngLength))
                        strData = Mid$(strData, lngLength + 1)
        Case (enftNumber):
                        If (lngDecPlaces > 0) Then lngLength = lngLength + 1
                        strValue = Left$(strData, lngLength)
                        curValue = CCur(strValue)
                        strData = Mid$(strData, lngLength + 1)
                        If (Left$(strData, 1) = "-") Then curValue = curValue * -1
                        GetValue = curValue
                        strData = Mid$(strData, 2)
        Case (enftDate):
                        strValue = Left(strData, 8)
                        If (strValue = "--/--/--") Then
                            GetValue = dteNullDate
                        Else
                            GetValue = CDate(strValue)
                        End If
                        strData = Mid$(strData, 9)
        Case (enftYesNo):
                        strValue = Left(strData, 1)
                        If (strValue = "Y") Then
                            GetValue = CBool(True)
                        Else
                            GetValue = CBool(False)
                        End If
                        strData = Mid$(strData, 2)
    End Select
    
End Function
Private Function ProcessPOSHeader(ByRef oPOSHeader As cPOSHeader, strData As String, strFileName As String, tsImportFile As TextStream) As Boolean

Dim strTemp     As String
Dim strName     As String
Dim strSignCode As String
Dim blnDummy    As Boolean
Dim lngCounter  As Long
Dim blnFound    As Boolean
    
On Error GoTo Bad_Process

    strTemp = GetValue(strData, enftString, 8)
    oPOSHeader.TranDate = GetValue(strData, enftDate)
    'Move file to the Old File folder if Transaction Date is over a year old
    If (oPOSHeader.TranDate < DateAdd("yyyy", -1, Date)) Then
        lstFiles.AddItem (" - BackUp Old File - " & strFileName)
        lstFiles.ListIndex = lstFiles.ListCount - 1
        Call tsImportFile.Close
        ProcessPOSHeader = False
        If (moFSO.FileExists(mstrOldFilePath & Right$(strFileName, 10)) = True) Then moFSO.DeleteFile (mstrOldFilePath & Right$(strFileName, 10))
        Call moFSO.MoveFile(strFileName, mstrOldFilePath)
        Exit Function
    End If
    oPOSHeader.TillID = GetValue(strData, enftCode, 2)
'    oPOSHeader.TillID = Format(Time, "HH")
    oPOSHeader.TransactionNo = GetValue(strData, enftCode, 4)
    oPOSHeader.cashierId = GetValue(strData, enftCode, 3)
    oPOSHeader.TransactionTime = GetValue(strData, enftString, 6)
    oPOSHeader.SupervisorNo = GetValue(strData, enftCode, 3)
    oPOSHeader.TransactionCode = GetValue(strData, enftString, 2)
    oPOSHeader.OpenDrawerCode = GetValue(strData, enftCode, 5)
    oPOSHeader.ReasonCode = GetValue(strData, enftCode, 5)
    oPOSHeader.Description = GetValue(strData, enftString, 20)
    oPOSHeader.OrderNumber = GetValue(strData, enftCode, 6)
    oPOSHeader.AccountSale = GetValue(strData, enftYesNo)
    oPOSHeader.Voided = GetValue(strData, enftYesNo)
    oPOSHeader.VoidSupervisor = GetValue(strData, enftCode, 3)
    oPOSHeader.TrainingMode = GetValue(strData, enftYesNo)
    oPOSHeader.DailyUpdateProc = GetValue(strData, enftYesNo)
    oPOSHeader.ExternalDocumentNo = GetValue(strData, enftCode, 8)
    oPOSHeader.SupervisorUsed = GetValue(strData, enftYesNo)
    oPOSHeader.StoreNumber = GetValue(strData, enftCode, 3)
    oPOSHeader.MerchandiseAmount = GetValue(strData, enftNumber, 8, 2)
    oPOSHeader.NonMerchandiseAmount = GetValue(strData, enftNumber, 8, 2)
    oPOSHeader.TaxAmount = GetValue(strData, enftNumber, 8, 2)
    oPOSHeader.DiscountAmount = GetValue(strData, enftNumber, 8, 2)
    oPOSHeader.DiscountSupervisor = GetValue(strData, enftCode, 3)
    oPOSHeader.TotalSaleAmount = GetValue(strData, enftNumber, 8, 2)
    oPOSHeader.CustomerAcctCardNo = GetValue(strData, enftCode, 6)
    oPOSHeader.CustomerAcctNo = GetValue(strData, enftCode, 2)
    oPOSHeader.CollectPostCode = GetValue(strData, enftYesNo)
    oPOSHeader.FromDCOrders = GetValue(strData, enftYesNo)
    oPOSHeader.TransactionComplete = GetValue(strData, enftYesNo)
    oPOSHeader.EmployeeDiscountOnly = GetValue(strData, enftYesNo)
    oPOSHeader.RefundCashier = GetValue(strData, enftCode, 3)
    oPOSHeader.RefundSupervisor = GetValue(strData, enftCode, 3)
    For lngCounter = 1 To 9 Step 1
        oPOSHeader.VATRates(lngCounter) = GetValue(strData, enftNumber, 5, 3)
    Next
    For lngCounter = 1 To 9 Step 1
        oPOSHeader.VATSymbol(lngCounter) = GetValue(strData, enftString, 1)
    Next
    For lngCounter = 1 To 9 Step 1
        oPOSHeader.ExVATValue(lngCounter) = GetValue(strData, enftNumber, 8, 2)
    Next
    For lngCounter = 1 To 9 Step 1
        oPOSHeader.VATValue(lngCounter) = GetValue(strData, enftNumber, 8, 2)
    Next
    oPOSHeader.TranParked = GetValue(strData, enftYesNo)
    oPOSHeader.RefundManager = GetValue(strData, enftCode, 3)
    oPOSHeader.TenderOverrideCode = GetValue(strData, enftCode, 2)
    oPOSHeader.RecoveredFromParked = GetValue(strData, enftYesNo)
    oPOSHeader.Offline = GetValue(strData, enftYesNo)
    strName = GetValue(strData, enftString, 22)
    strSignCode = GetValue(strData, enftCode, 5)
    blnDummy = GetValue(strData, enftYesNo)
    oPOSHeader.GiftTokensPrinted = GetValue(strData, enftNumber, 2)
    oPOSHeader.ColleagueCardNo = GetValue(strData, enftCode, 9)
    strTemp = GetValue(strData, enftCode, 2)  'extract and discard Save Status
    strTemp = GetValue(strData, enftCode, 4)  'extract and discard Save Sequence
    strTemp = GetValue(strData, enftString, 8) 'extract and discard CBBU Update flag
    oPOSHeader.DiscountCardNumber = GetValue(strData, enftCode, 19) 'Get Discount Card Number
    
    'Added 29/12/05 -M Milne.  After Reading Header Line - check it does not already exist
    'If (TransactionIDUnique(oPOSHeader.TranDate, oPOSHeader.TillID, oPOSHeader.TransactionNo) = False) Then
    '    Call MsgBoxEx("Transaction " & oPOSHeader.TillID & oPOSHeader.TransactionNo & " on " & Format(oPOSHeader.TranDate, "DD/MM/YY") & _
    '        " already exists on the Server Database." & vbCrLf & "Contact CTS to resolve situation.", vbInformation, "Update Offline Transactions Failed", , , , , vbRed)
    '    Call Err.Raise(1, "ProcessPOSHeader", "Duplicate Transaction IDs on Database Server in File-" & strFileName)
    '    Exit Function
    'End If
    
    
    lngCounter = 0
    blnFound = True
    Do
        oPOSHeader.TransactionNo = Format(Time, "HHNNSS") & Format(lngCounter, "00")
        blnFound = TransactionIDUnique(oPOSHeader.TranDate, oPOSHeader.TillID, oPOSHeader.TransactionNo)
        lngCounter = lngCounter + 1
        Call DebugMsg(MODULE_NAME, "ProcessPOSHeader", endlDebug, "Duplicate Transaction IDs on Database Server in File-" & strFileName)
    Loop While blnFound = False
    ProcessPOSHeader = True
    
    Exit Function
    
Bad_Process:

    Call Err.Raise(1, "ProcessPOSHeader", "Error in File-" & strFileName & "(" & Err.Number & "/" & Err.Description & ")")
    
End Function


Private Function ProcessPOSLine(ByRef oPOSHeader As cPOSHeader, strData As String, strFileName As String) As Boolean

Dim oPOSLine    As cPOSLine
Dim strOut      As String
Dim strTillID   As String
Dim dteTranDate As Date
Dim strTranNo   As String
Dim strSeqNo    As String
Dim strPartCode As String
Dim strTemp     As String

On Error GoTo Bad_Process
    
    strTemp = GetValue(strData, enftString, 8)
    dteTranDate = GetValue(strData, enftDate)
    strTillID = GetValue(strData, enftCode, 2)
    strTranNo = GetValue(strData, enftCode, 4)
    strSeqNo = GetValue(strData, enftCode, 5)
    strPartCode = GetValue(strData, enftCode, 6)
    Set oPOSLine = oPOSHeader.AddItem(strPartCode)
    oPOSLine.DepartmentNumber = GetValue(strData, enftCode, 2)
    oPOSLine.BarcodeEntry = GetValue(strData, enftYesNo)
    oPOSLine.SupervisorNumber = GetValue(strData, enftCode, 3)
    oPOSLine.QuantitySold = GetValue(strData, enftNumber, 6)
    oPOSLine.LookUpPrice = GetValue(strData, enftNumber, 8, 2)
    oPOSLine.ActualSellPrice = GetValue(strData, enftNumber, 8, 2)
    oPOSLine.ActualPriceExVAT = GetValue(strData, enftNumber, 8, 2)
    oPOSLine.ExtendedValue = GetValue(strData, enftNumber, 8, 2)
    oPOSLine.ExtendedCost = GetValue(strData, enftNumber, 9, 3)
    oPOSLine.RelatedItems = GetValue(strData, enftYesNo)
    oPOSLine.PriceOverrideReason = GetValue(strData, enftCode, 5)
    oPOSLine.ItemTagged = GetValue(strData, enftYesNo)
    oPOSLine.CatchAllItem = GetValue(strData, enftYesNo)
    oPOSLine.VATCode = GetValue(strData, enftString, 1)
    oPOSLine.TempPriceMarginAmount = GetValue(strData, enftNumber, 8, 2)
    oPOSLine.TempPriceMarginCode = GetValue(strData, enftCode, 6)
    oPOSLine.PriceOverrideAmount = GetValue(strData, enftNumber, 8, 2)
    oPOSLine.OverrideMarginCode = GetValue(strData, enftCode, 6)
    oPOSLine.QtyBreakMarginAmount = GetValue(strData, enftNumber, 8, 2)
    oPOSLine.QtyBreakMarginCode = GetValue(strData, enftCode, 6)
    oPOSLine.DealGroupMarginAmount = GetValue(strData, enftNumber, 8, 2)
    oPOSLine.DealGroupMarginCode = GetValue(strData, enftCode, 6)
    oPOSLine.MultiBuyMarginAmount = GetValue(strData, enftNumber, 8, 2)
    oPOSLine.MultiBuyMarginCode = GetValue(strData, enftCode, 6)
    oPOSLine.HierarchyMarginAmount = GetValue(strData, enftNumber, 8, 2)
    oPOSLine.HierarchyMarginCode = GetValue(strData, enftCode, 6)
    oPOSLine.EmpSalePrimaryMarginAmount = GetValue(strData, enftNumber, 8, 2)
    oPOSLine.EmpSalePrimaryMarginCode = GetValue(strData, enftCode, 6)
    oPOSLine.LineReversed = GetValue(strData, enftYesNo)
    oPOSLine.LastEventSequenceNo = GetValue(strData, enftCode, 6)
    oPOSLine.HierCategory = GetValue(strData, enftCode, 6)
    oPOSLine.HierGroup = GetValue(strData, enftCode, 6)
    oPOSLine.HierSubGroup = GetValue(strData, enftCode, 6)
    oPOSLine.HierStyle = GetValue(strData, enftCode, 6)
    oPOSLine.QuarantineSupervisor = GetValue(strData, enftCode, 3)
    oPOSLine.EmpSaleSecondaryMarginAmount = GetValue(strData, enftNumber, 8, 2)
    oPOSLine.MarkedDownStock = GetValue(strData, enftYesNo)
    oPOSLine.SaleType = GetValue(strData, enftString, 1)
    oPOSLine.VATCodeNumber = GetValue(strData, enftNumber, 1)
    oPOSLine.VATAmount = GetValue(strData, enftNumber, 9, 2)
    oPOSLine.BackDoorCollect = GetValue(strData, enftYesNo)
    oPOSLine.BackDoorCollect = GetValue(strData, enftYesNo) 'added M.Milne 4/9/07 - BDCO is twice in file ??
    oPOSLine.ReversalCode = GetValue(strData, enftCode, 2)
    ProcessPOSLine = True
    
    Exit Function
    
Bad_Process:

    Call Err.Raise(1, "ProcessPOSLine", "Error in File-" & strFileName & "(" & Err.Number & "/" & Err.Description & ")")
    
End Function


Private Function ProcessEANCheck(ByRef oPOSHeader As cPOSHeader, strData As String, strFileName As String, blnSaveNow As Boolean) As Boolean

Dim oEANCheck   As cEANCheck
Dim strOut      As String
Dim strTillID   As String
Dim dteTranDate As Date
Dim strTranNo   As String
Dim strSeqNo    As String
Dim strPartCode As String
Dim strTemp     As String

On Error GoTo Bad_Process
    
    If (blnSaveNow = False) Then
        Set oEANCheck = oPOSHeader.AddKeyedAudit("", "", "")
    Else
        Set oEANCheck = goDatabase.CreateBusinessObject(CLASSID_EAN_CHECK)
    End If
    strTemp = GetValue(strData, enftString, 8)
    oEANCheck.TranDate = GetValue(strData, enftDate)
    oEANCheck.TranTillID = GetValue(strData, enftCode, 2)
    oEANCheck.TranNo = GetValue(strData, enftCode, 4)
    oEANCheck.SequenceNo = GetValue(strData, enftCode, 5)
    oEANCheck.TranTime = GetValue(strData, enftCode, 4)
    oEANCheck.EntryMethod = GetValue(strData, enftString, 10)
    oEANCheck.ReasonCode = GetValue(strData, enftCode, 2)
    oEANCheck.Reason = GetValue(strData, enftString, 35)
    oEANCheck.EnteredValue = GetValue(strData, enftCode, 16)
    oEANCheck.ItemType = GetValue(strData, enftString, 10)
    oEANCheck.UserID = GetValue(strData, enftCode, 3)
    oEANCheck.EANFound = GetValue(strData, enftYesNo, 1)
    oEANCheck.EANSKU = GetValue(strData, enftCode, 6)
    oEANCheck.EANSKUFound = GetValue(strData, enftYesNo, 1)
    oEANCheck.CorrectSKU = GetValue(strData, enftCode, 6)
    
    If (blnSaveNow = True) Then Call oEANCheck.SaveIfNew
    
    ProcessEANCheck = True
    
    Exit Function
    
Bad_Process:

    Call Err.Raise(1, "ProcessEANCheck", "Error in File-" & strFileName & "(" & Err.Number & "/" & Err.Description & ")")
    
End Function


Private Function ProcessSKUReject(ByRef oPOSHeader As cPOSHeader, strData As String, strFileName As String, colSKURejects As Collection) As Boolean

Dim oItemRejectBO   As cSKUReject

Dim strOut      As String
Dim strTemp     As String

On Error GoTo Bad_Process
    
    Set oItemRejectBO = goDatabase.CreateBusinessObject(CLASSID_TRANREJECT)
    strTemp = GetValue(strData, enftString, 8)
    oItemRejectBO.TranDate = GetValue(strData, enftDate)
    oItemRejectBO.TranTillID = GetValue(strData, enftCode, 2)
    oItemRejectBO.TranNo = GetValue(strData, enftCode, 4)
    oItemRejectBO.SequenceNo = GetValue(strData, enftCode, 5)
    oItemRejectBO.UserID = GetValue(strData, enftCode, 3)
    oItemRejectBO.partCode = GetValue(strData, enftCode, 6)
    oItemRejectBO.RejectText = Replace(GetValue(strData, enftCode, 82), "''", vbCrLf)
    oItemRejectBO.GroupID = GetValue(strData, enftCode, 6)
    oItemRejectBO.PromptID = GetValue(strData, enftString, 8)
    Call colSKURejects.Add(oItemRejectBO)
    On Error GoTo 0
    
    ProcessSKUReject = True
    
    Exit Function
    
Bad_Process:

    Call Err.Raise(1, "ProcessSKUReject", "Error in File-" & strFileName & "(" & Err.Number & "/" & Err.Description & ")")
    
End Function


Private Function ProcessPOSPmt(ByRef oPOSHeader As cPOSHeader, strData As String, strFileName As String) As Boolean

Dim oPOSPaid    As cPOSPayment
Dim strTemp     As String
Dim strTillID   As String
Dim dteTranDate As Date
Dim strTranNo   As String
Dim strSeqNo    As String
Dim lngTendType As Long
Dim dblTendAmt  As Double

On Error GoTo Bad_Process
    
    strTemp = GetValue(strData, enftString, 8)
    dteTranDate = GetValue(strData, enftDate)
    strTillID = GetValue(strData, enftCode, 2)
    strTranNo = GetValue(strData, enftCode, 4)
    strSeqNo = GetValue(strData, enftCode, 5)
    lngTendType = GetValue(strData, enftNumber, 2)
    dblTendAmt = GetValue(strData, enftNumber, 8, 2)
    Set oPOSPaid = oPOSHeader.AddPayment(lngTendType, dblTendAmt)
    oPOSPaid.CreditCardNumber = GetValue(strData, enftCode, 19)
    oPOSPaid.CreditCardExpiryDate = GetValue(strData, enftCode, 4)
    oPOSPaid.CouponNumber = GetValue(strData, enftCode, 6)
    oPOSPaid.CouponClass = GetValue(strData, enftCode, 2)
    oPOSPaid.AuthorisationCode = GetValue(strData, enftString, 9)
    oPOSPaid.CCNumberKeyed = GetValue(strData, enftYesNo)
    oPOSPaid.SupervisorCode = GetValue(strData, enftCode, 3)
    oPOSPaid.CouponPostCode = GetValue(strData, enftString, 8)
    oPOSPaid.ChequeAccountNumber = GetValue(strData, enftCode, 10)
    oPOSPaid.ChequeSortCode = GetValue(strData, enftCode, 6)
    oPOSPaid.ChequeNumber = GetValue(strData, enftCode, 6)
    oPOSPaid.CashBalanceProcessed = GetValue(strData, enftYesNo)
    oPOSPaid.EFTPOSVoucherNo = GetValue(strData, enftCode, 4)
    oPOSPaid.IssuerNumber = GetValue(strData, enftString, 2)
    oPOSPaid.AuthorisationType = GetValue(strData, enftString, 1)
    oPOSPaid.EFTPOSMerchantNo = GetValue(strData, enftString, 15)
    oPOSPaid.CashBackAmount = GetValue(strData, enftNumber, 8, 2)
    oPOSPaid.DigitCount = GetValue(strData, enftCode, 5)
    oPOSPaid.EFTPOSCommsDone = GetValue(strData, enftYesNo)
    oPOSPaid.EFTConfirmID = GetValue(strData, enftString, 6)
'    oPOSPaid.EFTConfirmed = GetValue(strData, enftString, 1)
    ProcessPOSPmt = True
    
    Exit Function
    
Bad_Process:

    Call Err.Raise(1, "ProcessPOSPmt", "Error in File-" & strFileName & "(" & Err.Number & "/" & Err.Description & ")")
    
End Function

Public Function ProcessEventLine(ByRef oPOSHeader As cPOSHeader, strData As String, strFileName As String) As Boolean

Dim oEvtLine        As cEventTranLine
Dim strTillID       As String
Dim dteTranDate     As Date
Dim strTranNo       As String
Dim strTranLineNo   As String
Dim strTemp     As String

On Error GoTo Bad_Process
    
    strTemp = GetValue(strData, enftString, 8)
    dteTranDate = GetValue(strData, enftDate)
    strTillID = GetValue(strData, enftCode, 2)
    strTranNo = GetValue(strData, enftCode, 4)
    strTranLineNo = GetValue(strData, enftCode, 5)
    Set oEvtLine = oPOSHeader.AddEventItem(Val(strTranLineNo))
    oEvtLine.EventSeqNo = GetValue(strData, enftCode, 6)
    oEvtLine.EventType = GetValue(strData, enftString, 2)
    oEvtLine.DiscountAmount = GetValue(strData, enftNumber, 8, 2)
    ProcessEventLine = True
    
    Exit Function
    
Bad_Process:

    Call Err.Raise(1, "ProcessEventLine", "Error in File-" & strFileName & "(" & Err.Number & "/" & Err.Description & ")")
    
End Function

Private Function ProcessRetCust(ByRef oPOSHeader As cPOSHeader, strData As String, ByRef oRetCustBO As cReturnCust, strFileName As String) As Boolean

Dim strTemp     As String

On Error GoTo Bad_Process
    
    strTemp = GetValue(strData, enftString, 8)
    Set oRetCustBO = goDatabase.CreateBusinessObject(CLASSID_RETURNCUST)
    oRetCustBO.TransactionDate = GetValue(strData, enftDate)
    oRetCustBO.PCTillID = GetValue(strData, enftCode, 2)
    oRetCustBO.TransactionNumber = GetValue(strData, enftCode, 4)
    oRetCustBO.CustomerName = GetValue(strData, enftString, 30)
    strTemp = GetValue(strData, enftString, 4) 'discard house number
    oRetCustBO.PostCode = GetValue(strData, enftString, 8)
    oRetCustBO.OrigTranDate = GetValue(strData, enftDate)
    oRetCustBO.OrigTranTill = GetValue(strData, enftCode, 2)
    oRetCustBO.OrigTranNumb = GetValue(strData, enftCode, 4)
    oRetCustBO.HouseName = GetValue(strData, enftString, 15)
    oRetCustBO.AddressLine1 = GetValue(strData, enftString, 30)
    oRetCustBO.AddressLine2 = GetValue(strData, enftString, 30)
    oRetCustBO.AddressLine3 = GetValue(strData, enftString, 30)
    oRetCustBO.PhoneNo = GetValue(strData, enftString, 15)
    oRetCustBO.LineNumber = GetValue(strData, enftCode, 5)
    oRetCustBO.OrigTranValidated = GetValue(strData, enftYesNo)
    oRetCustBO.OrigTenderType = GetValue(strData, enftNumber, 2)
    oRetCustBO.RefundReason = GetValue(strData, enftCode, 2)
    oRetCustBO.LabelsRequired = GetValue(strData, enftYesNo)
    ProcessRetCust = True
    
    Exit Function
    
Bad_Process:

    Call Err.Raise(1, "ProcessRetCust", "Error in File-" & strFileName & "(" & Err.Number & "/" & Err.Description & ")")
    
End Function

Private Function ProcessEANs(ByRef oPOSHeader As cPOSHeader, strData As String, strFileName As String) As Boolean

Dim strTillID   As String
Dim dteTranDate As Date
Dim strTranNo   As String
Dim strSeqNo    As String
Dim strLineNo   As String
Dim strEANNo    As String
Dim strTemp     As String

On Error GoTo Bad_Process
    
    strTemp = GetValue(strData, enftString, 8)
    dteTranDate = GetValue(strData, enftDate)
    strTillID = GetValue(strData, enftCode, 2)
    strTranNo = GetValue(strData, enftCode, 4)
    strSeqNo = GetValue(strData, enftCode, 5)
    strLineNo = GetValue(strData, enftCode, 4)
    strEANNo = GetValue(strData, enftCode, 16)
    ProcessEANs = True
    
    Exit Function
    
Bad_Process:

    Call Err.Raise(1, "ProcessEANs", "Error in File-" & strFileName & "(" & Err.Number & "/" & Err.Description & ")")
    
End Function


Private Function ProcessGiftVouchers(ByRef oPOSHeader As cPOSHeader, strData As String, strFileName As String) As Boolean

Dim strTemp         As String
Dim oGiftVoucher    As cPOSGiftVoucher
Dim strTillID       As String
Dim dteTranDate     As Date
Dim strTranNo       As String
Dim strSeqNo        As String
Dim strSerialNo     As String
Dim strCashierID    As String
Dim strTranCode     As String
Dim curVoucherAmt   As Currency

On Error GoTo Bad_Process
    
    strTemp = GetValue(strData, enftString, 8)
    dteTranDate = GetValue(strData, enftDate)
    strTillID = GetValue(strData, enftCode, 2)
    strTranNo = GetValue(strData, enftCode, 4)
    strSeqNo = GetValue(strData, enftCode, 5)
    strSerialNo = GetValue(strData, enftCode, 8)
    strCashierID = GetValue(strData, enftCode, 3)
    strTranCode = GetValue(strData, enftString, 2)
    curVoucherAmt = GetValue(strData, enftNumber, 8, 2)
    Set oGiftVoucher = oPOSHeader.AddGiftVoucher(strSerialNo, curVoucherAmt, strTranCode)
    ProcessGiftVouchers = True
    
    Exit Function
    
Bad_Process:

    Call Err.Raise(1, "ProcessGiftVouchers", "Error in File-" & strFileName & "(" & Err.Number & "/" & Err.Description & ")")
    
End Function

Private Function ProcessHotVouchers(ByRef oPOSHeader As cPOSHeader, strData As String, ByRef cHotVoucher As cGiftVoucherHotFile, strFileName As String) As Boolean

Dim strTemp         As String
    
On Error GoTo Bad_Process
    
    strTemp = GetValue(strData, enftString, 8)
    Set cHotVoucher = goDatabase.CreateBusinessObject(CLASSID_GIFTVOUCHERHOTFILE)
    cHotVoucher.SerialNumber = GetValue(strData, enftCode, 8)
    cHotVoucher.RedemptionInfo = GetValue(strData, enftString, 40)
    cHotVoucher.HeadOfficeInfo = GetValue(strData, enftString, 40)
    cHotVoucher.Commed = GetValue(strData, enftYesNo)
    ProcessHotVouchers = True
    
    Exit Function
    
Bad_Process:

    Call Err.Raise(1, "ProcessHotVouchers", "Error in File-" & strFileName & "(" & Err.Number & "/" & Err.Description & ")")
    
End Function


Private Function ProcessPriceOverLines(ByRef oPOSHeader As cPOSHeader, ByVal strData As String, strFileName As String) As Boolean

Dim strTemp     As String
Dim strName     As String
Dim cPOLine     As cPriceLineInfo
    
On Error GoTo Bad_Process

    strTemp = GetValue(strData, enftString, 8)
    Set cPOLine = oPOSHeader.AddPricePromiseLine(0)
    cPOLine.TranDate = GetValue(strData, enftDate)
    cPOLine.TillID = GetValue(strData, enftCode, 2)
    cPOLine.TransactionNo = GetValue(strData, enftCode, 4)
    cPOLine.SequenceNo = GetValue(strData, enftCode, 5)
    cPOLine.CompetitorName = GetValue(strData, enftString, 30)
    cPOLine.CompetitorAddress1 = GetValue(strData, enftString, 30)
    cPOLine.CompetitorAddress2 = GetValue(strData, enftString, 30)
    cPOLine.CompetitorAddress3 = GetValue(strData, enftString, 30)
    cPOLine.CompetitorPostcode = GetValue(strData, enftString, 8)
    cPOLine.CompetitorPhone = GetValue(strData, enftString, 15)
    cPOLine.CompetitorFax = GetValue(strData, enftString, 15)
    cPOLine.EnteredPrice = GetValue(strData, enftNumber, 7, 2)
    cPOLine.IncludeVAT = GetValue(strData, enftYesNo)
    cPOLine.ConvertedPrice = GetValue(strData, enftNumber, 7, 2)
    cPOLine.Previous = GetValue(strData, enftYesNo)
    cPOLine.OrigStoreNumber = GetValue(strData, enftCode, 3)
    cPOLine.OrigTillNumber = GetValue(strData, enftCode, 2)
    cPOLine.OrigTransactionNumber = GetValue(strData, enftCode, 4)
    cPOLine.OrigTransDate = GetValue(strData, enftDate)
    cPOLine.OrigSellingPrice = GetValue(strData, enftNumber, 7, 2)
    
    ProcessPriceOverLines = True
    
    Exit Function
    
Bad_Process:

    Call Err.Raise(1, "ProcessPriceOverLines", "Error in File-" & strFileName & "(" & Err.Number & "/" & Err.Description & ")")
    
End Function


Private Function ProcessOverrideCust(ByRef oPOSHeader As cPOSHeader, strData As String, strFileName As String) As Boolean

Dim strTemp     As String
Dim cPOCust     As cPriceCusInfo
    
On Error GoTo Bad_Process
        
    strTemp = GetValue(strData, enftString, 8)
    Set cPOCust = oPOSHeader.AddPricePromiseCust("")
    cPOCust.TranDate = GetValue(strData, enftDate)
    cPOCust.Till = GetValue(strData, enftCode, 2)
    cPOCust.Tran = GetValue(strData, enftCode, 4)
    cPOCust.Name = GetValue(strData, enftString, 30)
    cPOCust.Address1 = GetValue(strData, enftString, 30)
    cPOCust.Address2 = GetValue(strData, enftString, 30)
    cPOCust.Address3 = GetValue(strData, enftString, 30)
    cPOCust.PostCode = GetValue(strData, enftString, 8)
    cPOCust.Phone = GetValue(strData, enftString, 15)
    
    ProcessOverrideCust = True
    
    Exit Function
    
Bad_Process:

    Call Err.Raise(1, "ProcessOverrideCust", "Error in File-" & strFileName & "(" & Err.Number & "/" & Err.Description & ")")
    
End Function

Private Function TransactionIDUnique(dteTranDate As Date, strTillNo As String, strTranNo As String) As Boolean

Dim oTranHead As cPOSHeader

    Set oTranHead = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    Call oTranHead.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, dteTranDate)
    Call oTranHead.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, strTillNo)
    Call oTranHead.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, strTranNo)
    Call oTranHead.AddLoadField(FID_POSHEADER_TransactionTime)
    
    If oTranHead.LoadMatches.Count = 0 Then
        TransactionIDUnique = True
    Else
        TransactionIDUnique = False
    End If
    
End Function

Private Sub ZReadTranUpdate(strTillNo As String, strTranID As String, strTranCode As String, _
                        curTotalSaleAmount As Currency, curDiscountAmount As Currency, _
                        curMerchandiseAmount As Currency, curNonMerchandiseAmount As Currency, strCashierID As String)
    
Dim strQuery As String

    On Error GoTo ZRU_Error
        
    If strTranCode <> "OD" Then
        ' Update Cash
        strQuery = "Update ZREADS set COUN = COUN + 1, AMNT = AMNT + " & curTotalSaleAmount & " where WSID = '" & strTillNo & "' and TYPE = 'TE' and STYP = '01 '"
        Call DebugMsg(MODULE_NAME, "ZReadUpdate", endlDebug, strQuery)
        Call goDatabase.ExecuteCommand(strQuery)
        
        ' Update Discounts
        strQuery = "Update ZREADS set COUN = COUN + 1, AMNT = AMNT + " & curDiscountAmount & " where WSID = '" & strTillNo & "' and TYPE = 'RT' and STYP = 'DIS'"
        Call DebugMsg(MODULE_NAME, "ZReadUpdate", endlDebug, strQuery)
        Call goDatabase.ExecuteCommand(strQuery)
        
        ' Update Merchandise
        strQuery = "Update ZREADS set COUN = COUN + 1, AMNT = AMNT + " & curMerchandiseAmount & " where WSID = '" & strTillNo & "' and TYPE = 'RT' and STYP = 'MER'"
        Call DebugMsg(MODULE_NAME, "ZReadUpdate", endlDebug, strQuery)
        Call goDatabase.ExecuteCommand(strQuery)
        
        ' Update Non-Merchandise
        strQuery = "Update ZREADS set COUN = COUN + 1, AMNT = AMNT + " & curNonMerchandiseAmount & " where WSID = '" & strTillNo & "' and TYPE = 'RT' and STYP = 'NME'"
        Call DebugMsg(MODULE_NAME, "ZReadUpdate", endlDebug, strQuery)
        Call goDatabase.ExecuteCommand(strQuery)
    End If

    With moTillTotals
        .CurrentTransactionNumber = strTranID
        .TillCheckingEvents = False
        If (strTranCode <> "CO") And (strTranCode <> "CC") Then
            .TransactionCount = .TransactionCount + 17
            If strTranCode <> "OD" Then .TransactionValues = .TransactionValues + curTotalSaleAmount
        End If
        If (strTranCode = "CC") Then
            .PreviousCashierNumber = .CashierNumber
            .CashierNumber = "999"
            .TimeLastSignedOff = Format$(Now, "HHMM")
        End If
        .SaveIfExists
    End With
    
    If (mblnAccCashier = True) Then
        With moCashierTotals
            If strTranCode <> "CC" And strTranCode <> "CO" Then
                .TransactionCount = .TransactionCount + 1
            End If
            If (strTranCode = "CC") Then
                .CurrentTillNumber = "99"
            End If
            If strTranCode <> "OD" Then
                .TransactionValue = .TransactionValue + curTotalSaleAmount
            End If
            .SaveIfExists
        End With
    End If
    
    If strTranCode <> "OD" Then
        If strTranCode <> "ZR" Then
            strQuery = "Update ZREADS set COUN = COUN + 1, AMNT = AMNT + " & curNonMerchandiseAmount & " where WSID = '" & strTillNo & "' and TYPE = '" & strTranCode & "' and STYP = '   '"
            Call DebugMsg(MODULE_NAME, "ZReadUpdate", endlDebug, strQuery)
            Call goDatabase.ExecuteCommand(strQuery)
        End If
    
        strQuery = "Update ZREADS set COUN = COUN + 1, AMNT = AMNT + " & curTotalSaleAmount & " where WSID = '" & strTillNo & "' and TYPE = 'ZR' and STYP = '   '"
        Call DebugMsg(MODULE_NAME, "ZReadUpdate", endlDebug, strQuery)
        Call goDatabase.ExecuteCommand(strQuery)
    
        strQuery = "Update ZREADS set COUN = COUN + 1, AMNT = AMNT + " & curTotalSaleAmount & " where WSID = '" & strTillNo & "' and TYPE = 'CA' and STYP = '" & strCashierID & "'"
        Call DebugMsg(MODULE_NAME, "ZReadUpdate", endlDebug, strQuery)
        Call goDatabase.ExecuteCommand(strQuery)
    End If
    Exit Sub

ZRU_Error:
    Call Err.Bubble(MODULE_NAME, "ZReadUpdate", CLng(Erl))
    
End Sub

Private Sub ZReadTenderUpdate(strTillNo As String, strTendType As String, curTenderAmount As Currency)
    
Dim strQuery As String

    On Error GoTo ZRU_Error
      
    strQuery = "Update ZREADS set AMNT = AMNT + " & curTenderAmount & " where WSID = '" & _
            strTillNo & "' and TYPE = 'TE' and STYP = '01 '"
    Call DebugMsg(MODULE_NAME, "ZReadUpdate", endlDebug, strQuery)
    Call goDatabase.ExecuteCommand(strQuery)
    strQuery = "Update ZREADS set COUN = COUN + 1, AMNT = AMNT - " & curTenderAmount & " where WSID = '" & _
            strTillNo & "' and TYPE = 'TE' and STYP = '" & Left$(strTendType & "   ", 3) & "'"
    Call DebugMsg(MODULE_NAME, "ZReadUpdate", endlDebug, strQuery)
    Call goDatabase.ExecuteCommand(strQuery)
    
    Exit Sub

ZRU_Error:
    Call Err.Bubble(MODULE_NAME, "ZReadUpdate", CLng(Erl))
    
End Sub


Private Sub UpdateFlashTotals(blnIsTender As Boolean, strTranCode As String, lngTenderPos As Long, curValue As Currency)

Dim lngOccurance As enFlashTotalCategory

    If moSaleTotals Is Nothing Then Exit Sub
    
    With moSaleTotals
        If blnIsTender Then
            lngOccurance = lngTenderPos
            If (lngOccurance = 99) Then lngOccurance = 1
            .TenderTypesCount(lngOccurance) = .TenderTypesCount(lngOccurance) + 1
            .TenderTypesValue(lngOccurance) = .TenderTypesValue(lngOccurance) - curValue
        Else
            Select Case (strTranCode)
                Case (TT_MISCIN):       lngOccurance = enftMiscPaidIn
                Case (TT_OPENDRAWER):   lngOccurance = enftMiscOpenDrawer
                Case (TT_MISCOUT):      lngOccurance = enftMiscPaidOut
                Case (TT_REFUND):       lngOccurance = enftRefunds
                Case (TT_SALE):         lngOccurance = enftSales
                Case Else
            End Select
            .TransactionCount(lngOccurance) = .TransactionCount(lngOccurance) + 1
            .TranLastTime(lngOccurance) = Format$(Now, "HHNN")
            .TransactionValue(lngOccurance) = .TransactionValue(lngOccurance) + curValue
        End If
        
        .SaveIfExists
    End With
    
End Sub


