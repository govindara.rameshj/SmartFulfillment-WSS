VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Begin VB.Form frmWorkStation 
   BackColor       =   &H00E0E0E0&
   Caption         =   "Work Station Editor"
   ClientHeight    =   6090
   ClientLeft      =   2130
   ClientTop       =   1665
   ClientWidth     =   6585
   Icon            =   "frmWorkStation.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   6090
   ScaleWidth      =   6585
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10-Exit"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   5280
      Width           =   975
   End
   Begin VB.CheckBox chkSetSecurity 
      Caption         =   "&Set Security"
      Height          =   255
      Left            =   1200
      TabIndex        =   1
      Top             =   5280
      Width           =   1455
   End
   Begin CTSProgBar.ucpbProgressBar ucProgress 
      Height          =   1860
      Left            =   540
      TabIndex        =   5
      Top             =   1560
      Width           =   5100
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      FillColor       =   16744703
      Object.Width           =   5100
      Object.Height          =   1860
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "F5-Save"
      Height          =   375
      Left            =   5520
      TabIndex        =   4
      Top             =   5280
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton cmdAddLine 
      Caption         =   "Ctrl F6-Add"
      Height          =   375
      Left            =   2880
      TabIndex        =   2
      Top             =   5280
      Width           =   1215
   End
   Begin VB.CommandButton cmdDelLine 
      Caption         =   "F8-Del User"
      Height          =   375
      Left            =   4200
      TabIndex        =   3
      Top             =   5280
      Width           =   1215
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F9-Print"
      Height          =   375
      Left            =   5520
      TabIndex        =   6
      Top             =   5280
      Width           =   975
   End
   Begin FPSpreadADO.fpSpread sprdEdit 
      Height          =   5055
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   6375
      _Version        =   393216
      _ExtentX        =   11245
      _ExtentY        =   8916
      _StockProps     =   64
      ColsFrozen      =   3
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   5
      MaxRows         =   1
      RowHeaderDisplay=   0
      SpreadDesigner  =   "frmWorkStation.frx":058A
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   8
      Top             =   5715
      Width           =   6585
      _ExtentX        =   11615
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmWorkStation.frx":0A18
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   3784
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "12:10"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmWorkStation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module :
'* Date   : 16/01/03
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Workstation Editor/frmWorkStation.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 2/27/03 2:05p $ $Revision: 4 $
'* Versions:
'* 19/09/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmWorkStation"

Const EDIT_NONE As Long = 0
Const EDIT_DEL As Long = 1
Const EDIT_NEW As Long = 2
Const EDIT_UPD As Long = 3

Const EDIT_COL_POS As Long = 1
Const COL_WSID As Long = 2
Const COL_DESC As Long = 3
Const COL_OUTLET As Long = 4
Const COL_ACTIVE As Long = 5
Const COL_TENDER As Long = 6
Const COL_GROUP As Long = 26

Const BORDER_WIDTH As Long = 60

Dim colEntries As Collection

Dim mdblTenderRowHeight As Double
Dim mdblMenuRowHeight   As Double
Dim blnLoading          As Boolean
Dim mblnIgnoreSet       As Boolean

Private Sub GetGroups()

Dim colGroups As Collection
Dim oGroup    As cMenuItem
Dim lngMaxWidth As Long

    Set oGroup = goDatabase.CreateBusinessObject(CLASSID_MENU)
    
    Call oGroup.AddLoadFilter(CMP_EQUAL, FID_MENU_MENUTYPE, 1)
    Call oGroup.AddLoadFilter(CMP_GREATERTHAN, FID_MENU_GROUPID, 0)
    
    Set colGroups = oGroup.LoadMatches
    
    sprdEdit.MaxCols = sprdEdit.MaxCols + 99
    For Each oGroup In colGroups
        'Add menu option
        sprdEdit.Col = oGroup.GroupID + COL_GROUP
        sprdEdit.Row = 1
        sprdEdit.Text = oGroup.MenuText
        If lngMaxWidth < sprdEdit.MaxTextColWidth(sprdEdit.Col) Then lngMaxWidth = sprdEdit.MaxTextColWidth(sprdEdit.Col)
        sprdEdit.Row = 0
        sprdEdit.Text = oGroup.MenuText
        sprdEdit.TypeTextOrient = TypeTextOrientUp
        sprdEdit.ColWidth(sprdEdit.Col) = 4
        sprdEdit.Row = -1
        sprdEdit.CellType = CellTypeCheckBox
        sprdEdit.TypeCheckCenter = True
        sprdEdit.BackColor = RGB_GREY
    Next oGroup

    sprdEdit.RowHeight(0) = lngMaxWidth * 6
    mdblMenuRowHeight = sprdEdit.RowHeight(0)

End Sub

Private Sub GetTenderTypes()

Dim oTenderOptions As cTenderOptions
Dim colTenderOpts  As Collection
Dim intColNo       As Integer
Dim lngMaxWidth As Long

    Set oTenderOptions = goDatabase.CreateBusinessObject(CLASSID_TENDEROPTIONS)
    Set colTenderOpts = oTenderOptions.LoadMatches
    If colTenderOpts.Count = 0 Then Exit Sub
    
    sprdEdit.MaxCols = sprdEdit.MaxCols + colTenderOpts.Count
    For intColNo = 1 To colTenderOpts.Count
        Set oTenderOptions = colTenderOpts(intColNo)
        'Add menu option
        sprdEdit.Col = intColNo + COL_TENDER - 1
        sprdEdit.Row = 1
        sprdEdit.Text = oTenderOptions.Description
        If lngMaxWidth < sprdEdit.MaxTextColWidth(sprdEdit.Col) Then lngMaxWidth = sprdEdit.MaxTextColWidth(sprdEdit.Col)
        sprdEdit.Row = 0
        sprdEdit.Text = oTenderOptions.Description
        sprdEdit.TypeTextOrient = TypeTextOrientUp
        sprdEdit.ColWidth(sprdEdit.Col) = 4
        sprdEdit.Row = -1
        sprdEdit.CellType = CellTypeCheckBox
        sprdEdit.TypeCheckCenter = True
        sprdEdit.BackColor = RGB_GREY
    Next intColNo

    sprdEdit.RowHeight(0) = lngMaxWidth * 7
    mdblTenderRowHeight = sprdEdit.RowHeight(0)

End Sub

Private Sub chkSetSecurity_Click()

Dim lngColNo As Long
Dim blnHide  As Boolean

    If mblnIgnoreSet = True Then Exit Sub 'override functionality as resetting value
    
    If cmdSave.Visible = True Then
        If MsgBox("Unable to switch between User and Security mode whilst changes pending" & vbCrLf & "Discard changes", vbYesNoCancel, "Switch edit mode") <> vbYes Then
            mblnIgnoreSet = True
            chkSetSecurity.Value = Abs(chkSetSecurity.Value - 1)
            mblnIgnoreSet = False
            Exit Sub
        End If
        cmdSave.Visible = False
        Call DiscardChanges
    End If
    mblnIgnoreSet = False
    sprdEdit.ReDraw = False
    If chkSetSecurity.Value = 0 Then
        blnHide = False
        sprdEdit.RowHeight(0) = mdblTenderRowHeight
    Else
        blnHide = True
        sprdEdit.RowHeight(0) = mdblMenuRowHeight
    End If
    'only allow full name to be edited when editing User details
    sprdEdit.Col = COL_DESC
    sprdEdit.Row = -1
    sprdEdit.Lock = blnHide
    'Step through rows and hide/show as required
    sprdEdit.Row = 0
    For lngColNo = COL_OUTLET To sprdEdit.MaxCols Step 1
        sprdEdit.Col = lngColNo
        If lngColNo < COL_GROUP Then
            If LenB(sprdEdit.Text) = 0 Then
                sprdEdit.ColHidden = True
            Else
                sprdEdit.ColHidden = blnHide
            End If
        Else
            If LenB(sprdEdit.Text) = 0 Then
                sprdEdit.ColHidden = True
            Else
                sprdEdit.ColHidden = Not blnHide
            End If
        End If
    Next lngColNo
    'can only add/delete line when editing user details
    cmdAddLine.Visible = Not blnHide
    cmdDelLine.Visible = Not blnHide
    
    sprdEdit.SetFocus
    sprdEdit.ReDraw = True
    DoEvents
    sprdEdit.LeftCol = 4
    
End Sub

Private Sub cmdAddLine_Click()

    sprdEdit.MaxRows = sprdEdit.MaxRows + 1
    sprdEdit.Row = sprdEdit.MaxRows
    sprdEdit.Col = EDIT_COL_POS
    sprdEdit.Text = EDIT_NEW
    sprdEdit.Col = COL_WSID
    sprdEdit.Text = "TBA"
    sprdEdit.Col = COL_ACTIVE
    sprdEdit.Value = 1
    'Add default new row details HERE
    sprdEdit.SetFocus
    sprdEdit.Col = COL_DESC
    sprdEdit.LeftCol = sprdEdit.ColsFrozen + 1
    Call sprdEdit.SetActiveCell(COL_DESC, sprdEdit.Row)

End Sub

Private Sub cmdAddLine_GotFocus()

    cmdAddLine.FontBold = True

End Sub

Private Sub cmdAddLine_LostFocus()
    
    cmdAddLine.FontBold = False

End Sub

Private Sub cmdDelLine_Click()
    
    'Check if value selected for deletion
    If sprdEdit.SelBlockRow = -1 Then
        Call MsgBox("Select entry to delete by clicking in Left Column to highlight value", vbExclamation, "Delete Entry")
        Exit Sub
    End If
    
    'prompt user to confirm deletion of entry
    sprdEdit.Row = sprdEdit.SelBlockRow
    sprdEdit.Col = EDIT_COL_POS
    If Val(sprdEdit.Text) <> EDIT_NEW Then
        sprdEdit.Col = COL_DESC
        If MsgBox("Unable to delete existing User - '" & sprdEdit.Text & "'" & vbCrLf & "Mark user as not active", vbYesNo, "Confirm disable User") = vbNo Then Exit Sub
        sprdEdit.Col = COL_ACTIVE
        sprdEdit.Value = 0
        Exit Sub
    Else
        sprdEdit.Col = COL_DESC
        If MsgBox("Confirm delete entry - '" & sprdEdit.Text & "'", vbYesNo, "Confirm Deletion") = vbNo Then Exit Sub
    End If
    
    'Delete Row from screen
    Call sprdEdit.DeleteRows(sprdEdit.Row, 1)
    sprdEdit.MaxRows = sprdEdit.MaxRows - 1
    
End Sub

Private Sub cmdDelLine_GotFocus()
    
    cmdDelLine.FontBold = True

End Sub

Private Sub cmdDelLine_LostFocus()
    
    cmdDelLine.FontBold = False

End Sub

Private Sub cmdExit_Click()

    Call Unload(Me)

End Sub

Private Sub cmdExit_GotFocus()
    
    cmdExit.FontBold = True

End Sub

Private Sub cmdExit_LostFocus()
    
    cmdExit.FontBold = False

End Sub

Private Sub cmdPrint_Click()

    sprdEdit.PrintSmartPrint = True
    sprdEdit.PrintFooter = "/lPrinted - " & Format$(Now(), "DD/MM/YY HH:NN") & "/r /pn of /pc"
    If chkSetSecurity.Value = 0 Then
        sprdEdit.PrintHeader = "/c /fb1Work Station List"
        sprdEdit.PrintJobName = "Work Station List"
    Else
        sprdEdit.PrintHeader = "/c /fb1Work Station Security Rights List"
        sprdEdit.PrintJobName = "Work Station Security List"
    End If
    Call sprdEdit.PrintSheet

End Sub

Private Sub cmdPrint_GotFocus()
    
    cmdPrint.FontBold = True

End Sub

Private Sub cmdPrint_LostFocus()
    
    cmdPrint.FontBold = False

End Sub

Private Sub cmdSave_Click()

Dim lEditCode   As Long
Dim lEntryNo    As Long
Dim oWStation   As cWorkStation
Dim dteNullDate As Date
Dim strErrorMsg As String
    
    ucProgress.Visible = True
    ucProgress.Caption1 = "Saving changes"
    sbStatus.Panels(PANEL_INFO).Text = "Saving changes"
    sprdEdit.SetFocus
    DoEvents
    
    If chkSetSecurity.Value = 0 Then
        Call SaveUserUpdates
    Else
        Call SaveSecurityUpdates
    End If
    
    ucProgress.Visible = False
    cmdSave.Visible = False
    sbStatus.Panels(PANEL_INFO).Text = vbNullString
    Screen.MousePointer = vbNormal
    DoEvents

End Sub

Private Sub SaveUserUpdates()

Dim lEditCode   As Long
Dim lEntryNo    As Long
Dim oWStation   As cWorkStation
Dim dteNullDate As Date
Dim strErrorMsg As String
Dim lngColNo    As Long
    
    ucProgress.Visible = True
    ucProgress.Caption1 = "Saving changes"
    sbStatus.Panels(PANEL_INFO).Text = "Saving changes"
    sprdEdit.SetFocus
    DoEvents
    
    ucProgress.Caption1 = "Verifying entries"
    For lEntryNo = 1 To sprdEdit.MaxRows Step 1
        'move to row and extract if edited
        sprdEdit.Row = lEntryNo
        sprdEdit.Col = EDIT_COL_POS
        lEditCode = Val(sprdEdit.Text)
        strErrorMsg = vbNullString
        If lEditCode <> EDIT_NONE Then
            sprdEdit.Col = COL_DESC
            If LenB(sprdEdit.Text) = 0 Then strErrorMsg = strErrorMsg & "  - Missing work station description" & vbCrLf
            sprdEdit.Col = COL_OUTLET
            If LenB(sprdEdit.Text) = 0 Then strErrorMsg = strErrorMsg & "  - Missing outlet function" & vbCrLf
        End If
        If LenB(strErrorMsg) <> 0 Then
            Call MsgBox("Error detected on edited Work-station details :" & vbCrLf & strErrorMsg & " Rectify errors and re-save", vbExclamation, "Unable to complete save")
            Call sprdEdit.SetActiveCell(COL_DESC, lEntryNo)
            ucProgress.Visible = False
            Exit Sub
        End If
    Next lEntryNo
    
    ucProgress.Caption1 = "Saving changes"
    Screen.MousePointer = vbHourglass
    For lEntryNo = 1 To sprdEdit.MaxRows Step 1
        ucProgress.Value = lEntryNo
        'move to row and extract if edited
        sprdEdit.Row = lEntryNo
        sprdEdit.Col = EDIT_COL_POS
        lEditCode = Val(sprdEdit.Text)
        If lEditCode <> EDIT_NONE Then
            If lEditCode = EDIT_UPD Then
                Set oWStation = colEntries(CLng(lEntryNo))
                sprdEdit.Col = COL_WSID
                If sprdEdit.Text <> oWStation.Id Then
                    MsgBox ("Mismatch " & sprdEdit.Text & "-" & oWStation.Id)
                    Exit Sub
                End If
            Else
                Set oWStation = goDatabase.CreateBusinessObject(CLASSID_WORKSTATIONCONFIG)
                Call colEntries.Add(oWStation)
            End If
            
            'Update user object with values from spread
            sprdEdit.Col = COL_DESC
            oWStation.Description = sprdEdit.Text
            sprdEdit.Col = COL_OUTLET
            oWStation.OutletFunction = sprdEdit.Text
            sprdEdit.Col = COL_ACTIVE
            oWStation.Active = sprdEdit.Text
            For lngColNo = COL_TENDER To COL_TENDER + 8 Step 1
                sprdEdit.Col = lngColNo
                oWStation.TenderGroups(lngColNo - COL_TENDER + 1) = sprdEdit.Value
            Next lngColNo
            'process entry according to edit type
            Select Case (lEditCode)
                Case (EDIT_NEW): Call oWStation.IBo_SaveIfNew
                                 sprdEdit.Col = COL_WSID
                                 sprdEdit.Text = oWStation.Id
                Case (EDIT_UPD): Call oWStation.IBo_SaveIfExists
            End Select
        End If
        'reset edit flag for next action
        sprdEdit.Col = EDIT_COL_POS
        sprdEdit.Text = EDIT_NONE
    Next
    ucProgress.Visible = False
    cmdSave.Visible = False
    sbStatus.Panels(PANEL_INFO).Text = vbNullString
    Screen.MousePointer = vbNormal
    DoEvents

End Sub 'SaveUserUpdates

Private Sub SaveSecurityUpdates()

Dim lngEntryNo  As Long
Dim lngColNo    As Long
Dim oWStation   As cWorkStation
    
    ucProgress.Visible = True
    ucProgress.Caption1 = "Saving changes"
    sbStatus.Panels(PANEL_INFO).Text = "Saving changes"
    sprdEdit.SetFocus
    DoEvents
    
    ucProgress.Caption1 = "Saving changes"
    Screen.MousePointer = vbHourglass
    For lngEntryNo = 1 To sprdEdit.MaxRows Step 1
        ucProgress.Value = lngEntryNo
        'move to row and extract if edited
        sprdEdit.Row = lngEntryNo
        sprdEdit.Col = EDIT_COL_POS
        If Val(sprdEdit.Text) = EDIT_UPD Then
            Set oWStation = colEntries(CLng(lngEntryNo))
            sprdEdit.Col = COL_WSID
            If sprdEdit.Text <> oWStation.Id Then
                MsgBox ("Mismatch " & sprdEdit.Text & "-" & oWStation.Id)
                Exit Sub
            End If
            For lngColNo = COL_GROUP To sprdEdit.MaxCols Step 1
                sprdEdit.Col = lngColNo
                sprdEdit.Row = 0
                Debug.Print (lngColNo - COL_GROUP)
                If LenB(sprdEdit.Text) <> 0 Then 'Group is a valid group number
                    sprdEdit.Row = lngEntryNo
                    oWStation.GroupLevels(lngColNo - COL_GROUP) = sprdEdit.Value
                End If
            Next lngColNo
            Call oWStation.IBo_SaveIfExists
        End If
        'reset edit flag for next action
        sprdEdit.Col = EDIT_COL_POS
        sprdEdit.Text = EDIT_NONE
    Next
    ucProgress.Visible = False
    cmdSave.Visible = False
    sbStatus.Panels(PANEL_INFO).Text = vbNullString
    Screen.MousePointer = vbNormal
    DoEvents

End Sub 'SaveSecurityUpdates
Private Sub DiscardChanges()

Dim lEditCode   As Long
Dim lngEntryNo  As Long
Dim oWStation       As cWorkStation
    
    Screen.MousePointer = vbHourglass
    For lngEntryNo = sprdEdit.MaxRows To 1 Step -1
        'move to row and extract if edited
        sprdEdit.Row = lngEntryNo
        sprdEdit.Col = EDIT_COL_POS
        lEditCode = Val(sprdEdit.Text)
        If lEditCode <> EDIT_NONE Then
            If lEditCode = EDIT_NEW Then
                Call sprdEdit.DeleteRows(lngEntryNo, 1)
                sprdEdit.MaxRows = sprdEdit.MaxRows - 1
            Else
                sprdEdit.Col = COL_WSID
                Set oWStation = colEntries(CLng(lngEntryNo))
                If sprdEdit.Text <> oWStation.Id Then
                    MsgBox ("Mismatch " & sprdEdit.Text & "-" & oWStation.Id)
                    Exit Sub
                End If
                Call DisplayWorkStation(lngEntryNo, oWStation)
            End If
        End If
        'reset edit flag for next action
        sprdEdit.Col = EDIT_COL_POS
        sprdEdit.Text = EDIT_NONE
    Next
    cmdSave.Visible = False
    sbStatus.Panels(PANEL_INFO).Text = vbNullString
    Screen.MousePointer = vbNormal
    DoEvents
    
End Sub


Private Sub cmdSave_GotFocus()
    
    cmdSave.FontBold = True

End Sub

Private Sub cmdSave_LostFocus()
    
    cmdSave.FontBold = False

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case (Shift)
        Case (0):
            Select Case (KeyCode)
                Case (vbKeyF5): 'if F5 then call save
                    If cmdSave.Visible Then Call cmdSave_Click
                Case (vbKeyF8):  'if F8 then call delete line
                    If cmdDelLine.Visible = True Then Call cmdDelLine_Click
                Case (vbKeyF9):  'if F9 then call print
                    If cmdPrint.Visible = True Then Call cmdPrint_Click
                Case (vbKeyF10): 'if F10 then exit
                    Call cmdExit_Click
            End Select 'Key pressed with no Shift/Alt combination
        Case (2) 'CTRL Pressed
            Select Case (KeyCode)
                Case (vbKeyF6):  'if Alt - F6 then create new line
                    If cmdAddLine.Visible = True Then
                        Call cmdAddLine_Click
                        KeyCode = 0
                    End If
            End Select 'ALT pressed
    End Select

End Sub

Private Sub Form_Load()

Dim lngEntryNo  As Long
Dim oEntry      As cWorkStation

    blnLoading = True 'ignore any events
    ' Set up the object and field that we wish to select on
    Set goRoot = GetRoot
    'Display initial values in Status Bar
    Call InitialiseStatusBar(sbStatus)
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    ucProgress.FillColor = Me.BackColor
    mdblTenderRowHeight = sprdEdit.RowHeight(0)
    chkSetSecurity.BackColor = Me.BackColor
    
    Me.Show
    
    sbStatus.Panels(PANEL_INFO).Text = "Loading Editor"
    ucProgress.Visible = True
    ucProgress.Caption1 = "Loading List from Database"
    DoEvents
    
    Call GetTenderTypes
    Call GetGroups
    
    blnLoading = False 'ignore any events
    
    Call chkSetSecurity_Click
    'Create single element that List must be retrieved for
    Set oEntry = goSession.Root.CreateBusinessObject(CLASSID_WORKSTATIONCONFIG, goSession)
    
    'Make call on entries to return collection of all entries
    Set colEntries = oEntry.LoadMatches

    ucProgress.Caption1 = "Populating grid"
    ucProgress.Min = 1
    ucProgress.Max = colEntries.Count
    ucProgress.Value = 1
    sbStatus.Panels(PANEL_INFO).Text = "Populating grid"
    DoEvents
    sprdEdit.MaxRows = 0
    For lngEntryNo = 1 To colEntries.Count Step 1
        sprdEdit.MaxRows = sprdEdit.MaxRows + 1
        ucProgress.Value = sprdEdit.MaxRows
        Call DisplayWorkStation(sprdEdit.MaxRows, colEntries(lngEntryNo))
    Next
    ucProgress.Visible = False
    sbStatus.Panels(PANEL_INFO).Text = vbNullString
    cmdSave.Visible = False
    sprdEdit.LeftCol = 4
    
End Sub

Private Sub DisplayWorkStation(lngRowNo As Long, oWStation As cWorkStation)

Dim lngGroupNo As Long
    
    sprdEdit.Row = lngRowNo
    sprdEdit.Col = COL_WSID
    sprdEdit.Text = oWStation.Id
    sprdEdit.Col = COL_DESC
    sprdEdit.Text = oWStation.Description
    sprdEdit.Col = COL_OUTLET
    sprdEdit.Text = oWStation.OutletFunction
    sprdEdit.Col = COL_ACTIVE
    sprdEdit.Text = oWStation.Active
    For lngGroupNo = 1 To 9 Step 1
        sprdEdit.Col = COL_TENDER + lngGroupNo - 1
        If oWStation.TenderGroups(lngGroupNo) = True Then
            sprdEdit.Value = 1
        Else
            sprdEdit.Value = 0
        End If
    Next lngGroupNo

    For lngGroupNo = 1 To 99 Step 1
        sprdEdit.Col = COL_GROUP + lngGroupNo
        If oWStation.GroupLevels(lngGroupNo) = True Then
            sprdEdit.Value = 1
        Else
            sprdEdit.Value = 0
        End If
    Next lngGroupNo
    'reset edit flag that may have been automatically triggered
    sprdEdit.Col = EDIT_COL_POS
    sprdEdit.Text = EDIT_NONE

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

    sbStatus.Panels(PANEL_INFO).Text = "Confirm Exit"
    'on exit check if changes should be saved
    If cmdSave.Visible = True Then
        If MsgBox("Discard changes made to entries", vbYesNo, "Confirm Exit") = vbNo Then Cancel = True
    End If
    sbStatus.Panels(PANEL_INFO).Text = vbNullString

End Sub

Private Sub Form_Resize()

Dim lSpacing As Long

    If Me.WindowState = vbMinimized Then Exit Sub
    lSpacing = sprdEdit.Left
    
    'Check form is not below minimum width
    If Me.Width < cmdSave.Width + cmdAddLine.Width + cmdDelLine.Width + (lSpacing * 4) Then
        Me.Width = cmdSave.Width + cmdAddLine.Width + cmdDelLine.Width + (lSpacing * 4)
        Exit Sub
    End If
    'Check form is not below minimum height
    If Me.Height < sprdEdit.Top + cmdSave.Height * 3 + sbStatus.Height Then
        Me.Height = sprdEdit.Top + cmdSave.Height * 3 + sbStatus.Height
        Exit Sub
    End If
    
    'relocate Progress bar
    If (Me.Width - ucProgress.Width) > 0 Then
        ucProgress.Left = (Me.Width - ucProgress.Width) / 2
    Else
        ucProgress.Left = 0
    End If
    If (Me.Height - ucProgress.Height) > 0 Then
        ucProgress.Top = (Me.Height - ucProgress.Height) / 2
    Else
        ucProgress.Top = 0
    End If
    
    
    'start resizing
    sprdEdit.Width = Me.Width - sprdEdit.Left * 2 - BORDER_WIDTH * 2
    sprdEdit.Height = Me.Height - (cmdSave.Height + sprdEdit.Top + (lSpacing * 3) + (BORDER_WIDTH * 4) + sbStatus.Height)
    
    cmdSave.Top = sprdEdit.Top + sprdEdit.Height + lSpacing
    cmdAddLine.Top = cmdSave.Top
    cmdDelLine.Top = cmdSave.Top
    cmdPrint.Top = cmdSave.Top
    cmdExit.Top = cmdSave.Top
    
    cmdPrint.Left = (sprdEdit.Width - cmdPrint.Width) + sprdEdit.Left
    cmdSave.Left = cmdPrint.Left
    cmdDelLine.Left = cmdPrint.Left - lSpacing - cmdDelLine.Width
    cmdAddLine.Left = cmdDelLine.Left - lSpacing - cmdAddLine.Width
    chkSetSecurity.Top = cmdDelLine.Top

End Sub

Private Sub sprdEdit_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)

    If blnLoading Then Exit Sub
    
    cmdSave.Visible = True
    
End Sub

Private Sub sprdEdit_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)

Dim strTemp   As String
Dim strInit   As String
Dim lngPos    As Long

    If Mode = 0 And ChangeMade = True Then
        sprdEdit.Row = Row
        sprdEdit.Col = EDIT_COL_POS
        If Val(sprdEdit.Text) <> EDIT_NEW Then sprdEdit.Text = EDIT_UPD
        sprdEdit.Col = Col
        cmdSave.Visible = True
    End If
    'if coming out of edit mode then jump to next column
    If Mode = 0 Then
        If Col = sprdEdit.MaxCols Then
            'jump to next row as no more columns to move to
            Col = COL_DESC
            Row = Row + 1
        Else
            Col = Col + 1
            sprdEdit.Col = Col
            sprdEdit.Row = Row
            'move to next column and further until editable cell found
            While ((sprdEdit.Lock = True) Or (sprdEdit.ColHidden = True)) And (Col <= sprdEdit.MaxCols)
                Col = Col + 1
                sprdEdit.Col = Col
            Wend
            'check that cell was found else move to next column
            If Col > sprdEdit.MaxCols Then
                Col = COL_DESC
                Row = Row + 1
            End If
        End If
        Call sprdEdit.SetActiveCell(Col, Row)
    End If

End Sub
