VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmChangePwd 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Change user password"
   ClientHeight    =   2565
   ClientLeft      =   1590
   ClientTop       =   1545
   ClientWidth     =   5295
   Icon            =   "frmChangePwd.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2565
   ScaleWidth      =   5295
   Tag             =   "116"
   Begin VB.TextBox txtConfirmPassword 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1800
      MaxLength       =   5
      PasswordChar    =   "*"
      TabIndex        =   8
      Top             =   1560
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "F10-Cancel"
      Height          =   375
      Left            =   3720
      TabIndex        =   11
      Tag             =   "114"
      Top             =   1560
      Width           =   1215
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "F5-Save"
      Height          =   375
      Left            =   3720
      TabIndex        =   10
      Tag             =   "113"
      Top             =   1080
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox txtNewPassword 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1800
      MaxLength       =   5
      PasswordChar    =   "*"
      TabIndex        =   6
      Top             =   1200
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.TextBox txtPassword 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1800
      MaxLength       =   5
      PasswordChar    =   "*"
      TabIndex        =   4
      Top             =   720
      Width           =   855
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   9
      Top             =   2190
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmChangePwd.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "17:36"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblConfirmPwdlbl 
      BackStyle       =   0  'Transparent
      Caption         =   "Confirm Password"
      Height          =   255
      Left            =   360
      TabIndex        =   7
      Tag             =   "115"
      Top             =   1560
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblFullName 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   2760
      TabIndex        =   2
      Top             =   240
      Width           =   2175
   End
   Begin VB.Label lblInitials 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1800
      TabIndex        =   1
      Top             =   240
      Width           =   735
   End
   Begin VB.Label lblNewPwdlbl 
      BackStyle       =   0  'Transparent
      Caption         =   "New Password"
      Height          =   255
      Left            =   360
      TabIndex        =   5
      Tag             =   "112"
      Top             =   1200
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Name"
      Height          =   255
      Left            =   360
      TabIndex        =   0
      Tag             =   "111"
      Top             =   240
      Width           =   855
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Password"
      Height          =   255
      Left            =   360
      TabIndex        =   3
      Tag             =   "110"
      Top             =   720
      Width           =   1095
   End
End
Attribute VB_Name = "frmChangePwd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmChangePwd
'* Date   : 26/11/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Change Password/frmChangePwd.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 3/03/03 10:07 $ $Revision: 2 $
'* Versions:
'* 26/11/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmChangePwd"

Dim moUser      As Object
Dim sFailedMsg  As String

Private Sub cmdCancel_Click()

    Unload Me

End Sub

Private Sub cmdCancel_GotFocus()
    
    cmdCancel.FontBold = True

End Sub

Private Sub cmdCancel_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 27 Then
        KeyAscii = 0
        Call SendKeys("+" & vbTab)
    End If

End Sub

Private Sub cmdCancel_LostFocus()
    
    cmdCancel.FontBold = False

End Sub

Private Sub cmdSave_GotFocus()

    cmdSave.FontBold = True

End Sub

Private Sub cmdSave_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 27 Then
        KeyAscii = 0
        Call SendKeys("+" & vbTab)
    End If

End Sub

Private Sub cmdSave_LostFocus()
    
    cmdSave.FontBold = False

End Sub

Private Sub Form_GotFocus()

    cmdSave.Visible = True

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If Shift = 0 Then
        Select Case (KeyCode)
            Case (vbKeyF5):
                    If cmdSave.Visible Then
                        KeyCode = 0
                        Call cmdSave_click
                    End If
            Case (vbKeyF10):
                    If cmdCancel.Visible Then
                        KeyCode = 0
                        Call cmdCancel_Click
                    End If
         End Select
     End If

End Sub

Private Sub Form_Load()

Dim oTrans   As CTSTranslator.clsTranslator
Dim lCtlNo   As Long

    Set oTrans = New clsTranslator
    'Translate form caption first
'    Me.Caption = oTrans.TranslateStr(Me.Tag, Me.Caption)
    'Step through each control and translate
    For lCtlNo = 0 To Me.Controls.Count - 1 Step 1
'        If Val(Controls(lCtlNo).Tag) > 0 Then Controls(lCtlNo).Caption = oTrans.TranslateStr(Controls(lCtlNo).Tag, Controls(lCtlNo).Caption)
        Debug.Print Controls(lCtlNo).Name
    Next lCtlNo
    sFailedMsg = oTrans.TranslateStr(117, "Invalid password for User ID, re-enter password")
    Set oTrans = Nothing
    
    Call GetRoot
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    Call InitialiseStatusBar(sbStatus)
    Call CentreForm(Me)
    lblInitials.Caption = goSession.UserInitials
    lblFullName.Caption = goSession.UserFullName

End Sub

Private Sub txtConfirmPassword_Change()

    cmdSave.Visible = True

End Sub

Private Sub txtPassword_GotFocus()
    
    txtPassword.SelStart = 0
    txtPassword.SelLength = Len(txtPassword.Text)
    txtPassword.BackColor = RGBEdit_Colour

End Sub

Private Sub txtPassword_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        KeyAscii = 0
        txtNewPassword.Visible = True
        lblNewPwdlbl.Visible = True
        Call SendKeys(vbTab)
    End If
    
    If KeyAscii = 27 Then
        KeyAscii = 0
        Call SendKeys("+" & vbTab)
    End If

End Sub

Private Sub cmdSave_click()
    
Const PROCEDURE_NAME As String = "cmdSave_Click"
    
    'check new password filled in and matches confirm password
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Auth UserID Validated")
    If txtNewPassword.Text = "" Then
        Call MsgBox("New password cannot be blank" & vbCrLf & "Enter valid password and re-save", vbInformation, "Invalid new password")
        Call txtNewPassword.SetFocus
        Exit Sub
    End If
    If txtNewPassword.Text <> txtConfirmPassword.Text Then
        Call MsgBox("New password does not match confirmation password", vbInformation, "Re-enter new password")
        Call txtNewPassword.SetFocus
        Exit Sub
    End If
    'Get User details
    Screen.MousePointer = vbHourglass
    If ((moUser Is Nothing) = True) Then
        Set moUser = goDatabase.CreateBusinessObject(CLASSID_USER)
        moUser.EmployeeID = goSession.UserID
        If moUser.IBo_Load = False Then
            Call MsgBox("Error occurred verifying User ID", vbExclamation, "Invalid system operation")
            Set moUser = Nothing
            End
        End If
    End If
    
    If txtPassword.Text <> moUser.Password Then
        Call MsgBox("Invalid user password entered", vbInformation, "Re-enter password")
        Call txtPassword.SetFocus
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        If moUser.SetPassword(txtNewPassword.Text) Then
            Call MsgBox("Password successfully updated", vbInformation, "Password Updated")
        Else
            Call MsgBox("Attempt to update password failed", vbInformation, "Password Update")
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
    Screen.MousePointer = vbNormal
    End

End Sub

Private Sub txtNewPassword_GotFocus()
    
    txtNewPassword.SelStart = 0
    txtNewPassword.SelLength = Len(txtNewPassword.Text)
    txtNewPassword.BackColor = RGBEdit_Colour

End Sub

Private Sub txtNewPassword_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        txtConfirmPassword.Visible = True
        lblConfirmPwdlbl.Visible = True
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = 27 Then
        KeyAscii = 0
        Call SendKeys("+" & vbTab)
    End If

End Sub

Private Sub txtNewPassword_LostFocus()
    
    txtNewPassword.BackColor = lblInitials.BackColor

End Sub

Private Sub txtConfirmPassword_GotFocus()

    txtConfirmPassword.SelStart = 0
    txtConfirmPassword.SelLength = Len(txtConfirmPassword.Text)
    txtConfirmPassword.BackColor = RGBEdit_Colour

End Sub

Private Sub txtConfirmPassword_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        KeyAscii = 0
        If cmdSave.Visible = True Then Call cmdSave_click
    End If
    
    If KeyAscii = 27 Then
        KeyAscii = 0
        Call SendKeys("+" & vbTab)
    End If

End Sub


Private Sub txtConfirmPassword_LostFocus()

    txtConfirmPassword.BackColor = lblInitials.BackColor

End Sub

Private Sub txtPassword_LostFocus()
    
    txtPassword.BackColor = lblInitials.BackColor

End Sub
