VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmChangeRefundPwd 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Change refund password"
   ClientHeight    =   1965
   ClientLeft      =   1590
   ClientTop       =   1545
   ClientWidth     =   5295
   Icon            =   "frmChangeRefundPwd.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1965
   ScaleWidth      =   5295
   Tag             =   "116"
   Begin VB.TextBox txtCurrentPwd 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1800
      MaxLength       =   5
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   240
      Width           =   855
   End
   Begin VB.TextBox txtConfirmPassword 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1800
      MaxLength       =   5
      PasswordChar    =   "*"
      TabIndex        =   5
      Top             =   1080
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "F10-Cancel"
      Height          =   375
      Left            =   3600
      TabIndex        =   8
      Tag             =   "114"
      Top             =   960
      Width           =   1215
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "F5-Save"
      Height          =   375
      Left            =   3600
      TabIndex        =   7
      Tag             =   "113"
      Top             =   480
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox txtNewPassword 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1800
      MaxLength       =   5
      PasswordChar    =   "*"
      TabIndex        =   3
      Top             =   720
      Visible         =   0   'False
      Width           =   855
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   6
      Top             =   1590
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmChangeRefundPwd.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "16:13"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Current Password"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Tag             =   "112"
      Top             =   240
      Width           =   1455
   End
   Begin VB.Label lblConfirmPwdlbl 
      BackStyle       =   0  'Transparent
      Caption         =   "Confirm Password"
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Tag             =   "115"
      Top             =   1080
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblNewPwdlbl 
      BackStyle       =   0  'Transparent
      Caption         =   "New Password"
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Tag             =   "112"
      Top             =   720
      Visible         =   0   'False
      Width           =   1215
   End
End
Attribute VB_Name = "frmChangeRefundPwd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>**************************************************************************************
'* Module : frmChangeRefundPwd
'* Date   : 20/03/03
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Change Refund Password/frmChangeRefundPwd.frm $
'********************************************************************************************
'* Summary: Used to update the Refund Password in the System Options(SYSOPT) table.  User
'*          must enter existing Refund password followed by new password and confirm.
'********************************************************************************************
'* $Author: Mauricem $ $Date: 7/10/03 10:11 $ $Revision: 3 $
'* Versions:
'* 26/11/02    mauricem
'*             Header added.
'</CAMH>*************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmChangeRefundPwd"

Dim mstrFailedMsg  As String

Private Sub cmdCancel_Click()

    Unload Me

End Sub

Private Sub cmdCancel_GotFocus()
    
    cmdCancel.FontBold = True

End Sub

Private Sub cmdCancel_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyEscape) Then
        KeyAscii = 0
        Call SendKeys("+" & vbTab)
    End If

End Sub

Private Sub cmdCancel_LostFocus()
    
    cmdCancel.FontBold = False

End Sub

Private Sub cmdSave_GotFocus()

    cmdSave.FontBold = True

End Sub

Private Sub cmdSave_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyEscape) Then
        KeyAscii = 0
        Call SendKeys("+" & vbTab)
    End If

End Sub

Private Sub cmdSave_LostFocus()
    
    cmdSave.FontBold = False

End Sub

Private Sub Form_GotFocus()

    cmdSave.Visible = True

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If Shift = 0 Then
        Select Case (KeyCode)
            Case (vbKeyF5):
                    If cmdSave.Visible Then
                        KeyCode = 0
                        Call cmdSave_click
                    End If
            Case (vbKeyF10):
                    If cmdCancel.Visible Then
                        KeyCode = 0
                        Call cmdCancel_Click
                    End If
         End Select
     End If

End Sub
Private Sub Form_Load()

Dim oTrans   As CTSTranslator.clsTranslator
Dim lCtlNo   As Long

    If LenB(Command) = 0 Then
        Call MsgBoxEx("This option can only be accessed from the Menu", vbCritical, "Change refund password", , , , , RGBMsgBox_WarnColour)
        End
    End If
    Set oTrans = New clsTranslator
    'Translate form caption first
'    Me.Caption = oTrans.TranslateStr(Me.Tag, Me.Caption)
    'Step through each control and translate
    For lCtlNo = 0 To Me.Controls.Count - 1 Step 1
'        If Val(Controls(lCtlNo).Tag) > 0 Then Controls(lCtlNo).Caption = oTrans.TranslateStr(Controls(lCtlNo).Tag, Controls(lCtlNo).Caption)
        Debug.Print Controls(lCtlNo).Name
    Next lCtlNo
    mstrFailedMsg = oTrans.TranslateStr(117, "Invalid password for User ID, re-enter password")
    Set oTrans = Nothing
    
    Call GetRoot
    
    If LenB(goSession.UserID) = 0 Then
        Call MsgBoxEx("Invalid user passed from Menu", vbCritical, "Change refund password", , , , , RGBMsgBox_WarnColour)
        End
    End If
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
    RGBMSGBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
    Call InitialiseStatusBar(sbStatus)
    Call CentreForm(Me)

End Sub

Private Sub txtConfirmPassword_Change()

    cmdSave.Visible = True

End Sub


Private Sub cmdSave_click()

Const PROCEDURE_NAME As String = "cmdSave_Click"
    
Dim oSysOpt As Object
Dim strPwd  As String 'used to hold password pre-padded with 0's to make 5 chars

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Refund password update")
    'check current refund password filled in
    If LenB(txtCurrentPwd.Text) = 0 Then
        Call MsgBoxEx("Current Refund password not entered" & vbCrLf & "Enter current refund password and re-save", vbInformation, "Missing refund password", , , , , RGBMsgBox_WarnColour)
        Call txtCurrentPwd.SetFocus
        Exit Sub
    End If
    'check new password filled in and matches confirm password
    If LenB(txtNewPassword.Text) = 0 Then
        Call MsgBoxEx("New password cannot be blank" & vbCrLf & "Enter valid password and re-save", vbInformation, "Invalid new password", , , , , RGBMsgBox_WarnColour)
        Call txtNewPassword.SetFocus
        Exit Sub
    End If
    If txtNewPassword.Text <> txtConfirmPassword.Text Then
        Call MsgBoxEx("New password does not match confirmation password", vbInformation, "Re-enter new password", , , , , RGBMsgBox_WarnColour)
        Call txtNewPassword.SetFocus
        Exit Sub
    End If
    
    'Get System Options for updating
    Screen.MousePointer = vbHourglass
    Set oSysOpt = goDatabase.CreateBusinessObject(CLASSID_SYSTEMOPTIONS)
    Call oSysOpt.IBo_Load
    'Pad existing password to make sure 5 characters long
    strPwd = txtCurrentPwd.Text
    While Len(strPwd) < 5
        strPwd = "0" & strPwd
    Wend
    'check new current password is correct
    If (oSysOpt.StoreManagerPassword <> strPwd) Then
        Call MsgBoxEx("Update failed-Current password invalid", vbInformation, "Refund Password Update", , , , , RGBMsgBox_WarnColour)
        Call txtCurrentPwd.SetFocus
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    'Pad new password to make sure 5 characters long
    strPwd = txtNewPassword.Text
    While Len(strPwd) < 5
        strPwd = "0" & strPwd
    Wend
    'Perform update and prompt user to indicate success or error
    If oSysOpt.ChangeRefundPassword(strPwd) Then 'inform user update successful and exit
        Call MsgBoxEx("Password successfully updated", vbInformation, "Refund Password Updated", , , , , RGBMSGBox_PromptColour)
        Screen.MousePointer = vbNormal
        End
    Else 'report error occurred
        Call MsgBoxEx("Attempt to update password failed", vbInformation, "Refund Password Update", , , , , RGBMsgBox_WarnColour)
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

End Sub

Private Sub txtCurrentPwd_GotFocus()
    
    txtCurrentPwd.SelStart = 0
    txtCurrentPwd.SelLength = Len(txtCurrentPwd.Text)
    txtCurrentPwd.BackColor = RGBEdit_Colour

End Sub

Private Sub txtCurrentPwd_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        txtNewPassword.Visible = True
        lblNewPwdlbl.Visible = True
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        KeyAscii = 0
        Call SendKeys("+" & vbTab)
    End If


End Sub

Private Sub txtCurrentPwd_LostFocus()
    
    txtCurrentPwd.BackColor = RGB_WHITE

End Sub

Private Sub txtNewPassword_GotFocus()
    
    txtNewPassword.SelStart = 0
    txtNewPassword.SelLength = Len(txtNewPassword.Text)
    txtNewPassword.BackColor = RGBEdit_Colour

End Sub

Private Sub txtNewPassword_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        txtConfirmPassword.Visible = True
        lblConfirmPwdlbl.Visible = True
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        KeyAscii = 0
        Call SendKeys("+" & vbTab)
    End If

End Sub

Private Sub txtNewPassword_LostFocus()
    
    txtNewPassword.BackColor = RGB_WHITE

End Sub

Private Sub txtConfirmPassword_GotFocus()

    txtConfirmPassword.SelStart = 0
    txtConfirmPassword.SelLength = Len(txtConfirmPassword.Text)
    txtConfirmPassword.BackColor = RGBEdit_Colour

End Sub

Private Sub txtConfirmPassword_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        If cmdSave.Visible = True Then Call cmdSave_click
    End If
    
    If (KeyAscii = vbKeyEscape) Then
        KeyAscii = 0
        Call SendKeys("+" & vbTab)
    End If

End Sub


Private Sub txtConfirmPassword_LostFocus()

    txtConfirmPassword.BackColor = RGB_WHITE

End Sub

