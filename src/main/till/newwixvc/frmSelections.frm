VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmSelections 
   Caption         =   "WixVC (Load Selections)"
   ClientHeight    =   2775
   ClientLeft      =   3960
   ClientTop       =   4935
   ClientWidth     =   6750
   Icon            =   "frmSelections.frx":0000
   LinkTopic       =   "Form2"
   ScaleHeight     =   2775
   ScaleWidth      =   6750
   Begin MSComDlg.CommonDialog dialog 
      Left            =   4560
      Top             =   120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      DialogTitle     =   "Load File"
      FileName        =   "*.wvc"
      InitDir         =   "c:\wixvc\"
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Continue >>"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4850
      TabIndex        =   5
      Top             =   2400
      Width           =   1815
   End
   Begin VB.Frame Frame2 
      Caption         =   "Options"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Left            =   4800
      TabIndex        =   1
      Top             =   120
      Width           =   1935
      Begin VB.TextBox Text1 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   7
         Text            =   "TillFunction.exe"
         Top             =   1380
         Width           =   1695
      End
      Begin VB.CommandButton Command4 
         Caption         =   "Add Entry"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   6
         Top             =   1680
         Width           =   1695
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Clear List"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   1695
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Load from Skynet"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   3
         Top             =   840
         Width           =   1695
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Loaded Selections"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4575
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1815
         Left            =   120
         Sorted          =   -1  'True
         TabIndex        =   2
         Top             =   240
         Width           =   4335
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Double Click an entry to remove from the list"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   2160
         Width           =   4335
      End
   End
End
Attribute VB_Name = "frmSelections"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()

Dim intFileHandle As Integer
Dim fulllist      As String
Dim strRETP       As String

    fulllist = "\\skynet\vol1\WickesVersionChecker\FullList.wvc"
    intFileHandle = FreeFile

    Open fulllist For Input As #intFileHandle
    
    Do While Not EOF(intFileHandle)
        Line Input #intFileHandle, strRETP
        List1.AddItem strRETP
        Loop
    Close #intFileHandle ' end read master

End Sub

Private Sub Command2_Click()

    List1.Clear

End Sub

Private Sub Command4_Click()

    List1.AddItem Text1.Text
    
End Sub

Private Sub Form_Load()

Dim strLocation   As String
Dim intFileHandle As String

    On Error GoTo error01

    Command4_Click

    strLocation = "\\skynet\vol1\WickesVersionChecker\Start.txt"
    intFileHandle = FreeFile
    Open strLocation For Input As #intFileHandle
    Close #intFileHandle
    
    If List1.listcount > 0 Then
        frmVersionChecker.Show
        Me.Hide
    Else
        MsgBox "There are no files to check!"
    End If

    Exit Sub

error01:

    MsgBox "Unable to connect to Skynet, Please ensure you are mapped and try again"
    End

End Sub

Private Sub List1_DblClick()
    
    If List1.Text > "" Then List1.RemoveItem (List1.ListIndex)
    
End Sub
