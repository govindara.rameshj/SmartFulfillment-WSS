VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{C8530F8A-C19C-11D2-99D6-9419F37DBB29}#1.1#0"; "ccrpprg6.ocx"
Begin VB.Form frmVersionChecker 
   AutoRedraw      =   -1  'True
   Caption         =   "Wickes Version Checker"
   ClientHeight    =   4095
   ClientLeft      =   2655
   ClientTop       =   3090
   ClientWidth     =   8160
   Icon            =   "frmVersionChecker.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4095
   ScaleWidth      =   8160
   Begin VB.Frame Frame5 
      Caption         =   "Details"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   3600
      TabIndex        =   19
      Top             =   120
      Width           =   4455
      Begin VB.Label listupdatedate 
         Alignment       =   2  'Center
         Caption         =   "Store List last updated: --GENERATED--"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   600
         Width           =   4215
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         Caption         =   "--Generated Version String--"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   360
         Width           =   4215
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Results"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3855
      Left            =   120
      TabIndex        =   17
      Top             =   120
      Width           =   3375
      Begin MSFlexGridLib.MSFlexGrid Grid 
         Height          =   3495
         Left            =   120
         TabIndex        =   18
         Top             =   240
         Width           =   3165
         _ExtentX        =   5583
         _ExtentY        =   6165
         _Version        =   393216
         AllowUserResizing=   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin MSComDlg.CommonDialog dialog 
      Left            =   120
      Top             =   5640
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   5000
      Left            =   9600
      Top             =   3600
   End
   Begin VB.CheckBox busy 
      Caption         =   "Busy"
      Height          =   195
      Left            =   120
      TabIndex        =   15
      Top             =   6000
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Frame Frame3 
      Caption         =   "Batch Poll"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   3600
      TabIndex        =   14
      Top             =   1200
      Width           =   4455
      Begin VB.CommandButton Command3 
         Caption         =   "Add Store"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1560
         TabIndex        =   4
         Top             =   600
         Width           =   1215
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1680
         TabIndex        =   3
         Top             =   240
         Width           =   975
      End
      Begin VB.ListBox stores 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1035
         Left            =   120
         Sorted          =   -1  'True
         TabIndex        =   8
         Top             =   240
         Width           =   1335
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   500
         Left            =   0
         Top             =   1560
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Batch Poll"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1560
         TabIndex        =   5
         Top             =   960
         Width           =   2775
      End
      Begin VB.ListBox pcs 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   0
         Sorted          =   -1  'True
         TabIndex        =   16
         Top             =   960
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   "Remaining Count:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2880
         TabIndex        =   23
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label listcount 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3240
         TabIndex        =   22
         Top             =   600
         Width           =   615
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Poll Store"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   6240
      TabIndex        =   11
      Top             =   2640
      Width           =   1815
      Begin VB.TextBox Text1 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   0
         Text            =   "086"
         Top             =   480
         Width           =   735
      End
      Begin VB.TextBox Text2 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   960
         TabIndex        =   1
         Text            =   "01"
         Top             =   480
         Width           =   735
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Poll File"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   960
         Width           =   1575
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Store"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "Till"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   960
         TabIndex        =   12
         Top             =   240
         Width           =   735
      End
   End
   Begin CCRProgressBar6.ccrpProgressBar progress 
      Height          =   270
      Left            =   3720
      Top             =   3600
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   476
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.ListBox List2 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   6240
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      Sorted          =   -1  'True
      TabIndex        =   6
      Top             =   6480
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Frame Frame1 
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   3600
      TabIndex        =   9
      Top             =   2640
      Width           =   2535
      Begin VB.Label Status 
         Alignment       =   2  'Center
         Caption         =   "...Waiting..."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   120
         TabIndex        =   10
         Top             =   360
         Width           =   2295
      End
   End
   Begin VB.Menu mnuMenu 
      Caption         =   "Menu"
      Visible         =   0   'False
      WindowList      =   -1  'True
      Begin VB.Menu rement 
         Caption         =   "Remove Entry"
      End
      Begin VB.Menu delvers 
         Caption         =   "Delete Version File"
      End
      Begin VB.Menu sendgo 
         Caption         =   "Send .GO File"
      End
      Begin VB.Menu cop2clip 
         Caption         =   "Copy to Clipboard"
      End
      Begin VB.Menu sep 
         Caption         =   "-"
      End
      Begin VB.Menu cancel 
         Caption         =   "Cancel"
      End
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuExport 
         Caption         =   "Save / Export Data"
      End
      Begin VB.Menu sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
      End
   End
End
Attribute VB_Name = "frmVersionChecker"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'*
'*$Archive: /Projects/OasysHW/VB/newwixvc/frmVersionChecker.frm $
'**********************************************************************************************
'* $Author: Jeremy Janisch $ $Date: 02/11/05 10:08 $ $Revision: 0 $
'* $Versions:1.0.00$
'*
'**********************************************************************************************
'* Summary:
'*
'**********************************************************************************************
'* Versions:
'*
'* 01/12/05 DaveF   v1.3.9  General coding tidy.
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*
'**********************************************************************************************
'</CAMH>***************************************************************************************

Option Explicit

Private Declare Function SetClipboardData Lib "user32" (ByVal wFormat As Long, ByVal hMem As Long) As Long
Private Declare Function CopyImage Lib "user32" (ByVal handle As Long, ByVal imageType As Long, ByVal newWidth As Long, ByVal newHeight As Long, ByVal lFlags As Long) As Long
Private Declare Function EmptyClipboard Lib "user32" () As Long
Private Declare Function CloseClipboard Lib "user32" () As Long
Private Declare Function OpenClipboard Lib "user32" (ByVal hwnd As Long) As Long

Private Sub Command1_Click()

    Call dopoll(Text1.Text, Text2.Text, False)

End Sub

Private Sub Command2_Click()

    pcs.AddItem "01"
    pcs.AddItem "02"
    pcs.AddItem "03"
    pcs.AddItem "04"
    pcs.AddItem "05"
    pcs.AddItem "06"
    pcs.AddItem "07"
    pcs.AddItem "08"
    pcs.AddItem "09"
    pcs.AddItem "10"
    pcs.AddItem "11"
    stores.ListIndex = 0
    pcs.ListIndex = 0
    
    Timer1.Enabled = True
    Command2.Enabled = False
    
    stores.Enabled = False
    Text1.Enabled = False
    Text2.Enabled = False
    Command2.Enabled = False
    Command1.Enabled = False
    Text3.Enabled = False
    Command3.Enabled = False

End Sub

Private Sub Command3_Click()

    If Text3.Text > "" Then
        stores.AddItem Text3.Text
        listcount.Caption = frmVersionChecker.stores.listcount
        Call UpdateStatus("Added : " & Text3.Text)
    Else
        Call UpdateStatus("Nothing to Add!")
    End If

End Sub

Private Sub cop2clip_Click()

Dim strHeader   As String
Dim strStore    As String
Dim strFileName As String
Dim strVersion  As String

    strHeader = "Wickes Version Checker - " & Date & " - " & Time
    strStore = Grid.TextMatrix(Grid.Row, 0)
    strFileName = "Filename : " & Grid.TextMatrix(0, Grid.Col)
    strVersion = "Current Version : " & Grid.Text
    Clipboard.SetText strHeader & vbCrLf & vbCrLf & strStore & vbCrLf & strFileName & vbCrLf & strVersion
    Call UpdateStatus("Copied...")

End Sub

Private Sub delvers_Click()

Dim fsFSO       As Scripting.FileSystemObject
Dim strfile2del As String
Dim strLocation As String
Dim astrInfo()  As String
Dim strStore    As String
Dim strPC       As String

    On Error GoTo delvers_Click_Error
    
    Set fsFSO = New Scripting.FileSystemObject
    
    strfile2del = Grid.TextMatrix(Grid.Row, 0)
    Call UpdateStatus("Deleting...")
    astrInfo = Split(strfile2del, " - ")
    
    strStore = astrInfo(0)
    strPC = astrInfo(1)
    
    strLocation = "\\" & strStore & "\VOL1\cts\Update\Workstations\ws" & strPC & "\Version\Version." & strPC
    
    fsFSO.DeleteFile strLocation
    Grid.RemoveItem (Grid.RowSel)
    
    Call UpdateStatus("File Removed!")

    Set fsFSO = Nothing
    
    Exit Sub
    
delvers_Click_Error:

    Set fsFSO = Nothing
    
End Sub

Private Sub Form_Load()

Dim intFileHandle As Integer
Dim strRETP       As String
Dim strLocation   As String
Dim strline       As String
Dim fsFSO         As Scripting.FileSystemObject
Dim objFile       As Scripting.File
Dim strFileName   As String
Dim lngSelc       As Long
Dim strStoreList  As String

    On Error GoTo Form_Load_Error
    
    Grid.RowHeight(1) = 0

    strline = "StoreID & PC" & vbTab
    lngSelc = 0
    
    While lngSelc < frmSelections.List1.listcount
        frmSelections.List1.ListIndex = lngSelc
        strline = strline & frmSelections.List1.Text & vbTab
        lngSelc = lngSelc + 1
    Wend

    strStoreList = "\\skynet\vol1\WickesVersionChecker\stores.txt"

    intFileHandle = FreeFile

    Open strStoreList For Input As #intFileHandle
    Do While Not EOF(intFileHandle)
        Line Input #intFileHandle, strRETP
        stores.AddItem strRETP
        Loop
    Close #intFileHandle ' end read master

    listcount.Caption = frmVersionChecker.stores.listcount

    Label7.Caption = "Version Checker v" & App.Major & "." & App.Minor & " - Revision: " & App.Revision & " by JJ"

    Grid.FormatString = "Store & PC ID" & vbTab & "TillFunction.exe"
    
    strFileName = strStoreList
    Set fsFSO = New Scripting.FileSystemObject
    Set objFile = fsFSO.GetFile(strFileName)
    listupdatedate.Caption = "Store list last modified: " & objFile.DateLastModified

    Set objFile = Nothing
    Set fsFSO = Nothing
    
    Exit Sub
    
Form_Load_Error:

    Set objFile = Nothing
    Set fsFSO = Nothing

End Sub

Private Sub Form_Unload(cancel As Integer)

    End
    
End Sub

Private Sub Grid_DblClick()

    If (Grid.Text > "") Then
        PopupMenu mnuMenu
    End If

End Sub

Private Sub mnuExit_Click()

    End

End Sub

Private Sub mnuExport_Click()

Dim xlheader As String

    dialog.FileName = ""
    dialog.Filter = "Excel Format (*.xls)|*.xls"
    dialog.ShowSave
    
    If dialog.FileName > "" Then
        xlheader = "Generated by Wickes " & Label7.Caption & " - " & Date & " - " & Time
        Call FG_SaveAsExcel(Grid, dialog.FileName, xlheader)
        Call UpdateStatus("Saved to .xls!")
    End If

End Sub

Private Sub rement_Click()

    Grid.RemoveItem (Grid.RowSel)
    Call UpdateStatus("Removed Row")
    
End Sub

Private Sub sendgo_Click()

Dim strMessage  As String
Dim strfile2del As String
Dim astrInfo()  As String
Dim strStore    As String
Dim strPC       As String
Dim strLocation As String

    strfile2del = Grid.TextMatrix(Grid.Row, 0)
    Call UpdateStatus("Sending...")
    astrInfo = Split(strfile2del, " - ")
    
    strStore = astrInfo(0)
    strPC = astrInfo(1)

    strLocation = "\\" & strStore & "\VOL1\cts\Update\Workstations\ws" & strPC & "\" & strPC & ".GO"
    strMessage = "GO File Created by Wickes Version Checker - " & Date & " - " & Time
    
    Open strLocation For Output As #1
    Print #1, strMessage
    Close #1
    
    Call UpdateStatus(".GO Created!")
    
End Sub

Private Sub stores_DblClick()

    stores.RemoveItem (stores.ListIndex)
    listcount.Caption = frmVersionChecker.stores.listcount
    Call UpdateStatus("Removed from List")

End Sub

Private Sub Timer1_Timer()

    If pcs.listcount > "0" And busy.Value = 0 Then
        pcs.ListIndex = 0
        Call dopoll(stores.Text, pcs.Text, True)
    End If
    
    If stores.listcount > "0" And pcs.listcount > "0" And busy.Value = 0 Then
        stores.ListIndex = 0
        pcs.ListIndex = 0
        Call dopoll(stores.Text, pcs.Text, True)
    End If
    
    If stores.listcount > "0" And pcs.listcount = "0" And busy.Value = 0 Then
        stores.RemoveItem (0)
        Grid.AddItem ""
    
        If stores.listcount > "0" Then
            stores.ListIndex = 0
            pcs.AddItem "01"
            pcs.AddItem "02"
            pcs.AddItem "03"
            pcs.AddItem "04"
            pcs.AddItem "05"
            pcs.AddItem "06"
            pcs.AddItem "07"
            pcs.AddItem "08"
            pcs.AddItem "09"
            pcs.AddItem "10"
            pcs.AddItem "11"
        End If
    End If
            
    If stores.listcount = "0" And pcs.listcount = "0" Then
        stores.Enabled = True
        Text1.Enabled = True
        Text2.Enabled = True
        Timer1.Enabled = False
        Command1.Enabled = True
        Text3.Enabled = True
        Command3.Enabled = True
        listcount.Caption = frmVersionChecker.stores.listcount
    End If
    
    listcount.Caption = frmVersionChecker.stores.listcount

End Sub

Public Function FG_SaveAsExcel(FG As MSFlexGrid, sFileName As String, Optional sHeader As String = "", Optional sFooter As String = "")

Dim myExcel     As ExcelFileV2
Dim lRow        As Integer
Dim lCol        As Integer
Dim excelDouble As Double
Dim rowOffset   As Long
Dim aTemp()     As String
  
    If Len(sHeader) > 0 Then
        aTemp = Split(sHeader, vbTab)
        rowOffset = UBound(aTemp) + 1
    End If
  
    Set myExcel = New ExcelFileV2
  
    With myExcel
  
        .OpenFile sFileName
      
        ' Heading
        For lRow = 1 To rowOffset
            .EWriteString lRow, 1, aTemp(lRow - 1)
          Next lRow
    
        ' FlexGrid -> Fixedrows
        For lRow = 1 To FG.FixedRows
            For lCol = 1 To FG.Cols
            .EWriteString lRow + rowOffset, lCol, FG.TextMatrix(lRow - 1, lCol - 1)
            Next lCol
        Next lRow
    
        ' Data
        For lRow = FG.FixedRows + 1 To FG.Rows
        
            ' FlexGrid -> Fixedcols
            For lCol = 1 To FG.FixedCols
                .EWriteString lRow + rowOffset, lCol, FG.TextMatrix(lRow - 1, lCol - 1)
            Next lCol
        
            ' FlexGrid -> Data
            For lCol = FG.FixedCols + 1 To FG.Cols
                If IsNumeric(FG.TextMatrix(lRow - 1, lCol - 1)) Then
                    excelDouble = CDbl(FG.TextMatrix(lRow - 1, lCol - 1)) + 0
                    .EWriteDouble lRow + rowOffset, lCol, excelDouble
                Else
                    .EWriteString lRow + rowOffset, lCol, FG.TextMatrix(lRow - 1, lCol - 1)
                End If
            Next lCol
        Next lRow
      
        ' Footer
        If Len(sFooter) > 0 Then
            aTemp = Split(sFooter, vbTab)
            For lRow = 0 To UBound(aTemp)
                .EWriteString lRow + rowOffset + FG.Rows + 1, 1, aTemp(lRow)
            Next lRow
        End If
        
        .CloseFile
  End With
  
End Function
