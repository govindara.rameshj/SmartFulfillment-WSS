Attribute VB_Name = "modRead"
Option Explicit

Public Function dopoll(store As String, pc As String, storepoll As Boolean)

Dim intFileHandle  As Integer
Dim strRETP        As String
Dim location       As String
Dim Name           As String
Dim line           As String
Dim intLines       As Integer
Dim intCounter     As Integer
Dim astrVersions() As String
Dim lngSelc        As Long
Dim lngSelcCount   As Long

    On Error GoTo error

    frmVersionChecker.List1.Clear
    frmVersionChecker.List2.Clear
    
    frmVersionChecker.busy.Value = 1
    
    frmVersionChecker.progress.Min = 0
    frmVersionChecker.progress.Max = 5
    
    frmVersionChecker.Status.Caption = "Checking " & store & " - " & pc

    frmVersionChecker.progress.Value = 1

    location = "\\wix" & store & "\VOL1\cts\Update\Workstations\ws" & pc & "\Version\Version." & pc

    intFileHandle = FreeFile

    Open location For Input As #intFileHandle
    Do While Not EOF(intFileHandle)
        Line Input #intFileHandle, strRETP
        frmVersionChecker.List1.AddItem strRETP
        DoEvents
    Loop
    Close #intFileHandle ' end read master
    
    frmVersionChecker.progress.Value = 2

    intLines = frmVersionChecker.List1.listcount 'lines to count TO

    frmVersionChecker.Status.Caption = "Processing File..."
    frmVersionChecker.Refresh

    frmVersionChecker.List2.Clear
    intLines = frmVersionChecker.List1.listcount 'lines to count TO

    intCounter = 0
    While intCounter < intLines
    
        frmVersionChecker.progress.Value = 3
        
        frmVersionChecker.List1.ListIndex = intCounter
        astrVersions = Split(frmVersionChecker.List1.Text, ",")
        
        lngSelc = 0
            
        While lngSelc < frmSelections.List1.listcount
            lngSelcCount = frmSelections.List1.listcount
        
            frmSelections.List1.ListIndex = lngSelc
            Name = frmSelections.List1.Text
            If astrVersions(0) = Name Then
                frmVersionChecker.List2.AddItem astrVersions(1)
            End If
        
            lngSelc = lngSelc + 1
        Wend
        
        intCounter = intCounter + 1
    Wend

    frmVersionChecker.progress.Value = 4

    line = "wix" & store & " - " & pc & vbTab

    lngSelc = 0
    While lngSelc < frmVersionChecker.List2.listcount
        frmVersionChecker.List2.ListIndex = lngSelc
        line = line & frmVersionChecker.List2.Text & vbTab
        lngSelc = lngSelc + 1
        DoEvents
    Wend

    If frmSelections.List1.listcount = frmVersionChecker.List2.listcount Then
        frmVersionChecker.Grid.AddItem line
    Else
        MsgBox "This entry has not been added due some files not being listed in the version file."
    End If

    frmVersionChecker.Status.Caption = "Added to table"
    frmVersionChecker.progress.Value = 5
    
    frmVersionChecker.busy.Value = 0

    If storepoll = True Then frmVersionChecker.pcs.RemoveItem (0)
    If frmVersionChecker.pcs.listcount = "0" Then frmVersionChecker.Command2.Enabled = True

    frmVersionChecker.listcount.Caption = frmVersionChecker.stores.listcount
    
    Exit Function

error:
    
    If storepoll = False Then MsgBox "Error Reading Requested File!"
    frmVersionChecker.progress.Value = 0
    frmVersionChecker.Status.Caption = "Error " & store & " - " & pc

End Function

Public Function getmaster()

Dim intFileHandle  As Integer
Dim strRETP        As String
Dim location       As String
Dim Name           As String
Dim line           As String
Dim intLines       As Integer
Dim intCounter     As Integer
Dim astrVersions() As String
Dim lngSelc        As Long
Dim lngSelcCount   As Long
Dim strStore       As String
Dim strPC          As String
Dim strStorePoll   As String

    On Error GoTo error
    
    frmVersionChecker.List1.Clear
    frmVersionChecker.List2.Clear
    
    frmVersionChecker.busy.Value = 1
    
    frmVersionChecker.progress.Min = 0
    frmVersionChecker.progress.Max = 5
    
    frmVersionChecker.Status.Caption = "Loading Master List..."
    
    frmVersionChecker.Refresh
    
    frmVersionChecker.progress.Value = 1
    
    intFileHandle = FreeFile
    
    Open location For Input As #intFileHandle
    Do While Not EOF(intFileHandle)
        Line Input #intFileHandle, strRETP
        frmVersionChecker.List1.AddItem strRETP
        DoEvents
    Loop
    Close #intFileHandle ' end read master
        
    frmVersionChecker.progress.Value = 2
    
    intLines = frmVersionChecker.List1.listcount 'lines to count TO
    
    frmVersionChecker.Status.Caption = "Processing File..."
    frmVersionChecker.Refresh
    
    frmVersionChecker.List2.Clear
    intLines = frmVersionChecker.List1.listcount 'lines to count TO
    
    intCounter = 0
    While intCounter < intLines
    
        frmVersionChecker.progress.Value = 3
        
        frmVersionChecker.List1.ListIndex = intCounter
        astrVersions = Split(frmVersionChecker.List1.Text, ",")
        
        lngSelc = 0
            
        While lngSelc < frmSelections.List1.listcount
            lngSelcCount = frmSelections.List1.listcount
            
            frmSelections.List1.ListIndex = lngSelc
            Name = frmSelections.List1.Text
            If astrVersions(0) = Name Then
                frmVersionChecker.List2.AddItem astrVersions(1)
            End If
            lngSelc = lngSelc + 1
        Wend
        
        intCounter = intCounter + 1
    Wend
    
    frmVersionChecker.progress.Value = 4
    
    line = "wix" & strStore & " - " & strPC & vbTab
    
    lngSelc = 0
    While lngSelc < frmVersionChecker.List2.listcount
        frmVersionChecker.List2.ListIndex = lngSelc
        line = line & frmVersionChecker.List2.Text & vbTab
        lngSelc = lngSelc + 1
        DoEvents
    Wend
    
    If frmSelections.List1.listcount = frmVersionChecker.List2.listcount Then
        frmVersionChecker.Grid.AddItem line
    Else
        MsgBox "This entry has not been added due some files not being listed in the version file."
    End If
    
    frmVersionChecker.Status.Caption = "Added to table"
    frmVersionChecker.progress.Value = 5
    
    frmVersionChecker.busy.Value = 0
    
    If strStorePoll = True Then frmVersionChecker.pcs.RemoveItem (0)
    If frmVersionChecker.pcs.listcount = "0" Then frmVersionChecker.Command2.Enabled = True
    
    Exit Function
    
error:

    If strStorePoll = False Then MsgBox "Error Reading Requested File!"
    frmVersionChecker.progress.Value = 0
    frmVersionChecker.Status.Caption = "Error " & strStore & " - " & strPC
    
End Function

Public Function UpdateStatus(newstatus As String)

    frmVersionChecker.Status.Caption = newstatus
    DoEvents
    frmVersionChecker.Refresh
    
End Function

