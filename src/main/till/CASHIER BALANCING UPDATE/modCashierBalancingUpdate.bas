Attribute VB_Name = "modCashierBalancingUpdate"
'<CAMH>****************************************************************************************
'* Module : modCashierBalancingUpdate
'* Date   : 04/05/04
'* Author :
'*$Archive: /Projects/OasysV2/VB/Cashier Balancing Update/modCashierBalancingUpdate.bas $Author:  $ $Date: 13/05/04 9:39 $ $Revision: 1 $
'* Versions:
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME               As String = "modCashierBalancingUpdate"
Const MODULE_FUNCTION           As String = "Cashier Balancing Update"
Const DISPLAY_UPDATE_INTERVAL   As Long = 1

Const LOADING_MESSAGE           As String = "Loading Transaction Data..."
Const PROCESSING_MESSAGE        As String = "Processing Transaction Data..."
Const BLANK_MESSAGE             As String = vbNullString

Dim mblnFromNightlyClose        As Boolean
Dim mstrAccountabilityType      As String
Dim mdteRRReportsDate           As Date
Dim mblnDidReversal             As Boolean

' Cashier/Till Totals
Dim mcurCTGrossSales                        As Currency
Dim mcurCTCashDiscountAmount                As Currency
Dim maintCTTransCodeCounts(1 To 8)          As Integer
Dim macurCTTransCodeAmounts(1 To 8)         As Currency
Dim macurCTTransCodeDiscountAmounts(1 To 8) As Currency
Dim maintCTTenderTypeCounts(1 To 40)        As Integer
Dim macurCTTenderTypeAmounts(1 To 40)       As Currency

Dim macurMiscIncomes(1 To 10)               As Currency
Dim macurMiscOutgoings(1 To 10)             As Currency

Public Sub Main()
Dim blnReverseUpdate As Boolean
    
    'Initialise Oasys objects
    GetRoot
    
    Call DecodeParameters(goSession.ProgramParams)
    
    blnReverseUpdate = False
    mblnDidReversal = False
    
    If mblnFromNightlyClose = False Then
        If MsgBox("Perform Cashier Balancing Update?", vbYesNo + vbQuestion, MODULE_FUNCTION) = vbNo Then
            End
        End If
        If MsgBox("Reverse Update?", vbYesNo + vbQuestion + vbDefaultButton2, MODULE_FUNCTION) = vbYes Then
            If MsgBox("Are You Sure That You Want to Reverse Update?", vbYesNo + vbQuestion + vbDefaultButton2, MODULE_FUNCTION) = vbYes Then
                blnReverseUpdate = True
            Else
                MsgBox "Update Cancelled.", vbExclamation, MODULE_FUNCTION
                End
            End If
        End If
    End If
    
    frmCashierBalancingUpdate.Show
    DoEvents
    frmCashierBalancingUpdate.lngUpdateInterval = DISPLAY_UPDATE_INTERVAL
    frmCashierBalancingUpdate.blnReverseVisible = blnReverseUpdate
    
    If blnReverseUpdate = True Then
        Call ReverseCashierBalancingUpdate
    Else
        Call PerformCashierBalancingUpdate
    End If

    If mblnFromNightlyClose = False Then
        If blnReverseUpdate = True Then
            If mblnDidReversal = True Then
                Call MsgBox("Cashier Balancing Update Reversal Complete.", vbInformation, MODULE_FUNCTION)
            Else
                Call MsgBox("Cashier Balancing Update Reversal Complete - Nothing to Reverse.", vbInformation, MODULE_FUNCTION)
            End If
        Else
            Call MsgBox("Cashier Balancing Update Complete.", vbInformation, MODULE_FUNCTION)
        End If
    Else
        Successful_End
    End If
    
    End
End Sub

Private Sub DecodeParameters(strCommand As String)

Const CALLED_FROM As String = "CF"
Const CF_CLOSE    As String = "C"

Dim strTempDate As String
Dim dteEndDate  As Date
Dim strSection  As String
Dim vntSection  As Variant
Dim lngSectNo   As Long
Dim blnSetOpts  As Boolean

    vntSection = Split(strCommand, ",")
    
    mblnFromNightlyClose = False
    If UBound(vntSection) = 0 Then Exit Sub
    
    For lngSectNo = 0 To UBound(vntSection) Step 1
        
        Debug.Print lngSectNo & "-" & vntSection(lngSectNo)
        strSection = vntSection(lngSectNo)
        
        Select Case (Left$(strSection, 2))
        Case CF_CLOSE
            Debug.Print Mid$(strSection, 4)
            If Mid$(strSection, 3) = CF_CLOSE Then
                mblnFromNightlyClose = True
            End If
        End Select
       
    Next lngSectNo
    
End Sub

Public Sub PerformCashierBalancingUpdate()

Const PROCEDURE_NAME As String = MODULE_NAME & ".PerformCashierBalancingUpdate"

Dim oRSelector      As Object
Dim oPOSHeader      As Object
Dim colPOSHeader    As Collection
Dim oBoSortKeys     As Object
Dim oRow            As Object
Dim blnUpdated      As Boolean
Dim strSaveCashTill As String
Dim intTranCode     As Integer
Dim oPOSPayment     As Object
Dim colPOSPayment   As Collection

    On Error GoTo errHandler
    
    Call ClearCTTotals
    Call ClearMiscTotals
    Call ReadRetailOptions
    
    If mstrAccountabilityType = "T" Then
        frmCashierBalancingUpdate.strCashierLabel = "for Till:"
    ElseIf mstrAccountabilityType = "C" Then
        frmCashierBalancingUpdate.strCashierLabel = "for Cashier:"
    Else
        Err.Raise OASYS_ERR_MISSING_RETAILEROPTIONS, PROCEDURE_NAME, "Invalid Accountability Type"
    End If
    
    'Start transaction
    goDatabase.StartTransaction
    
    'Loop through DLTOTS and add calculate totals
    Set oRSelector = goDatabase.CreateBoSelector(CLASSID_POSHEADER)
    
    Set oBoSortKeys = goSession.Root.CreateUtilityObject("CBoSortKeys")
    If mstrAccountabilityType = "T" Then
        oBoSortKeys.Add FID_POSHEADER_TillID
    Else
        oBoSortKeys.Add FID_POSHEADER_CashierID
    End If
    
    oRSelector.Override goDatabase.CreateBoSelector(FID_POSHEADER_TranDate, _
                        CMP_EQUAL, mdteRRReportsDate)
    oRSelector.Override goDatabase.CreateBoSelector(FID_POSHEADER_Voided, _
                        CMP_EQUAL, "False")
    oRSelector.Override goDatabase.CreateBoSelector(FID_POSHEADER_TrainingMode, _
                        CMP_EQUAL, "False")
    oRSelector.Override goDatabase.CreateBoSelector(FID_POSHEADER_TillID, _
                        CMP_NOTEQUAL, vbNullString)
    oRSelector.Override goDatabase.CreateBoSelector(FID_POSHEADER_CashierID, _
                        CMP_NOTEQUAL, vbNullString)
    
    'Get collection of POS headers
    If frmCashierBalancingUpdate.Visible Then
        frmCashierBalancingUpdate.strStatusMessage = LOADING_MESSAGE
    End If
    Set colPOSHeader = goDatabase.GetSortedBoCollection(oRSelector, oBoSortKeys)
    If frmCashierBalancingUpdate.Visible Then
        frmCashierBalancingUpdate.strStatusMessage = PROCESSING_MESSAGE
    End If

    strSaveCashTill = vbNullString
    
    For Each oPOSHeader In colPOSHeader
        
Debug.Print "DLTOTS: " & oPOSHeader.TranDate & " " & oPOSHeader.TransactionNo & " " & _
    oPOSHeader.TotalSaleAmount

        'Update the Form if it's visible
        If frmCashierBalancingUpdate.Visible Then
            If mstrAccountabilityType = "T" Then
                frmCashierBalancingUpdate.strCashier = oPOSHeader.TillID
            Else
                frmCashierBalancingUpdate.strCashier = oPOSHeader.CashierID
            End If
            frmCashierBalancingUpdate.strTransaction = oPOSHeader.TransactionNo
        End If

' If first time in, set save field for cashier/till
        If LenB(strSaveCashTill) = 0 Then
            If mstrAccountabilityType = "T" Then
                strSaveCashTill = oPOSHeader.TillID
            Else
                strSaveCashTill = oPOSHeader.CashierID
            End If
        End If
        
' Check to see if new cashier/till
        If mstrAccountabilityType = "T" Then
            If strSaveCashTill <> oPOSHeader.TillID Then
                Call UpdateCashierCashBal(strSaveCashTill)
                strSaveCashTill = oPOSHeader.TillID
            End If
        Else
            If strSaveCashTill <> oPOSHeader.CashierID Then
                Call UpdateCashierCashBal(strSaveCashTill)
                strSaveCashTill = oPOSHeader.CashierID
            End If
        End If

        intTranCode = 0
        If oPOSHeader.TransactionCode = "SA" And _
            oPOSHeader.TotalSaleAmount >= 0 Then
            intTranCode = 1
        ElseIf oPOSHeader.TransactionCode = "SA" And _
            oPOSHeader.TotalSaleAmount < 0 Then
            intTranCode = 2
        ElseIf oPOSHeader.TransactionCode = "RF" And _
            oPOSHeader.TotalSaleAmount <= 0 Then
            intTranCode = 3
        ElseIf oPOSHeader.TransactionCode = "RF" And _
            oPOSHeader.TotalSaleAmount > 0 Then
            intTranCode = 4
        ElseIf oPOSHeader.TransactionCode = "TS" And _
            oPOSHeader.TotalSaleAmount >= 0 Then
            intTranCode = 1
        ElseIf oPOSHeader.TransactionCode = "TS" And _
            oPOSHeader.TotalSaleAmount < 0 Then
            intTranCode = 2
        ElseIf oPOSHeader.TransactionCode = "TR" And _
            oPOSHeader.TotalSaleAmount <= 0 Then
            intTranCode = 3
        ElseIf oPOSHeader.TransactionCode = "TR" And _
            oPOSHeader.TotalSaleAmount > 0 Then
            intTranCode = 4
        ElseIf oPOSHeader.TransactionCode = "M+" Or _
            oPOSHeader.TransactionCode = "C+" Then
            intTranCode = 5
            macurMiscIncomes(CInt(oPOSHeader.ReasonCode)) = _
                macurMiscIncomes(CInt(oPOSHeader.ReasonCode)) + _
                oPOSHeader.NonMerchandiseAmount
        ElseIf oPOSHeader.TransactionCode = "M-" Or _
            oPOSHeader.TransactionCode = "C-" Then
            intTranCode = 6
            macurMiscOutgoings(CInt(oPOSHeader.ReasonCode)) = _
                macurMiscOutgoings(CInt(oPOSHeader.ReasonCode)) - _
                oPOSHeader.NonMerchandiseAmount
        End If
        
        If intTranCode <> 0 Then
            mcurCTGrossSales = mcurCTGrossSales + oPOSHeader.TotalSaleAmount
            mcurCTCashDiscountAmount = mcurCTCashDiscountAmount + oPOSHeader.DiscountAmount
            maintCTTransCodeCounts(intTranCode) = maintCTTransCodeCounts(intTranCode) + 1
            macurCTTransCodeAmounts(intTranCode) = _
                macurCTTransCodeAmounts(intTranCode) + oPOSHeader.TotalSaleAmount
            macurCTTransCodeDiscountAmounts(intTranCode) = _
                macurCTTransCodeDiscountAmounts(intTranCode) + oPOSHeader.DiscountAmount

' Add up the tender type totals for this transaction
            Set oPOSPayment = goDatabase.CreateBusinessObject(CLASSID_POSPAYMENT)
        
            Call oPOSPayment.IBo_AddLoadField(FID_POSPAYMENT_TenderAmount)      'AMNT
            Call oPOSPayment.IBo_AddLoadField(FID_POSPAYMENT_TenderType)        'TYPE
            Call oPOSPayment.IBo_AddLoadField(FID_POSPAYMENT_CreditCardNumber)  'CCNO
            Call oPOSPayment.IBo_AddLoadField(FID_POSPAYMENT_AuthorisationType) 'ATYP
        
            Call oPOSPayment.IBo_AddLoadFilter(CMP_EQUAL, FID_POSPAYMENT_TransactionDate, oPOSHeader.TranDate)
            Call oPOSPayment.IBo_AddLoadFilter(CMP_EQUAL, FID_POSPAYMENT_TillID, oPOSHeader.TillID)
            Call oPOSPayment.IBo_AddLoadFilter(CMP_EQUAL, FID_POSPAYMENT_TransactionNumber, oPOSHeader.TransactionNo)
            Call oPOSPayment.IBo_AddLoadFilter(CMP_NOTEQUAL, FID_POSPAYMENT_TenderAmount, 0)
            
            Set colPOSPayment = oPOSPayment.IBo_LoadMatches()
        
            macurCTTenderTypeAmounts(1) = macurCTTenderTypeAmounts(1) + _
                oPOSHeader.TotalSaleAmount
        
            For Each oPOSPayment In colPOSPayment
Debug.Print "DLPAID: " & oPOSPayment.TransactionDate & " " & oPOSPayment.TransactionNumber & " " & _
    oPOSPayment.TenderAmount
                
                If oPOSPayment.TenderType >= 2 And oPOSPayment.TenderType <= 20 Then
                    If oPOSPayment.TenderType = 2 And _
                        oPOSHeader.TransactionCode = "RF" And _
                        oPOSHeader.TotalSaleAmount < 0 Then
                        oPOSPayment.TenderType = oPOSPayment.TenderType + 20
                    End If
                    
                    If oPOSPayment.TenderType = 2 And _
                        oPOSHeader.TransactionCode = "TR" And _
                        oPOSHeader.TotalSaleAmount < 0 Then
                        oPOSPayment.TenderType = oPOSPayment.TenderType + 20
                    End If
                    
                    If oPOSPayment.CreditCardNumber = String$(19, "0") And _
                        oPOSPayment.TenderType <> 10 And _
                        oPOSPayment.TenderType <> 2 And _
                        LenB(Trim$(oPOSPayment.AuthorisationType)) = 0 Then
                        oPOSPayment.TenderType = oPOSPayment.TenderType + 20
                    End If
                    
                    If oPOSHeader.TransactionCode = "SA" And _
                        oPOSHeader.TotalSaleAmount < 0 Then
                        maintCTTenderTypeCounts(oPOSPayment.TenderType) = _
                            maintCTTenderTypeCounts(oPOSPayment.TenderType) - 1
                    ElseIf oPOSHeader.TransactionCode = "RF" And _
                        oPOSHeader.TotalSaleAmount > 0 Then
                        maintCTTenderTypeCounts(oPOSPayment.TenderType) = _
                            maintCTTenderTypeCounts(oPOSPayment.TenderType) - 1
                    ElseIf oPOSHeader.TransactionCode = "TS" And _
                        oPOSHeader.TotalSaleAmount < 0 Then
                        maintCTTenderTypeCounts(oPOSPayment.TenderType) = _
                            maintCTTenderTypeCounts(oPOSPayment.TenderType) - 1
                    ElseIf oPOSHeader.TransactionCode = "TR" And _
                        oPOSHeader.TotalSaleAmount > 0 Then
                        maintCTTenderTypeCounts(oPOSPayment.TenderType) = _
                            maintCTTenderTypeCounts(oPOSPayment.TenderType) - 1
                    Else
                        maintCTTenderTypeCounts(oPOSPayment.TenderType) = _
                            maintCTTenderTypeCounts(oPOSPayment.TenderType) + 1
                    End If
                    
                    macurCTTenderTypeAmounts(oPOSPayment.TenderType) = _
                        macurCTTenderTypeAmounts(oPOSPayment.TenderType) - _
                        oPOSHeader.TenderAmount
                    macurCTTenderTypeAmounts(1) = macurCTTenderTypeAmounts(1) + _
                        oPOSHeader.TenderAmount
                End If
            Next
        End If
    Next

    If LenB(strSaveCashTill) <> 0 Then
        Call UpdateCashierCashBal(strSaveCashTill)
    End If
    
' Update CBSCTL
    Call UpdateCashBalanceControl
    
'Commit transaction
    goDatabase.CommitTransaction
    
    If frmCashierBalancingUpdate.Visible Then
        frmCashierBalancingUpdate.strStatusMessage = BLANK_MESSAGE
    End If
    
    Exit Sub
    
errHandler:

    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)
    End
End Sub

' This routine clears the cashier/till totals variables
Private Sub ClearCTTotals()
Dim intCounter As Integer

    mcurCTGrossSales = 0
    mcurCTCashDiscountAmount = 0

    For intCounter = 1 To 8
        maintCTTransCodeCounts(intCounter) = 0
        macurCTTransCodeAmounts(intCounter) = 0
        macurCTTransCodeDiscountAmounts(intCounter) = 0
    Next

    For intCounter = 1 To 40
        maintCTTenderTypeCounts(intCounter) = 0
        macurCTTenderTypeAmounts(intCounter) = 0
    Next
End Sub

' This routine clears the miscellaneous in & out totals variables
Private Sub ClearMiscTotals()
Dim intCounter As Integer

    For intCounter = 1 To 10
        macurMiscIncomes(intCounter) = 0
        macurMiscOutgoings(intCounter) = 0
    Next
End Sub

Private Sub UpdateCashierCashBal(ByVal strSaveCashTill As String)
Dim oCashierCashBal As Object
Dim oRow            As Object
Dim blnAdding       As Boolean
Dim intI            As Integer

    Set oCashierCashBal = goDatabase.CreateBusinessObject(CLASSID_CASHIERCASHBAL)
    
    'Only load necessary fields
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_TotalsDate)            'DATE
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_CashierNumber)         'NUMB
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_GrossSales)            'GROS
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_DiscountAmount)        'DISC
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_NoOfTranTypes)         'COUN
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_TranTypeTotals)        'AMNT
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_TranDiscountTotals)    'DISA
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_FloatAmount)           'FLOT
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_PickUpTotal)           'PTOT
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_TenderTypeOccurence)   'STTC
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_TenderTypeAmount)      'STTA
    
    Call oCashierCashBal.IBo_AddLoadFilter(CMP_EQUAL, _
        FID_CASHIERCASHBAL_TotalsDate, mdteRRReportsDate)
    Call oCashierCashBal.IBo_AddLoadFilter(CMP_EQUAL, _
        FID_CASHIERCASHBAL_CashierNumber, strSaveCashTill)
    
    'Load Match
    Call oCashierCashBal.IBo_LoadMatches
    
    'Have we got any results
    If LenB(oCashierCashBal.TotalsDate) = 0 Then
        blnAdding = True
        oCashierCashBal.TotalsDate = mdteRRReportsDate
        oCashierCashBal.CashierNumber = strSaveCashTill
    Else
        blnAdding = False
    End If

    oCashierCashBal.GrossSales = mcurCTGrossSales
    oCashierCashBal.DiscountAmount = mcurCTCashDiscountAmount
    
    For intI = 1 To 8
        oCashierCashBal.NoOfTranTypes(intI) = maintCTTransCodeCounts(intI)
        oCashierCashBal.TranTypeTotals(intI) = macurCTTransCodeAmounts(intI)
        oCashierCashBal.TranDiscountTotals(intI) = macurCTTransCodeDiscountAmounts(intI)
    Next
    
    For intI = 1 To 40
        oCashierCashBal.TenderTypeOccurence(intI) = maintCTTenderTypeCounts(intI)
        oCashierCashBal.TenderTypeAmount(intI) = macurCTTenderTypeAmounts(intI)
    Next
    
    Set oRow = goSession.Root.CreateUtilityObject("CRow")
    
    'Add the primary keys first
    Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_TotalsDate))
    Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_CashierNumber))
        
    'Add the updated field
    Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_GrossSales))
    Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_DiscountAmount))
    Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_NoOfTranTypes))
    Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_TranTypeTotals))
    Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_TranDiscountTotals))
    Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_FloatAmount))
    Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_PickUpTotal))
    Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_TenderTypeOccurence))
    Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_TenderTypeAmount))
    
    'Save the row
    If blnAdding = True Then
        Call oCashierCashBal.IBo_SaveIfNew
    Else
        Call oCashierCashBal.IBo_SaveIfExists
    End If

    Call ClearCTTotals

    Set oRow = Nothing
    Set oCashierCashBal = Nothing
End Sub

Private Sub UpdateCashBalanceControl()
Dim oCashBalanceControl As Object
Dim oRow                As Object
Dim blnAdding           As Boolean
Dim intI                As Integer

    Set oCashBalanceControl = goDatabase.CreateBusinessObject(CLASSID_CASHBALANCECONTROL)
    
    'Only load necessary fields
    Call oCashBalanceControl.IBo_AddLoadField(FID_CASHBALANCECONTROL_ControlDate)   'DATE
    Call oCashBalanceControl.IBo_AddLoadField(FID_CASHBALANCECONTROL_OtherIncome)   'INVA
    Call oCashBalanceControl.IBo_AddLoadField(FID_CASHBALANCECONTROL_MiscOutgoings) 'OUVA
    
    Call oCashBalanceControl.IBo_AddLoadFilter(CMP_EQUAL, _
        FID_CASHBALANCECONTROL_ControlDate, mdteRRReportsDate)
    
    'Load Match
    Call oCashBalanceControl.IBo_LoadMatches
    
    'Have we got any results
    If LenB(oCashBalanceControl.ControlDate) = 0 Then
        blnAdding = True
        oCashBalanceControl.ControlDate = mdteRRReportsDate
    Else
        blnAdding = False
    End If

    For intI = 1 To 10
        oCashBalanceControl.OtherIncome(intI) = macurMiscIncomes(intI)
        oCashBalanceControl.MiscOutgoings(intI) = macurMiscOutgoings(intI)
    Next
    
    Set oRow = goSession.Root.CreateUtilityObject("CRow")
    
    'Add the primary keys first
    Call oRow.Add(oCashBalanceControl.IBo_GetField(FID_CASHBALANCECONTROL_ControlDate))
        
    'Add the updated field
    Call oRow.Add(oCashBalanceControl.IBo_GetField(FID_CASHBALANCECONTROL_OtherIncome))
    Call oRow.Add(oCashBalanceControl.IBo_GetField(FID_CASHBALANCECONTROL_MiscOutgoings))
    
    'Save the row
    If blnAdding = True Then
        Call oCashBalanceControl.IBo_SaveIfNew
    Else
        Call oCashBalanceControl.IBo_SaveIfExists
    End If

    Set oRow = Nothing
    Set oCashBalanceControl = Nothing
End Sub

Public Sub ReverseCashierBalancingUpdate()

Const PROCEDURE_NAME As String = MODULE_NAME & ".ReverseCashierBalancingUpdate"

Dim oCashBalanceControl As Object
Dim oCashierCashBal     As Object
Dim colCashierCashBal   As Collection
Dim oRow                As Object
Dim intI                As Integer

    On Error GoTo errHandler
    
    Call ReadRetailOptions
    
    If mstrAccountabilityType = "T" Then
        frmCashierBalancingUpdate.strCashierLabel = "for Till:"
    ElseIf mstrAccountabilityType = "C" Then
        frmCashierBalancingUpdate.strCashierLabel = "for Cashier:"
    Else
        Err.Raise OASYS_ERR_MISSING_RETAILEROPTIONS, PROCEDURE_NAME, "Invalid Accountability Type"
    End If
    
    Set oCashBalanceControl = goDatabase.CreateBusinessObject(CLASSID_CASHBALANCECONTROL)
    
    'Only load necessary fields
    Call oCashBalanceControl.IBo_AddLoadField(FID_CASHBALANCECONTROL_ControlDate)   'DATE
    Call oCashBalanceControl.IBo_AddLoadField(FID_CASHBALANCECONTROL_DayBalanced)   'DONE
    Call oCashBalanceControl.IBo_AddLoadField(FID_CASHBALANCECONTROL_TotalDeposits) 'TDEP
    Call oCashBalanceControl.IBo_AddLoadField(FID_CASHBALANCECONTROL_OtherIncome)   'INVA
    Call oCashBalanceControl.IBo_AddLoadField(FID_CASHBALANCECONTROL_MiscOutgoings) 'OUVA
    
    Call oCashBalanceControl.IBo_AddLoadFilter(CMP_EQUAL, _
        FID_CASHBALANCECONTROL_ControlDate, mdteRRReportsDate)
    
    'Load Match
    Call oCashBalanceControl.IBo_LoadMatches
    
    'Have we got any results
    If LenB(oCashBalanceControl.ControlDate) <> 0 Then
        If oCashBalanceControl.DayBalanced = False Then
            MsgBox "Date Balanced - Activate Date for Entry First.", vbExclamation, MODULE_FUNCTION
            End
        End If
    End If
    
    'Start transaction
    goDatabase.StartTransaction
    
    If LenB(oCashBalanceControl.ControlDate) <> 0 Then
        'Reverse Control Record
        If oCashBalanceControl.TotalDeposits <> 0 Then
            mblnDidReversal = True
        End If
        
        For intI = 1 To 10
            oCashBalanceControl.OtherIncome(intI) = 0
            oCashBalanceControl.MiscOutgoings(intI) = 0
        Next
    
        oCashBalanceControl.TotalDeposits = 0
        
        Set oRow = goSession.Root.CreateUtilityObject("CRow")
        
        'Add the primary keys first
        Call oRow.Add(oCashBalanceControl.IBo_GetField(FID_CASHBALANCECONTROL_ControlDate))
            
        'Add the updated field
        Call oRow.Add(oCashBalanceControl.IBo_GetField(FID_CASHBALANCECONTROL_OtherIncome))
        Call oRow.Add(oCashBalanceControl.IBo_GetField(FID_CASHBALANCECONTROL_MiscOutgoings))
        Call oRow.Add(oCashBalanceControl.IBo_GetField(FID_CASHBALANCECONTROL_TotalDeposits))
        
        'Save the row
        Call oCashBalanceControl.IBo_SaveIfExists
    End If

    'Reverse Cashier Records
    Set oCashierCashBal = goDatabase.CreateBusinessObject(CLASSID_CASHIERCASHBAL)
    
    'Only load necessary fields
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_TotalsDate)            'DATE
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_CashierNumber)         'NUMB
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_GrossSales)            'GROS
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_DiscountAmount)        'DISC
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_NoOfTranTypes)         'COUN
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_TranTypeTotals)        'AMNT
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_TranDiscountTotals)    'DISA
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_FloatAmount)           'FLOT
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_PickUpTotal)           'PTOT
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_TenderTypeOccurence)   'STTC
    Call oCashierCashBal.IBo_AddLoadField(FID_CASHIERCASHBAL_TenderTypeAmount)      'STTA
    
    Call oCashierCashBal.IBo_AddLoadFilter(CMP_EQUAL, _
        FID_CASHIERCASHBAL_TotalsDate, mdteRRReportsDate)
    
    'Load Match
    Set colCashierCashBal = oCashierCashBal.IBo_LoadMatches()
    
    For Each oCashierCashBal In colCashierCashBal
        mblnDidReversal = True
        
        'Update the Form if it's visible
        If frmCashierBalancingUpdate.Visible Then
            frmCashierBalancingUpdate.strCashier = oCashierCashBal.CashierNumber
        End If
    
        oCashierCashBal.GrossSales = 0
        oCashierCashBal.DiscountAmount = 0
        
        For intI = 1 To 8
            oCashierCashBal.NoOfTranTypes(intI) = 0
            oCashierCashBal.TranTypeTotals(intI) = 0
            oCashierCashBal.TranDiscountTotals(intI) = 0
        Next
        
        For intI = 1 To 40
            oCashierCashBal.TenderTypeOccurence(intI) = 0
            oCashierCashBal.TenderTypeAmount(intI) = 0
        Next
        
        Set oRow = goSession.Root.CreateUtilityObject("CRow")
        
        'Add the primary keys first
        Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_TotalsDate))
        Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_CashierNumber))
            
        'Add the updated field
        Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_GrossSales))
        Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_DiscountAmount))
        Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_NoOfTranTypes))
        Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_TranTypeTotals))
        Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_TranDiscountTotals))
        Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_FloatAmount))
        Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_PickUpTotal))
        Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_TenderTypeOccurence))
        Call oRow.Add(oCashierCashBal.IBo_GetField(FID_CASHIERCASHBAL_TenderTypeAmount))
        
        'Save the row
        Call oCashierCashBal.IBo_SaveIfExists
    Next

    'Commit transaction
    goDatabase.CommitTransaction
    
    Set oRow = Nothing
    Set oCashBalanceControl = Nothing
    Set oCashierCashBal = Nothing
    
    If frmCashierBalancingUpdate.Visible Then
        frmCashierBalancingUpdate.strStatusMessage = BLANK_MESSAGE
    End If
    
    Exit Sub
    
errHandler:

    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)
    End
End Sub

Private Sub ReadRetailOptions()
Dim oRetailOptions  As Object
    
    'Create retail options business object
    Set oRetailOptions = goDatabase.CreateBusinessObject(CLASSID_RETAILEROPTIONS)
    
    'Only load necessary fields
    Call oRetailOptions.IBo_AddLoadField(FID_RETAILEROPTIONS_AccountabilityType)
    Call oRetailOptions.IBo_AddLoadField(FID_RETAILEROPTIONS_RRReportsDate)
    
    'Load Match
    Call oRetailOptions.IBo_LoadMatches
    
    mstrAccountabilityType = oRetailOptions.AccountabilityType
    mdteRRReportsDate = oRetailOptions.RRReportsDate
    
'MsgBox "TODO: Take this out"
'    mdteRRReportsDate = "2/11/2004"
    
    Set oRetailOptions = Nothing
End Sub

Private Sub Successful_End()
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Erase file so that close program will know that this program
' completed successfully
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If LenB(Dir("\TASKCOMP")) <> 0 Then
        Kill ("\TASKCOMP")
    End If
End Sub
