VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmCashierBalancingUpdate 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cashier Balancing Update"
   ClientHeight    =   3270
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5955
   ControlBox      =   0   'False
   Icon            =   "frmCashierBalancingUpdate.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3270
   ScaleWidth      =   5955
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   3
      Top             =   2895
      Width           =   5955
      _ExtentX        =   10504
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmCashierBalancingUpdate.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   3149
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "04-May-04"
            TextSave        =   "04-May-04"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "12:23 PM"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblReverse 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "REVERSE"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   4500
      TabIndex        =   5
      Top             =   1140
      Visible         =   0   'False
      Width           =   2130
   End
   Begin VB.Label lblProcessingCashier 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "for Cashier:"
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   2640
      TabIndex        =   4
      Top             =   1140
      Width           =   870
   End
   Begin VB.Label lblTransaction 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   225
      Left            =   1920
      TabIndex        =   2
      Top             =   1140
      Width           =   585
   End
   Begin VB.Label lblCashier 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   225
      Left            =   3600
      TabIndex        =   1
      Top             =   1140
      Width           =   570
   End
   Begin VB.Label lblProcessingTransaction 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Processing Transaction:"
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   1140
      Width           =   1710
   End
End
Attribute VB_Name = "frmCashierBalancingUpdate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim mlngUpdates As Long
Dim mlngUpdateInterval As Long

Public Property Let lngUpdateInterval(lngValue As Long)
    
    mlngUpdateInterval = lngValue
    mlngUpdates = 0

End Property


Public Property Let strCashierLabel(strValue As String)

    lblProcessingCashier.Caption = strValue

End Property

Public Property Let strCashier(strValue As String)

    lblCashier.Caption = strValue

End Property

Public Property Let strTransaction(strValue As String)
    
    lblTransaction.Caption = strValue
    If mlngUpdates = 0 Then
        DoEvents
    End If
    If mlngUpdateInterval <> 0 Then
        mlngUpdates = (mlngUpdates + 1) Mod mlngUpdateInterval
    End If

End Property

Public Property Let strStatusMessage(strValue As String)

    sbStatus.Panels(3) = strValue
    DoEvents

End Property


Private Sub Form_Load()
    
    Call CentreForm(Me)

    ' Setup Display Colors
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    
    lblTransaction.BackColor = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
    lblCashier.BackColor = lblTransaction.BackColor
    
    Call InitialiseStatusBar(sbStatus)

End Sub

Public Property Let blnReverseVisible(blnValue As Boolean)
    
    lblReverse.Visible = blnValue
    
End Property
