REM - 27.03.12 - aln - Added HHTLabelCommon.dll registration
REM - 28.07.11 - aln - Reorganise 3rd party binary registration; add bin folder parameter; renamed batch; removed 'silent' switch from regsvr32; all for Continuous Integration project
REM - 26.07.11 - aln - Removed pause comand from end
REM - 27.06.11 - aln - Re-ordered accoring to file dependancy
REM - 24.01.11 - aln - Added COMOrderManager regasm command
REM - 18.10.10 - aln - Added missing and arranged alphabetically
REM - 10.09.10 - aln - Added ComLink.dll
REM - 21.07.10 - aln - Created
REM - Unregister all files registered by RegAll.bat.  A separate file so can be run separately.
REM - IE When working in a branched version of code and need to unregister files in a different branch
REM - before registering in the working branch
REM
REM Version 4
REM

Set Bin_Folder=%~1

rem 3rd party components
regsvr32 /u "%Bin_Folder%\SPR32X60.ocx"
regsvr32 /u "%Bin_Folder%\spr32d60.dll"
regsvr32 /u "%Bin_Folder%\OPOSPOSPrinter.ocx"
regsvr32 /u "%Bin_Folder%\OPOSCashDrawer.ocx"
regsvr32 /u "%Bin_Folder%\FPSPR70.ocx"
regsvr32 /u "%Bin_Folder%\fpSpr60.ocx"
regsvr32 /u "%Bin_Folder%\fpFlp30.ocx"
regsvr32 /u "%Bin_Folder%\Flp32d30.dll"
regsvr32 /u "%Bin_Folder%\Flp32a30.ocx"
regsvr32 /u "%Bin_Folder%\SS60PP.dll"
regsvr32 /u "%Bin_Folder%\ccrpprg6.ocx"
regsvr32 /u "%Bin_Folder%\mcdapi32.ocx"
regsvr32 /u "%Bin_Folder%\MSSOAP30.dll"
regsvr32 /u "%Bin_Folder%\TABCTL32.OCX"
regsvr32 /u "%Bin_Folder%\mswinsck.ocx"
regsvr32 /u "%Bin_Folder%\msstkprp.dll"
regsvr32 /u "%Bin_Folder%\MSSTDFMT.DLL"
regsvr32 /u "%Bin_Folder%\mscomm32.ocx"
regsvr32 /u "%Bin_Folder%\Mscomctl.ocx"
regsvr32 /u "%Bin_Folder%\mscomct2.ocx"
regsvr32 /u "%Bin_Folder%\MSADODC.OCX"
regsvr32 /u "%Bin_Folder%\COMDLG32.OCX"

rem Highest dependancy level (depends on some or all below)
regsvr32 /u "%Bin_Folder%\CaptureCustUC.ocx"
regsvr32 /u "%Bin_Folder%\cCashBalanceBO.dll"
regsvr32 /u "%Bin_Folder%\cOPEvents_Wickes.dll"
regsvr32 /u "%Bin_Folder%\cPrintQuote_Wickes.dll"
regsvr32 /u "%Bin_Folder%\cReturns.dll"
regsvr32 /u "%Bin_Folder%\cSummaries.dll"
regsvr32 /u "%Bin_Folder%\cTillPrompts.dll"
regsvr32 /u "%Bin_Folder%\CTSEOrderProcess.dll"
regsvr32 /u "%Bin_Folder%\CTSHOHSFileProcess.dll"
regsvr32 /u "%Bin_Folder%\CTSOasysTranslator.dll"
regsvr32 /u "%Bin_Folder%\EstoreImport4dotNet.dll"
regsvr32 /u "%Bin_Folder%\HotkeyList.ocx"
regsvr32 /u "%Bin_Folder%\ItemFilter.ocx"
regsvr32 /u "%Bin_Folder%\LogicTender.dll"
rem COMLink is 3rd party but might be needed until logictender is unregistered
regsvr32 /u "%Bin_Folder%\COMLink.dll"
regsvr32 /u "%Bin_Folder%\NumTextControl.ocx"
regsvr32 /u "%Bin_Folder%\OA2Logon.dll"
regsvr32 /u "%Bin_Folder%\OasysClientGeneric.dll"
regsvr32 /u "%Bin_Folder%\OASYSMenu.dll"
regsvr32 /u "%Bin_Folder%\OrderFilter.ocx"
regsvr32 /u "%Bin_Folder%\TillOrderItem.ocx"
regsvr32 /u "%Bin_Folder%\TillTranRetrieve.ocx"
regsvr32 /u "%Bin_Folder%\ucPostCodeEntry.ocx"
regsvr32 /u "%Bin_Folder%\Wsscommidea.dll"
rem Next dependancy level (depends on some or all below)
regsvr32 /u "%Bin_Folder%\cEOrderSalesBO.dll"
regsvr32 /u "%Bin_Folder%\cLookUps.dll"
regsvr32 /u "%Bin_Folder%\cReceiptPrint.dll"
regsvr32 /u "%Bin_Folder%\cTillEvents_Wickes.dll"
regsvr32 /u "%Bin_Folder%\CTSProgBar.ocx"
regsvr32 /u "%Bin_Folder%\KeyPadUC.ocx"
regsvr32 /u "%Bin_Folder%\OasysSqlDatabaseCom.dll"
regsvr32 /u "%Bin_Folder%\WickesLibrary.dll"
rem Next dependancy level (depends on some or all below)
regsvr32 /u "%Bin_Folder%\cCustomerOrders.dll"
regsvr32 /u "%Bin_Folder%\cCustResBO.dll"
regsvr32 /u "%Bin_Folder%\cInputBoxEx.dll"
regsvr32 /u "%Bin_Folder%\cPurchase.dll"
regsvr32 /u "%Bin_Folder%\cSale.dll"
regsvr32 /u "%Bin_Folder%\cSalesTotals.dll"
regsvr32 /u "%Bin_Folder%\EditDateCtl.ocx"
regsvr32 /u "%Bin_Folder%\EditNumberTill.ocx"
rem Next dependancy level (depends on some or all below)
regsvr32 /u "%Bin_Folder%\cInventory.dll"
regsvr32 /u "%Bin_Folder%\cMsgBoxEx.dll"
regsvr32 /u "%Bin_Folder%\cOPEvents.dll"
regsvr32 /u "%Bin_Folder%\cSalesOrder.dll"
regsvr32 /u "%Bin_Folder%\HHTLabelCommon.dll"
rem Next dependancy level (depends on some or all below)
regsvr32 /u "%Bin_Folder%\cEnterprise.dll"
regsvr32 /u "%Bin_Folder%\EditNumberCtl.ocx"
rem Next dependancy level (depends on some or all below)
regsvr32 /u "%Bin_Folder%\OasysStartCom.dll"
rem Next dependancy level (depends on some or all below)
regsvr32 /u "%Bin_Folder%\OasysRootCom.dll"
rem Next dependancy level (depends on some or all below)
regsvr32 /u "%Bin_Folder%\OasysDbInterfaceCom.dll"
rem Next dependancy level (depends on some or all below)
regsvr32 /u "%Bin_Folder%\OasysInterfaces.dll"
rem Next dependancy level (depends on some or all below)
regsvr32 /u "%Bin_Folder%\OasysCBaseCom.dll"
rem Lowest dependancy level (depends on nothing here)
regsvr32 /u "%Bin_Folder%\VbUtils.dll"

regasm /u COMOrderManager.dll /tlb /codebase
