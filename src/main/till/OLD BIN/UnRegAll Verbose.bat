REM - 27.03.12 - aln - Added HHTLabelCommon.dll registration
REM - 26.07.11 - aln - Removed pause comand from end
REM - 27.06.11 - aln - Re-ordered accoring to file dependancy
REM - 24.01.11 - aln - Added COMOrderManager regasm command
REM - 18.10.10 - aln - Added missing and arranged alphabetically
REM - 10.09.10 - aln - Added ComLink.dll
REM - 21.07.10 - aln - Created
REM - Unregister all files registered by RegAll.bat.  A separate file so can be run separately.
REM - IE When working in a branched version of code and need to unregister files in a different branch
REM - before registering in the working branch
REM
REM Version 3
REM
rem 3rd party components
regsvr32 /u "ccrpprg6.ocx"
regsvr32 /u "SS60PP.dll"
regsvr32 /u "Flp32a30.ocx"
regsvr32 /u "Flp32d30.dll"
regsvr32 /u "fpFlp30.ocx"
regsvr32 /u "fpSpr60.ocx"
regsvr32 /u "FPSPR70.ocx"
regsvr32 /u "OPOSCashDrawer.ocx"
regsvr32 /u "OPOSPOSPrinter.ocx"
regsvr32 /u "spr32d60.dll"
regsvr32 /u "SPR32X60.ocx"
regsvr32 /u "COMDLG32.OCX"
regsvr32 /u "mcdapi32.ocx"
regsvr32 /u "MSADODC.OCX"
regsvr32 /u "mscomct2.ocx"
regsvr32 /u "Mscomctl.ocx"
regsvr32 /u "mscomm32.ocx"
regsvr32 /u "MSSOAP30.dll"
regsvr32 /u "MSSTDFMT.DLL"
regsvr32 /u "msstkprp.dll"
regsvr32 /u "mswinsck.ocx"
regsvr32 /u "TABCTL32.OCX"
rem Highest dependancy level (depends on some or all below)
regsvr32 /u "CaptureCustUC.ocx"
regsvr32 /u "cCashBalanceBO.dll"
regsvr32 /u "cOPEvents_Wickes.dll"
regsvr32 /u "cPrintQuote_Wickes.dll"
regsvr32 /u "cReturns.dll"
regsvr32 /u "cSummaries.dll"
regsvr32 /u "cTillPrompts.dll"
regsvr32 /u "CTSEOrderProcess.dll"
regsvr32 /u "CTSHOHSFileProcess.dll"
regsvr32 /u "CTSOasysTranslator.dll"
regsvr32 /u "EstoreImport4dotNet.dll"
regsvr32 /u "HotkeyList.ocx"
regsvr32 /u "ItemFilter.ocx"
regsvr32 /u "LogicTender.dll"
rem COMLink is 3rd party but might be needed until logictender is unregistered
regsvr32 /u "COMLink.dll"
regsvr32 /u "NumTextControl.ocx"
regsvr32 /u "OA2Logon.dll"
regsvr32 /u "OasysClientGeneric.dll"
regsvr32 /u "OASYSMenu.dll"
regsvr32 /u "OrderFilter.ocx"
regsvr32 /u "TillOrderItem.ocx"
regsvr32 /u "TillTranRetrieve.ocx"
regsvr32 /u "ucPostCodeEntry.ocx"
regsvr32 /u "Wsscommidea.dll"
rem Next dependancy level (depends on some or all below)
regsvr32 /u "cEOrderSalesBO.dll"
regsvr32 /u "cLookUps.dll"
regsvr32 /u "cReceiptPrint.dll"
regsvr32 /u "cTillEvents_Wickes.dll"
regsvr32 /u "CTSProgBar.ocx"
regsvr32 /u "KeyPadUC.ocx"
regsvr32 /u "OasysSqlDatabaseCom.dll"
regsvr32 /u "WickesLibrary.dll"
rem Next dependancy level (depends on some or all below)
regsvr32 /u "cCustomerOrders.dll"
regsvr32 /u "cCustResBO.dll"
regsvr32 /u "cInputBoxEx.dll"
regsvr32 /u "cPurchase.dll"
regsvr32 /u "cSale.dll"
regsvr32 /u "cSalesTotals.dll"
regsvr32 /u "EditDateCtl.ocx"
regsvr32 /u "EditNumberTill.ocx"
rem Next dependancy level (depends on some or all below)
regsvr32 /u "cInventory.dll"
regsvr32 /u "cMsgBoxEx.dll"
regsvr32 /u "cOPEvents.dll"
regsvr32 /u "cSalesOrder.dll"
regsvr32 /u "HHTLabelCommon.dll"
rem Next dependancy level (depends on some or all below)
regsvr32 /u "cEnterprise.dll"
regsvr32 /u "EditNumberCtl.ocx"
rem Next dependancy level (depends on some or all below)
regsvr32 /u "OasysStartCom.dll"
rem Next dependancy level (depends on some or all below)
regsvr32 /u "OasysRootCom.dll"
rem Next dependancy level (depends on some or all below)
regsvr32 /u "OasysDbInterfaceCom.dll"
rem Next dependancy level (depends on some or all below)
regsvr32 /u "OasysInterfaces.dll"
rem Next dependancy level (depends on some or all below)
regsvr32 /u "OasysCBaseCom.dll"
rem Lowest dependancy level (depends on nothing here)
regsvr32 /u "VbUtils.dll"

regasm /u COMOrderManager.dll /tlb /codebase
regasm /u COMTPWickes.InterOp.Interface.dll /tlb /codebase
regasm /u COMTPWickes.InterOp.Wrapper.dll /tlb /codebase
regasm /u COMTPWickes.InterOp.Implementation.dll /tlb /codebase
