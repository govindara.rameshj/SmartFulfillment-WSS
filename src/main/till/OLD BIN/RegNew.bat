@echo off
cls
color 1f

rem 1.0 KM - New Base Registry Batch
rem 1.1 KM - Added WickesLibrary.dll
rem 1.2 KM - Re-Organised to Match UPTRAC version
rem 1.3 KM - Post Deployment Failure (LIVE ESTATE) - Improvements to Process

:Display
rem -------------------------------------- Display Start Dialog ---------------------------------------
echo.
echo   浜様様様様様様様様様様様様様様様様様様様様様様様様様様様�
echo   �                                                       �
echo   �            Starting Software Registration             �
echo   �                                                       �
echo   麺様様様様様様様様様様様様様様様様様様様様様様様様様様様�
echo   �                                                       �
echo   �                      Please wait...                   �
echo   �   This process can take a few minutes to complete     �
echo   �                                                       �
echo   �         ** DO NOT EXIT OR STOP THIS PROCESS **        �
echo   �                                                       �
echo   藩様様様様様様様様様様様様様様様様様様様様様様様様様様様� 
echo   KM 1.3
echo.
wait 1
Set /a FailRegister=0
Set PPath="C:\Program Files\CTS Retail\Oasys3"
Set WPath="C:\Windows\System32"
echo %date% > %temp%\sqldate.txt
for /f "tokens=1,2,3,4 delims=/ " %%a in (%temp%\sqldate.txt) do set SQLDate=%%c-%%b-%%a
regsvr32 /s "Test.dll"
if [%errorlevel%] EQU [5] (set callnext=none) & (set CanClose=TRUE) & (set ErrorLOCAL=WXASBO_ER_ERR43 - E43 Server REGSVR Blocked) & (set ErrorREMOTE=%nextupd% - Failed E43 REGSVR Server Fail) & (goto Error_Handling)
rem ---------------------------------------------------------------------------------------------------


rem ---------------------------------------------------------------------------------------------------
rem                           DE-REGISTER SOFTWARE BINARIES AND ASSEMBLEY FILES
rem ---------------------------------------------------------------------------------------------------


:RegisterSoftwareLoop1
rem ----------------------------------- DE-Register DLL's / OCX's -------------------------------------
Set /a Number=0
echo.
echo   Starting Software De-Registration..
echo.
:DeRegList
if [%Number%] EQU [0] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cCashBalanceBO.dll) & (goto Process)
if [%Number%] EQU [1] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cCustomerOrders.dll) & (goto Process)
if [%Number%] EQU [2] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cCustResBO.dll) & (goto Process)
if [%Number%] EQU [3] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cEnterprise.dll) & (goto Process)
if [%Number%] EQU [4] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cInputBoxEx.dll) & (goto Process)
if [%Number%] EQU [5] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cInventory.dll) & (goto Process)
if [%Number%] EQU [6] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cLookUps.dll) & (goto Process)
if [%Number%] EQU [7] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cMsgBoxEx.dll) & (goto Process)
if [%Number%] EQU [8] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=COMLink.dll) & (goto Process)
if [%Number%] EQU [9] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cOPEvents.dll) & (goto Process)
if [%Number%] EQU [10] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cOPEvents_Wickes.dll) & (goto Process)
if [%Number%] EQU [11] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cPurchase.dll) & (goto Process)
if [%Number%] EQU [12] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cReturns.dll) & (goto Process)
if [%Number%] EQU [13] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cReceiptPrint.dll) & (goto Process)
if [%Number%] EQU [14] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cSale.dll) & (goto Process)
if [%Number%] EQU [15] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cSalesOrder.dll) & (goto Process)
if [%Number%] EQU [16] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cSalesTotals.dll) & (goto Process)
if [%Number%] EQU [17] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cSummaries.dll) & (goto Process)
if [%Number%] EQU [18] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cTillEvents_Wickes.dll) & (goto Process)
if [%Number%] EQU [19] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cTillPrompts.dll) & (goto Process)
if [%Number%] EQU [20] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=CTSHOHSFileProcess.dll) & (goto Process)
if [%Number%] EQU [21] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=CTSOasysTranslator.dll) & (goto Process)
if [%Number%] EQU [22] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=Flp32d30.dll) & (goto Process)
if [%Number%] EQU [23] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=LogicTender.dll) & (goto Process)
if [%Number%] EQU [24] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=MSSTDFMT.DLL) & (goto Process)
if [%Number%] EQU [25] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=msstkprp.dll) & (goto Process)
if [%Number%] EQU [26] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=OA2Logon.dll) & (goto Process)
if [%Number%] EQU [27] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=OasysCBaseCom.dll) & (goto Process)
if [%Number%] EQU [28] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=OasysClientGeneric.dll) & (goto Process)
if [%Number%] EQU [29] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=OasysDbInterfaceCom.dll) & (goto Process)
if [%Number%] EQU [30] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=OasysInterfaces.dll) & (goto Process)
if [%Number%] EQU [31] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=OASYSMenu.dll) & (goto Process)
if [%Number%] EQU [32] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=OasysRootCom.dll) & (goto Process)
if [%Number%] EQU [33] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=OasysSqlDatabaseCom.dll) & (goto Process)
if [%Number%] EQU [34] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=OasysStartCom.dll) & (goto Process)
if [%Number%] EQU [35] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=spr32d60.dll) & (goto Process)
if [%Number%] EQU [36] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=SS60PP.dll) & (goto Process)
if [%Number%] EQU [37] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=VbUtils.dll) & (goto Process)
if [%Number%] EQU [38] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=WssCommidea.dll) & (goto Process)
if [%Number%] EQU [39] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=WickesLibrary.dll) & (goto Process)
if [%Number%] EQU [40] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=CaptureCustUC.ocx) & (goto Process)
if [%Number%] EQU [41] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=ccrpprg6.ocx) & (goto Process)
if [%Number%] EQU [42] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=COMDLG32.OCX) & (goto Process)
if [%Number%] EQU [43] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=CTSProgBar.ocx) & (goto Process)
if [%Number%] EQU [44] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=EditDateCtl.ocx) & (goto Process)
if [%Number%] EQU [45] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=EditNumberCtl.ocx) & (goto Process)
if [%Number%] EQU [46] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=EditNumberTill.ocx) & (goto Process)
if [%Number%] EQU [47] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=Flp32a30.ocx) & (goto Process)
if [%Number%] EQU [48] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=fpFlp30.ocx) & (goto Process)
if [%Number%] EQU [49] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=fpSpr60.ocx) & (goto Process)
if [%Number%] EQU [50] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=FPSPR70.ocx) & (goto Process)
if [%Number%] EQU [51] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=HotkeyList.ocx) & (goto Process)
if [%Number%] EQU [52] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=ItemFilter.ocx) & (goto Process)
if [%Number%] EQU [53] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=MSADODC.OCX) & (goto Process)
if [%Number%] EQU [54] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=McdApi32.ocx) & (goto Process)
if [%Number%] EQU [55] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=Mscomctl.ocx) & (goto Process)
if [%Number%] EQU [56] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=NumTextControl.ocx) & (goto Process)
if [%Number%] EQU [57] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=OPOSCashDrawer.ocx) & (goto Process)
if [%Number%] EQU [58] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=OPOSPOSPrinter.ocx) & (goto Process)
if [%Number%] EQU [59] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=OrderFilter.ocx) & (goto Process)
if [%Number%] EQU [60] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=SPR32X60.ocx) & (goto Process)
if [%Number%] EQU [61] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=TABCTL32.OCX) & (goto Process)
if [%Number%] EQU [62] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=TillOrderItem.ocx) & (goto Process)
if [%Number%] EQU [63] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=TillTranRetrieve.ocx) & (goto Process)
if [%Number%] EQU [64] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=ucPostCodeEntry.ocx) & (goto Process)
if [%Number%] EQU [65] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=CTSEOrderProcess.dll) & (goto Process)
if [%Number%] EQU [66] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cEOrderSalesBO.dll) & (goto Process)
if [%Number%] EQU [67] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=cPrintQuote_Wickes.dll) & (goto Process)
if [%Number%] EQU [68] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=KeyPadUC.ocx) & (goto Process)
if [%Number%] EQU [69] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=EstoreImport4dotNet.dll) & (goto Process)
if [%Number%] EQU [70] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=MSSOAP30.dll) & (goto Process)
if [%Number%] EQU [71] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=mswinsck.ocx) & (goto Process)
if [%Number%] EQU [72] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=mscomm32.ocx) & (goto Process)
if [%Number%] EQU [73] (Set CallBack=DeRegList) & (Set Mode=UnRegister) & (Set Type=Binary) & (Set File=mscomct2.ocx) & (goto Process)
wait 2
rem ---------------------------------------------------------------------------------------------------


rem ------------------------------------- DE-Register .NET Process ------------------------------------
Set /a Number=0
echo.
echo   Starting Order Manager and Till DotNET Software De-Registration..
echo.
:RSL2List
if [%Number%] EQU [0] (Set CallBack=RSL2List) & (Set Mode=UnRegister) & (Set Type=Assembly) & (Set File=COMOrderManager.dll) & (goto Process)
if [%Number%] EQU [1] (Set CallBack=RSL2List) & (Set Mode=UnRegister) & (Set Type=Assembly) & (Set File=COMTPWickes.InterOp.Interface.dll) & (goto Process)
if [%Number%] EQU [2] (Set CallBack=RSL2List) & (Set Mode=UnRegister) & (Set Type=Assembly) & (Set File=COMTPWickes.InterOp.Wrapper.dll) & (goto Process)
if [%Number%] EQU [3] (Set CallBack=RSL2List) & (Set Mode=UnRegister) & (Set Type=Assembly) & (Set File=COMTPWickes.InterOp.Implementation.dll) & (goto Process)
wait 2
rem ---------------------------------------------------------------------------------------------------


rem ---------------------------------------------------------------------------------------------------
rem                           REGISTER SOFTWARE BINARIES AND ASSEMBLEY FILES
rem ---------------------------------------------------------------------------------------------------


:RegisterSoftwareLoop3
rem ------------------------------------ Register DLL's / OCX's ---------------------------------------
Set /a Number=0
echo.
echo   Starting Software Registration..
echo.
:RegList
if [%Number%] EQU [0] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cCashBalanceBO.dll) & (goto Process)
if [%Number%] EQU [1] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cCustomerOrders.dll) & (goto Process)
if [%Number%] EQU [2] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cCustResBO.dll) & (goto Process)
if [%Number%] EQU [3] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cEnterprise.dll) & (goto Process)
if [%Number%] EQU [4] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cInputBoxEx.dll) & (goto Process)
if [%Number%] EQU [5] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cInventory.dll) & (goto Process)
if [%Number%] EQU [6] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cLookUps.dll) & (goto Process)
if [%Number%] EQU [7] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cMsgBoxEx.dll) & (goto Process)
if [%Number%] EQU [8] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=COMLink.dll) & (goto Process)
if [%Number%] EQU [9] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cOPEvents.dll) & (goto Process)
if [%Number%] EQU [10] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cOPEvents_Wickes.dll) & (goto Process)
if [%Number%] EQU [11] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cPurchase.dll) & (goto Process)
if [%Number%] EQU [12] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cReturns.dll) & (goto Process)
if [%Number%] EQU [13] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cReceiptPrint.dll) & (goto Process)
if [%Number%] EQU [14] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cSale.dll) & (goto Process)
if [%Number%] EQU [15] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cSalesOrder.dll) & (goto Process)
if [%Number%] EQU [16] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cSalesTotals.dll) & (goto Process)
if [%Number%] EQU [17] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cSummaries.dll) & (goto Process)
if [%Number%] EQU [18] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cTillEvents_Wickes.dll) & (goto Process)
if [%Number%] EQU [19] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cTillPrompts.dll) & (goto Process)
if [%Number%] EQU [20] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=CTSHOHSFileProcess.dll) & (goto Process)
if [%Number%] EQU [21] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=CTSOasysTranslator.dll) & (goto Process)
if [%Number%] EQU [22] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=Flp32d30.dll) & (goto Process)
if [%Number%] EQU [23] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=LogicTender.dll) & (goto Process)
if [%Number%] EQU [24] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=MSSTDFMT.DLL) & (goto Process)
if [%Number%] EQU [25] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=msstkprp.dll) & (goto Process)
if [%Number%] EQU [26] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=OA2Logon.dll) & (goto Process)
if [%Number%] EQU [27] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=OasysCBaseCom.dll) & (goto Process)
if [%Number%] EQU [28] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=OasysClientGeneric.dll) & (goto Process)
if [%Number%] EQU [29] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=OasysDbInterfaceCom.dll) & (goto Process)
if [%Number%] EQU [30] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=OasysInterfaces.dll) & (goto Process)
if [%Number%] EQU [31] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=OASYSMenu.dll) & (goto Process)
if [%Number%] EQU [32] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=OasysRootCom.dll) & (goto Process)
if [%Number%] EQU [33] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=OasysSqlDatabaseCom.dll) & (goto Process)
if [%Number%] EQU [34] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=OasysStartCom.dll) & (goto Process)
if [%Number%] EQU [35] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=spr32d60.dll) & (goto Process)
if [%Number%] EQU [36] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=SS60PP.dll) & (goto Process)
if [%Number%] EQU [37] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=VbUtils.dll) & (goto Process)
if [%Number%] EQU [38] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=WssCommidea.dll) & (goto Process)
if [%Number%] EQU [39] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=WickesLibrary.dll) & (goto Process)
if [%Number%] EQU [40] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=CaptureCustUC.ocx) & (goto Process)
if [%Number%] EQU [41] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=ccrpprg6.ocx) & (goto Process)
if [%Number%] EQU [42] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=COMDLG32.OCX) & (goto Process)
if [%Number%] EQU [43] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=CTSProgBar.ocx) & (goto Process)
if [%Number%] EQU [44] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=EditDateCtl.ocx) & (goto Process)
if [%Number%] EQU [45] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=EditNumberCtl.ocx) & (goto Process)
if [%Number%] EQU [46] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=EditNumberTill.ocx) & (goto Process)
if [%Number%] EQU [47] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=Flp32a30.ocx) & (goto Process)
if [%Number%] EQU [48] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=fpFlp30.ocx) & (goto Process)
if [%Number%] EQU [49] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=fpSpr60.ocx) & (goto Process)
if [%Number%] EQU [50] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=FPSPR70.ocx) & (goto Process)
if [%Number%] EQU [51] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=HotkeyList.ocx) & (goto Process)
if [%Number%] EQU [52] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=ItemFilter.ocx) & (goto Process)
if [%Number%] EQU [53] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=MSADODC.OCX) & (goto Process)
if [%Number%] EQU [54] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=McdApi32.ocx) & (goto Process)
if [%Number%] EQU [55] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=Mscomctl.ocx) & (goto Process)
if [%Number%] EQU [56] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=NumTextControl.ocx) & (goto Process)
if [%Number%] EQU [57] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=OPOSCashDrawer.ocx) & (goto Process)
if [%Number%] EQU [58] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=OPOSPOSPrinter.ocx) & (goto Process)
if [%Number%] EQU [59] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=OrderFilter.ocx) & (goto Process)
if [%Number%] EQU [60] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=SPR32X60.ocx) & (goto Process)
if [%Number%] EQU [61] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=TABCTL32.OCX) & (goto Process)
if [%Number%] EQU [62] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=TillOrderItem.ocx) & (goto Process)
if [%Number%] EQU [63] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=TillTranRetrieve.ocx) & (goto Process)
if [%Number%] EQU [64] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=ucPostCodeEntry.ocx) & (goto Process)
if [%Number%] EQU [65] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=CTSEOrderProcess.dll) & (goto Process)
if [%Number%] EQU [66] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cEOrderSalesBO.dll) & (goto Process)
if [%Number%] EQU [67] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=cPrintQuote_Wickes.dll) & (goto Process)
if [%Number%] EQU [68] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=KeyPadUC.ocx) & (goto Process)
if [%Number%] EQU [69] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=EstoreImport4dotNet.dll) & (goto Process)
if [%Number%] EQU [70] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=MSSOAP30.dll) & (goto Process)
if [%Number%] EQU [71] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=mswinsck.ocx) & (goto Process)
if [%Number%] EQU [72] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=mscomm32.ocx) & (goto Process)
if [%Number%] EQU [73] (Set CallBack=RegList) & (Set Mode=Register) & (Set Type=Binary) & (Set File=mscomct2.ocx) & (goto Process)
wait 2
rem ---------------------------------------------------------------------------------------------------


:RegisterSoftwareLoop4
rem -------------------------------------- Register .NET Process --------------------------------------
Set /a Number=0
echo.
echo   Starting Order Manager and Till DotNET Software De-Registration..
echo.
:RSL4List
if [%Number%] EQU [0] (Set CallBack=RSL4List) & (Set Mode=Register) & (Set Type=Assembly) & (Set File=COMOrderManager.dll) & (goto Process)
if [%Number%] EQU [1] (Set CallBack=RSL4List) & (Set Mode=Register) & (Set Type=Assembly) & (Set File=COMTPWickes.InterOp.Interface.dll) & (goto Process)
if [%Number%] EQU [2] (Set CallBack=RSL4List) & (Set Mode=Register) & (Set Type=Assembly) & (Set File=COMTPWickes.InterOp.Wrapper.dll) & (goto Process)
if [%Number%] EQU [3] (Set CallBack=RSL4List) & (Set Mode=Register) & (Set Type=Assembly) & (Set File=COMTPWickes.InterOp.Implementation.dll) & (goto Process)
wait 2
goto CountFails
rem ---------------------------------------------------------------------------------------------------


:Process
rem --------------------------------- Register DLL's / OCX's Process ----------------------------------
if [%Mode%] EQU [UnRegister] goto ProcessUnRegister
if [%Mode%] EQU [Register] goto ProcessRegister
:ProcessUnRegister
if [%Type%] EQU [Binary] (Set Command=%wpath:~0,-1%\RegSvr32.exe" /u /S %ppath:~0,-1%\%File%") & (goto ProcessFile)
if [%Type%] EQU [Assembly] (Set Command=%ppath:~0,-1%\RegAsm.exe" /u %ppath:~0,-1%\%File%" /tlb /codebase) & (goto ProcessFile)
:ProcessRegister
if [%Type%] EQU [Binary] (Set Command=%wpath:~0,-1%\RegSvr32.exe" /S %ppath:~0,-1%\%File%") & (goto ProcessFile)
if [%Type%] EQU [Assembly] (Set Command=%ppath:~0,-1%\RegAsm.exe" %ppath:~0,-1%\%File%" /tlb /codebase) & (goto ProcessFile)
:ProcessFile
nircmd debugwrite "Wickes IT Support ASBO Updater : WXASBO_SU_REGNW Registering File : %File%"
eventcreate /ID 1 /L Application /T Information /SO "Wickes ASBO" /D "WXASBO_SU_REGNW Registering File : %File%" > nul 
%Command%
if [%errorlevel%] NEQ [0] (Set /a FailRegister=%FailRegister% + 1) & (nircmd debugwrite "Wickes IT Support ASBO Updater : WXASBO_SU_REGNW Registering %File% FAILED...") & (eventcreate /ID 1 /L Application /T Information /SO "Wickes ASBO" /D "WXASBO_SU_REGNW Registering %File% FAILED..." > nul)
Set /a Number=%Number% +1
echo %Number% : %FailRegister%
goto %CallBack%
rem ---------------------------------------------------------------------------------------------------


:Error_Handling
rem ---------------------------------------------------------------------------------------------------
nircmd debugwrite "Wickes IT Support ASBO Updater : %ErrorLOCAL%"
eventcreate /ID 1 /L Application /T Information /SO "Wickes ASBO" /D "%ErrorLOCAL%" > nul
sqlcmd -SSRVIVE -E -d Updates -Q"Exec usp_UpdateMaster @sp_Store = N'8%store%', @sp_Workstation = N'%computername%', @sp_Date = '%SQLDate% %time%', @sp_Name = '%username%', @sp_Activity = '%ErrorREMOTE%'"
wait 2
exit
rem ---------------------------------------------------------------------------------------------------



:CountFails
rem --------------------------------- Count Registry Failures Process ---------------------------------
if exist "C:\Wix\REgFail.txt" del "C:\Wix\REgFail.txt"
echo Failure=%FailRegister% > "C:\Wix\RegFail.txt"
wait 2
rem ---------------------------------------------------------------------------------------------------

:Complete
rem ------------------------------------- Display Completed Dialog ------------------------------------
color 2f
echo.
echo   浜様様様様様様様様様様様様様様様様様様様様様様様様様様様�
echo   �                                                       �
echo   �          ** Software Registration Complete **         �
echo   �                                                       �
echo   藩様様様様様様様様様様様様様様様様様様様様様様様様様様様� 
echo.
wait 2
goto end
rem ---------------------------------------------------------------------------------------------------

:end
exit
