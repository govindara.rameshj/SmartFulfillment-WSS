VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cStockAdjustment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D5CB5B302E4"
'<CAMH>****************************************************************************************
'* Module : StockAdjustment
'* Date   : 28/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/InventoryBO/StockAdjustment.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 24/05/04 17:54 $ $Revision: 12 $
'* Versions:
'* 28/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "StockAdjustment"

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=3D5CC65E0014
Private mAdjustmentDate As Date

'##ModelId=3D5CC6660104
Private mAdjustmentCode As String

'##ModelId=3D5CC66D026C
Private mPartCode As String

'##ModelId=3D5CC6710352
Private mCommNo As String

'##ModelId=3D5CC67E021C
Private mAdjustmentBy As String

'##ModelId=3D5CC68B01F4
Private mDepartmentNo As String

'##ModelId=3D5CC69101AE
Private mOpeningQuantity As Double

'##ModelId=3D5CC69D0384
Private mAdjustmentQuantity As Long

'##ModelId=3D5CC6A60280
Private mPrice As Currency

'##ModelId=3D5CC6AC015E
Private mCost As Double

'##ModelId=3D5CC6AF01A4
Private mAdustmentType As String

'##ModelId=3D5CC6C50230
Private mTransferPartCode As String

'##ModelId=3D5CC6D20320
Private mTransferKeyPrice As Currency

'##ModelId=3D5CC6DA02F8
Private mTransferMarkdownValue As Currency

'##ModelId=3D5CC6E50212
Private mComment As String

'##ModelId=3D5CC6EE0172
Private mDRLNo As String

'##ModelId=3D5CC6F7017C
Private mSequenceNo As String

'##ModelId=3D5CC6FD0244
Private mQuantityPerUnit As Double

'##ModelId=3D5CC70602BC
Private mInformation2 As String

'##ModelId=3D5CC70F0064
Private mAuthorisedBy As String

Private mOrigQty As Long

Dim oConnection     As Connection
Dim rsStock         As New Recordset


'##ModelId=3D77291300FA
Public Property Get AuthorisedBy() As String
   Let AuthorisedBy = mAuthorisedBy
End Property

'##ModelId=3D7729120366
Public Property Let AuthorisedBy(ByVal Value As String)
    Let mAuthorisedBy = Value
End Property

'##ModelId=3D772912028A
Public Property Get Information2() As String
   Let Information2 = mInformation2
End Property

'##ModelId=3D772912010E
Public Property Let Information2(ByVal Value As String)
    Let mInformation2 = Value
End Property

'##ModelId=3D7729120032
Public Property Get QuantityPerUnit() As Double
   Let QuantityPerUnit = mQuantityPerUnit
End Property

'##ModelId=3D77291102D0
Public Property Let QuantityPerUnit(ByVal Value As Double)
    Let mQuantityPerUnit = Value
End Property

'##ModelId=3D77291101F4
Public Property Get SequenceNo() As String
   Let SequenceNo = mSequenceNo
End Property

'##ModelId=3D77291100AA
Public Property Let SequenceNo(ByVal Value As String)
    Let mSequenceNo = Value
End Property

'##ModelId=3D77291003B6
Public Property Get DRLNo() As String
   Let DRLNo = mDRLNo
End Property

'##ModelId=3D772910026C
Public Property Let DRLNo(ByVal Value As String)
    Let mDRLNo = Value
End Property

'##ModelId=3D7729100190
Public Property Get Comment() As String
   Let Comment = mComment
End Property

'##ModelId=3D7729100046
Public Property Let Comment(ByVal Value As String)
    Let mComment = Value
    mInformation2 = vbNullString
    mInformation2 = Mid$(mComment, 21)
    mComment = Left$(mComment, 20)
End Property

'##ModelId=3D77290F0384
Public Property Get TransferMarkdownValue() As Currency
   Let TransferMarkdownValue = mTransferMarkdownValue
End Property

'##ModelId=3D77290F023A
Public Property Let TransferMarkdownValue(ByVal Value As Currency)
    Let mTransferMarkdownValue = Value
End Property

'##ModelId=3D77290F019A
Public Property Get TransferKeyPrice() As Currency
   Let TransferKeyPrice = mTransferKeyPrice
End Property

'##ModelId=3D77290F0050
Public Property Let TransferKeyPrice(ByVal Value As Currency)
    Let mTransferKeyPrice = Value
End Property

'##ModelId=3D77290E0398
Public Property Get TransferPartCode() As String
   Let TransferPartCode = mTransferPartCode
End Property

'##ModelId=3D77290E0280
Public Property Let TransferPartCode(ByVal Value As String)
    Let mTransferPartCode = Value
End Property

'##ModelId=3D77290E01E0
Public Property Get AdustmentType() As String
   Let AdustmentType = mAdustmentType
End Property

'##ModelId=3D77290E00C8
Public Property Let AdustmentType(ByVal Value As String)
    Let mAdustmentType = Value
End Property

'##ModelId=3D77290D03D4
Public Property Get Cost() As Double
   Let Cost = mCost
End Property

'##ModelId=3D77290D02F8
Public Property Let Cost(ByVal Value As Double)
    Let mCost = Value
End Property

'##ModelId=3D77290D021C
Public Property Get Price() As Currency
   Let Price = mPrice
End Property

'##ModelId=3D77290D0140
Public Property Let Price(ByVal Value As Currency)
    Let mPrice = Value
End Property

'##ModelId=3D77290D00A0
Public Property Get AdjustmentQuantity() As Long
   Let AdjustmentQuantity = mAdjustmentQuantity
End Property

'##ModelId=3D77290C0370
Public Property Let AdjustmentQuantity(ByVal Value As Long)
    Let mAdjustmentQuantity = Value
End Property

'##ModelId=3D77290C0302
Public Property Get OpeningQuantity() As Double
   Let OpeningQuantity = mOpeningQuantity
End Property

'##ModelId=3D77290C01F4
Public Property Let OpeningQuantity(ByVal Value As Double)
    Let mOpeningQuantity = Value
End Property

'##ModelId=3D77290C0186
Public Property Get DepartmentNo() As String
   Let DepartmentNo = mDepartmentNo
End Property

'##ModelId=3D77290C00AA
Public Property Let DepartmentNo(ByVal Value As String)
    Let mDepartmentNo = Value
End Property

'##ModelId=3D77290C0000
Public Property Get AdjustmentBy() As String
   Let AdjustmentBy = mAdjustmentBy
End Property

'##ModelId=3D77290B030C
Public Property Let AdjustmentBy(ByVal Value As String)
    Let mAdjustmentBy = Value
End Property

'##ModelId=3D77290B029E
Public Property Get CommNo() As String
   Let CommNo = mCommNo
End Property

'##ModelId=3D77290B01C2
Public Property Let CommNo(ByVal Value As String)
    Let mCommNo = Value
End Property

'##ModelId=3D77290B0154
Public Property Get PartCode() As String
   Let PartCode = mPartCode
End Property

'##ModelId=3D77290B0082
Public Property Let PartCode(ByVal Value As String)
    Let mPartCode = Value
End Property

'##ModelId=3D77290B0014
Public Property Get AdjustmentCode() As String
   Let AdjustmentCode = mAdjustmentCode
End Property

'##ModelId=3D77290A0352
Public Property Let AdjustmentCode(ByVal Value As String)
    Let mAdjustmentCode = Value
End Property

'##ModelId=3D77290A02E4
Public Property Get AdjustmentDate() As Date
   Let AdjustmentDate = mAdjustmentDate
End Property


'##ModelId=3D77290A0208
Public Property Let AdjustmentDate(ByVal Value As Date)
    Let mAdjustmentDate = Value
End Property


Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function

Public Function SaveIfNew() As Boolean
    
    SaveIfNew = Save(SaveTypeIfNew)

End Function

'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database

Dim oStockLog As cStockLog
    
    Save = False
    
    If mQuantityPerUnit = 0 Then mQuantityPerUnit = 1
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
        Set oStockLog = m_oSession.Database.CreateBusinessObject(CLASSID_STOCKLOG)
        oStockLog.LogDate = Me.AdjustmentDate
        oStockLog.LogTime = Format$(Now, "HHMMSS")
        oStockLog.PartCode = mPartCode
        oStockLog.TransactionKey = Format$(oStockLog.LogDate, "MM/DD/YY") & mAdjustmentCode
        'oStockLog.TransactionKey = me
        'oStockLog.TransactionNo = DeliveryNote.DocNo
        oStockLog.StartStockQty = mOrigQty
        oStockLog.EndStockQty = mOrigQty + AdjustmentQuantity
        MsgBox "Stock Adjustment Save Removed"
        'Call oStockLog.SaveStockAdjustment
 
        If eSave = SaveTypeIfExists Then
            'Roll back previous adjustment
        End If
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cStockAdjustment
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_STOCKADJUST * &H10000) + 1 To FID_STOCKADJUST_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cStockAdjustment

End Function


Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_STOCKADJUST, FID_STOCKADJUST_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_STOCKADJUST_AdjustmentDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mAdjustmentDate
        Case (FID_STOCKADJUST_AdjustmentCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAdjustmentCode
        Case (FID_STOCKADJUST_PartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartCode
        Case (FID_STOCKADJUST_CommNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCommNo
        Case (FID_STOCKADJUST_AdjustmentBy):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAdjustmentBy
        Case (FID_STOCKADJUST_DepartmentNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDepartmentNo
        Case (FID_STOCKADJUST_OpeningQuantity):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mOpeningQuantity
        Case (FID_STOCKADJUST_AdjustmentQuantity):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mAdjustmentQuantity
        Case (FID_STOCKADJUST_Price):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mPrice
        Case (FID_STOCKADJUST_Cost):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mCost
        Case (FID_STOCKADJUST_AdustmentType):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAdustmentType
        Case (FID_STOCKADJUST_TransferPartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTransferPartCode
        Case (FID_STOCKADJUST_TransferKeyPrice):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mTransferKeyPrice
        Case (FID_STOCKADJUST_TransferMarkdownValue):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mTransferMarkdownValue
        Case (FID_STOCKADJUST_Comment):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mComment
        Case (FID_STOCKADJUST_DRLNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDRLNo
        Case (FID_STOCKADJUST_SequenceNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSequenceNo
        Case (FID_STOCKADJUST_QuantityPerUnit):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mQuantityPerUnit
        Case (FID_STOCKADJUST_Information2):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mInformation2
        Case (FID_STOCKADJUST_AuthorisedBy):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAuthorisedBy
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cStockAdjustment
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_STOCKADJUST

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Stock Adjustment " & mPartCode

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_STOCKADJUST_AdjustmentDate):      mAdjustmentDate = oField.ValueAsVariant
            Case (FID_STOCKADJUST_AdjustmentCode):      mAdjustmentCode = oField.ValueAsVariant
            Case (FID_STOCKADJUST_PartCode):            mPartCode = oField.ValueAsVariant
            Case (FID_STOCKADJUST_CommNo):              mCommNo = oField.ValueAsVariant
            Case (FID_STOCKADJUST_AdjustmentBy):        mAdjustmentBy = oField.ValueAsVariant
            Case (FID_STOCKADJUST_DepartmentNo):        mDepartmentNo = oField.ValueAsVariant
            Case (FID_STOCKADJUST_OpeningQuantity):     mOpeningQuantity = oField.ValueAsVariant
            Case (FID_STOCKADJUST_AdjustmentQuantity):  mAdjustmentQuantity = oField.ValueAsVariant
                                                        mOrigQty = mAdjustmentQuantity
            Case (FID_STOCKADJUST_Price):               mPrice = oField.ValueAsVariant
            Case (FID_STOCKADJUST_Cost):                mCost = oField.ValueAsVariant
            Case (FID_STOCKADJUST_AdustmentType):       mAdustmentType = oField.ValueAsVariant
            Case (FID_STOCKADJUST_TransferPartCode):    mTransferPartCode = oField.ValueAsVariant
            Case (FID_STOCKADJUST_TransferKeyPrice):    mTransferKeyPrice = oField.ValueAsVariant
            Case (FID_STOCKADJUST_TransferMarkdownValue): mTransferMarkdownValue = oField.ValueAsVariant
            Case (FID_STOCKADJUST_Comment):             mComment = oField.ValueAsVariant
            Case (FID_STOCKADJUST_DRLNo):               mDRLNo = oField.ValueAsVariant
            Case (FID_STOCKADJUST_SequenceNo):          mSequenceNo = oField.ValueAsVariant
            Case (FID_STOCKADJUST_QuantityPerUnit):     mQuantityPerUnit = oField.ValueAsVariant
            Case (FID_STOCKADJUST_Information2):        mInformation2 = oField.ValueAsVariant
                                                        mComment = mComment & mInformation2
            Case (FID_STOCKADJUST_AuthorisedBy):        mAuthorisedBy = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow



Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_STOCKADJUST_END_OF_STATIC

End Function

Public Sub SaveAdjustment()

Dim strQuery As String
    
    strQuery = "INSERT INTO STKADJ ("
    strQuery = strQuery & Chr(34) & "DATE1" & Chr(34) & ", "
    strQuery = strQuery & Chr(34) & "CODE" & Chr(34) & ", "
    strQuery = strQuery & Chr(34) & "SKUN" & Chr(34) & ", "
    strQuery = strQuery & Chr(34) & "SEQN" & Chr(34) & ", "
    strQuery = strQuery & Chr(34) & "COMM" & Chr(34) & ", "
    strQuery = strQuery & Chr(34) & "INIT" & Chr(34) & ", "
    strQuery = strQuery & Chr(34) & "SSTK" & Chr(34) & ", "
    strQuery = strQuery & Chr(34) & "QUAN" & Chr(34) & ", "
    strQuery = strQuery & Chr(34) & "PRIC" & Chr(34) & ", "
    strQuery = strQuery & Chr(34) & "COST" & Chr(34) & ", "
    strQuery = strQuery & Chr(34) & "TYPE" & Chr(34) & ", "
    strQuery = strQuery & Chr(34) & "INFO" & Chr(34) & ") "
    strQuery = strQuery & "VALUES ('" & Format(mAdjustmentDate, "yyyy-mm-dd") & "',"
    strQuery = strQuery & "'" & mAdjustmentCode & "', "
    strQuery = strQuery & "'" & mPartCode & "', "
    strQuery = strQuery & "'" & mSequenceNo & "', "
    strQuery = strQuery & "0, "
    strQuery = strQuery & "'" & mAdjustmentBy & "', "
    strQuery = strQuery & "'" & mOpeningQuantity & "', "
    strQuery = strQuery & "'" & mAdjustmentQuantity & "', "
    strQuery = strQuery & "'" & Format(mPrice, "0.00") & "', "
    strQuery = strQuery & "'" & Format(mCost, "0.00") & "', "
    strQuery = strQuery & "'" & mAdustmentType & "', "
    strQuery = strQuery & "'" & mComment & "')"
    On Error GoTo Already_There
    Call m_oSession.Database.ExecuteCommand(strQuery)
    Exit Sub
Already_There:
    Call MsgBox("Adjustment has already been made.", vbOKOnly, "Code 54 Stock Adjustment")
End Sub
