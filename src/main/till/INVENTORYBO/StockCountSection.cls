VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cStockCountHeader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D5CB61902E4"
'<CAMH>****************************************************************************************
'* Module : StockCountHeader
'* Date   : 28/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/InventoryBO/StockCountSection.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 16/05/03 12:27 $
'* $Revision: 14 $
'* Versions:
'* 28/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
'
' This class represents a transient image of a Stock Location for the duration of
' a stock take.
' It represents a collection of Stock Location items of class cStockCountItem.
' Each Item is identified by its ordinal number in the collection, starting
' from 1, also known as the sequence number.
' In the database, the HHTHDR table column NEXT represents the number of items
' at the location plus one and is logically derived from the count of HHTDET table
' rows with the same LOCN value.
'
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "StockCountHeader"
Const CSTOCKTAKE_LOCATION_ID_HEADER = "0000"

Const ASCII_UC_A = 65
Const ASCII_UC_Z = 90
Const ASCII_0 = 48
Const ASCII_9 = 57
Const ASCII_LC_A = 97
Const ASCII_LC_Z = 122
Const ASCII_SPACE = 32
Const MAX_SIZE_LOCATION_NUMBER = 4

' The m_bIsDirty flag is true when a persistent property has changed in this
' object but not yet been saved to the database.
Private m_bIsDirty  As Boolean

' Collection of cStockItem objects, not loaded by default.
Private m_colItems          As Collection

'##ModelId=3D5CCE3A02C6
Private mLocationNumber As String

'##ModelId=3D5CCE3E0230
Private mItemCount As Long

'##ModelId=3D5CCE460168
Private mAppliedCount As Long

'##ModelId=3D5CCE4B01D6
Private mDateFrozen As Date

'##ModelId=3D5CCE5300D2
Private mTimeFrozen As Date

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=3D77291F003C
'Public Property Get TimeFrozen() As String
'   Let TimeFrozen = Format$(mTimeFrozen, "HH:NN:SS")
'End Property

'##ModelId=3D77291E0348
'Public Property Let TimeFrozen(ByVal Value As String)
'    Let mTimeFrozen = Value
'End Property

'##ModelId=3D77291E02DA
Public Property Get DateTimeFrozen() As Date
   Let DateTimeFrozen = Format$(mDateFrozen, "DD/MM/YYYY ") & Format$(mTimeFrozen, "HH:NN:SS")
End Property

'##ModelId=3D77291E023A
Public Property Let DateTimeFrozen(ByVal Value As Date)
    m_bIsDirty = True
    Let mDateFrozen = Value
    Let mTimeFrozen = Value
End Property

'##ModelId=3D77291E01CC
Public Property Get AppliedCount() As Long
   Let AppliedCount = mAppliedCount
End Property

'##ModelId=3D77291E0122
Public Property Let AppliedCount(ByVal Value As Long)
    If AppliedCount <> Value Then
        m_bIsDirty = True
    End If
    Let mAppliedCount = Value
End Property

'##ModelId=3D77291E00B4
Public Property Get ItemCount() As Long
    If m_colItems Is Nothing Then
        ' We don't have a real count of the items, so use the derived value
        Let ItemCount = mItemCount
    Else
        ItemCount = m_colItems.Count
    End If
End Property

'##ModelId=3D77291E0046
Private Property Let ItemCount(ByVal Value As Long)
    ' We don't want outsiders to change this directly as the value represents
    ' the number of StockCountLine objects for the location which this class maintains.
    If mItemCount <> Value Then
        m_bIsDirty = True
    End If
    Let mItemCount = Value
End Property

'##ModelId=3D77291D03C0
Public Property Get LocationNumber() As String
   Let LocationNumber = mLocationNumber
End Property

'##ModelId=3D77291D0320
Public Property Let LocationNumber(ByVal Value As String)
    If IsValidLocationNumber(Value) Or Value = CSTOCKTAKE_LOCATION_ID_HEADER Then
        If mLocationNumber <> Value Then
            m_bIsDirty = True
        End If
        Let mLocationNumber = Value
    Else
        Let mLocationNumber = vbNullString
    End If
End Property

Public Function IsValidLocationNumber(ByVal Value As String) As Boolean
' Location number must be 1 upper-case alpha plus 3 numerics.
Dim aByte   As Byte
Dim i       As Long
    
    IsValidLocationNumber = False
    
    If Len(Value) = MAX_SIZE_LOCATION_NUMBER Then
        ' Length is ok
        aByte = AscB(Value)
        If aByte >= ASCII_UC_A And aByte <= ASCII_UC_Z Then
            ' First char is upper-case, so we are ok unless any char is not 0-9
            IsValidLocationNumber = True
            For i = 2 To MAX_SIZE_LOCATION_NUMBER
                aByte = AscB(Mid(Value, i, 1))
                If aByte < ASCII_0 Or aByte > ASCII_9 Then
                    IsValidLocationNumber = False
                    Exit Function
                End If
            Next i
        End If
    End If
End Function

Public Function NormaliseLocationNumber(ByVal Value As String) As String
    ' Location number must be 1 upper-case alpha plus 3 numerics, but the
    ' user may use lower case and not bother with leading zeros, so this
    ' method converts to the expected format or returns an empty string
    ' if not.
    Dim aByte           As Byte
    Dim strNormal       As String
    Dim nLength, i      As Long
    Dim aFirstOkByte    As Byte
    Dim nOkNumber       As Integer
    
    nLength = Len(Value)
    
    If nLength > 0 And nLength <= MAX_SIZE_LOCATION_NUMBER Then
        ' Length is ok
        aByte = AscB(Value)
        If aByte >= ASCII_LC_A And aByte <= ASCII_LC_Z Then
            ' Change to upper case
            aByte = aByte - ASCII_LC_A + ASCII_UC_A
        End If
        If aByte >= ASCII_UC_A And aByte <= ASCII_UC_Z Then
            ' First char is upper-case, so we are ok unless any char is not 0-9
            aFirstOkByte = aByte
            For i = 2 To nLength
                aByte = AscB(Mid$(Value, i, 1))
                If aByte = ASCII_SPACE Then
                    ' A space signals beyond the numbers
                    nLength = i - 1
                    Exit For
                End If
                If aByte < ASCII_0 Or aByte > ASCII_9 Then
                    nLength = 0     ' Flag invalid number
                    Exit For
                End If
                nOkNumber = (nOkNumber * 10) + (aByte - ASCII_0)
            Next i
            If nLength > 0 Then
                ' All Ok, so format a result
                NormaliseLocationNumber = Chr$(aFirstOkByte) & Format$(nOkNumber, "000")
            End If
        End If
    End If
End Function

Public Property Get Item(nIndex As Integer) As cStockCountItem
    If EnsureItemsLoaded() Then
        If nIndex > 0 And nIndex <= ItemCount Then
            Set Item = m_colItems.Item(CStr(nIndex))
            ' ensure IsDisplay algorithms are consistent
            Debug.Assert (Item.IsDisplay = IsDisplay)
        Else
            Debug.Assert False      ' Invalid index value
        End If
    End If
End Property

Public Function NewItem() As cStockCountItem
    ' Creates a new Item, adds it to the end of the collection and returns a pointer to it.
    If EnsureItemsLoaded() Then
        Set NewItem = m_oSession.Database.CreateBusinessObject(CLASSID_STOCKCOUNTLINE)
        Debug.Assert Not (NewItem Is Nothing)
        m_colItems.Add NewItem, CStr(ItemCount + 1)
        ItemCount = m_colItems.Count
        NewItem.LocationNumber = LocationNumber
        NewItem.SequenceNumber = ItemCount
        NewItem.DateTimeCounted = Now
    End If
End Function

Public Function GetQuantityForPartCode(PartCode As String) As Double
    ' A location may have sequence of item counts.  There may be more than one
    ' item count within a location for one Sku.  This routine tots up the
    ' individual quantities for a Sku within a location and returns the total.
    Dim oItem           As cStockCountItem
    EnsureItemsLoaded
    If Not (m_colItems Is Nothing) Then
        For Each oItem In m_colItems
            If oItem.PartCode = PartCode Then
                ' We have a match, so add quantity to total
                GetQuantityForPartCode = GetQuantityForPartCode + oItem.QuantityCounted
            End If
        Next oItem
    End If
End Function

Private Function EnsureItemsLoaded() As Boolean
    ' If items are not already loaded, then get then.
    Dim oRSelector      As IRowSelector
    Dim colRawItems     As Collection
    Dim oItem           As cStockCountItem
    
    If m_colItems Is Nothing Then
        Set m_colItems = New Collection
        Set oRSelector = m_oSession.Database.CreateBoSelector(CLASSID_STOCKCOUNTLINE)
        oRSelector.Override m_oSession.Database.CreateBoSelector(FID_STOCKCOUNTLINE_LocationNumber, _
                            CMP_EQUAL, LocationNumber)
        oRSelector.Override m_oSession.Database.CreateBoSelector(FID_STOCKCOUNTLINE_SequenceNumber, _
                            CMP_GREATERTHAN, 0)
        Set colRawItems = m_oSession.Database.GetBoCollection(oRSelector)
        ' We have the items, but we need to create the key.
        For Each oItem In colRawItems
            m_colItems.Add oItem, CStr(oItem.SequenceNumber)
        Next oItem
        Set colRawItems = Nothing
        ItemCount = m_colItems.Count
        EnsureItemsLoaded = True
    Else
        ' Already loaded
        EnsureItemsLoaded = True
    End If
End Function

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
Dim oRSelector  As IRowSelector
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
'    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
'    Call moRowSel.Merge(GetSelectAllRow)

    Set oRSelector = m_oSession.Root.CreateUtilityObject("CRowSelector")
    oRSelector.AddSelection CMP_EQUAL, GetField(FID_STOCKCOUNTHEADER_LocationNumber)
    
    oRSelector.Merge GetSelectAllRow
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oRSelector)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Private Sub Class_Initialize()
    m_bIsDirty = True
End Sub

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
    ' Save all properties to the database
    Dim oItem   As cStockCountItem
    Dim bRetOk  As Boolean
    
    Save = False
    bRetOk = True
    
    If IsValid() Then
        ' Do any member items need saving?
        If eSave = SaveTypeDelete Then
            ' If we are deleting the Header then we must delete the detail
            ' records, so we must load them first.
            EnsureItemsLoaded
        End If
        If Not (m_colItems Is Nothing) Then
            For Each oItem In m_colItems
                If eSave = SaveTypeDelete Then
                    If Not oItem.IBo_Delete Then bRetOk = False
                Else
                    If oItem.IsChanged Then
                        If Not oItem.IBo_Save Then bRetOk = False
                    End If
                End If
            Next oItem
        End If
        If bRetOk Then
            ' Get a row and pass it to the database to insert/update/delete
            If m_bIsDirty Then
                Save = m_oSession.Database.SaveBo(eSave, Me)
            Else
                Save = True
            End If
        End If
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Property Get IsChanged() As Boolean
    IsChanged = m_bIsDirty
End Property

Public Property Get IsDisplay() As Boolean
    ' Must be same algorithm as in detail class.
    IsDisplay = (Left$(mLocationNumber, 1) = "D")
End Property

Public Function Interface(Optional eInterfaceType As Long) As cStockCountHeader
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_STOCKCOUNTHEADER * &H10000) + 1 To FID_STOCKCOUNTHEADER_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Public Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cStockCountHeader

End Function


Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Public Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_STOCKCOUNTHEADER, FID_STOCKCOUNTHEADER_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_STOCKCOUNTHEADER_LocationNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLocationNumber
        Case (FID_STOCKCOUNTHEADER_ItemCount):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mItemCount
        Case (FID_STOCKCOUNTHEADER_AppliedCount):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mAppliedCount
        Case (FID_STOCKCOUNTHEADER_DateFrozen):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDateFrozen
        Case (FID_STOCKCOUNTHEADER_TimeFrozen):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mTimeFrozen
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cStockCountHeader
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_STOCKCOUNTHEADER

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "StockCount Location " & mLocationNumber & " Last Sequence #" & ItemCount

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    m_bIsDirty = False
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_STOCKCOUNTHEADER_LocationNumber): mLocationNumber = oField.ValueAsVariant
            Case (FID_STOCKCOUNTHEADER_ItemCount):      mItemCount = oField.ValueAsVariant
            Case (FID_STOCKCOUNTHEADER_AppliedCount):   mAppliedCount = oField.ValueAsVariant
            Case (FID_STOCKCOUNTHEADER_DateFrozen):     mDateFrozen = oField.ValueAsVariant
            Case (FID_STOCKCOUNTHEADER_TimeFrozen):     mTimeFrozen = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_STOCKCOUNTHEADER_END_OF_STATIC

End Function

