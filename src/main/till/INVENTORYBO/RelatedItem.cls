VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cRelatedItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D5CB7270096"
'<CAMH>****************************************************************************************
'* Module : RelatedItem
'* Date   : 28/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/InventoryBO/RelatedItem.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 20/05/04 9:24 $
'* $Revision: 7 $
'* Versions:
'* 28/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "RelatedItem"

'##ModelId=3D5CD89400D2
Private mItemNo As String

'##ModelId=3D5CD8A80398
Private mBulkItemNo As String

'##ModelId=3D5CD8B100D2
Private mSinglesPerPack As Long

'##ModelId=3D5CD8B9030C
Private mSinglesSoldNotUpdated As Double

'##ModelId=3D5CD8CD003C
Private mDeleted As Boolean

'##ModelId=3D5CD8D20262
Private mSinglesSoldToDate As Long

'##ModelId=3D5CD8E00262
Private mSinglesSoldToYear As Long

'##ModelId=3D5CD8E80352
Private mSinglesSoldPriorYear As Long

'##ModelId=3D5CD8EF01B8
Private mMarkUpTransThisWeek As Long

'##ModelId=3D5CD91A029E
Private mMarkUpTransPriorWeek As Long

'##ModelId=3D5CD92301CC
Private mMarkUpThisWeek As Currency

'##ModelId=3D5CD92B0028
Private mMarkUpPriorWeek As Currency

'##ModelId=3D5CD934028A
Private mMarkUpThisPeriod As Currency

'##ModelId=3D5CD93D0366
Private mMarkUpPriorPeriod As Currency

'##ModelId=3D5CD94803CA
Private mMarkUpThisYear As Currency

'##ModelId=3D5CD94E0352
Private mMarkUpPriorYear As Currency

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session


'##ModelId=3D772942010E
Public Property Get MarkUpPriorYear() As Currency
   Let MarkUpPriorYear = mMarkUpPriorYear
End Property

'##ModelId=3D77294103AC
Public Property Let MarkUpPriorYear(ByVal Value As Currency)
    Let mMarkUpPriorYear = Value
End Property

'##ModelId=3D772941030C
Public Property Get MarkUpThisYear() As Currency
   Let MarkUpThisYear = mMarkUpThisYear
End Property

'##ModelId=3D77294101F4
Public Property Let MarkUpThisYear(ByVal Value As Currency)
    Let mMarkUpThisYear = Value
End Property

'##ModelId=3D7729410118
Public Property Get MarkUpPriorPeriod() As Currency
   Let MarkUpPriorPeriod = mMarkUpPriorPeriod
End Property

'##ModelId=3D772941000A
Public Property Let MarkUpPriorPeriod(ByVal Value As Currency)
    Let mMarkUpPriorPeriod = Value
End Property

'##ModelId=3D7729400348
Public Property Get MarkUpThisPeriod() As Currency
   Let MarkUpThisPeriod = mMarkUpThisPeriod
End Property

'##ModelId=3D772940023A
Public Property Let MarkUpThisPeriod(ByVal Value As Currency)
    Let mMarkUpThisPeriod = Value
End Property

'##ModelId=3D7729400190
Public Property Get MarkUpPriorWeek() As Currency
   Let MarkUpPriorWeek = mMarkUpPriorWeek
End Property

'##ModelId=3D7729400082
Public Property Let MarkUpPriorWeek(ByVal Value As Currency)
    Let mMarkUpPriorWeek = Value
End Property

'##ModelId=3D77293F03C0
Public Property Get MarkUpThisWeek() As Currency
   Let MarkUpThisWeek = mMarkUpThisWeek
End Property

'##ModelId=3D77293F02E4
Public Property Let MarkUpThisWeek(ByVal Value As Currency)
    Let mMarkUpThisWeek = Value
End Property

'##ModelId=3D77293F0244
Public Property Get MarkUpTransPriorWeek() As Long
   Let MarkUpTransPriorWeek = mMarkUpTransPriorWeek
End Property

'##ModelId=3D77293F0168
Public Property Let MarkUpTransPriorWeek(ByVal Value As Long)
    Let mMarkUpTransPriorWeek = Value
End Property

'##ModelId=3D77293F00BE
Public Property Get MarkUpTransThisWeek() As Long
   Let MarkUpTransThisWeek = mMarkUpTransThisWeek
End Property

'##ModelId=3D77293E03CA
Public Property Let MarkUpTransThisWeek(ByVal Value As Long)
    Let mMarkUpTransThisWeek = Value
End Property

'##ModelId=3D77293E032A
Public Property Get SinglesSoldPriorYear() As Long
   Let SinglesSoldPriorYear = mSinglesSoldPriorYear
End Property

'##ModelId=3D77293E024E
Public Property Let SinglesSoldPriorYear(ByVal Value As Long)
    Let mSinglesSoldPriorYear = Value
End Property

'##ModelId=3D77293E01E0
Public Property Get SinglesSoldToYear() As Long
   Let SinglesSoldToYear = mSinglesSoldToYear
End Property

'##ModelId=3D77293E0104
Public Property Let SinglesSoldToYear(ByVal Value As Long)
    Let mSinglesSoldToYear = Value
End Property

'##ModelId=3D77293E0064
Public Property Get SinglesSoldToDate() As Long
   Let SinglesSoldToDate = mSinglesSoldToDate
End Property

'##ModelId=3D77293D03A2
Public Property Let SinglesSoldToDate(ByVal Value As Long)
    Let mSinglesSoldToDate = Value
End Property

'##ModelId=3D77293D0334
Public Property Get Deleted() As Boolean
   Let Deleted = mDeleted
End Property

'##ModelId=3D77293D0258
Public Property Let Deleted(ByVal Value As Boolean)
    Let mDeleted = Value
End Property

'##ModelId=3D77293D01EA
Public Property Get SinglesSoldNotUpdated() As Double
   Let SinglesSoldNotUpdated = mSinglesSoldNotUpdated
End Property

'##ModelId=3D77293D014A
Public Property Let SinglesSoldNotUpdated(ByVal Value As Double)
    Let mSinglesSoldNotUpdated = Value
End Property

'##ModelId=3D77293D00DC
Public Property Get SinglesPerPack() As Long
   Let SinglesPerPack = mSinglesPerPack
End Property

'##ModelId=3D77293D0032
Public Property Let SinglesPerPack(ByVal Value As Long)
    Let mSinglesPerPack = Value
End Property

'##ModelId=3D77293C03AC
Public Property Get BulkItemNo() As String
   Let BulkItemNo = mBulkItemNo
End Property

'##ModelId=3D77293C030C
Public Property Let BulkItemNo(ByVal Value As String)
    Let mBulkItemNo = Value
End Property

'##ModelId=3D77293C029E
Public Property Get ItemNo() As String
   Let ItemNo = mItemNo
End Property


'##ModelId=3D77293C0230
Public Property Let ItemNo(ByVal Value As String)
    Let mItemNo = Value
End Property
Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cRelatedItem
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_RELATEDITEM * &H10000) + 1 To FID_RELATEDITEM_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cRelatedItem

End Function


Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_RELATEDITEM, FID_RELATEDITEM_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_RELATEDITEM_ItemNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mItemNo
        Case (FID_RELATEDITEM_BulkItemNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mBulkItemNo
        Case (FID_RELATEDITEM_SinglesPerPack):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mSinglesPerPack
        Case (FID_RELATEDITEM_SinglesSoldNotUpdated):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mSinglesSoldNotUpdated
        Case (FID_RELATEDITEM_Deleted):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mDeleted
        Case (FID_RELATEDITEM_SinglesSoldToDate):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mSinglesSoldToDate
        Case (FID_RELATEDITEM_SinglesSoldToYear):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mSinglesSoldToYear
        Case (FID_RELATEDITEM_SinglesSoldPriorYear):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mSinglesSoldPriorYear
        Case (FID_RELATEDITEM_MarkUpTransThisWeek):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mMarkUpTransThisWeek
        Case (FID_RELATEDITEM_MarkUpTransPriorWeek):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mMarkUpTransPriorWeek
        Case (FID_RELATEDITEM_MarkUpThisWeek):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mMarkUpThisWeek
        Case (FID_RELATEDITEM_MarkUpPriorWeek):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mMarkUpPriorWeek
        Case (FID_RELATEDITEM_MarkUpThisPeriod):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mMarkUpThisPeriod
        Case (FID_RELATEDITEM_MarkUpPriorPeriod):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mMarkUpPriorPeriod
        Case (FID_RELATEDITEM_MarkUpThisYear):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mMarkUpThisYear
        Case (FID_RELATEDITEM_MarkUpPriorYear):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mMarkUpPriorYear
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cRelatedItem
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_RELATEDITEM

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "RelatedItem " & mItemNo & "/Bulk-" & mBulkItemNo

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_RELATEDITEM_ItemNo):                  mItemNo = oField.ValueAsVariant
            Case (FID_RELATEDITEM_BulkItemNo):              mBulkItemNo = oField.ValueAsVariant
            Case (FID_RELATEDITEM_SinglesPerPack):          mSinglesPerPack = oField.ValueAsVariant
            Case (FID_RELATEDITEM_SinglesSoldNotUpdated):   mSinglesSoldNotUpdated = oField.ValueAsVariant
            Case (FID_RELATEDITEM_Deleted):                 mDeleted = oField.ValueAsVariant
            Case (FID_RELATEDITEM_SinglesSoldToDate):       mSinglesSoldToDate = oField.ValueAsVariant
            Case (FID_RELATEDITEM_SinglesSoldToYear):       mSinglesSoldToYear = oField.ValueAsVariant
            Case (FID_RELATEDITEM_SinglesSoldPriorYear):    mSinglesSoldPriorYear = oField.ValueAsVariant
            Case (FID_RELATEDITEM_MarkUpTransThisWeek):     mMarkUpTransThisWeek = oField.ValueAsVariant
            Case (FID_RELATEDITEM_MarkUpTransPriorWeek):    mMarkUpTransPriorWeek = oField.ValueAsVariant
            Case (FID_RELATEDITEM_MarkUpThisWeek):          mMarkUpThisWeek = oField.ValueAsVariant
            Case (FID_RELATEDITEM_MarkUpPriorWeek):         mMarkUpPriorWeek = oField.ValueAsVariant
            Case (FID_RELATEDITEM_MarkUpThisPeriod):        mMarkUpThisPeriod = oField.ValueAsVariant
            Case (FID_RELATEDITEM_MarkUpPriorPeriod):       mMarkUpPriorPeriod = oField.ValueAsVariant
            Case (FID_RELATEDITEM_MarkUpThisYear):          mMarkUpThisYear = oField.ValueAsVariant
            Case (FID_RELATEDITEM_MarkUpPriorYear):         mMarkUpPriorYear = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_RELATEDITEM_END_OF_STATIC

End Function

