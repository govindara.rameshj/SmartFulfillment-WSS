VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cStockWriteOff"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D5CB5A70226"
'<CAMH>****************************************************************************************
'* Module : StockWriteOff
'* Date   : 28/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/InventoryBO/StockWriteOff.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 20/05/04 9:25 $  $Revision: 12 $
'* Versions:
'* 28/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "StockWriteOff"

Implements IBo
Implements ISysBo

'##ModelId=3D5CC3D40028
Private mBatchNo As String

'##ModelId=3D5CC3DC0302
Private mAdjustmentCode As String

'##ModelId=3D5CC4CB005A
Private mLineNo As String

'##ModelId=3D5CC4DA00FA
Private mPartCode As String

'##ModelId=3D5CC4DF00BE
Private mDateCreated As Date

'##ModelId=3D5CC4E3035C
Private mAuthorisedBy As String

'##ModelId=3D5CC4ED035C
Private mAuthorisedDate As Date

'##ModelId=3D5CC4F803B6
Private mQuantity As Long
Private mOrigQuantity As Long 'used to perform adjustments to stock levels

'##ModelId=3D5CC5060384
Private mPrice As Currency

'##ModelId=3D5CC50F03CA
Private mCost As Double

'##ModelId=3D5CC516029E
Private mAdjustedBy As String

'##ModelId=3D5CC51E01D6
Private mComment As String

Private mQuantityPerUnit As Long

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=3D77290900C8
Public Property Get Comment() As String
   Let Comment = mComment
End Property

'##ModelId=3D77290803A2
Public Property Let Comment(ByVal Value As String)
    Let mComment = Value
End Property

'##ModelId=3D7729080334
Public Property Get AdjustedBy() As String
   Let AdjustedBy = mAdjustedBy
End Property

'##ModelId=3D772908021C
Public Property Let AdjustedBy(ByVal Value As String)
    Let mAdjustedBy = Value
End Property

'##ModelId=3D77290801AE
Public Property Get Cost() As Double
   Let Cost = mCost
End Property

'##ModelId=3D77290800D2
Public Property Let Cost(ByVal Value As Double)
    Let mCost = Value
End Property

'##ModelId=3D7729080032
Public Property Get Price() As Currency
   Let Price = mPrice
End Property

'##ModelId=3D772907033E
Public Property Let Price(ByVal Value As Currency)
    Let mPrice = Value
End Property

'##ModelId=3D77290702D0
Public Property Get Quantity() As Long
   Let Quantity = mQuantity
End Property

'##ModelId=3D77290701F4
Public Property Let Quantity(ByVal Value As Long)
    Let mQuantity = Value
End Property

Public Property Let QuantityPerUnit(ByVal Value As Long)
    Let mQuantityPerUnit = Value
End Property

'##ModelId=3D7729070186
Public Property Get AuthorisedDate() As Date
   Let AuthorisedDate = mAuthorisedDate
End Property

'##ModelId=3D7729060348
Public Property Get AuthorisedBy() As String
   Let AuthorisedBy = mAuthorisedBy
End Property

'##ModelId=3D772906023A
Public Property Get DateCreated() As Date
   Let DateCreated = mDateCreated
End Property

'##ModelId=3D7729060190
Public Property Let DateCreated(ByVal Value As Date)
    Let mDateCreated = Value
End Property

'##ModelId=3D7729060122
Public Property Get PartCode() As String
   Let PartCode = mPartCode
End Property

'##ModelId=3D7729060082
Public Property Let PartCode(ByVal Value As String)
    Let mPartCode = Value
End Property

'##ModelId=3D7729060014
Public Property Get LineNo() As String
   Let LineNo = mLineNo
End Property

'##ModelId=3D7729050352
Public Property Let LineNo(ByVal Value As String)
    Let mLineNo = Value
End Property

'##ModelId=3D7729050320
Public Property Get AdjustmentCode() As String
   Let AdjustmentCode = mAdjustmentCode
End Property

'##ModelId=3D7729050276
Public Property Let AdjustmentCode(ByVal Value As String)
    Let mAdjustmentCode = Value
End Property

'##ModelId=3D7729050244
Public Property Get BatchNo() As String
   Let BatchNo = mBatchNo
End Property


'##ModelId=3D772905019A
Public Property Let BatchNo(ByVal Value As String)
    Let mBatchNo = Value
End Property

Private Function NextBatchNumber() As Long

Dim lLastBatch As Long
Dim vntValue    As Variant
    
    vntValue = m_oSession.Database.GetAggregateValue(AGG_MAX, IBo_GetField(FID_STOCKWOF_BatchNo), Nothing)
    If IsNull(vntValue) Then
        lLastBatch = 0
    Else
        lLastBatch = vntValue
    End If
    NextBatchNumber = lLastBatch + 1
    
End Function
Public Sub Authorise(strAuthorisedBy As String, dteAuthorisedWhen As Date)

    'update existing entry with Authorisation info
    mAuthorisedBy = strAuthorisedBy
    mAuthorisedDate = dteAuthorisedWhen
    Call Save(SaveTypeIfExists)

End Sub

Private Function NextLineNumber(strBatchNumber As String) As Long

Dim vntValue    As Variant
Dim lLastLineNo As Long
Dim oRow        As IRowSelector
Dim oField      As IField
    
    'Save updated fields back to Database
    Set oRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    'Create field for selection criteria Key into
    Set oField = GetField(FID_STOCKWOF_BatchNo)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = mBatchNo
    Call oRow.AddSelection(CMP_EQUAL, oField)
    
    vntValue = m_oSession.Database.GetAggregateValue(AGG_MAX, IBo_GetField(FID_STOCKWOF_LineNo), oRow)
    If IsNull(vntValue) Then
        lLastLineNo = 0
    Else
        lLastLineNo = Val(vntValue)
    End If
    NextLineNumber = lLastLineNo + 1
    
End Function

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
Dim oStockLog As cStock_Wickes.cStockLog

    Save = False
    If LenB(mBatchNo) = 0 Then mBatchNo = Format$(NextBatchNumber, "000000")
    If LenB(mLineNo) = 0 Then mLineNo = Format$(NextLineNumber(mBatchNo), "000000")
    If mQuantityPerUnit = 0 Then mQuantityPerUnit = 1
        
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        If eSave = SaveTypeIfExists Then 'delete existing entry as fields are not all modifiable
            Call IBo_Delete
            eSave = SaveTypeIfNew
        End If
        Save = m_oSession.Database.SaveBo(eSave, Me)
        If (eSave = SaveTypeDelete) Then
            'Reverse out previous stock adjustment
            Set oStockLog = m_oSession.Database.CreateBusinessObject(CLASSID_STOCKLOG)
            oStockLog.LogDate = Now()
            oStockLog.LogTime = Format$(Now, "HHMMSS")
            oStockLog.PartCode = mPartCode
            oStockLog.TransactionKey = Format$(oStockLog.LogDate, "MM/DD/YY") & mAdjustmentCode & mComment
            oStockLog.StartWriteOffSOH = mOrigQuantity
            oStockLog.EndWriteOffSOH = mOrigQuantity + mQuantity
            MsgBox "Stock Adjustment Save Removed"
            'Call oStockLog.SaveStockAdjustment
            Set oStockLog = Nothing
        End If
        If (eSave = SaveTypeIfExists) Or (eSave = SaveTypeIfNew) Then
            'Record Stock movement log entry
            Set oStockLog = m_oSession.Database.CreateBusinessObject(CLASSID_STOCKLOG)
            oStockLog.LogDate = Now
            oStockLog.LogTime = Format$(Now, "HHMMSS")
            oStockLog.PartCode = mPartCode
            oStockLog.TransactionKey = Format$(oStockLog.LogDate, "MM/DD/YY") & mAdjustmentCode & mComment
            oStockLog.StartWriteOffSOH = mOrigQuantity
            oStockLog.EndWriteOffSOH = mOrigQuantity - mQuantity
            MsgBox "Stock Adjustment Save Removed"
            'Call oStockLog.SaveStockAdjustment
            Set oStockLog = Nothing
            mOrigQuantity = mQuantity
        End If
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cStockWriteOff
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_STOCKWOF * &H10000) + 1 To FID_STOCKWOF_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Stock Write Off " & mPartCode

End Property


Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub
Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_STOCKWOF

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property


Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cStockWriteOff

End Function


Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_STOCKWOF, FID_STOCKWOF_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)

        Case (FID_STOCKWOF_BatchNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mBatchNo
        Case (FID_STOCKWOF_LineNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLineNo
        Case (FID_STOCKWOF_AdjustmentCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAdjustmentCode
        Case (FID_STOCKWOF_PartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartCode
        Case (FID_STOCKWOF_DateCreated):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDateCreated
        Case (FID_STOCKWOF_AuthorisedBy):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAuthorisedBy
        Case (FID_STOCKWOF_AuthorisedDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mAuthorisedDate
        Case (FID_STOCKWOF_Quantity):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mQuantity
        Case (FID_STOCKWOF_Price):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mPrice
        Case (FID_STOCKWOF_Cost):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mCost
        Case (FID_STOCKWOF_AdjustedBy):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAdjustedBy
        Case (FID_STOCKWOF_Comment):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mComment
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cStockWriteOff
    Set m_oSession = oSession
    Set Initialise = Me
End Function


Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_STOCKWOF_BatchNo):        mBatchNo = oField.ValueAsVariant
            Case (FID_STOCKWOF_LineNo):         mLineNo = oField.ValueAsVariant
            Case (FID_STOCKWOF_AdjustmentCode): mAdjustmentCode = oField.ValueAsVariant
            Case (FID_STOCKWOF_PartCode):       mPartCode = oField.ValueAsVariant
            Case (FID_STOCKWOF_DateCreated):    mDateCreated = oField.ValueAsVariant
            Case (FID_STOCKWOF_AuthorisedBy):   mAuthorisedBy = oField.ValueAsVariant
            Case (FID_STOCKWOF_AuthorisedDate): mAuthorisedDate = oField.ValueAsVariant
            Case (FID_STOCKWOF_Quantity):       mQuantity = oField.ValueAsVariant
                                                mOrigQuantity = oField.ValueAsVariant
            Case (FID_STOCKWOF_Price):          mPrice = oField.ValueAsVariant
            Case (FID_STOCKWOF_Cost):           mCost = oField.ValueAsVariant
            Case (FID_STOCKWOF_AdjustedBy):     mAdjustedBy = oField.ValueAsVariant
            Case (FID_STOCKWOF_Comment):        mComment = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_STOCKWOF_END_OF_STATIC

End Function


Public Function CreateBatchHeader(ByVal dteBatchDate As Date, ByVal strUserID As String) As String

    'Reset header fields not used
    mAdjustmentCode = vbNullString
    mPartCode = vbNullString
    mDateCreated = dteBatchDate
    mAuthorisedBy = vbNullString
    mQuantity = 0
    mOrigQuantity = 0
    mPrice = 0
    mCost = 0
    mAdjustedBy = strUserID
    mComment = vbNullString
    
    'Get next batch number
    mBatchNo = Format$(NextBatchNumber, "000000")
    mLineNo = "000000"
    
    'Save Business Object
    Call m_oSession.Database.SaveBo(SaveTypeIfNew, Me)
    CreateBatchHeader = mBatchNo

End Function

'<CACH>****************************************************************************************
'* Function:  UpdateBatchHeader()
'**********************************************************************************************
'* Description: Business rule called by Stock Write off editor.  Used to updates the batch
'*              header with number of items and the value of the batch.
'**********************************************************************************************
'* Parameters:
'*In:strBatchNo  String. - Btach header to update
'*In:dblQuantity Double. - Total batch quantity
'*In:dblValue    Double. - Total Batch Value
'**********************************************************************************************
'* History:
'* 08/04/03    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function UpdateBatchHeader(ByVal strBatchNo As String, ByVal dblQuantity As Double, ByVal dblValue As Double) As Boolean

Dim oRow As IRow
            
    mCost = dblValue
    mQuantity = dblQuantity
    mBatchNo = strBatchNo
    mLineNo = "000000"

    'Save updated fields back to Database
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    Call oRow.Add(GetField(FID_STOCKWOF_BatchNo))
    Call oRow.Add(GetField(FID_STOCKWOF_LineNo))
    Call oRow.Add(GetField(FID_STOCKWOF_Quantity))
    Call oRow.Add(GetField(FID_STOCKWOF_Cost))

    Call m_oSession.Database.SavePartialBo(Me, oRow)
    Set oRow = Nothing
   
End Function

