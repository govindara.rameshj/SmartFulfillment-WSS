VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPIMItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D5CB7340294"
'<CAMH>****************************************************************************************
'* Module : PIMItem
'* Date   : 23/06/06
'* Author : DaveF
'*$Archive: /Projects/OasysV2/VB/InventoryBO/PIMItem.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: DaveF $
'* $Date: 23/06/06 9:25 $
'* $Revision: 1 $
'* Versions:
'* 23/06/06    DaveF
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "PIMItem"

Private mPartCode As String
Private mCategoryCode As String
Private mCategoryDescription As String
Private mGroupCode As String
Private mGroupDescription As String
Private mSubGroupCode As String
Private mSubGroupDescription As String
Private mStyleCode As String
Private mStyleDescription As String

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

Public Property Get PartCode() As String
   Let PartCode = mPartCode
End Property

Public Property Let PartCode(ByVal Value As String)
    Let mPartCode = Value
End Property

Public Property Get CategoryCode() As String
   Let CategoryCode = mCategoryCode
End Property

Public Property Let CategoryCode(ByVal Value As String)
    Let mCategoryCode = Value
End Property

Public Property Get CategoryDescription() As String
   Let CategoryDescription = mCategoryDescription
End Property

Public Property Let CategoryDescription(ByVal Value As String)
    Let mCategoryDescription = Value
End Property

Public Property Get GroupCode() As String
   Let GroupCode = mGroupCode
End Property

Public Property Let GroupCode(ByVal Value As String)
    Let mGroupCode = Value
End Property

Public Property Get GroupDescription() As String
   Let GroupDescription = mGroupDescription
End Property

Public Property Let GroupDescription(ByVal Value As String)
    Let mGroupDescription = Value
End Property

Public Property Get SubGroupCode() As String
   Let SubGroupCode = mSubGroupCode
End Property

Public Property Let SubGroupCode(ByVal Value As String)
    Let mSubGroupCode = Value
End Property

Public Property Get SubGroupDescription() As String
   Let SubGroupDescription = mSubGroupDescription
End Property

Public Property Let SubGroupDescription(ByVal Value As String)
    Let mSubGroupDescription = Value
End Property

Public Property Get StyleCode() As String
   Let StyleCode = mStyleCode
End Property

Public Property Let StyleCode(ByVal Value As String)
    Let mStyleCode = Value
End Property

Public Property Get StyleDescription() As String
   Let StyleDescription = mStyleDescription
End Property

Public Property Let StyleDescription(ByVal Value As String)
    Let mStyleDescription = Value
End Property

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cEAN
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_EAN * &H10000) + 1 To FID_EAN_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cEAN

End Function

Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_EAN, FID_EAN_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_PIM_ITEM_PartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartCode
        Case (FID_PIM_ITEM_CategoryCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCategoryCode
        Case (FID_PIM_ITEM_CategoryDescription):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCategoryDescription
        Case (FID_PIM_ITEM_GroupCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mGroupCode
        Case (FID_PIM_ITEM_GroupDescription):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mGroupDescription
        Case (FID_PIM_ITEM_SubGroupCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSubGroupCode
        Case (FID_PIM_ITEM_SubGroupDescription):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSubGroupDescription
        Case (FID_PIM_ITEM_StyleCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mStyleCode
        Case (FID_PIM_ITEM_StyleDescription):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mStyleDescription
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cEAN
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_EAN

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "PartCode " & mPartCode

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_PIM_ITEM_PartCode): mPartCode = oField.ValueAsVariant
            Case (FID_PIM_ITEM_CategoryCode): mCategoryCode = oField.ValueAsVariant
            Case (FID_PIM_ITEM_CategoryDescription): mCategoryDescription = oField.ValueAsVariant
            Case (FID_PIM_ITEM_GroupCode): mGroupCode = oField.ValueAsVariant
            Case (FID_PIM_ITEM_GroupDescription): mGroupDescription = oField.ValueAsVariant
            Case (FID_PIM_ITEM_SubGroupCode): mSubGroupCode = oField.ValueAsVariant
            Case (FID_PIM_ITEM_SubGroupDescription): mSubGroupDescription = oField.ValueAsVariant
            Case (FID_PIM_ITEM_StyleCode): mStyleCode = oField.ValueAsVariant
            Case (FID_PIM_ITEM_StyleDescription): mStyleDescription = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_EAN_END_OF_STATIC

End Function

