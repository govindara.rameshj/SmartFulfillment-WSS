VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cMarkDownStock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : MarkDownStock
'* Date   : 02/12/08
'* Author : DaveF
'*$Archive: /Projects/OasysV2/VB/InventoryBO/cMarkDownStockm.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: MikeO $
'* $Date: 02/12/08 9:25 $
'* $Revision: 1 $
'* Versions:
'* 02/12/08    MikeO
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "MarkDownStock"

Private mSKUNumber As String
Private mSerial As String
Private mPrice As Currency
Private mLabelled As Boolean
Private mDateCreated As Date
Private mDateWrittenOff As Date
Private mDateLastRollover As Date
Private mWeekNumber As Integer
Private mReductionWeek(10) As Double
Private mReasonCode As String
Private mSoldDate As String
Private mSoldTill As String
Private mSoldTransaction As String

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

Public Property Get SKUNumber() As String
   Let SKUNumber = mSKUNumber
End Property

Public Property Let SKUNumber(ByVal Value As String)
    Let mSKUNumber = Value
End Property

Public Property Get Serial() As String
   Let Serial = mSerial
End Property

Public Property Let Serial(ByVal Value As String)
    Let mSerial = Value
End Property

Public Property Get Price() As Currency
   Let Price = mPrice
End Property

Public Property Let Price(ByVal Value As Currency)
    Let mPrice = Value
End Property

'Private mLabelled As Boolean
Public Property Get Labelled() As Boolean
   Let Labelled = mLabelled
End Property

Public Property Let Labelled(ByVal Value As Boolean)
    Let mLabelled = Value
End Property

Public Property Get DateCreated() As Date
   Let DateCreated = mDateCreated
End Property

Public Property Let DateCreated(ByVal Value As Date)
    Let mDateCreated = Value
End Property

Public Property Get DateWrittenOff() As Date
   Let DateWrittenOff = mDateWrittenOff
End Property

Public Property Let DateWrittenOff(ByVal Value As Date)
    Let mDateWrittenOff = Value
End Property

Public Property Get DateLastRollover() As Date
   Let DateLastRollover = mDateLastRollover
End Property

Public Property Let DateLastRollover(ByVal Value As Date)
    Let mDateLastRollover = Value
End Property

Public Property Get WeekNumber() As String
   Let WeekNumber = mWeekNumber
End Property

Public Property Let WeekNumber(ByVal Value As String)
    Let mWeekNumber = Value
End Property


Public Property Get ReductionWeek(Index As Variant) As Double
   Let ReductionWeek = mReductionWeek(Index)
End Property

Public Property Let ReductionWeek(Index As Variant, ByVal Value As Double)
    Let mReductionWeek(Index) = Value
End Property

Public Property Get ReasonCode() As String
   Let ReasonCode = mReasonCode
End Property

Public Property Let ReasonCode(ByVal Value As String)
    Let mReasonCode = Value
End Property

Public Property Get SoldDate() As String
   Let SoldDate = mSoldDate
End Property

Public Property Let SoldDate(ByVal Value As String)
    Let mSoldDate = Value
End Property

Public Property Get SoldTill() As String
   Let SoldTill = mSoldTill
End Property

Public Property Let SoldTill(ByVal Value As String)
    Let mSoldTill = Value
End Property

Public Property Get SoldTransaction() As String
   Let SoldTransaction = mSoldTransaction
End Property

Public Property Let SoldTransaction(ByVal Value As String)
    Let SoldTransaction = Value
End Property

'Private Function Load() As Boolean
'
'' Load up all properties from the database
'Dim oView       As IView
'Dim oRSelector  As IRowSelector
'
'    Load = False
'    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
'
'    If moRowSel Is Nothing Then
'        Set oRSelector = m_oSession.Root.CreateUtilityObject("CRowSelector")
'        Debug.Assert (LenB(mSKUNumber) <> 0)
'        oRSelector.AddSelection CMP_EQUAL, GetField(FID_MARKDOWNSTOCK_SKUNumber)
'        oRSelector.Merge GetSelectAllRow
'        ' Pass the selector to the database to get the data view
'        Set oView = m_oSession.Database.GetView(oRSelector)
'    Else
'        Call DebugMsg(MODULE_NAME, "Load", endlDebug, "GETALL")
'        Set oRSelector = GetSelectAllRow
'        Call DebugMsg(MODULE_NAME, "Load", endlDebug, "MERGE")
'        Call moRowSel.Merge(oRSelector)
'        Call DebugMsg(MODULE_NAME, "Load", endlDebug, "Get Data")
'        ' Pass the selector to the database to get the data view
'        Set oView = m_oSession.Database.GetView(moRowSel)
'    End If
'
'    Call DebugMsg(MODULE_NAME, "Load", endlDebug, "LoadFromRow")
'    If Not (oView Is Nothing) Then
'        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
'    End If
'    Call DebugMsg(MODULE_NAME, "Load", endlDebug, "Done")
'
'End Function

Private Function Load() As Boolean

' Load up all properties from the database
Dim oView       As IView

    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)

    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)

    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function

Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cMarkDownStock
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_MARKDOWNSTOCK * &H10000) + 1 To FID_MARKDOWNSTOCK_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cMarkDownStock

End Function

Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_MARKDOWNSTOCK, FID_MARKDOWNSTOCK_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_MARKDOWNSTOCK_SKUNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSKUNumber
        Case (FID_MARKDOWNSTOCK_Serial):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSerial
        Case (FID_MARKDOWNSTOCK_Price):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPrice
        Case (FID_MARKDOWNSTOCK_Labelled):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLabelled
        Case (FID_MARKDOWNSTOCK_DateCreated):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDateCreated
        Case (FID_MARKDOWNSTOCK_DateWrittenOff):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDateWrittenOff
        Case (FID_MARKDOWNSTOCK_DateLastRollover):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDateLastRollover
        Case (FID_MARKDOWNSTOCK_WeekNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mWeekNumber
        Case (FID_MARKDOWNSTOCK_ReductionWeek):
                Set GetField = SaveDoubleArray(mReductionWeek, m_oSession)
        Case (FID_MARKDOWNSTOCK_ReasonCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mReasonCode
        Case (FID_MARKDOWNSTOCK_SoldDate):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSoldDate
        Case (FID_MARKDOWNSTOCK_SoldTill):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSoldTill
        Case (FID_MARKDOWNSTOCK_SoldTransaction):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSoldTransaction
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cMarkDownStock
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_MARKDOWNSTOCK

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "PartCode " & mSKUNumber

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_MARKDOWNSTOCK_SKUNumber): mSKUNumber = oField.ValueAsVariant
            Case (FID_MARKDOWNSTOCK_Serial): mSerial = oField.ValueAsVariant
            Case (FID_MARKDOWNSTOCK_Price): mPrice = oField.ValueAsVariant
            Case (FID_MARKDOWNSTOCK_Labelled): mLabelled = oField.ValueAsVariant
            Case (FID_MARKDOWNSTOCK_DateCreated): mDateCreated = oField.ValueAsVariant
            Case (FID_MARKDOWNSTOCK_DateWrittenOff): mDateWrittenOff = oField.ValueAsVariant
            Case (FID_MARKDOWNSTOCK_DateLastRollover): mDateLastRollover = oField.ValueAsVariant
            Case (FID_MARKDOWNSTOCK_WeekNumber): mWeekNumber = oField.ValueAsVariant
            Case (FID_MARKDOWNSTOCK_ReductionWeek): Call LoadDoubleArray(mReductionWeek, oField, m_oSession)
            Case (FID_MARKDOWNSTOCK_ReasonCode): mReasonCode = oField.ValueAsVariant
            Case (FID_MARKDOWNSTOCK_SoldDate): mSoldDate = oField.ValueAsVariant
            Case (FID_MARKDOWNSTOCK_SoldTill): mSoldTill = oField.ValueAsVariant
            Case (FID_MARKDOWNSTOCK_SoldTransaction): mSoldTransaction = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_MARKDOWNSTOCK_END_OF_STATIC

End Function

'<CACH>****************************************************************************************
'* Sub:  RecordAsSold()
'**********************************************************************************************
'* Description: Business rule called by cPOSLine. Used to update information on
'*              Markdown showing that the item has been sold.
'**********************************************************************************************
'* Parameters:
'*In/Out:strSkun        String  - Unique Part Code used to load in Item if not already loaded
'*In/Out:strSerial      String  - Unique Serial number  used to load the item if not already loaded
'*In/Out:dteDateSold    Date.   - Actual date item sold
'*In/Out:strTranNumb    String. - Actual Transaction item sold on
'*In/Out:strTill        String. - Actual Till item sold on
'**********************************************************************************************
'* History:
'* 03/12/08    MikeO
'*             Header added.
'</CACH>***************************************************************************************
Public Function RecordAsSold(strSkun As String, strSerial As String, dteDateSold As Date, strTranNumb As String, strTill As String) As Boolean

Dim oRow As IRow
Dim blnSaved As Boolean

    If mSKUNumber <> strSkun Or mSerial <> strSerial Then
        Call AddLoadFilter(CMP_EQUAL, FID_MARKDOWNSTOCK_SKUNumber, strSkun)
        Call AddLoadFilter(CMP_EQUAL, FID_MARKDOWNSTOCK_Serial, strSerial)
        
        Call AddLoadField(FID_MARKDOWNSTOCK_SoldDate)
        Call AddLoadField(FID_MARKDOWNSTOCK_SoldTill)
        Call AddLoadField(FID_MARKDOWNSTOCK_SoldTransaction)
        
        Call LoadMatches
    End If
    
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_MARKDOWNSTOCK_SKUNumber))
    Call oRow.Add(GetField(FID_MARKDOWNSTOCK_Serial))
    
    'Add the field that need updating
    mSoldDate = Format(dteDateSold, "yyyy-mm-dd")
    mSoldTill = strTill
    mSoldTransaction = strTranNumb
    Call oRow.Add(GetField(FID_MARKDOWNSTOCK_SoldDate))
    Call oRow.Add(GetField(FID_MARKDOWNSTOCK_SoldTill))
    Call oRow.Add(GetField(FID_MARKDOWNSTOCK_SoldTransaction))
        
    blnSaved = m_oSession.Database.SavePartialBo(Me, oRow)
    Debug.Assert blnSaved
    Set oRow = Nothing
    
    RecordAsSold = blnSaved
    
    
    
    'Save updated fields back to Database
'    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
'
'    Call m_oSession.Database.SavePartialBo(Me, oRow)
    Set oRow = Nothing

End Function
