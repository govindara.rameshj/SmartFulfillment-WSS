VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cInventorys"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D748FE60316"
'<CAMH>****************************************************************************************
'* Module : cInventorys
'* Date   : 02/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/InventoryBO/cInventorys.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 20/05/04 9:25 $
'* $Revision: 12 $
'* Versions:
'* 02/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

'##ModelId=3D749F880334
Const MODULE_NAME As String = "cInventorys"

Const PART_CODE As Long = 1
Const DESC_POS As Long = 2
Const STATUS_POS As Long = 3

Implements IBo
Implements ISysBo

Dim EntryNo() As Long

'##ModelId=3D749F8803DE
Private mPartCode As String

'##ModelId=3D749F890064
Private mPartDescription As String

'##ModelId=3D749F890096
Private mStatusFlag As String

'##ModelId=3D749F8900D2
Private mStartOfList As Boolean

'##ModelId=3D749F890104
Private mEndOfList As Boolean

Private mView As IView

'##ModelId=3D749F890172
Private mCount As Long

'##ModelId=3D749F8901AE
Private m_oSession As Session

'##ModelId=3D749F8901E0
Private mCurrentPos As Long

Private moLoadRow As IRowSelector

Private moRowSel As IRowSelector
'##ModelId=3D749F89024E
Public Sub Locate(PartCode As String)

Dim lRowNo As Long
Dim oRow   As IRow

    For lRowNo = 1 To mView.Count Step 1
        If PartCode = mView.Row(CLng(EntryNo(lRowNo))).Field(PART_CODE).ValueAsVariant Then
            'Reset private variables
            mStartOfList = False
            mEndOfList = False
            'preserve Bookmark
            mCurrentPos = lRowNo
            'Extract entry to private variable for access by Property GET's
            Set oRow = mView.Row(CLng(EntryNo(lRowNo)))
            mPartCode = oRow.Field(PART_CODE).ValueAsVariant
            mPartDescription = oRow.Field(DESC_POS).ValueAsVariant
            mStatusFlag = oRow.Field(STATUS_POS).ValueAsVariant
            'Set BOF and EOF flags
            If mCurrentPos = 1 Then mStartOfList = True
            If mCurrentPos >= mCount Then mEndOfList = True
            Exit For
        End If
    Next lRowNo

End Sub

'##ModelId=3D749F8902BC
Public Sub MoveFirst()

Dim oRow As IRow

    mStartOfList = True
    mEndOfList = False
    mCurrentPos = 1
    If EntryNo(mCurrentPos) = 0 Then
        mEndOfList = True
        Exit Sub
    End If
    'Extract Row into private variable for access by Property GET's
    Set oRow = mView.Row(CLng(EntryNo(mCurrentPos)))
    mPartCode = oRow.Field(PART_CODE).ValueAsVariant
    mPartDescription = oRow.Field(DESC_POS).ValueAsVariant
    mStatusFlag = oRow.Field(STATUS_POS).ValueAsVariant
    If mCurrentPos >= mCount Then mEndOfList = True

End Sub

'##ModelId=3D749F8902F8
Public Sub MoveLast()
    
Dim oRow As IRow
    
    mEndOfList = True
    mStartOfList = False
    mCurrentPos = mCount
    'Extract Row into private variable for access by Property GET's
    Set oRow = mView.Row(CLng(EntryNo(mCurrentPos)))
    mPartCode = oRow.Field(PART_CODE).ValueAsVariant
    mPartDescription = oRow.Field(DESC_POS).ValueAsVariant
    mStatusFlag = oRow.Field(STATUS_POS).ValueAsVariant
    If mCurrentPos = 1 Then mStartOfList = True

End Sub

'##ModelId=3D749F8902F9
Public Property Get Count() As Long
   Count = mCount
End Property

'##ModelId=3D749F89032A
Public Sub MoveNext()

Dim oRow As IRow
    
    If mEndOfList = True Then
        Call Err.Raise(CLASSID_INVENTORYS, , "Unable to move past end of Inventory List")
        Exit Sub
    End If
    mStartOfList = False
    mEndOfList = False
    mCurrentPos = mCurrentPos + 1
    'Extract Row into private variable for access by Property GET's
    Set oRow = mView.Row(CLng(EntryNo(mCurrentPos)))
    mPartCode = oRow.Field(PART_CODE).ValueAsVariant
    mPartDescription = oRow.Field(DESC_POS).ValueAsVariant
    mStatusFlag = oRow.Field(STATUS_POS).ValueAsVariant
    If mCurrentPos >= mCount Then mEndOfList = True
    
End Sub

'##ModelId=3D749F890366
Public Sub MovePrev()

Dim oRow As IRow
    
    If mStartOfList = True Then
        Call Err.Raise(CLASSID_INVENTORYS, , "Unable to move past start of Inventory List")
        Exit Sub
    End If
    mEndOfList = False
    mStartOfList = False
    mCurrentPos = mCurrentPos - 1
    'Extract Row into private variable for access by Property GET's
    Set oRow = mView.Row(CLng(EntryNo(mCurrentPos)))
    mPartCode = oRow.Field(PART_CODE).ValueAsVariant
    mPartDescription = oRow.Field(DESC_POS).ValueAsVariant
    mStatusFlag = oRow.Field(STATUS_POS).ValueAsVariant
    If mCurrentPos = 1 Then mStartOfList = True

End Sub

'##ModelId=3D749F890398
Public Sub Retrieve()
    Call Load
End Sub

Public Property Get EntryString() As String

    EntryString = mPartCode & vbTab & mPartDescription & vbTab & StatusFlag
    
End Property

'##ModelId=3D749F890399
Public Function GetList() As Boolean

Dim oRSelector  As IRowSelector
Dim nFid        As Long
Dim oView       As IView
Dim lRowNo      As Long
Dim intFieldNo  As Integer
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
'    If lenb(lId) <> 0 Then mPartCode = sId
    
    Set oRSelector = m_oSession.Root.CreateUtilityObject("CRowSelector")
    'Add in selector first to overide the ALL's
    
    Call oRSelector.AddSelectAll(FID_INVENTORYS_PartCode)
    Call oRSelector.AddSelectAll(FID_INVENTORYS_Description)
    Call oRSelector.AddSelectAll(FID_INVENTORYS_Flag5)
    
    
    If Not moRowSel Is Nothing Then
        For intFieldNo = 1 To moRowSel.Count Step 1
            Call oRSelector.AddSelection(moRowSel.FieldSelector(intFieldNo).Comparator, moRowSel.FieldSelector(intFieldNo).Field.Interface)
        Next intFieldNo
    End If
    
    mStartOfList = True
    mEndOfList = True
    'Set mcolPartCode = New Collection
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oRSelector)
    Call DebugMsg(MODULE_NAME, "GetList", endlDebug, m_oSession.Database.GetSelectView(oRSelector))
    Call DebugMsg(MODULE_NAME, "GetList", endlDebug, "Got Data-" & Now())
    mCount = 0
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then
            mEndOfList = False
            Call LoadFromRow(oView.Row(1))
            mCurrentPos = 1
        End If
        Set mView = oView
        mCount = oView.Count
    End If
    'set up array to hold pointers to valid entries in List - this will start as 1=1 etc
    ReDim EntryNo(mCount + 1)
    For nFid = 1 To mCount Step 1
        EntryNo(nFid) = nFid
    Next nFid
    Call MoveFirst

End Function

Public Function GetSelectSQL() As String

Dim oRSelector  As IRowSelector
Dim nFid        As Long
Dim oView       As IView
Dim lRowNo      As Long
Dim intFieldNo  As Integer
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
'    If lenb(lId) <> 0 Then mPartCode = sId
    
    Set oRSelector = m_oSession.Root.CreateUtilityObject("CRowSelector")
    'Add in selector first to overide the ALL's
    
    Call oRSelector.AddSelectAll(FID_INVENTORYS_PartCode)
    Call oRSelector.AddSelectAll(FID_INVENTORYS_Description)
    Call oRSelector.AddSelectAll(FID_INVENTORYS_Flag5)
    
    
    If Not moRowSel Is Nothing Then
        For intFieldNo = 1 To moRowSel.Count Step 1
            Call oRSelector.AddSelection(moRowSel.FieldSelector(intFieldNo).Comparator, moRowSel.FieldSelector(intFieldNo).Field.Interface)
        Next intFieldNo
    End If
    
    'Set mcolPartCode = New Collection
    ' Pass the selector to the database to get the data view
    GetSelectSQL = m_oSession.Database.GetSelectView(moLoadRow, oRSelector)

End Function
'##ModelId=3D749F8903D4
Public Sub OrderBy()
End Sub

'##ModelId=3D749F8A001E
Public Sub Filter()

Dim lRowNo  As Long
Dim lMaxPos As Long
Dim oRow    As IRow
Dim PartCode As String

    lMaxPos = 0
    'Step through each Row of data and find matches
    For lRowNo = 1 To mView.Count Step 1
        Set oRow = mView.Row(CLng(lRowNo))
        If InStr(oRow(lRowNo).Field(PART_CODE).StringValue, PartCode) > 0 Then
            lMaxPos = lMaxPos + 1
            EntryNo(lMaxPos) = lRowNo
        Else 'if no match in Party Code then look at Description
            If InStr(oRow(lRowNo).Field(DESC_POS).StringValue, PartCode) > 0 Then
                lMaxPos = lMaxPos + 1
                EntryNo(lMaxPos) = lRowNo
            End If
        End If
    Next lRowNo

End Sub
Public Sub SeekPosition(ByVal lNewPos As Long)

Dim oRow As IRow
    
    If lNewPos < 1 Or lNewPos > mCount Then
        Call Err.Raise(CLASSID_INVENTORYS, , "Unable to seek outside of Inventory List")
        Exit Sub
    End If
    mEndOfList = False
    mStartOfList = False
    mCurrentPos = lNewPos
    'Extract Row into private variable for access by Property GET's
    Set oRow = mView.Row(CLng(EntryNo(mCurrentPos)))
    mPartCode = oRow.Field(PART_CODE).ValueAsVariant
    mPartDescription = oRow.Field(DESC_POS).ValueAsVariant
    mStatusFlag = oRow.Field(STATUS_POS).ValueAsVariant
    If mCurrentPos = 1 Then mStartOfList = True
    If mCurrentPos = mCount Then mEndOfList = True

End Sub

'##ModelId=3D749F8A001F
Public Property Get EndOfList() As Boolean
   Let EndOfList = mEndOfList
End Property


'##ModelId=3D749F8A008C
Public Property Get StartOfList() As Boolean
   Let StartOfList = mStartOfList
End Property

'##ModelId=3D749F8A0212
Public Property Get PartDescription() As String
   Let PartDescription = mPartDescription
End Property

'##ModelId=3D749F8A01A4
Public Property Let PartDescription(ByVal Value As String)
    Let mPartDescription = Value
End Property

'##ModelId=3D749F8A02EE
Public Property Get PartCode() As String
   Let PartCode = mPartCode
End Property


'##ModelId=3D749F8A0280
Public Property Let PartCode(ByVal Value As String)
    Let mPartCode = Value
End Property

Public Property Get StatusFlag() As String
    Select Case (mStatusFlag)
        Case ("C"): StatusFlag = "Current"
        Case Else: StatusFlag = "IST Only"
    End Select
End Property

'##ModelId=3D749F8A035C
Private Sub Class_Initialize()

    Call DebugMsg(MODULE_NAME, "Initialise", endlTraceIn)

End Sub

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 15/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
'##ModelId=3D749F8A038E
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_INVENTORYS_PartCode):          mPartCode = oField.ValueAsVariant
            Case (FID_INVENTORYS_Description):       mPartDescription = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow
'##ModelId=3D749F8B0014
Private Function IBo_CreateNew() As IBo

    Set IBo_CreateNew = New cInventorys

End Function

'##ModelId=3D749F8B0046
Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = vbNullString

End Property

'##ModelId=3D749F8B00B4
Public Function IBo_GetField(nFid As Long) As IField

    Set IBo_GetField = GetField(nFid)

End Function

'##ModelId=3D749F8B015E
Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function
'##ModelId=3D749F8B0190
Private Function Initialise(oSession As ISession) As cInventorys
    Set m_oSession = oSession
    Set Initialise = Me
End Function
'##ModelId=3D749F8B023A
Private Function IBo_Initialise(oSession As ISession) As IBo
    Set IBo_Initialise = Initialise(oSession)
End Function

'##ModelId=3D749F8B02A8
Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_INVENTORYS

End Property

'##ModelId=3D749F8B0316
Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

'##ModelId=3D749F8B0384
Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function
'##ModelId=3D749F8C000A
Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oRSelector  As IRowSelector
Dim nFid        As Long
Dim oView       As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
'    If lenb(lId) <> 0 Then mPartCode = sId
    
    Set oRSelector = m_oSession.Root.CreateUtilityObject("CRowSelector")
    oRSelector.AddSelection CMP_EQUAL, GetField(FID_INVENTORYS_PartCode)
    oRSelector.Merge GetSelectAllRow
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oRSelector)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(PART_CODE))
    End If

End Function

Public Function IBo_SetLoadField(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
'
End Function

'##ModelId=3D749F8C00AA
Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

'##ModelId=3D749F8C0154
Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

'##ModelId=3D749F8C01C2
Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_INVENTORYS_PartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartCode
        Case (FID_INVENTORYS_Description):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartDescription
        Case (FID_INVENTORYS_SupplierNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = "NA"
        Case (FID_INVENTORYS_Department):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = "NA"
        Case (FID_INVENTORYS_Flag1):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = "NA"
        Case (FID_INVENTORYS_Flag2):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = "NA"
        Case (FID_INVENTORYS_Flag3):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = "NA"
        Case (FID_INVENTORYS_Flag4):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = "NA"
        Case (FID_INVENTORYS_Flag5):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = "NA"
        Case (FID_INVENTORYS_Warehouse):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = vbNullString
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox("Damn")
    Resume Next

End Function

'##ModelId=3D749F8C0262
Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_INVENTORYS, FID_INVENTORYS_END_OF_STATIC, m_oSession)

End Function




Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cInventorys
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_INVENTORYS * &H10000) + 1 To FID_INVENTORYS_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_INVENTORYS_END_OF_STATIC

End Function

