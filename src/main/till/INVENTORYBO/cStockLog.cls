VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cStockLog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3DBFA0A202A8"
'<CAMH>****************************************************************************************
'* Module : cStockLog
'* Date   : 30/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/InventoryBO/cStockLog.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 20/05/04 9:25 $
'* $Revision: 12 $
'* Versions:
'* 30/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cStockLog"

Const TYPE_TILLSALE       As String = "01"
Const TYPE_TILLREFUND     As String = "02"
Const TYPE_PSSINSTORESALE As String = "03"
Const TYPE_MARKDOWNSALE   As String = "04"
Const TYPE_PSSSALE        As String = "11"
Const TYPE_PSSREFUND      As String = "12"
Const TYPE_SUPPRETURN     As String = "21"
Const TYPE_RELEASESUPPRTN As String = "22"
Const TYPE_NEGSTOCKADJ    As String = "31"
Const TYPE_POSSTOCKADJ    As String = "32"
Const TYPE_PSSPOSTOCKADJ  As String = "33"
Const TYPE_PSSNEGSTOCKADJ As String = "34"
Const TYPE_NEGRECEIPTADJ  As String = "35"
Const TYPE_POSRECEIPTADJ  As String = "36"
Const TYPE_NEGWRITEOFFADJ As String = "37"
Const TYPE_ISTOUT         As String = "41"
Const TYPE_ISTIN          As String = "42"
Const TYPE_PSSISTOUT      As String = "43"
Const TYPE_PSSISTIN       As String = "44"
Const TYPE_BULKTOSINGLE   As String = "51"
Const TYPE_PRICECHANGE    As String = "61"
Const TYPE_POSMARKDOWN    As String = "66"
Const TYPE_NEGMARKDOWN    As String = "67"
Const TYPE_POSWRITEOFF    As String = "68"
Const TYPE_NEGWRITEOFF    As String = "69"
Const TYPE_BBCISSUE       As String = "71"
Const TYPE_DIRECTRECEIPT  As String = "72"
Const TYPE_RECEIPTADJ     As String = "73"
Const TYPE_PSSRECEIPT     As String = "74"
Const TYPE_SYSTEMVARADJ   As String = "91"
Const TYPE_STOCKFILECONV  As String = "99"

Implements IBo
Implements ISysBo

Private mKey                As Long
Private mLogDate            As Date
Private mLogTime            As String
Private mPartCode           As String
Private mMovementType       As String

Private mMovementDesc       As String

Private mTransactionKey     As String
Private mCommedToHO         As Boolean
Private mEmployeeID         As String
Private mStartStockQty      As Long
Private mStartOpenReturnQty As Long
Private mStartPrice         As Currency
Private mStartMarkDownSOH   As Long
Private mStartWriteOffSOH   As Long
Private mEndStockQty        As Long
Private mEndOpenReturnQty   As Long
Private mEndPrice           As Currency
Private mEndMarkDownSOH     As Long
Private mEndWriteOffSOH     As Long
Private mDayNumber          As Long

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

Public Property Get Key() As Long
    Key = mKey
End Property

Public Property Let Key(ByVal Value As Long)
    mKey = Value
End Property

Public Property Get LogDate() As Date
    LogDate = mLogDate
End Property

Public Property Let LogDate(ByVal Value As Date)
    mLogDate = Value
End Property

Public Property Get LogTime() As String
    LogTime = mLogTime
End Property

Public Property Let LogTime(ByVal Value As String)
    mLogTime = Value
End Property

Public Property Get PartCode() As String
    PartCode = mPartCode
End Property

Public Property Let PartCode(ByVal Value As String)
    mPartCode = Value
End Property

Public Property Get MovementType() As String
    MovementType = mMovementType
End Property

Public Property Let MovementType(ByVal Value As String)
    mMovementType = Value
End Property

Public Property Get MovementDesc() As String
   Let MovementDesc = mMovementDesc
End Property

Public Property Let MovementDesc(ByVal Value As String)
    Let mMovementDesc = Value
End Property

Public Property Get TransactionKey() As String
    TransactionKey = mTransactionKey
End Property

Public Property Let TransactionKey(ByVal Value As String)
    mTransactionKey = Value
End Property

Public Property Get CommedToHO() As Boolean
    CommedToHO = mCommedToHO
End Property

Public Property Let CommedToHO(ByVal Value As Boolean)
    mCommedToHO = Value
End Property

Public Property Get EmployeeID() As String
    EmployeeID = mEmployeeID
End Property

Public Property Let EmployeeID(ByVal Value As String)
    mEmployeeID = Value
End Property

Public Property Get StartStockQty() As Long
    StartStockQty = mStartStockQty
End Property

Public Property Let StartStockQty(ByVal Value As Long)
    mStartStockQty = Value
End Property

Public Property Get StartOpenReturnQty() As Long
    StartOpenReturnQty = mStartOpenReturnQty
End Property

Public Property Let StartOpenReturnQty(ByVal Value As Long)
    mStartOpenReturnQty = Value
End Property

Public Property Get StartPrice() As Currency
    StartPrice = mStartPrice
End Property

Public Property Let StartPrice(ByVal Value As Currency)
    mStartPrice = Value
End Property

Public Property Get StartMarkDownSOH() As Long
    StartMarkDownSOH = mStartMarkDownSOH
End Property

Public Property Let StartMarkDownSOH(ByVal Value As Long)
    mStartMarkDownSOH = Value
End Property

Public Property Get StartWriteOffSOH() As Long
    StartWriteOffSOH = mStartWriteOffSOH
End Property

Public Property Let StartWriteOffSOH(ByVal Value As Long)
    mStartWriteOffSOH = Value
End Property

Public Property Get EndStockQty() As Long
    EndStockQty = mEndStockQty
End Property

Public Property Let EndStockQty(ByVal Value As Long)
    mEndStockQty = Value
End Property

Public Property Get EndOpenReturnQty() As Long
    EndOpenReturnQty = mEndOpenReturnQty
End Property

Public Property Let EndOpenReturnQty(ByVal Value As Long)
    mEndOpenReturnQty = Value
End Property

Public Property Get EndPrice() As Currency
    EndPrice = mEndPrice
End Property

Public Property Let EndPrice(ByVal Value As Currency)
    mEndPrice = Value
End Property

Public Property Get EndMarkDownSOH() As Long
    EndMarkDownSOH = mEndMarkDownSOH
End Property

Public Property Let EndMarkDownSOH(ByVal Value As Long)
    mEndMarkDownSOH = Value
End Property

Public Property Get EndWriteOffSOH() As Long
    EndWriteOffSOH = mEndWriteOffSOH
End Property

Public Property Let EndWriteOffSOH(ByVal Value As Long)
    mEndWriteOffSOH = Value
End Property

Public Property Get DayNumber() As Long
    DayNumber = mDayNumber
End Property

Public Property Let DayNumber(ByVal Value As Long)
    mDayNumber = Value
End Property

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Private Sub Class_Initialize()
        
    mMovementDesc = "UNKNOWN"

End Sub

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        If LenB(mEmployeeID) = 0 Then mEmployeeID = m_oSession.UserID
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
        On Error Resume Next
        Call m_oSession.Database.ExecuteCommand("UPDATE STKLOG SET RTI='S' WHERE TKEY=" & mKey)
        Call Err.Clear
    End If

End Function

Public Function SaveCustomerComplaint() As Boolean

Dim oItem As cInventory

    mMovementType = TYPE_POSWRITEOFF
    
    Set oItem = GetItem
    mStartStockQty = oItem.QuantityAtHand
    
    SaveCustomerComplaint = Save(SaveTypeIfNew)
    Call oItem.RecordItemSold(mPartCode, mStartStockQty - mEndStockQty)
    Set oItem = Nothing

End Function

Public Function SaveTillSale() As Boolean

Dim oItem As cInventory

    mMovementType = TYPE_TILLSALE
    
    Set oItem = GetItem
    mStartStockQty = oItem.QuantityAtHand
    
    SaveTillSale = Save(SaveTypeIfNew)
    Call oItem.RecordItemSold(mPartCode, mStartStockQty - mEndStockQty)
    Set oItem = Nothing

End Function

Public Function SaveTillRefund() As Boolean

Dim oItem As cInventory

    mMovementType = TYPE_TILLREFUND
    
    Set oItem = GetItem
    mStartStockQty = oItem.QuantityAtHand
    
    SaveTillRefund = Save(SaveTypeIfNew)
    Call oItem.RecordItemRefunded(mPartCode, mEndStockQty - mStartStockQty)
    Set oItem = Nothing

End Function


Private Function GetItem() As cInventory

    Set GetItem = m_oSession.Database.CreateBusinessObject(CLASSID_INVENTORY)
    Call GetItem.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, mPartCode)
    Call GetItem.AddLoadField(FID_INVENTORY_PartCode)
    Call GetItem.AddLoadField(FID_INVENTORY_QuantityAtHand)
    Call GetItem.LoadMatches
    
End Function
    

Public Function SaveISTOut() As Boolean

Dim oItem As cInventory

    mMovementType = TYPE_ISTOUT
    
    Set oItem = GetItem
    mStartStockQty = oItem.QuantityAtHand
    
    SaveISTOut = Save(SaveTypeIfNew)

    Call oItem.RecordISTItemIssued(mPartCode, mStartStockQty - mEndStockQty)
    Set oItem = Nothing

End Function

Public Function SavePriceChange() As Boolean

Dim oItem As cInventory

    mMovementType = TYPE_PRICECHANGE
    
    SavePriceChange = Save(SaveTypeIfNew)

End Function


Public Function SaveISTIn() As Boolean

Dim oItem As cInventory

    mMovementType = TYPE_ISTIN
    
    Set oItem = GetItem
    mStartStockQty = oItem.QuantityAtHand
    
    SaveISTIn = Save(SaveTypeIfNew)
    
    Call oItem.RecordISTItemReceived(mPartCode, mEndStockQty - mStartStockQty)
    Set oItem = Nothing

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cStockLog
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_STOCKLOG * &H10000) + 1 To FID_STOCKLOG_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cStockLog

End Function


Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_STOCKLOG, FID_STOCKLOG_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_STOCKLOG_Key):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mKey
        Case (FID_STOCKLOG_LogDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mLogDate
        Case (FID_STOCKLOG_LogTime):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLogTime
        Case (FID_STOCKLOG_PartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartCode
        Case (FID_STOCKLOG_MovementType):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mMovementType
        Case (FID_STOCKLOG_TransactionKey):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTransactionKey
        Case (FID_STOCKLOG_CommedToHO):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mCommedToHO
        Case (FID_STOCKLOG_EmployeeID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mEmployeeID
        Case (FID_STOCKLOG_StartStockQty):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mStartStockQty
        Case (FID_STOCKLOG_StartOpenReturnQty):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mStartOpenReturnQty
        Case (FID_STOCKLOG_StartPrice):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mStartPrice
        Case (FID_STOCKLOG_StartMarkDownSOH):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mStartMarkDownSOH
        Case (FID_STOCKLOG_StartWriteOffSOH):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mStartWriteOffSOH
        Case (FID_STOCKLOG_EndStockQty):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mEndStockQty
        Case (FID_STOCKLOG_EndOpenReturnQty):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mEndOpenReturnQty
        Case (FID_STOCKLOG_EndPrice):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mEndPrice
        Case (FID_STOCKLOG_EndMarkDownSOH):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mEndMarkDownSOH
        Case (FID_STOCKLOG_EndWriteOffSOH):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mEndWriteOffSOH
        Case (FID_STOCKLOG_DayNumber):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mDayNumber
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cStockLog
    Set m_oSession = oSession
    Set Initialise = Me
End Function




Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_STOCKLOG

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "StockLog " & mPartCode

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_STOCKLOG_Key):                mKey = oField.ValueAsVariant
            Case (FID_STOCKLOG_LogDate):            mLogDate = oField.ValueAsVariant
            Case (FID_STOCKLOG_LogTime):            mLogTime = oField.ValueAsVariant
            Case (FID_STOCKLOG_PartCode):           mPartCode = oField.ValueAsVariant
            Case (FID_STOCKLOG_MovementType):       mMovementType = oField.ValueAsVariant
                                                    Call SetMovementDesc
            Case (FID_STOCKLOG_TransactionKey):     mTransactionKey = oField.ValueAsVariant
            Case (FID_STOCKLOG_CommedToHO):         mCommedToHO = oField.ValueAsVariant
            Case (FID_STOCKLOG_EmployeeID):         mEmployeeID = oField.ValueAsVariant
            Case (FID_STOCKLOG_StartStockQty):      mStartStockQty = oField.ValueAsVariant
            Case (FID_STOCKLOG_StartOpenReturnQty): mStartOpenReturnQty = oField.ValueAsVariant
            Case (FID_STOCKLOG_StartPrice):         mStartPrice = oField.ValueAsVariant
            Case (FID_STOCKLOG_StartMarkDownSOH):   mStartMarkDownSOH = oField.ValueAsVariant
            Case (FID_STOCKLOG_StartWriteOffSOH):   mStartWriteOffSOH = oField.ValueAsVariant
            Case (FID_STOCKLOG_EndStockQty):        mEndStockQty = oField.ValueAsVariant
            Case (FID_STOCKLOG_EndOpenReturnQty):   mEndOpenReturnQty = oField.ValueAsVariant
            Case (FID_STOCKLOG_EndPrice):           mEndPrice = oField.ValueAsVariant
            Case (FID_STOCKLOG_EndMarkDownSOH):     mEndMarkDownSOH = oField.ValueAsVariant
            Case (FID_STOCKLOG_EndWriteOffSOH):     mEndWriteOffSOH = oField.ValueAsVariant
            Case (FID_STOCKLOG_DayNumber):          mDayNumber = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Sub SetMovementDesc()

    Select Case (mMovementType)
        Case (TYPE_TILLSALE):       mMovementDesc = "Sale"
        Case (TYPE_TILLREFUND):     mMovementDesc = "Refund"
        Case (TYPE_PSSINSTORESALE): mMovementDesc = "PSS In-store Sale Item"
        Case (TYPE_MARKDOWNSALE):   mMovementDesc = "Mark-down Stock Sale Item"
        Case (TYPE_PSSSALE):        mMovementDesc = "PSS Sale Item"
        Case (TYPE_PSSREFUND):      mMovementDesc = "PSS Refund Item"
        Case (TYPE_SUPPRETURN):     mMovementDesc = "Entered/Deleted Supplier Returns"
        Case (TYPE_RELEASESUPPRTN): mMovementDesc = "Released Supplier Returns"
        Case (TYPE_NEGSTOCKADJ):    mMovementDesc = "Negative Stock Adjustments"
        Case (TYPE_POSSTOCKADJ):    mMovementDesc = "Positive Stock Adjustments"
        Case (TYPE_PSSPOSTOCKADJ):  mMovementDesc = "PSS Negative Stock Adjustments"
        Case (TYPE_PSSNEGSTOCKADJ): mMovementDesc = "PSS Positive Stock Adjustments"
        Case (TYPE_NEGRECEIPTADJ):  mMovementDesc = "Negative Receipt Adjustments"
        Case (TYPE_POSRECEIPTADJ):  mMovementDesc = "Positive Receipt Adjustments"
        Case (TYPE_NEGWRITEOFFADJ): mMovementDesc = "Negative Write Off Adjustments"
        Case (TYPE_ISTOUT):         mMovementDesc = "Inter Store Transfer (Out)"
        Case (TYPE_ISTIN):          mMovementDesc = "Inter Store Transfer (In)"
        Case (TYPE_PSSISTOUT):      mMovementDesc = "PSS Inter Store Transfer (Out)"
        Case (TYPE_PSSISTIN):       mMovementDesc = "PSS Inter Store Transfer (In)"
        Case (TYPE_BULKTOSINGLE):   mMovementDesc = "Related Items Bulk to Single Transfer"
        Case (TYPE_PRICECHANGE):    mMovementDesc = "Price Change (Starting & Ending"
        Case (TYPE_POSMARKDOWN):    mMovementDesc = "Positive Mark-down Stock Transfer"
        Case (TYPE_NEGMARKDOWN):    mMovementDesc = "Negative Mark-down Stock Transfer"
        Case (TYPE_POSWRITEOFF):    mMovementDesc = "Positive Write Off Stock Transfer"
        Case (TYPE_NEGWRITEOFF):    mMovementDesc = "Negative Write Off Stock Transfer"
        Case (TYPE_BBCISSUE):       mMovementDesc = "BBC Issue Receipt"
        Case (TYPE_DIRECTRECEIPT):  mMovementDesc = "Direct Receipt"
        Case (TYPE_RECEIPTADJ):     mMovementDesc = "Receipt Adjustment"
        Case (TYPE_PSSRECEIPT):     mMovementDesc = "PSS Receipt"
        Case (TYPE_SYSTEMVARADJ):   mMovementDesc = "System Variance Adjustments"
        Case (TYPE_STOCKFILECONV):  mMovementDesc = "Stock File Conversion"
        Case Else: mMovementDesc = "UNKNOWN"
        
    End Select
    
End Sub


Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_STOCKLOG_END_OF_STATIC

End Function

