VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cStockMovement"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D3D59DE0096"
'<CAMH>****************************************************************************************
'* Module : StockMovement
'* Date   : 03/09/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/InventoryBO/StockMovement.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 20/05/04 9:24 $
'* $Revision: 12 $
'* Versions:
'* 28/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "StockMovement"

Implements IBo
Implements ISysBo

Private m_oSession As Session

'##ModelId=3D3D59E90352
Private mPartCode As String

'##ModelId=3D3D59F3012C
Private mStartQty(4) As Long

'##ModelId=3D3D59F60096
Private mStartPrice(4) As Double

'##ModelId=3D3D5A0103C0
Private mCyclicCountValue(4) As Double

'##ModelId=3D3D5A070230
Private mDRLAdjustmentValue(4) As Double

'##ModelId=3D3D5A6B005A
Private mDRLReceiptsQty(4) As Long

'##ModelId=3D3D5A0C00E6
Private mDRLReceiptsValue(4) As Double

'##ModelId=3D3D5A31038E
Private mAdjustmentsQty(4) As Long

'##ModelId=3D3D5A3700B4
Private mAdjustmentsValue(4) As Double

'##ModelId=3D3D5A440172
Private mPriceViolations(4) As Double

'##ModelId=3D58EF410118
Private mISTNETQuantity(4) As Long

'##ModelId=3D58EF5002C6
Private mISTNetValue(4) As Double

'##ModelId=3D58EF5E0370
Private mReturnsQuantity(4) As Long

'##ModelId=3D58EF670046
Private mReturnsValue(4) As Double

'##ModelId=3D58EF6E032A
Private mBulkToSingleQty(4) As Long

'##ModelId=3D58EF9E0032
Private mBulkToSingleValue(4) As Double

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private Const THIS_WEEK As Long = 1

Public Function SaveStockReceived(ByVal PartCode As String, ByVal ReceivedQty As Double, ByVal ReceivedValue As Double) As Boolean

Dim oRSelector As IRowSelector
Dim oRow       As IRow
Dim oGField    As CFieldGroup
Dim bRet       As Boolean
Dim blnCreate  As Boolean
Dim colMatch   As Collection
    
    Call AddLoadFilter(CMP_EQUAL, FID_STOCKMOVE_PartCode, PartCode)
    mPartCode = PartCode
    Call AddLoadField(FID_STOCKMOVE_PartCode)
    Call AddLoadField(FID_STOCKMOVE_DRLReceiptsQty)
    Call AddLoadField(FID_STOCKMOVE_DRLReceiptsValue)
    
    Set colMatch = LoadMatches
    If colMatch.Count = 0 Then blnCreate = True
    
    mDRLReceiptsQty(THIS_WEEK) = mDRLReceiptsQty(THIS_WEEK) + ReceivedQty
    mDRLReceiptsValue(THIS_WEEK) = mDRLReceiptsValue(THIS_WEEK) + ReceivedValue
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_STOCKMOVE_PartCode))
    ' Get the array of numbers and set occurrence
    Set oGField = GetField(FID_STOCKMOVE_DRLReceiptsQty)
    oGField.Subscript = THIS_WEEK
    ' and add to the row
    Call oRow.Add(oGField)
    ' Get the array of numbers and set occurrence
    Set oGField = GetField(FID_STOCKMOVE_DRLReceiptsValue)
    oGField.Subscript = THIS_WEEK
    ' and add to the row
    Call oRow.Add(oGField)
    
    If blnCreate = False Then
        SaveStockReceived = m_oSession.Database.SavePartialBo(Me, oRow)
    Else
        SaveStockReceived = IBo_SaveIfNew
    End If

End Function

Public Function SaveStockReturned(ByVal PartCode As String, ByVal ReturnedQty As Double, ByVal ReturnedValue As Double) As Boolean

Dim oRSelector As IRowSelector
Dim oRow       As IRow
Dim oGField    As CFieldGroup
Dim bRet       As Boolean
Dim blnCreate  As Boolean
Dim colMatch   As Collection
    
    mPartCode = PartCode
    Call AddLoadFilter(CMP_EQUAL, FID_STOCKMOVE_PartCode, PartCode)
    Call AddLoadField(FID_STOCKMOVE_ReturnsQuantity)
    Call AddLoadField(FID_STOCKMOVE_ReturnsValue)
    
    Set colMatch = LoadMatches
    If colMatch.Count = 0 Then blnCreate = True
    
    mReturnsQuantity(THIS_WEEK) = mReturnsQuantity(THIS_WEEK) + ReturnedQty
    mReturnsValue(THIS_WEEK) = mReturnsValue(THIS_WEEK) + ReturnedValue
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_STOCKMOVE_PartCode))
    ' Get the array of numbers and set occurrence
    Set oGField = GetField(FID_STOCKMOVE_ReturnsQuantity)
    oGField.Subscript = THIS_WEEK
    ' and add to the row
    Call oRow.Add(oGField)
    ' Get the array of numbers and set occurrence
    Set oGField = GetField(FID_STOCKMOVE_ReturnsValue)
    oGField.Subscript = THIS_WEEK
    ' and add to the row
    Call oRow.Add(oGField)
    
    SaveStockReturned = m_oSession.Database.SavePartialBo(Me, oRow)
    If blnCreate = False Then
        SaveStockReturned = m_oSession.Database.SavePartialBo(Me, oRow)
    Else
        SaveStockReturned = IBo_SaveIfNew
    End If

End Function

'Public Function SaveStockSold(ByVal PartCode As String, ByVal SoldQty As Long, ByVal SoldValue As Currency) As Boolean
'
'Dim oRSelector As IRowSelector
'Dim oRow       As IRow
'Dim oGField    As CFieldGroup
'Dim bRet       As Boolean
'Dim blnCreate  As Boolean
'Dim colMatch   As Collection
'
'    Call AddLoadFilter(CMP_EQUAL, FID_STOCKMOVE_PartCode, PartCode)
'    mPartCode = PartCode
'    Call AddLoadField(FID_STOCKMOVE_PartCode)
'    Call AddLoadField(FID_STOCKMOVE_SoldQty)
'    Call AddLoadField(FID_STOCKMOVE_SoldValueRetail)
'
'    Set colMatch = LoadMatches
'    If colMatch.Count = 0 Then blnCreate = True
'
'    mSoldQty(THIS_WEEK) = mSoldQty(THIS_WEEK) + SoldQty
'    mSoldValueRetail(THIS_WEEK) = mSoldValueRetail(THIS_WEEK) + SoldValue
'    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
'    ' Identify row by the key
'    Call oRow.Add(GetField(FID_STOCKMOVE_PartCode))
'    ' Get the array of numbers and set occurrence
'    Set oGField = GetField(FID_STOCKMOVE_SoldQty)
'    oGField.Subscript = THIS_WEEK
'    ' and add to the row
'    Call oRow.Add(oGField)
'    ' Get the array of numbers and set occurrence
'    Set oGField = GetField(FID_STOCKMOVE_SoldValueRetail)
'    oGField.Subscript = THIS_WEEK
'    ' and add to the row
'    Call oRow.Add(oGField)
'
'    If blnCreate = False Then
'        SaveStockSold = m_oSession.Database.SavePartialBo(Me, oRow)
'    Else
'        SaveStockSold = IBo_SaveIfNew
'    End If
'
'End Function

Public Function SaveStockAdjustValue(ByVal PartCode As String, ByVal AdjustValue As Currency) As Boolean

Dim oRSelector As IRowSelector
Dim oRow       As IRow
Dim oGField    As CFieldGroup
Dim bRet       As Boolean
Dim blnCreate  As Boolean
Dim colMatch   As Collection

    Call AddLoadFilter(CMP_EQUAL, FID_STOCKMOVE_PartCode, PartCode)
    mPartCode = PartCode
    Call AddLoadField(FID_STOCKMOVE_PartCode)
    Call AddLoadField(FID_STOCKMOVE_AdjustmentsValue)

    Set colMatch = LoadMatches
    If colMatch.Count = 0 Then blnCreate = True

    mAdjustmentsValue(THIS_WEEK) = mAdjustmentsValue(THIS_WEEK) + AdjustValue
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_STOCKMOVE_PartCode))
    ' Get the array of numbers and set occurrence
    Set oGField = GetField(FID_STOCKMOVE_AdjustmentsValue)
    oGField.Subscript = THIS_WEEK
    ' and add to the row
    Call oRow.Add(oGField)

    If blnCreate = False Then
        SaveStockAdjustValue = m_oSession.Database.SavePartialBo(Me, oRow)
    Else
        SaveStockAdjustValue = IBo_SaveIfNew
    End If

End Function

Public Property Get AdjustmentsQty(Index As Variant) As Long
    AdjustmentsQty = mAdjustmentsQty(Index)
End Property

Public Property Let AdjustmentsQty(Index As Variant, ByVal Value As Long)
    mAdjustmentsQty(Index) = Value
End Property

Public Property Get AdjustmentsValue(Index As Variant) As Double
    AdjustmentsValue = mAdjustmentsValue(Index)
End Property

Public Property Let AdjustmentsValue(Index As Variant, ByVal Value As Double)
    mAdjustmentsValue(Index) = Value
End Property

'##ModelId=3D58F39801E0
Public Property Get BulkToSingleValue(Index As Variant) As Double
   Let BulkToSingleValue = mBulkToSingleValue(Index)
End Property

'##ModelId=3D58F3970366
Public Property Let BulkToSingleValue(Index As Variant, ByVal Value As Double)
    Let mBulkToSingleValue(Index) = Value
End Property

'##ModelId=3D58F397021C
Public Property Get BulkToSingleQty(Index As Variant) As Long
   Let BulkToSingleQty = mBulkToSingleQty(Index)
End Property

'##ModelId=3D58F39603AC
Public Property Let BulkToSingleQty(Index As Variant, ByVal Value As Long)
    Let mBulkToSingleQty(Index) = Value
End Property

'##ModelId=3D58F39601F4
Public Property Get ReturnsValue(Index As Variant) As Double
   Let ReturnsValue = mReturnsValue(Index)
End Property

'##ModelId=3D58F395037A
Public Property Let ReturnsValue(Index As Variant, ByVal Value As Double)
    Let mReturnsValue(Index) = Value
End Property

'##ModelId=3D58F395023A
Public Property Get ReturnsQuantity(Index As Variant) As Long
   Let ReturnsQuantity = mReturnsQuantity(Index)
End Property

'##ModelId=3D58F3950014
Public Property Let ReturnsQuantity(Index As Variant, ByVal Value As Long)
    Let mReturnsQuantity(Index) = Value
End Property

'##ModelId=3D58F39402B2
Public Property Get ISTNetValue(Index As Variant) As Double
   Let ISTNetValue = mISTNetValue(Index)
End Property

'##ModelId=3D58F394008C
Public Property Let ISTNetValue(Index As Variant, ByVal Value As Double)
    Let mISTNetValue(Index) = Value
End Property

'##ModelId=3D58F393032A
Public Property Get ISTNETQuantity(Index As Variant) As Long
   Let ISTNETQuantity = mISTNETQuantity(Index)
End Property

'##ModelId=3D58F3930104
Public Property Let ISTNETQuantity(Index As Variant, ByVal Value As Long)
    Let mISTNETQuantity(Index) = Value
End Property

'##ModelId=3D3D5A840050
Public Sub Create()
End Sub

'##ModelId=3D3D5A940078
Public Sub Process()
End Sub

'##ModelId=3D58E24F02A8
Public Property Let StartQty(Index As Variant, ByVal Value As Long)
    Let mStartQty(Index) = Value
End Property

'##ModelId=3D58E24F0348
Public Property Get StartQty(Index As Variant) As Long
    Let StartQty = mStartQty(Index)
End Property




'##ModelId=3D58E25000AA
Public Property Get StartPrice(Index As Variant) As Double
    Let StartPrice = mStartPrice(Index)
End Property

'##ModelId=3D58E250000A
Public Property Let StartPrice(Index As Variant, ByVal Value As Double)
    Let mStartPrice(Index) = Value
End Property

'##ModelId=3D58E24F023A
Public Property Get PartCode() As String
   Let PartCode = mPartCode
End Property


'##ModelId=3D58E24F0190
Public Property Let PartCode(ByVal Value As String)
    Let mPartCode = Value
End Property

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_STOCKMOVE_PartCode):          mPartCode = oField.ValueAsVariant
            Case (FID_STOCKMOVE_StartQty):          Call LoadLongArray(mStartQty, oField, m_oSession)
            Case (FID_STOCKMOVE_StartPrice):        Call LoadDoubleArray(mStartPrice, oField, m_oSession)
            Case (FID_STOCKMOVE_CyclicCountValue):  Call LoadDoubleArray(mCyclicCountValue, oField, m_oSession)
            Case (FID_STOCKMOVE_DRLAdjustmentValue): Call LoadDoubleArray(mDRLAdjustmentValue, oField, m_oSession)
            Case (FID_STOCKMOVE_DRLReceiptsQty):    Call LoadLongArray(mDRLReceiptsQty, oField, m_oSession)
            Case (FID_STOCKMOVE_DRLReceiptsValue):  Call LoadDoubleArray(mDRLReceiptsValue, oField, m_oSession)
            Case (FID_STOCKMOVE_AdjustmentsQty):    Call LoadLongArray(mAdjustmentsQty, oField, m_oSession)
            Case (FID_STOCKMOVE_AdjustmentsValue):  Call LoadDoubleArray(mAdjustmentsValue, oField, m_oSession)
            Case (FID_STOCKMOVE_PriceViolations):   Call LoadDoubleArray(mPriceViolations, oField, m_oSession)
            Case (FID_STOCKMOVE_ISTNETQuantity):    Call LoadLongArray(mISTNETQuantity, oField, m_oSession)
            Case (FID_STOCKMOVE_ISTNetValue):       Call LoadDoubleArray(mISTNetValue, oField, m_oSession)
            Case (FID_STOCKMOVE_ReturnsQuantity):   Call LoadLongArray(mReturnsQuantity, oField, m_oSession)
            Case (FID_STOCKMOVE_ReturnsValue):      Call LoadDoubleArray(mReturnsValue, oField, m_oSession)
            Case (FID_STOCKMOVE_BulkToSingleQty):   Call LoadLongArray(mBulkToSingleQty, oField, m_oSession)
            Case (FID_STOCKMOVE_BulkToSingleValue): Call LoadDoubleArray(mBulkToSingleValue, oField, m_oSession)
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow



Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cStockMovement

End Function

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Stock Movement " & mPartCode

End Property

Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function
Private Function Initialise(oSession As ISession) As cStockMovement
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_STOCKMOVE

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function

Private Function PartialLoad(oRSelector As IRowSelector) As Boolean
' Load up just some of properties from the database - with care!
Dim oView       As IView
    
    PartialLoad = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oRSelector)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then PartialLoad = LoadFromRow(oView.Row(1))
    End If
End Function

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oRSelector  As IRowSelector
Dim nFid        As Long
Dim oView       As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
'    If lenb(lId) <> 0 Then mPartCode = sId
    
    If moRowSel Is Nothing Then
        Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
        Call moRowSel.AddSelection(CMP_EQUAL, GetField(FID_STOCKMOVE_PartCode))
    End If
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_STOCKMOVE_PartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartCode
        Case (FID_STOCKMOVE_StartQty):          Set GetField = SaveLongArray(mStartQty, m_oSession)
        Case (FID_STOCKMOVE_StartPrice):        Set GetField = SaveDoubleArray(mStartPrice, m_oSession)
        Case (FID_STOCKMOVE_CyclicCountValue):  Set GetField = SaveDoubleArray(mCyclicCountValue, m_oSession)
        Case (FID_STOCKMOVE_DRLAdjustmentValue): Set GetField = SaveDoubleArray(mDRLAdjustmentValue, m_oSession)
        Case (FID_STOCKMOVE_DRLReceiptsQty):    Set GetField = SaveLongArray(mDRLReceiptsQty, m_oSession)
        Case (FID_STOCKMOVE_DRLReceiptsValue):  Set GetField = SaveDoubleArray(mDRLReceiptsValue, m_oSession)
        Case (FID_STOCKMOVE_AdjustmentsQty):    Set GetField = SaveLongArray(mAdjustmentsQty, m_oSession)
        Case (FID_STOCKMOVE_AdjustmentsValue):  Set GetField = SaveDoubleArray(mAdjustmentsValue, m_oSession)
        Case (FID_STOCKMOVE_PriceViolations):   Set GetField = SaveDoubleArray(mPriceViolations, m_oSession)
        Case (FID_STOCKMOVE_ISTNETQuantity):    Set GetField = SaveLongArray(mISTNETQuantity, m_oSession)
        Case (FID_STOCKMOVE_ISTNetValue):       Set GetField = SaveDoubleArray(mISTNetValue, m_oSession)
        Case (FID_STOCKMOVE_ReturnsQuantity):   Set GetField = SaveLongArray(mReturnsQuantity, m_oSession)
        Case (FID_STOCKMOVE_ReturnsValue):      Set GetField = SaveDoubleArray(mReturnsValue, m_oSession)
        Case (FID_STOCKMOVE_BulkToSingleQty):   Set GetField = SaveLongArray(mBulkToSingleQty, m_oSession)
        Case (FID_STOCKMOVE_BulkToSingleValue): Set GetField = SaveDoubleArray(mBulkToSingleValue, m_oSession)
        End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox("Damn")
    Resume Next

End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_STOCKMOVE, FID_STOCKMOVE_END_OF_STATIC, m_oSession)

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database

    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cStockMovement
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_STOCKMOVE * &H10000) + 1 To FID_STOCKMOVE_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_STOCKMOVE_END_OF_STATIC

End Function

