VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPriceChange"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D5CB67C006E"
'<CAMH>****************************************************************************************
'* Module : PriceChange
'* Date   : 28/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/InventoryBO/PriceChange.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 20/05/04 9:24 $
'* $Revision: 7 $
'* Versions:
'* 28/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "PriceChange"

'##ModelId=3D5CD0760258
Private mPartCode As String

'##ModelId=3D5CD07B021C
Private mEffectiveDate As Date

'##ModelId=3D5CD08E0226
Private mNewPrice As Currency

'##ModelId=3D5CD0AC01E0
Private mChangeStatus As String

'##ModelId=3D5CD0BD037A
Private mShelfLabelPrinted As Boolean

'##ModelId=3D5CD0CA03C0
Private mAutoApplyDate As Date

'##ModelId=3D5CD0D000B4
Private mAutoAppliedDate As Date

'##ModelId=3D5CD0DA00E6
Private mMarkUpChange As Double

'##ModelId=3D5CD1610384
Private mCommedToHO As Boolean

Private mSmallLabel As Boolean
Private mMediumLabel  As Boolean
Private mLargeLabel As Boolean
Private mEventNumber As String
Private mEventPriority As String
Private mEmployeeID As String
Private mManagerID As String

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session


Public Property Get SmallLabel() As Boolean
   Let SmallLabel = mSmallLabel
End Property

Public Property Let SmallLabel(ByVal Value As Boolean)
    Let mSmallLabel = Value
End Property

Public Property Get MediumLabel() As Boolean
   Let MediumLabel = mMediumLabel
End Property

Public Property Let MediumLabel(ByVal Value As Boolean)
    Let mMediumLabel = Value
End Property

Public Property Get LargeLabel() As Boolean
   Let LargeLabel = mLargeLabel
End Property

Public Property Let LargeLabel(ByVal Value As Boolean)
    Let mLargeLabel = Value
End Property

Public Property Get EventNumber() As String
   Let EventNumber = mEventNumber
End Property

Public Property Let EventNumber(ByVal Value As String)
    Let mEventNumber = Value
End Property

Public Property Get EventPriority() As String
   Let EventPriority = mEventPriority
End Property

Public Property Let EventPriority(ByVal Value As String)
    Let mEventPriority = Value
End Property

Public Property Get EmployeeID() As String
   Let EmployeeID = mEmployeeID
End Property

Public Property Let EmployeeID(ByVal Value As String)
    Let mEmployeeID = Value
End Property

Public Property Get ManagerID() As String
   Let ManagerID = mManagerID
End Property

Public Property Let ManagerID(ByVal Value As String)
    Let mManagerID = Value
End Property

'##ModelId=3D772935005A
Public Property Get CommedToHO() As Boolean
   Let CommedToHO = mCommedToHO
End Property

'##ModelId=3D772934028A
Public Property Let CommedToHO(ByVal Value As Boolean)
    Let mCommedToHO = Value
End Property

'##ModelId=3D77292D03B6
Public Property Get MarkUpChange() As Double
   Let MarkUpChange = mMarkUpChange
End Property

'##ModelId=3D77292D026C
Public Property Let MarkUpChange(ByVal Value As Double)
    Let mMarkUpChange = Value
End Property

'##ModelId=3D77292D0190
Public Property Get AutoAppliedDate() As Date
   Let AutoAppliedDate = mAutoAppliedDate
End Property

'##ModelId=3D77292D0046
Public Property Let AutoAppliedDate(ByVal Value As Date)
    Let mAutoAppliedDate = Value
End Property

'##ModelId=3D77292C0352
Public Property Get AutoApplyDate() As Date
   Let AutoApplyDate = mAutoApplyDate
End Property

'##ModelId=3D77292C023A
Public Property Let AutoApplyDate(ByVal Value As Date)
    Let mAutoApplyDate = Value
End Property

'##ModelId=3D77292C0168
Public Property Get ShelfLabelPrinted() As Boolean
   Let ShelfLabelPrinted = mShelfLabelPrinted
End Property

'##ModelId=3D77292C001E
Public Property Let ShelfLabelPrinted(ByVal Value As Boolean)
    Let mShelfLabelPrinted = Value
End Property

'##ModelId=3D77292B0172
Public Property Get ChangeStatus() As String
   Let ChangeStatus = mChangeStatus
End Property

'##ModelId=3D77292B005A
Public Property Let ChangeStatus(ByVal Value As String)
    Let mChangeStatus = Value
End Property

'##ModelId=3D7729290294
Public Property Get NewPrice() As Currency
   Let NewPrice = mNewPrice
End Property

'##ModelId=3D7729290186
Public Property Let NewPrice(ByVal Value As Currency)
    Let mNewPrice = Value
End Property

'##ModelId=3D77292800B4
Public Property Get EffectiveDate() As Date
   Let EffectiveDate = mEffectiveDate
End Property

'##ModelId=3D7729280014
Public Property Let EffectiveDate(ByVal Value As Date)
    Let mEffectiveDate = Value
End Property

'##ModelId=3D7729270352
Public Property Get PartCode() As String
   Let PartCode = mPartCode
End Property


'##ModelId=3D77292702B2
Public Property Let PartCode(ByVal Value As String)
    Let mPartCode = Value
End Property
Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cPriceChange
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_PRICECHANGE * &H10000) + 1 To FID_PRICECHANGE_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cPriceChange

End Function


Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_PRICECHANGE, FID_PRICECHANGE_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_PRICECHANGE_PartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartCode
        Case (FID_PRICECHANGE_EffectiveDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mEffectiveDate
        Case (FID_PRICECHANGE_NewPrice):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mNewPrice
        Case (FID_PRICECHANGE_ChangeStatus):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mChangeStatus
        Case (FID_PRICECHANGE_ShelfLabelPrinted):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mShelfLabelPrinted
        Case (FID_PRICECHANGE_AutoApplyDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mAutoApplyDate
        Case (FID_PRICECHANGE_AutoAppliedDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mAutoAppliedDate
        Case (FID_PRICECHANGE_MarkUpChange):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mMarkUpChange
        Case (FID_PRICECHANGE_CommedToHO):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mCommedToHO
        Case (FID_PRICECHANGE_SmallLabel):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mSmallLabel
        Case (FID_PRICECHANGE_MediumLabel):
                 Set GetField = New CFieldBool
                GetField.ValueAsVariant = mMediumLabel
        Case (FID_PRICECHANGE_LargeLabel):
                 Set GetField = New CFieldBool
                GetField.ValueAsVariant = mLargeLabel
        Case (FID_PRICECHANGE_EventNumber):
                 Set GetField = New CFieldString
                GetField.ValueAsVariant = mEventNumber
        Case (FID_PRICECHANGE_EventPriority):
                 Set GetField = New CFieldString
                GetField.ValueAsVariant = mEventPriority
        Case (FID_PRICECHANGE_EmployeeID):
                 Set GetField = New CFieldString
                GetField.ValueAsVariant = mEmployeeID
        Case (FID_PRICECHANGE_ManagerID):
                 Set GetField = New CFieldString
                GetField.ValueAsVariant = mManagerID
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cPriceChange
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_PRICECHANGE

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Price Change " & mPartCode

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_PRICECHANGE_PartCode):            mPartCode = oField.ValueAsVariant
            Case (FID_PRICECHANGE_EffectiveDate):       mEffectiveDate = oField.ValueAsVariant
            Case (FID_PRICECHANGE_NewPrice):            mNewPrice = oField.ValueAsVariant
            Case (FID_PRICECHANGE_ChangeStatus):        mChangeStatus = oField.ValueAsVariant
            Case (FID_PRICECHANGE_ShelfLabelPrinted):   mShelfLabelPrinted = oField.ValueAsVariant
            Case (FID_PRICECHANGE_AutoApplyDate):       mAutoApplyDate = oField.ValueAsVariant
            Case (FID_PRICECHANGE_AutoAppliedDate):     mAutoAppliedDate = oField.ValueAsVariant
            Case (FID_PRICECHANGE_MarkUpChange):        mMarkUpChange = oField.ValueAsVariant
            Case (FID_PRICECHANGE_CommedToHO):          mCommedToHO = oField.ValueAsVariant
            Case (FID_PRICECHANGE_SmallLabel):          mSmallLabel = oField.ValueAsVariant
            Case (FID_PRICECHANGE_MediumLabel):         mMediumLabel = oField.ValueAsVariant
            Case (FID_PRICECHANGE_LargeLabel):          mLargeLabel = oField.ValueAsVariant
            Case (FID_PRICECHANGE_EventNumber):         mEventNumber = oField.ValueAsVariant
            Case (FID_PRICECHANGE_EventPriority):       mEventPriority = oField.ValueAsVariant
            Case (FID_PRICECHANGE_EmployeeID):          mEmployeeID = oField.ValueAsVariant
            Case (FID_PRICECHANGE_ManagerID):           mManagerID = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_PRICECHANGE_END_OF_STATIC

End Function

Public Function ApplyPriceChange(strStatus As String, ByVal blnAutoApplied As Boolean) As Boolean

Dim oItemBO     As cInventory
Dim oStkLogBO   As cStockLog
Dim curPrDiff   As Currency
Dim oRow        As IRow
Dim lngQty      As Long

    On Error GoTo Error_ApplyPriceChange
    
    Call m_oSession.Database.StartTransaction
    Set oItemBO = m_oSession.Database.CreateBusinessObject(CLASSID_INVENTORY)
    If (blnAutoApplied = False) Then
        Call oItemBO.IBo_AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, mPartCode)
        Call oItemBO.IBo_AddLoadField(FID_INVENTORY_QuantityAtHand)
        Call oItemBO.IBo_AddLoadField(FID_INVENTORY_MarkDownQuantity)
        Call oItemBO.IBo_AddLoadField(FID_INVENTORY_UnitsInOpenReturns)
        Call oItemBO.LoadMatches
        lngQty = oItemBO.QuantityAtHand + oItemBO.MarkDownQuantity + oItemBO.UnitsInOpenReturns
    End If
    
    mChangeStatus = strStatus
    Call oItemBO.ApplyPriceChange(mPartCode, mNewPrice, Date, curPrDiff, mChangeStatus, mEventNumber, mEventPriority, blnAutoApplied)
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    mEmployeeID = m_oSession.UserID
    mAutoAppliedDate = Date
    
    Call oRow.Add(GetField(FID_PRICECHANGE_PartCode))
    Call oRow.Add(GetField(FID_PRICECHANGE_EffectiveDate))
    Call oRow.Add(GetField(FID_PRICECHANGE_AutoAppliedDate))
    Call oRow.Add(GetField(FID_PRICECHANGE_EmployeeID))
    mMarkUpChange = curPrDiff * lngQty
    Call oRow.Add(GetField(FID_PRICECHANGE_MarkUpChange))
    Call oRow.Add(GetField(FID_PRICECHANGE_ChangeStatus))
    
    Call m_oSession.Database.SavePartialBo(Me, oRow)
    Set oRow = Nothing
    Set oItemBO = Nothing
    Call m_oSession.Database.CommitTransaction
    ApplyPriceChange = True
    Exit Function
    
Error_ApplyPriceChange:

    Call m_oSession.Database.RollbackTransaction
    Call Err.Raise(Err.Number, Err.Source, Err.Description)

End Function

Public Function UpdateLabelsPrinted(ByVal strSKU As String, dteDateEff As Date, ByVal strSize As String) As Boolean
                         
Dim lngRowNo As Long
Dim blnSavedRow As Boolean
Dim blnSaved As Boolean
Dim colPrices As Collection
Dim cPriceBO  As cStock_Wickes.cPriceChange
Dim oRow     As IRow 'list of fields to save

    blnSaved = True
    
    Call IBo_ClearLoadField
    Call IBo_ClearLoadFilter
    Call AddLoadFilter(CMP_EQUAL, FID_PRICECHANGE_PartCode, strSKU)
    Call AddLoadFilter(CMP_LESSEQUALTHAN, FID_PRICECHANGE_EffectiveDate, dteDateEff)
    Select Case (strSize)
        Case ("S"): Call AddLoadFilter(CMP_EQUAL, FID_PRICECHANGE_SmallLabel, True)
        Case ("M"): Call AddLoadFilter(CMP_EQUAL, FID_PRICECHANGE_MediumLabel, True)
        Case ("L"): Call AddLoadFilter(CMP_EQUAL, FID_PRICECHANGE_LargeLabel, True)
    End Select
    Call AddLoadField(FID_PRICECHANGE_PartCode)
    Call AddLoadField(FID_PRICECHANGE_EffectiveDate)
    Call AddLoadField(FID_PRICECHANGE_SmallLabel)
    Call AddLoadField(FID_PRICECHANGE_MediumLabel)
    Call AddLoadField(FID_PRICECHANGE_LargeLabel)
    Set colPrices = LoadMatches()
   
    For lngRowNo = 1 To colPrices.Count Step 1
    
        Set cPriceBO = colPrices(lngRowNo)
        
        ' and persist this new value in the database
        Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
            
        mPartCode = cPriceBO.PartCode
        mEffectiveDate = cPriceBO.EffectiveDate
        
        Call oRow.Add(GetField(FID_PRICECHANGE_PartCode))
        Call oRow.Add(GetField(FID_PRICECHANGE_EffectiveDate))
        
        Select Case (strSize)
            Case ("S"): mSmallLabel = False
                        Call oRow.Add(GetField(FID_PRICECHANGE_SmallLabel))
            Case ("M"): mMediumLabel = False
                        Call oRow.Add(GetField(FID_PRICECHANGE_MediumLabel))
            Case ("L"): mLargeLabel = False
                        Call oRow.Add(GetField(FID_PRICECHANGE_LargeLabel))
        End Select
    
        mShelfLabelPrinted = True
        Call oRow.Add(GetField(FID_PRICECHANGE_ShelfLabelPrinted))
        
        blnSaved = m_oSession.Database.SavePartialBo(Me, oRow)
        Debug.Assert blnSaved
        Set oRow = Nothing
                
        If blnSavedRow = False Then blnSaved = False
        
    Next lngRowNo
    
    UpdateLabelsPrinted = blnSaved

End Function


