VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cInputBoxEx"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : cInputBoxEx
'* Date   : 23/03/04
'* Author : MauriceM
'*$Archive: /Projects/OasysV2/VB/Input Box Extended/cInputBoxEx.cls $
'**********************************************************************************************
'* Summary: Input Box Extended Routine
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 7/04/04 11:49 $ $Revision: 2 $
'* Versions:
'* 23/03/04    MauriceM
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME       As String = "cInputBoxEx"

Public Enum enInputFormat
    enifAlphaNum = 0
    enifAlpha = 2
    enifNumeric = 3
    enifDate = 4
    enifNumbers = 5
End Enum

Private mstrResponse As String


Public Function InputBoxEx(ByVal strPrompt As String, _
                         Optional ByVal strTitle As String = vbNullString, _
                         Optional ByVal vntDefault As Variant = vbNullString, _
                         Optional ByVal enFormat As enInputFormat = enifAlphaNum, _
                         Optional ByVal blnMandatory As Boolean = False, _
                         Optional ByVal lngBackColour As Long = 12632256, _
                         Optional ByVal lngNumDecimals As Long = 0, _
                         Optional ByVal lngMaxLength As Long = 0) As String
    
' This function -
'  sets the options requested by the user
'  loads the form frmInputBoxExEx and displays the appropriate text & buttons.
' The response (e.g. vbOk) is returned to the user.

' Note: strButton1Caption, strButton2Caption & strButton3Caption are optional

'       If present they will override the normal button captions.
'       The response MUST be the same as for a normal message box
'       i.e. for vbOKCancel the default buttons are 'O.K' and 'Cancel'
'       Do NOT set strButton1Caption to 'Cancel It' and strButton2Caption to 'Accept'
'       as this would reverse the expected response codes!

' Note: lngHeightRequested is optional. If 0 the display will 'autosize'.
'       If it is too small, the program will override it.
'       It is recommended that this parameter is normally left at 0.

Dim frmInputBoxEx        As New frmInputBoxEx

Dim intCmdButton     As Integer
Dim intMessPosition  As Integer
Dim lngSpacer        As Long
Dim lngTopLab        As Long
Dim lngTopButton     As Long
Dim lngFormHeight    As Long
Dim lngButtonSpace   As Long
Dim lngButtonLeft    As Long
Dim lngWidthDerived  As Long
Dim lngMaxLen        As Long
Dim strMessDisplay   As String

    On Error GoTo MsgBoxEx_Err
    
    Load frmInputBoxEx
    
    lngSpacer = frmInputBoxEx.cmdOK.Height * 0.3
    
    Select Case (enFormat)
        Case (enifAlphaNum): frmInputBoxEx.txtInputBox.Visible = True
                             frmInputBoxEx.txtInputBox.MaxLength = lngMaxLength
        Case (enifAlpha):    frmInputBoxEx.txtInputBox.Visible = True
                             frmInputBoxEx.txtInputBox.MaxLength = lngMaxLength
        Case (enifNumbers):  frmInputBoxEx.txtInputBox.Visible = True
                             frmInputBoxEx.txtInputBox.MaxLength = lngMaxLength
        Case (enifNumeric):
                             If (lngNumDecimals > 0) Then 'a fudge to keep compatibility
                                 frmInputBoxEx.ucntInputBox.Visible = True
                                 frmInputBoxEx.ucntInputBox.DecimalPlaces = lngNumDecimals
                                 frmInputBoxEx.ucntInputBox.MaximumValue = Val(String(lngMaxLength, "9"))
                             Else
                                 frmInputBoxEx.uctnInputBox.Visible = True
                                 frmInputBoxEx.uctnInputBox.DecimalPlaces = Abs(lngNumDecimals)
                                 frmInputBoxEx.uctnInputBox.MaximumValue = Val(String(lngMaxLength, "9"))
                             End If
        Case (enifDate):     frmInputBoxEx.ucdtInputBox.Visible = True
        Case Else:
                    frmInputBoxEx.txtInputBox.Visible = True
    End Select
    
    
    strMessDisplay = strPrompt
    intMessPosition = 0
    
    ' fill the autosizing box
    ' adjust the display up to max width lngMaxLen
    Do
        Call FillLabel(frmInputBoxEx.lblMsgBox, frmInputBoxEx.Width - (frmInputBoxEx.lblMsgBox.Left * 2), strMessDisplay, intMessPosition)
        
    Loop Until intMessPosition = -1
    
    lngWidthDerived = frmInputBoxEx.lblMsgBox.Width
    frmInputBoxEx.BackColor = lngBackColour
                     
    ' width can not exceed the screen width!
    If lngWidthDerived > Screen.Width Then
        lngWidthDerived = Screen.Width
    End If
    
    lngWidthDerived = frmInputBoxEx.txtInputBox.Width + (frmInputBoxEx.txtInputBox.Left * 2)
    
    frmInputBoxEx.Width = lngWidthDerived
    frmInputBoxEx.Height = frmInputBoxEx.lblMsgBox.Height + frmInputBoxEx.txtInputBox.Height + frmInputBoxEx.cmdOK.Height + (lngSpacer * 6) + frmInputBoxEx.txtInputBox.Height
    frmInputBoxEx.txtInputBox.Top = frmInputBoxEx.lblMsgBox.Top + frmInputBoxEx.lblMsgBox.Height + lngSpacer
    frmInputBoxEx.ucdtInputBox.Top = frmInputBoxEx.txtInputBox.Top
    frmInputBoxEx.ucntInputBox.Top = frmInputBoxEx.txtInputBox.Top
    frmInputBoxEx.uctnInputBox.Top = frmInputBoxEx.txtInputBox.Top
    frmInputBoxEx.cmdOK.Top = frmInputBoxEx.txtInputBox.Top + frmInputBoxEx.txtInputBox.Height + lngSpacer
    frmInputBoxEx.cmdCancel.Top = frmInputBoxEx.cmdOK.Top
    
    Call frmInputBoxEx.lblMsgBox.Refresh
    
    lngTopLab = lngSpacer
    lngTopButton = (lngSpacer * 2) + frmInputBoxEx.lblMsgBox.Height
    ' go down one button width and 3 more spaces
    lngFormHeight = lngTopButton + (lngSpacer * 3)
                
    ' set top positions here
    frmInputBoxEx.lblMsgBox.Top = lngTopLab
    
    ' set mintResponse to -1 in case user cancels
    mstrResponse = vbNullString
    
    Call frmInputBoxEx.SetCallback(strTitle, Me, enFormat, blnMandatory, vntDefault)

    frmInputBoxEx.Show vbModal
    
    GoTo MsgBoxEx_Done
    
    Exit Function
    
MsgBoxEx_Done:
    On Error Resume Next
    Set frmInputBoxEx = Nothing
    
    InputBoxEx = mstrResponse
    
    Exit Function
    
MsgBoxEx_Err:
    strMessDisplay = "Error number " & Err.Number & vbCrLf & _
                     Err.Description
    Call MsgBox(strMessDisplay, vbCritical, "Error in routine InputBoxEx")
    mstrResponse = vbNullString
    
End Function

Friend Sub ReturnValue(ByVal strResponse As String)
    
    'Debug.Print intResponse
    
    mstrResponse = strResponse
    
End Sub


Private Sub FillLabel(ByRef labAutosizeLabel As Label, _
                      ByVal lngMaxLen As Long, _
                      ByRef strMessageAmended As String, _
                      ByRef intMessPosition As Integer)

' This routine is used to fill the label which 'autosizes'
' Because we do not want to exceed the width of the form
' carraige returns may need inserting into the displayed message.

Dim strCurrentChar As String
Dim strLeft        As String
Dim strRight       As String
Dim intPnt         As Integer
Dim intLen         As Integer

    ' Debug.Print "intMessPosition="; intMessPosition
    ' Debug.Print "Caption "; labAutosizeLabel.Caption

    If intMessPosition = 0 Then
        labAutosizeLabel.Caption = vbNullString
    End If
    
    intMessPosition = intMessPosition + 1
    
    strCurrentChar = Mid$(strMessageAmended, intMessPosition, 1)
    strLeft = vbNullString
    strRight = vbNullString
    
    If strCurrentChar = vbTab Then
        ' change tab into 8 spaces
        intLen = Len(strMessageAmended)
        intPnt = intMessPosition - 1
        If intPnt > 0 Then
            strLeft = Left$(strMessageAmended, intPnt)
        End If
        
        If intMessPosition < intLen Then
            strRight = Right$(strMessageAmended, intLen - intMessPosition)
        End If
        strMessageAmended = strLeft & Space$(8) & strRight
        ' now the current character is a space
        strCurrentChar = " "
    End If
    
    labAutosizeLabel.Caption = labAutosizeLabel.Caption + strCurrentChar
    intPnt = 0
    
    If labAutosizeLabel.Width > lngMaxLen Then
        ' is the current character a space?
        Select Case strCurrentChar
        Case Is = " "
            ' it is - so replace it
            Mid$(strMessageAmended, intMessPosition, 1) = vbCrLf
        Case Else
            ' Not a space - move back to previous space
            intPnt = intMessPosition
            Do
              intPnt = intPnt - 1
              strCurrentChar = Mid$(strMessageAmended, intPnt, 1)
            Loop Until intPnt = 1 Or strCurrentChar = " "
            
            If intPnt > 1 Then
              Mid$(strMessageAmended, intPnt, 1) = vbCrLf
            Else
              ' unable to find a space
              Mid$(strMessageAmended, intMessPosition, 1) = vbCrLf
            End If
        End Select
        intMessPosition = 0 ' flag need to start again !
    End If
    
    ' have we reached the end of strMessageAmended ?
    If intMessPosition > 0 Then
        If intMessPosition = Len(strMessageAmended) Then
            intMessPosition = -1 ' flag finished
        End If
    End If

End Sub
