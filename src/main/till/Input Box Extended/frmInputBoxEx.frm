VERSION 5.00
Object = "{9BA18739-054D-4172-8E42-118133CE2FC4}#1.0#0"; "EditDateCtl.ocx"
Object = "{FA666EFA-0D22-45D3-9849-6B0694037D1B}#2.1#0"; "EditNumberTill.ocx"
Object = "{C8C70ABD-5F8E-11D7-B075-0020AFC9A0C8}#1.0#0"; "EditNumberCtl.ocx"
Begin VB.Form frmInputBoxEx 
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "frmInputBoxEx"
   ClientHeight    =   2550
   ClientLeft      =   2445
   ClientTop       =   3480
   ClientWidth     =   6405
   Icon            =   "frmInputBoxEx.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2550
   ScaleWidth      =   6405
   ShowInTaskbar   =   0   'False
   Begin ucEditNumber.ucNumberText ucntInputBox 
      Height          =   495
      Left            =   480
      TabIndex        =   2
      Top             =   1320
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   873
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin EditNumberTill.ucTillNumberText uctnInputBox 
      Height          =   495
      Left            =   480
      TabIndex        =   4
      Top             =   1320
      Visible         =   0   'False
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   873
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin ucEditDate.ucDateText ucdtInputBox 
      Height          =   495
      Left            =   720
      TabIndex        =   3
      Top             =   1320
      Visible         =   0   'False
      Width           =   5415
      _ExtentX        =   9551
      _ExtentY        =   873
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DateFormat      =   "DD/MM/YY"
      Text            =   "23/03/04"
   End
   Begin VB.TextBox txtInputBox 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   1
      Top             =   1320
      Visible         =   0   'False
      Width           =   5895
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4920
      TabIndex        =   6
      Top             =   2040
      Width           =   1260
   End
   Begin VB.CommandButton cmdOK 
      BackColor       =   &H00C0C0FF&
      Caption         =   "OK"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3480
      MaskColor       =   &H0000FF00&
      TabIndex        =   5
      Top             =   2040
      UseMaskColor    =   -1  'True
      Width           =   1260
   End
   Begin VB.Label lblMsgBox 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "lblMsgBox"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   1470
   End
End
Attribute VB_Name = "frmInputBoxEx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'<CAMH>****************************************************************************************
'* Module: frmInputBoxEx
'* Date  : 23/03/04
'* Author: MauriceM
'**********************************************************************************************
'* Summary: Form that displays the message on screen and allows entry of value
'**********************************************************************************************
'* Versions:
'* 23/03/04    MauriceM
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME     As String = "frmInputBoxEx"
Private moInputBoxEx  As cInputBoxEx
Private menifFormat   As enInputFormat
Private mblnMandatory As Boolean

Private Sub cmdOK_Click()

Dim blnError As Boolean
    
    'Debug.Print Val(cmdMsgBox(Index).Tag)
    If (mblnMandatory = True) Then
    
        Select Case (menifFormat)
            Case (enifAlphaNum), (enifAlpha), (enifNumbers):
                If (LenB(txtInputBox.Text) = 0) Then blnError = True
            Case (enifDate):
                If ucdtInputBox.Text = "--/--/--" Then blnError = True
            Case (enifNumeric):
                If (ucntInputBox.Value = 0) And (uctnInputBox.Value = 0) Then blnError = True
                If (ucntInputBox.Visible = True) Then
                    txtInputBox.Text = ucntInputBox.Value
                Else
                    txtInputBox.Text = uctnInputBox.Value
                End If
            Case Else:
                        frmInputBoxEx.txtInputBox.Visible = True
        End Select
    End If 'field is mandatory
    
    If blnError = True Then
        Call MsgBoxEx("Valid entry required", vbExclamation + vbOKOnly, Me.Caption, , , , , RGB(192, 0, 0))
        Exit Sub
    End If
    
    If (menifFormat = enifNumeric) Then
        If (ucntInputBox.Visible = True) Then
            txtInputBox.Text = ucntInputBox.Value
        Else
            txtInputBox.Text = uctnInputBox.Value
        End If
    End If
        
    ' return the response to the Class Module
    Call moInputBoxEx.ReturnValue(txtInputBox.Text)
    
    Unload Me
    
End Sub

Private Sub cmdCancel_Click()
    
    'Debug.Print Val(cmdMsgBox(Index).Tag)
    
    ' return the response to the Class Module
    Call moInputBoxEx.ReturnValue(vbNullString)
    
    Unload Me
    
End Sub

Public Sub SetCallback(ByVal strTitle As String, _
                       ByRef oMenuObj As cInputBoxEx, _
                       ByVal enFormat As enInputFormat, _
                       ByVal blnMandatory As Boolean, _
                       ByVal vntStartValue As String)

    ' reference to the Class Module
    Set moInputBoxEx = oMenuObj
    mblnMandatory = blnMandatory
    menifFormat = enFormat
    Me.Caption = strTitle
    txtInputBox.Text = vntStartValue
    ucdtInputBox.Text = "--/--/--"
    ucntInputBox.Value = Val(vntStartValue)
    cmdCancel.Enabled = Not mblnMandatory
    
End Sub

Private Sub Form_Load()
    
    'centre form
    Me.Left = (Screen.Width - Me.Width) / 2
    Me.Top = (Screen.Height - Me.Height) / 2
    
    frmInputBoxEx.BackColor = RGB(255, 0, 0)

End Sub

Private Sub lblMsgBox_DblClick()

    lblMsgBox.ToolTipText = "InputBoxEx Ver." & App.Major & "." & App.Minor & "." & App.Revision
    ' Debug.Print "lblMsgBox_DblClick"
    
End Sub

Private Sub txtInputBox_KeyPress(KeyAscii As Integer)
        
    If (KeyAscii = 8) Or (KeyAscii = vbKeyReturn) Then Exit Sub
    Select Case (menifFormat)
        Case (enifAlpha):
            If ((KeyAscii < Asc("a")) Or (KeyAscii > Asc("z")) And (KeyAscii < Asc("A")) Or (KeyAscii > Asc("Z"))) Then
                KeyAscii = 0
            End If
        Case (enifNumbers):
            If ((KeyAscii < Asc("0")) Or (KeyAscii > Asc("9"))) And (KeyAscii <> Asc("+")) And (KeyAscii <> Asc(".")) And (KeyAscii <> Asc("-")) Then
                KeyAscii = 0
            End If
    End Select

End Sub

