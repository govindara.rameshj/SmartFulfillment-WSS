VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmStockFileUpdate 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Stock File Update"
   ClientHeight    =   3270
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9810
   Icon            =   "frmStockFileUpdate.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3270
   ScaleWidth      =   9810
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   2895
      Width           =   9810
      _ExtentX        =   17304
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmStockFileUpdate.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9948
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "22-Aug-05"
            TextSave        =   "22-Aug-05"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "12:36"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmStockFileUpdate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'*
'*$Archive: /Projects/OasysHW/VB/Stock File Update/frmStockFileUpdate.frm $
'**********************************************************************************************
'* $Author: Davidf $ $Date: 02/11/05 10:08 $ $Revision: 0 $
'* $Versions:1.0.00$
'*
'**********************************************************************************************
'* Summary: Application that is designed to read text files BCVSKU.txt and
'*                      BCVSUP.txt and import them to tables BCVSKU and BCVSUP. A text file
'*                      holding the date of the import is saved.
'*
'**********************************************************************************************
'* Versions:
'*
'* 02/11/05 DaveF   v1.0.0  Initial build. WIX1170.
'*
'* 02/05/06 DaveF   v1.0.1  Added extra debugging.
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*
'**********************************************************************************************
'</CAMH>***************************************************************************************

Option Explicit

Private Const MODULE_NAME  As String = "frmStockFileUpdate"

Private Const PRM_WIXCOMMSPATH As Integer = 914
Private Const PRM_WIXDATAPATH As Integer = 910

Private Const BCVSUP_FILE As String = "BCVSUP.txt"
Private Const BCVSKU_FILE As String = "BCVSKU.txt"
Private Const BCVSUP_TABLE As String = "BCVSUP"
Private Const BCVSKU_TABLE As String = "BCVSKU"
Private Const SUCCESS_FILE As String = "StockFileUpdate.txt"

Private mcnnDBConnection As ADODB.Connection
Private mstrWixComms     As String
Private mstrWixData      As String
Private mdatSUPFILE_DATE As Date
Private mdatSKUFILE_DATE As Date

' Form load event.
Private Sub Form_Load()

Const PROCEDURE_NAME As String = "Form_Load"

Dim objFSO        As Scripting.FileSystemObject
Dim tsSuccessFile As Scripting.TextStream
Dim strErrSource  As String
Dim strDate       As String

    On Error GoTo FormLoad_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Started on " & Now())
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Load")
    
'   Get the system setup.
    Call GetRoot
    Call InitialiseStatusBar(sbStatus)
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    
    ' Get the paths to use.
    mstrWixComms = goSession.GetParameter(PRM_WIXCOMMSPATH)
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Wix COMMS Path - " & mstrWixComms)
    Call TestPathExists(mstrWixComms)
    mstrWixData = goSession.GetParameter(PRM_WIXDATAPATH)
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Wix Data Path - " & mstrWixData)
    Call TestPathExists(mstrWixData)
    
'   Set up the database connection.
    Set mcnnDBConnection = goSession.Database.Connection
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Wix Database Connection - " & mcnnDBConnection.ConnectionString)
    If (mcnnDBConnection.State = adStateOpen) Then
        mcnnDBConnection.Close
    End If
    If (mcnnDBConnection Is Nothing) Then
        Call MsgBox("The Database connection to DSN - " & goSession.Database.Connection.DefaultDatabase & " - Failed")
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Error No - " & Err.Number & " - Error Desc - " & Err.Description)
        Call Err.Report(MODULE_NAME, PROCEDURE_NAME, 0, False)
    End If

    ' Check if the two text files exist, if not then Exit.
    If (CheckFilesExist = False) Then
        Unload Me
    End If
    
    ' Ask if the user would like to proceed, if no then Exit.
    If (DateDiff("d", mdatSUPFILE_DATE, mdatSKUFILE_DATE) <> 0) Then
        strDate = BCVSUP_FILE & " - " & CStr(mdatSUPFILE_DATE) & ", " & BCVSKU_FILE & " - " & CStr(mdatSKUFILE_DATE)
    Else
        strDate = CStr(mdatSUPFILE_DATE)
    End If
    If (MsgBox("Import files found and dated - " & Format(strDate, "dd/mm/yy") & "." & vbCrLf & vbCrLf & _
                "Do you wish to proceed?", vbQuestion + vbYesNo) = vbNo) Then
        Unload Me
    End If
    
    ' Delete the existing data and import the new data.
    If (ImportData = True) Then
        ' On success save a text file with todays date.
        Set objFSO = New Scripting.FileSystemObject
        Set tsSuccessFile = objFSO.CreateTextFile(mstrWixData & SUCCESS_FILE, True)
        Call tsSuccessFile.WriteLine(Now())
        tsSuccessFile.Close
        Set tsSuccessFile = Nothing
        Set objFSO = Nothing
        Call MsgBox("The data was imported successfully.", vbInformation + vbOKOnly, "Successful Data Import")
    End If
    
    Unload Me
    
    Exit Sub
    
FormLoad_Error:

    Set tsSuccessFile = Nothing
    Set objFSO = Nothing
    
    If (Err.Source <> "") Then
        strErrSource = PROCEDURE_NAME & " - " & Err.Source
    Else
        strErrSource = PROCEDURE_NAME
    End If
    Call MsgBox("Module: " & strErrSource & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, App.EXEName)
    Call DebugMsg(MODULE_NAME, strErrSource, endlDebug, "Error No - " & Err.Number & " - Error Desc - " & Err.Description)
    Call Err.Report(MODULE_NAME, strErrSource, 0, False)
    
    If (mcnnDBConnection.State = adStateOpen) Then
        mcnnDBConnection.Close
    End If

    Unload Me
    
End Sub

' Unload the form and empty all the used objects.
Private Sub Form_Unload(Cancel As Integer)

Const PROCEDURE_NAME As String = "Form_Unload"
    
    On Error GoTo Form_Unload_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Unload")
    
    If (mcnnDBConnection.State = adStateOpen) Then
        mcnnDBConnection.Close
    End If

    Set mcnnDBConnection = Nothing
    
    End

    Exit Sub
    
Form_Unload_Error:

    End
    
End Sub

' Check if the path exists, if not create it.
Private Sub TestPathExists(ByRef strPath As String)

Const PROCEDURE_NAME As String = "TestPathExists"

Dim objFSO As Scripting.FileSystemObject
Dim objFolder As Scripting.Folder

    On Error GoTo TestPathExists_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Test Path Exists")
    
    Set objFSO = New FileSystemObject
    
    ' Test if the folder exists.
    If (objFSO.FolderExists(strPath) = False) Then
        ' Create the folder.
        objFSO.CreateFolder (strPath)
    End If
    
    ' Make sure the path ends with '\'.
    If (Right(strPath, 1) <> "\") Then
        strPath = strPath & "\"
    End If

    Set objFSO = Nothing
    
    Exit Sub
    
TestPathExists_Error:

    Set objFSO = Nothing
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

' Check if the two text files exist, if so store the dates of the two files.
Private Function CheckFilesExist() As Boolean

Const PROCEDURE_NAME As String = "CheckFilesExist"

Dim objFSO    As Scripting.FileSystemObject
Dim objFile   As Scripting.File
Dim blnBCVSUP As Boolean
Dim blnBCVSKU As Boolean

    On Error GoTo CheckFilesExist_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Check Files Exist")
    
    Set objFSO = New FileSystemObject
    
    ' Test if the files exist, if so get the last modified date.
    blnBCVSUP = objFSO.FileExists(mstrWixComms & BCVSUP_FILE)
    If (blnBCVSUP = True) Then
        Set objFile = objFSO.GetFile(mstrWixComms & BCVSUP_FILE)
        mdatSUPFILE_DATE = objFile.DateLastModified
    End If
    blnBCVSKU = objFSO.FileExists(mstrWixComms & BCVSKU_FILE)
    If (blnBCVSKU = True) Then
        Set objFile = objFSO.GetFile(mstrWixComms & BCVSKU_FILE)
        mdatSKUFILE_DATE = objFile.DateLastModified
    End If
    
    Set objFile = Nothing
    Set objFSO = Nothing
    
    ' Warn if either file did not exist.
    CheckFilesExist = False
    If (blnBCVSUP = False) And (blnBCVSKU = False) Then
        Call MsgBox("The Files - " & vbCrLf & vbCrLf & _
                    "   " & mstrWixComms & BCVSUP_FILE & vbCrLf & _
                    "   " & mstrWixComms & BCVSKU_FILE & vbCrLf & vbCrLf & _
                    " could not be found.", vbCritical + vbOKOnly, "Import Files Not Found")
    ElseIf (blnBCVSUP = False) Then
        Call MsgBox("The File - " & vbCrLf & vbCrLf & _
                    "   " & mstrWixComms & BCVSUP_FILE & vbCrLf & vbCrLf & _
                    " could not be found.", vbCritical + vbOKOnly, "Import File - " & BCVSUP_FILE & " - Not Found")
    ElseIf (blnBCVSKU = False) Then
        Call MsgBox("The File - " & vbCrLf & vbCrLf & _
                    "   " & mstrWixComms & BCVSKU_FILE & vbCrLf & vbCrLf & _
                    " could not be found.", vbCritical + vbOKOnly, "Import File - " & BCVSKU_FILE & " - Not Found")
    Else
        CheckFilesExist = True
    End If
        
    Exit Function
    
CheckFilesExist_Error:

    CheckFilesExist = False
    
    Set objFile = Nothing
    Set objFSO = Nothing
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Function

' Delete the existing data and import the new data from the two text files.
Private Function ImportData() As Boolean

Const PROCEDURE_NAME As String = "ImportData"

Dim objFSO          As Scripting.FileSystemObject
Dim tsFileData      As Scripting.TextStream
Dim strDataLine     As String
    
Dim recData         As ADODB.Recordset
Dim strSQl          As String
Dim bytSection      As Byte
Dim intCounter      As Integer

    On Error GoTo ImportData_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Import Data")
    
    bytSection = 1
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Open Database Connection")
    If (mcnnDBConnection.State = adStateClosed) Then
        mcnnDBConnection.Open
    End If
    
    ' Delete any existing data from the two tables.
    strSQl = "DELETE FROM BCVSUP"
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "SQL - " & strSQl)
    Call mcnnDBConnection.Execute(strSQl, , adCmdText)
    bytSection = 2
    strSQl = "DELETE FROM BCVSKU"
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "SQL - " & strSQl)
    Call mcnnDBConnection.Execute(strSQl, , adCmdText)
    
    ' Import the new data.
    bytSection = 3
    ' Open the file for reading.
    Set objFSO = New FileSystemObject
    For intCounter = 1 To 2
        If (intCounter = 1) Then
            bytSection = 4
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Open File - " & mstrWixComms & BCVSUP_FILE)
            Set tsFileData = objFSO.OpenTextFile(mstrWixComms & BCVSUP_FILE, ForReading, False)
        Else
            bytSection = 5
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Open File - " & mstrWixComms & BCVSKU_FILE)
            Set tsFileData = objFSO.OpenTextFile(mstrWixComms & BCVSKU_FILE, ForReading, False)
        End If
        
        ' Import the data to the recordset.
        Set recData = New ADODB.Recordset
        If (intCounter = 1) Then
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Open Table - " & BCVSUP_TABLE)
            Call recData.Open(BCVSUP_TABLE, mcnnDBConnection, adOpenKeyset, adLockBatchOptimistic, adCmdTable)
        Else
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Open Table - " & BCVSKU_TABLE)
            Call recData.Open(BCVSKU_TABLE, mcnnDBConnection, adOpenKeyset, adLockBatchOptimistic, adCmdTable)
        End If
        
        With recData
            ' Loop over all the lines in the text file.
            Do Until (tsFileData.AtEndOfStream = True)
                strDataLine = tsFileData.ReadLine
                .AddNew
                If (intCounter = 1) Then
                    ' BCVSUP.
                    .Fields("SUPN").Value = Left(strDataLine, 5)
                    .Fields("NAME").Value = Replace(Mid(strDataLine, 6), Chr(13), "")
                Else
                    ' BCVSKU.
                    .Fields("SKUN").Value = Left(strDataLine, 6)
                    .Fields("DESCR").Value = Mid(strDataLine, 7, 40)
                    .Fields("EANN").Value = Mid(strDataLine, 47, 13)
                    ' Calculate the Wicked Barcode number.
                    .Fields("EANW").Value = GetWixBarCode(Left(strDataLine, 6))
                    .Fields("SUPN").Value = Mid(strDataLine, 60, 5)
                End If
            Loop
            
            ' Update the recordset data to the database.
            .UpdateBatch adAffectAll
        End With
        
        Set recData = Nothing
    Next intCounter
    
    Set tsFileData = Nothing
    Set objFSO = Nothing
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Import Success")

    ImportData = True
    
    Exit Function

ImportData_Error:
   
    ImportData = False
    
    Set tsFileData = Nothing
    Set objFSO = Nothing
    Set recData = Nothing
    
    Call MsgBox("There was an error importing the data.", vbCritical + vbOKOnly, "Error In Data Import")
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Section " & bytSection & _
                    " - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, "Section " & bytSection & " - " & Err.Description)

End Function

' Calculate the Wicked Barcode value.
Private Function GetWixBarCode(strSKUN As String) As String

Const PROCEDURE_NAME As String = "GetWixBarCode"

Dim lngTotal   As Long
Dim intCounter As Integer

    On Error GoTo GetWixBarCode_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Get Wix BarCode")
    
    ' The BarCode is calculated as follows,
    '   Prefix with '2'.
    '   The to get the Check Digit do, multiply each number by three or one, e.g 2123456
    '                                                                            3131313
    '   Add up the returns and take the last digit from 10, thus 29 = 10 - 9 = 1, 25 = 10 - 5 = 5, 20 = 10 - 0 = 0.
    '   Suffix the Check Digit to the rest of the number.
    strSKUN = "2" & strSKUN
    For intCounter = 1 To 7
        Select Case intCounter
            Case 1, 3, 5, 7
                lngTotal = lngTotal + (CLng(Mid(strSKUN, intCounter, 1)) * 3)
            Case 2, 4, 6
                lngTotal = lngTotal + (CLng(Mid(strSKUN, intCounter, 1)))
        End Select
    Next intCounter
    If (Right(CStr(lngTotal), 1) = "0") Then
        strSKUN = strSKUN & "0"
    Else
        strSKUN = strSKUN & CStr(10 - Right(CStr(lngTotal), 1))
    End If
    
    GetWixBarCode = strSKUN
    
    Exit Function
    
GetWixBarCode_Error:

    GetWixBarCode = ""
        
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Function
