VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cSupplierDet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : SupplierDetails
'* Date   : 03/12/07
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/PurchaseOrderBO/SuppDet.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of a single supplier
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 13/04/04 12:06 $ $Revision: 13 $
'* Versions:
'* 15/08/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************

Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_DepotType As String = "SupplierDet"

Private mSupplierNo     As String
Private mDepotType      As String
Private mOrderDepotNo   As String
Private mDeleted        As Boolean
Private mTradaNet       As Boolean
Private mBBCSiteNo      As String
Private mLeadDays(7)    As Long
Private mFixedLeadDays  As Long
Private mSOQReviewDay   As Long

Private m_oSession As Session

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Public Property Get SupplierNo() As String
    SupplierNo = mSupplierNo
End Property

Public Property Let SupplierNo(ByVal Value As String)
    mSupplierNo = Value
End Property

Public Property Get DepotType() As String
    DepotType = mDepotType
End Property

Public Property Let DepotType(ByVal Value As String)
    mDepotType = Value
End Property

Public Property Get OrderDepotNo() As String
    OrderDepotNo = mOrderDepotNo
End Property

Public Property Let OrderDepotNo(ByVal Value As String)
    mOrderDepotNo = Value
End Property

Public Property Get Deleted() As Boolean
    Deleted = mDeleted
End Property

Public Property Let Deleted(ByVal Value As Boolean)
    mDeleted = Value
End Property

Public Property Get TradaNet() As Boolean
    TradaNet = mTradaNet
End Property

Public Property Let TradaNet(ByVal Value As Boolean)
    mTradaNet = Value
End Property

Public Property Get BBCSiteNo() As String
    BBCSiteNo = mBBCSiteNo
End Property

Public Property Let BBCSiteNo(ByVal Value As String)
    mBBCSiteNo = Value
End Property

Public Property Get LeadDays(Index As Long) As Long
    LeadDays = mLeadDays(Index)
End Property

Public Property Let LeadDays(Index As Long, ByVal Value As Long)
    mLeadDays(Index) = Value
End Property

Public Property Get FixedLeadDays() As Long
    FixedLeadDays = mFixedLeadDays
End Property

Public Property Let FixedLeadDays(ByVal Value As Long)
    mFixedLeadDays = Value
End Property

Public Property Get SOQReviewDay() As Long
    SOQReviewDay = mSOQReviewDay
End Property

Public Property Let SOQReviewDay(ByVal Value As Long)
    mSOQReviewDay = Value
End Property

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 15/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_DepotType As String = MODULE_DepotType & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_SUPPLIERDET_SupplierNo):     mSupplierNo = oField.ValueAsVariant
            Case (FID_SUPPLIERDET_DepotType):      mDepotType = oField.ValueAsVariant
            Case (FID_SUPPLIERDET_OrderDepotNo):   mOrderDepotNo = oField.ValueAsVariant
            Case (FID_SUPPLIERDET_Deleted):        mDeleted = oField.ValueAsVariant
            Case (FID_SUPPLIERDET_TradaNet):       mTradaNet = oField.ValueAsVariant
            Case (FID_SUPPLIERDET_BBCSiteNo):      mBBCSiteNo = oField.ValueAsVariant
            Case (FID_SUPPLIERDET_LeadDays):       Call LoadLongArray(mLeadDays, oField, m_oSession)
            Case (FID_SUPPLIERDET_FixedLeadDays):  mFixedLeadDays = oField.ValueAsVariant
            Case (FID_SUPPLIERDET_SOQReviewDay):   mSOQReviewDay = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow
'<CACH>****************************************************************************************
'* Function:  Date NextDeliveryDate()
'**********************************************************************************************
'* Description: Function to return the next delivery using todays date and stepping forward the
'*              number of Supplier lead days, ignoring weekends.  If the SOQ delivery days are
'*              set, then continues stepping until next delivery date is found and returns that#
'*              instead.
'**********************************************************************************************
'* Parameters:
'* None.
'**********************************************************************************************
'* Returns:  Date
'**********************************************************************************************
'* History:
'* 28/01/03    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function NextDeliveryDate() As Date

'Dim lngNextDate As Long
'Dim blnUseDates As Boolean
'Dim dteTemp     As Date
'Dim oSysDate    As cSystemDates
'Dim lngDayNo    As Long
'Dim blnFound    As Boolean
'
'Const PROCEDURE_DepotType As String = MODULE_DepotType & ".NextDeliveryDate"
'
'    'Ensure that required info is loaded before attempting to calculate
'    If mblnDatesLoaded = False Then
'        Call IBo_AddLoadField(FID_SUPPLIER_LeadTimeDays)
'        Call IBo_AddLoadField(FID_SUPPLIER_SOQFrequency)
'        Call IBo_Load
'    End If
'
'    'check if SOQ delivery days has been set up, else just use Lead Days
'    blnUseDates = False
'    For lngNextDate = 1 To UBound(mSOQFrequency) Step 1
'        If mSOQFrequency(lngNextDate) = True Then
'            blnUseDates = True
'            Exit For
'        End If
'    Next lngNextDate
'
'    'Get Starting date and add lead days, stepping over weekends
'    dteTemp = Now
'    For lngNextDate = 1 To mLeadTimeDays Step 1
'        dteTemp = DateAdd("d", 1, dteTemp)
'        'if new date is at weekend then do not count
'        If (Format$(dteTemp, "w") = 1) Or (Format$(dteTemp, "w") = 7) Then lngNextDate = lngNextDate - 1
'    Next lngNextDate
'
'    'Check if delivery date must be moved to next available delivery date if set up
'    If (blnUseDates = True) Then
'        'load Cyclic delivery dates set up from system dates
'        Set oSysDate = m_oSession.Database.CreateBusinessObject(CLASSID_SYSTEMDATES)
'        Call oSysDate.IBo_AddLoadField(FID_SYSTEMDATES_TodaysDate)
'        Call oSysDate.IBo_AddLoadField(FID_SYSTEMDATES_TodaysDayNo)
'        Call oSysDate.IBo_AddLoadField(FID_SYSTEMDATES_CurrentSOQWeekNo)
'        Call oSysDate.IBo_AddLoadField(FID_SYSTEMDATES_SOQCycleLength)
'        Call oSysDate.IBo_Load
'        If (oSysDate.SOQCycleLength > 0) Then
'            'Calculate delivery dates location in cycle
'            lngDayNo = oSysDate.CurrentSOQWeekNo * 7 + oSysDate.TodaysDayNo + DateDiff("d", oSysDate.TodaysDate, dteTemp)
'            'ensure cycle date is within the max number of days in cycle
'            lngDayNo = ((lngDayNo - 1) Mod (7 * oSysDate.SOQCycleLength)) + 1
'            'Step through to end of cycle until next delivery day is found
'            For lngNextDate = lngDayNo To (7 * oSysDate.SOQCycleLength) Step 1
'                If mSOQFrequency(lngNextDate) = True Then
'                    dteTemp = DateAdd("d", lngNextDate - lngDayNo, dteTemp)
'                    blnFound = True
'                    Exit For
'                End If
'            Next lngNextDate
'            'If not found then step through from beginning of cycle
'            If blnFound = False Then
'                For lngNextDate = 1 To lngDayNo Step 1
'                    If mSOQFrequency(lngNextDate) = True Then
'                        dteTemp = DateAdd("d", lngNextDate + (7 * oSysDate.SOQCycleLength) - lngDayNo, dteTemp)
'                        Exit For
'                    End If
'                Next lngNextDate
'            End If
'        End If 'calculate next delivery date using Cycle dates
'
'    End If
'
'    'Pass out value
'    NextDeliveryDate = dteTemp
    
End Function 'NextDeliveryDate

Private Sub Class_Initialize()

    Call DebugMsg(MODULE_DepotType, "Initialise", endlTraceIn)

End Sub

Private Sub Class_Terminate()
    Call DebugMsg(MODULE_DepotType, "Class_Terminate", endlTraceIn)

    Set moRowSel = Nothing
    Set moLoadRow = Nothing
    Call DebugMsg(MODULE_DepotType, "Class_Terminate", endlTraceOut)

End Sub

Private Property Get IBo_ClassName() As String

    IBo_ClassName = "SupplierDet"
End Property

Public Function IBo_CreateNew() As IBo

    Set IBo_CreateNew = New cSupplierDet

End Function

Public Property Get IBo_DebugString() As String
    
    IBo_DebugString = vbNullString

End Property

Public Function IBo_GetField(nFid As Long) As IField

    Set IBo_GetField = GetField(nFid)

End Function

Public Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function
Private Function Initialise(oSession As ISession) As cSupplierDet
    Set m_oSession = oSession
    Set Initialise = Me
End Function
Private Function IBo_Initialise(oSession As ISession) As IBo
    Set IBo_Initialise = Initialise(oSession)
End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_SUPPLIERDET

End Property

Public Property Get IBo_ClassDepotType() As String

    IBo_ClassDepotType = MODULE_DepotType

End Property

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function
Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassDepotType)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassDepotType)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = New CRowSelector
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassDepotType) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_SUPPLIERDET_SupplierNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSupplierNo
        Case (FID_SUPPLIERDET_DepotType):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDepotType
        Case (FID_SUPPLIERDET_OrderDepotNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mOrderDepotNo
        Case (FID_SUPPLIERDET_Deleted):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mDeleted
        Case (FID_SUPPLIERDET_TradaNet):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mTradaNet
        Case (FID_SUPPLIERDET_BBCSiteNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mBBCSiteNo
        Case (FID_SUPPLIERDET_LeadDays):
                Set GetField = SaveLongArray(mLeadDays, m_oSession)
        Case (FID_SUPPLIERDET_FixedLeadDays):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mFixedLeadDays
        Case (FID_SUPPLIERDET_SOQReviewDay):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mSOQReviewDay
   End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox("Damn")
    Resume Next

End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_SUPPLIERDET, FID_SUPPLIERDET_END_OF_STATIC, m_oSession)

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_SUPPLIERDET * &H10000) + 1 To FID_SUPPLIERDET_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_DepotType As String = MODULE_DepotType & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter


'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_DepotType As String = MODULE_DepotType & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub


Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As cSupplier
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_SUPPLIERDET_END_OF_STATIC

End Function


