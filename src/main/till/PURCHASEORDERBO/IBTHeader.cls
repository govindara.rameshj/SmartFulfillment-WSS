VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cISTOutHeader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D5CB5870320"
'<CAMH>****************************************************************************************
'* Module: cISTOutHeader
'* Date  : 15/08/02
'* Author: mauricem
'*$Archive: /Projects/OasysV2/VB/PurchaseOrderBO/IBTHeader.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of the IST - Direct copy of Delivery Note
'**********************************************************************************************
'* $Author: Damians $ $Date: 7/05/04 16:16 $ $Revision: 18 $
'* Versions:
'* 15/08/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cISTOutHeader"
Const DOCTYPE_ISTOUT As Long = 2
Const ISTOUT_NOTE_FILECODE As String = "RC"

Implements IBo
Implements ISysBo

Private m_oSession As Session

Private mDocNo As String

Private mDocType As String

Private mValue As Currency

Private mCost As Double

Private mDelNoteDate As Date

Private mComm As String

Private mOrigDocKey As Long

Private mEnteredBy As String

Private mComment As String

Private mConsignmentRef As String

Private mSource As String

Private mStoreNumber As String

Private mIBT As String

Private mPrinted As Boolean

Private mConsignmentKey As String

Private mDespatchMethod As String

Private mInvCrnMatched As String

Private mInvCrnDate As Date

Private mInvCrnNumber As String

Private mVarianceValue As Currency

Private mInvoiceValue As Currency

Private mInvoiceVAT As Currency

Private mMatchingComment As String

Private mMatchingDate As Date

Private mReceiptDate As Date

Private mCarraigeValue As Currency

Private mCommit As String

Private mNarrative As String

Private mcolLines As Collection

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private mGetLines As Boolean

Private mintNarrStatus As Integer 'flag if narrative edited - -1 (Not Loaded), 0=Okay and 1 = Edited

Const NOT_LOADED As Long = -1
Const LOADED As Long = 0
Const EDITED As Long = 1

Public Property Let GetLines(ByVal Value As Boolean)
   Let mGetLines = Value
End Property


Public Property Let Printed(ByVal Value As Boolean)
    Let mPrinted = Value
End Property

Public Property Get Printed() As Boolean
   Let Printed = mPrinted
End Property
Public Property Let Narrative(ByVal Value As String)
    mNarrative = Value
    mintNarrStatus = EDITED
End Property

Public Property Get Narrative() As String

Dim oDRLNarr As cDelNoteNarrative

    If mintNarrStatus = NOT_LOADED Then
        Set oDRLNarr = m_oSession.Database.CreateBusinessObject(CLASSID_DELNOTENARRATIVE)
        Call oDRLNarr.IBo_AddLoadFilter(CMP_EQUAL, FID_DELNOTENARRATIVE_DocumentNumber, mDocNo)
        Call oDRLNarr.IBo_AddLoadFilter(CMP_EQUAL, FID_DELNOTENARRATIVE_StoreNumber, m_oSession.CurrentEnterprise.IEnterprise_StoreNumber)
        Call oDRLNarr.IBo_AddLoadFilter(CMP_EQUAL, FID_DELNOTENARRATIVE_PartCode, "000000")
        Call oDRLNarr.IBo_AddLoadFilter(CMP_EQUAL, FID_DELNOTENARRATIVE_SequenceNumber, "0000")
        Call oDRLNarr.IBo_Load
        mNarrative = oDRLNarr.NarrativeText1
        If LenB(oDRLNarr.NarrativeText2) <> 0 Then mNarrative = mNarrative & vbCrLf & oDRLNarr.NarrativeText2
        Set oDRLNarr = Nothing
        mintNarrStatus = LOADED
    End If
    Narrative = mNarrative
    
End Property


Public Function AddItem(PartCode As String) As cISTLine

Dim oLine As cPurchase_Wickes.cISTLine

    Set oLine = m_oSession.Database.CreateBusinessObject(CLASSID_ISTLINE)
    Set oLine.ISTOutNote = Me
    oLine.PartCode = PartCode
    
    If mcolLines Is Nothing Then Set mcolLines = New Collection
    Call mcolLines.Add(oLine)
    oLine.LineNo = mcolLines.Count
    Set AddItem = oLine

End Function

Public Property Get Lines() As Collection
    
    If (mcolLines Is Nothing) And (mGetLines = True) Then Call RetrieveLines
    If mcolLines Is Nothing Then Set mcolLines = New Collection
    Set Lines = mcolLines

End Property

Private Sub RetrieveLines()

Dim oLine     As cPurchase_Wickes.cISTLine
Dim lngLineNo As Long

    'If Order found then get lines for Order
    Set mcolLines = New Collection
    Set oLine = m_oSession.Database.CreateBusinessObject(CLASSID_ISTLINE)
    Call oLine.IBo_AddLoadFilter(CMP_EQUAL, FID_ISTLINE_DRLKey, mDocNo)
    Set mcolLines = oLine.IBo_LoadMatches
    'Point line to reference IST Out Header
    For lngLineNo = 1 To mcolLines.Count Step 1
        Set mcolLines(CLng(lngLineNo)).ISTOutNote = Me
    Next lngLineNo

End Sub
Public Property Get Commit() As String
   Let Commit = mCommit
End Property

Public Property Let Commit(ByVal Value As String)
    Let mCommit = Value
End Property

Public Property Get CarraigeValue() As Currency
   Let CarraigeValue = mCarraigeValue
End Property

Public Property Let CarraigeValue(ByVal Value As Currency)
    Let mCarraigeValue = Value
End Property

Public Property Get ReceiptDate() As Date
   Let ReceiptDate = mReceiptDate
End Property

Public Property Let ReceiptDate(ByVal Value As Date)
    Let mReceiptDate = Value
End Property

Public Property Get MatchingDate() As Date
   Let MatchingDate = mMatchingDate
End Property

Public Property Let MatchingDate(ByVal Value As Date)
    Let mMatchingDate = Value
End Property

Public Property Get MatchingComment() As String
   Let MatchingComment = mMatchingComment
End Property

Public Property Let MatchingComment(ByVal Value As String)
    Let mMatchingComment = Value
End Property

Public Property Get InvoiceVAT() As Currency
   Let InvoiceVAT = mInvoiceVAT
End Property

Public Property Let InvoiceVAT(ByVal Value As Currency)
    Let mInvoiceVAT = Value
End Property

Public Property Get InvoiceValue() As Currency
   Let InvoiceValue = mInvoiceValue
End Property

Public Property Let InvoiceValue(ByVal Value As Currency)
    Let mInvoiceValue = Value
End Property

Public Property Get VarianceValue() As Currency
   Let VarianceValue = mVarianceValue
End Property

Public Property Let VarianceValue(ByVal Value As Currency)
    Let mVarianceValue = Value
End Property

Public Property Get InvCrnNumber() As String
   Let InvCrnNumber = mInvCrnNumber
End Property

Public Property Let InvCrnNumber(ByVal Value As String)
    Let mInvCrnNumber = Value
End Property

Public Property Get InvCrnDate() As Date
   Let InvCrnDate = mInvCrnDate
End Property

Public Property Let InvCrnDate(ByVal Value As Date)
    Let mInvCrnDate = Value
End Property

Public Property Get InvCrnMatched() As String
   Let InvCrnMatched = mInvCrnMatched
End Property

Public Property Let InvCrnMatched(ByVal Value As String)
    Let mInvCrnMatched = Value
End Property

Public Property Get DespatchMethod() As String
   Let DespatchMethod = mDespatchMethod
End Property

Public Property Let DespatchMethod(ByVal Value As String)
    Let mDespatchMethod = Value
End Property


Public Property Get ConsignmentKey() As String
   Let ConsignmentKey = mConsignmentKey
End Property

Public Property Let ConsignmentKey(ByVal Value As String)
    Let mConsignmentKey = Value
End Property

Public Property Get IBT() As String
   Let IBT = mIBT
End Property

Public Property Let IBT(ByVal Value As String)
    Let mIBT = Value
End Property

Public Property Get StoreNumber() As String
   Let StoreNumber = mStoreNumber
End Property

Public Property Let StoreNumber(ByVal Value As String)
    Let mStoreNumber = Value
End Property

Public Property Get Source() As String
   Let Source = mSource
End Property

Public Property Let Source(ByVal Value As String)
    Let mSource = Value
End Property

Public Property Get ConsignmentRef() As String
   Let ConsignmentRef = mConsignmentRef
End Property

Public Property Let ConsignmentRef(ByVal Value As String)
    Let mConsignmentRef = Value
End Property

Public Property Get Comment() As String
   Let Comment = mComment
End Property

Public Property Let Comment(ByVal Value As String)
    Let mComment = Value
End Property

Public Property Get EnteredBy() As String
   Let EnteredBy = mEnteredBy
End Property

Public Property Let EnteredBy(ByVal Value As String)
    Let mEnteredBy = Value
End Property

Public Property Get OrigDocKey() As Long
   Let OrigDocKey = mOrigDocKey
End Property

Public Property Let OrigDocKey(ByVal Value As Long)
    Let mOrigDocKey = Value
End Property

Public Property Get Comm() As String
   Let Comm = mComm
End Property

Public Property Let Comm(ByVal Value As String)
    Let mComm = Value
End Property

Public Property Get DelNoteDate() As Date
   Let DelNoteDate = mDelNoteDate
End Property

Public Property Let DelNoteDate(ByVal Value As Date)
    Let mDelNoteDate = Value
End Property

Public Property Get Cost() As Double
   Let Cost = mCost
End Property

Public Property Let Cost(ByVal Value As Double)
    Let mCost = Value
End Property

Public Property Get Value() As Currency
   Let Value = mValue
End Property

Public Property Let Value(ByVal Value As Currency)
    Let mValue = Value
End Property

Public Property Get DocType() As String
   Let DocType = mDocType
End Property

Public Property Let DocType(ByVal Value As String)
    Let mDocType = Value
End Property

Public Property Get DocNo() As String
   Let DocNo = mDocNo
End Property


Public Property Let DocNo(ByVal Value As String)
    Let mDocNo = Value
End Property

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 15/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    mintNarrStatus = NOT_LOADED
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_ISTOUTHEADER_DocNo):            mDocNo = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_DocType):          mDocType = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_Value):            mValue = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_Cost):             mCost = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_DelNoteDate):      mDelNoteDate = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_Comm):             mComm = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_OrigDocKey):       mOrigDocKey = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_EnteredBy):        mEnteredBy = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_Comment):          mComment = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_ConsignmentRef):   mConsignmentRef = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_Source):           mSource = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_StoreNumber):      mStoreNumber = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_IBT):              mIBT = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_Printed):          mPrinted = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_ConsignmentKey):   mConsignmentKey = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_DespatchMethod):   mDespatchMethod = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_InvCrnMatched):    mInvCrnMatched = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_InvCrnDate):       mInvCrnDate = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_InvCrnNumber):     mInvCrnNumber = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_VarianceValue):    mVarianceValue = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_InvoiceValue):     mInvoiceValue = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_InvoiceVAT):       mInvoiceVAT = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_MatchingComment):  mMatchingComment = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_MatchingDate):     mMatchingDate = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_ReceiptDate):      mReceiptDate = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_CarraigeValue):    mCarraigeValue = oField.ValueAsVariant
            Case (FID_ISTOUTHEADER_Commit):           mCommit = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow



Private Sub Class_Initialize()
    
    Call DebugMsg(MODULE_NAME, "Initialise", endlTraceIn)

End Sub

Private Function IBo_CreateNew() As IBo

    Set IBo_CreateNew = New cISTOutHeader

End Function

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "IST Out No - " & mDocNo

End Property

Private Function IBo_GetField(nFid As Long) As IField

    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Private Function Initialise(oSession As ISession) As cISTOutHeader
    Set m_oSession = oSession
    Set Initialise = Me
End Function
Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_ISTOUTHEADER

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Public Function IBo_Load() As Boolean
    
    IBo_Load = Load

End Function
Private Function Load() As Boolean
    
Dim oView       As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
        
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count >= 1 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property


Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    Select Case (lFieldID)
        Case (FID_ISTOUTHEADER_DocNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDocNo
        Case (FID_ISTOUTHEADER_DocType):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDocType
        Case (FID_ISTOUTHEADER_Value):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mValue
        Case (FID_ISTOUTHEADER_Cost):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mCost
        Case (FID_ISTOUTHEADER_DelNoteDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDelNoteDate
        Case (FID_ISTOUTHEADER_Comm):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mComm
        Case (FID_ISTOUTHEADER_OrigDocKey):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mOrigDocKey
        Case (FID_ISTOUTHEADER_EnteredBy):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mEnteredBy
        Case (FID_ISTOUTHEADER_Comment):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mComment
        Case (FID_ISTOUTHEADER_ConsignmentRef):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mConsignmentRef
        Case (FID_ISTOUTHEADER_Source):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSource
        Case (FID_ISTOUTHEADER_StoreNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mStoreNumber
        Case (FID_ISTOUTHEADER_IBT):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mIBT
        Case (FID_ISTOUTHEADER_Printed):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mPrinted
        Case (FID_ISTOUTHEADER_ConsignmentKey):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mConsignmentKey
        Case (FID_ISTOUTHEADER_DespatchMethod):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDespatchMethod
        Case (FID_ISTOUTHEADER_InvCrnMatched):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mInvCrnMatched
        Case (FID_ISTOUTHEADER_InvCrnDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mInvCrnDate
        Case (FID_ISTOUTHEADER_InvCrnNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mInvCrnNumber
        Case (FID_ISTOUTHEADER_VarianceValue):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mVarianceValue
        Case (FID_ISTOUTHEADER_InvoiceValue):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mInvoiceValue
        Case (FID_ISTOUTHEADER_InvoiceVAT):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mInvoiceVAT
        Case (FID_ISTOUTHEADER_MatchingComment):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mMatchingComment
        Case (FID_ISTOUTHEADER_MatchingDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mMatchingDate
        Case (FID_ISTOUTHEADER_ReceiptDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mReceiptDate
        Case (FID_ISTOUTHEADER_CarraigeValue):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mCarraigeValue
        Case (FID_ISTOUTHEADER_Commit):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCommit
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID

End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_ISTOUTHEADER, FID_ISTOUTHEADER_END_OF_STATIC, m_oSession)

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    If LenB(mConsignmentRef) = 0 Then mConsignmentRef = "000000"
    If LenB(mConsignmentKey) = 0 Then mConsignmentKey = "000000"
    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
Dim lLineNo  As Long
Dim oPOrder  As cPurchaseOrder
Dim oPOLine  As cPurchaseLine
Dim oDelLine As cDeliveryNoteLine
Dim oSysNum  As cSystemNumbers
Dim oNarr    As cDelNoteNarrative
    
    Save = False
    mDocType = DOCTYPE_ISTOUT

    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        If eSave = SaveTypeIfNew Then
            'Get Next Purchase Order Number
            Set oSysNum = m_oSession.Database.CreateBusinessObject(CLASSID_SYSTEMNO)
            mDocNo = oSysNum.GetNewReceiptNumber
            Set oSysNum = Nothing
            If LenB(mNarrative) <> 0 Then 'if narrative captured then call sub BO to save
                Set oNarr = m_oSession.Database.CreateBusinessObject(CLASSID_DELNOTENARRATIVE)
                oNarr.DocumentNumber = mDocNo
                If InStr(mNarrative, vbCrLf) > 0 Then
                    If InStr(mNarrative, vbCrLf) > 1 Then
                        oNarr.NarrativeText1 = Left$(mNarrative, InStr(mNarrative, vbCrLf) - 1)
                    End If
                    oNarr.NarrativeText2 = mID$(mNarrative, InStr(mNarrative, vbCrLf) + 2)
                Else
                    oNarr.NarrativeText1 = mNarrative
                    oNarr.NarrativeText2 = vbNullString
                End If
                oNarr.StoreNumber = m_oSession.CurrentEnterprise.IEnterprise_StoreNumber
                oNarr.PartCode = "000000"
                oNarr.SequenceNumber = "0000"
                Call oNarr.IBo_SaveIfNew
                Set oNarr = Nothing
            End If
        End If
           
        Save = m_oSession.Database.SaveBo(eSave, Me)
        
        'Save each line for the IST Note
        For lLineNo = 1 To mcolLines.Count Step 1
            Set oDelLine = mcolLines(CLng(lLineNo))
            oDelLine.DRLKey = Me.DocNo
            Call oDelLine.IBo_SaveIfNew
            'After saving line - Update Purchase Order Line
            'Call oPOrder.RecordItemDelivered(oDelLine.POLineNumber, oDelLine.ReceivedQty, mDocNo, oDelLine.LineNo)
        Next lLineNo
    End If

End Function

Public Function Cancel() As Boolean

Dim lngLineNo As Long
        
    Cancel = False
    'mark IST Out as cancel and save
    mValue = 0
    If m_oSession.Database.SaveBo(SaveTypeIfExists, Me) = False Then Exit Function
    
    'ensure lines are loaded
    If mcolLines Is Nothing Then Call RetrieveLines
    'Save each line for the IST Note
    For lngLineNo = 1 To mcolLines.Count Step 1
        If mcolLines(CLng(lngLineNo)).Cancel = False Then Exit Function
    Next lngLineNo
    Cancel = True

End Function
Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_ISTOUTHEADER * &H10000) + 1 To FID_ISTOUTHEADER_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter



'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As cISTOutHeader

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set Interface = oBO
        Case Else:
            Set Interface = Me
    End Select

End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_ISTOUTHEADER_END_OF_STATIC

End Function

Public Function UpdateCommNumber(strISTOutKey As String, strCommNumber As String) As String

Dim oRow        As IRow
Dim oGField     As CFieldGroup

    If strISTOutKey <> mDocNo Then
        Call IBo_ClearLoadFilter
        Call IBo_AddLoadFilter(CMP_EQUAL, FID_ISTOUTHEADER_DocNo, strISTOutKey)
        Call IBo_AddLoadField(FID_ISTOUTHEADER_DocNo)
        Call IBo_AddLoadField(FID_ISTOUTHEADER_Comm)
        Call IBo_LoadMatches
    End If
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_ISTOUTHEADER_DocNo))
    
    Set oGField = GetField(FID_ISTOUTHEADER_Comm)
    ' and add to the row
    Call oRow.Add(oGField)
    
    mComm = strCommNumber
    
    Call m_oSession.Database.SavePartialBo(Me, oRow)

End Function

Public Function CreateFile()

Const PROCEDURE_NAME = MODULE_NAME & ".CreateFile"

Const EDI_NA As String = vbNullString

Dim oRowSel     As IRowSelector
Dim oField      As IField
Dim strHeader   As String
Dim strTrailer  As String
Dim lngLineNo   As Long
Dim strLine     As String
Dim tsOrder     As TextStream
Dim strFilename As String
Dim dteNA       As Date
Dim oFileNo     As cEDIFileControl
Dim oLine       As cISTLine
Dim strSeqNo    As String
Dim lngLineCnt  As Long

On Error GoTo Bad_CreateFile

    'Create field for storing Key into
    Set oField = GetField(FID_ISTOUTHEADER_DocNo)
    If oField Is Nothing Then Exit Function
    
    'Add field to Row selector
    Set oRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call oRowSel.AddSelection(CMP_EQUAL, oField)
    
    If LenB(mstrDateFmt) = 0 Then mstrDateFmt = UCase$(m_oSession.GetParameter(PRM_EDI_DATE_FMT))
    
    strFilename = m_oSession.GetParameter(PRM_TXOUT_FOLDER)
    If (Right$(strFilename, 1) <> "\") Then strFilename = strFilename & "\"
       
    Set oFileNo = m_oSession.Database.CreateBusinessObject(CLASSID_EDIFILECONTROL)
    
    'Pre-count any narrative lines to be added to the output file
    If LenB(mNarrative) <> 0 Then lngLineCnt = lngLineCnt + 1
    
    'Pre-count any text for the line items to be added to the output file
    For lngLineNo = 1 To Lines.Count Step 1
        Set oLine = mcolLines(CLng(lngLineNo))
        If LenB(oLine.Narrative) <> 0 Then lngLineCnt = lngLineCnt + 1
    Next lngLineNo
    
'GetEDITextStream(ByRef blnStoreToHeadOffice As Boolean, ByRef strShortCode As String, ByRef strFilePath As String, ByVal strSequenceNo As String) As TextStream
    Set tsOrder = oFileNo.GetEDITextStream(True, ISTOUT_NOTE_FILECODE, strFilename, strSeqNo)
    Call DebugMsg(MODULE_NAME, "CreateFile", endlDebug, "DRL SUM OUT File=" & strSeqNo)
    
    'Output file header record
    strHeader = "HR" & Format$(Now(), "DD/MM/YY HH:NN:SS") & " " & strSeqNo & " " & Format$(Lines.Count + 1 + lngLineCnt, "000000") & " " & m_oSession.CurrentEnterprise.IEnterprise_StoreNumber
    Call tsOrder.WriteLine(strHeader)

'    Call tsOrder.WriteLine("DS," & m_oSession.Database.GetLiteralView(oRowSel, 2, vbCrLf, ",", vbNullString))
    tsOrder.Write "DS," & mDocNo & "," & mDocType & "," & mValue & "," & mCost & "," & FormatEDIDate(mDelNoteDate) & ","
'   ds:numb,ds:type,ds:valu,ds:cost,ds:date
    tsOrder.Write mOrigDocKey & "," & mEnteredBy & "," & mComment & ",  000,"
'   ds:tkey,ds:init,ds:info,e ds:0sup wsup,wsup
    tsOrder.Write mConsignmentRef & ",000000," & FormatEDIDate(dteNA) & ",00,"
'   ds:0con,ds:0pon,ds:0dat,ds:0rel
    tsOrder.Write "000000,"
'   ds:0soq
    For lngLineNo = 1 To 9 Step 1
        tsOrder.Write ","
    Next lngLineNo
'   ds:0dl1,ds:0dl2,ds:0dl3,ds:0dl4,ds:0dl5,ds:0dl6,ds:0dl7,ds:0dl8,ds:0dl9
    tsOrder.Write mSource & "," & mStoreNumber & "," & mIBT & ","
'   ds:0src,ds:1str,ds:1ibt
    If mPrinted = True Then
        tsOrder.Write "Y"
    Else
        tsOrder.Write "N"
    End If
    tsOrder.Write "," & mConsignmentKey & ",  000," & FormatEDIDate(dteNA) & ",000000,"
'   ds:1prt,ds:1con,  e   ds:3sup   wsup,,wsup,ds:3dat,ds:3pon,ds:3ret
    tsOrder.Write "," & mInvCrnMatched & "," & FormatEDIDate(mInvCrnDate) & "," & mInvCrnNumber & "," & mVarianceValue & ","
'   ds:matf,ds:idat,ds:invn,rvrv
    tsOrder.Write mInvoiceValue & "," & mInvoiceVAT & "," & mMatchingComment & "," & FormatEDIDate(mMatchingDate) & "," & FormatEDIDate(mReceiptDate) & ","
'   ds:ival,ds:ivat,memo,ds:mdat,ds:tdat
    tsOrder.WriteLine mCarraigeValue & "," & mCommit
'   ds:carval,canc-ist?

    
    For lngLineNo = 1 To Lines.Count Step 1
        Set oLine = mcolLines(CLng(lngLineNo))
        
        tsOrder.Write "DD," & oLine.DRLKey & "," & oLine.LineNo & "," & oLine.PartCode & "," & oLine.PartCodeAlphaCode
'dd:numb,dd:seqn,dd:skun,dd:alph
        tsOrder.Write "," & oLine.OrderQty & "," & oLine.ReceivedQty & "," & oLine.OrderPrice & "," & oLine.BlanketDiscDesc
'dd:ordq'dd:recq'dd:ordp'dd:bdes
        tsOrder.Write "," & oLine.BlanketPartCode & "," & oLine.CurrentPrice & "," & oLine.CurrentCost & "," & oLine.ISTQuantity
'dd:bspc'dd:pric'dd:Cost'dd:ibtq
        tsOrder.Write "," & oLine.ReturnQty & "," & oLine.ToFollowQty & "," & oLine.ReturnReasonCode & "," & oLine.POLineNumber & ","
'dd:retq,'dd:folq'dd:reas'dd:poln
        If oLine.Checked = True Then
            tsOrder.Write "Y"
        Else
            tsOrder.Write "N"
        End If
'dd:ichk
        tsOrder.WriteLine "," & oLine.SinglesSellingUnit
'dd:qpsu

    Next lngLineNo
    
    If LenB(mNarrative) <> 0 Then
        tsOrder.Write "DN" & "," & m_oSession.CurrentEnterprise.IEnterprise_StoreNumber
        tsOrder.WriteLine "," & mDocNo & ",0000,000000," & Replace(mNarrative, vbCrLf, vbNullString)
    End If
    
    For lngLineNo = 1 To Lines.Count Step 1
        Set oLine = mcolLines(CLng(lngLineNo))
        If LenB(oLine.Narrative) <> 0 Then
            tsOrder.Write "DN" & "," & m_oSession.CurrentEnterprise.IEnterprise_StoreNumber & ","
            tsOrder.WriteLine oLine.DRLKey & "," & oLine.LineNo & "," & oLine.PartCode & "," & Replace(oLine.Narrative, vbCrLf, vbNullString)
        End If
    Next lngLineNo
    
    strTrailer = "TR (not included)"
    Call tsOrder.WriteLine(strTrailer)
    
    Call tsOrder.Close
        
    Call UpdateCommNumber(mDocNo, strSeqNo)
    
    Call oFileNo.MarkAsDone(True, "RC", strSeqNo, vbNullString, lngLineCnt)
    
    CreateFile = True
    
    Exit Function
    
Bad_CreateFile:

    CreateFile = False
    Call Err.Raise(Err.Number, Err.Source, Err.Description)
    
End Function




