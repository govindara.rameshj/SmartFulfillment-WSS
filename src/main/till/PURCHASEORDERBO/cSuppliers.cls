VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cSuppliers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D7313670186"
'<CAMH>****************************************************************************************
'* Module : cSuppliers
'* Date   : 15/11/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/PurchaseOrderBO/cSuppliers.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 24/05/04 12:04 $ $Revision: 9 $
'* Versions:
'* 15/11/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cSuppliers"

Const SUPP_NO      As Long = 1
Const SUPP_NAME    As Long = 2
Const SUPP_DELETED As Long = 3

Implements IBo
Implements ISysBo

'##ModelId=3D73137F0154
Private mSupplierNo As String

'##ModelId=3D73138501EA
Private mSupplierName As String

Private mDeleted As Boolean

'##ModelId=3D7313D50190
Private mStartOfList As Boolean

'##ModelId=3D7313D90352
Private mEndOfList As Boolean

'##ModelId=3D73179D038E
Private moView As IView

'##ModelId=3D731847012C
Private mCount As Long

'##ModelId=3D7318C703A2
Private m_oSession As Session

'##ModelId=3D733D7D0316
Private mCurrentPos As Long

Private EntryNo() As Long

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Public Property Get EntryString() As String

    EntryString = mSupplierNo & vbTab & mSupplierName
    
End Property

'##ModelId=3D74902403C0
Public Sub Locate(SupplierNo As String)

Dim lRowNo As Long
Dim oRow   As IRow

    For lRowNo = 1 To mCount Step 1
        If SupplierNo = moView.Row(CLng(lRowNo)).Field(SUPP_NO).ValueAsVariant Then
            mStartOfList = False
            mEndOfList = False
            mCurrentPos = lRowNo
            Set oRow = moView.Row(CLng(EntryNo(lRowNo)))
            mSupplierNo = oRow.Field(SUPP_NO).ValueAsVariant
            mSupplierName = oRow.Field(SUPP_NAME).ValueAsVariant
            mDeleted = oRow.Field(SUPP_DELETED).ValueAsVariant
            If mCurrentPos = 1 Then mStartOfList = True
            If mCurrentPos >= mCount Then mEndOfList = True
            Exit For
        End If
    Next lRowNo

End Sub

'##ModelId=3D733D6E0208
Public Sub MoveFirst()

Dim oRow   As IRow
    
    mStartOfList = True
    mCurrentPos = 1
    Set oRow = moView.Row(CLng(EntryNo(mCurrentPos)))
    mSupplierNo = oRow.Field(SUPP_NO).ValueAsVariant
    mSupplierName = oRow.Field(SUPP_NAME).ValueAsVariant
    mDeleted = oRow.Field(SUPP_DELETED).ValueAsVariant
    If mCurrentPos >= mCount Then mEndOfList = True

End Sub

'##ModelId=3D733D7400F0
Public Sub MoveLast()
    
Dim oRow   As IRow
    
    mEndOfList = True
    mCurrentPos = mCount
    Set oRow = moView.Row(CLng(EntryNo(mCurrentPos)))
    mSupplierNo = oRow.Field(SUPP_NO).ValueAsVariant
    mSupplierName = oRow.Field(SUPP_NAME).ValueAsVariant
    mDeleted = oRow.Field(SUPP_DELETED).ValueAsVariant
    If mCurrentPos = 1 Then mStartOfList = True

End Sub

'##ModelId=3D73186501C2
Public Property Get Count() As Long
   Let Count = mCount
End Property

'##ModelId=3D7313980294
Public Sub MoveNext()
    
Dim oRow   As IRow
    
    If mEndOfList = True Then
        Call Err.Raise(CLASSID_SUPPLIER, , "Unable to move past end of Supplier List")
        Exit Sub
    End If
    mEndOfList = False
    mCurrentPos = mCurrentPos + 1
    Set oRow = moView.Row(CLng(EntryNo(mCurrentPos)))
    mSupplierNo = oRow.Field(SUPP_NO).ValueAsVariant
    mSupplierName = oRow.Field(SUPP_NAME).ValueAsVariant
    mDeleted = oRow.Field(SUPP_DELETED).ValueAsVariant
    If mCurrentPos >= mCount Then mEndOfList = True

End Sub

'##ModelId=3D73139C037A
Public Sub MovePrev()
    
Dim oRow   As IRow
    
    If mStartOfList = True Then
        Call Err.Raise(CLASSID_SUPPLIER, , "Unable to move past start of Supplier List")
        Exit Sub
    End If
    mStartOfList = False
    mCurrentPos = mCurrentPos - 1
    Set oRow = moView.Row(CLng(EntryNo(mCurrentPos)))
    mSupplierNo = oRow.Field(SUPP_NO).ValueAsVariant
    mSupplierName = oRow.Field(SUPP_NAME).ValueAsVariant
    mDeleted = oRow.Field(SUPP_DELETED).ValueAsVariant
    If mCurrentPos = 1 Then mStartOfList = True

End Sub

'##ModelId=3D7313A10078
Public Sub Retrieve()
End Sub

'##ModelId=3D7313AF00B4
Public Function GetList() As Boolean

' Load up all properties from the database
Dim oRSelector  As IRowSelector
Dim nFid        As Long
Dim oView       As IView
Dim lRowNo      As Long
Dim oField      As IField
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
'    If lenb(lId) <> 0 Then mSupplierNo = sId
    
    Set oRSelector = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call oRSelector.AddSelectAll(FID_SUPPLIERS_SupplierNo)
    Call oRSelector.AddSelectAll(FID_SUPPLIERS_Name)
        
    'Add in default Document type filter for active suppliers only
    Set oField = GetField(FID_SUPPLIERS_Deleted)
    oField.ValueAsVariant = False
    Call oRSelector.AddSelection(CMP_EQUAL, oField)
    
    mStartOfList = True
    mEndOfList = True
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oRSelector)
    Set oRSelector = Nothing
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then
            mEndOfList = False
            Call LoadFromRow(oView.Row(1))
            mCurrentPos = 1
        End If
        Set moView = oView
        mCount = oView.Count
        Set oView = Nothing
    End If
    'set up array to hold pointers to valid entries in List - this will start as 1=1 etc
    ReDim EntryNo(mCount + 1)
    For nFid = 1 To mCount Step 1
        EntryNo(nFid) = nFid
    Next nFid
    Call MoveFirst

End Function

'##ModelId=3D7313BA006E
Public Sub OrderBy()
End Sub

'##ModelId=3D7313C600BE
Public Sub Filter()
End Sub

'##ModelId=3D7314B80320
Public Property Get EndOfList() As Boolean
   Let EndOfList = mEndOfList
End Property


'##ModelId=3D7314B80244
Public Property Get StartOfList() As Boolean
   Let StartOfList = mStartOfList
End Property

'##ModelId=3D7314B800FA
Public Property Get SupplierName() As String
   Let SupplierName = mSupplierName
End Property

'##ModelId=3D7314B80050
Public Property Let SupplierName(ByVal Value As String)
    Let mSupplierName = Value
End Property

'##ModelId=3D7314B8001E
Public Property Get SupplierNo() As String
   Let SupplierNo = mSupplierNo
End Property


'##ModelId=3D7314B7035C
Public Property Let SupplierNo(ByVal Value As String)
    Let mSupplierNo = Value
End Property

Public Property Get Deleted() As Boolean
   Let Deleted = mDeleted
End Property

Public Property Let Deleted(ByVal Value As Boolean)
    Let mDeleted = Value
End Property

Private Sub Class_Initialize()

    Call DebugMsg(MODULE_NAME, "Initialise", endlTraceIn)

End Sub

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 15/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_SUPPLIERS_SupplierNo):   mSupplierNo = oField.ValueAsVariant
            Case (FID_SUPPLIERS_Name):         mSupplierName = oField.ValueAsVariant
            Case (FID_SUPPLIERS_Deleted):          mDeleted = oField.ValueAsVariant
           Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Sub Class_Terminate()
    
    Call DebugMsg(MODULE_NAME, "Class_Terminate", endlTraceIn)
    
    Set moView = Nothing
    Set moRowSel = Nothing
    Set moLoadRow = Nothing
    Set m_oSession = Nothing
    
    Call DebugMsg(MODULE_NAME, "Class_Terminate", endlTraceOut)

End Sub

Private Function IBo_CreateNew() As IBo

    Set IBo_CreateNew = New cSuppliers

End Function

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = vbNullString

End Property

Private Function IBo_GetField(nFid As Long) As IField

    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function
Private Function Initialise(oSession As ISession) As cSuppliers
    Set m_oSession = oSession
    Set Initialise = Me
End Function
Private Function IBo_Initialise(oSession As ISession) As IBo
    Set IBo_Initialise = Initialise(oSession)
End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_SUPPLIERS

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Public Function IBo_Load() As Boolean

    IBo_Load = Load()

End Function
Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oRSelector  As IRowSelector
Dim nFid        As Long
Dim oView       As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
'    If lenb(lId) <> 0 Then mSupplierNo = sId
    
    Set oRSelector = m_oSession.Root.CreateUtilityObject("CRowSelector")
    oRSelector.AddSelection CMP_EQUAL, GetField(FID_SUPPLIERS_SupplierNo)
    oRSelector.Merge GetSelectAllRow
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oRSelector)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
'            Case (FID_SUPPLIER_ID):Set GetField = New CField")   Mvarid = getfield.value
        Case (FID_SUPPLIERS_SupplierNo):
                    Set GetField = New CFieldString
                    GetField.ValueAsVariant = mSupplierNo
        Case (FID_SUPPLIERS_Name):
                    Set GetField = New CFieldString
                    GetField.ValueAsVariant = mSupplierName
        Case (FID_SUPPLIERS_Deleted):
                    Set GetField = New CFieldBool
        Case (FID_SUPPLIERS_Deleted):
                    Set GetField = New CFieldBool
                    GetField.ValueAsVariant = mDeleted
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox("Damn")
    Resume Next

End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_SUPPLIERS, FID_SUPPLIERS_END_OF_STATIC, m_oSession)

End Function



Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_SUPPLIERS * &H10000) + 1 To FID_SUPPLIERS_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As cSuppliers

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set Interface = oBO
        Case Else:
            Set Interface = Me
    End Select

End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_SUPPLIERS_END_OF_STATIC

End Function

