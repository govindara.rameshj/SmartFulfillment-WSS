VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cISTLine"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D5CB59D01B8"
'<CAMH>****************************************************************************************
'* Module: cISTLine
'* Date  : 15/08/02
'* Author: mauricem
'*$Archive: /Projects/OasysV2/VB/PurchaseOrderBO/IBTLine.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of a single IST Line
'**********************************************************************************************
'* $Author: Damians $
'* $Date: 7/05/04 16:16 $
'* $Revision: 12 $
'* Versions:
'* 15/08/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cISTLine"

Implements IBo
Implements ISysBo

Private m_oSession As Session

Private mDRLKey As String

Private mLineNo As String

Private mPartCode As String

Private mPartCodeAlphaCode As String

Private mOrderQty As Long

Private mReceivedQty As Long

Private mOrderPrice As Currency

Private mBlanketDiscDesc As String

Private mBlanketPartCode As String

Private mCurrentPrice As Currency

Private mCurrentCost As Double

Private mISTQuantity As Long

Private mReturnQty As Long

Private mToFollowQty As Long

Private mReturnReasonCode As String

Private mPOLineNumber As Long

Private mChecked As Boolean

Private mSinglesSellingUnit As Currency

Private mNarrative As String

Private moRowSel As IRowSelector

Public ISTOutNote As cISTOutHeader

Public ISTInNote As cISTInHeader

Private moLoadRow As IRowSelector


Public Sub Create()
End Sub

Public Sub ReceiveQuantity()
End Sub

Public Sub ReturnQuantity()
End Sub

Public Sub MarkAsVerified()
End Sub

Public Property Get Narrative() As String
   Let Narrative = mNarrative
End Property

Public Property Let Narrative(ByVal Value As String)
    Let mNarrative = Value
End Property

Public Property Get SinglesSellingUnit() As Currency
   Let SinglesSellingUnit = mSinglesSellingUnit
End Property

Public Property Let SinglesSellingUnit(ByVal Value As Currency)
    Let mSinglesSellingUnit = Value
End Property

Public Property Get Checked() As Boolean
   Let Checked = mChecked
End Property

Public Property Let Checked(ByVal Value As Boolean)
    Let mChecked = Value
End Property

Public Property Get POLineNumber() As Long
   Let POLineNumber = mPOLineNumber
End Property

Public Property Let POLineNumber(ByVal Value As Long)
    Let mPOLineNumber = Value
End Property

Public Property Get ReturnReasonCode() As String
   Let ReturnReasonCode = mReturnReasonCode
End Property

Public Property Let ReturnReasonCode(ByVal Value As String)
    Let mReturnReasonCode = Value
End Property

Public Property Get ToFollowQty() As Long
   Let ToFollowQty = mToFollowQty
End Property

Public Property Let ToFollowQty(ByVal Value As Long)
    Let mToFollowQty = Value
End Property

Public Property Get ReturnQty() As Long
   Let ReturnQty = mReturnQty
End Property

Public Property Let ReturnQty(ByVal Value As Long)
    Let mReturnQty = Value
End Property

Public Property Get ISTQuantity() As Long
   Let ISTQuantity = mISTQuantity
End Property

Public Property Let ISTQuantity(ByVal Value As Long)
    Let mISTQuantity = Value
End Property

Public Property Get CurrentCost() As Double
   Let CurrentCost = mCurrentCost
End Property

Public Property Let CurrentCost(ByVal Value As Double)
    Let mCurrentCost = Value
End Property

Public Property Get CurrentPrice() As Currency
   Let CurrentPrice = mCurrentPrice
End Property

Public Property Let CurrentPrice(ByVal Value As Currency)
    Let mCurrentPrice = Value
End Property

Public Property Get BlanketPartCode() As String
   Let BlanketPartCode = mBlanketPartCode
End Property

Public Property Let BlanketPartCode(ByVal Value As String)
    Let mBlanketPartCode = Value
End Property

Public Property Get BlanketDiscDesc() As String
   Let BlanketDiscDesc = mBlanketDiscDesc
End Property

Public Property Let BlanketDiscDesc(ByVal Value As String)
    Let mBlanketDiscDesc = Value
End Property

Public Property Get OrderPrice() As Currency
   Let OrderPrice = mOrderPrice
End Property

Public Property Let OrderPrice(ByVal Value As Currency)
    Let mOrderPrice = Value
End Property

Public Property Get ReceivedQty() As Long
   Let ReceivedQty = mReceivedQty
End Property

Public Property Let ReceivedQty(ByVal Value As Long)
    Let mReceivedQty = Value
End Property

Public Property Get OrderQty() As Long
   Let OrderQty = mOrderQty
End Property

Public Property Let OrderQty(ByVal Value As Long)
    Let mOrderQty = Value
End Property

Public Property Get PartCodeAlphaCode() As String
   Let PartCodeAlphaCode = mPartCodeAlphaCode
End Property

Public Property Let PartCodeAlphaCode(ByVal Value As String)
    Let mPartCodeAlphaCode = Value
End Property

Public Property Get PartCode() As String
   Let PartCode = mPartCode
End Property

Public Property Let PartCode(ByVal Value As String)
    Let mPartCode = Value
End Property

Public Property Get LineNo() As String
   Let LineNo = mLineNo
End Property

Public Property Let LineNo(ByVal Value As String)
    Let mLineNo = Format$(Val(Value), "0000")
End Property

Public Property Get DRLKey() As String
   Let DRLKey = mDRLKey
End Property


Public Property Let DRLKey(ByVal Value As String)
    Let mDRLKey = Value
End Property

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 15/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_ISTLINE_DRLKey): mDRLKey = oField.ValueAsVariant
            Case (FID_ISTLINE_LineNo): mLineNo = oField.ValueAsVariant
            Case (FID_ISTLINE_PartCode): mPartCode = oField.ValueAsVariant
            Case (FID_ISTLINE_PartCodeAlphaCode): mPartCodeAlphaCode = oField.ValueAsVariant
            Case (FID_ISTLINE_OrderQty): mOrderQty = oField.ValueAsVariant
            Case (FID_ISTLINE_ReceivedQty): mReceivedQty = oField.ValueAsVariant
            Case (FID_ISTLINE_OrderPrice): mOrderPrice = oField.ValueAsVariant
            Case (FID_ISTLINE_BlanketDiscDesc): mBlanketDiscDesc = oField.ValueAsVariant
            Case (FID_ISTLINE_BlanketPartCode): mBlanketPartCode = oField.ValueAsVariant
            Case (FID_ISTLINE_CurrentPrice): mCurrentPrice = oField.ValueAsVariant
            Case (FID_ISTLINE_CurrentCost): mCurrentCost = oField.ValueAsVariant
            Case (FID_ISTLINE_ISTQuantity): mISTQuantity = oField.ValueAsVariant
            Case (FID_ISTLINE_ReturnQty): mReturnQty = oField.ValueAsVariant
            Case (FID_ISTLINE_ToFollowQty): mToFollowQty = oField.ValueAsVariant
            Case (FID_ISTLINE_ReturnReasonCode): mReturnReasonCode = oField.ValueAsVariant
            Case (FID_ISTLINE_POLineNumber): mPOLineNumber = oField.ValueAsVariant
            Case (FID_ISTLINE_Checked): mChecked = oField.ValueAsVariant
            Case (FID_ISTLINE_SinglesSellingUnit): mSinglesSellingUnit = oField.ValueAsVariant
            Case (FID_ISTLINE_Narrative): mNarrative = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow



Private Sub Class_Initialize()
    
    Call DebugMsg(MODULE_NAME, "Initialise", endlTraceIn)

End Sub

Private Function IBo_CreateNew() As IBo

    Set IBo_CreateNew = New cISTLine

End Function

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "IST Line " & mDRLKey & " Line " & mLineNo

End Property

Private Function IBo_GetField(nFid As Long) As IField

    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Private Function Initialise(oSession As ISession) As cISTLine
    Set m_oSession = oSession
    Set Initialise = Me
End Function
Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_ISTLINE

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Public Function IBo_Load() As Boolean
    
    IBo_Load = Load

End Function
Private Function Load() As Boolean
    
Dim oView       As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
        
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count >= 1 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    Select Case (lFieldID)
        Case (FID_ISTLINE_DRLKey):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDRLKey
        Case (FID_ISTLINE_LineNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLineNo
        Case (FID_ISTLINE_PartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartCode
        Case (FID_ISTLINE_PartCodeAlphaCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartCodeAlphaCode
        Case (FID_ISTLINE_OrderQty):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mOrderQty
        Case (FID_ISTLINE_ReceivedQty):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mReceivedQty
        Case (FID_ISTLINE_OrderPrice):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mOrderPrice
        Case (FID_ISTLINE_BlanketDiscDesc):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mBlanketDiscDesc
        Case (FID_ISTLINE_BlanketPartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mBlanketPartCode
        Case (FID_ISTLINE_CurrentPrice):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mCurrentPrice
        Case (FID_ISTLINE_CurrentCost):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mCurrentCost
        Case (FID_ISTLINE_ISTQuantity):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mISTQuantity
        Case (FID_ISTLINE_ReturnQty):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mReturnQty
        Case (FID_ISTLINE_ToFollowQty):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mToFollowQty
        Case (FID_ISTLINE_ReturnReasonCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mReturnReasonCode
        Case (FID_ISTLINE_POLineNumber):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mPOLineNumber
        Case (FID_ISTLINE_Checked):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mChecked
        Case (FID_ISTLINE_SinglesSellingUnit):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mSinglesSellingUnit
        Case (FID_ISTLINE_Narrative):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mNarrative
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID

End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_ISTLINE, FID_ISTLINE_END_OF_STATIC, m_oSession)

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
Dim oStockLog  As cStockLog
Dim oStockMove As cStockMovement
Dim oNarrLine  As cDelNoteNarrative
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
        If eSave = SaveTypeIfNew Then
            MsgBox "Stock Log code removed"
'            Set oStockLog = m_oSession.Database.CreateBusinessObject(CLASSID_STOCKLOG)
'            oStockLog.LogTime = Format$(Now, "HHMMSS")
'            oStockLog.PartCode = mPartCode
'            oStockLog.QuantityAdjusted = mISTQuantity
'            If ((ISTOutNote Is Nothing) = False) And (mISTQuantity > 0) Then oStockLog.QuantityAdjusted = mISTQuantity * -1
'            oStockLog.ExtCost = (oStockLog.QuantityAdjusted / mSinglesSellingUnit) * mCurrentCost
'            oStockLog.ExtRetailValue = (oStockLog.QuantityAdjusted / mSinglesSellingUnit) * mCurrentPrice
'            oStockLog.QuantityPerSellUnit = mSinglesSellingUnit
'            If (ISTOutNote Is Nothing) = False Then
'                oStockLog.LogDate = ISTOutNote.DelNoteDate
'                oStockLog.TransactionKey = DRLKey & mLineNo & ISTOutNote.ConsignmentKey & ISTOutNote.StoreNumber
'                oStockLog.TransactionNo = ISTOutNote.DocNo
'                Call oStockLog.SaveISTOut
'                'Update Stock Movement History
'                Set oStockMove = m_oSession.Database.CreateBusinessObject(CLASSID_STOCKMOVE)
'                Call oStockMove.SaveISTOutIssued(mPartCode, mISTQuantity, (mISTQuantity / mSinglesSellingUnit) * mOrderPrice)
'                Set oStockMove = Nothing
'            End If
'            If (ISTInNote Is Nothing) = False Then
'                oStockLog.LogDate = ISTInNote.DelNoteDate
'                oStockLog.TransactionKey = DRLKey & mLineNo & ISTInNote.ConsignmentKey & ISTInNote.StoreNumber
'                oStockLog.TransactionNo = ISTInNote.DocNo
'                Call oStockLog.SaveISTIn
'                'Update Stock Movement History
'                Set oStockMove = m_oSession.Database.CreateBusinessObject(CLASSID_STOCKMOVE)
'                Call oStockMove.SaveISTInReceived(mPartCode, mISTQuantity, (mISTQuantity / mSinglesSellingUnit) * mOrderPrice)
'                Set oStockMove = Nothing
'            End If
            If LenB(mNarrative) <> 0 Then 'if narrative captured then call sub BO to save
                Set oNarrLine = m_oSession.Database.CreateBusinessObject(CLASSID_DELNOTENARRATIVE)
                oNarrLine.DocumentNumber = mDRLKey
                oNarrLine.NarrativeText1 = mNarrative
                oNarrLine.NarrativeText2 = vbNullString
                oNarrLine.StoreNumber = m_oSession.CurrentEnterprise.IEnterprise_StoreNumber
                oNarrLine.PartCode = mPartCode
                oNarrLine.SequenceNumber = mLineNo
                Call oNarrLine.IBo_SaveIfNew
                Set oNarrLine = Nothing
            End If
        End If
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function
Public Function Cancel() As Boolean

'Reverse Stock Movement and Stock Log entries
Dim oStockLog  As cStockLog
Dim oStockMove As cStockMovement
    
    Cancel = False
    
    If IsValid() Then
            MsgBox "Stock Log code removed"
'            Set oStockLog = m_oSession.Database.CreateBusinessObject(CLASSID_STOCKLOG)
'            oStockLog.LogTime = Format$(Now, "HHMMSS")
'            oStockLog.LogDate = Now()
'            oStockLog.PartCode = mPartCode
'            oStockLog.QuantityPerSellUnit = mSinglesSellingUnit
'            If (ISTOutNote Is Nothing) = False Then
'                oStockLog.QuantityAdjusted = mISTQuantity
'                oStockLog.ExtCost = (oStockLog.QuantityAdjusted / mSinglesSellingUnit) * mCurrentCost
'                oStockLog.ExtRetailValue = (oStockLog.QuantityAdjusted / mSinglesSellingUnit) * mCurrentPrice
'                oStockLog.TransactionKey = DRLKey
'                oStockLog.TransactionNo = ISTOutNote.DocNo
'                Call oStockLog.SaveISTOut
'                'Update Stock Movement History
'                Set oStockMove = m_oSession.Database.CreateBusinessObject(CLASSID_STOCKMOVE)
'                Call oStockMove.SaveISTOutIssued(mPartCode, mISTQuantity * -1, (mISTQuantity / mSinglesSellingUnit) * mOrderPrice * -1)
'                Set oStockMove = Nothing
'            End If
'            If (ISTInNote Is Nothing) = False Then
'                oStockLog.QuantityAdjusted = mISTQuantity * -1 'reverse out stock levels
'                oStockLog.ExtCost = (oStockLog.QuantityAdjusted / mSinglesSellingUnit) * mCurrentCost
'                oStockLog.ExtRetailValue = (oStockLog.QuantityAdjusted / mSinglesSellingUnit) * mCurrentPrice
'                oStockLog.TransactionKey = DRLKey
'                oStockLog.TransactionNo = ISTInNote.DocNo
'                Call oStockLog.SaveISTIn
'                'Update Stock Movement History
'                Set oStockMove = m_oSession.Database.CreateBusinessObject(CLASSID_STOCKMOVE)
'                Call oStockMove.SaveISTInReceived(mPartCode, mISTQuantity * -1, (mISTQuantity / mSinglesSellingUnit) * mOrderPrice * -1)
'                Set oStockMove = Nothing
'            End If
    End If
    Cancel = True

End Function
Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_ISTLINE * &H10000) + 1 To FID_ISTLINE_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter


'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As cISTLine

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set Interface = oBO
        Case Else:
            Set Interface = Me
    End Select

End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_ISTLINE_END_OF_STATIC

End Function






