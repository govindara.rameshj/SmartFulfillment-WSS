VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cDeliveryNoteLine"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D400AC90302"
'<CAMH>****************************************************************************************
'* Module: DeliveryNoteLine
'* Date  : 15/08/02
'* Author: mauricem
'*$Archive: /Projects/OasysV2/VB/PurchaseOrderBO/DeliveryNoteLine.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of a single Delivery Note Line
'**********************************************************************************************
'* $Author: Damians $
'* $Date: 24/05/04 20:22 $
'* $Revision: 13 $
'* Versions:
'* 15/08/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "DeliveryNoteLine"

Implements IBo
Implements ISysBo

Private m_oSession As Session
'##ModelId=3D400AE0026C
Private mDRLKey As String

'##ModelId=3D400B280014
Private mLineNo As String

'##ModelId=3D400B2D0316
Private mPartCode As String

'##ModelId=3D400B35035C
Private mPartCodeAlphaCode As String

'##ModelId=3D400B3E021C
Private mOrderQty As Long

'##ModelId=3D400B4300D2
Private mReceivedQty As Long

'##ModelId=3D400B49001E
Private mOrderPrice As Currency

'##ModelId=3D400B4C017C
Private mBlanketDiscDesc As String

'##ModelId=3D400B5500AA
Private mBlanketPartCode As String

'##ModelId=3D400B5F01B8
Private mCurrentPrice As Currency

'##ModelId=3D400B660136
Private mCurrentCost As Double

'##ModelId=3D400B6A01AE
Private mShortageQty As Long

'##ModelId=3D400B710190
Private mReturnQty As Long

'##ModelId=3D400B79005A
Private mToFollowQty As Long

'##ModelId=3D400B7E01E0
Private mReturnReasonCode As String

'##ModelId=3D400B8302B2
Private mPOLineNumber As Long

'##ModelId=3D400B8C01E0
Private mChecked As Boolean

'##ModelId=3D400B9B035C
Private mSinglesSellingUnit As Currency

'##ModelId=3D400DC30028
Private mNarrative As String

Private mOverQty As Currency

Private moRowSel As IRowSelector

'##ModelId=3D400D2F026D
Public DeliveryNote As cDeliveryNote

Private moLoadRow As IRowSelector

Private mintNarrStatus As Integer 'flag if narrative edited - -1 (Not Loaded), 0=Okay and 1 = Edited

Private mPreviousQty As Double 'used to hold del qty if updating entry

Private mPreviousShortage As Double 'used to hold shortage if updating

Private mPreviousReturn As Double 'used to hold return qty if updating

Const NOT_LOADED As Long = -1
Const LOADED As Long = 0
Const EDITED As Long = 1

'##ModelId=3D400C830096
Public Sub Create()
End Sub

'##ModelId=3D400CB700DC
Public Sub ReceiveQuantity()
End Sub

'##ModelId=3D400CBF0082
Public Sub ReturnQuantity()
End Sub

'##ModelId=3D400CD000A0
Public Sub MarkAsVerified()
End Sub

Public Property Get OverQty() As Currency
   Let OverQty = mOverQty
End Property

Public Property Let OverQty(ByVal Value As Currency)
    Let mOverQty = Value
End Property

'##ModelId=3D5B74660230
Public Property Get Narrative() As String

Dim oDRLNarr As cDelNoteNarrative

    If mintNarrStatus = NOT_LOADED Then
        Set oDRLNarr = m_oSession.Database.CreateBusinessObject(CLASSID_DELNOTENARRATIVE)
        Call oDRLNarr.IBo_Load
        Set oDRLNarr = Nothing
        mintNarrStatus = LOADED
    End If
    Narrative = mNarrative

End Property

'##ModelId=3D5B746600B4
Public Property Let Narrative(ByVal Value As String)
    
    If mNarrative <> Value Then
        mNarrative = Value
        mintNarrStatus = EDITED
    End If

End Property

'##ModelId=3D5B74650384
Public Property Get SinglesSellingUnit() As Currency
   Let SinglesSellingUnit = mSinglesSellingUnit
End Property

'##ModelId=3D5B74650208
Public Property Let SinglesSellingUnit(ByVal Value As Currency)
    Let mSinglesSellingUnit = Value
End Property

'##ModelId=3D5B7465012C
Public Property Get Checked() As Boolean
   Let Checked = mChecked
End Property

'##ModelId=3D5B7464038E
Public Property Let Checked(ByVal Value As Boolean)
    Let mChecked = Value
End Property

'##ModelId=3D5B746402B2
Public Property Get POLineNumber() As Long
   Let POLineNumber = mPOLineNumber
End Property

'##ModelId=3D5B74640168
Public Property Let POLineNumber(ByVal Value As Long)
    Let mPOLineNumber = Value
End Property

'##ModelId=3D5B7464008C
Public Property Get ReturnReasonCode() As String
   Let ReturnReasonCode = mReturnReasonCode
End Property

'##ModelId=3D5B7463032A
Public Property Let ReturnReasonCode(ByVal Value As String)
    Let mReturnReasonCode = Value
End Property

'##ModelId=3D5B74630258
Public Property Get ToFollowQty() As Long
   Let ToFollowQty = mToFollowQty
End Property

'##ModelId=3D5B7463010E
Public Property Let ToFollowQty(ByVal Value As Long)
    Let mToFollowQty = Value
End Property

'##ModelId=3D5B74630032
Public Property Get ReturnQty() As Long
   Let ReturnQty = mReturnQty
End Property

'##ModelId=3D5B74620302
Public Property Let ReturnQty(ByVal Value As Long)
    Let mReturnQty = Value
End Property

'##ModelId=3D5B74620226
Public Property Get ShortageQty() As Long
   Let ShortageQty = mShortageQty
End Property

'##ModelId=3D5B746200DC
Public Function RecordShortage(strPartCode As String, lngQuantity As Long, lngLineNo As Long) As Boolean

Dim oRow As IRow
    
    If (mPartCode <> strPartCode) Or (mLineNo <> lngLineNo) Then
        Exit Function
    End If
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    Call oRow.Add(GetField(FID_DELNOTELINE_DRLKey))
    Call oRow.Add(GetField(FID_DELNOTELINE_LineNo))
    mShortageQty = lngQuantity
    Call oRow.Add(GetField(FID_DELNOTELINE_ShortageQty))
    
    Call m_oSession.Database.SavePartialBo(Me, oRow)
    
    RecordShortage = True
    
End Function

Public Function RecordReturn(strPartCode As String, lngQuantity As Long, lngLineNo As Long) As Boolean

Dim oRow As IRow
    
    If (mPartCode <> strPartCode) Or (mLineNo <> lngLineNo) Then
        Exit Function
    End If
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    Call oRow.Add(GetField(FID_DELNOTELINE_DRLKey))
    Call oRow.Add(GetField(FID_DELNOTELINE_LineNo))
    mReturnQty = lngQuantity
    Call oRow.Add(GetField(FID_DELNOTELINE_ReturnQty))
    
    Call m_oSession.Database.SavePartialBo(Me, oRow)
    
    RecordReturn = True
    
End Function

'##ModelId=3D5B7462003C
Public Property Get CurrentCost() As Double
   Let CurrentCost = mCurrentCost
End Property

'##ModelId=3D5B7461030C
Public Property Let CurrentCost(ByVal Value As Double)
    Let mCurrentCost = Value
End Property

'##ModelId=3D5B74610230
Public Property Get CurrentPrice() As Currency
   Let CurrentPrice = mCurrentPrice
End Property

'##ModelId=3D5B74610122
Public Property Let CurrentPrice(ByVal Value As Currency)
    Let mCurrentPrice = Value
End Property

'##ModelId=3D5B74610078
Public Property Get BlanketPartCode() As String
   Let BlanketPartCode = mBlanketPartCode
End Property

'##ModelId=3D5B74600352
Public Property Let BlanketPartCode(ByVal Value As String)
    Let mBlanketPartCode = Value
End Property

'##ModelId=3D5B74600276
Public Property Get BlanketDiscDesc() As String
   Let BlanketDiscDesc = mBlanketDiscDesc
End Property

'##ModelId=3D5B7460019A
Public Property Let BlanketDiscDesc(ByVal Value As String)
    Let mBlanketDiscDesc = Value
End Property

'##ModelId=3D5B746000F0
Public Property Get OrderPrice() As Currency
   Let OrderPrice = mOrderPrice
End Property

'##ModelId=3D5B745F03CA
Public Property Let OrderPrice(ByVal Value As Currency)
    Let mOrderPrice = Value
End Property

'##ModelId=3D5B745F032A
Public Property Get ReceivedQty() As Long
   Let ReceivedQty = mReceivedQty
End Property

'##ModelId=3D5B745F024E
Public Property Let ReceivedQty(ByVal Value As Long)
    Let mReceivedQty = Value
End Property

Public Sub UpdateQuantity(ByVal dblNewQuantity As Double)

    mPreviousQty = mReceivedQty
    Let mReceivedQty = dblNewQuantity

End Sub

Friend Property Get AdjustedQty() As Double
   Let AdjustedQty = mReceivedQty - mPreviousQty
End Property


'##ModelId=3D5B745F01A4
Public Property Get OrderQty() As Long
   Let OrderQty = mOrderQty
End Property

'##ModelId=3D5B745F00C8
Public Property Let OrderQty(ByVal Value As Long)
    Let mOrderQty = Value
End Property

'##ModelId=3D5B745F0028
Public Property Get PartCodeAlphaCode() As String
   Let PartCodeAlphaCode = mPartCodeAlphaCode
End Property

'##ModelId=3D5B745E0334
Public Property Let PartCodeAlphaCode(ByVal Value As String)
    Let mPartCodeAlphaCode = Value
End Property

'##ModelId=3D5B745E02C6
Public Property Get PartCode() As String
   Let PartCode = mPartCode
End Property

'##ModelId=3D5B745E01AE
Public Property Let PartCode(ByVal Value As String)
    Let mPartCode = Value
End Property

'##ModelId=3D5B745E0140
Public Property Get LineNo() As String
   Let LineNo = mLineNo
End Property

'##ModelId=3D5B745E0064
Public Property Let LineNo(ByVal Value As String)
    Let mLineNo = Format$(Val(Value), "0000")
End Property

'##ModelId=3D5B745D03DE
Public Property Get DRLKey() As String
   Let DRLKey = mDRLKey
End Property


'##ModelId=3D5B745D033E
Public Property Let DRLKey(ByVal Value As String)
    Let mDRLKey = Value
End Property

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 15/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_DELNOTELINE_DRLKey):          mDRLKey = oField.ValueAsVariant
            Case (FID_DELNOTELINE_LineNo):          mLineNo = oField.ValueAsVariant
            Case (FID_DELNOTELINE_PartCode):        mPartCode = oField.ValueAsVariant
            Case (FID_DELNOTELINE_PartCodeAlphaCode): mPartCodeAlphaCode = oField.ValueAsVariant
            Case (FID_DELNOTELINE_OrderQty):        mOrderQty = oField.ValueAsVariant
            Case (FID_DELNOTELINE_ReceivedQty):     mReceivedQty = oField.ValueAsVariant
                                                    mPreviousQty = mReceivedQty
            Case (FID_DELNOTELINE_OrderPrice):      mOrderPrice = oField.ValueAsVariant
            Case (FID_DELNOTELINE_BlanketDiscDesc): mBlanketDiscDesc = oField.ValueAsVariant
            Case (FID_DELNOTELINE_BlanketPartCode): mBlanketPartCode = oField.ValueAsVariant
            Case (FID_DELNOTELINE_CurrentPrice):    mCurrentPrice = oField.ValueAsVariant
            Case (FID_DELNOTELINE_CurrentCost):     mCurrentCost = oField.ValueAsVariant
            Case (FID_DELNOTELINE_ShortageQty):     mShortageQty = oField.ValueAsVariant
                                                    mPreviousShortage = mShortageQty
            Case (FID_DELNOTELINE_ReturnQty):       mReturnQty = oField.ValueAsVariant
                                                    mPreviousReturn = mReturnQty
            Case (FID_DELNOTELINE_ToFollowQty):     mToFollowQty = oField.ValueAsVariant
            Case (FID_DELNOTELINE_ReturnReasonCode): mReturnReasonCode = oField.ValueAsVariant
            Case (FID_DELNOTELINE_POLineNumber):    mPOLineNumber = oField.ValueAsVariant
            Case (FID_DELNOTELINE_Checked):         mChecked = oField.ValueAsVariant
            Case (FID_DELNOTELINE_SinglesSellingUnit): mSinglesSellingUnit = oField.ValueAsVariant
            Case (FID_DELNOTELINE_OverQty):         mOverQty = oField.ValueAsVariant
            Case (FID_DELNOTELINE_Narrative):       mNarrative = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow



Private Sub Class_Initialize()
    
    Call DebugMsg(MODULE_NAME, "Initialise", endlTraceIn)

End Sub

Private Function IBo_CreateNew() As IBo

    Set IBo_CreateNew = New cDeliveryNoteLine

End Function

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Delivery Note Line " & mDRLKey & " Line " & mLineNo

End Property

Private Function IBo_GetField(nFid As Long) As IField

    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Private Function Initialise(oSession As ISession) As cDeliveryNoteLine
    Set m_oSession = oSession
    Set Initialise = Me
End Function
Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_DELNOTELINE

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Public Function IBo_Load() As Boolean
    
    IBo_Load = Load

End Function
Private Function Load() As Boolean
    
Dim oView       As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
        
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count >= 1 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    Select Case (lFieldID)
        Case (FID_DELNOTELINE_DRLKey):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDRLKey
        Case (FID_DELNOTELINE_LineNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLineNo
        Case (FID_DELNOTELINE_PartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartCode
        Case (FID_DELNOTELINE_PartCodeAlphaCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartCodeAlphaCode
        Case (FID_DELNOTELINE_OrderQty):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mOrderQty
        Case (FID_DELNOTELINE_ReceivedQty):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mReceivedQty
        Case (FID_DELNOTELINE_OrderPrice):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mOrderPrice
        Case (FID_DELNOTELINE_BlanketDiscDesc):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mBlanketDiscDesc
        Case (FID_DELNOTELINE_BlanketPartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mBlanketPartCode
        Case (FID_DELNOTELINE_CurrentPrice):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mCurrentPrice
        Case (FID_DELNOTELINE_CurrentCost):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mCurrentCost
        Case (FID_DELNOTELINE_ShortageQty):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mShortageQty
        Case (FID_DELNOTELINE_ReturnQty):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mReturnQty
        Case (FID_DELNOTELINE_ToFollowQty):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mToFollowQty
        Case (FID_DELNOTELINE_ReturnReasonCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mReturnReasonCode
        Case (FID_DELNOTELINE_POLineNumber):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mPOLineNumber
        Case (FID_DELNOTELINE_Checked):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mChecked
        Case (FID_DELNOTELINE_SinglesSellingUnit):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mSinglesSellingUnit
        Case (FID_DELNOTELINE_OverQty):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mOverQty
        Case (FID_DELNOTELINE_Narrative):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mNarrative
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID

End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_DELNOTELINE, FID_DELNOTELINE_END_OF_STATIC, m_oSession)

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    If LenB(mReturnReasonCode) = 0 Then mReturnReasonCode = "00"
    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
Dim oStockLog    As cStockLog
Dim oStockMove   As cStockMovement
Dim dblSellUnits As Double
Dim oNarrLine    As cDelNoteNarrative
Dim blnOnPOrder  As Boolean
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
        MsgBox "Stock Log code removed"
        'Record Stock movement log entry
'        Set oStockLog = m_oSession.Database.CreateBusinessObject(CLASSID_STOCKLOG)
'        If eSave = SaveTypeIfExists Then 'Log date must match delivery note - unless maintaining
'            oStockLog.LogDate = Now()
'        Else
'            oStockLog.LogDate = DeliveryNote.DelNoteDate
'        End If
'        oStockLog.LogTime = Format$(Now, "HHMMSS")
'        oStockLog.PartCode = mPartCode
'        oStockLog.TransactionKey = DRLKey
'        blnOnPOrder = False
'        If mPOLineNumber <> 0 Then blnOnPOrder = True
'        Call oStockLog.SaveStockReceived(blnOnPOrder)
'        Set oStockLog = Nothing
'        'Update Stock Movement History
'        Set oStockMove = m_oSession.Database.CreateBusinessObject(CLASSID_STOCKMOVE)
'        dblSellUnits = mSinglesSellingUnit
'        If dblSellUnits = 0 Then dblSellUnits = 1
'        'Save new stock total, previous value is rolled back separately
'        Call oStockMove.SaveStockReceived(mPartCode, mReceivedQty, (mReceivedQty / dblSellUnits) * mOrderPrice)
'        Set oStockMove = Nothing
        If eSave = SaveTypeIfNew Then
            If LenB(mNarrative) <> 0 Then 'if narrative captured then call sub BO to save
                Set oNarrLine = m_oSession.Database.CreateBusinessObject(CLASSID_DELNOTENARRATIVE)
                oNarrLine.DocumentNumber = mDRLKey
                oNarrLine.NarrativeText1 = mNarrative
                oNarrLine.NarrativeText2 = vbNullString
                oNarrLine.StoreNumber = m_oSession.CurrentEnterprise.IEnterprise_StoreNumber
                oNarrLine.PartCode = mPartCode
                oNarrLine.SequenceNumber = mLineNo
                Call oNarrLine.IBo_SaveIfNew
                Set oNarrLine = Nothing
            End If
        End If 'new entry
        If eSave = SaveTypeIfExists Then 'roll back previous stock movements
            'Update Stock Movement History
            Set oStockMove = m_oSession.Database.CreateBusinessObject(CLASSID_STOCKMOVE)
            dblSellUnits = mSinglesSellingUnit
            If dblSellUnits = 0 Then dblSellUnits = 1
            Call oStockMove.SaveStockReceived(mPartCode, mPreviousQty * -1, (mReceivedQty / dblSellUnits) * mOrderPrice * -1)
            Set oStockMove = Nothing
        End If
        
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_DELNOTELINE * &H10000) + 1 To FID_DELNOTELINE_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter


'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As cDeliveryNoteLine

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set Interface = oBO
        Case Else:
            Set Interface = Me
    End Select

End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_DELNOTELINE_END_OF_STATIC

End Function

