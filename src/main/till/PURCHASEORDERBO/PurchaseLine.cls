VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPurchaseLine"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D53AD1F024E"
'<CAMH>****************************************************************************************
'* Module : PurchaseLine
'* Date   : 04/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/PurchaseOrderBO/PurchaseLine.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of a single Purchase Order Line Item.
'* This object should be used in conjunction with the Purchase Order object
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 5/05/04 10:09 $
'* $Revision: 11 $
'* Versions:
'* 04/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "cPurchaseLine"

Private m_oSession As Session
'##ModelId=3D53AD780046
Private mPurchaseOrderNo As Long

'##ModelId=3D53AD830000
Private mLineNo As Long

'##ModelId=3D53AD88010E
Private mPartCode As String

'##ModelId=3D53AD8B0082
Private mPartAlphaKey As String

'##ModelId=3D53AD93035C
Private mManufacturerCode As String

'##ModelId=3D53AD9D00FA
Private mSupplierPartCode As String

'##ModelId=3D53ADA9000A
Private mOrderQuantity As Double

'##ModelId=3D53ADB0035C
Private mOrderPrice As Currency

'##ModelId=3D53ADB502BC
Private mCostPrice As Double

'##ModelId=3D53ADB903A2
Private mLastOrderDate As Date

'##ModelId=3D53ADC40280
Private mQuantityReceived As Long

'##ModelId=3D53ADCB003C
Private mQuantityBackordered As Long

'##ModelId=3D53ADD5014A
Private mComplete As Boolean

'##ModelId=3D53ADDB028A
Private mDelNoteNumber As String

'##ModelId=3D53ADE40294
Private mDelNoteLineNo As String

'##ModelId=3D53ADF0008C
Private mDateLastReceipt As Date

'##ModelId=3D53ADF70226
Private mMessage As String

'##ModelId=3D53AE060000
Private mQuantityPerUnit As Long

Private mEDIConfirmedQuantity As Long

Private mEDIQuantityChangedCode As String

Private mPreviousQty As Long

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private mPurchaseOrder As cPurchaseOrder

Public Function UpdateOrderQuantity(ByVal dblNewQuantity As Double) As Boolean

Dim oRow As IRow
    
    mPreviousQty = mOrderQuantity
    Let mOrderQuantity = dblNewQuantity

    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    Call oRow.Add(GetField(FID_PURCHASEORDERLINE_PurchaseOrderNo))
    Call oRow.Add(GetField(FID_PURCHASEORDERLINE_LineNo))
    Call oRow.Add(GetField(FID_PURCHASEORDERLINE_OrderQuantity))
    
    Call m_oSession.Database.SavePartialBo(Me, oRow)
    Set oRow = Nothing
    
    UpdateOrderQuantity = True
    
End Function 'UpdateOrderQuantity

Friend Function UpdateConfirmedQuantity(lngNewQuantity As Long)

Dim oRow As IRow
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    Call oRow.Add(GetField(FID_PURCHASEORDERLINE_PurchaseOrderNo))
    mEDIConfirmedQuantity = lngNewQuantity
    Call oRow.Add(GetField(FID_PURCHASEORDERLINE_LineNo))
    Call oRow.Add(GetField(FID_PURCHASEORDERLINE_EDIConfirmedQuantity))
    
    Call m_oSession.Database.SavePartialBo(Me, oRow)
    Set oRow = Nothing
    
    UpdateConfirmedQuantity = True
    
End Function

Public Property Get PurchaseOrder() As cPurchaseOrder
   Set PurchaseOrder = mPurchaseOrder
End Property

Friend Property Let PurchaseOrder(ByRef Value As cPurchaseOrder)
    Set mPurchaseOrder = Value
End Property

'##ModelId=3D5B74720028
Public Property Get QuantityPerUnit() As Long
   Let QuantityPerUnit = mQuantityPerUnit
End Property

'##ModelId=3D5B74710294
Public Property Let QuantityPerUnit(ByVal Value As Long)
    Let mQuantityPerUnit = Value
End Property

'##ModelId=3D5B7471017C
Public Property Get Message() As String

Const PROCEDURE_NAME As String = MODULE_NAME & ".Message_Get"
   Let Message = mMessage
End Property 'Message

'##ModelId=3D5B74710000
Public Property Let Message(ByVal Value As String)
    Let mMessage = Value
End Property

'##ModelId=3D5B7470030C
Public Property Get DateLastReceipt() As Date
   Let DateLastReceipt = mDateLastReceipt
End Property

'##ModelId=3D5B74700186
Public Property Let DateLastReceipt(ByVal Value As Date)
    Let mDateLastReceipt = Value
End Property

'##ModelId=3D5B747000AA
Public Property Get DelNoteLineNo() As String
    Let DelNoteLineNo = mDelNoteLineNo
End Property

'##ModelId=3D5B746F0316
Public Property Let DelNoteLineNo(ByVal Value As String)
    If LenB(Value) = 0 Then Value = "0000"
     Let mDelNoteLineNo = Value
End Property

'##ModelId=3D5B746F023A
Public Property Get DelNoteNumber() As String
    Let DelNoteNumber = mDelNoteNumber
End Property

'##ModelId=3D5B746F00B4
Public Property Let DelNoteNumber(ByVal Value As String)
    If LenB(Value) = 0 Then Value = "000000"
    Let mDelNoteNumber = Value
End Property

'##ModelId=3D5B746E0352
Public Property Get Complete() As Boolean
   Let Complete = mComplete
End Property

'##ModelId=3D5B746E0212
Public Property Let Complete(ByVal Value As Boolean)
    Let mComplete = Value
End Property

'##ModelId=3D5B746E0136
Public Property Get QuantityBackordered() As Long
   Let QuantityBackordered = mQuantityBackordered
End Property

'##ModelId=3D5B746D03D4
Public Property Let QuantityBackordered(ByVal Value As Long)
    Let mQuantityBackordered = Value
    If mQuantityBackordered = 0 Then mComplete = True
End Property

'##ModelId=3D5B746D032A
Public Property Get QuantityReceived() As Long
   Let QuantityReceived = mQuantityReceived
End Property

'##ModelId=3D5B746D01E0
Public Property Let QuantityReceived(ByVal Value As Long)
    Let mQuantityReceived = Value
End Property

'##ModelId=3D5B746D0104
Public Property Get LastOrderDate() As Date
   Let LastOrderDate = mLastOrderDate
End Property

'##ModelId=3D5B746C0302
Public Property Let LastOrderDate(ByVal Value As Date)
    Let mLastOrderDate = Value
End Property

'##ModelId=3D5B746C0258
Public Property Get CostPrice() As Double
   Let CostPrice = mCostPrice
End Property

'##ModelId=3D5B746C00DC
Public Property Let CostPrice(ByVal Value As Double)
    Let mCostPrice = Value
End Property

'##ModelId=3D5B746C0032
Public Property Get OrderPrice() As Currency
   Let OrderPrice = mOrderPrice
End Property

'##ModelId=3D5B746B030C
Public Property Let OrderPrice(ByVal Value As Currency)
    Let mOrderPrice = Value
End Property

'##ModelId=3D5B746B0046
Public Property Get OrderQuantity() As Long
   Let OrderQuantity = mOrderQuantity
End Property

'##ModelId=3D5B746A0316
Public Property Let OrderQuantity(ByVal Value As Long)
    Let mOrderQuantity = Value
End Property

'##ModelId=3D5B746A0276
Public Property Get SupplierPartCode() As String
   Let SupplierPartCode = mSupplierPartCode
End Property

'##ModelId=3D5B746A019A
Public Property Let SupplierPartCode(ByVal Value As String)
    Let mSupplierPartCode = Value
End Property

Public Property Get EDIConfirmedQuantity() As Long
   Let EDIConfirmedQuantity = mEDIConfirmedQuantity
End Property

Public Property Let EDIConfirmedQuantity(ByVal Value As Long)
    Let mEDIConfirmedQuantity = Value
End Property

Public Property Get EDIQuantityChangedCode() As String
   Let EDIQuantityChangedCode = mEDIQuantityChangedCode
End Property

Public Property Let EDIQuantityChangedCode(ByVal Value As String)
    Let mEDIQuantityChangedCode = Value
End Property

'##ModelId=3D5B746A00F0
Public Property Get ManufacturerCode() As String
   Let ManufacturerCode = mManufacturerCode
End Property

'##ModelId=3D5B7469035C
Public Property Let ManufacturerCode(ByVal Value As String)
    Let mManufacturerCode = Value
End Property

'##ModelId=3D5B746902B2
Public Property Get PartAlphaKey() As String
   Let PartAlphaKey = mPartAlphaKey
End Property

'##ModelId=3D5B74690212
Public Property Let PartAlphaKey(ByVal Value As String)
    Let mPartAlphaKey = Value
End Property

'##ModelId=3D5B74690168
Public Property Get PartCode() As String
   Let PartCode = mPartCode
End Property

'##ModelId=3D5B746900C8
Public Property Let PartCode(ByVal Value As String)
    Let mPartCode = Value
End Property

'##ModelId=3D5B7469001E
Public Property Get LineNo() As Long
   Let LineNo = mLineNo
End Property

'##ModelId=3D5B74680366
Public Property Let LineNo(ByVal Value As Long)
    Let mLineNo = Value
End Property

'##ModelId=3D5B746802F8
Public Property Get PurchaseOrderNo() As Long
   Let PurchaseOrderNo = mPurchaseOrderNo
End Property


'##ModelId=3D5B7468024E
Public Property Let PurchaseOrderNo(ByVal Value As Long)
    Let mPurchaseOrderNo = Value
End Property

Public Sub RecordItemOrdered(sPartCode As String, dblQuantity As Double)

End Sub

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 15/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_PURCHASEORDERLINE_PurchaseOrderNo):        mPurchaseOrderNo = oField.ValueAsVariant
            Case (FID_PURCHASEORDERLINE_LineNo):                 mLineNo = oField.ValueAsVariant
            Case (FID_PURCHASEORDERLINE_PartCode):               mPartCode = oField.ValueAsVariant
            Case (FID_PURCHASEORDERLINE_PartAlphaKey):           mPartAlphaKey = oField.ValueAsVariant
            Case (FID_PURCHASEORDERLINE_ManufacturerCode):       mManufacturerCode = oField.ValueAsVariant
            Case (FID_PURCHASEORDERLINE_SupplierPartCode):       mSupplierPartCode = oField.ValueAsVariant
            Case (FID_PURCHASEORDERLINE_OrderQuantity):          mOrderQuantity = oField.ValueAsVariant
            Case (FID_PURCHASEORDERLINE_OrderPrice):             mOrderPrice = oField.ValueAsVariant
            Case (FID_PURCHASEORDERLINE_CostPrice):              mCostPrice = oField.ValueAsVariant
            Case (FID_PURCHASEORDERLINE_LastOrderDate):          mLastOrderDate = oField.ValueAsVariant
            Case (FID_PURCHASEORDERLINE_QuantityReceived):       mQuantityReceived = oField.ValueAsVariant
            Case (FID_PURCHASEORDERLINE_QuantityBackordered):    mQuantityBackordered = oField.ValueAsVariant
            Case (FID_PURCHASEORDERLINE_Complete):               mComplete = oField.ValueAsVariant
            Case (FID_PURCHASEORDERLINE_DelNoteNumber):          mDelNoteNumber = oField.ValueAsVariant
            Case (FID_PURCHASEORDERLINE_DelNoteLineNo):          mDelNoteLineNo = oField.ValueAsVariant
            Case (FID_PURCHASEORDERLINE_DateLastReceipt):        mDateLastReceipt = oField.ValueAsVariant
            Case (FID_PURCHASEORDERLINE_Message):                mMessage = oField.ValueAsVariant
            Case (FID_PURCHASEORDERLINE_QuantityPerUnit):        mQuantityPerUnit = oField.ValueAsVariant
            Case (FID_PURCHASEORDERLINE_EDIConfirmedQuantity):   mEDIConfirmedQuantity = oField.ValueAsVariant
            Case (FID_PURCHASEORDERLINE_EDIQuantityChangedCode): mEDIQuantityChangedCode = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow


Private Sub Class_Initialize()
    
    Call DebugMsg(MODULE_NAME, "Initialise", endlTraceIn)
    'Reset Line counters to initial values
    mDelNoteNumber = "000000"
    mDelNoteLineNo = "0000"

End Sub

Public Function IBo_CreateNew() As IBo

    Set IBo_CreateNew = New cPurchaseLine

End Function

Public Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Purchase Order Line " & mPurchaseOrderNo & " Line " & LineNo

End Property

Public Function IBo_GetField(nFid As Long) As IField

    Set IBo_GetField = GetField(nFid)

End Function

Public Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_PURCHASEORDERLINE

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Public Function IBo_Load() As Boolean
    
    IBo_Load = Load

End Function
Public Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count = 1 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function
Friend Function LoadLines() As Collection

' Load up all properties from the database
Dim oView       As IView
Dim lLineNo     As Long
Dim colLines    As Collection
Dim oLine       As cPurchaseLine
Dim oRow        As IRow
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    'Merge selection criteria together
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        'Build up a collection of Purchase Order Lines that will be passed out
        Set colLines = New Collection
        For lLineNo = 1 To oView.Count
            Set oLine = m_oSession.Database.CreateBusinessObject(CLASSID_PURCHASEORDERLINE)
            Set oRow = oView.Row(CLng(lLineNo))
            Call oLine.LoadFromRow(oRow)
            Call colLines.Add(oLine)
        Next lLineNo
    End If
    Set LoadLines = colLines

End Function
Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    Select Case (lFieldID)
        Case (FID_PURCHASEORDERLINE_PurchaseOrderNo):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mPurchaseOrderNo
        Case (FID_PURCHASEORDERLINE_LineNo):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mLineNo
        Case (FID_PURCHASEORDERLINE_PartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartCode
        Case (FID_PURCHASEORDERLINE_PartAlphaKey):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartAlphaKey
        Case (FID_PURCHASEORDERLINE_ManufacturerCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mManufacturerCode
        Case (FID_PURCHASEORDERLINE_SupplierPartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSupplierPartCode
        Case (FID_PURCHASEORDERLINE_OrderQuantity):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mOrderQuantity
        Case (FID_PURCHASEORDERLINE_OrderPrice):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mOrderPrice
        Case (FID_PURCHASEORDERLINE_CostPrice):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mCostPrice
        Case (FID_PURCHASEORDERLINE_LastOrderDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mLastOrderDate
        Case (FID_PURCHASEORDERLINE_QuantityReceived):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mQuantityReceived
        Case (FID_PURCHASEORDERLINE_QuantityBackordered):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mQuantityBackordered
        Case (FID_PURCHASEORDERLINE_Complete):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mComplete
        Case (FID_PURCHASEORDERLINE_DelNoteNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDelNoteNumber
        Case (FID_PURCHASEORDERLINE_DelNoteLineNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDelNoteLineNo
        Case (FID_PURCHASEORDERLINE_DateLastReceipt):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDateLastReceipt
        Case (FID_PURCHASEORDERLINE_Message):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mMessage
        Case (FID_PURCHASEORDERLINE_QuantityPerUnit):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mQuantityPerUnit
        Case (FID_PURCHASEORDERLINE_EDIConfirmedQuantity):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mEDIConfirmedQuantity
        Case (FID_PURCHASEORDERLINE_EDIQuantityChangedCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mEDIQuantityChangedCode
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID

End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_PURCHASEORDERLINE, FID_PURCHASEORDERLINE_END_OF_STATIC, m_oSession)

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database

Dim oItem As cInventory
'Dim oOrder As cOrderLine
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
        If eSave = SaveTypeIfNew Then
            Set oItem = m_oSession.Database.CreateBusinessObject(CLASSID_INVENTORY)
            Call oItem.RecordItemOrdered(mPartCode, mPurchaseOrder.OrderDate, mOrderQuantity)
            Set oItem = Nothing
            
            'Check if New Order Line is for a Customer Order - if so mark line as ordered
           
        End If
    End If

End Function
Public Sub RecordItemDelivered(dblQuantityRecd As Double, dblQuantityBackOrder As Double, sDelNoteNo As String, lDelNoteLineNo As Long, dteDelNoteDate As Date)

Dim oRow As IRow

    'Update values
    Me.QuantityReceived = Me.QuantityReceived + dblQuantityRecd
    Me.QuantityBackordered = dblQuantityBackOrder
    mDelNoteNumber = sDelNoteNo
    mDelNoteLineNo = lDelNoteLineNo
    mDateLastReceipt = dteDelNoteDate
    'Save back to database
    
    'Save updated fields back to Database
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    Call oRow.Add(GetField(FID_PURCHASEORDERLINE_PurchaseOrderNo))
    Call oRow.Add(GetField(FID_PURCHASEORDERLINE_LineNo))
    Call oRow.Add(GetField(FID_PURCHASEORDERLINE_QuantityReceived))
    Call oRow.Add(GetField(FID_PURCHASEORDERLINE_QuantityBackordered))
    Call oRow.Add(GetField(FID_PURCHASEORDERLINE_DelNoteNumber))
    Call oRow.Add(GetField(FID_PURCHASEORDERLINE_DelNoteLineNo))
    Call oRow.Add(GetField(FID_PURCHASEORDERLINE_DateLastReceipt))
    
    Call m_oSession.Database.SavePartialBo(Me, oRow)
    Set oRow = Nothing

End Sub

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_PURCHASEORDERLINE * &H10000) + 1 To FID_PURCHASEORDERLINE_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

Private Function Initialise(oSession As ISession) As cPurchaseLine
    Set m_oSession = oSession
    Set Initialise = Me
End Function

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As cPurchaseLine

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set Interface = oBO
        Case Else:
            Set Interface = Me
    End Select

End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_PURCHASEORDERLINE_END_OF_STATIC

End Function


