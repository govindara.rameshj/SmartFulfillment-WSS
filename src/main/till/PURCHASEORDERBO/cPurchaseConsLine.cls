VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPurchaseConsLine"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3E1E95DD012C"
'<CAMH>****************************************************************************************
'* Module : cPurchaseConsLine
'* Date   : 10/01/03
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/PurchaseOrderBO/cPurchaseConsLine.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Damians $
'* $Date: 7/05/04 16:16 $
'* $Revision: 3 $
'* Versions:
'* 10/01/03    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cPurchaseConsLine"

Implements IBo
Implements ISysBo

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=3E1E968D01F4
Private mPurchaseOrderKey As Long

'##ModelId=3E1E96DE0046
Private mLineNumber As Long

'##ModelId=3E1E96E302DA
Private mPurchaseOrderNumber As String

Private mPurchaseOrderLineNo As Long

'##ModelId=3E1E97250398
Private mPartCode As String

'##ModelId=3E1E972A01D6
Private mProductCode As String

'##ModelId=3E1E97310078
Private mQuantity As Long

'##ModelId=3E1E974102F8
Private mPrice As String

'##ModelId=3E1E97450302
Private mCost As String

'##ModelId=3E1E97490046
Private mQuantityPerUnit As Double

'##ModelId=3E1E976B01C2
Private mComplete As Boolean

'##ModelId=3E1E977500AA
Private mOrderDate As Date

'##ModelId=3E1E9EB200BE
Public Property Get OrderDate() As Date
   Let OrderDate = mOrderDate
End Property

'##ModelId=3E1E9EB2001E
Public Property Let OrderDate(ByVal Value As Date)
    Let mOrderDate = Value
End Property

'##ModelId=3E1E9EB10398
Public Property Get Complete() As Boolean
   Let Complete = mComplete
End Property

'##ModelId=3E1E9EB1032A
Public Property Let Complete(ByVal Value As Boolean)
    Let mComplete = Value
End Property

'##ModelId=3E1E9EB102BC
Public Property Get QuantityPerUnit() As Double
   Let QuantityPerUnit = mQuantityPerUnit
End Property

'##ModelId=3E1E9EB1021C
Public Property Let QuantityPerUnit(ByVal Value As Double)
    Let mQuantityPerUnit = Value
End Property

'##ModelId=3E1E9EB101AE
Public Property Get Cost() As String
   Let Cost = mCost
End Property

'##ModelId=3E1E9EB10140
Public Property Let Cost(ByVal Value As String)
    Let mCost = Value
End Property

'##ModelId=3E1E9EB100D2
Public Property Get Price() As String
   Let Price = mPrice
End Property

'##ModelId=3E1E9EB10028
Public Property Let Price(ByVal Value As String)
    Let mPrice = Value
End Property

'##ModelId=3E1E9EB003A2
Public Property Get Quantity() As Long
   Let Quantity = mQuantity
End Property

'##ModelId=3E1E9EB00334
Public Property Let Quantity(ByVal Value As Long)
    Let mQuantity = Value
End Property

'##ModelId=3E1E9EB002C6
Public Property Get ProductCode() As String
   Let ProductCode = mProductCode
End Property

'##ModelId=3E1E9EB00258
Public Property Let ProductCode(ByVal Value As String)
    Let mProductCode = Value
End Property

'##ModelId=3E1E9EB00226
Public Property Get PartCode() As String
   Let PartCode = mPartCode
End Property

'##ModelId=3E1E9EB0017C
Public Property Let PartCode(ByVal Value As String)
    Let mPartCode = Value
End Property

'##ModelId=3E1E9EAF037B
Public Property Get PurchaseOrderNumber() As String
   Let PurchaseOrderNumber = mPurchaseOrderNumber
End Property

'##ModelId=3E1E9EAF033E
Public Property Let PurchaseOrderNumber(ByVal Value As String)
    Let mPurchaseOrderNumber = Value
End Property

Public Property Get PurchaseOrderLineNo() As Long
   Let PurchaseOrderLineNo = mPurchaseOrderLineNo
End Property

Public Property Let PurchaseOrderLineNo(ByVal Value As Long)
    Let mPurchaseOrderLineNo = Value
End Property

'##ModelId=3E1E9EAF030C
Public Property Get LineNumber() As Long
   Let LineNumber = mLineNumber
End Property

'##ModelId=3E1E9EAF029E
Public Property Let LineNumber(ByVal Value As Long)
    Let mLineNumber = Value
End Property

'##ModelId=3E1E9EAF0262
Public Property Get PurchaseOrderKey() As Long
   Let PurchaseOrderKey = mPurchaseOrderKey
End Property


'##ModelId=3E1E9EAF0230
Public Property Let PurchaseOrderKey(ByVal Value As Long)
    Let mPurchaseOrderKey = Value
End Property
Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database

'Dim oItem As cOrderLine
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
        
        If eSave = SaveTypeIfNew Then
            'Check if New Order Line is for a Customer Order - if so mark line as ordered
        End If
            
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_PURCHASECONSLINE * &H10000) + 1 To FID_PURCHASECONSLINE_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cPurchaseConsLine

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_PURCHASECONSLINE, FID_PURCHASECONSLINE_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_PURCHASECONSLINE_PurchaseOrderKey):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = PurchaseOrderKey
        Case (FID_PURCHASECONSLINE_LineNumber):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = LineNumber
        Case (FID_PURCHASECONSLINE_PurchaseOrderNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = PurchaseOrderNumber
        Case (FID_PURCHASECONSLINE_PurchaseOrderLineNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = PurchaseOrderLineNo
        Case (FID_PURCHASECONSLINE_PartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = PartCode
        Case (FID_PURCHASECONSLINE_ProductCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = ProductCode
        Case (FID_PURCHASECONSLINE_Quantity):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = Quantity
        Case (FID_PURCHASECONSLINE_Price):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = Price
        Case (FID_PURCHASECONSLINE_Cost):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = Cost
        Case (FID_PURCHASECONSLINE_QuantityPerUnit):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = QuantityPerUnit
        Case (FID_PURCHASECONSLINE_Complete):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = Complete
        Case (FID_PURCHASECONSLINE_OrderDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = OrderDate
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cPurchaseConsLine
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_PURCHASECONSLINE

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Purchase Order Consol Line " & mPurchaseOrderKey & " " & mLineNumber

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_PURCHASECONSLINE_PurchaseOrderKey):    PurchaseOrderKey = oField.ValueAsVariant
            Case (FID_PURCHASECONSLINE_LineNumber):          LineNumber = oField.ValueAsVariant
            Case (FID_PURCHASECONSLINE_PurchaseOrderNumber): PurchaseOrderNumber = oField.ValueAsVariant
            Case (FID_PURCHASECONSLINE_PurchaseOrderLineNo): PurchaseOrderLineNo = oField.ValueAsVariant
            Case (FID_PURCHASECONSLINE_PartCode):            PartCode = oField.ValueAsVariant
            Case (FID_PURCHASECONSLINE_ProductCode):         ProductCode = oField.ValueAsVariant
            Case (FID_PURCHASECONSLINE_Quantity):            Quantity = oField.ValueAsVariant
            Case (FID_PURCHASECONSLINE_Price):               Price = oField.ValueAsVariant
            Case (FID_PURCHASECONSLINE_Cost):                Cost = oField.ValueAsVariant
            Case (FID_PURCHASECONSLINE_QuantityPerUnit):     QuantityPerUnit = oField.ValueAsVariant
            Case (FID_PURCHASECONSLINE_Complete):            Complete = oField.ValueAsVariant
            Case (FID_PURCHASECONSLINE_OrderDate):           OrderDate = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As cPurchaseConsLine

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set Interface = oBO
        Case Else:
            Set Interface = Me
    End Select

End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_PURCHASECONSLINE_END_OF_STATIC

End Function

