VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cConsignmentHeader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D5CB1D7008C"
'<CAMH>****************************************************************************************
'* Module : ConsignmentHeader
'* Date   : 08/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/PurchaseOrderBO/ConsignmentHeader.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Damians $ $Date: 7/05/04 16:16 $ $Revision: 8 $
'* Versions:
'* 08/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "ConsignmentHeader"

'##ModelId=3D5CB33E0014
Private mConsignmentNo As String

'##ModelId=3D5CB3490186
Private mPurchaseOrderNo As String

'##ModelId=3D5CB35102A8
Private mPurchaseOrderRelNo As Long

'##ModelId=3D5CB35F03C0
Private mRecordedInSystem As Boolean

'##ModelId=3D5CB38C0348
Private mConsignmentDate As Date

'##ModelId=3D5CB39502A8
Private mSupplierNo As String

'##ModelId=3D5CB399035C
Private mIBTConsignment As Boolean

'##ModelId=3D5CB3A50078
Private mIBTStoreNo As String

'##ModelId=3D5CB3AE0046
Private mDeliveryNoteNo(9) As String

'##ModelId=3D5CB3B60136
Private mPrintInitials1 As String

'##ModelId=3D5CB3E2008C
Private mPrintTime1 As String

'##ModelId=3D5CB436033E
Private mPrintInitials2 As String

'##ModelId=3D5CB43C03D4
Private mPrintTime2 As String

'##ModelId=3D5CB4430050
Private mPrintInitials3 As String

'##ModelId=3D5CB44A0370
Private mPrintTime3 As String

'##ModelId=3D5CB44F017C
Private mPrintInitials4 As String

'##ModelId=3D5CB45600FA
Private mPrintTime4 As String

'##ModelId=3D5CB461029E
Private mPrintInitials5 As String

'##ModelId=3D5CB46B03B6
Private mPrintTime5 As String

'##ModelId=3D5CB47402DA
Private mPrintInitials6 As String

'##ModelId=3D5CB47E01CC
Private mPrintTime6 As String

'##ModelId=3D5CB48202E4
Private mPrintedCount As Long

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session



'##ModelId=3D73151F00E6
Public Property Get PrintedCount() As Long
   Let PrintedCount = mPrintedCount
End Property

'##ModelId=3D73151E0352
Public Property Let PrintedCount(ByVal Value As Long)
    Let mPrintedCount = Value
End Property

'##ModelId=3D73151E023A
Public Property Get PrintTime6() As String
   Let PrintTime6 = mPrintTime6
End Property

'##ModelId=3D73151E00BE
Public Property Let PrintTime6(ByVal Value As String)
    Let mPrintTime6 = Value
End Property

'##ModelId=3D73151D03CA
Public Property Get PrintInitials6() As String
   Let PrintInitials6 = mPrintInitials6
End Property

'##ModelId=3D73151D0244
Public Property Let PrintInitials6(ByVal Value As String)
    Let mPrintInitials6 = Value
End Property

'##ModelId=3D73151D0136
Public Property Get PrintTime5() As String
   Let PrintTime5 = mPrintTime5
End Property

'##ModelId=3D73151C03D4
Public Property Let PrintTime5(ByVal Value As String)
    Let mPrintTime5 = Value
End Property

'##ModelId=3D73151C02C6
Public Property Get PrintInitials5() As String
   Let PrintInitials5 = mPrintInitials5
End Property

'##ModelId=3D73151C017C
Public Property Let PrintInitials5(ByVal Value As String)
    Let mPrintInitials5 = Value
End Property

'##ModelId=3D73151C00A0
Public Property Get PrintTime4() As String
   Let PrintTime4 = mPrintTime4
End Property

'##ModelId=3D73151B0302
Public Property Let PrintTime4(ByVal Value As String)
    Let mPrintTime4 = Value
End Property

'##ModelId=3D73151B0226
Public Property Get PrintInitials4() As String
   Let PrintInitials4 = mPrintInitials4
End Property

'##ModelId=3D73151B00DC
Public Property Let PrintInitials4(ByVal Value As String)
    Let mPrintInitials4 = Value
End Property

'##ModelId=3D73151B0000
Public Property Get PrintTime3() As String
   Let PrintTime3 = mPrintTime3
End Property

'##ModelId=3D73151A029E
Public Property Let PrintTime3(ByVal Value As String)
    Let mPrintTime3 = Value
End Property

'##ModelId=3D73151A01FE
Public Property Get PrintInitials3() As String
   Let PrintInitials3 = mPrintInitials3
End Property

'##ModelId=3D73151A00B4
Public Property Let PrintInitials3(ByVal Value As String)
    Let mPrintInitials3 = Value
End Property

'##ModelId=3D73151903C0
Public Property Get PrintTime2() As String
   Let PrintTime2 = mPrintTime2
End Property

'##ModelId=3D73151902A8
Public Property Let PrintTime2(ByVal Value As String)
    Let mPrintTime2 = Value
End Property

'##ModelId=3D73151901CC
Public Property Get PrintInitials2() As String
   Let PrintInitials2 = mPrintInitials2
End Property

'##ModelId=3D73151900BE
Public Property Let PrintInitials2(ByVal Value As String)
    Let mPrintInitials2 = Value
End Property

'##ModelId=3D73151803CA
Public Property Get PrintTime1() As String
   Let PrintTime1 = mPrintTime1
End Property

'##ModelId=3D73151802BC
Public Property Let PrintTime1(ByVal Value As String)
    Let mPrintTime1 = Value
End Property

'##ModelId=3D7315180212
Public Property Get PrintInitials1() As String
   Let PrintInitials1 = mPrintInitials1
End Property

'##ModelId=3D73151800C8
Public Property Let PrintInitials1(ByVal Value As String)
    Let mPrintInitials1 = Value
End Property

'##ModelId=3D7315180028
Public Property Get DeliveryNoteNo(Index As Variant) As String
   Let DeliveryNoteNo = mDeliveryNoteNo(Index)
End Property

'##ModelId=3D73151702F8
Public Property Let DeliveryNoteNo(Index As Variant, ByVal Value As String)
    Let mDeliveryNoteNo(Index) = Value
End Property

'##ModelId=3D7315170258
Public Property Get IBTStoreNo() As String
   Let IBTStoreNo = mIBTStoreNo
End Property

'##ModelId=3D731517017C
Public Property Let IBTStoreNo(ByVal Value As String)
    Let mIBTStoreNo = Value
End Property

'##ModelId=3D73151700D2
Public Property Get IBTConsignment() As Boolean
   Let IBTConsignment = mIBTConsignment
End Property

'##ModelId=3D73151603AC
Public Property Let IBTConsignment(ByVal Value As Boolean)
    Let mIBTConsignment = Value
End Property

'##ModelId=3D7315160302
Public Property Get SupplierNo() As String
   Let SupplierNo = mSupplierNo
End Property

'##ModelId=3D7315160226
Public Property Let SupplierNo(ByVal Value As String)
    Let mSupplierNo = Value
End Property

'##ModelId=3D7315160186
Public Property Get ConsignmentDate() As Date
   Let ConsignmentDate = mConsignmentDate
End Property

'##ModelId=3D73151600AA
Public Property Let ConsignmentDate(ByVal Value As Date)
    Let mConsignmentDate = Value
End Property

'##ModelId=3D731516003C
Public Property Get RecordedInSystem() As Boolean
   Let RecordedInSystem = mRecordedInSystem
End Property

'##ModelId=3D7315150348
Public Property Let RecordedInSystem(ByVal Value As Boolean)
    Let mRecordedInSystem = Value
End Property

'##ModelId=3D731515029E
Public Property Get PurchaseOrderRelNo() As Long
   Let PurchaseOrderRelNo = mPurchaseOrderRelNo
End Property

'##ModelId=3D73151501CC
Public Property Let PurchaseOrderRelNo(ByVal Value As Long)
    Let mPurchaseOrderRelNo = Value
End Property

'##ModelId=3D731515015E
Public Property Get PurchaseOrderNo() As String
   Let PurchaseOrderNo = mPurchaseOrderNo
End Property

'##ModelId=3D73151403C0
Public Property Let PurchaseOrderNo(ByVal Value As String)
    Let mPurchaseOrderNo = Value
End Property

'##ModelId=3D7315140352
Public Property Get ConsignmentNo() As String
   Let ConsignmentNo = mConsignmentNo
End Property


'##ModelId=3D73151402B2
Public Property Let ConsignmentNo(ByVal Value As String)
    Let mConsignmentNo = Value
End Property



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_CONSHEADER

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Consigment No " & mConsignmentNo

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_CONSHEADER_ConsignmentNo):    mConsignmentNo = oField.ValueAsVariant
            Case (FID_CONSHEADER_PurchaseOrderNo):  mPurchaseOrderNo = oField.ValueAsVariant
            Case (FID_CONSHEADER_PurchaseOrderRelNo): mPurchaseOrderRelNo = oField.ValueAsVariant
            Case (FID_CONSHEADER_RecordedInSystem): mRecordedInSystem = oField.ValueAsVariant
            Case (FID_CONSHEADER_ConsignmentDate):  mConsignmentDate = oField.ValueAsVariant
            Case (FID_CONSHEADER_SupplierNo):       mSupplierNo = oField.ValueAsVariant
            Case (FID_CONSHEADER_IBTConsignment):   mIBTConsignment = oField.ValueAsVariant
            Case (FID_CONSHEADER_IBTStoreNo):       mIBTStoreNo = oField.ValueAsVariant
            Case (FID_CONSHEADER_DeliveryNoteNo):   Call LoadStringArray(mDeliveryNoteNo, oField, m_oSession)
            Case (FID_CONSHEADER_PrintInitials1):   mPrintInitials1 = oField.ValueAsVariant
            Case (FID_CONSHEADER_PrintTime1):       mPrintTime1 = oField.ValueAsVariant
            Case (FID_CONSHEADER_PrintInitials2):   mPrintInitials2 = oField.ValueAsVariant
            Case (FID_CONSHEADER_PrintTime2):       mPrintTime2 = oField.ValueAsVariant
            Case (FID_CONSHEADER_PrintInitials3):   mPrintInitials3 = oField.ValueAsVariant
            Case (FID_CONSHEADER_PrintTime3):       mPrintTime3 = oField.ValueAsVariant
            Case (FID_CONSHEADER_PrintInitials4):   mPrintInitials4 = oField.ValueAsVariant
            Case (FID_CONSHEADER_PrintTime4):       mPrintTime4 = oField.ValueAsVariant
            Case (FID_CONSHEADER_PrintInitials5):   mPrintInitials5 = oField.ValueAsVariant
            Case (FID_CONSHEADER_PrintTime5):       mPrintTime5 = oField.ValueAsVariant
            Case (FID_CONSHEADER_PrintInitials6):   mPrintInitials6 = oField.ValueAsVariant
            Case (FID_CONSHEADER_PrintTime6):       mPrintTime6 = oField.ValueAsVariant
            Case (FID_CONSHEADER_PrintedCount):     mPrintedCount = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
Dim oSysNum  As cSystemNumbers
    
    Save = False
    
    If IsValid() Then
        If eSave = SaveTypeIfNew Then
            'Get Next Purchase Order Number
            Set oSysNum = m_oSession.Database.CreateBusinessObject(CLASSID_SYSTEMNO)
            mConsignmentNo = oSysNum.GetNewConsignmentNoteNumber
            Set oSysNum = Nothing
        End If
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_CONSHEADER * &H10000) + 1 To FID_CONSHEADER_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cConsignmentHeader

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_CONSHEADER, FID_CONSHEADER_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_CONSHEADER_ConsignmentNo):
                    Set GetField = New CFieldString
                    GetField.ValueAsVariant = mConsignmentNo
        Case (FID_CONSHEADER_PurchaseOrderNo):
                    Set GetField = New CFieldString
                    GetField.ValueAsVariant = mPurchaseOrderNo
        Case (FID_CONSHEADER_PurchaseOrderRelNo):
                    Set GetField = New CFieldLong
                    GetField.ValueAsVariant = mPurchaseOrderRelNo
        Case (FID_CONSHEADER_RecordedInSystem):
                    Set GetField = New CFieldBool
                    GetField.ValueAsVariant = mRecordedInSystem
        Case (FID_CONSHEADER_ConsignmentDate):
                    Set GetField = New CFieldDate
                    GetField.ValueAsVariant = mConsignmentDate
        Case (FID_CONSHEADER_SupplierNo):
                    Set GetField = New CFieldString
                    GetField.ValueAsVariant = mSupplierNo
        Case (FID_CONSHEADER_IBTConsignment):
                    Set GetField = New CFieldBool
                    GetField.ValueAsVariant = mIBTConsignment
        Case (FID_CONSHEADER_IBTStoreNo):
                    Set GetField = New CFieldString
                    GetField.ValueAsVariant = mIBTStoreNo
        Case (FID_CONSHEADER_DeliveryNoteNo): Set GetField = SaveStringArray(mDeliveryNoteNo, m_oSession)
        Case (FID_CONSHEADER_PrintInitials1):
                    Set GetField = New CFieldString
                    GetField.ValueAsVariant = mPrintInitials1
        Case (FID_CONSHEADER_PrintTime1):
                    Set GetField = New CFieldString
                    GetField.ValueAsVariant = mPrintTime1
        Case (FID_CONSHEADER_PrintInitials2):
                    Set GetField = New CFieldString
                    GetField.ValueAsVariant = mPrintInitials2
        Case (FID_CONSHEADER_PrintTime2):
                    Set GetField = New CFieldString
                    GetField.ValueAsVariant = mPrintTime2
        Case (FID_CONSHEADER_PrintInitials3):
                    Set GetField = New CFieldString
                    GetField.ValueAsVariant = mPrintInitials3
        Case (FID_CONSHEADER_PrintTime3):
                    Set GetField = New CFieldString
                    GetField.ValueAsVariant = mPrintTime3
        Case (FID_CONSHEADER_PrintInitials4):
                    Set GetField = New CFieldString
                    GetField.ValueAsVariant = mPrintInitials4
        Case (FID_CONSHEADER_PrintTime4):
                    Set GetField = New CFieldString
                    GetField.ValueAsVariant = mPrintTime4
        Case (FID_CONSHEADER_PrintInitials5):
                    Set GetField = New CFieldString
                    GetField.ValueAsVariant = mPrintInitials5
        Case (FID_CONSHEADER_PrintTime5):
                    Set GetField = New CFieldString
                    GetField.ValueAsVariant = mPrintTime5
        Case (FID_CONSHEADER_PrintInitials6):
                    Set GetField = New CFieldString
                    GetField.ValueAsVariant = mPrintInitials6
        Case (FID_CONSHEADER_PrintTime6):
                    Set GetField = New CFieldString
                    GetField.ValueAsVariant = mPrintTime6
        Case (FID_CONSHEADER_PrintedCount):
                    Set GetField = New CFieldLong
                    GetField.ValueAsVariant = mPrintedCount
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cConsignmentHeader
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As cConsignmentHeader

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set Interface = oBO
        Case Else:
            Set Interface = Me
    End Select

End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_CONSHEADER_END_OF_STATIC

End Function


