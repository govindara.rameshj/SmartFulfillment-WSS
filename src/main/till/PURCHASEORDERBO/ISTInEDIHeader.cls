VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cISTInEDIHeader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3DF8AB850050"
'<CAMH>****************************************************************************************
'* Module : ISTInEDIHeader
'* Date   : 12/12/02
'* Author : Unknown
'*$Archive: /Projects/OasysV2/VB/PurchaseOrderBO/ISTInEDIHeader.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Damians $
'* $Date: 7/05/04 16:16 $
'* $Revision: 5 $
'* Versions:
'* 12/12/02    Unknown
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "ISTInEDIHeader"

Implements IBo
Implements ISysBo

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

Private mcolLines As Collection

'##ModelId=3DF8AD3E01D6
Private mSourceStoreNumber As String

'##ModelId=3DF8AD4F0370
Private mISTOutNumber As String

'##ModelId=3DF8AD6B03AC
Private mSentDate As Date

'##ModelId=3DF8AD7202EE
Private mReceivedDate As Date

'##ModelId=3DF8AD7D0050
Private mReceiptDate As Date

'##ModelId=3DF8AD8203C0
Private mTotalQuantity As Long

'##ModelId=3DF8AD8C015E
Private mTotalValue As Double

'##ModelId=3DF8AD91026C
Private mTotalCost As Double

'##ModelId=3DF8AD9D02C6
Private mReceived As Boolean

'##ModelId=3DF8ADA2028A
Private mCancelled As Boolean

'##ModelId=3DF8ADA70028
Private mReceivedBy As String

'##ModelId=3DF8ADB20050
Private mISTInQuantity As Long

'##ModelId=3DF8ADB90302
Private mISTOutCancelled As String

'##ModelId=3DF8ADCE0140
Private mISTInNumber As String

Private mGetLines As Boolean
Public Function RecordCancelled(strSourceStoreNo As String, strISTOutNo As String, blnEDICancel As Boolean) As Boolean
'mark IST In as Received, cancelled and flag cancelled source

Const CANCEL_EDI As String = "C"
Const CANCEL_MANUAL As String = "M"

Dim oRow        As IRow
Dim oGField     As CFieldGroup
    
    'Ensure IST Out is in memory
    RecordCancelled = False
    If (strSourceStoreNo <> mSourceStoreNumber) Or (strISTOutNo <> mISTOutNumber) Then
        Call IBo_ClearLoadFilter
        Call IBo_AddLoadFilter(CMP_EQUAL, FID_ISTINEDIHEADER_ISTOutNumber, mISTOutNumber)
        Call IBo_AddLoadFilter(CMP_EQUAL, FID_ISTINEDIHEADER_SourceStoreNumber, mSourceStoreNumber)
        Call IBo_AddLoadField(FID_ISTINEDIHEADER_Cancelled)
        Call IBo_LoadMatches
    End If
    
    'Set combination of fields to mark as deleted
    mCancelled = True
    mReceived = True
    If blnEDICancel = False Then
        mReceivedBy = m_oSession.UserInitials
        mISTOutCancelled = CANCEL_MANUAL
    Else
        mISTOutCancelled = CANCEL_EDI
    End If
    mReceiptDate = Now()
    
    'build up saving row
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_ISTINEDIHEADER_SourceStoreNumber))
    Call oRow.Add(GetField(FID_ISTINEDIHEADER_ISTOutNumber))
    Call oRow.Add(GetField(FID_ISTINEDIHEADER_Cancelled))
    Call oRow.Add(GetField(FID_ISTINEDIHEADER_Received))
    Call oRow.Add(GetField(FID_ISTINEDIHEADER_ReceivedBy))
    Call oRow.Add(GetField(FID_ISTINEDIHEADER_ReceiptDate))
    Call oRow.Add(GetField(FID_ISTINEDIHEADER_ISTOutCancelled))
    
    If m_oSession.Database.SavePartialBo(Me, oRow) = False Then Exit Function
    
    RecordCancelled = True

End Function

Public Property Let GetLines(ByVal Value As Boolean)
   Let mGetLines = Value
End Property

'##ModelId=3DF8BD5901F4
Public Property Get ISTInNumber() As String
   Let ISTInNumber = mISTInNumber
End Property

'##ModelId=3DF8BD580316
Public Property Get ISTOutCancelled() As String
   Let ISTOutCancelled = mISTOutCancelled
End Property

'##ModelId=3DF8BD580190
Public Property Let ISTOutCancelled(ByVal Value As String)
    Let mISTOutCancelled = Value
End Property

'##ModelId=3DF8BD580082
Public Property Get ISTInQuantity() As Long
   Let ISTInQuantity = mISTInQuantity
End Property

'##ModelId=3DF8BD5701D6
Public Property Get ReceivedBy() As String
   Let ReceivedBy = mReceivedBy
End Property

'##ModelId=3DF8BD560366
Public Property Get Cancelled() As Boolean
   Let Cancelled = mCancelled
End Property

'##ModelId=3DF8BD5601E0
Public Property Let Cancelled(ByVal Value As Boolean)
    Let mCancelled = Value
End Property

'##ModelId=3DF8BD5600D2
Public Property Get Received() As Boolean
   Let Received = mReceived
End Property

'##ModelId=3DF8BD550294
Public Property Get TotalCost() As Double
   Let TotalCost = mTotalCost
End Property

'##ModelId=3DF8BD55014A
Public Property Let TotalCost(ByVal Value As Double)
    Let mTotalCost = Value
End Property

'##ModelId=3DF8BD55006E
Public Property Get TotalValue() As Double
   Let TotalValue = mTotalValue
End Property

'##ModelId=3DF8BD54030C
Public Property Let TotalValue(ByVal Value As Double)
    Let mTotalValue = Value
End Property

'##ModelId=3DF8BD540230
Public Property Get TotalQuantity() As Long
   Let TotalQuantity = mTotalQuantity
End Property

'##ModelId=3DF8BD5400E6
Public Property Let TotalQuantity(ByVal Value As Long)
    Let mTotalQuantity = Value
End Property

'##ModelId=3DF8BD54003C
Public Property Get ReceiptDate() As Date
   Let ReceiptDate = mReceiptDate
End Property

'##ModelId=3DF8BD53023A
Public Property Get ReceivedDate() As Date
   Let ReceivedDate = mReceivedDate
End Property

'##ModelId=3DF8BD5300F0
Public Property Let ReceivedDate(ByVal Value As Date)
    Let mReceivedDate = Value
End Property

'##ModelId=3DF8BD530050
Public Property Get SentDate() As Date
   Let SentDate = mSentDate
End Property

'##ModelId=3DF8BD520320
Public Property Let SentDate(ByVal Value As Date)
    Let mSentDate = Value
End Property

'##ModelId=3DF8BD520244
Public Property Get ISTOutNumber() As String
   Let ISTOutNumber = mISTOutNumber
End Property

'##ModelId=3DF8BD520168
Public Property Let ISTOutNumber(ByVal Value As String)
    Let mISTOutNumber = Value
End Property

'##ModelId=3DF8BD5200C8
Public Property Get SourceStoreNumber() As String
   Let SourceStoreNumber = mSourceStoreNumber
End Property


'##ModelId=3DF8BD510398
Public Property Let SourceStoreNumber(ByVal Value As String)
    Let mSourceStoreNumber = Value
End Property

Public Sub RecordReceived(ISTInDocNumber As String, ISTReceiptDate As Date, ISTQuantityReceived As Double, ISTReceivedBy As String)

Dim lngLineNo   As Long

    mISTInNumber = ISTInDocNumber
    mReceiptDate = ISTReceiptDate
    mISTInQuantity = CLng(ISTQuantityReceived)
    mReceivedBy = ISTReceivedBy
    mReceived = True
    Call IBo_SaveIfExists
    For lngLineNo = 1 To mcolLines.Count Step 1
        Call mcolLines(CLng(lngLineNo)).IBo_SaveIfExists
    Next lngLineNo
    
    
End Sub
Public Sub RecordItemReceived(PartCode As String, LineNumber As String, ISTQuantityReceived As Double)

Dim oLine       As cPurchase_Wickes.cISTInEDILine
Dim lngLineNo   As Long

    For lngLineNo = 1 To mcolLines.Count Step 1
        If (mcolLines(CLng(lngLineNo)).PartCode = PartCode) And (Val(mcolLines(CLng(lngLineNo)).LineNumber) = LineNumber) Then
            Set oLine = mcolLines(CLng(lngLineNo))
            Exit For
        End If
    Next lngLineNo
    If Not oLine Is Nothing Then oLine.QuantityReceived = CLng(ISTQuantityReceived)

End Sub
Public Function AddItem(PartCode As String) As cISTInEDILine

Dim oLine As cPurchase_Wickes.cISTInEDILine

    Set oLine = m_oSession.Database.CreateBusinessObject(CLASSID_ISTINEDILINE)
    Set oLine.Header = Me
    oLine.PartCode = PartCode
    oLine.SourceStoreNumber = mSourceStoreNumber
    oLine.ISTOutNumber = mISTOutNumber
    
    If mcolLines Is Nothing Then Set mcolLines = New Collection
    Call mcolLines.Add(oLine)
    oLine.LineNumber = mcolLines.Count
    Set AddItem = oLine

End Function

Private Sub RetrieveLines()

Dim oLine As cPurchase_Wickes.cISTInEDILine

    'If Order found then get lines for Order
    Set mcolLines = New Collection
    Set oLine = m_oSession.Database.CreateBusinessObject(CLASSID_ISTINEDILINE)
    Call oLine.IBo_AddLoadFilter(CMP_EQUAL, FID_ISTINEDILINE_SourceStoreNumber, mSourceStoreNumber)
    Call oLine.IBo_AddLoadFilter(CMP_EQUAL, FID_ISTINEDILINE_ISTOutNumber, mISTOutNumber)
    Set mcolLines = oLine.IBo_LoadMatches

End Sub
Public Function Lines() As Collection

    If (mcolLines Is Nothing) And (mGetLines = True) Then Call RetrieveLines
    Set Lines = mcolLines
    
End Function

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
Dim lngLineNo As Long
    
    Save = False
    
    If IsValid() Then
        If eSave = SaveTypeIfNew Then mReceivedDate = Now()
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
        'Save each line for the Delivery Note
        If mcolLines Is Nothing = False Then
            For lngLineNo = 1 To mcolLines.Count Step 1
                Call mcolLines(CLng(lngLineNo)).IBo_SaveIfNew
            Next lngLineNo
        End If
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_ISTINEDIHEADER * &H10000) + 1 To FID_ISTINEDIHEADER_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cISTInEDIHeader

End Function


Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_ISTINEDIHEADER, FID_ISTINEDIHEADER_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_ISTINEDIHEADER_SourceStoreNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSourceStoreNumber
        Case (FID_ISTINEDIHEADER_ISTOutNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mISTOutNumber
        Case (FID_ISTINEDIHEADER_SentDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mSentDate
        Case (FID_ISTINEDIHEADER_ReceivedDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mReceivedDate
        Case (FID_ISTINEDIHEADER_ReceiptDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mReceiptDate
        Case (FID_ISTINEDIHEADER_TotalQuantity):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mTotalQuantity
        Case (FID_ISTINEDIHEADER_TotalValue):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mTotalValue
        Case (FID_ISTINEDIHEADER_TotalCost):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mTotalCost
        Case (FID_ISTINEDIHEADER_Received):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mReceived
        Case (FID_ISTINEDIHEADER_Cancelled):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mCancelled
        Case (FID_ISTINEDIHEADER_ReceivedBy):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mReceivedBy
        Case (FID_ISTINEDIHEADER_ISTInQuantity):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mISTInQuantity
        Case (FID_ISTINEDIHEADER_ISTOutCancelled):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mISTOutCancelled
        Case (FID_ISTINEDIHEADER_ISTInNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mISTInNumber
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cISTInEDIHeader
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_ISTINEDIHEADER

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "IST In EDI Header " & mSourceStoreNumber & "-" & mISTOutNumber

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_ISTINEDIHEADER_SourceStoreNumber): mSourceStoreNumber = oField.ValueAsVariant
            Case (FID_ISTINEDIHEADER_ISTOutNumber):      mISTOutNumber = oField.ValueAsVariant
            Case (FID_ISTINEDIHEADER_SentDate):          mSentDate = oField.ValueAsVariant
            Case (FID_ISTINEDIHEADER_ReceivedDate):      mReceivedDate = oField.ValueAsVariant
            Case (FID_ISTINEDIHEADER_ReceiptDate):       mReceiptDate = oField.ValueAsVariant
            Case (FID_ISTINEDIHEADER_TotalQuantity):     mTotalQuantity = oField.ValueAsVariant
            Case (FID_ISTINEDIHEADER_TotalValue):        mTotalValue = oField.ValueAsVariant
            Case (FID_ISTINEDIHEADER_TotalCost):         mTotalCost = oField.ValueAsVariant
            Case (FID_ISTINEDIHEADER_Received):          mReceived = oField.ValueAsVariant
            Case (FID_ISTINEDIHEADER_Cancelled):         mCancelled = oField.ValueAsVariant
            Case (FID_ISTINEDIHEADER_ReceivedBy):        mReceivedBy = oField.ValueAsVariant
            Case (FID_ISTINEDIHEADER_ISTInQuantity):     mISTInQuantity = oField.ValueAsVariant
            Case (FID_ISTINEDIHEADER_ISTOutCancelled):   mISTOutCancelled = oField.ValueAsVariant
            Case (FID_ISTINEDIHEADER_ISTInNumber):       mISTInNumber = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As cISTInEDIHeader

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set Interface = oBO
        Case Else:
            Set Interface = Me
    End Select

End Function
Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_ISTINEDIHEADER_END_OF_STATIC

End Function

