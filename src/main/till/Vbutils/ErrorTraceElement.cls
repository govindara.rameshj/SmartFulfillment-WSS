VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CErrorTraceElement"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'<CAMH>****************************************************************************************
'* Module: CErrorTraceElement
'* Date  : 05/08/02
'* Author: mauricem
'**********************************************************************************************
'* Summary:
'   An object that has the same properties (but not methods) as the standard error object
'   in addition to extra properties useful to us for error stack tracing. It is intended
'   that this type of object only ever be created by the Add method of the CErrorTrace
'   collection object. This object really only has meaning as an element of an error trace.
'**********************************************************************************************
'* Versions:
'* 05/08/02    various
'*             Header added.
'**********************************************************************************************
Option Explicit

Const MODULE_NAME As String = "CErrorTraceElement"

'Private variables used to hold the values of our public properties that mimic the standard
'VB error object.
Private mvarNumber        As Long
Private mvarDescription   As String
Private mvarSource        As String
Private mvarHelpContext   As Long
Private mvarHelpFile      As String
Private mvarLastDLLError  As Long

'Private variables used to hold our extended properties dealing with error location.
Private mvarLineNumber    As Long
Private mvarModuleName    As String
Private mvarProcedureName As String

Public Property Let Number(ByVal lNumber As Long)

    'Store the error number supplied.
    mvarNumber = lNumber
    
End Property

Public Property Get Number() As Long

    'Retrieve the error number that we stored for ourselves.
    Number = mvarNumber
    
End Property

Public Property Let Description(ByVal sDescription As String)

    'Store the error description supplied.
    mvarDescription = sDescription
    
End Property

Public Property Get Description() As String

    'Retrieve the error description that we stored for ourselves.
    Description = mvarDescription
    
End Property

Public Property Let Source(ByVal sSource As String)

    'Store the error source supplied.
    mvarSource = sSource
    
End Property

Public Property Get Source() As String

    'Retrieve the error source that we stored for ourselves.
    Source = mvarSource
    
End Property

Public Property Let HelpFile(ByVal sHelpFile As String)

    'Store the Help file supplied.
    mvarHelpFile = sHelpFile
    
End Property

Public Property Get HelpFile() As String

    'Retrieve the Help file that we stored for ourselves.
    HelpFile = mvarHelpFile
    
End Property

Public Property Let HelpContext(ByVal lHelpContext As Long)

    'Store the Help file context ID supplied.
    mvarHelpContext = lHelpContext
    
End Property

Public Property Get HelpContext() As Long

    'Retrieve the Help file context ID that we stored for ourselves.
    HelpContext = mvarHelpContext
    
End Property

Public Property Let LineNumber(ByVal lLineNumber As Long)

    'Store the line number supplied.
    mvarLineNumber = lLineNumber
    
End Property

Public Property Get LineNumber() As Long

    'Retrieve the line number that we stored for ourselves.
    LineNumber = mvarLineNumber
    
End Property

Public Property Let ModuleName(ByVal sModuleName As String)

    'Store the Module name supplied.
    mvarModuleName = sModuleName
    
End Property

Public Property Get ModuleName() As String

    'Retrieve the Module name that we stored for ourselves.
    ModuleName = mvarModuleName
    
End Property

Public Property Let ProcedureName(ByVal sProcedureName As String)

    'Store the Procedure name supplied.
    mvarProcedureName = sProcedureName
    
End Property

Public Property Get ProcedureName() As String

    'Retrieve the Procedure name that we stored for ourselves.
    ProcedureName = mvarProcedureName
    
End Property

Public Property Let LastDLLError(ByVal lLastDllError As Long)

    'Store the Last DLL error number supplied.
    mvarLastDLLError = lLastDllError
    
End Property

Public Property Get LastDLLError() As Long

    'Retrieve the Last DLL error number that we stored for ourselves.
    LastDLLError = mvarLastDLLError
    
End Property

