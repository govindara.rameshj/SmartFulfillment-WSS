VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "RegValue"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"No"
Option Explicit

Public Key As String
Attribute Key.VB_VarDescription = "The name of the item in the registry."

Public Enum DataTypes
    dtString = 1
    dtLong = 2
    dtByteArray = 3
End Enum

Private mvarValue As Variant 'local copy
Private mvarDataType As DataTypes 'local copy
Friend Property Let DataType(ByVal vData As DataTypes)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DataType = 5
    mvarDataType = vData
End Property


Public Property Get DataType() As DataTypes
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DataType
    DataType = mvarDataType
End Property


Friend Property Let Value(ByVal vData As Variant)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Value = 5
    mvarValue = vData
End Property


Public Property Get Value() As Variant
Attribute Value.VB_Description = "The value of the item in the registry."
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Value
    Value = mvarValue
End Property



