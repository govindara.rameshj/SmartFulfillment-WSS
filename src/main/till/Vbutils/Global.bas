Attribute VB_Name = "modGlobal"
'<CAMH>****************************************************************************************
'* Module: modGlobal
'* Date  : 05/08/02
'* Author: various
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* Versions:
'* 05/08/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "modGlobal"

' API declares
Public Declare Function GetSystemDirectory Lib "kernel32" Alias "GetSystemDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long
Public Declare Function GetWindowsDirectory Lib "kernel32" Alias "GetWindowsDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long

Public Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" ( _
                            lpVersionInformation As OSVERSIONINFO) As Long

Public Declare Function RegisterEventSource Lib "advapi32.dll" Alias "RegisterEventSourceA" ( _
                            ByVal lpUNCServerName As String, _
                            ByVal lpSourceName As String) As Long
                            
Public Declare Function ReportEvent Lib "advapi32.dll" Alias "ReportEventA" ( _
                            ByVal hEventLog As Long, _
                            ByVal wType As Long, _
                            ByVal wCategory As Long, _
                            ByVal dwEventID As Long, _
                            ByVal lpUserSid As Any, _
                            ByVal wNumStrings As Long, _
                            ByVal dwDataSize As Long, _
                            lpStrings As String, _
                            ByVal lpRawData As Any) As Long
                            
Public Declare Function DeregisterEventSource Lib "advapi32.dll" (ByVal hEventLog As Long) As Long

Public Declare Function RegCreateKey Lib "advapi32.dll" Alias "RegCreateKeyA" ( _
                            ByVal hKey As Long, _
                            ByVal lpSubKey As String, _
                            phkResult As Long) As Long
                                
Public Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long

Public Declare Function RegConnectRegistry Lib "advapi32.dll" Alias "RegConnectRegistryA" ( _
                            ByVal lpMachineName As String, _
                            ByVal hKey As Long, _
                            phkResult As Long) As Long

Public Declare Function RegCreateKeyEx Lib "advapi32.dll" Alias "RegCreateKeyExA" ( _
                            ByVal hKey As Long, _
                            ByVal lpSubKey As String, _
                            ByVal Reserved As Long, _
                            ByVal lpClass As String, _
                            ByVal dwOptions As Long, _
                            ByVal samDesired As Long, _
                            ByVal lpSecurityAttributes As String, _
                            phkResult As Long, _
                            lpdwDisposition As Long) As Long
                                
Public Declare Function RegDeleteValue Lib "advapi32.dll" Alias "RegDeleteValueA" ( _
                            ByVal hKey As Long, _
                            ByVal lpValueName As String) As Long
                                
Public Declare Function RegDeleteKey Lib "advapi32.dll" Alias "RegDeleteKeyA" ( _
                            ByVal hKey As Long, _
                            ByVal lpSubKey As String) As Long
                                
Public Declare Function RegEnumKeyEx Lib "advapi32.dll" Alias "RegEnumKeyExA" ( _
                            ByVal hKey As Long, _
                            ByVal dwIndex As Long, _
                            ByVal lpName As String, _
                            lpcbName As Long, _
                            ByVal lpReserved As Long, _
                            ByVal lpClass As String, _
                            lpcbClass As Long, _
                            lpftLastWriteTime As Any) As Long
                                
Public Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" ( _
                            ByVal hKey As Long, _
                            ByVal lpSubKey As String, _
                            ByVal ulOptions As Long, _
                            ByVal samDesired As Long, _
                            phkResult As Long) As Long
                                
Public Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" ( _
                            ByVal hKey As Long, _
                            ByVal lpValueName As String, _
                            ByVal lpReserved As Long, _
                            lpType As Long, _
                            lpData As Any, _
                            lpcbData As Long) As Long
                                
Public Declare Function RegSetValueEx Lib "advapi32.dll" Alias "RegSetValueExA" ( _
                            ByVal hKey As Long, _
                            ByVal lpValueName As String, _
                            ByVal Reserved As Long, _
                            ByVal dwType As Long, _
                            lpData As Any, _
                            ByVal cbData As Long) As Long

Public Declare Function RegQueryInfoKey Lib "advapi32.dll" Alias "RegQueryInfoKeyA" ( _
                            ByVal hKey As Long, _
                            ByVal lpClass As String, _
                            lpcbClass As Long, _
                            ByVal lpReserved As Long, _
                            lpcSubKeys As Long, _
                            lpcbMaxSubKeyLen As Long, _
                            lpcbMaxClassLen As Long, _
                            lpcValues As Long, _
                            lpcbMaxValueNameLen As Long, _
                            lpcbMaxValueLen As Long, _
                            lpcbSecurityDescriptor As Long, _
                            lpftLastWriteTime As Any) As Long
                                
Public Declare Function RegEnumValue Lib "advapi32.dll" Alias "RegEnumValueA" ( _
                            ByVal hKey As Long, _
                            ByVal dwIndex As Long, _
                            ByVal lpValueName As String, _
                            lpcbValueName As Long, _
                            ByVal lpReserved As Long, _
                            lpType As Long, _
                            lpData As Byte, _
                            lpcbData As Long) As Long

Public Declare Function ExpandEnvironmentStrings Lib "kernel32" Alias "ExpandEnvironmentStringsA" ( _
                            ByVal lpSrc As String, _
                            ByVal lpDst As String, _
                            ByVal nSize As Long) As Long

Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" ( _
                            Destination As Any, _
                            Source As Any, _
                            ByVal Length As Long)

Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Public Declare Function SHGetPathFromIDList Lib "shell32.dll" Alias "SHGetPathFromIDListA" ( _
                            ByVal pidl As Long, ByVal pszPath As String) As Long
                                
Public Declare Function SHBrowseForFolder Lib "shell32.dll" Alias "SHBrowseForFolderA" ( _
                            lpBrowseInfo As BrowseInfo) As Long
                                
Public Declare Function SHGetSpecialFolderLocation Lib "shell32" ( _
                            ByVal hWndOwner As Long, _
                            ByVal nFolder As SPECIAL_FOLDER_IDS, _
                            ByRef pidl As Long) As Long
                                
Public Declare Function SHGetDesktopFolder Lib "shell32" ( _
                            ByRef pshf As IVBShellFolder) As Long
                                
Public Declare Function SHGetMalloc Lib "shell32" (ByRef pMalloc As IVBMalloc) As Long

Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" ( _
                            ByVal hWnd As Long, ByVal wMsg As Long, _
                            ByVal wParam As Long, ByVal lParam As String) As Long
                            
Public Declare Function lstrcat Lib "kernel32" Alias "lstrcatA" ( _
                            ByVal lpString1 As String, ByVal lpString2 As String) As Long

Public Declare Sub OutputDebugString Lib "kernel32" Alias "OutputDebugStringA" ( _
                            ByVal lpOutputString As String)
' Constants
Public Const BIF_STATUSTEXT         As Long = &H4&
Public Const BIF_RETURNONLYFSDIRS   As Long = 1
Public Const BIF_DONTGOBELOWDOMAIN  As Long = 2
Public Const MAX_PATH               As Long = 260

Public Const WM_USER                As Long = &H400
Public Const BFFM_INITIALIZED       As Long = 1
Public Const BFFM_SELCHANGED        As Long = 2
Public Const BFFM_SETSTATUSTEXT     As Long = (WM_USER + 100)
Public Const BFFM_SETSELECTION      As Long = (WM_USER + 102)

Public Const REG_OPTION_NON_VOLATILE = 0       ' Key is preserved when system is rebooted

Public Const KEY_QUERY_SETTING = &H1
Public Const KEY_QUERY_VALUE = &H1
Public Const KEY_SET_SETTING = &H2
Public Const KEY_SET_VALUE = &H2
Public Const KEY_CREATE_SUB_KEY = &H4
Public Const KEY_ENUMERATE_SUB_KEYS = &H8
Public Const KEY_NOTIFY = &H10
Public Const KEY_CREATE_LINK = &H20
Public Const STANDARD_RIGHTS_ALL = &H1F0000
Public Const SYNCHRONIZE = &H100000
Public Const KEY_ALL_ACCESS = ((STANDARD_RIGHTS_ALL Or KEY_QUERY_SETTING Or KEY_SET_SETTING Or KEY_CREATE_SUB_KEY Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY Or KEY_CREATE_LINK) And (Not SYNCHRONIZE))
Public Const READ_CONTROL = &H20000
Public Const STANDARD_RIGHTS_READ = (READ_CONTROL)
Public Const KEY_READ = ((STANDARD_RIGHTS_READ Or KEY_QUERY_SETTING Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY) And (Not SYNCHRONIZE))

Public Const DELETE = &H10000

Public Const REG_SZ = 1                         ' Unicode nul terminated string
Public Const REG_EXPAND_SZ = 2                  ' Unicode nul terminated string
Public Const REG_BINARY = 3                     ' Free form binary
Public Const REG_DWORD = 4                      ' 32-bit number

Public Const ERROR_FILE_NOT_FOUND = 2&
Public Const ERROR_NO_MORE_ITEMS = 259&

Public Const OFFSET_4 = 4294967296#
Public Const MAXINT_4 = 2147483647
Public Const OFFSET_2 = 65536
Public Const MAXINT_2 = 32767

Public Const EVENT_LOG = "System\CurrentControlSet\Services\EventLog\Application"

Public Const SINGLE_QUOTE = "'"

'String constant used to delimit elements of the error stack when they are packed into the
'error source during a Detach method call. This string is also used to recognise a packed
'Source property.
Public Const STACK_ELEMENT_DELIMETER As String = "<@>"

'Name used to identify the component in debug and trace messages.
Public Const APP_NAME As String = "vbUtils"

' User defined types

Public Type BrowseInfo          'Browse Dialog
   hOwner         As Long
   pidlRoot       As Long
   pszDisplayName As String
   lpszTitle      As Long
   ulFlags        As Long
   lpcbCallback   As Long
   lParam         As Long
   iImage         As Long
End Type

Public Type OSVERSIONINFO
    dwOSVersionInfoSize As Long
    dwMajorVersion As Long
    dwMinorVersion As Long
    dwBuildNumber As Long
    dwPlatformId As Long
    szCSDVersion As String * 128      '  Maintenance string for PSS usage
End Type

Public Enum SPECIAL_FOLDER_IDS
    sfidDESKTOP = &H0
    sfidPROGRAMS = &H2
    sfidPERSONAL = &H5
    sfidFAVORITES = &H6
    sfidSTARTUP = &H7
    sfidRECENT = &H8
    sfidSENDTO = &H9
    sfidSTARTMENU = &HB
    sfidDESKTOPDIRECTORY = &H10
    sfidNETHOOD = &H13
    sfidFONTS = &H14
    sfidTEMPLATES = &H15
    sfidCOMMON_STARTMENU = &H16
    sfidCOMMON_PROGRAMS = &H17
    sfidCOMMON_STARTUP = &H18
    sfidCOMMON_DESKTOPDIRECTORY = &H19
    sfidAPPDATA = &H1A
    sfidPRINTHOOD = &H1B
    sfidProgramFiles = &H10000
    sfidCommonFiles = &H10001
End Enum

' Enums

Public Enum eResfileStrings
    rfsError = 1
    rfsWarning = 2
    rfsInformation = 4
    rfsGeneralError = 100
    rfsRegAccessError = 101
    rfsRegisterEventSourceError = 102
End Enum
