VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "RegSettings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"RegValue"
Attribute VB_Ext_KEY = "Member0" ,"RegValue"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable to hold collection
Private mCol As Collection

'Used in debug and trace calls.
Private Const MODULE_NAME As String = "RegSettings"

'A reference to an instance of vbExt used by trace and debug calls.
Private m_oVbExt As vbExt

Public Function Add(Key As String, DataType As SettingTypes, Optional Value As Variant) As RegSetting

Dim objNewMember As RegSetting
Dim sErrSource   As String

Const PROCEDURE_NAME As String = MODULE_NAME & ".Add"

    On Error GoTo GeneralErrorHandler
    
    Call m_oVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn)

   
    'create a new object
    Set objNewMember = New RegSetting

    'set the properties passed into the method
    objNewMember.Key = Key
    objNewMember.Value = Value
    objNewMember.DataType = DataType
    
    mCol.Add objNewMember, Key

    'return the object created
    Set Add = objNewMember
    Set objNewMember = Nothing


    Call m_oVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceOut)

    Exit Function
    
GeneralErrorHandler:

    If Err.Source = App.Title Then
        sErrSource = STACK_ELEMENT_DELIMETER & PROCEDURE_NAME
    ElseIf (InStr(Err.Source, STACK_ELEMENT_DELIMETER) = 1) Then
        'Err.Source already starts with a stack delimiter
        sErrSource = STACK_ELEMENT_DELIMETER & PROCEDURE_NAME & Err.Source
    Else
        'Prefix Err.source with a stack delimiter
        sErrSource = STACK_ELEMENT_DELIMETER & PROCEDURE_NAME & STACK_ELEMENT_DELIMETER & Err.Source
    End If

    Err.Raise Err.Number, sErrSource, Err.Description

End Function

Public Property Get Item(vntIndexKey As Variant) As RegSetting
Attribute Item.VB_UserMemId = 0
    'used when referencing an element in the collection
    'vntIndexKey contains either the Index or Key to the collection,
    'this is why it is declared as a Variant
    'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
  Set Item = mCol(vntIndexKey)
End Property



Public Property Get Count() As Long
    'used when retrieving the number of elements in the
    'collection. Syntax: Debug.Print x.Count
    Count = mCol.Count
End Property


Public Sub Remove(vntIndexKey As Variant)
    'used when removing an element from the collection
    'vntIndexKey contains either the Index or Key, which is why
    'it is declared as a Variant
    'Syntax: x.Remove(xyz)

Const PROCEDURE_NAME As String = MODULE_NAME & ".Remove"

    Call m_oVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn)


    mCol.Remove vntIndexKey
    
    
    Call m_oVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceOut)

End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()

Dim sErrSource As String

Const PROCEDURE_NAME As String = MODULE_NAME & ".Class_Initialize"
   
    On Error GoTo GeneralErrorHandler

    Call m_oVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn)

    
    'creates the collection when this class is created
    Set mCol = New Collection
    

    Call m_oVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceOut)

    Exit Sub
    
GeneralErrorHandler:

    If Err.Source = App.Title Then
        sErrSource = STACK_ELEMENT_DELIMETER & PROCEDURE_NAME
    ElseIf (InStr(Err.Source, STACK_ELEMENT_DELIMETER) = 1) Then
        'Err.Source already starts with a stack delimiter
        sErrSource = STACK_ELEMENT_DELIMETER & PROCEDURE_NAME & Err.Source
    Else
        'Prefix Err.source with a stack delimiter
        sErrSource = STACK_ELEMENT_DELIMETER & PROCEDURE_NAME & STACK_ELEMENT_DELIMETER & Err.Source
    End If

    Err.Raise Err.Number, sErrSource, Err.Description

End Sub


Private Sub Class_Terminate()

Dim sErrSource As String

Const PROCEDURE_NAME As String = MODULE_NAME & ".Class_Terminate"

    On Error GoTo GeneralErrorHandler
    
    Call m_oVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn)

    
    'destroys collection when this class is terminated
    Set mCol = Nothing
    

    Call m_oVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceOut)

    Exit Sub
    
GeneralErrorHandler:

    If Err.Source = App.Title Then
        sErrSource = STACK_ELEMENT_DELIMETER & PROCEDURE_NAME
    ElseIf (InStr(Err.Source, STACK_ELEMENT_DELIMETER) = 1) Then
        'Err.Source already starts with a stack delimiter
        sErrSource = STACK_ELEMENT_DELIMETER & PROCEDURE_NAME & Err.Source
    Else
        'Prefix Err.source with a stack delimiter
        sErrSource = STACK_ELEMENT_DELIMETER & PROCEDURE_NAME & STACK_ELEMENT_DELIMETER & Err.Source
    End If

    Err.Raise Err.Number, sErrSource, Err.Description

End Sub

