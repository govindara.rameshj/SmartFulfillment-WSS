VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ErrorObject"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Description = "This object acts as a replacement for the standard VB error object. It contains the same properties and methods as the standard error object in addition to several methods and properties designed to supplement the information provided by the standard object. All the standard properties ""inherit"" from the error object reference passed to us from the initiating application.\r\nIt will create and maintain a trace of procedures activated from the point of the error to the point where it is finally reported. It will maintain this trace across process boundaries. It will log this information either in a log file or an event log if the operating system that the application/component is running on supports it."
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'<CAMH>****************************************************************************************
'* Module: ErrorObject
'* Date  : 05/08/02
'* Author: Various
'**********************************************************************************************
'* Summary:
'*   This object acts as a replacement for the standard VB error object. It contains
'*   the same properties and methods as the standard error object in addition to several
'*   methods and properties designed to supplement the information provided by the
'*   standard object. All the standard properties "inherit" from the error object reference
'*   passed to us from the initiating application.
'*   It will create and maintain a trace of procedures activated from the point of the
'*   error to the point where it is finally reported. It will maintain this trace across
'*   process boundaries. It will log this information either in a log file or an event
'*   log if the operating system that the application/component is running on supports it.
'**********************************************************************************************
'* Versions:
'* 05/08/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

'Collection class holding the current stack of error traces.
Private moErrorTrace As CErrorTrace

'Our connection to the standard error object active in the application or component to
'to which error handling has been added.
Private moHostVBAErr As VBA.ErrObject

'The number of references held to our Error object
Private mlRefCount As Long

'A reference to an instance of vbExt - created in the Register method
Private moHostVbExt As vbExt

'Temporary storage for the current error (used by the Push and Pop methods)
Private moSavedErr As CErrorTrace

Private Const MODULE_NAME As String = "ErrorObject"

Friend Property Set HostVbExt(ByVal vData As vbExt)
Attribute HostVbExt.VB_Description = "Reference to the instance of VbExtend.vbExt that created this class.\r\nNeeded so that the logging will work correctly."

'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.HostVbExt = Form1
    Set moHostVbExt = vData

End Property

Friend Property Set HostVBAErr(ByRef oHostVBAErr As VBA.ErrObject)

    'Link to standard VBA error object.
    Set moHostVBAErr = oHostVBAErr

End Property

Friend Property Get HostVBAErr() As VBA.ErrObject

    'Link to standard VBA error object.
    Set HostVBAErr = moHostVBAErr

End Property

'<CACH>****************************************************************************************
'* Property Get:  Long RefCount()
'**********************************************************************************************
'* Description: returns number of objects
'**********************************************************************************************
'* Parameters:
'* None.
'**********************************************************************************************
'* Returns:  Long
'**********************************************************************************************
'* History:
'* 05/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Property Get RefCount() As Long
Attribute RefCount.VB_Description = "The number of objects that are attached to this instance of the ErrorObject."

Const PROCEDURE_NAME As String = MODULE_NAME & ".RefCount_Get"
    
    RefCount = mlRefCount

End Property 'RefCount

Public Property Get LastDLLError() As Long
Attribute LastDLLError.VB_Description = "Returns system error code from a call to a DLL."

    'Hand out the current LastDLLError number.
    LastDLLError = moHostVBAErr.LastDLLError

End Property

Public Property Get Number() As Long
Attribute Number.VB_Description = "Returns or sets a numeric value representing an error."
    
    'Hand out the current error number.
    Number = moHostVBAErr.Number

End Property

Public Property Let Number(ByVal lNumber As Long)
    
    'Set the error number.
    moHostVBAErr.Number = lNumber

End Property

Public Property Get Description() As String
Attribute Description.VB_Description = "Returns or sets the string expression containing a descriptive string associated with an object."

    'Hand out the current error description.
    Description = moHostVBAErr.Description
    
End Property

Public Property Let Description(ByVal sDescription As String)

    'Set the error description.
    moHostVBAErr.Description = sDescription

End Property

Public Property Get Source() As String
Attribute Source.VB_Description = "Returns or sets the name of the object that originated the error."
    
    'Hand out the current error source.
    Source = moHostVBAErr.Source

End Property

Public Property Let Source(ByVal sSource As String)
    
    'Set the error source.
    moHostVBAErr.Source = sSource

End Property

Public Property Get HelpFile() As String
Attribute HelpFile.VB_Description = "Returns or sets a fully qualified path to a Help file."
    
    'Hand out the current error help file.
    HelpFile = moHostVBAErr.HelpFile

End Property

Public Property Let HelpFile(ByVal sHelpFile As String)
    
    'Set the error help file.
    moHostVBAErr.HelpFile = sHelpFile

End Property

Public Property Get HelpContext() As Long
Attribute HelpContext.VB_Description = "Returns or sets a context ID for a topic in a Help file."

    'Hand out the current error help context id.
    HelpContext = moHostVBAErr.HelpContext

End Property

Public Property Let HelpContext(ByVal lHelpContext As Long)
    
    'Set the error help context id.
    moHostVBAErr.HelpContext = lHelpContext

End Property

'<Auto-Procedure-Header>=======================================================================
' Sub:  AddRef()
'
' Description:  Increment the number of objects that are attached to this object.
'               This method should be called when attaching an object to an already instantiated
'               ErrorObject.
'
'----------------------------------------------------------------------------------------------
' Arguments (Name, Type, Description):
' None.
'----------------------------------------------------------------------------------------------
' Version History (When, Who, What):
' 07/03/2001    davem       Created.
'
'</Auto-Procedure-Header>======================================================================
Friend Sub AddRef()
Attribute AddRef.VB_Description = "Increment the number of objects that are attached to this object. This method should be called when attaching an object to an already instantiated ErrorObject."

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddRef"

    Call moHostVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn)


    mlRefCount = mlRefCount + 1


End Sub
'<Auto-Procedure-Header>=======================================================================
' Sub:  Push()
'
' Description:
'
'----------------------------------------------------------------------------------------------
' Arguments (Name, Type, Description):
' None.
'----------------------------------------------------------------------------------------------
' Version History (When, Who, What):
' 07/06/2001    davem
'               Header inserted.
' 08/06/2001    davem
'               Only pack the stack if it isn't already packed.
' 22/06/2001    rogers
'               Moved creation of moSavedError collection to the Initialisation procedure
'               for this class. Was causing VB error object to be cleared.
'</Auto-Procedure-Header>======================================================================
Public Sub Push()
Attribute Push.VB_Description = "Place the current error onto the stack used to preserve error details whilst error processing is underway."
Dim sSource As String

Const PROCEDURE_NAME As String = MODULE_NAME & ".Push"

    Call moHostVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn)

    'Create the collection is it doesn't already exist.
    'Collection now created when class is initialised (because it initialises a vbExt
    'object for its own use that clears the VB error object).
    ''If moSavedErr Is Nothing Then
    ''    Set moSavedErr = New CErrorTrace
    ''End If
    
    'Add the current ErrorObject values to the collection
    With Me
        'If the current source doesn't contain a packed stack then pack the stack.
        If Not .StackDetached Then
            sSource = PackStack(.Source)
        Else
            sSource = .Source
        End If
        
        moSavedErr.Add .Number, sSource, .Description, .HelpContext, .HelpFile, _
                       .LastDLLError, vbNullString, vbNullString, 0
    End With
    
End Sub
'<Auto-Procedure-Header>=======================================================================
' Sub:  Pop()
'
' Description:
'
'----------------------------------------------------------------------------------------------
' Arguments (Name, Type, Description):
' None.
'----------------------------------------------------------------------------------------------
' Version History (When, Who, What):
' 07/06/2001    davem
'               Header inserted.
'</Auto-Procedure-Header>======================================================================
Public Sub Pop()
Attribute Pop.VB_Description = "Extract the first item from the error stack created when Err.Push is called."
Dim oErrorElement   As CErrorTraceElement

Const PROCEDURE_NAME As String = MODULE_NAME & ".Pop"

    Call moHostVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn)

    'Make sure that we have something on the stack
    If (Not moSavedErr Is Nothing) Then
        If (moSavedErr.Count > 0) Then

        'Get the error values off the stack
        Set oErrorElement = moSavedErr.Item(1)
        
        'Remove the error values from the stack
        Call moSavedErr.Remove(1)
        
        With Me
            'Populate the error properties
            .Description = oErrorElement.Description
            .HelpContext = oErrorElement.HelpContext
            .HelpFile = oErrorElement.HelpFile
            .Number = oErrorElement.Number
            .Source = oErrorElement.Source
            
            'Clear the error stack now that it has been used for reporting.
            moErrorTrace.Clear
            Call AttachStack(.Source)
        End With
        End If
    End If

End Sub
'<Auto-Procedure-Header>=======================================================================
' Function:  Long Release()
'
' Description:  Decrement the count of objects attached to this object (mlRefCount)
'
'----------------------------------------------------------------------------------------------
' Arguments (Name, Type, Description):
' None.
'----------------------------------------------------------------------------------------------
' Returns:  Long    - the number of objects that are still attached to this object
'----------------------------------------------------------------------------------------------
' Version History (When, Who, What):
' 07/03/2001    davem       Created.
'
'</Auto-Procedure-Header>======================================================================
Public Function Release() As Long
Attribute Release.VB_Description = "Decrement the count of objects attached to this instance of the ErrorObject.\r\nReturns number of objects that are still attached to this instance of the ErrorObject.\r\nIf the return count is 0 then the calling application should destroy the ErrorObject."

Const PROCEDURE_NAME As String = MODULE_NAME & ".Release"

    Call moHostVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn)


    mlRefCount = mlRefCount - 1
    Release = mlRefCount
    

End Function
'<Auto-Procedure-Header>=======================================================================
' Sub:  DecodeModuleName()
'
' Description:  Check the sModuleName and sProcedureName arguments for an empty argument and
'               one containing a concatenated name in the format <module name>.<procedure name>
'               If a concatenated name is found then split it into sModuleName and
'               sProcedureName.
'----------------------------------------------------------------------------------------------
' Arguments (Name, Type, Description):
' In:  sModuleName  String.
'      sProcedureName   String.
'
' Out: sModuleName  String.
'      sProcedureName   String.
'----------------------------------------------------------------------------------------------
' Version History (When, Who, What):
' 08/06/2001    davem
'               Header inserted.
'</Auto-Procedure-Header>======================================================================
Private Sub DecodeModuleName(ByRef sModuleName As String, ByRef sProcedureName As String)
Dim lPos    As Long
Dim sTemp   As String

Const PROCEDURE_NAME As String = MODULE_NAME & ".DecodeModuleName"

    Call moHostVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn)


    If Len(sModuleName) = 0 Then
        'Module name empty so check the procedure name argument
        sTemp = sProcedureName
    ElseIf Len(sProcedureName) = 0 Then
        'Procedure name empty so check the module name argument
        sTemp = sModuleName
    End If
    
    If Len(sTemp) > 0 Then
        'We have an argument to check
        lPos = InStr(1, sTemp, ".", vbBinaryCompare)
        If lPos > 0 Then
            'It contains a concatenated name so split it up
            sModuleName = Left$(sTemp, lPos - 1)
            sProcedureName = Mid$(sTemp, lPos + 1)
        End If
    End If
        

End Sub 'DecodeModuleName
'<Auto-Procedure-Header>=======================================================================
' Sub:  Bubble()
'
' Description:
'   Bubble the current error up the procedure chain towards a handler that either reports the
'   error or passes it out from the current component.
'----------------------------------------------------------------------------------------------
' Arguments (Name, Type, Description):
' In:  ModuleName       String  - module from which this method was called
'      ProcedureName    String  - procedure from which this method was called
'      LineNumber       Long    - number of the line last executed in the procedure from which
'                               - this method was called
'----------------------------------------------------------------------------------------------
' Version History (When, Who, What):
' 06/03/2001    Roger Swift
'               Header added.
' 08/06/2001    davem
'               Added call to "DecodeModuleName" to allow for either the module or procedure
'               name arguments containing both names (e.g. Form1.Form_Load)
'</Auto-Procedure-Header>======================================================================
Public Sub Bubble(ByVal ModuleName As String, _
                    ByVal ProcedureName As String, _
                    ByVal LineNumber As Long)
Attribute Bubble.VB_Description = "This method causes the current error to be raised to the next procedure higher up the call stack that has an error handler."

Const PROCEDURE_NAME As String = MODULE_NAME & ".Bubble"

    Call moHostVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn)


    'Add a trace element to the (head of the) trace stack (collection).
    With Me
        
        'First, check the Source to see if it contains a detached stack.
        'If it does then Attach it to this error object.
        If .StackDetached Then Call AttachStack(.Source)
        
        'Decode the module and procedure names if necessary.
        Call DecodeModuleName(ModuleName, ProcedureName)
        
        'Now, add the trace element to the trace stack.
        moErrorTrace.Add .Number, .Source, .Description, .HelpContext, .HelpFile, _
                         .LastDLLError, ModuleName, ProcedureName, LineNumber
    End With
    
    'Now re-raise the error to the standard error object so that it will propogate back
    'up the call chain.
    With moHostVBAErr
        .Raise .Number, .Source, .Description, .HelpFile, .HelpContext
    End With

End Sub

Friend Property Get StackDetached() As Boolean

Const PROCEDURE_NAME As String = MODULE_NAME & ".StackDetached"

    Call moHostVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn)


    'If the current Source property starts with the stack delimeter string then we
    'have a detached stack.
    StackDetached = (InStr(Me.Source, STACK_ELEMENT_DELIMETER) = 1)
    

End Property

'<Auto-Procedure-Header>=======================================================================
' Sub:  Detach()
'
' Description:
'   Detach the error stack from our local error object and bundle it into the Source property.
'   This procedure is called from the error handlers in the public methods of a component.
'   This is the mechanism by which trace information is passed out of the current component
'   (even if it is running in a different process to the calling app/component).
'----------------------------------------------------------------------------------------------
' Arguments (Name, Type, Description):
' In:  ModuleName       String          - module from which this method was called
'      ProcedureName    String          - procedure from which this method was called
'      LineNumber       Long            - number of the line last executed in the procedure
'                                         from which this method was called
'      UpdateLog        Boolean = False - Write the error in the log file
'----------------------------------------------------------------------------------------------
' Version History (When, Who, What):
' 06/03/2001    Roger Swift
'               Header added.
' 13/03/2001    davem
'               Added "UpdateLog" parameter.
' 08/06/2001    davem
'               Added call to "DecodeModuleName" to allow for either the module or procedure
'               name arguments containing both names (e.g. Form1.Form_Load)
'</Auto-Procedure-Header>======================================================================
Public Sub Detach(ByVal ModuleName As String, _
                    ByVal ProcedureName As String, _
                    ByVal LineNumber As Long, _
                    Optional ByVal UpdateLog As Boolean = False)
Attribute Detach.VB_Description = "This method is used in the public interfaces of a component to raise the current error to the external procedure that called into the component. It packs the current error stack into the Source property so that the information is preserved across compone"

Dim sSource As String

Const PROCEDURE_NAME As String = MODULE_NAME & ".Detach"

    Call moHostVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn)


    If UpdateLog = False Then
        
        'Add a trace element to the (head of the) trace stack (collection).
        With Me
    
            'First, check the Source to see if it contains a detached stack.
            'If it does then Attach it to this error object before adding the next trace element.
            If .StackDetached Then Call AttachStack(.Source)
        
            'Decode the module and procedure names if necessary.
            Call DecodeModuleName(ModuleName, ProcedureName)
        
            'Now add the next trace element.
            moErrorTrace.Add .Number, .Source, .Description, .HelpContext, .HelpFile, _
                             .LastDLLError, ModuleName, ProcedureName, LineNumber
       End With
        
    Else
    
        'Need to log the error in the log file.
        'This will attach any stack that is held in the Source property
        Call Report(ModuleName, ProcedureName, LineNumber, False)
        
    End If
    
    'Pack the error trace stack into the source string,
    sSource = PackStack(moHostVBAErr.Source)
    
    'Now clear the error trace stack.
    moErrorTrace.Clear
    
    'Now re-raise the error to the standard error object so that it will propogate back
    'up the call chain.
    'Note however, that the trace stack is packed into the Source property.
    With moHostVBAErr
        .Raise .Number, sSource, .Description, .HelpFile, .HelpContext
    End With

End Sub

'<Auto-Procedure-Header>=======================================================================
' Function:  String PackStack()
'
' Description:
'   Take the current error stack and pack it into a string along with the supplied Source
'   string. The delimeter used is set at the module level.
'----------------------------------------------------------------------------------------------
' Arguments (Name, Type, Description):
' In:  sSource  String  - the Source property of the current error.
'----------------------------------------------------------------------------------------------
' Returns:  String - the packed error stack and source
'----------------------------------------------------------------------------------------------
' Version History (When, Who, What):
' 06/03/2001  Roger Swift
'             Header added.
'
'</Auto-Procedure-Header>======================================================================
Private Function PackStack(ByVal sSource As String) As String

Dim lCount   As Long
Dim sPackage As String

Const PROCEDURE_NAME As String = MODULE_NAME & ".PackStack"

    Call moHostVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn)


    'Package up the stack.
    For lCount = 1 To moErrorTrace.Count
    
        sPackage = sPackage & STACK_ELEMENT_DELIMETER _
                & moErrorTrace(lCount).ModuleName & "." _
                & moErrorTrace(lCount).ProcedureName & "." _
                & moErrorTrace(lCount).LineNumber

    Next lCount
    
    'Add in the Source and return the packed stack.
    PackStack = sPackage & STACK_ELEMENT_DELIMETER & sSource
    

End Function

'<Auto-Procedure-Header>=======================================================================
' Sub:  AttachStack()
'
' Description:
'   Unpack the error stack from the supplied Source string and insert it into the current error
'   trace. Used when an error has been raised from a component using our error handling code.
'----------------------------------------------------------------------------------------------
' Arguments (Name, Type, Description):
' In:  sSource  String - an error trace packed into an error source string
'----------------------------------------------------------------------------------------------
' Version History (When, Who, What):
' 06/03/2001  Roger Swift
'             Header added.
'
'</Auto-Procedure-Header>======================================================================
Private Sub AttachStack(ByVal sSource As String)

Dim lCount              As Long
Dim sTraceElements()    As String
Dim sElementDetails()   As String
Dim lFinalIndex         As Long
Dim lLineNumber         As Long

Const PROCEDURE_NAME As String = MODULE_NAME & ".AttachStack"

    Call moHostVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn)


    'Split the various trace elements out of the SOurce string.
    sTraceElements = Split(sSource, STACK_ELEMENT_DELIMETER)
    
    'Index of the last element found (which is the original Source property).
    lFinalIndex = UBound(sTraceElements)
    
    'The real Source property is the last element in the array.
    Me.Source = sTraceElements(lFinalIndex)
    
    'Unpackage the stack and build it into the current trace.
    'Ignore the first element found as this is actually an empty string (because the
    'Source string starts with the delimeter used when splitting).
    'Work backwards through the trace element array to get the stack in the correct order.
    For lCount = lFinalIndex - 1 To 1 Step -1
    
        'Get the module name, procedure name and line number contained in the trace element.
        sElementDetails = Split(sTraceElements(lCount), ".")
        
        'Allow for the line number being missing.
        'The line number will be the final element if it is present.
        If UBound(sElementDetails) > 1 Then
            'Check the final element to see if it is a number.
            If IsNumeric(sElementDetails(UBound(sElementDetails))) Then
                lLineNumber = CLng(sElementDetails(UBound(sElementDetails)))
            Else
                lLineNumber = 0
            End If
            
        Else
            'Nothing found so supply a default of zero.
            lLineNumber = 0
        End If
        
        With Me
            moErrorTrace.Add .Number, .Source, .Description, .HelpContext, .HelpFile, _
                             .LastDLLError, sElementDetails(0), sElementDetails(1), lLineNumber
        End With
                
    Next lCount
    

End Sub

'<Auto-Procedure-Header>=======================================================================
' Sub:  Clear()
'
' Description:  Wrapper for the Clear method of the standard error object.
'
'----------------------------------------------------------------------------------------------
' Arguments (Name, Type, Description):
' None.
'----------------------------------------------------------------------------------------------
' Version History (When, Who, What):
' 07/06/2001    davem
'               Header inserted.
' 07/06/2001    davem
'               Added code to clear the error trace
'</Auto-Procedure-Header>======================================================================
Public Sub Clear()
Attribute Clear.VB_Description = "Clears all property settings of the ErrorObject."

Const PROCEDURE_NAME As String = MODULE_NAME & ".Clear"

    Call moHostVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn)

    'Clear the error object.
    moHostVBAErr.Clear

    'Clear the error stack now that it has been used for reporting.
    moErrorTrace.Clear
    

End Sub 'Clear

Private Sub Class_Initialize()

Const PROCEDURE_NAME As String = MODULE_NAME & ".Class_Initialize"

    Set moHostVbExt = New vbExt
    
    Call moHostVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn, "V" & 1)


    'Create the error trace stack collection.
    Set moErrorTrace = New CErrorTrace
        
    ' Initialise the reference count for the object
    mlRefCount = 1
    
    'Create the error trace stack collection used by the Push and Pop procedures.
    'If we do it any later (say, the first time Push is called) then the initialisation
    'of the CErrorTrace class causes the error context to be wiped (through an
    'initialisation of a local vbExt instance).
    Set moSavedErr = New CErrorTrace

End Sub

Private Sub Class_Terminate()

Const PROCEDURE_NAME As String = MODULE_NAME & ".Class_Terminate"

    Call moHostVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn)


    'Destroy the error trace stack collection used by the Push and Pop procedures.
    Set moSavedErr = Nothing
    
    'Destroy the error trace stack collection.
    Set moErrorTrace = Nothing
    

    Call moHostVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceOut)
    Set moHostVbExt = Nothing
    
End Sub

'<Auto-Procedure-Header>=======================================================================
' Sub:  Report()
'
' Description:
'   Write details of the fatal error (including the trace stack) to the log.
'   Optionally display a message box with the error details.
'   For this procedure to actually write to the log the application must have previously
'   used the "StartLogging" method.
'
'----------------------------------------------------------------------------------------------
' Arguments (Name, Type, Description):
' In:  ModuleName       String          - module from which this method was called
'      ProcedureName    String          - procedure from which this method was called
'      LineNumber       Long            - number of the line last executed in the procedure
'                                         from which this method was called
'      DisplayReqd      Boolean = True  - display the error message as well as logging it
'      Caption          String = ""     - the caption for the message box
'      UserMessage      String = ""     - a user friendly message to be displayed with the
'                                         error
'----------------------------------------------------------------------------------------------
' Version History (When, Who, What):
' 06/03/2001  Roger Swift
'             Header added.
' 12/03/2001    davem
'               Added Error object to vbUtils so need to instantiate our own oVbext object
'               to use "LogEvent"
' 07/06/2001    davem
'               Included Thread ID in report written to the Event Log
' 08/06/2001    davem
'               Added call to "DecodeModuleName" to allow for either the module or procedure
'               name arguments containing both names (e.g. Form1.Form_Load)
'</Auto-Procedure-Header>======================================================================
Public Sub Report(ByVal ModuleName As String, _
                    ByVal ProcedureName As String, _
                    ByVal LineNumber As Long, _
                    Optional ByVal DisplayReqd As Boolean = True, _
                    Optional ByVal Caption As String = vbNullString, _
                    Optional ByVal UserMessage As String = vbNullString)
Attribute Report.VB_Description = "Write details of the fatal error (including the trace stack) to the log.\r\nOptionally display a message box with the error details."

Dim sStack          As String
Dim sMessage        As String
Dim iCount          As Integer
Dim lErrNumber      As Long
Dim sSource         As String
Dim sDescription    As String
Dim sHelpFile       As String
Dim lHelpContext    As Long

Const PROCEDURE_NAME As String = MODULE_NAME & ".Report"

    Call moHostVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn)


    'Add the error details to the stack, even though we are about to log/report the error.
    'Saves having to worry about an empty stack if the error occured in a top level procedure
    'that calls this method directly (i.e. no bubbling done).
    With Me
        
        'Preserve the current error values
        lErrNumber = .Number
        sSource = .Source
        sDescription = .Description
        sHelpFile = .HelpFile
        lHelpContext = .HelpContext
        
        'Decode the module and procedure names if necessary.
        Call DecodeModuleName(ModuleName, ProcedureName)
        
        'First, check the Source to see if it contains a detached stack.
        'If it does then Attach it to this error object before adding the next trace element.
        If .StackDetached Then Call AttachStack(.Source)
        
        'Now, add the trace element to the trace stack.
        moErrorTrace.Add .Number, .Source, .Description, .HelpContext, .HelpFile, _
                         .LastDLLError, ModuleName, ProcedureName, LineNumber
    End With
    
    'Now gather up the error stack.
    sStack = "Error stack:"
    For iCount = 1 To moErrorTrace.Count
    
        With moErrorTrace(iCount)
        
            'Add module and procedure name.
            sStack = sStack & vbCrLf _
                            & .ModuleName & "." & .ProcedureName
            
            'Add the line number if there is one.
            If .LineNumber > 0 Then sStack = sStack & "." & .LineNumber
            
        End With
    
    Next iCount
    
    ' Build up the error message
    With moErrorTrace(moErrorTrace.Count)
    
        sMessage = "Error" & vbTab & ": "
        
        'Map the error number to a "meaningful" value if it has originated
        'from an external component.
        If .Number >= vbObjectError And .Number <= vbObjectError + 65535 Then
            sMessage = sMessage & CStr(.Number - vbObjectError) & vbCrLf
        Else
            sMessage = sMessage & CStr(.Number) & vbCrLf
        End If
        
        If .LastDLLError <> 0 Then
            sMessage = sMessage & "DLL Error" & vbTab & ": " & CStr(.LastDLLError) & vbCrLf
        End If
        
        sMessage = sMessage _
                    & "Message" & vbTab & ": " & .Description & vbCrLf _
                    & "Source" & vbTab & ": " & .Source

    End With
    
    'Log the error message now that it is finally constructed (if logging enabled).
    If Not moHostVbExt Is Nothing Then
        'Add details to the log
        Call moHostVbExt.LogEvent(sMessage & vbCrLf _
                                & "Thread" & vbTab & ": " & App.ThreadID & vbCrLf & vbCrLf _
                                & sStack, vbLogEventTypeError)
    End If

    'Display the message on screen if required.
    If DisplayReqd = True Then
    
        'Provide a caption if necessary.
        If Len(Caption) = 0 Then Caption = "Fatal Error"
        
        'Add a user friendly message if one has been supplied.
        If Len(UserMessage) > 0 Then sMessage = UserMessage & vbCrLf & vbCrLf & sMessage
            
        'Finally display the error to the user.
        Call MsgBox(sMessage, vbCritical + vbOKOnly, Caption)
        
    End If
    
    With Me
        
        'Restore the  error values
        .Number = lErrNumber
        .Source = sSource
        .Description = sDescription
        .HelpFile = sHelpFile
        .HelpContext = lHelpContext
    
    End With
    

End Sub

Public Sub Raise(ByVal Number As Long, _
                    Optional ByVal Source As String, _
                    Optional ByVal Description As String, _
                    Optional ByVal HelpFile As String, _
                    Optional ByVal HelpContext As Long)
Attribute Raise.VB_Description = "Generate an error with the supplied parameters."

Const PROCEDURE_NAME As String = MODULE_NAME & ".Raise"

    Call moHostVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn)


'Wrapper for Raise method of standard error object.

    moHostVBAErr.Raise Number, Source, Description, HelpFile, HelpContext

End Sub
