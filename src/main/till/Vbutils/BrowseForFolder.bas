Attribute VB_Name = "modBrowseForFolder"
Option Explicit

Public pusStartFolder  As String   'The current directory

'Used in debug and trace messages.
Private Const MODULE_NAME As String = "modBrowseForFolder"

'A reference to an instance of vbExt used by trace and debug calls.
Private m_oVbExt As vbExt

'-----------------------------------------------------------
' FUNCTION: BrowseCallbackProc
'
' This is a Callback function used by BrowseForFolderEx to set the current directory
' on the Browseforfolder dialog
'-----------------------------------------------------------
Public Function BrowseCallbackProc(ByVal hWnd As Long, ByVal uMsg As Long, _
                                    ByVal lp As Long, ByVal pData As Long) As Long
Dim lpIDList    As Long
Dim lRet        As Long
Dim sBuffer     As String

Const PROCEDURE_NAME As String = MODULE_NAME & ".BrowseCallbackProc"
  
    On Error Resume Next    ' Sugested by MS to prevent an error from propagating back

    Set m_oVbExt = New vbExt
    
    Call m_oVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceIn)

                            ' into the calling process.
     
    Select Case uMsg
    Case BFFM_INITIALIZED
        ' Set the start folder
        Call SendMessage(hWnd, BFFM_SETSELECTION, 1, pusStartFolder)
      
    Case BFFM_SELCHANGED
        ' Update the Ststus Text with the new path
        sBuffer = Space$(MAX_PATH)
      
        lRet = SHGetPathFromIDList(lp, sBuffer)
        If lRet = 1 Then
            Call SendMessage(hWnd, BFFM_SETSTATUSTEXT, 0, sBuffer)
        End If
      
    End Select
  
    BrowseCallbackProc = 0
  

    Call m_oVbExt.DebugMsg(APP_NAME, PROCEDURE_NAME, endlTraceOut)

    Set m_oVbExt = Nothing
    
End Function

