Attribute VB_Name = "modCommonBO"
'<CAMH>****************************************************************************************
'* Module : modCommonBO
'* Date   : 01/10/02
'* Author : mauricem
' $Archive: /Projects/OasysV2/VB/Common/modCommonBO.bas $
'**********************************************************************************************
'* Summary: Provides common routines used by the Business Objects
'**********************************************************************************************
'* $Revision: 1 $
'* $Date: 12/09/03 8:20 $
'* $Author: Markp $
'* Versions:
'* 01/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "modCommonBO"

Const SYSTEM_NAME As String = "Oasys Tile"

Public Const PRM_EDI_DATE_FMT As Long = 126
Public Const PRM_EDIPATH As Long = 211

Public Enum enFmtType
    enftString = 0
    enftNumber = 1
    enftDate = 2
    enftYesNo = 3
    enftCode = 4
End Enum

Public mstrDateFmt As String

' When we save a BO to the database we have the options to save regardless,
' save only if it doesn't already exist, save only if is does exist or delete.
' This must match the same enum in IBoDatabase.cls
Public Enum enSaveType
    SaveTypeAllCases = 0             ' Default value
    SaveTypeIfNew = 1
    SaveTypeIfExists = 2
    SaveTypeDelete = 3
End Enum

Public Function GetRoot() As IOasysRoot
    
Dim oStart     As OasysStartCom_Wickes.OasysStart
Dim oOasysRoot As OasysRootCom_Wickes.OasysRoot
    
    Set oStart = New OasysStart
    Set oOasysRoot = oStart.GetRoot(Command)
    Set oStart = Nothing
    Set GetRoot = oOasysRoot
    
End Function

Public Function CheckIsUnInitialised(ByRef oClassSession As ISession, ByVal sClassName As String)
    ' Return true if there is a problem
    If oClassSession Is Nothing Then
        MsgBox "Invalid " & sClassName & " object; use IDatabase.CreateBusinessObject()"
        CheckIsUnInitialised = True
    End If

End Function

Public Function GetSelectAllRows(ByVal lClassID As Long, ByVal lMaxPropertyID As Long, ByRef oCurrSession As ISession) As CRowSelector
' Returns a CRowSelector object containing one SelectAll field selector for each property.
    
Dim oRSelector  As CRowSelector
Dim nFid        As Long
    
    Set oRSelector = oCurrSession.Root.CreateUtilityObject("CRowSelector")
    Call DebugMsg(MODULE_NAME, "GetSelectAllRows", endlTraceIn)
    For nFid = (lClassID * &H10000) + 1 To lMaxPropertyID Step 1
        oRSelector.AddSelectAll nFid
    Next nFid
    Call DebugMsg(MODULE_NAME, "GetSelectAllRows", endlTraceOut)
    Set GetSelectAllRows = oRSelector
5767169
End Function

Public Function LoadLongArray(nValue() As Long, oGroupField As CFieldGroup, ByRef oCurrSession As ISession) As Boolean
' Helper function for LoadFromRow
Dim iRowNo   As Integer
Dim oRow     As IRow
Dim oMyField As CFieldLong

    Debug.Assert (oGroupField.IField_FieldType = 6)    ' 6 = FieldTypeGroup
    Set oRow = oGroupField.GroupRow
    iRowNo = oGroupField.Subscript
    If iRowNo > 0 Then
        ' We pick just one item from the array
        Set oMyField = oRow.Field(1)
        nValue(iRowNo) = oMyField.LongValue
    Else
        ' We handle the whole array
        For iRowNo = 1 To oRow.Count
            Set oMyField = oRow.Field(iRowNo)
            nValue(iRowNo) = oMyField.LongValue
        Next iRowNo
    End If
    
End Function

Public Function LoadStringArray(nValue() As String, oGroupField As CFieldGroup, ByRef oCurrSession As ISession) As Boolean
' Helper function for LoadFromRow
Dim iRowNo   As Integer
Dim oRow     As IRow
Dim oMyField As CFieldString

    Debug.Assert (oGroupField.IField_FieldType = 6)    ' 6 = FieldTypeGroup
    Set oRow = oGroupField.GroupRow
    iRowNo = oGroupField.Subscript
    If iRowNo > 0 Then
        ' We pick just one item from the array
        Set oMyField = oRow.Field(1)
        nValue(iRowNo) = oMyField.StringValue
    Else
        ' We handle the whole array
        For iRowNo = 1 To oRow.Count
            Set oMyField = oRow.Field(iRowNo)
            nValue(iRowNo) = oMyField.StringValue
        Next iRowNo
    End If
    
End Function

Public Function LoadDoubleArray(nValue() As Double, oGroupField As CFieldGroup, ByRef oCurrSession As ISession) As Boolean
' Helper function for LoadFromRow
Dim iRowNo   As Integer
Dim oRow     As IRow
Dim oMyField As CFieldDouble

    Debug.Assert (oGroupField.IField_FieldType = 6)    ' 6 = FieldTypeGroup
    Set oRow = oGroupField.GroupRow
    iRowNo = oGroupField.Subscript
    If iRowNo > 0 Then
        ' We pick just one item from the array
        Set oMyField = oRow.Field(1)
        nValue(iRowNo) = oMyField.DoubleValue
    Else
        ' We handle the whole array
        For iRowNo = 1 To oRow.Count
            Set oMyField = oRow.Field(iRowNo)
            nValue(iRowNo) = oMyField.DoubleValue
        Next iRowNo
    End If

End Function

Public Function LoadDateArray(nValue() As Date, oGroupField As CFieldGroup, ByRef oCurrSession As ISession) As Boolean
' Helper function for LoadFromRow
Dim iRowNo   As Integer
Dim oRow     As IRow
Dim oMyField As CFieldDate

    Debug.Assert (oGroupField.IField_FieldType = 6)    ' 6 = FieldTypeGroup
    Set oRow = oGroupField.GroupRow
    iRowNo = oGroupField.Subscript
    If iRowNo > 0 Then
        ' We pick just one item from the array
        Set oMyField = oRow.Field(1)
        nValue(iRowNo) = oMyField.DateValue
    Else
        ' We handle the whole array
        For iRowNo = 1 To oRow.Count
            Set oMyField = oRow.Field(iRowNo)
            nValue(iRowNo) = oMyField.DateValue
        Next iRowNo
    End If

End Function

Public Function LoadBooleanArray(nValue() As Boolean, oGroupField As CFieldGroup, ByRef oCurrSession As ISession) As Boolean
' Helper function for LoadFromRow
Dim iRowNo   As Integer
Dim oRow     As IRow
Dim oMyField As CFieldBool

    Debug.Assert (oGroupField.IField_FieldType = 6)    ' 6 = FieldTypeGroup
    Set oRow = oGroupField.GroupRow
    iRowNo = oGroupField.Subscript
    If iRowNo > 0 Then
        ' We pick just one item from the array
        Set oMyField = oRow.Field(1)
        nValue(iRowNo) = oMyField.BoolValue
    Else
        ' We handle the whole array
        For iRowNo = 1 To oRow.Count
            Set oMyField = oRow.Field(iRowNo)
            nValue(iRowNo) = oMyField.BoolValue
        Next iRowNo
    End If
    
End Function

Public Function SaveLongArray(nValue() As Long, ByRef oCurrSession As ISession) As CFieldGroup
' Helper function for GetField
Dim iSeqNo      As Integer
Dim oRow        As IRow
Dim oSubField   As CFieldLong
    
    Set SaveLongArray = oCurrSession.Root.CreateUtilityObject("CFieldGroup")
    Set oSubField = New CFieldLong
    Set oRow = SaveLongArray.GroupRow
    
    For iSeqNo = 1 To UBound(nValue)
        oSubField.LongValue = nValue(iSeqNo)
        oRow.Add oSubField.Interface
    Next iSeqNo

End Function

Public Function SaveStringArray(nValue() As String, ByRef oCurrSession As Session) As CFieldGroup
' Helper function for GetField
Dim iSeqNo      As Integer
Dim oRow        As IRow
Dim oSubField   As CFieldString
    
    Set SaveStringArray = oCurrSession.Root.CreateUtilityObject("CFieldGroup")
    Set oSubField = New CFieldString
    Set oRow = SaveStringArray.GroupRow
    
    For iSeqNo = 1 To UBound(nValue)
        oSubField.StringValue = nValue(iSeqNo)
        oRow.Add oSubField.IField_Interface
    Next iSeqNo

End Function

Public Function SaveDoubleArray(nValue() As Double, ByRef oCurrSession As ISession) As CFieldGroup
    ' Helper function for GetField
Dim iSeqNo      As Integer
Dim oRow        As IRow
Dim oSubField   As CFieldDouble
    
    Set SaveDoubleArray = oCurrSession.Root.CreateUtilityObject("CFieldGroup")
    Set oSubField = New CFieldDouble
    Set oRow = SaveDoubleArray.GroupRow
    
    For iSeqNo = 1 To UBound(nValue)
        oSubField.DoubleValue = nValue(iSeqNo)
        oRow.Add oSubField.IField_Interface
    Next iSeqNo

End Function

Public Function SaveDateArray(nValue() As Date, ByRef oCurrSession As ISession) As CFieldGroup
    ' Helper function for GetField
Dim iSeqNo      As Integer
Dim oRow        As IRow
Dim oSubField   As CFieldDate
    
    Set SaveDateArray = oCurrSession.Root.CreateUtilityObject("CFieldGroup")
    Set oSubField = New CFieldDate
    Set oRow = SaveDateArray.GroupRow
    
    For iSeqNo = 1 To UBound(nValue)
        oSubField.DateValue = nValue(iSeqNo)
        oRow.Add oSubField.IField_Interface
    Next iSeqNo

End Function

Public Function SaveBooleanArray(nValue() As Boolean, ByRef oCurrSession As ISession) As CFieldGroup
    ' Helper function for GetField
Dim iSeqNo      As Integer
Dim oRow        As IRow
Dim oSubField   As CFieldBool
    
    Set SaveBooleanArray = oCurrSession.Root.CreateUtilityObject("CFieldGroup")
    Set oSubField = New CFieldBool
    Set oRow = SaveBooleanArray.GroupRow
    
    For iSeqNo = 1 To UBound(nValue)
        oSubField.BoolValue = nValue(iSeqNo)
        oRow.Add oSubField.IField_Interface
    Next iSeqNo

End Function


Public Function FormatEDIDate(ByVal dteInDate As Date) As String

Dim strTempDate As String

    'check if date is invalid and so return as dashes
    If Format$(dteInDate, "DD/MM/YY HH:NN:SS") = "30/12/99 00:00:00" Then
        strTempDate = Replace(mstrDateFmt, "D", "-")
        strTempDate = Replace(strTempDate, "M", "-")
        strTempDate = Replace(strTempDate, "Y", "-")
        FormatEDIDate = strTempDate
    Else
        'return date in specified format, with time if required
        FormatEDIDate = Format$(dteInDate, mstrDateFmt)
    End If
    
End Function

Public Function FmtVal(ByVal vntValue As Variant, ByVal enftType As enFmtType, Optional ByVal lngLength As Long = 1, Optional ByVal lngDecPlaces As Long = 0)

Dim strValue        As String 'used to hold value whilst it is being formatted
Dim strDecHolder    As String

    Select Case (enftType)
        Case (enftString):
                        strValue = vntValue & (Space$(lngLength - Len(vntValue)))
        Case (enftCode):
                        If (vntValue = "") Then vntValue = "0"
                        strValue = Format$(vntValue, String$(lngLength, "0"))
        Case (enftNumber):
                        If (lngDecPlaces > 0) Then
                            strDecHolder = "." & String$(lngDecPlaces, "0")
                        End If 'decimal places must be dealt with
                        strDecHolder = String$(lngLength - lngDecPlaces, "0") & strDecHolder
                        strDecHolder = strDecHolder & " ;" & strDecHolder & "-"
                        strValue = Format$(vntValue, strDecHolder)
        Case (enftDate):
                        strValue = Format$(Date, "DD/MM/YY")
        Case (enftYesNo):
                        If vntValue = True Then
                            strValue = "Y"
                        Else
                            strValue = "N"
                        End If
    End Select
    
    FmtVal = strValue
    
End Function
