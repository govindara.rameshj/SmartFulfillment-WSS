VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cConResHeader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : ConResHeader
'* Date   : 10/12/08
'* Author : MikeO
'*$Archive: /Projects/OasysV2/VB/InventoryBO/cConResHeader.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: MikeO $
'* $Date: 10/12/08 9:25 $
'* $Revision: 1 $
'* Versions:
'* 10/12/08    MikeO
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "cConResHeader"

Private mStoreId As String
Private mTransactionDate As String
Private mTransactionTill As String
Private mTransactionNumber As String
Private mContact1Salutation As String
Private mContact1FirstName As String
Private mContact1Surname As String
Private mCompanyName As String
Private mPostCode As String
Private mHouseNumberName As String
Private mAddressLine1 As String
Private mAddressLine2 As String
Private mTown As String
Private mCounty As String
Private mCountry As String
Private mReasonCode As String
Private mConcernDesc(4) As String
Private mEmployeeId As String
Private mRTI As String
Private mRTIDateSent As String

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

Public Property Get StoreId() As String
   Let StoreId = mStoreId
End Property

Public Property Let StoreId(ByVal Value As String)
    Let mStoreId = Value
End Property

Public Property Get TransactionDate() As String
   Let TransactionDate = mTransactionDate
End Property

Public Property Let TransactionDate(ByVal Value As String)
    Let mTransactionDate = Value
End Property

Public Property Get TransactionTill() As String
   Let TransactionTill = mTransactionTill
End Property

Public Property Let TransactionTill(ByVal Value As String)
    Let mTransactionTill = Value
End Property

Public Property Get TransactionNumber() As String
   Let TransactionNumber = mTransactionNumber
End Property

Public Property Let TransactionNumber(ByVal Value As String)
    Let mTransactionNumber = Value
End Property

Public Property Get Contact1Salutation() As String
   Let Contact1Salutation = mContact1Salutation
End Property

Public Property Let Contact1Salutation(ByVal Value As String)
    Let mContact1Salutation = Value
End Property

Public Property Get Contact1FirstName() As String
   Let Contact1FirstName = mContact1FirstName
End Property

Public Property Let Contact1FirstName(ByVal Value As String)
    Let mContact1FirstName = Value
End Property

Public Property Get Contact1SurName() As String
   Let Contact1SurName = mContact1Surname
End Property

Public Property Let Contact1SurName(ByVal Value As String)
    Let mContact1Surname = Value
End Property

Public Property Get CompanyName() As String
   Let CompanyName = mCompanyName
End Property

Public Property Let CompanyName(ByVal Value As String)
    Let mCompanyName = Value
End Property

Public Property Get PostCode() As String
   Let PostCode = mPostCode
End Property

Public Property Let PostCode(ByVal Value As String)
    Let mPostCode = Value
End Property

Public Property Get HouseNumberName() As String
   Let HouseNumberName = mHouseNumberName
End Property

Public Property Let HouseNumberName(ByVal Value As String)
    Let mHouseNumberName = Value
End Property

Public Property Get AddressLine1() As String
   Let AddressLine1 = mAddressLine1
End Property

Public Property Let AddressLine1(ByVal Value As String)
    Let mAddressLine1 = Value
End Property

Public Property Get AddressLine2() As String
   Let AddressLine2 = mAddressLine2
End Property

Public Property Let AddressLine2(ByVal Value As String)
    Let mAddressLine2 = Value
End Property

Public Property Get Town() As String
   Let Town = mTown
End Property

Public Property Let Town(ByVal Value As String)
    Let mTown = Value
End Property

Public Property Get County() As String
   Let County = mCounty
End Property

Public Property Let County(ByVal Value As String)
    Let mCounty = Value
End Property

Public Property Get Country() As String
   Let Country = mCountry
End Property

Public Property Let Country(ByVal Value As String)
    Let mCountry = Value
End Property

Public Property Get ReasonCode() As String
   Let ReasonCode = mReasonCode
End Property

Public Property Let ReasonCode(ByVal Value As String)
    Let mReasonCode = Value
End Property

Public Property Get ConcernDesc(Index As Variant) As String
   Let ConcernDesc = mConcernDesc(Index)
End Property

Public Property Let ConcernDesc(Index As Variant, ByVal Value As String)
    Let mConcernDesc(Index) = Value
End Property

Public Property Get EmployeeId() As String
   Let EmployeeId = mEmployeeId
End Property

Public Property Let EmployeeId(ByVal Value As String)
    Let mEmployeeId = Value
End Property

Public Property Get RTI() As String
   Let RTI = mRTI
End Property

Public Property Let RTI(ByVal Value As String)
    Let mRTI = Value
End Property

'Public Property Get RTIDateSent() As String
'   Let RTIDateSent = mRTIDateSent
'End Property
'
'Public Property Let RTIDateSent(ByVal Value As String)
'    Let mRTIDateSent = Value
'End Property

Private Function Load() As Boolean

' Load up all properties from the database
Dim oView       As IView

    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)

    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)

    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function

Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cConResHeader
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_CONRESHEADER * &H10000) + 1 To FID_CONRESHEADER_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cConResHeader

End Function

Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_CONRESHEADER, FID_CONRESHEADER_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_CONRESHEADER_StoreId):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mStoreId
        Case (FID_CONRESHEADER_TransactionDate):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTransactionDate
        Case (FID_CONRESHEADER_TransactionTill):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTransactionTill
        Case (FID_CONRESHEADER_TransactionNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTransactionNumber
        Case (FID_CONRESHEADER_Contact1Salutation):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mContact1Salutation
        Case (FID_CONRESHEADER_Contact1FirstName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mContact1FirstName
        Case (FID_CONRESHEADER_Contact1SurName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mContact1Surname
        Case (FID_CONRESHEADER_CompanyName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCompanyName
        Case (FID_CONRESHEADER_PostCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPostCode
        Case (FID_CONRESHEADER_HouseNumberName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mHouseNumberName
        Case (FID_CONRESHEADER_AddressLine1):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine1
        Case (FID_CONRESHEADER_AddressLine2):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine2
        Case (FID_CONRESHEADER_Town):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTown
        Case (FID_CONRESHEADER_County):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCounty
        Case (FID_CONRESHEADER_Country):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCountry
        Case (FID_CONRESHEADER_ReasonCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mReasonCode
        Case (FID_CONRESHEADER_ConcernDescription):
                Set GetField = SaveStringArray(mConcernDesc, m_oSession)
        Case (FID_CONRESHEADER_EmployeeId):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mEmployeeId
        Case (FID_CONRESHEADER_RTI):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mRTI
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cConResHeader
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_CONRESHEADER

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Date " & mTransactionDate

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_CONRESHEADER_StoreId): mStoreId = oField.ValueAsVariant
            Case (FID_CONRESHEADER_TransactionDate): mTransactionDate = oField.ValueAsVariant
            Case (FID_CONRESHEADER_TransactionTill): mTransactionTill = oField.ValueAsVariant
            Case (FID_CONRESHEADER_TransactionNumber): mTransactionNumber = oField.ValueAsVariant
            Case (FID_CONRESHEADER_Contact1Salutation): mContact1Salutation = oField.ValueAsVariant
            Case (FID_CONRESHEADER_Contact1FirstName): mContact1FirstName = oField.ValueAsVariant
            Case (FID_CONRESHEADER_Contact1SurName): mContact1Surname = oField.ValueAsVariant
            Case (FID_CONRESHEADER_CompanyName): mCompanyName = oField.ValueAsVariant
            Case (FID_CONRESHEADER_PostCode): mPostCode = oField.ValueAsVariant
            Case (FID_CONRESHEADER_HouseNumberName): mHouseNumberName = oField.ValueAsVariant
            Case (FID_CONRESHEADER_AddressLine1): mAddressLine1 = oField.ValueAsVariant
            Case (FID_CONRESHEADER_AddressLine2): mAddressLine2 = oField.ValueAsVariant
            Case (FID_CONRESHEADER_Town): mTown = oField.ValueAsVariant
            Case (FID_CONRESHEADER_County): mCounty = oField.ValueAsVariant
            Case (FID_CONRESHEADER_Country): mCountry = oField.ValueAsVariant
            Case (FID_CONRESHEADER_ReasonCode): mReasonCode = oField.ValueAsVariant
            Case (FID_CONRESHEADER_ConcernDescription): Call LoadStringArray(mConcernDesc, oField, m_oSession)
            Case (FID_CONRESHEADER_EmployeeId): mEmployeeId = oField.ValueAsVariant
            Case (FID_CONRESHEADER_RTI): mRTI = oField.ValueAsVariant
            'Case (FID_CONRESHEADER_RTIDateSent): mRTIDateSent = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_CONRESHEADER_END_OF_STATIC

End Function
