VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{9BA18739-054D-4172-8E42-118133CE2FC4}#1.0#0"; "EditDateCtl.ocx"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Begin VB.Form frmEANReport 
   Caption         =   "Incorrect EAN Association Report"
   ClientHeight    =   7800
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   11880
   Icon            =   "frmPrintLabels.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   7800
   ScaleWidth      =   11880
   StartUpPosition =   3  'Windows Default
   Begin CTSProgBar.ucpbProgressBar ucpbProgress 
      Height          =   1860
      Left            =   3180
      TabIndex        =   18
      Top             =   2520
      Width           =   5100
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      Object.Width           =   5100
      Object.Height          =   1860
   End
   Begin VB.Frame fraCriteria 
      Caption         =   "Search Criteria"
      Height          =   975
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   11715
      Begin VB.ComboBox cmbEntryMethod 
         Height          =   315
         ItemData        =   "frmPrintLabels.frx":058A
         Left            =   4560
         List            =   "frmPrintLabels.frx":0597
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton cmdReset 
         Caption         =   "F3-Reset"
         Height          =   375
         Left            =   9900
         TabIndex        =   11
         Top             =   240
         Width           =   915
      End
      Begin VB.ComboBox cmbReasonCode 
         Height          =   315
         ItemData        =   "frmPrintLabels.frx":05B0
         Left            =   6780
         List            =   "frmPrintLabels.frx":05B2
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   240
         Width           =   2055
      End
      Begin ucEditDate.ucDateText dtxtEndDate 
         Height          =   315
         Left            =   3180
         TabIndex        =   5
         Top             =   240
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DateFormat      =   "DD/MM/YY"
         Text            =   ""
      End
      Begin VB.ComboBox cmbDateCrit 
         Height          =   315
         ItemData        =   "frmPrintLabels.frx":05B4
         Left            =   1080
         List            =   "frmPrintLabels.frx":05B6
         Style           =   2  'Dropdown List
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   240
         Width           =   915
      End
      Begin ucEditDate.ucDateText dtxtStartDate 
         Height          =   315
         Left            =   2040
         TabIndex        =   3
         Top             =   240
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DateFormat      =   "DD/MM/YY"
         Text            =   ""
      End
      Begin VB.CommandButton cmdApply 
         Caption         =   "F7-Apply"
         Height          =   375
         Left            =   8880
         TabIndex        =   10
         Top             =   240
         Width           =   915
      End
      Begin VB.ComboBox cmbSortBy 
         Height          =   315
         ItemData        =   "frmPrintLabels.frx":05B8
         Left            =   1080
         List            =   "frmPrintLabels.frx":05C5
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   600
         Width           =   1455
      End
      Begin VB.Label Label3 
         Caption         =   "Entry"
         Height          =   255
         Left            =   4140
         TabIndex        =   6
         Top             =   300
         Width           =   495
      End
      Begin VB.Label Label4 
         Caption         =   "Reason Code"
         Height          =   255
         Left            =   5700
         TabIndex        =   8
         Top             =   300
         Width           =   975
      End
      Begin VB.Label lblTolbl 
         Caption         =   "to"
         Height          =   255
         Left            =   2940
         TabIndex        =   4
         Top             =   300
         Width           =   255
      End
      Begin VB.Label Label2 
         Caption         =   "Date Range"
         Height          =   315
         Left            =   120
         TabIndex        =   1
         Top             =   300
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Sort By"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   660
         Width           =   675
      End
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F9 - Print"
      Height          =   435
      Left            =   8280
      TabIndex        =   17
      Top             =   6600
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10 - Exit"
      Height          =   435
      Left            =   120
      TabIndex        =   16
      Top             =   6660
      Width           =   1155
   End
   Begin FPSpreadADO.fpSpread sprdReport 
      Height          =   4635
      Left            =   60
      TabIndex        =   14
      Top             =   1020
      Visible         =   0   'False
      Width           =   14415
      _Version        =   458752
      _ExtentX        =   25426
      _ExtentY        =   8176
      _StockProps     =   64
      EditModeReplace =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   14
      MaxRows         =   1
      OperationMode   =   2
      RowHeaderDisplay=   0
      SpreadDesigner  =   "frmPrintLabels.frx":05EB
      UserResize      =   2
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   15
      Top             =   7425
      Width           =   11880
      _ExtentX        =   20955
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmPrintLabels.frx":0C15
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13123
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "11:20"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmEANReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module :
'* Date   : 19/01/07
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Labels Request/frmEANReport.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 26/06/06 16:19 $ $Revision: 8 $
'* Versions:
'* 19/09/02    mauricem
'*             Header added.
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*      TS  Start Date followed by one of the following =  >  <
'*                           e.g. (/P='TS=20030326,  or  (/P>'TS>20030429,
'*      SB  Show Button      if parameter is present Cash & Other Totals CAN be revealed
'*                           if parameter is absent  Cash & Other Totals cannot be revealed
'*      DC  Destination Code DCP Printer or DCS Preview
'*      CF  Called From      If CFC then the form is called in
'*                           in a automatic (Close) fashion.
'*
'**********************************************************************************************
'* SAMPLE (/P='DB=20031020,SB,STO,DCS,GRD,DL0' /u='044' -c='ffff')
'</CAMH>***************************************************************************************
Option Explicit

Const COL_DATE          As Long = 1
Const COL_TIME          As Long = 2
Const COL_TILLID        As Long = 3
Const COL_TRANNO        As Long = 4
Const COL_USERID        As Long = 5
Const COL_ENTRY         As Long = 6
Const COL_CODE          As Long = 7
Const COL_VALUE         As Long = 8
Const COL_EAN_FOUND     As Long = 9
Const COL_EAN_SKU       As Long = 10
Const COL_EAN_SKU_FND   As Long = 11
Const COL_SKU           As Long = 12
Const COL_SORTDATE      As Long = 13
Const COL_STORE_NO      As Long = 14

Private Const CRIT_EQUALS        As Long = 0
Private Const CRIT_GREATERTHAN   As Long = 1
Private Const CRIT_LESSTHAN      As Long = 2
Private Const CRIT_ALL           As Long = 3
Private Const CRIT_FROM          As Long = 4
Private Const CRIT_WEEKENDING    As Long = 5

Const SKU_REJECT = 93
Const IN_HOUSE_SKU_MISS As String = 94
Const SKU_MISSING As String = 95
Const IN_HOUSE_EAN_MISS As String = 96
Const EAN_SKU_MISS_CODE As String = 97
Const EAN_MISS_CHECK_CODE As String = 98


Const PRM_TRANSNO   As Long = 911

Private mdteSystemDate  As Date
Private mblnAutoApply   As Boolean

Private mblnFromNightlyClose As Boolean
Private mstrDestCode         As Boolean

Private mlngDatedPos    As Long
Public records As New ADODB.Recordset
Private mycon As New Connection
Public myrecords As New ADODB.Recordset
Private rectotal As Integer

Private Sub LoadReasonCodes()

Dim oBCCodes As cBarcodeBroken
Dim colCodes As Collection

    cmbReasonCode.AddItem ("All Reason Codes")
    cmbReasonCode.ItemData(cmbReasonCode.NewIndex) = 0
    cmbReasonCode.ListIndex = 0
    
    cmbReasonCode.AddItem ("Item Mismatch")
    cmbReasonCode.ItemData(cmbReasonCode.NewIndex) = 93
    cmbReasonCode.AddItem ("In House SKU Missing")
    cmbReasonCode.ItemData(cmbReasonCode.NewIndex) = 94
    cmbReasonCode.AddItem ("SKU Missing")
    cmbReasonCode.ItemData(cmbReasonCode.NewIndex) = 95
    cmbReasonCode.AddItem ("In House EAN Missing")
    cmbReasonCode.ItemData(cmbReasonCode.NewIndex) = 96
    cmbReasonCode.AddItem ("EAN SKU Missing")
    cmbReasonCode.ItemData(cmbReasonCode.NewIndex) = 97
    cmbReasonCode.AddItem ("EAN Missing")
    cmbReasonCode.ItemData(cmbReasonCode.NewIndex) = 98

    cmbReasonCode.AddItem ("Item Keyed")
    cmbReasonCode.ItemData(cmbReasonCode.NewIndex) = 99
    
    Set oBCCodes = goDatabase.CreateBusinessObject(CLASSID_BARCODEFAILED)
    Set colCodes = oBCCodes.LoadMatches
        
    For Each oBCCodes In colCodes
        Call cmbReasonCode.AddItem(oBCCodes.Description)
        cmbReasonCode.ItemData(cmbReasonCode.NewIndex) = Val(oBCCodes.Code)
    Next

End Sub


Private Sub cmbReasonCode_GotFocus()
    
    Call SendKeys(KEY_DROPDOWN)

End Sub

Private Sub cmbReasonCode_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub cmbSortBy_Click()

    If (cmbSortBy.ListIndex = -1) Then Exit Sub
    sprdReport.Redraw = False
    Screen.MousePointer = vbHourglass
    'sort by selected criteria (SKU/Plangram
    Select Case (cmbSortBy.ItemData(cmbSortBy.ListIndex))
        Case (0):   sprdReport.SortKey(1) = COL_SORTDATE
        Case (1):   sprdReport.SortKey(1) = COL_CODE
        Case (2):   sprdReport.SortKey(1) = COL_VALUE
    End Select
    sprdReport.SortKeyOrder(1) = SortKeyOrderAscending
    Call sprdReport.Sort(-1, -1, -1, -1, SortByRow)
        
    Screen.MousePointer = vbNormal
    sprdReport.Redraw = True

End Sub

Private Sub cmbDateCrit_Click()
    
    If cmbDateCrit.ListIndex = -1 Then Exit Sub
    dtxtEndDate.Visible = False
    lblTolbl.Visible = False
    If cmbDateCrit.ItemData(cmbDateCrit.ListIndex) = CRIT_ALL Then
        dtxtStartDate.Visible = False
    Else
        dtxtStartDate.Visible = True
        If cmbDateCrit.ItemData(cmbDateCrit.ListIndex) = CRIT_FROM Then
            dtxtEndDate.Visible = True
            lblTolbl.Visible = True
        End If
    End If
End Sub

Private Sub cmbDateCrit_GotFocus()

    Call SendKeys(KEY_DROPDOWN)

End Sub

Private Sub cmbDateCrit_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If

End Sub

Private Sub cmbSortBy_GotFocus()
    
    Call SendKeys(KEY_DROPDOWN)

End Sub

Private Sub cmdApply_Click()

    Call LoadEANChecks

End Sub
Private Sub cmdApply_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub cmdExit_Click()

    End

End Sub

Private Sub cmdPrint_Click()

Dim strHeader   As String
Dim oSysOptBO   As cSystemOptions
Dim lngLineNo   As Long

    Screen.MousePointer = vbHourglass
    'If printed in PlanoGram order then hide all non Primary Planogram Locations
    
    Set oSysOptBO = goSession.Database.CreateBusinessObject(CLASSID_SYSTEMOPTIONS)
    Call oSysOptBO.IBo_AddLoadField(FID_SYSTEMOPTIONS_StoreNumber)
    Call oSysOptBO.IBo_AddLoadField(FID_SYSTEMOPTIONS_StoreName)
    Call oSysOptBO.LoadMatches

    ' 02/05/03  KVB  remove Printed dd/mm/yy from strHeader
    strHeader = "Store No: " & oSysOptBO.StoreNumber & " " & _
                oSysOptBO.StoreName & "/n/cIncorrect EAN Association Report"
            strHeader = strHeader & "/n/cDated "
            Select Case (cmbDateCrit.ItemData(cmbDateCrit.ListIndex))
                Case (CRIT_ALL): strHeader = strHeader & ": ALL"
                Case (CRIT_FROM): strHeader = strHeader & "From " & dtxtStartDate.Text & " to " & dtxtEndDate.Text
                Case (CRIT_EQUALS): strHeader = strHeader & ": " & dtxtStartDate.Text
                Case Else: strHeader = strHeader & cmbDateCrit.Text & ": " & dtxtStartDate.Text
            End Select
    
    ' /c is Centre text      /fb1 is Font bold on
    sprdReport.PrintHeader = "/c/fb1" & strHeader & "   Ordered by: " & cmbSortBy.Text
    
    sprdReport.PrintJobName = "Incorrect EAN Association Report"
       
    sprdReport.PrintFooter = GetFooter

    sprdReport.PrintOrientation = PrintOrientationLandscape
    
    sprdReport.Refresh
    
    sprdReport.PrintFooter = GetFooter
    sprdReport.PrintRowHeaders = False
    sprdReport.Refresh
    sprdReport.PrintSheet
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub cmdReset_Click()
        
    cmbDateCrit.Enabled = True
    dtxtStartDate.Enabled = True
    dtxtEndDate.Enabled = True
    cmbReasonCode.Enabled = True
    sprdReport.MaxRows = 0
    cmbEntryMethod.Enabled = True
    cmbEntryMethod.ListIndex = 0
    cmbDateCrit.SetFocus
    cmdPrint.Visible = False
End Sub


Private Sub dtxtEndDate_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub dtxtStartDate_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub Form_Load()

Dim oSysDat As cSystemDates
Dim lngOptNo    As Long
Dim lngItemNo   As Long

    Call GetRoot
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    RGBMSGBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
    Call sprdReport.SetOddEvenRowColor(RGB(224, 224, 224), vbBlack, vbWhite, vbBlack)
    
    Call InitialiseStatusBar(sbStatus)
    
    ' Date Combo Box
    ' ==============
    cmbDateCrit.Clear
    Call cmbDateCrit.AddItem("After")
    cmbDateCrit.ItemData(0) = CRIT_GREATERTHAN
    Call cmbDateCrit.AddItem("Before")
    cmbDateCrit.ItemData(1) = CRIT_LESSTHAN
    Call cmbDateCrit.AddItem("Dated")
    cmbDateCrit.ItemData(2) = CRIT_EQUALS
    Call cmbDateCrit.AddItem("All")
    cmbDateCrit.ItemData(3) = CRIT_ALL
    Call cmbDateCrit.AddItem("From")
    cmbDateCrit.ItemData(4) = CRIT_FROM
    
    For lngItemNo = 0 To cmbDateCrit.ListCount Step 1
        If cmbDateCrit.ItemData(lngItemNo) = CRIT_EQUALS Then
            mlngDatedPos = lngItemNo
            cmbDateCrit.ListIndex = lngItemNo
            Exit For
        End If
    Next lngItemNo

    cmbSortBy.ListIndex = 0
    cmbEntryMethod.ListIndex = 0
    Call Me.Show
    DoEvents


    Set oSysDat = goDatabase.CreateBusinessObject(CLASSID_SYSTEMDATES)
    Call oSysDat.LoadMatches

    mdteSystemDate = oSysDat.TodaysDate
    dtxtStartDate.Text = Format(mdteSystemDate, "dd/mm/yy")
    dtxtEndDate.Text = Format(mdteSystemDate, "dd/MM/YY")
    Set oSysDat = Nothing

    Call LoadReasonCodes

    Call DecodeParameters(goSession.ProgramParams)

    If (mblnFromNightlyClose = True) Then
        Call cmdApply_Click
        Call cmdPrint_Click
        End
    End If
    Screen.MousePointer = vbNormal
           
End Sub

Private Sub Form_Resize()

    If (Me.WindowState = vbMinimized) Then Exit Sub
    If (Me.Height < 4300) Then
        Me.Height = 4300
        Exit Sub
    End If
    
    If (Me.Width < 4500) Then
        Me.Width = 4500
        Exit Sub
    End If
    
    sprdReport.Width = Me.Width - sprdReport.Left * 4
    sprdReport.Height = Me.Height - sprdReport.Top - cmdPrint.Height - 720 - sbStatus.Height
    cmdPrint.Top = sprdReport.Top + sprdReport.Height + 120
    cmdExit.Top = cmdPrint.Top
    cmdPrint.Left = sprdReport.Left + sprdReport.Width - cmdPrint.Width
    ucpbProgress.Left = (Me.Width - ucpbProgress.Width) / 2
    ucpbProgress.Top = (Me.Height - 480 - ucpbProgress.Height) / 2

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case (Shift)
        Case (0):
            Select Case (KeyCode)
                Case (vbKeyF3): 'if F3 then reset
                    If (cmdReset.Visible = True) Then Call cmdReset_Click
                Case (vbKeyF7): 'if F9 then retrieve report
                    If (cmdApply.Visible = True) Then Call cmdApply_Click
                Case (vbKeyF9): 'if F9 then print report
                    If (cmdPrint.Visible = True) Then Call cmdPrint_Click
                Case (vbKeyF10): 'if F10 then exit
                    Call cmdExit_Click
            End Select 'Key pressed with no Shift/Alt combination
    End Select
End Sub

    
Private Sub DecodeParameters(strCommand As String)

Const TRAN_START_DATE    As String = "TS"
Const SELECTION_DATE_DB  As String = "DB"  ' This program called from Daily Banking
Const TRAN_END_DATE      As String = "TD"
Const CALLED_FROM        As String = "CF"  'Added 22/10/04 v1.0.11
Const CF_CLOSE           As String = "C"   'Added 22/10/04 v1.0.11


Dim strTempDate  As String
Dim dteTempDate  As Date
Dim strSection   As String
Dim strSignFound As String
Dim vntSection   As Variant
Dim lngSectNo    As Long
Dim lngItem      As Long
Dim blnSetOpts   As Boolean

    vntSection = Split(strCommand, ",")
    
    blnSetOpts = False
    mblnFromNightlyClose = False
    
    For lngSectNo = 0 To UBound(vntSection) Step 1
        'Debug.Print lngSectNo & "-" & vntSection(lngSectNo)
        strSection = vntSection(lngSectNo)
        Select Case (Left$(strSection, 2))
            Case (CALLED_FROM):
                If Mid(strSection, 3) = CF_CLOSE Then
                    mblnFromNightlyClose = True
                    blnSetOpts = True
                End If
        End Select
    Next lngSectNo
    

End Sub


Private Sub LoadEANChecks()

Dim rsStock As ADODB.Recordset
Dim lngRowNo    As Long
Dim lngComp     As Long
Dim lngRCodeNo  As Long

Dim dteDateValue    As Date
Dim cn As New ADODB.Connection
Dim strFilter As String
Dim count As Integer
Dim xxstrfilter As String

    
    ucpbProgress.Visible = True
    ucpbProgress.Value = 0
    ucpbProgress.Caption1 = "Retrieving EANs"
    ucpbProgress.Refresh
    sprdReport.MaxRows = 0
    
    strFilter = ""

    'Set Date criteria
    If cmbDateCrit.ItemData(cmbDateCrit.ListIndex) <> CRIT_ALL Then
        dteDateValue = GetDate(dtxtStartDate.Text)
        If Year(dteDateValue) > 1899 Then
            Select Case (cmbDateCrit.ItemData(cmbDateCrit.ListIndex))
                Case Is = CRIT_LESSTHAN
                   ' lngComp = CMP_LESSEQUALTHAN
                    strFilter = "Date1 < '" & Format(dteDateValue, "dd/MM/YY") & "'"
                Case CRIT_GREATERTHAN, CRIT_FROM
                    'lngComp = CMP_GREATEREQUALTHAN
                      strFilter = "Date1 > '" & Format(dteDateValue, "dd/MM/YY") & "'"
                Case Else
                    'lngComp = CMP_EQUAL
                    strFilter = "Date1 = '" & Format(dteDateValue, "dd/MM/YY") & "'"
            End Select
            'Call oEANCheckBO.IBo_AddLoadFilter(lngComp, FID_EAN_CHECK_TranDate, dteDateValue)
        End If
        If (cmbDateCrit.ItemData(cmbDateCrit.ListIndex) = CRIT_FROM) Then
            'Call oEANCheckBO.IBo_AddLoadFilter(CMP_LESSEQUALTHAN, FID_EAN_CHECK_TranDate, GetDate(dtxtEndDate.Text))
            strFilter = "date1 >= '" & Format(dteDateValue, "dd/MM/YY") & "' and date1 <= '" & Format(dtxtEndDate.Text, "dd/MM/YY") & "'"
        End If
    End If
    
    If (cmbEntryMethod.ListIndex <> 0) Then
        'Call oEANCheckBO.IBo_AddLoadFilter(CMP_EQUAL, FID_EAN_CHECK_EntryMethod, cmbEntryMethod.Text)
        strFilter = "entry = '" & cmbEntryMethod.Text & "'"
    End If
    
    If (Val(cmbReasonCode.ItemData(cmbReasonCode.ListIndex)) <> 0) Then
        strFilter = "reasoncode = " & Format(cmbReasonCode.ItemData(cmbReasonCode.ListIndex), "00")
    End If
    
   Set records = Nothing
   Set records = goDatabase.ExecuteCommand("select * from EANCHK")
   records.Filter = strFilter
   rectotal = 0
   Do While Not records.EOF
        rectotal = rectotal + 1
        records.MoveNext
   Loop
      
   If rectotal > 0 Then
   count = 0
   records.MoveFirst
   Do While Not records.EOF
                  
   ' For lngRowNo = 1 To colEANCheck.Count Step 1
        ucpbProgress.Value = count
        count = count + 1
'        Set oEANCheckBO = colEANCheck(lngRowNo)
        sprdReport.MaxRows = rectotal
        sprdReport.Row = count
        sprdReport.Col = COL_DATE
        sprdReport.Text = DisplayDate(records.Fields.Item("date"), False)
        sprdReport.Col = COL_TIME
        sprdReport.Text = Left$(records.Fields.Item("time"), 2) & ":" & Mid$(records.Fields.Item("Time"), 3)
        sprdReport.Col = COL_TILLID
        sprdReport.Text = records.Fields.Item("till")
        sprdReport.Col = COL_TRANNO
        sprdReport.Text = records.Fields.Item("tran1")
        sprdReport.Col = COL_USERID
        sprdReport.Text = records.Fields.Item("seqn")
        sprdReport.Col = COL_ENTRY
        sprdReport.Text = records.Fields.Item("entry")
        sprdReport.Col = COL_CODE
        For lngRCodeNo = 0 To cmbReasonCode.ListCount - 1 Step 1
            If (Val(records.Fields.Item("reasoncode")) = cmbReasonCode.ItemData(lngRCodeNo)) Then
                sprdReport.Text = cmbReasonCode.List(lngRCodeNo)
                Exit For
            End If
        Next lngRCodeNo
        sprdReport.Col = COL_VALUE
        sprdReport.Text = records.Fields.Item("item")
        sprdReport.Col = COL_EAN_FOUND
        sprdReport.Text = IIf(records.Fields.Item("eafound"), "Y", "N")
        sprdReport.Col = COL_EAN_SKU
        sprdReport.Text = records.Fields.Item("easkun")
        sprdReport.Col = COL_EAN_SKU_FND
        sprdReport.Text = IIf(records.Fields.Item("easkunfound"), "Y", "N")
        Set rsStock = New ADODB.Recordset
        Call rsStock.Open("SELECT SKUn,DESCR FROM STKMAS WHERE SKUN='" & records.Fields.Item("easkun") & "'", mycon)
        sprdReport.Col = COL_SKU
        sprdReport.Text = rsStock.Fields("SKUN") & "-" & rsStock.Fields("DESCR")
        rsStock.Close
        sprdReport.Col = COL_SORTDATE
        sprdReport.Text = Format(records.Fields.Item("date"), "YYYYMMDD") & records.Fields.Item("time")
        sprdReport.Col = COL_STORE_NO
        sprdReport.Text = records.Fields.Item("stor")
    
        records.MoveNext
        Loop
   End If
    
    sprdReport.ColWidth(COL_SKU) = sprdReport.MaxTextColWidth(COL_SKU)
    sprdReport.ColWidth(COL_VALUE) = sprdReport.MaxTextColWidth(COL_VALUE)
    ucpbProgress.Visible = False
    If (sprdReport.MaxRows > 0) Then
        cmbDateCrit.Enabled = False
        dtxtStartDate.Enabled = False
        dtxtEndDate.Enabled = False
        cmbReasonCode.Enabled = False
        cmbEntryMethod.Enabled = False
        sprdReport.Visible = True
        cmdPrint.Visible = True
        sprdReport.SetFocus
    Else
        Call MsgBoxEx("No EAN's found for specified criteria", vbOKOnly, "Retrieve Records", , , , , RGBMSGBox_PromptColour)
        Call cmdReset_Click
    End If
    
    Call cmbSortBy_Click
    Screen.MousePointer = vbNormal

End Sub

