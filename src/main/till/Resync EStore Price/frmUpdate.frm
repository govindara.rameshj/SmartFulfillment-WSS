VERSION 5.00
Begin VB.Form frmUpdate 
   Caption         =   "Topps - Create DLTOTS"
   ClientHeight    =   5610
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6150
   Icon            =   "frmUpdate.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5610
   ScaleWidth      =   6150
   StartUpPosition =   1  'CenterOwner
   Begin VB.Label lblStockTake 
      BackStyle       =   0  'Transparent
      Height          =   735
      Left            =   240
      TabIndex        =   1
      Top             =   4320
      Width           =   5535
   End
   Begin VB.Label lblStatus 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3735
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   5895
   End
End
Attribute VB_Name = "frmUpdate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim madoConn        As ADODB.Connection
Dim moFSO            As New FileSystemObject
Dim mstrErrorPath   As String

Private Sub Update()

Dim rsWOrders    As New Recordset
Dim rsWOrdLines  As New Recordset
Dim strSql       As String
Dim lngCount    As Long
Dim tsOrder As TextStream
Dim strData As String
Dim strSKU  As String
Dim curPrice As Currency
Dim intQty As Integer

On Error GoTo UpdateError

    Set madoConn = New ADODB.Connection
    Call madoConn.Open("OASYS")
    'create FileSystemObject
    
    lngCount = 0
    
    Call rsWOrders.Open("SELECT * from WEBTOTS WHERE ORDN BETWEEN '139548' and '139648'", madoConn)
                
    lblStatus.Caption = "Accessing List Of Open Web Orders"
    Me.Refresh
    GetErrorPath
    'read the context of the text file and add it to the text hold variable
    While Not rsWOrders.EOF
        'Open XML File and get Item price, Quantity and SKU for 1st line
        Set tsOrder = moFSO.OpenTextFile(mstrErrorPath & "\" & rsWOrders.Fields("ORDN") & "00.xml", ForReading, False)
        strData = tsOrder.ReadAll
        tsOrder.Close
        strData = Mid$(strData, InStr(strData, "<quantity>") + 10)
        intQty = Val(Left$(strData, InStr(strData, "<") - 1))
        strData = Mid$(strData, InStr(strData, "<item>") + 6)
        strSKU = Left$(strData, InStr(strData, "<") - 1)
        strData = Mid$(strData, InStr(strData, "<sellingprice>") + 14)
        curPrice = Val(Left$(strData, InStr(strData, "<") - 1))
        curPrice = Round(curPrice / intQty, 2)
        'Get Web Transaction Line
        Call rsWOrdLines.Open("SELECT PRIC,SKUN from WEBLINE WHERE Date1='" & Format(rsWOrders.Fields("DATE1"), "YYYY-MM-DD") & "' AND Till='" & rsWOrders.Fields("TILL") & "' AND TRAN='" & rsWOrders.Fields("TRAN") & "' AND NUMB=1", madoConn)
        'Check if SKU matches but prices do not
        If (rsWOrdLines("SKUN") = strSKU) Then
            If rsWOrdLines("PRIC") <> curPrice Then
                'Reset all prices and values on line
                lblStatus.Caption = "Updating WEBLINE= " & rsWOrders.Fields("ORDN")
                strSql = "Date1='" & Format(rsWOrders.Fields("DATE1"), "YYYY-MM-DD") & "' AND Till='" & rsWOrders.Fields("TILL") & "' AND TRAN='" & rsWOrders.Fields("TRAN") & "'"
                Call madoConn.Execute("UPDATE WEBLINE SET dgpd=0,dgme='000000',pric=" & curPrice & ",prve=0,Porc=0,popd=0,extp=0,SPARE='SYS' WHERE " & strSql & " AND NUMB=1")
                Call madoConn.Execute("UPDATE WEBLINE SET PORC=10,popd=SPRI-PRIC,EXTP=(PRIC*QUAN) WHERE " & strSql & " AND NUMB=1")
                Call madoConn.Execute("UPDATE WEBLINE SET PRVE=EXTP/1.15 WHERE " & strSql & " AND NUMB=1")
                Call madoConn.Execute("UPDATE WEBLINE SET VATV=EXTP-PRVE WHERE " & strSql & " AND NUMB=1")
                Call rsWOrdLines.Close
                'get Line totals and apply to header
                Call rsWOrdLines.Open("SELECT SUM(EXTP) as EXTP,SUM(PRVE) AS PRVE from WEBLINE WHERE Date1='" & Format(rsWOrders.Fields("DATE1"), "YYYY-MM-DD") & "' AND Till='" & rsWOrders.Fields("TILL") & "' AND TRAN='" & rsWOrders.Fields("TRAN") & "'", madoConn)
                Call madoConn.Execute("UPDATE WEBTOTS SET MERC=" & rsWOrdLines("EXTP") & ",TAXA=" & rsWOrdLines("EXTP") - rsWOrdLines("PRVE") & ",TOTL=" & rsWOrdLines("EXTP") & ",XVAT1=" & rsWOrdLines("PRVE") & ",VATV1=" & rsWOrdLines("EXTP") - rsWOrdLines("PRVE") & _
                        ",SPARE='SYS' WHERE " & strSql)
                Call rsWOrdLines.Close
                Call rsWOrdLines.Open("SELECT DCVD from WEBTOTS WHERE " & strSql, madoConn)
                'If dispatched then update DLTOTS and DLLINE
                If (IsNull(rsWOrdLines("DCVD")) = False) Then
                    Call rsWOrdLines.Close
                    Call rsWOrdLines.Open("SELECT DATE1,TILL,TRAN from DLTOTS WHERE ORDN='" & rsWOrders.Fields("ORDN") & "'", madoConn)
                    strSql = "Date1='" & Format(rsWOrdLines.Fields("DATE1"), "YYYY-MM-DD") & "' AND Till='" & rsWOrdLines.Fields("TILL") & "' AND TRAN='" & rsWOrdLines.Fields("TRAN") & "'"
                    Call madoConn.Execute("UPDATE DLLINE SET dgpd=0,dgme='000000',pric=" & curPrice & ",prve=0,Porc=0,popd=0,extp=0,SPARE='SYS' WHERE " & strSql & " AND NUMB=1")
                    Call madoConn.Execute("UPDATE DLLINE SET PORC=10,popd=SPRI-PRIC,EXTP=(PRIC*QUAN) WHERE " & strSql & " AND NUMB=1")
                    Call madoConn.Execute("UPDATE DLLINE SET PRVE=EXTP/1.15 WHERE " & strSql & " AND NUMB=1")
                    Call madoConn.Execute("UPDATE DLLINE SET VATV=EXTP-PRVE WHERE " & strSql & " AND NUMB=1")
                    'Update line totals onto Header
                    Call rsWOrdLines.Close
                    'get Line totals and apply to header
                    Call rsWOrdLines.Open("SELECT SUM(EXTP) as EXTP,SUM(PRVE) AS PRVE from DLLINE WHERE " & strSql, madoConn)
                    Call madoConn.Execute("UPDATE DLTOTS SET MERC=" & rsWOrdLines("EXTP") & ",TAXA=" & rsWOrdLines("EXTP") - rsWOrdLines("PRVE") & ",TOTL=" & rsWOrdLines("EXTP") & ",XVAT1=" & rsWOrdLines("PRVE") & ",VATV1=" & rsWOrdLines("EXTP") - rsWOrdLines("PRVE") & _
                            ",SSEQ='9999' WHERE " & strSql)
                End If
            End If
        End If
        Call rsWOrdLines.Close
        Me.Refresh
        'Preserve cost on STKADJ First
        Call rsWOrders.MoveNext
    Wend
    
    'close the text file
    Call rsWOrders.Close
    
    
    Call madoConn.Close
    lblStatus.Caption = lblStatus.Caption & "Update Complete"
    
    Exit Sub
    
UpdateError:
    Call MsgBox(Err.Description)
    Call Err.Clear
    
    
End Sub 'Update

Private Sub GetErrorPath()
   
Const EORDER_REGKEY As String = "Software\CTS Retail\EOrder"
    
    mstrErrorPath = GetRegSetting(EORDER_REGKEY, "ErrorPath", "", HKEY_LOCAL_MACHINE)
    
    If (mstrErrorPath = "") Then Call Err.Raise(1, "InitialiseRegistry", "ErrorPath - key in registry not set" & vbCrLf & "Exiting System")
    
    If (Right$(mstrErrorPath, 1) <> "\") Then mstrErrorPath = mstrErrorPath & "\"
    
    Set moFSO = New FileSystemObject
    If (moFSO.FolderExists(mstrErrorPath) = False) Then Call Err.Raise(1, "InitialiseRegistry", "ErrorPath - path in registry not valid ('" & mstrErrorPath & "')" & vbCrLf & "Exiting System")
    
    Set moFSO = Nothing

End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
Dim oFSO As New FileSystemObject
Dim tsLogFile As TextStream
    
    Me.Show
    Call Update
    Set tsLogFile = oFSO.OpenTextFile(App.Path & "\" & App.EXEName & ".LOG", ForAppending, True)
    Call tsLogFile.Close
    End

End Sub 'form_load

