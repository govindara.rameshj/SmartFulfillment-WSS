VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "HistoryImportFrmFctry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private m_InitialisedUseLongerTokenIDImplementation As Boolean
Private m_UseLongerTokenIDImplementation As Boolean

Private Const ImplementationParameterLongerTokenID As Long = 980870

Friend Property Get UseLongerTokenIDImplementation() As Boolean

    UseLongerTokenIDImplementation = m_UseLongerTokenIDImplementation
End Property

Public Function Initialise(ByVal oSession As Session)

    Call GetUseLongerTokenIDParameterValue(oSession)
End Function

Public Function GetTokenIDLengthImplementation() As ITokenIDLength
    
    If UseLongerTokenIDImplementation Then
        'using implementation with 870 fix
        Set GetTokenIDLengthImplementation = New UseLongerTokenIDLength
    Else
        'using live implementation
        Set GetTokenIDLengthImplementation = New HistoryImportFormLive
    End If
End Function

Friend Sub GetUseLongerTokenIDParameterValue(ByVal oSession As Session)

    If Not m_InitialisedUseLongerTokenIDImplementation Then
On Error Resume Next
        m_UseLongerTokenIDImplementation = oSession.GetParameter(ImplementationParameterLongerTokenID)
        m_InitialisedUseLongerTokenIDImplementation = True
On Error GoTo 0
    End If
End Sub

