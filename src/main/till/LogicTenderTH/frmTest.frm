VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   795
      Left            =   900
      TabIndex        =   0
      Top             =   960
      Width           =   1035
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()


    Dim objLT As LogicTender.CCTender
    Dim objTenderResult As TranResult
    Dim bResult As Boolean
    
    Set objLT = New LogicTender.CCTender
    
    ' Incoming Parameters
    ' ===================
    ' Name                Type        Description
    ' ------------------- ----------- ---------------------------------------
    ' IsSale              Boolean     Transaction Type, True if Sale, False for Refund
    ' TransactionAmount   Integer     Transaction amount in Pence
    ' Customer Present    Boolean     Indicates if customer present, if not then system must preset Card Number entry
    ' AllowCashback       Boolean     Indicate if Cashback is available on Card, whether to provide facility.
    ' IsCheque            Boolean     Indicates if payment is a cheque.  If True, then display entry of Cheque details
    ' PrintFormat         String      This will be how the receipt will be printed either as SLIP or A4
    ' PrintLayout         String      This will be the format that the confirmation slip will be printed in.
    ' CashierName         String      @@@ NEW PARAMETER @@@
    
    
    MsgBox (objLT.AuthoriseEFTPayment(True, 10023, True, False, False, "A4", "A4", "Cashier Name", objTenderResult))
    

End Sub
