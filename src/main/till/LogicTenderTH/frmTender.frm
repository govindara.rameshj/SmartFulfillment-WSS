VERSION 5.00
Begin VB.Form frmTender 
   BackColor       =   &H80000009&
   Caption         =   "Record tender details"
   ClientHeight    =   6855
   ClientLeft      =   2715
   ClientTop       =   1275
   ClientWidth     =   7395
   ControlBox      =   0   'False
   Icon            =   "frmTender.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6855
   ScaleWidth      =   7395
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdDecline 
      Caption         =   "Decline"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4320
      TabIndex        =   46
      Top             =   6240
      Width           =   2895
   End
   Begin VB.CommandButton cmdAuthorise 
      Caption         =   "Authorise"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   45
      Top             =   6240
      Width           =   2895
   End
   Begin VB.Frame fraEntry 
      Caption         =   "Frame1"
      Height          =   6255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7095
      Begin VB.PictureBox fraBorder 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   4455
         Left            =   0
         ScaleHeight     =   4425
         ScaleWidth      =   7065
         TabIndex        =   6
         Top             =   1200
         Width           =   7095
         Begin VB.PictureBox fraAuthNum 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   735
            Left            =   120
            ScaleHeight     =   735
            ScaleWidth      =   6855
            TabIndex        =   7
            Top             =   3600
            Width           =   6855
            Begin VB.TextBox txtAuthNum 
               BackColor       =   &H00C0FFFF&
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   3240
               MaxLength       =   6
               TabIndex        =   23
               Top             =   180
               Width           =   1815
            End
            Begin VB.Label lblAuthNum 
               BackStyle       =   0  'Transparent
               Caption         =   "Authorisation No"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   240
               TabIndex        =   22
               Top             =   180
               Width           =   3015
            End
         End
         Begin VB.PictureBox fraChequeCard 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   3675
            Left            =   120
            ScaleHeight     =   3675
            ScaleWidth      =   6855
            TabIndex        =   24
            Top             =   -120
            Visible         =   0   'False
            Width           =   6855
            Begin VB.TextBox txtEndYear 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   4320
               MaxLength       =   2
               TabIndex        =   15
               Top             =   1680
               Width           =   735
            End
            Begin VB.TextBox txtEndMonth 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   3240
               MaxLength       =   2
               TabIndex        =   14
               Top             =   1680
               Width           =   735
            End
            Begin VB.TextBox txtStartYear 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   4320
               MaxLength       =   2
               TabIndex        =   12
               Top             =   960
               Width           =   735
            End
            Begin VB.TextBox txtCardNo 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   2280
               MaxLength       =   19
               TabIndex        =   9
               Top             =   240
               Width           =   4575
            End
            Begin VB.TextBox txtStartMonth 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   3240
               MaxLength       =   2
               TabIndex        =   11
               Top             =   960
               Width           =   735
            End
            Begin VB.TextBox txtIssueNo 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   555
               Left            =   2280
               MaxLength       =   3
               TabIndex        =   17
               Top             =   2400
               Width           =   975
            End
            Begin VB.TextBox txtCustPresent 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   555
               Left            =   5760
               MaxLength       =   2
               TabIndex        =   19
               Text            =   "Y"
               Top             =   2400
               Width           =   495
            End
            Begin VB.TextBox txtCVV 
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   555
               Left            =   2280
               MaxLength       =   3
               TabIndex        =   21
               Top             =   3120
               Width           =   975
            End
            Begin VB.Label Label8 
               BackStyle       =   0  'Transparent
               Caption         =   "End Month / Year"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   13
               Top             =   1680
               Width           =   2895
            End
            Begin VB.Label Label7 
               BackStyle       =   0  'Transparent
               Caption         =   "Card No"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   8
               Top             =   240
               Width           =   1935
            End
            Begin VB.Label Label6 
               BackStyle       =   0  'Transparent
               Caption         =   "Start Month / Year"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   10
               Top             =   960
               Width           =   3015
            End
            Begin VB.Label Label5 
               BackStyle       =   0  'Transparent
               Caption         =   "Issue No"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   16
               Top             =   2400
               Width           =   1935
            End
            Begin VB.Label Label4 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Cust Present"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   435
               Left            =   3540
               TabIndex        =   18
               Top             =   2400
               Width           =   2055
            End
            Begin VB.Label Label9 
               BackStyle       =   0  'Transparent
               Caption         =   "CVV"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   20
               Top             =   3120
               Width           =   1935
            End
         End
      End
      Begin VB.PictureBox fraChequeType 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   1215
         Left            =   0
         ScaleHeight     =   1215
         ScaleWidth      =   7095
         TabIndex        =   1
         Top             =   660
         Visible         =   0   'False
         Width           =   7095
         Begin VB.CommandButton cmdNoTransaxChq 
            Caption         =   "N-No Transax"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   4800
            TabIndex        =   4
            Top             =   0
            Width           =   2300
         End
         Begin VB.CommandButton cmdCompanyChq 
            Caption         =   "C-Company"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   2400
            TabIndex        =   3
            Top             =   0
            Width           =   2300
         End
         Begin VB.CommandButton cmdPersonalChq 
            Caption         =   "P-Personal"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   0
            TabIndex        =   2
            Top             =   0
            Width           =   2300
         End
         Begin VB.CommandButton cmdEscCheque 
            Caption         =   "Esc-Cancel"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   5040
            TabIndex        =   5
            Top             =   600
            Width           =   2055
         End
      End
      Begin VB.PictureBox fraAccountNo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   2415
         Left            =   0
         ScaleHeight     =   2385
         ScaleWidth      =   7065
         TabIndex        =   25
         Top             =   1320
         Visible         =   0   'False
         Width           =   7095
         Begin VB.TextBox txtChequeNo 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   2280
            MaxLength       =   6
            TabIndex        =   27
            Top             =   240
            Width           =   1455
         End
         Begin VB.TextBox txtSortCode 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   2280
            MaxLength       =   6
            TabIndex        =   29
            Top             =   960
            Width           =   1455
         End
         Begin VB.TextBox txtAccountNo 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   2280
            MaxLength       =   10
            TabIndex        =   31
            Top             =   1680
            Width           =   3015
         End
         Begin VB.Label Label1 
            BackStyle       =   0  'Transparent
            Caption         =   "Cheque No"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   240
            TabIndex        =   26
            Top             =   240
            Width           =   1935
         End
         Begin VB.Label Label2 
            BackStyle       =   0  'Transparent
            Caption         =   "Sort Code"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   240
            TabIndex        =   28
            Top             =   960
            Width           =   1695
         End
         Begin VB.Label Label3 
            BackStyle       =   0  'Transparent
            Caption         =   "Account No"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   240
            TabIndex        =   30
            Top             =   1680
            Width           =   1935
         End
      End
      Begin VB.PictureBox fraComplete 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   2295
         Left            =   0
         ScaleHeight     =   2265
         ScaleWidth      =   7065
         TabIndex        =   32
         Top             =   1200
         Visible         =   0   'False
         Width           =   7095
         Begin VB.CommandButton cmdConfirm 
            Caption         =   "&Yes"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   120
            TabIndex        =   35
            Top             =   1320
            Width           =   1815
         End
         Begin VB.CommandButton cmdCancel 
            Caption         =   "&No"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   5160
            TabIndex        =   34
            Top             =   1320
            Width           =   1815
         End
         Begin VB.CommandButton cmdExit 
            Caption         =   "EXIT"
            Height          =   495
            Left            =   2760
            TabIndex        =   33
            Top             =   1200
            Width           =   975
         End
         Begin VB.Label lblPrintPrompt 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Insert Cheque - Esc Cancel"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   120
            TabIndex        =   36
            Top             =   240
            Visible         =   0   'False
            Width           =   6855
         End
         Begin VB.Label lblPrintChq 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Print front of cheque?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   120
            TabIndex        =   37
            Top             =   2160
            Visible         =   0   'False
            Width           =   6975
         End
      End
      Begin VB.PictureBox fraInsertCard 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   2535
         Left            =   0
         ScaleHeight     =   2505
         ScaleWidth      =   7065
         TabIndex        =   38
         Top             =   1200
         Visible         =   0   'False
         Width           =   7095
         Begin VB.Timer tmrESocket 
            Left            =   0
            Top             =   0
         End
         Begin VB.Label lblInsert 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1695
            Left            =   240
            TabIndex        =   39
            Top             =   120
            Width           =   6615
         End
      End
      Begin VB.Label lblStatus 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H0000FFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   0
         TabIndex        =   44
         Top             =   5760
         Width           =   7095
      End
      Begin VB.Label lblActionRequired 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Type of Cheque"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   0
         TabIndex        =   43
         Top             =   0
         Visible         =   0   'False
         Width           =   7095
      End
      Begin VB.Label lblChequeType 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   0
         TabIndex        =   42
         Top             =   600
         Visible         =   0   'False
         Width           =   7095
      End
      Begin VB.Label lblAuthTranMod 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Authorisation Server Mode: "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   0
         TabIndex        =   41
         Top             =   720
         Visible         =   0   'False
         Width           =   7095
      End
      Begin VB.Label lblAction 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Enter Credit Card"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   615
         Left            =   0
         TabIndex        =   40
         Top             =   60
         Visible         =   0   'False
         Width           =   7095
      End
   End
End
Attribute VB_Name = "frmTender"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const MODULE_NAME As String = "frmTender"

Const RGBMsgBox_WarnColour As Long = vbRed
Const RGBMsgBox_PromptColour As Long = vbYellow
Const RGBEdit_Colour As Long = 16777152
Const RGB_WHITE As Long = vbWhite

Dim mlngBackColor As Long

' Types of Sale
Const TT_SALE       As String = "SA"
Const TT_REFUND     As String = "RF"
Const TT_SIGNON     As String = "CO"
Const TT_COLLECT    As String = "IC"
Const TT_SIGNOFF    As String = "CC"
Const TT_OPENDRAWER As String = "OD"
Const TT_LOGO       As String = "RL"
Const TT_XREAD      As String = "XR"
Const TT_ZREAD      As String = "ZR"
Const TT_MISCIN     As String = "M+"
Const TT_MISCOUT    As String = "M-"
Const TT_RECALL     As String = "RP"
Const TT_QUOTE      As String = "QD"
Const TT_EXT_SALE   As String = "ES"
Const TT_TELESALES  As String = "TS"
Const TT_TELEREFUND As String = "TR"
Const TT_REPRINTREC As String = "RR"
Const TT_LOOKUP     As String = "LU"
Const TT_CORRIN     As String = "C+"
Const TT_CORROUT    As String = "C-"
Const TT_EAN_CHECK  As String = "EC"

Const RGB_LTYELLOW As Long = 12648447

Enum enCaptureMode
    encmCheque = 1
    encmCCard = 2
    encmAuthCard = 3
    encmKeyed = 4
End Enum

Enum enConfirmMode
    encmReprintSlip = 1
    encmSignatureCon = 2
    encmNon = 3
End Enum

Const VERSION_NUM                   As String = "1.0.1"

'Constants to hold credit card entry modes
Const ENTRY_ICCS                    As String = "ICC Signed"
Const ENTRY_ICCP                    As String = "ICC"
Const ENTRY_ICC_PIN_BYPASS          As String = "ICC Pin Bypass"
Const ENTRY_KEY                     As String = "Keyed"
Const ENTRY_SWIPE                   As String = "Swipe"
'Responces returned from modPayware.InitiliseTransaction
Const READ_ICC                      As Integer = 0
Const READ_EMV                      As Integer = 30
Const MANUAL_ENTRY                  As String = "Manual Entry"
'Cheque types
Const CHEQUE_PERSONAL               As String = "Personal"
Const CHEQUE_COMP                   As String = "Company"
Const CHEQUE_NOTRANSAX              As String = "No Transax"

'Constants refering to the authorisation server
Const AUTH_ONLINE                   As String = "Online"
Const AUTH_OFFLINE                  As String = "Offline"
Const AUTHORISED_BY_PED             As String = "PED"
Const AUTH_SERVER_FOUND             As String = "0"
Const AUTH_SERVER_NOT_FOUND         As String = "1"
Const PRM_AUTHTMODE                 As Long = "827"
Const PRM_CARREG                    As Long = "808"

Const SELECT_CHEQUE_MSG             As String = "Press P for personal cheque" & vbCrLf & _
        "Press C for company cheque" & vbCrLf & _
        "Press N for No Transax" & vbCrLf & "Or press escape to cancel"

Private enEntryMode                 As enCaptureMode
Private enConfirmationMode          As enConfirmMode
Private moOPOSPrinter               As Object

Private mTOSType_Code   As String
Private moTranHeader     As Object
Private goSession As Object
Private goRoot As Object
Private goDatabase As Object

Dim mblnRefund                      As Boolean
Dim mblnCNPTill                     As Boolean

Dim mlngUseEFTPOS                   As Long
Dim mblnAuthEntered                 As Boolean
Dim mstrTransactionAmount           As String
Dim mdteTransactionDate             As Date
Dim mstrTransactionNo               As String
Dim mstrCreditCardNum               As String
Dim mstrExpiryDate                  As String
Dim mstrStartDate                   As String
Dim mstrIssueNum                    As String
Dim mstrAuthNum                     As String
Dim mstrTrack2                      As String
Dim mstrTmode                       As String
Dim mstrAuthResponse                As String
Dim mstrAuthType                    As String
Dim mstrTerminalVerificationResult  As String
Dim mstrTransactionStatusInfo       As String
Dim mstrAuthResponceCode            As String
Dim mstrUnpredictableNumber         As String
Dim mstrApplicationIdentifier       As String
Dim mstrCashierName                 As String
Dim mstrEntryMethod                 As String
Dim mstrVerification                As String
Dim mstrAuthSuccess                 As String
Dim mstrAuthServerFound             As String
Dim mstrTransactionType             As String
Dim mstrCaptureCarReg               As String
Dim mstrCarReg                      As String
Dim mstrChequeGuaranteeValue        As String
Dim mstrChequeNo                    As String
Dim mstrSortCode                    As String
Dim mstrAccountNo                   As String
Dim mstrChequeType                  As String
Dim mstrEFTTransID                  As String
Dim mlngAttemptNum                  As Long
Dim mstrFallBackType                As String
Dim mstrEFTTranID                   As String
Dim mstrTransactionSource           As String

Dim mblnInitiateCheque     As Boolean


' eSocket variables
Const ERR_CONNECTION As Long = 10061
 


Dim mstrResponseType                        As String
Dim mblnSendComplete                        As Boolean
Dim mblnCardDetailsEntered                  As Boolean
Dim mstrData                                As String
Dim mstrCardProductName                     As String
Dim mstrSignatureRequired                   As String
Dim mstrPanEntryMode                        As String
Dim mstrEmvAmount                           As String
Dim mstrEmvAmountOther                      As String
Dim mstrEmvApplicationIdentifier            As String
Dim mstrEmvApplicationInterchangeProfile    As String
Dim mstrEmvApplicationLabel                 As String
Dim mstrEmvApplicationTransactionCounter    As String
Dim mstrEmvApplicationUsageControl          As String
Dim mstrEmvApplicationVersionNumber         As String
Dim mstrEmvAuthorizationResponseCode        As String
Dim mstrEmvCryptogram                       As String
Dim mstrEmvCryptogramInformationData        As String
Dim mstrEmvCvmResults                       As String
Dim mstrEmvIssuerApplicationData            As String
Dim mstrEmvTerminalCapabilities             As String
Dim mstrEmvTerminalCountryCode              As String
Dim mstrEmvTerminalType                     As String
Dim mstrEmvTerminalVerificationResult       As String
Dim mstrEmvTransactionCurrencyCode          As String
Dim mstrEmvTransactionDate                  As String
Dim mstrEmvTransactionStatusInformation     As String
Dim mstrEmvTransactionType                  As String
Dim mstrEmvUnpredictableNumber              As String
Dim mstrIACDefault                          As String
Dim mstrCardSequenceNum                     As String
Dim mstrIACDenial                           As String
Dim mstrIACOnline                           As String
Dim mstrTID                                 As String
Dim mstrMerchantID                          As String
Dim mblnCustPresent                         As Boolean

Dim mblnShowNumPad   As Boolean
Dim mblnUsingNumPad  As Boolean 'flag if using Number pad for screen resizing
Dim mblnResizedKeyPad As Boolean
Dim mblnProcessCheque As Boolean

Dim mTranResult     As TranResult
Dim mPrintFormat    As String
Dim mPrintLayout    As String
Dim mTranAmount     As Long
Dim mIsSale         As Boolean
Dim mAllowCashback  As Boolean

Public Event Duress()

Const RI_STATUS_CODE As String = "S"
Const RI_OK_CODE As String = "0"
Const RI_AUTH_ONLINE_CODE As String = "1"
Const RI_AUTH_TERMINAL_CODE As String = "2"
Const RI_AUTH_MANUAL_CODE As String = "3"
Const RI_DECLINE_CODE As String = "4"
Const RI_CANCEL_CODE As String = "5"
Const RI_REFERRAL_CODE As String = "6"
Const RI_MANAUTH_CODE As String = "7"
Const RI_SIGNATURE_CODE As String = "8"
Const RI_PHONE_CODE As String = "9"


Public Function AuthoriseEFTPayment(TranHeader As Object, _
                                    POSPrinter As Object, _
                                    IsSale As Boolean, _
                                    TransactionAmount As Long, _
                                    CustomerPresent As Boolean, _
                                    AllowCashback As Boolean, _
                                    IsCheque As Boolean, _
                                    PrintFormat As String, _
                                    PrintLayout As String, _
                                    ByRef blnCheckCNP As Boolean, _
                                    CashierName As String, _
                                    ByRef TransactionResult As TranResult) As Boolean

Dim AuthString As String
Dim NotValidResponse As Boolean

    On Error GoTo AuthoriseEFTError
    
    Set moOPOSPrinter = POSPrinter
    Set moTranHeader = TranHeader
'    mTOSType_Code = moTranHeader.TransactionCode
    
    mPrintFormat = PrintFormat
    mPrintLayout = PrintLayout
    mTranAmount = TransactionAmount
    mIsSale = IsSale
    mAllowCashback = AllowCashback
    mstrCashierName = CashierName
    
    fraBorder.Visible = False
    fraInsertCard.Visible = True
    lblStatus.Visible = True
    lblAction.Visible = True
    txtAuthNum.BackColor = RGBEdit_Colour
    txtIssueNo.BackColor = RGBEdit_Colour
    
    mblnCNPTill = Not CustomerPresent 'if Customer not present then switch on Keying
        
    If (IsSale = False) Then mblnCNPTill = False 'only allow CNP if sales
    
    'SWitch into CNP Till mode if required
    If (mblnCNPTill = True) Then
        mstrEntryMethod = ENTRY_KEY
        Call TypeCCDetails
        txtCVV.Enabled = True
        txtCustPresent.Text = "N"
        CustomerPresent = False
        txtCustPresent.Enabled = False
        blnCheckCNP = True
        txtCardNo.SetFocus
        mblnCardDetailsEntered = False
        Do
            DoEvents
        Loop Until mblnCardDetailsEntered = True
        TransactionResult = mTranResult
        Exit Function
    End If
        
    Me.Show (vbModal)
    TransactionResult = mTranResult
        
    DoEvents
    Exit Function
    
AuthoriseEFTError:

    Call MsgBox("Error has occurred in EFT system" & vbNewLine & Err.Number & ":" & Err.Description, vbExclamation, "Error Detected")
    Call Err.Clear
         
End Function

'************************************************************************
'* Procedure to perform the comms to the PinPad and obtain authorisation
'************************************************************************
Private Sub PerformAuth(TranAmount As Long, Keyed As Boolean, IsSale As Boolean, CustomerPresent As Boolean, AllowCashback As Boolean, ByRef tRes As TranResult)

Dim PayObject As Object
             
    With PayObject.Trans.Card
        tRes.AcquirerIdentifier = .AcquirerIdentifier
        tRes.AcquirerName = .AcquirerName
        tRes.ActionCodeDefault = .ActionCodeDefault
        tRes.AuthCode = PayObject.Trans.AuthCode
        tRes.ActualverificationMethod = PayObject.Trans.ActualverificationMethod
        tRes.ApplicationID = .ApplicationID
        tRes.ApplicationLabel = .ApplicationLabel
        tRes.ApplicationName = .ApplicationName
        tRes.AuthorisedAmount = PayObject.Trans.AuthorisedAmount
        tRes.AuthorisingEntity = PayObject.Trans.AuthorisingEntity
        tRes.ExpiryDate = .ExpiryDate
        tRes.StartDate = .StartDate
        tRes.TransDate = PayObject.Trans.TransDate
        tRes.TransTime = PayObject.Trans.TransTime
        tRes.Number = .Number
        tRes.AuthRespCode = PayObject.Trans.AuthRespCode
        tRes.EntryMethod = mstrEntryMethod
        txtAuthNum.Text = tRes.AuthCode
        
    End With
    
    
End Sub

'************************************************************************
'* For Till based printing with items, creates string for printing of EFT info
'************************************************************************
Private Function POSPrintSlip(IsSale As Boolean, CustomerPresent As Boolean, MerchantNumber As String, _
                TerminalID As String, TransactionNo As String, _
                CardName As String, CardReadMethod As String, TranDate As String, TranTime As String, _
                CardNumber As String, IssueNo As String, StartDate As String, ExpiryDate As String, _
                ApplicationID As String, AuthorisationCode As String, TotalAmount As Currency, _
                VerifyMessage As String, Fallbacks As String, OnlineAuthFailed As String, _
                 ICCDebugInfo As String)
                
Dim sprdSlip As String

    sprdSlip = sprdSlip & "Customer Copy"
    sprdSlip = sprdSlip & IIf(mIsSale, "Sale", "Refund")
    If (CustomerPresent = False) Then
        sprdSlip = sprdSlip & "Cardholder Not Present"
    Else
        sprdSlip = sprdSlip & ""
    End If
    
    sprdSlip = sprdSlip & "Merchant:" & MerchantNumber
    sprdSlip = sprdSlip & "TID:" & TerminalID
    sprdSlip = sprdSlip & "Tran No:" & TransactionNo
    
    sprdSlip = sprdSlip & CardName
    sprdSlip = sprdSlip & CardReadMethod
    sprdSlip = sprdSlip & Left$(TranDate, 2) & "/" & Mid$(TranDate, 3, 2) & "/" & Mid$(TranDate, 5, 2)
    sprdSlip = sprdSlip & TranTime
    
    sprdSlip = sprdSlip & "XXXX XXXX XXXX " & Right$(CardNumber, 4)
    sprdSlip = sprdSlip & IssueNo
    
    If (Len(StartDate) = 4) Then
        sprdSlip = sprdSlip & Left$(StartDate, 2) & "/" & Mid$(StartDate, 3)
    Else
        sprdSlip = sprdSlip & Mid$(StartDate, 3, 2) & "/" & Mid$(StartDate, 5, 2)
    End If
        
    sprdSlip = sprdSlip & ExpiryDate
    If (Len(StartDate) = 4) Then
        sprdSlip = sprdSlip & Left$(ExpiryDate, 2) & "/" & Mid$(ExpiryDate, 3)
    Else
        sprdSlip = sprdSlip & Mid$(ExpiryDate, 3, 2) & "/" & Mid$(ExpiryDate, 5, 2)
    End If
    
    sprdSlip = sprdSlip & ApplicationID
    
    sprdSlip = sprdSlip & AuthorisationCode
    sprdSlip = sprdSlip & "Amount:" & Format(TotalAmount, "currency")
    
    Dim reversed As Boolean
    If ((IsSale = True) And (reversed = False)) Or ((IsSale = False) And (reversed = True)) Then
        sprdSlip = sprdSlip & "Please debit my account with the amount specified"
    Else
        sprdSlip = sprdSlip & "Please credit my account with the amount specified"
    End If
    
    sprdSlip = sprdSlip & VerifyMessage
    sprdSlip = sprdSlip & Fallbacks
    If ((IsSale = True) And (reversed = False)) Or ((IsSale = False) And (reversed = True)) Then
        sprdSlip = sprdSlip & "2.5% of this is paid by me to Wickes Retail Services Ltd for handling this transaction for me." & vbNewLine & "The total amount paid is the same however I pay."
    Else
        sprdSlip = sprdSlip & "The amount above includes a refund of 2.5% card handling charge made by Wickes Retail Services."
    End If
    
    If (ICCDebugInfo <> "") Then
        sprdSlip = sprdSlip & "ICC Debug Information"
        sprdSlip = sprdSlip & ICCDebugInfo
    End If

    POSPrintSlip = sprdSlip
    
End Function



'************************************************************************
'* Routine to extract values from PayObject into local variables for displaying and passing out
'************************************************************************
'************************************************************************
'* Routine to display card details and enable Auth Box for entry of Authorisation Code,
'* wait for entry and if valid, print slip.
'************************************************************************
Private Sub GetManualAuthorisation()

    lblAction.Caption = "Card Details"
    txtCardNo.Text = String$(Len(mstrCreditCardNum) - 4, "X") & Right$(mstrCreditCardNum, 4)
    txtEndMonth.Text = Mid$(mstrExpiryDate, 3, 2)
    txtEndYear.Text = Left$(mstrExpiryDate, 2)
    txtIssueNo.Text = mstrCardSequenceNum
    mblnAuthEntered = False
    lblAction.Caption = "Enter Authorisation Number"
    lblStatus.Caption = "Please enter a manual authorisation number"
    fraChequeCard.Enabled = False
    fraChequeType.Enabled = False
    fraBorder.Enabled = True
    fraAuthNum.Enabled = True
    DoEvents
    Call MsgBox("Please phone the card issuer for a manual authorisation number", vbOKOnly, "Authorisation Required")
        
    txtAuthNum.SetFocus

    Do
        DoEvents
    Loop Until mblnAuthEntered = True
    While Len(txtAuthNum.Text) < 6
        txtAuthNum.Text = "0" & txtAuthNum.Text
    Wend
    DoEvents
    If (txtAuthNum.Text) = "" Then
        lblStatus.Caption = "Transaction Cancelled"
        Exit Sub
    End If
    
    mstrAuthNum = txtAuthNum.Text

    lblAction.Caption = "Printing"
    lblStatus.Caption = "Printing"
    Call PrintPOSSignatureSlip

    lblStatus.Caption = "Transaction Successful"

End Sub

Private Sub cmdAuthorise_Click()

    mTranResult.AuthCode = "D" & Format(Time, "HHNNSS")
    mblnCardDetailsEntered = True
    Me.Hide
    
End Sub

Private Sub cmdCompanyChq_Click()
    
    mstrChequeType = CHEQUE_COMP
    cmdPersonalChq.Value = True

End Sub 'cmdCompanyChq_Click

Private Function ConfirmPrintedOk() As Boolean
        
Dim dblTransactionAmount        As Double
Dim strTransaction              As String
Dim intLen                      As Integer
Dim strActionCode               As String
Dim strAuthorisationNumber      As String
Dim strDateTime                 As String
Dim mstrPosCondition             As String
Dim strResponseCode             As String
Dim strMessageReasonCode        As String
Dim strServiceRestrictionCode   As String
Dim strError                    As String
Dim strEventType                As String
Dim strMsgBoxResponse           As String

    dblTransactionAmount = mstrTransactionAmount

    Select Case enConfirmationMode
        
        'Confirming that the signature slip has printed ok
        Case encmReprintSlip
            enConfirmationMode = encmSignatureCon
            If Val(mstrTransactionAmount) > (50 * 100) Then
                If (mstrEntryMethod = ENTRY_KEY) And (txtCustPresent.Text = "Y") And (mblnRefund = False) Then
                    Call MsgBox("An imprint of the card must be obtained on a verification voucher for this tender." & _
                        vbCrLf & "Confirm verification completed and signed by customer", vbOKOnly, _
                        "Manual Imprint Required", "Confirm")
                End If
            End If
            'For Customer not present do not confirm Signature, just complete
            If (mblnCNPTill = True) Then
                Call ConfirmPrintedOk
                Exit Function
            End If
            
            If MsgBox("Is the Signature ok?", vbYesNo, "Check Signature") = vbYes Then
                Call ConfirmPrintedOk
                Exit Function
            Else
'                Call ConfirmPrintedNotOK
            End If
        
        'Confirming that the signature is ok
        Case encmSignatureCon
            
            Select Case enEntryMode
            
                Case encmCheque
                    Dim o As Object 'clsReceiptPrinting
                    Set o.Printer = moOPOSPrinter
                    Set o.goSession = goSession
                    Set o.goRoot = goRoot
                    Set o.goDatabase = goDatabase
                    Call o.PrintChequeFranking(moTranHeader.TranDate, moTranHeader.TransactionTime, goSession.CurrentEnterprise.IEnterprise_StoreNumber, moTranHeader.TillID, moTranHeader.TransactionNo, moTranHeader.CashierID, moTranHeader.TransactionCode, mstrTransactionAmount / 100, mstrCreditCardNum, mstrExpiryDate, , mstrAuthNum, mstrMerchantID)
                    
                    'Check the signature on the cheque
                    If MsgBox("Is the Signature on the cheque ok?", vbYesNo, "Check Signature") = vbYes Then
                        'mstrEntryMethod = "Cheque"
                        mstrEntryMethod = vbNullString
                        Call Me.Hide
                        Exit Function
                    Else
                        Call MsgBox("Not Authorised. Present customer with declination card", vbOKOnly _
                            , "Declination Card")
                        Call Me.Hide
                        Exit Function
                    End If
                    
                Case encmAuthCard
                    DoEvents
                    fraInsertCard.Visible = False
                    lblStatus.Visible = True
                    fraChequeCard.Visible = True
                    fraBorder.Visible = True
                    fraAuthNum.Visible = True
                    txtAuthNum.BackColor = RGB_LTYELLOW
                    fraComplete.Visible = False
                    lblPrintChq.Visible = True
                    lblPrintPrompt.Visible = False
                    'Completes the transaction - online
                    lblStatus.Caption = "Completing Transaction Please Wait"
                    DoEvents

                    'Send Confirmation
                        
                       
                    DoEvents
                    lblAction.Caption = "Sending Confirmation"
                    lblStatus.Caption = "Sending Confirmation"
                    While mblnSendComplete = False
                        DoEvents
                    Wend
                    mstrData = "BYPASS"
                    lblAction.Caption = "Printing Receipt"
                    lblStatus.Caption = "Printing Receipt"
                    DoEvents
                    'Call PrintCreditCardreceipt - taken out for WIX1180
                    lblStatus.Caption = "Please remove any credit cards from the Pin Pad"
                    DoEvents
                    Call Me.Hide
                    Exit Function
                    
                Case encmKeyed
                    fraInsertCard.Visible = False
                    lblStatus.Visible = True
                    fraChequeCard.Visible = True
                    fraBorder.Visible = True
                    fraAuthNum.Visible = True
                    fraAuthNum.Visible = False
                    txtAuthNum.BackColor = RGB_LTYELLOW
                    fraComplete.Visible = False
                    lblPrintChq.Visible = True
                    lblPrintPrompt.Visible = False
                    DoEvents
                    'Completes the transaction - online
                    lblStatus.Caption = "Completing Transaction Please Wait"
                    DoEvents
                    'Call PrintCreditCardreceipt - taken out for WIX1180

                    'Send Confirmation
                    If mstrTransactionType <> "REFUND" Then

                        mblnSendComplete = False
                        lblAction.Caption = "Sending Confirmation"
                        lblStatus.Caption = "Sending Confirmation"
                        While mblnSendComplete = False
                            DoEvents
                        Wend

                         'Loop until there is a response from the eSocket software
                    End If

                    lblStatus.Caption = "Transaction Complete"
                    Call Me.Hide
                    Exit Function
                
            End Select
            
        Case Else
    
    End Select 'enConfirmationMode
    
End Function 'ConfirmPrintedOk

Private Sub cmdDecline_Click()
    mblnCardDetailsEntered = True
    Me.Hide
    
End Sub

Private Sub cmdExit_Click()

    cmdExit.SetFocus
    Call Me.Hide

End Sub 'cmdExit_Click

Private Sub cmdNoTransaxChq_Click()
    
    mstrChequeType = CHEQUE_NOTRANSAX
    fraInsertCard.Visible = False
    lblChequeType.Caption = "Cheque Type - No Transax"
    lblChequeType.Visible = True
    Call ChequeNoTransax

End Sub 'cmdNoTransaxChq_Click

Private Sub cmdPersonalChq_Click()

    If (mstrChequeType = CHEQUE_COMP) Then
        fraInsertCard.Visible = False
    Else
        mstrChequeType = CHEQUE_PERSONAL
    End If
    
    fraChequeType.Visible = False
    lblActionRequired.Visible = False
    lblAction.Visible = True
    fraBorder.Visible = True
    fraComplete.Top = fraChequeCard.Top
    lblAction.Caption = "Enter Cheque Details"
    fraBorder.Visible = False
    fraAccountNo.Visible = True
    fraAccountNo.Top = lblAction.Height + 320
    txtChequeNo.SetFocus
    
        
End Sub 'cmdPersonalChq_Click

Private Sub CaptureCardDetails()

    fraChequeType.Visible = False
    fraChequeCard.Visible = True
    fraBorder.Visible = True
    fraAuthNum.Visible = True
        
    If (enEntryMode = encmCCard) Or enEntryMode = encmAuthCard Then
        lblAction.Caption = "Card Details"
    Else
        lblAction.Caption = "Enter Cheque Guarantee Card Details"
        lblAction.Font.Size = "18"
    End If
    
    If mstrEntryMethod = ENTRY_KEY Then
        lblAction.Caption = "Enter Card Details"
    End If

End Sub 'CaptureCardDetails

Private Sub Form_Activate()

    If (txtCardNo.Visible = True) And (txtCardNo.Enabled = True) Then txtCardNo.SetFocus

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    

End Sub 'Form_Keydown

Public Sub InitialiseStatusBar()
    

End Sub

Private Sub Form_KeyPress(KeyCode As Integer)

    KeyCode = Asc(UCase$(Chr$(KeyCode)))
    
    If (lblInsert.Caption = SELECT_CHEQUE_MSG) And (lblInsert.Visible = True) And (fraAuthNum.Visible = False) Then
        If KeyCode = vbKeyP Then cmdPersonalChq.Value = True
        If KeyCode = vbKeyC Then cmdCompanyChq.Value = True
        If KeyCode = vbKeyN Then cmdNoTransaxChq.Value = True
    End If

    If KeyCode = vbKeyEscape Then
        If ((enEntryMode = encmCheque) And (mblnInitiateCheque = False)) Or (fraAuthNum.Enabled = True) Then
            KeyCode = 0
            mstrEntryMethod = vbNullString
            If (Me.Visible = True) Then
                mlngAttemptNum = mlngAttemptNum + 1
                mstrAuthNum = vbNullString
                txtChequeNo.Text = vbNullString
                txtSortCode.Text = vbNullString
                txtAccountNo.Text = vbNullString
                txtAuthNum.Text = vbNullString
                txtEndMonth.Text = vbNullString
                txtStartMonth.Text = vbNullString
                txtStartYear.Text = vbNullString
                txtEndYear.Text = vbNullString
                txtCardNo.Text = vbNullString
                txtIssueNo.Text = vbNullString
                mblnCustPresent = False
                txtCustPresent.Text = "Y"
                mstrAuthNum = vbNullString
                mstrTrack2 = vbNullString
                mstrTransactionAmount = 0
                mstrChequeType = vbNullString
                Call Me.Hide
                mblnCardDetailsEntered = True
                mblnAuthEntered = True
            End If
        End If
        
    End If
    
End Sub 'form_keypress

Private Sub Form_Load()


'    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
'    RGBMsgBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
'    mlngUseEFTPOS = goSession.GetParameter(PRM_USE_EFTPOS)
'    mstrCaptureCarReg = goSession.GetParameter(PRM_CARREG)
mlngBackColor = vbYellow

    Me.BackColor = mlngBackColor
    fraEntry.BackColor = Me.BackColor
    fraBorder.Visible = False
    fraChequeType.BackColor = Me.BackColor
    Call InitialiseStatusBar
    Me.Visible = False

End Sub 'Form_Load

Private Sub txtCVV_GotFocus()
    
    txtCVV.SelStart = 0
    txtCVV.SelLength = Len(txtCVV.Text)
    txtCVV.BackColor = RGBEdit_Colour

End Sub

Private Sub txtCVV_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        Call txtCustPresent_KeyPress(KeyAscii)
    End If
    
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

    If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0

End Sub

Private Sub txtCVV_LostFocus()
    
    txtCVV.BackColor = RGB_WHITE
    If (Trim$(txtCVV.Text) <> "") Then txtCVV.Text = Format$(Val(txtCVV.Text), "000")

End Sub

Private Sub ISnitiate(ByVal blnStartTransaction As Boolean)

Dim strInt                  As String
Dim strTransaction          As String
Dim lngCounter              As Long
Dim intLen                  As Integer
Dim strResponseCode         As String
Dim strMessageReasonCode    As String
Dim strError                As String
Dim strEventType            As String
Dim strRead                 As String
Dim strTillID               As String

    fraInsertCard.Visible = False
    lblStatus.Visible = True
    fraChequeCard.Visible = True
    fraAuthNum.Visible = True
    fraBorder.Visible = True
    fraBorder.Enabled = False
    lblAction.Visible = True
    If (txtCVV.Text = "") Then mstrCreditCardNum = vbNullString
    Call DisableKeyedEntry(False)
    Call ResizeFormLiveMode
    DoEvents
    
    strTillID = goSession.CurrentEnterprise.IEnterprise_WorkstationID
    'Get terminal ids from eftauth.par
    lblStatus.Visible = True
    fraChequeCard.Visible = True
    fraBorder.Enabled = False
    lblAction.Visible = True
    Call DisableKeyedEntry(False)
    Call ResizeFormLiveMode
    
    fraInsertCard.Visible = True
    fraInsertCard.BackColor = RGBMsgBox_PromptColour
    lblInsert.Caption = ("Please insert \ swipe the card in Pin Pad")
    lblAction.Caption = "Insert Credit Card"
    fraBorder.Visible = False
    DoEvents
        
End Sub 'Initiate

Private Sub txtAccountNo_GotFocus()
    
    txtAccountNo.SelStart = 0
    txtAccountNo.SelLength = Len(txtAccountNo.Text)
    txtAccountNo.BackColor = RGBEdit_Colour

End Sub 'txtAccountNo_GotFocus

Private Sub txtAccountNo_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        If Len(txtAccountNo.Text) < 7 Then
            Call MsgBox("Account number must be at least 7 digits" & vbCrLf & "Enter valid account number", vbOKOnly, "Invalid Details Entered")
            Exit Sub
        End If
        If Len(txtChequeNo.Text) < 6 Then
            Call MsgBox("Cheque number must be at least 6 digits" & vbCrLf & "Enter valid account number", vbOKOnly, "Invalid Details Entered")
            Exit Sub
        End If
        If Len(txtSortCode.Text) < 6 Then
            Call MsgBox("Sortcode number must be at least 6 digits" & vbCrLf & "Enter valid account number", vbOKOnly, "Invalid Details Entered")
            Exit Sub
        End If
        If txtAccountNo.Text <> vbNullString Then
            If mstrChequeType = CHEQUE_COMP Then
               enConfirmationMode = encmSignatureCon
               mstrChequeNo = txtChequeNo.Text
               mstrSortCode = txtSortCode.Text
               mstrAccountNo = txtAccountNo.Text
               enEntryMode = encmCheque
               Call AuthoriseCheque
               
            Else
                Call CaptureCardDetails
                fraAuthNum.Enabled = False
                txtAuthNum.BackColor = RGB_LTYELLOW
                fraAuthNum.Visible = False
                DoEvents
                Call ResizeFormChequeMode
                
                '** Changed 11/7/05 - force Cheque Gaurentee to 1p to force online
                '** Authorisation of all cheques
                
                'Request chegue guarantee amount
                'mstrChequeGuaranteeValue = InputBoxEx("Please enter the cheque guarantee amount", _
                        "Cheque Guarantee Amount", "50", enifNumbers, , RGBMSGBox_PromptColour, , 4)
                mstrChequeGuaranteeValue = "0.01"
                
                If Val(mstrChequeGuaranteeValue) > 0 Then
                
                    mstrChequeNo = txtChequeNo.Text
                    mstrSortCode = txtSortCode.Text
                    mstrAccountNo = txtAccountNo.Text
                    enEntryMode = encmCheque
                    lblStatus.Visible = True
                    lblStatus.Caption = "Please enter the cheque guarantee card details"
                    txtCardNo.SetFocus
                    'Call Cheque routine
                    If Val(mstrTransactionAmount) > (Val(mstrChequeGuaranteeValue) * 100) Then
                        Call AuthoriseCheque
                    Else
                        Call ChequeWithOutAuth
                    End If
                Else
                    Call Me.Hide
                End If
            End If
        Else
            Call MsgBox("Unable to continue a cheque transaction without an account number", vbOKOnly, _
                "Unable to contiune")
            txtAccountNo.SetFocus
        End If
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")
    If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0

End Sub 'txtAccountNo_KeyPress

Private Sub txtAccountNo_LostFocus()

    txtAccountNo.BackColor = RGB_WHITE

End Sub 'txtAccountNo_LostFocus

Private Sub txtAuthNum_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        'This is what happens if a manual authorisation is accepted
        If Len(txtAuthNum.Text) > 0 Then
            If mstrChequeType <> vbNullString Then
                If Val(txtAuthNum.Text) <= 4 Then
                    Call MsgBox("WARNING: Invalid authorisation number", vbOKOnly, "Invalid Auth Number")
                    Exit Sub
                End If
            End If
            If (mstrChequeType <> vbNullString) And (txtCVV.Enabled = True) And (Val(Trim(txtCVV.Text)) = 0) Then
                Call MsgBox("WARNING: Invalid CVV number (must be the 3 Digit Number from the back of the Card)", vbOKOnly, "Invalid CVV Number")
                mblnAuthEntered = False
                Exit Sub
            End If
            If (txtCVV.Enabled = True) And (Trim(txtCVV.Text) <> "") Then
                Call txtCustPresent_KeyPress(vbKeyReturn)
                If (fraBorder.Enabled = True) Then Exit Sub
            End If
            
            mstrAuthNum = txtAuthNum.Text
            txtAuthNum.BackColor = RGB_LTYELLOW
            fraAuthNum.Enabled = False
            fraBorder.Enabled = False
            lblStatus.Caption = "Sending Confirmation"
            
            'Release control back to loop in tmrESocket
            mblnAuthEntered = True
            DoEvents
        End If
    End If
    
    If (KeyAscii = vbKeyEscape) Then
        'This is what happens if a manual authorisation is declined
        mlngAttemptNum = mlngAttemptNum + 1
        mstrAuthNum = vbNullString
        txtChequeNo.Text = vbNullString
        txtSortCode.Text = vbNullString
        txtAccountNo.Text = vbNullString
        txtAuthNum.Text = vbNullString
        txtEndMonth.Text = vbNullString
        txtStartMonth.Text = vbNullString
        txtStartYear.Text = vbNullString
        txtEndYear.Text = vbNullString
        txtCardNo.Text = vbNullString
        txtIssueNo.Text = vbNullString
        mblnCustPresent = False
        txtCustPresent.Text = "Y"
        mstrAuthNum = vbNullString
        mstrTrack2 = vbNullString
        Call Me.Hide
    End If

End Sub 'txtAuthNum_KeyPress
Private Sub txtAuthNum_GotFocus()

    txtAuthNum.BackColor = RGBEdit_Colour

End Sub 'txtAuthNum_GotFocus

Private Sub txtCardNo_GotFocus()

    txtCardNo.SelStart = 0
    txtCardNo.SelLength = Len(txtCardNo.Text)
    txtCardNo.BackColor = RGBEdit_Colour

End Sub 'txtCardNo_GotFocus

Private Sub txtCardNo_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        mblnCardDetailsEntered = True
        mstrAuthNum = vbNullString
        txtAccountNo.Text = vbNullString
        txtChequeNo.Text = vbNullString
        txtSortCode.Text = vbNullString
        Unload Me
'        Call Me.Hide
    End If
    Debug.Print Time$, KeyAscii, Chr$(KeyAscii)
    
    If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0
    
End Sub 'txtCardNo_KeyPress

Private Sub txtCardNo_LostFocus()

        txtCardNo.BackColor = RGB_WHITE
    
End Sub 'txtCardNo_LostFocus

Private Sub txtChequeNo_GotFocus()
    
    txtChequeNo.SelStart = 0
    txtChequeNo.SelLength = Len(txtChequeNo.Text)
    txtChequeNo.BackColor = RGBEdit_Colour

End Sub 'txtChequeNo_GotFocus

Private Sub txtChequeNo_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        If Val(txtChequeNo.Text) > 0 Then
            While Len(txtChequeNo.Text) < 6
                txtChequeNo.Text = "0" & txtChequeNo.Text
            Wend
            Call SendKeys(vbTab)
        End If
    End If
    If (KeyAscii = vbKeyEscape) Then
        mstrAuthNum = vbNullString
        txtAccountNo.Text = vbNullString
        txtChequeNo.Text = vbNullString
        txtSortCode.Text = vbNullString
        Call Me.Hide
    End If
    If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0
    
End Sub 'txtChequeNo_KeyPress

Private Sub txtChequeNo_LostFocus()

    txtChequeNo.BackColor = RGB_WHITE

End Sub 'txtChequeNo_LostFocus

Private Sub txtEndMonth_GotFocus()
    
    txtEndMonth.SelStart = 0
    txtEndMonth.SelLength = Len(txtEndMonth.Text)
    txtEndMonth.BackColor = RGBEdit_Colour

End Sub 'txtEndMonth_GotFocus

Private Sub txtEndMonth_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub 'txtEndMonth_KeyPress

Private Sub txtEndMonth_LostFocus()

    txtEndMonth.BackColor = RGB_WHITE
    txtEndMonth.Text = Format$(Val(txtEndMonth.Text), "00")
    If (Val(txtEndMonth.Text) <= 0) Or (Val(txtEndMonth.Text) > 12) Then txtEndMonth.Text = "--"

End Sub 'txtEndMonth_LostFocus

Private Sub txtEndYear_GotFocus()
    
    txtEndYear.SelStart = 0
    txtEndYear.SelLength = Len(txtEndYear.Text)
    txtEndYear.BackColor = RGBEdit_Colour

End Sub 'txtEndYear_GotFocus

Private Sub txtEndYear_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub 'txtEndYear_KeyPress

Private Sub txtEndYear_LostFocus()

    txtEndYear.BackColor = RGB_WHITE
    txtEndYear.Text = Format$(Val(txtEndYear.Text), "00")
    If (Val(txtEndYear.Text) <= 0) Or (Val(txtEndYear.Text) > 99) Then txtEndYear.Text = "--"

End Sub 'txtEndYear_LostFocus

Private Sub txtCustPresent_GotFocus()

    txtCustPresent.SelStart = 0
    txtCustPresent.SelLength = Len(txtCustPresent.Text)
    txtCustPresent.BackColor = RGBEdit_Colour
 
End Sub 'txtCustPresent_GotFocus

Private Sub txtIssueNo_GotFocus()
    
    txtIssueNo.SelStart = 0
    txtIssueNo.SelLength = Len(txtIssueNo.Text)
    txtIssueNo.BackColor = RGBEdit_Colour

End Sub

Private Sub txtIssueNo_LostFocus()

    If LenB(txtIssueNo.Text) = 0 Then txtIssueNo.Text = "00"
    txtIssueNo.Text = Format$(txtIssueNo.Text, "00")

    txtIssueNo.BackColor = RGB_WHITE

End Sub

Private Sub txtCustPresent_KeyPress(KeyAscii As Integer)
    
Dim strSQL          As String
Dim strDSN          As String

'    strDSN = goDatabase.ConnectionString

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        mblnCardDetailsEntered = False
        DoEvents
        mstrExpiryDate = txtEndYear.Text & txtEndMonth.Text
        If ValidateExpiryDate(mstrExpiryDate) = False Then
            Call MsgBox("Please enter a valid expiry date", vbOKOnly, "Invalid Expiry Date")
            txtCardNo.SetFocus
            Exit Sub
        End If
        If (Trim$(txtCVV.Text) = "") And (txtCVV.Enabled = True) Then
            Call MsgBox("Please enter a valid CVV number from the Card", vbOKOnly, "Invalid CVV")
            txtCVV.SetFocus
            Exit Sub
        End If

        KeyAscii = 0
        mblnCardDetailsEntered = True
        fraBorder.Enabled = False
        
        mstrStartDate = txtStartYear.Text & txtStartMonth.Text
        mstrExpiryDate = txtEndYear.Text & txtEndMonth.Text
        mstrCreditCardNum = txtCardNo.Text
        mstrCardSequenceNum = IIf(Val(txtIssueNo.Text) = 0, "", txtIssueNo.Text)
        If txtCustPresent = "Y" Then
            mblnCustPresent = True
        Else
            mblnCustPresent = False
        End If

        If (Trim$(txtCVV.Text) <> "") Then
            mstrEntryMethod = ENTRY_KEY
            Call PerformAuth(mTranAmount, True, mIsSale, mblnCustPresent, mAllowCashback, mTranResult)
            mblnCardDetailsEntered = True
        End If

        'Release control back to loop in tmrESocket
        DoEvents
    End If

    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub txtIssueNo_KeyPress(KeyAscii As Integer)

    If (mblnCNPTill = False) Then fraAuthNum.Enabled = False

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub


Private Sub txtSortCode_GotFocus()
    
    txtSortCode.SelStart = 0
    txtSortCode.SelLength = Len(txtSortCode.Text)
    txtSortCode.BackColor = RGBEdit_Colour

End Sub

Private Sub txtSortCode_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        If Len(txtSortCode.Text) < 6 Then
            Call MsgBox("The Sort Code must be six characters long.", vbOKOnly, "Incorrect SortCode")
        Else
            Call SendKeys(vbTab)
        End If
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")
    If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0

End Sub

Private Sub txtSortCode_LostFocus()

    txtSortCode.BackColor = RGB_WHITE

End Sub

Private Sub txtStartMonth_GotFocus()
    
    txtStartMonth.SelStart = 0
    txtStartMonth.SelLength = Len(txtStartMonth.Text)
    txtStartMonth.BackColor = RGBEdit_Colour

End Sub

Private Sub txtStartMonth_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")
If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0

End Sub

Private Sub txtStartMonth_LostFocus()

    txtStartMonth.BackColor = RGB_WHITE
    txtStartMonth.Text = Format$(Val(txtStartMonth.Text), "00")
    If (Val(txtStartMonth.Text) <= 0) Or (Val(txtStartMonth.Text) > 12) Then txtStartMonth.Text = "--"

End Sub

Private Sub txtStartYear_GotFocus()

    txtStartYear.SelStart = 0
    txtStartYear.SelLength = Len(txtStartYear.Text)
    txtStartYear.BackColor = RGBEdit_Colour

End Sub

Private Sub txtStartYear_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub txtStartYear_LostFocus()

    txtStartYear.BackColor = RGB_WHITE
    txtStartYear.Text = Format$(Val(txtStartYear.Text), "00")
    If (Val(txtStartYear.Text) <= 0) Or (Val(txtStartYear.Text) > 99) Then txtStartYear.Text = "--"

End Sub


Public Sub TypeCCDetails()

    'Stop the timer which calls modPayware
    fraInsertCard.Visible = False
    fraChequeCard.Visible = True
    fraBorder.Visible = True
    fraAuthNum.Visible = True
    fraAuthNum.Enabled = False
    enEntryMode = encmAuthCard
    txtIssueNo.BackColor = vbWhite

    lblAction.Visible = True
    'Displays a label if the authorisation server is in test mode
    If mstrTmode = "mTest" Then
        lblAuthTranMod.Visible = True
        lblAuthTranMod.Caption = "* * * TEST MODE * * *"
        Call ResizeFormTestMode 'Resizes form
    Else
        Call ResizeFormLiveMode 'Resizes form
    End If
    DoEvents
    Call CaptureCardDetails

End Sub 'TypeCCDetails

Public Sub PrintPOSSignatureSlip()

Dim dblAmount   As Double
Dim strVerified As String
    
    dblAmount = mstrTransactionAmount / 100

mstrSignatureRequired = "TRUE"

End Sub 'PrintSignatureSlip

Private Sub SetEntryMethod()
    
    Select Case mstrTransactionSource
        Case "1", "2", "3", "5", "7"
            mstrEntryMethod = ENTRY_KEY
        Case "0", "4"
            mstrEntryMethod = ENTRY_SWIPE
        Case "i"
            mstrEntryMethod = ENTRY_ICCP
        Case Else
            mstrEntryMethod = "Unknown(" & mstrTransactionSource & ")"
    End Select
    'Extract the Verification method
    Select Case UCase(mstrAuthType)
        Case "SIGNATURE":   mstrEntryMethod = mstrEntryMethod & " Signature Verified"
        Case "PIN":         mstrEntryMethod = mstrEntryMethod & " PIN Verified"
        Case "NOT_PERFORMED": mstrEntryMethod = mstrEntryMethod & " Not Verified"
        Case "PIN_AND_SIGNATURE": mstrEntryMethod = mstrEntryMethod & " PIN and Signature Verified"
        Case "FAILED": mstrEntryMethod = mstrEntryMethod & " Failed Verification"
        Case "UNKNOWN", "": mstrEntryMethod = mstrEntryMethod & " Unknown"
    End Select

End Sub

Private Sub PrintCreditCardreceipt()

Dim dblAmount As Double
Dim strStage  As String
    
    On Error GoTo Err_Print_CCReceipt
    
    strStage = "Starting"
    dblAmount = Val(mstrTransactionAmount) / 100
    
    strStage = "PrintHeader-" & mstrCashierName

    lblStatus.Caption = "Printing Receipt-Details"
    DoEvents
    strStage = "PrintDetails-" & mstrCardProductName
    
    lblStatus.Caption = "Printing Receipt-Slip"
    DoEvents
    strStage = "PrintingSlip-" & mstrCardProductName
        
    Exit Sub

Err_Print_CCReceipt:

    Call MsgBox("WARNING : An error has occurred when printing Credit Card Receipt - system will attempt to continue to complete transaction", vbOKOnly, "Print Error")
    Call Err.Report(MODULE_NAME, "PrintCreditCardReceipt-" & strStage, 1, False)
    Call Err.Clear

End Sub 'PrintCreditCardreceipt

Private Sub DisableKeyedEntry(DisableAuth As Boolean)

    
    fraBorder.Enabled = Not DisableAuth
    DoEvents
    txtEndMonth.BackColor = RGB_LTYELLOW
    txtEndYear.BackColor = RGB_LTYELLOW
    txtStartMonth.BackColor = RGB_LTYELLOW
    txtStartYear.BackColor = RGB_LTYELLOW
    txtCardNo.BackColor = RGB_LTYELLOW
    txtIssueNo.BackColor = RGB_LTYELLOW
    txtCustPresent.BackColor = RGB_LTYELLOW
    If (DisableAuth = False) Then
        txtEndMonth.Enabled = False
        txtEndYear.Enabled = False
        txtStartMonth.Enabled = False
        txtStartYear.Enabled = False
        txtCardNo.Enabled = False
        txtIssueNo.Enabled = False
        txtCustPresent.Enabled = False
    End If

End Sub 'DisableKeyedEntry

Private Sub EnableKeyedEntry()

    txtEndMonth.BackColor = RGB_WHITE
    txtEndYear.BackColor = RGB_WHITE
    txtStartMonth.BackColor = RGB_WHITE
    txtStartYear.BackColor = RGB_WHITE
    txtCardNo.BackColor = RGB_WHITE
    txtIssueNo.BackColor = RGB_WHITE
    txtCustPresent.BackColor = RGB_WHITE
    txtEndMonth.Enabled = True
    txtEndYear.Enabled = True
    txtStartMonth.Enabled = True
    txtStartYear.Enabled = True
    txtCardNo.Enabled = True
    txtIssueNo.Enabled = True
    txtCustPresent.Enabled = True
    fraBorder.Enabled = True
    DoEvents

End Sub 'EnableKeyedEntry

Public Function ConfirmCardRead() As Boolean

    txtEndMonth.Text = Format$(Val(txtEndMonth.Text), "00")
    If Val(txtStartMonth.Text) < 0 Then
        txtStartMonth.Text = Format$(Val(txtStartMonth.Text), "00")
        Exit Function
    End If
        
    If LenB(txtCardNo.Text) = 0 Then
        Call MsgBox("Invalid card detected: No Credit card number" & vbCrLf & vbCrLf & "Please select an alternative tender type ", vbOKOnly, "Warning: No Card number")
        Exit Function
    End If
    
    If IsNumeric(txtCardNo.Text) = False Then
        Call MsgBox("Invalid card detected: Invalid Credit card number" & vbCrLf & vbCrLf & "Please select an alternative tender type ", vbOKOnly, "Warning: Invalid Card number")
        Exit Function
    End If
    
    If Val(txtEndYear.Text & txtEndMonth.Text) > 0 Then
        
        If Val(txtEndYear.Text & txtEndMonth.Text) < Format$(Date, "YYMM") Then
            Call MsgBox("Invalid card detected: Card has expired" & vbCrLf & vbCrLf & "Please select an alternative tender type ", vbOKOnly, "Warning: Card Expired")
            Exit Function
        End If
        
        If (txtStartYear.Text & txtStartMonth.Text) > (txtEndYear & txtEndMonth) Then
            Call MsgBox("Invalid card detected: Start date is after expiry date" & vbCrLf & vbCrLf & "Please select an alternative tender type ", vbOKOnly, "Warning: Bad start date")
            Exit Function
        End If
    End If
    
    ConfirmCardRead = True
    
End Function 'ConfirmCardRead

Private Sub ResizeFormTestMode()

'Resizes the form accordingly when the authorisation server is set to test mode

    fraBorder.Height = fraChequeCard.Height + fraAuthNum.Height + 12
    fraBorder.Top = lblAction.Top + lblAction.Height + lblAuthTranMod.Height + 220
    lblStatus.Top = fraBorder.Top + fraBorder.Height + 120
    DoEvents

End Sub 'ResizeFormTestMode

Private Sub ResizeFormLiveMode()

'Resizes the form accordingly when the authorisation server is set to live mode

    fraBorder.Height = fraChequeCard.Height + fraAuthNum.Height + 12
    fraBorder.Top = lblAction.Top + lblAction.Height + 120
    lblStatus.Top = fraBorder.Top + fraBorder.Height + 120
    DoEvents

End Sub 'ResizeFormLiveMode

Private Sub ResizeFormChequeMode()

'Resizes the form accordingly when the authorisation server is set to live mode

    fraBorder.Height = fraChequeCard.Height + fraAuthNum.Height + 12
    fraBorder.Top = lblAction.Top + lblAction.Height + 120
    fraChequeCard.Top = 120
    fraAuthNum.Top = fraChequeCard.Height - 12
    fraAuthNum.Visible = False
    DoEvents

End Sub 'ResizeFormLiveMode

Private Sub PrintChequeReceipt()

Dim dblAmount As Double
    
    dblAmount = mstrTransactionAmount / 100
    
End Sub 'PrintChequeReceipt

Private Sub AuthoriseCheque()

    mstrTransactionNo = Format(Now, "DDHHMMSS")
    mstrTransactionNo = Mid(mstrTransactionNo, 2, 6)
    If Left(mstrTransactionNo, 1) = "0" Then
        mstrTransactionNo = Replace(Left(mstrTransactionNo, 1), "0", "9") & _
        Mid(mstrTransactionNo, 2, 5)
    End If
    
'    If Winsock1.State = sckConnected Then
'        Call InitiateCheque
'    Else
        txtChequeNo.Text = vbNullString
        txtSortCode.Text = vbNullString
        txtAccountNo.Text = vbNullString
        txtAuthNum.Text = vbNullString
        txtEndMonth.Text = vbNullString
        txtStartMonth.Text = vbNullString
        txtStartYear.Text = vbNullString
        txtEndYear.Text = vbNullString
        txtCardNo.Text = vbNullString
        txtIssueNo.Text = vbNullString
        mblnCustPresent = False
        txtCustPresent.Text = "Y"
        mstrAuthNum = vbNullString
        mstrTrack2 = vbNullString
 '   End If
    'Unload Me
    
End Sub 'AuthoriseCheque

Private Sub InitiateCheque()

Dim strInt                  As String
Dim strTransaction          As String
Dim lngCounter              As Long
Dim intLen                  As Integer
Dim strResponseCode         As String
Dim strMessageReasonCode    As String
Dim strError                As String
Dim strEventType            As String
Dim strRead                 As String
Dim strTillID               As String

    fraInsertCard.Visible = False
    lblStatus.Visible = True
    fraChequeCard.Visible = True
    fraAuthNum.Visible = True
    fraBorder.Visible = True
    fraBorder.Enabled = False
    fraAccountNo.Visible = False
    lblAction.Visible = True
    mstrCreditCardNum = vbNullString
    Call DisableKeyedEntry(False)
    Call ResizeFormLiveMode
    DoEvents
    
    strTillID = goSession.CurrentEnterprise.IEnterprise_WorkstationID
    'Get terminal ids from eftauth.par
    
    lblStatus.Visible = True
    fraChequeCard.Visible = True
    fraBorder.Enabled = False
    lblAction.Visible = True
    Call DisableKeyedEntry(False)
    Call ResizeFormLiveMode
    If mstrChequeType <> CHEQUE_COMP Then fraInsertCard.Visible = True
    fraInsertCard.BackColor = RGBMsgBox_PromptColour
    lblInsert.Caption = ("Please insert \ swipe the Cheque Guarantee card in Pin Pad")
    lblAction.Caption = "Insert Cheque Guarantee Card"
    fraBorder.Visible = False
    DoEvents
    
    Call ChequeInquiry
    
End Sub 'InitiateCheque

Private Sub ChequeInquiry()

Dim dblTransactionAmount        As Double
Dim strTransaction              As String
Dim intLen                      As Integer
Dim strActionCode               As String
Dim strAuthorisationNumber      As String
Dim strDateTime                 As String
Dim mstrPosCondition            As String
Dim strResponseCode             As String
Dim strMessageReasonCode        As String
Dim strServiceRestrictionCode   As String
Dim strError                    As String
Dim strEventType                As String
Dim strMsgBoxResponse           As String
Dim strStartDate                As String

    'Clear gobal variable holding the data
    mstrFallBackType = vbNullString
    mstrEntryMethod = vbNullString
    lblStatus.Caption = vbNullString
    dblTransactionAmount = mstrTransactionAmount
    mstrTransactionType = IIf(mIsSale, "Sale", "Refund")
    While Len(mstrAccountNo) < 16
        mstrAccountNo = mstrAccountNo & " "
    Wend
    While Len(mstrSortCode) < 8
        mstrSortCode = mstrSortCode & " "
    Wend

    If (Val(txtStartMonth.Text) > 0) Then
        strStartDate = " StartDate=" & Chr(34) & txtStartYear.Text & txtStartMonth.Text & Chr(34)
    End If
    mstrTransactionNo = Format(Now, "DDHHMMSS")
    mstrTransactionNo = Mid(mstrTransactionNo, 2, 6)
    If Left(mstrTransactionNo, 1) = "0" Then
        mstrTransactionNo = Replace(Left(mstrTransactionNo, 1), "0", "9") & _
        Mid(mstrTransactionNo, 2, 5)
    End If
    
    strTransaction = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & _
        " encoding=" & Chr(34) & "UTF-8" & Chr(34) & "?>"

    If mstrChequeType = CHEQUE_COMP Then
        strTransaction = strTransaction & "<Esp:Interface Version=" & Chr(34) & "1.0" & Chr(34) & _
            " xmlns:Esp=" & Chr(34) & "http://www.mosaicsoftware.com/Postilion/eSocket.POS/" & Chr(34) & _
            "><Esp:Inquiry TerminalId=" & Chr(34) & mstrTID & Chr(34) & _
            " TransactionId=" & Chr(34) & mstrTransactionNo & Chr(34) & " Type=" & Chr(34) & _
            " CHEQUE_GUARANTEE" & Chr(34) & _
            " CardNumber=" & Chr(34) & "0000000000000000" & Chr(34) & _
            " ExpiryDate=" & Chr(34) & "0000" & Chr(34) & _
            " ChequeAccountNumber=" & Chr(34) & mstrAccountNo & Chr(34) & _
            " ChequeInstitutionCode=" & Chr(34) & mstrSortCode & Chr(34) & _
            " ChequeNumber=" & Chr(34) & mstrChequeNo & Chr(34) & _
            " DateTime=" & Chr(34) & Format(Now, "MMDDHHNNSS") & Chr(34) & _
            " TransactionAmount=" & Chr(34) & dblTransactionAmount & Chr(34) & _
            " /></Esp:Interface>"
            
    Else
    
        If txtCardNo.Text = vbNullString Then
            strTransaction = strTransaction & "<Esp:Interface Version=" & Chr(34) & "1.0" & Chr(34) & _
                " xmlns:Esp=" & Chr(34) & "http://www.mosaicsoftware.com/Postilion/eSocket.POS/" & Chr(34) & _
                "><Esp:Inquiry TerminalId=" & Chr(34) & mstrTID & Chr(34) & _
                " TransactionId=" & Chr(34) & mstrTransactionNo & Chr(34) & " Type=" & Chr(34) & _
                " CHEQUE_GUARANTEE" & Chr(34) & _
                " ChequeAccountNumber=" & Chr(34) & mstrAccountNo & Chr(34) & _
                " ChequeInstitutionCode=" & Chr(34) & mstrSortCode & Chr(34) & _
                " ChequeNumber=" & Chr(34) & mstrChequeNo & Chr(34) & _
                " DateTime=" & Chr(34) & Format(Now, "MMDDHHNNSS") & Chr(34) & _
                " TransactionAmount=" & Chr(34) & dblTransactionAmount & Chr(34) & _
                " /></Esp:Interface>"
        Else
        
        'Keyed entry
            mstrExpiryDate = txtEndYear.Text & txtEndMonth.Text
            If ValidateExpiryDate(mstrExpiryDate) = False Then
                Call MsgBox("Please enter a valid expiry date", vbOKOnly, "Invalid Expiry Date")
                txtCardNo.SetFocus
                Exit Sub
            End If
            mblnCardDetailsEntered = True
            mstrStartDate = txtStartYear.Text & txtStartMonth.Text
            mstrExpiryDate = txtEndYear.Text & txtEndMonth.Text
            mstrCreditCardNum = txtCardNo.Text
            While Len(mstrCreditCardNum) < 16
                mstrCreditCardNum = "0" & mstrCreditCardNum
            Wend
            mstrCardSequenceNum = IIf(Val(txtIssueNo.Text) = 0, "", txtIssueNo.Text)
        
            strTransaction = strTransaction & "<Esp:Interface Version=" & Chr(34) & "1.0" & Chr(34) & _
                " xmlns:Esp=" & Chr(34) & "http://www.mosaicsoftware.com/Postilion/eSocket.POS/" & Chr(34) & _
                "><Esp:Inquiry TerminalId=" & Chr(34) & mstrTID & Chr(34) & _
                " TransactionId=" & Chr(34) & mstrTransactionNo & Chr(34) & " Type=" & Chr(34) & _
                " CHEQUE_GUARANTEE" & Chr(34) & _
                " ChequeAccountNumber=" & Chr(34) & mstrAccountNo & Chr(34) & _
                " ChequeInstitutionCode=" & Chr(34) & mstrSortCode & Chr(34) & _
                " CardNumber=" & Chr(34) & mstrCreditCardNum & Chr(34) & _
                " ExpiryDate=" & Chr(34) & mstrExpiryDate & Chr(34) & _
                strStartDate & _
                IIf(mstrCardSequenceNum = "", "", " CardSequenceNumber=" & Chr(34) & mstrCardSequenceNum & Chr(34)) & _
                " ChequeNumber=" & Chr(34) & mstrChequeNo & Chr(34) & _
                " DateTime=" & Chr(34) & Format(Now, "MMDDHHNNSS") & Chr(34) & _
                " TransactionAmount=" & Chr(34) & dblTransactionAmount & Chr(34) & _
                " /></Esp:Interface>"
       
        End If

    End If
        
    intLen = Len(strTransaction)
    strTransaction = Chr(intLen \ 256) & Chr(intLen Mod 256) & strTransaction
    mblnSendComplete = False
    'Sending Transaction message to eSocket Software
    lblStatus.Caption = "Sending Transaction"
    mstrData = vbNullString
    mstrResponseType = vbNullString

    lblStatus.Caption = "Waiting"
    lblAction.Caption = "Waiting"
    While mblnSendComplete = False
        DoEvents
    Wend

     'Loop until there is a response from the eSocket software
    mblnInitiateCheque = True
    mblnInitiateCheque = False
    lblAction.Caption = "Processing"
    fraBorder.Visible = True
    fraChequeCard.Visible = True
    fraAuthNum.Visible = False
    fraAccountNo.Visible = False
    Me.Refresh



End Sub 'ChequeInquiry

Private Sub ChequeWithOutAuth()

Dim strTrack1   As String
Dim strTrack2   As String
Dim strTrack3   As String

    mstrTransactionNo = Format(Now, "DDHHMMSS")
    mstrTransactionNo = Mid(mstrTransactionNo, 2, 6)
    If Left(mstrTransactionNo, 1) = "0" Then
        mstrTransactionNo = Replace(Left(mstrTransactionNo, 1), "0", "9") & _
        Mid(mstrTransactionNo, 2, 5)
    End If
    
    Call DisableKeyedEntry(False)
    fraInsertCard.Visible = True
    fraChequeType.Visible = False
    fraAccountNo.Visible = False
    DoEvents
    fraInsertCard.BackColor = RGBMsgBox_PromptColour
    fraBorder.Visible = False
    lblInsert.Caption = ("Please insert \ swipe the Cheque Guarantee card in Pin Pad")
    lblAction.Caption = "Insert \ Swipe Cheque Guarantee Card"
    lblStatus.Caption = "Insert \ Swipe Cheque Guarantee Card"
    DoEvents
    Call RetrieveTracks(strTrack1, strTrack2, strTrack3)
    
    If strTrack2 <> vbNullString Then
    
        fraBorder.Visible = True
        fraInsertCard.Visible = False
        fraChequeType.Visible = True
        lblAction.Caption = "Card Details Received"
        lblStatus.Caption = "Card Details Received"
        DoEvents
        
        txtAuthNum.Text = mstrTransactionNo
        mstrAuthNum = txtAuthNum.Text
        
        txtCardNo.Text = Left(strTrack2, InStr(strTrack2, "=") - 1)
        mstrCreditCardNum = txtCardNo.Text
        txtEndYear.Text = Mid(strTrack2, InStr(strTrack2, "=") + 1, 2)
        txtEndMonth.Text = Mid(strTrack2, InStr(strTrack2, "=") + 3, 2)
        mstrExpiryDate = txtEndMonth.Text & txtEndYear.Text
        
        lblAction.Caption = "Printing"
        lblStatus.Caption = "Printing"
        enEntryMode = encmCheque
        enConfirmationMode = encmSignatureCon
        
        Call ConfirmPrintedOk
        lblStatus.Caption = "Cheque Inquiry Successful"
    Else
        mlngAttemptNum = mlngAttemptNum + 1
        mstrAuthNum = vbNullString
        txtChequeNo.Text = vbNullString
        txtSortCode.Text = vbNullString
        txtAccountNo.Text = vbNullString
        txtAuthNum.Text = vbNullString
        txtEndMonth.Text = vbNullString
        txtStartMonth.Text = vbNullString
        txtStartYear.Text = vbNullString
        txtEndYear.Text = vbNullString
        txtCardNo.Text = vbNullString
        txtIssueNo.Text = vbNullString
        mblnCustPresent = False
        txtCustPresent.Text = "Y"
        mstrAuthNum = vbNullString
        mstrTrack2 = vbNullString
        mstrTransactionAmount = 0
        Call Me.Hide
    End If

End Sub 'ChequeWithOutAuth

Private Sub txtCustPresent_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyTab Then Call txtCustPresent_KeyDown(vbKeyReturn, 0)

End Sub

Private Function ValidateExpiryDate(ByVal strExpiryDate) As Boolean

    ValidateExpiryDate = True
    strExpiryDate = Replace(strExpiryDate, "-", "")
    If strExpiryDate = vbNullString Then ValidateExpiryDate = False
    If Len(strExpiryDate) <> 4 Then ValidateExpiryDate = False
    
End Function 'ValidateExpiryDate

Private Sub ChequeNoTransax()

'    Call MsgBoxEx("Warning transaction amount exceeds the account credit limit" & _
'        vbCrLf & "Please enter a supervisor password", vbOKOnly, "Transaction Exceeds Account Credit Limit", _
'        , , , , RGBMsgBox_WarnColour)
        fraInsertCard.Visible = True
        lblChequeType.Visible = False

End Sub 'ChequeNoTransax
Public Sub RetrieveTracks(Track1 As String, Track2 As String, Track3 As String)

    
    Exit Sub
    
RetrieveTracks_Error:
    Call Err.Clear
    
End Sub




