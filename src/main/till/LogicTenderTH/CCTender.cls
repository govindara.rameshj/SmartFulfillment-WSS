VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCTender"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public TranHeader As Object
Public POSPrinter As Object
Public Session As Object
Public CustomerNotPresentTill As Boolean
Public MaxValidCashier As String
Public DebugEFTPrint As String

Public Type TranResult
     AcquirerIdentifier As String
     AcquirerName As String
     ActionCodeDefault As String
     ActionCodeDenial As String
     ActionCodeOnline As String
     Active As String
     ApplicationID As String
     ApplicationLabel As String
     ApplicationName As String
     ApplicationPriority As String
     ApplicationVersion As String
     AVSAddress As String
     AVSPostcode As String
     CardRange As String
     Country As String
     Curr As String
     CV2Data As String
     CV2Required As String
     CVMList As String
     ExpiryDate As String
     InterchangeProfile As String
     IssueNumber As String
     IssuerSuppliedData As String
     Number As String
     OptionalData1 As String
     OptionalData2 As String
     seqNum As String
     ServiceCode As String
     Sponsor As String
     StartDate As String
     Track1Details As String
     Track2Details As String
     Track3Details As String
     TransactionCounter As String
     UsageControl As String
     ActualverificationMethod As String
     APACSRequestHeader As String
     APACSResponse As String
     ApacsResponseCode As String
     AppRspCryptogram As String
     AuthCode As String
     AuthorisedAmount As String
     AuthorisingEntity As String
     AuthReqCrypto As String
     AuthRespCode As String
     AuxiliaryData As String
     AVSAddressValidationResponse As String
     AVSPostcodeValidationResponse As String
     BankIdentifier As String
     BankName As String
     BankSortCode As String
     Card As String
     CardDetails As String
     CardDetailsCount As String
     CardholderResult As String
     CashbackAmount As String
     ChequeAccountNumber As String
     ChequeNumber As String
     ContactPhoneNumber As String
'     Country As String
     CryptoInfoData As String
     CryptoTransType As String
'     Curr As String
     CV2ValidationResponse As String
     DCC As String
     DelayDate As String
     ErrorCode As String
     Fallback As String
     FollowOnMsg As String
     ForceCV2AVS As String
     ICCFallbackVerifyValue As String
     MerchantCategoryCode As String
     MerchantNumber As String
     MethodOfAuth As String
     OptionalData As String
     OptionFileName As String
     Passed As String
     ResponseIndicator As String
     SalesDetails As String
     Script As String
     ScriptResults As String
     SecurityDetails As String
     SetCardDetails As String
     SourceTerminalIdentifier As String
     StartTime As String
     StatusCategory As String
     StatusEvent As String
     StatusInfo As String
     StatusMessage As String
     Target As String
     TermIFD As String
     TerminalCap As String
     TerminalResult As String
     TerminalType As String
     Time As String
     TransactionAmount As String
     TransactionCategoryCode As String
     TransactionNumber As String
     TransactionOption As String
     TransactionSource As String
     TransactionType As String
     TransDate As String
     TransTime As String
     UnpredictableNum As String
     EFTPrintSlip As String
     EntryMethod  As String
End Type


Public Function AuthoriseEFTPayment(IsSale As Boolean, _
                                    TransactionAmount As Long, _
                                    CustomerPresent As Boolean, _
                                    AllowCashback As Boolean, _
                                    IsCheque As Boolean, _
                                    PrintFormat As String, _
                                    PrintLayout As String, _
                                    CashierName As String, _
                                    ByRef TransactionResult As TranResult) As Boolean

    Set goSession = Session
    Load frmTender
    AuthoriseEFTPayment = frmTender.AuthoriseEFTPayment(TranHeader, POSPrinter, IsSale, TransactionAmount, _
                            CustomerPresent, AllowCashback, IsCheque, PrintFormat, _
                            PrintLayout, CustomerNotPresentTill, CashierName, TransactionResult)
    Unload frmTender
    TransactionResult.EFTPrintSlip = mstrPOSPrintSlip
    AuthoriseEFTPayment = (TransactionResult.AuthCode <> "")

End Function
        

Public Function RetrieveTracks(ByRef strTrack1 As String, _
                          ByRef strTrack2 As String, _
                          ByRef strTrack3 As String) As Boolean
    
    strTrack1 = vbNullString
    strTrack2 = vbNullString
    strTrack3 = vbNullString
    
    
    RetrieveTracks = (strTrack1 <> "") Or (strTrack2 <> "") Or (strTrack3 <> "")
    

End Function 'AuthoriseCheque

Public Sub ExitWaitNow()

End Sub

    




