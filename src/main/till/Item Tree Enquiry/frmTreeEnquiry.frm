VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmTreeEnquiry 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Item Enquiry"
   ClientHeight    =   8385
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11670
   Icon            =   "frmTreeEnquiry.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8385
   ScaleWidth      =   11670
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10 - Exit"
      Height          =   495
      Left            =   10080
      TabIndex        =   4
      Top             =   7560
      Width           =   1215
   End
   Begin VB.CommandButton cmdEnterTree 
      Caption         =   "F4 - Enter Tree"
      Height          =   495
      Left            =   4080
      TabIndex        =   3
      Top             =   400
      Width           =   1215
   End
   Begin VB.CommandButton cmdExpandTree 
      Caption         =   "F3 - Expand Tree"
      Height          =   495
      Left            =   2220
      TabIndex        =   2
      Top             =   400
      Width           =   1215
   End
   Begin VB.CommandButton cmdContractTree 
      Caption         =   "F2 - Contract Tree"
      Height          =   495
      Left            =   360
      TabIndex        =   1
      Top             =   400
      Width           =   1095
   End
   Begin MSComctlLib.TreeView tvwItems 
      Height          =   6975
      Left            =   360
      TabIndex        =   0
      Top             =   1000
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   12303
      _Version        =   393217
      LineStyle       =   1
      Style           =   7
      Appearance      =   1
   End
End
Attribute VB_Name = "frmTreeEnquiry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const MODULE_NAME  As String = "frmTreeEnquiry"

Private Const TREE_LEVELS As Integer = 5
Private Const CAT_LEVEL As Integer = 2
Private Const GRP_LEVEL As Integer = 3
Private Const SGRP_LEVEL As Integer = 4
Private Const STYLE_LEVEL As Integer = 5
Private Const CAT_CODE As String = "CA"
Private Const GRP_CODE As String = "GR"
Private Const SGRP_CODE As String = "SG"
Private Const STYLE_CODE As String = "ST"

Private mcnnDBConnection As ADODB.Connection

Private mlngCatCounter As Long
Private mlngGrpCounter As Long
Private mlngSGrpCounter As Long
Private mlngStyleCounter As Long
Private mstrCatNo As String
Private mstrGrpNo As String
Private mstrSGrpNo As String

Private Enum enmFieldType
    ftCategory = 1
    ftGroup = 2
    ftSubGroup = 3
    ftStyle = 4
    ftDescription = 5
End Enum

Private Sub Form_Load()

    Const PROCEDURE_NAME As String = "Form_Load"

'    Dim nNode As Node
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Started on " & Now())
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Load")
'   Get the system setup.
    Call GetRoot
'    Call InitialiseStatusBar(sbStatus)
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    
    ' Get the User ID, Workstation ID and if running as the StandAlone version or not.
'    mstrUserID = goSession.UserID
'    mstrWSID = goSession.CurrentEnterprise.IEnterprise_WorkstationID
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "ProgramParams - " & goSession.ProgramParams)
'    If (goSession.ProgramParams = "L") Then
'        mblnStandAlone = True
'        Me.cmdSkuEnquiry.Visible = False
'        Me.Caption = Me.Caption & " - Standalone"
'    Else
'        mblnStandAlone = False
'        Me.Caption = Me.Caption & " - Store"
'    End If
    
    ' Get the paths to use.
'    mstrWixData = goSession.GetParameter(PRM_WIXDATAPATH)
'    Call TestPathExists(mstrWixData)
'    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Wix Data Path - " & mstrWixData)

'   Set up the database connection.
    Set mcnnDBConnection = goSession.Database.Connection
    If (mcnnDBConnection.State = adStateOpen) Then
        mcnnDBConnection.Close
    End If
    If (mcnnDBConnection Is Nothing) Then
        Call MsgBox("The Database connection to DSN - " & goSession.Database.Connection.DefaultDatabase & " - Failed")
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Error No - " & Err.Number & " - Error Desc - " & Err.Description)
        Call Err.Report(MODULE_NAME, PROCEDURE_NAME, 0, False)
    End If
    
    


    ' Build the tree
    Call LoadTreeData
    
    
'    Set nNode = tvwItems.Nodes.Add(, , "A1", "Item List")
'    ' Set the Tag value for the node to its Level value in the Tree.
'    nNode.Tag = 1
'    ' Need to sort parent node on creation to sort child nodes, i.e. sorting node1
'    '     won't sort node3's children.
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("A1", tvwChild, "B1", "Category 1")
'    nNode.Tag = 2
'    Set nNode = tvwItems.Nodes.Add("A1", tvwChild, "B2", "Category 2")
'    nNode.Tag = 2
'    Set nNode = tvwItems.Nodes.Add("A1", tvwChild, "B3", "Category 3")
'    nNode.Tag = 2
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("B1", tvwChild, "C1", "Group 1")
'    nNode.Tag = 3
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("B1", tvwChild, "C2", "Group 2")
'    nNode.Tag = 3
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("B2", tvwChild, "C3", "Group 3")
'    nNode.Tag = 3
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("B2", tvwChild, "C4", "Group 4")
'    nNode.Tag = 3
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("B3", tvwChild, "C5", "Group 5")
'    nNode.Tag = 3
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("B3", tvwChild, "C6", "Group 6")
'    nNode.Tag = 3
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("C1", tvwChild, "D1", "Sub Group 1")
'    nNode.Tag = 4
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("C1", tvwChild, "D2", "Sub Group 2")
'    nNode.Tag = 4
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("C2", tvwChild, "D3", "Sub Group 3")
'    nNode.Tag = 4
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("C2", tvwChild, "D4", "Sub Group 4")
'    nNode.Tag = 4
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("C3", tvwChild, "D5", "Sub Group 5")
'    nNode.Tag = 4
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("C3", tvwChild, "D6", "Sub Group 6")
'    nNode.Tag = 4
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("C4", tvwChild, "D7", "Sub Group 7")
'    nNode.Tag = 4
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("C4", tvwChild, "D8", "Sub Group 8")
'    nNode.Tag = 4
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("C5", tvwChild, "D9", "Sub Group 9")
'    nNode.Tag = 4
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("C5", tvwChild, "D10", "Sub Group 10")
'    nNode.Tag = 4
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("C6", tvwChild, "D11", "Sub Group 11")
'    nNode.Tag = 4
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("C6", tvwChild, "D12", "Sub Group 12")
'    nNode.Tag = 4
'    nNode.Sorted = True
'    Set nNode = tvwItems.Nodes.Add("D1", tvwChild, "E1", "Style 1")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D1", tvwChild, "E2", "Style 2")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D2", tvwChild, "E3", "Style 3")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D2", tvwChild, "E4", "Style 4")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D3", tvwChild, "E5", "Style 5")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D3", tvwChild, "E6", "Style 6")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D4", tvwChild, "E7", "Style 7")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D4", tvwChild, "E8", "Style 8")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D5", tvwChild, "E9", "Style 9")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D5", tvwChild, "E10", "Style 10")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D6", tvwChild, "E11", "Style 11")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D6", tvwChild, "E12", "Style 12")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D7", tvwChild, "E13", "Style 13")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D7", tvwChild, "E14", "Style 14")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D8", tvwChild, "E15", "Style 15")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D8", tvwChild, "E16", "Style 16")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D9", tvwChild, "E17", "Style 17")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D9", tvwChild, "E18", "Style 18")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D10", tvwChild, "E19", "Style 19")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D10", tvwChild, "E20", "Style 20")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D11", tvwChild, "E21", "Style 21")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D11", tvwChild, "E22", "Style 22")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D12", tvwChild, "E23", "Style 23")
'    nNode.Tag = 5
'    Set nNode = tvwItems.Nodes.Add("D12", tvwChild, "E24", "Style 24")
'    nNode.Tag = 5
'
'    Set nNode = Nothing
    
End Sub

Private Sub cmdContractTree_Click()

    Call ManipulateTree(False)  ' Contract Tree.
    
End Sub

Private Sub cmdExpandTree_Click()

    Call ManipulateTree(True)  ' Expand Tree.

End Sub

Private Sub cmdEnterTree_Click()

    Me.tvwItems.SetFocus
    
End Sub

Private Sub cmdExit_Click()

    Unload Me
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If Shift = 0 Then
        Select Case (KeyCode)
            Case vbKeyF2
                    KeyCode = 0
                    Call ManipulateTree(False)  ' Contract Tree.
            
            Case vbKeyF3
                    KeyCode = 0
                    Call ManipulateTree(True)   ' Expand Tree.
            
            Case vbKeyF4
                    KeyCode = 0
                    Call cmdEnterTree_Click     ' Enter the tree.
         
            Case vbKeyF10
                    KeyCode = 0
                    Call cmdExit_Click          ' Exit.
         
         End Select
     End If

End Sub

' Load the tree with the data.
Private Sub LoadTreeData()

    Dim recItemData As ADODB.Recordset
    Dim strSQl As String
    
    Dim nNode As Node
    
    Dim strCatNode As String
    Dim strGrpNode As String
    Dim strSGrpNode As String
    Dim strStyleNode As String
    
    Dim blnBuildCategory As Boolean
    Dim blnBuildGroup As Boolean
    Dim blnBuildSubGroup As Boolean

    mlngCatCounter = 0
    mlngGrpCounter = 0
    mlngSGrpCounter = 0
    mlngStyleCounter = 0
    
    ' SQL to return the tree.
    strSQl = "SELECT a.NUMB AS CatNo, a.DESCR AS CatDesc, b.GROU AS GrpNo, b.DESCR AS GrpDesc, " & _
                "c.SGRP AS SGrpNo, c.DESCR AS SGRPDesc, d.STYL AS StylNo, d.DESCR AS StylDesc " & _
                "FROM HIECAT a INNER JOIN HIEGRP b on a.NUMB = b.NUMB " & _
                "INNER JOIN HIESGP c on b.GROU = c.GROU " & _
                "INNER JOIN HIESTY d ON c.SGRP = d.SGRP " & _
                "ORDER BY CatDesc, GrpDesc, SGRPDesc, StylDesc"
                
    ' Open the recordset for adding the data to the tree.
    If (mcnnDBConnection.State = adStateClosed) Then
        mcnnDBConnection.Open
    End If
    Set recItemData = New ADODB.Recordset
    Call recItemData.Open(strSQl, mcnnDBConnection, adOpenForwardOnly, adLockReadOnly, adCmdText)
    
    ' Build the initial tree values.
    Set nNode = tvwItems.Nodes.Add(, , "A1", "Item List")
    ' Set the Tag value for the node to its Level value in the Tree.
    nNode.Tag = 1
    ' Need to sort parent node on creation to sort child nodes, i.e. sorting node1 won't sort node3's children.
    nNode.Sorted = True
    If (recItemData.BOF = False) And (recItemData.EOF = False) Then
        ' Create Category node.
        strCatNode = CAT_CODE & GetDBField(recItemData.Fields("CatNo"), False, , ftCategory)
        Set nNode = tvwItems.Nodes.Add("A1", tvwChild, strCatNode, _
                    Trim(GetDBField(recItemData.Fields("CatDesc"), False, CAT_CODE & CStr(mlngCatCounter), ftDescription)))
        nNode.Tag = CAT_LEVEL
        ' Create Group node.
        strGrpNode = GRP_CODE & GetDBField(recItemData.Fields("GrpNo"), False, , ftGroup)
        Set nNode = tvwItems.Nodes.Add(strCatNode, tvwChild, strGrpNode, _
                    Trim(GetDBField(recItemData.Fields("GrpDesc"), False, GRP_CODE & CStr(mlngGrpCounter), ftDescription)))
        nNode.Tag = GRP_LEVEL
        ' Create Sub Group node.
        strSGrpNode = SGRP_CODE & GetDBField(recItemData.Fields("SGrpNo"), False, , ftSubGroup)
        Set nNode = tvwItems.Nodes.Add(strGrpNode, tvwChild, strSGrpNode, _
                    Trim(GetDBField(recItemData.Fields("SGrpDesc"), False, SGRP_CODE & CStr(mlngSGrpCounter), ftDescription)))
        nNode.Tag = SGRP_LEVEL
        ' Create Style leaf node.
        strStyleNode = STYLE_CODE & GetDBField(recItemData.Fields("StylNo"), False, , ftStyle)
        Set nNode = tvwItems.Nodes.Add(strSGrpNode, tvwChild, strStyleNode, _
                    Trim(GetDBField(recItemData.Fields("StylDesc"), False, STYLE_CODE & CStr(mlngStyleCounter), ftDescription)))
        nNode.Tag = STYLE_LEVEL
        
        ' Read the next record.
        recItemData.MoveNext
    End If
    
    ' Loop over the records and build the tree.
    Do Until (recItemData.BOF) Or (recItemData.EOF)
        ' Add the Category, Group, Sub Group and Style and loop over styles till any of the parent node values change, when
        '   this happens build a new branch and continue with the styles.
        blnBuildCategory = False
        blnBuildGroup = False
        blnBuildSubGroup = False
        ' If a change of category.
        If (mstrCatNo <> GetDBField(recItemData.Fields("CatNo"), True, , ftCategory)) Then
            ' Build new Category branch.
            blnBuildCategory = True
            blnBuildGroup = True
            blnBuildSubGroup = True
        ' If change of group.
        ElseIf (mstrGrpNo <> GetDBField(recItemData.Fields("GrpNo"), True, , ftGroup)) Then
            ' Build new Group branch.
            blnBuildGroup = True
            blnBuildSubGroup = True
        ' If change of sub group.
        ElseIf (mstrSGrpNo <> GetDBField(recItemData.Fields("SGrpNo"), True, , ftSubGroup)) Then
            ' Build new Sub Group branch.
            blnBuildSubGroup = True
        End If
        
        ' If required build the relavant branch.
        ' Add category branch.
        If (blnBuildCategory = True) Then
            strCatNode = CAT_CODE & GetDBField(recItemData.Fields("CatNo"), False, , ftCategory)
            Set nNode = tvwItems.Nodes.Add("A1", tvwChild, strCatNode, _
                        Trim(GetDBField(recItemData.Fields("CatDesc"), False, CAT_CODE & CStr(mlngCatCounter), ftDescription)))
            nNode.Tag = CAT_LEVEL
        End If
        
        ' Add Group branch.
        If (blnBuildGroup = True) Then
            strGrpNode = GRP_CODE & GetDBField(recItemData.Fields("GrpNo"), False, , ftGroup)
            Set nNode = tvwItems.Nodes.Add(strCatNode, tvwChild, strGrpNode, _
                        Trim(GetDBField(recItemData.Fields("GrpDesc"), False, GRP_CODE & CStr(mlngGrpCounter), ftDescription)))
            nNode.Tag = GRP_LEVEL
        End If
        
        ' Add Sub Group branch.
        If (blnBuildSubGroup = True) Then
            strSGrpNode = SGRP_CODE & GetDBField(recItemData.Fields("SGrpNo"), False, , ftSubGroup)
            Set nNode = tvwItems.Nodes.Add(strGrpNode, tvwChild, strSGrpNode, _
                        Trim(GetDBField(recItemData.Fields("SGrpDesc"), False, SGRP_CODE & CStr(mlngSGrpCounter), ftDescription)))
            nNode.Tag = SGRP_LEVEL
        End If
        
        ' Add the new style leaf.
        strStyleNode = STYLE_CODE & GetDBField(recItemData.Fields("StylNo"), False, , ftStyle)
        Set nNode = tvwItems.Nodes.Add(strSGrpNode, tvwChild, strStyleNode, _
                    Trim(GetDBField(recItemData.Fields("StylDesc"), False, STYLE_CODE & CStr(mlngStyleCounter), ftDescription)))
        nNode.Tag = STYLE_LEVEL
        
        ' Read the next record.
        recItemData.MoveNext
    Loop
    
    Set nNode = Nothing
    Set recItemData = Nothing
    
End Sub

' Test if the database value is null, if so return the default value, If blnTest = TRUE then just test the value code number.
Private Function GetDBField(ByVal field As ADODB.field, blnTest As Boolean, _
                    Optional strDefaultValue As String, Optional ftFieldType As enmFieldType) As String

    Dim blnDefault As Boolean
    Dim strValue As String
    
    ' Increment the relevant counter.
    If (blnTest = False) Then
        Select Case ftFieldType
            Case ftCategory
                mlngCatCounter = mlngCatCounter + 1
            Case ftGroup
                mlngGrpCounter = mlngGrpCounter + 1
            Case ftSubGroup
                mlngSGrpCounter = mlngSGrpCounter + 1
            Case ftStyle
                mlngStyleCounter = mlngStyleCounter + 1
        End Select
    End If
    
    ' Test if the field has a valid value.
    blnDefault = False
    If IsNull(field) Then
        blnDefault = True
    ElseIf (Trim(field) = "") Then
        blnDefault = True
    Else
        If (blnTest = False) Then
            Select Case ftFieldType
                Case ftCategory
                    mstrCatNo = Trim(field)
                Case ftGroup
                    mstrGrpNo = Trim(field)
                Case ftSubGroup
                    mstrSGrpNo = Trim(field)
            End Select
        End If
        GetDBField = Trim(field)
    End If
    
    ' Add a default value if required, if testing the Category, Group or Sub Group code then return a blank value if not found.
    If (blnDefault = True) Then
        Select Case ftFieldType
            Case ftCategory
                If (blnTest = True) Then
                    mstrCatNo = ""
                    GetDBField = ""
                Else
                    GetDBField = CAT_CODE & CStr(mlngCatCounter)
                End If
            Case ftGroup
                If (blnTest = True) Then
                    mstrGrpNo = ""
                    GetDBField = ""
                Else
                    GetDBField = GRP_CODE & CStr(mlngGrpCounter)
                End If
            Case ftSubGroup
                If (blnTest = True) Then
                    mstrSGrpNo = ""
                    GetDBField = ""
                Else
                    GetDBField = SGRP_CODE & CStr(mlngSGrpCounter)
                End If
            Case ftStyle
                GetDBField = STYLE_CODE & CStr(mlngStyleCounter)
            Case ftDescription
                GetDBField = strDefaultValue
        End Select
    End If
    
End Function
    
' Expand or contract the tree one level.
Private Sub ManipulateTree(ByVal blnExpand As Boolean)

    Dim nNode As Node
    
    Dim intLastSiblingIndex As Integer
    
    Dim intTreeLevel As Integer
    Dim intLevel As Integer
    Dim intCounter As Integer
    
    ' Get the max tree branch level visible.
    intTreeLevel = GetMaxTreeLevel
    
    ' If already at the max contraction  then do nothing.
    If (intTreeLevel = 1) And (blnExpand = False) Then
        Me.tvwItems.SetFocus
        Exit Sub
    End If
        
    ' Stop the tree from scrolling.
    Me.tvwItems.Scroll = False
    
    ' Get the Root node.
    Set nNode = Me.tvwItems.Nodes(1)
    
    ' Expansion
    If (blnExpand = True) Then
        
        ' If required to see the max branch level then adjust the tree level value to select the max branch.
        If (intTreeLevel = TREE_LEVELS) Then
            intTreeLevel = intTreeLevel - 1
        End If
        ' Loop over all the nodes.
        For intCounter = 1 To Me.tvwItems.Nodes.Count
            ' If the node is at the required level then show it.
            Set nNode = Me.tvwItems.Nodes(intCounter)
            If (nNode.Tag = intTreeLevel + 1) Then
                ' Show the node.
                nNode.EnsureVisible
                ' Contract any sub nodes.
                nNode.Expanded = False
            End If
        Next intCounter
        
        ' Move to the first node of the required level.
        Set nNode = Me.tvwItems.Nodes(1)
        For intCounter = 1 To intTreeLevel
            Set nNode = nNode.Child
        Next intCounter
    
    Else
    ' Contraction
    
        ' Loop over all the nodes.
        For intCounter = 1 To Me.tvwItems.Nodes.Count
            ' If the node is at the required level then show it.
            Set nNode = Me.tvwItems.Nodes(intCounter)
            If (nNode.Tag = intTreeLevel - 1) Then
                ' Show the node.
                nNode.EnsureVisible
                ' Contract any sub nodes.
                nNode.Expanded = False
            End If
        Next intCounter
    
        ' Move to the first node of the required level.
        Set nNode = Me.tvwItems.Nodes(1)
        For intCounter = 1 To intTreeLevel - 2
            Set nNode = nNode.Child
        Next intCounter
    
    End If
    
    ' Select the first node of the required level.
    nNode.Selected = True
    Me.tvwItems.SetFocus
        
    Set nNode = Nothing
    
    ' Allow the tree to scroll.
    Me.tvwItems.Scroll = True
    
End Sub

' Scan the tree to get the max tree level for use in contraction or expansion of the tree.
Private Function GetMaxTreeLevel() As Integer

    Dim nNode As Node
    
    Dim intCounter As Integer
    Dim intMaxLevel As Integer
    
    ' Loop over all the nodes.
    For intCounter = 1 To Me.tvwItems.Nodes.Count
        ' Store the highest level of visible node.
        Set nNode = Me.tvwItems.Nodes(intCounter)
        If (nNode.Visible = True) Then
            If (nNode.Tag > intMaxLevel) Then
                intMaxLevel = nNode.Tag
                ' If the visible node is at the max level, deepest branch then exit the loop as know at the end of a branch.
                If (intMaxLevel = TREE_LEVELS) Then
                    Exit For
                End If
            End If
        End If
    Next intCounter
    
    GetMaxTreeLevel = intMaxLevel
    Set nNode = Nothing
    
End Function

Private Sub Form_Unload(Cancel As Integer)

    If (mcnnDBConnection.State = adStateOpen) Then
        mcnnDBConnection.Close
    End If
    Set mcnnDBConnection = Nothing
    
    End
End Sub
