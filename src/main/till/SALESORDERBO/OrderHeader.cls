VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cOrderHeader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D539050001E"
'<CAMH>****************************************************************************************
'* Module : cOrderHeader
'* Date   : 16/10/02
'* Date   : 14/08/02
'*$Archive: /Projects/OasysV2/VB/SalesOrderBO/OrderHeader.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of a single Sales Order Header
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 8/04/04 9:17 $
'* $Revision: 4 $
'* Versions:
'* 16/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cOrderHeader"

Implements IBo
Implements ISysBo

'##ModelId=3D53921C008C
Private mOrderNo As String

'##ModelId=3D5392230000
Private mCustomerNo As String

'##ModelId=3D53922B023A
Private mSalesman As String

'##ModelId=3D5392320226
Private mTillID As String

'##ModelId=3D53923701AE
Private mCustomerPresent As Boolean

'##ModelId=3D53923F03B6
Private mName As String

'##ModelId=3D53924200FA
Private mAddressLine1 As String

'##ModelId=3D5392450226
Private mAddressLine2 As String

'##ModelId=3D5392490230
Private mAddressLine3 As String

'##ModelId=3D53924F0000
Private mPostCode As String

'##ModelId=3D5392530154
Private mTelephoneNo As String

'##ModelId=3D53925900A0
Private mSecondTelNo As String

'##ModelId=3D53926100B4
Private mDepositNumber As String

'##ModelId=3D53926500BE
Private mDepositValue As Currency

'##ModelId=3D53926E008C
Private mOrderProcessed As Boolean

'##ModelId=3D53927700C8
Private mPrinted As Boolean

'##ModelId=3D53927D0352
Private mStatus As String

'##ModelId=3D5392860280
Private mQuoteDate As Date

'##ModelId=3D53928B02E4
Private mQuoteExpiry As Date

'##ModelId=3D5392940028
Private mOrderDate As Date

'##ModelId=3D53929700DC
Private mETADate As Date

'##ModelId=3D53938201D6
Private mNoDeliveries As Long

'##ModelId=3D53938A0258
Private mComplete As Boolean

Private mLastDespatchDate As Date

'##ModelId=3D53938F0258
Private mCancelled As Boolean

'##ModelId=3D53939D010E
Private mCancelledDate As Date

'##ModelId=3D5393A80172
Private mCancelledCashierID As String

'##ModelId=3D5393B100D2
Private mOrderValue As Currency

'##ModelId=3D5393C3037A
Private mOutStandingValue As Currency

'##ModelId=3D5393CA00C8
Private mRemarks(4) As String

'##ModelId=3D5393D50352
Private mNameAlphaKey As String

'##ModelId=3D5393DD0398
Private mDiscountReasonCode As String

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

Private mcolLines As Collection 'local collection of Purchase Order Lines

Private mGetLines As Boolean

Private Sub RetrieveLines()

Dim oLine As cSalesOrder_Wickes.cOrderLine

    'If Order found then get lines for Order
    Set mcolLines = New Collection
    Set oLine = m_oSession.Database.CreateBusinessObject(CLASSID_SOLINE)
    Call oLine.IBo_AddLoadFilter(CMP_EQUAL, FID_SOLINE_OrderNo, mOrderNo)
    Set mcolLines = oLine.LoadLines

End Sub

Public Function RecordAsPrinted() As Boolean

Const PROCEDURE_NAME = "RecordAsPrinted"

Dim oRow        As IRow
Dim oGField     As CFieldGroup

    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_SOHEADER_OrderNo))
    
    mPrinted = True
    Set oGField = GetField(FID_SOHEADER_Printed)
    ' and add to the row
    Call oRow.Add(oGField)
    
    Call m_oSession.Database.SavePartialBO(Me, oRow)
        
    Call DebugMsg(MODULE_NAME, "RecordAsPrinted", endlDebug, "Record as printed - " & mOrderNo)

End Function

Public Function AddItem(PartCode As String) As cSalesOrder_Wickes.cOrderLine

Dim oLine As cSalesOrder_Wickes.cOrderLine

    Set oLine = m_oSession.Database.CreateBusinessObject(CLASSID_SOLINE)
    oLine.PartCode = PartCode
    oLine.Complete = False
    
    If mcolLines Is Nothing Then Set mcolLines = New Collection
    Call mcolLines.Add(oLine)
    oLine.LineNo = mcolLines.Count
    Set AddItem = oLine

End Function

Public Sub DeleteItem()
End Sub

Public Function Lines() As Collection

    If (mcolLines Is Nothing) And (mGetLines = True) Then
        Call RetrieveLines
    End If
    Set Lines = mcolLines
    
End Function

'##ModelId=3D5A501E0226
Public Property Get DiscountReasonCode() As String
   Let DiscountReasonCode = mDiscountReasonCode
End Property

'##ModelId=3D5A501E00A0
Public Property Let DiscountReasonCode(ByVal Value As String)
    Let mDiscountReasonCode = Value
End Property

'##ModelId=3D5A501D037A
Public Property Get NameAlphaKey() As String
   Let NameAlphaKey = mNameAlphaKey
End Property

'##ModelId=3D5A501D0230
Public Property Let NameAlphaKey(ByVal Value As String)
    Let mNameAlphaKey = Value
End Property

'##ModelId=3D5A501D0118
Public Property Get Remarks() As String
   Let Remarks = mRemarks(1) & vbCrLf & mRemarks(2) & vbCrLf & mRemarks(3) & vbCrLf & mRemarks(4)
End Property

'##ModelId=3D5A501C03C0
Public Property Let Remarks(ByVal Value As String)

    'Reset remarks holders to clear out old data
    Let mRemarks(1) = ""
    Let mRemarks(2) = ""
    Let mRemarks(3) = ""
    Let mRemarks(4) = ""
    mRemarks(1) = Value
    'if instr(value,vbcrlf ) > 0 then
    
End Property

'##ModelId=3D5A501C02E4
Public Property Get OutStandingValue() As Currency
   Let OutStandingValue = mOutStandingValue
End Property

'##ModelId=3D5A501C019A
Public Property Let OutStandingValue(ByVal Value As Currency)
    Let mOutStandingValue = Value
End Property

'##ModelId=3D5A501C0082
Public Property Get OrderValue() As Currency
   Let OrderValue = mOrderValue
End Property

'##ModelId=3D5A501B0320
Public Property Let OrderValue(ByVal Value As Currency)
    Let mOrderValue = Value
End Property

'##ModelId=3D5A501B0244
Public Property Get CancelledCashierID() As String
   Let CancelledCashierID = mCancelledCashierID
End Property

'##ModelId=3D5A501B00FA
Public Property Let CancelledCashierID(ByVal Value As String)
    Let mCancelledCashierID = Value
End Property

'##ModelId=3D5A501B001E
Public Property Get CancelledDate() As Date
   Let CancelledDate = mCancelledDate
End Property

'##ModelId=3D5A501A02BC
Public Property Let CancelledDate(ByVal Value As Date)
    Let mCancelledDate = Value
End Property

Public Property Get LastDespatchDate() As Date
   Let LastDespatchDate = mLastDespatchDate
End Property

'##ModelId=3D5A501A02BC
Public Property Let LastDespatchDate(ByVal Value As Date)
    Let mLastDespatchDate = Value
End Property

'##ModelId=3D5A501A01E0
Public Property Get Cancelled() As Boolean
   Let Cancelled = mCancelled
End Property

'##ModelId=3D5A501A00D2
Public Property Let Cancelled(ByVal Value As Boolean)
    Let mCancelled = Value
End Property

'##ModelId=3D5A501903DE
Public Property Get Complete() As Boolean
   Let Complete = mComplete
End Property

'##ModelId=3D5A50190294
Public Property Let Complete(ByVal Value As Boolean)
    Let mComplete = Value
End Property

'##ModelId=3D5A501901F4
Public Property Get NoDeliveries() As Long
   Let NoDeliveries = mNoDeliveries
End Property

'##ModelId=3D5A501900AA
Public Property Let NoDeliveries(ByVal Value As Long)
    Let mNoDeliveries = Value
End Property

'##ModelId=3D5A501803B6
Public Property Get ETADate() As Date
   Let ETADate = mETADate
End Property

'##ModelId=3D5A5018029E
Public Property Let ETADate(ByVal Value As Date)
    Let mETADate = Value
End Property

'##ModelId=3D5A501801FE
Public Property Get OrderDate() As Date
   Let OrderDate = mOrderDate
End Property

'##ModelId=3D5A501800E6
Public Property Let OrderDate(ByVal Value As Date)
    Let mOrderDate = Value
End Property

'##ModelId=3D5A5018000A
Public Property Get QuoteExpiry() As Date
   Let QuoteExpiry = mQuoteExpiry
End Property

'##ModelId=3D5A501702E4
Public Property Let QuoteExpiry(ByVal Value As Date)
    Let mQuoteExpiry = Value
End Property

'##ModelId=3D5A5017023A
Public Property Get QuoteDate() As Date
   Let QuoteDate = mQuoteDate
End Property

'##ModelId=3D5A5017012C
Public Property Let QuoteDate(ByVal Value As Date)
    Let mQuoteDate = Value
End Property

'##ModelId=3D5A50170082
Public Property Get Status() As String
   Let Status = mStatus
End Property

'##ModelId=3D5A5016035C
Public Property Let Status(ByVal Value As String)
    Let mStatus = Value
End Property

'##ModelId=3D5A501602B2
Public Property Get Printed() As Boolean
   Let Printed = mPrinted
End Property

'##ModelId=3D5A501601A4
Public Property Let Printed(ByVal Value As Boolean)
    Let mPrinted = Value
End Property

'##ModelId=3D5A501600FA
Public Property Get OrderProcessed() As Boolean
   Let OrderProcessed = mOrderProcessed
End Property

'##ModelId=3D5A5016001E
Public Property Let OrderProcessed(ByVal Value As Boolean)
    Let mOrderProcessed = Value
End Property

'##ModelId=3D5A50150334
Public Property Get DepositValue() As Currency
   Let DepositValue = mDepositValue
End Property

'##ModelId=3D5A50150258
Public Property Let DepositValue(ByVal Value As Currency)
    Let mDepositValue = Value
End Property

'##ModelId=3D5A501501AE
Public Property Get DepositNumber() As String
   Let DepositNumber = mDepositNumber
End Property

'##ModelId=3D5A501500D2
Public Property Let DepositNumber(ByVal Value As String)
    Let mDepositNumber = Value
End Property

'##ModelId=3D5A50150064
Public Property Get SecondTelNo() As String
   Let SecondTelNo = mSecondTelNo
End Property

'##ModelId=3D5A50140370
Public Property Let SecondTelNo(ByVal Value As String)
    Let mSecondTelNo = Value
End Property

'##ModelId=3D5A501402D0
Public Property Get TelephoneNo() As String
   Let TelephoneNo = mTelephoneNo
End Property

'##ModelId=3D5A501401F4
Public Property Let TelephoneNo(ByVal Value As String)
    Let mTelephoneNo = Value
End Property

'##ModelId=3D5A50140186
Public Property Get PostCode() As String
   Let PostCode = mPostCode
End Property

'##ModelId=3D5A501400AA
Public Property Let PostCode(ByVal Value As String)
    Let mPostCode = Value
End Property

'##ModelId=3D5A5014003C
Public Property Get AddressLine3() As String
   Let AddressLine3 = mAddressLine3
End Property

'##ModelId=3D5A50130348
Public Property Let AddressLine3(ByVal Value As String)
    Let mAddressLine3 = Value
End Property

'##ModelId=3D5A501302DA
Public Property Get AddressLine2() As String
   Let AddressLine2 = mAddressLine2
End Property

'##ModelId=3D5A50130230
Public Property Let AddressLine2(ByVal Value As String)
    Let mAddressLine2 = Value
End Property

'##ModelId=3D5A501301C2
Public Property Get AddressLine1() As String
   Let AddressLine1 = mAddressLine1
End Property

'##ModelId=3D5A50130122
Public Property Let AddressLine1(ByVal Value As String)
    Let mAddressLine1 = Value
End Property

'##ModelId=3D5A50130078
Public Property Get Name() As String
   Let Name = mName
End Property

'##ModelId=3D5A501203C0
Public Property Let Name(ByVal Value As String)
    Let mName = Value
End Property

'##ModelId=3D5A50120352
Public Property Get CustomerPresent() As Boolean
   Let CustomerPresent = mCustomerPresent
End Property

'##ModelId=3D5A501202E4
Public Property Let CustomerPresent(ByVal Value As Boolean)
    Let mCustomerPresent = Value
End Property

'##ModelId=3D5A50120276
Public Property Get TillID() As String
   Let TillID = mTillID
End Property

'##ModelId=3D5A501201CC
Public Property Let TillID(ByVal Value As String)
    Let mTillID = Value
End Property

'##ModelId=3D5A5012015E
Public Property Get Salesman() As String
   Let Salesman = mSalesman
End Property

'##ModelId=3D5A501200F0
Public Property Let Salesman(ByVal Value As String)
    Let mSalesman = Value
End Property

'##ModelId=3D5A5012008C
Public Property Get CustomerNo() As String
   Let CustomerNo = mCustomerNo
End Property

'##ModelId=3D5A501103CA
Public Property Let CustomerNo(ByVal Value As String)
    Let mCustomerNo = Value
End Property

'##ModelId=3D5A50110398
Public Property Get OrderNo() As String
   Let OrderNo = mOrderNo
End Property

'##ModelId=3D5A5011032A
Public Property Let OrderNo(ByVal Value As String)
    Let mOrderNo = Value
End Property
Public Property Get GetLines() As Boolean
   Let GetLines = mGetLines
End Property

Public Property Let GetLines(ByVal Value As Boolean)
    Let mGetLines = Value
End Property


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 14/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_SOHEADER_OrderNo):            mOrderNo = oField.ValueAsVariant
            Case (FID_SOHEADER_CustomerNo):         mCustomerNo = oField.ValueAsVariant
            Case (FID_SOHEADER_Salesman):           mSalesman = oField.ValueAsVariant
            Case (FID_SOHEADER_TillID):             mTillID = oField.ValueAsVariant
            Case (FID_SOHEADER_CustomerPresent):    mCustomerPresent = oField.ValueAsVariant
            Case (FID_SOHEADER_Name):               mName = oField.ValueAsVariant
            Case (FID_SOHEADER_AddressLine1):       mAddressLine1 = oField.ValueAsVariant
            Case (FID_SOHEADER_AddressLine2):       mAddressLine2 = oField.ValueAsVariant
            Case (FID_SOHEADER_AddressLine3):       mAddressLine3 = oField.ValueAsVariant
            Case (FID_SOHEADER_PostCode):           mPostCode = oField.ValueAsVariant
            Case (FID_SOHEADER_TelephoneNo):        mTelephoneNo = oField.ValueAsVariant
            Case (FID_SOHEADER_SecondTelNo):        mSecondTelNo = oField.ValueAsVariant
            Case (FID_SOHEADER_DepositNumber):      mDepositNumber = oField.ValueAsVariant
            Case (FID_SOHEADER_DepositValue):       mDepositValue = oField.ValueAsVariant
            Case (FID_SOHEADER_OrderProcessed):     mOrderProcessed = oField.ValueAsVariant
            Case (FID_SOHEADER_Printed):            mPrinted = oField.ValueAsVariant
            Case (FID_SOHEADER_Status):             mStatus = oField.ValueAsVariant
            Case (FID_SOHEADER_QuoteDate):          mQuoteDate = oField.ValueAsVariant
            Case (FID_SOHEADER_QuoteExpiry):        mQuoteExpiry = oField.ValueAsVariant
            Case (FID_SOHEADER_OrderDate):          mOrderDate = oField.ValueAsVariant
            Case (FID_SOHEADER_ETADate):            mETADate = oField.ValueAsVariant
            Case (FID_SOHEADER_NoDeliveries):       mNoDeliveries = oField.ValueAsVariant
            Case (FID_SOHEADER_Complete):           mComplete = oField.ValueAsVariant
            Case (FID_SOHEADER_LastDespatchDate):   mLastDespatchDate = oField.ValueAsVariant
            Case (FID_SOHEADER_Cancelled):          mCancelled = oField.ValueAsVariant
            Case (FID_SOHEADER_CancelledDate):      mCancelledDate = oField.ValueAsVariant
            Case (FID_SOHEADER_CancelledCashierID): mCancelledCashierID = oField.ValueAsVariant
            Case (FID_SOHEADER_OrderValue):         mOrderValue = oField.ValueAsVariant
            Case (FID_SOHEADER_OutStandingValue):   mOutStandingValue = oField.ValueAsVariant
            Case (FID_SOHEADER_Remarks):            Call LoadStringArray(mRemarks, oField, m_oSession)
            Case (FID_SOHEADER_NameAlphaKey):       mNameAlphaKey = oField.ValueAsVariant
            Case (FID_SOHEADER_DiscountReasonCode): mDiscountReasonCode = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow



Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Sales Order " & mOrderNo

End Property
Private Function Initialise(oSession As ISession) As cOrderHeader
    Set m_oSession = oSession
    Set Initialise = Me
End Function


Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_SOHEADER

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property


Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function
Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_SOHEADER, FID_SOHEADER_END_OF_STATIC, m_oSession)

End Function


Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function
Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cOrderHeader

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function

Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database

Dim lLineNo   As Long
Dim oRow      As IRow 'need to access Row saved as ID will be passed back for Lines
Dim oSysNum   As cSystemNumbers
    
    Save = False
    
    If IsValid() Then
        Set oRow = GetAllFieldsAsRow()
        ' Get a row and pass it to the database to insert/update/delete
        If eSave = SaveTypeIfNew Then
            'Get Next Sales Order Number
            Set oSysNum = m_oSession.Database.CreateBusinessObject(CLASSID_SYSTEMNO)
            mOrderNo = oSysNum.GetNewCustomerOrderNumber
            Set oSysNum = Nothing
            If mCustomerNo = "" Then mCustomerNo = "000000"
            If mDepositNumber = "" Then mDepositNumber = "000000"
            If mStatus = "" Then mStatus = "O"
            If mDiscountReasonCode = "" Then mDiscountReasonCode = "00"
            For lLineNo = 1 To mcolLines.Count Step 1
                If mcolLines(CLng(lLineNo)).OrderNo = "" Then mcolLines(CLng(lLineNo)).OrderNo = mOrderNo
                Call mcolLines(CLng(lLineNo)).IBo_SaveIfNew
            Next lLineNo
        End If
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_SOHEADER * &H10000) + 1 To FID_SOHEADER_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_AddLoadFilter = True

End Function 'IBo_AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'IBo_AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function
Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_SOHEADER_OrderNo):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                    GetField.ValueAsVariant = mOrderNo
        Case (FID_SOHEADER_CustomerNo):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                    GetField.ValueAsVariant = mCustomerNo
        Case (FID_SOHEADER_Salesman):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                    GetField.ValueAsVariant = mSalesman
        Case (FID_SOHEADER_TillID):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                    GetField.ValueAsVariant = mTillID
        Case (FID_SOHEADER_CustomerPresent):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldBool")
                    GetField.ValueAsVariant = mCustomerPresent
        Case (FID_SOHEADER_Name):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                    GetField.ValueAsVariant = mName
        Case (FID_SOHEADER_AddressLine1):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                    GetField.ValueAsVariant = mAddressLine1
        Case (FID_SOHEADER_AddressLine2):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                    GetField.ValueAsVariant = mAddressLine2
        Case (FID_SOHEADER_AddressLine3):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                    GetField.ValueAsVariant = mAddressLine3
        Case (FID_SOHEADER_PostCode):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                    GetField.ValueAsVariant = mPostCode
        Case (FID_SOHEADER_TelephoneNo):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                    GetField.ValueAsVariant = mTelephoneNo
        Case (FID_SOHEADER_SecondTelNo):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                    GetField.ValueAsVariant = mSecondTelNo
        Case (FID_SOHEADER_DepositNumber):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                    GetField.ValueAsVariant = mDepositNumber
        Case (FID_SOHEADER_DepositValue):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldDouble")
                    GetField.ValueAsVariant = mDepositValue
        Case (FID_SOHEADER_OrderProcessed):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldBool")
                    GetField.ValueAsVariant = mOrderProcessed
        Case (FID_SOHEADER_Printed):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldBool")
                    GetField.ValueAsVariant = mPrinted
        Case (FID_SOHEADER_Status):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                    GetField.ValueAsVariant = mStatus
        Case (FID_SOHEADER_QuoteDate):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldDate")
                    GetField.ValueAsVariant = mQuoteDate
        Case (FID_SOHEADER_QuoteExpiry):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldDate")
                    GetField.ValueAsVariant = mQuoteExpiry
        Case (FID_SOHEADER_OrderDate):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldDate")
                    GetField.ValueAsVariant = mOrderDate
        Case (FID_SOHEADER_ETADate):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldDate")
                    GetField.ValueAsVariant = mETADate
        Case (FID_SOHEADER_NoDeliveries):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldLong")
                    GetField.ValueAsVariant = mNoDeliveries
        Case (FID_SOHEADER_Complete):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldBool")
                    GetField.ValueAsVariant = mComplete
        Case (FID_SOHEADER_LastDespatchDate):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldDate")
                    GetField.ValueAsVariant = mLastDespatchDate
        Case (FID_SOHEADER_Cancelled):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldBool")
                    GetField.ValueAsVariant = mCancelled
        Case (FID_SOHEADER_CancelledDate):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldDate")
                    GetField.ValueAsVariant = mCancelledDate
        Case (FID_SOHEADER_CancelledCashierID):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                    GetField.ValueAsVariant = CancelledCashierID
        Case (FID_SOHEADER_OrderValue):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldDouble")
                    GetField.ValueAsVariant = mOrderValue
        Case (FID_SOHEADER_OutStandingValue):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldDouble")
                    GetField.ValueAsVariant = mOutStandingValue
        Case (FID_SOHEADER_Remarks): Set GetField = SaveStringArray(mRemarks, m_oSession)
        Case (FID_SOHEADER_NameAlphaKey):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                    GetField.ValueAsVariant = mNameAlphaKey
        Case (FID_SOHEADER_DiscountReasonCode):
                    Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                    GetField.ValueAsVariant = mDiscountReasonCode
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function


Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As cOrderHeader
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_SOHEADER_END_OF_STATIC

End Function

