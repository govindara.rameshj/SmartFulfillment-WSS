VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cOrderLine"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D5394DC0186"
'<CAMH>****************************************************************************************
'* Module: cOrderLine
'* Date  : 14/08/02
'* Author: mauricem
'*$Archive: /Projects/OasysV2/VB/SalesOrderBO/OrderLine.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of a single sales order line
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 4/04/03 12:25p $
'* $Revision: 5 $
'* Versions:
'* 14/08/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cOrderLine"

Implements IBo
Implements ISysBo

'##ModelId=3D53952100A0
Private mOrderNo As String

'##ModelId=3D53952602F8
Private mLineNo As String

'##ModelId=3D53952C00FA
Private mPartCode As String

'##ModelId=3D53952F0226
Private mDescription As String

'##ModelId=3D539538000A
Private mSupplierNo As String

'##ModelId=3D53953C026C
Private mOrderQuantity As Long

'##ModelId=3D53954A0352
Private mLookUpPrice As Currency

'##ModelId=3D53954F00F0
Private mOrderPrice As Currency

'##ModelId=3D53955303C0
Private mExtendedValue As Currency

'##ModelId=3D53955E0190
Private mExtendedCost As Double

'##ModelId=3D539567015E
Private mLineDiscount As Currency

'##ModelId=3D53957400C8
Private mQtyPerUnit As Long

Private mLastDelNoteNo As Long

Private mQuantitySold As Long

'##ModelId=3D5395890276
Private mRaiseOrder As Boolean

'##ModelId=3D5395990262
Private mPurchaseOrderNo As String

'##ModelId=3D5395A100BE
Private mCancelled As Boolean

'##ModelId=3D5395A30370
Private mComplete As Boolean

'##ModelId=3D5395A70230
Private mCancellationReason As String

'##ModelId=3D5395B2010E
Private mDateCreated As Date

'##ModelId=3D5395B70032
Private mDiscountReasonCode As String

'##ModelId=3D5395C0033E
Private mRefundAllowed As String

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=3D5A502701C2
Public Property Get RefundAllowed() As String
   Let RefundAllowed = mRefundAllowed
End Property

'##ModelId=3D5A502700AA
Public Property Let RefundAllowed(ByVal Value As String)
    Let mRefundAllowed = Value
End Property

'##ModelId=3D5A5027000A
Public Property Get DiscountReasonCode() As String
   Let DiscountReasonCode = mDiscountReasonCode
End Property

'##ModelId=3D5A50260316
Public Property Let DiscountReasonCode(ByVal Value As String)
    Let mDiscountReasonCode = Value
End Property

'##ModelId=3D5A5026026C
Public Property Get DateCreated() As Date
   Let DateCreated = mDateCreated
End Property

'##ModelId=3D5A50260190
Public Property Let DateCreated(ByVal Value As Date)
    Let mDateCreated = Value
End Property

Public Property Get LastDelNoteNo() As Long
   Let LastDelNoteNo = mLastDelNoteNo
End Property

Public Property Let LastDelNoteNo(ByVal Value As Long)
    Let mLastDelNoteNo = Value
End Property

Public Property Get QuantitySold() As Long
   Let QuantitySold = mQuantitySold
End Property

Public Property Let QuantitySold(ByVal Value As Long)
    Let mQuantitySold = Value
End Property

'##ModelId=3D5A502600F0
Public Property Get CancellationReason() As String
   Let CancellationReason = mCancellationReason
End Property

'##ModelId=3D5A50260014
Public Property Let CancellationReason(ByVal Value As String)
    Let mCancellationReason = Value
End Property

'##ModelId=3D5A50250352
Public Property Get Complete() As Boolean
   Let Complete = mComplete
End Property

'##ModelId=3D5A502502B2
Public Property Let Complete(ByVal Value As Boolean)
    Let mComplete = Value
End Property

'##ModelId=3D5A50250208
Public Property Get Cancelled() As Boolean
   Let Cancelled = mCancelled
End Property

'##ModelId=3D5A5025012C
Public Property Let Cancelled(ByVal Value As Boolean)
    Let mCancelled = Value
End Property

'##ModelId=3D5A502500BE
Public Property Get PurchaseOrderNo() As String
   Let PurchaseOrderNo = mPurchaseOrderNo
End Property

'##ModelId=3D5A502403CA
Public Property Let PurchaseOrderNo(ByVal Value As String)
    Let mPurchaseOrderNo = Value
End Property

'##ModelId=3D5A5024035C
Public Property Get RaiseOrder() As Boolean
   Let RaiseOrder = mRaiseOrder
End Property

'##ModelId=3D5A502402BC
Public Property Let RaiseOrder(ByVal Value As Boolean)
    Let mRaiseOrder = Value
End Property

'##ModelId=3D5A5024021C
Public Property Get QtyPerUnit() As Long
   Let QtyPerUnit = mQtyPerUnit
End Property

'##ModelId=3D5A50240172
Public Property Let QtyPerUnit(ByVal Value As Long)
    Let mQtyPerUnit = Value
End Property

'##ModelId=3D5A50240104
Public Property Get LineDiscount() As Currency
   Let LineDiscount = mLineDiscount
End Property

'##ModelId=3D5A50240064
Public Property Let LineDiscount(ByVal Value As Currency)
    Let mLineDiscount = Value
End Property

'##ModelId=3D5A502303DE
Public Property Get ExtendedCost() As Double
   Let ExtendedCost = mExtendedCost
End Property

'##ModelId=3D5A50230334
Public Property Let ExtendedCost(ByVal Value As Double)
    Let mExtendedCost = Value
End Property

'##ModelId=3D5A502302C6
Public Property Get ExtendedValue() As Currency
   Let ExtendedValue = mExtendedValue
End Property

'##ModelId=3D5A50230226
Public Property Let ExtendedValue(ByVal Value As Currency)
    Let mExtendedValue = Value
End Property

'##ModelId=3D5A502301B8
Public Property Get OrderPrice() As Currency
   Let OrderPrice = mOrderPrice
End Property

'##ModelId=3D5A5023014A
Public Property Let OrderPrice(ByVal Value As Currency)
    Let mOrderPrice = Value
End Property

'##ModelId=3D5A502300DC
Public Property Get LookUpPrice() As Currency
   Let LookUpPrice = mLookUpPrice
End Property

'##ModelId=3D5A50230032
Public Property Let LookUpPrice(ByVal Value As Currency)
    Let mLookUpPrice = Value
End Property

'##ModelId=3D5A50230000
Public Property Get OrderQuantity() As Long
   Let OrderQuantity = mOrderQuantity
End Property

'##ModelId=3D5A5022037A
Public Property Let OrderQuantity(ByVal Value As Long)
    Let mOrderQuantity = Value
End Property

'##ModelId=3D5A5022030C
Public Property Get SupplierNo() As String
   Let SupplierNo = mSupplierNo
End Property

'##ModelId=3D5A5022029E
Public Property Let SupplierNo(ByVal Value As String)
    Let mSupplierNo = Value
End Property

'##ModelId=3D5A50220230
Public Property Get Description() As String
   Let Description = mDescription
End Property

'##ModelId=3D5A502201C2
Public Property Let Description(ByVal Value As String)
    Let mDescription = Value
End Property

'##ModelId=3D5A50220186
Public Property Get PartCode() As String
   Let PartCode = mPartCode
End Property

'##ModelId=3D5A50220118
Public Property Let PartCode(ByVal Value As String)
    Let mPartCode = Value
End Property

'##ModelId=3D5A502200E6
Public Property Get LineNo() As String
   Let LineNo = mLineNo
End Property

'##ModelId=3D5A50220078
Public Property Let LineNo(ByVal Value As String)
    Let mLineNo = Value
End Property

'##ModelId=3D5A5022003C
Public Property Get OrderNo() As String
   Let OrderNo = mOrderNo
End Property

'##ModelId=3D5A5022000A
Public Property Let OrderNo(ByVal Value As String)
    Let mOrderNo = Value
End Property
Friend Function LoadLines() As Collection

' Load up all properties from the database
Dim oView       As IView
Dim lLineNo     As Long
Dim colLines    As Collection
Dim oLine       As cOrderLine
Dim oRow        As IRow
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    'Merge selection criteria together
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        'Build up a collection of Purchase Order Lines that will be passed out
        Set colLines = New Collection
        For lLineNo = 1 To oView.Count
            Set oLine = m_oSession.Database.CreateBusinessObject(CLASSID_SOLINE)
            Set oRow = oView.Row(CLng(lLineNo))
            Call oLine.LoadFromRow(oRow)
            Call colLines.Add(oLine)
        Next lLineNo
    End If
    Set LoadLines = colLines

End Function



'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 14/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_SOLINE_OrderNo):              mOrderNo = oField.ValueAsVariant
            Case (FID_SOLINE_LineNo):               mLineNo = oField.ValueAsVariant
            Case (FID_SOLINE_PartCode):             mPartCode = oField.ValueAsVariant
            Case (FID_SOLINE_Description):          mDescription = oField.ValueAsVariant
            Case (FID_SOLINE_SupplierNo):           mSupplierNo = oField.ValueAsVariant
            Case (FID_SOLINE_OrderQuantity):        mOrderQuantity = oField.ValueAsVariant
            Case (FID_SOLINE_LookUpPrice):          mLookUpPrice = oField.ValueAsVariant
            Case (FID_SOLINE_OrderPrice):           mOrderPrice = oField.ValueAsVariant
            Case (FID_SOLINE_ExtendedValue):        mExtendedValue = oField.ValueAsVariant
            Case (FID_SOLINE_ExtendedCost):         mExtendedCost = oField.ValueAsVariant
            Case (FID_SOLINE_LineDiscount):         mLineDiscount = oField.ValueAsVariant
            Case (FID_SOLINE_QtyPerUnit):           mQtyPerUnit = oField.ValueAsVariant
            Case (FID_SOLINE_RaiseOrder):           mRaiseOrder = oField.ValueAsVariant
            Case (FID_SOLINE_PurchaseOrderNo):      mPurchaseOrderNo = oField.ValueAsVariant
            Case (FID_SOLINE_LastDelNoteNo):        mLastDelNoteNo = oField.ValueAsVariant
            Case (FID_SOLINE_QuantitySold):         mQuantitySold = oField.ValueAsVariant
            Case (FID_SOLINE_Cancelled):            mCancelled = oField.ValueAsVariant
            Case (FID_SOLINE_Complete):             mComplete = oField.ValueAsVariant
            Case (FID_SOLINE_CancellationReason):   mCancellationReason = oField.ValueAsVariant
            Case (FID_SOLINE_DateCreated):          mDateCreated = oField.ValueAsVariant
            Case (FID_SOLINE_DiscountReasonCode):   mDiscountReasonCode = oField.ValueAsVariant
            Case (FID_SOLINE_RefundAllowed):        mRefundAllowed = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow



Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Order " & mOrderNo & " Line " & mLineNo

End Property

Private Function Initialise(oSession As ISession) As cOrderLine
    Set m_oSession = oSession
    Set Initialise = Me
End Function


Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_SOLINE

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_SOLINE * &H10000) + 1 To FID_SOLINE_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_AddLoadFilter = True

End Function 'IBo_AddLoadFilter


'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'IBo_AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cOrderLine

End Function


Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_SOLINE, FID_SOLINE_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_SOLINE_OrderNo):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mOrderNo
        Case (FID_SOLINE_LineNo):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mLineNo
        Case (FID_SOLINE_PartCode):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mPartCode
        Case (FID_SOLINE_Description):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mDescription
        Case (FID_SOLINE_SupplierNo):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mSupplierNo
        Case (FID_SOLINE_OrderQuantity):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldLong")
                GetField.ValueAsVariant = mOrderQuantity
        Case (FID_SOLINE_LookUpPrice):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldDouble")
                GetField.ValueAsVariant = mLookUpPrice
        Case (FID_SOLINE_OrderPrice):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldDouble")
                GetField.ValueAsVariant = mOrderPrice
        Case (FID_SOLINE_ExtendedValue):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldDouble")
                GetField.ValueAsVariant = mExtendedValue
        Case (FID_SOLINE_ExtendedCost):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldDouble")
                GetField.ValueAsVariant = mExtendedCost
        Case (FID_SOLINE_LineDiscount):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldDouble")
                GetField.ValueAsVariant = mLineDiscount
        Case (FID_SOLINE_QtyPerUnit):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldLong")
                GetField.ValueAsVariant = mQtyPerUnit
        Case (FID_SOLINE_RaiseOrder):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldBool")
                GetField.ValueAsVariant = mRaiseOrder
        Case (FID_SOLINE_PurchaseOrderNo):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mPurchaseOrderNo
        Case (FID_SOLINE_LastDelNoteNo):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldLong")
                GetField.ValueAsVariant = mLastDelNoteNo
        Case (FID_SOLINE_QuantitySold):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldLong")
                GetField.ValueAsVariant = mQuantitySold
        Case (FID_SOLINE_Cancelled):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldBool")
                GetField.ValueAsVariant = mCancelled
        Case (FID_SOLINE_Complete):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldBool")
                GetField.ValueAsVariant = mComplete
        Case (FID_SOLINE_CancellationReason):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mCancellationReason
        Case (FID_SOLINE_DateCreated):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldDate")
                GetField.ValueAsVariant = mDateCreated
        Case (FID_SOLINE_DiscountReasonCode):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mDiscountReasonCode
        Case (FID_SOLINE_RefundAllowed):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mRefundAllowed
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

'<CACH>****************************************************************************************
'* Sub:  RecordItemOrdered()
'**********************************************************************************************
'* Description: Business rule called by Purchase Order.  Used to update a Customer's Sales
'*              Orders to flag the item as ordered.  This ensures that the item can only be
'*              ordered once by setting the Purchase Order Number.
'**********************************************************************************************
'* Parameters:
'*In/Out:sSalesOrderNo  String. - Unique Sales Order No used to load Line if not already loaded
'*In/Out:sLineNo   String. - Unique Sales Order No used to load Line Item if not already loaded
'*In/Out:sPurchaseOrderNo   String. - Purchase Order that line was ordered on.
'**********************************************************************************************
'* History:
'* 11/11/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub RecordItemOrdered(sSalesOrderNo As String, sLineNo As String, sPurchaseOrderNo As String)

Dim oRow As IRow
            
    If (mOrderNo <> sSalesOrderNo) And (mLineNo <> sLineNo) Then
        Call IBo_AddLoadFilter(CMP_EQUAL, FID_SOLINE_OrderNo, sSalesOrderNo)
        Call IBo_AddLoadFilter(CMP_EQUAL, FID_SOLINE_LineNo, sLineNo)
        
        Call IBo_AddLoadField(FID_SOLINE_PurchaseOrderNo)
        
        Call IBo_Load
    End If
    mPurchaseOrderNo = sPurchaseOrderNo

    'Save updated fields back to Database
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    Call oRow.Add(GetField(FID_SOLINE_OrderNo))
    Call oRow.Add(GetField(FID_SOLINE_LineNo))
    Call oRow.Add(GetField(FID_SOLINE_PurchaseOrderNo))

    Call m_oSession.Database.SavePartialBO(Me, oRow)
    Set oRow = Nothing

End Sub 'RecordItemOrdered

'<CACH>****************************************************************************************
'* Sub:  CancelPurchaseOrder()
'**********************************************************************************************
'* Description: Business rule called by Purchase Order, when a Purchase Order is cancelled with
'*              items from a Customer's Sales Orders.  This clears the Purchase Order number so
'*              that the item can be placed on a new order.
'**********************************************************************************************
'* Parameters:
'*In/Out:sSalesOrderNo  String. - Unique Sales Order No used to load Line if not already loaded
'*In/Out:sLineNo   String. - Unique Sales Order No used to load Line Item if not already loaded
'**********************************************************************************************
'* History:
'* 04/04/03    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub CancelPurchaseOrder(sSalesOrderNo As String, sLineNo As String)

Dim oRow As IRow
            
    If (mOrderNo <> sSalesOrderNo) And (mLineNo <> sLineNo) Then
        Call IBo_AddLoadFilter(CMP_EQUAL, FID_SOLINE_OrderNo, sSalesOrderNo)
        Call IBo_AddLoadFilter(CMP_EQUAL, FID_SOLINE_LineNo, sLineNo)
        
        Call IBo_AddLoadField(FID_SOLINE_PurchaseOrderNo)
        
        Call IBo_Load
    End If
    mPurchaseOrderNo = "000000"

    'Save updated fields back to Database
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    Call oRow.Add(GetField(FID_SOLINE_OrderNo))
    Call oRow.Add(GetField(FID_SOLINE_LineNo))
    Call oRow.Add(GetField(FID_SOLINE_PurchaseOrderNo))

    Call m_oSession.Database.SavePartialBO(Me, oRow)
    Set oRow = Nothing

End Sub 'CancelPurchaseOrder

Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As cOrderLine
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function


Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_SOLINE_END_OF_STATIC

End Function

