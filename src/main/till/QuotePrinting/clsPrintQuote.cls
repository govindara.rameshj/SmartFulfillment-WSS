VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPrintQuote"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Const MODULE_NAME As String = "clsPrintQuote"

Public Function PrintQuote(oQuoteBO As cQuoteHeader, goSession As Session, goRoot As OasysRoot, goDatabase As IBoDatabase, strDiscountName As String, dblDiscountAmount As Double, strCardNumber As String) As Boolean

    Load frmPrintLayout
    PrintQuote = frmPrintLayout.PrintQuote(oQuoteBO, goSession, goRoot, goDatabase, strDiscountName, dblDiscountAmount, strCardNumber)
    Unload frmPrintLayout
    PrintQuote = True

End Function

Public Function PrintOrder(oOrderBO As cCustOrderHeader, goSession As Session, goRoot As OasysRoot, goDatabase As IBoDatabase, strDiscountName As String, dblDiscountAmount As Double, strCardNumber As String) As Boolean

    Load frmPrintLayout
    PrintOrder = frmPrintLayout.PrintOrder(oOrderBO, goSession, goRoot, goDatabase, strDiscountName, dblDiscountAmount, strCardNumber)
    Unload frmPrintLayout
    PrintOrder = True

End Function


