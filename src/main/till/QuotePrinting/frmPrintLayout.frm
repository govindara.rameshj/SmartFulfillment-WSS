VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Begin VB.Form frmPrintLayout 
   Caption         =   "Form1"
   ClientHeight    =   10470
   ClientLeft      =   2010
   ClientTop       =   945
   ClientWidth     =   11400
   LinkTopic       =   "Form1"
   ScaleHeight     =   10470
   ScaleWidth      =   11400
   Begin FPSpreadADO.fpSpread sprdPrint 
      Height          =   10935
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   12315
      _Version        =   458752
      _ExtentX        =   21722
      _ExtentY        =   19288
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   16
      MaxRows         =   56
      SpreadDesigner  =   "frmPrintLayout.frx":0000
   End
End
Attribute VB_Name = "frmPrintLayout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const SINGLE_SPACE As String = " "
Const AMOUNT_FORMAT As String = "0.00 ;0.00-"

Const PRM_WHICH_DISCOUNT_SCHEME     As Long = 956
Const PRM_GENERAL_PATH  As Long = 910

Const COL_INFO As Long = 5
Const COL_SKU As Long = 3
Const COL_DESC As Long = 3
Const COL_QTY As Long = 4
Const COL_PRICE As Long = 5
Const COL_TOTAL As Long = 6
Const COL_VATCODE As Long = 7

Const COL_LOGO As Long = 12
Const COL_RIGHTSIDE As Long = 13

Const ROW_PAGE_NO As Long = 12
Const ROW_TOTAL As Long = 52
Const ROW_FOOTER As Long = 56

Const ITEMS_PER_PAGE As Long = 20

Dim mCurrChar As String

Private marrDiscountCodes(1 To 6)   As String
Private marrDiscountDescr(1 To 6)   As String
Private marrDiscountTotal(1 To 6)   As Currency

Private Enum enWhichDiscountScheme
    enVerifyDiscountCardScheme = 1
    enVerifyColleagueCardScheme
    enVerifyBoth
End Enum

Public Function PrintDocument(DocTypeQuote As Boolean, _
                              DocNo As String, _
                              TranDate As Date, _
                              TillID As String, _
                              TranNo As String, _
                              MerchandiseValue As Currency, _
                              goSession As Session, _
                              goRoot As OasysRoot, _
                              goDatabase As IBoDatabase, _
                              strDiscountName As String, _
                              dblDiscountAmount As Double, _
                              strCardNumber As String, _
                              DeliveryDate As Date) As Boolean

Dim strItemDesc  As String
Dim oSaleLineBO  As cPOSLine
Dim oItemBO      As cInventory

Dim oSalesBO     As cPOSHeader

Dim lngNoPages   As Long
Dim lngPageNo    As Long

Dim oReturnCust  As cReturnCust
Dim oStoreBO     As cEnterprise_Wickes.cStore
Dim oCashierBO   As cEnterprise_Wickes.cUser
Dim oOrderTaker  As cEnterprise_Wickes.cUser
Dim oOrderHdrBO  As cCustOrderHeader
Dim oOrderInfoBO As cCustOrderInfo
Dim oPayment     As cPOSPayment
Dim intRow       As Integer

Dim lngDiscCodes As Long

Dim blnPaid As Boolean

Dim oEvent  As cEventTranLine
Dim colDLEvent  As Collection
Dim strEventDesc    As String
Dim strEventAmt     As String
Dim lngNoEvents     As Long
Dim curNoItems      As Currency

Dim strDelDate      As String

Dim oFSO            As New FileSystemObject
Dim tsClause        As TextStream
Dim strClauseHeader As String
Dim strOrderClause  As String
Dim strQuoteClause  As String

Dim strReason       As String

Dim intDiscSchemeType   As Integer

Dim curEmpDiscAmt   As Currency
Dim strHeader As String
Dim strStoreId As String
Dim oPriceOverrideCode As cPriceOverrideCode


On Error GoTo Print_Quote_Error

    Set modCommonUS.goSession = goSession
    
    sprdPrint.PrintColHeaders = False
    sprdPrint.PrintRowHeaders = False
    sprdPrint.PrintBorder = False
    sprdPrint.PrintColor = True
    sprdPrint.PrintGrid = False
    
    Call SetDiscountTypes 'reset Events and set-up labels
    
    'If (DeliveryDate Is Nothing = False) Then
    strDelDate = Format(DeliveryDate, "dddd") & "  " & Format(DeliveryDate, "DD/MM/YY")
    sprdPrint.Col = 3
    sprdPrint.Row = 1
    
    strStoreId = IIf(Len(goSession.CurrentEnterprise.IEnterprise_StoreNumber) < 4, "8" + goSession.CurrentEnterprise.IEnterprise_StoreNumber, goSession.CurrentEnterprise.IEnterprise_StoreNumber)
    If (DocTypeQuote = True) Then
        sprdPrint.Text = "Customer Quote:" & DocNo
    Else
        sprdPrint.Text = "Customer Order:" & strStoreId & "-" & DocNo
    End If
    
    sprdPrint.FontSize = 16     'MO'C 9/7/2007 - Reduced font Size As it did not fit on some printers (Was 18)
    sprdPrint.FontBold = True
    sprdPrint.TypeVAlign = TypeVAlignCenter
    sprdPrint.RowHeight(1) = sprdPrint.MaxTextRowHeight(1)
        
    sprdPrint.AllowCellOverflow = True

    Set oStoreBO = goDatabase.CreateBusinessObject(CLASSID_STORE)
    Call oStoreBO.IBo_AddLoadFilter(CMP_EQUAL, FID_STORE_StoreNumber, goSession.CurrentEnterprise.IEnterprise_StoreNumber)
    Call oStoreBO.IBo_AddLoadField(FID_STORE_AddressLine1)
    Call oStoreBO.IBo_AddLoadField(FID_STORE_AddressLine2)
    Call oStoreBO.IBo_AddLoadField(FID_STORE_AddressLine3)
    Call oStoreBO.IBo_AddLoadField(FID_STORE_AddressLine4)
    Call oStoreBO.IBo_AddLoadField(FID_STORE_PhoneNo)
    Call oStoreBO.IBo_AddLoadField(FID_STORE_FaxNo)
    Call oStoreBO.IBo_LoadMatches
    
    sprdPrint.Col = COL_RIGHTSIDE
    sprdPrint.Row = 4
    sprdPrint.Text = oStoreBO.AddressLine1
    sprdPrint.Row = 5
    sprdPrint.Text = oStoreBO.AddressLine2
    sprdPrint.Row = 6
    sprdPrint.Text = oStoreBO.AddressLine3
    sprdPrint.Row = 7
    sprdPrint.Text = oStoreBO.AddressLine4
    sprdPrint.Row = 8
    sprdPrint.Text = "Telephone : " & oStoreBO.PhoneNo
    sprdPrint.Row = 9
    sprdPrint.Text = "Fax Number : " & oStoreBO.FaxNo
    
    Set oItemBO = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
    Call oItemBO.AddLoadField(FID_INVENTORY_Description)
    
    Set oSalesBO = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    Call oSalesBO.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, TranDate)
    Call oSalesBO.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, TranNo)
    Call oSalesBO.IBo_AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, TillID)
    Call oSalesBO.IBo_LoadMatches
    
    If (oSalesBO.Lines.Count > 0) Then
    End If
    
    '** For page count, calculate the no of lines that will be printed **
    curNoItems = 0
    lngPageNo = 0
    lngNoPages = 0
    intRow = ROW_PAGE_NO + 1
    
    'If Discount Card/Colleague then add 2 Items to Page Count Calc
    If strDiscountName <> "" And dblDiscountAmount > 0 Then intRow = intRow + 2
        
    'WIX1381 - If a Quote add an extra two Rows for Take Now And To Be Delivered Headers
    If DocTypeQuote = False Then intRow = intRow + 2
    For Each oSaleLineBO In oSalesBO.Lines
        
        'For each new page, display page number
        If (intRow > (ROW_TOTAL - 5)) Or (lngPageNo = 0) Then
            lngPageNo = lngPageNo + 1
            lngNoPages = lngNoPages + 1
            intRow = ROW_PAGE_NO
        End If
        
        If (oSaleLineBO.SaleType <> "W") And (oSaleLineBO.LineReversed = False) Then
        
            'Add 2 rows for a standard line item
            intRow = intRow + 2
            
            'Add 1 rows for a Temp Price Event
            If (oSaleLineBO.TempPriceMarginAmount > 0) Then intRow = intRow + 1
                        
            'Add 1 rows for a Price Override
            If (oSaleLineBO.PriceOverrideAmount > 0) Then intRow = intRow + 1
        
            'Add 1 rows for a PRF Charge
            If (oSaleLineBO.WEEELineCharge > 0) Then intRow = intRow + 1

            
            'Get DlEvnts and build discount section of receipt
            Set oEvent = goDatabase.CreateBusinessObject(CLASSID_POSEVENTLINE)
            oEvent.AddLoadField (FID_POSEVENTLINE_EventType)
            oEvent.AddLoadField (FID_POSEVENTLINE_DiscountAmount)
            
            oEvent.IBo_ClearLoadFilter
            oEvent.AddLoadFilter CMP_EQUAL, FID_POSEVENTLINE_TranDate, oSalesBO.TranDate
            oEvent.AddLoadFilter CMP_EQUAL, FID_POSEVENTLINE_TillID, oSalesBO.TillID
            oEvent.AddLoadFilter CMP_EQUAL, FID_POSEVENTLINE_TransactionNo, oSalesBO.TransactionNo
            oEvent.AddLoadFilter CMP_EQUAL, FID_POSEVENTLINE_TransactionLineNo, oSaleLineBO.SequenceNo
            
            Set colDLEvent = oEvent.LoadMatches
        
            If ((colDLEvent Is Nothing) = False) Then
                For Each oEvent In colDLEvent
                    If (LenB(Trim(oEvent.EventType)) > 0) Then
                        For lngDiscCodes = 1 To UBound(marrDiscountCodes)
                            If (marrDiscountCodes(lngDiscCodes) = UCase(Trim(oEvent.EventType))) Then
                                marrDiscountTotal(lngDiscCodes) = marrDiscountTotal(lngDiscCodes) + oEvent.DiscountAmount
                                Exit For
                            End If
                        Next lngDiscCodes
                    End If 'event type is valid
                Next oEvent
            End If 'any events to add into total, for current line

        End If 'WEEE SKU
    Next 'Sale line to count
            
    Set oCashierBO = goDatabase.CreateBusinessObject(CLASSID_USER)
    Call oCashierBO.IBo_AddLoadFilter(CMP_EQUAL, FID_USER_EmployeeID, oSalesBO.CashierID)
    Call oCashierBO.IBo_AddLoadField(FID_USER_TillReceiptName)
    Call oCashierBO.IBo_LoadMatches
            
    Set oReturnCust = goDatabase.CreateBusinessObject(CLASSID_RETURNCUST)
        
    Call oReturnCust.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionDate, TranDate)
    Call oReturnCust.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_TransactionNumber, TranNo)
    Call oReturnCust.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_PCTillID, TillID)
    Call oReturnCust.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNCUST_LineNumber, 0)
    Call oReturnCust.IBo_LoadMatches
            
        
    sprdPrint.PrintFooter = "/cPage " & CStr(lngPageNo) & " of " & CStr(lngNoPages) '"/cPage /p of /pc"
    sprdPrint.Col = COL_INFO
    sprdPrint.Row = 4
    Call sprdPrint.AddCellSpan(COL_INFO, sprdPrint.Row, 2, 1)
    sprdPrint.Text = oReturnCust.CustomerName
    sprdPrint.Row = 5
    Call sprdPrint.AddCellSpan(COL_INFO, sprdPrint.Row, 2, 1)
    sprdPrint.Text = oReturnCust.HouseName & " " & oReturnCust.AddressLine1
    sprdPrint.Row = 6
    Call sprdPrint.AddCellSpan(COL_INFO, sprdPrint.Row, 2, 1)
    sprdPrint.Text = oReturnCust.AddressLine2
    sprdPrint.Row = 7
    Call sprdPrint.AddCellSpan(COL_INFO, sprdPrint.Row, 2, 1)
    sprdPrint.Text = oReturnCust.AddressLine3
    sprdPrint.Row = 8
    Call sprdPrint.AddCellSpan(COL_INFO, sprdPrint.Row, 2, 1)
    sprdPrint.Text = oReturnCust.PostCode
    sprdPrint.Row = 9
    Call sprdPrint.AddCellSpan(COL_INFO, sprdPrint.Row, 2, 1)
    sprdPrint.Text = DisplayDate(TranDate, False) & "  " & TillID & "  " & TranNo
                
    sprdPrint.Row = 10
    If (DocTypeQuote = False) Then
        Set oOrderHdrBO = goDatabase.CreateBusinessObject(CLASSID_CUSTORDERHEADER)
        Call oOrderHdrBO.IBo_AddLoadFilter(CMP_EQUAL, FID_CUSTORDERHEADER_OrderNumber, DocNo)
        Call oOrderHdrBO.IBo_LoadMatches
        
        sprdPrint.Col = COL_DESC
        sprdPrint.FontBold = True
        If (oOrderHdrBO.IsForDelivery = True) Then
            sprdPrint.Text = "Delivery Date"
        Else
            sprdPrint.Text = "Collection Date"
        End If
        sprdPrint.Col = COL_DESC + 1
        sprdPrint.FontBold = True
        sprdPrint.Text = ":"
        sprdPrint.Col = COL_INFO
        Call sprdPrint.AddCellSpan(COL_INFO, sprdPrint.Row, 2, 1)
        sprdPrint.Text = strDelDate
    End If
    intRow = 11
    
    sprdPrint.Row = intRow
    
    If (DocTypeQuote = True) Then
        
        Call sprdPrint.AddCellSpan(COL_INFO, sprdPrint.Row, 2, 1)
        'MO'C added EmployeeID to cashier name on 19/07/2007 - WIX1240
        sprdPrint.Text = oCashierBO.EmployeeID & " - " & oCashierBO.TillReceiptName
        intRow = intRow + 2
        
    Else
        
        Set oOrderInfoBO = goDatabase.CreateBusinessObject(CLASSID_CUSTORDERINFO)
        Call oOrderInfoBO.IBo_AddLoadFilter(CMP_EQUAL, FID_CUSTORDERINFO_OrderNumber, DocNo)
        Call oOrderInfoBO.IBo_LoadMatches
        
        Set oOrderTaker = goDatabase.CreateBusinessObject(CLASSID_USER)
        Call oOrderTaker.IBo_AddLoadFilter(CMP_EQUAL, FID_USER_EmployeeID, oOrderInfoBO.UserID)
        Call oOrderTaker.IBo_AddLoadField(FID_USER_TillReceiptName)
        Call oOrderTaker.IBo_LoadMatches
        
        Set oPayment = goDatabase.CreateBusinessObject(CLASSID_POSPAYMENT)
        Call oPayment.IBo_AddLoadFilter(CMP_EQUAL, FID_POSPAYMENT_TransactionDate, TranDate)
        Call oPayment.IBo_AddLoadFilter(CMP_EQUAL, FID_POSPAYMENT_TransactionNumber, TranNo)
        Call oPayment.IBo_AddLoadFilter(CMP_EQUAL, FID_POSPAYMENT_TillID, TillID)
        If (oPayment.IBo_LoadMatches.Count > 0) Then blnPaid = True
                    
        sprdPrint.Col = COL_DESC
        sprdPrint.FontBold = True
        sprdPrint.Text = "Order Taker"
        sprdPrint.Col = COL_DESC + 1
        sprdPrint.FontBold = True
        sprdPrint.Text = ":"
        sprdPrint.Col = COL_INFO
        Call sprdPrint.AddCellSpan(COL_INFO, sprdPrint.Row, 2, 1)
        sprdPrint.Text = oOrderTaker.EmployeeID & " - " & oOrderTaker.TillReceiptName
        intRow = intRow + 1
        
        sprdPrint.Row = intRow
        sprdPrint.Col = COL_DESC
        sprdPrint.FontBold = True
        sprdPrint.Text = "Cashier"
        sprdPrint.Col = COL_DESC + 1
        sprdPrint.FontBold = True
        sprdPrint.Text = ":"
        sprdPrint.Col = COL_INFO
        Call sprdPrint.AddCellSpan(COL_INFO, sprdPrint.Row, 2, 1)
        sprdPrint.Text = oCashierBO.EmployeeID & " - " & oCashierBO.TillReceiptName
        intRow = intRow + 1
        
        Set oOrderTaker = Nothing
    End If
    
    'Fill in Right Side Terms and Conditions
    sprdPrint.Row = 13
    sprdPrint.Col = COL_RIGHTSIDE
    If (DocTypeQuote = True) Then
        'Get Quote Clause and Header if file exists
        On Error Resume Next
        If (oFSO.FileExists(goSession.GetParameter(PRM_GENERAL_PATH) & "\QuoteClause.txt") = True) Then
            Set tsClause = oFSO.OpenTextFile(goSession.GetParameter(PRM_GENERAL_PATH) & "\QuoteClause.txt", ForReading, False)
            strQuoteClause = tsClause.ReadAll
            tsClause.Close
        End If
        'Set default Header
        strClauseHeader = "THIS QUOTE IS VALID FOR TODAY ONLY"
        
        'Check if alternative Clause exists so extract and display
        If (InStr(strQuoteClause, vbCrLf) > 0) Then
            strClauseHeader = Left$(strQuoteClause, InStr(strQuoteClause, vbCrLf) - 1)
            strQuoteClause = Mid$(strQuoteClause, InStr(strQuoteClause, vbCrLf) + 2)
        End If
        sprdPrint.Text = strClauseHeader
        sprdPrint.Row = 15
        sprdPrint.Col = COL_RIGHTSIDE
        sprdPrint.Text = strQuoteClause
        
    Else
    
        On Error Resume Next
        If (oFSO.FileExists(goSession.GetParameter(PRM_GENERAL_PATH) & "\OrderClause.txt") = True) Then
            Set tsClause = oFSO.OpenTextFile(goSession.GetParameter(PRM_GENERAL_PATH) & "\OrderClause.txt", ForReading, False)
            strOrderClause = tsClause.ReadAll
            tsClause.Close
            'Set default Header
            strClauseHeader = "Wickes Delivery/Collection Service"
            
            'Check if alternative Clause exists so extract and display
            If (InStr(strOrderClause, vbCrLf) > 0) Then
                strClauseHeader = Left$(strOrderClause, InStr(strOrderClause, vbCrLf) - 1)
                strOrderClause = Mid$(strOrderClause, InStr(strOrderClause, vbCrLf) + 2)
            End If
            
            sprdPrint.Row = 15
            sprdPrint.Col = COL_RIGHTSIDE
            sprdPrint.Text = strOrderClause
        End If
    End If 'quote so delete Order Terms and Conditions
    
    
    'Retrieve and display any Footer comments (used for Franchise agreement
    If (oFSO.FileExists(goSession.GetParameter(PRM_GENERAL_PATH) & "\OrderFooter.txt") = True) Then
        Set tsClause = oFSO.OpenTextFile(goSession.GetParameter(PRM_GENERAL_PATH) & "\OrderFooter.txt", ForReading, False)
        strQuoteClause = tsClause.ReadAll
        tsClause.Close
        sprdPrint.Row = ROW_FOOTER
        sprdPrint.Col = 2
        sprdPrint.Text = strQuoteClause
        sprdPrint.TypeEditMultiLine = True
        sprdPrint.TypeHAlign = TypeHAlignCenter
        sprdPrint.RowHeight(ROW_FOOTER) = sprdPrint.MaxTextRowHeight(ROW_FOOTER)
    End If
        
    lngPageNo = 0
    
    'Wix1381 - If a Quote we want to display the Items Taken Now First under a Header,
    'Then the to be delivered
    If DocTypeQuote = False Then
        strHeader = "Items Taken Now"

        Call PrintOrderLines(goDatabase, oSalesBO, oEvent, colDLEvent, oItemBO, intRow, lngPageNo, lngNoPages, curEmpDiscAmt, oOrderHdrBO, strHeader, DocTypeQuote, True)

        If (oOrderHdrBO.IsForDelivery = True) Then
            strHeader = "To Be Delivered"
        Else
            strHeader = "To Be Collected"
        End If
       
        Call PrintOrderLines(goDatabase, oSalesBO, oEvent, colDLEvent, oItemBO, intRow, lngPageNo, lngNoPages, curEmpDiscAmt, oOrderHdrBO, strHeader, DocTypeQuote, False)
    
    Else
        'Its a quote so leave as before
        Call PrintOrderLines(goDatabase, oSalesBO, oEvent, colDLEvent, oItemBO, intRow, lngPageNo, lngNoPages, curEmpDiscAmt, oOrderHdrBO, "", DocTypeQuote, False)
    End If
    
    lngNoEvents = PrintDiscountLines(strEventDesc, strEventAmt)
    If (lngNoEvents > 0) Then
        sprdPrint.Row = sprdPrint.Row + 1
        sprdPrint.Col = COL_SKU
        Call sprdPrint.AddCellSpan(COL_SKU, sprdPrint.Row, COL_PRICE - COL_SKU, lngNoEvents)
        sprdPrint.TypeEditMultiLine = True
        sprdPrint.Text = strEventDesc
        Call sprdPrint.AddCellSpan(COL_TOTAL, sprdPrint.Row, 1, lngNoEvents)
        sprdPrint.Col = COL_TOTAL
        sprdPrint.Text = strEventAmt
        sprdPrint.TypeEditMultiLine = True
        sprdPrint.TypeHAlign = TypeHAlignRight
    End If
    
'.TempPriceMarginAmount,
    'MO'C Added 6/7/2007 Wix1311 Referral 17 & 18
    If strDiscountName <> "" And dblDiscountAmount > 0 Then
        intDiscSchemeType = goSession.GetParameter(PRM_WHICH_DISCOUNT_SCHEME)
        If intDiscSchemeType = enVerifyColleagueCardScheme Then
            If (oSalesBO.EmployeeDiscountOnly = True) Then
                sprdPrint.Row = sprdPrint.Row + 2
                sprdPrint.Col = COL_PRICE
                sprdPrint.Text = "Sub-Total"
                sprdPrint.Col = COL_TOTAL
                sprdPrint.TypeHAlign = TypeHAlignRight
                sprdPrint.Text = Format(oSalesBO.TotalSaleAmount + curEmpDiscAmt, "0.00")
                sprdPrint.Row = sprdPrint.Row + 1
                sprdPrint.Col = COL_DESC
                sprdPrint.Text = oSalesBO.StoreNumber & " " & strDiscountName
                sprdPrint.Col = COL_TOTAL
                sprdPrint.TypeHAlign = TypeHAlignRight
                sprdPrint.Text = Format(curEmpDiscAmt * -1, "0.00")
                
                sprdPrint.Row = sprdPrint.Row + 1
                sprdPrint.Col = COL_DESC
                sprdPrint.Text = "Card No: " & strCardNumber
                
                sprdPrint.Row = sprdPrint.Row + 1
                sprdPrint.Text = CStr(dblDiscountAmount) & "% discount has been applied to prices above."
            End If
        End If
        If intDiscSchemeType = enVerifyDiscountCardScheme Then
            sprdPrint.Row = sprdPrint.Row + 2
            sprdPrint.Col = COL_PRICE
            sprdPrint.Text = "Sub-Total."
            sprdPrint.Col = COL_TOTAL
            sprdPrint.TypeHAlign = TypeHAlignRight
            sprdPrint.Text = Format(oSalesBO.TotalSaleAmount + curEmpDiscAmt, "0.00")
            sprdPrint.Row = sprdPrint.Row + 1
            sprdPrint.Col = COL_DESC
            sprdPrint.Text = oSalesBO.StoreNumber & " " & strDiscountName
            sprdPrint.Col = COL_TOTAL
            sprdPrint.TypeHAlign = TypeHAlignRight
            sprdPrint.Text = Format(curEmpDiscAmt * -1, "0.00")
            
            sprdPrint.Row = sprdPrint.Row + 1
            sprdPrint.Col = COL_DESC
            sprdPrint.Text = "Card No: " & strCardNumber
            
            sprdPrint.Row = sprdPrint.Row + 1
            sprdPrint.Text = CStr(dblDiscountAmount) & "% discount has been applied to prices above."
        End If
    End If
     
    sprdPrint.Row = ROW_TOTAL
    sprdPrint.Col = 3
    If (DocTypeQuote = True) Then
        sprdPrint.Text = "QUOTE TOTAL"
    Else
        sprdPrint.Text = "ORDER TOTAL"
    End If
    
    sprdPrint.FontBold = True
    sprdPrint.FontSize = 14
    
    If (blnPaid = True) Then
        sprdPrint.Row = ROW_TOTAL + 2
        sprdPrint.Col = 3
        sprdPrint.Text = "PAID on " & Format(oPayment.TransactionDate, "DD/MM/YY") & "  " & oPayment.TillID & "-" & oPayment.TransactionNumber
        sprdPrint.TypeHAlign = TypeHAlignCenter
        sprdPrint.FontBold = True
        sprdPrint.FontSize = 10
        sprdPrint.RowHeight(ROW_TOTAL + 2) = sprdPrint.MaxTextRowHeight(ROW_TOTAL + 2)
        sprdPrint.Row = ROW_TOTAL
    End If

    sprdPrint.Col = 6
    sprdPrint.CellType = CellTypeStaticText
    sprdPrint.TypeHAlign = TypeHAlignRight
    sprdPrint.Text = mCurrChar & Format(MerchandiseValue, AMOUNT_FORMAT)
    sprdPrint.FontBold = True
    sprdPrint.FontSize = 14
    sprdPrint.Col = 1
    sprdPrint.Row = sprdPrint.MaxRows
    sprdPrint.Text = "."
    
    Set oOrderHdrBO = Nothing
    
    Call sprdPrint.PrintSheet
    PrintDocument = True
    
    Exit Function
    
Print_Quote_Error:
Resume Next


End Function

Public Function PrintQuote(oQuoteBO As cQuoteHeader, goSession As Session, goRoot As OasysRoot, goDatabase As IBoDatabase, strDiscountName As String, dblDiscountAmount As Double, strCardNumber As String) As Boolean
    
    mCurrChar = goSession.GetParameter(PRM_COUNTRY_CODE)
    Select Case (mCurrChar)
        Case ("IE"): mCurrChar = Chr(128)
        Case Else
            mCurrChar = ChrW(163)
    End Select
    Call PrintDocument(True, oQuoteBO.QuoteNumber, oQuoteBO.TranDate, oQuoteBO.TillID, oQuoteBO.TransactionNumber, oQuoteBO.MerchandiseValue, goSession, goRoot, goDatabase, strDiscountName, dblDiscountAmount, strCardNumber, Now)

End Function


Public Function PrintOrder(oOrderBO As cCustOrderHeader, goSession As Session, goRoot As OasysRoot, goDatabase As IBoDatabase, strDiscountName As String, dblDiscountAmount As Double, strCardNumber As String) As Boolean

    mCurrChar = goSession.GetParameter(PRM_COUNTRY_CODE)
    Select Case (mCurrChar)
        Case ("IE"): mCurrChar = Chr(128)
        Case Else
            mCurrChar = ChrW(163)
    End Select
    Call PrintDocument(False, oOrderBO.OrderNumber, oOrderBO.TranDate, oOrderBO.TillID, oOrderBO.TransactionNumber, CCur(oOrderBO.MerchandiseValue), goSession, goRoot, goDatabase, strDiscountName, dblDiscountAmount, strCardNumber, oOrderBO.DateRequired)

End Function


Private Sub SetDiscountTypes()

Dim lngTotal As Long
    
    For lngTotal = 1 To UBound(marrDiscountTotal)
        marrDiscountTotal(lngTotal) = 0
    Next lngTotal
    
    marrDiscountCodes(1) = "DG"
    marrDiscountCodes(2) = "HS"
    marrDiscountCodes(3) = "MS"
    marrDiscountCodes(4) = "QS"
    marrDiscountCodes(5) = "TM"
    marrDiscountCodes(6) = "TS"
    
    marrDiscountDescr(1) = "* Project Saving    "
    marrDiscountDescr(2) = "* Spend Level Saving"
    marrDiscountDescr(3) = "* Multibuy Saving   "
    marrDiscountDescr(4) = "* Bulk Saving       "
    marrDiscountDescr(5) = "* Temporary Markdown"
    marrDiscountDescr(6) = "* Promotional Saving"

End Sub

Private Function PrintDiscountLines(ByRef strDiscDesc As String, ByRef strDiscAmount As String) As Long

Dim lngDiscCode As Long
Dim lngNoLines  As Long
Dim strDesc     As String
Dim strAmounts  As String

    lngNoLines = 0
    
    For lngDiscCode = 1 To UBound(marrDiscountCodes)
        If (marrDiscountTotal(lngDiscCode) > 0) Then
            lngNoLines = lngNoLines + 1
            strDesc = strDesc & marrDiscountDescr(lngDiscCode) & vbNewLine
            strAmounts = strAmounts & mCurrChar & Format((marrDiscountTotal(lngDiscCode) * -1), AMOUNT_FORMAT) & vbNewLine
        End If
    Next lngDiscCode
    
    strDiscDesc = strDesc
    strDiscAmount = strAmounts
    PrintDiscountLines = lngNoLines

End Function


Private Function PrintOrderLines(goDatabase As IBoDatabase, ByRef oSalesBO As cPOSHeader, ByRef oEvent As cEventTranLine, ByRef colDLEvent As Collection, ByRef oItemBO As cInventory, ByRef intRow As Integer, ByRef lngPageNo As Long, ByRef lngNoPages As Long, ByRef curEmpDiscAmt As Currency, ByRef oOrderHdrBO As cCustOrderHeader, strHeader As String, ByVal DocTypeQuote As Boolean, ByVal blnItemsTakenNow As Boolean)

Dim oSaleLineBO As cPOSLine
Dim blnPrintLine As Boolean
Dim lngDiscCodes As Long
Dim strItemDesc As String
Dim strReason As String
Dim oCurlinBO As cCustOrderLine
Dim blnHeaderPrinted As Boolean
Dim intQty As Integer
Dim intLine As Integer
Dim intLoop As Integer
Dim intOffset As Integer
Dim oPriceOverrideCode As cPriceOverrideCode
Dim colPriceOverride As Collection
    
    intOffset = 0
    For Each oSaleLineBO In oSalesBO.Lines
        'For each new page, display page number
        If (sprdPrint.Row > (ROW_TOTAL - 5)) Or (lngPageNo = 0) Then
            If (lngPageNo > 0) Then Call sprdPrint.PrintSheet 'force print before updating page numbers
            sprdPrint.Row = ROW_PAGE_NO
            sprdPrint.Col = COL_INFO + 1
            lngPageNo = lngPageNo + 1
            sprdPrint.PrintFooter = "/cPage " & CStr(lngPageNo) & " of " & CStr(lngNoPages)
            sprdPrint.TypeHAlign = TypeHAlignRight
            'Clear down existing SKU's for next print page
            For intRow = ROW_PAGE_NO + 1 To ROW_TOTAL - 2 Step 1
                sprdPrint.Row = intRow
                sprdPrint.Col = COL_SKU
                Call sprdPrint.RemoveCellSpan(COL_SKU, intRow)
                sprdPrint.Text = ""
                sprdPrint.Col = COL_QTY
                sprdPrint.Text = ""
                sprdPrint.Col = COL_PRICE
                sprdPrint.Text = ""
                sprdPrint.Col = COL_TOTAL
                sprdPrint.Text = ""
                sprdPrint.Col = COL_VATCODE
                sprdPrint.Text = ""
            Next intRow
            sprdPrint.Row = ROW_PAGE_NO
        End If
        
        If DocTypeQuote = False Then
            
            If blnHeaderPrinted = False Then
                sprdPrint.Row = sprdPrint.Row + 1
                sprdPrint.Col = COL_DESC
                sprdPrint.FontBold = True
                sprdPrint.Text = strHeader
                blnHeaderPrinted = True
            End If
            
            intQty = 0
            intLine = 0
            For Each oCurlinBO In oOrderHdrBO.Lines(True)
                If (intLoop) = intLine Then  'Make Sure were testing like for like
                    If oSaleLineBO.LineReversed = False Then
                        If oSaleLineBO.PartCode = oCurlinBO.PartCode Then
                             If blnItemsTakenNow = False Then  'Looking for delivery sku's
                                 If (oCurlinBO.UnitsOrdered - oCurlinBO.UnitsTaken) = 0 Then
                                     blnPrintLine = False
                                 Else
                                     blnPrintLine = True
                                     intQty = (oCurlinBO.UnitsOrdered - oCurlinBO.UnitsTaken)
                                 End If
                             Else
                                 If oCurlinBO.UnitsTaken > 0 Then 'Looking for items taken
                                     blnPrintLine = True
                                     intQty = oCurlinBO.UnitsTaken
                                 Else
                                     blnPrintLine = False
                                 End If
                             End If
                             Exit For
                        End If
                    End If
                End If
                intLine = intLine + 1
            Next
        Else
            blnPrintLine = True
            intQty = oSaleLineBO.QuantitySold
        End If
        
        If blnPrintLine Then
            If (oSaleLineBO.SaleType <> "W") And (oSaleLineBO.LineReversed = False) Then
            
                Call oItemBO.IBo_ClearLoadFilter
                Call oItemBO.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, oSaleLineBO.PartCode)
                Call oItemBO.LoadMatches
                sprdPrint.Row = sprdPrint.Row + 1
                sprdPrint.Col = COL_DESC
                sprdPrint.FontBold = False
                Call sprdPrint.AddCellSpan(COL_DESC, sprdPrint.Row, 5, 1)
                strItemDesc = oItemBO.Description
                sprdPrint.Text = strItemDesc
            
                sprdPrint.Row = sprdPrint.Row + 1
                sprdPrint.Col = COL_SKU
                sprdPrint.FontBold = False
                sprdPrint.Text = "Sku " & oSaleLineBO.PartCode
            
                sprdPrint.Col = COL_QTY
                sprdPrint.Text = intQty 'oSaleLineBO.QuantitySold
                sprdPrint.TypeHAlign = TypeHAlignRight
            
                sprdPrint.Col = COL_PRICE
                sprdPrint.Text = " @ " & mCurrChar & Format((oSaleLineBO.ExtendedValue / oSaleLineBO.QuantitySold) + oSaleLineBO.EmpSalePrimaryMarginAmount + oSaleLineBO.WEEELineCharge + oSaleLineBO.TempPriceMarginAmount + oSaleLineBO.PriceOverrideAmount, AMOUNT_FORMAT)
                sprdPrint.Col = COL_TOTAL
                sprdPrint.CellType = CellTypeNumber
                sprdPrint.Text = ((oSaleLineBO.ExtendedValue / oSaleLineBO.QuantitySold) * intQty) + ((oSaleLineBO.WEEELineCharge + oSaleLineBO.EmpSalePrimaryMarginAmount + oSaleLineBO.TempPriceMarginAmount + oSaleLineBO.PriceOverrideAmount) * intQty)
                sprdPrint.Col = COL_VATCODE
                sprdPrint.Text = oSaleLineBO.VATCode
              
                'For some strange reason could not read Price override here
                If (oSaleLineBO.PriceOverrideAmount <> 0) Then
                    If (Val(oSaleLineBO.PriceOverrideReason) <> 0) Then
                        
                        Set oPriceOverrideCode = goDatabase.CreateBusinessObject(CLASSID_PRICEOVERRIDE)
                        oPriceOverrideCode.AddLoadFilter CMP_EQUAL, FID_PRICEOVERRIDE_Code, Format(oSaleLineBO.PriceOverrideReason, "00")
                        Call oPriceOverrideCode.AddLoadField(FID_PRICEOVERRIDE_Description)
                        Set colPriceOverride = oPriceOverrideCode.LoadMatches
                        
                        If (colPriceOverride.Count > 0) Then
                            Set oPriceOverrideCode = colPriceOverride(1)
                            strReason = oPriceOverrideCode.Description
                        Else
                            strReason = oSaleLineBO.PriceOverrideReason
                            If Val(strReason) > 50 Then
                                strReason = "Mark Down"
                            Else
                                strReason = "Not Found"
                            End If
                        End If
                    End If
                End If

                'Check for TempPrice
                If (oSaleLineBO.TempPriceMarginAmount > 0) Then
                    sprdPrint.Row = sprdPrint.Row + 1
                    sprdPrint.Col = COL_SKU
                    sprdPrint.Text = "* Promotional Saving"
                    sprdPrint.Col = COL_TOTAL
                    sprdPrint.TypeHAlign = TypeHAlignRight
                    sprdPrint.Text = Format(oSaleLineBO.TempPriceMarginAmount * -1 * oSaleLineBO.QuantitySold, AMOUNT_FORMAT)
                End If
            
                'Check for TempPrice
                If (oSaleLineBO.PriceOverrideAmount > 0) Then
                    sprdPrint.Row = sprdPrint.Row + 1
                    sprdPrint.Col = COL_SKU
                    sprdPrint.Text = UCase(strReason)
                    sprdPrint.Col = COL_TOTAL
                    sprdPrint.TypeHAlign = TypeHAlignRight
                    sprdPrint.Text = Format(oSaleLineBO.PriceOverrideAmount * -1 * oSaleLineBO.QuantitySold, AMOUNT_FORMAT)
                End If
            
                If (oSaleLineBO.WEEELineCharge > 0) Then
                    sprdPrint.Row = sprdPrint.Row + 1
                    sprdPrint.Col = COL_SKU
                    With oSaleLineBO
                        Call sprdPrint.AddCellSpan(sprdPrint.Col, sprdPrint.Row, COL_VATCODE - COL_SKU, 1)
                        sprdPrint.Text = "(Includes " & mCurrChar & Format(.QuantitySold * .WEEELineCharge, "0.00") & " [" & CInt(.QuantitySold) & " x " & mCurrChar & Format(.WEEELineCharge, "0.00") & "] Producer Recycling Fund)"
                    End With
                End If
                
                If oSaleLineBO.SaleType <> "V" Then
                    curEmpDiscAmt = curEmpDiscAmt + (oSaleLineBO.EmpSalePrimaryMarginAmount * oSaleLineBO.QuantitySold)
                End If
            Else
                intOffset = intOffset + 1
            End If 'WEEE SKU
        End If
        If oSaleLineBO.LineReversed = False Then
            intLoop = intLoop + 1
        End If
    Next 'Sale line to display

End Function
