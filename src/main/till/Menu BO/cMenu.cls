VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cMenuItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module: cMenuItem
'* Date  : 08/08/02
'* Author: mauricem
'*$Archive: /Projects/OasysV2/VB/Menu BO/cMenu.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of a single menu item
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 7/05/03 10:00 $
'* $Revision: 6 $
'* Versions:
'* 08/08/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "cMenuItem"

Implements IBo
Implements ISysBo

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'Variable to hold local copy of object properties
Private mvarMenuText    As String
Private mvarMenuID      As Long
Private mvarExecPath    As String
Private mvarASPPath     As String
Private mvarParameters  As String
Private mvarParentID    As Long
Private mvarSecurity    As Long
Private mvarMenuType    As Long
Private mvarShortcutKey As String
Private mvarIconID      As Long
Private mvarWaitEXE     As Boolean
Private mvarAddPath     As Boolean
Private mvarSendSession As Boolean
Private mvarGroupID     As Long
Private mcolChildren    As Collection
Private mcolMenuOptions As Collection
Private mcolSubMenus    As Collection
Private mcolReports     As Collection
Private mcolMaintenance As Collection

Private colErrors   As New Collection

Private Enum enMenuType
    enmtSubMenu = 1
    enmtMenuOption = 2
    enmtReportOption = 3
    enmtMaintOption = 4
End Enum
Property Get MenuID() As Long

    MenuID = mvarMenuID

End Property

Property Let MenuID(ByVal lData As Long)

    mvarMenuID = lData

End Property

Property Get GroupID() As Long

    GroupID = mvarGroupID

End Property


Property Get MenuText() As String

    MenuText = mvarMenuText

End Property

Property Let MenuText(ByVal sData As String)

    mvarMenuText = sData

End Property


Property Get ExecPath() As String

    ExecPath = mvarExecPath

End Property

Property Let ExecPath(ByVal sData As String)

    mvarExecPath = sData

End Property
Property Get Parameters() As String

    Parameters = mvarParameters

End Property

Property Let Parameters(ByVal sData As String)

    mvarParameters = sData

End Property
Property Get ASPPath() As String

    ASPPath = mvarASPPath

End Property

Property Let ASPPath(ByVal sData As String)

    mvarASPPath = sData

End Property
Property Get ParentID() As Long

    ParentID = mvarParentID

End Property

Friend Property Let ParentID(Value As Long)

    mvarParentID = Value

End Property

Property Get Security() As Long

    Security = mvarSecurity

End Property

Property Let Security(Value As Long)

    mvarSecurity = Value

End Property

Property Get ShortcutKey() As String

    ShortcutKey = mvarShortcutKey

End Property

Property Let ShortcutKey(Value As String)

    mvarShortcutKey = Value

End Property

Property Get IconID() As Long

    If mvarIconID = 0 Then mvarIconID = 1
    IconID = mvarIconID

End Property

Property Let IconID(Value As Long)

    mvarIconID = Value

End Property

Property Get WaitEXE() As Boolean

    WaitEXE = mvarWaitEXE

End Property

Property Let WaitEXE(Value As Boolean)

    mvarWaitEXE = Value

End Property

Property Get AddPath() As Boolean

    AddPath = mvarAddPath

End Property

Property Let AddPath(Value As Boolean)

    mvarAddPath = Value

End Property


Property Get SendSession() As Boolean

    SendSession = mvarSendSession

End Property

Property Let SendSession(Value As Boolean)

    mvarSendSession = Value

End Property



Property Get MenuType() As Long

    MenuType = mvarMenuType

End Property

Friend Property Let MenuType(ByVal lData As Long)

    mvarMenuType = lData

End Property
Property Get Children() As Collection

    Set Children = mcolChildren

End Property

Property Get MenuOptions() As Collection

    Set MenuOptions = mcolMenuOptions

End Property

Property Get SubMenus() As Collection

    Set SubMenus = mcolSubMenus

End Property

Property Get Reports() As Collection

    Set Reports = mcolReports

End Property

Property Get Maintenance() As Collection

    Set Maintenance = mcolMaintenance

End Property

Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cMenuItem

End Function
Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function
Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function
Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_MENU, FID_MENU_END_OF_STATIC, m_oSession)

End Function



'<CACH>****************************************************************************************
'* Function:  Object GetField()
'**********************************************************************************************
'* Description: Returns value of property as a Field object - used for retrieving data from DB
'**********************************************************************************************
'* Parameters:
'*In    :lFieldID   Long. - predefined ID of field
'**********************************************************************************************
'* Returns:  Object
'**********************************************************************************************
'* History:
'* 08/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function GetField(ByVal lFieldID As Long) As IField

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".GetField"

    Select Case (lFieldID)
        Case (FID_MENU_ID):
                Set oField = New CFieldLong
                oField.ValueAsVariant = mvarMenuID
        Case (FID_MENU_TEXT):
                Set oField = New CFieldString
                oField.ValueAsVariant = mvarMenuText
        Case (FID_MENU_EXECPATH):
                Set oField = New CFieldString
                oField.ValueAsVariant = mvarExecPath
        Case (FID_MENU_ASPPAGE):
                Set oField = New CFieldString
                oField.ValueAsVariant = mvarASPPath
        Case (FID_MENU_PARENTID):
                Set oField = New CFieldLong
                oField.ValueAsVariant = mvarParentID
        Case (FID_MENU_SECURITY):
                Set oField = New CFieldLong
                oField.ValueAsVariant = mvarSecurity
        Case (FID_MENU_MENUTYPE):
                Set oField = New CFieldLong
                oField.ValueAsVariant = mvarMenuType
        Case (FID_MENU_SHORTCUTKEY):
                Set oField = New CFieldString
                oField.ValueAsVariant = mvarShortcutKey
        Case (FID_MENU_ICONID):
                Set oField = New CFieldLong
                oField.ValueAsVariant = mvarIconID
        Case (FID_MENU_WAITEXE):
                Set oField = New CFieldBool
                oField.ValueAsVariant = mvarWaitEXE
        Case (FID_MENU_ADDPATH):
                Set oField = New CFieldBool
                oField.ValueAsVariant = mvarAddPath
        Case (FID_MENU_GROUPID):
                Set oField = New CFieldLong
                oField.ValueAsVariant = mvarGroupID
        Case (FID_MENU_PARAMETERS):
                Set oField = New CFieldString
                oField.ValueAsVariant = mvarParameters
        Case (FID_MENU_SENDSESSION):
                Set oField = New CFieldBool
                oField.ValueAsVariant = mvarSendSession
    End Select
    oField.Id = lFieldID
    
    Set GetField = oField
    
End Function 'GetField

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_MENU

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Public Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Menu Object" & mvarMenuID

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo

    Set mcolChildren = New Collection
    Set mcolSubMenus = New Collection
    Set mcolReports = New Collection
    Set mcolMaintenance = New Collection
    Set mcolMenuOptions = New Collection
    Set m_oSession = oSession
    Set IBo_Initialise = Me
    
End Function
Public Function IBo_LoadFromRow(oRow As IRow) As Boolean

    Call LoadFromRow(oRow)

End Function

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 08/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_MENU_ID):           mvarMenuID = oField.ValueAsVariant
            Case (FID_MENU_TEXT):         mvarMenuText = oField.ValueAsVariant
            Case (FID_MENU_EXECPATH):     mvarExecPath = oField.ValueAsVariant
            Case (FID_MENU_ASPPAGE):      mvarASPPath = oField.ValueAsVariant
            Case (FID_MENU_PARENTID):     mvarParentID = oField.ValueAsVariant
            Case (FID_MENU_SECURITY):     mvarSecurity = oField.ValueAsVariant
            Case (FID_MENU_MENUTYPE):     mvarMenuType = oField.ValueAsVariant
            Case (FID_MENU_SHORTCUTKEY):  mvarShortcutKey = oField.ValueAsVariant
            Case (FID_MENU_ICONID):       mvarIconID = oField.ValueAsVariant
            Case (FID_MENU_WAITEXE):      mvarWaitEXE = oField.ValueAsVariant
            Case (FID_MENU_ADDPATH):      mvarAddPath = oField.ValueAsVariant
            Case (FID_MENU_GROUPID):      mvarGroupID = oField.ValueAsVariant
            Case (FID_MENU_SENDSESSION):  mvarSendSession = oField.ValueAsVariant
            Case (FID_MENU_PARAMETERS):   mvarParameters = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'IBo_LoadFromRow

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Public Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision
    
End Property

Public Function Retrieve(ByVal lMenuID As Long, ByVal GetChildren As Boolean) As Boolean
    
Dim lngMenuNo As Long
Dim oMenuItem As cMenuItem
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    mvarMenuID = lMenuID
    ' Pass the selector and get Menu Item specified in MenuID
    Call Me.IBo_AddLoadFilter(CMP_EQUAL, FID_MENU_ID, lMenuID)
    Retrieve = Load

    'Create second selector to retrieve all Sub Menus and Options for given Menu ID
    If GetChildren Then
    
        'create selector based on Menu Items where parent is specified MenuID
        Call ClearLoadFilter
        Call AddLoadFilter(CMP_EQUAL, FID_MENU_PARENTID, lMenuID)
        Call moRowSel.Merge(GetSelectAllRow)
    
        ' Pass the selector to the database to get a collection of Menu Items
        Set mcolChildren = m_oSession.Database.GetBoCollection(moRowSel)
        'step through children and copy into Sub Collections
        For lngMenuNo = 1 To mcolChildren.Count Step 1
            Set oMenuItem = mcolChildren(lngMenuNo)
            Select Case (oMenuItem.MenuType)
                Case (enmtSubMenu):       Call mcolSubMenus.Add(oMenuItem, CStr(oMenuItem.MenuID))
                Case (enmtMenuOption):    Call mcolMenuOptions.Add(oMenuItem, CStr(oMenuItem.MenuID))
                Case (enmtReportOption):  Call mcolReports.Add(oMenuItem, CStr(oMenuItem.MenuID))
                Case (enmtMaintOption):   Call mcolMaintenance.Add(oMenuItem, CStr(oMenuItem.MenuID))
            End Select
        Next lngMenuNo
    End If


End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Public Sub IBo_ClearLoadFilter()

    ClearLoadFilter
    
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub


Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = New CRowSelector
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(1))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 22/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'IBo_AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 22/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'IBo_AddLoadField



Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database

Dim lngNewGroupID As Long
Dim oField        As IField
Dim oRow          As IRowSelector

    Save = False
    
    If IsValid() Then
        'if sub menu and not a group then get next Group ID available
        If (mvarMenuType = enmtSubMenu) And (mvarGroupID = 0) And (eSave = SaveTypeIfNew) Then
            
            Set oRow = New CRowSelector
            
            'Create field for selection criteria and add to row selector
            Set oField = GetField(FID_MENU_MENUTYPE)
            If oField Is Nothing Then Exit Function
            oField.ValueAsVariant = enmtSubMenu
            Call oRow.AddSelection(CMP_EQUAL, oField)
    
            'Create field for selection criteria and add to row selector
            Set oField = GetField(FID_MENU_MENUTYPE)
            If oField Is Nothing Then Exit Function
            oField.ValueAsVariant = 0
            Call oRow.AddSelection(CMP_GREATERTHAN, oField)
                
            lngNewGroupID = m_oSession.Database.GetAggregateValue(AGG_MAX, GetField(FID_MENU_GROUPID), oRow)
            mvarGroupID = lngNewGroupID + 1
        End If
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If
    
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = New CRow
    For nFid = (CLASSID_MENU * &H10000) + 1 To FID_MENU_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

Public Function AddMenuOption(ByVal oNewMenu As cMenuItem) As Long

    'make the new menu item a child of this Menu Item
    oNewMenu.ParentID = mvarMenuID
    oNewMenu.MenuType = enmtMenuOption
    Call oNewMenu.IBo_SaveIfNew
    Call mcolMenuOptions.Add(oNewMenu, CStr(oNewMenu.MenuID))
    Call mcolChildren.Add(oNewMenu)
    AddMenuOption = oNewMenu.MenuID

End Function

Public Function AddReportOption(ByVal oNewMenu As cMenuItem) As Long

    'make the new menu item a child of this Menu Item
    oNewMenu.ParentID = mvarMenuID
    oNewMenu.MenuType = enmtReportOption
    Call oNewMenu.IBo_SaveIfNew
    Call mcolReports.Add(oNewMenu, CStr(oNewMenu.MenuID))
    Call mcolChildren.Add(oNewMenu)
    AddReportOption = oNewMenu.MenuID

End Function

Public Function AddMaintOption(ByVal oNewMenu As cMenuItem) As Long

    'make the new menu item a child of this Menu Item
    oNewMenu.ParentID = mvarMenuID
    oNewMenu.MenuType = enmtMaintOption
    Call oNewMenu.IBo_SaveIfNew
    Call mcolMaintenance.Add(oNewMenu, CStr(oNewMenu.MenuID))
    Call mcolChildren.Add(oNewMenu)
    AddMaintOption = oNewMenu.MenuID

End Function

Public Function AddSubMenu(ByVal oNewMenu As cMenuItem) As Long

    'make the new menu item a child of this Menu Item
    oNewMenu.ParentID = mvarMenuID
    oNewMenu.MenuType = enmtSubMenu
    If oNewMenu.IBo_SaveIfNew = True Then
        Call mcolSubMenus.Add(oNewMenu, CStr(oNewMenu.MenuID))
        Call mcolChildren.Add(oNewMenu)
        AddSubMenu = oNewMenu.MenuID
    End If

End Function

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_MENU_END_OF_STATIC

End Function


Public Function Interface(Optional eInterfaceType As Long) As cMenuItem

Dim oBO As IBo
    
    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function

