VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cCashier"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D3FC70D0276"
'<CAMH>****************************************************************************************
'* Module : cCashier
'* Date   : 25/09/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/EnterpriseBO/Cashier.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 4/12/03 15:58 $
'* $Revision: 11 $
'* Versions:
'* 25/09/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cCashier"

Implements IBo
Implements ISysBo

Private m_oSession As Session

Private mCashierNumber              As String
Private mDeleted                    As Boolean
Private mName                       As String
Private mPosition                   As String
Private mSignOnCode                 As String
Private mSupervisor                 As Boolean
Private mDefaultFloatAmount         As Currency
Private mNoOfSKULines(7)            As Long
Private mSKURetailSales(7)          As Double
Private mSKUCostOfSales(7)          As Double
Private mNoOfPriceViolations(7)     As Long
Private mMarkUpValueViolations(7)   As Double
Private mProjectSalesFlag           As String
Private mNoOfPSSQuotes(6)           As Long
Private mNoOfPSSCustomers(6)        As Long
Private mNoOfPSSQuoteConversions(6) As Long
Private mNoOfPSSFinancedSales(6)    As Long
Private mPSSSales(6)                As Double
Private mPSSFinancedSales(6)        As Double
Private mPSSInstallations(6)        As Double
Private mPSSAccessories(6)          As Double
Private mPSSCommissions(6)          As Double
Private mPSSValues(6)               As Double
Private mPSSCancelledOrders(6)      As Double

Dim moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Public Property Get CashierNumber() As String
    CashierNumber = mCashierNumber
End Property

Public Property Let CashierNumber(ByVal Value As String)
    mCashierNumber = Value
End Property

Public Property Get Deleted() As Boolean
    Deleted = mDeleted
End Property

Public Property Let Deleted(ByVal Value As Boolean)
    mDeleted = Value
End Property

Public Property Get Name() As String
    Name = mName
End Property

Public Property Let Name(ByVal Value As String)
    mName = Value
End Property

Public Property Get Position() As String
    Position = mPosition
End Property

Public Property Let Position(ByVal Value As String)
    mPosition = Value
End Property

Public Property Get SignOnCode() As String
    SignOnCode = mSignOnCode
End Property

Public Property Let SignOnCode(ByVal Value As String)
    mSignOnCode = Value
End Property

Public Property Get Supervisor() As Boolean
    Supervisor = mSupervisor
End Property

Public Property Let Supervisor(ByVal Value As Boolean)
    mSupervisor = Value
End Property

Public Property Get DefaultFloatAmount() As Currency
    DefaultFloatAmount = mDefaultFloatAmount
End Property

Public Property Let DefaultFloatAmount(ByVal Value As Currency)
    mDefaultFloatAmount = Value
End Property

Public Property Get NoOfSKULines(Index As Long) As Long
    NoOfSKULines = mNoOfSKULines(Index)
End Property

Public Property Let NoOfSKULines(Index As Long, ByVal Value As Long)
    mNoOfSKULines(Index) = Value
End Property

Public Property Get SKURetailSales(Index As Long) As Double
    SKURetailSales = mSKURetailSales(Index)
End Property

Public Property Let SKURetailSales(Index As Long, ByVal Value As Double)
    mSKURetailSales(Index) = Value
End Property

Public Property Get SKUCostOfSales(Index As Long) As Double
    SKUCostOfSales = mSKUCostOfSales(Index)
End Property

Public Property Let SKUCostOfSales(Index As Long, ByVal Value As Double)
    mSKUCostOfSales(Index) = Value
End Property

Public Property Get NoOfPriceViolations(Index As Long) As Long
    NoOfPriceViolations = mNoOfPriceViolations(Index)
End Property

Public Property Let NoOfPriceViolations(Index As Long, ByVal Value As Long)
    mNoOfPriceViolations(Index) = Value
End Property

Public Property Get MarkUpValueViolations(Index As Long) As Double
    MarkUpValueViolations = mMarkUpValueViolations(Index)
End Property

Public Property Let MarkUpValueViolations(Index As Long, ByVal Value As Double)
    mMarkUpValueViolations(Index) = Value
End Property

Public Property Get ProjectSalesFlag() As String
    ProjectSalesFlag = mProjectSalesFlag
End Property

Public Property Let ProjectSalesFlag(ByVal Value As String)
    mProjectSalesFlag = Value
End Property

Public Property Get NoOfPSSQuotes(Index As Long) As Long
    NoOfPSSQuotes = mNoOfPSSQuotes(Index)
End Property

Public Property Let NoOfPSSQuotes(Index As Long, ByVal Value As Long)
    mNoOfPSSQuotes(Index) = Value
End Property

Public Property Get NoOfPSSCustomers(Index As Long) As Long
    NoOfPSSCustomers = mNoOfPSSCustomers(Index)
End Property

Public Property Let NoOfPSSCustomers(Index As Long, ByVal Value As Long)
    mNoOfPSSCustomers(Index) = Value
End Property

Public Property Get NoOfPSSQuoteConversions(Index As Long) As Long
    NoOfPSSQuoteConversions = mNoOfPSSQuoteConversions(Index)
End Property

Public Property Let NoOfPSSQuoteConversions(Index As Long, ByVal Value As Long)
    mNoOfPSSQuoteConversions(Index) = Value
End Property

Public Property Get NoOfPSSFinancedSales(Index As Long) As Long
    NoOfPSSFinancedSales = mNoOfPSSFinancedSales(Index)
End Property

Public Property Let NoOfPSSFinancedSales(Index As Long, ByVal Value As Long)
    mNoOfPSSFinancedSales(Index) = Value
End Property

Public Property Get PSSSales(Index As Long) As Double
    PSSSales = mPSSSales(Index)
End Property

Public Property Let PSSSales(Index As Long, ByVal Value As Double)
    mPSSSales(Index) = Value
End Property

Public Property Get PSSFinancedSales(Index As Long) As Double
    PSSFinancedSales = mPSSFinancedSales(Index)
End Property

Public Property Let PSSFinancedSales(Index As Long, ByVal Value As Double)
    mPSSFinancedSales(Index) = Value
End Property

Public Property Get PSSInstallations(Index As Long) As Double
    PSSInstallations = mPSSInstallations(Index)
End Property

Public Property Let PSSInstallations(Index As Long, ByVal Value As Double)
    mPSSInstallations(Index) = Value
End Property

Public Property Get PSSAccessories(Index As Long) As Double
    PSSAccessories = mPSSAccessories(Index)
End Property

Public Property Let PSSAccessories(Index As Long, ByVal Value As Double)
    mPSSAccessories(Index) = Value
End Property

Public Property Get PSSCommissions(Index As Long) As Double
    PSSCommissions = mPSSCommissions(Index)
End Property

Public Property Let PSSCommissions(Index As Long, ByVal Value As Double)
    mPSSCommissions(Index) = Value
End Property

Public Property Get PSSValues(Index As Long) As Double
    PSSValues = mPSSValues(Index)
End Property

Public Property Let PSSValues(Index As Long, ByVal Value As Double)
    mPSSValues(Index) = Value
End Property

Public Property Get PSSCancelledOrders(Index As Long) As Double
    PSSCancelledOrders = mPSSCancelledOrders(Index)
End Property

Public Property Let PSSCancelledOrders(Index As Long, ByVal Value As Double)
    mPSSCancelledOrders(Index) = Value
End Property



'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 08/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_CASHIER_CashierNumber):           mCashierNumber = oField.ValueAsVariant
            Case (FID_CASHIER_Deleted):                 mDeleted = oField.ValueAsVariant
            Case (FID_CASHIER_Name):                    mName = oField.ValueAsVariant
            Case (FID_CASHIER_Position):                mPosition = oField.ValueAsVariant
            Case (FID_CASHIER_SignOnCode):              mSignOnCode = oField.ValueAsVariant
            Case (FID_CASHIER_Supervisor):              mSupervisor = oField.ValueAsVariant
            Case (FID_CASHIER_DefaultFloatAmount):      mDefaultFloatAmount = oField.ValueAsVariant
            Case (FID_CASHIER_NoOfSKULines):            Call LoadLongArray(mNoOfSKULines, oField, m_oSession)
            Case (FID_CASHIER_SKURetailSales):          Call LoadDoubleArray(mSKURetailSales, oField, m_oSession)
            Case (FID_CASHIER_SKUCostOfSales):          Call LoadDoubleArray(mSKUCostOfSales, oField, m_oSession)
            Case (FID_CASHIER_NoOfPriceViolations):     Call LoadLongArray(mNoOfPriceViolations, oField, m_oSession)
            Case (FID_CASHIER_MarkUpValueViolations):   Call LoadDoubleArray(mMarkUpValueViolations, oField, m_oSession)
            Case (FID_CASHIER_ProjectSalesFlag):        mProjectSalesFlag = oField.ValueAsVariant
            Case (FID_CASHIER_NoOfPSSQuotes):           Call LoadLongArray(mNoOfPSSQuotes, oField, m_oSession)
            Case (FID_CASHIER_NoOfPSSCustomers):        Call LoadLongArray(mNoOfPSSCustomers, oField, m_oSession)
            Case (FID_CASHIER_NoOfPSSQuoteConversions): Call LoadLongArray(mNoOfPSSQuoteConversions, oField, m_oSession)
            Case (FID_CASHIER_NoOfPSSFinancedSales):    Call LoadLongArray(mNoOfPSSFinancedSales, oField, m_oSession)
            Case (FID_CASHIER_PSSSales):                Call LoadDoubleArray(mPSSSales, oField, m_oSession)
            Case (FID_CASHIER_PSSFinancedSales):        Call LoadDoubleArray(mPSSFinancedSales, oField, m_oSession)
            Case (FID_CASHIER_PSSInstallations):        Call LoadDoubleArray(mPSSInstallations, oField, m_oSession)
            Case (FID_CASHIER_PSSAccessories):          Call LoadDoubleArray(mPSSAccessories, oField, m_oSession)
            Case (FID_CASHIER_PSSCommissions):          Call LoadDoubleArray(mPSSCommissions, oField, m_oSession)
            Case (FID_CASHIER_PSSValues):               Call LoadDoubleArray(mPSSValues, oField, m_oSession)
            Case (FID_CASHIER_PSSCancelledOrders):      Call LoadDoubleArray(mPSSCancelledOrders, oField, m_oSession)
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow



Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Cashier " & mCashierNumber

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_CASHIER

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Function IBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cCashier

End Function

Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    
    Select Case (lFieldID)
        Case (FID_CASHIER_CashierNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCashierNumber
        Case (FID_CASHIER_Deleted):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mDeleted
        Case (FID_CASHIER_Name):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mName
        Case (FID_CASHIER_Position):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPosition
        Case (FID_CASHIER_SignOnCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSignOnCode
        Case (FID_CASHIER_Supervisor):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mSupervisor
        Case (FID_CASHIER_DefaultFloatAmount):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mDefaultFloatAmount
        Case (FID_CASHIER_NoOfSKULines):
                Set GetField = SaveLongArray(mNoOfSKULines, m_oSession)
        Case (FID_CASHIER_SKURetailSales):
                Set GetField = SaveDoubleArray(mSKURetailSales, m_oSession)
        Case (FID_CASHIER_SKUCostOfSales):
                Set GetField = SaveDoubleArray(mSKUCostOfSales, m_oSession)
        Case (FID_CASHIER_NoOfPriceViolations):
                Set GetField = SaveLongArray(mNoOfPriceViolations, m_oSession)
        Case (FID_CASHIER_MarkUpValueViolations):
                Set GetField = SaveDoubleArray(mMarkUpValueViolations, m_oSession)
        Case (FID_CASHIER_ProjectSalesFlag):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mProjectSalesFlag
        Case (FID_CASHIER_NoOfPSSQuotes):
                Set GetField = SaveLongArray(mNoOfPSSQuotes, m_oSession)
        Case (FID_CASHIER_NoOfPSSCustomers):
                Set GetField = SaveLongArray(mNoOfPSSCustomers, m_oSession)
        Case (FID_CASHIER_NoOfPSSQuoteConversions):
                Set GetField = SaveLongArray(mNoOfPSSQuoteConversions, m_oSession)
        Case (FID_CASHIER_NoOfPSSFinancedSales):
                Set GetField = SaveLongArray(mNoOfPSSFinancedSales, m_oSession)
        Case (FID_CASHIER_PSSSales):
                Set GetField = SaveDoubleArray(mPSSSales, m_oSession)
        Case (FID_CASHIER_PSSFinancedSales):
                Set GetField = SaveDoubleArray(mPSSFinancedSales, m_oSession)
        Case (FID_CASHIER_PSSInstallations):
                Set GetField = SaveDoubleArray(mPSSInstallations, m_oSession)
        Case (FID_CASHIER_PSSAccessories):
                Set GetField = SaveDoubleArray(mPSSAccessories, m_oSession)
        Case (FID_CASHIER_PSSCommissions):
                Set GetField = SaveDoubleArray(mPSSCommissions, m_oSession)
        Case (FID_CASHIER_PSSValues):
                Set GetField = SaveDoubleArray(mPSSValues, m_oSession)
        Case (FID_CASHIER_PSSCancelledOrders):
                Set GetField = SaveDoubleArray(mPSSCancelledOrders, m_oSession)
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_CASHIER, FID_CASHIER_END_OF_STATIC, m_oSession)

End Function

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function


'##ModelId=3D3FC7220262
Public Function Retrieve() As Collection
    
Dim oBOCol      As Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    ' Pass the selector to the database to get the data view
    Set Retrieve = m_oSession.Database.GetBoCollection(GetSelectAllRow)

End Function



Private Function Initialise(oSession As ISession) As cCashier
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3FC71E0370
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

'##ModelId=3D3FC71E0370
Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cCashier
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_CASHIER * &H10000) + 1 To FID_CASHIER_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean IBo_SetLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_SetLoadField(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_SetLoadField"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_SetLoadField = True

End Function 'IBo_SetLoadField




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_CASHIER_END_OF_STATIC

End Function


Public Function UpdateUserInfo(strUserName As String, _
                                strPosition As String, _
                                strSignOnCode As String, _
                                blnSupervisor As Boolean, _
                                blnDeleted As Boolean) As Boolean
                            
Dim blnSaved As Boolean
Dim oRow     As IRow

    mName = strUserName
    mPosition = strPosition
    mSignOnCode = strSignOnCode
    mSupervisor = blnSupervisor
    mDeleted = blnDeleted

    ' and persist this new value in the database
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_CASHIER_CashierNumber))
    Call oRow.Add(GetField(FID_CASHIER_Name))
    Call oRow.Add(GetField(FID_CASHIER_Position))
    Call oRow.Add(GetField(FID_CASHIER_SignOnCode))
    Call oRow.Add(GetField(FID_CASHIER_Supervisor))
    Call oRow.Add(GetField(FID_CASHIER_Deleted))
       
    blnSaved = m_oSession.Database.SavePartialBo(Me, oRow)
    Debug.Assert blnSaved
    Set oRow = Nothing
    
    UpdateUserInfo = blnSaved

End Function 'UpdateUserInfo

Friend Function SetPassword(strCashierID As String, _
                            strPassword As String) As Boolean
                            
Dim blnSaved As Boolean
Dim oRow     As IRow

    mSignOnCode = strPassword
    mCashierNumber = strCashierID

    ' and persist this new value in the database
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_CASHIER_CashierNumber))
    Call oRow.Add(GetField(FID_CASHIER_SignOnCode))
       
    blnSaved = m_oSession.Database.SavePartialBo(Me, oRow)
    Debug.Assert blnSaved
    Set oRow = Nothing
    
    SetPassword = blnSaved

End Function 'SetPassword


