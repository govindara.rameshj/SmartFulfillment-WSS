VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cNightMaster"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D53820F0032"
'<CAMH>****************************************************************************************
'* Module: cNightMaster
'* Date  : 14/08/02
'* Author: mauricem
'*$Archive: /Projects/OasysV2/VB/EnterpriseBO/NightMaster.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of a single Night Master Task
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 6/10/03 9:53 $
'* $Revision: 11 $
'* Versions:
'* 14/08/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cNightMaster"

Implements IBo
Implements ISysBo

Private m_oSession As Session

'##ModelId=3D53822103AC
Private mSetNumber As String

'##ModelId=3D53822603AC
Private mTaskNumber As String

'##ModelId=3D53822E0320
Private mDescription As String

'##ModelId=3D53823400B4
Private mProgramPath As String

'##ModelId=3D53823C005A
Private mRunNightly As Boolean

'##ModelId=3D53824601D6
Private mRunInRetry As Boolean

'##ModelId=3D53826200D2
Private mRunAtWeekEnd As Boolean

'##ModelId=3D53826A037A
Private mRunAtPeriodEnd As Boolean

'##ModelId=3D53829001FE
Private mRunBeforeStoreLive As Boolean

'##ModelId=3D53829B003C
Private mAbortOnError As Boolean

Private mOptionNumber As Long

'##ModelId=3D5382A30348
Private mErrorHandler As String

'##ModelId=3D5382BB0208
Private mRunOnDayWeek(7) As Boolean

'##ModelId=3D5382CA0014
Private mStartDate As Date

'##ModelId=3D5382CF030C
Private mEndDate As Date

Dim moRowSel As IRowSelector

Private moLoadRow As IRowSelector

'##ModelId=3D5A14A90334
Public Property Get EndDate() As Date
   Let EndDate = mEndDate
End Property

'##ModelId=3D5A14A901EA
Public Property Let EndDate(ByVal Value As Date)
    Let mEndDate = Value
End Property

'##ModelId=3D5A14A9010E
Public Property Get StartDate() As Date
   Let StartDate = mStartDate
End Property

'##ModelId=3D5A14A803AC
Public Property Let StartDate(ByVal Value As Date)
    Let mStartDate = Value
End Property

'##ModelId=3D5A14A802D0
Public Property Get RunOnDayWeek(Index As Variant) As Boolean
   Let RunOnDayWeek = mRunOnDayWeek(Index)
End Property

'##ModelId=3D5A14A801B8
Public Property Let RunOnDayWeek(Index As Variant, ByVal Value As Boolean)
    Let mRunOnDayWeek(Index) = Value
End Property

'##ModelId=3D5A14A800DC
Public Property Get ErrorHandler() As String
   Let ErrorHandler = mErrorHandler
End Property

'##ModelId=3D5A14A7037A
Public Property Let ErrorHandler(ByVal Value As String)
    Let mErrorHandler = Value
End Property

'##ModelId=3D5A14A702DA
Public Property Get AbortOnError() As Boolean
   Let AbortOnError = mAbortOnError
End Property

'##ModelId=3D5A14A70190
Public Property Let AbortOnError(ByVal Value As Boolean)
    Let mAbortOnError = Value
End Property

'##ModelId=3D5A14A700E6
Public Property Get RunBeforeStoreLive() As Boolean
   Let RunBeforeStoreLive = mRunBeforeStoreLive
End Property

'##ModelId=3D5A14A603C0
Public Property Let RunBeforeStoreLive(ByVal Value As Boolean)
    Let mRunBeforeStoreLive = Value
End Property

'##ModelId=3D5A14A60316
Public Property Get RunAtPeriodEnd() As Boolean
   Let RunAtPeriodEnd = mRunAtPeriodEnd
End Property

'##ModelId=3D5A14A60208
Public Property Let RunAtPeriodEnd(ByVal Value As Boolean)
    Let mRunAtPeriodEnd = Value
End Property

'##ModelId=3D5A14A6012C
Public Property Get RunAtWeekEnd() As Boolean
   Let RunAtWeekEnd = mRunAtWeekEnd
End Property

'##ModelId=3D5A14A60050
Public Property Let RunAtWeekEnd(ByVal Value As Boolean)
    Let mRunAtWeekEnd = Value
End Property

'##ModelId=3D5A14A50398
Public Property Get RunInRetry() As Boolean
   Let RunInRetry = mRunInRetry
End Property

'##ModelId=3D5A14A50280
Public Property Let RunInRetry(ByVal Value As Boolean)
    Let mRunInRetry = Value
End Property

'##ModelId=3D5A14A501E0
Public Property Get RunNightly() As Boolean
   Let RunNightly = mRunNightly
End Property

'##ModelId=3D5A14A50104
Public Property Let RunNightly(ByVal Value As Boolean)
    Let mRunNightly = Value
End Property

'##ModelId=3D5A14A5005A
Public Property Get ProgramPath() As String
   Let ProgramPath = mProgramPath
End Property

'##ModelId=3D5A14A40366
Public Property Let ProgramPath(ByVal Value As String)
    Let mProgramPath = Value
End Property

'##ModelId=3D5A14A402F8
Public Property Get Description() As String
   Let Description = mDescription
End Property

'##ModelId=3D5A14A4021C
Public Property Let Description(ByVal Value As String)
    Let mDescription = Value
End Property

'##ModelId=3D5A14A401AE
Public Property Get TaskNumber() As String
   Let TaskNumber = mTaskNumber
End Property

'##ModelId=3D5A14A400D2
Public Property Let TaskNumber(ByVal Value As String)
    Let mTaskNumber = Value
End Property

'##ModelId=3D5A14A40064
Public Property Get SetNumber() As String
   Let SetNumber = mSetNumber
End Property

'##ModelId=3D5A14A30370
Public Property Let SetNumber(ByVal Value As String)
    Let mSetNumber = Value
End Property

Public Property Get OptionNumber() As Long
   Let OptionNumber = mOptionNumber
End Property

'##ModelId=3D5A14A30370
Public Property Let OptionNumber(ByVal Value As Long)
    Let mOptionNumber = Value
End Property

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 14/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
'##ModelId=3D749F8A038E
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_NIGHTMASTER_SetNumber):       mSetNumber = oField.ValueAsVariant
            Case (FID_NIGHTMASTER_TaskNumber):      mTaskNumber = oField.ValueAsVariant
            Case (FID_NIGHTMASTER_Description):     mDescription = oField.ValueAsVariant
            Case (FID_NIGHTMASTER_ProgramPath):     mProgramPath = oField.ValueAsVariant
            Case (FID_NIGHTMASTER_RunNightly):      mRunNightly = oField.ValueAsVariant
            Case (FID_NIGHTMASTER_RunInRetry):      mRunInRetry = oField.ValueAsVariant
            Case (FID_NIGHTMASTER_RunAtWeekEnd):    mRunAtWeekEnd = oField.ValueAsVariant
            Case (FID_NIGHTMASTER_RunAtPeriodEnd):  mRunAtPeriodEnd = oField.ValueAsVariant
            Case (FID_NIGHTMASTER_RunBeforeStoreLive): mRunBeforeStoreLive = oField.ValueAsVariant
            Case (FID_NIGHTMASTER_OptionNumber):    mOptionNumber = oField.ValueAsVariant
            Case (FID_NIGHTMASTER_AbortOnError):    mAbortOnError = oField.ValueAsVariant
            Case (FID_NIGHTMASTER_ErrorHandler):    mErrorHandler = oField.ValueAsVariant
            Case (FID_NIGHTMASTER_RunOnDayWeek):    Call LoadBooleanArray(mRunOnDayWeek, oField, m_oSession)
            Case (FID_NIGHTMASTER_StartDate):       mStartDate = oField.ValueAsVariant
            Case (FID_NIGHTMASTER_EndDate):         mEndDate = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow



Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Night Master " & mSetNumber

End Property
Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function


Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_NIGHTMASTER

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property
Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cNightMaster

End Function


Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    
    Select Case (lFieldID)
        Case (FID_NIGHTMASTER_SetNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSetNumber
        Case (FID_NIGHTMASTER_TaskNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTaskNumber
        Case (FID_NIGHTMASTER_Description):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDescription
        Case (FID_NIGHTMASTER_ProgramPath):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mProgramPath
        Case (FID_NIGHTMASTER_RunNightly):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mRunNightly
        Case (FID_NIGHTMASTER_RunInRetry):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mRunInRetry
        Case (FID_NIGHTMASTER_RunAtWeekEnd):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mRunAtWeekEnd
        Case (FID_NIGHTMASTER_RunAtPeriodEnd):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mRunAtPeriodEnd
        Case (FID_NIGHTMASTER_RunBeforeStoreLive):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mRunBeforeStoreLive
        Case (FID_NIGHTMASTER_OptionNumber):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mOptionNumber
        Case (FID_NIGHTMASTER_AbortOnError):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mAbortOnError
        Case (FID_NIGHTMASTER_ErrorHandler):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mErrorHandler
        Case (FID_NIGHTMASTER_RunOnDayWeek): Set GetField = SaveBooleanArray(mRunOnDayWeek, m_oSession)
        Case (FID_NIGHTMASTER_StartDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mStartDate
        Case (FID_NIGHTMASTER_EndDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mEndDate
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_NIGHTMASTER, FID_NIGHTMASTER_END_OF_STATIC, m_oSession)

End Function

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
'    If lenb(lId) <> 0 Then mPartCode = sId
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function


Public Function Retrieve() As Collection
    
Dim oBOCol      As Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    ' Pass the selector to the database to get the data view
    Set Retrieve = m_oSession.Database.GetBoCollection(GetSelectAllRow)

End Function


Private Function Initialise(oSession As ISession) As cNightMaster
    Set m_oSession = oSession
    Set Initialise = Me
End Function


Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cNightMaster
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_NIGHTMASTER * &H10000) + 1 To FID_NIGHTMASTER_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_SetLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_SetLoadField(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_SetLoadField"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_SetLoadField = True

End Function 'IBo_SetLoadField




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_NIGHTMASTER_END_OF_STATIC

End Function

'<CACH>****************************************************************************************
'* Function:  Variant GetNightMasterSets()
'**********************************************************************************************
'* Description: function to return array of distinct Night Master sets
'**********************************************************************************************
'* Parameters:
'* None.
'**********************************************************************************************
'* Returns:  Variant
'**********************************************************************************************
'* History:
'* 20/01/03    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function GetNightMasterSets() As Variant

Dim oField    As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".GetNightMasterSets"
 
    Set oField = IBo_GetField(FID_NIGHTMASTER_SetNumber)
    
    GetNightMasterSets = m_oSession.Database.GetAggregateValue(AGG_DIST, oField, Nothing)

End Function 'GetDisplayLocations

