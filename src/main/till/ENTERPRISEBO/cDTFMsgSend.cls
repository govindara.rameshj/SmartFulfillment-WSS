VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cDTFMsgSend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module: cDTFMsgPend
'* Date  : 14/03/07
'* Author: mauricem
'*$Archive: /Projects/OasysV2/VB/EnterpriseBO/cDTFMsgSend.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of a single Night Log entry
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 12/03/07 12:01 $
'* $Revision: 1 $
'* Versions:
'* 14/08/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit


Const MODULE_NAME As String = "cDTFMsgPend"

Implements IBo
Implements ISysBo

Private m_oSession As Object

Dim moRowSel As Object

Private moLoadRow As Object

Private mMessageID As Long
Private mMessageType As Long
Private mMessageDesc As String
Private mToStore As String
Private mMasterKey As String
Private mMasterApp As String
Private mDateAdded As Date
Private mNoAttempts As Long
Private mLastAttempt As Date
Private mDateSent As Date
Private mCompleted As Boolean

Public Property Get MessageID() As Long
   Let MessageID = mMessageID
End Property

Public Property Let MessageID(ByVal Value As Long)
    Let mMessageID = Value
End Property

Public Property Get MessageType() As Long
   Let MessageType = mMessageType
End Property

Public Property Let MessageType(ByVal Value As Long)
    Let mMessageType = Value
End Property

Public Property Get MessageDesc() As String
   Let MessageDesc = mMessageDesc
End Property

Public Property Let MessageDesc(ByVal Value As String)
    Let mMessageDesc = Value
End Property


Public Property Get ToStore() As String
   Let ToStore = mToStore
End Property

Public Property Let ToStore(ByVal Value As String)
    Let mToStore = Value
End Property

Public Property Get MasterKey() As String
   Let MasterKey = mMasterKey
End Property

Public Property Let MasterKey(ByVal Value As String)
    Let mMasterKey = Value
End Property

Public Property Get MasterApp() As String
   Let MasterApp = mMasterApp
End Property

Public Property Let MasterApp(ByVal Value As String)
    Let mMasterApp = Value
End Property

Public Property Get DateAdded() As Date
   Let DateAdded = mDateAdded
End Property

Public Property Let DateAdded(ByVal Value As Date)
    Let mDateAdded = Value
End Property

Public Property Get NoAttempts() As Long
   Let NoAttempts = mNoAttempts
End Property

Public Property Let NoAttempts(ByVal Value As Long)
    Let mNoAttempts = Value
End Property

Public Property Get LastAttempt() As Date
   Let LastAttempt = mLastAttempt
End Property

Public Property Let LastAttempt(ByVal Value As Date)
    Let mLastAttempt = Value
End Property

Public Property Get DateSent() As Date
   Let DateSent = mDateSent
End Property

Public Property Let DateSent(ByVal Value As Date)
    Let mDateSent = Value
End Property

Public Property Get Completed() As Boolean
   Let Completed = mCompleted
End Property

Public Property Let Completed(ByVal Value As Boolean)
    Let mCompleted = Value
End Property

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* MasterApp: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 14/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
'##ModelId=3D749F8A038E
Public Function LoadFromRow(oRow As Object) As Boolean

Dim lFieldNo As Integer
Dim oField   As Object

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.IField_Id)
            Case (FID_DTF_SEND_MessageID):   mMessageID = oField.LongValue
            Case (FID_DTF_SEND_MessageType): mMessageType = oField.LongValue
            Case (FID_DTF_SEND_MessageDesc): mMessageDesc = oField.StringValue
            Case (FID_DTF_SEND_ToStore):     mToStore = oField.StringValue
            Case (FID_DTF_SEND_MasterKey):   mMasterKey = oField.StringValue
            Case (FID_DTF_SEND_MasterApp):   mMasterApp = oField.StringValue
            Case (FID_DTF_SEND_DateAdded):   mDateAdded = oField.DateValue
            Case (FID_DTF_SEND_NoAttempts):  mNoAttempts = oField.LongValue
            Case (FID_DTF_SEND_LastAttempt): mLastAttempt = oField.DateValue
            Case (FID_DTF_SEND_DateSent):    mDateSent = oField.DateValue
            Case (FID_DTF_SEND_Completed):   mDateSent = oField.BoolValue
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow


Public Function CreateMessageToStore(strStoreNo As String, lngMessageType As Long, strMasterKey As String) As Boolean

    mMessageType = lngMessageType
    mToStore = strStoreNo
    mMasterKey = strMasterKey
    mDateAdded = Now()
    mMasterApp = GetMasterApp(lngMessageType)
    CreateMessageToStore = IBo_SaveIfNew

End Function

Public Function CreateMessageToHeadOffice(lngMessageType As Long, strMasterKey As String) As Boolean

    mMessageType = lngMessageType
    mToStore = "999"
    mMasterKey = strMasterKey
    mDateAdded = Now()
    mNoAttempts = 0
    mCompleted = False
    mMasterApp = GetMasterApp(lngMessageType)
    CreateMessageToHeadOffice = IBo_SaveIfNew

End Function

Private Function GetMasterApp(lngMessageType As Long) As String

    Select Case (lngMessageType)
        Case (1): GetMasterApp = "TillFunction"
                  mMessageDesc = "SALES"
        Case Else
            GetMasterApp = "Unknown(" & lngMessageType & ")"
    End Select
    
End Function

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "DTF Pend-" & mMessageID & " Type=" & mMessageType & " Store-" & mToStore & " Data ID-" & mMasterKey

End Property
Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function


Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_DTF_SEND

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property
Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cDTFMsgSend

End Function

Public Function IBo_Load() As Boolean
    
    IBo_Load = Load

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property


Private Function GetField(lFieldID As Long) As Object

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    Select Case (lFieldID)
        Case (FID_DTF_SEND_MessageID):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldLong")
                GetField.LongValue = mMessageID
        Case (FID_DTF_SEND_MessageType):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldLong")
                GetField.LongValue = mMessageType
        Case (FID_DTF_SEND_MessageDesc):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.StringValue = mMessageDesc
        Case (FID_DTF_SEND_ToStore):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.StringValue = mToStore
        Case (FID_DTF_SEND_MasterKey):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.StringValue = mMasterKey
        Case (FID_DTF_SEND_MasterApp):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.StringValue = mMasterApp
        Case (FID_DTF_SEND_DateAdded):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldDate")
                GetField.DateValue = mDateAdded
        Case (FID_DTF_SEND_NoAttempts):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldLong")
                GetField.LongValue = mNoAttempts
        Case (FID_DTF_SEND_LastAttempt):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldDate")
                GetField.DateValue = mLastAttempt
        Case (FID_DTF_SEND_DateSent):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldDate")
                GetField.DateValue = mDateSent
        Case (FID_DTF_SEND_Completed):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldBool")
                GetField.BoolValue = mCompleted
            Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.IField_Id = lFieldID
    
    Exit Function
    
End Function

Private Function GetSelectAllRow() As Object
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_DTF_SEND, FID_DTF_SEND_END_OF_STATIC, m_oSession)

End Function

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As Object
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function


Private Function Initialise(oSession As Object) As cDTFMsgSend
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As IBo
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As Object
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_DTF_SEND * &H10000) + 1 To FID_DTF_SEND_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean IBo_SetLoadField()
'**********************************************************************************************
'* MasterApp: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_SetLoadField(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As Object

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_SetLoadField"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.IField_ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_SetLoadField = True

End Function 'IBo_SetLoadField



'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* MasterApp: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As Object

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.IField_ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_AddLoadFilter = True

End Function 'IBo_AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* MasterApp: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'IBo_AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As Object
Dim colBO       As Collection
Dim oBO         As Object
Dim lBONo       As Long
Dim oLoadFields As Object
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(1))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set IBo_LoadMatches = colBO

End Function

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_DTF_SEND_END_OF_STATIC

End Function


