VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cDiscountCardList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D3FC70D0276"
'<CAMH>****************************************************************************************
'* Module : cDiscountCardList
'* Date   : 05/06/07
'* Author :MichaelO'C
'*$Archive: /Projects/OasysV2/VB/EnterpriseBO/cDiscountCardList.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cDiscountCardList"
Const CARD_SCHEME_INCLUSIVE = "I"
Const CARD_SCHEME_EXCLUSIVE = "E"

Implements IBo
Implements ISysBo

Private m_oSession As Session

Private mCardNumber As String
Private mCustomerName As String
Private mCustomerAddressLine1 As String
Private mCustomerAddressLine2 As String
Private mCustomerAddressLine3 As String
Private mCustomerAddressLine4 As String
Private mCustomerPostCode As String
Private mHideAddress As Boolean
Private mCardDeleted As Boolean

Dim moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Public Property Get CardNumber() As String
    CardNumber = mCardNumber
End Property

Public Property Let CardNumber(ByVal Value As String)
    mCardNumber = Value
End Property

Public Property Get CustomerName() As String
    CustomerName = mCustomerName
End Property

Public Property Let CustomerName(ByVal Value As String)
    mCustomerName = Value
End Property

Public Property Get CustomerAddressLine1() As String
    CustomerAddressLine1 = mCustomerAddressLine1
End Property

Public Property Let CustomerAddressLine1(ByVal Value As String)
    mCustomerAddressLine1 = Value
End Property

Public Property Get CustomerAddressLine2() As String
    CustomerAddressLine2 = mCustomerAddressLine2
End Property

Public Property Let CustomerAddressLine2(ByVal Value As String)
    mCustomerAddressLine2 = Value
End Property

Public Property Get CustomerAddressLine3() As String
    CustomerAddressLine3 = mCustomerAddressLine3
End Property

Public Property Let CustomerAddressLine3(ByVal Value As String)
    mCustomerAddressLine3 = Value
End Property

Public Property Get CustomerAddressLine4() As String
    CustomerAddressLine4 = mCustomerAddressLine4
End Property

Public Property Let CustomerAddressLine4(ByVal Value As String)
    mCustomerAddressLine4 = Value
End Property

Public Property Get CustomerPostCode() As String
    CustomerPostCode = mCustomerPostCode
End Property

Public Property Let CustomerPostCode(ByVal Value As String)
    mCustomerPostCode = Value
End Property

Public Property Get HideAdress() As Boolean
    HideAdress = mHideAddress
End Property

Public Property Let HideAdress(ByVal Value As Boolean)
    mHideAddress = Value
End Property

Public Property Get CardDeleted() As Boolean
    CardDeleted = mCardDeleted
End Property

Public Property Let CardDeleted(ByVal Value As Boolean)
    mCardDeleted = Value
End Property

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 08/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_DISCOUNTCARD_LIST_CardNumber):                mCardNumber = oField.ValueAsVariant
            Case (FID_DISCOUNTCARD_LIST_CustomerName):              mCustomerName = oField.ValueAsVariant
            Case (FID_DISCOUNTCARD_LIST_CustomerAddressLine1):      mCustomerAddressLine1 = oField.ValueAsVariant
            Case (FID_DISCOUNTCARD_LIST_CustomerAddressLine2):      mCustomerAddressLine2 = oField.ValueAsVariant
            Case (FID_DISCOUNTCARD_LIST_CustomerAddressLine3):      mCustomerAddressLine3 = oField.ValueAsVariant
            Case (FID_DISCOUNTCARD_LIST_CustomerAddressLine4):      mCustomerAddressLine4 = oField.ValueAsVariant
            Case (FID_DISCOUNTCARD_LIST_CustomerPostCode):          mCustomerPostCode = oField.ValueAsVariant
            Case (FID_DISCOUNTCARD_LIST_HideAddress):               mHideAddress = (oField.ValueAsVariant = "Y")
            Case (FID_DISCOUNTCARD_LIST_CardDeleted):               mCardDeleted = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow



Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Cashier " & mCardNumber

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_DISCOUNTCARD_LIST

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Function IBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cDiscountCardList

End Function

Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    
    Select Case (lFieldID)
        Case (FID_DISCOUNTCARD_LIST_CardNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCardNumber
        Case (FID_DISCOUNTCARD_LIST_CustomerName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCustomerName
        Case (FID_DISCOUNTCARD_LIST_CustomerAddressLine1):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCustomerAddressLine1
        Case (FID_DISCOUNTCARD_LIST_CustomerAddressLine2):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCustomerAddressLine2
        Case (FID_DISCOUNTCARD_LIST_CustomerAddressLine3):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCustomerAddressLine3
        Case (FID_DISCOUNTCARD_LIST_CustomerAddressLine4):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCustomerAddressLine4
        Case (FID_DISCOUNTCARD_LIST_CustomerPostCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCustomerPostCode
        Case (FID_DISCOUNTCARD_LIST_HideAddress):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = IIf(mHideAddress = True, "Y", "N")
        Case (FID_DISCOUNTCARD_LIST_CardDeleted):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mCardDeleted
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_DISCOUNTCARD_LIST, FID_DISCOUNTCARD_LIST_END_OF_STATIC, m_oSession)

End Function

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function


'##ModelId=3D3FC7220262
Public Function Retrieve() As Collection
    
Dim oBOCol      As Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    ' Pass the selector to the database to get the data view
    Set Retrieve = m_oSession.Database.GetBoCollection(GetSelectAllRow)

End Function



Private Function Initialise(oSession As ISession) As cDiscountCardList
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3FC71E0370
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

'##ModelId=3D3FC71E0370
Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cDiscountCardList
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_DISCOUNTCARD_LIST * &H10000) + 1 To FID_DISCOUNTCARD_LIST_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean IBo_SetLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_SetLoadField(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_SetLoadField"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_SetLoadField = True

End Function 'IBo_SetLoadField

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    
    ClearLoadFilter

End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    
    ClearLoadField

End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    
    Set IBo_Interface = Interface(eInterfaceType)

End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_DISCOUNTCARD_LIST_END_OF_STATIC

End Function
