VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cUser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"438460C10212"
'<CAMH>****************************************************************************************
'* Module : cUser
'* Date   : 23/11/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/EnterpriseBO/cUser.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of a single User
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 12/01/04 14:07 $ $Revision: 15 $
'* Versions:
'* 23/11/02    mauricem
'*             Header added.
'*
'*  09/11/05    DaveF   1.0.252 Changed method GetField() to correctly use mPayrollNumber instead of
'*                                  mPosition when refencing the Employee Payroll number field.
'*
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit
Const MODULE_NAME As String = "cUser"

Implements IBo
Implements ISysBo

'##ModelId=4384612601AE
Private mEmployeeID As String

'##ModelId=4384612C005A
Private mFullName As String

'##ModelId=43846136038E
Private mInitials As String

'##ModelId=4384613A03CA
Private mPosition As String

Private mPayrollNumber As String

'##ModelId=4384614900C8
Private mPassword As String

'##ModelId=4384614D021C
Private mPasswordValidTill As Date

'##ModelId=43846157035C
Private mSupervisor As Boolean

'##ModelId=438461610212
Private mAuthorisationCode As String

'##ModelId=4384616C015E
Private mAuthorisationValidTill As Date

'##ModelId=4384617602A8
Private mCurrentOutlet As String

'##ModelId=43846182021C
Private mGroupLevels(99) As Boolean

'##ModelId=4384618D001E
Private mSecurityLevels(99) As String

'##ModelId=438461A001A4
Private mDeleted As Boolean

'##ModelId=438461A70154
Private mDateDeleted As Date

'##ModelId=438461B0015E
Private mTimeDeleted As String

'##ModelId=438461B700D2
Private mDeletedBy As String

'##ModelId=438461BC0258
Private mDeletedOutlet As String

'##ModelId=438461D6012C
Private mTillReceiptName As String

'##ModelId=438461E0015E
Private mFloatAmount As Double

'##ModelId=438461E70258
Private mLanguageCode As String

Private mManager As Boolean

Private m_oSession As Session

Dim moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private moCashier As cCashier

Private mblnUpdateCashier As Boolean

' The Session object holds a User object for the current user.  This determines
' the user ID and her permissions.  We want to be able to protect the object
' so that this stuf cannot be inadvertently overridden.  We set the Protected
' flag, which cannot be undone except by deleting the object and creating a new
' one.  Protection involves not being able to change any significant property
' values via any method except for Load.  The protection only works if there is
' a valid User Id set.
Private m_bIsProtected As Boolean

'##ModelId=438465600384
Public Property Get LanguageCode() As String
   Let LanguageCode = mLanguageCode
End Property

'##ModelId=43846560023A
Public Property Let LanguageCode(ByVal Value As String)
    Let mLanguageCode = Value
End Property

Public Property Get Manager() As Boolean
   Let Manager = mManager
End Property

Public Property Let Manager(ByVal Value As Boolean)
   mManager = Value
End Property

'##ModelId=43846560015E
Public Property Get FloatAmount() As Double
   Let FloatAmount = mFloatAmount
End Property

'##ModelId=438465600014
Public Property Let FloatAmount(ByVal Value As Double)
    Let mFloatAmount = Value
End Property

'##ModelId=4384655F035C
Public Property Get TillReceiptName() As String
   Let TillReceiptName = mTillReceiptName
End Property

'##ModelId=4384655F0212
Public Property Let TillReceiptName(ByVal Value As String)
    Let mTillReceiptName = Value
End Property

'##ModelId=4384655F0136
Public Property Get DeletedOutlet() As String
   Let DeletedOutlet = mDeletedOutlet
End Property

'##ModelId=4384655E032A
Public Property Get DeletedBy() As String
   Let DeletedBy = mDeletedBy
End Property

'##ModelId=4384655E0172
Public Property Get TimeDeleted() As String
   Let TimeDeleted = mTimeDeleted
End Property

'##ModelId=4384655D0370
Public Property Get DateDeleted() As Date
   Let DateDeleted = mDateDeleted
End Property

Public Function RecordAsDeleted(ByVal strUserID As String, strDeleteOutletID As String) As Boolean

Dim oRow     As IRow 'list of fields to save
Dim blnSaved As Boolean
    
    If LenB(mEmployeeID) = 0 Then Exit Function
    
    mDateDeleted = Now()
    mDeleted = True
    mTimeDeleted = Format$(Now, "HHNNSS")
    mDeletedBy = strUserID
    mDeletedOutlet = strDeleteOutletID
    
    ' and persist this new value in the database
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_USER_EmployeeID))
    Call oRow.Add(GetField(FID_USER_Deleted))
    Call oRow.Add(GetField(FID_USER_DateDeleted))
    Call oRow.Add(GetField(FID_USER_TimeDeleted))
    Call oRow.Add(GetField(FID_USER_DeletedBy))
    Call oRow.Add(GetField(FID_USER_DeletedOutlet))
       
    blnSaved = m_oSession.Database.SavePartialBo(Me, oRow)
    Debug.Assert blnSaved
    Set oRow = Nothing
    
    RecordAsDeleted = blnSaved

End Function

'##ModelId=4384655D0186
Public Property Get Deleted() As Boolean
   Let Deleted = mDeleted
End Property

'##ModelId=4384655C030C
Public Property Get SecurityLevels(Index As Variant) As String
    Let SecurityLevels = mSecurityLevels(Index)
End Property

'##ModelId=4384655C0190
Public Property Let SecurityLevels(Index As Variant, ByVal Value As String)
    If Not m_bIsProtected Then Let mSecurityLevels(Index) = Value
End Property

'##ModelId=4384655C0078
Public Property Get GroupLevels(Index As Variant) As Boolean
    Let GroupLevels = mGroupLevels(Index)
End Property

'##ModelId=4384655B02E4
Public Property Let GroupLevels(Index As Variant, ByVal Value As Boolean)
    If Not m_bIsProtected Then Let mGroupLevels(Index) = Value
End Property

'##ModelId=4384655B0276
Public Property Get CurrentOutlet() As String
   Let CurrentOutlet = mCurrentOutlet
End Property

'##ModelId=4384655B015E
Public Property Let CurrentOutlet(ByVal Value As String)
    Let mCurrentOutlet = Value
End Property

'##ModelId=4384655B00BE
Public Property Get AuthorisationValidTill() As Date
   Let AuthorisationValidTill = mAuthorisationValidTill
End Property

'##ModelId=4384655A03CA
Public Property Let AuthorisationValidTill(ByVal Value As Date)
    Let mAuthorisationValidTill = Value
End Property

'##ModelId=4384655A0320
Public Property Get AuthorisationCode() As String
   Let AuthorisationCode = mAuthorisationCode
End Property

'##ModelId=4384655A0244
Public Property Let AuthorisationCode(ByVal Value As String)
    Let mAuthorisationCode = Value
End Property

'##ModelId=4384655A01D6
Public Property Get Supervisor() As Boolean
   Let Supervisor = mSupervisor
End Property

'##ModelId=4384655A00FA
Public Property Let Supervisor(ByVal Value As Boolean)
    Let mSupervisor = Value
End Property

'##ModelId=4384655A008C
Public Property Get PasswordValidTill() As Date
   Let PasswordValidTill = mPasswordValidTill
End Property

'##ModelId=4384655903A2
Public Property Let PasswordValidTill(ByVal Value As Date)
    Let mPasswordValidTill = Value
End Property

'##ModelId=438465590334
Public Property Get Password() As String
   Let Password = mPassword
End Property

'##ModelId=438465590258
Public Property Let Password(ByVal Value As String)
    If Not m_bIsProtected Then Let mPassword = Value
End Property

'##ModelId=4384655900D2
Public Property Get Position() As String
   Let Position = mPosition
End Property

'##ModelId=438465590032
Public Property Let Position(ByVal Value As String)
    Let mPosition = Value
End Property
'##ModelId=4384655900D2
Public Property Get PayrollNumber() As String
   Let PayrollNumber = mPayrollNumber
End Property

Public Property Let PayrollNumber(ByVal Value As String)
    Let mPayrollNumber = Value
End Property

'##ModelId=438465580370
Public Property Get Initials() As String
   Let Initials = mInitials
End Property

'##ModelId=438465580302
Public Property Let Initials(ByVal Value As String)
    Let mInitials = Value
End Property

'##ModelId=438465580294
Public Property Get FullName() As String
   Let FullName = mFullName
End Property

'##ModelId=4384655801F4
Public Property Let FullName(ByVal Value As String)
    Let mFullName = Value
End Property

'##ModelId=4384655801B8
Public Property Get EmployeeID() As String
   Let EmployeeID = mEmployeeID
End Property

'##ModelId=438465580118
Public Property Let EmployeeID(ByVal Value As String)
    If Not m_bIsProtected Then Let mEmployeeID = Value
End Property

Public Function Protect() As Boolean
    If LenB(EmployeeID) <> 0 Then
        m_bIsProtected = True
        Protect = True
    End If
End Function
Public Function IsProtected() As Boolean
    IsProtected = m_bIsProtected
End Function
Public Function UpdateLastTillID() As Boolean

Dim oRow    As IRow
Dim oGField As IField
Dim blnRet  As Boolean

    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_USER_EmployeeID))
    
    mCurrentOutlet = m_oSession.CurrentEnterprise.IEnterprise_WorkstationID
    Set oGField = GetField(FID_USER_CurrentOutlet)
    ' and add to the row
    Call oRow.Add(oGField)
    
    blnRet = m_oSession.Database.SavePartialBo(Me, oRow)
    Set oRow = Nothing
    UpdateLastTillID = blnRet

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 14/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_USER_EmployeeID):         mEmployeeID = oField.ValueAsVariant
            Case (FID_USER_FullName):           mFullName = oField.ValueAsVariant
            Case (FID_USER_Initials):           mInitials = oField.ValueAsVariant
            Case (FID_USER_Position):           mPosition = oField.ValueAsVariant
            Case (FID_USER_PayrollNumber):      mPayrollNumber = oField.ValueAsVariant
            Case (FID_USER_Password):           mPassword = oField.ValueAsVariant
            Case (FID_USER_PasswordValidTill):  mPasswordValidTill = oField.ValueAsVariant
            Case (FID_USER_Supervisor):         mSupervisor = oField.ValueAsVariant
            Case (FID_USER_AuthorisationCode):  mAuthorisationCode = oField.ValueAsVariant
            Case (FID_USER_AuthorisationValidTill): mAuthorisationValidTill = oField.ValueAsVariant
            Case (FID_USER_CurrentOutlet):      mCurrentOutlet = oField.ValueAsVariant
            Case (FID_USER_GroupLevels):        Call LoadBooleanArray(mGroupLevels, oField, m_oSession)
            Case (FID_USER_SecurityLevels):     Call LoadStringArray(mSecurityLevels, oField, m_oSession)
            Case (FID_USER_Deleted):            mDeleted = oField.ValueAsVariant
            Case (FID_USER_DateDeleted):        mDateDeleted = oField.ValueAsVariant
            Case (FID_USER_TimeDeleted):        mTimeDeleted = oField.ValueAsVariant
            Case (FID_USER_DeletedBy):          mDeletedBy = oField.ValueAsVariant
            Case (FID_USER_DeletedOutlet):      mDeletedOutlet = oField.ValueAsVariant
            Case (FID_USER_TillReceiptName):    mTillReceiptName = oField.ValueAsVariant
            Case (FID_USER_FloatAmount):        mFloatAmount = oField.ValueAsVariant
            Case (FID_USER_LanguageCode):       mLanguageCode = oField.ValueAsVariant
            Case (FID_USER_Manager):            mManager = oField.ValueAsVariant
            
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow



Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "User " & mEmployeeID

End Property
Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function


Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_USER

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property
Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cUser

End Function


Public Function IBo_Load() As Boolean
    
    IBo_Load = Load

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property


Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    
    Select Case (lFieldID)
        Case (FID_USER_EmployeeID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mEmployeeID
        Case (FID_USER_FullName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mFullName
        Case (FID_USER_Initials):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mInitials
        Case (FID_USER_Position):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPosition
        Case (FID_USER_PayrollNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPayrollNumber
        Case (FID_USER_Password):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPassword
        Case (FID_USER_PasswordValidTill):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mPasswordValidTill
        Case (FID_USER_Supervisor):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mSupervisor
        Case (FID_USER_AuthorisationCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAuthorisationCode
        Case (FID_USER_AuthorisationValidTill):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mAuthorisationValidTill
        Case (FID_USER_CurrentOutlet):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCurrentOutlet
        Case (FID_USER_GroupLevels):    Set GetField = SaveBooleanArray(mGroupLevels, m_oSession)
        Case (FID_USER_SecurityLevels): Set GetField = SaveStringArray(mSecurityLevels, m_oSession)
        Case (FID_USER_Deleted):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mDeleted
        Case (FID_USER_DateDeleted):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDateDeleted
        Case (FID_USER_TimeDeleted):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTimeDeleted
        Case (FID_USER_DeletedBy):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDeletedBy
        Case (FID_USER_DeletedOutlet):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDeletedOutlet
        Case (FID_USER_TillReceiptName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTillReceiptName
        Case (FID_USER_FloatAmount):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mFloatAmount
        Case (FID_USER_LanguageCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLanguageCode
        Case (FID_USER_Manager):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mManager
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
       
End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_USER, FID_USER_END_OF_STATIC, m_oSession)

End Function

Public Function Load(Optional lId As Long) As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then
        Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
        If LenB(mEmployeeID) <> 0 Then Call moRowSel.AddSelection(CMP_EQUAL, GetField(FID_USER_EmployeeID))
    End If
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function



Private Function Initialise(oSession As ISession) As cUser
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
Dim lngNewNo As Long
    
    Save = False
    
    If IsValid() Then
        If LenB(mCurrentOutlet) = 0 Then mCurrentOutlet = "00"
        If LenB(mTimeDeleted) = 0 Then mTimeDeleted = "000000"
        If LenB(mDeletedBy) = 0 Then mDeletedBy = "000"
        If LenB(mDeletedOutlet) = 0 Then mDeletedOutlet = "00"
        If LenB(mTillReceiptName) = 0 Then mTillReceiptName = mFullName
        If (eSave = SaveTypeIfNew) And (Val(mEmployeeID) = 0) Then
            lngNewNo = m_oSession.Database.GetAggregateValue(AGG_MAX, GetField(FID_USER_EmployeeID), Nothing)
            mEmployeeID = Format$(lngNewNo + 1, "000")
        End If
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
        If mblnUpdateCashier Then
            If (moCashier Is Nothing) = True Then 'ensure that Cashier BO has been initialised
                Set moCashier = m_oSession.Database.CreateBusinessObject(CLASSID_CASHIER)
                If eSave = SaveTypeIfNew Then 'if new User then create Cashier Record
                    moCashier.CashierNumber = mEmployeeID
                    moCashier.IBo_SaveIfNew
                Else
                    Call moCashier.AddLoadFilter(CMP_EQUAL, FID_CASHIER_CashierNumber, mEmployeeID)
                    Call moCashier.LoadMatches
                End If
            End If
            Call moCashier.UpdateUserInfo(mTillReceiptName, mPosition, mPassword, mSupervisor, _
                        mDeleted)
            mblnUpdateCashier = False
        End If
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cUser
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_USER * &H10000) + 1 To FID_USER_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function
'<CACH>****************************************************************************************
'* Function:  Boolean IBo_SetLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_SetLoadField(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_SetLoadField"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_SetLoadField = True

End Function 'IBo_SetLoadField



'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_USER_END_OF_STATIC

End Function

Public Function SetPassword(strNewPassword As String) As Boolean

Dim oRSelector As IRowSelector
Dim oRow       As IRow
Dim oGField    As IField
Dim oSysOpt    As cSystemOptions
    
    SetPassword = False
    
    Set oSysOpt = m_oSession.Database.CreateBusinessObject(CLASSID_SYSTEMOPTIONS)
    Call oSysOpt.IBo_AddLoadField(FID_SYSTEMOPTIONS_PasswordValidFor)
    Call oSysOpt.IBo_LoadMatches
    
    ' and persist this new value in the database
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_USER_EmployeeID))
    
    mPassword = strNewPassword
    If LenB(strNewPassword) <> 0 Then mPasswordValidTill = DateAdd("d", oSysOpt.PasswordValidFor, Now())
    Set oGField = GetField(FID_USER_Password)
    Call oRow.Add(oGField)
    
    Set oGField = GetField(FID_USER_PasswordValidTill)
    ' and add to the row
    Call oRow.Add(oGField)
    
    SetPassword = m_oSession.Database.SavePartialBo(Me, oRow)
    Set oRow = Nothing

    If (moCashier Is Nothing) = True Then 'ensure that Cashier BO has been initialised
        Set moCashier = m_oSession.Database.CreateBusinessObject(CLASSID_CASHIER)
        Call moCashier.IBo_AddLoadFilter(CMP_EQUAL, FID_CASHIER_CashierNumber, mEmployeeID)
        Call moCashier.IBo_LoadMatches
    End If
    Call moCashier.SetPassword(mEmployeeID, mPassword)

End Function 'SetPassword

Public Function ClearPassword() As Boolean

Dim oRSelector As IRowSelector
Dim oRow       As IRow
Dim oGField    As IField
Dim oSysOpt    As cSystemOptions
    
    ClearPassword = False
    
    Set oSysOpt = m_oSession.Database.CreateBusinessObject(CLASSID_SYSTEMOPTIONS)
    Call oSysOpt.IBo_AddLoadField(FID_SYSTEMOPTIONS_PasswordValidFor)
    Call oSysOpt.IBo_LoadMatches
    
    ' and persist this new value in the database
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_USER_EmployeeID))
    
    'Clear password and set expired date
    mPassword = vbNullString
    mPasswordValidTill = DateAdd("d", "-1", Now())
    Set oGField = GetField(FID_USER_Password)
    Call oRow.Add(oGField)
    
    Set oGField = GetField(FID_USER_PasswordValidTill)
    ' and add to the row
    Call oRow.Add(oGField)
    
    ClearPassword = m_oSession.Database.SavePartialBo(Me, oRow)
    Set oRow = Nothing

End Function 'ClearPassword

Public Function SetAuthorisationCode(strNewAuthorityCode As String) As Boolean

Dim oRSelector As IRowSelector
Dim oRow       As IRow
Dim oGField    As IField
Dim oSysOpt    As cSystemOptions
    
    SetAuthorisationCode = False
    
    Set oSysOpt = m_oSession.Database.CreateBusinessObject(CLASSID_SYSTEMOPTIONS)
    Call oSysOpt.IBo_AddLoadField(FID_SYSTEMOPTIONS_AuthorisationValidFor)
    Call oSysOpt.IBo_LoadMatches
    
    ' and persist this new value in the database
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_USER_EmployeeID))
    
    mAuthorisationCode = strNewAuthorityCode
    If LenB(strNewAuthorityCode) <> 0 Then mAuthorisationValidTill = DateAdd("d", oSysOpt.AuthorisationValidFor, Now())
    Set oGField = GetField(FID_USER_AuthorisationCode)
    Call oRow.Add(oGField)
    
    Set oGField = GetField(FID_USER_AuthorisationValidTill)
    ' and add to the row
    Call oRow.Add(oGField)
    
    SetAuthorisationCode = m_oSession.Database.SavePartialBo(Me, oRow)
    Set oRow = Nothing

End Function

Public Function SignedOn(ByVal strTillID As String, ByRef strTransactionNo As String) As Boolean

Dim oLog As Object

    Set oLog = m_oSession.Database.CreateBusinessObject(CLASSID_POSHEADER)
    Call oLog.SaveSignOn(strTillID, mEmployeeID)
    strTransactionNo = oLog.TransactionNo
    Set oLog = Nothing
    SignedOn = True

End Function

Public Function SignedOff(ByVal strTillID As String, ByRef strTransactionNo As String)

Dim oLog As Object

    Set oLog = m_oSession.Database.CreateBusinessObject(CLASSID_POSHEADER)
    Call oLog.SaveSignOff(strTillID, mEmployeeID)
    strTransactionNo = oLog.TransactionNo
    Set oLog = Nothing
    SignedOff = True

End Function


