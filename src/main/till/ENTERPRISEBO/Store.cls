VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cStore"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D4E447C0050"
'<CAMH>****************************************************************************************
'* Module : cStore
'* Date   : 27/09/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/EnterpriseBO/Store.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of a single Store's details
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 2/27/03 3:38p $
'* $Revision: 10 $
'* Versions:
'* 14/08/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cStore"

Implements IBo
Implements ISysBo

Private m_oSession As Session

'##ModelId=3D4E447C019A
Private mStoreNumber As String

'##ModelId=3D4E447C008C
Private mAddressLine1 As String

'##ModelId=3D4E447C00BE
Private mAddressLine2 As String

'##ModelId=3D4E447C00BF
Private mAddressLine3 As String

'##ModelId=3D4E447C00C0
Private mAddressLine4 As String

'##ModelId=3D4E447C00FA
Private mFaxNo As String

'##ModelId=3D4E447C00FB
Private mStoreCategory As String

'##ModelId=3D4E447C012C
Private mPhoneNo As String

'##ModelId=3D4E447C012D
Private mPostCode As String

'##ModelId=3D4E447C012E
Private mPriceBand As Long

'##ModelId=3D4E447C0168
Private mSOQDay As String

'##ModelId=3D4E447C019B
Private mTOPPS As String

'##ModelId=3D4E447C01D6
Private mWarehouse As String

Private mCountryCode As String 'Added 13/4/08 WIX1312 to hold stores country code configuration

Dim moRowSel As IRowSelector

Private moLoadRow As IRowSelector


'##ModelId=3D5A1495006E
Public Property Get Warehouse() As String
   Let Warehouse = mWarehouse
End Property

'##ModelId=3D5A1494033E
Public Property Let Warehouse(ByVal Value As String)
    Let mWarehouse = Value
End Property

'##ModelId=3D5A14940262
Public Property Get TOPPS() As String
   Let TOPPS = mTOPPS
End Property

'##ModelId=3D5A14940154
Public Property Let TOPPS(ByVal Value As String)
    Let mTOPPS = Value
End Property

'##ModelId=3D5A14940078
Public Property Get StoreNumber() As String
   Let StoreNumber = mStoreNumber
End Property

'##ModelId=3D5A14930352
Public Property Let StoreNumber(ByVal Value As String)
    Let mStoreNumber = Value
End Property

'##ModelId=3D5A14930276
Public Property Get SOQDay() As String
   Let SOQDay = mSOQDay
End Property

'##ModelId=3D5A1493015E
Public Property Let SOQDay(ByVal Value As String)
    Let mSOQDay = Value
End Property

'##ModelId=3D5A149300BE
Public Property Get PriceBand() As Long
   Let PriceBand = mPriceBand
End Property

'##ModelId=3D5A1492038E
Public Property Let PriceBand(ByVal Value As Long)
    Let mPriceBand = Value
End Property

'##ModelId=3D5A149202EE
Public Property Get PostCode() As String
   Let PostCode = mPostCode
End Property

'##ModelId=3D5A149201D6
Public Property Let PostCode(ByVal Value As String)
    Let mPostCode = Value
End Property

'##ModelId=3D5A14920136
Public Property Get PhoneNo() As String
   Let PhoneNo = mPhoneNo
End Property

'##ModelId=3D5A1492005A
Public Property Let PhoneNo(ByVal Value As String)
    Let mPhoneNo = Value
End Property

'##ModelId=3D5A14910398
Public Property Get StoreCategory() As String
   Let StoreCategory = mStoreCategory
End Property

'##ModelId=3D5A149102BC
Public Property Let StoreCategory(ByVal Value As String)
    Let mStoreCategory = Value
End Property

'##ModelId=3D5A1491021C
Public Property Get FaxNo() As String
   Let FaxNo = mFaxNo
End Property

'##ModelId=3D5A14910140
Public Property Let FaxNo(ByVal Value As String)
    Let mFaxNo = Value
End Property

'##ModelId=3D5A14910096
Public Property Get AddressLine4() As String
   Let AddressLine4 = mAddressLine4
End Property

'##ModelId=3D5A149003DE
Public Property Let AddressLine4(ByVal Value As String)
    Let mAddressLine4 = Value
End Property

'##ModelId=3D5A14900334
Public Property Get AddressLine3() As String
   Let AddressLine3 = mAddressLine3
End Property

'##ModelId=3D5A14900294
Public Property Let AddressLine3(ByVal Value As String)
    Let mAddressLine3 = Value
End Property

'##ModelId=3D5A149001EA
Public Property Get AddressLine2() As String
   Let AddressLine2 = mAddressLine2
End Property

'##ModelId=3D5A1490014A
Public Property Let AddressLine2(ByVal Value As String)
    Let mAddressLine2 = Value
End Property

'##ModelId=3D5A149000DC
Public Property Get AddressLine1() As String
   Let AddressLine1 = mAddressLine1
End Property


'##ModelId=3D5A1490003C
Public Property Let AddressLine1(ByVal Value As String)
    Let mAddressLine1 = Value
End Property

Public Property Get CountryCode() As String
    CountryCode = mCountryCode
End Property

Public Property Let CountryCode(ByVal Value As String)
    mCountryCode = Value
End Property

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 14/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_STORE_StoreNumber):   mStoreNumber = oField.ValueAsVariant
            Case (FID_STORE_AddressLine1):  mAddressLine1 = oField.ValueAsVariant
            Case (FID_STORE_AddressLine2):  mAddressLine2 = oField.ValueAsVariant
            Case (FID_STORE_AddressLine3):  mAddressLine3 = oField.ValueAsVariant
            Case (FID_STORE_AddressLine4):  mAddressLine4 = oField.ValueAsVariant
            Case (FID_STORE_PostCode):      mPostCode = oField.ValueAsVariant
            Case (FID_STORE_PhoneNo):       mPhoneNo = oField.ValueAsVariant
            Case (FID_STORE_FaxNo):         mFaxNo = oField.ValueAsVariant
            Case (FID_STORE_SOQDay):        mSOQDay = oField.ValueAsVariant
            Case (FID_STORE_PriceBand):     mPriceBand = oField.ValueAsVariant
            Case (FID_STORE_TOPPS):         mTOPPS = oField.ValueAsVariant
            Case (FID_STORE_Warehouse):     mWarehouse = oField.ValueAsVariant
            Case (FID_STORE_StoreCategory): mStoreCategory = oField.ValueAsVariant
            Case (FID_STORE_CountryCode):   mCountryCode = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow



Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Store " & mStoreNumber

End Property
Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function


Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_STORE

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property
Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cStore

End Function


Public Function IBo_Load() As Boolean
    
    IBo_Load = Load

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property


Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    
    Select Case (lFieldID)
        Case (FID_STORE_StoreNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mStoreNumber
        Case (FID_STORE_AddressLine1):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine1
        Case (FID_STORE_AddressLine2):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine2
        Case (FID_STORE_AddressLine3):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine3
        Case (FID_STORE_AddressLine4):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine4
        Case (FID_STORE_PostCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPostCode
        Case (FID_STORE_PhoneNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPhoneNo
        Case (FID_STORE_FaxNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mFaxNo
        Case (FID_STORE_SOQDay):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSOQDay
        Case (FID_STORE_PriceBand):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mPriceBand
        Case (FID_STORE_TOPPS):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTOPPS
        Case (FID_STORE_Warehouse):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mWarehouse
        Case (FID_STORE_StoreCategory):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mStoreCategory
        Case (FID_STORE_CountryCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCountryCode
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
       
End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_STORE, FID_STORE_END_OF_STATIC, m_oSession)

End Function

Public Function Load(Optional lId As Long) As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function



Private Function Initialise(oSession As ISession) As cStore
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database

Dim lngNewNo As Long
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        If eSave = SaveTypeIfNew Then
            lngNewNo = m_oSession.Database.GetAggregateValue(AGG_MAX, GetField(FID_STORE_StoreNumber), Nothing)
            mStoreNumber = Format$(lngNewNo + 1, "000")
        End If
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cStore
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_STORE * &H10000) + 1 To FID_STORE_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function
'<CACH>****************************************************************************************
'* Function:  Boolean IBo_SetLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_SetLoadField(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_SetLoadField"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_SetLoadField = True

End Function 'IBo_SetLoadField



'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_STORE_END_OF_STATIC

End Function




