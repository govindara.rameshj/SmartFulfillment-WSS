VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cNightLog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D5380C9037A"
'<CAMH>****************************************************************************************
'* Module: cNightLog
'* Date  : 14/08/02
'* Author: mauricem
'*$Archive: /Projects/OasysV2/VB/EnterpriseBO/NightLog.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of a single Night Log entry
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 6/10/03 9:53 $
'* $Revision: 11 $
'* Versions:
'* 14/08/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cNightLog"

Implements IBo
Implements ISysBo

Private m_oSession As Session

'##ModelId=3D5380F50186
Private mLogDate As Date

'##ModelId=3D5381080046
Private mSetNumber As String

'##ModelId=3D53810E0032
Private mTaskNumber As String

'##ModelId=3D53811201F4
Private mDescription As String

'##ModelId=3D538117003C
Private mProgramPath As String

'##ModelId=3D53811F0208
Private mDateStarted As Date

'##ModelId=3D538124015E
Private mTimeStarted As String

'##ModelId=3D5381280244
Private mDateEnded As Date

'##ModelId=3D53812C0104
Private mTimeEnded As String

'##ModelId=3D5381310398
Private mDateAborted As Date

'##ModelId=3D53813502F8
Private mTimeAborted As String

'##ModelId=3D53813B0398
Private mJobError As String

'##ModelId=3D5381410014
Private mErrorMessage As String

Dim moRowSel As IRowSelector

Private moLoadRow As IRowSelector

'##ModelId=3D5A14A2023A
Public Property Get ErrorMessage() As String
   Let ErrorMessage = mErrorMessage
End Property

'##ModelId=3D5A14A200F0
Public Property Let ErrorMessage(ByVal Value As String)
    Let mErrorMessage = Value
End Property

'##ModelId=3D5A14A20046
Public Property Get JobError() As String
   Let JobError = mJobError
End Property

'##ModelId=3D5A14A102E4
Public Property Let JobError(ByVal Value As String)
    Let mJobError = Value
End Property

'##ModelId=3D5A14A10244
Public Property Get TimeAborted() As String
   Let TimeAborted = mTimeAborted
End Property

'##ModelId=3D5A14A1012C
Public Property Let TimeAborted(ByVal Value As String)
    Let mTimeAborted = Value
End Property

'##ModelId=3D5A14A10050
Public Property Get DateAborted() As Date
   Let DateAborted = mDateAborted
End Property

'##ModelId=3D5A14A0032A
Public Property Let DateAborted(ByVal Value As Date)
    Let mDateAborted = Value
End Property

'##ModelId=3D5A14A00280
Public Property Get TimeEnded() As String
   Let TimeEnded = mTimeEnded
End Property

'##ModelId=3D5A14A00172
Public Property Let TimeEnded(ByVal Value As String)
    Let mTimeEnded = Value
End Property

'##ModelId=3D5A14A000C8
Public Property Get DateEnded() As Date
   Let DateEnded = mDateEnded
End Property

'##ModelId=3D5A149F03D4
Public Property Let DateEnded(ByVal Value As Date)
    Let mDateEnded = Value
End Property

'##ModelId=3D5A149F0334
Public Property Get TimeStarted() As String
   Let TimeStarted = mTimeStarted
End Property

'##ModelId=3D5A149F021C
Public Property Let TimeStarted(ByVal Value As String)
    Let mTimeStarted = Value
End Property

'##ModelId=3D5A149F01AE
Public Property Get DateStarted() As Date
   Let DateStarted = mDateStarted
End Property

'##ModelId=3D5A149F00A0
Public Property Let DateStarted(ByVal Value As Date)
    Let mDateStarted = Value
End Property

'##ModelId=3D5A149F0032
Public Property Get ProgramPath() As String
   Let ProgramPath = mProgramPath
End Property

'##ModelId=3D5A149E033E
Public Property Let ProgramPath(ByVal Value As String)
    Let mProgramPath = Value
End Property

'##ModelId=3D5A149E029E
Public Property Get Description() As String
   Let Description = mDescription
End Property

'##ModelId=3D5A149E01C2
Public Property Let Description(ByVal Value As String)
    Let mDescription = Value
End Property

'##ModelId=3D5A149E0154
Public Property Get TaskNumber() As String
   Let TaskNumber = mTaskNumber
End Property

'##ModelId=3D5A149E0078
Public Property Let TaskNumber(ByVal Value As String)
    Let mTaskNumber = Value
End Property

'##ModelId=3D5A149E000A
Public Property Get SetNumber() As String
   Let SetNumber = mSetNumber
End Property

'##ModelId=3D5A149D0348
Public Property Let SetNumber(ByVal Value As String)
    Let mSetNumber = Value
End Property

'##ModelId=3D5A149D02DA
Public Property Get LogDate() As Date
   Let LogDate = mLogDate
End Property

'##ModelId=3D5A149D01FE
Public Property Let LogDate(ByVal Value As Date)
    Let mLogDate = Value
End Property

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 14/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
'##ModelId=3D749F8A038E
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_NIGHTLOG_LogDate):     mLogDate = oField.ValueAsVariant
            Case (FID_NIGHTLOG_SetNumber):   mSetNumber = oField.ValueAsVariant
            Case (FID_NIGHTLOG_TaskNumber):  mTaskNumber = oField.ValueAsVariant
            Case (FID_NIGHTLOG_Description): mDescription = oField.ValueAsVariant
            Case (FID_NIGHTLOG_ProgramPath): mProgramPath = oField.ValueAsVariant
            Case (FID_NIGHTLOG_DateStarted): mDateStarted = oField.ValueAsVariant
            Case (FID_NIGHTLOG_TimeStarted): mTimeStarted = oField.ValueAsVariant
            Case (FID_NIGHTLOG_DateEnded):   mDateEnded = oField.ValueAsVariant
            Case (FID_NIGHTLOG_TimeEnded):   mTimeStarted = oField.ValueAsVariant
            Case (FID_NIGHTLOG_DateAborted): mDateAborted = oField.ValueAsVariant
            Case (FID_NIGHTLOG_TimeAborted): mTimeAborted = oField.ValueAsVariant
            Case (FID_NIGHTLOG_JobError):    mJobError = oField.ValueAsVariant
            Case (FID_NIGHTLOG_ErrorMessage): mErrorMessage = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow



Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "NightLog " & mLogDate & " Set Number - " & mSetNumber

End Property
Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function


Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_NIGHTLOG

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property
Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cNightLog

End Function

Public Function IBo_Load() As Boolean
    
    IBo_Load = Load

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property


Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    
    Select Case (lFieldID)
        Case (FID_NIGHTLOG_LogDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mLogDate
        Case (FID_NIGHTLOG_SetNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSetNumber
        Case (FID_NIGHTLOG_TaskNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTaskNumber
        Case (FID_NIGHTLOG_Description):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDescription
        Case (FID_NIGHTLOG_ProgramPath):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mProgramPath
        Case (FID_NIGHTLOG_DateStarted):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDateStarted
        Case (FID_NIGHTLOG_TimeStarted):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTimeStarted
        Case (FID_NIGHTLOG_DateEnded):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDateEnded
        Case (FID_NIGHTLOG_TimeEnded):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTimeEnded
        Case (FID_NIGHTLOG_DateAborted):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDateAborted
        Case (FID_NIGHTLOG_TimeAborted):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTimeAborted
        Case (FID_NIGHTLOG_JobError):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mJobError
        Case (FID_NIGHTLOG_ErrorMessage):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mErrorMessage
            Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
    Exit Function
    
End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_NIGHTLOG, FID_NIGHTLOG_END_OF_STATIC, m_oSession)

End Function

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function


Private Function Initialise(oSession As ISession) As cNightLog
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cNightLog
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_NIGHTLOG * &H10000) + 1 To FID_NIGHTLOG_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean IBo_SetLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_SetLoadField(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_SetLoadField"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_SetLoadField = True

End Function 'IBo_SetLoadField



'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_NIGHTLOG_END_OF_STATIC

End Function

