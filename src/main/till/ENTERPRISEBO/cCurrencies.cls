VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cCurrencies"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : Currencies
'* Date   : 23/10/06
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/EnterpriseBO/cCurrencies.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: mauricem $
'* $Date: 23/10/06 9:24 $
'* $Revision: 1 $
'* Versions:
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "cCurrencies"


' Private Storage for Properties
Private mCurrencyCode As String '
Private mCurrDescription As String '
Private mDaysValidFor As Long
Private mDeleted As Boolean
Private mLargestDenom As Currency '
Private mMinCashDenom As Currency '
Private mAmountInSafe As Currency
Private mTenderType(20) As Boolean
Private mEuroLinked As Boolean

Private mCurrencySymbol As String
Private mPrintSymbol As Long
Private mCurrencyActiveDate As Date
Private mCurrencyInactiveDate As Date
Private mSystemDefaultCurrency As Boolean
Private mMaxAccepted As Long
Private mDenomValues(20) As Double
Private mDenomDesc(20) As String
Private mDenomDispSeq(20) As Long

' Standard IBo private storage
Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'Properties
Public Property Get CurrencyCode() As String
    CurrencyCode = mCurrencyCode
End Property

Public Property Let CurrencyCode(ByVal Value As String)
    mCurrencyCode = Value
End Property

Public Property Get CurrDescription() As String
    CurrDescription = mCurrDescription
End Property

Public Property Let CurrDescription(ByVal Value As String)
    mCurrDescription = Value
End Property

Public Property Get LargestDenom() As Currency
    LargestDenom = mLargestDenom
End Property

Public Property Let LargestDenom(ByVal Value As Currency)
    mLargestDenom = Value
End Property

Public Property Get MinCashDenom() As Currency
    MinCashDenom = mMinCashDenom
End Property

Public Property Let MinCashDenom(ByVal Value As Currency)
    mMinCashDenom = Value
End Property

Public Property Get AmountInSafe() As Currency
    AmountInSafe = mAmountInSafe
End Property

Public Property Let AmountInSafe(ByVal Value As Currency)
    mAmountInSafe = Value
End Property

Public Property Get TenderType(Index As Variant) As Long
   Let TenderType = mTenderType(Index)
End Property

Public Property Let TenderType(Index As Variant, ByVal Value As Long)
    Let mTenderType(Index) = Value
End Property

Public Property Get EuroLinked() As String
    EuroLinked = mEuroLinked
End Property

Public Property Let EuroLinked(ByVal Value As String)
    mEuroLinked = Value
End Property

Public Property Get CurrencySymbol() As String
    CurrencySymbol = mCurrencySymbol
End Property

Public Property Let CurrencySymbol(ByVal Value As String)
    mCurrencySymbol = Value
End Property

Public Property Get PrintSymbol() As Long
    PrintSymbol = mPrintSymbol
End Property

Public Property Let PrintSymbol(ByVal Value As Long)
    mPrintSymbol = Value
End Property

Public Property Get CurrencyActiveDate() As Date
    CurrencyActiveDate = mCurrencyActiveDate
End Property

Public Property Let CurrencyActiveDate(ByVal Value As Date)
    mCurrencyActiveDate = Value
End Property

Public Property Get CurrencyInactiveDate() As Date
    CurrencyInactiveDate = mCurrencyInactiveDate
End Property

Public Property Let CurrencyInactiveDate(ByVal Value As Date)
    mCurrencyInactiveDate = Value
End Property

Public Property Get SystemDefaultCurrency() As Boolean
    SystemDefaultCurrency = mSystemDefaultCurrency
End Property

Public Property Let SystemDefaultCurrency(ByVal Value As Boolean)
    mSystemDefaultCurrency = Value
End Property

Public Property Get MaxAccepted() As Long
    MaxAccepted = mMaxAccepted
End Property

Public Property Let MaxAccepted(ByVal Value As Long)
    mMaxAccepted = Value
End Property

Public Property Get DenomValues(Index As Variant) As Currency
   Let DenomValues = mDenomValues(Index)
End Property

Public Property Let DenomValues(Index As Variant, ByVal Value As Currency)
    Let mDenomValues(Index) = Value
End Property

Public Property Get DenomDesc(Index As Variant) As String
   Let DenomDesc = mDenomDesc(Index)
End Property

Public Property Let DenomDesc(Index As Variant, ByVal Value As String)
    Let mDenomDesc(Index) = Value
End Property

Public Property Get DenomDispSeq(Index As Variant) As Long
   Let DenomDispSeq = mDenomDispSeq(Index)
End Property

Public Property Let DenomDispSeq(Index As Variant, ByVal Value As Long)
    Let mDenomDispSeq(Index) = Value
End Property


Public Function IBo_Load() As Boolean
    IBo_Load = Load
End Function

Public Function Load() As Boolean
' Load up all properties from the database

Dim oView As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean
    IBo_Save = Save(SaveTypeAllCases)
End Function

Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Public Function IBo_SaveIfNew() As Boolean
    IBo_SaveIfNew = SaveIfNew
End Function

Public Function SaveIfNew() As Boolean
    
    SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean
    IBo_SaveIfExists = SaveIfExists
End Function

Public Function SaveIfExists() As Boolean

    SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean
    IBo_Delete = Delete
End Function

Public Function Delete() As Boolean

    Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean
    IBo_IsValid = IsValid()
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As IBo
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = New CRow
    For nFid = (CLASSID_CURRENCY * &H10000) + 1 To FID_CURRENCY_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()
    
    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = New CRowSelector
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cCurrencies

End Function


Public Function IBo_GetField(nFid As Long) As IField
    Set IBo_GetField = GetField(nFid)
End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)

        Case (FID_CURRENCY_CurrencyCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCurrencyCode
        Case (FID_CURRENCY_CurrDescription):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCurrDescription
        Case (FID_CURRENCY_DaysValidFor):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mDaysValidFor
        Case (FID_CURRENCY_Deleted):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mDeleted
        Case (FID_CURRENCY_LargestDenom):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mLargestDenom
        Case (FID_CURRENCY_MinCashDenom):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mMinCashDenom
        Case (FID_CURRENCY_AmountInSafe):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mAmountInSafe
        Case (FID_CURRENCY_TenderType): Set GetField = SaveBooleanArray(mTenderType, m_oSession)
        Case (FID_CURRENCY_EuroLinked):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mEuroLinked
        Case (FID_CURRENCY_CurrencySymbol):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCurrencySymbol
        Case (FID_CURRENCY_PrintSymbol):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mPrintSymbol
        Case (FID_CURRENCY_CurrencyActiveDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mCurrencyActiveDate
        Case (FID_CURRENCY_CurrencyInactiveDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mCurrencyInactiveDate
        Case (FID_CURRENCY_SystemDefaultCurrency):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mSystemDefaultCurrency
        Case (FID_CURRENCY_MaxAccepted):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mMaxAccepted
        Case (FID_CURRENCY_DenomValues):    Set GetField = SaveDoubleArray(mDenomValues, m_oSession)
        Case (FID_CURRENCY_DenomDesc):      Set GetField = SaveStringArray(mDenomDesc, m_oSession)
        Case (FID_CURRENCY_DenomDispSeq):   Set GetField = SaveLongArray(mDenomDispSeq, m_oSession)
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    Set IBo_GetSelectAllRow = GetSelectAllRow
End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_CURRENCY, FID_CURRENCY_END_OF_STATIC, m_oSession)

End Function

Private Function IBo_Initialise(oSession As ISession) As IBo
    Set IBo_Initialise = Initialise(oSession)
End Function

Private Function Initialise(oSession As ISession) As cCurrencies
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_CURRENCY

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = MODULE_NAME & ": " & mCurrencyCode & "-" & mCurrDescription

End Property

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    Call LoadFromRow(oRow)
End Function

Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_CURRENCY_CurrencyCode):     mCurrencyCode = oField.ValueAsVariant
            Case (FID_CURRENCY_CurrDescription):  mCurrDescription = oField.ValueAsVariant
            Case (FID_CURRENCY_DaysValidFor):     mDaysValidFor = oField.ValueAsVariant
            Case (FID_CURRENCY_Deleted):          mDeleted = oField.ValueAsVariant
            Case (FID_CURRENCY_LargestDenom):     mLargestDenom = oField.ValueAsVariant
            Case (FID_CURRENCY_MinCashDenom):     mMinCashDenom = oField.ValueAsVariant
            Case (FID_CURRENCY_AmountInSafe):     mAmountInSafe = oField.ValueAsVariant
            Case (FID_CURRENCY_TenderType):       Call LoadBooleanArray(mTenderType, oField, m_oSession)
            Case (FID_CURRENCY_EuroLinked):       mEuroLinked = oField.ValueAsVariant
            Case (FID_CURRENCY_CurrencySymbol):   mCurrencySymbol = oField.ValueAsVariant
            Case (FID_CURRENCY_PrintSymbol):      mPrintSymbol = oField.ValueAsVariant
            Case (FID_CURRENCY_CurrencyActiveDate):     mCurrencyActiveDate = oField.ValueAsVariant
            Case (FID_CURRENCY_CurrencyInactiveDate):   mCurrencyInactiveDate = oField.ValueAsVariant
            Case (FID_CURRENCY_SystemDefaultCurrency):  mSystemDefaultCurrency = oField.ValueAsVariant
            Case (FID_CURRENCY_MaxAccepted):            mMaxAccepted = oField.ValueAsVariant
            Case (FID_CURRENCY_DenomValues):     Call LoadDoubleArray(mDenomValues, oField, m_oSession)
            Case (FID_CURRENCY_DenomDesc):       Call LoadStringArray(mDenomDesc, oField, m_oSession)
            Case (FID_CURRENCY_DenomDispSeq):    Call LoadLongArray(mDenomDispSeq, oField, m_oSession)
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_CURRENCY_END_OF_STATIC

End Function

