VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cWorkStation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"4383ABA801CC"
'<CAMH>****************************************************************************************
'* Module : cWorkStation
'* Date   : 22/11/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/EnterpriseBO/cWorkStation.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of a single Work Station Configuration
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 6/10/03 9:53 $
'* $Revision: 4 $
'* Versions:
'* 22/11/02    mauricem
'*             Header added.
'* 25/06/07    Mike O'C
'*             WIX1240 - Updated to add New UserId field in CORHDR
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cWorkStation"

Implements IBo
Implements ISysBo

'##ModelId=4383ABEE00A0
Private mID As String

'##ModelId=4383ABF20118
Private mDescription As String

'##ModelId=4383ABFA02A8
Private mOutletFunction As String

'##ModelId=4383AC1C01CC
Private mActive As Boolean

'##ModelId=4383AC2200AA
Private mTenderGroups(9) As Boolean

'##ModelId=4383AC2F001E
Private mGroupLevels(99) As Boolean

Private mDateLastLoggedOn As Date

Private mBarcodeBroken As Boolean

Private mUseTouchScreen As Boolean

Private mQuoteTill As Boolean

Private mUseCashDrawer As Boolean

Private mUseEFT As Boolean

Private mPrinterType As String

Private m_oSession As Session

Dim moRowSel As IRowSelector

Private moLoadRow As IRowSelector


'##ModelId=4383AD7603DE
Public Property Get GroupLevels(Index As Variant) As Boolean
    Let GroupLevels = mGroupLevels(Index)
End Property

'##ModelId=4383AD760302
Public Property Let GroupLevels(Index As Variant, ByVal Value As Boolean)
    Let mGroupLevels(Index) = Value
End Property

'##ModelId=4383AD760294
Public Property Get TenderGroups(Index As Variant) As Boolean
    Let TenderGroups = mTenderGroups(Index)
End Property

'##ModelId=4383AD7601B8
Public Property Let TenderGroups(Index As Variant, ByVal Value As Boolean)
    Let mTenderGroups(Index) = Value
End Property

'##ModelId=4383AD76014A
Public Property Get Active() As Boolean
    Active = mActive
End Property

'##ModelId=4383AD7600DC
Public Property Let Active(ByVal Value As Boolean)
    Let mActive = Value
End Property

'##ModelId=4383AD7600A0
Public Property Get OutletFunction() As String
    Let OutletFunction = mOutletFunction
End Property

'##ModelId=4383AD760032
Public Property Let OutletFunction(ByVal Value As String)
    Let mOutletFunction = Value
End Property

'##ModelId=4383AD760000
Public Property Get Description() As String
    Let Description = mDescription
End Property

'##ModelId=4383AD7503AC
Public Property Let Description(ByVal Value As String)
    Let mDescription = Value
End Property

'##ModelId=4383AD75037A
Public Property Get ID() As String
    Let ID = mID
End Property

'##ModelId=4383AD75030C
Public Property Let ID(ByVal Value As String)
    Let mID = Value
End Property

Public Property Get DateLastLoggedOn() As Date
    Let DateLastLoggedOn = mDateLastLoggedOn
End Property

Public Property Let DateLastLoggedOn(ByVal Value As Date)
    Let mDateLastLoggedOn = Value
End Property

Public Property Get BarcodeBroken() As Boolean
    BarcodeBroken = mBarcodeBroken
End Property

Public Property Let BarcodeBroken(ByVal Value As Boolean)
    Let mBarcodeBroken = Value
End Property

Public Property Get UseTouchScreen() As Boolean
    UseTouchScreen = mUseTouchScreen
End Property

Public Property Let UseTouchScreen(ByVal Value As Boolean)
    Let mUseTouchScreen = Value
End Property

Public Property Get QuoteTill() As Boolean
    QuoteTill = mQuoteTill
End Property

Public Property Let QuoteTill(ByVal Value As Boolean)
    Let mQuoteTill = Value
End Property

Public Property Get UseCashDrawer() As Boolean
    UseCashDrawer = mUseCashDrawer
End Property

Public Property Let UseCashDrawer(ByVal Value As Boolean)
    Let mUseCashDrawer = Value
End Property

Public Property Get UseEFT() As Boolean
    UseEFT = mUseEFT
End Property

Public Property Let UseEFT(ByVal Value As Boolean)
    Let mUseEFT = Value
End Property

Public Property Get PrinterType() As String
    PrinterType = mPrinterType
End Property

Public Property Let PrinterType(ByVal Value As String)
    Let mPrinterType = Value
End Property

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 14/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_WORKSTATIONCONFIG_id):              mID = oField.ValueAsVariant
            Case (FID_WORKSTATIONCONFIG_Description):     mDescription = oField.ValueAsVariant
            Case (FID_WORKSTATIONCONFIG_OutletFunction):  mOutletFunction = oField.ValueAsVariant
            Case (FID_WORKSTATIONCONFIG_Active):          mActive = oField.ValueAsVariant
            Case (FID_WORKSTATIONCONFIG_TenderGroups):    Call LoadBooleanArray(mTenderGroups, oField, m_oSession)
            Case (FID_WORKSTATIONCONFIG_GroupLevels):     Call LoadBooleanArray(mGroupLevels, oField, m_oSession)
            Case (FID_WORKSTATIONCONFIG_DateLastLoggedON): mDateLastLoggedOn = oField.ValueAsVariant
            Case (FID_WORKSTATIONCONFIG_BarcodeBroken):   mBarcodeBroken = (oField.ValueAsVariant = "Y")
            Case (FID_WORKSTATIONCONFIG_UseTouchScreen):  mUseTouchScreen = (oField.ValueAsVariant = "Y")
            Case (FID_WORKSTATIONCONFIG_QuoteTill):       mQuoteTill = (oField.ValueAsVariant = "Y")
            Case (FID_WORKSTATIONCONFIG_UseCashDrawer):   mUseCashDrawer = (oField.ValueAsVariant = "Y")
            Case (FID_WORKSTATIONCONFIG_UseEFT):          mUseEFT = (oField.ValueAsVariant = "Y")
            Case (FID_WORKSTATIONCONFIG_PrinterType):     mPrinterType = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow



Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Work Station Config " & mID

End Property
Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function


Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_WORKSTATIONCONFIG

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property
Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cWorkStation

End Function


Public Function IBo_Load() As Boolean
    
    IBo_Load = Load

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property


Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    
    Select Case (lFieldID)
        Case (FID_WORKSTATIONCONFIG_id):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mID
        Case (FID_WORKSTATIONCONFIG_Description):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDescription
        Case (FID_WORKSTATIONCONFIG_OutletFunction):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mOutletFunction
        Case (FID_WORKSTATIONCONFIG_Active):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mActive
        Case (FID_WORKSTATIONCONFIG_TenderGroups): Set GetField = SaveBooleanArray(mTenderGroups, m_oSession)
        Case (FID_WORKSTATIONCONFIG_GroupLevels): Set GetField = SaveBooleanArray(mGroupLevels, m_oSession)
        Case (FID_WORKSTATIONCONFIG_DateLastLoggedON):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDateLastLoggedOn
        Case (FID_WORKSTATIONCONFIG_BarcodeBroken):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = IIf(mBarcodeBroken = True, "Y", "N")
        Case (FID_WORKSTATIONCONFIG_UseTouchScreen):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = IIf(mUseTouchScreen = True, "Y", "N")
        Case (FID_WORKSTATIONCONFIG_QuoteTill):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = IIf(mQuoteTill = True, "Y", "N")
        Case (FID_WORKSTATIONCONFIG_UseCashDrawer):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = IIf(mUseCashDrawer = True, "Y", "N")
        Case (FID_WORKSTATIONCONFIG_UseEFT):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = IIf(mUseEFT = True, "Y", "N")
        Case (FID_WORKSTATIONCONFIG_PrinterType):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPrinterType
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
       
End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_WORKSTATIONCONFIG, FID_WORKSTATIONCONFIG_END_OF_STATIC, m_oSession)

End Function

Public Function Load(Optional lId As Long) As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function



Private Function Initialise(oSession As ISession) As cWorkStation
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
Dim lngNewNo As Long
    
    Save = False
    
    If IsValid() Then
        If (eSave = SaveTypeIfNew) And (Val(mID) = 0) Then
            lngNewNo = m_oSession.Database.GetAggregateValue(AGG_MAX, GetField(FID_WORKSTATIONCONFIG_id), Nothing)
            mID = Format$(lngNewNo + 1, "00")
        End If
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cWorkStation
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_WORKSTATIONCONFIG * &H10000) + 1 To FID_WORKSTATIONCONFIG_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function
'<CACH>****************************************************************************************
'* Function:  Boolean IBo_SetLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_SetLoadField(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_SetLoadField"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_SetLoadField = True

End Function 'IBo_SetLoadField



'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_WORKSTATIONCONFIG_END_OF_STATIC

End Function

Public Function RecordWSAsLoggedIn(ByVal strWSID As String) As Boolean

Dim oRSelector As IRowSelector
Dim oRow       As IRow
Dim oGField    As IField
    
    mID = strWSID
    mActive = True
    mDateLastLoggedOn = Date
    
    ' and persist this new value in the database
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_WORKSTATIONCONFIG_id))
    Call oRow.Add(GetField(FID_WORKSTATIONCONFIG_Active))
    Call oRow.Add(GetField(FID_WORKSTATIONCONFIG_DateLastLoggedON))
    
    RecordWSAsLoggedIn = m_oSession.Database.SavePartialBo(Me, oRow)
    Set oRow = Nothing

End Function

Public Function RecordWSAsActive(ByVal strWSID As String) As Boolean

Dim oRSelector As IRowSelector
Dim oRow       As IRow
Dim oGField    As IField
    
    mID = strWSID
    mActive = True
    
    ' and persist this new value in the database
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_WORKSTATIONCONFIG_id))
    Call oRow.Add(GetField(FID_WORKSTATIONCONFIG_Active))
    
    RecordWSAsActive = m_oSession.Database.SavePartialBo(Me, oRow)
    Set oRow = Nothing

End Function

Public Function RecordWSAsLoggedOut(ByVal strWSID As String) As Boolean

Dim oRSelector As IRowSelector
Dim oRow       As IRow
Dim oGField    As IField
    
    mID = strWSID
    mActive = False
    
    ' and persist this new value in the database
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_WORKSTATIONCONFIG_id))
    Call oRow.Add(GetField(FID_WORKSTATIONCONFIG_Active))
    
    RecordWSAsLoggedOut = m_oSession.Database.SavePartialBo(Me, oRow)
    Set oRow = Nothing

End Function

Public Function RecordBarcodeBroken(ByVal strWSID As String, ByVal blnBarcodeBroken As Boolean) As Boolean

Dim oRSelector As IRowSelector
Dim oRow       As IRow
Dim oGField    As IField
    
    mID = strWSID
    mBarcodeBroken = blnBarcodeBroken
    
    ' and persist this new value in the database
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_WORKSTATIONCONFIG_id))
    Call oRow.Add(GetField(FID_WORKSTATIONCONFIG_BarcodeBroken))
    
    RecordBarcodeBroken = m_oSession.Database.SavePartialBo(Me, oRow)
    Set oRow = Nothing

End Function



