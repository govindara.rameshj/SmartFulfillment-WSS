VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cEDIFileControl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3E5CCA6D02BE"
'<CAMH>****************************************************************************************
'* Module : cEDIFileControl
'* Date   : 26/02/03
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/EnterpriseBO/cEDIFileControl.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 6/05/04 15:36 $ $Revision: 8 $
'* Versions:
'* 26/02/03    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cEDIFileControl"

Implements IBo
Implements ISysBo

'##ModelId=3E5CCA6D032A
Private mFileCode As String

'##ModelId=3E5CCA6D0366
Private mDescription As String

'##ModelId=3E5CCA6D0367
Private mLastSequenceNo As String

'##ModelId=3E5CCA6D0368
Private mDoneSequenceNo As String

'##ModelId=3E5CCA6D0369
Private mProcessingType As String

'##ModelId=3E5CCA6D0398
Private mEndToEndApplies As Boolean

Private moRowSel As CRowSelector

Private moLoadRow As CRowSelector

Private m_oSession As Session ' Object

Private moTCFMaster As cEDILogEntry 'Used for tracking TCF Master record when processing file

'##ModelId=3E5CCA6D03D4
Public Property Get EndToEndApplies() As Boolean
   Let EndToEndApplies = mEndToEndApplies
End Property

'##ModelId=3E5CCA6D0399
Public Property Let EndToEndApplies(ByVal Value As Boolean)
    Let mEndToEndApplies = Value
End Property

'##ModelId=3E5CCA6D03D7
Public Property Get ProcessingType() As String
   Let ProcessingType = mProcessingType
End Property

'##ModelId=3E5CCA6D03D5
Public Property Let ProcessingType(ByVal Value As String)
    Let mProcessingType = Value
End Property

'##ModelId=3E5CCA6E001F
Public Property Get DoneSequenceNo() As String
   Let DoneSequenceNo = mDoneSequenceNo
End Property

'##ModelId=3E5CCA6D03D8
Public Property Let DoneSequenceNo(ByVal Value As String)
    Let mDoneSequenceNo = Value
End Property

'##ModelId=3E5CCA6E005A
Public Property Get LastSequenceNo() As String
   Let LastSequenceNo = mLastSequenceNo
End Property

'##ModelId=3E5CCA6E0020
Public Property Let LastSequenceNo(ByVal Value As String)
    Let mLastSequenceNo = Value
End Property

'##ModelId=3E5CCA6E005D
Public Property Get Description() As String
   Let Description = mDescription
End Property

'##ModelId=3E5CCA6E005B
Public Property Let Description(ByVal Value As String)
    Let mDescription = Value
End Property

'##ModelId=3E5CCA6E008E
Public Property Get FileCode() As String
   Let FileCode = mFileCode
End Property


'##ModelId=3E5CCA6E008C
Public Property Let FileCode(ByVal Value As String)
    Let mFileCode = Value
End Property

Public Function GetEDITextStream(ByVal blnStoreToHeadOffice As Boolean, ByVal strShortCode As String, ByVal strFilePath As String, ByRef strSequenceNo As String) As TextStream

Const PROCEDURE_NAME = "GetEDITextStream"

Dim oFSO        As FileSystemObject
Dim oTS         As TextStream
Dim strFilename As String
Dim oRow        As IRow
Dim oGField     As IField

    Set oFSO = New FileSystemObject
    
    If blnStoreToHeadOffice = True Then
        strFilename = "SH" & strShortCode
    Else
        strFilename = "HS" & strShortCode
    End If
    
    If oFSO.FolderExists(strFilePath) = False Then Call Err.Raise(OASYS_ERR_INVALID_EDI_PATH, PROCEDURE_NAME, "Unable to locate EDI path - " & strFilePath)
    
    mFileCode = strFilename
    'Retrieve latest values
    Call IBo_AddLoadFilter(CMP_EQUAL, FID_EDIFILECONTROL_FileCode, strFilename)
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_EDIFILECONTROL_FileCode))
    
    Set oGField = GetField(FID_EDIFILECONTROL_DoneSequenceNo)
    ' and add to the row
    Call oRow.Add(oGField)
    
    Set oGField = GetField(FID_EDIFILECONTROL_LastSequenceNo)
    ' and add to the row
    Call oRow.Add(oGField)
    
    Call IBo_LoadMatches
    
    'if failed then create new file control record
    If (LenB(mDescription) = 0) And (Val(mLastSequenceNo) = 0) Then
        If blnStoreToHeadOffice = True Then
            mDescription = "Store -> H/O (" & strShortCode & ")"
        Else
            mDescription = "H/O -> Store (" & strShortCode & ")"
        End If
        mDoneSequenceNo = "0000"
        mEndToEndApplies = True
        mFileCode = strFilename
        mLastSequenceNo = "0000"
        mProcessingType = "U"
        Call IBo_SaveIfNew
    End If
    
    strSequenceNo = Val(DoneSequenceNo)
    
    While oTS Is Nothing
        'Move to next file in sequence
        strSequenceNo = Format$(Val(strSequenceNo) + 1, "0000")
        Set oTS = Nothing
        On Error Resume Next
        Set oTS = oFSO.CreateTextFile(strFilePath & "\" & strFilename & strSequenceNo & "." & m_oSession.CurrentEnterprise.IEnterprise_StoreNumber, False)
        If (Err.Number <> 0) And (Err.Number <> 58) Then 'if file exists then loop round to next file in sequence
            Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)
        End If
    Wend
    
    On Error GoTo 0
    If oTS Is Nothing Then Exit Function
    mDoneSequenceNo = strSequenceNo
    'Check that Control Counter relects new Counter record
    If Val(strSequenceNo) > Val(mLastSequenceNo) Then mLastSequenceNo = strSequenceNo
    Call m_oSession.Database.SavePartialBo(Me, oRow)
    
    Call DebugMsg(MODULE_NAME, "GetEDITextStream", endlDebug, "Saving TCF Mas" & strSequenceNo)
    'Create a new Control entry for the file
    Set moTCFMaster = m_oSession.Database.CreateBusinessObject(CLASSID_TCFMASTER)
    moTCFMaster.FileCode = mFileCode
    moTCFMaster.SequenceNumber = strSequenceNo
    moTCFMaster.SourceSequenceNumber = "000000"
    moTCFMaster.TransmissionDate = Now()
    moTCFMaster.TransmissionTime = Format$(Now(), "HHNNSS")
    moTCFMaster.DateReady = moTCFMaster.TransmissionDate
    moTCFMaster.TimeReady = moTCFMaster.TransmissionTime
    Call moTCFMaster.IBo_SaveIfNew
    
    Set GetEDITextStream = oTS

End Function
'opens an existing file for reading in
Public Function OpenEDITextStream(ByVal blnStoreToHeadOffice As Boolean, _
                                  ByVal strShortCode As String, _
                                  ByVal strFilePath As String, _
                                  ByRef strSequenceNo As String, _
                                  ByVal strStoreNumber As String) As TextStream

Const PROCEDURE_NAME = "OpenEDITextStream"

Dim oFSO        As FileSystemObject
Dim oFile       As File
Dim oTS         As TextStream
Dim strFilename As String
Dim oRow        As IRow
Dim oGField     As IField

    Set oFSO = New FileSystemObject
    
    'Compile file name from Direction and File Code
    If blnStoreToHeadOffice = True Then
        strFilename = "SH" & strShortCode
    Else
        strFilename = "HS" & strShortCode
    End If
    
    'Ensure TXIn/Out path exists
    If oFSO.FolderExists(strFilePath) = False Then Call Err.Raise(OASYS_ERR_INVALID_EDI_PATH, PROCEDURE_NAME, "Unable to locate EDI path - " & strFilename)
    
    mFileCode = strFilename
    
    Call IBo_ClearLoadFilter
    'Retrieve latest values for access to sequence number to process
    Call IBo_AddLoadFilter(CMP_EQUAL, FID_EDIFILECONTROL_FileCode, strFilename)
    
    Call IBo_LoadMatches
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_EDIFILECONTROL_FileCode))
    Call oRow.Add(GetField(FID_EDIFILECONTROL_DoneSequenceNo))
    
    'if failed then create new file control record
    If LenB(mDescription) = 0 Then
        If blnStoreToHeadOffice = True Then
            mDescription = "Store -> H/O (" & strShortCode & ")"
        Else
            mDescription = "H/O -> Store (" & strShortCode & ")"
        End If
        mDoneSequenceNo = "0000"
        mEndToEndApplies = True
        mFileCode = strFilename
        mLastSequenceNo = "0000"
        mProcessingType = "U"
        Call IBo_SaveIfNew 'Save new control record to DB
    End If
    
    strSequenceNo = Val(DoneSequenceNo)
    
    'Move to next file in sequence
    strSequenceNo = Format$(Val(strSequenceNo) + 1, "0000")
        
    'Attempt to open file, catch error if does not exists
    On Error Resume Next
    Set oTS = oFSO.OpenTextFile(strFilePath & "\" & strFilename & strSequenceNo & "." & strStoreNumber, ForReading, False)
    If (Err.Number <> 0) And (Err.Number <> 53) Then 'if error then pass error out
        Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description & "  " & strFilePath & "\" & strFilename & strSequenceNo & "." & strStoreNumber)
    End If
    
    'if no file opened then get out
    If oTS Is Nothing Then Exit Function
       
    Call DebugMsg(MODULE_NAME, "OpenEDITextStream", endlDebug, "Getting TCF Mas" & strSequenceNo)
    'Retrieve TCF log entry
    Set moTCFMaster = m_oSession.Database.CreateBusinessObject(CLASSID_TCFMASTER)
    Call moTCFMaster.IBo_AddLoadFilter(CMP_EQUAL, FID_TCFMASTER_FileCode, mFileCode)
    Call moTCFMaster.IBo_AddLoadFilter(CMP_EQUAL, FID_TCFMASTER_SequenceNumber, strSequenceNo)
    Call moTCFMaster.IBo_LoadMatches
    'if no entry then create a new Control entry for the file
    If (moTCFMaster.FileCode <> mFileCode) And (moTCFMaster.SequenceNumber <> strSequenceNo) Then
        Set oFile = oFSO.GetFile(strFilePath & "\" & strFilename & strSequenceNo & ".")
        moTCFMaster.FileCode = mFileCode
        moTCFMaster.SequenceNumber = strSequenceNo
        moTCFMaster.SourceSequenceNumber = "000000"
        moTCFMaster.TransmissionDate = oFile.DateCreated
        moTCFMaster.TransmissionTime = Format$(oFile.DateCreated, "HHNNSS")
        moTCFMaster.DateReady = Now()
        moTCFMaster.TimeReady = Format$(Now(), "HHNNSS")
        Set oFile = Nothing
        moTCFMaster.SentRecordCount = 0
        Call moTCFMaster.IBo_SaveIfNew
        'Check that Control Counter relects new Counter record
        If Val(strSequenceNo) > Val(mLastSequenceNo) Then
            mLastSequenceNo = strSequenceNo
            Call m_oSession.Database.SavePartialBo(Me, oRow)
        End If
    
    End If
        
    On Error GoTo 0
    'Pass the Text Stream for the open file out to the calling module
    Set OpenEDITextStream = oTS
    Call DebugMsg(MODULE_NAME, "OpenEDITextStream", endlDebug, strFilePath & "\" & strFilename & strSequenceNo)

End Function

Friend Sub UpdateLast(strFilename As String, strSequenceNo As String)

Dim oRow   As IRow

    Call IBo_ClearLoadField
    Call IBo_ClearLoadFilter
    Call IBo_AddLoadFilter(CMP_EQUAL, FID_EDIFILECONTROL_FileCode, strFilename)
    
    Call IBo_LoadMatches
    
    'Build up saving Row to update Done in TCFCTL table
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    If Val(mLastSequenceNo) < Val(strSequenceNo) Then mLastSequenceNo = Format$(Val(strSequenceNo), "0000")
    Call oRow.Add(GetField(FID_EDIFILECONTROL_FileCode))
    Call oRow.Add(GetField(FID_EDIFILECONTROL_LastSequenceNo))
    
    Call m_oSession.Database.SavePartialBo(Me, oRow)
       
End Sub

Public Sub MarkAsDone(ByVal blnStoreToHeadOffice As Boolean, _
                      ByVal strShortCode As String, _
                      ByVal strSequenceNo As String, _
                      ByVal strStoreNumber As String, _
                      ByVal lngNoRecords As Long)
    
Dim oRow        As IRow
Dim oGField     As IField
Dim strTXPath   As String
Dim strTXSave   As String
Dim oFSO        As FileSystemObject
    
    'start building file name and get path to data location and back-up folder
    If blnStoreToHeadOffice = True Then
        strShortCode = "SH" & strShortCode
        strTXPath = m_oSession.GetParameter(PRM_TXOUT_FOLDER)
        strTXSave = m_oSession.GetParameter(PRM_TXOUT_BACKUP_FOLDER)
    Else
        strShortCode = "HS" & strShortCode
        strTXPath = m_oSession.GetParameter(PRM_TXIN_FOLDER)
        strTXSave = m_oSession.GetParameter(PRM_TXIN_BACKUP_FOLDER)
    End If
    
    'Retrieve latest values
    Call IBo_AddLoadFilter(CMP_EQUAL, FID_EDIFILECONTROL_FileCode, strShortCode)
    Call IBo_LoadMatches
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_EDIFILECONTROL_FileCode))
    Call oRow.Add(GetField(FID_EDIFILECONTROL_DoneSequenceNo))
    
    'Move the Done file counter forward, ensuring that another process has not superceeded it
    If Val(strSequenceNo) > Val(mDoneSequenceNo) Then
        mDoneSequenceNo = strSequenceNo
        Call m_oSession.Database.SavePartialBo(Me, oRow)
    End If
    
    'Update Control File to indicate that file was processed successfully
    If moTCFMaster Is Nothing Then Set moTCFMaster = m_oSession.Database.CreateBusinessObject(CLASSID_TCFMASTER)
    
    Call moTCFMaster.RecordAsProcessed(mFileCode, strSequenceNo, lngNoRecords)
    Call DebugMsg(MODULE_NAME, "MarkAsDone", endlDebug, "Updating TCF Mas (" & mFileCode & strSequenceNo & ")")
    
    'after file has been processed, move file to back up folder
    strShortCode = "\" & strShortCode & strSequenceNo & "." & strStoreNumber
    Set oFSO = New FileSystemObject
    If blnStoreToHeadOffice = False Then
        Call DebugMsg(MODULE_NAME, "MarkAsDone", endlDebug, "Move-" & strTXPath & strShortCode)
        Call oFSO.MoveFile(strTXPath & strShortCode, strTXSave & strShortCode)
    End If

End Sub 'MarkAsDone


Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_EDIFILECONTROL * &H10000) + 1 To FID_EDIFILECONTROL_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cEDIFileControl

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_EDIFILECONTROL, FID_EDIFILECONTROL_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_EDIFILECONTROL_FileCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mFileCode
        Case (FID_EDIFILECONTROL_Description):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDescription
        Case (FID_EDIFILECONTROL_LastSequenceNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLastSequenceNo
        Case (FID_EDIFILECONTROL_DoneSequenceNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDoneSequenceNo
        Case (FID_EDIFILECONTROL_ProcessingType):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mProcessingType
        Case (FID_EDIFILECONTROL_EndToEndApplies):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mEndToEndApplies
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cEDIFileControl
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_EDIFILECONTROL

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "EDI File Control " & mFileCode

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_EDIFILECONTROL_FileCode):        mFileCode = oField.ValueAsVariant
            Case (FID_EDIFILECONTROL_Description):     mDescription = oField.ValueAsVariant
            Case (FID_EDIFILECONTROL_LastSequenceNo):  mLastSequenceNo = oField.ValueAsVariant
            Case (FID_EDIFILECONTROL_DoneSequenceNo):  mDoneSequenceNo = oField.ValueAsVariant
            Case (FID_EDIFILECONTROL_ProcessingType):  mProcessingType = oField.ValueAsVariant
            Case (FID_EDIFILECONTROL_EndToEndApplies): mEndToEndApplies = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_EDIFILECONTROL_END_OF_STATIC

End Function

Public Function Interface(Optional eInterfaceType As Long) As cEDIFileControl

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set Interface = oBO
        Case Else:
            Set Interface = Me
    End Select

End Function


