VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cActivityLog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3E2FC62F024E"
'<CAMH>****************************************************************************************
'* Module : cActivityLog
'* Date   : 23/01/03
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/EnterpriseBO/cActivityLog.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Damians $ $Date: 17/05/04 17:44 $ $Revision: 4 $
'* Versions:
'* 23/01/03    mauricem
'*             Header added.
'</CAMH>***************************************************************************************

Option Base 0
Option Explicit

Const MODULE_NAME As String = "cActivityLog"

Implements IBo
Implements ISysBo

'##ModelId=3E2FC6480352
Private mKey As Long

'##ModelId=3E2FC650017C
Private mSystemDate As Date

'##ModelId=3E2FC65A017C
Private mEmployeeID As String

'##ModelId=3E2FC6620122
Private mWorkstationID As String

'##ModelId=3E2FC6690028
Private mGroupID As String

'##ModelId=3E2FC67A0078
Private mProgramID As String

'##ModelId=3E2FC67E0316
Private mLoggingIn As Boolean

'##ModelId=3E2FC68B024E
Private mAutoLoggedOut As Boolean

'##ModelId=3E2FC6920046
Private mStartTime As String

'##ModelId=3E2FC69D006E
Private mEndTime As String

'##ModelId=3E2FC6A0015E
Private mEndDate As Date

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

Public Function Login() As Boolean

    On Error GoTo errHandler
    
    mEmployeeID = m_oSession.UserID
    mWorkstationID = m_oSession.CurrentEnterprise.IEnterprise_WorkstationID
    mGroupID = "00"
    mProgramID = "00"
    mLoggingIn = True
    mStartTime = Format$(Time, "hhnnss")
    mSystemDate = Date
    mEndTime = "000000"
    mAutoLoggedOut = False
    
    Call IBo_SaveIfNew
        
    Login = True
    
    Exit Function
    
errHandler:
    Call DebugMsg(MODULE_NAME, "Login", endlDebug, "Error: " & Err.Description)
    
    Login = False
        
End Function

Public Function LogOut() As Boolean
    
    On Error GoTo errHandler
    
    mEmployeeID = m_oSession.UserID
    mWorkstationID = m_oSession.CurrentEnterprise.IEnterprise_WorkstationID
    mGroupID = "00"
    mProgramID = "00"
    mLoggingIn = False
    mStartTime = Format$(Time, "hhnnss")
    mSystemDate = Date
    mEndTime = "000000"
    mAutoLoggedOut = False
    
    Call IBo_SaveIfNew
    
    LogOut = True
    
    Exit Function
    
errHandler:
    Call DebugMsg(MODULE_NAME, "LogOut", endlDebug, "Error: " & Err.Description)
    
    LogOut = False
    
End Function

Public Function ForceLogOut() As Boolean
    
    On Error GoTo errHandler
    
    mEmployeeID = m_oSession.UserID
    mWorkstationID = m_oSession.CurrentEnterprise.IEnterprise_WorkstationID
    mGroupID = "00"
    mProgramID = "00"
    mLoggingIn = False
    mStartTime = Format$(Time, "hhnnss")
    mSystemDate = Date
    mEndTime = "000000"
    mAutoLoggedOut = True
    
    Call IBo_SaveIfNew
    
    ForceLogOut = True
    
    Exit Function
    
errHandler:
    Call DebugMsg(MODULE_NAME, "ForceLogOut", endlDebug, "Error: " & Err.Description)
    
    ForceLogOut = False
    
End Function

Public Function StartMenuItem(ByVal strGroupID As String, ByVal strMenuItem As String) As Boolean
    
    On Error GoTo errHandler
    
    mEmployeeID = m_oSession.UserID
    mWorkstationID = m_oSession.CurrentEnterprise.IEnterprise_WorkstationID
    mGroupID = Format$(strGroupID, "00")
    mProgramID = Format$(strMenuItem, "00")
    mLoggingIn = False
    mStartTime = Format$(Time, "hhnnss")
    mSystemDate = Date
    mEndTime = "000000"
    mAutoLoggedOut = False
    
    Call IBo_SaveIfNew
    
    StartMenuItem = True
    
    Exit Function
    
errHandler:
    Call DebugMsg(MODULE_NAME, "StartMenuItem", endlDebug, "Error: " & Err.Description)
    
    StartMenuItem = False
    
End Function

Public Function EndMenuItem() As Boolean
    
    On Error GoTo errHandler
    
    mEndDate = Date
    mEndTime = Format$(Time, "hhnnss")
        
    Call IBo_SaveIfExists
    
    EndMenuItem = True
    
    Exit Function
    
errHandler:
    Call DebugMsg(MODULE_NAME, "EndMenuItem", endlDebug, "Error: " & Err.Description)
    
    EndMenuItem = False
    
End Function

'##ModelId=3E2FC7C602EE
Public Property Get EndDate() As Date
   Let EndDate = mEndDate
End Property

'##ModelId=3E2FC7C60172
Public Property Let EndDate(ByVal Value As Date)
    Let mEndDate = Value
End Property

'##ModelId=3E2FC7C502F8
Public Property Get EndTime() As String
   Let EndTime = mEndTime
End Property

'##ModelId=3E2FC7C5017C
Public Property Let EndTime(ByVal Value As String)
    Let mEndTime = Value
End Property

'##ModelId=3E2FC7C500A0
Public Property Get StartTime() As String
   Let StartTime = mStartTime
End Property

'##ModelId=3E2FC7C40302
Public Property Let StartTime(ByVal Value As String)
    Let mStartTime = Value
End Property

'##ModelId=3E2FC7C40262
Public Property Get AutoLoggedOut() As Boolean
   Let AutoLoggedOut = mAutoLoggedOut
End Property

'##ModelId=3E2FC7C400E6
Public Property Let AutoLoggedOut(ByVal Value As Boolean)
    Let mAutoLoggedOut = Value
End Property

'##ModelId=3E2FC7C4003C
Public Property Get LoggingIn() As Boolean
   Let LoggingIn = mLoggingIn
End Property

'##ModelId=3E2FC7C302DA
Public Property Let LoggingIn(ByVal Value As Boolean)
    Let mLoggingIn = Value
End Property

'##ModelId=3E2FC7C3023A
Public Property Get ProgramID() As String
   Let ProgramID = mProgramID
End Property

'##ModelId=3E2FC7C30122
Public Property Let ProgramID(ByVal Value As String)
    Let mProgramID = Value
End Property

'##ModelId=3E2FC7C30082
Public Property Get GroupID() As String
   Let GroupID = mGroupID
End Property

'##ModelId=3E2FC7C2038E
Public Property Let GroupID(ByVal Value As String)
    Let mGroupID = Value
End Property

'##ModelId=3E2FC7C202E4
Public Property Get WorkstationID() As String
   Let WorkstationID = mWorkstationID
End Property

'##ModelId=3E2FC7C20208
Public Property Let WorkstationID(ByVal Value As String)
    Let mWorkstationID = Value
End Property

'##ModelId=3E2FC7C20168
Public Property Get EmployeeID() As String
   Let EmployeeID = mEmployeeID
End Property

'##ModelId=3E2FC7C2008C
Public Property Let EmployeeID(ByVal Value As String)
    Let mEmployeeID = Value
End Property

'##ModelId=3E2FC7C103CA
Public Property Get SystemDate() As Date
   Let SystemDate = mSystemDate
End Property

'##ModelId=3E2FC7C102EE
Public Property Let SystemDate(ByVal Value As Date)
    Let mSystemDate = Value
End Property

'##ModelId=3E2FC7C10280
Public Property Get Key() As Long
   Let Key = mKey
End Property

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_ACTIVITYLOG * &H10000) + 1 To FID_ACTIVITYLOG_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cActivityLog

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_ACTIVITYLOG, FID_ACTIVITYLOG_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_ACTIVITYLOG_Key):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mKey
        Case (FID_ACTIVITYLOG_SystemDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mSystemDate
        Case (FID_ACTIVITYLOG_EmployeeID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mEmployeeID
        Case (FID_ACTIVITYLOG_WorkstationID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mWorkstationID
        Case (FID_ACTIVITYLOG_GroupID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mGroupID
        Case (FID_ACTIVITYLOG_ProgramID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mProgramID
        Case (FID_ACTIVITYLOG_LoggingIn):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mLoggingIn
        Case (FID_ACTIVITYLOG_AutoLoggedOut):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mAutoLoggedOut
        Case (FID_ACTIVITYLOG_StartTime):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mStartTime
        Case (FID_ACTIVITYLOG_EndTime):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mEndTime
        Case (FID_ACTIVITYLOG_EndDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mEndDate
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cActivityLog
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_ACTIVITYLOG

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "ActivityLog ID" & mKey & "(" & mSystemDate & ")"

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_ACTIVITYLOG_Key):           mKey = oField.ValueAsVariant
            Case (FID_ACTIVITYLOG_SystemDate):    mSystemDate = oField.ValueAsVariant
            Case (FID_ACTIVITYLOG_EmployeeID):    mEmployeeID = oField.ValueAsVariant
            Case (FID_ACTIVITYLOG_WorkstationID): mWorkstationID = oField.ValueAsVariant
            Case (FID_ACTIVITYLOG_GroupID):       mGroupID = oField.ValueAsVariant
            Case (FID_ACTIVITYLOG_ProgramID):     mProgramID = oField.ValueAsVariant
            Case (FID_ACTIVITYLOG_LoggingIn):     mLoggingIn = oField.ValueAsVariant
            Case (FID_ACTIVITYLOG_AutoLoggedOut): mAutoLoggedOut = oField.ValueAsVariant
            Case (FID_ACTIVITYLOG_StartTime):     mStartTime = oField.ValueAsVariant
            Case (FID_ACTIVITYLOG_EndTime):       mEndTime = oField.ValueAsVariant
            Case (FID_ACTIVITYLOG_EndDate):       mEndDate = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_ACTIVITYLOG_END_OF_STATIC

End Function

Public Function Interface(Optional eInterfaceType As Long) As cActivityLog

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function


