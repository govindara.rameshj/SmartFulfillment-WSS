VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cRetailOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3DE3A3DE0384"
'<CAMH>****************************************************************************************
'* Module : cRetailOptions
'* Date   : 28/11/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/EnterpriseBO/cRetailOptions.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 14/01/04 13:37 $
'* $Revision: 5 $
'* Versions:
'* 28/11/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cRetailOptions"

Implements IBo
Implements ISysBo

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

Private mID                    As String
Private mStoreNumber           As String
Private mStoreName             As String
Private mAddressLine1          As String
Private mAddressLine2          As String
Private mAddressLine3          As String
Private mVatRatesInclusive     As Boolean
Private mAccountabilityType    As String
Private mMaxValidCashierNumber As String
Private mKeepSalesPeriod       As Long
Private mEmployeeDiscount      As Double
Private mDefaultFloatAmount    As Currency
Private mMaximumCashGiven      As Currency
Private mMaxTillDrawerFloat    As Currency
Private mRRReportsDate         As String
Private mRRDateLastRun         As String
Private mVATDiscountsActive    As Boolean
Private mMaxGiftVoucherValue   As Currency
Private mRefundAuthLevelMgr    As Currency
Private mRefundAuthLevelSupv   As Currency
Private mMaximumUnauthorisedSale As Currency
Private mVATDiscountPercentage As Currency
Private mMinChangeAsGiftVoucher As Currency
Private mCollDiscLevelSupv     As Currency
Private mCollDiscLevelMgr      As Currency
Private mSystemUpload          As Boolean
Private mAutoApplyPrices       As Boolean
Private mCountryCode           As String 'Added 13/4/08 WIX1312 to hold stores country code configuration


Public Property Get ID() As String
    ID = mID
End Property

Public Property Let ID(ByVal Value As String)
    mID = Value
End Property

Public Property Get StoreNumber() As String
    StoreNumber = mStoreNumber
End Property

Public Property Let StoreNumber(ByVal Value As String)
    mStoreNumber = Value
End Property

Public Property Get StoreName() As String
    StoreName = mStoreName
End Property

Public Property Let StoreName(ByVal Value As String)
    mStoreName = Value
End Property

Public Property Get AddressLine1() As String
    AddressLine1 = mAddressLine1
End Property

Public Property Let AddressLine1(ByVal Value As String)
    mAddressLine1 = Value
End Property

Public Property Get AddressLine2() As String
    AddressLine2 = mAddressLine2
End Property

Public Property Let AddressLine2(ByVal Value As String)
    mAddressLine2 = Value
End Property

Public Property Get AddressLine3() As String
    AddressLine3 = mAddressLine3
End Property

Public Property Let AddressLine3(ByVal Value As String)
    mAddressLine3 = Value
End Property

Public Property Get VatRatesInclusive() As Boolean
    VatRatesInclusive = mVatRatesInclusive
End Property

Public Property Let VatRatesInclusive(ByVal Value As Boolean)
    mVatRatesInclusive = Value
End Property

Public Property Get AccountabilityType() As String
    AccountabilityType = mAccountabilityType
End Property

Public Property Let AccountabilityType(ByVal Value As String)
    mAccountabilityType = Value
End Property

Public Property Get MaxValidCashierNumber() As String
    MaxValidCashierNumber = mMaxValidCashierNumber
End Property

Public Property Let MaxValidCashierNumber(ByVal Value As String)
    mMaxValidCashierNumber = Value
End Property

Public Property Get KeepSalesPeriod() As Long
    KeepSalesPeriod = mKeepSalesPeriod
End Property

Public Property Let KeepSalesPeriod(ByVal Value As Long)
    mKeepSalesPeriod = Value
End Property

Public Property Get EmployeeDiscount() As Double
    EmployeeDiscount = mEmployeeDiscount
End Property

Public Property Let EmployeeDiscount(ByVal Value As Double)
    mEmployeeDiscount = Value
End Property

Public Property Get DefaultFloatAmount() As Currency
    DefaultFloatAmount = mDefaultFloatAmount
End Property

Public Property Let DefaultFloatAmount(ByVal Value As Currency)
    mDefaultFloatAmount = Value
End Property

Public Property Get MaximumCashGiven() As Currency
    MaximumCashGiven = mMaximumCashGiven
End Property

Public Property Let MaximumCashGiven(ByVal Value As Currency)
    mMaximumCashGiven = Value
End Property

Public Property Get MaxTillDrawerFloat() As Currency
    MaxTillDrawerFloat = mMaxTillDrawerFloat
End Property

Public Property Let MaxTillDrawerFloat(ByVal Value As Currency)
    mMaxTillDrawerFloat = Value
End Property

Public Property Get RRReportsDate() As String
    RRReportsDate = mRRReportsDate
End Property

Public Property Let RRReportsDate(ByVal Value As String)
    mRRReportsDate = Value
End Property

Public Property Get RRDateLastRun() As String
    RRDateLastRun = mRRDateLastRun
End Property

Public Property Let RRDateLastRun(ByVal Value As String)
    mRRDateLastRun = Value
End Property

Public Property Get VATDiscountsActive() As Boolean
    VATDiscountsActive = mVATDiscountsActive
End Property

Public Property Let VATDiscountsActive(ByVal Value As Boolean)
    mVATDiscountsActive = Value
End Property

Public Property Get MaxGiftVoucherValue() As Currency
    MaxGiftVoucherValue = mMaxGiftVoucherValue
End Property

Public Property Let MaxGiftVoucherValue(ByVal Value As Currency)
    mMaxGiftVoucherValue = Value
End Property

Public Property Get RefundAuthLevelMgr() As Currency
    RefundAuthLevelMgr = mRefundAuthLevelMgr
End Property

Public Property Let RefundAuthLevelMgr(ByVal Value As Currency)
    mRefundAuthLevelMgr = Value
End Property

Public Property Get RefundAuthLevelSupv() As Currency
    RefundAuthLevelSupv = mRefundAuthLevelSupv
End Property

Public Property Let RefundAuthLevelSupv(ByVal Value As Currency)
    mRefundAuthLevelSupv = Value
End Property

Public Property Get MaximumUnauthorisedSale() As Currency
    MaximumUnauthorisedSale = mMaximumUnauthorisedSale
End Property

Public Property Let MaximumUnauthorisedSale(ByVal Value As Currency)
    mMaximumUnauthorisedSale = Value
End Property

Public Property Get VATDiscountPercentage() As Currency
    VATDiscountPercentage = mVATDiscountPercentage
End Property

Public Property Let VATDiscountPercentage(ByVal Value As Currency)
    mVATDiscountPercentage = Value
End Property

Public Property Get MinChangeAsGiftVoucher() As Currency
    MinChangeAsGiftVoucher = mMinChangeAsGiftVoucher
End Property

Public Property Let MinChangeAsGiftVoucher(ByVal Value As Currency)
    mMinChangeAsGiftVoucher = Value
End Property

Public Property Get CollDiscLevelSupv() As Currency
    CollDiscLevelSupv = mCollDiscLevelSupv
End Property

Public Property Let CollDiscLevelSupv(ByVal Value As Currency)
    mCollDiscLevelSupv = Value
End Property

Public Property Get CollDiscLevelMgr() As Currency
    CollDiscLevelMgr = mCollDiscLevelMgr
End Property

Public Property Let CollDiscLevelMgr(ByVal Value As Currency)
    mCollDiscLevelMgr = Value
End Property

Public Property Get SystemUpload() As String
    SystemUpload = mSystemUpload
End Property

Public Property Let SystemUpload(ByVal Value As String)
    mSystemUpload = Value
End Property

Public Property Get AutoApplyPrices() As Boolean
    AutoApplyPrices = mAutoApplyPrices
End Property

Public Property Let AutoApplyPrices(ByVal Value As Boolean)
    mAutoApplyPrices = Value
End Property

Public Property Get CountryCode() As String
    CountryCode = mCountryCode
End Property

Public Property Let CountryCode(ByVal Value As String)
    mCountryCode = Value
End Property

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
Dim oRSelector  As IRowSelector
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then
        Set oRSelector = m_oSession.Root.CreateUtilityObject("CRowSelector")
        Debug.Assert (LenB(mID) <> 0)
        oRSelector.AddSelection CMP_EQUAL, GetField(FID_RETAILEROPTIONS_ID)
        oRSelector.Merge GetSelectAllRow
        ' Pass the selector to the database to get the data view
        Set oView = m_oSession.Database.GetView(oRSelector)
    Else
        Call moRowSel.Merge(GetSelectAllRow)
        ' Pass the selector to the database to get the data view
        Set oView = m_oSession.Database.GetView(moRowSel)
    End If
    
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Private Sub Class_Initialize()
    ' Th retail oprions table has only one record with Id "01", so set it here.
    mID = "01"
End Sub

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_RETAILEROPTIONS * &H10000) + 1 To FID_RETAILEROPTIONS_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cRetailOptions

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_RETAILEROPTIONS, FID_RETAILEROPTIONS_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_RETAILEROPTIONS_ID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mID
        Case (FID_RETAILEROPTIONS_StoreNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mStoreNumber
        Case (FID_RETAILEROPTIONS_StoreName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mStoreName
        Case (FID_RETAILEROPTIONS_AddressLine1):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine1
        Case (FID_RETAILEROPTIONS_AddressLine2):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine2
        Case (FID_RETAILEROPTIONS_AddressLine3):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine3
        Case (FID_RETAILEROPTIONS_VatRatesInclusive):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mVatRatesInclusive
        Case (FID_RETAILEROPTIONS_AccountabilityType):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAccountabilityType
        Case (FID_RETAILEROPTIONS_MaxValidCashierNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mMaxValidCashierNumber
        Case (FID_RETAILEROPTIONS_KeepSalesPeriod):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mKeepSalesPeriod
        Case (FID_RETAILEROPTIONS_EmployeeDiscount):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mEmployeeDiscount
        Case (FID_RETAILEROPTIONS_DefaultFloatAmount):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mDefaultFloatAmount
        Case (FID_RETAILEROPTIONS_MaximumCashGiven):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mMaximumCashGiven
        Case (FID_RETAILEROPTIONS_MaxTillDrawerFloat):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mMaxTillDrawerFloat
        Case (FID_RETAILEROPTIONS_RRReportsDate):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mRRReportsDate
        Case (FID_RETAILEROPTIONS_RRDateLastRun):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mRRDateLastRun
        Case (FID_RETAILEROPTIONS_VATDiscountsActive):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mVATDiscountsActive
        Case (FID_RETAILEROPTIONS_MaxGiftVoucherValue):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mMaxGiftVoucherValue
        Case (FID_RETAILEROPTIONS_RefundAuthLevelMgr):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mRefundAuthLevelMgr
        Case (FID_RETAILEROPTIONS_RefundAuthLevelSupv):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mRefundAuthLevelSupv
        Case (FID_RETAILEROPTIONS_MaximumUnauthorisedSale):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mMaximumUnauthorisedSale
        Case (FID_RETAILEROPTIONS_VATDiscountPercentage):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mVATDiscountPercentage
        Case (FID_RETAILEROPTIONS_MinChangeAsGiftVoucher):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mMinChangeAsGiftVoucher
        Case (FID_RETAILEROPTIONS_CollDiscLevelSupv):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mCollDiscLevelSupv
        Case (FID_RETAILEROPTIONS_CollDiscLevelMgr):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mCollDiscLevelMgr
        Case (FID_RETAILEROPTIONS_SystemUpload):
                Set GetField = New CFieldString
                If (mSystemUpload = True) Then
                    GetField.ValueAsVariant = "Y"
                Else
                    GetField.ValueAsVariant = "N"
                End If
        Case (FID_RETAILEROPTIONS_AutoApplyPrices):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mAutoApplyPrices
        Case (FID_RETAILEROPTIONS_CountryCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCountryCode
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cRetailOptions
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_RETAILEROPTIONS

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Retailer Options " & mStoreNumber

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_RETAILEROPTIONS_ID):                    mID = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_StoreNumber):           mStoreNumber = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_StoreName):             mStoreName = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_AddressLine1):          mAddressLine1 = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_AddressLine2):          mAddressLine2 = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_AddressLine3):          mAddressLine3 = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_VatRatesInclusive):     mVatRatesInclusive = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_AccountabilityType):    mAccountabilityType = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_MaxValidCashierNumber): mMaxValidCashierNumber = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_KeepSalesPeriod):       mKeepSalesPeriod = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_EmployeeDiscount):      mEmployeeDiscount = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_DefaultFloatAmount):    mDefaultFloatAmount = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_MaximumCashGiven):      mMaximumCashGiven = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_MaxTillDrawerFloat):    mMaxTillDrawerFloat = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_RRReportsDate):         mRRReportsDate = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_RRDateLastRun):         mRRDateLastRun = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_VATDiscountsActive):    mVATDiscountsActive = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_MaxGiftVoucherValue):   mMaxGiftVoucherValue = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_RefundAuthLevelMgr):    mRefundAuthLevelMgr = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_RefundAuthLevelSupv):   mRefundAuthLevelSupv = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_MaximumUnauthorisedSale): mMaximumUnauthorisedSale = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_VATDiscountPercentage): mVATDiscountPercentage = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_MinChangeAsGiftVoucher): mMinChangeAsGiftVoucher = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_CollDiscLevelSupv):     mCollDiscLevelSupv = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_CollDiscLevelMgr):      mCollDiscLevelMgr = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_SystemUpload):          mSystemUpload = (oField.ValueAsVariant = "Y")
            Case (FID_RETAILEROPTIONS_AutoApplyPrices):       mAutoApplyPrices = oField.ValueAsVariant
            Case (FID_RETAILEROPTIONS_CountryCode):           mCountryCode = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision
End Property


Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_RETAILEROPTIONS_END_OF_STATIC

End Function

Public Function Interface(Optional eInterfaceType As Long) As cRetailOptions
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function


